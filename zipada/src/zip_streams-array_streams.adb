
--with System;                            use System;

package body Zip_Streams.Array_Streams is

    ---------------------------------------------------------------------------
    -- Array_Stream: stream based on an in-memory Unbounded_String           --
    ---------------------------------------------------------------------------

    procedure Close( Str : in out Array_Stream ) is
    begin
        Str.Loc := 1;
        Delete( Str.Buf );
    end Close;

    ---------------------------------------------------------------------------

    procedure Delete( Str : in out A_Array_Stream ) is
        procedure Free is new Ada.Unchecked_Deallocation( Array_Stream'Class, A_Array_Stream );
    begin
        if Str /= null then
            Close( Str.all );
            Free( Str );
        end if;
    end Delete;

    ---------------------------------------------------------------------------

    procedure Set( Str : in out Array_Stream; Buf : in out A_SEA ) is
    begin
        Str.Close;
        Str.Buf := Buf;
        if Str.Buf /= null then
            Str.Loc := 1;
        end if;
        Buf := null;
    end Set;

    ---------------------------------------------------------------------------

    procedure Read( Stream : in out Array_Stream;
                    Item   : out Stream_Element_Array;
                    Last   : out Stream_Element_Offset ) is

--      procedure Memmove( dst, src : Address; len : Integer );
--      pragma Import( C, memmove, "memmove" );

--      len : Integer;
    begin
        -- Item is read from the stream. If (and only if) the stream is
        -- exhausted, Last will be < Item'Last. In that case, T'Read will
        -- raise an End_Error exception.
        --
        -- Cf: RM 13.13.1(8), RM 13.13.1(11), RM 13.13.2(37) and
        -- explanations by Tucker Taft
        --
        Last := Item'First - 1;

        -- if Item is empty, the following loop is skipped; if Stream.Loc
        -- is already indexing out of Stream.Unb, that value is also appropriate
        if Stream.Buf /= null then
--          if Stream.Loc <= Stream.Buf'Length then
--              if Item'Length > 256 then
--                  -- optimize transfer for larger sizes
--                  len := Integer'Min( Item'Length, Integer(Stream.Buf'Length - Stream.Loc + 1) );
--                  Memmove( Item(Item'First)'Address,
--                           Stream.Buf(Stream.Buf'First + (Stream.Loc - 1))'Address,
--                           len );
--                  Stream.Loc := Stream.Loc + Stream_Element_Offset(len);
--                  Last := Item'First + Stream_Element_Offset(len) - 1;
--              else
                    for i in Item'Range loop
                        if Stream.Loc > Stream.Buf'Length then
                            exit;
                        end if;
                        Item(i) := Stream.Buf(Stream.Buf'First + (Stream.Loc - 1));
                        Stream.Loc := Stream.Loc + 1;
                        Last := i;
                    end loop;
--              end if;
--          end if;
        else
            raise Null_Stream with "Stream backing array is null";
        end if;
    end Read;

    ---------------------------------------------------------------------------

    procedure Write( Stream : in out Array_Stream;
                     Item   : Stream_Element_Array ) is
    begin
        if Stream.Buf = null then
            raise Null_Stream with "Stream backing array is null";
        end if;

        for I in Item'Range loop
            if Stream.Loc > Stream.Buf'Length then
                raise Stream_Overflow with "Attempted write past end of stream";
            end if;
            Stream.Buf(Stream.Buf'First + (Stream.Loc - 1)) := Item(I);
            Stream.Loc := Stream.Loc + 1;
        end loop;
    end Write;

    ---------------------------------------------------------------------------

    procedure Set_Index( Str : in out Array_Stream; To : ZS_Index_Type ) is
    begin
        if Str.Buf = null then
            raise Null_Stream with "Stream backing array is null";
        end if;
        -- Allow the index to be set to one past the end of the stream, where
        -- End_Of_Stream will be True, but any further than that and someone is
        -- doing something wrong.
        if Stream_Element_Offset(To) > Str.Buf'Length + 1 then
           raise Out_Of_Bounds with "Index past end of stream";
        end if;
        Str.Loc := Stream_Element_Offset(To);
    end Set_Index;

    ---------------------------------------------------------------------------

    function Size( Str : in Array_Stream ) return ZS_Size_Type is
    begin
        if Str.Buf = null then
            return 0;
        end if;
        return Str.Buf'Length;
    end Size;

    ---------------------------------------------------------------------------

    function Index( Str : in Array_Stream ) return ZS_Index_Type is (ZS_Index_Type(Str.Loc));

    ---------------------------------------------------------------------------

    function End_Of_Stream( Str : in Array_Stream ) return Boolean is (Str.Index > Str.Size);

end Zip_Streams.Array_Streams;
