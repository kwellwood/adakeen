
with Ada.Unchecked_Deallocation;

package Zip_Streams.Array_Streams is

    type A_SEA is access all Stream_Element_Array;

    procedure Delete is new Ada.Unchecked_Deallocation( Stream_Element_Array, A_SEA );

    ---------------------------------------------------------------------
    -- Array_Stream: stream based on an in-memory Stream_Element_Array --
    ---------------------------------------------------------------------
    type Array_Stream is new Root_Zipstream_Type with private;
    type A_Array_Stream is access all Array_Stream'Class;

    -- Set a value in the stream, the index will be set
    -- to null and old data in the stream will be lost.
    procedure Set( Str : in out Array_Stream; Buf : in out A_SEA );

    -- Close the Array_Stream and delete its backing buffer.
    procedure Close( Str : in out Array_Stream );

    -- Returns true if the index is at the end of the stream.
    function End_Of_Stream( Str : in Array_Stream ) return Boolean;

    -- Returns the current index of the stream.
    function Index( Str : in Array_Stream ) return ZS_Index_Type;

   -- Set the index of the stream.
   procedure Set_Index( Str : in out Array_Stream; To : ZS_Index_Type );

   -- returns the Size of the stream
   function Size( Str : in Array_Stream ) return ZS_Size_Type;

    procedure Delete( Str : in out A_Array_Stream );

    Out_Of_Bounds,
    Null_Stream,
    Stream_Overflow : exception;

private

    -- Array Stream spec
    type Array_Stream is new Root_Zipstream_Type with
        record
            Buf : A_SEA := null;
            Loc : Stream_Element_Offset := 1;
        end record;

    -- Read data from the stream.
    procedure Read( Stream : in out Array_Stream;
                    Item   : out Stream_Element_Array;
                    Last   : out Stream_Element_Offset );

    -- write data to the stream, starting from the current index.
    -- Data will be overwritten from index.
    procedure Write( Stream : in out Array_Stream;
                     Item   : Stream_Element_Array );

end Zip_Streams.Array_Streams;
