--
-- Copyright (c) 2013-2014 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Assertions;                    use Ada.Assertions;
with Ada.Exceptions;                    use Ada.Exceptions;
with Ada.Streams;                       use Ada.Streams;
with Ada.Streams.Stream_IO;             use Ada.Streams.Stream_IO;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Ada.Text_IO;
with Streams.Buffers;                   use Streams.Buffers;
with Scribble.Tokens;                   use Scribble.Tokens;
with Scribble.Tokenizers;               use Scribble.Tokenizers;

package body Scribble.Tests.Tokenizer_Test is

    procedure Test is

        ----------------------------------------------------------------------------

        function Expect( t : not null A_Tokenizer; tt : Token_Type ) return Boolean is
            token : constant Token_Ptr := t.Next;
        begin
            if token.Get.Get_Type /= tt then
                Ada.Text_IO.Put_Line( "Expected " & Token_Type'Image( tt ) &
                                      ", found " & Token_Type'Image( token.Get.Get_Type ) );
            end if;
            return token.Get.Get_Type = tt;
        end Expect;

        ----------------------------------------------------------------------------

        function Dont_Expect( t : not null A_Tokenizer; tt : Token_Type ) return Boolean is
            token : constant Token_Ptr := t.Next;
        begin
            if token.Get.Get_Type = tt then
                Ada.Text_IO.Put_Line( "Unexpected " & Token_Type'Image( tt ) );
            end if;
            return token.Get.Get_Type /= tt;
        end Dont_Expect;

        ----------------------------------------------------------------------------

        function Expect_Exception( t : not null A_Tokenizer; msg : String ) return Boolean is
            token : Token_Ptr;
        begin
            token := t.Next;
            Ada.Text_IO.Put_Line( "Expected exception '" & msg & "', found " & token.Get.Get_Type'Img );
            return False;
        exception
            when e : Token_Error =>
                if msg /= Exception_Message( e ) then
                    Ada.Text_IO.Put_Line( "Expected exception '" & msg & "', " &
                                          "found exception '" & Exception_Message( e ) & "'" );
                end if;
                return msg = Exception_Message( e );
        end Expect_Exception;

        ----------------------------------------------------------------------------

        -- Tests that a simple empty stream returns an EOF token.
        procedure Test_Empty_Stream is
            t : A_Tokenizer := Create_Tokenizer;
            s : A_Buffer_Stream;
        begin
            -- test with no input stream
            Assert( Expect( t, TK_EOF ) );

            -- test an empty string
            s := Stream( "" );
            t.Set_Input( s );
            Assert( Expect( t, TK_EOF ) );
            t.Set_Input( null );
            Close( s );
            Delete( t );
        end Test_Empty_Stream;

        ----------------------------------------------------------------------------

        -- Tests handling of unexpected ASCII characters.
        procedure Test_Bad_Characters is
            t : A_Tokenizer := Create_Tokenizer;
            s : A_Buffer_Stream;
        begin
            s := Stream( "$" );
            t.Set_Input( s );
            Assert( Expect_Exception( t, "Unrecognized token at 1:1" ) );
            t.Set_Input( null );
            Close( s );

            s := Stream( "`" );
            t.Set_Input( s );
            Assert( Expect_Exception( t, "Unrecognized token at 1:1" ) );
            t.Set_Input( null );
            Close( s );

            s := Stream( String'(1=>ASCII.BEL) );
            t.Set_Input( s );
            Assert( Expect_Exception( t, "Unrecognized token at 1:1" ) );
            t.Set_Input( null );
            Close( s );

            s := Stream( String'(1=>ASCII.NUL) );
            t.Set_Input( s );
            Assert( Expect_Exception( t, "Unrecognized token at 1:1" ) );
            t.Set_Input( null );
            Close( s );

            s := Stream( String'(1=>Character'Val( 255 )) );
            t.Set_Input( s );
            Assert( Expect_Exception( t, "Unrecognized token at 1:1" ) );
            t.Set_Input( null );
            Close( s );

            Delete( t );
        end Test_Bad_Characters;

        ----------------------------------------------------------------------------

        -- Tests reading an arbitrary series of simple tokens without whitespace.
        procedure Test_Simple_tokens is
            type Token_Array is array (Integer range <>) of Token_Type;
            t : A_Tokenizer := Create_Tokenizer;
            s : A_Buffer_Stream;
        begin
            -- try to identify all the tokens in a string with minimal whitespace
            declare
                expected : constant Token_Array := (
                    TK_CONSTANT, TK_FUNCTION, TK_RETURN, TK_BLOCK, TK_ELSIF, TK_WHILE, TK_ELSE, TK_EXIT, TK_LOOP,
                    TK_SELF, TK_STEP, TK_THEN, TK_WHEN, TK_AND, TK_END, TK_FOR, TK_VAR, TK_XOR, TK_IF,
                    TK_IN, TK_IS, TK_OR, TK_TO, TK_DOT, TK_COLON_EQUALS, TK_PLUS_EQUALS, TK_MINUS_EQUALS,
                    TK_STAR_EQUALS, TK_SLASH_EQUALS, TK_AMP_EQUALS, TK_GREATER_EQUALS, TK_LESS_EQUALS,
                    TK_BANG_EQUALS, TK_EQUALS, TK_GREATER, TK_LESS, TK_MINUS, TK_DOUBLE_LESS, TK_DOUBLE_GREATER,
                    TK_BANG, TK_STAR, TK_SLASH, TK_PERCENT, TK_PLUS, TK_AMP, TK_CARAT, TK_QUESTION, TK_DOUBLE_COLON, TK_COLON,
                    TK_SEMICOLON, TK_LPAREN, TK_RPAREN, TK_LBRACE, TK_RBRACE, TK_LBRACKET, TK_RBRACKET, TK_COMMA
                );
            begin
                s := Stream(
                    "constant function return block elsif while else exit loop " &
                    "self step then when and end for var xor if " &
                    "in is or to.:=+=-=" &
                    "*=/=&=>=<=" &
                    "!==><-<<>>" &
                    "!*/%+&^?:::" &
                    ";(){}[],"
                );
                t.Set_Input( s );
                for i in expected'Range loop
                    Assert( Expect( t, expected(i) ) );
                end loop;
                Assert( Expect( t, TK_EOF ) );
                t.Set_Input( null );
                Close( s );
            end;

            -- mix up the order and test again
            declare
                expected : constant Token_Array := (
                    TK_WHILE, TK_TO, TK_IS, TK_BLOCK, TK_LOOP, TK_ELSIF, TK_CARAT, TK_VAR, TK_STAR_EQUALS,
                    TK_SLASH_EQUALS, TK_LESS_EQUALS, TK_IN, TK_SLASH, TK_MINUS, TK_STAR, TK_AND, TK_SEMICOLON,
                    TK_SELF, TK_EQUALS, TK_PLUS_EQUALS, TK_GREATER, TK_BANG, TK_BANG_EQUALS, TK_DOUBLE_LESS,
                    TK_LESS, TK_DOUBLE_GREATER, TK_GREATER_EQUALS, TK_FUNCTION, TK_LPAREN, TK_RETURN, TK_COLON,
                    TK_ELSE, TK_AMP, TK_AMP_EQUALS, TK_RBRACKET, TK_THEN, TK_LBRACKET, TK_QUESTION, TK_COMMA,
                    TK_XOR, TK_RPAREN, TK_OR, TK_MINUS_EQUALS, TK_COLON_EQUALS, TK_PLUS, TK_EXIT, TK_PERCENT,
                    TK_RBRACE, TK_END, TK_LBRACE, TK_IF, TK_DOT, TK_WHEN, TK_DOUBLE_COLON, TK_FOR, TK_STEP, TK_CONSTANT
                );
            begin
                s := Stream(
                    "while to is block loop elsif^var*=" &
                    "/=<=in/-*and;" &
                    "self=+=>!!=<<" &
                    "<>>>=function(return:" &
                    "else&&=]then[?," &
                    "xor)or-=:=+exit%" &
                    "}end{if.when::for step constant"
                );
                t.Set_Input( s );
                for i in expected'Range loop
                    Assert( Expect( t, expected(i) ) );
                end loop;
                Assert( Expect( t, TK_EOF ) );
                t.Set_Input( null );
                Close( s );
            end;

            Delete( t );
        end Test_Simple_tokens;

        ----------------------------------------------------------------------------

        -- Tests parsing string tokens.
        procedure Test_Strings is
            t : A_Tokenizer := Create_Tokenizer;
            s : A_Buffer_Stream;
        begin
            -- Test: "Hello, World!"
            s := Stream( """Hello, World!""" );
            t.Set_Input( s );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_EOF ) );
            t.Set_Input( null );
            Close( s );

            -- Test:  "Hello, World!"       (note the whitespace around the double quotes)
            s := Stream( " ""Hello, World!"" " );
            t.Set_Input( s );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_EOF ) );
            t.Set_Input( null );
            Close( s );

            -- Test: "\"Hello, World!\""    (test escaping double quotes in the string)
            s := Stream( """\""Hello, World!\""""" );
            t.Set_Input( s );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_EOF ) );
            t.Set_Input( null );
            Close( s );

            -- test escaping characters: newline, tab, backslash
            -- Test: "\n\t\\"
            s := Stream( """\n\t\\""" );
            t.Set_Input( s );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_EOF ) );
            t.Set_Input( null );
            Close( s );

            -- test a bad escape sequence
            s := Stream( """\k""" );
            t.Set_Input( s );
            Assert( Expect_Exception( t, "Invalid escape sequence at 1:3" ) );
            t.Set_Input( null );
            Close( s );

            -- test unterminated string
            s := Stream( """Hello, World" );
            t.Set_Input( s );
            Assert( Expect_Exception( t, "Unterminated string at 1:1" ) );
            t.Set_Input( null );
            Close( s );

            -- test unterminated string with line feed terminator
            s := Stream( """Hello, World" & ASCII.LF & '"' );
            t.Set_Input( s );
            Assert( Expect_Exception( t, "Unterminated string at 1:1" ) );
            t.Set_Input( null );
            Close( s );

            -- test unterminated string with line feed terminator and dangling escape character
            s := Stream( """Hello, World\" & ASCII.LF & '"' );
            t.Set_Input( s );
            Assert( Expect_Exception( t, "Unterminated string at 1:1" ) );
            t.Set_Input( null );
            Close( s );

            -- test unterminated string with dangling escape character
            s := Stream( """Hello, World\" );
            t.Set_Input( s );
            Assert( Expect_Exception( t, "Unterminated string at 1:1" ) );
            t.Set_Input( null );
            Close( s );

            Delete( t );
        end Test_Strings;

        ----------------------------------------------------------------------------

        -- Tests parsing Boolean keywords.
        procedure Test_Booleans is
            t : A_Tokenizer := Create_Tokenizer;
            s : A_Buffer_Stream;
        begin
            -- test boolean true and false tokens
            s := Stream( " false true " );
            t.Set_Input( s );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_EOF ) );
            t.Set_Input( null );
            Close( s );

            -- test boolean token ended by EOF
            s := Stream( "false" );
            t.Set_Input( s );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_EOF ) );
            t.Set_Input( null );
            Close( s );

            -- test boolean token ended by a delimiter/whitespace
            s := Stream( "false " );
            t.Set_Input( s );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_EOF ) );
            t.Set_Input( null );
            Close( s );

            -- test boolean token ended by a new-line character
            s := Stream( "true" & ASCII.LF );
            t.Set_Input( s );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_EOF ) );
            t.Set_Input( null );
            Close( s );

            -- test boolean token with an invalid character at the end
            s := Stream( "trueish" );
            t.Set_Input( s );
            Assert( Dont_Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_EOF ) );
            t.Set_Input( null );
            Close( s );

            -- test boolean token with a delimiter at the end
            s := Stream( "false." );
            t.Set_Input( s );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_DOT ) );
            Assert( Expect( t, TK_EOF ) );
            t.Set_Input( null );
            Close( s );

            -- test boolean token with a digit at the end
            s := Stream( "true1" );
            t.Set_Input( s );
            Assert( Dont_Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_EOF ) );
            t.Set_Input( null );
            Close( s );

            Delete( t );
        end Test_Booleans;

        ----------------------------------------------------------------------------

        -- Tests parsing the Null keyword.
        procedure Test_Null is
            t : A_Tokenizer := Create_Tokenizer;
            s : A_Buffer_Stream;
        begin
            -- test null token ended by EOF
            s := Stream( "null" );
            t.Set_Input( s );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_EOF ) );
            t.Set_Input( null );
            Close( s );

            -- test null token ended by a delimiter/whitespace
            s := Stream( "null " );
            t.Set_Input( s );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_EOF ) );
            t.Set_Input( null );
            Close( s );

            -- test null token ended by a new-line character
            s := Stream( "null" & ASCII.LF );
            t.Set_Input( s );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_EOF ) );
            t.Set_Input( null );
            Close( s );

            -- test null token with an invalid character at the end
            s := Stream( "nullified" );
            t.Set_Input( s );
            Assert( Dont_Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_EOF ) );
            t.Set_Input( null );
            Close( s );

            -- test null token with a non-whitespace delimiter at the end
            s := Stream( "null.value" );
            t.Set_Input( s );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_DOT ) );
            Assert( Dont_Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_EOF ) );
            t.Set_Input( null );
            Close( s );

            -- test null token without all its letters
            s := Stream( "nul" );
            t.Set_Input( s );
            Assert( Dont_Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_EOF ) );
            t.Set_Input( null );
            Close( s );

            Delete( t );
        end Test_Null;

        ----------------------------------------------------------------------------

        -- Tests parsing symbols.
        procedure Test_Symbols is
            t : A_Tokenizer := Create_Tokenizer;
            s : A_Buffer_Stream;
        begin
            -- test symbols ended by space, delimiters, new-line and EOF
            s := Stream( "a b-c" & ASCII.LF & "d" );
            t.Set_Input( s );
            Assert( Expect( t, TK_SYMBOL ) );
            Assert( Expect( t, TK_SYMBOL ) );
            Assert( Dont_Expect( t, TK_SYMBOL ) );
            Assert( Expect( t, TK_SYMBOL ) );
            Assert( Expect( t, TK_SYMBOL ) );
            Assert( Expect( t, TK_EOF ) );
            t.Set_Input( null );
            Close( s );

            -- test various symbols
            s := Stream( "a a_ a1 a_a a_1 a__a a1_ a1__ abc " );
            t.Set_Input( s );
            Assert( Expect( t, TK_SYMBOL ) );
            Assert( Expect( t, TK_SYMBOL ) );
            Assert( Expect( t, TK_SYMBOL ) );
            Assert( Expect( t, TK_SYMBOL ) );
            Assert( Expect( t, TK_SYMBOL ) );
            Assert( Expect( t, TK_SYMBOL ) );
            Assert( Expect( t, TK_SYMBOL ) );
            Assert( Expect( t, TK_SYMBOL ) );
            Assert( Expect( t, TK_SYMBOL ) );
            Assert( Expect( t, TK_EOF ) );
            t.Set_Input( null );
            Close( s );

            -- test a symbol that's not valid
            s := Stream( "ca$h" );
            t.Set_Input( s );
            Assert( Expect_Exception( t, "Invalid symbol at 1:1" ) );
            t.Set_Input( null );
            Close( s );

            -- test a symbol that's not valid
            s := Stream( "_a" );
            t.Set_Input( s );
            Assert( Expect_Exception( t, "Unrecognized token at 1:1" ) );
            t.Set_Input( null );
            Close( s );

            -- test a symbol that's not valid
            s := Stream( "1a" );
            t.Set_Input( s );
            Assert( Expect_Exception( t, "Invalid number at 1:1" ) );
            t.Set_Input( null );
            Close( s );

            Delete( t );
        end Test_Symbols;

        ----------------------------------------------------------------------------

        -- Tests numbers.
        procedure Test_Numbers is
            t : A_Tokenizer := Create_Tokenizer;
            s : A_Buffer_Stream;
        begin
            -- test an integer number ended by whitespace, a delimiter and eof
            s := Stream( "1 2" & ASCII.LF & "3+4" );
            t.Set_Input( s );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_PLUS ) );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_EOF ) );
            t.Set_Input( null );
            Close( s );

            -- test fractional numbers
            s := Stream( "1.234 5.6 " );
            t.Set_Input( s );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_EOF ) );
            t.Set_Input( null );
            Close( s );

            -- test negative numbers
            s := Stream( "-1-2.5-.1-" );
            t.Set_Input( s );
            Assert( Expect( t, TK_MINUS ) );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_MINUS ) );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_MINUS ) );
            Assert( Expect( t, TK_DOT ) );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_MINUS ) );
            Assert( Expect( t, TK_EOF ) );
            t.Set_Input( null );
            Close( s );

            -- test invalid numbers
            s := Stream( "1. " );
            t.Set_Input( s );
            Assert( Expect_Exception( t, "Invalid number at 1:1" ) );
            t.Set_Input( null );
            Close( s );

            -- test invalid numbers
            s := Stream( ".1 " );
            t.Set_Input( s );
            Assert( Expect( t, TK_DOT ) );
            Assert( Expect( t, TK_VALUE ) );
            t.Set_Input( null );
            Close( s );

            -- test invalid numbers
            s := Stream( "1a " );
            t.Set_Input( s );
            Assert( Expect_Exception( t, "Invalid number at 1:1" ) );
            t.Set_Input( null );
            Close( s );

            -- test invalid numbers
            s := Stream( "1.a " );
            t.Set_Input( s );
            Assert( Expect_Exception( t, "Invalid number at 1:1" ) );
            t.Set_Input( null );
            Close( s );

            -- test invalid numbers
            s := Stream( "1.+" );
            t.Set_Input( s );
            Assert( Expect_Exception( t, "Invalid number at 1:1" ) );
            t.Set_Input( null );
            Close( s );

            -- test invalid numbers
            s := Stream( "1.0a " );
            t.Set_Input( s );
            Assert( Expect_Exception( t, "Invalid number at 1:1" ) );
            t.Set_Input( null );
            Close( s );

            -- test the termination of numbers by end of line
            s := Stream( "1" & ASCII.LF & "1.0" & ASCII.LF & ".1" & ASCII.LF );
            t.Set_Input( s );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_DOT ) );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_EOF ) );
            t.Set_Input( null );
            Close( s );

            -- test the termination of numbers by end of file
            s := Stream( "1" );
            t.Set_Input( s );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_EOF ) );
            t.Set_Input( null );
            Close( s );

            -- test the termination of numbers by end of file
            s := Stream( "1.0" );
            t.Set_Input( s );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_EOF ) );
            t.Set_Input( null );
            Close( s );

            Delete( t );
        end Test_Numbers;

        ----------------------------------------------------------------------------

        -- Tests parsing whitespace between tokens.
        procedure Test_Whitespace is
            t : A_Tokenizer := Create_Tokenizer;
            s : A_Buffer_Stream;
        begin
            s := Stream( """abc""abc - " & ASCII.LF & "+   2" &
                                          ASCII.LF & ASCII.LF & "3" & ASCII.HT & "4  " &
                                          ASCII.LF & ASCII.LF & ASCII.HT & ASCII.LF &
                                          " 5" & ASCII.LF );
            t.Set_Input( s );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_SYMBOL ) );
            Assert( Expect( t, TK_MINUS ) );
            Assert( Expect( t, TK_PLUS ) );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_EOF ) );
            t.Set_Input( null );
            Close( s );
            Delete( t );
        end Test_Whitespace;

        ----------------------------------------------------------------------------

        -- Tests parsing comments between whitespace and tokens.
        procedure Test_Comments is
            t   : A_Tokenizer := Create_Tokenizer;
            s   : A_Buffer_Stream;
            str : Unbounded_String;
        begin
            str := To_Unbounded_String( "/// comment" & ASCII.LF & "1//" );
            for i in 1..1100 loop
                str := str & "-";
            end loop;
            str := str & ASCII.LF;
            str := str & "//line 2 comment!@#$^ " & ASCII.HT & ASCII.HT & " &%*&*(&*(_*\634";
            for i in 1..250 loop
                str := str & "// " & ASCII.LF;
            end loop;
            str := str & "123//next comment" & ASCII.LF & "+";

            s := Stream( To_String( str ) );
            t.Set_Input( s );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_VALUE ) );
            Assert( Expect( t, TK_PLUS ) );
            Assert( Expect( t, TK_EOF ) );
            t.Set_Input( null );
            Close( s );
            Delete( t );
        end Test_Comments;

        ----------------------------------------------------------------------------

    begin
        Test_Empty_Stream;      Ada.Text_IO.Put( "." );
        Test_Bad_Characters;    Ada.Text_IO.Put( "." );
        Test_Simple_tokens;     Ada.Text_IO.Put( "." );
        Test_Strings;           Ada.Text_IO.Put( "." );
        Test_Booleans;          Ada.Text_IO.Put( "." );
        Test_Null;              Ada.Text_IO.Put( "." );
        Test_Symbols;           Ada.Text_IO.Put( "." );
        Test_Numbers;           Ada.Text_IO.Put( "." );
        Test_Whitespace;        Ada.Text_IO.Put( "." );
        Test_Comments;          Ada.Text_IO.Put( "." );

        Ada.Text_IO.Put_Line( ": tokenizer tests OK" );
    exception
        when e : others =>
            Ada.Text_IO.Put_Line( ": tokenizer test FAILED" );
            raise;      -- yes this will crash; use the debugger to catch it
    end Test;

end Scribble.Tests.Tokenizer_Test;
