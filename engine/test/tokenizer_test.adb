--
-- Copyright (c) 2012 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Exceptions;                    use Ada.Exceptions;
with Ada.Streams;                       use Ada.Streams;
with Ada.Streams.Stream_IO;             use Ada.Streams.Stream_IO;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Ada.Text_IO;
with Streams.Buffers;                   use Streams.Buffers;
with Tokens;                            use Tokens;
with Tokens.Tokenizers;                 use Tokens.Tokenizers;

procedure Tokenizer_Test is
    tokenizer : A_Tokenizer := Create_Tokenizer;
    stream    : A_Buffer_Stream;
    token     : A_Token;
    result    : Unbounded_String;
    failed    : Boolean := False;

    type Test_Case is
        record
            input  : String_Access;
            expect : String_Access;
        end record;

    type Test_Array is array(Positive range <>) of Test_Case;

    tests : Test_Array := Test_Array'(
        (new String'("&&>=<=!=||!,;/=>(<-%*+)"), new String'("TK_AND TK_GREATER_EQUALS TK_LESS_EQUALS TK_NOT_EQUALS TK_OR TK_BANG TK_COMMA TK_SEMICOLON TK_SLASH TK_EQUALS TK_GREATER TK_LEFT_PARENTHESIS TK_LESS TK_MINUS TK_PERCENT TK_STAR TK_PLUS TK_RIGHT_PARENTHESIS TK_EOF")),
        (new String'("Kevin"), new String'("TK_IDENTIFIER TK_EOF")),

        -- testing strings
        (new String'("""Hello, World"""), new String'("TK_STRING TK_EOF")),
        (new String'(" ""Hello, World"" "), new String'("TK_STRING TK_EOF")),
        (new String'("""\""Hello, World\"""""), new String'("TK_STRING TK_EOF")),
        (new String'(" ""\""Hello, World\"""" "), new String'("TK_STRING TK_EOF")),
        (new String'("""\n\t\\"""), new String'("TK_STRING TK_EOF")),
        (new String'(" ""\kevin\wellwood"" "), new String'("Token_Exception(Invalid escape sequence at 1:5)")),
        (new String'("""Hello"), new String'("Token_Exception(Unterminated string at 1:1)")),
        (new String'("""Hello" & ASCII.LF & """"), new String'("Token_Exception(Unterminated string at 1:1)")),
        (new String'("""Hello\" & ASCII.LF & """"), new String'("Token_Exception(Unterminated string at 1:1)")),
        (new String'("""Hello\"), new String'("Token_Exception(Unterminated string at 1:1)")),

        -- testing numbers
        (new String'("1"), new String'("TK_NUMBER TK_EOF")),
        (new String'("123.456"), new String'("TK_NUMBER TK_EOF")),
        (new String'(" 123.456 3 "), new String'("TK_NUMBER TK_NUMBER TK_EOF")),
        (new String'(" 2 "), new String'("TK_NUMBER TK_EOF")),
        (new String'("1."), new String'("TK_NUMBER TK_EOF")),

        -- testing identifiers
        (new String'("kevin"), new String'("TK_IDENTIFIER TK_EOF")),
        (new String'("kevin123"), new String'("TK_IDENTIFIER TK_EOF")),
        (new String'("kev1n"), new String'("TK_IDENTIFIER TK_EOF")),
        (new String'(" kevin "), new String'("TK_IDENTIFIER TK_EOF")),
        (new String'(" _kevin "), new String'("Token_Exception(Unrecognized token '_kevin' at 1:2)")),
        (new String'("kevin__wellwood"), new String'("TK_IDENTIFIER TK_EOF")),
        (new String'("kevin.wellwood"), new String'("TK_IDENTIFIER TK_EOF")),
        (new String'("kevin wellwood"), new String'("TK_IDENTIFIER TK_IDENTIFIER TK_EOF")),
        (new String'("This_is_a_really_long_string_that_will_increase_the_size_of_the_token_buffer"), new String'("TK_IDENTIFIER TK_EOF")),

        -- testing comments
        (new String'("// comment!"), new String'("TK_EOF")),
        (new String'("/* block comment */"), new String'("TK_EOF")),
        (new String'("// comment then a /* block comment */"), new String'("TK_EOF")),
        (new String'("//// two line" & ASCII.LF & " // comments"), new String'("TK_EOF")),
        (new String'("123// comment"), new String'("TK_NUMBER TK_EOF")),
        (new String'("abc// comment"), new String'("TK_IDENTIFIER TK_EOF")),
        (new String'("""kevin""// comment"), new String'("TK_STRING TK_EOF")),
        (new String'("/ // comment"), new String'("TK_SLASH TK_EOF")),
        (new String'("abc/**/def"), new String'("TK_IDENTIFIER TK_IDENTIFIER TK_EOF"))
    );

begin
    for i in tests'Range loop
        result := To_Unbounded_String( "" );
        stream := Streams.Buffers.Stream( tests(i).input.all );
        tokenizer.Set_Input( Stream_Access(stream) );
        begin
            loop
                token := tokenizer.Get_Next;
                result := result & token.Get_Type'Img & ' ';
                exit when token.Get_Type = TK_EOF;
            end loop;
        exception
            when e : Token_Exception =>
                result := result & "Token_Exception(" & Exception_Message( e ) & ")";
        end;
        tokenizer.Set_Input( null );
        Close( stream );
        Trim( result, Right );
        if To_String( result ) /= tests(i).expect.all then
            Ada.Text_IO.Put_Line( "Test" & i'Img & ": Failed" );
            Ada.Text_IO.Put_Line( "  Expected: " & tests(i).expect.all );
            Ada.Text_IO.Put_Line( "  Actual  : " & To_String( result ) );
            failed := True;
        end if;

        Free( tests(i).input );
        Free( tests(i).expect );
    end loop;
    Delete( tokenizer );

    if not failed then
        Ada.Text_IO.Put_Line( "All tests passed." );
    end if;
end Tokenizer_Test;
