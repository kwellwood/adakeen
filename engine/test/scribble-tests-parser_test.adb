--
-- Copyright (c) 2013-2014 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Assertions;
with Ada.Exceptions;                    use Ada.Exceptions;
with Ada.Streams;                       use Ada.Streams;
with Ada.Streams.Stream_IO;             use Ada.Streams.Stream_IO;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Ada.Text_IO;
with GNAT.Source_Info;
with Streams.Buffers;                   use Streams.Buffers;
with Scribble.Ast;                      use Scribble.Ast;
with Scribble.Ast.Expressions;          use Scribble.Ast.Expressions;
with Scribble.Ast.Functions;            use Scribble.Ast.Functions;
with Scribble.Parsers;                  use Scribble.Parsers;
with Scribble.Runtimes;                 use Scribble.Runtimes;
with Scribble.Scanners;                 use Scribble.Scanners;
with Scribble.Tests.Ast_Validators;     use Scribble.Tests.Ast_Validators;

package body Scribble.Tests.Parser_Test is

    procedure Test is
        p : A_Parser;
        s : A_Buffer_Stream;
        r : A_Scribble_Runtime := Create_Scribble_Runtime;

        ----------------------------------------------------------------------------

        procedure Setup is
        begin
            Delete( p );
            p := Create_Parser( r );
            Close( s );
            s := Stream( "" );
        end Setup;

        ----------------------------------------------------------------------------

        procedure Set_Input( str : String ) is
        begin
            Close( s );
            s := Stream( str );
        end Set_Input;

        ----------------------------------------------------------------------------

        function U( str : String ) return Unbounded_String renames To_Unbounded_String;

        ----------------------------------------------------------------------------

        procedure Assert( cond : Boolean; loc : String := GNAT.Source_Info.Source_Location ) is
        begin
            Ada.Assertions.Assert( cond, loc );
        end Assert;

        ----------------------------------------------------------------------------

        procedure Expect_Expression is
            e : A_Ast_Expression;
        begin
            e := p.Expect_Expression( s );  -- raises exception if expression not found
            Delete( A_Ast_Node(e) );
        end Expect_Expression;

        ----------------------------------------------------------------------------

        function Validate( expected : String_Array ) return Boolean is
            expr      : A_Ast_Expression;
            validator : A_Ast_Validator := Create_Ast_Validator;
            result    : Boolean;
        begin
            expr := p.Expect_Expression( s );
            result := validator.Validate( A_Ast_Node(expr), expected );
            Delete( A_Ast_Node(expr) );
            Delete( validator );
            return result;
        exception
            when others =>
                Delete( validator );
                raise;
        end Validate;

        ----------------------------------------------------------------------------

        function Validate_Function( expected : String_Array ) return Boolean is
            expr      : A_Ast_Node;
            validator : A_Ast_Validator := Create_Ast_Validator;
            result    : Boolean;
        begin
            -- raises a Parse_Error if 'ss' has syntax errors
            expr := A_Ast_Node(p.Scan_Function_Definition( s ));
            if expr = null then
                raise Parse_Error with "Expected function definition";
            end if;
            result := validator.Validate( expr, expected );
            Delete( expr );
            Delete( validator );
            return result;
        exception
            when others =>
                Delete( validator );
                raise;
        end Validate_Function;

        ----------------------------------------------------------------------------

        procedure Test_Empty_Input is
        begin
            Set_Input( "" );
            Assert( p.Scan_Expression( s ) = null );
            begin
                Expect_Expression;              -- Parse_Error expected
                Assert( False );
            exception
                when Parse_Error =>
                    null;
            end;

            -- test an empty function definition
            Set_Input( " // comment" );
            Assert( p.Scan_Function_Definition( s ) = null );
        end Test_Empty_Input;

        ----------------------------------------------------------------------------

        procedure Test_Operands is
        begin
            Set_Input( " true " );
            Assert( Validate( (1=>U("literal")) ) );

            Set_Input( " null " );
            Assert( Validate( (1=>U("literal")) ) );

            Set_Input( " 3.0 " );
            Assert( Validate( (1=>U("literal")) ) );

            Set_Input( " ""Hello, World!"" " );
            Assert( Validate( (1=>U("literal")) ) );

            Set_Input( " symbol " );
            Assert( Validate( (1=>U("symbol")) ) );

            -- test an invalid number/symbol
            Set_Input( " 4symbol " );
            begin
                Expect_Expression;
                Assert( False );
            exception when Parse_Error => null;
            end;

            -- a local symbol as a function call
            Set_Input( " symbol() " );
            Assert( Validate( (U("symbol"), U("("), U(")")) ) );

            -- a built-in function name
            Set_Input( " keys() " );
            Assert( Validate( (U("keys"), U("("), U(")")) ) );

            -- a static namespace name
            Set_Input( " namespace::name" );
            Assert( Validate( (1=>U("namespace::name")) ) );

            -- test namespace, static and dynamic
            Set_Input( " namespace::[""name""]" );
            Assert( Validate( (U("namespace"), U("::["), U("literal"), U("]")) ) );

            begin
                -- test using a function definition as an operand
                Set_Input( " function():impl ");
                Expect_Expression;
                Assert( False );
            exception when Parse_Error => null;
            end;
        end Test_Operands;

        ----------------------------------------------------------------------------

        procedure Test_Function_Calls is
        begin
            Set_Input( " foo(true) " );
            Assert( Validate( (U("foo"), U("("), U("literal"), U(")")) ) );

            Set_Input( " foo(bar := true) " );
            Assert( Validate( (U("foo"), U("("), U(":="), U("bar"), U("literal"), U(")")) ) );

            Set_Input( " foo(true, ""false"", 1) " );
            Assert( Validate( (U("foo"), U("("), U("literal"), U(","), U("literal"), U(","), U("literal"), U(")")) ) );

            Set_Input( " self() " );
            Assert( Validate( (U("self"), U("("), U(")")) ) );

            -- test maximum number of arguments (255)
            Set_Input( " foo(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0) " );
            Expect_Expression;
            Assert( True );

            begin
                -- test missing closing paren after no args
                Set_Input( " foo( " );
                Expect_Expression;           -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- test missing closing paren after first arg
                Set_Input( " foo( 3 " );
                Expect_Expression;           -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- test missing closing paren after multiple args
                Set_Input( " foo( 3, 2 " );
                Expect_Expression;           -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- test syntax error in an argument
                Set_Input( " foo( ""unterminated " );
                Expect_Expression;           -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- test parsing a function call when the open parenthesis is a syntax error
                Set_Input( " foo ""unterminated " );
                Expect_Expression;           -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- test parsing a function call when the comma is a syntax error
                Set_Input( " foo( 1 ""bad comma " );
                Expect_Expression;           -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- test missing argument
                Set_Input( " foo( 1, ) " );
                Expect_Expression;           -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- test missing comma
                Set_Input( " foo( 1 1 ) " );
                Expect_Expression;           -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- test invalid expression in first argument
                Set_Input( " foo( 1 + " );
                Expect_Expression;           -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- test invalid expression in second or later argument
                Set_Input( " foo( 1, 2 + " );
                Expect_Expression;           -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- test missing parenthesis after 'self' token
                Set_Input( " self; " );
                Expect_Expression;           -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- test using a function definition as an argument
                Set_Input( " foo( function():impl ) " );
                Expect_Expression;           -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- test too many arguments (256)
                Set_Input( " foo(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0) " );
                Expect_Expression;           -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;
        end Test_Function_Calls;

        ----------------------------------------------------------------------------

        procedure Test_Index is
        begin
            Set_Input( " list[0] " );
            Assert( Validate( (U("index"), U("list"), U("literal")) ) );
            --                                list       0

            Set_Input( " association [""key"" & ""here""] " );
            Assert( Validate( (U("index"), U("association"), U("&"), U("literal"), U("literal")) ) );
            --                   (index       association      (&       "key"         "here" ))

            Set_Input( " array_3d[1][2][3] " );
            Assert( Validate( (U("index"), U("index"), U("index"), U("array_3d"), U("literal"), U("literal"), U("literal")) ) );
            --                   (index      (index      (index       array_3d       1      )      2      )      3      )

            Set_Input( " array + func(call)[index] " );
            Assert( Validate( (U("+"), U("array"), U("index"), U("func"), U("("), U("call"), U(")"), U("index")) ) );
            --                   (+       array      (index      (function                    call       )       index))

            Set_Input( " !list[0] " );
            Assert( Validate( (U("!"), U("index"), U("list"), U("literal")) ) );
            --                   (!      (index       list       0      ))

            Set_Input( " (!list)[0] " );
            Assert( Validate( (U("index"), U("!"), U("list"), U("literal")) ) );
            --                   (index      (!       list)      0      )

            begin
                -- test missing expression
                Set_Input( " list[ ] " );
                Expect_Expression;                   -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- test missing closing bracket
                Set_Input( " list[0 " );
                Expect_Expression;                   -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- test bad expression in index
                Set_Input( " list[0 + " );
                Expect_Expression;                   -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- test syntax error in list
                Set_Input( " list["" " );
                Expect_Expression;                   -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- test syntax error instead of opening bracket
                Set_Input( " list ""key ] " );
                Expect_Expression;                   -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- test using a function definition as an index
                Set_Input( " list[ function():impl ] " );
                Expect_Expression;                   -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;
        end Test_Index;

        ----------------------------------------------------------------------------

        procedure Test_Terms is
        begin
            Set_Input( " !!true " );
            Assert( Validate( (U("!"), U("!"), U("literal")) ) );

            Set_Input( " -1 " );
            Assert( Validate( (U("-"), U("literal")) ) );

            declare
                expected : String_Array(1..81);
            begin
                Set_Input( " --------------------------------------------------------------------------------1 " );
                for i in 1..80 loop
                    expected(i) := U("-");
                end loop;
                expected(81) := U("literal");
                Assert( validate( expected ) );
            end;

            begin
                -- expected expression after unary op
                Set_Input( " ! ] " );
                Expect_Expression;                  -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;
        end Test_Terms;

        ----------------------------------------------------------------------------

        procedure Test_Binary_Terms is
        begin
            Set_Input( " 1+2 " );
            Assert( Validate( (U("+"), U("literal"), U("literal")) ) );
            --                   (+       1             2      )

            Set_Input( " 1+2+3 " );
            Assert( Validate( (U("+"), U("+"), U("literal"), U("literal"), U("literal")) ) );
            --                   (+      (+       1             2      )      3      )

            Set_Input( " a+2*3--4 " );
            Assert( Validate( (U("-"), U("+"), U("a"), U("*"), U("literal"), U("literal"), U("-"), U("literal")) ) );
            --                   (-      (+       a      (*       2             3      ))    (-       4      ))

            Set_Input( " 5%2^2 " );
            Assert( Validate( (U("%"), U("literal"), U("^"), U("literal"), U("literal")) ) );
            --                   (%       5            (^       2             2      ))

            begin
                -- missing expression after binary op
                Set_Input( " 4 + 3 + " );
                Expect_Expression;                -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- syntax error instead of binary operator
                Set_Input( " 4 ""plus " );
                Expect_Expression;                -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;
        end Test_Binary_Terms;

        ----------------------------------------------------------------------------

        procedure Test_Ternary_Terms is
        begin
            Set_Input( " a?1:c " );
            Assert( Validate( (U("?"), U("a"), U(":"), U("literal"), U("c")) ) );
            --                   (?       a      (:       1             c))

            Set_Input( " a ? b?2:1 : 0 " );
            Assert( Validate( (U("?"), U("a"), U(":"), U("?"), U("b"), U(":"), U("literal"), U("literal"), U("literal")) ) );
            --                   (?       a      (:      (?       b      (:       2             1      ))     0      ))

            Set_Input( "true ? 2 + 3 * -4 : 0 + 0 & ""that"" " );
            Assert( Validate( (U("?"), U("literal"), U(":"), U("+"), U("literal"), U("*"), U("literal"), U("-"), U("literal"), U("&"), U("+"), U("literal"), U("literal"), U("literal")) ) );
            --                   (?,      2            (:      (+       2            (*       3            (-       4      )))   (&      (+       0             0      )      "that" )))

            begin
                -- missing expression for true case
                Set_Input( " a ? " );
                Expect_Expression;            -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- syntax error in true case
                Set_Input( " a ? ""oops " );
                Expect_Expression;            -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- missing colon after true case
                Set_Input( " a ? 1 " );
                Expect_Expression;            -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- syntax error for colon in true case
                Set_Input( " a ? 1 ""oops " );
                Expect_Expression;            -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- missing false case
                Set_Input( " a ? 1 : " );
                Expect_Expression;            -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- syntax error in false case
                Set_Input( " a ? 1 : ""oops " );
                Expect_Expression;            -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- test using a function definition as a condition
                Set_Input( " function():impl ? 1 : 0 " );
                Expect_Expression;            -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- test using a function definition as a true clause
                Set_Input( " true ? function():impl : 0 " );
                Expect_Expression;            -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- test using a function definition as a false clause
                Set_Input( " true ? 1 : function():impl " );
                Expect_Expression;            -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;
        end Test_Ternary_Terms;

        ----------------------------------------------------------------------------

        procedure Test_Parenthesis is
        begin
            Set_Input( " a (0) " );
            Assert( Validate( (U("a"), U("("), U("literal"), U(")")) ) );
            --                                                0

            Set_Input( " (0) " );
            Assert( Validate( (1=>U("literal")) ) );
            --                       0

            Set_Input( " !(1 + 1) * 5 " );
            Assert( Validate( (U("*"), U("!"), U("+"), U("literal"), U("literal"), U("literal")) ) );
            --                   (*      (!      (+       1             1      ))     5      )

            Set_Input( " (a + 1) * 2 ? (""b"") : (3 * 5)^2 " );
            Assert( Validate( (U("?"), U("*"), U("+"), U("a"), U("literal"), U("literal"), U(":"), U("literal"), U("^"), U("*"), U("literal"), U("literal"), U("literal")) ) );
            --                   (?      (*      (+       a       1      )      2      )     (:       "b"          (^      (*       3             5      )      2      )))

            begin
                Set_Input( " ( " );
                Expect_Expression;            -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                Set_Input( " (1 " );
                Expect_Expression;            -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                Set_Input( " (1+ " );
                Expect_Expression;            -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                Set_Input( " (1+) " );
                Expect_Expression;            -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                Set_Input( " ( a ? ) b : c " );
                Expect_Expression;            -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                Set_Input( " ( a ? ) b : c " );
                Expect_Expression;            -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                Set_Input( " a ? ( b : c " );
                Expect_Expression;            -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                Set_Input( " a ? ( b : c " );
                Expect_Expression;            -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- test using a function definition inside parenthesis
                Set_Input( " (function():impl) " );
                Expect_Expression;            -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;
        end Test_Parenthesis;

        ----------------------------------------------------------------------------

        procedure Test_Assign is
        begin
            Set_Input( " path:=0 " );
            --                   (:=       path       0      )
            Assert( Validate( (U(":="), U("path"), U("literal")) ) );

            -- test nested assignments
            Set_Input( " path := another := 0 " );
            Assert( Validate( (U(":="), U("path"), U(":="), U("another"), U("literal")) ) );

            Set_Input( " path+=0 " );
            Assert( Validate( (U("+="), U("path"), U("literal")) ) );

            Set_Input( " path-=0 " );
            Assert( Validate( (U("-="), U("path"), U("literal")) ) );

            Set_Input( " path*=0 " );
            Assert( Validate( (U("*="), U("path"), U("literal")) ) );

            Set_Input( " path/=0 " );
            Assert( Validate( (U("/="), U("path"), U("literal")) ) );

            Set_Input( " path&=0 " );
            Assert( Validate( (U("&="), U("path"), U("literal")) ) );

            -- test assign index
            Set_Input( " path[0]:=foo " );
            Assert( Validate( (U(":="), U("index"), U("path"), U("literal"), U("foo")) ) );

            -- test assign index
            Set_Input( " path[path2[0]]:=foo " );
            Assert( Validate( (U(":="), U("index"), U("path"), U("index"), U("path2"), U("literal"), U("foo")) ) );

            -- test assign index with function definition as value
            Set_Input( " path[0]:=function() end function " );
            Assert( Validate( (U(":="), U("index"), U("path"), U("literal"), U("function"), U("block"), U("return"), U("literal"), U("end")) ) );

            -- test assign multi-index
            Set_Input( " path[0][1+bar]:=foo " );
            Assert( Validate( (U(":="), U("index"), U("index"), U("path"), U("literal"), U("+"), U("literal"), U("bar"), U("foo")) ) );

            -- test assign multi-index
            Set_Input( " path[null]:=foo " );     -- not useful but legal
            Assert( Validate( (U(":="), U("index"), U("path"), U("literal"), U("foo")) ) );

            -- test arbitrary expression on left hand side (not semantically allowed)
            Set_Input( " 1 := 2" );
            Assert( Validate( (U(":="), U("literal"), U("literal")) ) );

            begin
                -- syntax error as assignment op
                Set_Input( " path ""oops " );
                Expect_Expression;                 -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- missing value
                Set_Input( " path := " );
                Expect_Expression;                 -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- syntax error as value
                Set_Input( " path := ""oops " );
                Expect_Expression;                 -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- test assign index with bad index expression
                Set_Input( " path[1*] := 0" );
                Expect_Expression;                 -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- test assigning function defintion with a bad op
                Set_Input( " path += function() end function" );
                Expect_Expression;                 -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;
            begin
                Set_Input( " path -= function() end function" );
                Expect_Expression;                 -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;
            begin
                Set_Input( " path *= function() end function" );
                Expect_Expression;                 -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;
            begin
                Set_Input( " path /= function() end function" );
                Expect_Expression;                 -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;
            begin
                Set_Input( " path %= function() end function" );
                Expect_Expression;                 -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;
            begin
                Set_Input( " path &= function() end function" );
                Expect_Expression;                 -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- test function definition inside left hand index
                Set_Input( " path[function() end function] := 0" );
                Expect_Expression;                 -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;

            begin
                -- test using a function definition as left hand
                Set_Input( " function():impl := 1 " );
                Expect_Expression;                 -- Parse_Error expected
                Assert( False );
            exception when Parse_Error => null;
            end;
        end Test_Assign;

        ----------------------------------------------------------------------------

        procedure Test_Function_Definition is
            empty : String_Array(1..0);
        begin
            Set_Input( " function() : cpp_impl " );
            Assert( Validate_Function( (U("function"), U(":"), U("cpp_impl")) ) );

            Set_Input( " function(x) : cpp_impl " );
            Assert( Validate_Function( (U("function"), U("x"), U(":"), U("cpp_impl")) ) );

            Set_Input( " function(x, y) : cpp_impl " );
            Assert( Validate_Function( (U("function"), U("x"), U("y"), U(":"), U("cpp_impl")) ) );

            Set_Input( " function(x, y := 2) : cpp_impl " );
            Assert( Validate_Function( (U("function"), U("x"), U("y"), U(":="), U("literal"), U(":"), U("cpp_impl")) ) );

            Set_Input( " function(x, y := 1+2) : cpp_impl " );
            Assert( Validate_Function( (U("function"), U("x"), U("y"), U(":="), U("literal"), U(":"), U("cpp_impl")) ) );

            Set_Input( " function(x, y := z) : cpp_impl " );     -- not semantically allowed
            Assert( Validate_Function( (U("function"), U("x"), U("y"), U(":="), U("z"), U(":"), U("cpp_impl")) ) );

            Set_Input( " function(x, y := z+2) : cpp_impl " );   -- not semantically allowed
            Assert( Validate_Function( (U("function"), U("x"), U("y"), U(":="), U("+"), U("z"), U("literal"), U(":"), U("cpp_impl")) ) );

            Set_Input( " function(x, y := [1,2]) : cpp_impl " );
            Assert( Validate_Function( (U("function"), U("x"), U("y"), U(":="), U("literal"), U(":"), U("cpp_impl")) ) );

            Set_Input( " function(x, y := {1:2}) : cpp_impl " );
            Assert( Validate_Function( (U("function"), U("x"), U("y"), U(":="), U("literal"), U(":"), U("cpp_impl")) ) );

            Set_Input( " function(x, y := [a]) : cpp_impl " );   -- not semantically allowed
            Assert( Validate_Function( (U("function"), U("x"), U("y"), U(":="), U("["), U("a"), U("]"), U(":"), U("cpp_impl")) ) );

            Set_Input( " function(x := 1, y) : cpp_impl " );     -- not semantically allowed
            Assert( Validate_Function( (U("function"), U("x"), U(":="), U("literal"), U("y"), U(":"), U("cpp_impl")) ) );

            Set_Input( " function(a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a) : cpp_impl " );
            Assert( Validate_Function( (U("function"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U("a"), U(":"), U("cpp_impl")) ) );

            begin
                -- test missing ":"/"end"
                Set_Input( " function() " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- test impl symbol as path expression
                Set_Input( " function(x, y) : path.not.allowed" );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- test missing impl symbol
                Set_Input( " function(x, y) : " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- test bad colon
                Set_Input( " function(x, y) + impl " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- test missing colon
                Set_Input( " function(x, y) impl " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- test missing close parenthesis
                Set_Input( " function(x, y : impl " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- test missing parameter name
                Set_Input( " function(x, " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- test bad parameter name
                Set_Input( " function(_x) : impl " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- test non-simple parameter name
                Set_Input( " function(x.y) : cpp_impl " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- test missing close paren/param name
                Set_Input( " function( : impl" );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- test missing open paren
                Set_Input( " function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- test too many parameters (256)
                Set_Input( " function(a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a) : cpp_impl" );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- test a function definition as a parameter default value
                Set_Input( " function( f := function():impl ) : cpp_impl " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;
        end Test_Function_Definition;

        ----------------------------------------------------------------------------

        procedure Test_Function_Definition_Body is
        begin
            -- test that a "return null" is added to prevent execution falling out of the function
            Set_Input( " function(x) end function " );
            Assert( Validate_Function( (U("function"), U("x"),
                                        U("block"),
                                            U("return"), U("literal"),     -- added automatically
                                        U("end")) ) );

            -- test with all semicolons
            Set_Input( " function(x) ; ;;" & ASCII.LF & "; end function " );
            Assert( Validate_Function( (U("function"), U("x"),
                                        U("block"),
                                            U("return"), U("literal"),     -- added automatically
                                        U("end")) ) );

            -- test with an actual statement
            Set_Input( " function(x) return 0; end function " );
            Assert( Validate_Function( (U("function"), U("x"),
                                        U("block"),
                                            U("return"), U("literal"),
                                            U("return"), U("literal"),     -- added automatically
                                        U("end")) ) );
        end Test_Function_Definition_Body;

        ----------------------------------------------------------------------------

        procedure Test_Lists is
            empty : String_Array(1..0);
        begin
            -- test empty list
            Set_Input( " [] " );
            Assert( Validate( (U("["), U("]")) ) );

            -- test single element
            Set_Input( " [1] " );
            Assert( Validate( (U("["), U("literal"), U("]")) ) );

            -- test multiple elements
            Set_Input( " [1, 2 * foo(), []] " );
            Assert( Validate( (U("["), U("literal"), U(","),
                                       U("*"), U("literal"), U("foo"), U("("), U(")"), U(","),
                                       U("["), U("]"),
                               U("]")) ) );

            begin
                -- test a list containing a function definition
                Set_Input( " [function() end function] " );
                Assert( Validate_Function( empty ) or False );
            exception when Parse_Error => null;
            end;

            begin
                -- test missing closing bracket
                Set_Input( " [ " );
                Assert( Validate_Function( empty ) or False );
            exception when Parse_Error => null;
            end;

            begin
                -- test missing closing bracket
                Set_Input( " [ 1, ");
                Assert( Validate_Function( empty ) or False );
            exception when Parse_Error => null;
            end;

            begin
                -- test missing comma
                Set_Input( " [ 1 ");
                Assert( Validate_Function( empty ) or False );
            exception when Parse_Error => null;
            end;

            begin
                -- test missing comma
                Set_Input( " [ 1 2 ] ");
                Assert( Validate_Function( empty ) or False );
            exception when Parse_Error => null;
            end;

            begin
                -- test bad expression element
                Set_Input( " [ 1* ] ");
                Assert( Validate_Function( empty ) or False );
            exception when Parse_Error => null;
            end;
        end Test_Lists;

        ----------------------------------------------------------------------------

        procedure Test_Associations is
            empty : String_Array(1..0);
        begin
            -- test empty association
            Set_Input( " {} " );
            Assert( Validate( (U("{"), U("}")) ) );

            -- test single pair
            Set_Input( " {""one"":1} " );
            Assert( Validate( (U("{"), U("literal"), U(":"), U("literal"), U("}")) ) );

            -- test multiple pairs
            Set_Input( " {1+0:true, bar:2 * foo(), {a:b}:[]} " );
            Assert( Validate( (U("{"), U("+"), U("literal"), U("literal"), U(":"), U("literal"), U(","),
                                       U("bar"), U(":"), U("*"), U("literal"), U("foo"), U("("), U(")"), U(","),
                                       U("{"), U("a"), U(":"), U("b"), U("}"), U(":"), U("["), U("]"),
                               U("}")) ) );

            begin
                -- test an association containing a function definition value
                Set_Input( " {""f"":function() end function} " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- test an association containing a function definition key
                Set_Input( " {function() end function:1} " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- test missing closing brace
                Set_Input( " { " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- test missing closing bracket
                Set_Input( " { 1:true, " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- test missing comma
                Set_Input( " { 1:true 2:false } " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- test missing colon
                Set_Input( " { 1 true } " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- test bad expression value
                Set_Input( " { 1:1* } " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- test bad expression key
                Set_Input( " { 1+:1 } " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;
        end Test_Associations;

        ----------------------------------------------------------------------------

        procedure Test_Statement_Return is
            empty : String_Array(1..0);
        begin
            -- the valid format was already tested above

            begin
                -- test missing semicolon
                Set_Input( " function() return 0 end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- test missing expression
                Set_Input( " function() return; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;
        end Test_Statement_Return;

        ----------------------------------------------------------------------------

        procedure Test_Statement_Expression is
            empty : String_Array(1..0);
        begin
            -- test a literal as an expression statement
            Set_Input( " function() 1; end function " );
            Assert( Validate_Function( (U("function"),
                                        U("block"),
                                            U("expr"), U("literal"),
                                            U("return"), U("literal"),   -- added automatically
                                        U("end")) ) );

            -- test a simple expression as an expression statement
            Set_Input( " function() 1 + 1; end function " );
            Assert( Validate_Function( (U("function"),
                                        U("block"),
                                            U("expr"), U("+"), U("literal"), U("literal"),
                                            U("return"), U("literal"),   -- added automatically
                                        U("end")) ) );

            -- test a function call as an expression statement
            Set_Input( " function() foo(); end function " );
            Assert( Validate_Function( (U("function"),
                                        U("block"),
                                            U("expr"), U("foo"), U("("), U(")"),
                                            U("return"), U("literal"),   -- added automatically
                                        U("end")) ) );

            -- test an assignment as an expression statement
            Set_Input( " function() a := 3; end function " );
            Assert( Validate_Function( (U("function"),
                                        U("block"),
                                            U("expr"), U(":="), U("a"), U("literal"),
                                            U("return"), U("literal"),   -- added automatically
                                        U("end")) ) );

            begin
                -- test a function definition as an expression statement
                Set_Input( " function() function() : impl; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- test a missing semicolon
                Set_Input( " function() foo() end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- test a bad expression
                Set_Input( " function() a := 3 +; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;
        end Test_Statement_Expression;

        ----------------------------------------------------------------------------

        procedure Test_Statement_If is
            empty : String_Array(1..0);
        begin
            -- test a empty if
            Set_Input( " function() if true then end if; end function " );
            Assert( Validate_Function( (U("function"),
                                        U("block"),
                                            U("if"), U("literal"), U("then"),
                                                U("block"),
                                                U("end"),
                                            U("end"),
                                            U("return"), U("literal"),   -- added automatically
                                        U("end")) ) );

            -- test a valid if
            Set_Input( " function() if true then foo(); end if; end function " );
            Assert( Validate_Function( (U("function"),
                                        U("block"),
                                            U("if"), U("literal"), U("then"),
                                                U("block"),
                                                    U("expr"), U("foo"), U("("), U(")"),
                                                U("end"),
                                            U("end"),
                                            U("return"), U("literal"),   -- added automatically
                                        U("end")) ) );

            -- test a valid if/else
            Set_Input( " function() if true then foo(); else bar(); end if; end function " );
            Assert( Validate_Function( (U("function"),
                                        U("block"),
                                            U("if"), U("literal"), U("then"),
                                                U("block"),
                                                    U("expr"), U("foo"), U("("), U(")"),
                                                U("end"),
                                            U("else"),
                                                U("block"),
                                                    U("expr"), U("bar"), U("("), U(")"),
                                                U("end"),
                                            U("end"),
                                            U("return"), U("literal"),   -- added automatically
                                        U("end")) ) );

            -- test a valid if/elsif
            Set_Input( " function() if true then foo(); elsif true then bar(); end if; end function " );
            Assert( Validate_Function( (U("function"),
                                        U("block"),
                                            U("if"), U("literal"), U("then"),
                                                U("block"),
                                                    U("expr"), U("foo"), U("("), U(")"),
                                                U("end"),
                                            U("elsif"), U("literal"), U("then"),
                                                U("block"),
                                                    U("expr"), U("bar"), U("("), U(")"),
                                                U("end"),
                                            U("end"),
                                            U("return"), U("literal"),   -- added automatically
                                        U("end")) ) );

            -- test a valid if/else
            Set_Input( " function() if true then foo(); elsif true then baz(); elsif true then bat(); else bar(); end if; end function " );
            Assert( Validate_Function( (U("function"),
                                        U("block"),
                                            U("if"), U("literal"), U("then"),
                                                U("block"),
                                                    U("expr"), U("foo"), U("("), U(")"),
                                                U("end"),
                                            U("elsif"), U("literal"), U("then"),
                                                U("block"),
                                                    U("expr"), U("baz"), U("("), U(")"),
                                                U("end"),
                                            U("elsif"), U("literal"), U("then"),
                                                U("block"),
                                                    U("expr"), U("bat"), U("("), U(")"),
                                                U("end"),
                                            U("else"),
                                                U("block"),
                                                    U("expr"), U("bar"), U("("), U(")"),
                                                U("end"),
                                            U("end"),
                                            U("return"), U("literal"),   -- added automatically
                                        U("end")) ) );

            begin
                -- missing semicolon
                Set_Input( " function() if true then end if end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- missing 'end'
                Set_Input( " function() if true then     ; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- missing 'if'
                Set_Input( " function() if true then 1; end     ; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- missing 'then'
                Set_Input( " function() if true     end if; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- missing if condition
                Set_Input( " function() if     then end if; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- missing elsif-then
                Set_Input( " function() if true then elsif true     end if; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- missing elsif condition
                Set_Input( " function() if true then elsif     then else end if; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;
        end Test_Statement_If;

        ----------------------------------------------------------------------------

        procedure Test_Statement_Block is
            empty : String_Array(1..0);
        begin
            -- test nested blocks
            Set_Input( " function() block block end block; end block; end function " );
            Assert( Validate_Function( (U("function"),
                                        U("block"),
                                            U("block"),
                                                U("block"),
                                                U("end"),
                                            U("end"),
                                            U("return"), U("literal"),   -- added automatically
                                        U("end")) ) );

            begin
                -- test missing semicolon
                Set_Input( " function() block block end block; end block end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- test missing end
                Set_Input( " function() block block ; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;


            begin
                -- test missing block
                Set_Input( " function() block end      ; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;
        end Test_Statement_Block;

        ----------------------------------------------------------------------------

        procedure Test_Statement_Loop is
            empty : String_Array(1..0);
        begin
            -- test nested loops
            Set_Input( " function() loop loop end loop; end loop; end function " );
            Assert( Validate_Function( (U("function"),
                                        U("block"),
                                            U("loop"),
                                                U("block"),
                                                    U("loop"),
                                                        U("block"),
                                                        U("end"),
                                                    U("end"),
                                                U("end"),
                                            U("end"),
                                            U("return"), U("literal"),   -- added automatically
                                        U("end")) ) );

            begin
                -- test missing semicolon
                Set_Input( " function() loop end loop end function" );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- test missing end
                Set_Input( " function() loop ; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;


            begin
                -- test missing loop
                Set_Input( " function() loop end; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;
        end Test_Statement_Loop;

        ----------------------------------------------------------------------------

        procedure Test_Statement_Exit is
            empty : String_Array(1..0);
        begin
            -- test bare exit statement
            Set_Input( " function() exit; end function " );        -- not semantically correct
            Assert( Validate_Function( (U("function"),
                                        U("block"),
                                            U("exit"),
                                            U("return"), U("literal"),   -- added automatically
                                        U("end")) ) );

            -- test exit with when clause
            Set_Input( " function() exit when true; end function " );        -- not semantically correct
            Assert( Validate_Function( (U("function"),
                                        U("block"),
                                            U("if"), U("literal"), U("then"),
                                                U("block"),
                                                    U("exit"),
                                                U("end"),
                                            U("end"),
                                            U("return"), U("literal"),   -- added automatically
                                        U("end")) ) );

            -- test exit inside loop
            Set_Input( " function() loop exit; end loop; end function " );
            Assert( Validate_Function( (U("function"),
                                        U("block"),
                                            U("loop"),
                                                U("block"),
                                                    U("exit"),
                                                U("end"),
                                            U("end"),
                                            U("return"), U("literal"),   -- added automatically
                                        U("end")) ) );

            begin
                -- test missing semicolon
                Set_Input( " function() loop exit end loop; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- test missing expression after when
                Set_Input( " function() loop exit when; end loop; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- missing when
                Set_Input( " function() loop exit true; end loop; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- function definition as condition
                Set_Input( " function() loop exit when function() return true; end function; end loop; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;
        end Test_Statement_Exit;

        ----------------------------------------------------------------------------

        procedure Test_Statement_Var is
            empty : String_Array(1..0);
        begin
            -- test bare var statement
            Set_Input( " function() var a; end function " );
            Assert( Validate_Function( (U("function"),
                                        U("block"),
                                            U("var"), U("a"), U(":="), U("literal"),   -- := null
                                            U("return"), U("literal"),   -- added automatically
                                        U("end")) ) );

            -- test var statement with explicit initializer
            Set_Input( " function() var a := foo + 1; end function " );
            Assert( Validate_Function( (U("function"),
                                        U("block"),
                                            U("var"), U("a"), U(":="), U("+"), U("foo"), U("literal"),
                                            U("return"), U("literal"),   -- added automatically
                                         U("end")) ) );

            -- test var statement with function definition initializer
            Set_Input( " function() var a := function() end function; end function " );
            Assert( Validate_Function( (U("function"),
                                        U("block"),
                                            U("var"), U("a"), U(":="),
                                                U("function"),
                                                U("block"),
                                                    U("return"), U("literal"),   -- added automatically
                                                U("end"),
                                            U("return"), U("literal"),   -- added automatically
                                         U("end")) ) );

            -- test var statement in a block after a non-var statement (not semantically allowed)
            Set_Input( " function() foo(); var a; end function " );
            Assert( Validate_Function( (U("function"),
                                        U("block"),
                                            U("expr"), U("foo"), U("("), U(")"),
                                            U("var"), U("a"), U(":="), U("literal"),
                                            U("return"), U("literal"),   -- added automatically
                                         U("end")) ) );

            begin
                -- missing semicolon
                Set_Input( " function() var a end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- missing expression
                Set_Input( " function() var a := ; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- bad assignment op
                Set_Input( " function() var a += 1; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- missing assignment op
                Set_Input( " function() var a 1; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- missing variable name
                Set_Input( " function() var a 1; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- var name not an identifier
                Set_Input( " function() var system.param; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;
        end Test_Statement_Var;

        ----------------------------------------------------------------------------

        procedure Test_Statement_While is
            empty : String_Array(1..0);
        begin
            -- test generation of while statement as loop construct
            Set_Input( " function() while 1 < foo loop var i := null; end loop; end function " );
            Assert( Validate_Function( (U("function"),
                                        U("block"),
                                            U("loop"),
                                                U("block"),
                                                    U("if"), U("!"), U("<"), U("literal"), U("foo"), U("then"),
                                                        U("block"),
                                                            U("exit"),
                                                        U("end"),
                                                    U("end"),
                                                    U("block"),
                                                        U("var"), U("i"), U(":="), U("literal"),
                                                    U("end"),
                                                U("end"),
                                            U("end"),
                                            U("return"), U("literal"),   -- added automatically
                                        U("end")) ) );

            begin
                -- missing semicolon
                Set_Input( " function() while true loop end loop end function" );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- missing end
                Set_Input( " function() while true loop ; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- missing loop
                Set_Input( " function() while true var i; end loop; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- missing condition expression
                Set_Input( " function() while loop var i; end loop; end function" );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- bad condition expression
                Set_Input( " function() while 1 + loop var i; end loop; end function" );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- function definition as condition expression
                Set_Input( " function() while function() return true; end function loop var i; end loop; end function" );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;
        end Test_Statement_While;

        ----------------------------------------------------------------------------

        procedure Test_Statement_For is
            empty : String_Array(1..0);
        begin
            -- test generation of for statement as loop construct
            Set_Input( " function() for i is x to y(10) step 2 loop var j := 0; end loop; end function " );
            Assert( Validate_Function( (U("function"),
                                        U("block"),
                                            U("block"),
                                                U("var"), U("i"), U(":="), U("x"),     -- var i := x
                                                U("loop"),
                                                    U("block"),
                                                        U("if"), U(">"), U("i"), U("y"), U("("), U("literal"), U(")"), U("then"), -- i > y
                                                            U("block"),
                                                                U("exit"),
                                                            U("end"),
                                                        U("end"),
                                                        U("block"),
                                                            U("var"), U("j"), U(":="), U("literal"),
                                                        U("end"),
                                                        U("expr"), U("+="), U("i"), U("literal"),  -- i += 2
                                                    U("end"),
                                                U("end"),
                                            U("end"),
                                            U("return"), U("literal"),   -- added automatically
                                        U("end")) ) );

            -- for statement without a step clause
            Set_Input( " function() for i is 1 to 10 loop var j := 0; end loop; end function " );
            Assert( Validate_Function( (U("function"),
                                        U("block"),
                                            U("block"),
                                                U("var"), U("i"), U(":="), U("literal"),   -- var i := 1
                                                U("loop"),
                                                    U("block"),
                                                        U("if"), U(">"), U("i"), U("literal"), U("then"), -- i > 10
                                                            U("block"),
                                                                U("exit"),
                                                            U("end"),
                                                        U("end"),
                                                        U("block"),
                                                            U("var"), U("j"), U(":="), U("literal"),
                                                        U("end"),
                                                        U("expr"), U("+="), U("i"), U("literal"),  -- i += 1
                                                    U("end"),
                                                U("end"),
                                            U("end"),
                                            U("return"), U("literal"),   -- added automatically
                                        U("end")) ) );

            -- for statement expressions and non-numeric values for first, last, step values
            -- (syntactically legal, but undefined behavior)
            Set_Input( " function() for i is ""a""&""b"" to 10.3+foo step null loop var j := 0; end loop; end function " );
            Assert( Validate_Function( (U("function"),
                                        U("block"),
                                            U("block"),
                                                U("var"), U("i"), U(":="), U("&"), U("literal"), U("literal"),   -- var i := "a" & "b"
                                                U("loop"),
                                                    U("block"),
                                                        U("if"), U(">"), U("i"), U("+"), U("literal"), U("foo"), U("then"), -- i > 10[g]+foo
                                                            U("block"),
                                                                U("exit"),
                                                            U("end"),
                                                        U("end"),
                                                        U("block"),
                                                            U("var"), U("j"), U(":="), U("literal"),
                                                        U("end"),
                                                        U("expr"), U("+="), U("i"), U("literal"),  -- i += null
                                                    U("end"),
                                                U("end"),
                                            U("end"),
                                            U("return"), U("literal"),   -- added automatically
                                        U("end")) ) );

            begin
                -- missing semicolon
                Set_Input( " function() for i is 1 to 10 step 1 loop var j := 0; end loop   end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- missing end
                Set_Input( " function() for i is 1 to 10 step 1 loop var j := 0;   ; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- missing loop
                Set_Input( " function() for i is 1 to 10 step 1    var j := 0; end loop; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- missing step expression
                Set_Input( " function() for i is 1 to 10 step  loop var j := 0; end loop; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- bad step expression
                Set_Input( " function() for i is 1 to 10 step 1 / loop var j := 0; end loop; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- function definition as step expression
                Set_Input( " function() for i is 1 to 10 step function() return 1; end function loop var j := 0; end loop; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- missing last expression
                Set_Input( " function() for i is 1 to   step 1 loop var j := 0; end loop; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- bad last expression
                Set_Input( " function() for i is 1 to 3 + step 1 loop var j := 0; end loop; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- function definition as last expression
                Set_Input( " function() for i is 1 to function() return 10; end function step 1 loop var j := 0; end loop; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- missing to
                Set_Input( " function() for i is 1   10 step 1 loop var j := 0; end loop; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- missing first expression
                Set_Input( " function() for i is   to 10 step 1 loop var j := 0; end loop; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- bad first expression
                Set_Input( " function() for i is 1 + to 10 step 1 loop var j := 0; end loop; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                --  function definition as first expression
                Set_Input( " function() for i is function() return 1; end function to 10 step 1 loop var j := 0; end loop; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- missing is
                Set_Input( " function() for i   1 to 10 step 1 loop var j := 0; end loop; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- non-simple variable name
                Set_Input( " function() for dotted.name is 1 to 10 step 1 loop var j := 0; end loop; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;

            begin
                -- missing variable name
                Set_Input( " function() for   is 1 to 10 step 1 loop var j := 0; end loop; end function " );
                Assert( Validate_Function( empty ) );
            exception when Parse_Error => null;
            end;
        end Test_Statement_For;

        ----------------------------------------------------------------------------

    begin
        Setup; Test_Empty_Input;               Ada.Text_IO.Put( "." );
        Setup; Test_Operands;                  Ada.Text_IO.Put( "." );
        Setup; Test_Function_Calls;            Ada.Text_IO.Put( "." );
        Setup; Test_Index;                     Ada.Text_IO.Put( "." );
        Setup; Test_Terms;                     Ada.Text_IO.Put( "." );
        Setup; Test_Binary_Terms;              Ada.Text_IO.Put( "." );
        Setup; Test_Ternary_Terms;             Ada.Text_IO.Put( "." );
        Setup; Test_Parenthesis;               Ada.Text_IO.Put( "." );
        Setup; Test_Assign;                    Ada.Text_IO.Put( "." );
        Setup; Test_Function_Definition;       Ada.Text_IO.Put( "." );
        Setup; Test_Function_Definition_Body;  Ada.Text_IO.Put( "." );
        Setup; Test_Lists;                     Ada.Text_IO.Put( "." );
        Setup; Test_Associations;              Ada.Text_IO.Put( "." );
        Setup; Test_Statement_Return;          Ada.Text_IO.Put( "." );
        Setup; Test_Statement_Expression;      Ada.Text_IO.Put( "." );
        Setup; Test_Statement_If;              Ada.Text_IO.Put( "." );
        Setup; Test_Statement_Block;           Ada.Text_IO.Put( "." );
        Setup; Test_Statement_Loop;            Ada.Text_IO.Put( "." );
        Setup; Test_Statement_Exit;            Ada.Text_IO.Put( "." );
        Setup; Test_Statement_Var;             Ada.Text_IO.Put( "." );
        Setup; Test_Statement_While;           Ada.Text_IO.Put( "." );
        Setup; Test_Statement_For;             Ada.Text_IO.Put( "." );

        Close( s );
        Delete( p );
        Delete( r );

        Ada.Text_IO.Put_Line( ": parser tests OK" );
    exception
        when e : others =>
            Ada.Text_IO.Put_Line( ": parser test FAILED" );
            raise;      -- yes this will crash; use the debugger to catch it
    end Test;

end Scribble.Tests.Parser_Test;
