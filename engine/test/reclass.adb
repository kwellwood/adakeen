
with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Exceptions;                    use Ada.Exceptions;
with Ada.Streams;                       use Ada.Streams;
with Ada.Streams.Stream_IO;             use Ada.Streams.Stream_IO;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Ada.Text_IO;
with Ada.Unchecked_Deallocation;
with GNAT.Directory_Operations.Iteration;
 use GNAT.Directory_Operations.Iteration;
with Interfaces;                        use Interfaces;

procedure Reclass is

    procedure Examine( path  : String;
                       index : Positive;
                       quit  : in out Boolean ) is

        type ASEA is access all Stream_Element_Array;

        procedure Free is new Ada.Unchecked_Deallocation( Stream_Element_Array, ASEA );

        MARKER : constant Stream_Element_Array := (Character'Pos('V'),
                                                   Character'Pos('A'),
                                                   Character'Pos('L'),
                                                   Character'Pos('U'),
                                                   Character'Pos('E'),
                                                   Character'Pos('S'),
                                                   Character'Pos('.'));

        type String_Access is access all String;
        type Tag_Array is array (Integer range <>) of String_Access;

        oldTags : Tag_Array := (new String'("VALUES.ASSOCIATIONS.ASSOC_VALUE"),
                                new String'("VALUES.BOOLEANS.BOOLEAN_VALUE"),
                                new String'("VALUES.ERRORS.ERROR_VALUE"),
                                new String'("VALUES.FUNCTIONS.FUNCTION_VALUE"),
                                new String'("VALUES.IDS.ID_VALUE"),
                                new String'("VALUES.LISTS.LIST_VALUE"),
                                new String'("VALUES.NULLS.NULL_VALUE"),
                                new String'("VALUES.NUMBERS.NUMBER_VALUE"),
                                new String'("VALUES.STRINGS.STRING_VALUE"));

        newTags : Tag_Array := (new String'("VASS"),
                                new String'("VBOO"),
                                new String'("VERR"),
                                new String'("VFNC"),
                                new String'("VID"),
                                new String'("VLST"),
                                new String'("VNUL"),
                                new String'("VNUM"),
                                new String'("VSTR"));

        pragma Unreferenced( index, quit );
        ft     : Ada.Streams.Stream_IO.File_Type;
        data   : ASEA;
        length : Stream_Element_Offset;
        i      : Stream_Element_Offset;
    begin
        Ada.Text_IO.Put_Line( "Reclassing " & path );
        Open( ft, In_File, path );

        data := new Stream_Element_Array(1..Stream_Element_Offset(Size( ft ) * 2));
        length := data'First - 1;
        loop
            Read( ft, data(length+1..data'Last), length );
            if length >= Stream_Element_Offset(Size( ft )) then
                exit;
            end if;
        end loop;
        Close( ft );

        i := data'First;
        loop
            if i > data'Last then
                exit;
            end if;

            if i - (4 + MARKER'Length) >= data'First then
                if data(i-MARKER'Length+1..i) = MARKER then
                    declare
                        last    : Stream_Element_Array := data(i-MARKER'Length-4+1..i-MARKER'Length-1+1);
                        for last'Alignment use 4;
                        taglen  : Integer;
                        for taglen'Address use last'Address;
                        oldtag  : String(1..taglen);
                        for oldtag'Address use data(i-MARKER'Length+1)'Address;
                        tagNum  : Integer := 0;
                        lenDiff : Integer := 0;
                    begin
                        -- determine the replacement tag
                        for i in oldTags'Range loop
                            if oldTag = oldTags(i).all then
                                tagNum := i;
                                exit;
                            end if;
                        end loop;
                        if tagNum = 0 then
                            Ada.Text_IO.Put_Line( "Found unexpected class!" ) ;
                            return;
                        end if;

                        -- shift the file contents
                        lenDiff := newTags(tagNum).all'Length - oldTag'Length;
                        if lenDiff > 0 then
                            -- shift the data to the right to make room
                            for j in reverse (i - MARKER'Length + oldTag'Length + 1)..length loop
                                data(j + Stream_Element_Offset(lenDiff)) := data(j);
                            end loop;
                        else
                            -- shift the data to the left to fill empty space
                            for j in (i - MARKER'Length + oldTag'Length + 1)..length loop
                                data(j + Stream_Element_Offset(lenDiff)) := data(j);
                            end loop;
                        end if;
                        length := length + Stream_Element_Offset(lenDiff);

                        -- write the new tag and its length
                        declare
                            newTag : String(1..newTags(tagNum).all'Length);
                            for newTag'Address use data(i-MARKER'Length+1)'Address;
                        begin
                            newTag := newTags(tagNum).all;
                            taglen := newTags(tagNum).all'Length;
                            data(i-MARKER'Length-4+1..i-MARKER'Length-1+1) := last;
                        end;

                    end;
                end if;
            end if;

            i := i + 1;
        end loop;

        Open( ft, Out_File, path );
        Write( ft, data(data'First..length) );
        Close( ft );

        Free( data );
    exception
        when e : others =>
            if Is_Open( ft ) then
                Close( ft );
            end if;
            Ada.Text_IO.Put_Line( Exception_Name( e ) & " -- " & Exception_Message( e ) );
            raise;
    end Examine;

    procedure Iterate is new Wildcard_Iterator( Examine );

    quit : Boolean := False;
begin
    if Argument_Count = 0 then
        Ada.Text_IO.Put_Line( "reclass: <world-root-dir>" );
        return;
    end if;

    for i in 1..Argument_Count loop
        if Index( Argument( i ), "*" ) < 1 then
            Examine( Argument( i ), i, quit );
        else
            Iterate( Argument( 1 ) );
        end if;
    end loop;
end Reclass;
