--
-- Copyright (c) 2013-2014 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Text_IO;
with Scribble.Ast.Processors;           use Scribble.Ast.Processors;
with Scribble.Tokens;                   use Scribble.Tokens;

package body Scribble.Tests.Ast_Validators is

    function Create_Ast_Validator return A_Ast_Validator is
        this : constant A_Ast_Validator := new Ast_Validator;
    begin
        this.Construct;
        return this;
    end Create_Ast_Validator;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Ast_Validator ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Ast_Validator ) is
    begin
        Delete( this.expected );
        Limited_Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Check( this : not null access Ast_Validator'Class; name : String ) is
    begin
        if this.valid and then this.index <= this.expected'Last then
            if To_String( this.expected(this.index) ) /= name then
                Ada.Text_IO.Put( "(Expected '" & To_String( this.expected(this.index) ) & "', found '" & name & "')" );
                this.valid := False;
            end if;
        else
            this.valid := False;
        end if;
        if this.valid then
            this.index := this.index + 1;
        end if;
    end Check;

    ----------------------------------------------------------------------------

    function Validate( this     : not null access Ast_Validator'Class;
                       root     : not null A_Ast_Node;
                       expected : String_Array ) return Boolean is
    begin
        this.expected := new String_Array(expected'Range);
        this.expected.all := expected;
        this.valid := True;
        this.index := this.expected'First;

        root.Process( A_Ast_Processor(this) );

        -- make sure we didn't expect more than we got
        this.valid := this.valid and then this.index = this.expected'Last + 1;

        Delete( this.expected );
        return this.valid;
    end Validate;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Literal( this : access Ast_Validator; node : A_Ast_Literal ) is
    begin
        this.Check( "literal" );
    end Process_Literal;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Association( this : access Ast_Validator; node : A_Ast_Association ) is
    begin
        this.Check( "{" );
        for i in 1..node.Length loop
            if i > 1 then
                this.Check( "," );
            end if;
            node.Get_Key( i ).Process( A_Ast_Processor(this) );
            this.Check( ":" );
            node.Get_Value( i ).Process( A_Ast_Processor(this) );
        end loop;
        this.Check( "}" );
    end Process_Association;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_List( this : access Ast_Validator; node : A_Ast_List ) is
    begin
        this.Check( "[" );
        for i in 1..node.Length loop
            if i > 1 then
                this.Check( "," );
            end if;
            node.Get_Element( i ).Process( A_Ast_Processor(this) );
        end loop;
        this.Check( "]" );
    end Process_List;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Function_Call( this : access Ast_Validator; node : A_Ast_Function_Call ) is
    begin
        node.Get_Call_Expression.Process( A_Ast_Processor(this) );
        this.Check( "(" );
        for i in 1..node.Arguments_Count loop
            if i > 1 then
                this.Check( "," );
            end if;
            node.Get_Argument( i ).Process( A_Ast_Processor(this) );
        end loop;
        this.Check( ")" );
    end Process_Function_Call;

    ----------------------------------------------------------------------------

    procedure Process_Self( this : access Ast_Validator; node : A_Ast_Self ) is
    begin
        this.Check( "self" );
    end Process_Self;

    ----------------------------------------------------------------------------

    procedure Process_Builtin( this : access Ast_Validator; node : A_Ast_Builtin ) is
    begin
        this.Check( node.Get_Name );
    end Process_Builtin;

    ----------------------------------------------------------------------------

    procedure Process_Identifier( this : access Ast_Validator; node : A_Ast_Identifier ) is
    begin
        if node.No_Namespace then
            this.Check( node.Get_Name );
        else
            this.Check( node.Get_Namespace & "::" & node.Get_Name );
        end if;
    end Process_Identifier;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Dereference( this : access Ast_Validator; node : A_Ast_Dereference ) is
    begin
        node.Get_Expression.Process( A_Ast_Processor(this) );
        this.Check( "." );
        this.Check( node.Get_Name );
    end Process_Dereference;

    ----------------------------------------------------------------------------

    procedure Process_Name_Expr( this : access Ast_Validator; node : A_Ast_Name_Expr ) is
    begin
        this.Check( node.Get_Namespace );
        this.Check( "::[" );
        node.Get_Name.Process( A_Ast_Processor(this) );
        this.Check( "]" );
    end Process_Name_Expr;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Unary_Op( this : access Ast_Validator; node : A_Ast_Unary_Op ) is
    begin
        this.Check( Image( node.Get_Token_Type ) );
        node.Get_Right.Process( A_Ast_Processor(this) );
    end Process_Unary_Op;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Binary_Op( this : access Ast_Validator; node : A_Ast_Binary_Op ) is
    begin
        this.Check( Image( node.Get_Token_Type ) );
        node.Get_Left.Process( A_Ast_Processor(this) );
        node.Get_Right.Process( A_Ast_Processor(this) );
    end Process_Binary_Op;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Assign( this : access Ast_Validator; node : A_Ast_Assign ) is
    begin
        this.Process_Binary_Op( A_Ast_Binary_Op(node) );
    end Process_Assign;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Index( this : access Ast_Validator; node : A_Ast_Index ) is
    begin
        this.Check( "index" );
        node.Get_Left.Process( A_Ast_Processor(this) );
        node.Get_Right.Process( A_Ast_Processor(this) );
    end Process_Index;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Conditional( this : access Ast_Validator; node : A_Ast_Conditional ) is
    begin
        this.Check( "?" );
        node.Get_Condition.Process( A_Ast_Processor(this) );
        this.Check( ":" );
        node.Get_True_Case.Process( A_Ast_Processor(this) );
        node.Get_False_Case.Process( A_Ast_Processor(this) );
    end Process_Conditional;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Function_Definition( this : access Ast_Validator; node : A_Ast_Function_Definition ) is
    begin
        this.Check( "function" );
        for i in 1..node.Parameters_Count loop
            this.Check( node.Get_Parameter( i ).Get_Name );
            if node.Get_Parameter_Default( i ) /= null then
                this.Check( ":=" );
                node.Get_Parameter_Default( i ).Process( A_Ast_Processor(this) );
            end if;
        end loop;
        if node.Get_Internal_Name'Length > 0 then
            this.Check( ":" );
            this.Check( node.Get_Internal_Name );
        else
            node.Get_Body.Process( A_Ast_Processor(this) );     -- Ast_Block
        end if;
    end Process_Function_Definition;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Block( this : access Ast_Validator; node : A_Ast_Block ) is
        statement : A_Ast_Statement;
    begin
        this.Check( "block" );
        statement := node.Get_First;
        while statement /= null loop
            statement.Process( A_Ast_Processor(this) );
            statement := statement.Get_Next;
        end loop;
        this.Check( "end" );
    end Process_Block;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Exit( this : access Ast_Validator; node : A_Ast_Exit ) is
    begin
        this.Check( "exit" );
    end Process_Exit;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Expression_Statement( this : access Ast_Validator; node : A_Ast_Expression_Statement ) is
    begin
        this.Check( "expr" );
        node.Get_Expression.Process( A_Ast_Processor(this) );
    end Process_Expression_Statement;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_If( this : access Ast_Validator; node : A_Ast_If ) is
    begin
        this.Check( "if" );
        node.Get_Case_Condition( 1 ).Process( A_Ast_Processor(this) );
        this.Check( "then" );
        node.Get_Case_Block( 1 ).Process( A_Ast_Processor(this) );
        for i in 2..node.Case_Count loop
            this.Check( "elsif" );
            node.Get_Case_Condition( i ).Process( A_Ast_Processor(this) );
            this.Check( "then" );
            node.Get_Case_Block( i ).Process( A_Ast_Processor(this) );
        end loop;
        if node.Get_Else_Block /= null then
            this.Check( "else" );
            node.Get_Else_Block.Process( A_Ast_Processor(this) );
        end if;
        this.Check( "end" );
    end Process_If;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Loop( this : access Ast_Validator; node : A_Ast_Loop ) is
    begin
        this.Check( "loop" );
        node.Get_Body.Process( A_Ast_Processor(this) );
        this.Check( "end" );
    end Process_Loop;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Return( this : access Ast_Validator; node : A_Ast_Return ) is
    begin
        this.Check( "return" );
        node.Get_Expression.Process( A_Ast_Processor(this) );
    end Process_Return;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Var( this : access Ast_Validator; node : A_Ast_Var ) is
    begin
        this.Check( "var" );
        this.Check( node.Get_Variable.Get_Name );
        this.Check( ":=" );
        node.Get_Expression.Process( A_Ast_Processor(this) );
    end Process_Var;

end Scribble.Tests.Ast_Validators;
