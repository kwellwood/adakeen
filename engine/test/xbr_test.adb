--
-- Copyright (c) 2013 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Exceptions;                    use Ada.Exceptions;
with Ada.Text_IO;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.System;                    use Allegro.System;
with xBR;

package body xbr_test is

    procedure Ada_Main is
        input, output : A_Allegro_Bitmap;
    begin
        if not Al_Initialize then
            Ada.Text_IO.Put_Line( "Could not init Allegro." );
            return;
        end if;

        if not Al_Init_Image_Addon then
            Ada.Text_IO.Put_Line( "Could not init image addon." );
            return;
        end if;

        if Argument_Count /= 3 then
            Ada.Text_IO.Put_Line( "xbr_test.exe <2x|3x|4x> <input-image> <output-image>" );
            return;
        end if;

        Al_Set_New_Bitmap_Flags( ALLEGRO_MEMORY_BITMAP );
        input := Al_Load_Bitmap( Argument( 2 ) );
        if input = null then
            Ada.Text_IO.Put_Line( "File not found: " & Argument( 2 ) );
            return;
        end if;

        xbr.Initialize;

        if Argument( 1 ) = "2x" then
            output := Al_Create_Bitmap( Al_Get_Bitmap_Width( input )  * 2,
                                        Al_Get_Bitmap_Height( input ) * 2 );
            xbr.xBR2( input, output, 0, 0 );
        elsif Argument( 1 ) = "3x" then
            output := Al_Create_Bitmap( Al_Get_Bitmap_Width( input )  * 3,
                                        Al_Get_Bitmap_Height( input ) * 3 );
            xbr.xBR3( input, output, 0, 0 );
        elsif Argument( 1 ) = "4x" then
            output := Al_Create_Bitmap( Al_Get_Bitmap_Width( input )  * 4,
                                        Al_Get_Bitmap_Height( input ) * 4 );
            xbr.xBR4( input, output, 0, 0 );
        else
            Ada.Text_IO.Put_Line( "Bad size: " & Argument( 1 ) );
            return;
        end if;

        if Al_Save_Bitmap( Argument( 3 ), output ) then
            Ada.Text_IO.Put_Line( "Wrote " & Argument( 3 ) );
        else
            Ada.Text_IO.Put_Line( "Error writing " & Argument( 3 ) );
        end if;

        Al_Destroy_Bitmap( output );
        Al_Destroy_Bitmap( input );
    exception
        when e : others =>
            Ada.Text_IO.Put_Line( Exception_Name( e ) & ": " & Exception_Message( e ) );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end xbr_test;
