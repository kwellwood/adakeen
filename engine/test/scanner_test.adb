--
-- Copyright (c) 2012 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Exceptions;                    use Ada.Exceptions;
with Ada.Streams;                       use Ada.Streams;
with Ada.Streams.Stream_IO;             use Ada.Streams.Stream_IO;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Ada.Text_IO;
with Ada.Unchecked_Deallocation;
with Streams.Buffers;                   use Streams.Buffers;
with Tokens;                            use Tokens;
with Tokens.Scanners;                   use Tokens.Scanners;
with Tokens.Tokenizers;                 use Tokens.Tokenizers;

procedure Scanner_Test is
    scanner : A_Token_Scanner := Create_Token_Scanner;
    stream  : A_Buffer_Stream;
    token   : A_Token;
    result  : Unbounded_String;
    failed  : Boolean := False;

    type Action_Type is (AT_Acc, AT_Exp, AT_Acc_Bin, AT_Acc_Un);

    type Token_Action is
        record
            action    : Action_Type;
            tokenType : Token_Type;
        end record;

    type TAA is array(Positive range <>) of Token_Action;
    type A_TAA is access all TAA;

    procedure Free is new Ada.Unchecked_Deallocation( TAA, A_TAA );

    type Test_Case is
        record
            input  : String_Access;
            tokens : A_TAA;
            expect : String_Access;
        end record;

    type Test_Array is array(Positive range <>) of Test_Case;

    tests : Test_Array := Test_Array'(
        -- expect encounters a correct number
        (new String'("123"),
             new TAA'(1=>(AT_Exp,TK_NUMBER)),
             new String'("TK_NUMBER")),

        -- expect encounters a correct identifier and EOF
        (new String'("Kevin"),
            new TAA'((AT_Exp,TK_IDENTIFIER), (AT_Exp,TK_EOF)),
            new String'("TK_IDENTIFIER TK_EOF")),

        -- expect encounters the wrong token
        (new String'("""Hello"""),
            new TAA'(1=>(AT_Exp,TK_IDENTIFIER)),
            new String'("Parse_Exception(Expected TK_IDENTIFIER, found TK_STRING at 1:1)")),

        -- expect encounters a bad token
        (new String'("""Hello"),
            new TAA'(1=>(AT_Exp,TK_STRING)),
            new String'("Token_Exception(Unterminated string at 1:1)")),

        -- expect encounters a bad token, with previous success
        (new String'("+""Hello"),
            new TAA'((AT_Exp,TK_PLUS), (AT_Exp,TK_STRING)),
            new String'("TK_PLUS Token_Exception(Unterminated string at 1:2)")),

        -- accept encounters a bad token
        (new String'("13i"),
            new TAA'(1=>(AT_Acc,TK_PLUS)),
            new String'("Token_Exception(Unrecognized token '13i' at 1:1)")),

        -- accept encounters the right token and EOF
        (new String'("!"),
            new TAA'((AT_Acc,TK_BANG), (AT_Acc,TK_EOF)),
            new String'("TK_BANG TK_EOF")),

        -- accept doesn't encounter the right token with serveral tries
        (new String'("-"),
            new TAA'((AT_Acc,TK_PLUS), (AT_Acc,TK_BANG), (AT_Acc,TK_EOF)),
            new String'("")),

        -- accept encounters correct token after retry
        (new String'(""),
            new TAA'((AT_Acc,TK_BANG), (AT_Acc,TK_EOF)),
            new String'("TK_EOF")),

        -- expect encounters correct token after retry
        (new String'("="),
            new TAA'((AT_Exp,TK_PLUS), (AT_Exp,TK_EQUALS)),
            new String'("Parse_Exception(Expected TK_PLUS, found TK_EQUALS at 1:1) TK_EQUALS")),

        -- accept encounters correct token after expect fail
        (new String'("="),
            new TAA'((AT_Exp,TK_PLUS), (AT_Acc,TK_EQUALS)),
            new String'("Parse_Exception(Expected TK_PLUS, found TK_EQUALS at 1:1) TK_EQUALS")),

        -- expect all correct operators
        (new String'("-!+-*/%&&||=><<=>=!="),
            new TAA'((AT_Acc_Un, TK_MINUS),
                     (AT_Acc_Un, TK_BANG),
                     (AT_Acc_Bin, TK_PLUS),
                     (AT_Acc_Bin, TK_MINUS),
                     (AT_Acc_Bin, TK_SLASH),
                     (AT_Acc_Bin, TK_STAR),
                     (AT_Acc_Bin, TK_PERCENT),
                     (AT_Acc_Bin, TK_AND),
                     (AT_Acc_Bin, TK_OR),
                     (AT_Acc_Bin, TK_EQUALS),
                     (AT_Acc_Bin, TK_GREATER),
                     (AT_Acc_Bin, TK_LESS),
                     (AT_Acc_Bin, TK_LESS_EQUALS),
                     (AT_Acc_Bin, TK_GREATER_EQUALS),
                     (AT_Acc_Bin, TK_NOT_EQUALS)),
            new String'("TK_MINUS TK_BANG TK_PLUS TK_MINUS TK_STAR TK_SLASH TK_PERCENT TK_AND TK_OR TK_EQUALS TK_GREATER TK_LESS TK_LESS_EQUALS TK_GREATER_EQUALS TK_NOT_EQUALS")),

        -- accept encounters an incorrect binary operator and then unary accepts it
        (new String'("!"),
            new TAA'((AT_Acc_Bin, TK_BANG), (AT_Acc_Un, TK_BANG)),
            new String'("TK_BANG")),

        -- accept unary encounters incorrent operator and then binary accepts it
        (new String'("+"),
            new TAA'((AT_Acc_Un, TK_PLUS), (AT_Acc_Bin, TK_PLUS)),
            new String'("TK_PLUS")),

        -- accept unary and binary for the same token type
        (new String'("--"),
            new TAA'((AT_Acc_Un, TK_MINUS), (AT_Acc_Bin, TK_MINUS)),
            new String'("TK_MINUS TK_MINUS"))
    );

begin
    for i in tests'Range loop
        result := To_Unbounded_String( "" );
        stream := Streams.Buffers.Stream( tests(i).input.all );
        scanner.Set_Input( Stream_Access(stream) );
        for j in tests(i).tokens'Range loop
            begin
                case tests(i).tokens(j).action is
                    when AT_Acc     => token := scanner.Accept_Token( tests(i).tokens(j).tokenType );
                    when AT_Exp     => token := scanner.Expect_Token( tests(i).tokens(j).tokenType );
                    when AT_Acc_Bin => token := scanner.Accept_Binary_Op;
                    when AT_Acc_Un  => token := scanner.Accept_Unary_Op;
                end case;
                if token /= null then
                    result := result & token.Get_Type'Img & ' ';
                end if;
            exception
                when e : Token_Exception =>
                    result := result & "Token_Exception(" & Exception_Message( e ) & ") ";
                when e : Parse_Exception =>
                    result := result & "Parse_Exception(" & Exception_Message( e ) & ") ";
            end;
        end loop;
        scanner.Set_Input( null );
        Close( stream );
        Trim( result, Right );
        if To_String( result ) /= tests(i).expect.all then
            Ada.Text_IO.Put_Line( "Test" & i'Img & ": Failed" );
            Ada.Text_IO.Put_Line( "  Expected: " & tests(i).expect.all );
            Ada.Text_IO.Put_Line( "  Actual  : " & To_String( result ) );
            failed := True;
        end if;

        Free( tests(i).input );
        Free( tests(i).tokens );
        Free( tests(i).expect );
    end loop;

    Delete( scanner );

    if not failed then
        Ada.Text_IO.Put_Line( "All tests passed." );
    end if;
end Scanner_Test;
