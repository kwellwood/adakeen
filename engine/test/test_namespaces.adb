--
-- Copyright (c) 2013-2014 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Unchecked_Deallocation;

package body Test_Namespaces is

    overriding
    function Get_Namespace_Name( this : access Simple_Namespace; name : String ) return Value_Ptr is
    begin
        return this.names.Get.Get( name );
    end Get_Namespace_Name;

    ----------------------------------------------------------------------------

    overriding
    function Set_Namespace_Name( this : access Simple_Namespace;
                                 name : String;
                                 val  : Value_Ptr'Class ) return Value_Ptr is
    begin
        this.names.Get.Set( name, val );
        return Value_Ptr(val);
    end Set_Namespace_Name;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Simple_Namespace ) is
        procedure Free is new Ada.Unchecked_Deallocation( Simple_Namespace'Class, A_Simple_Namespace );
    begin
        Free( this );
    end Delete;

end Test_Namespaces;
