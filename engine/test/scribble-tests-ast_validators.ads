--
-- Copyright (c) 2013-2014 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Ada.Unchecked_Deallocation;
with Objects;                           use Objects;
with Scribble.Ast;                      use Scribble.Ast;
with Scribble.Ast.Expressions;          use Scribble.Ast.Expressions;
with Scribble.Ast.Expressions.Operators;use Scribble.Ast.Expressions.Operators;
with Scribble.Ast.Expressions.Symbols;  use Scribble.Ast.Expressions.Symbols;
with Scribble.Ast.Functions;            use Scribble.Ast.Functions;
with Scribble.Ast.Statements;           use Scribble.Ast.Statements;
with Scribble.Ast.Processors;           use Scribble.Ast.Processors;

private package Scribble.Tests.Ast_Validators is

    type String_Array is array (Integer range <>) of Unbounded_String;
    type A_String_Array is access all String_Array;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Ast_Validator is new Limited_Object and Ast_Processor with private;
    type A_Ast_Validator is access all Ast_Validator'Class;

    function Create_Ast_Validator return A_Ast_Validator;
    pragma Postcondition( Create_Ast_Validator'Result /= null );

    -- 'expected' must be in pre-order
    function Validate( this     : not null access Ast_Validator'Class;
                       root     : not null A_Ast_Node;
                       expected : String_Array ) return Boolean;

    -- expressions: operands
    procedure Process_Literal( this : access Ast_Validator; node : A_Ast_Literal );
    procedure Process_Association( this : access Ast_Validator; node : A_Ast_Association );
    procedure Process_List( this : access Ast_Validator; node : A_Ast_List );
    procedure Process_Function_Call( this : access Ast_Validator; node : A_Ast_Function_Call );
    procedure Process_Self( this : access Ast_Validator; node : A_Ast_Self );
    procedure Process_Builtin( this : access Ast_Validator; node : A_Ast_Builtin );
    procedure Process_Identifier( this : access Ast_Validator; node : A_Ast_Identifier );
    procedure Process_Dereference( this : access Ast_Validator; node : A_Ast_Dereference );
    procedure Process_Name_Expr( this : access Ast_Validator; node : A_Ast_Name_Expr );

    -- expressions: operators
    procedure Process_Unary_Op( this : access Ast_Validator; node : A_Ast_Unary_Op );
    procedure Process_Binary_Op( this : access Ast_Validator; node : A_Ast_Binary_Op );
    procedure Process_Assign( this : access Ast_Validator; node : A_Ast_Assign );
    procedure Process_Index( this : access Ast_Validator; node : A_Ast_Index );
    procedure Process_Conditional( this : access Ast_Validator; node : A_Ast_Conditional );

    -- expressions: function definition
    procedure Process_Function_Definition( this : access Ast_Validator; node : A_Ast_Function_Definition );

    -- statements
    procedure Process_Block( this : access Ast_Validator; node : A_Ast_Block );
    procedure Process_Exit( this : access Ast_Validator; node : A_Ast_Exit );
    procedure Process_Expression_Statement( this : access Ast_Validator; node : A_Ast_Expression_Statement );
    procedure Process_If( this : access Ast_Validator; node : A_Ast_If );
    procedure Process_Loop( this : access Ast_Validator; node : A_Ast_Loop );
    procedure Process_Return( this : access Ast_Validator; node : A_Ast_Return );
    procedure Process_Var( this : access Ast_Validator; node : A_Ast_Var );

    procedure Delete( this : in out A_Ast_Validator );

private

    procedure Delete is new Ada.Unchecked_Deallocation( String_Array, A_String_Array );

    type Ast_Validator is new Limited_Object and Ast_Processor with
        record
            valid    : Boolean := True;
            index    : Integer := 0;
            expected : A_String_Array;
        end record;

    procedure Delete( this : in out Ast_Validator );

    -- Checks that 'name' is expected and updates 'valid' and 'index' accordingly.
    procedure Check( this : not null access Ast_Validator'Class; name : String );

end Scribble.Tests.Ast_Validators;
