--
-- Copyright (c) 2013-2014 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Scribble.Tests.Tokenizer_Test;

procedure Scribble_Tokenizer is
begin
    Scribble.Tests.Tokenizer_Test.Test;
end Scribble_Tokenizer;
