--
-- Copyright (c) 2012-2013 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Text_IO;                       use Ada.Text_IO;
with Values;                            use Values;
with Values.Associations;               use Values.Associations;
with Values.Booleans;                   use Values.Booleans;
with Values.Errors;                     use Values.Errors;
with Values.Lists;                      use Values.Lists;
with Values.Nulls;                      use Values.Nulls;
with Values.Numbers;                    use Values.Numbers;
with Values.Strings;                    use Values.Strings;

procedure Values_Test is

    task type Thread is
        entry Init( v : Value_Ptr );
    end Thread;

    task body Thread is
        val : Value_Ptr;
    begin
        accept Init( v : Value_Ptr ) do
            val := v;
        end Init;

        for i in 1..1000 loop
            declare
                v2 : Value_Ptr;
            begin
                v2 := val;
            end;
        end loop;
    end Thread;

    v  : Value_Ptr;
    v2 : Value_Ptr;
    b  : Value_Ptr;
    n  : Value_Ptr;
begin
    Put_Line( "-STRING-" );
    v := Create( "Hello, World!" ).As_Value;
    Put_Line( v.Get.Get_Type'Img );
    if v.Get.Get_Type = V_STRING then
        Put_Line( String_Value(v.Get.all).To_String );
    end if;
    Put_Line( "" );

    Put_Line( "-STRING_PTR-" );
    declare
        a : String_Ptr := Create( "Hello" );
        b : String_Ptr := a & ", World";
    begin
        Put_Line( b.Get.To_String );
        b := "I say, " & a & "!";
        Put_Line( b.Get.To_String );
        b := a & ", Yourself";
        Put_Line( b.Get.To_String );
    end;
    Put_Line( "" );

    Put_Line( "-Operators-" );
    v2 := v;
    Put_Line( "Assignment? " & Boolean'Image( v2.Refcount = 2 and v.Refcount = 2 ) );
    Put_Line( "Operator = ok? " & Boolean'Image( Create( 42 ) = Create( 42 ) ) );
    Put_Line( "" );

    declare
        v3 : Value_Ptr := v2;
    begin
        Put_Line( "v3 = v ? " & Boolean'Image( v3 = v ) );
        Put_Line( "v3.Refs = " & v3.Refcount'Img );
    end;
    Put_Line( "" );

    Put_Line( "-NUMBER-" );
    v := Create( 5.5 ).As_Value;
    Put_Line( v.Get.Get_Type'Img );
    if v.Get.Get_Type = V_NUMBER then
        Put_Line( Number_Value(v.Get.all).To_Int'Img );
        Put_Line( Number_Value(v.Get.all).To_Float'Img );
    end if;
    Put_Line( "" );

    Put_Line( "-NUMBER_PTR-" );
    declare
        a : Number_Ptr := Create( 5 );
        b : Number_Ptr := Create( 2 );
        v : Value_Ptr := Create( 3 ).As_Value;
    begin
        if As_Number( v ).Get.To_Int /= 3 then
            Put_Line( "Cast to number broken" );
        end if;
        v := Create( "Switch value_ptr class" ).As_Value;
        begin
            if As_Number( v ) /= Numbers.Nul then     -- raises Cast_Exception
                Put_Line( "Cast exception not working" );
            end if;
        exception
            when Cast_Exception =>
                Put_Line( "Cast exception ok" );
        end;
        if a + b /= 7 then
            Put_Line( "addition broken" );
        end if;
        if a - b /= 3 then
            Put_Line( "subtraction broken" );
        end if;
        if a * b /= 10 then
            Put_Line( "multiplication broken" );
        end if;
        if a / b /= (5.0 / 2.0) then
            Put_Line( "division broken" );
        end if;
        if a mod b /= 1 then
            Put_Line( "mod broken" );
        end if;
        Put_Line( "done test" );
    end;
    Put_Line( "" );

    Put_Line( "-Comparison-" );
    Put_Line( "v.Compare(v2) = " & v.Get.Compare( v2.Get )'Img );
    Put_Line( "v < v2 ? " & Boolean'Image( v.Get.all < v2.Get.all ) );
    Put_Line( "v < Nul ? " & Boolean'Image( v < Values.Nul ) );
    Put_Line( "v < Null ? " & Boolean'Image( v < Create_Null ) );
    Put_Line( "v2.Compare(v) = " & v2.Get.Compare( v.Get )'Img );
    Put_Line( "v.Compare(v) = " & v.Get.Compare( v.Get )'Img );
    Put_Line( "" );

    Put_Line( "-BOOLEAN-" );
    b := Create( True ).As_Value;
    Put_Line( b.Get.Get_Type'Img );
    if b.Get.Get_Type = V_BOOLEAN then
        Put_Line( As_Boolean(b).Get.To_Boolean'Img );
    end if;
    Put_Line( "" );

    Put_Line( "-NULL-" );
    n := Create_Null;
    Put_Line( n.Get.Get_Type'Img );
    n := Values.Nul;
    Put_Line( "" );

    Put_Line( "-LIST-" );
    v := Create_List.As_Value;
    Put_Line( v.Get.Get_Type'Img );
    if v.Get.Get_Type = V_LIST then
        Put_Line( "Length: " & List_Value(v.Get.all).Length'Img );
        List_Value(v.Get.all).Set( 3, Value_Ptr(Numbers.Create( 3.3 )) );
        if List_Value(v.Get.all).Length = 3 then
            Put_Line( "Set() works" );
        end if;
    end if;
    declare
        inner : Value_Ptr := Create( "Inner" ).As_Value;
    begin
        declare
            list : List_Ptr := Create_List;
        begin
            list.Get.Append( inner );
            if inner.Refcount /= 2 then
                Put_Line( "Refcounted inside list is not working" );
            end if;
            -- list is delete here, decrementing refcount of 'inner'
        end;
        if inner.Refcount = 1 then
            Put_Line( "List deletes refcounts of contents: ok" );
        else
            Put_Line( "List deletes refcounts of contents: BROKEN" );
        end if;
    end;
    Put_Line( "" );

    Put_Line( "-ASSOCIATION-" );
    declare
        a : Assoc_Ptr := Create_Assoc;
        n : Value_Ptr;
    begin
        a.Get.Set( "foo", Value_Ptr(Create( 42 )) );
        Put_Line( "empty after set? " & a.Get.Is_Empty'Img );
        if a.Get.Get( "foo" ) = Create( 42 ).As_Value then
            Put_Line( "Assoc_Value works!" );
        else
            Put_Line( "Assoc_Value doesn't work" );
        end if;
        Put_Line( "Assoc key case sensitive? " &
                  Boolean'Image( a.Get.Get( "FOO" ) = Create_Null ) );
    end;
    Put_Line( "" );

    Put_Line( "-ERROR-" );
    declare
        e : Value_Ptr := Errors.Create( Generic_Exception, "Unknown exception" );
    begin
        Put_Line( e.Get.Get_Type'Img );
        if e.Get.Get_Type = V_ERROR then
            Put_Line( Error_Value(e.Get.all).Get_Code'Img );
            Put_Line( Error_Value(e.Get.all).Get_Message );
        end if;
    end;
    Put_Line( "" );

    Put_Line( "-Threading-" );
    declare
        type Thread_Array is array (Integer range 1..32) of Thread;
        threads : Thread_Array;
        v       : Value_Ptr := Create( "Hello, World" ).As_Value;
    begin
        for i in threads'Range loop
            threads(i).Init( v );
        end loop;
        v := Values.Nul;
    end;
    Put_Line( "done" );
    Put_Line( "" );

end Values_Test;
