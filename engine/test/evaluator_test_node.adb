--
-- Copyright (c) 2012 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Calendar;                      use Ada.Calendar;
with Ada.Calendar.Formatting;           use Ada.Calendar.Formatting;

package body Evaluator_Test_Node is

    function Evaluate_Function( this : access Eval_Node;
                                name : String;
                                args : Value_Array ) return A_Value is
    begin
        if name = "min" then
            if args'Length = 2 then
                return Create_Value( Integer'Min( args(args'First).As_Integer,
                                                  args(args'First+1).As_Integer ) );
            end if;
        elsif name = "max" then
            if args'Length = 2 then
                return Create_Value( Integer'Max( args(args'First).As_Integer,
                                                  args(args'First+1).As_Integer ) );
            end if;
        elsif name = "day" then
            if args'Length = 0 then
                return Create_Value( Day_Of_Week( Clock )'Img );
            end if;
        end if;
        return null;
    end Evaluate_Function;

    ----------------------------------------------------------------------------

    function Evaluate_Symbol( this   : access Eval_Node;
                              symbol : String ) return A_Value is
    begin
        if symbol = "one" then
            return Create_Value( 1 );
        elsif symbol = "yes" then
            return Create_Value( True );
        elsif symbol = "b.c" then
            return Create_Value( "bee.sea" );
        end if;
        return null;
    end Evaluate_Symbol;

end Evaluator_Test_Node;
