--
-- Copyright (c) 2012 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Scripting;                         use Scripting;
with Values;                            use Values;

package Evaluator_Test_Node is

    type Eval_Node is new Evaluation_Node with null record;

    function Evaluate_Function( this : access Eval_Node;
                                name : String;
                                args : Value_Array ) return A_Value;

    function Evaluate_Symbol( this   : access Eval_Node;
                              symbol : String ) return A_Value;

end Evaluator_Test_Node;
