--
-- Copyright (c) 2013-2014 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Scribble.Namespaces;               use Scribble.Namespaces;
with Values;                            use Values;
with Values.Associations;               use Values.Associations;

package Test_Namespaces is

    type Simple_Namespace is new Scribble_Namespace with
        record
            names : Assoc_Ptr := Create_Assoc;
        end record;
    type A_Simple_Namespace is access all Simple_Namespace'Class;

    overriding
    function Get_Namespace_Name( this : access Simple_Namespace; name : String ) return Value_Ptr;

    overriding
    function Set_Namespace_Name( this : access Simple_Namespace;
                                 name : String;
                                 val  : Value_Ptr'Class ) return Value_Ptr;

    procedure Delete( this : in out A_Simple_Namespace );

end Test_Namespaces;
