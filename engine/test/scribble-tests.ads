--
-- Copyright (c) 2014 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Scribble.Tests is

    -- This package provides a parent for Scribble testing packages.

end Scribble.Tests;
