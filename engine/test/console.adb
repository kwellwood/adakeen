--
-- Copyright (c) 2014 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Exceptions;                    use Ada.Exceptions;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Ada.Text_IO;                       use Ada.Text_IO;
with Scribble.Compilers;                use Scribble.Compilers;
with Scribble.Namespaces;               use Scribble.Namespaces;
with Scribble.Runtimes;                 use Scribble.Runtimes;
with Scribble.VMs;                      use Scribble.VMs;
with Test_Namespaces;                   use Test_Namespaces;
with Values;                            use Values;
with Values.Associations;               use Values.Associations;
with Values.Casting;                    use Values.Casting;
with Values.Errors;                     use Values.Errors;
with Values.Functions;                  use Values.Functions;
with Values.Lists;                      use Values.Lists;
with Values.Nulls;                      use Values.Nulls;
with Values.Numbers;                    use Values.Numbers;
with Values.Strings;                    use Values.Strings;

procedure Console is
    runtime  : A_Scribble_Runtime := Create_Scribble_Runtime;
    compiler : A_Scribble_Compiler := Create_Scribble_Compiler( runtime );
    vm       : A_Scribble_VM := Create_Scribble_VM( runtime );

    ----------------------------------------------------------------------------

    function Check_Types( args : Value_Array; types : Type_Array ) return Boolean with Inline;

    function Check_Types( args : Value_Array; types : Type_Array ) return Boolean is
    begin
        for i in args'Range loop
            if args(i).Get.Get_Type /= types(types'First + (i - args'First)) then
                return False;
            end if;
        end loop;
        return True;
    end Check_Types;

    ----------------------------------------------------------------------------

    procedure Display_Parse_Error( source : String; message : String ) is

        function Get_Line( text : String; line : Positive ) return String is
            first : Integer := text'First;
            last  : Integer := Index( text, "" & ASCII.LF, first );
        begin
            for i in 2..line loop
                first := last + 1;
                if first > text'Last then
                    return "<unknown source location>";
                end if;
                last := Index( text, "" & ASCII.LF, first );
            end loop;

            if last < first then
                last := text'Last + 1;
            end if;

            return source(first..(last-1));
        end Get_Line;

        row : Integer := 0;
        col : Integer := 0;
    begin
        begin
            row := Integer'Value( message((Index( message, " ", Backward ) + 1)..
                                          (Index( message, ":", Backward ) - 1)) );
            col := Integer'Value( message((Index( message, ":", Backward ) + 1)..message'Last) );
        exception
            when Constraint_Error => null;
        end;

        Ada.Text_IO.Put_Line( "Parse Error: " & message );
        if col > 0 then
            Ada.Text_IO.Put_Line( "  """ & Get_Line( source, row ) & """" );
            Ada.Text_IO.Put_Line( ((2 + col) * ".") & "^");
        end if;
    end Display_Parse_Error;

    --==========================================================================

    -- type( value : any ) return string
    function Get_Type( args : Value_Array ) return Value_Ptr
    is (Strings.Create( Value_Type'Image( args(1).Get.Get_Type ) ).As_Value);

    ----------------------------------------------------------------------------

    -- head( value : string|list, length : number ) return string|list
    function Head( args : Value_Array ) return Value_Ptr is
    begin
        if Check_Types( args, (V_STRING, V_NUMBER) ) then
            declare
                len : constant Integer := Integer'Max( Cast_Int( args(2) ), 0 );
                str : constant String := Cast_String( args(1) );
            begin
                if len < str'Length then
                    return Create( Head( str, len ) ).As_Value;
                else
                    return Clone( args(1) );
                end if;
            end;

        elsif Check_Types( args, (V_LIST, V_NUMBER) ) then
            declare
                len  : constant Integer := Integer'Max( Cast_Int( args(2) ), 0 );
                list : List_Ptr;
            begin
                if len < As_List( args(1) ).Get.Length then
                    list := Create_List;
                    for i in 1..len loop
                        list.Get.Append( As_List( args(1) ).Get.Get( i ) );
                    end loop;
                    return list.As_Value;
                else
                    return Clone( args(1) );
                end if;
            end;
        end if;

        return Errors.Create( Invalid_Arguments, "Expected (string|list, number)" );
    end Head;

    ----------------------------------------------------------------------------

    -- img( what : any ) return string
    function Image( args : Value_Array ) return Value_Ptr is (Strings.Create( Image( args(1) ) ).As_Value);

    ----------------------------------------------------------------------------

    -- keys( value : association ) return list
    function Keys( args : Value_Array ) return Value_Ptr is
    begin
        if args(1).Get.Get_Type /= V_ASSOCIATION then
            return Errors.Create( Invalid_Arguments, "Expected (association)" );
        end if;

        return As_Assoc( args(1) ).Get.Get_Keys.As_Value;
    end Keys;

    ----------------------------------------------------------------------------

    -- length( value : association|list|string ) return number
    function Length( args : Value_Array ) return Value_Ptr is
    begin
        case args(1).Get.Get_Type is
            when V_ASSOCIATION => return Numbers.Create( As_Assoc( args(1) ).Get.Size ).As_Value;
            when V_LIST        => return Numbers.Create( As_List( args(1) ).Get.Length ).As_Value;
            when V_STRING      => return Numbers.Create( As_String( args(1) ).Get.Length ).As_Value;
            when others        => return Errors.Create( Invalid_Arguments, "Expected (association|list|string)" );
        end case;
    end Length;

    ----------------------------------------------------------------------------

    -- print( str : string ) return null
    function Print( args : Value_Array ) return Value_Ptr is
    begin
        if args(1).Get.Get_Type /= V_STRING then
            return Errors.Create( Invalid_Arguments, "Expected (string)" );
        end if;

        Ada.Text_IO.Put( Cast_String( args(1) ) );
        return Create_Null;
    end Print;

    ----------------------------------------------------------------------------

    -- println( str : string ) return null
    function Print_Line( args : Value_Array ) return Value_Ptr is
    begin
        if args(1).Get.Get_Type /= V_STRING then
            return Errors.Create( Invalid_Arguments, "Expected (string)" );
        end if;

        Ada.Text_IO.Put( Cast_String( args(1) ) & ASCII.LF );
        return Create_Null;
    end Print_Line;

    ----------------------------------------------------------------------------

    -- reverse( value : string|list ) return string|list
    function Reversed( args : Value_Array ) return Value_Ptr is
    begin
        if args(1).Get.Get_Type = V_STRING then
            declare
                str    : constant String := Cast_String( args(1) );
                result : String(1..str'Length);
            begin
                for i in 1..str'Length loop
                    result(i) := str(str'Last-i+1);
                end loop;
                return Strings.Create( result ).As_Value;
            end;

        elsif args(1).Get.Get_Type = V_List then
            declare
                result : constant List_Ptr := Create_List;
            begin
                for i in reverse 1..As_List( args(1) ).Get.Length loop
                    result.Get.Append( As_List( args(1) ).Get.Get( i ) );
                end loop;
                return result.As_Value;
            end;
        end if;

        return Errors.Create( Invalid_Arguments, "Expected (string|list)" );
    end Reversed;

    ----------------------------------------------------------------------------

    -- run( path : string ) return string
    function Run( args : Value_Array ) return Value_Ptr is
        file     : File_Type;
        contents : Unbounded_String;
        func     : Function_Ptr;
        thread   : A_Thread;
        result   : Value_Ptr;
    begin
        if args(1).Get.Get_Type /= V_STRING then
            return Errors.Create( Invalid_Arguments, "Expected (string)" );
        end if;

        Ada.Text_IO.Open( file, In_File, Cast_String( args(1) ) );
        while not End_Of_File( file ) loop
            contents := contents & Get_Line( file ) & ASCII.LF;
        end loop;
        Close( file );

        declare
            source : constant String := "function() " & To_String( contents ) & " end function";
        begin
            func := compiler.Compile( source );
            thread := vm.Create_Thread( func );
            vm.Run_Complete( thread );
            result := thread.Get_Result;
            Delete( thread );
        exception
            when e : Parse_Error =>
                Ada.Text_IO.New_Line;
                Display_Parse_Error( source, Exception_Message( e ) );
                Ada.Text_IO.New_Line;
                raise;
        end;

        return result;
    exception
        when e : others =>
            return Errors.Create( Generic_Exception,
                                  Exception_Name( e ) & ": " & Exception_Message( e ) );
    end Run;

    ----------------------------------------------------------------------------

    -- slice( value : string|list, start : number, length : number ) return string|list
    function Slice( args : Value_Array ) return Value_Ptr is
        first    : Integer;
        outFirst : Integer;
        outLast  : Integer;
    begin
        if Check_Types( args, (V_STRING, V_NUMBER, V_NUMBER) ) then
            declare
                str : constant String := Cast_String( args(1) );
            begin
                first := Cast_Int( args(2) );
                outFirst := Integer'Max( str'First, first );
                outLast := Integer'Min( str'Last, first + Integer'Max( Cast_Int( args(3) ), 0 ) - 1 );
                return Strings.Create( str(outFirst..outLast) ).As_Value;
            end;

          elsif Check_Types( args, (V_LIST, V_NUMBER, V_NUMBER) ) then
              declare
                  list : constant List_Ptr := Create_List;
              begin
                  first := Cast_Int( args(2) );
                  outFirst := Integer'Max( 1, first );
                  outLast := Integer'Min( As_List( args(1) ).Get.Length,
                                          first + Integer'Max( Cast_Int( args(3) ), 0 ) - 1 );
                  for i in outFirst..outLast loop
                      list.Get.Append( As_List( args(1) ).Get.Get( i ) );
                  end loop;
                  return list.As_Value;
              end;
        end if;

        return Errors.Create( Invalid_Arguments, "Expected (string|list, number)" );
    end Slice;

    ----------------------------------------------------------------------------

    -- tail( value : string|list, length : number ) return string|list
    function Tail( args : Value_Array ) return Value_Ptr is
    begin
        if Check_Types( args, (V_STRING, V_NUMBER) ) then
            declare
                len : constant Integer := Integer'Max( Cast_Int( args(2) ), 0 );
                str : constant String := Cast_String( args(1) );
            begin
                if len < str'Length then
                    return Create( Tail( str, len ) ).As_Value;
                else
                    return args(1);
                end if;
            end;

        elsif Check_Types( args, (V_LIST, V_NUMBER) ) then
            declare
                len   : constant Integer := Integer'Max( Cast_Int( args(2) ), 0 );
                inLen : constant Integer := As_List( args(1) ).Get.Length;
                list  : List_Ptr;
            begin
                if len < inLen then
                    list := Create_List;
                    for i in (inLen - len + 1)..inLen loop
                        list.Get.Append( As_List( args(1) ).Get.Get( i ) );
                    end loop;
                    return list.As_Value;
                else
                    return Clone( args(1) );
                end if;
            end;
        end if;

        return Errors.Create( Invalid_Arguments, "Expected (string|list, number)" );
    end Tail;

    ----------------------------------------------------------------------------

    -- trim( str : string ) return string
    function Trim( args : Value_Array ) return Value_Ptr is
    begin
        if args(1).Get.Get_Type /= V_STRING then
            return Errors.Create( Invalid_Arguments, "Expected (string)" );
        end if;

        return Strings.Create( Trim( Cast_String( args(1) ), Both ) ).As_Value;
    end Trim;

    --==========================================================================

    done   : Boolean := False;
    input  : String(1..1024);
    last   : Integer := input'First - 1;
    func   : Function_Ptr;

    thread : A_Thread;

    threadNamespace : A_Scribble_Namespace := new Simple_Namespace;
begin
    runtime.Define( Fid_Hash( "head" ), Console.Head'Unrestricted_Access,          2, 2, True );
    runtime.Define( Fid_Hash( "image" ), Console.Image'Unrestricted_Access,        1, 1, True );
    runtime.Define( Fid_Hash( "keys" ), Console.Keys'Unrestricted_Access,          1, 1, True );
    runtime.Define( Fid_Hash( "length" ), Console.Length'Unrestricted_Access,      1, 1, True );
    runtime.Define( Fid_Hash( "print" ), Console.Print'Unrestricted_Access,        1, 1, True );
    runtime.Define( Fid_Hash( "println" ), Console.Print_Line'Unrestricted_Access, 1, 1, True );
    runtime.Define( Fid_Hash( "reverse" ), Console.Reversed'Unrestricted_Access,   1, 1, True );
    runtime.Define( Fid_Hash( "slice" ), Console.Slice'Unrestricted_Access,        3, 3, True );
    runtime.Define( Fid_Hash( "tail" ), Console.Tail'Unrestricted_Access,          2, 2, True );
    runtime.Define( Fid_Hash( "trim" ), Console.Trim'Unrestricted_Access,          1, 1, True );
    runtime.Define( Fid_Hash( "type" ), Console.Get_Type'Unrestricted_Access,      1, 1, True );

    runtime.Define( Fid_Hash( "run" ), Console.Run'Unrestricted_Access,            1, 1, True );

    runtime.Reserve_Namespace( "reserved" );
    runtime.Reserve_Namespace( "thread" );
    runtime.Register_Namespace( "test", new Simple_Namespace );
    runtime.Register_Namespace( "", new Simple_Namespace );   -- the anonymous namespace

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    Ada.Text_IO.Put_Line( "Scribble Console v1.0" );
    Ada.Text_IO.Put_Line( "Copyright (C) Kevin Wellwood 2014" );
    Ada.Text_IO.Put_Line( "Type 'quit' to quit." );
    Ada.Text_IO.Put_Line( "" );

    while not done loop
        Ada.Text_IO.Put( "> " );
        Ada.Text_IO.Get_Line( input, last );
        exit when input(input'First..last) = "quit";

        if last >= input'First then
            declare
                source : constant String := "function() return " & input(input'First..last) & "; end function";
            begin
                func := compiler.Compile( source );
                thread := vm.Create_Thread( func );
                thread.Register_Namespace( "thread", threadNamespace );
                vm.Run_Complete( thread );
                if thread.Get_Result /= Create_Null then
                    Ada.Text_IO.Put_Line( Image( thread.Get_Result ) );
                end if;
                Delete( thread );
            exception
                when e : Parse_Error =>
                    Ada.Text_IO.New_Line;
                    Display_Parse_Error( source, Exception_Message( e ) );
                    Ada.Text_IO.New_Line;
            end;
        end if;
    end loop;

    Delete( compiler );
    Delete( vm );
    Delete( runtime );
end Console;
