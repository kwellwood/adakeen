--
-- Copyright (c) 2012 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Calendar;                      use Ada.Calendar;
with Ada.Calendar.Formatting;           use Ada.Calendar.Formatting;
with Ada.Exceptions;                    use Ada.Exceptions;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Ada.Text_IO;
with Evaluator_Test_Node;               use Evaluator_Test_Node;
with Scripting;                         use Scripting;
with Scripting.Evaluators;              use Scripting.Evaluators;
with Values;                            use Values;

procedure Evaluator_Test is

    ----------------------------------------------------------------------------

    function astr( str : String ) return String_Access is
    begin
        return new String'(str);
    end astr;
    pragma Inline_Always( astr );

    ----------------------------------------------------------------------------

    type Test_Case is
        record
            input  : String_Access;
            expect : String_Access;
        end record;

    type Test_Array is array(Positive range <>) of Test_Case;

    expressions : Test_Array := Test_Array'(
        -- basic tests
        (astr("1"),          astr("1")),
        (astr("1.75"),       astr("2")),
        (astr("""Hello!"""), astr("""Hello!""")),
        (astr("undefined"),  astr("null")),
        (astr("1+"),         astr("TOKENS.SCANNERS.PARSE_EXCEPTION - Expected expression at 1:2")),

        -- +
        (astr("1+2"),                         astr("3")),
        (astr("1+2+3"),                       astr("6")),
        (astr("1+undefined"),                 astr("null")),
        (astr("1+""Hello"""),                 astr("null")),
        (astr("""Hello""+1"),                 astr("null")),
        (astr("""1""+2"),                     astr("null")),
        (astr("1+""2"""),                     astr("null")),
        (astr("""Hello""+"", ""+""World!"""), astr("""Hello, World!""")),
        (astr("""Hello""+undefined"),         astr("null")),
        (astr("undefined+""Hello"""),         astr("null")),

        -- - (binary)
        (astr("1-2"),         astr("-1")),
        (astr("1-2-3"),       astr("-4")),
        (astr("1-undefined"), astr("null")),
        (astr("undefined-1"), astr("null")),
        (astr("""a""-""b"""), astr("null")),

        -- *
        (astr("2*3"),         astr("6")),
        (astr("1*0*1"),       astr("0")),
        (astr("1+2*3"),       astr("7")),
        (astr("(1+2)*3"),     astr("9")),
        (astr("0*undefined"), astr("null")),
        (astr("5*""five"""),  astr("null")),

        -- /
        (astr("6/3"),         astr("2")),
        (astr("21/11"),       astr("1")),
        (astr("4/3"),         astr("1")),
        (astr("8/4/2"),       astr("1")),
        (astr("1+8/4"),       astr("3")),
        (astr("(8+8)/4"),     astr("4")),
        (astr("1/undefined"), astr("null")),
        (astr("5/""five"""),  astr("null")),

        -- %
        (astr("1%2"),          astr("1")),
        (astr("9%5%3"),        astr("1")),
        (astr("10%undefined"), astr("null")),
        (astr("-3%2"),         astr("1")),
        (astr("5%-3"),         astr("-1")),
        (astr("undefined%10"), astr("null")),
        (astr("""a""%""b"""),  astr("null")),

        -- order of operations
        (astr("5-3+4"),            astr("6")),
        (astr("5-(3+4)"),          astr("-2")),
        (astr("2+3*3"),            astr("11")),
        (astr("(2+3)*3"),          astr("15")),
        (astr("(2-1)+(15-(5-3))"), astr("14")),

        -- =
        (astr("1=1"),                 astr("True")),
        (astr("2*3=5+1"),             astr("True")),
        (astr("4-5=0-1"),             astr("True")),
        (astr("""a""=""a"""),         astr("True")),
        (astr("(1=1)=(1=1)"),         astr("True")),
        (astr("2.3=2.4 // floats not yet supported"), astr("False")),
        (astr("2.4=2.6 // floats not yet supported"), astr("False")),
        (astr("1=2"),                 astr("False")),
        (astr("""a""=""A"""),         astr("False")),
        (astr("1=undefined"),         astr("null")),
        (astr("""a""=undefined"),     astr("null")),
        (astr("(1=0)=undefined"),     astr("null")),
        (astr("(1=1)=undefined"),     astr("null")),
        (astr("undefined=undefined"), astr("null")),

        -- !=
        (astr("1!=1"),                 astr("False")),
        (astr("2*3!=5+1"),             astr("False")),
        (astr("4-5!=0-1"),             astr("False")),
        (astr("""a""!=""a"""),         astr("False")),
        (astr("(1=1)!=(1=1)"),         astr("False")),
        (astr("2.3!=2.4 // floats not yet supported"), astr("True")),
        (astr("2.4!=2.6 // floats not yet supported"), astr("True")),
        (astr("1!=2"),                 astr("True")),
        (astr("""a""!=""A"""),         astr("True")),
        (astr("1!=undefined"),         astr("null")),
        (astr("""a""!=undefined"),     astr("null")),
        (astr("(1!=0)!=undefined"),    astr("null")),
        (astr("(1!=1)!=undefined"),    astr("null")),
        (astr("undefined!=undefined"), astr("null")),

        -- >
        (astr("5>4"),               astr("True")),
        (astr("2.6>2.4 // floats not yet supported"), astr("True")),
        (astr("2.4>2.3 // floats not yet supported"), astr("True")),
        (astr("0-3>0-4"),           astr("True")),
        (astr("""a"">""A"""),       astr("True")),
        (astr("""2"">""10"""),      astr("True")),
        (astr("""a"">""a"""),       astr("False")),
        (astr("""abc"">""bcd"""),   astr("False")),
        (astr("1>1"),               astr("False")),
        (astr("""abc"">undefined"), astr("null")),
        (astr("undefined>""abc"""), astr("null")),
        (astr("""one"">2"),         astr("null")),

        -- >=
        (astr("5>=4"),                 astr("True")),
        (astr("3>=3.2 // floats not yet supported"), astr("False")),
        (astr("3>=3"),                 astr("True")),
        (astr("""a"">=""a"""),         astr("True")),
        (astr("""b"">=""a"""),         astr("True")),
        (astr("""bet"">=""bad"""),     astr("True")),
        (astr("""bolt"">=""bop"""),    astr("False")),
        (astr("1>=undefined"),         astr("null")),
        (astr("undefined>=1"),         astr("null")),
        (astr("undefined>=""b"""),     astr("null")),
        (astr("undefined>=undefined"), astr("null")),

        -- <
        (astr("4<5"),               astr("True")),
        (astr("2.4<2.6 // floats not yet supported"), astr("True")),
        (astr("2.3<2.4 // floats not yet supported"), astr("True")),
        (astr("0-4<0-3"),           astr("True")),
        (astr("""A""<""a"""),       astr("True")),
        (astr("""10""<""2"""),      astr("True")),
        (astr("""a""<""a"""),       astr("False")),
        (astr("""bcd""<""abc"""),   astr("False")),
        (astr("1<1"),               astr("False")),
        (astr("""abc"">undefined"), astr("null")),
        (astr("undefined>""abc"""), astr("null")),
        (astr("""one"">2"),         astr("null")),

        -- <=
        (astr("4<=5"),                 astr("True")),
        (astr("3.2<=3 // floats not yet supported"), astr("False")),
        (astr("3<=3"),                 astr("True")),
        (astr("""a""<=""a"""),         astr("True")),
        (astr("""a""<=""b"""),         astr("True")),
        (astr("""bad""<=""bet"""),     astr("True")),
        (astr("""bop""<=""bolt"""),    astr("False")),
        (astr("undefined<=1"),         astr("null")),
        (astr("1<=undefined"),         astr("null")),
        (astr("""b""<=undefined"),     astr("null")),
        (astr("undefined<=undefined"), astr("null")),

        -- &&
        (astr("0&&0"),                astr("False")),
        (astr("1&&0"),                astr("False")),
        (astr("0&&1"),                astr("False")),
        (astr("1&&1"),                astr("True")),
        (astr("0.4&&1 // floats not yet supported"), astr("True")),
        (astr("0.6&&1 // floats not yet supported"), astr("True")),
        (astr("!0&&!0"),              astr("True")),
        (astr("!0&&!0&&!0&&!0"),      astr("True")),
        (astr("!0&&!1"),              astr("False")),
        (astr("!0&&!0&&!1&&!0"),      astr("False")),
        (astr("1&&-1"),               astr("True")),
        (astr("1&&1-1"),              astr("False")),
        (astr("1&&undefined"),        astr("False")),
        (astr("undefined&&""a"""),    astr("null")),
        (astr("""True""&&""False"""), astr("null")),

        -- ||
        (astr("1||1"),             astr("True")),
        (astr("1||0"),             astr("True")),
        (astr("0||1"),             astr("True")),
        (astr("0||0"),             astr("False")),
        (astr("1||undefined"),     astr("True")),
        (astr("0||undefined"),     astr("False")),
        (astr("undefined||1"),     astr("True")),
        (astr("""a""||""a"""),     astr("null")),
        (astr("1||""a"""),         astr("null")),
        (astr("undefined||""a"""), astr("null")),

        -- - (unary)
        (astr("-1"),         astr("-1")),
        (astr("-""2"""),     astr("null")),
        (astr("-(1)"),       astr("-1")),
        (astr("-(2*3)"),     astr("-6")),
        (astr("-(2*-3)"),    astr("6")),
        (astr("-0"),         astr("0")),
        (astr("-(1=2)"),     astr("0")),
        (astr("-(1=1)"),     astr("-1")),
        (astr("-1=2"),       astr("False")),
        (astr("--2"),        astr("2")),
        (astr("-undefined"), astr("null")),
        (astr("-""a"""),     astr("null")),
        (astr("3--2"),       astr("5")),
        (astr("-!(0=0)"),    astr("0")),

        -- !
        (astr("!(2=1)"),        astr("True")),
        (astr("!(1=1)=!(2=2)"), astr("True")),
        (astr("!0"),            astr("True")),
        (astr("!2"),            astr("False")),
        (astr("!(1=1)"),        astr("False")),
        (astr("!2=1"),          astr("null")),
        (astr("!undefined"),    astr("null")),
        (astr("!"""""),         astr("null")),
        (astr("!""True"""),     astr("null")),
        (astr("!-!(0=0)"),      astr("True")),
        (astr("!-0"),           astr("True")),

        -- order of operations contd.
        (astr("(!0*4>5&&-4+8!=3--1)||5+3=8"), astr("True")),

        -- variables
        (astr("one"),     astr("1")),
        (astr("one+one"), astr("2")),
        (astr("yes"),     astr("True")),
        (astr("!yes"),    astr("False")),
        (astr("b.c"),     astr("""bee.sea""")),
        (astr("b.c+b.c"), astr("""bee.seabee.sea""")),

        -- functions
        (astr("min(-1,1)"),  astr("-1")),
        (astr("max(3,4)"),   astr("4")),
        (astr("day()"),      astr("""" & Day_Of_Week( Clock )'Img & """")),
        (astr("day(True)"),  astr("null")),
        (astr("max(1,2,3)"), astr("null")),
        (astr("min(0)"),     astr("null")),

        (astr(""), astr("null"))
    );

    scripts : Test_Array := Test_Array'(
        -- script tests
        (astr("min(0, 1);"),    astr("")),
        (astr("min(1);"),       astr("")),
        (astr("min();"),        astr("")),
        (astr("min()"),         astr("TOKENS.SCANNERS.PARSE_EXCEPTION - Expected TK_SEMICOLON, found TK_EOF at 1:5")),
        (astr("undefined(1);"), astr("")),
        (astr("undefined();"),  astr("")),
        (astr("undefined()"),   astr("TOKENS.SCANNERS.PARSE_EXCEPTION - Expected TK_SEMICOLON, found TK_EOF at 1:11")),

        (astr("min(0, 1); min(0, 1);"), astr("")),
        (astr("min(1); min(0, 1);"),    astr("")),
        (astr("min(); min(0, 1);"),     astr("")),
        (astr("min() min(0, 1);"),      astr("TOKENS.SCANNERS.PARSE_EXCEPTION - Expected TK_SEMICOLON, found TK_IDENTIFIER at 1:7")),

        (astr("min(0, 1); min(1);"), astr("")),
        (astr("min(1); min(1);"),    astr("")),
        (astr("min(); min(1);"),     astr("")),
        (astr("min() min(1);"),      astr("TOKENS.SCANNERS.PARSE_EXCEPTION - Expected TK_SEMICOLON, found TK_IDENTIFIER at 1:7")),

        (astr("min(0, 1); min()"), astr("TOKENS.SCANNERS.PARSE_EXCEPTION - Expected TK_SEMICOLON, found TK_EOF at 1:16")),
        (astr("min(1); min()"),    astr("TOKENS.SCANNERS.PARSE_EXCEPTION - Expected TK_SEMICOLON, found TK_EOF at 1:13")),
        (astr("min(); min()"),     astr("TOKENS.SCANNERS.PARSE_EXCEPTION - Expected TK_SEMICOLON, found TK_EOF at 1:12")),
        (astr("min() min()"),      astr("TOKENS.SCANNERS.PARSE_EXCEPTION - Expected TK_SEMICOLON, found TK_IDENTIFIER at 1:7")),

        (astr(""), astr(""))
    );

    ----------------------------------------------------------------------------

    evaluator : A_Evaluator;
    node      : A_Evaluation_Node;
    failed    : Boolean := False;
begin
    evaluator := Create_Evaluator;
    node := new Eval_Node;
    for i in expressions'Range loop
        declare
            value : A_Value;
        begin
            evaluator.Evaluate_Expression( expressions(i).input.all, node, value );
            if expressions(i).expect.all /= Img( value ) then
                Ada.Text_IO.Put_Line( "Expression test" & i'Img & ": Failed" );
                Ada.Text_IO.Put_Line( "  Input   : " & expressions(i).input.all );
                Ada.Text_IO.Put_Line( "  Expected: " & expressions(i).expect.all );
                Ada.Text_IO.Put_Line( "  Actual  : " & Img( value ) );
                failed := True;
            end if;
            Delete( value );
        exception
            when e : others =>
                if expressions(i).expect.all /= Exception_Name( e ) & " - " & Exception_Message( e ) then
                    Ada.Text_IO.Put_Line( "Expression test" & i'Img & ": Failed" );
                    Ada.Text_IO.Put_Line( "  Input   : " & expressions(i).input.all );
                    Ada.Text_IO.Put_Line( "  Expected: " & expressions(i).expect.all );
                    Ada.Text_IO.Put_Line( "  Actual  : " & Exception_Name( e ) & " - " & Exception_Message( e ) );
                    failed := True;
                end if;
                Delete( value );
        end;
    end loop;
    for i in scripts'Range loop
        declare
            value : A_Value;
        begin
            evaluator.Evaluate_Script( scripts(i).input.all, node );
        exception
            when e : others =>
                if scripts(i).expect.all /= Exception_Name( e ) & " - " & Exception_Message( e ) then
                    Ada.Text_IO.Put_Line( "Script test" & i'Img & ": Failed" );
                    Ada.Text_IO.Put_Line( "  Input   : " & scripts(i).input.all );
                    Ada.Text_IO.Put_Line( "  Expected: " & scripts(i).expect.all );
                    Ada.Text_IO.Put_Line( "  Actual  : " & Exception_Name( e ) & " - " & Exception_Message( e ) );
                    failed := True;
                end if;
                Delete( value );
        end;
    end loop;
    Delete( evaluator );

    if not failed then
        Ada.Text_IO.Put_Line( "All tests passed." );
    end if;
end Evaluator_Test;
