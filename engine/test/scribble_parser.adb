--
-- Copyright (c) 2013-2014 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Scribble.Tests.Parser_Test;

procedure Scribble_Parser is
begin
    Scribble.Tests.Parser_Test.Test;
end Scribble_Parser;
