
with Ada.Text_IO;                       use Ada.Text_IO;
with Values2;                           use Values2;
with Values2.Lists;                     use Values2.Lists;
with Values2.Strings;                   use Values2.Strings;

procedure Valtest is
    n : Value;
    x : Value;
    y : Value;
    b1 : Value;
    b2 : Value;
begin
    Values2.Write := Ada.Text_IO.Put_Line'Access;
    Put_Line( "Valtest" );

    x  := Create( 12345 );
    y  := Create( 123.45 );
    b1 := Create( True );
    b2 := Create( False );

    if n.Is_Null then
        Put_Line( "n is null" );
    end if;

    if x.Is_Number then
        Put_Line( "x is number" );
    end if;

    if not y.Is_Boolean then
        Put_Line( "y not boolean" );
    end if;

    if b1.Is_Boolean then
        Put_Line( "b1 is boolean" );
    end if;
    if b2.Is_Boolean then
        Put_Line( "b2 is boolean" );
    end if;
    if b1 = b2 then
        Put_Line( "b1 = b2" );
    else
        Put_Line( "b1 /= b2" );
    end if;

    b1 := x;
    if b1.Is_Number then
        Put_Line( "b1 is now number" );
    end if;
    if b1 = x then
        Put_Line( "b1 = x = " & Float'Image( b1.To_Float ) );
    end if;

    --------
    Put_Line( "  " );

    declare
        s : Value := Create( "Hello, World" );
    begin
        if s.Is_String then
            Put_Line( "s is string = '" & s.Str.To_String & "'" );
        end if;

        Put_Line( "s := y" );
        s := y;
        Put_Line( "s = " & s.To_Int'Img );

        Put_Line( "s = y ? " & Boolean'Image( s = y ) );

        s := Create( 123.45 );
        Put_Line( "s := 123.45, s = y ? " & Boolean'Image( s = y ) );
    end;

    --------
    Put_Line( "  " );

    declare
        s1 : Value := Create( "Same" );
        s2 : Value := Create( "Same" );
    begin
        Put_Line( "Compare by value works? " & Boolean'Image( s1 = s2 ) );
    end;

    --------
    Put_Line( "  " );

    declare
        target : Value := Create( "original" );
        copy   : VAlue := target;
        ref    : Value;
    begin
        Put_Line( "target = '" & target.Str.To_String & "'" );

        ref := Create_Ref( target'Unrestricted_Access );
        Put_Line( "Created ref: ref.deref = '" & ref.Deref.Str.To_String & "'" );

        Put_Line( "ref.deref := 'new!'" );
        Replace( ref, Create( "new!" ) );

        Put_Line( "target = '" & target.Str.To_String & "'" );
        Put_Line( "Verify: ref.deref = '" & ref.Deref.Str.To_String & "'" );
    end;

    --------
    Put_Line( "  " );

    declare
        l : Value := Create_List( (Create( 1 ), Create( "Two" )) );
    begin
        Put_Line( "list len = " & l.Lst.Length'Img );
        As_List( l ).Append( Create( 3 ) );
        Put_Line( "list len = " & l.Lst.Length'Img );
    end;

    --------
    Put_Line( "  " );

    declare
        l : Value := Create_List( (Create( "" ), Create( "" )) );
    begin
        Put_Line( "tail(3) = len " & l.Lst.Tail( 3 ).Lst.Length'Img );
    end;

end Valtest;

