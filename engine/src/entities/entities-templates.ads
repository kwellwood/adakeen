--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Vectors;
with Entities.Component_Definitions;    use Entities.Component_Definitions;
with Version;

package Entities.Templates is

    type Template is new Limited_Object with private;
    type A_Template is access all Template'Class;

    -- Creates a new entity template, given the components and attributes of
    -- which it is comprised. This template can instantiate entities matching
    -- its configuration. 'attrDefinitions' contains entity attribute definitions
    -- that describe how the attribute should be handled in the level editor.
    -- 'attrValues' contains default values for entity attributes. 'toolParams'
    -- contains parameters describing how tools should handle this template and
    -- the entities created from it in the level editor.
    function Create_Template( name            : String;
                              attrDefinitions : Map_Value;
                              attrValues      : Map_Value;
                              toolParams      : Map_Value ) return A_Template;

    -- Creates a new entity parameterized with 'properties'. The 'properties'
    -- map should contain map values only, keyed by the names of the template's
    -- component definitions. The only exception is the "id" key, which may be
    -- given with a V_ID value to explicitly set the new entity's id instead of
    -- using a randomly generated one.
    function Create_Entity( this       : not null access Template'Class;
                            properties : Map_Value ) return A_Entity;

    -- Returns a map of the template's entity attributes by reference. This is
    -- not a copy.
    function Get_Attributes( this : not null access Template'Class ) return Map_Value;
    pragma Postcondition( Get_Attributes'Result.Valid );

    -- Returns the entity attribute definitions for entity template 'template'
    -- (by reference). These are used by the level editor to determine how
    -- attributes are displayed and constrained.
    function Get_Attribute_Definitions( this : not null access Template'Class ) return Map_Value;
    pragma Postcondition( Get_Attribute_Definitions'Result.Valid );

    -- Returns the template's name.
    function Get_Name( this : not null access Template'Class ) return String;

    -- Returns the level editor tool parameters for entity template 'template'
    -- (by reference). These are used by the level editor to define how tools
    -- should interact with entities of this template.
    function Get_Tool_Parameters( this : not null access Template'Class ) return Map_Value;
    pragma Postcondition( Get_Tool_Parameters'Result.Valid );

    -- Includes component 'def' in the entity, parameterized with overriding
    -- properties 'properties' at instantiation.
    procedure Include_Component( this       : not null access Template'Class;
                                 def        : A_Definition;
                                 properties : Map_Value );

    -- Returns True if the template contains at least one component definition
    -- 'defName', or a component definition that extends from it.
    function Includes_Component( this    : not null access Template'Class;
                                 defName : String ) return Boolean;

    procedure Delete( this : in out A_Template );

private

    type Component_Inclusion is
        record
            def        : A_Definition;        -- component definition (not owned!)
            properties : Map_Value;           -- properties for instantiation
        end record;

    package Component_Lists is new Ada.Containers.Vectors( Positive, Component_Inclusion );

    type Template is new Limited_Object with
        record
            name       : Unbounded_String;
            components : Component_Lists.Vector;
            attributes : Map_Value;
            attrDefs   : Map_Value;
            toolParams : Map_Value;
        end record;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    DO_VALIDATION_CHECKS : constant Boolean := Version.Is_Debug;

end Entities.Templates;
