--
-- Copyright (c) 2014-2015 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Unchecked_Deallocation;

package body Entities.States is

    function Create_State_Machine( global       : not null A_State;
                                   initialState : State_Id := GLOBAL_STATE
                                 ) return A_State_Machine is
        this : constant A_State_Machine := new State_Machine;
    begin
        this.Construct( global, initialState );
        return this;
    end Create_State_Machine;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this         : access State_Machine;
                         global       : not null A_State;
                         initialState : State_Id ) is

        procedure Find_State is
            use State_Maps;
            pos : State_Maps.Cursor;
        begin
            if this.stack.First_Element.id = initialState or else
               this.stack.First_Element.subStates.Is_Empty
            then
                return;
            end if;

            pos := this.stack.First_Element.subStates.First;
            while Has_Element( pos ) loop
                this.stack.Prepend( Element( pos ) );
                Find_State;
                exit when this.stack.First_Element.id = initialState;
                this.stack.Delete_First;
                Next( pos );
            end loop;
        end Find_State;

    begin
        Limited_Object(this.all).Construct;
        global.id := GLOBAL_STATE;
        global.mach := A_State_Machine(this);
        this.stack.Prepend( global );
        Find_State;
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out State_Machine ) is
        global : A_State;
    begin
        -- the global state will always be at the bottom of the stack. deleting
        -- it will recursively delete all possible sub-states.
        if not this.stack.Is_Empty then
            global := this.stack.Last_Element;
            Delete( global );
        end if;
        Limited_Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Get_Frame( this : not null access State_Machine'Class ) return Natural is
        use State_Lists;
        initialSeq : constant Integer := this.seqNum;
        pos        : Cursor := this.stack.First;
        frame      : Natural := 0;
    begin
        while Has_Element( pos ) loop
            Element( pos ).mach := A_State_Machine(this);
            frame := Element( pos ).Get_Frame;
            exit when frame /= 0 or else this.seqNum /= initialSeq;
            Next( pos );
        end loop;
        return frame;
    end Get_Frame;

    ----------------------------------------------------------------------------

    function Get_Sequence( this : not null access State_Machine'Class ) return Integer is (this.seqNum);

    ----------------------------------------------------------------------------

    function Get_State( this : not null access State_Machine'Class ) return State_Id is (this.stack.First_Element.id);

    ----------------------------------------------------------------------------

    procedure Handle_Message( this   : not null access State_Machine'Class;
                              name   : Hashed_String;
                              params : Map_Value ) is
        use State_Lists;
        initialSeq : constant Integer := this.seqNum;
        pos        : Cursor := this.stack.First;
        handled    : Boolean := False;
    begin
        while Has_Element( pos ) loop
            Element( pos ).mach := A_State_Machine(this);
            Element( pos ).Handle_Message( name, params, handled );
            exit when handled or else this.seqNum /= initialSeq;
            Next( pos );
        end loop;
    end Handle_Message;

    ----------------------------------------------------------------------------

    function Host( this : not null access State_Machine'Class ) return Host_Access is (this.myHost);

    ----------------------------------------------------------------------------

    function In_State( this : not null access State_Machine'Class; id : State_Id ) return Boolean is
    begin
        for s of this.stack loop
            if s.id = id then
                return True;
            end if;
        end loop;
        return False;
    end In_State;

    ----------------------------------------------------------------------------

    procedure Pop_State( this : not null access State_Machine'Class ) is
        temp : A_State;
    begin
        if this.stack.Length > 1 then
            this.seqNum := this.seqNum + 1;
            temp := this.stack.First_Element;
            this.stack.Delete_First;
            temp.mach := A_State_Machine(this);
            temp.On_Exit;
        end if;
    end Pop_State;

    ----------------------------------------------------------------------------

    procedure Set_Host( this : not null access State_Machine'Class; host : Host_Access ) is
    begin
        this.myHost := host;
    end Set_Host;

    ----------------------------------------------------------------------------

    procedure Set_State( this : not null access State_Machine'Class; id : State_Id ) is
        use State_Lists;
        use State_Maps;
        subState : State_Maps.Cursor;
        temp     : A_State;
    begin
        pragma Assert( not this.stack.Is_Empty );

        loop
            -- check for matching ancestor
            if this.stack.First_Element.id = id then
                exit;  -- machine is in the matching state
            end if;

            -- look in substates
            subState := this.stack.First_Element.subStates.Find( id );
            if Has_Element( subState ) then
                this.seqNum := this.seqNum + 1;
                this.stack.Prepend( Element( subState ) );
                this.stack.First_Element.mach := A_State_Machine(this);
                this.stack.First_Element.On_Enter;
                exit;
            end if;

            if Integer(this.stack.Length) = 1 then
                exit;  -- machine reached the global state; can't back out further
            end if;

            -- back out to check the super state's other sub states
            this.seqNum := this.seqNum + 1;
            temp := this.stack.First_Element;
            this.stack.Delete_First;
            temp.mach := A_State_Machine(this);
            temp.On_Exit;
        end loop;
    end Set_State;

    ----------------------------------------------------------------------------

    procedure Update( this : not null access State_Machine'Class; time : Tick_Time ) is
        use State_Lists;
        initialSeq : constant Integer := this.seqNum;
        pos        : State_Lists.Cursor := this.stack.First;
        handled    : Boolean := False;
    begin
        while Has_Element( pos ) loop
            Element( pos ).mach := A_State_Machine(this);
            Element( pos ).Update( time, handled );
            exit when handled or else this.seqNum /= initialSeq;
            Next( pos );
        end loop;
    end Update;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_State_Machine ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

    --==========================================================================

    procedure Add_State( this  : not null access State'Class;
                         id    : State_Id;
                         state : not null A_State ) is
    begin
        state.id := id;
        this.subStates.Insert( state.id, state );
    end Add_State;

    ----------------------------------------------------------------------------

    function Host( this : not null access State'Class ) return Host_Access is (this.mach.myHost);

    ----------------------------------------------------------------------------

    function Machine( this : access State ) return A_State_Machine is (this.mach);

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_State ) is
        procedure Free is new Ada.Unchecked_Deallocation( State'Class, A_State );
    begin
        for sub of this.subStates loop
            Delete( sub );
        end loop;
        Free( this );
    end Delete;

end Entities.States;
