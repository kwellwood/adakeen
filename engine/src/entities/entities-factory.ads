--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Vectors;
with Entities.Component_Definitions;    use Entities.Component_Definitions;
with Entities.Templates;                use Entities.Templates;
with Version;

package Entities.Factory is

    -- Constructs entity components and entities from templated definitions
    type Entity_Factory is new Limited_Object with private;
    type A_Entity_Factory is access all Entity_Factory'Class;

    -- Creates and returns a new entity factory.
    function Create_Entity_Factory return A_Entity_Factory;
    pragma Postcondition( Create_Entity_Factory'Result /= null );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Loads all entity templates from file 'filename' in the "definitions"
    -- resource, returning True on success or False on failure.
    --
    -- The file contains a Scribble list of maps, each map describing an entity
    -- template. Alternativelty, the file may contain a map that describes a
    -- single entity template. The format follows:
    --
    -- [
    --     {Entity: "<new-template-name>"
    --         Components: {
    --             <definition-name>: {<property-name>: <property-value>, ...}
    --             ...
    --         }
    --         Attributes: {<attribute-name>: <attribute-value>, ...}
    --     }
    --     ...
    -- ]
    --
    -- The template constructs an entity by creating and adding components, in
    -- component dependency order. The names of the components to be added are
    -- eys within a "Components" map in the entity map. The map value associated
    -- with each component name is a set of property names and values to provide
    -- to the component instance at construction time. After adding all the
    -- listed components, a new entity is then assigned the entity attributes
    -- listed in the "Attributes" map of the entity map.
    --
    -- Multiple entity template files can be loaded but template names cannot be
    -- more than once.
    function Load_Entity_Templates( this     : not null access Entity_Factory'Class;
                                    filename : String ) return Boolean;

    -- Automatically finds and loads all *.entity files found in resources.
    procedure Load_Entity_Templates( this : not null access Entity_Factory'Class );

    -- Creates a new Entity, using the entity template named 'template'. The
    -- 'properties' map contains entity instance-specific values used to
    -- parameterize the new entity's components.
    --
    -- The structure of 'properties' is as follows:
    -- * If a key named 'id' is defined with a value type of Id_Value, its value
    --   will be used for the new entity's id. Otherwise, a random id will be
    --   assigned.
    -- * Each key that matches the name of a component template used by the
    --   entity template must have a map value. This value contains entity
    --   instance-specific properties to pass to the named component template.
    --   Depending on the component template, certain properties may be required
    --   (e.g. "x" and "y" for the "Location" component template), or may be
    --   optionally used to override the component template's defaults. See the
    --   specific component template's description for details.
    --
    -- Returns null if 'template' is unknown or cannot be constructed
    -- (ex: contains duplicate components).
    function Create_Entity( this       : not null access Entity_Factory'Class;
                            template   : String;
                            properties : Map_Value ) return A_Entity;

    -- Creates and returns a new entity based on entity 'e'. It will be created
    -- with the same template, the same id, same location, and same attributes.
    -- Beyond that, it will be a fresh copy of the same type of entity. This is
    -- used for refreshing the content of entities during development, when an
    -- entity template or component changes. The old entity 'e' must be deleted
    -- before the recreation can be used in the same world.
    --
    -- If the original entity template cannot be found, null will be returned.
    function Copy_Entity( this : not null access Entity_Factory'Class;
                          e    : not null A_Entity ) return A_Entity;

    -- Returns an entity template by name. Returns null if 'name' is undefined.
    function Get_Template( this : not null access Entity_Factory'Class;
                           name : String ) return A_Template;

    -- Iterates over all loaded entity templates, calling 'examine' with each.
    -- Templates are iterated in the order in which they were loaded, which is
    -- alphabetically by filename and first to last within each file.
    procedure Iterate_Templates( this    : not null access Entity_Factory'Class;
                                 examine : not null access procedure( template : A_Template ) );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Loads all entity component definitions from file 'filename' in the
    -- "definitions" resource, returning True on success or False on failure.
    --
    -- A component definition that was loaded from disk extends another named
    -- component definition by overriding or adding additional properties. All
    -- definitions written in Scribble must either directly extend a built-in
    -- component definition, or extend another component definition. For example:
    -- Component 'D' extends 'C', which extends 'B', which extends built-in 'A'
    --
    -- One or more component definitions are stored in a "*.component" file, in
    -- the "definitions" resource group. The file may contain a list of maps,
    -- each one defining a component, or a single map defining one component.
    -- The format follows:
    --
    -- [
    --     {Component: "<definition-name>"
    --         Extends: "<existing-definition-name>"
    --         Properties: {<property-name>: <property-value>, ...}
    --     }
    --     ...
    -- ]
    --
    -- The "Component" field must specify a new, unique component definition
    -- name. The "Extends" field must contain the name of an existing component
    -- definition, either a built-in (root) component definition, or the name of
    -- another component definition. The order in which components are defined
    -- does not matter- dependency names are not resolved until all components
    -- have been loaded.
    procedure Load_Component_Definitions( this : not null access Entity_Factory'Class );

    -- Creates a new Component with the component definition 'name', where
    -- 'properties' is a map of instance-specific property values used to
    -- parameterize the new component. Returns null if 'name' is not a
    -- recognized component definition.
    function Create_Component( this       : not null access Entity_Factory'Class;
                               name       : String;
                               properties : Map_Value ) return A_Component;

    function Create_Component( this : not null access Entity_Factory'Class;
                               name : String ) return A_Component
        is (this.Create_Component( name, Create_Map.Map ));

    -- Defines a root component definition by name 'name', given an allocator
    -- function 'allocator'. All components instantiated by the definition will
    -- be of the same Component family and will share the same initial properties,
    -- except for instance-specific properties given at the time of instantiation.
    -- This component definition can be extended by other Scribble component
    -- definitions in components.sv. See Load_Component_Definitions() for
    -- details.
    --
    -- 'propNames' is a list of the property names accepted by the allocator.
    -- Only property names in this list can be specified in components.sv,
    -- entities.sv, or Create_Component(). Unknown properties will be rejected.
    --
    -- 'required' is a list of names of components that must also be present in
    -- an Entity when this component is added.
    --
    -- A Constraint_Error will be raised if 'name' matches an existing component
    -- definition.
    procedure Define_Component( this      : not null access Entity_Factory'Class;
                                name      : String;
                                allocator : not null A_Component_Allocator;
                                propNames : Value_Array := Empty_Array;
                                required  : Value_Array := Empty_Array );


    -- Returns a component definition by name. Returns null if 'name' is undefined.
    function Get_Definition( this : not null access Entity_Factory'Class;
                             name : String ) return A_Definition;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Frees up all Entity Templates and Component Definitions on shutdown. Be
    -- sure to call this when shutting down. It should be called before
    -- shutting down any other systems that a component might depend on.
    procedure Delete( this : in out A_Entity_Factory );

    Global : A_Entity_Factory := null;

private

    package Definition_Registry is new Ada.Containers.Indefinite_Ordered_Maps( String, A_Definition, "<", "=" );

    package Template_Registry is new Ada.Containers.Indefinite_Ordered_Maps( String, A_Template, "<", "=" );

    package Template_List is new Ada.Containers.Vectors( Positive, A_Template, "=" );

    type Entity_Factory is new Limited_Object with
        record
            definitions        : Definition_Registry.Map;
            templates          : Template_Registry.Map;
            templatesLoadOrder : Template_List.Vector;       -- 'templates', in order of loading
        end record;

    procedure Delete( this : in out Entity_Factory );

    -- Validates all component definitions after they have been defined,
    -- ensuring none are parent-less or use invalid properties.
    function Validate_Definitions( this : not null access Entity_Factory'Class ) return Boolean;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    DO_VALIDATION_CHECKS : constant Boolean := Version.Is_Debug;

end Entities.Factory;

