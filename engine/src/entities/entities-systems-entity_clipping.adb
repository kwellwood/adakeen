--
-- Copyright (c) 2014-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Doubly_Linked_Lists;
with Ada.Containers.Ordered_Sets;
with Debugging;                         use Debugging;
with Entities.Solids;                   use Entities.Solids;
with Preferences;                       use Preferences;
with Vector_Math;                       use Vector_Math;

package body Entities.Systems.Entity_Clipping is

    DEFAULT_MAX_ITERATIONS : constant := 6;

    --==========================================================================

    function Create_Entity_Clipping_System return A_Component_System is
        this : constant A_Entity_Clipping_System := new Entity_Clipping_System;
    begin
        this.Construct;
        this.iterations := Get_Pref( "development", "systems.EntityClipping.iterations", DEFAULT_MAX_ITERATIONS );
        return A_Component_System(this);
    end Create_Entity_Clipping_System;

    ----------------------------------------------------------------------------

    procedure Resolve_Collision( man : Manifold ) is
        vRel         : Vec2;
        vAlongNormal : Float;
        entityAwins  : Boolean := False;
    begin
        -- relative velocity
        vRel := man.b.Get_V - man.a.Get_V;

        -- calculate relative velocity along the normal vector
        vAlongNormal := Dot( vRel, man.normal );
        if vAlongNormal > 0.0 then
            return;    -- entities are separating
        end if;

        if man.a.Get_Mass > man.b.Get_Mass then
            -- entity B will be moved out of entity A
            if man.normal.x /= 0.0 then
                man.b.Set_VX( man.a.Get_V.x );
                man.b.Set_X( man.a.Get_X + man.normal.x );
            else
                man.b.Set_VY( man.a.Get_V.y );
                man.b.Set_Y( man.a.Get_Y + man.normal.y );
            end if;

        elsif man.a.Get_Mass = man.b.Get_Mass then
            -- the entity that moved into the other will be moved out
            --
            -- because we know the entities are moving toward each other, the
            -- entity with the largest absolute velocity along the collision
            -- axis is the one that "loses" (e.g. moves) in the resolution.
            if man.normal.x /= 0.0 then
                -- todo: which one should win if a.VX = b.VX?
                --   both move 50%?
                --   use .Prev_X and .Prev_Y to determine which one moved furthest?
                entityAwins := (abs man.a.Get_V.x < abs man.b.Get_V.x);
                if entityAwins then
                    man.b.Set_VX( man.a.Get_V.x );
                    man.b.Set_X( man.a.Get_X + man.normal.x );
                else
                    man.a.Set_VX( man.b.Get_V.x );
                    man.a.Set_X( man.b.Get_X - man.normal.x );
                end if;
            else -- man.normal.y /= 0.0 then
                -- todo: which one should win if a.VY = b.VY?
                entityAwins := (abs man.a.Get_V.y < abs man.b.Get_V.y);
                if entityAwins then
                    man.b.Set_VY( man.a.Get_V.y );
                    man.b.Set_Y( man.a.Get_Y + man.normal.y );
                else
                    man.a.Set_VY( man.b.Get_V.y );
                    man.a.Set_Y( man.b.Get_Y - man.normal.y );
                end if;
            end if;

        else
            -- entity A will be moved out of entity B
            if man.normal.x /= 0.0 then
                man.a.Set_VX( man.b.Get_V.x );
                man.a.Set_X( man.b.Get_X - man.normal.x );
            else
                man.a.Set_VY( man.b.Get_V.y );
                man.a.Set_Y( man.b.Get_Y - man.normal.y );
            end if;

        end if;

        -- mark the contact between A and B so that the next collision
        -- resolution affecting A, B, or one of their contacts will respect this
        -- resolved constraint.
        man.a.Add_Contact(  man.normal, man.b );
        man.b.Add_Contact( -man.normal, man.a );
    end Resolve_Collision;

    ----------------------------------------------------------------------------

    package Solid_Lists is new Ada.Containers.Doubly_Linked_Lists( A_Solid, "=" );
    package Collision_Sets is new Ada.Containers.Ordered_Sets( Manifold, "<", "=" );

    overriding
    procedure Tick( this : access Entity_Clipping_System; time : Tick_Time ) is
        pragma Unreferenced( time );
        maxIters   : Integer := this.iterations;
        abortFrame : Boolean := False;

        -- Aborts the frame early to avoid the spiral of death. This is called
        -- when we detect the calculations are taking too long, and only when
        -- compiled DEBUG. Design your game such that this doesn't happen.
        procedure Abort_Frame is
        begin
            abortFrame := True;
        end Abort_Frame;

        -- Do one more iteration to check for unresolved collisions. This is
        -- only called when compiled DEBUG.
        procedure Test_System_Stability is
        begin
            maxIters := maxIters + 1;
        end Test_System_Stability;

        startTime  : constant Ada.Real_Time.Time := Clock;
        moved      : Solid_Lists.List;
        collisions : Collision_Sets.Set;
        a, b       : A_Solid;
        man        : Manifold;
    begin
        -- clear the contact cache built on the previous tick
        for n of this.components loop
            A_Solid(n).Stick_Down_To_Platform;
            A_Solid(n).Clear_Contacts;
        end loop;

        -- build a list of the entities that moved on the last update
        for component of this.components loop
            if A_Solid(component).Moved then
                moved.Append( A_Solid(component) );
            end if;
        end loop;

        -- do one extra iteration of collision checking only, to determine if
        -- the system has become unstable.
        pragma Debug( Test_System_Stability );

        for iter in 1..maxIters loop
            -- loop over all moved entities (m)
            for m of moved loop
                a := A_Solid(m);

                -- check for collisions with all other entities (n)
                for n of this.components loop
                    b := A_Solid(n);

                    if a /= b then
                        a.Detect_Collision( b, man );
                        if man.collision then
                            -- don't add reversed collisions (e.g. don't add B->A if A->B exists)
                            -- 'moved' and 'this.components' are already ordered,
                            -- so we know that we will always try to add A->B
                            -- before B->A, where A.Get_Id < B.Get_Id.
                            if a.Get_Owner.Get_Id < b.Get_Owner.Get_Id or else
                               not collisions.Contains( Manifold'(b, a, others => <>) )
                            then
                                collisions.Insert( man );
                            end if;
                        end if;
                    end if;
                end loop;
            end loop;

            if iter > this.iterations and then not collisions.Is_Empty then
                -- we can only get here when compiled DEBUG
                if this.stableSteps = STABILITY_STEPS_THRESHOLD then
                    -- the system was stable but this tick is ending with
                    -- unresolved collisions. the system is now unstable.
                    Dbg( "EntityClipping system is unstable" &
                         " (iterations =" & this.iterations'Img & ")",
                         D_PHYSICS, Warning );
                end if;
                this.stableSteps := 0;
                exit;
            end if;

            if collisions.Is_Empty then
                if this.stableSteps < STABILITY_STEPS_THRESHOLD then
                    -- we can only get here when compiled DEBUG
                    this.stableSteps := this.stableSteps + 1;
                    if this.stableSteps = STABILITY_STEPS_THRESHOLD then
                        -- we have now seen STABILITY_STEPS_THRESHOLD ticks since
                        -- the last tick that ended with unresolved collisions.
                        -- consider the sytem stable again.
                        Dbg( "EntityClipping system restabilized", D_PHYSICS, Warning );
                    end if;
                end if;
                exit;
            end if;

            pragma Debug( Dbg( "EntityClipping: Iteration" & iter'Img &
                               ": Solving" & collisions.Length'Img & " collisions",
                               D_PHYSICS, Info ) );
            for man of collisions loop
                Resolve_Collision( man );
                pragma Debug( Clock - startTime >= ABORT_THRESHOLD, Abort_Frame );
                exit when abortFrame;
            end loop;

            exit when abortFrame;
            collisions.Clear;
        end loop;

        for n of this.components loop
            A_Solid(n).Send_Collision_Messages;
        end loop;

        -- move entities affected by gravity that are standing on other solid,
        -- moving entities.
        for m of moved loop
            m.Move_With_Platform;
        end loop;
    end Tick;

end Entities.Systems.Entity_Clipping;

