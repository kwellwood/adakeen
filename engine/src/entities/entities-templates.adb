--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Debugging;                         use Debugging;
with Entities.Entity_Attributes;        use Entities.Entity_Attributes;
with Values.Casting;                    use Values.Casting;
with Values.Construction;               use Values.Construction;
with Values.Lists;                      use Values.Lists;

package body Entities.Templates is

    function Create_Template( name            : String;
                              attrDefinitions : Map_Value;
                              attrValues      : Map_Value;
                              toolParams      : Map_Value ) return A_Template is
        this : constant A_Template := new Template;
    begin
        this.Construct;
        this.name := To_Unbounded_String( name );
        this.attrDefs   := (if attrDefinitions.Valid then Clone( attrDefinitions ).Map else Create_Map.Map);
        this.attributes := (if attrValues.Valid then Clone( attrValues ).Map else Create_Map.Map);
        this.toolParams := (if toolParams.Valid then Clone( toolParams ).Map else Create_Map.Map);

        return this;
    end Create_Template;

    ----------------------------------------------------------------------------

    function Create_Entity( this       : not null access Template'Class;
                            properties : Map_Value ) return A_Entity is
        e : A_Entity := null;
    begin
        -- verify 'properties' only contains valid properties of components in
        -- in the entity template.
        declare
            procedure Validate_Components( componentName : String; props : Value ) is
                pragma Unreferenced( props );
            begin
                if componentName = "id" then
                    return;    -- special case for setting entity id
                elsif componentName = "name" then
                    return;    -- special case for setting entity name
                end if;
                for c of this.components loop
                    if componentName = c.def.Get_Name then
                        return;
                    end if;
                end loop;
                Dbg( "Properties given for unused component '" &
                     componentName & "' in entity template '" &
                     To_String( this.name ) & "'",
                     D_ENTITY, Warning );
            end Validate_Components;
        begin
            if DO_VALIDATION_CHECKS and then properties.Valid then
                properties.Iterate( Validate_Components'Access );
            end if;
        end;

        -- construct the entity object
        e := new Entity;
        e.Construct;
        if properties.Get( "id" ).Is_Id then
            -- override the entity's uniquely generated id
            -- useful for rebuilding an entity
            e.id := To_Tagged_Id( properties.Get( "id" ) );
        end if;
        if properties.Get( "name" ).Is_String then
            -- provide a unique name; its uniqueness will be verified when it's
            -- added to a world
            e.name := To_Unbounded_String( Cast_String( properties.Get( "name" ) ) );
        end if;
        e.template := this.name;

        -- populate the entity with components
        for c of this.components loop
            declare
                instanceProps : constant Map_Value := properties.Get( c.def.Get_Name ).Map;
                actualProps   : Map_Value;
                cInst         : A_Component;
            begin
                if DO_VALIDATION_CHECKS and then not c.def.Validate_Properties( instanceProps ) then
                    Dbg( "Unknown property given for component '" & c.def.Get_Name &
                         "' in entity template '" & To_String( this.name ) & "'",
                         D_ENTITY, Warning );
                end if;

                -- recursively merge the given instance properties with the
                -- entity template's defaults. 'instanceProps' overrides.
                actualProps := Recursive_Merge( c.properties, instanceProps ).Map;

                cInst := c.def.Create_Component( actualProps );
                if e.Add_Component( cInst ) then
                    e.Get_Attributes.Set_Defaults( c.def.Get_Default_Attributes );
                else
                    Delete( cInst );
                    Dbg( "Failed to create entity from template '" & To_String( this.name ) &
                         "': Duplicate component '" & c.def.Get_Name & "'",
                         D_ENTITY, Error );
                    Delete( e );
                    return null;
                end if;
            end;
        end loop;

        -- set entity attributes
        declare
            procedure Examine( key : String; val : Value ) is
            begin
                e.Get_Attributes.Set_Value( key, val );
            end Examine;
        begin
            this.attributes.Iterate( Examine'Access );
        end;

        return e;
    end Create_Entity;

    ----------------------------------------------------------------------------

    function Get_Attributes( this : not null access Template'Class ) return Map_Value is (this.attributes);

    ----------------------------------------------------------------------------

    function Get_Attribute_Definitions( this : not null access Template'Class ) return Map_Value is (this.attrDefs);

    ----------------------------------------------------------------------------

    function Get_Name( this : not null access Template'Class ) return String is (To_String( this.name ));

    ----------------------------------------------------------------------------

    function Get_Tool_Parameters( this : not null access Template'Class ) return Map_Value is (this.toolParams);

    ----------------------------------------------------------------------------

    procedure Include_Component( this       : not null access Template'Class;
                                 def        : A_Definition;
                                 properties : Map_Value ) is
    begin
        if this.components.Length = 0 or else
           def.Get_Dependency_Order > this.components.Last_Element.def.Get_Dependency_Order
        then
            -- special case: add the component last
            this.components.Append( Component_Inclusion'(def, properties) );
        else
            -- insert the component before others with the same or greater dependency order
            for i in 1..Integer(this.components.Length) loop
                if def.Get_Dependency_Order <= this.components.Element( i ).def.Get_Dependency_Order then
                    this.components.Insert( i, Component_Inclusion'(def, properties) );
                    exit;
                end if;
            end loop;
        end if;
    end Include_Component;

    ----------------------------------------------------------------------------

    function Includes_Component( this    : not null access Template'Class;
                                 defName : String ) return Boolean is
    begin
        for c of this.components loop
            if c.def.Extends( defName ) then
                return True;
            end if;
        end loop;
        return False;
    end Includes_Component;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Template ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

end Entities.Templates;
