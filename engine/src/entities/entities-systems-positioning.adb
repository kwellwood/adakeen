--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Entities.Locations;                use Entities.Locations;
with Entities.Movements;                use Entities.Movements;
with Events.Entities;                   use Events.Entities;
with Events.Entities.Locations;         use Events.Entities.Locations;
with Events.World;                      use Events.World;
with Vector_Math;                       use Vector_Math;

package body Entities.Systems.Positioning is

    function Create_Positioning_System return A_Component_System is
        this : constant A_Component_System := new Positioning_System;
    begin
        Positioning_System(this.all).Construct;
        return this;
    end Create_Positioning_System;

    ----------------------------------------------------------------------------

    overriding
    procedure Tick( this : access Positioning_System; time : Tick_Time ) is
        pragma Unreferenced( time );

        loc      : A_Location;
        mov      : A_Movement;
        velocity : Vec2;
    begin
        for c of this.components loop
            loc := A_Location(c);

            if loc.Is_Moved or else loc.Moved_Last_Frame then
                mov := A_Movement(loc.Get_Owner.Get_Component( MOVEMENT_ID ));
                velocity := (if mov /= null then mov.Get_V else (0.0, 0.0));
                Queue_Entity_Moved( loc.Get_Owner.Get_Id,
                                    loc.Get_X, loc.Get_Y,
                                    loc.Get_Prev_X, loc.Get_Prev_Y,
                                    velocity.x, velocity.y );
                if loc.Is_Moved then
                    Queue_World_Modified;
                end if;
            end if;
            if loc.Is_Resized then
                Queue_Entity_Resized( loc.Get_Owner.Get_Id,
                                      loc.Get_Width, loc.Get_Height,
                                      loc.Get_Prev_Width, loc.Get_Prev_Height );
                Queue_World_Modified;
            end if;

            loc.Update_Previous_State;
        end loop;
    end Tick;

end Entities.Systems.Positioning;
