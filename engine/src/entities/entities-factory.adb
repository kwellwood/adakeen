--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Assets.Scribble;                   use Assets.Scribble;
with Debugging;                         use Debugging;
with Entities.Entity_Attributes;        use Entities.Entity_Attributes;
with Entities.Locations;                use Entities.Locations;
with Resources;                         use Resources;
with Support;                           use Support;
with Support.Paths;                     use Support.Paths;
with Values.Casting;                    use Values.Casting;
with Values.Construction;               use Values.Construction;
with Values.Lists;                      use Values.Lists;
with Values.Strings;                    use Values.Strings;

package body Entities.Factory is

    function Load_Component_Definitions( this     : not null access Entity_Factory'Class;
                                         filename : String ) return Boolean;

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Entity_Factory return A_Entity_Factory is
        this : constant A_Entity_Factory := new Entity_Factory;
    begin
        this.Construct;
        return this;
    end Create_Entity_Factory;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Entity_Factory ) is
    begin
        for d of this.definitions loop
            Delete( d );
        end loop;
        this.definitions.Clear;

        for t of this.templates loop
            Delete( t );
        end loop;
        this.templates.Clear;
        this.templatesLoadOrder.Clear;

        Limited_Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Create_Entity( this       : not null access Entity_Factory'Class;
                            template   : String;
                            properties : Map_Value ) return A_Entity is
        t : A_Template;
    begin
        -- find the entity template
        t := this.Get_Template( template );
        if t = null then
            Dbg( "Cannot create entity from unknown template '" & template & "'",
                 D_ENTITY, Error );
            return null;
        end if;
        return t.Create_Entity( properties );
    end Create_Entity;

    ----------------------------------------------------------------------------

    function Copy_Entity( this : not null access Entity_Factory'Class;
                          e    : not null A_Entity ) return A_Entity is
        clone : A_Entity;
        attrs      : Map_Value;
        keys       : List_Value;
    begin
        clone := this.Create_Entity( e.Get_Template,
                                     Create(
                                         (Pair( "id", To_Id_Value( e.Get_Id ) ),
                                          Pair( "Location", Create(
                                                (Pair( "x", Create( A_Location(e.Get_Component( LOCATION_ID )).Get_X ) ),
                                                 Pair( "y", Create( A_Location(e.Get_Component( LOCATION_ID )).Get_Y ) ),
                                                 Pair( "width", Create( A_Location(e.Get_Component( LOCATION_ID )).Get_Width) ),
                                                 Pair( "height", Create( A_Location(e.Get_Component( LOCATION_ID )).Get_Height) )),
                                                 consume => True ) )
                                         ),
                                         consume => True
                                     ).Map
                                   );
        if clone = null then
            -- Create_Entity() already logged the error
            return null;
        end if;

        attrs := e.Get_Attributes.Get_Local_Values;
        keys := attrs.Get_Keys.Lst;
        for i in 1..keys.Length loop
            clone.Get_Attributes.Set_Value( keys.Get_String( i ),
                                            attrs.Get( keys.Get_String( i ) ) );
        end loop;
        return clone;
    end Copy_Entity;

    ----------------------------------------------------------------------------

    function Get_Template( this : not null access Entity_Factory'Class; name : String ) return A_Template is
        crsr : Template_Registry.Cursor;
    begin
        crsr := this.templates.Find( name );
        if Template_Registry.Has_Element( crsr ) then
            return Template_Registry.Element( crsr );
        end if;
        return null;
    end Get_Template;

    ----------------------------------------------------------------------------

    procedure Iterate_Templates( this    : not null access Entity_Factory'Class;
                                 examine : not null access procedure( template : A_Template ) ) is
    begin
        for t of this.templatesLoadOrder loop
            examine.all( t );
        end loop;
    end Iterate_Templates;

    ----------------------------------------------------------------------------

    function Load_Entity_Templates( this     : not null access Entity_Factory'Class;
                                    filename : String ) return Boolean is

        ------------------------------------------------------------------------

        function Create_Template( name       : String;
                                  attributes : Map_Value;
                                  toolParams : Map_Value ) return A_Template is
            attrDefs   : constant Map_Value:= Create_Map.Map;
            attrValues : constant Map_Value := Create_Map.Map;

            --------------------------------------------------------------------

            procedure Examine_Attribute( name : String; val : Value ) is
            begin
                if val.Is_Map then
                    -- this is an entity attribute definition
                    -- extract the value and store it separately
                    attrValues.Set( name, val.Map.Get( "value" ) );   -- may be Null
                    val.Map.Remove( "value" );
                    attrDefs.Set( name, val );
                else
                    -- this is a naked entity attribute value
                    attrValues.Set( name, val );
                end if;
            end Examine_Attribute;

            --------------------------------------------------------------------

        begin
            -- separate attribute values from attribute definitions
            if attributes.Valid then
                attributes.Iterate( Examine_Attribute'Access );
            end if;

            return Create_Template( name, attrDefs, attrValues, toolParams );
        end Create_Template;

        ------------------------------------------------------------------------

        -- Parses and creates a new entity template named 'Entity' with entity
        -- attributes 'Attributes' and components 'Components'. A template with
        -- the same name must not already exist. 'Tool', if defined, contains
        -- properties describing how the level editor should treat the entity.
        --
        -- [
        --     {Entity: "<new-template-name>"
        --         Components: [
        --             {<component-definition-name>: {<property-name>: <property-value>, ...}}
        --             ...
        --         ]
        --         Attributes: {<attribute-name>: <ATTRIBUTE-DEFINITION>, ...}
        --         Tool: {<tool-parameter-name>: <tool-parameter-value>, ...}
        --     }
        --     ...
        -- ]
        --
        -- <ATTRIBUTE-DEFINITION> is:
        --     <attribute-value>
        --   OR
        --     {"value": <attribute-value>, <constraint-name>: <constraint-value>, ...}
        --
        function Parse_Template( filename : String; templateMap : Map_Value ) return Boolean is
            entityName : constant String := templateMap.Get_String( "Entity" );
            success    : Boolean := True;
            components : List_Value;
        begin
            if entityName'Length = 0 then
                Dbg( "Entity template name missing in " & filename, D_ENTITY, Error );
                return False;
            elsif this.Get_Template( entityName ) /= null then
                Dbg( "Duplicate entity template '" & entityName & "' in " & filename,
                     D_ENTITY, Error );
                return False;
            end if;

            -- read list of components and their properties
            components := templateMap.Get( "Components" ).Lst;
            if not components.Valid then
                Dbg( "Component list missing from entity template '" & entityName &
                     "' in " & filename,
                     D_ENTITY, Error );
                return False;
            end if;

            -- check to see if any unrecognized fields are defined in the template.
            -- might be junk, might be a misspelling.
            declare
                procedure Validate_Field( field : String; val : Value ) is
                    pragma Unreferenced( val );
                begin
                    if field /= "Entity"     and
                       field /= "Components" and
                       field /= "Attributes" and
                       field /= "Tool"
                    then
                        Dbg( "Unrecognized field '" & field &
                             "' in entity template '" & entityName &
                             "' in " & filename,
                             D_ENTITY, Warning );
                    end if;
                end Validate_Field;
            begin
                -- only do this in debug builds
                if DO_VALIDATION_CHECKS then
                    templateMap.Iterate( Validate_Field'Access );
                end if;
            end;

            declare
                template : A_Template;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

                procedure Include_Component( defName : String; props : Value ) is
                    def        : A_Definition;
                    properties : Map_Value;
                begin
                    def := this.Get_Definition( defName );
                    if def = null then
                        Dbg( "Unknown component '" & defName &
                             "' of entity template '" & entityName &
                             "' in " & filename,
                             D_ENTITY, Error );
                        success := False;
                        return;
                    end if;

                    properties := props.Map;
                    if not properties.Valid then
                        Dbg( "Invalid properties for component '" & defName &
                             "' of entity template '" & entityName &
                             "' in " & filename,
                             D_ENTITY, Error );
                        success := False;
                        return;
                    end if;

                    if DO_VALIDATION_CHECKS then
                        if not def.Validate_Properties( properties ) then
                            Dbg( "Unknown property in component '" & defName &
                                 "' of entity template '" & entityName &
                                 "' in " & filename,
                                 D_ENTITY, Error );
                            success := False;
                            return;
                        end if;
                    end if;

                    template.Include_Component( def, properties );
                end Include_Component;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

                procedure Verify_Requirements( defName : String; props : Value ) is
                    def      : constant A_Definition := this.Get_Definition( defName );
                    required : constant List_Value := def.Get_Required;
                    pragma Unreferenced( props );
                begin
                    for i in 1..required.Length loop
                        if not template.Includes_Component( required.Get_String( i ) ) then
                            Dbg( "Entity template '" & entityName &
                                 "' is missing component '" & required.Get_String( i ) &
                                 "', required by component '" & defName & "'",
                                 D_ENTITY, Warning );
                        end if;
                    end loop;
                end Verify_Requirements;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

            begin
                template := Create_Template( entityName,
                                             templateMap.Get( "Attributes" ).Map,
                                             templateMap.Get( "Tool" ).Map );

                -- add all components to the template
                for i in 1..components.Length loop
                    if components.Get( i ).Is_Map and then components.Get( i ).Map.Size = 1 then
                        components.Get( i ).Map.Iterate( Include_Component'Access );
                    else
                        Dbg( "Invalid component at index " & Image( i ) & " in entity template '" &
                             entityName & "' in " & filename,
                             D_ENTITY, Error );
                        success := False;
                        exit;
                    end if;
                end loop;
                if not success then
                    Delete( template );
                    return False;
                end if;

                -- automatically define the mandatory Attributes component if it
                -- wasn't included in the entity template.
                if not template.Includes_Component( "Attributes" ) then
                    pragma Assert( this.Get_Definition( "Attributes" ) /= null );
                    template.Include_Component( this.Get_Definition( "Attributes" ), Create_Map.Map );
                end if;

                -- verify that no required components are missing
                if DO_VALIDATION_CHECKS then
                    for i in 1..components.Length loop
                        components.Get( i ).Map.Iterate( Verify_Requirements'Access );
                    end loop;
                end if;

                pragma Debug( Dbg( "Loaded entity template '" & entityName &
                                   "' from " & filename,
                                   D_ENTITY, Info ) );

                pragma Debug( Dbg( "Defining entity template '" & entityName & "'", D_ENTITY, Info ) );
                if this.templates.Contains( entityName ) then
                    Dbg( "Entity template '" & entityName & "' defined twice", D_ENTITY, Error );
                end if;

                this.templates.Include( entityName, template );
                this.templatesLoadOrder.Append( template );
            end;

            return True;
        end Parse_Template;

        ------------------------------------------------------------------------

        file    : A_Scribble_Asset;
        loaded  : Boolean := False;
        list    : List_Value;
        element : Map_Value;
    begin
        file := Load_Value( filename, "definitions" );

        if not file.Is_Loaded then
            null;
        elsif file.Get_Value.Is_List then
            list := file.Get_Value.Lst;
            loaded := True;
        elsif file.Get_Value.Is_Map and then file.Get_Value.Map.Get( "Entity" ).Is_String then
            list := Create_List.Lst;
            list.Append( file.Get_Value );
            loaded := True;
        else
            Dbg( "Failed to load " & filename & ": Contents unrecognized", D_ENTITY, Error );
        end if;

        if loaded then
            for i in 1..list.Length loop
                element := list.Get( i ).Map;
                if element.Valid then
                    -- {Entity: "template", ...}
                    if not Parse_Template( filename, element ) then
                        loaded := False;
                        exit;
                    end if;
                else
                    Dbg( "Failed to load " & filename &
                         ": Expected Include or Entity template",
                         D_ENTITY, Error );
                    loaded := False;
                    exit;
                end if;
            end loop;
        end if;

        Delete( file );
        return loaded;
    end Load_Entity_Templates;

    ----------------------------------------------------------------------------

    procedure Load_Entity_Templates( this : not null access Entity_Factory'Class ) is
        files    : List_Value;
        compiled : List_Value;
        filename : Value;
    begin
        -- find all entity templates in source format
        files := Search_Resources( "*.entity", "definitions" );

        -- find entity templates that have been compiled
        -- add them to the list of files to load, without the ".sco" extension
        compiled := Search_Resources( "*.entity.sco", "definitions" );
        for i in 1..compiled.Length loop
            filename := Create( Remove_Extension( Cast_String( compiled.Get( i ) ) ) );
            if not files.Contains( filename ) then
                files.Append( filename );
            end if;
        end loop;

        -- load each file found, using only the ".entity" filename, whether or
        -- not it's actually compiled
        for i in 1..files.Length loop
            if not this.Load_Entity_Templates( Cast_String( files.Get( i ) ) ) then
                Dbg( "Failed to load entities from '" & Cast_String( files.Get( i ) ) & "'",
                     D_ENTITY, Error );
            end if;
        end loop;
    end Load_Entity_Templates;

    ----------------------------------------------------------------------------

    function Create_Component( this       : not null access Entity_Factory'Class;
                               name       : String;
                               properties : Map_Value ) return A_Component is
        def : constant A_Definition := this.Get_Definition( name );
    begin
        if def = null then
            -- unknown component definition
            return null;
        end if;

        -- a failed validation only produces a warning message
        if DO_VALIDATION_CHECKS and then not def.Validate_Properties( properties ) then
            Dbg( "Unknown property given for component '" & name & "'", D_ENTITY, Warning );
        end if;

        return def.Create_Component( properties );
    end Create_Component;

    ----------------------------------------------------------------------------

    procedure Define_Component( this      : not null access Entity_Factory'Class;
                                name      : String;
                                allocator : not null A_Component_Allocator;
                                propNames : Value_Array := Empty_Array;
                                required  : Value_Array := Empty_Array ) is
        propSet   : String_Sets.Set;
        reqSet    : String_Sets.Set;
        depsOrder : Integer;
    begin
        if this.Get_Definition( name ) /= null then
            raise Constraint_Error with "Duplicate component definition '" & name & "'";
        end if;

        pragma Debug( Dbg( "Defining root component '" & name & "'", D_ENTITY, Info ) );

        for i in propNames'Range loop
            propSet.Include( Cast_String( propNames(i) ) );
        end loop;

        for i in required'Range loop
            reqSet.Include( Cast_String( required(i) ) );
        end loop;

        declare
            c : A_Component;
        begin
            -- the allocator might raise an exception (even though it shouldn't)
            -- be sure to catch it and fail the component definition.
            c := allocator.all( Create_Map.Map );
            if c = null then
                raise Constraint_Error;
            end if;
            depsOrder := c.Get_Dependency_Order;
            Delete( c );
        exception
            when e : others =>
                Dbg( "Exception in allocator of component definition '" & name & "'", D_ENTITY, Error );
                Dbg( e, D_ENTITY, Error );
                raise Constraint_Error with "Exception in allocator for component definition '" & name & "'";
        end;

        this.definitions.Insert( name, Create_Root( name,
                                                    allocator,
                                                    depsOrder,
                                                    propSet,
                                                    reqSet ) );
    end Define_Component;

    ----------------------------------------------------------------------------

    function Get_Definition( this : not null access Entity_Factory'Class;
                             name : String ) return A_Definition is
        crsr : Definition_Registry.Cursor;
    begin
        crsr := this.definitions.Find( name );
        if Definition_Registry.Has_Element( crsr ) then
            return Definition_Registry.Element( crsr );
        end if;
        return null;
    end Get_Definition;

    ----------------------------------------------------------------------------

    function Load_Component_Definitions( this     : not null access Entity_Factory'Class;
                                         filename : String ) return Boolean is

        -- Parses a component definition 'Component' that extends a parent
        -- definition 'Extends'. 'Properties' overrides any of the parent
        -- definition's properties. 'Attributes' will fill in default values for
        -- any null entity attributes at the time when the component is added to
        -- an entity. They will override a parent definition's attribute defaults.
        -- 'Requires' contains names of required components that are in addition
        -- to the parent definition's requirements.
        --
        -- A definition with the same name must not already exist. Components
        -- may only be extended MAX_COMPONENT_DEFS_DEPTH levels deep.
        --
        -- [
        --     {Component: "<definition-name>"
        --         Extends: "<parent-definition-name>"
        --         Properties: {<property-name>: <property-value>, ...}
        --         Attributes: {<attribute-name>: <default-value>, ...}
        --     }
        --     ...
        -- ]
        --
        function Parse_Component( filename : String; definition : Map_Value ) return Boolean is
            name       : Unbounded_String;
            parent     : Unbounded_String;
            properties : Map_Value;
            attributes : Map_Value;
            required   : List_Value;
            reqSet     : String_Sets.Set;
        begin
            name := definition.Get_Unbounded_String( "Component" );
            if Length( name ) = 0 then
                Dbg( "Component definition name missing in " & filename, D_ENTITY, Error );
                return False;
            elsif this.Get_Definition( To_String( name ) ) /= null then
                Dbg( "Duplicate component definition '" & To_String( name ) &
                     "' in " & filename,
                     D_ENTITY, Error );
                return False;
            end if;

            parent := definition.Get_Unbounded_String( "Extends" );
            if Length( parent ) = 0 then
                Dbg( "Field 'Extends' missing from component definition '" &
                     To_String( name ) & "' in " & filename,
                     D_ENTITY, Error );
                return False;
            end if;

            properties := definition.Get( "Properties" ).Map;
            if not properties.Valid then
                Dbg( "Field 'Properties' missing from component definition '" &
                     To_String( name ) & "' in " & filename,
                     D_ENTITY, Error );
                return False;
            end if;

            attributes := definition.Get( "Attributes" ).Map;

            required := definition.Get( "Requires" ).Lst;
            if required.Valid then
                for i in 1..required.Length loop
                    if not required.Get( i ).Is_String then
                        Dbg( "Field 'Requires' contains bad value in component '" &
                             To_String( name ) & "' in " & filename,
                             D_ENTITY, Error );
                        return False;
                    end if;
                    reqSet.Include( required.Get_String( i ) );
                end loop;
            elsif not required.Is_Null then
                Dbg( "Field 'Requires' is malformed in component '" &
                     To_String( name ) & "' in " & filename,
                     D_ENTITY, Error );
                return False;
            end if;

            -- check to see if any unrecognized fields are defined in the
            -- component. might be junk, might be a misspelling.
            declare
                procedure Validate_Field( field : String; val : Value ) is
                    pragma Unreferenced( val );
                begin
                    if field /= "Component"  and then
                       field /= "Extends"    and then
                       field /= "Properties" and then
                       field /= "Attributes" and then
                       field /= "Requires"
                    then
                        Dbg( "Unrecognized field '" & field &
                             "' in component definition '" & To_String( name ) &
                             "' in " & filename,
                             D_ENTITY, Warning );
                    end if;
                end Validate_Field;
            begin
                if DO_VALIDATION_CHECKS then
                    definition.Iterate( Validate_Field'Access );
                end if;
            end;

            pragma Debug( Dbg( "Loaded component definition '" & To_String( name ) &
                               "' (extends '" & To_String( parent ) &
                               "') from " & filename,
                               D_ENTITY, Info ) );

            if this.definitions.Contains( To_String( name ) ) then
                Dbg( "Duplicate definition for component '" & To_String( name ) & "'", D_ENTITY, Error );
            end if;

            this.definitions.Include( To_String( name ),
                                      Create_Extension( To_String( name ),
                                                        To_String( parent ),
                                                        properties,
                                                        attributes,
                                                        reqSet ) );

            return True;
        end Parse_Component;

        ------------------------------------------------------------------------

        file    : A_Scribble_Asset;
        loaded  : Boolean := False;
        list    : List_Value;
        element : Map_Value;
    begin
        file := Load_Value( filename, "definitions" );

        if not file.Is_Loaded then
            null;
        elsif file.Get_Value.Is_List then
            list := file.Get_Value.Lst;
            loaded := True;
        elsif file.Get_Value.Is_Map and then file.Get_Value.Map.Get( "Component" ).Is_String then
            list := Create_List.Lst;
            list.Append( file.Get_Value );
            loaded := True;
        else
            Dbg( "Failed to load " & filename & ": Contents unrecognized", D_ENTITY, Error );
        end if;

        if loaded then
            for i in 1..list.Length loop
                element := list.Get( i ).Map;
                if element.Valid then
                    -- {Component: "definition", ...}
                    if not Parse_Component( filename, element ) then
                        loaded := False;
                        exit;
                    end if;
                else
                    Dbg( "Failed to load '" & filename &
                         "': Expected Include or Component definition",
                         D_ENTITY, Error );
                    loaded := False;
                    exit;
                end if;
            end loop;
        end if;

        Delete( file );
        return loaded;
    end Load_Component_Definitions;

    ----------------------------------------------------------------------------

    procedure Load_Component_Definitions( this : not null access Entity_Factory'Class ) is
        files    : List_Value;
        compiled : List_Value;
        filename : Value;
        success  : Boolean := True;
    begin
        -- find all component definitions in source format
        files := Search_Resources( "*.component", "definitions" );

        -- find component definitions that have been compiled
        -- add them to the list of files to load, without the ".sco" extension
        compiled := Search_Resources( "*.component.sco", "definitions" );
        for i in 1..compiled.Length loop
            filename := Create( Remove_Extension( Cast_String( compiled.Get( i ) ) ) );
            if not files.Contains( filename ) then
                files.Append( filename );
            end if;
        end loop;

        -- load each file found, using only the ".component" filename, whether or
        -- not it's actually compiled
        for i in 1..files.Length loop
            if not this.Load_Component_Definitions( Cast_String( files.Get( i ) ) ) then
                Dbg( "Failed to load components from '" & Cast_String( files.Get( i ) ) & "'",
                     D_ENTITY, Error );
                success := False;
            end if;
        end loop;

        if success then
            if not this.Validate_Definitions then
                Dbg( "Failed to load component definitions", D_ENTITY, Error );
            end if;
        end if;
    end Load_Component_Definitions;

    ----------------------------------------------------------------------------

    function Validate_Definitions( this : not null access Entity_Factory'Class ) return Boolean is
        success : Boolean := True;

        ------------------------------------------------------------------------

        function Validate_Parents( def : A_Definition ) return Boolean is
            def2   : A_Definition;
            parent : A_Definition;
            depth  : Integer := 1;
        begin
            def2 := def;
            loop
                exit when def2.Is_Root;              -- this is a root definition
                exit when def2.Get_Parent /= null;   -- already found the parent

                -- loop through ancestors toward root
                parent := this.Get_Definition( def2.Get_Parent_Name );
                if parent = null then
                    Dbg( "Component definition '" & def2.Get_Name &
                         "' extends unknown component definition '" &
                         def2.Get_Parent_Name & "'",
                         D_ENTITY, Error );
                    return False;
                elsif parent = def2 then
                    Dbg( "Component definition '" & def2.Get_Name &
                         "' extends self",
                         D_ENTITY, Error );
                    return False;
                elsif depth >= MAX_COMPONENT_DEFS_DEPTH then
                    Dbg( "Component definition '" & def2.Get_Name &
                         "' is involved in a cycle",
                         D_ENTITY, Error );
                    return False;
                else
                    -- record the parent reference
                    def2.Set_Parent( parent );
                end if;
                def2 := parent;
                depth := depth + 1;
            end loop;
            return True;
        end Validate_Parents;

        ------------------------------------------------------------------------

    begin
        -- verify all component definitions extend from a root and then connect
        -- definitions to their parents.
        for d of this.definitions loop
            success := success or Validate_Parents( d );
        end loop;

        -- verify the properties in the component definitions are valid
        if DO_VALIDATION_CHECKS and then success then
            for def of this.definitions loop
                if not def.Is_Root then
                    success := def.Validate_Properties( def.Get_Own_Properties );
                    exit when not success;
                end if;
            end loop;
        end if;

        return success;
    end Validate_Definitions;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Entity_Factory ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

end Entities.Factory;
