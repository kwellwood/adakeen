--
-- Copyright (c) 2012-2013 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Entities.Systems.Collision is

    function Create_Collision_System return A_Component_System;

private

    type Collision_System is new Component_System with null record;

    overriding
    procedure Tick( this : access Collision_System; time : Tick_Time );

end Entities.Systems.Collision;
