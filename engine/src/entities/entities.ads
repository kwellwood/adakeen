--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers;                    use Ada.Containers;
with Ada.Containers.Doubly_Linked_Lists;
with Ada.Containers.Indefinite_Ordered_Maps;
with Ada.Containers.Ordered_Maps;
with Ada.Real_Time;                     use Ada.Real_Time;
with Ada.Streams;                       use Ada.Streams;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Ada.Unchecked_Deallocation;
with Component_Families;                use Component_Families;
with Directives;                        use Directives;
with Events;                            use Events;
with Hashed_Strings;                    use Hashed_Strings;
with Object_Ids;                        use Object_Ids;
with Objects;                           use Objects;
with Processes;                         use Processes;
with Values;                            use Values;
with Values.Maps;                       use Values.Maps;
with Values.Tagged_Ids;                 use Values.Tagged_Ids;

limited with Entities.Entity_Attributes;
limited with Entities.Systems;
limited with Worlds;

package Entities is

    -- Sent to an entity when its deletion is pending, to notify it to perform
    -- any last actions before destructing.
    --
    -- Destruct {}
    MSG_Destruct : constant Hashed_String := To_Hashed_String( "Destruct" );

    -- Sent to an entity when it is spawned into the world. This occurs only
    -- once during the entity's lifespan.
    --
    -- Spawned {}
    MSG_Spawned : constant Hashed_String := To_Hashed_String( "Spawned" );

    ----------------------------------------------------------------------------

    -- An Entity represents any distinct object within a World. It is composed
    -- of a collection of Components that provide its state and behavior.
    --
    -- Each Entity in the game world is updated on a Tick() for each logic frame
    -- during game play. It receives messages from other entities and dispatches
    -- them to its registered Components. Each entity is also affected by a set
    -- of Directives given to it, which may last for one or more ticks.
    type Entity is new Limited_Object with private;
    type A_Entity is access all Entity'Class;

    -- A Component is an abstract object that contributes to an individual
    -- entity's state and behavior within the game's Component Entity Model.
    --
    -- Every entity has one or more Components that are updated on a Tick() for
    -- each logic frame. Components may send messages to entities and receive
    -- messages from other entities.
    --
    -- Each component within an entity is (generally) of a different component
    -- family, each adding a unique behavior to the entity. Components are also
    -- generally indepedent from each other within an entity, to limit coupling,
    -- although some dependencies are necessary.
    type Component is abstract new Object with private;
    type A_Component is access all Component'Class;

    ----------------------------------------------------------------------------
    -- Entity Class --

    -- - - - - - - - - - - - - - - - - -
    -- Accessors / Modifiers

    -- Returns the age of the entity in time spent in game play.
    function Get_Age( this : not null access Entity'Class ) return Time_Span;

    -- Returns an access to the entity's Attributes component. This component is
    -- always available to other components, for registering attributes and
    -- getting/setting attribute values.
    function Get_Attributes( this : not null access Entity'Class ) return access Entity_Attributes.Attributes'Class;

    -- Returns the entity's unique id.
    function Get_Id( this : not null access Entity'Class ) return Entity_Id;

    -- Returns the entity's unique name, or an empty string if undefined.
    function Get_Name( this : not null access Entity'Class ) return String;

    -- Returns the entity's controlling entity system, if it currently has one.
    function Get_System( this : not null access Entity'Class ) return access Systems.Entity_System'Class;

    -- Returns the name of the entity template that created the entity.
    function Get_Template( this : not null access Entity'Class ) return String;

    -- Returns the World containing the entity.
    function Get_World( this : not null access Entity'Class ) return access Worlds.World_Object'Class;

    -- Sets the entity's name. It is not checked for uniqueness; this should
    -- only be called from the World, which does the check.
    procedure Set_Name( this : not null access Entity'Class;
                        name : String );

    -- Sets the entity's pointer to its controlling entity system.
    procedure Set_System( this   : not null access Entity'Class;
                          system : access Systems.Entity_System'Class );

    -- - - - - - - - - - - - - - - - - -
    -- Components

    -- Adds a component to the entity and sets the owner of 'component',
    -- consuming 'component' and returning True on success. False will be
    -- returned (and 'component' will not be consumed) if the component could
    -- not be added because the entity already has an entity of the same family.
    -- An entity may only have one component of each family unless the
    -- component's family specifically allows multiple.
    function Add_Component( this      : not null access Entity'Class;
                            component : not null A_Component ) return Boolean;
    pragma Precondition( component.Get_Owner = null );

    -- Removes and deletes the entity's component of type 'family'. If the
    -- entity does not have a component of 'family', or if the entity has more
    -- than one of 'family', nothing will happen.
    procedure Delete_Component( this   : not null access Entity'Class;
                                family : Family_Id );

    -- Removes and deletes the entity's component 'component'. If 'component' is
    -- null, nothing will happen. if 'component' does not belong to this entity
    -- then a COMPONENT_ERROR exception will be raised.
    procedure Delete_Component( this      : not null access Entity'Class;
                                component : in out A_Component );
    pragma Postcondition( component = null );

    -- Returns a pointer to the entity's component of family 'family', if it
    -- has one. Otherwise, null will be returned. If the entity has multiple
    -- components of 'family', null will be returned; they cannot be queried.
    -- The Entity maintains ownership of the Component.
    function Get_Component( this   : not null access Entity'Class;
                            family : Family_Id ) return A_Component;

    -- - - - - - - - - - - - - - - - - -
    -- Directives

    -- Cancels all directives currently active for the entity.
    procedure Cancel_Directives( this : not null access Entity'Class );

    -- Sets a directive (e.g.: from the player, via input) to control the
    -- entity's behavior. A Component can check the state of a directive during
    -- its Tick() and act on it appropriately. If 'mode' is Ongoing, directives
    -- 'directives' will be active until it is set as Inactive. If 'mode' is
    -- 'Once' then the directives will automatically become inactive after the
    -- next Tick().
    procedure Set_Directive( this       : not null access Entity'Class;
                             directives : Directive_Bits;
                             mode       : Directive_Mode );

    -- - - - - - - - - - - - - - - - - -
    -- Messaging

    -- Call this to dispatch a message 'name' with parameters 'params' to the
    -- entity's components that are listening for 'name' messages. 'params' will
    -- not be modified.
    procedure Dispatch_Message( this   : not null access Entity'Class;
                                name   : Hashed_String;
                                params : Map_Value );
    pragma Precondition( not params.Is_Null );

    -- Same as Dispatch_Message above, but parameters are not required.
    procedure Dispatch_Message( this : not null access Entity'Class;
                                name : Hashed_String );

    -- - - - - - - - - - - - - - - - - -
    -- Miscellaneous

    -- Creates and returns a new Entity_Spawned event.
    function Create_Spawned_Event( this : not null access Entity'Class ) return A_Event;

    -- Updates the entity and all its components on each logic frame of
    -- gameplay. 'time' contains the amount of game time elapsed since the
    -- previous tick.
    procedure Tick( this : not null access Entity'Class; time : Tick_Time );

    -- Returns a string representation of the Entity for debugging purposes.
    function To_String( this : access Entity ) return String;

    -- Deletes an entity.
    procedure Delete( this : in out A_Entity );
    pragma Postcondition( this = null );

    ----------------------------------------------------------------------------
    -- Component Class --

    -- This enum represents notification scopes of entity attributes.
    --   * 'Silent_Scope' suppresses value change notifications.
    --   * 'Entity_Scope' notifies the entity via AttributeChanged messages.
    --   * 'Global_Scope' additionally notifies the Game_View via Entity_Attribute_Changed events.
    type Notify_Scope is (Silent_Scope, Entity_Scope, Global_Scope);

    -- - - - - - - - - - - - - - - - - -
    -- Abstract Functions

    -- Returns True if multiple components of the same family are allowed in a
    -- single entity. For most component families, this is False.
    not overriding
    function Allow_Multiple( this : access Component ) return Boolean is (False);

    -- Notifies the component that it has gained an association with the entity
    -- system, either because it has just been created or because its world
    -- is becoming active.
    not overriding
    procedure On_Enter_System( this : access Component ) is null;

    -- Notifies the component that it is about to lose its association with the
    -- entity system, either because it is being removed from its entity, or its
    -- entity is being deleted, or its world is becoming inactive.
    not overriding
    procedure On_Exit_System( this : access Component ) is null;

    -- Returns the inter-component dependency order. Because certain component
    -- families depend on components of other families existing within the same
    -- entity (an inter-component dependency), the relative order of components
    -- is important during entity construction (via creation or streaming).
    --
    -- A component of order 'n' may depend on any component of a lesser order
    -- (0 through n - 1).
    function Get_Dependency_Order( this : access Component ) return Natural is abstract;

    -- Returns the component's family identifier.
    function Get_Family( this : access Component ) return Family_Id is abstract;

    -- Returns the Tick order of the component with respect to other components
    -- in the entity. The components are updated on each Tick from low order to
    -- high order. The relative Tick order of two components with the same order
    -- number is undefined.
    function Get_Tick_Order( this : access Component ) return Natural is abstract;

    -- Reads a Component object from a stream and returns it. All concrete
    -- Component subclasses must implement this function. All subclasses must
    -- declare "for MyComponent'Input use Object_Input;" on the definition.
    function Object_Input( stream : access Root_Stream_Type'Class ) return Component is abstract;

    -- - - - - - - - - - - - - - - - - -
    -- Accessors / Modifiers

    -- Returns the age of the component in time spent in game play.
    function Get_Age( this : not null access Component'Class ) return Time_Span with Inline;

    -- Returns the name of the component definition used to create this component.
    function Get_Definition( this : not null access Component'Class ) return String with Inline;

    -- Returns the component's unique id.
    function Get_Id( this : not null access Component'Class ) return Component_Id with Inline;

    -- Returns a pointer to the component's entity.
    function Get_Owner( this : not null access Component'Class ) return A_Entity with Inline;

    -- Creates and returns a name/value Map of the component's public state.
    -- This is the state that is shared with the Game_View when the entity is
    -- spawned. Individual items in the state may be kept synchronized by events.
    function Get_Public_Values( this : access Component ) return Map_Value is (Null_Value.Map);

    -- - - - - - - - - - - - - - - - - -
    -- Miscellaneous

    -- Override this to update the component on each logic frame of gameplay.
    -- 'time' contains the age of the component (.total) and the amount of game
    -- time elapsed since the previous tick (.elaped). An overriding
    -- implementation must NOT call this base implementation.
    procedure Tick( this : access Component; time : Tick_Time );

    -- Returns a string representation of the Component for debugging purposes.
    function To_String( this : access Component ) return String;

    function A_Component_Input( stream : access Root_Stream_Type'Class ) return A_Component;
    for A_Component'Input use A_Component_Input;

    procedure A_Component_Output( stream : access Root_Stream_Type'Class; obj : A_Component );
    for A_Component'Output use A_Component_Output;

    -- Deletes an component.
    procedure Delete( this : in out A_Component );
    pragma Postcondition( this = null );

    package Component_Lists is new Ada.Containers.Doubly_Linked_Lists( A_Component, "=" );
    type A_Component_List is access all Component_Lists.List;

    ----------------------------------------------------------------------------

    -- Raised on attempt to add two components of the same family to an entity,
    -- when the component family does not allow duplicates.
    DUPLICATE_COMPONENT : exception;

    -- Raised on attempt to remove a component from an entity to which it does
    -- not belong.
    COMPONENT_ERROR : exception;

private

    type Registered_Attribute is
        record
            component : A_Component := null;
            ref       : Value;
        end record;

    package Attribute_Maps is new Ada.Containers.Indefinite_Ordered_Maps( String, Registered_Attribute, "<", "=" );

    ----------------------------------------------------------------------------

    procedure Delete is new Ada.Unchecked_Deallocation( Component_Lists.List, A_Component_List );

    package Listener_Maps is new Ada.Containers.Ordered_Maps( Hashed_String, A_Component_List, "<", "=" );
    package Component_Maps is new Ada.Containers.Ordered_Maps( Family_Id, A_Component_List, "<", "=" );

    ----------------------------------------------------------------------------

    type Entity is new Limited_Object with
        record
            id         : Entity_Id := NULL_ID;          -- unique id
            name       : Unbounded_String;              -- unique entity name
            template   : Unbounded_String;              -- entity template name
            age        : Time_Span := Time_Span_Zero;   -- incremented each Tick
            attrs      : access Entity_Attributes.Attributes'Class;
            components : Component_Maps.Map;            -- its components, mapped by Family_Id

            -- these fields are not streamed --
            system         : access Systems.Entity_System'Class := null;  -- controlling system
            directives     : Directive_Modes := (others => NO_DIRECTIVES);
            listeners      : Listener_Maps.Map;         -- components registered as message listeners
            iteratingComponents : Boolean := False;     -- true while iterating in Tick()
            componentCount      : Natural := 0;         -- number of components
            api            : Map_Value;
        end record;
    for Entity'External_Tag use "Entity";

    -- Constructs a new Entity. A unique Entity_Id will be assigned to it.
    procedure Construct( this : access Entity );

    -- Deletes the Entity and all its Components.
    procedure Delete( this : in out Entity );

    -- - - - - - - - - - - - - - - - - -
    -- Components

    -- Removes 'comp' from the entity without deleting it, transferring
    -- ownership to the caller. If 'comp' is null, nothing will happen. If
    -- 'comp' does not belong to the entity, a COMPONENT_ERROR exception will be
    -- raised.
    procedure Remove_Component( this : not null access Entity'Class;
                                comp : access Component'Class );

    -- - - - - - - - - - - - - - - - - -
    -- Directives

    -- Returns True if all directives 'directives' are currently active.
    function All_Directives( this       : not null access Entity'Class;
                             directives : Directive_Bits ) return Boolean with Inline;

    -- Returns True if any directives 'directives' are currently active.
    function Any_Directive( this       : not null access Entity'Class;
                            directives : Directive_Bits ) return Boolean with Inline;

    -- Returns True if directive 'directive' is currently active.
    function Directive( this      : not null access Entity'Class;
                        directive : Directive_Bits ) return Boolean renames All_Directives;

    -- - - - - - - - - - - - - - - - - -
    -- Messaging

    -- Registers 'comp' as a listener for 'messageName' messages. If it has
    -- already been registered, it will be registered again. Components are
    -- automatically unregistered as message listeners when they are removed.
    procedure Register_Listener( this        : not null access Entity'Class;
                                 messageName : Hashed_String;
                                 comp        : not null access Component'Class );

    -- Unregisters 'component' as a listener for 'messageName' messages. If it
    -- has not been registered, nothing will happen. If has been registered more
    -- more than once, then just the most recent registration will be removed.
    -- Components are automatically unregistered as listeners when they are
    -- removed from the entity.
    procedure Unregister_Listener( this        : not null access Entity'Class;
                                   messageName : Hashed_String;
                                   comp        : not null access Component'Class );
    pragma Precondition( comp.Get_Owner = null or else comp.Get_Owner = this );

    -- Unregisters 'component' as a listener for any message that it has been
    -- registered for.
    procedure Unregister_Listener( this : not null access Entity'Class;
                                   comp : not null access Component'Class );
    pragma Precondition( comp.Get_Owner = null or else comp.Get_Owner = this );

    -- - - - - - - - - - - - - - - - - -
    -- Streaming

    -- Reads the contents of the Entity from a stream.
    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Entity );
    for Entity'Read use Object_Read;

    -- Writes the contents of the Entity to a stream.
    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Entity );
    for Entity'Write use Object_Write;

    function A_Entity_Input( stream : access Root_Stream_Type'Class ) return A_Entity;
    for A_Entity'Input use A_Entity_Input;

    procedure A_Entity_Output( stream : access Root_Stream_Type'Class; obj : A_Entity );
    for A_Entity'Output use A_Entity_Output;

    ----------------------------------------------------------------------------

    type Component is abstract new Object with
        record
            id         : Component_Id := NULL_ID;
            definition : Unbounded_String;
            age        : Time_Span := Time_Span_Zero;   -- incremented each Tick

            -- these fields are not streamed --
            owner      : A_Entity := null;
            tickable   : Boolean := True;
        end record;
    for Component'External_Tag use "Component";

    -- Constructs a new Component. A unique Component_Id will be assigned to it.
    procedure Construct( this : access Component );

    -- Raises COPY_NOT_ALLOWED.
    procedure Adjust( this : access Component );

    -- - - - - - - - - - - - - - - - - -
    -- Entity Service Helpers

    -- These directive functions are for components that want to check the status
    -- of a directive given to the entity. They are just a pass-through to the
    -- component's entity.
    function All_Directives( this       : not null access Component'Class;
                             directives : Directive_Bits ) return Boolean
    is (this.owner.All_Directives( directives ))
    with Inline_Always;

    function Any_Directive( this       : not null access Component'Class;
                            directives : Directive_Bits ) return Boolean
    is (this.owner.Any_Directive( directives ))
    with Inline_Always;

    function Directive( this      : not null access Component'Class;
                        directive : Directive_Bits ) return Boolean renames All_Directives;

    -- This is a helper function for components that want to access other
    -- components within the same entity. It's just a pass-through to the
    -- component's entity.
    function Get_Component( this   : not null access Component'Class;
                            family : Family_Id ) return A_Component
    is (if this.owner /= null then this.owner.Get_Component( family ) else null)
    with Inline_Always;

    -- This is a helper function to search for another Entity in the world, by id.
    function Get_Entity( this : not null access Component'Class; id : Entity_Id ) return A_Entity;

    -- - - - - - - - - - - - - - - - - -
    -- Game State

    -- Returns the value of game session variable 'name'.
    function Get_Session_Var( this : not null access Component'Class; name : String ) return Value;

    -- Sets the value of game session variable 'name' to 'val'.
    procedure Set_Session_Var( this : not null access Component'Class; name : String; val : Value'Class );

    -- Returns a pointer to the component's entity's world. This helper function
    -- is just a pass-through to the component's entity.
    function Get_World( this : not null access Component'Class ) return access Worlds.World_Object'Class
    is (this.owner.Get_World)
    with Inline_Always;

    -- Returns True if the entity exists with a game Session that is acting as
    -- an editor. This flag may be used to suppress certain messages or behaviors
    -- that should not occur in a game editing environment.
    --
    -- This is equivalent to calling:
    -- Get_Game.Get_Session.Is_Editor
    --
    -- Null pointers are handled safely, allowing this to be called any time.
    -- Defaults to True if no Game or Session object exists.
    function In_Editor( this : not null access Component'Class ) return Boolean;

    -- - - - - - - - - - - - - - - - - -
    -- Entity Attributes

    -- Registers entity attribute 'name', provided by the component. This
    -- component is responsible for calling the Attributes component's
    -- Notify_Changed() procedure when the value of a registered attribute
    -- changes, as necessary.
    --
    -- This is equivalent to calling:
    -- this.Get_Owner.Get_Attributes.Register( name, this, reference, scope );
    --
    procedure Register_Entity_Attribute( this      : not null access Component'Class;
                                         name      : String;
                                         reference : Value'Class;
                                         scope     : Notify_Scope := Silent_Scope );
    pragma Precondition( this.owner /= null );
    pragma Precondition( reference.Is_Ref );

    -- Unregisters entity attribute 'name' with the component's entity.
    --
    -- This is equivalent to calling this.Get_Owner.Get_Attributes.Unregister( name, this ).
    --
    procedure Unregister_Entity_Attribute( this : not null access Component'Class;
                                           name : String );
    pragma Precondition( this.owner /= null );

    -- - - - - - - - - - - - - - - - - -
    -- Messaging

    -- This is called when the component receives a message of a type for which
    -- it registered. 'name' is the message name (or type) and 'params' is a
    -- map of named parameters included with the message.
    procedure Handle_Message( this   : access Component;
                              name   : Hashed_String;
                              params : Map_Value ) is null;

    -- Registers the component with its owning entity as a listener for messages
    -- named 'messageName'. The component must be owned. This is equivalent to
    -- calling this.Get_Owner.Register_Listener( this ).
    procedure Register_Listener( this        : not null access Component'Class;
                                 messageName : Hashed_String );
    pragma Precondition( this.owner /= null );

    -- Unregisters the component with its owning entity as a listener for
    -- messages named 'messageName'. The component must be owned. This is
    -- equivalent to calling this.Get_Owner.Unregister_Listener( this ).
    procedure Unregister_Listener( this        : not null access Component'Class;
                                   messageName : Hashed_String );
    pragma Precondition( this.owner /= null );

    -- - - - - - - - - - - - - - - - - -
    -- Owner Management

    -- Called just after the component is added to an entity. Override this to
    -- allow the component to register itself to receive messages, etc.
    procedure On_Added( this : access Component ) is null;

    -- Called just before the component is removed from an entity. Override this
    -- to allow the component to unregister itself as a message listener, etc.
    procedure On_Removed( this : access Component ) is null;

    -- Sets the entity that owns the component. This is called by the Entity
    -- when the component is added to it and removed from it.
    procedure Set_Owner( this  : not null access Component'Class;
                         owner : access Entity'Class );

    -- - - - - - - - - - - - - - - - - -
    -- Streaming

    -- Reads the contents of the Component from a stream. Overriding procedures
    -- must call this first, and the definition must declare
    -- "for MyComponent'Read use Object_Read;"
    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Component );
    for Component'Read use Object_Read;

    -- Writes the contents of the Component to a stream. Overriding procedures
    -- must call this first, and the definition must declare
    -- "for MyComponent'Write use Object_Write;"
    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Component );
    for Component'Write use Object_Write;

end Entities;

