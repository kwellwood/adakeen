--
-- Copyright (c) 2014-2015 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Entities.Systems.Positioning is

    function Create_Positioning_System return A_Component_System;

private

    type Positioning_System is new Component_System with null record;

    -- Queues Entity_Moved and Entity_Resized events if a component's state has
    -- changed since the previous Tick.
    overriding
    procedure Tick( this : access Positioning_System; time : Tick_Time );

end Entities.Systems.Positioning;
