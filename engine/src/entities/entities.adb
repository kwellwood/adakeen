--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Ada.Exceptions;                    use Ada.Exceptions;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Ada.Tags;                          use Ada.Tags;
with Debugging;                         use Debugging;
with Entities.Entity_Attributes;        use Entities.Entity_Attributes;
with Entities.Systems;                  use Entities.Systems;
with Events.Entities;                   use Events.Entities;
with Events.World;                      use Events.World;
with Games;                             use Games;
with GNAT.Heap_Sort_G;
with Sessions;                          use Sessions;
with Support;                           use Support;
with Values.Lists;

package body Entities is

    type Component_Array is array (Integer range <>) of A_Component;

    ----------------------------------------------------------------------------

    function Component_Count( cmap : Component_Maps.Map ) return Integer is
        count : Integer := 0;
    begin
        for list of cmap loop
            count := count + Integer(list.Length);
        end loop;
        return count;
    end Component_Count;

    ----------------------------------------------------------------------------

    procedure Sort_By_Dependency_Order( src : Component_Maps.Map; dest : in out Component_Array );
    pragma Precondition( Component_Count( src ) <= dest'Length );

    procedure Sort_By_Dependency_Order( src : Component_Maps.Map; dest : in out Component_Array ) is
        sorted : Component_Array(0..dest'Length);  -- (0 is needed by Heap_Sort)

        function Lt( l, r : Natural ) return Boolean is (sorted(l).Get_Dependency_Order < sorted(r).Get_Dependency_Order);
        pragma Inline( Lt );

        procedure Move( from, to : Natural ) is
        begin
            sorted(to) := sorted(from);
        end Move;
        pragma Inline( Move );

        package Heap_Sort is new GNAT.Heap_Sort_G(Move, Lt);
        i : Integer := 1;
    begin
        for list of src loop
            for c of list.all loop
                sorted(i) := c;
                i := i + 1;
            end loop;
        end loop;
        Heap_Sort.Sort( sorted'Last );

        dest := sorted(1..sorted'Last);
    end Sort_By_Dependency_Order;

    ----------------------------------------------------------------------------

    procedure Sort_By_Tick_Order( src : Component_Maps.Map; dest : in out Component_Array ) is
        sorted : Component_Array(0..dest'Length);  -- (0 is needed by Heap_Sort)

        function Lt( l, r : Natural ) return Boolean is (sorted(l).Get_Tick_Order < sorted(r).Get_Tick_Order);
        pragma Inline( Lt );

        procedure Move( from, to : Natural ) is
        begin
            sorted(to) := sorted(from);
        end Move;
        pragma Inline( Move );

        package Heap_Sort is new GNAT.Heap_Sort_G(Move, Lt);
        last  : Integer := 0;                      -- last tickable component
        first : Integer := sorted'Last + 1;        -- first untickable component
    begin
        for list of src loop
            for c of list.all loop
                if c.tickable then
                    last := last + 1;
                    sorted(last) := c;
                else
                    first := first - 1;
                    sorted(first) := c;
                end if;
            end loop;
        end loop;
        pragma Assert( last = first - 1, "Error in Sort_By_Tick_Order" );
        Heap_Sort.Sort( last );        -- only sort the tickable at the front

        dest := sorted(1..sorted'Last);   -- discard the first element
    end Sort_By_Tick_Order;

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    overriding
    procedure Construct( this : access Entity ) is
    begin
        Limited_Object(this.all).Construct;
        this.id := Unique_Tagged_Id( Entity_Tag );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Entity ) is
        components : A_Component_List;
        component  : A_Component;
        pos        : Component_Maps.Cursor;
    begin
        this.Cancel_Directives;
        for list of this.listeners loop
            Delete( list );
        end loop;
        this.listeners.Clear;

        -- the reference to the attributes component must be cleared here to
        -- avoid the need to delete components in dependency order, which would
        -- be slow.
        --
        -- Remove_Component() unregisters each component with this.attrs, but
        -- a) it doesn't need to be done in this case, and b) it will crash
        -- unless the Attributes component is deleted last.
        this.attrs := null;

        while not this.components.Is_Empty loop
            pos := this.components.First;
            components := Component_Maps.Element( pos );
            while not components.Is_Empty loop
                component := components.First_Element;
                this.Remove_Component( component );
                Delete( component );
            end loop;
            Delete( components );
            this.components.Delete( pos );
        end loop;

        Limited_Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Add_Component( this      : not null access Entity'Class;
                            component : not null A_Component ) return Boolean is
        components : A_Component_List;
        crsr       : Component_Maps.Cursor;
    begin
        crsr := this.components.Find( component.Get_Family );
        if not Component_Maps.Has_Element( crsr ) then
            -- insert a new Component_List
            components := new Component_Lists.List;
            components.Append( component );
            this.components.Insert( component.Get_Family, components );
            this.componentCount := this.componentCount + 1;
        else
            -- add the component if there aren't any of the same family already,
            -- or if the component's family allows multiples.
            components := Component_Maps.Element( crsr );
            if components.Is_Empty or else component.Allow_Multiple then
                components.Append( component );
                this.componentCount := this.componentCount + 1;
            else
                -- duplicate component
                return False;
            end if;
        end if;
        if component.Get_Family = ATTRIBUTES_ID then
            this.attrs := A_Attributes(component);
        end if;
        component.Set_Owner( this );
        if this.system /= null then
            -- register the component with all interested systems
            this.system.Associate( component );
            component.On_Enter_System;
            -- the component is being added during gameplay to an existing entity
            component.Handle_Message( MSG_Spawned, Create_Map.Map );
        end if;
        return True;
    end Add_Component;

    ----------------------------------------------------------------------------

    function All_Directives( this       : not null access Entity'Class;
                             directives : Directive_Bits ) return Boolean
    is (((this.directives(Once) or this.directives(Ongoing)) and directives) = directives);

    ----------------------------------------------------------------------------

    function Any_Directive( this       : not null access Entity'Class;
                            directives : Directive_Bits ) return Boolean
    is (((this.directives(Once) or this.directives(Ongoing)) and directives) /= NO_DIRECTIVES);

    ----------------------------------------------------------------------------

    procedure Cancel_Directives( this : not null access Entity'Class ) is
    begin
        this.directives := (others => NO_DIRECTIVES);
    end Cancel_Directives;

    ----------------------------------------------------------------------------

    function Create_Spawned_Event( this : not null access Entity'Class ) return A_Event is
        initialState : constant Map_Value := Create_Map.Map;
    begin
        for cList of this.components loop
            -- add the state for the first component instance of each family
            if cList.Length > 0 then
                initialState.Set( Family_Name( cList.First_Element.Get_Family ),
                                  cList.First_Element.Get_Public_Values,
                                  consume => True );
            end if;
        end loop;
        return Create_Entity_Spawned_Event( this.id,
                                            this.Get_Name,
                                            this.Get_Template,
                                            initialState );
    end Create_Spawned_Event;

    ----------------------------------------------------------------------------

    procedure Delete_Component( this   : not null access Entity'Class;
                                family : Family_Id ) is
        component : A_Component := this.Get_Component( family );
    begin
        -- Get_Component returns null if there are no components of 'family', or
        -- if there is more than one component of 'family'.
        if component /= null then
            if this.system /= null then
                -- the component is being removed during gameplay from an entity
                -- that is not being deleted.
                component.Handle_Message( MSG_Destruct, Create_Map.Map );
            end if;
            this.Remove_Component( component );

            -- if we're currently iterating through the components in Tick, the
            -- component can't be safely deleted here so we flag it for deletion
            -- later (by removing its owner) and leave it for Tick to clean up
            -- after iteration.
            if not this.iteratingComponents then
                Delete( component );
            end if;
        end if;
    end Delete_Component;

    ----------------------------------------------------------------------------

    procedure Delete_Component( this      : not null access Entity'Class;
                                component : in out A_Component ) is
    begin
        if component /= null then
            if this.system /= null then
                -- the component is being removed during gameplay from an entity
                -- that is not being deleted.
                component.Handle_Message( MSG_Destruct, Create_Map.Map );
            end if;
            this.Remove_Component( component );

            if this.iteratingComponents then
                -- if we're currently iterating through the components in Tick,
                -- the component can't be safely deleted here so we flag it for
                -- deletion later (by removing its owner) and leave it for Tick
                -- to clean up after iteration.
                component := null;
            else
                Delete( component );
            end if;
        end if;
    end Delete_Component;

    ----------------------------------------------------------------------------

    procedure Dispatch_Message( this   : not null access Entity'Class;
                                name   : Hashed_String;
                                params : Map_Value ) is
        use Listener_Maps;
        mapCrsr : constant Listener_Maps.Cursor := this.listeners.Find( name );
    begin
        if not Has_Element( mapCrsr ) then
            return;    -- no listeners
        end if;

        declare
            pos        : Component_Lists.Cursor := Element( mapCrsr ).First;
            recipients : Component_Array(1..Integer(Element( mapCrsr ).Length));
        begin
            -- create a copy of all recipients before dispatching the message,
            -- in case a recipient of the message decides to remove another
            -- recipient of the message (requires updating this.listeners, so we
            -- can't iterate over it while dispatching).
            for i in recipients'Range loop
                recipients(i) := Component_Lists.Element( pos );
                Component_Lists.Next( pos );
            end loop;

            for i in recipients'Range loop
                if recipients(i).Get_Owner /= null then
                    recipients(i).Handle_Message( name, params );
                end if;
            end loop;
        end;
    end Dispatch_Message;

    ----------------------------------------------------------------------------

    procedure Dispatch_Message( this : not null access Entity'Class;
                                name : Hashed_String ) is
    begin
        this.Dispatch_Message( name, Create_Map.Map );
    end Dispatch_Message;

    ----------------------------------------------------------------------------

    function Get_Age( this : not null access Entity'Class ) return Time_Span is (this.age);

    ----------------------------------------------------------------------------

    function Get_Attributes( this : not null access Entity'Class ) return access Entity_Attributes.Attributes'Class is (this.attrs);

    ----------------------------------------------------------------------------

    function Get_Component( this   : not null access Entity'Class;
                            family : Family_Id ) return A_Component is
        crsr       : Component_Maps.Cursor;
        components : A_Component_List;
    begin
        crsr := this.components.Find( family );
        if Component_Maps.Has_Element( crsr ) then
            components := Component_Maps.Element( crsr );
            if components.Length = 1 then
                return components.First_Element;
            end if;
        end if;
        return null;
    end Get_Component;

    ----------------------------------------------------------------------------

    function Get_Id( this : not null access Entity'Class ) return Entity_Id is (this.id);

    ----------------------------------------------------------------------------

    function Get_Name( this : not null access Entity'Class ) return String is (To_String( this.name ));

    ----------------------------------------------------------------------------

    function Get_System( this : not null access Entity'Class ) return access Entity_System'Class is (this.system);

    ----------------------------------------------------------------------------

    function Get_Template( this : not null access Entity'Class ) return String is (To_String( this.template ));

    ----------------------------------------------------------------------------

    function Get_World( this : not null access Entity'Class ) return access Worlds.World_Object'Class is (this.system.Get_World);

    ----------------------------------------------------------------------------

    not overriding
    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Entity ) is
        component  : A_Component;
        components : A_Component_List;
        crsr       : Component_Maps.Cursor;
    begin
        obj.id := Entity_Id'Input( stream );
        obj.name := Read_String_16( stream );
        obj.template := Read_String_16( stream );
        obj.age := Time_Span'Input( stream );

        obj.componentCount := Natural'Input( stream );
        for i in 1..obj.componentCount loop
            component := A_Component'Input( stream );
            if component.Get_Family = ATTRIBUTES_ID then
                -- there should only be one Attributes component
                pragma Assert( obj.attrs = null );
                obj.attrs := A_Attributes(component);
            end if;

            -- add the component, but don't set its owner yet because 'obj'
            -- is a temporary object that we can't get an access to.
            crsr := obj.components.Find( component.Get_Family );
            if not Component_Maps.Has_Element( crsr ) then
                components := new Component_Lists.List;
                components.Append( component );
                obj.components.Insert( component.Get_Family, components );
                component := null;
            else
                if component.Allow_Multiple then
                    Component_Maps.Element( crsr ).Append( component );
                    component := null;
                else
                    raise DUPLICATE_COMPONENT with
                        "Found duplicate " & component.To_String & " in entity " &
                        Image( obj.id ) & " (template=" & To_String( obj.template ) & ")";
                end if;
            end if;
        end loop;
    exception
        when others =>
            Delete( component );
            raise;
    end Object_Read;

    ----------------------------------------------------------------------------

    not overriding
    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Entity ) is
        components : Component_Array(1..obj.componentCount);
    begin
        Entity_Id'Output( stream, obj.id );
        Write_String_16( stream, obj.name );
        Write_String_16( stream, obj.template );
        Time_Span'Output( stream, obj.age );

        Natural'Output( stream, obj.componentCount );
        Sort_By_Dependency_Order( obj.components, components );
        for c of components loop
            A_Component'Output( stream, c );
        end loop;
    end Object_Write;

    ----------------------------------------------------------------------------

    procedure Register_Listener( this        : not null access Entity'Class;
                                 messageName : Hashed_String;
                                 comp   : not null access Component'Class ) is
        use Listener_Maps;
        crsr : Listener_Maps.Cursor;
        list : A_Component_List;
    begin
        crsr := this.listeners.Find( messageName );
        if Has_Element( crsr ) then
            Element( crsr ).Append( comp );
        else
            list := new Component_Lists.List;
            list.Append( comp );
            this.listeners.Insert( messageName, list );
        end if;
    end Register_Listener;

    ----------------------------------------------------------------------------

    procedure Remove_Component( this : not null access Entity'Class;
                                comp : access Component'Class ) is
        crsr       : Component_Maps.Cursor;
        components : A_Component_List;
        lcrsr      : Component_Lists.Cursor;
    begin
        if comp = null then
            return;
        end if;

        crsr := this.components.Find( comp.Get_Family );
        if Component_Maps.Has_Element( crsr ) then
            components := Component_Maps.Element( crsr );
            lcrsr := components.Find( comp );
            if Component_Lists.Has_Element( lcrsr ) then

                if this.system /= null then
                    comp.On_Exit_System;
                    this.system.Unassociate( comp );
                end if;
                this.Unregister_Listener( comp );
                if this.attrs /= null then
                    -- 'this.attrs' will be null when the whole Entity is being
                    -- deleted. this is so we don't have to delete in dependency
                    -- order to make sure this.attrs is deleted last (slow!).
                    this.attrs.Unregister_All( comp );
                end if;
                comp.Set_Owner( null );

                components.Delete( lcrsr );
                this.componentCount := this.componentCount - 1;
                return;
            end if;
        end if;
        raise COMPONENT_ERROR with "Component does not belong to entity " & Image( this.id );
    end Remove_Component;

    ----------------------------------------------------------------------------

    procedure Set_Directive( this       : not null access Entity'Class;
                             directives : Directive_Bits;
                             mode       : Directive_Mode ) is
    begin
        pragma Debug( Dbg( "Entity " & this.To_String &
                           " directive(s) " & Directive_Bits'Image( directives ) &
                           ": " & Directive_Mode'Image( mode ),
                           D_ENTITY, Info ) );

        this.directives(Once) := this.directives(Once) and not directives;
        this.directives(Ongoing) := this.directives(Ongoing) and not directives;
        if mode in this.directives'Range then
            this.directives(mode) := this.directives(mode) or directives;
        end if;

        -- assume the directive modified the world state in some way
        Queue_World_Modified;
    end Set_Directive;

    ----------------------------------------------------------------------------

    procedure Set_Name( this : not null access Entity'Class; name : String ) is
    begin
        this.name := To_Unbounded_String( name );
    end Set_Name;

    ----------------------------------------------------------------------------

    procedure Set_Session_Var( this : not null access Component'Class; name : String; val : Value'Class ) is
    begin
        this.owner.Get_System.Set_Session_Var( name, val );
    end Set_Session_Var;

    ----------------------------------------------------------------------------

    procedure Set_System( this   : not null access Entity'Class;
                          system : access Systems.Entity_System'Class ) is
    begin
        if this.system /= null then
            -- unregister the entity's components from the system
            for family of this.components loop
                for c of family.all loop
                    c.On_Exit_System;
                    this.system.Unassociate( c );
                end loop;
            end loop;
        end if;
        this.system := system;
        if this.system /= null then
            -- register entity's components with the system
            for family of this.components loop
                for c of family.all loop
                    this.system.Associate( c );
                    c.On_Enter_System;
                end loop;
            end loop;
        end if;
    end Set_System;

    ----------------------------------------------------------------------------

    procedure Tick( this : not null access Entity'Class; time : Tick_Time ) is
    begin
        this.age := this.age + time.elapsed;

        -- tick all components
        declare
            components : Component_Array(1..this.componentCount);
        begin
            -- build the iteration list for this Tick
            Sort_By_Tick_Order( this.components, components );

            -- iterate over all components that have not been removed. those
            -- that have been flagged for removal, by being disassociated with
            -- their owner entity, will be deleted after iteration.
            this.iteratingComponents := True;
            for j in components'Range loop
                if components(j).tickable then
                    components(j).age := components(j).age + time.elapsed;
                    if components(j).Get_Owner /= null then
                        components(j).Tick( (total   => components(j).age,
                                             elapsed => time.elapsed) );
                    end if;
                else
                    -- found the first of the components that do not implement
                    -- Tick. we still need to update their age.
                    for k in j..components'Last loop
                        components(k).age := components(k).age + time.elapsed;
                    end loop;
                    exit;    -- done iterating
                end if;
            end loop;
            this.iteratingComponents := False;

            -- delete any components flagged for deletion during iteration. they
            -- have already been removed from the component map.
            for j in components'Range loop
                if components(j).Get_Owner = null then
                    Delete( components(j) );
                end if;
            end loop;
        end;

        -- clear all Once (non-ongoing) directives
        this.directives(Once) := NO_DIRECTIVES;
    end Tick;

    ----------------------------------------------------------------------------

    overriding
    function To_String( this : access Entity ) return String
    is ("<" & this.Get_Class_Name & " template=" & To_String( this.template ) &
        ", id=" & Image( this.id ) & ", name=" & To_String( this.name ) & ">");

    ----------------------------------------------------------------------------

    procedure Unregister_Listener( this        : not null access Entity'Class;
                                   messageName : Hashed_String;
                                   comp        : not null access Component'Class ) is
        mapCrsr  : Listener_Maps.Cursor;
        listCrsr : Component_Lists.Cursor;
        list     : A_Component_List;
    begin
        mapCrsr := this.listeners.Find( messageName );
        if Listener_Maps.Has_Element( mapCrsr ) then
            list := Listener_Maps.Element( mapCrsr );
            listCrsr := list.Reverse_Find( comp );
            if Component_Lists.Has_Element( listCrsr ) then
                list.Delete( listCrsr );
            end if;
        end if;
    end Unregister_Listener;

    ----------------------------------------------------------------------------

    procedure Unregister_Listener( this : not null access Entity'Class;
                                   comp : not null access Component'Class ) is
        use Component_Lists;
        listPos, listNext : Component_Lists.Cursor;
    begin
        for list of this.listeners loop
            listPos := list.First;
            while Has_Element( listPos ) loop
                listNext := Next( listPos );
                if Element( listPos ) = comp then
                    list.Delete( listPos );
                end if;
                listPos := listNext;
            end loop;
        end loop;
    end Unregister_Listener;

    ----------------------------------------------------------------------------

    function A_Entity_Input( stream : access Root_Stream_Type'Class ) return A_Entity is
        obj : A_Entity := null;
    begin
        if Boolean'Input( stream ) then
            obj := new Entity;
            obj.Construct;
            Entity'Read( stream, obj.all );

            -- associate the entity's components with it, now that we have an
            -- access to the entity. they should be added to the entity in
            -- dependency order.
            declare
                components : Component_Array(1..Component_Count( obj.components ));
            begin
                Sort_By_Dependency_Order( obj.components, components );
                for i in components'Range loop
                    components(i).Set_Owner( obj );
                end loop;
            end;
        end if;

        -- This temporary code allows an entity to be respawned in place,
        -- effectively refreshing its composition from the current entity
        -- template. This is useful for rebuilding entities that are out of date
        -- or removing dependencies on old Component classes before they are
        -- deleted.
        --declare
        --    function Needs_Respawn( template : String ) return Boolean is (
        --        template = "..." or else
        --        template = "..."
        --    );
        --    respawned : A_Entity;
        --begin
        --    if Needs_Respawn( obj.Get_Template ) then
        --        respawned := Create_Entity( obj.Get_Template, Maps.Create( (
        --            Pair("id", To_Id_Value( obj.Get_Id )),
        --            Pair("Location", Maps.Create( (
        --                Pair("x", Create( A_Location(obj.Get_Component( LOCATION_ID )).Get_X )),
        --                Pair("y", Create( A_Location(obj.Get_Component( LOCATION_ID )).Get_Y ))
        --            ) ))
        --        ) ).Map);
        --        if respawned /= null then
        --            Delete( obj );
        --            obj := respawned;
        --        end if;
        --    end if;
        --end;

        return obj;
    exception
        when e : Tag_Error =>
            Delete( obj );
            declare
                msg : constant String := Exception_Message( e );
            begin
                raise Constraint_Error with "Found unknown entity class " &
                      "<" & To_Lower( msg((Index( msg, ".", Backward ) + 1)..msg'Last) ) & ">";
            end;
        when others =>
            Delete( obj );
            raise;
    end A_Entity_Input;

    ----------------------------------------------------------------------------

    procedure A_Entity_Output( stream : access Root_Stream_Type'Class; obj : A_Entity ) is
    begin
        Boolean'Output( stream, obj /= null );
        if obj /= null then
            Entity'Write( stream, obj.all );
        end if;
    end A_Entity_Output;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Entity ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

    --==========================================================================

    overriding
    procedure Construct( this : access Component ) is
    begin
        Object(this.all).Construct;
        this.id := Unique_Tagged_Id( Component_Tag );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Adjust( this : access Component ) is
    begin
        raise COPY_NOT_ALLOWED;
    end Adjust;

    ----------------------------------------------------------------------------

    function Get_Age( this : not null access Component'Class ) return Time_Span is (this.age);

    ----------------------------------------------------------------------------

    function Get_Definition( this : not null access Component'Class ) return String is (To_String( this.definition ));

    ----------------------------------------------------------------------------

    function Get_Entity( this : not null access Component'Class; id : Entity_Id ) return A_Entity
    is (this.owner.Get_System.Get_Entity( id ));

    ----------------------------------------------------------------------------

    function Get_Id( this : not null access Component'Class ) return Component_Id is (this.id);

    ----------------------------------------------------------------------------

    function Get_Owner( this : not null access Component'Class ) return A_Entity is (this.owner);

    ----------------------------------------------------------------------------

    function Get_Session_Var( this : not null access Component'Class; name : String ) return Value
    is (this.owner.Get_System.Get_Session_Var( name ));

    ----------------------------------------------------------------------------

    function In_Editor( this : not null access Component'Class ) return Boolean is
        pragma Unreferenced( this );
    begin
        -- default to True because gameplay requires a Session
        return Get_Game = null or else
               Get_Game.Get_Session = null or else
               Get_Game.Get_Session.Is_Editor;
    end In_Editor;

    ----------------------------------------------------------------------------

    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Component ) is
    begin
        Object'Read( stream, Object(obj) );
        obj.id := Component_Id'Input( stream );
        obj.definition := Read_String_16( stream );
        obj.age := Time_Span'Input( stream );
    end Object_Read;

    ----------------------------------------------------------------------------

    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Component ) is
    begin
        Object'Write( stream, Object(obj) );
        Component_Id'Output( stream, obj.id );
        Write_String_16( stream, obj.definition );
        Time_Span'Output( stream, obj.age );
    end Object_Write;

    ----------------------------------------------------------------------------

    procedure Register_Entity_Attribute( this      : not null access Component'Class;
                                         name      : String;
                                         reference : Value'Class;
                                         scope     : Notify_Scope := Silent_Scope ) is
    begin
        this.owner.Get_Attributes.Register( name, A_Component(this), reference, scope );
    end Register_Entity_Attribute;

    ----------------------------------------------------------------------------

    procedure Register_Listener( this        : not null access Component'Class;
                                 messageName : Hashed_String ) is
    begin
        this.owner.Register_Listener( messageName, this );
    end Register_Listener;

    ----------------------------------------------------------------------------

    procedure Set_Owner( this  : not null access Component'Class;
                         owner : access Entity'Class ) is
    begin
        if owner /= null then
            this.owner := A_Entity(owner);
            this.On_Added;
        else
            this.On_Removed;
            this.owner := null;
        end if;
    end Set_Owner;

    ----------------------------------------------------------------------------

    not overriding
    procedure Tick( this : access Component; time : Tick_Time ) is
        pragma Unreferenced( time );
    begin
        -- Tick isn't implemented so don't call it again
        this.tickable := False;
    end Tick;

    ----------------------------------------------------------------------------

    overriding
    function To_String( this : access Component ) return String
    is (
        "<" & External_Tag( Component'Class(this.all)'Tag ) & " : owner=" &
        (if this.Get_Owner = null then "null" else Image( this.Get_Owner.Get_Id )) & ">"
    );

    ----------------------------------------------------------------------------

    procedure Unregister_Entity_Attribute( this : not null access Component'Class;
                                           name : String ) is
    begin
        this.owner.Get_Attributes.Unregister( name, this );
    end Unregister_Entity_Attribute;

    ----------------------------------------------------------------------------

    procedure Unregister_Listener( this        : not null access Component'Class;
                                   messageName : Hashed_String ) is
    begin
        this.owner.Unregister_Listener( messageName, this );
    end Unregister_Listener;

    ----------------------------------------------------------------------------

    function A_Component_Input( stream : access Root_Stream_Type'Class ) return A_Component is
        obj : A_Component := null;
    begin
        if Boolean'Input( stream ) then
            obj := new Component'Class'(Component'Class'Input( stream ));
        end if;
        return obj;
    exception
        when e : Tag_Error =>
            declare
                msg : constant String := Exception_Message( e );
            begin
                raise Constraint_Error with "Found unknown entity component class " &
                      "<" & To_Lower( msg((Index( msg, ".", Backward ) + 1)..msg'Last) ) & ">";
            end;
    end A_Component_Input;

    ----------------------------------------------------------------------------

    procedure A_Component_Output( stream : access Root_Stream_Type'Class; obj : A_Component ) is
    begin
        Boolean'Output( stream, obj /= null );
        if obj /= null then
            Component'Class'Output( stream, obj.all );
        end if;
    end A_Component_Output;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Component ) is
    begin
        Delete( A_Object(this) );
    end Delete;

end Entities;
