--
-- Copyright (c) 2014 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Entities.Systems.Entity_Clipping is

    function Create_Entity_Clipping_System return A_Component_System;

private

    -- if the system becomes unstable, this is the number of stable steps
    -- required before it is considered stable again.
    STABILITY_STEPS_THRESHOLD : constant := 20;

    -- bail out if we spend more than this amount of time calculating a frame.
    ABORT_THRESHOLD : constant Time_Span := Milliseconds( 500 );

    type Entity_Clipping_System is new Component_System with
        record
            stableSteps : Natural := STABILITY_STEPS_THRESHOLD;
            iterations  : Integer := 1;
        end record;

    type A_Entity_Clipping_System is access all Entity_Clipping_System'Class;

    overriding
    procedure Tick( this : access Entity_Clipping_System; time : Tick_Time );

end Entities.Systems.Entity_Clipping;
