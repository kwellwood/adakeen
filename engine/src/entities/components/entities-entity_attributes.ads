--
-- Copyright (c) 2016-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Indefinite_Ordered_Maps;
with Ada.Unchecked_Deallocation;
with Entities.Components;               use Entities.Components;
with Entities.Factory;                  use Entities.Factory;
with Scribble.Namespaces;               use Scribble.Namespaces;

package Entities.Entity_Attributes is

    -- Sent to an entity when the value of one of its attributes has changed.
    --
    -- AttributeChanged {"name": "<name>", "value": <new-value>}
    MSG_AttributeChanged : constant Hashed_String := To_Hashed_String( "AttributeChanged" );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Property     Type   Description
    -- --------     ----   -----------
    -- attributes   map    Contains attribute definitions. Each key is the name
    --                     of an attribute and each value is its initial value.

    -- Defines the Attributes component with the entity factory.
    procedure Define_Component( factory : not null A_Entity_Factory );

    ----------------------------------------------------------------------------

    -- The Attributes class is a Component that encapsulates all of an entity's
    -- named attributes. Attributes can either be registered by other components,
    -- with accessors and modifiers, or they can be defined on-the-fly by
    -- scripts and entity templates.
    --
    -- When the value of an attribute changes, an AttributeChanged message is
    -- sent to the entity and an Entity_Attribute_Changed event is queued.
    -- Registered attributes can optionally silence these notifications and
    -- declare their values to be read-only.
    --
    -- Attributes are the scripting interface of an entity. A script can affect
    -- an entity either by sending it a message or by changing its attributes
    -- (which also sends an AttributeChanged message).
    --
    -- Attributes are also the GUI interface of an entity. Attributes are
    -- available to the View via Attribute_Changed messages, giving the View
    -- valuable pieces of information that can be configured. An editor app
    -- could, for example, allow the user to see an entity's attributes and to
    -- change them, affecting the entity's state.
    --
    -- Component families that register attributes must share the attribute
    -- name space. Component families 'A' and 'B' should not both register an
    -- attribute named 'x'. If they do, the first component added to the entity
    -- will "win"- subsequent registrations of the same name will be ignored.
    -- This also applies to component families that can have multiple instances
    -- simultaneously in an entity. The first instance of the component family
    -- that registers its attributes with the entity will prevent subsequent
    -- instances in the same entity from registering their attributes.
    type Attributes is new Component and Scribble_Namespace with private;
    type A_Attributes is access all Attributes'Class;

    -- Only one Attributes component can be added to an entity, and it is always
    -- present.
    overriding
    function Allow_Multiple( this : access Attributes ) return Boolean is (False);

    overriding
    function Get_Dependency_Order( this : access Attributes ) return Natural is (DEPS_ATTRIBUTES);

    overriding
    function Get_Family( this : access Attributes ) return Family_Id is (ATTRIBUTES_ID);

    -- Returns the value of attribute 'name' within the namespace (without
    -- copying), or Null if 'name' is undefined. Registered attributes will
    -- override script attributes.
    function Get_Namespace_Name( this : access Attributes;
                                 name : String ) return Value;

    -- Returns a reference to the value of attribute 'name' within the namespace.
    -- Registered attributes will override script attributes. If attribute 'name'
    -- is undefined, it will be defined with a Null value.
    function Get_Namespace_Ref( this : access Attributes;
                                name : String ) return Value;
    pragma Postcondition( Get_Namespace_Ref'Result.Is_Ref or else
                          Get_Namespace_Ref'Result.Is_Null );

    overriding
    function Get_Tick_Order( this : access Attributes ) return Natural is (TCK_ATTRIBUTES);

    -- Returns a map of current public (broadcast to the Game_View) attribute
    -- values, registered or not. The returned value is NOT a copy. DO NOT
    -- modify it!
    function Get_Public_Values( this : access Attributes ) return Map_Value;

    -- Returns a map of locally stored attribute values; it does not include
    -- attributes that were registered by other components. The returned value
    -- is NOT a copy. DO NOT modify it!
    function Get_Local_Values( this : Attributes'Class ) return Map_Value;

    -- Dispatches notifications that the value of attribute 'name', registered
    -- by component 'comp', has changed. Notifications will be dispatched
    -- according to the scope with which the attribute was registered.
    --
    -- Nothing will happen if the entity is not attached to the entity system,
    -- or if 'comp' did not register attribute 'name'. This may be called even
    -- if 'name' was registered with Silent_Scope.
    procedure Notify_Changed( this : not null access Attributes'Class;
                              comp : not null A_Component;
                              name : String );

    -- Registers attribute 'name', provided by component 'comp'. The registering
    -- component handles reads and writes to the reference value 'reference'.
    -- (See Values.Indirects.Generic) The registering component is responsible
    -- for calling Notify_Changed() when the value of the attribute changes.
    --
    -- 'scope' determines the scope of notifications when 'comp' detects the
    -- value has changed and calls Notify_Changed(). To suppress notifications,
    -- pass 'Silent_Scope'. To notify only the owning entity, pass 'Entity_Scope'.
    -- To additionally notify the Game_View, pass 'Global_Scope'.
    --
    -- Only one component in an entity can register attribute 'name', to prevent
    -- ambiguity. If the attribute has already been registered by another
    -- component, the second registration will be ignored.
    --
    -- This procedure should be called by implementations of the
    -- Component.On_Added() procedure, as necessary. All names registered by a
    -- component will be automatically unregistered when the component is
    -- removed.
    --
    -- To register a read-only attribute, pass an indirect value that does not
    -- have a writer function and make sure the reader function returns only
    -- copies of the current value (in case the value is a container value).
    --
    -- To register a container value attribute (e.g. list, map) that has
    -- editable elements but also prevents itself from being replaced entirely
    -- by another value, register an indirect value whose reader does not return
    -- a copy and whose writer is undefined.
    --
    -- All attributes can be accessed from Scribble with the member operator
    -- ('.'). For example, "world.player.vx := 10" would set the "vx" attribute
    -- of the "player" entity (represented by an Id value) to 10.
    procedure Register( this      : not null access Attributes'Class;
                        name      : String;
                        comp      : not null A_Component;
                        reference : Value'Class;
                        scope     : Notify_Scope := Silent_Scope );
    pragma Precondition( reference.Is_Ref );
    pragma Precondition( comp.Get_Owner = this.Get_Owner );

    -- Sets any attributes contained in 'defaults' that are currently Null. If
    -- an attribute is already defined, it will not be copied from 'defaults'.
    procedure Set_Defaults( this     : not null access Attributes'Class;
                            defaults : Map_Value );

    -- Sets the value of attribute 'name', by copy. If 'name' is a registered
    -- attribute, the registering component will process the set and may reject
    -- it (i.e.: the attribute is readonly.) If the attribute value changes, an
    -- AttributeChanged message will be sent to the entity and an
    -- Entity_Attribute_Changed event will be queued to the event manager.
    procedure Set_Value( this : not null access Attributes'Class;
                         name : String;
                         val  : Value'Class );

    -- Unregisters attribute 'name', originally registered by 'comp'. If 'comp'
    -- did not register attribute 'name', the unregistration will be ignored.
    procedure Unregister( this : not null access Attributes'Class;
                          name : String;
                          comp : not null A_Component );

    -- Unregisters all entity attributes previously registered by 'comp'.
    procedure Unregister_All( this : not null access Attributes'Class;
                              comp : not null A_Component );

private

    type Attribute_Def is
        record
            component : A_Component := null;          -- registering component (if any)
            exRef     : Value;                        -- external value reference
            ref       : Value;                        -- actual value reference
            val       : aliased Value;                -- locally stored value
            scope     : Notify_Scope := Silent_Scope; -- value change notification scope
        end record;
    type A_Attribute_Def is access all Attribute_Def;

    procedure Free is new Ada.Unchecked_Deallocation( Attribute_Def, A_Attribute_Def );

    package Attribute_Maps is new Ada.Containers.Indefinite_Ordered_Maps( String, A_Attribute_Def, "<", "=" );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Attributes is new Component and Scribble_Namespace with
        record
            names      : Attribute_Maps.Map;
            api        : Map_Value;
            storedVals : Map_Value;     -- read from stream, used in On_Added
        end record;
    for Attributes'External_Tag use "Component.Attributes";

    procedure Construct( this : access Attributes );

    procedure Notify_World_Modified( this : not null access Attributes'Class );

    function Object_Input( stream : access Root_Stream_Type'Class ) return Attributes;
    for Attributes'Input use Object_Input;

    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Attributes );
    for Attributes'Read use Object_Read;

    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Attributes );
    for Attributes'Write use Object_Write;

    procedure On_Added( this : access Attributes );

    procedure On_Enter_System( this : access Attributes );

end Entities.Entity_Attributes;
