--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Clipping;                          use Clipping;
--with Debugging;                         use Debugging;
with Directions;                        use Directions;
with Entities.Locations;                use Entities.Locations;
with Entities.Movements;                use Entities.Movements;
with Entities.Systems;                  use Entities.Systems;
with Support;                           use Support;
with Values.Strings;                    use Values.Strings;
with Vector_Math;                       use Vector_Math;
with Worlds;                            use Worlds;

package body Entities.Map_Clipping.Precise is

    function Create_Precise_Clipped( properties : Map_Value ) return A_Component is
        pragma Unreferenced( properties );
        this : constant A_Precise_Clipped := new Precise_Clipped;
    begin
        this.Construct;
        return A_Component(this);
    end Create_Precise_Clipped;

    ----------------------------------------------------------------------------

    overriding
    function Object_Input( stream : access Root_Stream_Type'Class ) return Precise_Clipped is
        this : aliased Precise_Clipped;
    begin
        this.Construct;
        Precise_Clipped'Read( stream, this );
        return this;
    end Object_Input;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Precise_Clipped ) is
    begin
        Map_Clipped'Read( stream, Map_Clipped(obj) );
    end Object_Read;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Precise_Clipped ) is
    begin
        Map_Clipped'Write( stream, Map_Clipped(obj) );
    end Object_Write;

    ----------------------------------------------------------------------------

    overriding
    procedure Tick( this : access Precise_Clipped; time : Tick_Time ) is
        pragma Unreferenced( time );
        world      : constant A_World := this.Get_Owner.Get_System.Get_World;
        tileWidth  : constant Positive := world.Tile_Width;
        fTileWidth : constant Float := Float(tileWidth);
        mov        : constant A_Movement := A_Movement(this.Get_Component( MOVEMENT_ID ));
        loc        : constant A_Location := A_Location(this.Get_Component( LOCATION_ID ));
        initialX   : constant Float := loc.Get_X;
        initialY   : constant Float := loc.Get_Y;
        prevX      : constant Float := loc.Get_Prev_X;
        prevY      : constant Float := loc.Get_Prev_Y;
        width      : constant Natural := loc.Get_Width;
        height     : constant Natural := loc.Get_Height;
        halfWidth  : constant Float := Float(width) / 2.0;
        halfHeight : constant Float := Float(height) / 2.0;

        type Axis_Type is (X_Axis, Y_Axis);

        ------------------------------------------------------------------------

        procedure Notify_Hit_Wall( dir : Cardinal_Direction ) is
        begin
            this.Get_Owner.Dispatch_Message( MSG_HitWall, Maps.Create( (1=>Pair("dir", Create( Lower( dir'Img ) ))) ).Map );
        end Notify_Hit_Wall;

        ------------------------------------------------------------------------

        procedure Clip_Left_Edge( x, y      : in out Float;
                                  collision : out Boolean ) is
            leftX    : constant Float := x - halfWidth;
            col      : constant Integer := Integer(Float'Floor(leftX / fTileWidth));
            topRow   : constant Integer := Integer(Float'Floor((y - halfHeight) / fTileWidth));
            botRow   : constant Integer := Integer(Float'Floor(Float'Ceiling(y + halfHeight - 1.0) / fTileWidth));
            clipType : Clip_Type;
            slopeY   : Float;
            edgeY    : Float;
        begin
            collision := False;
            if col < 0 then
                collision := True;
                return;
            end if;

            for row in topRow..botRow loop
                clipType := world.Get_Tile_Clip_Type( col, row );

                if clipType = Wall or else
                   Is_Slope_A( clipType ) or else
                   Is_Slope_C( clipType )
                then
                    collision := True;

                elsif Is_Slope_B( clipType ) then
                    if row < botRow then
                        -- ran smack into the slope (solid as a wall)
                        collision := True;
                    else
                        -- adjust the bottom up, out of the slope
                        slopeY := Slope_Y( clipType, leftX, col, row, tileWidth );
                        edgeY := y + halfHeight - 1.0;
                        if edgeY + 1.0 > slopeY then
                            y := (slopeY - halfHeight + 1.0) - 1.0;
                            collision := True;
                            return;
                        end if;
                    end if;

                elsif Is_Slope_D( clipType ) then
                    if row > topRow then
                        -- ran smack into the slope (solid as a wall)
                        collision := True;
                    else
                        -- adjust the top down, out of the slope
                        slopeY := Slope_Y( clipType, leftX, col, row, tileWidth );
                        edgeY := y - halfHeight;
                        if edgeY < slopeY then
                            y := slopeY + halfHeight;
                            collision := True;
                            return;
                        end if;
                    end if;

                end if;

                if collision then
                    x := Float((col + 1) * tileWidth) + halfWidth;
                    return;
                end if;
            end loop;
        end Clip_Left_Edge;

        ------------------------------------------------------------------------

        procedure Clip_Right_Edge( x, y      : in out Float;
                                   collision : out Boolean ) is
            rightX   : constant Float := x + halfWidth - 1.0;
            col      : constant Integer := Integer(Float'Floor(Float'Ceiling(rightX) / fTileWidth));
            topRow   : constant Integer := Integer(Float'Floor((y - halfHeight) / fTileWidth));
            botRow   : constant Integer := Integer(Float'Floor(Float'Ceiling(y + halfHeight - 1.0) / fTileWidth));
            clipType : Clip_Type;
            slopeY   : Float;
            edgeY    : Float;
        begin
            collision := False;
            if col < 0 or else col >= world.Get_Map_Columns then
                collision := True;
                return;
            end if;

            for row in topRow..botRow loop
                clipType := world.Get_Tile_Clip_Type( col, row );

                if clipType = Wall or else
                   Is_Slope_B( clipType ) or else
                   Is_Slope_D( clipType )
                then
                    collision := True;

                elsif Is_Slope_A( clipType ) then
                    if row < botRow then
                        -- ran smack into the slope (solid as a wall)
                        collision := True;
                    else
                        -- adjust the bottom up, out of the slope
                        slopeY := Slope_Y( clipType, rightX, col, row, tileWidth );
                        edgeY := y + halfHeight - 1.0;
                        if edgeY + 1.0 > slopeY then
                            y := (slopeY - halfHeight + 1.0) - 1.0;
                            collision := True;
                            return;
                        end if;
                    end if;

                elsif Is_Slope_C( clipType ) then
                    if row > topRow then
                        -- ran smack into the slope (solid as a wall)
                        collision := True;
                    else
                        -- adjust the top down, out of the slope
                        slopeY := Slope_Y( clipType, rightX, col, row, tileWidth );
                        edgeY := y - halfHeight;
                        if edgeY < slopeY then
                            y := slopeY + halfHeight;
                            collision := True;
                            return;
                        end if;
                    end if;

                end if;

                if collision then
                    x := Float(col * tileWidth) - halfWidth;
                    return;
                end if;
            end loop;
        end Clip_Right_Edge;

        ------------------------------------------------------------------------

        procedure Clip_Top_Edge( x, y      : in out Float;
                                 collision : out Boolean ) is
            topY     : constant Float := y - halfHeight;
            row      : constant Integer := Integer(Float'Floor(topY / fTileWidth));
            leftCol  : constant Integer := Integer(Float'Floor((x - halfWidth) / fTileWidth));
            rightCol : constant Integer := Integer(Float'Floor(Float'Ceiling(x + halfWidth - 1.0) / fTileWidth));
            clipType : Clip_Type;
            slopeX   : Float;
            edgeX    : Float;
        begin
            collision := False;
            if row < 0 then
                collision := True;
                return;
            end if;

            for col in leftCol..rightCol loop
                clipType := world.Get_Tile_Clip_Type( col, row );

                if clipType = Wall or else
                   Is_Slope_A( clipType ) or else
                   Is_Slope_B( clipType )
                then
                    collision := True;

                elsif Is_Slope_D( clipType ) then
                    if col > leftCol then
                        -- ran smack into the slope (solid as a wall)
                        collision := True;
                    else
                        -- adjust the left edge to the right, out of the slope
                        slopeX := Slope_X( clipType, topY, col, row, tileWidth );
                        edgeX := x - halfWidth;
                        if edgeX < slopeX then
                            x := slopeX + halfWidth;
                            collision := True;
                            return;
                        end if;
                    end if;

                elsif Is_Slope_C( clipType ) then
                    if col < rightCol then
                        -- ran smack into the slope (solid as a wall)
                        collision := True;
                    else
                        -- adjust the right edge to the left, out of the slope
                        slopeX := Slope_X( clipType, topY, col, row, tileWidth );
                        edgeX := x + halfWidth - 1.0;
                        if edgeX + 1.0 > slopeX then
                            x := (slopeX - halfWidth + 1.0) - 1.0;
                            collision := True;
                            return;
                        end if;
                    end if;

                end if;

                if collision then
                    y := Float((row + 1) * tileWidth) + halfHeight;
                    return;
                end if;
            end loop;
        end Clip_Top_Edge;

        ------------------------------------------------------------------------

        procedure Clip_Bottom_Edge( x, y      : in out Float;
                                    collision : out Boolean ) is
            botY     : constant Float := y + halfHeight - 1.0;
            row      : constant Integer := Integer(Float'Floor(Float'Ceiling(botY) / fTileWidth));
            leftCol  : constant Integer := Integer(Float'Floor((x - halfWidth) / fTileWidth));
            rightCol : constant Integer := Integer(Float'Floor(Float'Ceiling(x + halfWidth - 1.0) / fTileWidth));
            clipType : Clip_Type;
            slopeX   : Float;
            edgeX    : Float;
        begin
            collision := False;
            if row < 0 or else row >= world.Get_Map_Rows then
                collision := True;
                return;
            end if;

            for col in leftCol..rightCol loop
                clipType := world.Get_Tile_Clip_Type( col, row );

                if clipType = Wall or else
                   Is_Slope_C( clipType ) or else
                   Is_Slope_D( clipType )
                then
                    collision := True;

                elsif Is_Slope_B( clipType ) then
                    if col > leftCol then
                        -- ran smack into the slope (solid as a wall)
                        collision := True;
                    else
                        -- adjust the left edge to the right, out of the slope
                        slopeX := Slope_X( clipType, botY, col, row, tileWidth );
                        edgeX := x - halfWidth;
                        if edgeX < slopeX then
                            x := slopeX + halfWidth;
                            collision := True;
                            return;
                        end if;
                    end if;

                elsif Is_Slope_A( clipType ) then
                    if col < rightCol then
                        -- ran smack into the slope (solid as a wall)
                        collision := True;
                    else
                        -- adjust the right edge to the left, out of the slope
                        slopeX := Slope_X( clipType, botY, col, row, tileWidth );
                        edgeX := x + halfWidth - 1.0;
                        if edgeX + 1.0 > slopeX then
                            x := (slopeX - halfWidth + 1.0) - 1.0;
                            collision := True;
                            return;
                        end if;
                    end if;

                end if;

                if collision then
                    y := Float(row * tileWidth) - halfHeight;
                    return;
                end if;
            end loop;
        end Clip_Bottom_Edge;

        ------------------------------------------------------------------------

        procedure Clip_X( x, y      : in out Float;
                          collision : out Boolean ) is
        begin
            collision := False;
            if initialX < prevX then
                Clip_Left_Edge( x, y, collision );
            elsif initialX > prevX then
                Clip_Right_Edge( x, y, collision );
            end if;
        end Clip_X;

        ------------------------------------------------------------------------

        procedure Clip_Y( x, y      : in out Float;
                          collision : out Boolean ) is
        begin
            collision := False;
            if initialY < prevY then
                Clip_Top_Edge( x, y, collision );
            elsif initialY > prevY then
                Clip_Bottom_Edge( x, y, collision );
            end if;
        end Clip_Y;

        ------------------------------------------------------------------------

        -- Apply clipping in both the X and Y axis, using 'primary' as the axis
        -- to clip first. If collisions are detected on both axes, this
        -- procedure will recurse just one level to swap the primary axis and
        -- try again.
        procedure Clip_X_and_Y( x, y       : in out Float;
                                primary    : Axis_Type;
                                swapOnFail : Boolean := True ) is
            xOld       : constant Float := x;
            yOld       : constant Float := y;
            xCollision,
            yCollision : Boolean;
            ignored    : Boolean;
        begin
            if primary = X_Axis then
                Clip_X( x, y, xCollision );
                Clip_Y( x, y, yCollision );
                if xCollision then
                    Clip_X( x, y, ignored );
                end if;
                if yCollision then
                    Clip_Y( x, y, ignored );
                end if;
            else
                Clip_Y( x, y, yCollision );
                Clip_X( x, y, xCollision );
                if yCollision then
                    Clip_Y( x, y, ignored );
                end if;
                if xCollision then
                    Clip_X( x, y, ignored );
                end if;
            end if;

            if swapOnFail and then xCollision and then yCollision then
                x := xOld;
                y := yOld;
                Clip_X_and_Y( x, y,
                              (if primary = X_Axis then Y_Axis else X_Axis),
                              swapOnFail => False );
            end if;
        end Clip_X_and_Y;

        ------------------------------------------------------------------------

        -- Detects if the entity's movement is blocked in direction 'dir' if
        -- its location is 'x', 'y'.
        function Is_Blocked( dir : Cardinal_Direction; x, y : Float ) return Boolean is
            xTmp, yTmp : Float;
            blocked    : Boolean;
        begin
            case dir is
                when Left =>
                    xTmp := x - 0.01; yTmp := y;
                    Clip_Left_Edge( xTmp, yTmp, blocked );
                when Right =>
                    xTmp := x + 0.01; yTmp := y;
                    Clip_Right_Edge( xTmp, yTmp, blocked );
                when Up =>
                    xTmp := x; yTmp := y - 0.01;
                    Clip_Top_Edge( xTmp, yTmp, blocked );
                when Down =>
                    xTmp := x; yTmp := y + 0.01;
                    Clip_Bottom_Edge( xTmp, yTmp, blocked );
            end case;
            return blocked;
        end Is_Blocked;

        ------------------------------------------------------------------------

        -- Attempts to guess the primary axis for clipping. Sometimes we want to
        -- clip X before Y (e.g. sliding down against a vertical wall), and
        -- sometimes we want to clip Y before X (e.g. sliding along a horizontal
        -- wall.)
        --
        -- The guess is based on a comparison of the distances of the entity's
        -- edges (in the direction of movement) from the nearest tile boundary.
        -- For example, if the entity is moving left and downward and the left
        -- edge is slightly to the left of a tile column boundary and the bottom
        -- edge is halfway between tile row boundaries, then the X axis will be
        -- the primary clipping axis because it's likely that the left edge
        -- crossed a tile boundary more recently than the bottom edge.
        function Guess_Primary_Clip_Axis( x, y : Float ) return Axis_Type is
            edgeX, edgeY : Float;
            distX, distY : Float;
        begin
            if x = prevX then
                return Y_Axis;
            elsif y = prevY then
                return X_Axis;
            end if;

            if x < prevX then
                edgeX := x - halfWidth;
                distX := (Float'Floor( edgeX / fTileWidth ) + 1.0) * fTileWidth - edgeX;
            else -- x > prevX
                edgeX := Float'Ceiling(x + halfWidth - 1.0);
                distX := edgeX - Float'Floor(edgeX / fTileWidth) * fTileWidth;
            end if;

            if y < prevY then
                edgeY := y - halfHeight;
                distY := (Float'Floor( edgeY / fTileWidth ) + 1.0) * fTileWidth - edgeY;
            else -- y > prevY
                edgeY := Float'Ceiling(y + halfHeight - 1.0);
                distY := edgeY - Float'Floor(edgeY / fTileWidth) * fTileWidth;
            end if;

            return (if distX < distY then X_Axis else Y_Axis);
        end Guess_Primary_Clip_Axis;

        ------------------------------------------------------------------------

        -- Constrains the entity to the map's boundaries, stopping its movement
        -- when any edge of the entity reaches the edge of the map.
        procedure Clip_To_Boundaries( x, y : in out Float; velocity : in out Vec2 ) is
        begin
            -- stay within map boundaries
            if x - halfWidth < 0.0 then
                x := 0.0 + halfWidth;
                velocity.x := Float'Max( 0.0, velocity.x );
            elsif Float'Ceiling(x + halfWidth - 1.0) >= Float(world.Get_Width) then
                x := Float(world.Get_Width) - halfWidth;
                if velocity.x > 0.0 then
                    velocity.x := 0.0;
                else
                    -- this happens when an object is teleported outside of the
                    -- boundaries, probably because the ked user dragged it out.
                    -- subtract a small "fuzzy" amount from the exact edge of
                    -- the map so that an Entity_Moved event will always be
                    -- sent to the view. this prevents the entity from looking
                    -- like it's stuck out of bounds wherever it was dropped
                    -- simply because it was clipped back to the same location
                    -- it was dragged from, which would avoid the Entity_Moved
                    -- event.
                    x := x - 0.01 * Random_Float;
                end if;
            end if;
            if y - halfHeight < 0.0 then
                y := 0.0 + halfHeight;
                velocity.y := Float'Max( 0.0, velocity.y );
            elsif Float'Ceiling(y + halfHeight - 1.0) >= Float(world.Get_Height) then
                y := Float(world.Get_Height) - halfHeight;
                if velocity.y > 0.0 then
                    velocity.y := 0.0;
                else
                    -- see the note above
                    y := y - 0.01 * Random_Float;
                end if;
            end if;
        end Clip_To_Boundaries;

        ------------------------------------------------------------------------

        procedure Update_Blocked_State( x, y : Float ) is
            blocked : Boolean;
        begin
            blocked := Is_Blocked( Left, x, y );
            if blocked and then not this.blocked(Left) then
                Notify_Hit_Wall( Left );
            end if;
            this.blocked(Left) := blocked;

            blocked := Is_Blocked( Right, x, y );
            if blocked and then not this.blocked(Right) then
                Notify_Hit_Wall( Right );
            end if;
            this.blocked(Right) := blocked;

            blocked := Is_Blocked( Up, x, y );
            if blocked and then not this.blocked(Up) then
                Notify_Hit_Wall( Up );
            end if;
            this.blocked(Up) := blocked;

            blocked := Is_Blocked( Down, x, y );
            if blocked and then not this.blocked(Down) then
                Notify_Hit_Wall( Down );
            end if;
            this.blocked(Down) := blocked;
        end Update_Blocked_State;

        ------------------------------------------------------------------------

        x   : Float := loc.Get_X;
        y   : Float := loc.Get_Y;
        vel : Vec2  := mov.Get_V;
    begin
        Clip_X_and_Y( x, y, Guess_Primary_Clip_Axis( x, y ) );
        Clip_To_Boundaries( x, y, vel );
        Update_Blocked_State( x, y );

        loc.Set_XY( x, y );
        mov.Set_V( vel );
    end Tick;

    --==========================================================================

    procedure Define_Component( factory : not null A_Entity_Factory ) is
    begin
        factory.Define_Component( "PreciseClipped",
                                  Create_Precise_Clipped'Access,
                                  required =>
                                      (Create( Family_Name( LOCATION_ID ) ),
                                       Create( Family_Name( MOVEMENT_ID ) )) );
    end Define_Component;

end Entities.Map_Clipping.Precise;
