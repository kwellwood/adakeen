--
-- Copyright (c) 2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Indefinite_Ordered_Maps;

-- This generic state machine implementation is a cross between a hierarchical
-- state machine (FA) and a pushdown automata (PDA). The states are organized in
-- a tree, where each state is a substate of its parent. The current state is
-- represented by a stack of states (hence PDA) where the root node is always
-- at the bottom of the stack.
--
--     Tree:    Stack: (upside down)
--
--      (C)  D          Example: See the state tree on the left. The tree
--        \ /           describes the state hierarchy and possible transitions.
--   A    (B)     C     State transitions always follow the branches of the
--    \    /      B     state tree. When the current state is C, the states B
--    (ROOT)    ROOT    and ROOT will be on the state stack underneath it. To
--                      transition to state A from C, C will be exited, B will
--                      be exited, and then A will be entered. ROOT will stay.
--
-- This implementation is intended for use by entity component classes with
-- stateful behavior. Messages and frame ticks are dispatched to the current
-- state first and then down the stack until a state blocks further processing.
--
package Entities.Statefuls is

    -- A Stateful component is a hierarchical Finite State Automata whose
    -- behavior is driven by a tree of states. It maintains a stack of current
    -- states, dispatches messages to them, and handles state transitions.
    type Stateful is abstract new Component with private;
    type A_Stateful is access all Stateful'Class;

    -- Handles message 'name' with parameters 'params' that was received by the
    -- component's entity. The message will be dispatched to the state stack.
    procedure Handle_Message( this   : access Stateful;
                              name   : Hashed_String;
                              params : Map_Value );

    -- Handles gameplay update ticks. The states of the stack will be Tick'd
    -- from top to bottom until a state blocks further the action.
    procedure Tick( this : access Stateful; time : Tick_Time );

    ----------------------------------------------------------------------------

    -- A single state within the machine. The state may have substates. Enter
    -- and Exit actions are triggered when the machine transitions into and out
    -- of a state. Legal transitions are to a substate, ancestor state, or the
    -- direct sibling of an ancestor state.
    --
    -- Actions implemented by each state are Tick and Handle_Message. If a state
    -- does not explicitly handle the action, its parent state gets an
    -- opportunity to handle the action. This supports common behavior between
    -- substates with a shared parent.
    type State is abstract new Limited_Object with private;
    type A_State is access all State'Class;

private

    -- State ids identify states within the state machine. Each state must have
    -- a unique id.
    type State_Id is new Integer;

    -- the root (global) state's id
    ROOT_STATE : constant State_Id := 0;

    package State_Maps is new Ada.Containers.Indefinite_Ordered_Maps( State_Id, A_State, "<", "=" );

    ----------------------------------------------------------------------------

    type Stateful is abstract new Component with
        record
            root   : A_State;
            nextId : State_Id;         -- next state after the current action
            busy   : Natural := 0;     -- is a state action on the stack?

            -- *** these fields are streamed ***

            top    : A_State;          -- the top of the state stack (current state)
        end record;

    procedure Construct( this         : access Stateful;
                         root         : not null access State'Class;
                         initialState : State_Id );

    procedure Delete( this : in out Stateful );

    -- Returns the next state to enter after the current action completes. If
    -- the Stateful object is not executing an action (Tick or Handle_Message)
    -- then the current state's id will be returned.
    function Get_Next( this : not null access Stateful'Class ) return State_Id;

    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Stateful );
    for Stateful'Read use Object_Read;

    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Stateful );
    for Stateful'Write use Object_Write;

    -- Sets the next state to 'id', transitioning after the current action is
    -- complete. There must be a valid transition from the current (top of the
    -- stack) state to 'id'.
    --
    -- Valid transitions from the current state are: substates, ancestors, and
    -- siblings of ancestors. If 'id' cannot be located, nothing will happen.
    procedure Set_Next( this : not null access Stateful'Class; id : State_Id );

    -- Performs the state transitions to the next state. If the object is busy,
    -- nothing will happen.
    procedure Transition( this : not null access Stateful'Class );

    ----------------------------------------------------------------------------

    type State is abstract new Limited_Object with
        record
            owner     : A_Stateful := null;
            id        : State_Id := ROOT_STATE;
            parent    : A_State := null;
            subStates : State_Maps.Map;
        end record;

    -- Adds 'subState' as a substate of this. The state tree must be fully
    -- constructed before the root is added to the Stateful component.
    procedure Add_State( this     : not null access State'Class;
                         id       : State_Id;
                         subState : not null A_State );

    procedure Delete( this : in out State );

    -- Finds the state matching 'id' in or under this state, returning a pointer
    -- to it. Returns null if not found.
    function Find( this : not null access State'Class; id : State_Id ) return A_State;

    -- Handles a message 'name' with parameters 'parameters' received by the
    -- component's entity. Returns True if the message has been handled,
    -- otherwise the state's parent will be given an opportunity to handle the
    -- message.
    not overriding
    function Handle_Message( this    : access State;
                             name    : Hashed_String;
                             params  : Map_Value ) return Boolean is (False);

    -- Called when the state is entered and pushed onto the stack. This will not
    -- be called again until the state has been exited.
    not overriding
    procedure On_Enter( this : access State ) is null;

    -- Called when the state is exited and popped from the stack. This will not
    -- be called again unless the state is re-entered. During a state
    -- transition, the On_Exit of the previous state is always called before
    -- the On_Enter of the next state.
    not overriding
    procedure On_Exit( this : access State ) is null;

    -- Called when the state becomes the top state on the stack. This occurs
    -- when a state is pushed onto the stack and when its substates are popped
    -- off the stack. This is used to catch the situation where substate B is
    -- popped off the stack and substate A (which was not exited and will not be
    -- re-entered by the action) needs to react to being back on top again.
    --
    -- When this is called due to a state being pushed onto the stack, it will
    -- be called after On_Enter().
    not overriding
    procedure On_Top( this : access State ) is null;

    -- Pops the state off the current state stack as the next state transition.
    -- The transition will not occur until the current action has completed.
    procedure Pop( this : not null access State'Class );

    -- Handles a gameplay tick. Returns True if the action has been handled,
    -- otherwise the state's parent will be given an opportunity to tick.
    not overriding
    function Tick( this : access State; time : Tick_Time ) return Boolean is (False);

    -- Sets the next state to be 'id', when the current action has completed.
    procedure Set_Next( this : not null access State'Class; id : State_Id );

    -- Sets the state's Stateful pointer when it's added for the first time.
    procedure Set_Owner( this  : not null access State'Class;
                         owner : not null A_Stateful );

    -- Delete the state and all its substates.
    procedure Delete( this : in out A_State );
    pragma Postcondition( this = null );

    ----------------------------------------------------------------------------

    type Timer_Type is tagged
        record
            owner   : access Stateful'Class := null;
            period  : Time_Span := Time_Span_Zero;
            endTime : Time_Span := Time_Span_Zero;
        end record;

    -- Initializes the timer with a pointer to its owning Stateful instance.
    -- This must be called after the Stateful object has been constructed (NOT
    -- in the constructor) and before the timer is used.
    --
    -- Best practice: Initialize timers in the component's On_Added() callback.
    procedure Init( this : in out Timer_Type'Class; owner : access Stateful'Class );

    -- Clears the remaining time, expiring the timer immediately.
    procedure Clear( this : in out Timer_Type'Class );

    -- Returns True if the timer has expired. Until the timer is initially set,
    -- it will be expired.
    function Expired( this : Timer_Type'Class ) return Boolean;

    -- Returns the amount of time remaining before expiration.
    function Remaining( this : Timer_Type'Class ) return Time_Span;

    -- Start the timer again with the perious period. This should only be used
    -- after Start, otherwise nothing will happen.
    procedure Restart( this : in out Timer_Type'Class );

    -- Starts the timer with a period before expiration, given in the Stateful
    -- component's age. The timer must be initialized with its Stateful owner
    -- before this is called.
    procedure Start( this : in out Timer_Type'Class; period : Time_Span );

end Entities.Statefuls;
