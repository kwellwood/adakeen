--
-- Copyright (c) 2014-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Ordered_Sets;
with Directions;                        use Directions;
with Entities.Components;               use Entities.Components;
with Entities.Factory;                  use Entities.Factory;
with Entities.Locations;                use Entities.Locations;
with Entities.Movements;                use Entities.Movements;
with Vector_Math;                       use Vector_Math;

package Entities.Solids is

    -- This message is sent to the component's entity when it collides with a
    -- another entity. The 'edge' parameter indicates which edge came into
    -- contact with entity 'eid'. Possible values of 'edge' are "top", "bottom",
    -- "left", "right".
    --
    -- HitEntity {"edge": <collidingEdge>, "eid": <collidingEntityId>}
    MSG_HitEntity : constant Hashed_String := To_Hashed_String( "HitEntity" );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Registered Entity Attributes:
    --
    -- Name          Read/Write   Type      Description
    -- ----          ----------   ----      -----------
    -- solid         read/write   boolean   All edges are solid
    -- solidTop      read/write   boolean   Top edge is solid
    -- solidBottom   read/write   boolean   Bottom edge is solid
    -- solidLeft     read/write   boolean   Left edge is solid
    -- solidRight    read/write   boolean   Right edge is solid
    -- solidMass     read/write   boolean   Relative mass of the entity

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    SOLID_ID : Family_Id := To_Family_Id( "Solid" );

    -- Property    Type       Description
    -- --------    ----       -----------
    -- solid       boolean    All edges are solid
    -- top         boolean    Top edge is solid (overrides "solid")
    -- bottom      boolean    Bottom edge is solid (overrides "solid")
    -- left        boolean    Left edge is solid (overrides "solid")
    -- right       boolean    Right edge is solid (overrides "solid")
    -- mass        number     Relative mass of the entity (more massive blocks less massive)

    procedure Define_Component( factory : not null A_Entity_Factory );

    ----------------------------------------------------------------------------

    type Solid is new Component with private;
    type A_Solid is access all Solid'Class;

    type Manifold is
        record
            a, b      : A_Solid := null;
            collision : Boolean := False;    -- did a collision occur?
            normal    : Vec2;                -- collision normal (w/ magnitude of minimum distance)
        end record;

    -- Names of edges are "top", "bottom", "left", "right".
    function To_String( edge : Edge_Type ) return String with Inline;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Records 'contact' as contacting the edge specified by 'normal', relative
    -- to this solid body.
    procedure Add_Contact( this    : not null access Solid'Class;
                           normal  : Vec2;
                           contact : not null A_Solid );

    -- Clears all contacts from the Contact cache. This should be done every
    -- frame after collision resolution.
    procedure Clear_Contacts( this : not null access Solid'Class );

    -- Checks for a collision between A (this) and B (other), then calculates
    -- the collision normal and the minimum distance constraint, if there is a
    -- collision.
    procedure Detect_Collision( this  : not null access Solid'Class;
                                other : not null A_Solid;
                                man   : out Manifold );

    overriding
    function Get_Dependency_Order( this : access Solid ) return Natural is (DEPS_SOLID);

    overriding
    function Get_Family( this : access Solid ) return Family_Id is (SOLID_ID);

    -- Returns the mass of the entity. When colliding with other solids, the
    -- entity with the largest mass blocks the entity smallest mass. If two
    -- entities with the same mass collide, the entity moving the fastest will
    -- be blocked.
    function Get_Mass( this : not null access Solid'Class ) return Float;

    -- The Solid must be ticked after all other components that affect the
    -- entity's position.
    overriding
    function Get_Tick_Order( this : access Solid ) return Natural is (TCK_SOLID);

    -- Returns the component's entity's velocity.
    function Get_V( this : not null access Solid'Class ) return Vec2 with Inline;

    -- Returns the component's entity's size.
    function Get_Height( this : not null access Solid'Class ) return Integer with Inline;
    function Get_Width( this : not null access Solid'Class ) return Integer with Inline;

    -- Returns the component's entity's position.
    function Get_X( this : not null access Solid'Class ) return Float with Inline;
    function Get_Y( this : not null access Solid'Class ) return Float with Inline;

    -- Returns True if the Solid is in contact with another Solid in the given
    -- 'dir'. The contacts cache, populated by Entity_Clipping system, will be
    -- queried.
    function In_Contact( this : not null access Solid'Class; edge : Edge_Type ) return Boolean with Inline;

    -- Returns the solidity of each edge of the entity. If an edge is not solid,
    -- it will not block collisions and other entities can move into it.
    function Is_Top_Solid( this : not null access Solid'Class ) return Boolean with Inline;
    function Is_Bottom_Solid( this : not null access Solid'Class ) return Boolean with Inline;
    function Is_Left_Solid( this : not null access Solid'Class ) return Boolean with Inline;
    function Is_Right_Solid( this : not null access Solid'Class ) return Boolean with Inline;

    -- Iterates over all Solid components currently in contact with edge 'edge'.
    procedure Iterate_Contacts( this    : not null access Solid'Class;
                                edge    : Edge_Type;
                                examine : not null access procedure( contact : A_Solid ) );

    -- Moves the entity, if it's under the effects of gravity, in concert with
    -- another solid entity it may be standing on (a platform).
    procedure Move_With_Platform( this : not null access Solid'Class );

    -- Returns True if the component's entity has moved since the last frame.
    -- This is used to help limit the number of solids checked for collisions.
    function Moved( this : not null access Solid'Class ) return Boolean with Inline;

    -- Sends "HitEntity" messages to all new contacts.
    procedure Send_Collision_Messages( this : not null access Solid'Class );

    -- Sets/clears the solidity of all edges of the entity.
    procedure Set_Solid( this : not null access Solid'Class; allEdges : Boolean );

    -- Sets/clears the solidity of each edge of the entity.
    procedure Set_Top_Solid( this : not null access Solid'Class; edgeSolid : Boolean );
    procedure Set_Bottom_Solid( this : not null access Solid'Class; edgeSolid : Boolean );
    procedure Set_Left_Solid( this : not null access Solid'Class; edgeSolid : Boolean );
    procedure Set_Right_Solid( this : not null access Solid'Class; edgeSolid : Boolean );

    -- Sets the component's entity's velocity.
    procedure Set_V( this : not null access Solid'Class; v : Vec2 ) with Inline;
    procedure Set_VX( this : not null access Solid'Class; vx : Float ) with Inline;
    procedure Set_VY( this : not null access Solid'Class; vy : Float ) with Inline;

    -- Sets the component's entity's position.
    procedure Set_X( this : not null access Solid'Class; x : Float ) with Inline;
    procedure Set_Y( this : not null access Solid'Class; y : Float ) with Inline;
    procedure Set_XY( this : not null access Solid'Class; x, y : Float ) with Inline;

    -- If the component's entity is under the effects of gravity, stick it down
    -- to another entity it might have been standing on, if it drifted apart
    -- from it during the current frame. This is called by the entity clipping
    -- system at the start of each Tick, before the previous contact points are
    -- cleared. If the entity is accelerating upward, it will not stick down.
    procedure Stick_Down_To_Platform( this : not null access Solid'Class );

    -- Checks if the entity's Location component has been updated this frame.
    -- The answer is queried later by the entity clipping system with Moved().
    procedure Tick( this : access Solid; time : Tick_Time );

    function "<"( l, r : A_Solid ) return Boolean is (l.Get_Owner.Get_Id < r.Get_Owner.Get_Id) with Inline;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Earlier collisions have more time since impact.
    function "<"( l, r : Manifold ) return Boolean is
        (l.a.Get_Owner.Get_Id < r.a.Get_Owner.Get_Id or else
        (l.a = r.a and then l.b.Get_Owner.Get_Id < r.b.Get_Owner.Get_Id)) with Inline;

    function "="( l, r : Manifold ) return Boolean is (l.a = r.a and then l.b = r.b) with Inline;

private

    type Constraint is
        record
            obj     : A_Solid := null;
            minDist : Float := 0.0;
        end record;

    function "<"( l, r : Constraint ) return Boolean is (l.obj < r.obj);
    function "="( l, r : Constraint ) return Boolean is (l.obj = r.obj);

    package Contact_Sets is new Ada.Containers.Ordered_Sets( Constraint, "<", "=" );
    type A_Contact_Set is access all Contact_Sets.Set;

    type Contact_Cache is array (Edge_Type) of A_Contact_Set;

    -- Returns the index of the edge with normal vector 'normal'. The vector is
    -- expected to have a magnitude in either the X or Y axis only.
    --     +---1---+
    --     |       |
    --     2       4
    --     |       |
    --     +---3---+
    function To_Edge( normal : Vec2 ) return Edge_Type with Inline;

    type Solid is new Component with
        record
            solidTop    : Boolean := False;
            solidBottom : Boolean := False;
            solidLeft   : Boolean := False;
            solidRight  : Boolean := False;
            mass        : Float := 0.0;

            -- these fields are not streamed --
            loc         : A_Location := null;
            mov         : A_Movement := null;

            locChanged  : Boolean := False;       -- location changed since last tick?
            contacts    : Contact_Cache;          -- contacts on each edge
            oldContacts : Contact_Cache;          -- contacts from previous tick
            updatingContacts : Boolean := False;
        end record;
    for Solid'External_Tag use "Component.Solid";

    overriding
    procedure Delete( this : in out Solid );

    function Object_Input( stream : access Root_Stream_Type'Class ) return Solid;
    for Solid'Input use Object_Input;

    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Solid );
    for Solid'Read use Object_Read;

    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Solid );
    for Solid'Write use Object_Write;

    -- Grabs pointers to the entity's Location and Velocity components.
    procedure On_Added( this : access Solid );

end Entities.Solids;
