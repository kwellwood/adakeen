--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Entities.Factory;                  use Entities.Factory;

package Entities.Map_Clipping.Precise is

    -- A Precise_Clipped component performs precise clipping against map tiles,
    -- preventing any part of an entity's rectangle from entering into a solid
    -- tile or solid area of a slope.

    -- Property      Type    Description
    -- --------      ----    -----------
    -- none

    procedure Define_Component( factory : not null A_Entity_Factory );

    ----------------------------------------------------------------------------

    type Precise_Clipped is new Map_Clipped with private;
    type A_Precise_Clipped is access all Precise_Clipped'Class;

    -- Updates the entity's Location and Velocity components by clipping its
    -- location to solid tiles in the world map.
    procedure Tick( this : access Precise_Clipped; time : Tick_Time );

private

    type Precise_Clipped is new Map_Clipped with null record;
    for Precise_Clipped'External_Tag use "Component.Precise_Clipped";

    function Object_Input( stream : access Root_Stream_Type'Class ) return Precise_Clipped;
    for Precise_Clipped'Input use Object_Input;

    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Precise_Clipped );
    for Precise_Clipped'Read use Object_Read;

    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Precise_Clipped );
    for Precise_Clipped'Write use Object_Write;

end Entities.Map_Clipping.Precise;
