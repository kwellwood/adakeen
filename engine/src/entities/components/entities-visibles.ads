--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Assets.Libraries;                  use Assets.Libraries;
with Entities.Components;               use Entities.Components;
with Entities.Factory;                  use Entities.Factory;

package Entities.Visibles is

    -- Registered Entity Attributes:
    --
    -- Name    Read/Write   Type      Description
    -- ----    ----------   ----      -----------
    -- frame   read/write   number    Tile ID of the current frame
    -- z       read/write   number    Current visual Z depth of the entity
    -- zOrder  read         number    Draw order of the entity within its Z plane

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Property   Type            Description
    -- --------   ----            -----------
    -- lib        string          Name of the tile library containing frames
    -- frame      string/number   Name or id of the initial tile to display
    -- z          number := 0     Visual Z depth of the entity
    -- zOrder     number := 0     Z-order in a shared Z plane (smaller in foreground)
    -- editorOnly boolean         Entity is visible only in the level editor

    procedure Define_Component( factory : not null A_Entity_Factory );

    ----------------------------------------------------------------------------

    type Visible is new Component with private;
    type A_Visible is access all Visible'Class;

    overriding
    function Get_Dependency_Order( this : access Visible ) return Natural is (DEPS_VISIBLE);

    overriding
    function Get_Family( this : access Visible ) return Family_Id is (VISIBLE_ID);

    -- Returns the tile id of the current frame.
    function Get_Frame( this : not null access Visible'Class ) return Natural with Inline;

    -- Returns a reference to the component's tile library.
    function Get_Library( this : not null access Visible'Class ) return Library_Ptr with Inline;

    -- Public values:
    --
    -- lib     (string)
    -- frame   (integer)
    -- z       (float)
    -- zOrder  (integer)
    function Get_Public_Values( this : access Visible ) return Map_Value;

    overriding
    function Get_Tick_Order( this : access Visible ) return Natural is (TCK_VISIBLE);

    -- Returns the Z depth of the entity.
    function Get_Z( this : not null access Visible'Class ) return Float with Inline;

    -- Returns the entity's relative Z-order within its shared Z plane. Smaller
    -- values are in the foreground.
    function Get_ZOrder( this : not null access Visible'Class ) return Integer with Inline;

    -- Sets the visible frame by tile id. A Frame_Changed event will be queued
    -- if the tile library has been loaded.
    procedure Set_Frame( this : not null access Visible'Class; id : Natural ) with Inline;

    -- Sets the visual Z depth of the entity.
    procedure Set_Z( this : not null access Visible'Class; z : Float );

    -- Sets the entity's relative Z-order for drawing within its Z plane.
    -- Smaller values are in the foreground.
    procedure Set_ZOrder( this : not null access Visible'Class; zOrder : Integer );

private

    type Visible is new Component with
        record
            editorOnly : Boolean := False;    -- only visible in the editor
            frame      : Natural := 0;
            libName    : Unbounded_String;    -- tile library name
            z          : Float := 0.0;        -- default is middleground
            zOrder     : Integer := 0;        -- within a shared Z plane

            -- these fields are not streamed --
            lib        : Library_Ptr;
        end record;
    for Visible'External_Tag use "Component.Visible";

    -- Loads the tile library used by this component for its frames.
    --
    -- If the editorOnly field is True then the library will only be loaded if
    -- the application is running within the context of an editor (determined by
    -- "Session.Is_Editor"). This prevents the graphical representation of
    -- game-invisible entities from being loaded anywhere but the editor.
    procedure Load_Library( this : not null access Visible'Class );

    function Object_Input( stream : access Root_Stream_Type'Class ) return Visible;
    for Visible'Input use Object_Input;

    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Visible );
    for Visible'Read use Object_Read;

    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Visible );
    for Visible'Write use Object_Write;

    -- Registers names: frame
    procedure On_Added( this : access Visible );

end Entities.Visibles;
