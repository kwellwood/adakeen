--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Entities.Factory;                  use Entities.Factory;

package Entities.Map_Clipping.Platform is

    -- A Platform_Clipped component performs platformer-style clipping, allowing
    -- entities to more realistically interact with slopes. When walking up a
    -- slope or jumping up and banging into a slope, only the center X point is
    -- clipped, allowing the left and right corners to "dig in" to the ground.

    -- Property      Type    Description
    -- --------      ----    -----------
    -- none

    procedure Define_Component( factory : not null A_Entity_Factory );

    ----------------------------------------------------------------------------

    type Platform_Clipped is new Map_Clipped with private;
    type A_Platform_Clipped is access all Platform_Clipped'Class;

    -- Updates the entity's Location and Velocity components by clipping its
    -- location to solid tiles in the world map.
    procedure Tick( this : access Platform_Clipped; time : Tick_Time );

private

    type Platform_Clipped is new Map_Clipped with null record;
    for Platform_Clipped'External_Tag use "Component.Platform_Clipped";

    function Object_Input( stream : access Root_Stream_Type'Class ) return Platform_Clipped;
    for Platform_Clipped'Input use Object_Input;

    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Platform_Clipped );
    for Platform_Clipped'Read use Object_Read;

    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Platform_Clipped );
    for Platform_Clipped'Write use Object_Write;

end Entities.Map_Clipping.Platform;
