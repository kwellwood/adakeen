--
-- Copyright (c) 2015-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Assets.Scripts;                    use Assets.Scripts;
with Debugging;                         use Debugging;
with Entities.Systems;                  use Entities.Systems;
with Games;                             use Games;
with Scribble;                          use Scribble;
with Scribble.Compilers;                use Scribble.Compilers;
with Script_VMs;                        use Script_VMs;
with Support;                           use Support;
with Values.Construction;               use Values.Construction;
with Values.Errors;                     use Values.Errors;
with Values.Streaming;                  use Values.Streaming;
with Values.Strings;                    use Values.Strings;

package body Entities.Scripts is

    function Create_Script( properties : Map_Value ) return A_Component is
        this : constant A_Script := new Script;
        file : A_Script_Asset := null;
    begin
        this.Construct;
        this.filename := properties.Get_Unbounded_String( "filename" );

        -- load the script file containing members and timers
        if Length( this.filename ) > 0 then
            file := Load_Script( To_String( this.filename ), "scripts" );
        end if;
        if file = null or else not file.Is_Loaded then
            -- no script filename was given or it could not be loaded
            this.names := Create_Map.Map;
            Delete( file );
            return A_Component(this);
        end if;

        -- build the script namespace using members from the file and merging
        -- the "members" property over it.
        this.names := Clone( file.Get_Members ).Map;
        Recursive_Merge( this.names, properties.Get( "members" ) );

        -- the OnTick procedure is cached to avoid the lookup on every frame
        this.tickHandler := this.names.Get( "OnTick" ).Func;

        declare

            -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

            procedure Set_Timer( val : Map_Value ) is
                name   : constant Unbounded_String := val.Get_Unbounded_String( "name" );
                period : Time_Span;
            begin
                -- timer format: {name: <funcName>, period: <ms>, repeat: <bool>, args: <list>}
                period := Milliseconds( val.Get_Int( "period", 0 ) );
                this.timers.Append( Timer'(funcName => name,
                                           handler  => this.names.Get( name ).Func,
                                           period   => period,
                                           expires  => this.Get_Age + period,
                                           repeat   => val.Get_Boolean( "repeat", False ),
                                           args     => Clone( val.Get( "args" ) ).Lst) );
            end Set_Timer;

            -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

            fileTimers : constant List_Value := file.Get_Timers;
            propTimers : constant List_Value := properties.Get( "timers" ).Lst;
        begin
            -- build the script's timers using the "timers" list field from the
            -- file and then appending the "timers" list property's elements to it.
            if fileTimers.Valid then
                for i in 1..fileTimers.Length loop
                    if fileTimers.Get( i ).Is_Map then
                        Set_Timer( fileTimers.Get( i ).Map );
                    else
                        Dbg( "Invalid timer in script '" & To_String( this.filename ) & "'", D_SCRIPT, Warning );
                    end if;
                end loop;
            end if;
            if propTimers.Valid then
                for i in 1..propTimers.Length loop
                    if propTimers.Get( i ).Is_Map then
                        Set_Timer( propTimers.Get( i ).Map );
                    else
                        Dbg( "Invalid timer in properties to script '" & To_String( this.filename ) & "'", D_SCRIPT, Warning );
                    end if;
                end loop;
           end if;
        end;

        Delete( file );
        return A_Component(this);
    end Create_Script;

    ----------------------------------------------------------------------------

    procedure Cancel_Timer( this     : not null access Script'Class;
                            funcName : String ) is
        use Timer_Vectors;
        crsr : Cursor := this.timers.First;
    begin
        while Has_Element( crsr ) loop
            if Element( crsr ).funcName = funcName then
                this.timers.Delete( crsr );
                return;
            end if;
            Next( crsr );
        end loop;
    end Cancel_Timer;

    ----------------------------------------------------------------------------

    procedure Define_Api( this : not null access Script'Class ) is
        compiler : constant A_Scribble_Compiler := this.Get_Owner.Get_System.Get_Game.Get_Script_VM.Get_Compiler;
    begin
        this.names.Set( "entity",      To_Id_Value( this.Get_Owner.Get_Id ),                                          consume => True );

        this.names.Set( "SetTimer",    compiler.Bind( "SetScriptTimer", defaults => (Create( False ), Create_List) ), consume => True );
        this.names.Set( "CancelTimer", compiler.Bind( "CancelScriptTimer" ),                                          consume => True );
    end Define_Api;

    ----------------------------------------------------------------------------

    overriding
    function Get_Namespace_Name( this : access Script;
                                 name : String ) return Value is
    begin
        return this.names.Get( name );
    end Get_Namespace_Name;

    ----------------------------------------------------------------------------

    overriding
    function Get_Namespace_Ref( this : access Script;
                                name : String ) return Value is
    begin
        return this.names.Reference( name, createMissing => True );
    end Get_Namespace_Ref;

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Message( this   : access Script;
                              name   : Hashed_String;
                              params : Map_Value ) is
        handler : Function_Value;
    begin
        -- run the "On" handler for message 'name'.
        declare
            handlerName : constant String := "On" & To_String( name );
        begin
            handler := this.names.Get( handlerName ).Func;
            if handler.Valid then
                this.Run_Handler( handlerName, params, handler );
            end if;
        end;

        -- after executing the standard AttributeChanged method, check the
        -- attribute name and look for a handler for a specific attribute change.
        if name = "AttributeChanged" and then params.Valid then
            declare
                handlerName : constant String := "On" & Capitalize_First( params.Get_String( "name" ) ) & "Changed";
            begin
                handler := this.names.Get( handlerName ).Func;
                if handler.Valid then
                    this.Run_Handler( handlerName, params.Get( "value" ), handler );
                end if;
            end;
        end if;
    end Handle_Message;

    ----------------------------------------------------------------------------

    overriding
    function Object_Input( stream : access Root_Stream_Type'Class ) return Script is
        this : aliased Script;
    begin
        this.Construct;
        Script'Read( stream, this );
        return this;
    end Object_Input;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Script ) is
    begin
        Component'Read( stream, Component(obj) );
        obj.filename := Read_String_16( stream );
        obj.names := Value_Input( stream ).Map;

        -- copy all functions from the script file and component definition (they aren't persisted)
        declare
            procedure Examine( name : String; val : Value ) is
            begin
                if val.Is_Function then
                    obj.names.Set( name, val );
                end if;
            end Examine;

            defProps    : constant Map_Value := Entities.Factory.Global.Get_Definition( To_String( obj.definition ) ).Get_Properties;  -- this is a copy
            defScript   : constant Map_Value := defProps.Get( "members" ).Map;
            file        : A_Script_Asset;
            fileMembers : Map_Value;
        begin
            file := Load_Script( To_String( obj.filename ), "scripts" );
            if file.Is_Loaded then
                fileMembers := Clone( file.Get_Members ).Map;
            end if;
            Delete( file );

            if not fileMembers.Valid then
                fileMembers := Create_Map.Map;
            end if;

            -- override functions in the script file with functions in the definition
            Recursive_Merge( fileMembers, defScript );
            fileMembers.Iterate( Examine'Access );
            obj.tickHandler := fileMembers.Get( "OnTick" ).Func;
        end;

        declare
            timerCount : constant Integer := Integer'Input( stream );
            t          : Timer;
        begin
            for i in 1..timerCount loop
                t.funcName := Read_String_16( stream );
                t.handler  := obj.names.Get( To_String( t.funcName ) ).Func;
                t.period   := Time_Span'Input( stream );
                t.expires  := Time_Span'Input( stream );
                t.repeat   := Boolean'Input( stream );
                t.args     := Value_Input( stream ).Lst;
                obj.timers.Append( t );
            end loop;
        end;
    end Object_Read;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Script ) is
    begin
        Component'Write( stream, Component(obj) );
        Write_String_16( stream, obj.filename );

        -- only persist names with non-function values
        -- function names will be loaded from the script file and component definition
        declare
            storedNames : constant Map_Value := Create_Map.Map;

            procedure Store_Name( name : String; val : Value ) is
            begin
                if not val.Is_Function then
                    storedNames.Set( name, val, consume => True );
                end if;
            end Store_Name;
        begin
            obj.names.Iterate( Store_Name'Access );
            storedNames.Remove( "entity" );      -- remove the auto-generated 'entity' reference

            Value_Output( stream, storedNames );
        end;

        Integer'Output( stream, Integer(obj.timers.Length) );
        for t of obj.timers loop
            Write_String_16( stream, t.funcName );
            Time_Span'Output( stream, t.period );
            Time_Span'Output( stream, t.expires );
            Boolean'Output( stream, t.repeat );
            Value_Output( stream, t.args );
        end loop;
    end Object_Write;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Added( this : access Script ) is
        attributeChanged : Boolean := False;     -- only register as a listener once

        procedure Register_Listener( name : String; val : Value ) is
        begin
            if val.Is_Function
               and then name'Length > 2
               and then name(name'First..name'First+1) = "On"
               and then Is_Upper( name(name'First+2) )
            then
                if name'Length > 8 and then name(name'Last-6..name'Last) = "Changed" then
                    -- "OnXXXChanged" functions all listen for AttributeChanged
                    -- messages but only handle changes in specifically named
                    -- attributes. Make sure the AttributeChanged message is
                    -- only registered once, to prevent duplicate messages.
                    if not attributeChanged then
                        this.Register_Listener( To_Hashed_String( "AttributeChanged" ) );
                        attributeChanged := True;
                    end if;
                else
                    this.Register_Listener( To_Hashed_String( name(name'First+2..name'Last) ) );
                end if;
            end if;
        end Register_Listener;

    begin
        this.names.Iterate( Register_Listener'Access );
    end On_Added;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Enter_System( this : access Script ) is
    begin
        this.Define_Api;
    end On_Enter_System;

    ----------------------------------------------------------------------------

    procedure Run( this     : not null access Script'Class;
                   funcName : String;
                   func     : Functions.Function_Value;
                   args     : Value_Array ) is
        scriptVM  : constant A_Script_VM := this.Get_Owner.Get_System.Get_Game.Get_Script_VM;
        frameName : constant String := this.Get_Owner.Get_Template & "(" & Image( this.Get_Owner.Get_Id ) & ")." &
                                       this.Get_Definition & "(" & Image( this.Get_Id ) & ")." & funcName;
        args2     : Value_Array(1..args'Length+1);
        thread    : A_Thread;
    begin
        if not func.Valid then
            Dbg( "Entity " & this.Get_Owner.To_String & ": " &
                 "Component " & this.Get_Definition & ": " &
                 "Missing function " & funcName & "()",
                 D_SCRIPT, Error );
            return;
        end if;

        thread := Create_Thread;
        thread.Register_Namespace( ANONYMOUS_NAMESPACE, scriptVM.Get_Globals );

        if func.Is_Member then
            -- insert the component id as the implicit 'this' argument
            args2(args2'First) := To_Id_Value( this.id );
            args2(args2'First+1..args2'Last) := args;

            -- may raise Runtime_Error
            thread.Load( func, args2, frameName );
        else
            -- may raise Runtime_Error
            thread.Load( func, args, frameName );
        end if;

        scriptVM.Get_VM.Run( thread, Run_Complete );

        if thread.Is_Errored then
            Dbg( Image( thread.Get_Result ), D_SCRIPT, Error );
            declare
                procedure Examine( str : String ) is
                begin
                    Dbg( "  " & str, D_SCRIPT, Error );
                end Examine;
            begin
                thread.Iterate_Trace( Examine'Access );
            end;
        end if;

        Delete( thread );
    exception
        when others =>
            Delete( thread );
            raise;
    end Run;

    ----------------------------------------------------------------------------

    procedure Run( this     : not null access Script'Class;
                   funcName : String;
                   func     : Functions.Function_Value ) is
        args : Value_Array(1..0);
    begin
        this.Run( funcName, func, args );
    end Run;

    ----------------------------------------------------------------------------

    procedure Run_Handler( this : not null access Script'Class;
                           name : String;
                           args : Value'Class;
                           func : Function_Value ) is
        argCount : Integer;
    begin
        -- don't count implicit 'this' argument
        argCount := (if func.Is_Member then func.Arg_Count - 1 else func.Arg_Count);

        -- Run the handler as one of the following prototypes:
        --   handler()
        --   handler( args : map )
        --   handler( message : string, args : map )
        if argCount = 0 then
            this.Run( name, func );                                     -- ()
        elsif argCount = 1 then
            this.Run( name, func, Value_Array'(1 => Clone( args )) );   -- ( args )
        elsif argCount = 2 then
            this.Run( name, func,
                      Value_Array'(Create( name ), Clone( args )) );    -- ( message : string, args )
        else
            Dbg( "Entity " & this.Get_Owner.To_String & ": " &
                 "Component " & this.Get_Definition & ": " &
                 "Invalid prototype of message handler " & name & "()",
                 D_SCRIPT, Error );
        end if;
    exception
        when e : others =>
            Dbg( "Entity " & this.Get_Owner.To_String & ": " &
                 "Component " & this.Get_Definition & ": " &
                 "Exception in message handler " & name & "(): ...",
                 D_SCRIPT, Error );
            Dbg( e );
    end Run_Handler;

    ----------------------------------------------------------------------------

    procedure Set_Timer( this     : not null access Script'Class;
                         funcName : String;
                         waitTime : Time_Span;
                         repeat   : Boolean;
                         args     : List_Value ) is
    begin
        for t of this.timers loop
            if funcName = t.funcName then
                -- update the existing timer that will call 'funcName'
                t.period := waitTime;
                t.expires := this.Get_Age + waitTime;
                t.repeat := repeat;
                t.args := Clone( args ).Lst;
                return;
            end if;
        end loop;

        -- add a new timer
        this.timers.Append( Timer'(funcName => To_Unbounded_String( funcName ),
                                   handler  => this.names.Get( funcName ).Func,
                                   period   => waitTime,
                                   expires  => this.Get_Age + waitTime,
                                   repeat   => repeat,
                                   args     => Clone( args ).Lst) );
    end Set_Timer;

    ----------------------------------------------------------------------------

    overriding
    procedure Tick( this : access Script; time : Tick_Time ) is
        timers   : Timer_Vectors.Vector;
        argCount : Integer;
    begin
        -- run expired timers' handlers
        -- iterate over a copy because this.timers can change
        timers := this.timers.Copy;
        for t of timers loop
            if t.expires <= this.Get_Age then
                if t.args.Valid then
                    declare
                        args : Value_Array(1..t.args.Length);
                    begin
                        -- call the timer handler with arguments
                        for i in args'Range loop
                            args(i) := t.args.Get( i );
                        end loop;
                        this.Run( To_String( t.funcName ), t.handler, args );
                    end;
                else
                    -- call the timer handler with no arguments
                    this.Run( To_String( t.funcName ), t.handler );
                end if;
            end if;
        end loop;

        -- reset or cancel expired timers
        for t of this.timers loop
            if t.expires <= this.Get_Age then
                if t.repeat then
                    t.expires := t.expires + t.period;   -- reset the timer
                else
                    t.expires := Time_Span_First;        -- cancel the timer
                end if;
            end if;
        end loop;

        -- delete cancelled timers
        declare
            i : Integer := 1;
        begin
            while i <= Integer(this.timers.Length) loop
                if this.timers.Element( i ).expires = Time_Span_First then
                    this.timers.Delete( i );
                else
                    i := i + 1;
                end if;
            end loop;
        end;

        -- don't tick entities while in the editor
        if not this.In_Editor then
            if this.tickHandler.Valid then
                argCount := (if this.tickHandler.Is_Member then this.tickHandler.Arg_Count - 1   -- don't count implicit 'this' argument
                             else this.tickHandler.Arg_Count);
                if argCount = 0 then
                    this.Run( "OnTick", this.tickHandler );
                else
                    -- pass in the number of milliseconds elapsed since the previous
                    -- tick (fractional precision in nanoseconds).
                    this.Run( "OnTick", this.tickHandler, Value_Array'(1 => Create( Long_Float(time.elapsed / Nanoseconds( 1 )) / 1_000_000.0 )) );
                end if;
            end if;
        end if;
    end Tick;

    --==========================================================================

    procedure Define_Component( factory : not null A_Entity_Factory ) is
    begin
        factory.Define_Component( Family_Name( SCRIPT_ID ),
                                  Create_Script'Access,
                                  propNames =>
                                      (Create( "filename" ),
                                       Create( "members"  ),
                                       Create( "timers"   )) );
    end Define_Component;

end Entities.Scripts;
