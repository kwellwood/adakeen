--
-- Copyright (c) 2015-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Entities.Components;               use Entities.Components;
with Entities.Factory;                  use Entities.Factory;

package Entities.Emitters is

    EMITTER_ID : Family_Id := To_Family_Id( "Emitter" );

    -- Property    Type        Description
    -- --------    ----        -----------
    -- emitter     string/map  Emitter name or description (Required)
    -- rate        number      Emission rate in particles / second (Required)
    -- x, y        number      Location of emitter (relative, if attached) (default=0,0)
    -- z           number      Visual Z depth of the particles (relative, if attached) (default=0)
    -- attached    boolean     Move with parent entity? (default=true)

    procedure Define_Component( factory : not null A_Entity_Factory );

    ----------------------------------------------------------------------------

    type Emitter is new Component with private;
    type A_Emitter is access all Emitter'Class;

    overriding
    function Allow_Multiple( this : access Emitter ) return Boolean is (True);

    overriding
    function Get_Dependency_Order( this : access Emitter ) return Natural is (DEPS_EMITTER);

    overriding
    function Get_Family( this : access Emitter ) return Family_Id is (EMITTER_ID);

    overriding
    function Get_Tick_Order( this : access Emitter ) return Natural is (TCK_EMITTER);

private

    type Emitter is new Component with
        record
            emitter  : Value;            -- map or string
            x, y     : Float := 0.0;
            z        : Float := 0.0;
            rate     : Integer := 0;
            attached : Boolean := True;
        end record;
    for Emitter'External_Tag use "Component.Emitter";

    procedure Handle_Message( this   : access Emitter;
                              name   : Hashed_String;
                              params : Map_Value );

    function Object_Input( stream : access Root_Stream_Type'Class ) return Emitter;
    for Emitter'Input use Object_Input;

    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Emitter );
    for Emitter'Read use Object_Read;

    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Emitter );
    for Emitter'Write use Object_Write;

    procedure On_Added( this : access Emitter );

end Entities.Emitters;
