--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Entities.Components;               use Entities.Components;
with Entities.Factory;                  use Entities.Factory;
with Tiles;                             use Tiles;
with Values.Lists;                      use Values.Lists;

package Entities.Animations is

    ANIMATION_ID : Family_Id := To_Family_Id( "Animation" );

    -- Property   Type      Description
    -- --------   ----      -----------
    -- frames     list      Tile ids of animation frames
    -- delay      number    Time between animation frames (milliseconds) or,
    -- duration   number    Time the animation should last (milliseconds)
    -- looped     boolean   Is the animation looped?
    -- global     boolean   If looped, is the frame based on the global
    --                         timer? (true) or the entity's age? (false, default)

    procedure Define_Component( factory : not null A_Entity_Factory );

    ----------------------------------------------------------------------------

    type Animation is new Component with private;
    type A_Animation is access all Animation'Class;

    overriding
    function Get_Dependency_Order( this : access Animation ) return Natural is (DEPS_ANIMATION);

    overriding
    function Get_Family( this : access Animation ) return Family_Id is (ANIMATION_ID);

    overriding
    function Get_Tick_Order( this : access Animation ) return Natural is (TCK_ANIMATION);

    -- Updates the entity's frame (in its visible component).
    procedure Tick( this : access Animation; time : Tick_Time );

private

    type Animation is new Component with
        record
            currentFrame : Integer := -1;
            anmStart     : Time_Span := Time_Span_Zero;
            frames       : List_Value;
            nextFrame    : Integer := -1;
            frameDelay   : Time_Span := Time_Span_Zero;
            looped       : Boolean := False;
            global       : Boolean := False;

            -- these fields are not streamed --
            frameIds     : A_Tile_Id_Array := null;
        end record;
    for Animation'External_Tag use "Component.Animation";

    procedure Delete( this : in out Animation );

    -- Calculates and returns what the current frame should be, based on the
    -- stored animation info. The component must have an owner.
    function Calculate_Frame( this : not null access Animation'Class ) return Natural;

    -- Attempts to read animation info from a tile's attributes. This is called
    -- when no animation is given on construction and when the entity's frame is
    -- changed by another component.
    procedure Get_Anm_Info( this : not null access Animation'Class;
                            tile : A_Tile );

    function Object_Input( stream : access Root_Stream_Type'Class ) return Animation;
    for Animation'Input use Object_Input;

    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Animation );
    for Animation'Read use Object_Read;

    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Animation );
    for Animation'Write use Object_Write;

    -- Resolves the tile ids of the frames in the animation using the tile
    -- library of the Visible component.
    procedure On_Added( this : access Animation );

    -- Sets the entity's current frame. A copy of the frame is kept in
    -- .currentFrame to determine when another component changes the frame.
    procedure Set_Frame( this : not null access Animation; id : Natural );

end Entities.Animations;

