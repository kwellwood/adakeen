--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Directions;                        use Directions;
with Entities.Components;               use Entities.Components;
with Entities.Factory;                  use Entities.Factory;
with Vector_Math;                       use Vector_Math;

package Entities.Movements is

    -- A positive acceleration value that acts as an impulse, causing an
    -- immediate change in velocity. Accelerations greater than IMPULSE or less
    -- than -IMPULSE will have the same effect.
    IMPULSE : constant Float;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Registered Entity Attributes:
    --
    -- Name         Read/Write    Type      Description
    -- ----         ----------    ----      -----------
    -- vx           read/write    number    Velocity X axis
    -- vy           read/write    number    Velocity Y axis
    -- frictionX    read/write    number    Frictional acceleration X axis
    -- frictionY    read/write    number    Frictional acceleration Y axis
    -- gravity      read/write    number    Gravitational acceleration

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    MOVEMENT_ID : Family_Id := To_Family_Id( "Movement" );

    -- A Movement component can be parameterized with velocities: initial and
    -- maximum (pixels/sec), and accelerations: gravity and friction
    -- (pixels/sec^2). If no maximum velocity is specified, it will be unlimited
    -- but a limit is highly recommended if clipping will be applied. All
    -- defaults are 0.

    --
    -- Property        Type      Description
    -- --------        ----      -----------
    -- vx, vy          number    Initial velocity
    -- vxMax, vyMax    number    Maximum velocity of the entity
    -- gravity         number    Gravitational acceleration
    -- frictionX       number    Frictional acceleration X axis
    -- frictionY       number    Frictional acceleration Y axis

    procedure Define_Component( factory : not null A_Entity_Factory );

    ----------------------------------------------------------------------------

    type Movement is new Component with private;
    type A_Movement is access all Movement'Class;

    overriding
    function Get_Dependency_Order( this : access Movement ) return Natural is (DEPS_MOVEMENT);

    overriding
    function Get_Family( this : access Movement ) return Family_Id is (MOVEMENT_ID);

    function Get_F( this : not null access Movement'Class ) return Vec2 with Inline;

    function Get_F_Initial( this : not null access Movement'Class ) return Vec2 with Inline;

    function Get_Gravity( this : not null access Movement'Class ) return Float with Inline;

    function Get_Gravity_Initial( this : not null access Movement'Class ) return Float with Inline;

    overriding
    function Get_Tick_Order( this : access Movement ) return Natural is (TCK_MOVEMENT);

    -- Returns the entity's velocity.
    function Get_V( this : not null access Movement'Class ) return Vec2 with Inline;

    -- Returns the entity's target velocity that it's accelerating toward.
    function Get_V_Target( this : not null access Movement'Class ) return Vec2 with Inline;

    -- Returns True if the target velocity is in the direction of 'dir'. For
    -- example, if the target velocity X component is negative then
    -- Is_Moving( Left ) would return True and Is_Moving( Right ) would return
    -- False. If the target velocity Y component is 0, both Is_Moving( Up ) and
    -- Is_Moving( down ) would return False.
    --
    -- NOTE: The result does not depend on the actual velocity or acceleration!
    -- It depends only on the target velocity.
    function Is_Moving( this : not null access Movement'Class;
                        dir  : Cardinal_Direction ) return Boolean with Inline;

    -- Returns True if the target velocity is non-zero, indicating a target
    -- velocity has been set in some direction.
    function Is_Moving( this : not null access Movement'Class ) return Boolean with Inline;

    procedure Set_F( this : not null access Movement'Class; friction : Vec2 ) with Inline;
    procedure Set_FX( this : not null access Movement'Class; fx : Float ) with Inline;
    procedure Set_FY( this : not null access Movement'Class; fy : Float ) with Inline;

    procedure Set_Gravity( this : not null access Movement'Class; gravity : Float ) with Inline;

    -- Sets the entity's velocity. This will not change the target velocity, if
    -- one has been set, so it may continue accelerating to a different
    -- velocity.
    procedure Set_V( this : not null access Movement'Class; v : Vec2 ) with Inline;
    procedure Set_VX( this : not null access Movement'Class; vx : Float ) with Inline;
    procedure Set_VY( this : not null access Movement'Class; vy : Float ) with Inline;

    -- Sets the target velocity (in both the X and Y axes). The entity will
    -- accelerate to 'target' velocity at a rate of 'acc' (pixels/sec^2). If the
    -- acceleration is IMPULSE, the velocity will change immediately.
    procedure Set_V_Target( this   : not null access Movement'Class;
                            target : Vec2;
                            acc    : Vec2 );
    pragma Precondition( acc.x >= 0.0 and then acc.y >= 0.0 );

    -- Sets the target velocity in the X axis only. The entity will accelerate
    -- to 'target' velocity at a rate of 'acc' (pixels/sec^2). If the
    -- acceleration is IMPULSE, the velocity will change immediately.
    procedure Set_VX_Target( this   : not null access Movement'Class;
                             target : Float;
                             acc    : Float );
    pragma Precondition( acc >= 0.0 );

    -- Sets the target velocity in the Y axis only. The entity will accelerate
    -- to 'target' velocity at a rate of 'acc' (pixels/sec^2). If the
    -- acceleration is IMPULSE, the velocity will change immediately.
    procedure Set_VY_Target( this   : not null access Movement'Class;
                             target : Float;
                             acc    : Float );
    pragma Precondition( acc >= 0.0 );

    -- Integrates acceleration into velocity, and velocity into position.
    procedure Tick( this : access Movement; time : Tick_Time );

private

    IMPULSE : constant Float := 100_000.0;

    type Movement is new Component with
        record
            velocity    : Vec2;
            vTarget     : Vec2;
            vMax        : Vec2;
            acc         : Vec2;
            gravInitial : Float := 0.0;
            fricInitial : Vec2;
            gravity     : Float := 0.0;
            friction    : Vec2;
        end record;
    for Movement'External_Tag use "Component.Movement";

    function Object_Input( stream : access Root_Stream_Type'Class ) return Movement;
    for Movement'Input use Object_Input;

    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Movement );
    for Movement'Read use Object_Read;

    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Movement );
    for Movement'Write use Object_Write;

    -- Registers names: vx, vy, gravity, frictionX, frictionY
    procedure On_Added( this : access Movement );

end Entities.Movements;

