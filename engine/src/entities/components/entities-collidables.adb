--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Entities.Systems;                  use Entities.Systems;
with Values.Lists;                      use Values.Lists;
with Values.Strings;                    use Values.Strings;
with Vector_Math;                       use Vector_Math;

package body Entities.Collidables is

    function Create_Collidable( properties : Map_Value ) return A_Component is
        pragma Unreferenced( properties );
        this : constant A_Collidable := new Collidable;
    begin
        this.Construct;
        return A_Component(this);
    end Create_Collidable;

    ----------------------------------------------------------------------------

    function Detect_Collision( this  : not null access Collidable'Class;
                               other : not null A_Collidable ) return Boolean
    is (Intersect( this.loc.Get_Bounds, other.loc.Get_Bounds ));

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Message( this   : access Collidable;
                              name   : Hashed_String;
                              params : Map_Value ) is
        pragma Unreferenced( params );

        procedure Clear_Touching_List is
            sys  : constant A_Entity_System := this.Get_Owner.Get_System;
            e    : A_Entity;
            comp : A_Collidable;
        begin
            -- Sends "Separated" messages to all touched entities to avoid out-
            -- of-date entity id references in other entities' touching lists.
            if sys /= null then
                for id of this.touching loop
                    e := sys.Get_Entity( id );
                    if e /= null then
                        comp := A_Collidable(e.Get_Component( COLLIDABLE_ID ));
                        if comp /= null then
                            comp.Set_Touching( this.Get_Owner.Get_Id, touching => False );
                        end if;
                    end if;
                end loop;
            end if;
            this.touching.Clear;
        end Clear_Touching_List;

    begin
        if name = MSG_Destruct then
            Clear_Touching_List;
        end if;
    end Handle_Message;

    ----------------------------------------------------------------------------

    function Is_Changed( this : not null access Collidable'Class ) return Boolean is (this.locChanged);

    ----------------------------------------------------------------------------

    function Is_Touching( this : not null access Collidable'Class;
                          id   : Entity_Id ) return Boolean is (this.touching.Contains( id ));

    ----------------------------------------------------------------------------

    procedure Iterate_Touching( this    : not null access Collidable'Class;
                                examine : not null access procedure( e : A_Entity ) ) is
        e : A_Entity;
    begin
        for id of this.touching loop
            e := this.Get_Owner.Get_System.Get_Entity( id );
            pragma Assert( e /= null, "Deleted entity id found in touch list" );
            examine.all( e );
        end loop;
    end Iterate_Touching;

    ----------------------------------------------------------------------------

    overriding
    function Object_Input( stream : access Root_Stream_Type'Class ) return Collidable is
        this : aliased Collidable;
    begin
        this.Construct;
        Collidable'Read( stream, this );
        return this;
    end Object_Input;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Collidable ) is
    begin
        Component'Read( stream, Component(obj) );
    end Object_Read;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Collidable ) is
    begin
        Component'Write( stream, Component(obj) );
    end Object_Write;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Added( this : access Collidable ) is
    begin
        this.loc := A_Location(this.Get_Owner.Get_Component( LOCATION_ID ));
        pragma Assert( this.loc /= null, "Collidable component depends on Location component" );

        this.Register_Listener( MSG_Destruct );
    end On_Added;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Removed( this : access Collidable ) is
    begin
        this.touching.Clear;
        this.loc := null;
    end On_Removed;

    ----------------------------------------------------------------------------

    procedure Set_Touching( this     : not null access Collidable'Class;
                            id       : Entity_Id;
                            touching : Boolean ) is
        idParams : constant Map_Value := Create_Map( (1=>Pair("eid", To_Id_Value( id ))) ).Map;
        crsr     : Id_Sets.Cursor;
    begin
        if touching then
            this.touching.Include( id );
            -- don't send entity collision messages in the editor
            if not this.In_Editor then
                this.Get_Owner.Dispatch_Message( MSG_Collided, idParams );
            end if;
        else
            crsr := this.touching.Find( id );
            if Id_Sets.Has_Element( crsr ) then
                if not this.In_Editor then
                    this.Get_Owner.Dispatch_Message( MSG_Separated, idParams );
                end if;
                this.touching.Delete( crsr );
            end if;
        end if;
    end Set_Touching;

    ----------------------------------------------------------------------------

    procedure Tick( this : access Collidable; time : Tick_Time ) is
        pragma Unreferenced( time );
    begin
        -- this.brandNew is used for a special case where the collidable always
        -- needs to be checked against others on the first tick even though it
        -- may not have moved yet.
        this.locChanged := this.brandNew or this.loc.Is_Moved or this.loc.Is_Resized;
        this.brandNew := False;
    end Tick;

    --==========================================================================

    procedure Define_Component( factory : not null A_Entity_Factory ) is
    begin
        factory.Define_Component( Family_Name( COLLIDABLE_ID ),
                                  Create_Collidable'Access,
                                  required =>
                                      (1 => Create( Family_Name( LOCATION_ID ) )) );
    end Define_Component;

end Entities.Collidables;
