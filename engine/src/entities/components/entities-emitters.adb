--
-- Copyright (c) 2015-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Entities;
with Entities.Locations;                use Entities.Locations;
with Entities.Visibles;                 use Entities.Visibles;
with Events.Particles;                  use Events.Particles;
with Values.Construction;               use Values.Construction;
with Values.Strings;                    use Values.Strings;
with Values.Streaming;                  use Values.Streaming;
with Worlds;                            use Worlds;

package body Entities.Emitters is

    function Create_Emitter( properties : Map_Value ) return A_Component is
        this : constant A_Emitter := new Emitter;
    begin
        this.Construct;
        this.emitter  := Clone( properties.Get( "emitter" ) );
        this.x        := properties.Get_Float( "x", 0.0 );
        this.y        := properties.Get_Float( "y", 0.0 );
        this.z        := properties.Get_Float( "z", 0.0 );
        this.rate     := properties.Get_Int( "rate", 0 );
        this.attached := properties.Get_Boolean( "attached", True );
        return A_Component(this);
    end Create_Emitter ;

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Message( this   : access Emitter;
                              name   : Hashed_String;
                              params : Map_Value ) is
        pragma Unreferenced( params );
        loc  : A_Location;
        vis  : A_Visible;
        eid  : Entity_Id := NULL_ID;
        x    : Float := this.x;
        y    : Float := this.y;
        z    : Float := this.z;
    begin
        if name = MSG_WorldLoaded or else name = MSG_Spawned then
            if this.attached then
                eid := this.owner.Get_Id;
                loc := A_Location(this.Get_Component( LOCATION_ID ));
                if loc /= null then
                    x := x + loc.Get_X;
                    y := y + loc.Get_Y;
                end if;
                vis := A_Visible(this.Get_Component( VISIBLE_ID ));
                if vis /= null then
                    z := z + vis.Get_Z;
                end if;
            end if;
            Queue_Start_Particles( this.id, this.emitter, x, y, z, this.rate, eid );

        elsif name = MSG_WorldUnloaded or else name = MSG_Destruct then
            Queue_Stop_Particles( this.id );
        end if;
    end Handle_Message;

    ----------------------------------------------------------------------------

    overriding
    function Object_Input( stream : access Root_Stream_Type'Class ) return Emitter is
        this : aliased Emitter;
    begin
        this.Construct;
        Emitter'Read( stream, this );
        return this;
    end Object_Input;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Emitter ) is
    begin
        Component'Read( stream, Component(obj) );
        obj.emitter := Value_Input( stream );
        obj.x := Float'Input( stream );
        obj.y := Float'Input( stream );
        obj.z := Float'Input( stream );
        obj.rate := Integer'Input( stream );
        obj.attached := Boolean'Input( stream );
    end Object_Read;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Emitter ) is
    begin
        Component'Write( stream, Component(obj) );
        Value_Output( stream, obj.emitter );
        Float'Output( stream, obj.x );
        Float'Output( stream, obj.y );
        Float'Output( stream, obj.z );
        Integer'Output( stream, obj.rate );
        Boolean'Output( stream, obj.attached );
    end Object_Write;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Added( this : access Emitter ) is
    begin
        this.Register_Listener( MSG_WorldLoaded );
        this.Register_Listener( MSG_WorldUnloaded );
        this.Register_Listener( MSG_Spawned );
        this.Register_Listener( MSG_Destruct );
    end On_Added;

    --==========================================================================

    procedure Define_Component( factory : not null A_Entity_Factory ) is
    begin
        factory.Define_Component( Family_Name( EMITTER_ID ),
                                  Create_Emitter'Access,
                                  propNames =>
                                      (Create( "emitter"  ),
                                       Create( "rate"     ),
                                       Create( "z"        ),
                                       Create( "x"        ),
                                       Create( "y"        ),
                                       Create( "attached" )) );
    end Define_Component;

end Entities.Emitters;

