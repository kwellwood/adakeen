--
-- Copyright (c) 2014-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Entities.Components;               use Entities.Components;
with Entities.Factory;                  use Entities.Factory;

package Entities.Camera_Targets is

    CAMERA_TARGET_ID : Family_Id := To_Family_Id( "CameraTarget" );

    -- Property   Type      Description
    -- --------   ----      -----------
    -- enabled    boolean   Camera should follow this target (default = true)
    -- top        number    Top edge of the bounding box, relative to camera's center (default = 0)
    -- bottom     number    Bottom edge of the bounding box (default = 0)
    -- left       number    Left edge of the bounding box (default = 0)
    -- right      number    Right edge of the bounding box (default = 0)

    procedure Define_Component( factory : not null A_Entity_Factory );

    type Bounds_Type is
        record
            top, bottom, left, right : Integer := 0;
        end record;

    ----------------------------------------------------------------------------

    type Camera_Target is new Component with private;
    type A_Camera_Target is access all Camera_Target'Class;

    procedure Center( this : not null access Camera_Target'Class );

    procedure Enable( this    : not null access Camera_Target'Class;
                      enabled : Boolean );

    overriding
    function Get_Dependency_Order( this : access Camera_Target ) return Natural is (DEPS_CAMERA_TARGET);

    overriding
    function Get_Family( this : access Camera_Target ) return Family_Id is (CAMERA_TARGET_ID);

    overriding
    function Get_Tick_Order( this : access Camera_Target ) return Natural is (TCK_CAMERA_TARGET);

    function Is_Enabled( this : not null access Camera_Target'Class ) return Boolean;

    procedure Set_Bounds( this : not null access Camera_Target'Class; bounds : Bounds_Type );

private

    type Camera_Target is new Component with
        record
            enabled : Boolean := True;
            bounds  : Bounds_Type;
        end record;
    for Camera_Target'External_Tag use "Component.Camera_Target";

    procedure Handle_Message( this   : access Camera_Target;
                              name   : Hashed_String;
                              params : Map_Value );

    function Object_Input( stream : access Root_Stream_Type'Class ) return Camera_Target;
    for Camera_Target'Input use Object_Input;

    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Camera_Target );
    for Camera_Target'Read use Object_Read;

    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Camera_Target );
    for Camera_Target'Write use Object_Write;

    procedure On_Added( this : access Camera_Target );

end Entities.Camera_Targets;
