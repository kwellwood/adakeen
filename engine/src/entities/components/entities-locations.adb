--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Entities;
with Values.Casting;                    use Values.Casting;
with Values.Construction;               use Values.Construction;
with Values.Indirects;                  use Values.Indirects;
with Values.Strings;                    use Values.Strings;

package body Entities.Locations is

    package Location_Refs is new Values.Indirects.Generics( Location );

    ----------------------------------------------------------------------------

    function Get_Bottom( this : not null access Location'Class ) return Value is (Create( this.Get_Bottom ));

    function Get_Height( this : not null access Location'Class ) return Value is (Create( this.Get_Height ));

    function Get_Left( this : not null access Location'Class ) return Value is (Create( this.Get_Left ));

    function Get_Right( this : not null access Location'Class ) return Value is (Create( this.Get_Right ));

    function Get_Top( this : not null access Location'Class ) return Value is (Create( this.Get_Top ));

    function Get_Width( this : not null access Location'Class ) return Value is (Create( this.Get_Width ));

    function Get_X( this : not null access Location'Class ) return Value is (Create( this.Get_X ));

    function Get_Y( this : not null access Location'Class ) return Value is (Create( this.Get_Y ));

    ----------------------------------------------------------------------------

    procedure Set_Height( this : not null access Location'Class; height : Value'Class ) is
    begin
        this.Set_Size( this.width, Cast_Int( height, this.height ) );
    end Set_Height;

    procedure Set_Width( this : not null access Location'Class; width : Value'Class ) is
    begin
        this.Set_Size( Cast_Int( width, this.width ), this.height );
    end Set_Width;

    procedure Set_X( this : not null access Location'Class; x : Value'Class ) is
    begin
        this.Set_X( Cast_Float( x, this.x ) );
    end Set_X;

    procedure Set_Y( this : not null access Location'Class; y : Value'Class ) is
    begin
        this.Set_Y( Cast_Float( y, this.y ) );
    end Set_Y;

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Location( properties : Map_Value ) return A_Component is
        this : constant A_Location := new Location;
    begin
        this.Construct;

        this.x := properties.Get_Float( "x", 0.0 );
        this.y := properties.Get_Float( "y", 0.0 );
        this.width  := properties.Get_Int( "width", 0 );
        this.height := properties.Get_Int( "height", 0 );

        this.prevX := this.x;
        this.prevY := this.y;
        this.prevWidth := this.width;
        this.prevHeight := this.height;

        this.minX := this.x;
        this.maxX := this.x;
        this.minY := this.y;
        this.maxY := this.y;

        return A_Component(this);
    end Create_Location;

    ----------------------------------------------------------------------------

    procedure Add_X( this : not null access Location'Class; distance : Float ) is
    begin
        this.x := this.x + distance;
    end Add_X;

    ----------------------------------------------------------------------------

    procedure Add_Y( this : not null access Location'Class; distance : Float ) is
    begin
        this.y := this.y + distance;
    end Add_Y;

    ----------------------------------------------------------------------------

    procedure Add_XY( this : not null access Location'Class; dX, dY : Float ) is
    begin
        this.x := this.x + dX;
        this.y := this.y + dY;
    end Add_XY;

    ----------------------------------------------------------------------------

    function Get_Bounds( this : not null access Location'Class ) return Rectangle
    is (Rectangle'(this.x - Float(this.width) / 2.0, this.y - Float(this.height) / 2.0, Float(this.width), Float(this.height)));

    function Get_X( this : not null access Location'Class ) return Float is (this.x);

    function Get_Y( this : not null access Location'Class ) return Float is (this.y);

    function Get_Height( this : not null access Location'Class ) return Natural is (this.height);

    function Get_Width( this : not null access Location'Class ) return Natural is (this.width);

    function Get_Left( this : not null access Location'Class ) return Float is (this.x - Float(this.width) / 2.0);

    function Get_Right( this : not null access Location'Class ) return Float is (this.x + Float(this.width) / 2.0);

    function Get_Top( this : not null access Location'Class ) return Float is (this.y - Float(this.height) / 2.0);

    function Get_Top_Max_Y( this : not null access Location'Class ) return Float is (this.maxY - Float(this.height) / 2.0);

    function Get_Bottom( this : not null access Location'Class ) return Float is (this.y + Float(this.height) / 2.0);

    function Get_Bottom_Min_Y( this : not null access Location'Class ) return Float is (this.minY + Float(this.height) / 2.0);

    function Get_Max_X( this : not null access Location'Class ) return Float is (this.maxX);

    function Get_Max_Y( this : not null access Location'Class ) return Float is (this.maxY);

    function Get_Min_X( this : not null access Location'Class ) return Float is (this.minX);

    function Get_Min_Y( this : not null access Location'Class ) return Float is (this.minY);

    function Get_Prev_X( this : not null access Location'Class ) return Float is (this.prevX);

    function Get_Prev_Y( this : not null access Location'Class ) return Float is (this.prevY);

    function Get_Prev_Width( this : not null access Location'Class ) return Natural is (this.prevWidth);

    function Get_Prev_Height( this : not null access Location'Class ) return Natural is (this.prevHeight);

    function Is_Moved( this : not null access Location'Class ) return Boolean
    is (this.x /= this.prevX or else this.y /= this.prevY);

    function Is_Resized( this : not null access Location'Class ) return Boolean
    is (this.width /= this.prevWidth or else this.height /= this.prevHeight);

    ----------------------------------------------------------------------------

    overriding
    function Get_Public_Values( this : access Location ) return Map_Value is
        state : constant Map_Value := Create_Map.Map;
    begin
        state.Set( "x", Create( this.x ) );
        state.Set( "y", Create( this.y ) );
        state.Set( "width", Create( this.width ) );
        state.Set( "height", Create( this.height ) );
        return state;
    end Get_Public_Values;

    ----------------------------------------------------------------------------

    procedure Move( this         : not null access Location'Class;
                    xDist, yDist : Float ) is
    begin
        this.x := this.x + xDist;
        this.y := this.y + yDist;
    end Move;

    ----------------------------------------------------------------------------

    function Moved_Last_Frame( this : not null access Location'Class ) return Boolean is (this.movedLastFrame);

    ----------------------------------------------------------------------------

    overriding
    function Object_Input( stream : access Root_Stream_Type'Class ) return Location is
        this : aliased Location;
    begin
        this.Construct;
        Location'Read( stream, this );
        return this;
    end Object_Input;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Location ) is
    begin
        Component'Read( stream, Component(obj) );
        obj.x := Float'Input( stream );
        obj.y := Float'Input( stream );
        obj.width := Natural'Input( stream );
        obj.height := Natural'Input( stream );
        obj.prevX := Float'Input( stream );
        obj.prevY := Float'Input( stream );
        obj.prevWidth := Natural'Input( stream );
        obj.prevHeight := Natural'Input( stream );
        obj.minX := Float'Input( stream );
        obj.maxX := Float'Input( stream );
        obj.minY := Float'Input( stream );
        obj.maxY := Float'Input( stream );
    end Object_Read;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Location ) is
    begin
        Component'Write( stream, Component(obj) );
        Float'Output( stream, obj.x );
        Float'Output( stream, obj.y );
        Natural'Output( stream, obj.width );
        Natural'Output( stream, obj.height );
        Float'Output( stream, obj.prevX );
        Float'Output( stream, obj.prevY );
        Natural'Output( stream, obj.prevWidth );
        Natural'Output( stream, obj.prevHeight );
        Float'Output( stream, obj.minX );
        Float'Output( stream, obj.maxX );
        Float'Output( stream, obj.minY );
        Float'Output( stream, obj.maxY );
    end Object_Write;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Added( this : access Location ) is
    begin
        -- for performance reasons, the entity is not notified of changes to x,y
        this.Register_Entity_Attribute( "x",      Location_Refs.Create_Indirect( this, Get_X'Access,      Set_X'Access      ) );
        this.Register_Entity_Attribute( "y",      Location_Refs.Create_Indirect( this, Get_Y'Access,      Set_Y'Access      ) );
        this.Register_Entity_Attribute( "width",  Location_Refs.Create_Indirect( this, Get_Width'Access,  Set_Width'Access  ) );
        this.Register_Entity_Attribute( "height", Location_Refs.Create_Indirect( this, Get_Height'Access, Set_Height'Access ) );
        this.Register_Entity_Attribute( "left",   Location_Refs.Create_Indirect( this, Get_Left'Access,   null              ) );
        this.Register_Entity_Attribute( "right",  Location_Refs.Create_Indirect( this, Get_Right'Access,  null              ) );
        this.Register_Entity_Attribute( "top",    Location_Refs.Create_Indirect( this, Get_Top'Access,    null              ) );
        this.Register_Entity_Attribute( "bottom", Location_Refs.Create_Indirect( this, Get_Bottom'Access, null              ) );
    end On_Added;

    ----------------------------------------------------------------------------

    procedure Set_Size( this          : not null access Location'Class;
                        width, height : Natural ) is
    begin
        this.width := width;
        this.height := height;

        -- notify the other components that the entity size changed
        if this.owner /= null then
            this.Get_Owner.Dispatch_Message( MSG_Resized,
                                             Create( (Pair( "width", Create( this.width ) ),
                                                      Pair( "height", Create( this.height ) )),
                                                      consume => True ).Map );
        end if;
    end Set_Size;

    ----------------------------------------------------------------------------

    procedure Set_X( this : not null access Location'Class; x : Float ) is
    begin
        this.x := x;
    end Set_X;

    ----------------------------------------------------------------------------

    procedure Set_Y( this : not null access Location'Class; y : Float ) is
    begin
        this.y := y;
    end Set_Y;

    ----------------------------------------------------------------------------

    procedure Set_XY( this : not null access Location'Class; x, y : Float ) is
    begin
        this.x := x;
        this.y := y;
    end Set_XY;

    ----------------------------------------------------------------------------

    procedure Update_Previous_State( this : not null access Location'Class ) is
    begin
        if this.x > this.prevX then
            this.maxX := this.x;
        elsif this.x < this.prevX then
            this.minX := this.x;
        end if;
        if this.y > this.prevY then
            this.maxY := this.y;
        elsif this.y < this.prevY then
            this.minY := this.y;
        end if;

        this.movedLastFrame := (this.prevX /= this.x or else this.prevY /= this.y);

        this.prevX := this.x;
        this.prevY := this.y;
        this.prevWidth := this.width;
        this.prevHeight := this.height;
    end Update_Previous_State;

    --==========================================================================

    procedure Define_Component( factory : not null A_Entity_Factory ) is
    begin
        factory.Define_Component( Family_Name( LOCATION_ID ),
                                  Create_Location'Access,
                                  propNames =>
                                      (Create( "x"      ),
                                       Create( "y"      ),
                                       Create( "width"  ),
                                       Create( "height" )) );
    end Define_Component;

end Entities.Locations;

