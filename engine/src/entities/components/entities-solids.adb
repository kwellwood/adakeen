--
-- Copyright (c) 2014-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Unchecked_Deallocation;
--with Debugging;                         use Debugging;
with Entities.Systems;                  use Entities.Systems;
with Values.Casting;                    use Values.Casting;
with Values.Construction;               use Values.Construction;
with Values.Indirects;                  use Values.Indirects;
with Values.Strings;                    use Values.Strings;

package body Entities.Solids is

    package Solid_Refs is new Values.Indirects.Generics( Solid );

    ----------------------------------------------------------------------------

    function Get_Mass( this : not null access Solid'Class ) return Value is (Create( this.mass ));

    function Get_Solid( this : not null access Solid'Class ) return Value
    is (Create( this.solidTop and then
                this.solidBottom and then
                this.solidLeft and then
                this.solidRight ));

    function Get_Solid_Top( this : not null access Solid'Class ) return Value is (Create( this.solidTop ));

    function Get_Solid_Bottom( this : not null access Solid'Class ) return Value is (Create( this.solidBottom ));

    function Get_Solid_Left( this : not null access Solid'Class ) return Value is (Create( this.solidLeft ));

    function Get_Solid_Right( this : not null access Solid'Class ) return Value is (Create( this.solidRight ));

    ----------------------------------------------------------------------------

    procedure Set_Mass( this : not null access Solid'Class; mass : Value'Class ) is
    begin
        this.mass := Cast_Float( mass, this.mass );
    end Set_Mass;

    procedure Set_Solid( this : not null access Solid'Class; allSolid : Value'Class ) is
    begin
        this.solidTop    := Cast_Boolean( allSolid, this.solidTop );
        this.solidBottom := Cast_Boolean( allSolid, this.solidBottom );
        this.solidLeft   := Cast_Boolean( allSolid, this.solidLeft );
        this.solidRight  := Cast_Boolean( allSolid, this.solidRight );
    end Set_Solid;

    procedure Set_Solid_Top( this : not null access Solid'Class; top : Value'Class ) is
    begin
        this.solidTop := Cast_Boolean( top, this.solidTop );
    end Set_Solid_Top;

    procedure Set_Solid_Bottom( this : not null access Solid'Class; bottom : Value'Class ) is
    begin
        this.solidBottom := Cast_Boolean( bottom, this.solidBottom );
    end Set_Solid_Bottom;

    procedure Set_Solid_Left( this : not null access Solid'Class; left : Value'Class ) is
    begin
        this.solidLeft := Cast_Boolean( left, this.solidLeft );
    end Set_Solid_Left;

    procedure Set_Solid_Right( this : not null access Solid'Class; right : Value'Class ) is
    begin
        this.solidRight := Cast_Boolean( right, this.solidRight );
    end Set_Solid_Right;

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function To_Edge( normal : Vec2 ) return Edge_Type
    is ((
        if   normal.y < 0.0  then TopEdge
        elsif normal.y > 0.0 then BottomEdge
        elsif normal.x < 0.0 then LeftEdge
        else                      RightEdge
    ));

    ----------------------------------------------------------------------------

    function To_String( edge : Edge_Type ) return String
    is ((
        case edge is
            when TopEdge    => "top",
            when BottomEdge => "bottom",
            when LeftEdge   => "left",
            when RightEdge  => "right"
    ));

    --==========================================================================

    function Create_Solid( properties : Map_Value ) return A_Component is
        this : constant A_Solid := new Solid;
    begin
        this.Construct;

        this.solidTop    := properties.Get_Boolean( "solid", False );
        this.solidBottom := this.solidTop;
        this.solidLeft   := this.solidTop;
        this.solidRight  := this.solidTop;
        this.solidTop    := properties.Get_Boolean( "top",    this.solidTop );
        this.solidBottom := properties.Get_Boolean( "bottom", this.solidBottom );
        this.solidLeft   := properties.Get_Boolean( "left",   this.solidLeft );
        this.solidRight  := properties.Get_Boolean( "right",  this.solidRight );
        this.mass        := properties.Get_Float( "mass", 0.0 );

        for i in this.contacts'Range loop
            this.contacts(i) := new Contact_Sets.Set;
            this.oldContacts(i) := new Contact_Sets.Set;
        end loop;

        return A_Component(this);
    end Create_Solid;

    ----------------------------------------------------------------------------

    procedure Add_Contact( this    : not null access Solid'Class;
                           normal  : Vec2;
                           contact : not null A_Solid ) is
        dist : Float;
    begin
        -- constrain the distance to the contact to be its current distance
        dist := (if normal.y /= 0.0 then normal.y else normal.x);
        this.contacts(To_Edge( normal )).Include( Constraint'(contact, dist) );
    end Add_Contact;

    ----------------------------------------------------------------------------

    procedure Clear_Contacts( this : not null access Solid'Class ) is
    begin
        for edge in this.contacts'Range loop
            this.oldContacts(edge).Move( this.contacts(edge).all );
        end loop;
    end Clear_Contacts;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out Solid ) is
        procedure Free is new Ada.Unchecked_Deallocation( Contact_Sets.Set, A_Contact_Set );
    begin
        for edge in this.contacts'Range loop
            Free( this.contacts(edge) );
            Free( this.oldContacts(edge) );
        end loop;
        Component(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Detect_Collision( this  : not null access Solid'Class;
                                other : not null A_Solid;
                                man   : out Manifold ) is
        dist    : Vec2;
        minDist : Vec2;
        overlap : Vec2;
    begin
        man := Manifold'(A_Solid(this), A_Solid(other), others => <>);

        -- vector from A (this) to B (other) (written as B - A)
        dist := Vec2'(man.b.Get_X - man.a.Get_X,
                      man.b.Get_Y - man.a.Get_Y);

        -- minimum allowed distances in the X and Y axes
        minDist := (Float(man.a.Get_Width + man.b.Get_Width) / 2.0,
                    Float(man.a.Get_Height + man.b.Get_Height) / 2.0);

        -- check for collision in the X axis
        overlap.x := minDist.x - abs dist.x;
        if overlap.x <= 0.0 then
            return;
        end if;

        -- check for collision in the Y axis
        overlap.y := minDist.y - abs dist.y;
        if overlap.y <= 0.0 then
            return;
        end if;

        if abs (overlap.x - overlap.y) <= 0.2 and then Float'Max( overlap.x, overlap.y ) <= 2.0 then
            -- delay the collision because the difference between the X and Y
            -- overlap is so small that we can't really be sure which is the
            -- axis of least penetration. if the penetration becomes deep
            -- enough, however, we're going to have to do something about it. no
            -- passing diagonally through objects.
            --
            -- this fixes a case were an entity is walking across a series of
            -- boxes, or sliding down a neatly stacked tower of boxes. we don't
            -- want it to get hitched up on the seams, so delaying this check
            -- allows one of the other boxes it's colliding with to push it away
            -- in the axis that is clearly correct.
            return;
        end if;

        -- determine axis of least penetration
        if overlap.y <= overlap.x then

            -- resolve collision in the Y axis
            if dist.y < 0.0 then
                if not (man.a.solidTop and then man.b.solidBottom and then
                        man.a.loc.Get_Top - man.b.loc.Get_Bottom_Min_Y >= -3.0)
                then
                    -- no collision: one of the colliding edges is not solid
                    -- or the edges were never apart.
                    --
                    -- when colliding with a one-way platform, don't collide
                    -- with its top until the entity has jumped fully above it.
                    --
                    -- the fudge factor above covers the case where an entity is
                    -- riding on a vertically moving platform. when going up,
                    -- the top entity's Bottom_Min_Y is slightly greater than
                    -- the platform's top edge (at most, by however many pixels
                    -- the platform travelled upward during he frame).
                    return;
                end if;
                man.normal := Vec2'(0.0, -minDist.y);
            else
                if not (man.a.solidBottom and then man.b.solidTop and then
                        man.b.loc.Get_Top - man.a.loc.Get_Bottom_Min_Y >= -3.0)
                then
                    return; -- see the other no collision case above
                end if;
                man.normal := Vec2'(0.0, minDist.y);
            end if;
            man.collision := True;
            --man.penetration := overlap.y;

        else

            -- resolve collision in the X axis
            if dist.x < 0.0 then
                if not man.a.solidLeft or else not man.b.solidRight then
                    return;  -- one of the colliding edges is not solid
                end if;
                man.normal := Vec2'(-minDist.x, 0.0);
            else
                if not man.a.solidRight or else not man.b.solidLeft then
                    return;  -- one of the colliding edges is not solid
                end if;
                man.normal := Vec2'(minDist.x, 0.0);
            end if;
            man.collision := True;
            --man.penetration := overlap.x;

        end if;
    end Detect_Collision;

    ----------------------------------------------------------------------------

    function Get_Mass( this : not null access Solid'Class ) return Float is (this.mass);

    function Get_V( this : not null access Solid'Class ) return Vec2 is (this.mov.Get_V);

    function Get_Height( this : not null access Solid'Class ) return Integer is (this.loc.Get_Height);

    function Get_Width( this : not null access Solid'Class ) return Integer is (this.loc.Get_Width);

    function Get_X( this : not null access Solid'Class ) return Float is (this.loc.Get_X);

    function Get_Y( this : not null access Solid'Class ) return Float is (this.loc.Get_Y);

    function In_Contact( this : not null access Solid'Class; edge : Edge_Type ) return Boolean is (not this.contacts(edge).Is_Empty);

    function Is_Top_Solid( this : not null access Solid'Class ) return Boolean is (this.solidTop);

    function Is_Bottom_Solid( this : not null access Solid'Class ) return Boolean is (this.solidBottom);

    function Is_Left_Solid( this : not null access Solid'Class ) return Boolean is (this.solidLeft);

    function Is_Right_Solid( this : not null access Solid'Class ) return Boolean is (this.solidRight);

    ----------------------------------------------------------------------------

    procedure Iterate_Contacts( this    : not null access Solid'Class;
                                edge    : Edge_Type;
                                examine : not null access procedure( contact : A_Solid ) ) is
    begin
        for c of this.contacts(edge).all loop
            examine.all( c.obj );
        end loop;
    end Iterate_Contacts;

    ----------------------------------------------------------------------------

    procedure Move_With_Platform( this : not null access Solid'Class ) is
        dx   : Float;
        plat : A_Solid;
    begin
        -- this entity must be under the influence of gravity to move with a
        -- platform.
        if this.mov.Get_Gravity <= 0.0 then
            return;
        end if;

        -- loop across bottom contacts for moving platforms
        for c of this.contacts(BottomEdge).all loop
            plat := c.obj;
            if plat.mov.Get_V /= (0.0, 0.0) then

                -- stay linked with the platform in X (perpedicular to gravity)
                dx := plat.loc.Get_X - plat.loc.Get_Prev_X;
                if dx /= 0.0 then
                    this.loc.Add_X( dx );
                    if this.mov.Get_V.x = 0.0 then
                        -- round the X distance between this entity and the
                        -- platform 'plat' so it doesn't appear to jitter as the
                        -- platform moves in X.
                        this.loc.Set_X( plat.loc.Get_X + Float'Rounding( this.loc.Get_X - plat.loc.Get_X ) );
                    end if;
                end if;

                if plat.mov.Get_V.y /= 0.0 and then this.mov.Get_V.y >= plat.mov.Get_V.y then
                    -- constrain the Y distance between the entities to the
                    -- smallest integer distance, so this entity doesn't appear
                    -- to jitter as the platform moves vertically. of course,
                    -- that will cause this solid to penetrate the top of the
                    -- platform by < 1 pixel. only do the constraint if the
                    -- platform is moving vertically and this entity is not
                    -- accelerating upward, off of it.
                    this.loc.Set_Y( plat.Get_Y - Float'Floor( plat.Get_Y - this.loc.Get_Y ) );
                end if;

                exit;    -- don't move with more than one platform
            end if;
        end loop;
    end Move_With_Platform;

    ----------------------------------------------------------------------------

    function Moved( this : not null access Solid'Class ) return Boolean is (this.locChanged);

    ----------------------------------------------------------------------------

    overriding
    function Object_Input( stream : access Root_Stream_Type'Class ) return Solid is
        this : aliased Solid;
    begin
        this.Construct;
        Solid'Read( stream, this );
        return this;
    end Object_Input;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Solid ) is
    begin
        Component'Read( stream, Component(obj) );
        obj.solidTop    := Boolean'Input( stream );
        obj.solidBottom := Boolean'Input( stream );
        obj.solidLeft   := Boolean'Input( stream );
        obj.solidRight  := Boolean'Input( stream );
        obj.mass        := Float'Input( stream );
        for edge in obj.contacts'Range loop
            obj.contacts(edge) := new Contact_Sets.Set;
            obj.oldContacts(edge) := new Contact_Sets.Set;
        end loop;
    end Object_Read;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Solid ) is
    begin
        Component'Write( stream, Component(obj) );
        Boolean'Output( stream, obj.solidTop );
        Boolean'Output( stream, obj.solidBottom );
        Boolean'Output( stream, obj.solidLeft );
        Boolean'Output( stream, obj.solidRight );
        Float'Output( stream, obj.mass );
    end Object_Write;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Added( this : access Solid ) is
    begin
        this.loc := A_Location(this.Get_Owner.Get_Component( LOCATION_ID ));
        this.mov := A_Movement(this.Get_Owner.Get_Component( MOVEMENT_ID ));
        pragma Assert( this.loc /= null, "Solid component depends on Location component" );
        pragma Assert( this.mov /= null, "Solid component depends on Movement component" );

        -- changes to mass and the solidity of specific edges are sent to the entity.
        -- 'solid' is a composite value so it is not.
        this.Register_Entity_Attribute( "solid",       Solid_Refs.Create_Indirect( this, Get_Solid'Access,        Set_Solid'Access        ) );
        this.Register_Entity_Attribute( "solidTop",    Solid_Refs.Create_Indirect( this, Get_Solid_Top'Access,    Set_Solid_Top'Access    ) );
        this.Register_Entity_Attribute( "solidBottom", Solid_Refs.Create_Indirect( this, Get_Solid_Bottom'Access, Set_Solid_Bottom'Access ) );
        this.Register_Entity_Attribute( "solidLeft",   Solid_Refs.Create_Indirect( this, Get_Solid_Left'Access,   Set_Solid_Left'Access   ) );
        this.Register_Entity_Attribute( "solidRight",  Solid_Refs.Create_Indirect( this, Get_Solid_Right'Access,  Set_Solid_Right'Access  ) );
        this.Register_Entity_Attribute( "solidMass",   Solid_Refs.Create_Indirect( this, Get_Mass'Access,         Set_Mass'Access         ) );
    end On_Added;

    ----------------------------------------------------------------------------

    procedure Send_Collision_Messages( this : not null access Solid'Class ) is
    begin
        for edge in this.contacts'Range loop
            for c of this.contacts(edge).all loop
                if not this.oldContacts(edge).Contains( c ) then
                    this.Get_Owner.Dispatch_Message( MSG_HitEntity,
                                                     Create( (Pair( "edge", Create( To_String( edge ) ) ),
                                                              Pair( "eid", To_Id_Value( this.Get_Owner.Get_Id ) )),
                                                             consume => True ).Map );
                end if;
            end loop;
        end loop;
    end Send_Collision_Messages;

    ----------------------------------------------------------------------------

    procedure Set_Solid( this : not null access Solid'Class; allEdges : Boolean ) is
    begin
        this.solidTop    := allEdges;
        this.solidBottom := allEdges;
        this.solidLeft   := allEdges;
        this.solidRight  := allEdges;
    end Set_Solid;

    procedure Set_Top_Solid( this : not null access Solid'Class; edgeSolid : Boolean ) is
    begin
        this.solidTop := edgeSolid;
    end Set_Top_Solid;

    procedure Set_Bottom_Solid( this : not null access Solid'Class; edgeSolid : Boolean ) is
    begin
        this.solidBottom := edgeSolid;
    end Set_Bottom_Solid;

    procedure Set_Left_Solid( this : not null access Solid'Class; edgeSolid : Boolean ) is
    begin
        this.solidLeft := edgeSolid;
    end Set_Left_Solid;

    procedure Set_Right_Solid( this : not null access Solid'Class; edgeSolid : Boolean ) is
    begin
        this.solidRight := edgeSolid;
    end Set_Right_Solid;

    ----------------------------------------------------------------------------

    procedure Set_V( this : not null access Solid'Class; v : Vec2 ) is
    begin
        this.mov.Set_V( v );
    end Set_V;

    procedure Set_VX( this : not null access Solid'Class; vx : Float ) is
    begin
        this.mov.Set_VX( vx );
    end Set_VX;

    procedure Set_VY( this : not null access Solid'Class; vy : Float ) is
    begin
        this.mov.Set_VY( vy );
    end Set_VY;

    ----------------------------------------------------------------------------

    procedure Set_X( this : not null access Solid'Class; x : Float ) is
        dist : constant Float := x - this.loc.Get_X;
    begin
        this.loc.Set_X( x );

        -- an update cycle can occur when there is too much chaos between
        -- too many physics bodies and strange movements occur over
        -- iterations within the same time step.
        if not this.updatingContacts then
            this.updatingContacts := True;
            for c of this.contacts(To_Edge( Vec2'(dist, 0.0) )).all loop
                c.obj.Set_X( x + c.minDist );
            end loop;
            this.updatingContacts := False;
        end if;
    end Set_X;

    ----------------------------------------------------------------------------

    procedure Set_Y( this : not null access Solid'Class; y : Float ) is
        dist : constant Float := y - this.loc.Get_Y;
    begin
        this.loc.Set_Y( y );

        if not this.updatingContacts then
            this.updatingContacts := True;
            for c of this.contacts(To_Edge( Vec2'(0.0, dist) )).all loop
                c.obj.Set_Y( y + c.minDist );
            end loop;
            this.updatingContacts := False;
        end if;
    end Set_Y;

    ----------------------------------------------------------------------------

    procedure Set_XY( this : not null access Solid'Class; x, y : Float ) is
    begin
        this.Set_X( x );
        this.Set_Y( y );
    end Set_XY;

    ----------------------------------------------------------------------------

    procedure Stick_Down_To_Platform( this : not null access Solid'Class ) is
        tileWidth : constant Float := Float(this.Get_Owner.Get_System.Get_World.Tile_Width);
        distY     : Float := -1.0;
        plat      : A_Solid := null;
    begin
        if this.mov.Get_Gravity > 0.0 and then this.mov.Get_V_Target.y >= 0.0 then
            -- find the nearest top edge of the current bottom edge contacts and
            -- stick to it. don't adjust velocity.
            for c of this.contacts(BottomEdge).all loop

                -- only stick down to solids that are near. a "large" distance
                -- means one of the bodies was teleported (or moved in the editor).
                if c.obj.loc.Get_Top - this.loc.Get_Bottom < tileWidth then
                    if plat = null then
                        -- found the first possible candidate
                        distY := Float'Max( 0.0, c.obj.loc.Get_Top - this.loc.Get_Bottom );
                        plat := c.obj;
                    elsif Float'Max( 0.0, c.obj.loc.Get_Top - this.loc.Get_Bottom ) < distY then
                        -- found a closer platform
                        distY := Float'Max( 0.0, c.obj.loc.Get_Top - this.loc.Get_Bottom );
                        plat := c.obj;
                    end if;
                end if;
            end loop;
            if plat /= null then
                -- force a collision to keep it stuck down
                this.loc.Add_Y( distY + 0.25 );
                this.mov.Set_VY( plat.mov.Get_V.y );
            end if;
        end if;
    end Stick_Down_To_Platform;

    ----------------------------------------------------------------------------

    overriding
    procedure Tick( this : access Solid; time : Tick_Time ) is
        pragma Unreferenced( time );
    begin
        this.locChanged := this.loc.Is_Moved or else this.loc.Is_Resized;
    end Tick;

    --==========================================================================

    procedure Define_Component( factory : not null A_Entity_Factory ) is
    begin
        factory.Define_Component( Family_Name( SOLID_ID ),
                                  Create_Solid'Access,
                                  propNames =>
                                      (Create( "bottom" ),
                                       Create( "left"   ),
                                       Create( "mass"   ),
                                       Create( "right"  ),
                                       Create( "solid"  ),
                                       Create( "top"    )),
                                  required =>
                                      (Create( Family_Name( LOCATION_ID ) ),
                                       Create( Family_Name( MOVEMENT_ID ) )) );
    end Define_Component;

end Entities.Solids;

