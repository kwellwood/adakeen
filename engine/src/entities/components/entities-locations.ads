--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Entities.Components;               use Entities.Components;
with Entities.Factory;                  use Entities.Factory;
with Vector_Math;                       use Vector_Math;

package Entities.Locations is

    -- This message is sent to the component's entity when it collides with a
    -- another entity. The 'edge' parameter indicates which edge came into
    -- contact with entity 'eid'. Possible values of 'edge' are "top", "bottom",
    -- "left", "right".
    --
    -- Resized {"width": <newWidth>, "height": <newHeight>}
    MSG_Resized : constant Hashed_String := To_Hashed_String( "Resized" );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Registered Entity Attributes:
    --
    -- Name            Read/Write   Type     Description
    -- ----            ----------   ----     -----------
    -- x, y            read/write   number   Location of the entity
    -- width, height   read/write   number   Size of the entity
    -- left            read         number   X location of the left edge
    -- right           read         number   X location of the right edge
    -- top             read         number   Y location of the top edge
    -- bottom          read         number   Y location of the bottom edge

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Property        Type     Description
    -- --------        ----     -----------
    -- x, y            number   Location of the entity
    -- width, height   number   Size of the entity

    procedure Define_Component( factory : not null A_Entity_Factory );

    ----------------------------------------------------------------------------

    type Location is new Component with private;
    type A_Location is access all Location'Class;

    -- Adds 'distance' to the X world coordinate of the component's entity.
    procedure Add_X( this : not null access Location'Class; distance : Float ) with Inline;

    -- Adds 'distance' to the Y world coordinate of the component's entity.
    procedure Add_Y( this : not null access Location'Class; distance : Float ) with Inline;

    -- Adds 'dX', 'dY' to the world coordinates of the component's entity.
    procedure Add_XY( this : not null access Location'Class; dX, dY : Float ) with Inline;

    overriding
    function Get_Dependency_Order( this : access Location ) return Natural is (DEPS_LOCATION);

    overriding
    function Get_Family( this : access Location ) return Family_Id is (LOCATION_ID);

    overriding
    function Get_Tick_Order( this : access Location ) return Natural is (TCK_LOCATION);

    -- Returns the bounding box of the entity in world coordinates.
    function Get_Bounds( this : not null access Location'Class ) return Rectangle with Inline;

    -- Returns the X world coordinate of the entity.
    function Get_X( this : not null access Location'Class ) return Float with Inline;

    -- Returns the Y world coordinate of the entity.
    function Get_Y( this : not null access Location'Class ) return Float with Inline;

    -- Returns the width the entity in world units.
    function Get_Width( this : not null access Location'Class ) return Natural with Inline;

    -- Returns the height the entity in world units.
    function Get_Height( this : not null access Location'Class ) return Natural with Inline;

    -- Returns the X coordinate of the left edge of the entity in world units.
    function Get_Left( this : not null access Location'Class ) return Float with Inline;

    -- Returns the X coordinate of the right edge of the entity in world units.
    function Get_Right( this : not null access Location'Class ) return Float with Inline;

    -- Returns the Y coordinate of the top edge of the entity in world units.
    function Get_Top( this : not null access Location'Class ) return Float with Inline;

    -- Returns the maximum Y world coordinate of the top edge of the entity
    -- since the last time it moved in a positive Y direction.
    function Get_Top_Max_Y( this : not null access Location'Class ) return Float with Inline;

    -- Returns the Y coordinate of the bottom edge of the entity in world units.
    function Get_Bottom( this : not null access Location'Class ) return Float with Inline;

    -- Returns the minimum Y world coordinate of the bottom edge of the entity
    -- since the last time it moved in a negative Y direction.
    function Get_Bottom_Min_Y( this : not null access Location'Class ) return Float with Inline;

    -- Returns the maximum X world coordinate of the entity since the last time
    -- it moved in a positive X direction. See Get_Y_Min for details.
    function Get_Max_X( this : not null access Location'Class ) return Float with Inline;

    -- Returns the maximum Y world coordinate of the entity since the last time
    -- it moved in a positive Y direction. See Get_Y_Min for details.
    function Get_Max_Y( this : not null access Location'Class ) return Float with Inline;

    -- Returns the minimum X world coordinate of the entity since the last time
    -- it moved in a negative X direction. See Get_Y_Min for details.
    function Get_Min_X( this : not null access Location'Class ) return Float with Inline;

    -- Returns the minimum Y world coordinate of the entity since the last time
    -- it moved in a negative Y direction. This is updated by Update_Previous_State()
    -- each time the change in Y is negative, and remains constant when the
    -- change in Y is positive. This value can be useful, for example, in
    -- determining if an entity has jumped completely above a platform before
    -- falling back down onto it, in order to know if the platform should
    -- support the entity.
    function Get_Min_Y( this : not null access Location'Class ) return Float with Inline;

    -- Returns the X value at the time of the previous Tick.
    function Get_Prev_X( this : not null access Location'Class ) return Float with Inline;

    -- Returns the Y value at the time of the previous Tick.
    function Get_Prev_Y( this : not null access Location'Class ) return Float with Inline;

    -- Returns the width at the time of the previous Tick.
    function Get_Prev_Width( this : not null access Location'Class ) return Natural with Inline;

    -- Returns the height at the time of the previous Tick.
    function Get_Prev_Height( this : not null access Location'Class ) return Natural with Inline;

    -- Public values:
    --
    -- x       (float)
    -- y       (float)
    -- width   (integer)
    -- height  (integer)
    function Get_Public_Values( this : access Location ) return Map_Value;

    -- Returns True if the entity's position changed since the previous Tick.
    function Is_Moved( this : not null access Location'Class ) return Boolean with Inline;

    -- Returns True if the entity's size changed size the previous Tick.
    function Is_Resized( this : not null access Location'Class ) return Boolean with Inline;

    -- Changes the location, relative to its current value. 'xDist' and 'yDist'
    -- will be added to the location's current x, y values.
    procedure Move( this         : not null access Location'Class;
                    xDist, yDist : Float ) with Inline;

    -- Returns True if the entity moved during the previous frame. This is
    -- updated in Update_Previous_State by the positioning system.
    function Moved_Last_Frame( this : not null access Location'Class ) return Boolean with Inline;

    -- Sets the size of the entity in world units. An Entity_Resized event will
    -- be queued.
    procedure Set_Size( this          : not null access Location'Class;
                        width, height : Natural ) with Inline;

    -- Sets the X world coordinate of the component's entity.
    procedure Set_X( this : not null access Location'Class; x : Float ) with Inline;

    -- Sets the Y world coordinate of the component's entity.
    procedure Set_Y( this : not null access Location'Class; y : Float ) with Inline;

    -- Sets the world coordinates of the component's entity.
    procedure Set_XY( this : not null access Location'Class; x, y : Float ) with Inline;

    -- Overwrites the Location's previous state with its current state. This is
    -- called by the positioning system after it has notified the view of the
    -- entity's current location.
    procedure Update_Previous_State( this : not null access Location'Class ) with Inline;

private

    type Location is new Component with
        record
            x,
            y          : Float := 0.0;
            width,
            height     : Natural := 0;
            prevX,
            prevY      : Float := 0.0;
            prevWidth,
            prevHeight : Natural := 0;
            minX, maxX : Float := 0.0;
            minY, maxY : Float := 0.0;
            movedLastFrame : Boolean := False;
        end record;
    for Location'External_Tag use "Component.Location";

    function Object_Input( stream : access Root_Stream_Type'Class ) return Location;
    for Location'Input use Object_Input;

    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Location );
    for Location'Read use Object_Read;

    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Location );
    for Location'Write use Object_Write;

    -- Registers names: x, y, width, height
    procedure On_Added( this : access Location );

end Entities.Locations;
