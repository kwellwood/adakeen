--
-- Copyright (c) 2015-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Color;                     use Allegro.Color;
with Entities.Components;               use Entities.Components;
with Entities.Factory;                  use Entities.Factory;

package Entities.Lights is

    -- Registered Entity Attributes:
    --
    -- Name            Read/Write   Type     Description
    -- ----            ----------   ----     -----------
    -- lightShape      read         string   Shape of light: "point" or "area"
    -- lightZ          read/write   number   Visual z depth of the light (foreground < 0)
    -- lightOn         read/write   boolean  Light is shining?
    -- lightColor      read/write   string   Light color (HTML format RGB)
    -- lightIntensity  read/write   number   Light intensity/brightness (0.0-1.0)
    -- lightDiffuse    read/write   number   Diffuse light level (0.0-1.0)
    -- lightRadius     read/write   number   Lit area radius (point lights)
    -- lightWidth      read/write   number   Width of the lit area (area lights)
    -- lightHeight     read/write   number   Height of the lit area (area lights)
    -- lightAngle      read/write   number   Angle of the light in degrees
    -- lightArc        read/write   number   Angle of the lit arc in degrees (0.0-180.0,360.0)
    -- shadows         read/write   boolean  Light casts shadows?

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    LIGHT_ID : Family_Id := To_Family_Id( "Light" );

    -- Property    Type        Description
    -- --------    ----        -----------
    -- shape       string      Shape of light: "point" or "area"
    -- on          boolean     Light is shining?
    -- color       string      Color of the light (default=White)
    -- intensity   number      Intensity of the light (default=1)
    -- diffuse     number      Ratio of diffuse light (unshadowed) to direct (shadowed) (default=0)
    -- shadows     boolean     Does the light cast shadows? (default=true)
    -- x, y        number      Location of light relative to the entity (default=0,0)
    -- z           number      Visual Z depth of the light; smaller Z depths are in the foreground
    -- angle       number      Direction of the light in degrees (default=0, right/east)
    -- fill        boolean     Automatically fill entity size (default=false)
    -- arc         number      Angle of the lit arc in degrees (default=360) (shape = "point")
    -- radius      number      Radius of the light (shape = "point")
    -- width       number      Width of the light area (shape = "area")
    -- height      number      Height of the light area (shape = "area")

    procedure Define_Component( factory : not null A_Entity_Factory );

    ----------------------------------------------------------------------------

    type Light is new Component with private;
    type A_Light is access all Light'Class;

    overriding
    function Allow_Multiple( this : access Light ) return Boolean is (True);

    overriding
    function Get_Dependency_Order( this : access Light ) return Natural is (DEPS_LIGHT);

    overriding
    function Get_Family( this : access Light ) return Family_Id is (LIGHT_ID);

    overriding
    function Get_Tick_Order( this : access Light ) return Natural is (TCK_LIGHT);

private

    type Light is new Component with
        record
            shape     : Unbounded_String;
            on        : Boolean := True;
            color     : Allegro_Color;
            intensity : Float := 1.0;
            diffuse   : Float := 0.0;
            shadows   : Boolean := True;
            x, y      : Float := 0.0;
            z         : Float := 0.0;
            radius    : Float := 64.0;
            width,
            height    : Float := 128.0;
            fill      : Boolean := False;
            angle     : Float := 0.0;
            arc       : Float := 180.0;
        end record;
    for Light'External_Tag use "Component.Light";

    procedure Handle_Message( this   : access Light;
                              name   : Hashed_String;
                              params : Map_Value );

    function Object_Input( stream : access Root_Stream_Type'Class ) return Light;
    for Light'Input use Object_Input;

    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Light );
    for Light'Read use Object_Read;

    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Light );
    for Light'Write use Object_Write;

    procedure On_Added( this : access Light );

end Entities.Lights;
