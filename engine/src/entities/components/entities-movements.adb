--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Debugging;                         use Debugging;
with Entities.Locations;                use Entities.Locations;
with Support;                           use Support;
with Values.Casting;                    use Values.Casting;
with Values.Construction;               use Values.Construction;
with Values.Indirects;                  use Values.Indirects;
with Values.Strings;                    use Values.Strings;

package body Entities.Movements is

    package Movement_Refs is new Values.Indirects.Generics( Movement );

    -- Any velocity less than the low velocity threshold is clamped to zero.
    LOW_VELOCITY_THRESHOLD : constant := 8.0;

    ----------------------------------------------------------------------------

    function Get_FX( this : not null access Movement'Class ) return Value is (Create( this.friction.x ));
    function Get_FY( this : not null access Movement'Class ) return Value is (Create( this.friction.y ));

    function Get_Gravity( this : not null access Movement'Class ) return Value is (Create( this.gravity ));

    function Get_VX( this : not null access Movement'Class ) return Value is (Create( this.velocity.x ));
    function Get_VY( this : not null access Movement'Class ) return Value is (Create( this.velocity.y ));

    ----------------------------------------------------------------------------

    procedure Set_FX( this : not null access Movement'Class; fx : Value'Class ) is
    begin
        this.Set_FX( Cast_Float( fx, this.friction.x ) );
    end Set_FX;

    procedure Set_FY( this : not null access Movement'Class; fy : Value'Class ) is
    begin
        this.Set_FY( Cast_Float( fy, this.friction.y ) );
    end Set_FY;

    procedure Set_Gravity( this : not null access Movement'Class; gravity : Value'Class ) is
    begin
        this.Set_Gravity( Cast_Float( gravity, this.gravity ) );
    end Set_Gravity;

    procedure Set_VX( this : not null access Movement'Class; vx : Value'Class ) is
    begin
        this.Set_VX( Cast_Float( vx, this.velocity.x ) );
    end Set_VX;

    procedure Set_VY( this : not null access Movement'Class; vy : Value'Class ) is
    begin
        this.Set_VY( Cast_Float( vy, this.velocity.y ) );
    end Set_VY;

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Movement( properties : Map_Value ) return A_Component is
        this : constant A_Movement := new Movement;
    begin
        this.Construct;
        this.velocity.x := properties.Get_Float( "vx", 0.0 );
        this.velocity.y := properties.Get_Float( "vy", 0.0 );
        this.vMax.x := properties.Get_Float( "vxMax", 100_000.0 );
        this.vMax.y := properties.Get_Float( "vyMax", 100_000.0 );
        this.gravInitial := properties.Get_Float( "gravity", 0.0 );
        this.gravity := this.gravInitial;
        this.fricInitial.x := properties.Get_Float( "frictionX", 0.0 );
        this.fricInitial.y := properties.Get_Float( "frictionY", 0.0 );
        this.friction := this.fricInitial;
        return A_Component(this);
    end Create_Movement;

    ----------------------------------------------------------------------------

    function Get_F( this : not null access Movement'Class ) return Vec2 is (this.friction);

    function Get_F_Initial( this : not null access Movement'Class ) return Vec2 is (this.fricInitial);

    function Get_Gravity( this : not null access Movement'Class ) return Float is (this.gravity);

    function Get_Gravity_Initial( this : not null access Movement'Class ) return Float is (this.gravInitial);

    function Get_V( this : not null access Movement'Class ) return Vec2 is (this.velocity);

    function Get_V_Target( this : not null access Movement'Class ) return Vec2 is (this.vTarget);

    ----------------------------------------------------------------------------

    function Is_Moving( this : not null access Movement'Class;
                        dir  : Cardinal_Direction ) return Boolean
    is (
        case dir is
            when Left  => this.vTarget.x < 0.0,
            when Right => this.vTarget.x > 0.0,
            when Up    => this.vTarget.y < 0.0,
            when Down  => this.vTarget.y > 0.0
    );

    function Is_Moving( this : not null access Movement'Class ) return Boolean
    is (this.vTarget.x /= 0.0 or this.vTarget.y /= 0.0);

    ----------------------------------------------------------------------------

    overriding
    function Object_Input( stream : access Root_Stream_Type'Class ) return Movement is
        this : aliased Movement;
    begin
        this.Construct;
        Movement'Read( stream, this );
        return this;
    end Object_Input;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Movement ) is
    begin
        Component'Read( stream, Component(obj) );
        obj.velocity := Vec2'Input( stream );
        obj.vTarget := Vec2'Input( stream );
        obj.vMax := Vec2'Input( stream );
        obj.acc := Vec2'Input( stream );
        obj.gravInitial := Float'Input( stream );
        obj.fricInitial := Vec2'Input( stream );
        obj.gravity := Float'Input( stream );
        obj.friction := Vec2'Input( stream );
    end Object_Read;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Movement ) is
    begin
        Component'Write( stream, Component(obj) );
        Vec2'Output( stream, obj.velocity );
        Vec2'Output( stream, obj.vTarget );
        Vec2'Output( stream, obj.vMax );
        Vec2'Output( stream, obj.acc );
        Float'Output( stream, obj.gravInitial );
        Vec2'Output( stream, obj.fricInitial );
        Float'Output( stream, obj.gravity );
        Vec2'Output( stream, obj.friction );
    end Object_Write;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Added( this : access Movement ) is
    begin
        this.Register_Entity_Attribute( "frictionX", Movement_Refs.Create_Indirect( this, Get_FX'Access,      Set_FX'Access      ) );
        this.Register_Entity_Attribute( "frictionY", Movement_Refs.Create_Indirect( this, Get_FY'Access,      Set_FY'Access      ) );
        this.Register_Entity_Attribute( "gravity",   Movement_Refs.Create_Indirect( this, Get_Gravity'Access, Set_Gravity'Access ) );
        this.Register_Entity_Attribute( "vx",        Movement_Refs.Create_Indirect( this, Get_VX'Access,      Set_VX'Access      ) );
        this.Register_Entity_Attribute( "vy",        Movement_Refs.Create_Indirect( this, Get_VY'Access,      Set_VY'Access      ) );
    end On_Added;

    ----------------------------------------------------------------------------

    procedure Set_F( this : not null access Movement'Class; friction : Vec2 ) is
    begin
        this.friction := friction;
    end Set_F;

    ----------------------------------------------------------------------------

    procedure Set_FX( this : not null access Movement'Class; fx : Float ) is
    begin
        this.friction.x := fx;
    end Set_FX;

    ----------------------------------------------------------------------------

    procedure Set_FY( this : not null access Movement'Class; fy : Float ) is
    begin
        this.friction.y := fy;
    end Set_FY;

    ----------------------------------------------------------------------------

    procedure Set_Gravity( this : not null access Movement'Class; gravity : Float ) is
    begin
        this.gravity := gravity;
    end Set_Gravity;

    ----------------------------------------------------------------------------

    procedure Set_V( this : not null access Movement'Class; v : Vec2 ) is
    begin
        this.velocity := (x => Constrain( v.x, -this.vMax.x, this.vMax.x),
                          y => Constrain( v.y, -this.vMax.y, this.vMax.y));
    end Set_V;

    ----------------------------------------------------------------------------

    procedure Set_VX( this : not null access Movement'Class; vx : Float ) is
    begin
        this.velocity.x := Constrain( vx, -this.vMax.x, this.vMax.x );
    end Set_VX;

    ----------------------------------------------------------------------------

    procedure Set_VY( this : not null access Movement'Class; vy : Float ) is
    begin
        this.velocity.y := Constrain( vy, -this.vMax.y, this.vMax.y );
    end Set_VY;

    ----------------------------------------------------------------------------

    procedure Set_V_Target( this   : not null access Movement'Class;
                            target : Vec2;
                            acc    : Vec2 ) is
    begin
        this.vTarget := target;
        this.acc := acc;
        if this.acc.x >= IMPULSE then
            this.velocity.x := this.vTarget.x;
            if this.vTarget.x = 0.0 then
                this.acc.x := 0.0;   -- don't maintain 0 velocity
            end if;
        end if;
        if this.acc.y >= IMPULSE then
            this.velocity.y := this.vTarget.y;
            if this.vTarget.y = 0.0 then
                this.acc.y := 0.0;   -- don't maintain 0 velocity
            end if;
        end if;
        pragma Debug( Dbg( "Movement.Set_V_Target( " &
                           Image( this.owner.Get_Id ) & ", " &
                           Image( target ) & ", (" &
                           (if abs acc.x >= IMPULSE then "IMPULSE" else Image( acc.x )) & ", " &
                           (if abs acc.y >= IMPULSE then "IMPULSE" else Image( acc.y )) & ") )",
                           D_PHYSICS, Info ) );
    end Set_V_Target;

    ----------------------------------------------------------------------------

    procedure Set_VX_Target( this   : not null access Movement'Class;
                             target : Float;
                             acc    : Float ) is
    begin
        this.vTarget.x := target;
        this.acc.x := acc;
        if this.acc.x >= IMPULSE then
            this.velocity.x := this.vTarget.x;
            if this.vTarget.x = 0.0 then
                this.acc.x := 0.0;
            end if;
        end if;
        pragma Debug( Dbg( "Movement.Set_VX_Target( " &
                           Image( this.owner.Get_Id ) & ", " &
                           Image( target ) & ", " &
                           (if abs acc >= IMPULSE then "IMPULSE" else Image( acc )) & " )",
                           D_PHYSICS, Info ) );
    end Set_VX_Target;

    ----------------------------------------------------------------------------

    procedure Set_VY_Target( this   : not null access Movement'Class;
                             target : Float;
                             acc    : Float ) is
    begin
        this.vTarget.y := target;
        this.acc.y := acc;
        if this.acc.y >= IMPULSE then
            this.velocity.y := this.vTarget.y;
            if this.vTarget.y = 0.0 then
                this.acc.y := 0.0;
            end if;
        end if;
        pragma Debug( Dbg( "Movement.Set_VY_Target( " &
                           Image( this.owner.Get_Id ) & ", " &
                           Image( target ) & ", " &
                           (if abs acc >= IMPULSE then "IMPULSE" else Image( acc )) & " )",
                           D_PHYSICS, Info ) );
    end Set_VY_Target;

    ----------------------------------------------------------------------------

    overriding
    procedure Tick( this : access Movement; time : Tick_Time ) is
        dt : constant Float := Float(To_Duration( time.elapsed ));
    begin
        -- apply gravity
        this.velocity.y := this.velocity.y + (this.gravity * dt);

        -- apply friction
        if this.vTarget.x = 0.0 and this.velocity.x /= 0.0 and this.friction.x > 0.0 then
            if this.velocity.x > 0.0 then
                this.velocity.x := Float'Max( this.velocity.x - this.friction.x * dt, 0.0 );
            else
                this.velocity.x := Float'Min( this.velocity.x + this.friction.x * dt, 0.0 );
            end if;
            if abs this.velocity.x < LOW_VELOCITY_THRESHOLD then
                this.velocity.x := 0.0;
            end if;
        end if;
        if this.vTarget.y = 0.0 and this.velocity.y /= 0.0 and this.friction.y > 0.0 then
            if this.velocity.y > 0.0 then
                this.velocity.y := Float'Max( this.velocity.y - this.friction.y * dt, 0.0 );
            else
                this.velocity.y := Float'Min( this.velocity.y + this.friction.y * dt, 0.0 );
            end if;
            if abs this.velocity.y < LOW_VELOCITY_THRESHOLD then
                this.velocity.y := 0.0;
            end if;
        end if;

        -- accelerate to maintain target velocity
        if this.acc.x > 0.0 then
            if this.velocity.x < this.vTarget.x then
                this.velocity.x := this.velocity.x + (this.acc.x * dt);
                if this.velocity.x > this.vTarget.x then
                    -- reached target velocity
                    this.velocity.x := this.vTarget.x;
                    if this.vTarget.x = 0.0 then
                        this.acc.x := 0.0;      -- don't maintain 0 velocity
                    end if;
                end if;
            elsif this.velocity.x > this.vTarget.x then
                this.velocity.x := this.velocity.x - (this.acc.x * dt);
                if this.velocity.x < this.vTarget.x then
                    -- reached target velocity
                    this.velocity.x := this.vTarget.x;
                    if this.vTarget.x = 0.0 then
                        this.acc.x := 0.0;
                    end if;
                end if;
            end if;
        end if;
        if this.acc.y > 0.0 then
            if this.velocity.y < this.vTarget.y then
                this.velocity.y := this.velocity.y + (this.acc.y * dt);
                if this.velocity.y > this.vTarget.y then
                    -- reached target velocity
                    this.velocity.y := this.vTarget.y;
                    if this.vTarget.y = 0.0 then
                        this.acc.y := 0.0;      -- don't maintain 0 velocity
                    end if;
                end if;
            elsif this.velocity.y > this.vTarget.y then
                this.velocity.y := this.velocity.y - (this.acc.y * dt);
                if this.velocity.y < this.vTarget.y then
                    -- reached target velocity
                    this.velocity.y := this.vTarget.y;
                    if this.vTarget.y = 0.0 then
                        this.acc.y := 0.0;
                    end if;
                end if;
            end if;
        end if;

        -- constrain velocity
        this.velocity.x := Constrain( this.velocity.x, -this.vMax.x, this.vMax.x );
        this.velocity.y := Constrain( this.velocity.y, -this.vMax.y, this.vMax.y );

        -- integrate velocity into position
        A_Location(this.Get_Component( LOCATION_ID )).Move( this.velocity.x * dt, this.velocity.y * dt );
    end Tick;

    --==========================================================================

    procedure Define_Component( factory : not null A_Entity_Factory ) is
    begin
        factory.Define_Component( Family_Name( MOVEMENT_ID ),
                                  Create_Movement'Access,
                                  propNames =>
                                      (Create( "vx"        ),
                                       Create( "vy"        ),
                                       Create( "vxMax"     ),
                                       Create( "vyMax"     ),
                                       Create( "frictionX" ),
                                       Create( "frictionY" ),
                                       Create( "gravity"   )),
                                  required =>
                                      (1 => Create( Family_Name( LOCATION_ID ) )) );
    end Define_Component;

end Entities.Movements;

