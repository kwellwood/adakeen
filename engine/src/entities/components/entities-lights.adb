--
-- Copyright (c) 2015-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Numerics;                      use Ada.Numerics;
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
--with Debugging;                         use Debugging;
with Entities;
with Entities.Entity_Attributes;        use Entities.Entity_Attributes;
with Entities.Locations;                use Entities.Locations;
with Events.Lighting;                   use Events.Lighting;
with Palette;                           use Palette;
with Support;                           use Support;
with Values.Casting;                    use Values.Casting;
with Values.Construction;               use Values.Construction;
with Values.Indirects;                  use Values.Indirects;
with Values.Strings;                    use Values.Strings;
with Worlds;                            use Worlds;

package body Entities.Lights is

    package Light_Refs is new Values.Indirects.Generics( Light );

    -- Notifies the View that property 'property' changed to 'val'. An
    -- AttributeChanged message will also be dispatched for attribute 'attribute'.
    -- Both the property and attribute names are given because they are not the
    -- same for most fields of the Light.
    procedure Notify_Changed( this      : not null access Light'Class;
                              property  : String;
                              attribute : String;
                              val       : Value'Class );

    -- Shows/hides the light in the view, by queueing events.
    procedure Show_Light( this : not null access Light'Class; show : Boolean );

    ----------------------------------------------------------------------------

    function Wrap_Angle_0_360( degrees : Float ) return Float is
        -- convert from 0 -> 360 to -Pi -> +Pi
        radians : constant Float := degrees * Float(Pi / 180.0) - Pi;
    begin
        return (Arctan( Sin( radians ), Cos( radians ) ) + Pi) * Float(180.0 / Pi);
    end Wrap_Angle_0_360;

    ----------------------------------------------------------------------------

    function Get_Angle( this : not null access Light'Class ) return Value is (Create( this.angle ));

    function Get_Arc( this : not null access Light'Class ) return Value is (Create( this.arc ));

    function Get_Color( this : not null access Light'Class ) return Value is (Create( Color_To_Html( this.color, False ) ));

    function Get_Diffuse( this : not null access Light'Class ) return Value is (Create( this.diffuse ));

    function Get_Height( this : not null access Light'Class ) return Value is (Create( this.height ));

    function Get_Intensity( this : not null access Light'Class ) return Value is (Create( this.intensity ));

    function Get_On( this : not null access Light'Class ) return Value is (Create( this.on ));

    function Get_Radius( this : not null access Light'Class ) return Value is (Create( this.radius ));

    function Get_Shadows( this : not null access Light'Class ) return Value is (Create( this.shadows ));

    function Get_Shape( this : not null access Light'Class ) return Value is (Create( this.shape ));

    function Get_Width( this : not null access Light'Class ) return Value is (Create( this.width ));

    function Get_Z( this : not null access Light'Class ) return Value is (Create( this.z ));

    ----------------------------------------------------------------------------

    procedure Set_Angle( this : not null access Light'Class; angle : Value'Class ) is
    begin
        this.angle := Wrap_Angle_0_360( Cast_Float( angle, this.angle ) );
        this.Notify_Changed( "angle", "lightAngle", Create( this.angle ) );
    end Set_Angle;

    ----------------------------------------------------------------------------

    procedure Set_Arc( this : not null access Light'Class; arc : Value'Class ) is
    begin
        if this.shape = "point" then
            this.arc := Constrain( Cast_Float( arc, this.arc ), 0.0, 180.0 );
            this.Notify_Changed( "arc", "lightArc", Create( this.arc ) );
        end if;
    end Set_Arc;

    ----------------------------------------------------------------------------

    procedure Set_Color( this : not null access Light'Class; color : Value'Class ) is
    begin
        this.color := Opacity( Html_To_Color( Cast_String( color ), this.color ), 1.0 );
        this.Notify_Changed( "color", "lightColor", Create( Color_To_Html( this.color, False ) ) );
    end Set_Color;

    ----------------------------------------------------------------------------

    procedure Set_Diffuse( this : not null access Light'Class; diffuse : Value'Class ) is
    begin
        this.diffuse := Cast_Float( diffuse, this.diffuse );
        this.Notify_Changed( "diffuse", "lightDiffuse", Create( this.diffuse ) );
    end Set_Diffuse;

    ----------------------------------------------------------------------------

    procedure Set_Height( this : not null access Light'Class; height : Value'Class ) is
    begin
        if this.shape = "area" then
            this.height := Cast_Float( height, this.height );
            this.Notify_Changed( "height", "lightHeight", Create( this.height ) );
        end if;
    end Set_Height;

    ----------------------------------------------------------------------------

    procedure Set_Intensity( this : not null access Light'Class; intensity : Value'Class ) is
    begin
        this.intensity := Cast_Float( intensity, this.intensity );
        this.Notify_Changed( "intensity", "lightIntensity", Create( this.intensity ) );
    end Set_Intensity;
    ----------------------------------------------------------------------------

    procedure Set_On( this : not null access Light'Class; on : Value'Class ) is
    begin
        if Cast_Boolean( on, this.on ) /= this.on then
            this.on := not this.on;
            this.Show_Light( this.on );
            this.Get_Owner.Get_Attributes.Notify_Changed( this, "lightOn" );
        end if;
    end Set_On;

    ----------------------------------------------------------------------------

    procedure Set_Radius( this : not null access Light'Class; radius : Value'Class ) is
    begin
        if this.shape = "point" then
            this.radius := Cast_Float( radius, this.radius );
            this.Notify_Changed( "radius", "lightRadius", Create( this.radius ) );
        end if;
    end Set_Radius;

    ----------------------------------------------------------------------------

    procedure Set_Shadows( this : not null access Light'Class; shadows : Value'Class ) is
    begin
        this.shadows := Cast_Boolean( shadows, this.shadows );
        this.Notify_Changed( "shadows", "shadows", Create( this.shadows ) );
    end Set_Shadows;

    ----------------------------------------------------------------------------

    procedure Set_Width( this : not null access Light'Class; width : Value'Class ) is
    begin
        if this.shape = "area" then
            this.width := Cast_Float( width, this.width );
            this.Notify_Changed( "width", "lightWidth", Create( this.width ) );
        end if;
    end Set_Width;

    ----------------------------------------------------------------------------

    procedure Set_Z( this : not null access Light'Class; z : Value'Class ) is
    begin
        this.z := Cast_Float( z, this.z );
        this.Notify_Changed( "z", "lightZ", Create( this.z ) );
    end Set_Z;

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Light( properties : Map_Value ) return A_Component is
        this : constant A_Light := new Light;
    begin
        this.Construct;
        this.shape     := properties.Get_Unbounded_String( "shape", "point" );
        this.on        := properties.Get_Boolean( "on", this.on );
        this.color     := Opacity( Html_To_Color( properties.Get_String( "color" ), White ), 1.0 );
        this.intensity := properties.Get_Float( "intensity", this.intensity );
        this.diffuse   := properties.Get_Float( "diffuse", this.diffuse );
        this.shadows   := properties.Get_Boolean( "shadows", this.shadows );
        this.angle     := Wrap_Angle_0_360( properties.Get_Float( "angle", this.angle ) );
        this.arc       := Constrain( properties.Get_Float( "arc", this.arc ), 0.0, 180.0 );
        this.x         := properties.Get_Float( "x", 0.0 );
        this.y         := properties.Get_Float( "y", 0.0 );
        this.z         := properties.Get_Float( "z", 0.0 );
        this.fill      := properties.Get_Boolean( "fill", this.fill );
        this.radius    := properties.Get_Float( "radius", this.radius );
        this.width     := this.radius * 2.0;
        if this.width > 0.0 then
            this.height := this.width;
        else
            this.width  := properties.Get_Float( "width", 128.0 );
            this.height := properties.Get_Float( "height", this.width );
        end if;

        if this.shape /= "point" and then
           this.shape /= "area"
        then
            this.shape := To_Unbounded_String( "point" );
        end if;

        return A_Component(this);
    end Create_Light;

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Message( this   : access Light;
                              name   : Hashed_String;
                              params : Map_Value ) is
    begin
        if name = MSG_Resized then
            -- this is only received if .fill is True
            if this.shape = "point" then
                this.radius := Float'Min( params.Get_Float( "width", this.width ),
                                          params.Get_Float( "height", this.height ) ) / 2.0;
                this.Notify_Changed( "radius", "lightRadius", Create( this.radius ) );
            elsif this.shape = "area" then
                this.width := params.Get_Float( "width", this.width );
                this.height := params.Get_Float( "height", this.height );
                this.Notify_Changed( "width", "lightWidth", Create( this.width ) );
                this.Notify_Changed( "height", "lightHeight", Create( this.height ) );
            end if;

        elsif name = MSG_WorldLoaded or else name = MSG_Spawned then
            if this.on then
                this.Show_Light( True );
            end if;

        elsif name = MSG_WorldUnloaded or else name = MSG_Destruct then
            if this.on then
                this.Show_Light( False );
            end if;

        end if;
    end Handle_Message;

    ----------------------------------------------------------------------------

    procedure Notify_Changed( this      : not null access Light'Class;
                              property  : String;
                              attribute : String;
                              val       : Value'Class ) is
    begin
        if this.on then
            Queue_Light_Updated( this.Get_Id, Maps.Create( (1=>Pair(property, val )) ).Map );
        end if;
        this.Get_Owner.Get_Attributes.Notify_Changed( this, attribute );
    end Notify_Changed;

    ----------------------------------------------------------------------------

    overriding
    function Object_Input( stream : access Root_Stream_Type'Class ) return Light is
        this : aliased Light;
    begin
        this.Construct;
        Light'Read( stream, this );
        return this;
    end Object_Input;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Light ) is
    begin
        Component'Read( stream, Component(obj) );
        obj.shape := Read_String_16( stream );
        obj.on := Boolean'Input( stream );
        obj.color := Html_To_Color( To_String( Read_String_16( stream ) ), White );
        obj.intensity := Float'Input( stream );
        obj.diffuse := Float'Input( stream );
        obj.shadows := Boolean'Input( stream );
        obj.x := Float'Input( stream );
        obj.y := Float'Input( stream );
        obj.z := Float'Input( stream );
        obj.radius := Float'Input( stream );
        obj.width := Float'Input( stream );
        obj.height := Float'Input( stream );
        obj.fill := Boolean'Input( stream );
        obj.angle := Float'Input( stream );
        obj.arc := Float'Input( stream );
    end Object_Read;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Light ) is
    begin
        Component'Write( stream, Component(obj) );
        Write_String_16( stream, obj.shape );
        Boolean'Output( stream, obj.on );
        Write_String_16( stream, To_Unbounded_String( Color_To_Html( obj.color, False ) ) );
        Float'Output( stream, obj.intensity );
        Float'Output( stream, obj.diffuse );
        Boolean'Output( stream, obj.shadows );
        Float'Output( stream, obj.x );
        Float'Output( stream, obj.y );
        Float'Output( stream, obj.z );
        Float'Output( stream, obj.radius );
        Float'Output( stream, obj.width );
        Float'Output( stream, obj.height );
        Boolean'Output( stream, obj.fill );
        Float'Output( stream, obj.angle );
        Float'Output( stream, obj.arc );
    end Object_Write;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Added( this : access Light ) is
        loc : A_Location;
    begin
        this.Register_Listener( MSG_WorldLoaded );   -- when world is loaded
        this.Register_Listener( MSG_Spawned );       -- when entity is spawned in play
        this.Register_Listener( MSG_Destruct );      -- when entity is deleted in play
        this.Register_Listener( MSG_WorldUnloaded ); -- when world is unloaded
        if this.fill then
            this.Register_Listener( MSG_Resized );   -- when entity size changes
        end if;

        this.Register_Entity_Attribute( "lightShape",     Light_Refs.Create_Indirect( this, Get_Shape'Access,     null                 ), scope => Silent_Scope );
        this.Register_Entity_Attribute( "lightZ",         Light_Refs.Create_Indirect( this, Get_Z'Access,         Set_Z'Access         ), scope => Global_Scope );
        this.Register_Entity_Attribute( "lightOn",        Light_Refs.Create_Indirect( this, Get_On'Access,        Set_On'Access        ), scope => Global_Scope );
        this.Register_Entity_Attribute( "lightColor",     Light_Refs.Create_Indirect( this, Get_Color'Access,     Set_Color'Access     ), scope => Global_Scope );
        this.Register_Entity_Attribute( "lightIntensity", Light_Refs.Create_Indirect( this, Get_Intensity'Access, Set_Intensity'Access ), scope => Global_Scope );
        this.Register_Entity_Attribute( "lightDiffuse",   Light_Refs.Create_Indirect( this, Get_Diffuse'Access,   Set_Diffuse'Access   ), scope => Global_Scope );
        this.Register_Entity_Attribute( "lightWidth",     Light_Refs.Create_Indirect( this, Get_Width'Access,     Set_Width'Access     ), scope => Global_Scope );
        this.Register_Entity_Attribute( "lightHeight",    Light_Refs.Create_Indirect( this, Get_Height'Access,    Set_Height'Access    ), scope => Global_Scope );
        this.Register_Entity_Attribute( "lightRadius",    Light_Refs.Create_Indirect( this, Get_Radius'Access,    Set_Radius'Access    ), scope => Global_Scope );
        this.Register_Entity_Attribute( "lightAngle",     Light_Refs.Create_Indirect( this, Get_Angle'Access,     Set_Angle'Access     ), scope => Global_Scope );
        this.Register_Entity_Attribute( "lightArc",       Light_Refs.Create_Indirect( this, Get_Arc'Access,       Set_Arc'Access       ), scope => Global_Scope );
        this.Register_Entity_Attribute( "shadows",        Light_Refs.Create_Indirect( this, Get_Shadows'Access,   Set_Shadows'Access   ), scope => Global_Scope );

        if this.fill then
            loc := A_Location(this.Get_Component( LOCATION_ID ));
            this.width := Float(loc.Get_Width);
            this.height := Float(loc.Get_Height);
        end if;
    end On_Added;

    ----------------------------------------------------------------------------

    procedure Show_Light( this : not null access Light'Class; show : Boolean ) is
        loc   : A_Location;
        props : Map_Value;
    begin
        if show then
            loc := A_Location(this.Get_Component( LOCATION_ID ));
            props := Create_Map( (Pair( "shape", Create( this.shape ) ),
                                  Pair( "color", Create( Color_To_Html( this.color, False ) ) ),
                                  Pair( "intensity", Create( this.intensity ) ),
                                  Pair( "diffuse", Create( this.diffuse ) ),
                                  Pair( "shadows", Create( this.shadows ) ),
                                  Pair( "x", Create( loc.Get_X + this.x ) ),
                                  Pair( "y", Create( loc.Get_Y + this.y ) ),
                                  Pair( "z", Create( this.z ) ),
                                  Pair( "angle", Create( this.angle ) )) ).Map;
            if this.shape = "point" then
                props.Set( "radius", Create( this.radius ) );
                props.Set( "arc", Create( this.arc ) );
            elsif this.shape = "area" then
                props.Set( "width", Create( this.width ) );
                props.Set( "height", Create( this.height ) );
            end if;
            Queue_Light_Created( this.id, this.owner.Get_Id, props );
        else
            Queue_Light_Deleted( this.id );
        end if;
    end Show_Light;

    --==========================================================================

    procedure Define_Component( factory : not null A_Entity_Factory ) is
    begin
        factory.Define_Component( Family_Name( LIGHT_ID ),
                                  Create_Light'Access,
                                  propNames =>
                                      (Create( "shape"     ),
                                       Create( "on"        ),
                                       Create( "shadows"   ),
                                       Create( "angle"     ),
                                       Create( "color"     ),
                                       Create( "intensity" ),
                                       Create( "diffuse"   ),
                                       Create( "x"         ),
                                       Create( "y"         ),
                                       Create( "z"         ),
                                       Create( "width"     ),       -- area lights only
                                       Create( "height"    ),       -- area lights only
                                       Create( "fill"      ),
                                       Create( "arc"       ),       -- point lights only
                                       Create( "radius"    )),      -- point lights only
                                  required =>
                                      (1 => Create( Family_Name( LOCATION_ID ) )) );
    end Define_Component;

end Entities.Lights;
