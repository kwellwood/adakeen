--
-- Copyright (c) 2016-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Debugging;                         use Debugging;
with Entities.Systems;                  use Entities.Systems;
with Events.Entities;                   use Events.Entities;
with Events.Entities.Attributes;        use Events.Entities.Attributes;
with Events.World;                      use Events.World;
with Games;                             use Games;
with Scribble.Compilers;                use Scribble.Compilers;
with Values.Construction;               use Values.Construction;
with Values.Indirects;
with Values.Streaming;                  use Values.Streaming;
with Values.Strings;                    use Values.Strings;

package body Entities.Entity_Attributes is

    package Attribute_Refs is new Values.Indirects.Generics( Attributes );

    ----------------------------------------------------------------------------

    procedure Define_Api( this : not null access Attributes'Class );

    -- Defines locally stored attribute 'name', with initial value 'initial'.
    -- 'initial' will not be copieid of 'consume' is True. The attribute 'name'
    -- must not already be defined.
    function Define_Attribute( this    : not null access Attributes'Class;
                               name    : String;
                               initial : Value'Class;
                               consume : Boolean ) return Attribute_Maps.Cursor;

    -- Dispatches a notification to 'scope' that 'name' now equals 'val'.
    procedure Dispatch_Notification( this  : not null access Attributes'Class;
                                     name  : String;
                                     val   : Value'Class;
                                     scope : Notify_Scope );

    -- Returns the entity's age in milliseconds.
    function Read_Age( this : not null access Attributes'Class ) return Value is (Create( Long_Float(To_Duration( this.Get_Owner.Get_Age )) * 1000.0 ));

    -- Returns a built-in method from the entity's Scribble API.
    function Read_Api( this : not null access Attributes'Class; name : String ) return Value is (Clone( this.api.Get( name ) ));

    -- This procedure is called from an attribute Reference to read the value.
    --
    -- Returns the value of attribute 'name'. If the attribute is registered,
    -- the registering attibute will return the value.
    function Read_Attribute( this : not null access Attributes'Class;
                             name : String ) return Value;

    -- Returns the name of the entity's template.
    function Read_Template( this : not null access Attributes'Class ) return Value is (Create( this.Get_Owner.Get_Template ));

    -- This procedure is called from an attribute Reference to change the value.
    --
    -- Sets the 'name' attribute of the entity, by copy. If 'val' is Null and
    -- the attribute is a locally stored attribute, it will be deleted. If
    -- 'name' is a registered attribute, the registering component will process
    -- the set and may reject it (i.e.: the attribute is readonly.) If the
    -- attribute value changes, an AttributeChanged message will be sent to the
    -- entity and an Entity_Attribute_Changed event will be queued to the event
    -- manager.
    procedure Write_Attribute( this : not null access Attributes'Class;
                               name : String;
                               val  : Value'Class );

    -- Same as Write_Attribute above, but this one does the work.
    procedure Write_Attribute( this : not null access Attributes'Class;
                               name : String;
                               val  : Value'Class;
                               pos  : Attribute_Maps.Cursor );
    pragma Precondition( Attribute_Maps.Has_Element( pos ) );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Attributes( properties : Map_Value ) return A_Component is
        this : constant A_Attributes := new Attributes;

        procedure Define( name : String; val : Value ) is
            pos : Attribute_Maps.Cursor;
            pragma Warnings( Off, pos );
        begin
            -- entity attributes that are defined when the Attributes component
            -- is instantiated (on entity creation) are local to this component,
            -- so changes are automatically notified and visible to the Game_View.
            pos := this.Define_Attribute( name    => name,
                                          initial => val,
                                          consume => False );
        end Define;

    begin
        this.Construct;
        properties.Iterate( Define'Access );
        return A_Component(this);
    end Create_Attributes;

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access Attributes ) is
    begin
        Component(this.all).Construct;
        this.api := Create_Map.Map;
    end Construct;

    ----------------------------------------------------------------------------

    function Define_Attribute( this    : not null access Attributes'Class;
                               name    : String;
                               initial : Value'Class;
                               consume : Boolean ) return Attribute_Maps.Cursor is
        def      : A_Attribute_Def;
        pos      : Attribute_Maps.Cursor;
        inserted : Boolean;
    begin
        def := new Attribute_Def'(component => null,
                                  exRef     => Attribute_Refs.Create_Indirect( this,
                                                                               Read_Attribute'Access,
                                                                               Write_Attribute'Access,
                                                                               name ),
                                  ref       => Null_Value,         -- the reference is initialized next
                                  val       => (if consume then Value(initial) else Clone( initial ) ),
                                  scope     => (if initial.Is_Function then Silent_Scope else Global_Scope));
        this.names.Insert( name,
                           def,
                           pos,
                           inserted );
        pragma Assert( inserted, "Entity attribute already defined" );
        this.names.Reference( pos ).ref := Create_Ref( this.names.Reference( pos ).val'Access );
        return pos;
    end Define_Attribute;

    ----------------------------------------------------------------------------

    procedure Define_Api( this : not null access Attributes'Class ) is
        compiler : constant A_Scribble_Compiler := this.Get_Owner.Get_System.Get_Game.Get_Script_VM.Get_Compiler;
    begin
        if not this.api.Is_Empty then
            -- already defined
            return;
        end if;

        -- Entity Methods
        this.api.Set( "AddComponent",          compiler.Bind( "AddComponent", defaults => (1 => Create_Map) ), consume => True );
        this.api.Set( "Destroy",               compiler.Bind( "Destroy"                                     ), consume => True );
        this.api.Set( "NearPlayer",            compiler.Bind( "NearPlayer"                                  ), consume => True );
        this.api.Set( "RemoveComponent",       compiler.Bind( "RemoveComponent"                             ), consume => True );
        this.api.Set( "RemoveComponentFamily", compiler.Bind( "RemoveComponentFamily"                       ), consume => True );
        this.api.Set( "Send",                  compiler.Bind( "Send", defaults => (1 => Create_Map)         ), consume => True );

        -- Built-in Attributes (readonly)
        this.Register( "age",      A_Component(this), Attribute_Refs.Create_Indirect( this, Read_Age'Access,      null ) );
        this.Register( "template", A_Component(this), Attribute_Refs.Create_Indirect( this, Read_Template'Access, null ) );

        -- Built-in Methods (readonly)
        this.Register( "AddComponent",          A_Component(this), Attribute_Refs.Create_Indirect( this, Read_Api'Access, null, "AddComponent"          ) );
        this.Register( "Destroy",               A_Component(this), Attribute_Refs.Create_Indirect( this, Read_Api'Access, null, "Destroy"               ) );
        this.Register( "NearPlayer",            A_Component(this), Attribute_Refs.Create_Indirect( this, Read_Api'Access, null, "NearPlayer"            ) );
        this.Register( "RemoveComponent",       A_Component(this), Attribute_Refs.Create_Indirect( this, Read_Api'Access, null, "RemoveComponent"       ) );
        this.Register( "RemoveComponentFamily", A_Component(this), Attribute_Refs.Create_Indirect( this, Read_Api'Access, null, "RemoveComponentFamily" ) );
        this.Register( "Send",                  A_Component(this), Attribute_Refs.Create_Indirect( this, Read_Api'Access, null, "Send"                  ) );
    end Define_Api;

    ----------------------------------------------------------------------------

    procedure Dispatch_Notification( this  : not null access Attributes'Class;
                                     name  : String;
                                     val   : Value'Class;
                                     scope : Notify_Scope ) is
    begin
        if this.Get_Owner.Get_System /= null then
            if scope /= Silent_Scope then
                if scope = Global_Scope then
                    Queue_Entity_Attribute_Changed( this.Get_Owner.Get_Id, name, val );
                end if;
                this.Get_Owner.Dispatch_Message( MSG_AttributeChanged,
                                                 Create( (Pair( "name", Create( name ) ),
                                                          Pair( "value", val )),
                                                         consume => True ).Map );
            end if;
        end if;
    end Dispatch_Notification;

    ----------------------------------------------------------------------------

    overriding
    function Get_Public_Values( this : access Attributes ) return Map_Value is
        use Attribute_Maps;
        pos    : Attribute_Maps.Cursor;
        values : constant Map_Value := Create_Map.Map;
    begin
        pos := this.names.First;
        while Has_Element( pos ) loop
            if this.names.Constant_Reference( pos ).scope = Global_Scope then
                values.Set( Key( pos ), this.names.Constant_Reference( pos ).ref.Deref, consume => True );
            end if;
            Next( pos );
        end loop;
        return values;
    end Get_Public_Values;

    ----------------------------------------------------------------------------

    function Get_Local_Values( this : Attributes'Class ) return Map_Value is
        use Attribute_Maps;
        pos    : Attribute_Maps.Cursor;
        values : constant Map_Value := Create_Map.Map;
    begin
        pos := this.names.First;
        while Has_Element( pos ) loop
            if this.names.Constant_Reference( pos ).component = null then
                values.Set( Key( pos ), this.names.Constant_Reference( pos ).val, consume => True );
            end if;
            Next( pos );
        end loop;
        return values;
    end Get_Local_Values;

    ----------------------------------------------------------------------------

    overriding
    function Get_Namespace_Name( this : access Attributes;
                                 name : String ) return Value is
        use Attribute_Maps;
        pos : constant Attribute_Maps.Cursor := this.names.Find( name );
    begin
        if Has_Element( pos ) then
            return Value(Element( pos ).ref.Deref);
        end if;
        return Null_Value;
    end Get_Namespace_Name;

    ----------------------------------------------------------------------------

    overriding
    function Get_Namespace_Ref( this : access Attributes;
                                name : String ) return Value is
        use Attribute_Maps;
        pos : Attribute_Maps.Cursor := this.names.Find( name );
    begin
        if not Has_Element( pos ) then
            -- a reference by a script at runtime to an entity attribute that
            -- does not exist yet will create the locally owned attribute with a
            -- Null value. changes will be automatically notified and visible to
            -- the Game_View.
            pos := this.Define_Attribute( name    => name,
                                          initial => Null_Value,
                                          consume => True );   -- consuming Null_Value
        end if;
        return Element( pos ).exRef;
    end Get_Namespace_Ref;

    ----------------------------------------------------------------------------

    procedure Notify_Changed( this : not null access Attributes'Class;
                              comp : not null A_Component;
                              name : String ) is
        use Attribute_Maps;
        pos : constant Attribute_Maps.Cursor := this.names.Find( name );
    begin
        if Has_Element( pos ) then
            if this.names.Constant_Reference( pos ).component = comp then
                this.Dispatch_Notification( name,
                                            this.names.Constant_Reference( pos ).ref.Deref,
                                            this.names.Constant_Reference( pos ).scope );
                this.Notify_World_Modified;
            end if;
        end if;
    end Notify_Changed;

    ----------------------------------------------------------------------------

    procedure Notify_World_Modified( this : not null access Attributes'Class ) is
    begin
        -- notify the view that the world's state has been modified (because the
        -- state of this entity has been modified). suppress the notification if
        -- the entity is not yet (or no longer) associated with the ECS.
        if this.Get_Owner.Get_System /= null then
            Queue_World_Modified;
        end if;
    end Notify_World_Modified;

    ----------------------------------------------------------------------------

    overriding
    function Object_Input( stream : access Root_Stream_Type'Class ) return Attributes is
        this : aliased Attributes;
    begin
        this.Construct;
        Attributes'Read( stream, this );
        return this;
    end Object_Input;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Attributes ) is
    begin
        Component'Read( stream, Component(obj) );

        -- the values read during construction of the object require a pointer
        -- to 'this', but it hasn't finished being allocated yet. So, the values
        -- are read here but set as attributes in On_Added, after we know the
        -- object has been fully constructed.
        obj.storedVals := Value_Input( stream ).Map;
    end Object_Read;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Attributes ) is
    begin
        Component'Write( stream, Component(obj) );
        Value_Output( stream, obj.Get_Local_Values );
    end Object_Write;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Added( this : access Attributes ) is

        procedure Define_Attribute( key : String; val : Value ) is
            pos : Attribute_Maps.Cursor;
            pragma Warnings( Off, pos );
        begin
            -- defining an attribute that was locally owned and streamed back in
            pos := this.Define_Attribute( name    => key,
                                          initial => val,
                                          consume => True );  -- consuming from .storedVals
        end Define_Attribute;

    begin
        -- if the object was streamed in, now it's time to define the locally
        -- stored attributes read from the stream.
        if this.storedVals.Valid then
            this.storedVals.Iterate( Define_Attribute'Access );
            this.storedVals := As_Map( Null_Value );       -- erase it
        end if;
    end On_Added;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Enter_System( this : access Attributes ) is
    begin
        -- define the API now that the component has entered the Entity_System
        -- and has access to the game's script VM.
        this.Define_Api;
    end On_Enter_System;

    ----------------------------------------------------------------------------

     function Read_Attribute( this : not null access Attributes'Class;
                              name : String ) return Value is
        use Attribute_Maps;
         pos : Attribute_Maps.Cursor := this.names.Find( name );
     begin
         if not Has_Element( pos ) then
             -- this covers the 1% case where a script maintains a reference
             -- Value to an entity attribute but the attribute no longer exists.
             --
             -- Just like Get_Namespace_Ref(),
             --
             --   a reference by a script at runtime to an entity attribute that
             --   does not exist yet will create the locally owned attribute
             --   with a Null value. changes will be automatically notified and
             --   visible to the Game_View.
            pos := this.Define_Attribute( name    => name,
                                          initial => Null_Value,
                                          consume => True );   -- consuming Null_Value
         end if;

         return Value(this.names.Constant_Reference( pos ).ref.Deref);
     end Read_Attribute;

    ----------------------------------------------------------------------------

    procedure Register( this      : not null access Attributes'Class;
                        name      : String;
                        comp      : not null A_Component;
                        reference : Value'Class;
                        scope     : Notify_Scope := Silent_Scope ) is
        pos      : Attribute_Maps.Cursor;
        inserted : Boolean;
        def      : A_Attribute_Def := new Attribute_Def'(component => comp,
                                                         exRef     => Attribute_Refs.Create_Indirect( this,
                                                                                                      Read_Attribute'Access,
                                                                                                      Write_Attribute'Access,
                                                                                                      name ),
                                                         ref       => Value(reference),
                                                         val       => Null_Value,
                                                         scope     => scope);
    begin
        this.names.Insert( name, def, pos, inserted );
        if not inserted then
            if this.names.Constant_Reference( pos ).component = null then
                -- the attribute was defined by a script- override it
                Free( this.names.Reference( pos ) );
                this.names.Replace_Element( pos, def );
                inserted := True;
            else
                -- the attribute was already registered by a component
                Free( def );
            end if;
        end if;

        if inserted then
            -- send an initial attribute notification to let the Game_View know
            -- that the attribute exists now. 'comp' will be responsible for
            -- calling Notify_Changed() when its value changes in the future.
            this.Dispatch_Notification( name,
                                        this.names.Constant_Reference( pos ).ref.Deref,
                                        this.names.Constant_Reference( pos ).scope );
        end if;
    end Register;

    ----------------------------------------------------------------------------

    procedure Set_Defaults( this     : not null access Attributes'Class;
                            defaults : Map_Value ) is

        procedure Set_Default( name : String; val : Value ) is
            use Attribute_Maps;
            pos : Attribute_Maps.Cursor := this.names.Find( name );
        begin
            if not Has_Element( pos ) then
                -- define the attribute because it doesn't exist, setting the
                -- value to the given default. it will be locally stored and the
                -- Global_Scope will be notified of value changes.
                pos := this.Define_Attribute( name    => name,
                                              initial => val,
                                              consume => False );
            elsif this.names.Constant_Reference( pos ).component = null and then
                  this.names.Constant_Reference( pos ).ref.Deref.Is_Null
            then
                -- set the attribute's value to the default because it is a
                -- locally stored attribute and it doesn't already have a value.
                this.names.Reference( pos ).ref.Replace( Clone( val ) );
            end if;
        end Set_Default;

    begin
        defaults.Iterate( Set_Default'Access );
    end Set_Defaults;

    ----------------------------------------------------------------------------

    procedure Set_Value( this : not null access Attributes'Class;
                         name : String;
                         val  : Value'Class ) is
        use Attribute_Maps;
        pos : Attribute_Maps.Cursor := this.names.Find( name );
    begin
        if not Has_Element( pos ) then
            -- define the locally stored attribute. this Set_Value() is called
            -- as an entity is created by the factory, and when a
            -- Set_Entity_Attribute event from the Game_View is handled at
            -- runtime.
            pos := this.Define_Attribute( name    => name,
                                          initial => Null_Value,
                                          consume => True );   -- consuming Null_Value

            -- if the attribute is being assigned a function, suppress all value
            -- changed notifications. this attribute is being used as a scripting
            -- interface to the entity, not as a part of the entity state.
            if val.Is_Function then
                Element( pos ).scope := Silent_Scope;
            end if;
        end if;

        -- this method sends change notifications
        this.Write_Attribute( name, val, pos );
    end Set_Value;

    ----------------------------------------------------------------------------

    procedure Unregister( this : not null access Attributes'Class;
                          name : String;
                          comp : not null A_Component ) is
        use Attribute_Maps;
        pos   : Attribute_Maps.Cursor := this.names.Find( name );
        scope : Notify_Scope;
    begin
        if Has_Element( pos ) then
            if this.names.Constant_Reference( pos ).component = comp then
                scope := this.names.Constant_Reference( pos ).scope;
                Free( this.names.Reference( pos ) );
                this.names.Delete( pos );
                this.Dispatch_Notification( name, Null_Value, scope );
            end if;
        end if;
    end Unregister;

    ----------------------------------------------------------------------------

    procedure Unregister_All( this : not null access Attributes'Class;
                              comp : not null A_Component ) is
        use Attribute_Maps;
        kept    : Attribute_Maps.Map;
        removed : Attribute_Maps.Map;
        pos     : Attribute_Maps.Cursor := this.names.First;
        def     : A_Attribute_Def;
        scope   : Notify_Scope;
    begin
        -- unregister the attributes by building a new map that does not contain
        -- the attributes registered by 'comp'. this avoids deleting during
        -- iteration, or iterating repeatedly after each delete.
        while Has_Element( pos ) loop
            def := Element( pos );
            if def.component /= comp then
                kept.Insert( Key( pos ), def );
            else
                removed.Insert( Key( pos ), def );
            end if;
            Next( pos );
        end loop;

        if not removed.Is_Empty then
            this.names.Move( kept );
            pos := removed.First;
            while Has_Element( pos ) loop
                scope := removed.Reference( pos ).scope;
                Free( removed.Reference( pos ) );
                this.Dispatch_Notification( Key( pos ), Null_Value, scope );
                Next( pos );
            end loop;
        end if;
    end Unregister_All;

    ----------------------------------------------------------------------------

    procedure Write_Attribute( this : not null access Attributes'Class;
                               name : String;
                               val  : Value'Class ) is
        use Attribute_Maps;
        pos : constant Attribute_Maps.Cursor := this.names.Find( name );
    begin
        if Has_Element( pos ) then
            this.Write_Attribute( name, val, pos );
        else
            -- there shouldn't be any references to registered attributes that
            -- aren't currently registered, unless the attribute has since been
            -- unregistered. ignore the write instead of defining a new locally
            -- stored attribute, because that was probably not the user's intent.
            Dbg( "Attempted to use stale entity attribute reference",
                 D_ENTITY or D_SCRIPT, Warning );
        end if;
    end Write_Attribute;

    ----------------------------------------------------------------------------

    procedure Write_Attribute( this : not null access Attributes'Class;
                               name : String;
                               val  : Value'Class;
                               pos  : Attribute_Maps.Cursor ) is
        use Attribute_Maps;
        oldVal : Value;
    begin
        oldVal := Value(this.names.Constant_Reference( pos ).ref.Deref);

        -- write the value
        this.names.Constant_Reference( pos ).ref.Replace( Clone( val ) );

        -- if the value change notifications are enabled, then when it changes,
        -- an AttributeChanged message will be dispatched and the view will be
        -- notified with an Entity_Attribute_Changed event.
        if val.Compare( oldVal ) /= 0 then
            this.Dispatch_Notification( name, val, this.names.Constant_Reference( pos ).scope );
            this.Notify_World_Modified;
        end if;
    end Write_Attribute;

    --==========================================================================

    procedure Define_Component( factory : not null A_Entity_Factory ) is
    begin
        factory.Define_Component( Family_Name( ATTRIBUTES_ID ),
                                  Create_Attributes'Access,
                                  propNames =>
                                      (1 => Create( "attributes" )) );
    end Define_Component;

end Entities.Entity_Attributes;
