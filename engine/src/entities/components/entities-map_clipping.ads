--
-- Copyright (c) 2014-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Directions;                        use Directions;
with Entities.Components;               use Entities.Components;

package Entities.Map_Clipping is

    -- This message is sent to the component's entity when it collides with a
    -- solid tile or a world boundary line. The 'dir' parameter indicates which
    -- direction the entity was moving when it was blocked. Possible values of
    -- 'dir' are: "up", "down", "left", "right".
    --
    -- HitWall {"dir": <direction>}
    MSG_HitWall : constant Hashed_String := To_Hashed_String( "HitWall" );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Registered Entity Attributes:
    --
    -- Name             Read/Write   Type      Description
    -- ----             ----------   ----      -----------
    -- isBlockedLeft    read         boolean   Is the entity blocked to its left?
    -- isBlockedRight   read         boolean   Is the entity blocked to its right?
    -- isBlockedUp      read         boolean   Is the entity blocked in the upward direction?
    -- isBlockedDown    read         boolean   Is the entity blocked in the downward direction?

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    MAP_CLIPPED_ID : Family_Id := To_Family_Id( "MapClipped" );

    ----------------------------------------------------------------------------

    type Map_Clipped is abstract new Component with private;
    type A_Map_Clipped is access all Map_Clipped'Class;

    overriding
    function Get_Dependency_Order( this : access Map_Clipped ) return Natural is (DEPS_MAPCLIP);

    overriding
    function Get_Family( this : access Map_Clipped ) return Family_Id is (MAP_CLIPPED_ID);

    overriding
    function Get_Tick_Order( this : access Map_Clipped ) return Natural is (TCK_MAPCLIP);

    -- Returns True if the entity is blocked from moving in the direction 'dir'
    -- by a solid tile.
    function Is_Blocked( this : not null access Map_Clipped'Class; dir : Cardinal_Direction ) return Boolean;

    -- Returns True if the entity's movement is blocked in the downward
    -- direction. Generally, this indicates an entity is sitting on the ground
    -- after landing under the effects of gravity.
    function Is_Grounded( this : not null access Map_Clipped'Class ) return Boolean is (this.Is_Blocked( Down )) with Inline;

    -- Updates the entity's Movement and Velocity components by clipping its
    -- location to solid tiles in the map.
    procedure Tick( this : access Map_Clipped; time : Tick_Time ) is abstract;

private

    type Map_Clipped is abstract new Component with
       record
           blocked : Direction_Booleans := (others => False);
       end record;

    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Map_Clipped );
    for Map_Clipped'Read use Object_Read;

    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Map_Clipped );
    for Map_Clipped'Write use Object_Write;

    -- Registers names:
    --     isBlockedLeft (read only)
    --     isBlockedRight (read only)
    --     isBlockedUp (read only)
    --     isBlockedDown (read only)
    procedure On_Added( this : access Map_Clipped );

end Entities.Map_Clipping;
