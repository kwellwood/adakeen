--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Clipping;                          use Clipping;
--with Debugging;                         use Debugging;
with Directions;                        use Directions;
with Entities.Locations;                use Entities.Locations;
with Entities.Movements;                use Entities.Movements;
with Entities.Systems;                  use Entities.Systems;
with Support;                           use Support;
with Values.Strings;                    use Values.Strings;
with Vector_Math;                       use Vector_Math;
with Worlds;                            use Worlds;

package body Entities.Map_Clipping.Platform is

    function Create_Platform_Clipped( properties : Map_Value ) return A_Component is
        pragma Unreferenced( properties );
        this : constant A_Platform_Clipped := new Platform_Clipped;
    begin
        this.Construct;
        return A_Component(this);
    end Create_Platform_Clipped;

    ----------------------------------------------------------------------------

    overriding
    function Object_Input( stream : access Root_Stream_Type'Class ) return Platform_Clipped is
        this : aliased Platform_Clipped;
    begin
        this.Construct;
        Platform_Clipped'Read( stream, this );
        return this;
    end Object_Input;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Platform_Clipped ) is
    begin
        Map_Clipped'Read( stream, Map_Clipped(obj) );
    end Object_Read;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Platform_Clipped ) is
    begin
        Map_Clipped'Write( stream, Map_Clipped(obj) );
    end Object_Write;

    ----------------------------------------------------------------------------

    overriding
    procedure Tick( this : access Platform_Clipped; time : Tick_Time ) is
        pragma Unreferenced( time );
        world      : constant A_World := this.Get_Owner.Get_System.Get_World;
        tileWidth  : constant Positive := world.Tile_Width;
        fTileWidth : constant Float := Float(tileWidth);
        mov        : constant A_Movement := A_Movement(this.Get_Component( MOVEMENT_ID ));
        loc        : constant A_Location := A_Location(this.Get_Component( LOCATION_ID ));
        initialX   : constant Float := loc.Get_X;
        initialY   : constant Float := loc.Get_Y;
        prevX      : constant Float := loc.Get_Prev_X;
        prevY      : constant Float := loc.Get_Prev_Y;
        width      : constant Natural := loc.Get_Width;
        height     : constant Natural := loc.Get_Height;
        halfWidth  : constant Float := Float(width) / 2.0;
        halfHeight : constant Float := Float(height) / 2.0;

        type Axis_Type is (X_Axis, Y_Axis);

        ------------------------------------------------------------------------

        procedure Notify_Hit_Wall( dir : Cardinal_Direction ) is
        begin
            this.Get_Owner.Dispatch_Message( MSG_HitWall, Maps.Create( (1=>Pair("dir", Create( Lower( dir'Img ) ))) ).Map );
        end Notify_Hit_Wall;

        ------------------------------------------------------------------------

        procedure Clip_Left_Edge( x, y      : in out Float;
                                  velocity  : in out Vec2;
                                  collision : out Boolean ) is
            leftX     : constant Float := x - halfWidth;
            col       : constant Integer := Integer(Float'Floor(leftX / fTileWidth));
            midCol    : constant Integer := Integer(Float'Floor(x / fTileWidth));
            topRow    : Integer;
            botRow    : Integer;
            ignoreRow : Boolean;
            clipType  : Clip_Type;
            slopeY    : Float;
            edgeY     : Float;
        begin
            collision := False;
            if col < 0 then
                collision := True;
                return;
            end if;

            topRow := Integer(Float'Floor((y - halfHeight) / fTileWidth));
            botRow := Integer(Float'Floor(Float'Ceiling(y + halfHeight - 1.0) / fTileWidth));

            for row in topRow..botRow loop
                ignoreRow := False;

                if row = topRow then
                    clipType := world.Get_Tile_Clip_Type( Integer(Float'Floor(x / fTileWidth)), row );
                    if Is_Slope_D( clipType ) then
                        -- the top of the entity is moving down a D slope
                        -- ignore the tile in the top row of the left edge
                        ignoreRow := True;
                    end if;
                end if;
                if not ignoreRow and then row = botRow then
                    clipType := world.Get_Tile_Clip_Type( Integer(Float'Floor(x / fTileWidth)), row );
                    if Is_Slope_B( clipType ) then
                        -- the bottom of the entity is moving up a B slope
                        -- ignore the tile in the bottom row of the left edge
                        ignoreRow := True;
                    end if;
                end if;

                if not ignoreRow then
                    clipType := world.Get_Tile_Clip_Type( col, row );
                    if clipType = Wall then
                        collision := True;
                        x := Float((col + 1) * tileWidth) + halfWidth;
                        velocity.x := Float'Max( 0.0, velocity.x );
                        return;
                    end if;
                end if;
            end loop;

            -- move the top edge down, sliding down a D slope to the left
            clipType := world.Get_Tile_Clip_Type( midCol, topRow );
            if Is_Slope_D( clipType ) then
                slopeY := Slope_Y( clipType, x, midCol, topRow, tileWidth );
                edgeY := y - halfHeight;
                if edgeY < slopeY then
                    y := slopeY + halfHeight;
                    velocity.y := Float'Max( 0.0, velocity.y );
                    return;
                end if;
            end if;

            -- move the bottom edge up, sliding up a B slope to the left
            clipType := world.Get_Tile_Clip_Type( midCol, botRow );
            if Is_Slope_B( clipType ) then
                slopeY := Slope_Y( clipType, x, midCol, botRow, tileWidth );
                edgeY := y + halfHeight - 1.0;
                if edgeY + 1.0 > slopeY then
                    y := (slopeY - halfHeight + 1.0) - 1.0;
                    velocity.y := Float'Min( 0.0, velocity.y );
                    return;
                end if;
            end if;
        end Clip_Left_Edge;

        ------------------------------------------------------------------------

        procedure Clip_Right_Edge( x, y      : in out Float;
                                   velocity  : in out Vec2;
                                   collision : out Boolean ) is
            rightX    : constant Float := x + halfWidth - 1.0;
            col       : constant Integer := Integer(Float'Floor(Float'Ceiling(rightX) / fTileWidth));
            midCol    : constant Integer := Integer(Float'Floor(x / fTileWidth));
            topRow    : Integer;
            botRow    : Integer;
            ignoreRow : Boolean;
            clipType  : Clip_Type;
            slopeY    : Float;
            edgeY     : Float;
        begin
            collision := False;
            if col < 0 or else col >= world.Get_Map_Columns then
                collision := True;
                return;
            end if;

            topRow := Integer(Float'Floor((y - halfHeight) / fTileWidth));
            botRow := Integer(Float'Floor(Float'Ceiling(y + halfHeight - 1.0) / fTileWidth));

            for row in topRow..botRow loop
                ignoreRow := False;

                if row = topRow then
                    clipType := world.Get_Tile_Clip_Type( Integer(Float'Floor(x / fTileWidth)), row );
                    if Is_Slope_C( clipType ) then
                        -- the top of the entity is moving down a C slope
                        -- ignore the tile in the top row of the right edge
                        ignoreRow := True;
                    end if;
                end if;
                if not ignoreRow and then row = botRow then
                    clipType := world.Get_Tile_Clip_Type( Integer(Float'Floor(x / fTileWidth)), row );
                    if Is_Slope_A( clipType ) then
                        -- the bottom of the entity is moving up an A slope
                        -- ignore the tile in the bottom row of the right edge
                        ignoreRow := True;
                    end if;
                end if;

                if not ignoreRow then
                    clipType := world.Get_Tile_Clip_Type( col, row );
                    if clipType = Wall then
                        collision := True;
                        x := Float(col * tileWidth) - halfWidth;
                        velocity.x := Float'Min( 0.0, velocity.x );
                        return;
                    end if;
                end if;
            end loop;

            -- move the bottom edge up, following an A slope to the right
            clipType := world.Get_Tile_Clip_Type( midCol, botRow );
            if Is_Slope_A( clipType ) then
                slopeY := Slope_Y( clipType, x, midCol, botRow, tileWidth );
                edgeY := y + halfHeight - 1.0;
                if edgeY + 1.0 > slopeY then
                    y := (slopeY - halfHeight + 1.0) - 1.0;
                    velocity.y := Float'Min( 0.0, velocity.y );
                    return;
                end if;
            end if;

            -- move the top edge down, following a C slope to the right
            clipType := world.Get_Tile_Clip_Type( midCol, topRow );
            if Is_Slope_C( clipType ) then
                slopeY := Slope_Y( clipType, x, midCol, topRow, tileWidth );
                edgeY := y - halfHeight;
                if edgeY < slopeY then
                    y := slopeY + halfHeight;
                    velocity.y := Float'Max( 0.0, velocity.y );
                    return;
                end if;
            end if;
        end Clip_Right_Edge;

        ------------------------------------------------------------------------

        procedure Clip_Top_Edge( x         : Float;
                                 y         : in out Float;
                                 velocity  : in out Vec2;
                                 collision : out Boolean ) is
            topY     : constant Float := y - halfHeight;
            row      : constant Integer := Integer(Float'Floor(topY / fTileWidth));
            midCol   : Integer;
            leftCol  : Integer;
            rightCol : Integer;
            clipType : Clip_Type;
            slopeY   : Float;
            edgeY    : Float;
        begin
            collision := False;
            if row < 0 then
                collision := True;
                return;
            end if;

            midCol := Integer(Float'Floor(x / fTileWidth));
            leftCol := Integer(Float'Floor((x - halfWidth) / fTileWidth));
            rightCol := Integer(Float'Floor(Float'Ceiling(x + halfWidth - 1.0) / fTileWidth));

            -- adjust the top edge down, out of a C or D slope
            clipType := world.Get_Tile_Clip_Type( midCol, row );
            if Is_Slope_C( clipType ) or else Is_Slope_D( clipType ) then
                slopeY := Slope_Y( clipType, x, midCol, row, tileWidth );
                edgeY := y - halfHeight;
                if edgeY < slopeY then
                    collision := True;
                    y := slopeY + halfHeight;
                    velocity.y := Float'Max( 0.0, velocity.y );
                end if;

                -- the slope in the center overrides any other blocking tiles
                -- in the top row.
                return;
            end if;

            for col in leftCol..rightCol loop
                clipType := world.Get_Tile_Clip_Type( col, row );

                if clipType = Wall then
                    collision := True;
                    y := Float((row + 1) * tileWidth) + halfHeight;
                    velocity.y := Float'Max( 0.0, velocity.y );
                    return;
                end if;
            end loop;
        end Clip_Top_Edge;

        ------------------------------------------------------------------------

        procedure Clip_Bottom_Edge( x         : Float;
                                    y         : in out Float;
                                    velocity  : in out Vec2;
                                    collision : out Boolean ) is
            botY     : constant Float := y + halfHeight - 1.0;
            row      : constant Integer := Integer(Float'Floor(Float'Ceiling(botY) / fTileWidth));
            midCol   : Integer;
            leftCol  : Integer;
            rightCol : Integer;
            clipType : Clip_Type;
            slopeY   : Float;
            edgeY    : Float;
        begin
            collision := False;
            if row < 0 or else row >= world.Get_Map_Rows then
                collision := True;
                return;
            end if;

            midCol := Integer(Float'Floor(x / fTileWidth));
            leftCol := Integer(Float'Floor((x - halfWidth) / fTileWidth));
            rightCol := Integer(Float'Floor(Float'Ceiling(x + halfWidth - 1.0) / fTileWidth));

            -- adjust the bottom edge up, out of an A or B slope
            clipType := world.Get_Tile_Clip_Type( midCol, row );
            if Is_Slope_A( clipType ) or else Is_Slope_B( clipType ) then
                slopeY := Slope_Y( clipType, x, midCol, row, tileWidth );
                edgeY := y + halfHeight - 1.0;
                if edgeY + 1.0 > slopeY then
                    collision := True;
                    y := (slopeY - halfHeight + 1.0) - 1.0;
                    velocity.y := Float'Min( 0.0, velocity.y );
                end if;

                -- the slope in the center overrides any other blocking tiles
                -- in the bottom row.
                return;
            end if;

            for col in leftCol..rightCol loop
                clipType := world.Get_Tile_Clip_Type( col, row );

                if clipType = Wall or else
                   (
                       clipType = OneWay and then
                       -- for the bottom edge to collide with a OneWay tile, the
                       -- entity must be moving down and must have previously
                       -- been above the oneway floor tile.
                       velocity.y >= 0.0 and then
                       row > Integer(Float'Floor(Float'Ceiling(prevY + halfHeight - 1.0) / fTileWidth))
                   )
                then
                    collision := True;
                    y := Float(row * tileWidth) - halfHeight;
                    velocity.y := Float'Min( 0.0, velocity.y );
                    exit;
                end if;
            end loop;
        end Clip_Bottom_Edge;

        ------------------------------------------------------------------------

        -- Performs clipping in the X axis, clipping the left or right edge,
        -- depending on which direction the entity moved in the X axis. No
        -- clipping will be performed if the entity didn't move in the X axis.
        --
        -- Due to the behavior of slopes, the entity may be shifted in the Y
        -- axis if it moves left or right into a slope.
        procedure Clip_X( x, y      : in out Float;
                          velocity  : in out Vec2;
                          collision : out Boolean ) is
        begin
            collision := False;
            if initialX < prevX then
                Clip_Left_Edge( x, y, velocity, collision );
            elsif initialX > prevX then
                Clip_Right_Edge( x, y, velocity, collision );
            end if;
        end Clip_X;

        ------------------------------------------------------------------------

        -- Performs clipping in the Y axis, clipping the top or bottom edge,
        -- depending on which direction the entity moved in the Y axis. No
        -- clipping will be performed if the entity didn't move in the Y axis.
        procedure Clip_Y( x         : Float;
                          y         : in out Float;
                          velocity  : in out Vec2;
                          collision : out Boolean ) is
        begin
            collision := False;
            if initialY < prevY then
                Clip_Top_Edge( x, y, velocity, collision );
            elsif initialY > prevY then
                Clip_Bottom_Edge( x, y, velocity, collision );
            end if;
        end Clip_Y;

        ------------------------------------------------------------------------

        -- Apply clipping in both the X and Y axis, using 'primary' as the axis
        -- to clip first. If collisions are detected on both axes, this
        -- procedure will recurse just one level to swap the primary axis and
        -- try again.
        procedure Clip_X_and_Y( x, y       : in out Float;
                                velocity   : in out Vec2;
                                primary    : Axis_Type;
                                swapOnFail : Boolean := True ) is
            xOld       : constant Float := x;
            yOld       : constant Float := y;
            vOld       : constant Vec2 := velocity;
            xCollision,
            yCollision : Boolean;
            ignored    : Boolean;
        begin
            if primary = X_Axis then
                Clip_X( x, y, velocity, xCollision );
                Clip_Y( x, y, velocity, yCollision );
            else
                Clip_Y( x, y, velocity, yCollision );
                Clip_X( x, y, velocity, xCollision );
            end if;
            if yCollision then
                -- clip Y one more time; it could be behind a slope
                Clip_Y( x, y, velocity, ignored );
            end if;

            if swapOnFail and then xCollision and then yCollision then
                x := xOld;
                y := yOld;
                velocity := vOld;
                Clip_X_and_Y( x, y, velocity,
                              (if primary = X_Axis then Y_Axis else X_Axis),
                              swapOnFail => False );
            end if;
        end Clip_X_and_Y;

        ------------------------------------------------------------------------

        -- Detects if the entity's movement is blocked in direction 'dir' if
        -- its location is 'x', 'y'.
        function Is_Blocked( dir : Cardinal_Direction; x, y : Float ) return Boolean is
            vTmp       : Vec2;
            xTmp, yTmp : Float;
            blocked    : Boolean;
        begin
            case dir is
                when Left =>
                    xTmp := x - 0.01; yTmp := y;
                    Clip_Left_Edge( xTmp, yTmp, vTmp, blocked );
                when Right =>
                    xTmp := x + 0.01; yTmp := y;
                    Clip_Right_Edge( xTmp, yTmp, vTmp, blocked );
                when Up =>
                    xTmp := x; yTmp := y - 0.01;
                    Clip_Top_Edge( xTmp, yTmp, vTmp, blocked );
                when Down =>
                    xTmp := x; yTmp := y + 0.01;
                    Clip_Bottom_Edge( xTmp, yTmp, vTmp, blocked );
            end case;
            return blocked;
        end Is_Blocked;

        ------------------------------------------------------------------------

        -- Attempts to guess the primary axis for clipping. Sometimes we want to
        -- clip X before Y (e.g. sliding down against a vertical wall), and
        -- sometimes we want to clip Y before X (e.g. walking along the ground.)
        --
        -- The guess is based on a comparison of the distances of the entity's
        -- edges (in the direction of movement) from the nearest tile boundary.
        -- For example, if the entity is moving left and downward and the left
        -- edge is slightly to the left of a tile column boundary and the bottom
        -- edge is halfway between tile row boundaries, then the X axis will be
        -- the primary clipping axis because it's likely that the left edge
        -- crossed a tile boundary more recently than the bottom edge.
        function Guess_Primary_Clip_Axis( x, y : Float ) return Axis_Type is
            edgeX, edgeY : Float;
            distX, distY : Float;
        begin
            if x = prevX then
                return Y_Axis;
            elsif y = prevY then
                return X_Axis;
            end if;

            if x < prevX then
                edgeX := x - halfWidth;
                distX := (Float'Floor( edgeX / fTileWidth ) + 1.0) * fTileWidth - edgeX;
            else -- x > prevX
                edgeX := Float'Ceiling(x + halfWidth - 1.0);
                distX := edgeX - Float'Floor(edgeX / fTileWidth) * fTileWidth;
            end if;

            if y < prevY then
                edgeY := y - halfHeight;
                distY := (Float'Floor( edgeY / fTileWidth ) + 1.0) * fTileWidth - edgeY;
            else -- y > prevY
                edgeY := Float'Ceiling(y + halfHeight - 1.0);
                distY := edgeY - Float'Floor(edgeY / fTileWidth) * fTileWidth;
            end if;

            return (if distX < distY then X_Axis else Y_Axis);
        end Guess_Primary_Clip_Axis;

        ------------------------------------------------------------------------

        -- Checks if the entity could be moving down a slope, and attempts to
        -- move the entity down by a small amount (STICKY_DISTANCE_Y), forcing
        -- it back into contact with the ground.
        --
        -- This procedure doesn't explicitly check for a slope; it simply checks
        -- if the entity is moving down (because of gravity) and to the left or
        -- right. On a slope, moving in the downward direction of the slope
        -- causes it to comie off the ground when the X displacement exceeds
        -- gravity's Y displacement.
        --
        -- If the entity's bottom edge is 1/4 of a tile width or less above the
        -- ground, this procedure will stick it back down.
        procedure Stick_To_Slopes( x        : Float;
                                   y        : in out Float;
                                   velocity : in out Vec2 ) is
            STICKY_DISTANCE_Y : constant Float := Float(tileWidth / 4);
            groundedY : Float;
            groundedV : Vec2;
            collision : Boolean;
        begin
            if this.blocked(Down) and then not Is_Blocked( Down, x, y ) then
                -- only try to re-ground the entity if it's moving down and it's
                -- moving left or right (which is what causes a loss of
                -- grounding on a slope).
                if prevY < initialY and then initialX /= prevX then
                    groundedY := y + STICKY_DISTANCE_Y;
                    groundedV := velocity;
                    Clip_Y( x, groundedY, groundedV, collision );
                    if collision then
                        y := groundedY;
                        velocity.y := groundedV.y;   -- will be 0.0
                        Clip_Y( x, y, velocity, collision );
                    end if;
                end if;
            end if;
        end Stick_To_Slopes;

        ------------------------------------------------------------------------

        -- Constrains the entity to the map's boundaries, stopping its movement
        -- when the center of the entity reaches the edge of the map.
        procedure Clip_To_Boundaries( x, y : in out Float; velocity : in out Vec2 ) is
        begin
            -- stay within map boundaries
            if x < 0.0 then
                x := 0.0;
                velocity.x := Float'Max( 0.0, velocity.x );
            elsif Float'Ceiling(x) >= Float(world.Get_Width) then
                x := Float(world.Get_Width) - 1.0;
                if velocity.x > 0.0 then
                    velocity.x := 0.0;
                else
                    -- this happens when an object is teleported outside of the
                    -- boundaries, probably because the ked user dragged it out.
                    -- subtract a small "fuzzy" amount from the exact edge of
                    -- the map so that an Entity_Moved event will always be
                    -- sent to the view. this prevents the entity from looking
                    -- like it's stuck out of bounds wherever it was dropped
                    -- simply because it was clipped back to the same location
                    -- it was dragged from, which would avoid the Entity_Moved
                    -- event. typically this is only a problem when the object
                    -- is dragged out in both the X and Y axis, but solving in
                    -- just the X will fix it. don't mess with the Y axis
                    -- because of gravity.
                    x := x - 0.01 * Random_Float;
                end if;
            end if;
            if y < 0.0 then
                y := 0.0;
                velocity.y := Float'Max( 0.0, velocity.y );
            elsif Float'Ceiling(y) >= Float(world.Get_Height) then
                y := Float(world.Get_Height) - 1.0;
                velocity.y := Float'Min( 0.0, velocity.y );
            end if;
        end Clip_To_Boundaries;

        ------------------------------------------------------------------------

        procedure Update_Blocked_State( x, y : Float ) is
            blocked : Boolean;
        begin
            blocked := Is_Blocked( Left, x, y );
            if blocked and then not this.blocked(Left) then
                Notify_Hit_Wall( Left );
            end if;
            this.blocked(Left) := blocked;

            blocked := Is_Blocked( Right, x, y );
            if blocked and then not this.blocked(Right) then
                Notify_Hit_Wall( Right );
            end if;
            this.blocked(Right) := blocked;

            blocked := Is_Blocked( Up, x, y );
            if blocked and then not this.blocked(Up) then
                Notify_Hit_Wall( Up );
            end if;
            this.blocked(Up) := blocked;

            blocked := Is_Blocked( Down, x, y );
            if blocked and then not this.blocked(Down) then
                Notify_Hit_Wall( Down );
            end if;
            this.blocked(Down) := blocked;
        end Update_Blocked_State;

        ------------------------------------------------------------------------

        x   : Float := loc.Get_X;
        y   : Float := loc.Get_Y;
        vel : Vec2 := mov.Get_V;
    begin
        Clip_X_and_Y( x, y, vel, Guess_Primary_Clip_Axis( x, y ) );
        Stick_To_Slopes( x, y, vel );
        Clip_To_Boundaries( x, y, vel );
        Update_Blocked_State( x, y );

        loc.Set_XY( x, y );
        mov.Set_V( vel );
    end Tick;

    --==========================================================================

    procedure Define_Component( factory : not null A_Entity_Factory ) is
    begin
        factory.Define_Component( "PlatformClipped",
                                  Create_Platform_Clipped'Access,
                                  required =>
                                      (Create( Family_Name( LOCATION_ID ) ),
                                       Create( Family_Name( MOVEMENT_ID ) )) );
    end Define_Component;

end Entities.Map_Clipping.Platform;

