--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Entities.Systems;                  use Entities.Systems;
with Entities.Visibles;                 use Entities.Visibles;
with Values.Casting;                    use Values.Casting;
with Values.Construction;               use Values.Construction;
with Values.Streaming;                  use Values.Streaming;
with Values.Strings;                    use Values.Strings;

package body Entities.Animations is

    function Create_Animation( properties : Map_Value ) return A_Component is
        this : constant A_Animation := new Animation;
    begin
        this.Construct;

        -- if a non-empty 'frames' list is given, then use the given animation
        -- info. otherwise, the animation info will be read from the current
        -- frame's tile properties in On_Added.
        this.frames := Clone( properties.Get( "frames" ) ).Lst;
        if this.frames.Valid and then this.frames.Length > 0 then
            this.frameIds := new Tile_Id_Array(1..this.frames.Length);

            -- set the frame delay; duration overrides delay if both are given
            if properties.Get_Int( "duration" ) > 0 and then this.frameIds'Length > 0 then
                this.frameDelay := Milliseconds( properties.Get_Int( "duration" ) ) / this.frameIds'Length;
            else
                this.frameDelay := Milliseconds( properties.Get_Int( "delay" ) );
            end if;
            this.looped := properties.Get_Boolean( "looped" );
            this.global := this.looped and then properties.Get_Boolean( "global" );
        else
            this.frames := Null_Value.Lst;
        end if;

        return A_Component(this);
    end Create_Animation;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Animation ) is
    begin
        Delete( this.frameIds );
        Component(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Calculate_Frame( this : not null access Animation'Class ) return Natural is
        vis      : constant A_Visible := A_Visible(this.Get_Component( VISIBLE_ID ));
        anmTimer : Time_Span := this.Get_Age - this.anmStart;
    begin
        pragma Assert( this.Get_Owner /= null, "Calculate_Frame called without owner" );
        if this.global and then this.Get_Owner.Get_System /= null then
            anmTimer := this.Get_Owner.Get_System.Get_Global_Timer;
        end if;

        if this.frameIds /= null then

            -- the animation is driven by a frame list
            if this.looped then
                -- the frame changes every frameDelay in a looped sequence,
                -- starting relative to this component or the global timer.
                return this.frameIds(this.frameIds'First + ((anmTimer / this.frameDelay) mod this.frameIds'Length));
            else
                -- each frame is displayed once over the duration of the animation
                return this.frameIds(Integer'Min( this.frameIds'First + anmTimer / this.frameDelay, this.frameIds'Last ));
            end if;

        elsif this.nextFrame >= 0 then

            -- the animation has a next frame (single-shot)
            if this.Get_Age >= this.anmStart + this.frameDelay then
                return this.nextFrame;
            end if;

        end if;

        -- no frame change needed
        return vis.Get_Frame;
    end Calculate_Frame;

    ----------------------------------------------------------------------------

    procedure Get_Anm_Info( this : not null access Animation'Class;
                            tile : A_Tile ) is
    begin
        -- clear all the animation info
        this.frames := Null_Value.Lst;
        Delete( this.frameIds );
        this.nextFrame := -1;
        this.looped := False;
        this.global := False;
        this.frameDelay := Time_Span_Zero;

        if tile = null then
            return;
        end if;

        -- check if the tile has a frames list
        this.frames := tile.Get_Attribute( "frames" ).Lst;
        if this.frames.Valid and then this.frames.Length > 0 then

            -- tile is a looped animation
            this.frameIds := new Tile_Id_Array(1..this.frames.Length);
            for i in this.frameIds'Range loop
                this.frameIds(i) := this.frames.Get_Int( i );
            end loop;
            this.looped := True;
            this.global := tile.Get_Attribute( "globalAnm" ).To_Boolean;
            this.frameDelay := Milliseconds( Cast_Int( tile.Get_Attribute( "delay" ), 0 ) );
            this.anmStart := this.Get_Age;

        else

            -- check if the tile has a next frame
            this.frames := Null_Value.Lst;
            this.nextFrame := Cast_Int( tile.Get_Attribute( "next" ), -1 );
            if this.nextFrame >= 0 then

                -- tile is a frame in a single-shot animation
                this.looped := False;
                this.global := False;
                this.frameDelay := Milliseconds( Cast_Int( tile.Get_Attribute( "delay" ), 0 ) );
                this.anmStart := this.Get_Age;

            else

                -- tile has no animation info
                this.nextFrame := -1;

            end if;
        end if;
    end Get_Anm_Info;

    ----------------------------------------------------------------------------

    overriding
    function Object_Input( stream : access Root_Stream_Type'Class ) return Animation is
        this : aliased Animation;
    begin
        this.Construct;
        Animation'Read( stream, this );
        return this;
    end Object_Input;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Animation ) is
    begin
        Component'Read( stream, Component(obj) );
        obj.currentFrame := Integer'Input( stream );
        obj.anmStart := Time_Span'Input( stream );
        obj.frames := Value_Input( stream ).Lst;
        if obj.frames.Valid then
            obj.frameIds := new Tile_Id_Array(1..obj.frames.Length);
            for i in obj.frameIds'Range loop
                obj.frameIds(i) := obj.frames.Get_Int( i );
            end loop;
        else
            obj.frames := Null_Value.Lst;
        end if;
        obj.nextFrame := Integer'Input( stream );
        obj.frameDelay := Time_Span'Input( stream );
        obj.looped := Boolean'Input( stream );
        obj.global := Boolean'Input( stream );
    end Object_Read;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Animation ) is
    begin
        Component'Write( stream, Component(obj) );
        Integer'Output( stream, obj.currentFrame );
        Time_Span'Output( stream, obj.anmStart );
        Value_Output( stream, obj.frames );
        Integer'Output( stream, obj.nextFrame );
        Time_Span'Output( stream, obj.frameDelay );
        Boolean'Output( stream, obj.looped );
        Boolean'Output( stream, obj.global );
    end Object_Write;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Added( this : access Animation ) is
        vis : constant A_Visible := A_Visible(this.Get_Component( VISIBLE_ID ));
    begin
        pragma Assert( vis /= null, "Animation component depends on Visible component" );

        -- check if this component is newly created (not unserialized) and was
        -- given an animation in its properties.
        if this.currentFrame < 0 and then this.frames.Valid then

            -- initialize the .frameIds array
            for i in this.frameIds'Range loop
                if this.frames.Get(i).Is_Number then
                    this.frameIds(i) := this.frames.Get_Int( i );
                else
                    this.frameIds(i) := vis.Get_Library.Get.Get_Id( this.frames.Get_String( i ) );
                end if;
            end loop;

            this.anmStart := this.Get_Age;
            this.Set_Frame( this.Calculate_Frame );
        end if;
    end On_Added;

    ----------------------------------------------------------------------------

    procedure Set_Frame( this : not null access Animation; id : Natural ) is
    begin
        this.currentFrame := id;
        A_Visible(this.Get_Component( VISIBLE_ID )).Set_Frame( this.currentFrame );
    end Set_Frame;

    ----------------------------------------------------------------------------

    overriding
    procedure Tick( this : access Animation; time : Tick_Time ) is
        pragma Unreferenced( time );
        vis : constant A_Visible := A_Visible(this.Get_Component( VISIBLE_ID ));
    begin
        -- check if another component changed the frame, potentially starting a
        -- new animation using its tile attributes.
        if this.currentFrame /= vis.Get_Frame then
            this.Get_Anm_Info( vis.Get_Library.Get.Get_Tile( vis.Get_Frame ) );
        end if;

        -- calculate what the frame should be. if the tile doesn't have an
        -- animation, then the frame won't change.
        this.Set_Frame( this.Calculate_Frame );

        -- if the frame just changed to the next frame in a single-shot
        -- animation, the animation info needs to be updated.
        if this.nextFrame = this.currentFrame then
            this.Get_Anm_Info( vis.Get_Library.Get.Get_Tile( this.currentFrame ) );
        end if;
    end Tick;

    --==========================================================================

    procedure Define_Component( factory : not null A_Entity_Factory ) is
    begin
        factory.Define_Component( Family_Name( ANIMATION_ID ),
                                  Create_Animation'Access,
                                  propNames =>
                                      (Create( "frames"   ),
                                       Create( "delay"    ),
                                       Create( "duration" ),
                                       Create( "looped"   ),
                                       Create( "global"   )),
                                  required =>
                                      (1 => Create( Family_Name( VISIBLE_ID ) )) );
    end Define_Component;

end Entities.Animations;
