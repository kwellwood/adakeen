--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Events.Entities;                   use Events.Entities;
with Events.Entities.Sprites;           use Events.Entities.Sprites;
with Support;                           use Support;
with Values.Casting;                    use Values.Casting;
with Values.Construction;               use Values.Construction;
with Values.Indirects;                  use Values.Indirects;
with Values.Strings;                    use Values.Strings;

package body Entities.Visibles is

    package Visible_Refs is new Values.Indirects.Generics( Visible );

    ----------------------------------------------------------------------------

    function Get_Frame( this : not null access Visible'Class ) return Value is (Create( this.frame ));

    function Get_Z( this : not null access Visible'Class ) return Value is (Create( this.z ));

    function Get_ZOrder( this : not null access Visible'Class ) return Value is (Create( this.zOrder ));

    ----------------------------------------------------------------------------

    procedure Set_Frame( this : not null access Visible'Class; id : Value'Class ) is
        id2 : Natural;
    begin
        if id.Is_Number then
            this.Set_Frame( Integer'Max( id.To_Int, 0 ) );
        elsif id.Is_String then
            if this.lib.Not_Null then
                id2 := this.lib.Get.Get_Id( id.Str.To_String );
                if id2 > 0 then
                    this.Set_Frame( id2 );
                end if;
            end if;
        end if;
    end Set_Frame;

    ----------------------------------------------------------------------------

    procedure Set_Z( this : not null access Visible'Class; z : Value'Class ) is
    begin
        this.Set_Z( Cast_Float( z, this.z ) );
    end Set_Z;

    ----------------------------------------------------------------------------

    procedure Set_ZOrder( this : not null access Visible'Class; zOrder : Value'Class ) is
    begin
        this.Set_ZOrder( Cast_Int( zOrder, this.zOrder ) );
    end Set_ZOrder;

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Visible( properties : Map_Value ) return A_Component is
        this : constant A_Visible := new Visible;
    begin
        this.Construct;

        this.libName := properties.Get_Unbounded_String( "lib" );
        this.Load_Library;

        if properties.Get( "frame" ).Is_String then
            -- initialize frame by name
            if this.lib.Not_Null then
                this.frame := this.lib.Get.Get_Id( properties.Get_String( "frame" ) );
            end if;
        else
            -- initialize frame by id
            this.frame := properties.Get_Int( "frame" );
        end if;
        this.z := Constrain( properties.Get_Float( "z", 0.0 ), -1024.0, 1024.0 );
        this.zOrder := properties.Get_Int( "zOrder", 0 );
        this.editorOnly := properties.Get_Boolean( "editorOnly", False );

        return A_Component(this);
    end Create_Visible;

    ----------------------------------------------------------------------------

    function Get_Frame( this : not null access Visible'Class ) return Natural is (this.frame);

    function Get_Library( this : not null access Visible'Class ) return Library_Ptr is (this.lib);

    function Get_Z( this : not null access Visible'Class ) return Float is (this.z);

    function Get_ZOrder( this : not null access Visible'Class ) return Integer is (this.zOrder);

    ----------------------------------------------------------------------------

    overriding
    function Get_Public_Values( this : access Visible ) return Map_Value is
        state : Map_Value;
    begin
        if not this.editorOnly or else this.In_Editor then
            state := Create_Map.Map;
            state.Set( "render", Create( "sprite" ) );
            state.Set( "lib",    Create( this.libName ) );
            state.Set( "frame",  Create( this.frame ) );
            state.Set( "z",      Create( this.z ) );
            state.Set( "zOrder", Create( this.zOrder ) );
        end if;
        return state;
    end Get_Public_Values;

    ----------------------------------------------------------------------------

    procedure Load_Library( this : not null access Visible'Class ) is
    begin
        if this.lib.Is_Null and then Length( this.libName ) > 0 then
            -- only load if the entity will be visible. it will not be visible if
            -- we're outside the editor and this entity has editor-only visibility.
            if not this.editorOnly or else this.In_Editor then
                this.lib := Load_Library( To_String( this.libName ), bitmaps => False );
            end if;
        end if;
    end Load_Library;

    ----------------------------------------------------------------------------

    overriding
    function Object_Input( stream : access Root_Stream_Type'Class ) return Visible is
        this : aliased Visible;
    begin
        this.Construct;
        Visible'Read( stream, this );
        return this;
    end Object_Input;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Visible ) is
    begin
        Component'Read( stream, Component(obj) );
        obj.editorOnly := Boolean'Input( stream );
        obj.libName := To_Unbounded_String( String'Input( stream ) );
        obj.frame := Natural'Input( stream );
        obj.z := Float'Input( stream );
        obj.zOrder := Integer'Input( stream );

        obj.Load_Library;
    end Object_Read;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Visible ) is
    begin
        Component'Write( stream, Component(obj) );
        Boolean'Output( stream, obj.editorOnly );
        String'Output( stream, To_String( obj.libName ) );
        Natural'Output( stream, obj.frame );
        Float'Output( stream, obj.z );
        Integer'Output( stream, obj.zOrder );
    end Object_Write;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Added( this : access Visible ) is
    begin
        this.Register_Entity_Attribute( "frame",  Visible_Refs.Create_Indirect( this, Get_Frame'Access,  Set_Frame'Access  ) );
        this.Register_Entity_Attribute( "z",      Visible_Refs.Create_Indirect( this, Get_Z'Access,      Set_Z'Access      ) );
        this.Register_Entity_Attribute( "zOrder", Visible_Refs.Create_Indirect( this, Get_ZOrder'Access, Set_ZOrder'Access ) );
    end On_Added;

    ----------------------------------------------------------------------------

    procedure Set_Frame( this : not null access Visible'Class; id : Natural ) is
    begin
        if this.frame /= id then
            this.frame := id;
            if this.owner /= null and then this.lib.Not_Null then
                Queue_Frame_Changed( this.owner.Get_Id, this.frame );
            end if;
        end if;
    end Set_Frame;

    ----------------------------------------------------------------------------

    procedure Set_Z( this : not null access Visible'Class; z : Float ) is
    begin
        if this.z /= z then
            this.z := z;
            if this.owner /= null and then this.lib.Not_Null then
                Queue_Entity_Z_Changed( this.owner.Get_Id, this.z, this.zOrder );
            end if;
        end if;
    end Set_Z;

    ----------------------------------------------------------------------------

    procedure Set_ZOrder( this : not null access Visible'Class; zOrder : Integer ) is
    begin
        if this.zOrder /= zOrder then
            this.zOrder := zOrder;
            if this.owner /= null and then this.lib.Not_Null then
                Queue_Entity_Z_Changed( this.owner.Get_Id, this.z, this.zOrder );
            end if;
        end if;
    end Set_ZOrder;

    --==========================================================================

    procedure Define_Component( factory : not null A_Entity_Factory ) is
    begin
        factory.Define_Component( Family_Name( VISIBLE_ID ),
                                  Create_Visible'Access,
                                  propNames =>
                                      (Create( "lib"        ),
                                       Create( "frame"      ),
                                       Create( "z"          ),
                                       Create( "zOrder"     ),
                                       Create( "editorOnly" )) );
    end Define_Component;

end Entities.Visibles;
