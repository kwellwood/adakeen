--
-- Copyright (c) 2014-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Values.Construction;               use Values.Construction;
with Values.Indirects;                  use Values.Indirects;

package body Entities.Map_Clipping is

    package Map_Clipped_Refs is new Values.Indirects.Generics( Map_Clipped );

    ----------------------------------------------------------------------------

    function Is_Blocked_Left( this : not null access Map_Clipped'Class ) return Value is (Create( this.Is_Blocked( Left ) ));

    function Is_Blocked_Right( this : not null access Map_Clipped'Class ) return Value is (Create( this.Is_Blocked( Right ) ));

    function Is_Blocked_Up( this : not null access Map_Clipped'Class ) return Value is (Create( this.Is_Blocked( Up) ));

    function Is_Blocked_Down( this : not null access Map_Clipped'Class ) return Value is (Create( this.Is_Blocked( Down ) ));

    --==========================================================================

    function Is_Blocked( this : not null access Map_Clipped'Class;
                         dir  : Cardinal_Direction ) return Boolean is (this.blocked(dir));

    ----------------------------------------------------------------------------

    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Map_Clipped ) is
    begin
        Component'Read( stream, Component(obj) );
        for i in obj.blocked'Range loop
            obj.blocked(i) := Boolean'Input( stream );
        end loop;
    end Object_Read;

    ----------------------------------------------------------------------------

    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Map_Clipped ) is
    begin
        Component'Write( stream, Component(obj) );
        for i in obj.blocked'Range loop
            Boolean'Output( stream, obj.blocked(i) );
        end loop;
    end Object_Write;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Added( this : access Map_Clipped ) is
    begin
        -- these attributes do not notify of change. The HitWall message is sent
        -- instead, when the entity collides with a wall.
        this.Register_Entity_Attribute( "isBlockedLeft",  Map_Clipped_Refs.Create_Indirect( this, Is_Blocked_Left'Access,  null ) );
        this.Register_Entity_Attribute( "isBlockedRight", Map_Clipped_Refs.Create_Indirect( this, Is_Blocked_Right'Access, null ) );
        this.Register_Entity_Attribute( "isBlockedUp",    Map_Clipped_Refs.Create_Indirect( this, Is_Blocked_Up'Access,    null ) );
        this.Register_Entity_Attribute( "isBlockedDown",  Map_Clipped_Refs.Create_Indirect( this, Is_Blocked_Down'Access,  null ) );
    end On_Added;

end Entities.Map_Clipping;
