--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Entities.Components;               use Entities.Components;
with Entities.Factory;                  use Entities.Factory;

package Entities.Tile_Sensors is

    -- This message is sent to the component's entity when its bounding box
    -- intersects a new tile location in the world. The coordinates are given in
    -- tile units.
    --
    -- EnterTile {"x": <tileX>, "y": <tileY>}
    MSG_EnterTile : constant Hashed_String := To_Hashed_String( "EnterTile" );

    -- This message is sent to the component's entity when its bounding box no
    -- longer intersects a tile location in the world. The coordinates are given
    -- in tile units.
    --
    -- ExitTile {"x": <tileX>, "y": <tileY>}
    MSG_ExitTile  : constant Hashed_String := To_Hashed_String( "ExitTile" );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    TILE_SENSOR_ID : Family_Id := To_Family_Id( "TileSensor" );

    -- Property      Type    Description
    -- --------      ----    -----------
    -- none

    procedure Define_Component( factory : not null A_Entity_Factory );

    ----------------------------------------------------------------------------

    type Tile_Sensor is new Component with private;
    type A_Tile_Sensor is access all Tile_Sensor'Class;

    overriding
    function Get_Dependency_Order( this : access Tile_Sensor ) return Natural is (DEPS_TILESENSOR);

    overriding
    function Get_Family( this : access Tile_Sensor ) return Family_Id is (TILE_SENSOR_ID);

    overriding
    function Get_Tick_Order( this : access Tile_Sensor ) return Natural is (TCK_TILESENSOR);

    -- Detects tile intersections and dispatches EnterTile / ExitTile events.
    procedure Tick( this : access Tile_Sensor; time : Tick_Time );

private

    type Tile_Sensor is new Component with null record;
    for Tile_Sensor'External_Tag use "Component.Tile_Sensor";

    function Object_Input( stream : access Root_Stream_Type'Class ) return Tile_Sensor;
    for Tile_Sensor'Input use Object_Input;

    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Tile_Sensor );
    for Tile_Sensor'Read use Object_Read;

    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Tile_Sensor );
    for Tile_Sensor'Write use Object_Write;

end Entities.Tile_Sensors;
