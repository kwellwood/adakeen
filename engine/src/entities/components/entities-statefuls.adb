--
-- Copyright (c) 2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Debugging;                         use Debugging;

package body Entities.Statefuls is

    not overriding
    procedure Construct( this         : access Stateful;
                         root         : not null access State'Class;
                         initialState : State_Id ) is
    begin
        Component(this.all).Construct;
        this.root := A_State(root);
        this.top := this.root.Find( initialState );
        pragma Assert( this.top /= null, "State " & initialState'Img & " not found" );
        this.nextId := this.top.id;
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Stateful ) is
    begin
        Delete( this.root );
        Component(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Get_Next( this : not null access Stateful'Class ) return State_Id is (this.nextId);

    ----------------------------------------------------------------------------

    procedure Handle_Message( this   : access Stateful;
                              name   : Hashed_String;
                              params : Map_Value ) is
        nextId : constant State_Id := this.nextId;
        state  : A_State := this.top;
    begin
        if this.root.owner = null then
            this.root.Set_Owner( this );
        end if;

        this.busy := this.busy + 1;
        while nextId = this.nextId and then              -- stop on state change request
              state /= null and then                     -- stop at the root
              not state.Handle_Message( name, params )   -- stop when 'state' blocks us
        loop
            state := state.parent;
        end loop;
        this.busy := this.busy - 1;
        this.Transition;
    end Handle_Message;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Stateful ) is
    begin
        Component'Read( stream, Component(obj) );

        -- the Stateful's state tree must have been constructed by this subclass's
        -- default constructor, which runs before reading happens.
        pragma Assert( obj.root /= null, "State tree not constructed" );

        obj.top := obj.root.Find( State_Id'Input( stream ) );
        if obj.top = null then
            raise Constraint_Error with "Read bad state id";
        end if;
    end Object_Read;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Stateful ) is
    begin
        Component'Write( stream, Component(obj) );
        State_Id'Output( stream, obj.top.id );
    end Object_Write;

    ----------------------------------------------------------------------------

    procedure Set_Next( this : not null access Stateful'Class; id : State_Id ) is
    begin
        this.nextId := id;
        this.Transition;
    end Set_Next;

    ----------------------------------------------------------------------------

    overriding
    procedure Tick( this : access Stateful; time : Tick_Time ) is
        nextId : constant State_Id := this.nextId;
        state  : A_State := this.top;
    begin
        if this.root.owner = null then
            this.root.Set_Owner( this );
        end if;

        this.busy := this.busy + 1;
        while nextId = this.nextId and then              -- stop on state change
              state /= null and then                     -- stop at the root
              not state.Tick( time )                     -- stop when 'state' blocks us
        loop
            state := state.parent;
        end loop;
        this.busy := this.busy - 1;
        this.Transition;
    end Tick;

    ----------------------------------------------------------------------------

    procedure Transition( this : not null access Stateful'Class ) is
        oldTopId : constant State_Id := this.top.id;
        pos      : State_Maps.Cursor;
    begin
        if this.busy > 0 then
            -- can't change states while busy
            return;
        end if;

        loop
            -- check for matching self
            if this.nextId = this.top.id then
                if this.top.id /= oldTopId then
                    this.top.On_Top;
                end if;
                exit;
            end if;

            -- look in substates
            pos := this.top.subStates.Find( this.nextId );
            if State_Maps.Has_Element( pos ) then
                this.top := State_Maps.Element( pos );
                this.top.On_Enter;
                this.top.On_Top;
                exit;
            end if;

            if this.top.parent = null then
                -- searched to the root; can't find the state!
                this.nextId := this.top.id;
                Dbg( "Invalid state transition: " & this.top.id'Img & " -> " & this.nextId'Img &
                     " in " & this.To_String,
                     D_ENTITY, Error );
                exit;
            end if;

            -- exit to the parent state
            this.top.On_Exit;
            this.top := this.top.parent;
        end loop;
    end Transition;

    --==========================================================================

    procedure Add_State( this     : not null access State'Class;
                         id       : State_Id;
                         subState : not null A_State ) is
    begin
        -- construct 'state' first and then add it
        pragma Assert( subState.parent = null );
        subState.Construct;
        subState.id := id;
        subState.parent := A_State(this);

        this.subStates.Insert( subState.id, subState );
    end Add_State;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out State ) is
    begin
        for sub of this.subStates loop
            Delete( sub );
        end loop;
        this.subStates.Clear;
        Limited_Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Find( this : not null access State'Class; id : State_Id ) return A_State is
        found : A_State := null;
    begin
        if this.id = id then
            return A_State(this);
        end if;
        for sub of this.subStates loop
            found := sub.Find( id );
            exit when found /= null;
        end loop;
        return found;
    end Find;

    ----------------------------------------------------------------------------

    procedure Pop( this : not null access State'Class ) is
    begin
        if this.parent /= null then
            this.Set_Next( this.parent.id );
        end if;
    end Pop;

    ----------------------------------------------------------------------------

    procedure Set_Next( this : not null access State'Class; id : State_Id ) is
    begin
        this.owner.Set_Next( id );
    end Set_Next;

    ----------------------------------------------------------------------------

    procedure Set_Owner( this : not null access State'Class; owner : not null A_Stateful ) is
    begin
        this.owner := owner;
        for state of this.subStates loop
            state.Set_Owner( owner );
        end loop;
    end Set_Owner;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_State ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

    --==========================================================================

    procedure Clear( this : in out Timer_Type'Class ) is
    begin
        this.endTime := Time_Span_Zero;
    end Clear;

    ----------------------------------------------------------------------------

    function Expired( this : Timer_Type'Class ) return Boolean is (this.endTime <= this.owner.Get_Age);

    ----------------------------------------------------------------------------

    procedure Init( this : in out Timer_Type'Class; owner : access Stateful'Class ) is
    begin
        this.owner := owner;
    end Init;

    ----------------------------------------------------------------------------

    function Remaining( this : Timer_Type'Class ) return Time_Span is (this.endTime - this.owner.Get_Age);

    ----------------------------------------------------------------------------

    procedure Restart( this : in out Timer_Type'Class ) is
    begin
        this.Start( this.period );
    end Restart;

    ----------------------------------------------------------------------------

    procedure Start( this : in out Timer_Type'Class; period : Time_Span ) is
    begin
        this.period := period;
        this.endTime := this.owner.Get_Age + period;
    end Start;

end Entities.Statefuls;
