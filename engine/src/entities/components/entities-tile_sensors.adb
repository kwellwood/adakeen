--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Entities.Locations;                use Entities.Locations;
with Entities.Systems;                  use Entities.Systems;
with Values.Construction;               use Values.Construction;
with Values.Lists;                      use Values.Lists;
with Values.Strings;                    use Values.Strings;
with Worlds;                            use Worlds;

package body Entities.Tile_Sensors is

    function Create_Tile_Sensor( properties : Map_Value ) return A_Component is
        pragma Unreferenced( properties );
        this : constant A_Tile_Sensor := new Tile_Sensor;
    begin
        this.Construct;
        return A_Component(this);
    end Create_Tile_Sensor;

    ----------------------------------------------------------------------------

    overriding
    function Object_Input( stream : access Root_Stream_Type'Class ) return Tile_Sensor is
        this : aliased Tile_Sensor;
    begin
        this.Construct;
        Tile_Sensor'Read( stream, this );
        return this;
    end Object_Input;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Tile_Sensor ) is
    begin
        Component'Read( stream, Component(obj) );
    end Object_Read;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Tile_Sensor ) is
    begin
        Component'Write( stream, Component(obj) );
    end Object_Write;

    ----------------------------------------------------------------------------

    overriding
    procedure Tick( this : access Tile_Sensor; time : Tick_Time ) is
        pragma Unreferenced( time );
        world      : constant A_World := this.Get_Owner.Get_System.Get_World;
        fTileWidth : constant Float := Float(world.Tile_Width);
        loc        : constant A_Location := A_Location(this.Get_Component( LOCATION_ID ));

        x          : constant Float := loc.Get_X;
        y          : constant Float := loc.Get_Y;
        halfWidth  : constant Float := Float(loc.Get_Width) / 2.0;
        halfHeight : constant Float := Float(loc.Get_Height) / 2.0;
        topRow     : constant Integer := Integer'Max( Integer(Float'Floor((y - halfHeight) / fTileWidth)), 0 );
        botRow     : constant Integer := Integer'Min( Integer(Float'Floor(Float'Ceiling(y + halfHeight - 1.0) / fTileWidth)), world.Get_Map_Rows - 1 );
        leftCol    : constant Integer := Integer'Max( Integer(Float'Floor((x - halfWidth) / fTileWidth)), 0 );
        rightCol   : constant Integer := Integer'Min( Integer(Float'Floor(Float'Ceiling(x + halfWidth - 1.0) / fTileWidth)), world.Get_Map_Columns - 1 );

        xOld          : constant Float := loc.Get_Prev_X;
        yOld          : constant Float := loc.Get_Prev_Y;
        halfWidthOld  : constant Float := Float(loc.Get_Prev_Width) / 2.0;
        halfHeightOld : constant Float := Float(loc.Get_Prev_Height) / 2.0;
        topRowOld     : constant Integer := Integer'Max( Integer(Float'Floor((yOld - halfHeightOld) / fTileWidth)), 0 );
        botRowOld     : constant Integer := Integer'Min( Integer(Float'Floor(Float'Ceiling(yOld + halfHeightOld - 1.0) / fTileWidth)), world.Get_Map_Rows - 1 );
        leftColOld    : constant Integer := Integer'Max( Integer(Float'Floor((xOld - halfWidthOld) / fTileWidth)), 0 );
        rightColOld   : constant Integer := Integer'Min( Integer(Float'Floor(Float'Ceiling(xOld + halfWidthOld - 1.0) / fTileWidth)), world.Get_Map_Columns - 1 );
    begin
        -- no generating TileEnter/TileExit events in the editor
        if this.In_Editor then
            return;
        end if;

        -- Touching previously: (topRowOld,leftColOld)..(botRowOld,rightColOld)
        -- Touching now: (topRow,leftCol)..(botRow,rightCol)

        for r in topRowOld..botRowOld loop
            for c in leftColOld..rightColOld loop
                if c < leftCol or else c > rightCol or else
                    r < topRow or else r > botRow
                then
                    this.Get_Owner.Dispatch_Message( MSG_ExitTile,
                                                     Create_Map( (Pair("x", Create( c )),
                                                                  Pair("y", Create( r )))
                                                               ).Map );
                end if;
            end loop;
        end loop;

        for r in topRow..botRow loop
            for c in leftCol..rightCol loop
                if c < leftColOld or else c > rightColOld or else
                   r < topRowOld or else r > botRowOld
                then
                    this.Get_Owner.Dispatch_Message( MSG_EnterTile,
                                                     Create_Map( (Pair("x", Create( c )),
                                                                  Pair("y", Create( r )))
                                                               ).Map );
                end if;
            end loop;
        end loop;
    end Tick;

    --==========================================================================

    procedure Define_Component( factory : not null A_Entity_Factory ) is
    begin
        factory.Define_Component( Family_Name( TILE_SENSOR_ID ),
                                  Create_Tile_Sensor'Access,
                                  required =>
                                      (1 => Create( Family_Name( LOCATION_ID ) )) );
    end Define_Component;

end Entities.Tile_Sensors;
