--
-- Copyright (c) 2014-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Events.Entities;                   use Events.Entities;
with Events.Entities.Camera_Targets;    use Events.Entities.Camera_Targets;
with Values.Strings;                    use Values.Strings;
with Worlds;                            use Worlds;

package body Entities.Camera_Targets is

    function Create_Camera_Target( properties : Map_Value ) return A_Component is
        this : constant A_Camera_Target := new Camera_Target;
    begin
        this.Construct;

        this.enabled := properties.Get_Boolean( "enabled", True );
        this.bounds := (top    => properties.Get_Int( "top", 0 ),
                        bottom => properties.Get_Int( "bottom", 0 ),
                        left   => properties.Get_Int( "left", 0 ),
                        right  => properties.Get_Int( "right", 0 ));

        return A_Component(this);
    end Create_Camera_Target;

    ----------------------------------------------------------------------------

    procedure Apply_Bounds( this : not null access Camera_Target'Class ) is
    begin
        if this.Get_Owner /= null and this.enabled then
            Queue_Follow_Entity( this.Get_Owner.Get_Id,
                                 this.bounds.top,
                                 this.bounds.bottom,
                                 this.bounds.left,
                                 this.bounds.right );
        end if;
    end Apply_Bounds;

    ----------------------------------------------------------------------------

    procedure Center( this : not null access Camera_Target'Class ) is
    begin
        if this.Get_Owner /= null and this.enabled then
            Queue_Follow_Entity( this.Get_Owner.Get_Id, 0, 0, 0, 0 );
            this.Apply_Bounds;         -- reapply the current bounds
        end if;
    end Center;

    ----------------------------------------------------------------------------

    procedure Enable( this    : not null access Camera_Target'Class;
                      enabled : Boolean ) is
    begin
        if enabled /= this.enabled then
            this.enabled := enabled;
            if not this.enabled then
                Queue_Follow_Entity( NULL_ID );
            else
                this.Apply_Bounds;
            end if;
        end if;
    end Enable;

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Message( this   : access Camera_Target;
                              name   : Hashed_String;
                              params : Map_Value ) is
        pragma Unreferenced( params );
    begin
        if name = MSG_WorldLoaded or else name = MSG_PlayStarted then
            if this.enabled then
                this.Apply_Bounds;
            end if;
        end if;
    end Handle_Message;

    ----------------------------------------------------------------------------

    function Is_Enabled( this : not null access Camera_Target'Class ) return Boolean is (this.enabled);

    ----------------------------------------------------------------------------

    overriding
    function Object_Input( stream : access Root_Stream_Type'Class ) return Camera_Target is
        this : aliased Camera_Target;
    begin
        this.Construct;
        Camera_Target'Read( stream, this );
        return this;
    end Object_Input;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Camera_Target ) is
    begin
        Component'Read( stream, Component(obj) );
        obj.enabled := Boolean'Input( stream );
        obj.bounds.top    := Integer'Input( stream );
        obj.bounds.bottom := Integer'Input( stream );
        obj.bounds.left   := Integer'Input( stream );
        obj.bounds.right  := Integer'Input( stream );
    end Object_Read;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Camera_Target ) is
    begin
        Component'Write( stream, Component(obj) );
        Boolean'Output( stream, obj.enabled );
        Integer'Output( stream, obj.bounds.top );
        Integer'Output( stream, obj.bounds.bottom );
        Integer'Output( stream, obj.bounds.left );
        Integer'Output( stream, obj.bounds.right );
    end Object_Write;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Added( this : access Camera_Target ) is
    begin
        this.Register_Listener( MSG_WorldLoaded );
        this.Register_Listener( MSG_PlayStarted );
    end On_Added;

    ----------------------------------------------------------------------------

    procedure Set_Bounds( this   : not null access Camera_Target'Class;
                          bounds : Bounds_Type ) is
    begin
        if bounds /= this.bounds then
            this.bounds := bounds;
            this.Apply_Bounds;
        end if;
    end Set_Bounds;

    --==========================================================================

    procedure Define_Component( factory : not null A_Entity_Factory ) is
    begin
        factory.Define_Component( Family_Name( CAMERA_TARGET_ID ),
                                  Create_Camera_Target'Access,
                                  propNames =>
                                      (Create( "enabled" ),
                                       Create( "top"     ),
                                       Create( "bottom"  ),
                                       Create( "left"    ),
                                       Create( "right"   )) );
    end Define_Component;

end Entities.Camera_Targets;

