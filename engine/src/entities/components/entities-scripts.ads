--
-- Copyright (c) 2015-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Indefinite_Vectors;
with Entities.Components;               use Entities.Components;
with Entities.Factory;                  use Entities.Factory;
with Scribble.Namespaces;               use Scribble.Namespaces;
with Scribble.VMs;                      use Scribble.VMs;
with Values.Functions;                  use Values.Functions;
with Values.Lists;                      use Values.Lists;

package Entities.Scripts is

    SCRIPT_ID : Family_Id := To_Family_Id( "Script" );

    -- Property   Type   Description
    -- --------   ----   -----------
    -- filename   string The filename of the script. The script file is composed
    --                   of functions, variables and timers. The functions can
    --                   be called to handle messages (message handler functions),
    --                   triggered by timers, or called from other functions.
    --                   Any members of the script file can be overridden by
    --                   names in the "script" and "timers" properties described
    --                   below.
    --
    -- script     map    All names in this map property are added to the
    --                   component's namespace, overriding matching names in the
    --                   script file. Names must follow the rules of Scribble
    --                   identifiers. All names beginning with "On", followed by
    --                   a message name, will be registered as handler messages.
    --                   (e.g. name "OnWorldLoaded" will register its function
    --                   value to handle "WorldLoaded" messages.
    --
    --                   As a special exception, the OnTick(elapsed) handler, if
    --                   defined, will be executed once each game tick. The
    --                   optional 'elapsed' parameter will contain the number of
    --                   milliseconds in game time since the previous tick. The
    --                   implementation must handle a value of zero (for the
    --                   very first tick).
    --
    --                   Message handler functions must use one of these prototypes:
    --
    --                       function OnMessageName()
    --                       function OnMessageName( args : map )
    --                       function OnMessageName( message : string, args : map )
    --
    --                   The following built-in names comprise the component's
    --                   Scribble API:
    --
    --                   entity
    --                       - the component's entity
    --
    --                   SetTimer( timerName : string, time : number, repeat : boolean, args : list )
    --                       - Sets a timer to call handler 'timerName' in 'time'
    --                         milliseconds from now. The timer will automatically
    --                         repeat if 'repeat' is true. 'args' will be passed
    --                         to the handler as additional arguments, if it
    --                         accepts them.
    --
    --                   CancelTimer( timerName : string )
    --                       - Cancels the timer calling handler 'timerName'.
    --
    -- timers     map    Each member is a named timer. The name determines which
    --                   message timer handler will be executed when the timer
    --                   expires. For example, the Complete timer, when expired,
    --                   will execute the Complete() handler if defined. Timer
    --                   handlers take no arguments unless explicitly specified
    --                   (see below). Timers defined in this map will override
    --                   matching timers in the script file.
    --
    --                   Each value in the map may be either a simple number of
    --                   milliseconds or a map of the form: {time: <ms>,
    --                   repeat: <bool>, args: <list>}, where 'time' is the
    --                   timer delay in milliseconds, optional 'repeat'
    --                   indicates if the timer automatically repeats (default
    --                   is False), and optional 'args' is a list of arguments
    --                   to pass to the associated handler function.

    -- Defines the Script component with the entity factory.
    procedure Define_Component( factory : not null A_Entity_Factory );

    ----------------------------------------------------------------------------

    -- The Script class is a Component that uses Scribble functions to handle
    -- messages and timers. Each Script component has its own namespace
    -- containing the functions, variables and timers that comprise the script.
    -- Scripts can process events sent to the parent entity, react to (or cause)
    -- entity attribute changes, send messages to other entities, etc. Script
    -- components allow for game entities with completely scripted behavior.
    type Script is new Component and Scribble_Namespace with private;
    type A_Script is access all Script'Class;

    -- Multiple scripts can be added to a single entity.
    overriding
    function Allow_Multiple( this : access Script ) return Boolean is (True);

    -- Cancels the timer set to call 'funcName' on expiration. If there is no
    -- timer matching this description, nothing happens.
    procedure Cancel_Timer( this     : not null access Script'Class;
                            funcName : String );

    overriding
    function Get_Dependency_Order( this : access Script ) return Natural is (DEPS_SCRIPT);

    overriding
    function Get_Family( this : access Script ) return Family_Id is (SCRIPT_ID);

    -- Returns the value of a name within the component's namespace (without
    -- copying), or Null if 'name' is undefined.
    function Get_Namespace_Name( this : access Script;
                                 name : String ) return Value;

    -- Returns a reference to the value of variable 'name' within the
    -- component's namespace.
    function Get_Namespace_Ref( this : access Script;
                                name : String ) return Value;
    pragma Postcondition( Get_Namespace_Ref'Result.Is_Ref or else
                          Get_Namespace_Ref'Result.Is_Null );

    overriding
    function Get_Tick_Order( this : access Script ) return Natural is (TCK_SCRIPT);

    -- Sets a timer to invoke 'funcName' after 'waitTime' elapsed game time. If
    -- 'repeat' is True, the timer will automatically repeat at the given
    -- interval. If 'args' is given, they will be passed on to 'funcName' when
    -- invoked.
    procedure Set_Timer( this     : not null access Script'Class;
                         funcName : String;
                         waitTime : Time_Span;
                         repeat   : Boolean;
                         args     : List_Value );

    -- Checks for expired timers and runs timer handlers.
    procedure Tick( this : access Script; time : Tick_Time );

private

    -- Each Script component has a namespace available to its Scribble functions
    -- that allows them to read and write shared variables. (e.g. functions can
    -- can share data by reading and writing the same name in the component's
    -- namespace.) The namespace is private to each instance of the Script
    -- component, so names are not shared between two instances of a Script,
    -- even within the same entity.
    --
    -- The Script's namespace is actually comprised of two layers. All writes go
    -- into the component's namespace, but reads of names not defined in the
    -- component's namespace will fall back to the engine's global namespace
    -- defined in global.sv. This allows access to global functions and
    -- constants.
    --
    -- When a Script component is serialized, all values in its namespace are
    -- stored (except for Functions.) Names with Function values are always read
    -- from the component's script file and its definition when unserialized.

    type Timer is
        record
            funcName : Unbounded_String;
            handler  : Functions.Function_Value;
            period   : Time_Span := Time_Span_Zero;
            expires  : Time_Span := Time_Span_Zero;
            repeat   : Boolean := False;
            args     : List_Value;
        end record;

    package Timer_Vectors is new Ada.Containers.Indefinite_Vectors( Positive, Timer, "=" );

    ----------------------------------------------------------------------------

    -- the maximum depth of messages being handled by the script component. this
    -- happens when a script sends a message to itself, or to another component
    -- that then send a message back, and so on. we maintain pointers to up to
    -- this many threads to avoid reallocating them. if the message depth is
    -- exceeded, log an error and don't process the message; we probably got
    -- stuck in a messaging loop.
    MAX_MESSAGE_DEPTH : constant := 8;

    ----------------------------------------------------------------------------

    type Script is new Component and Scribble_Namespace with
        record
            filename    : Unbounded_String;             -- script filename
            names       : Map_Value;                    -- component's private namespace
            timers      : Timer_Vectors.Vector;         -- active timers
            tickHandler : Functions.Function_Value;
        end record;
    for Script'External_Tag use "Component.Script";

    -- Writes the script's built-in API into the component's namespace to make
    -- it available to Scribble.
    procedure Define_Api( this : not null access Script'Class );

    procedure Handle_Message( this   : access Script;
                              name   : Hashed_String;
                              params : Map_Value );

    function Object_Input( stream : access Root_Stream_Type'Class ) return Script;
    for Script'Input use Object_Input;

    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Script );
    for Script'Read use Object_Read;

    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Script );
    for Script'Write use Object_Write;

    -- Registers for messages.
    procedure On_Added( this : access Script );

    -- Defines script-specific functions (SetTimer/CancelTimer etc.)
    procedure On_Enter_System( this : access Script );

    -- Runs function 'func', named 'funcName', with arguments 'args' within the
    -- context of this Script component.
    procedure Run( this     : not null access Script'Class;
                   funcName : String;
                   func     : Functions.Function_Value;
                   args     : Value_Array );

    -- Runs function 'func', named 'funcName', without any arguments within the
    -- context of this Script component.
    procedure Run( this     : not null access Script'Class;
                   funcName : String;
                   func     : Functions.Function_Value );

    procedure Run_Handler( this : not null access Script'Class;
                           name : String;
                           args : Value'Class;
                           func : Function_Value );
    pragma Precondition( func.Valid );

end Entities.Scripts;
