--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Ordered_Sets;
with Entities.Components;               use Entities.Components;
with Entities.Factory;                  use Entities.Factory;
with Entities.Locations;                use Entities.Locations;

package Entities.Collidables is

    -- When two entities collide, this event is sent to each at the moment of
    -- first contact. 'eid' denotes the entity that the recipient collided with.
    --
    -- Collided {"eid": <entityId>}
    MSG_Collided  : constant Hashed_String := To_Hashed_String( "Collided" );

    -- When two entities that were previously intersecting are no longer
    -- intersecting, this message is sent to each. 'eid' denotes the entity that
    -- the recipient is no longer touching.
    --
    -- Separated {"eid": <entityId>}
    MSG_Separated : constant Hashed_String := To_Hashed_String( "Separated" );

    ----------------------------------------------------------------------------

    COLLIDABLE_ID : Family_Id := To_Family_Id( "Collidable" );

    -- Property      Type    Description
    -- --------      ----    -----------
    -- none

    procedure Define_Component( factory : not null A_Entity_Factory );

    ----------------------------------------------------------------------------

    type Collidable is new Component with private;
    type A_Collidable is access all Collidable'Class;

    -- Detects a collision with 'other' using the entities' Location components.
    -- Returns True if this intersects with the other.
    function Detect_Collision( this  : not null access Collidable'Class;
                               other : not null A_Collidable ) return Boolean;

    overriding
    function Get_Dependency_Order( this : access Collidable ) return Natural is (DEPS_COLLIDABLE);

    overriding
    function Get_Family( this : access Collidable ) return Family_Id is (COLLIDABLE_ID);

    overriding
    function Get_Tick_Order( this : access Collidable ) return Natural is (TCK_COLLIDABLE);

    function Is_Changed( this : not null access Collidable'Class ) return Boolean with Inline;

    -- Returns True if this entity is current touching entity 'id'.
    function Is_Touching( this : not null access Collidable'Class;
                          id   : Entity_Id ) return Boolean with Inline;

    -- Iterates through the list of entities that this entity is touching.
    procedure Iterate_Touching( this    : not null access Collidable'Class;
                                examine : not null access procedure( e : A_Entity ) );

    -- Sets whether or not the entity is touching entity 'id'.
    procedure Set_Touching( this     : not null access Collidable'Class;
                            id       : Entity_Id;
                            touching : Boolean );

    -- Checks if the entity's Location component has been updated this Tick.
    -- The Collision system needs to know, but after the Location component has
    -- already updated its previous values.
    procedure Tick( this : access Collidable; time : Tick_Time );

private

    package Id_Sets is new Ada.Containers.Ordered_Sets( Entity_Id, "<", "=" );

    type Collidable is new Component with
        record
            -- these fields are not streamed --
            loc        : A_Location := null;
            touching   : Id_Sets.Set;
            brandNew   : Boolean := True;           -- spawned 0 ticks ago?
            locChanged : Boolean := False;
        end record;
    for Collidable'External_Tag use "Component.Collidable";

    procedure Handle_Message( this   : access Collidable;
                              name   : Hashed_String;
                              params : Map_Value );

    function Object_Input( stream : access Root_Stream_Type'Class ) return Collidable;
    for Collidable'Input use Object_Input;

    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Collidable );
    for Collidable'Read use Object_Read;

    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Collidable );
    for Collidable'Write use Object_Write;

    -- Grabs a reference to the entity's Location component.
    procedure On_Added( this : access Collidable );

    procedure On_Removed( this : access Collidable );

end Entities.Collidables;
