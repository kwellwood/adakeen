--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Debugging;                         use Debugging;
with Values.Construction;               use Values.Construction;
with Values.Strings;                    use Values.Strings;

package body Entities.Component_Definitions is

    function Create_Extension( name       : String;
                               parentName : String;
                               properties : Map_Value;
                               attributes : Map_Value;
                               required   : String_Sets.Set ) return A_Definition is
        this : constant A_Definition := new Definition;
    begin
        this.Construct;
        this.name       := To_Unbounded_String( name );
        this.parentName := To_Unbounded_String( parentName );
        this.properties := (if properties.Valid then Clone( properties ).Map else Create_Map.Map);
        this.attributes := (if attributes.Valid then Clone( attributes ).Map else Create_Map.Map);
        this.required   := required.Copy;
        return this;
    end Create_Extension;

    ----------------------------------------------------------------------------

    function Create_Root( name            : String;
                          allocator       : A_Component_Allocator;
                          dependencyOrder : Integer;
                          propertyNames   : String_Sets.Set;
                          required        : String_Sets.Set ) return A_Definition is
        this : constant A_Definition := new Definition;
    begin
        this.Construct;
        this.name       := To_Unbounded_String( name );
        this.allocator  := allocator;
        this.depsOrder  := dependencyOrder;
        this.propNames  := propertyNames.Copy;
        this.properties := Create_Map.Map;
        this.attributes := Create_Map.Map;
        this.required   := required.Copy;
        return this;
    end Create_Root;

    ----------------------------------------------------------------------------

    function Create_Component( this       : not null access Definition'Class;
                               properties : Map_Value ) return A_Component is
        actualProps : constant Map_Value := this.Get_Properties;
        result      : A_Component;
    begin
        -- 'properties' overrides definition properties
        actualProps.Recursive_Merge( properties );

        -- instantiate a component from the definition
        result := this.Get_Allocator.all( actualProps );
        result.definition := this.name;
        return result;
    end Create_Component;

    ----------------------------------------------------------------------------

    function Extends( this : not null access Definition'Class; defName : String ) return Boolean
    is (this.name = defName or else (this.parent /= null and then this.parent.Extends( defName )));

    ----------------------------------------------------------------------------

    function Get_Allocator( this : not null access Definition'Class ) return A_Component_Allocator is
    begin
        if this.allocator /= null then
            return this.allocator;
        end if;
        return this.parent.Get_Allocator;
    end Get_Allocator;

    ----------------------------------------------------------------------------

    function Get_Default_Attributes( this : not null access Definition'Class ) return Map_Value is
    begin
        -- root definition case
        if this.Is_Root then
            -- root component definitions don't specify default entity attributes
            return Create_Map.Map;
        end if;

        -- override parent attributes
        return mergedAttrs : constant Map_Value := this.parent.Get_Default_Attributes do
            mergedAttrs.Recursive_Merge( this.attributes );
        end return;
    end Get_Default_Attributes;

    ----------------------------------------------------------------------------

    function Get_Dependency_Order( this : not null access Definition'Class ) return Integer is
        def : A_Definition := A_Definition(this);
    begin
        while not def.Is_Root loop
            def := def.parent;
        end loop;

        -- only root component definitions have a hard-coded dependency order
        pragma Assert( def.depsOrder >= 0 );
        return def.depsOrder;
    end Get_Dependency_Order;

    ----------------------------------------------------------------------------

    function Get_Name( this : not null access Definition'Class ) return String is (To_String( this.name ));

    ----------------------------------------------------------------------------

    function Get_Own_Properties( this : not null access Definition'Class ) return Map_Value is (this.properties);

    ----------------------------------------------------------------------------

    function Get_Parent( this : not null access Definition'Class ) return A_Definition is (this.parent);

    ----------------------------------------------------------------------------

    function Get_Parent_Name( this : not null access Definition'Class ) return String is (To_String( this.parentName ));

    ----------------------------------------------------------------------------

    function Get_Properties( this : not null access Definition'Class ) return Map_Value is
    begin
        -- root definition case
        if this.Is_Root then
            return Clone( this.properties ).Map;
        end if;

        -- override parent properties
        return mergedProps : constant Map_Value := this.parent.Get_Properties do
            mergedProps.Recursive_Merge( this.properties );
        end return;
    end Get_Properties;

    ----------------------------------------------------------------------------

    function Get_Required( this : not null access Definition'Class ) return List_Value is
        required : constant List_Value := Create_List.Lst;
        obj      : A_Definition := A_Definition(this);
    begin
        while obj /= null loop
            for reqName of this.required loop
                required.Append( Create( reqName ) );
            end loop;
            obj := obj.parent;
        end loop;
        return required;
    end Get_Required;

    ----------------------------------------------------------------------------

    function Get_Root_Name( this : not null access Definition'Class ) return String is
    begin
        if this.parent /= null then
            return this.parent.Get_Name;
        end if;
        return this.Get_Name;
    end Get_Root_Name;

    ----------------------------------------------------------------------------

    function Is_Root( this : not null access Definition'Class ) return Boolean is (this.allocator /= null);

    ----------------------------------------------------------------------------

    function Is_Valid_Property( this     : not null access Definition'Class;
                                propName : String ) return Boolean is
    begin
        if this.parent /= null then
            return this.parent.Is_Valid_Property( propName );
        end if;
        return this.propNames.Contains( propName );
    end Is_Valid_Property;

    ----------------------------------------------------------------------------

    procedure Set_Parent( this   : not null access Definition'Class;
                          parent : not null A_Definition ) is
    begin
        pragma Assert( this.parent = null, "Parent is already set" );
        this.parent := parent;
    end Set_Parent;

    ----------------------------------------------------------------------------

    function Validate_Properties( this  : not null access Definition'Class;
                                  props : Map_Value ) return Boolean is
        success : Boolean := True;

        procedure Validate( propName : String; val : Value ) is
            pragma Unreferenced( val );
        begin
            if success then
                if not this.Is_Valid_Property( propName ) then
                    Dbg( "Unknown property '" & propName &
                         "' in component definition '" & To_String( this.name ) & "'",
                         D_ENTITY, Error );
                    success := False;
                end if;
            end if;
        end Validate;

    begin
        if props.Valid then
            props.Iterate( Validate'Access );
        end if;
        return success;
    end Validate_Properties;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Definition ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

end Entities.Component_Definitions;

