--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Exceptions;                    use Ada.Exceptions;
with Ada.Unchecked_Deallocation;
with Debugging;                         use Debugging;
with Entities.Entity_Attributes;        use Entities.Entity_Attributes;
with Entities.Factory;                  use Entities.Factory;
with Entities.Locations;                use Entities.Locations;
with Events.Entities;                   use Events.Entities;
with Events.Entities.Attributes;        use Events.Entities.Attributes;
with Events.Entities.Locations;         use Events.Entities.Locations;
with Events.World;                      use Events.World;
with Games;                             use Games;
with Scribble.Namespaces;               use Scribble.Namespaces;
with Support;                           use Support;
with Values.Casting;                    use Values.Casting;
with Values.Errors;                     use Values.Errors;
with Values.Functions;                  use Values.Functions;

package body Entities.Systems is

    procedure Handle_Delete_Entity( this : not null access Entity_System'Class;
                                    evt  : not null A_Entity_Event );

    procedure Handle_Entity_Directive( this : not null access Entity_System'Class;
                                       evt  : not null A_Entity_Directive_Event );

    procedure Handle_Message_Entity( this : not null access Entity_System'Class;
                                     evt  : not null A_Message_Entity_Event );

    procedure Handle_Move_Entity( this : not null access Entity_System'Class;
                                  evt  : not null A_Entity_Location_Event );

    procedure Handle_Resize_Entity( this : not null access Entity_System'Class;
                                    evt  : not null A_Entity_Size_Event );

    procedure Handle_Set_Entity_Attribute( this : not null access Entity_System'Class;
                                           evt  : not null A_Entity_Attribute_Event );

    procedure Handle_Spawn_Entity( this : not null access Entity_System'Class;
                                   evt  : not null A_Spawn_Entity_Event );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Entity_System( game : not null access Games.Game'Class ) return A_Entity_System is
        this : constant A_Entity_System := new Entity_System;
    begin
        this.Construct( game );
        return this;
    end Create_Entity_System;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Entity_System; game : not null access Games.Game'Class ) is
    begin
        Limited_Object(this.all).Construct;

        this.game := game;
        this.game.Get_Corral.Add_Listener( this, EVT_DELETE_ENTITY );
        this.game.Get_Corral.Add_Listener( this, EVT_ENTITY_DIRECTIVE );
        this.game.Get_Corral.Add_Listener( this, EVT_MESSAGE_ENTITY );
        this.game.Get_Corral.Add_Listener( this, EVT_MOVE_ENTITY );
        this.game.Get_Corral.Add_Listener( this, EVT_SPAWN_ENTITY );
        this.game.Get_Corral.Add_Listener( this, EVT_RESIZE_ENTITY );
        this.game.Get_Corral.Add_Listener( this, EVT_SET_ENTITY_ATTRIBUTE );

        this.thread := Create_Thread;
        this.thread.Register_Namespace( ANONYMOUS_NAMESPACE, game.Get_Script_VM.Get_Globals );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Entity_System ) is
        procedure Delete is new Ada.Unchecked_Deallocation( System_Lists.List, A_System_List );
    begin
        -- delete the registered systems
        for system of this.systemList loop
            Delete( system );
        end loop;
        this.systemList.Clear;
        for systemList of this.familyMap loop
            -- delete the system list of each family id
            Delete( systemList );
        end loop;
        this.familyMap.Clear;
        this.componentMap.Clear;

        Delete( this.thread );

        Limited_Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Associate( this : not null access Entity_System'Class;
                         comp : not null access Component'Class ) is
        crsr          : Family_Maps.Cursor;
        familySystems : A_System_List;
    begin
        this.componentMap.Include( comp.Get_Id, comp );

        crsr := this.familyMap.Find( comp.Get_Family );
        if Family_Maps.Has_Element( crsr ) then
            -- associate the component with each system in the list
            familySystems := Family_Maps.Element( crsr );
            for system of familySystems.all loop
                system.Associate( comp );
            end loop;
        end if;
    end Associate;

    ----------------------------------------------------------------------------

    procedure Delete_Entity( this : not null access Entity_System'Class; eid : Entity_Id ) is
        pos : Entity_Map.Cursor;
        e   : A_Entity;
    begin
        if this.entities /= null then
            pos := this.entities.Find( eid );
            if Entity_Map.Has_Element( pos ) then
                Entity_Map.Element( pos ).Dispatch_Message( MSG_Destruct );
            end if;
            -- search again in case the "Destruct" message invalidated the cursor
            pos := this.entities.Find( eid );
            if Entity_Map.Has_Element( pos ) then
                e := Entity_Map.Element( pos );
                this.entities.Delete( pos );
                pragma Debug( Dbg( "ECS: Deleting " & e.To_String, D_ENTITY, Info ) );
                Delete( e );
                Queue_Entity_Deleted( eid );
                Queue_World_Modified;
            end if;
        end if;
    end Delete_Entity;

    ----------------------------------------------------------------------------

    function Get_Component( this : not null access Entity_System'Class;
                            id   : Component_Id ) return A_Component is
        crsr : ComponentId_Maps.Cursor;
    begin
        crsr := this.componentMap.Find( id );
        if ComponentId_Maps.Has_Element( crsr ) then
            return ComponentId_Maps.Element( crsr );
        end if;
        return null;
    end Get_Component;

    ----------------------------------------------------------------------------

    function Get_Entity( this : not null access Entity_System'Class;
                         id   : Entity_Id ) return A_Entity is
        crsr : Entity_Map.Cursor;
    begin
        if this.entities /= null then
            crsr := this.entities.Find( id );
            if Entity_Map.Has_Element( crsr ) then
                return Entity_Map.Element( crsr );
            end if;
        end if;
        return null;
    end Get_Entity;

    ----------------------------------------------------------------------------

    function Get_Game( this : not null access Entity_System'Class ) return access Games.Game'Class is (this.game);

    ----------------------------------------------------------------------------

    function Get_Global_Timer( this : not null access Entity_System'Class ) return Time_Span is (this.globalTimer);

    ----------------------------------------------------------------------------

    function Get_Session_Var( this : not null access Entity_System'Class;
                              name : String ) return Value
    is (this.game.Get_Session.Get_Namespace_Name( name ));

    ----------------------------------------------------------------------------

    function Get_World( this : not null access Entity_System'Class ) return A_World is (this.world);

    ----------------------------------------------------------------------------

    procedure Handle_Event( this : access Entity_System;
                            evt  : in out A_Event;
                            resp : out Response_Type ) is
        pragma Unreferenced( resp );
    begin
        case evt.Get_Event_Id is
            when EVT_ENTITY_DIRECTIVE     => this.Handle_Entity_Directive( A_Entity_Directive_Event(evt) );
            when EVT_DELETE_ENTITY        => this.Handle_Delete_Entity( A_Entity_Event(evt) );
            when EVT_SPAWN_ENTITY         => this.Handle_Spawn_Entity( A_Spawn_Entity_Event(evt) );
            when EVT_MESSAGE_ENTITY       => this.Handle_Message_Entity( A_Message_Entity_Event(evt) );
            when EVT_MOVE_ENTITY          => this.Handle_Move_Entity( A_Entity_Location_Event(evt) );
            when EVT_RESIZE_ENTITY        => this.Handle_Resize_Entity( A_Entity_Size_Event(evt) );
            when EVT_SET_ENTITY_ATTRIBUTE => this.Handle_Set_Entity_Attribute( A_Entity_Attribute_Event(evt) );
            when others                   => null;
        end case;
    end Handle_Event;

    ----------------------------------------------------------------------------

    procedure Handle_Delete_Entity( this : not null access Entity_System'Class;
                                    evt  : not null A_Entity_Event ) is
    begin
        this.Delete_Entity( evt.Get_Id );
    end Handle_Delete_Entity;

    ----------------------------------------------------------------------------

    procedure Handle_Entity_Directive( this : not null access Entity_System'Class;
                                       evt  : not null A_Entity_Directive_Event ) is
        e : constant A_Entity := this.Get_Entity( evt.Get_Id );
    begin
        if e /= null then
            pragma Debug( Dbg( "ECS: Directive [" & Directive_Bits'Image( evt.Get_Directive ) &
                               ":" & Directive_Mode'Image( evt.Get_Mode ) &
                               "] -> Entity " & Image( evt.Get_Id ),
                               D_ENTITY, Info ) );
            e.Set_Directive( evt.Get_Directive, evt.Get_Mode );
        else
            -- this is not an exceptional case
            null;
            pragma Debug( Dbg( "ECS: Cannot set directive [" &
                               Directive_Bits'Image( evt.Get_Directive ) & ":" &
                               Directive_Mode'Image( evt.Get_Mode ) &
                               "]: Entity " & Image( evt.Get_Id ) & " not found",
                               D_ENTITY, Info ) );
        end if;
    end Handle_Entity_Directive;

    ----------------------------------------------------------------------------

    procedure Handle_Message_Entity( this : not null access Entity_System'Class;
                                     evt  : not null A_Message_Entity_Event ) is
        e : constant A_Entity := this.Get_Entity( evt.Get_Id );
    begin
        if e /= null then
            pragma Debug( Dbg( "ECS: Message '" & To_String( evt.Get_Message ) &
                               "' to " & e.To_String,
                               D_ENTITY, Info ) );
            e.Dispatch_Message( evt.Get_Message, evt.Get_Parameters );
        end if;
    end Handle_Message_Entity;

    ----------------------------------------------------------------------------

    procedure Handle_Move_Entity( this : not null access Entity_System'Class;
                                  evt  : not null A_Entity_Location_Event ) is
        e   : constant A_Entity := this.Get_Entity( evt.Get_Id );
        loc : A_Location;
    begin
        if e /= null then
            pragma Debug( Dbg( "ECS: Move " & e.To_String & " to " &
                               Image( evt.Get_X ) & ", " & Image( evt.Get_Y ),
                               D_ENTITY, Info ) );
            loc := A_Location(e.Get_Component( LOCATION_ID ));
            if loc /= null then
                loc.Set_XY( evt.Get_X, evt.Get_Y );
            end if;
        else
            Dbg( "Entity move failed: " &  Image( evt.Get_Id ) & " not found", D_ENTITY, Error );
        end if;
    end Handle_Move_Entity;

    ----------------------------------------------------------------------------

    procedure Handle_Resize_Entity( this : not null access Entity_System'Class;
                                    evt  : not null A_Entity_Size_Event ) is
        e   : constant A_Entity := this.Get_Entity( evt.Get_Id );
        loc : A_Location;
    begin
        if e /= null then
            pragma Debug( Dbg( "ECS: Resize " & e.To_String & " to " &
                               Image( evt.Get_Width ) & "x" & Image( evt.Get_Height ),
                               D_ENTITY, Info ) );
            loc := A_Location(e.Get_Component( LOCATION_ID ));
            if loc /= null then
                loc.Set_Size( evt.Get_Width, evt.Get_Height );
            end if;
        else
            Dbg( "Entity resize failed: " &  Image( evt.Get_Id ) & " not found", D_ENTITY, Error );
        end if;
    end Handle_Resize_Entity;

    ----------------------------------------------------------------------------

    procedure Handle_Set_Entity_Attribute( this : not null access Entity_System'Class;
                                           evt  : not null A_Entity_Attribute_Event ) is
        e : constant A_Entity := this.Get_Entity( evt.Get_Id );
    begin
        if e /= null then
            e.Get_Attributes.Set_Value( evt.Get_Attribute, evt.Get_Value );
        else
            Dbg( "Set entity attribute failed: " &  Image( evt.Get_Id ) & " not found", D_ENTITY, Error );
        end if;
    end Handle_Set_Entity_Attribute;

    ----------------------------------------------------------------------------

    procedure Handle_Spawn_Entity( this : not null access Entity_System'Class;
                                   evt  : not null A_Spawn_Entity_Event ) is
    begin
        if Is_Null( this.Spawn_Entity( evt.Get_Template, evt.Get_Properties ) ) then
            Dbg( "Failed to spawn entity <template: " & evt.Get_Template & ">", D_ENTITY, Error );
        end if;
    end Handle_Spawn_Entity;

    ----------------------------------------------------------------------------

    procedure Register_System( this   : not null access Entity_System'Class;
                               family : Family_Id;
                               system : access Component_System'Class ) is
        crsr     : Family_Maps.Cursor;
        inserted : Boolean;
    begin
        -- maintain the registration order of the systems
        this.systemList.Append( system );

        -- associate the system with its component family id
        crsr := this.familyMap.Find( family );
        if not Family_Maps.Has_Element( crsr ) then
            -- add the new family id with an empty system list
            this.familyMap.Insert( family, new System_Lists.List, crsr, inserted );
        end if;
        Family_Maps.Element( crsr ).Append( system );
    end Register_System;

    ----------------------------------------------------------------------------

    procedure Set_Session_Var( this : not null access Entity_System'Class;
                               name : String;
                               val  : Value'Class ) is
    begin
        this.game.Get_Session.Set_Var( name, val );
    end Set_Session_Var;

    ----------------------------------------------------------------------------

    procedure Set_World( this  : not null access Entity_System'Class;
                         world : A_World ) is
    begin
        -- detach the current world's entities from the system
        if this.world /= null then
            pragma Debug( Dbg( "ECS: Releasing world '" & Cast_String( this.world.Get_Property( "filepath" ) ) & "'",
                               D_ENTITY or D_WORLD, Info ) );

            -- send WorldUnloaded messages to all entities
            for e of this.entities.all loop
                e.Dispatch_Message( MSG_WorldUnloaded );
            end loop;
            for e of this.entities.all loop
                e.Set_System( null );
                pragma Debug( Dbg( "ECS: Released " & e.To_String, D_ENTITY, Info ) );
            end loop;

            this.entities := null;
        end if;

        this.world := world;
        if this.world /= null then
            pragma Debug( Dbg( "ECS: Managing world '" & Cast_String( this.world.Get_Property( "filepath" ) ) & "'",
                               D_ENTITY or D_WORLD, Info ) );

            -- attach the new world's entities to the system
            this.entities := world.Get_Entities;
            for e of this.entities.all loop
                e.Set_System( this );
                pragma Debug( Dbg( "ECS: Added " & e.To_String, D_ENTITY, Info ) );
            end loop;

            -- run the game's WorldLoaded handler
            this.game.Get_Session.Handle_Message( "WorldLoaded" );

            -- send WorldLoaded messages to all entities
            for e of this.entities.all loop
                e.Dispatch_Message( MSG_WorldLoaded );
            end loop;
        end if;
    end Set_World;

    ----------------------------------------------------------------------------

    procedure Shutdown( this : not null access Entity_System'Class ) is
    begin
        this.Set_World( null );

        this.game.Get_Corral.Remove_Listener( this );
        this.game := null;
    end Shutdown;

    ----------------------------------------------------------------------------

    function Spawn_Entity( this       : not null access Entity_System'Class;
                           template   : String;
                           properties : Map_Value ) return Entity_Id is
        eid : Entity_Id;
        e   : A_Entity;
        e2  : A_Entity;
    begin
        pragma Debug( Dbg( "ECS: Spawning entity '" & template & "'", D_ENTITY, Info ) );
        e := Entities.Factory.Global.Create_Entity( template, properties );
        if e = null then
            return Tagged_Ids.NULL_ID;
        end if;

        eid := e.Get_Id;
        e.Set_System( this );
        e2 := e;
        this.world.Add_Entity( e2 );       -- consumes 'e2', saved a copy in 'e'
        e.Dispatch_Message( MSG_Spawned );
        return eid;
    exception
        when e : others =>
            Dbg( "ECS: Failed spawn entity '" & template & ": " & Exception_Message( e ), D_ENTITY, Error );
            return Tagged_Ids.NULL_ID;
    end Spawn_Entity;

    ----------------------------------------------------------------------------

    procedure Start_Gameplay( this : not null access Entity_System'Class ) is
    begin
        pragma Debug( Dbg( "ECS: Starting gameplay", D_ENTITY or D_GAME, Info ) );
        pragma Assert( this.world /= null );
        if not this.game.Get_Session.Is_Editor then
            for e of this.entities.all loop
                e.Dispatch_Message( MSG_PlayStarted );
            end loop;
        end if;
    end Start_Gameplay;

    ----------------------------------------------------------------------------

    procedure Stop_Gameplay( this : not null access Entity_System'Class ) is
    begin
        -- cancel directives and send PlayStopped messages
        pragma Debug( Dbg( "ECS: Stopping gameplay", D_ENTITY or D_GAME, Info ) );
        for e of this.entities.all loop
            e.Cancel_Directives;
        end loop;
        if not this.game.Get_Session.Is_Editor then
            for e of this.entities.all loop
               e.Dispatch_Message( MSG_PlayStopped );
            end loop;
        end if;
    end Stop_Gameplay;

    ----------------------------------------------------------------------------

    overriding
    procedure Tick( this : access Entity_System; time : Tick_Time ) is
    begin
        -- backed by the total time Entity_System has been a running process
        this.globalTimer := time.total;

        -- tick all entities
        for e of this.entities.all loop
            e.Tick( time );
        end loop;

        -- tick all component systems
        for s of this.systemList loop
            s.Tick( time );
        end loop;
    end Tick;

    ----------------------------------------------------------------------------

    procedure Unassociate( this : not null access Entity_System'Class;
                           comp : not null access Component'Class ) is
        crsr          : Family_Maps.Cursor;
        crsr2         : ComponentId_Maps.Cursor;
        familySystems : A_System_List;
    begin
        crsr := this.familyMap.Find( comp.Get_Family );
        if Family_Maps.Has_Element( crsr ) then
            -- unassociate the component with each system in the list
            familySystems := Family_Maps.Element( crsr );
            for system of familySystems.all loop
                system.Unassociate( comp );
            end loop;
        end if;

        crsr2 := this.componentMap.Find( comp.Get_Id );
        if ComponentId_Maps.Has_Element( crsr2 ) then
            this.componentMap.Delete( crsr2 );
        end if;
    end Unassociate;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Entity_System ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

    --==========================================================================

    overriding
    procedure Delete( this : in out Component_System ) is
    begin
        this.components.Clear;

        Limited_Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Associate( this : not null access Component_System'Class;
                         comp : access Component'Class ) is
        crsr     : Component_Maps.Cursor;
        inserted : Boolean;
    begin
        this.components.Insert( comp.Get_Owner.Get_Id, comp, crsr, inserted );
        if not inserted then
            raise Constraint_Error with
                "Component " & comp.To_String &
                " doubly associated with a component system";
        end if;
    end Associate;

    ----------------------------------------------------------------------------

    procedure Unassociate( this : not null access Component_System'Class;
                           comp : access Component'Class ) is
        crsr : Component_Maps.Cursor;
    begin
        crsr := this.components.Find( comp.Get_Owner.Get_Id );
        if Component_Maps.Has_Element( crsr ) then
            this.components.Delete( crsr );
        end if;
    end Unassociate;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Component_System ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

end Entities.Systems;
