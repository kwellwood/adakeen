--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Indefinite_Hashed_Sets;
with Ada.Strings.Hash;
with Values.Lists;                      use Values.Lists;

package Entities.Component_Definitions is

    package String_Sets is new Ada.Containers.Indefinite_Hashed_Sets( String, Ada.Strings.Hash, "=", "=" );

    -- A Component_Allocator is responsible for allocating and initializing a
    -- new Component before it is added to an entity. The function must be
    -- registered with Define_Component using a unique name for the component
    -- definition. The allocator must always produce Components of the same class.
    --
    -- The 'properties' map passed to the allocator contains values that
    -- parameterize the new instance. Only instance-specific properties should
    -- be included. Properties that are common to all components produced by the
    -- allocator should be encapsulated within the allocator.
    type A_Component_Allocator is access
        function( properties : Map_Value ) return A_Component;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Definition is new Limited_Object with private;
    type A_Definition is access all Definition'Class;

    -- Creates and returns a component definition 'name' that extends component
    -- definition 'parentName'. At component construction, the new component
    -- instance's properties will be initialized to 'properties', which override
    -- the parent component definition's properties. At Entity construction,
    -- entity attributes 'attributes' will be defined.
    function Create_Extension( name       : String;
                               parentName : String;
                               properties : Map_Value;
                               attributes : Map_Value;
                               required   : String_Sets.Set ) return A_Definition;

    -- Creates and returns a root component definition named 'name', allocated
    -- by Ada function 'allocator'. Its dependency order relative to other
    -- component definitions during Entity construction is 'dependencyOrder',
    -- where greater numbers indicate more dependencies. The set of the
    -- component's defined property names is 'propertyNames'. When added to an
    -- Entity, the named components in 'required' (or extensions of them) must
    -- also be present.
    function Create_Root( name            : String;
                          allocator       : A_Component_Allocator;
                          dependencyOrder : Integer;
                          propertyNames   : String_Sets.Set;
                          required        : String_Sets.Set ) return A_Definition;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Creates a component instance from the definition. 'properties' given now
    -- at the time of instantiation, overrides any definition properties.
    function Create_Component( this       : not null access Definition'Class;
                               properties : Map_Value ) return A_Component;

    -- Returns True if the component definition is, or extends, component
    -- definition 'defName'.
    function Extends( this : not null access Definition'Class; defName : String ) return Boolean;

    -- Returns the allocator of the root component definition.
    function Get_Allocator( this : not null access Definition'Class ) return A_Component_Allocator;
    pragma Postcondition( Get_Allocator'Result /= null );

    -- Returns (by copy) the full, merged set of the definition's default
    -- entity attributes. The attributes of this definition are merged (and
    -- overwrite) the parent's default entity attributes, and so on, back to the
    -- root definition (which does not specify default attributes). The default
    -- entity attributes returned from this function will be assigned into an
    -- entity when a component is added, but only if the entity's matching
    -- attribute is Null.
    function Get_Default_Attributes( this : not null access Definition'Class ) return Map_Value;
    pragma Postcondition( Get_Default_Attributes'Result.Valid );

    -- Returns the definition's dependency order, relative to other components.
    -- Higher orders depend on lower orders. For example, the "Movement"
    -- definition depends on the "Location" definition and thus it has a higher
    -- order number.
    function Get_Dependency_Order( this : not null access Definition'Class ) return Integer;

    -- Returns the name of the definition.
    function Get_Name( this : not null access Definition'Class ) return String;

    -- Returns just the definition's own properties (by reference), unmerged
    -- with the parent's properties. To get the full, merged properties for
    -- instantiation, use Get_Properties.
    function Get_Own_Properties( this : not null access Definition'Class ) return Map_Value;
    pragma Postcondition( Get_Own_Properties'Result.Valid );

    -- Returns a pointer to the parent definition. This will return null until
    -- all component definitions have been loaded and validated.
    function Get_Parent( this : not null access Definition'Class ) return A_Definition;

    -- Returns the name of the component definition that this extends, or an
    -- empty if this is a root definition.
    function Get_Parent_Name( this : not null access Definition'Class ) return String;

    -- Returns (by copy) the full, merged set of the definition's properties.
    -- The properties of this definition are merged (and overwrite) the parent's
    -- properties, and so on, back to the root definition.
    function Get_Properties( this : not null access Definition'Class ) return Map_Value;
    pragma Postcondition( Get_Properties'Result.Valid );

    -- Returns a list of strings naming other component definitions that must be
    -- present in an entity when this one is used. The returned list may contain
    -- duplicate names.
    function Get_Required( this : not null access Definition'Class ) return List_Value;
    pragma Postcondition( Get_Required'Result.Valid );

    -- Returns the name of the definition's root.
    function Get_Root_Name( this : not null access Definition'Class ) return String;

    -- Returns True if this is a root component definition, registered by the
    -- application instead of extended from another.
    function Is_Root( this : not null access Definition'Class ) return Boolean;

    -- Returns True if 'propName' is a valid property name for component
    -- definition 'componentName'.
    function Is_Valid_Property( this     : not null access Definition'Class;
                                propName : String ) return Boolean;

    -- Sets the pointer to the component definition that this definition extends.
    procedure Set_Parent( this   : not null access Definition'Class;
                          parent : not null A_Definition );

    -- Returns True if all keys in 'props' are valid properties for this
    -- definition.
    function Validate_Properties( this  : not null access Definition'Class;
                                  props : Map_Value ) return Boolean;

    procedure Delete( this : in out A_Definition );

    MAX_COMPONENT_DEFS_DEPTH : constant := 32;

private

    type Definition is new Limited_Object with
        record
            name       : Unbounded_String;
            parentName : Unbounded_String;
            parent     : A_Definition := null;
            allocator  : A_Component_Allocator := null;
            propNames  : String_Sets.Set;
            depsOrder  : Integer := -1;
            properties : Map_Value;
            attributes : Map_Value;
            required   : String_Sets.Set;
        end record;

end Entities.Component_Definitions;
