
-- Copyright (c) 2012-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Entities.Components is

    -- Relative Tick Order of Components
    --
    TCK_ATTRIBUTES    : constant := 0;      -- not ticked
    TCK_VISIBLE       : constant := 0;      -- not ticked
    TCK_LOCATION      : constant := 0;      -- not ticked
    TCK_CAMERA_TARGET : constant := 0;      -- not ticked
    TCK_EMITTER       : constant := 0;      -- not ticked
    TCK_LIGHT         : constant := 0;      -- not ticked
    TCK_SCRIPT        : constant := 0;
    TCK_ANIMATION     : constant := 120;
    TCK_MOVEMENT      : constant := 140;
    TCK_MAPCLIP       : constant := 150;
    TCK_SOLID         : constant := 160;
    TCK_COLLIDABLE    : constant := 160;
    TCK_TILESENSOR    : constant := 160;

    -- Relative Dependency Order of Components
    --
    DEPS_ATTRIBUTES    : constant := 0;
    DEPS_LOCATION      : constant := DEPS_ATTRIBUTES + 10;
    DEPS_VISIBLE       : constant := DEPS_ATTRIBUTES + 10;
    DEPS_SCRIPT        : constant := DEPS_ATTRIBUTES + 10;
    DEPS_CAMERA_TARGET : constant := DEPS_ATTRIBUTES + 10;
    DEPS_EMITTER       : constant := DEPS_LOCATION + 10;
    DEPS_LIGHT         : constant := DEPS_LOCATION + 10;
    DEPS_COLLIDABLE    : constant := DEPS_LOCATION + 10;
    DEPS_TILESENSOR    : constant := DEPS_LOCATION + 10;
    DEPS_ANIMATION     : constant := DEPS_VISIBLE  + 10;
    DEPS_MOVEMENT      : constant := DEPS_LOCATION + 10;
    DEPS_SOLID         : constant := Integer'Max( DEPS_LOCATION, DEPS_MOVEMENT ) + 10;
    DEPS_MAPCLIP       : constant := Integer'Max( DEPS_LOCATION, DEPS_MOVEMENT ) + 10;

end Entities.Components;
