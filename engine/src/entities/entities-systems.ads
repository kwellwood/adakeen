--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Events.Listeners;                  use Events.Listeners;
with Scribble.VMs;                      use Scribble.VMs;
with Worlds;                            use Worlds;

limited with Games;

package Entities.Systems is

    -- Class Diagram of the Entity System
    --
    -- +------------------+      +-----------+
    -- | Component_System |----->| Component |
    -- +------------------+    * +-----------+
    --         ^ *                     ^ *
    --         |                       |
    --  +---------------+          +--------+
    --  | Entity_System |<-------->| Entity |
    --  +---------------+        * +--------+
    --         |                       ^
    --         |                       |
    --         |                   +-------+
    --         +------------------>| World |
    --                             +-------+

    -- The Entity_System is responsible for controlling and updating all
    -- entities, and their components, within the world during gameplay.
    --
    -- It runs as a Process in the Game's process manager, updating each of the
    -- entities once per tick, and it handles entity-related events, such as
    -- Spawn_Entity, Delete_Entity, etc.
    --
    -- The Entity_System updates each of the entities by calling their Tick
    -- procedure, which in turn allows each entity's components to update
    -- themselves using their own encapsulated logic.
    --
    -- The Entity_System also manages a list of component systems
    -- (Component_System objects) that directly update components of specific
    -- families. Each of these component systems is updated during Tick.
    -- Components are registered with the appropriate subsystems via Register
    -- and Unregister as they are added to / removed from entities in the world.
    type Entity_System is new Limited_Object and Event_Listener and Process with private;
    type A_Entity_System is access all Entity_System'Class;

    -- A Component_System is responsible for controlling and updating all the
    -- components of entities in the world that belong to a specific component
    -- family. (e.g.: The Collision_System handles the Location components of
    -- all the entities in the world and detects collisions between them.)
    --
    -- The difference between the logic encapsulated in a Component's Tick()
    -- procedure, and the the logic encapsulated in a Component_System's Tick()
    -- procedure is that the component's logic only relates to itself, but the
    -- system's logic relates to the interaction between many entities.
    type Component_System is abstract new Limited_Object with private;
    type A_Component_System is access all Component_System'Class;

    ----------------------------------------------------------------------------
    -- Entity_System Class --

    -- Creates a new Entity_System instance.
    function Create_Entity_System( game : not null access Games.Game'Class ) return A_Entity_System;

    -- Associates the component of an entity in the world with all interested
    -- component systems. The component must be unassociated later, before the
    -- active world is changed. If 'comp' has already been associate with
    -- component systems, the behavior is undefined.
    procedure Associate( this : not null access Entity_System'Class;
                         comp : not null access Component'Class );

    -- Deletes entity 'eid' by id. The entity will receive the "Destruct" message
    -- and appropriate events will be queued.
    procedure Delete_Entity( this : not null access Entity_System'Class; eid : Entity_Id );

    -- Returns component 'id' from the active world, or null if the component
    -- does not exist.
    function Get_Component( this : not null access Entity_System'Class;
                            id   : Component_Id ) return A_Component;

    -- Returns entity 'id' from the active world, or null if the entity does not
    -- exist.
    function Get_Entity( this : not null access Entity_System'Class;
                         id   : Entity_Id ) return A_Entity;

    -- Returns an access to the Game that owns the entity system.
    function Get_Game( this : not null access Entity_System'Class ) return access Games.Game'Class;

    -- Returns a global timer for animation. The absolute Time_Span value isn't
    -- important, but it can be used as a globally-consistent frequency
    -- reference so that animation frames across multiple entities can update in
    -- synchronization.
    function Get_Global_Timer( this : not null access Entity_System'Class ) return Time_Span with Inline;

    -- Returns the name of the object as a Process.
    overriding
    function Get_Process_Name( this : access Entity_System ) return String is ("Entity System");

    -- Returns (by copy) the current value of session variable 'name', or Null
    -- if it has not yet been assigned. This is a convenience function for
    -- components to access session variables because there is no direct
    -- reference path from Component to Session.
    function Get_Session_Var( this : not null access Entity_System'Class;
                              name : String ) return Value with Inline;

    -- Returns the active world. Null will be returned if no world is active,
    -- e.g. gameplay is not in progress.
    function Get_World( this : not null access Entity_System'Class ) return A_World with Inline;

    -- Registers a component system 'system' with the entity system, declaring
    -- its interest in processing components of the family 'family'. The
    -- relative processing order of the component systems is the same as their
    -- registration order. 'system' will be owned by this Entity_System
    -- (although, as a convenience, the pointer will be not consumed.)
    procedure Register_System( this   : not null access Entity_System'Class;
                               family : Family_Id;
                               system : access Component_System'Class );

    -- Sets (by copy) the current value of session variable 'name'. This is a
    -- convenience function for components to access session variables because
    -- there is no direct reference path from Component to Session.
    procedure Set_Session_Var( this : not null access Entity_System'Class;
                               name : String;
                               val  : Value'Class ) with Inline;

    -- Sets the active world containing the entities to be controlled. If
    -- 'world' is null, no entities will be controlled (e.g. at end of game, or
    -- in between world transitions).
    procedure Set_World( this  : not null access Entity_System'Class;
                         world : A_World );

    -- Spawns an entity in the active world using the entity template 'template'
    -- and entity properties 'properties'. See Entities.Factory.Create_Entity
    -- for details on how the properties map is structured. The new entity's id
    -- will be returned, or NULL_ID if the entity could not be spawned.
    function Spawn_Entity( this       : not null access Entity_System'Class;
                           template   : String;
                           properties : Map_Value ) return Entity_Id;

    -- Prepares the entity system to start/resume gameplay. This occurs after a
    -- world has been loaded and after play resumes from a paused state.
    -- PlayStarted messages will be sent to all entities.
    procedure Start_Gameplay( this : not null access Entity_System'Class );
    pragma Precondition( this.Get_World /= null );

    -- Prepares the entity system to stop gameplay. This occurs before the game
    -- session ends, before the world is changed, and when gameplay is paused.
    -- Any active entity directives will be cancelled and PlayStopped messages
    -- will be sent to all entities.
    procedure Stop_Gameplay( this : not null access Entity_System'Class );
    pragma Precondition( this.Get_World /= null );

    -- Updates all entities and then all registered component systems.
    overriding
    procedure Tick( this : access Entity_System; time : Tick_Time );

    -- Unassociates the component of an entity in the world with all component
    -- systems. If the component was not previously associated with any, nothing
    -- will happen.
    procedure Unassociate( this : not null access Entity_System'Class;
                           comp : not null access Component'Class );

    -- Shuts down the entity system and unregisters it as an event listener.
    -- Call this before deleting it.
    procedure Shutdown( this : not null access Entity_System'Class );

    -- Deletes the entity system and all its registered component systems.
    procedure Delete( this : in out A_Entity_System );
    pragma Postcondition( this = null );

    ----------------------------------------------------------------------------
    -- Abstract Component_System Class --

    -- Associates a component with the system. The component's family will match
    -- the system's component family of interest, as declared when the component
    -- system was added to the entity system. If 'comp' has already been
    -- associated, the behavior is undefined.
    procedure Associate( this : not null access Component_System'Class;
                         comp : access Component'Class );

    -- Updates all components associated with the system. This logic is
    -- implemented specifically by each system.
    not overriding
    procedure Tick( this : access Component_System; time : Tick_Time ) is abstract;

    -- Unassociates a component with the system. If the component was not
    -- associated with the system, nothing will happen.
    procedure Unassociate( this : not null access Component_System'Class;
                           comp : access Component'Class );

    -- Deletes the component system.
    procedure Delete( this : in out A_Component_System );
    pragma Postcondition( this = null );

private

    package System_Lists is new Ada.Containers.Doubly_Linked_Lists( A_Component_System, "=" );
    type A_System_List is access all System_Lists.List;

    package Family_Maps is new Ada.Containers.Ordered_Maps( Family_Id, A_System_List, "<", "=" );

    package ComponentId_Maps is new Ada.Containers.Ordered_Maps( Component_Id, A_Component, "<", "=" );

    type Entity_System is new Limited_Object and Event_Listener and Process with
        record
            game         : access Games.Game'Class := null;
            thread       : A_Thread := null;            -- Scribble execution thread
            systemList   : System_Lists.List;           -- in registration order
            familyMap    : Family_Maps.Map;             -- Family_Id -> component systems
            componentMap : ComponentId_Maps.Map;        -- Component_Id -> component
            world        : A_World := null;             -- the active world
            entities     : A_Entity_Map := null;        -- the world's entities
            globalTimer  : Time_Span := Time_Span_Zero; -- global animation timer
        end record;

    not overriding
    procedure Construct( this : access Entity_System; game : not null access Games.Game'Class );

    overriding
    procedure Delete( this : in out Entity_System );

    overriding
    procedure Handle_Event( this : access Entity_System;
                            evt  : in out A_Event;
                            resp : out Response_Type );

    ----------------------------------------------------------------------------

    package Component_Maps is new Ada.Containers.Ordered_Maps( Entity_Id, A_Component, "<", "=" );

    type Component_System is abstract new Limited_Object with
        record
            components : Component_Maps.Map;
        end record;

    overriding
    procedure Delete( this : in out Component_System );

end Entities.Systems;
