--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Hashed_Strings;                    use Hashed_Strings;

package Component_Families is

    -- Identifies a Component's family (or base class). Every component of the
    -- same family shares a common interface.
    subtype Family_Id is Hashed_String;

    -- Creates a Family_Id from a unique family name string.
    function To_Family_Id( name : String ) return Family_Id is (To_Hashed_String( name ));

    function Family_Name( id : Family_Id ) return String renames Hashed_Strings.To_String;

    ----------------------------------------------------------------------------

    ATTRIBUTES_ID : constant Family_Id := To_Family_Id( "Attributes" );
    LOCATION_ID   : constant Family_Id := To_Family_Id( "Location" );
    VISIBLE_ID    : constant Family_Id := To_Family_Id( "Visible" );

end Component_Families;
