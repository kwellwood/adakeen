--
-- Copyright (c) 2014-2015 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Doubly_Linked_Lists;
with Ada.Containers.Indefinite_Ordered_Maps;
with Hashed_Strings;                    use Hashed_Strings;
with Objects;                           use Objects;
with Processes;                         use Processes;
with Values.Maps;                       use Values.Maps;

-- This generic state machine implementation is a cross between a hierarchical
-- state machine (FA) and a pushdown automata (PDA). The states are organized in
-- tree, where each state in the tree is actually a substate of its parent. The
-- current state is represented with a stack of states (hence PDA) where the
-- state tree's root node is always at the bottom of the stack, topped by its
-- tree node descendents, in order.
--
--     Tree:    Stack: (upside down)
--
--   (GLOBAL)   GLOBAL  Example: See the state tree on the left. The tree
--    /    \      B     describes the state hierarchy and possible transitions.
--   A    (B)     C     State transitions always follow the branches of the
--        / \           state tree. When the current state is C, the states B
--      (C)  D          and GLOBAL will be on the state stack underneath it. To
--                      transition to state A from C, C will be exited, B will
--                      be exited, and then A will be entered. GLOBAL will stay.
--
-- This implementation is intended for use by entity component classes with
-- stateful behavior, because the states specifically provide Update,
-- Handle_Message, and Get_Frame actions that are handled by components.
-- Get_Frame isn't a standard component action, but it can be used to provide a
-- frame (tile id) for a visible entity in a stateful manner.
--
generic
    type Host_Access is private;
package Entities.States is

    -- A single state within the machine. The state may have substates. Enter
    -- and Exit actions are triggered when the machine transitions into and out
    -- of a state. Legal transitions are to a substate, ancestor state, or the
    -- direct sibling of an ancestor state.
    --
    -- Actions implemented by each state are Update, Handle_Message, and
    -- Get_Frame. If a state does not explicitly handle the action, its ancestor
    -- state gets an opportunity to handle the action. This supports common
    -- behavior between substates with a shared parent.
    type State is abstract tagged private;
    type A_State is access all State'Class;

    -- State ids identify states within a state machine. Each state should have
    -- a unique id within a machine.
    type State_Id is new Integer;

    -- the global state's id
    GLOBAL_STATE : constant State_Id := 0;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- The state machine maintains the current state, dispatches actions to it,
    -- and handles state transitions.
    type State_Machine is new Limited_Object with private;
    type A_State_Machine is access all State_Machine'Class;

    -- Creates a new state machine using 'global' as the global / root state.
    -- All its substates and their substates should have been added already.
    -- 'initialState' is the id of the machine's initial state. No On_Enter
    -- actions will be fired. Pass GLOBAL_STATE to start in 'global' itself.
    function Create_State_Machine( global       : not null A_State;
                                   initialState : State_Id := GLOBAL_STATE
                                 ) return A_State_Machine;
    pragma Postcondition( Create_State_Machine'Result /= null );

    -- An action that returns a visible frame (tile id) for the current state.
    function Get_Frame( this : not null access State_Machine'Class ) return Natural;

    -- Returns a sequence number that can be used to determine if the state
    -- machine has changed state. Each time the state changes, the sequence
    -- number changes. The meaning of an individual sequence number is undefined
    -- beyond the fact that a change in it indicates a state change.
    function Get_Sequence( this : not null access State_Machine'Class ) return Integer;

    -- Returns the id of the current state. This is the id of the deepest active
    -- state in the state tree provided at construction.
    function Get_State( this : not null access State_Machine'Class ) return State_Id;

    -- Returns an access to the component that owns the state machine. This
    -- allows states to access data and functionality provided by the component.
    function Host( this : not null access State_Machine'Class ) return Host_Access;

    -- An action that handles messages sent to the machine's host component.
    procedure Handle_Message( this   : not null access State_Machine'Class;
                              name   : Hashed_String;
                              params : Map_Value );

    -- Returns True if 'id' matches any state on the stack, not just the current
    -- deepest state.
    function In_State( this : not null access State_Machine'Class; id : State_Id ) return Boolean;

    -- Exits the deepest sub state, popping it off the state stack. If the
    -- global state is the current state, nothing will happen.
    procedure Pop_State( this : not null access State_Machine'Class );

    -- Sets the state machine's access to its host. This must be called before
    -- using the state machine.
    procedure Set_Host( this : not null access State_Machine'Class; host : Host_Access );

    -- Changes the current state to 'id'. There must be a valid transition from
    -- the current state. Valid transitions are substates of the current state,
    -- ancestors of the current state (including GLOBAL_STATE), or siblings of
    -- ancestors. If 'id' cannot be located, the machine will be left in the
    -- global state.
    procedure Set_State( this : not null access State_Machine'Class; id : State_Id );

    -- An action that handles gameplay update ticks.
    procedure Update( this : not null access State_Machine'Class; time : Tick_Time );

    -- Deletes the state machine and all its state objects.
    procedure Delete( this : in out A_State_Machine );
    pragma Postcondition( this = null );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Adds 'state' as a substate of this, identified uniquely within the state
    -- machine as 'id'. The state tree must be fully constructed before the
    -- state machine is created.
    procedure Add_State( this  : not null access State'Class;
                         id    : State_Id;
                         state : not null A_State );

    -- An action that returns a visible frame (tile id) for the current state.
    -- If the state returns 0, the parent state will be given an opportunity to
    -- handle the action.
    function Get_Frame( this : access State ) return Natural is (0);

    -- Returns an access to the state's owning component (same as the machine's
    -- host).
    function Host( this : not null access State'Class ) return Host_Access;

    -- An action that handles messages sent to the state machine's host. If the
    -- state does not set 'handled' to True, the parent state will be given an
    -- opportunity to handle the message.
    procedure Handle_Message( this    : access State;
                              name    : Hashed_String;
                              params  : Map_Value;
                              handled : in out Boolean ) is null;

    -- Returns the state's owner state machine.
    function Machine( this : access State ) return A_State_Machine;

    -- Called when the state is entered and pushed onto the stack. This will not
    -- be called again until the state has been exited.
    procedure On_Enter( this : access State ) is null;

    -- Called when the state is exited and popped from the stack. This will not
    -- be called again unless the state is re-entered. During a state
    -- transition, the On_Exit of the previous state is always called before
    -- the On_Enter of the next state.
    procedure On_Exit( this : access State ) is null;

    -- An action that handles gameplay update ticks. If the state does not set
    -- 'handled' to True, the parent state will be given an opportunity to
    -- handle the message.
    procedure Update( this    : access State;
                      time    : Tick_Time;
                      handled : in out Boolean ) is null;

    -- Delete the state and all its substates.
    procedure Delete( this : in out A_State );
    pragma Postcondition( this = null );

private

    package State_Lists is new Ada.Containers.Doubly_Linked_Lists( A_State, "=" );

    package State_Maps is new Ada.Containers.Indefinite_Ordered_Maps( State_Id, A_State, "<", "=" );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type State_Machine is new Limited_Object with
        record
            myHost : Host_Access;
            stack  : State_Lists.List;    -- contains the current states (last is global)
            seqNum : Integer := 0;        -- incremented on state change
        end record;

    procedure Construct( this         : access State_Machine;
                         global       : not null A_State;
                         initialState : State_Id );

    procedure Delete( this : in out State_Machine );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type State is abstract tagged
        record
            mach      : A_State_Machine := null;   -- set when an action is called
            id        : State_Id;                  -- set when added to another state
            subStates : State_Maps.Map;
        end record;

end Entities.States;
