--
-- Copyright (c) 2012-2013 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Doubly_Linked_Lists;
with Entities.Collidables;              use Entities.Collidables;

package body Entities.Systems.Collision is

    function Create_Collision_System return A_Component_System is
        this : constant A_Component_System := new Collision_System;
    begin
        Collision_System(this.all).Construct;
        return this;
    end Create_Collision_System;

    ----------------------------------------------------------------------------

    overriding
    procedure Tick( this : access Collision_System; time : Tick_Time ) is
        pragma Unreferenced( time );

        package Collidable_Lists is new Ada.Containers.Doubly_Linked_Lists( A_Collidable, "=" );

        moved        : Collidable_Lists.List;
        col1         : A_Collidable;
        col2         : A_Collidable;
        wereTouching : Boolean;
        areTouching  : Boolean;
    begin
        -- build a list of the entities that moved on the last update
        for component of this.components loop
            if A_Collidable(component).Is_Changed then
                moved.Append( A_Collidable(component) );
            end if;
        end loop;

        -- loop over all moved entities (m)
        for m of moved loop
            col1 := m;

            -- check for collisions with all other entities (n)
            for n of this.components loop
                col2 := A_Collidable(n);

                if col1 /= col2 then
                    wereTouching := col1.Is_Touching( col2.Get_Owner.Get_Id );
                    areTouching := col1.Detect_Collision( col2 );
                    if areTouching /= wereTouching then
                        col1.Set_Touching( col2.Get_Owner.Get_Id, areTouching );
                        col2.Set_Touching( col1.Get_Owner.Get_Id, areTouching );
                    end if;
                end if;

            end loop;
        end loop;
    end Tick;

end Entities.Systems.Collision;
