--
-- Copyright (c) 2013-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Directives is

    pragma Pure;

    -- Up to 64 directives are supported, one for each bit in Directive_Type.
    type Directive_Bits is mod 2**64;

    NO_DIRECTIVES : constant Directive_Bits := 0;

    -- Converts a single directive number (0..63) into a Directive_Bits type.
    function To_Directive( d : Natural ) return Directive_Bits is (Directive_Bits(2**d));

    -- The mode of a directive to an entity, to control behavior. A 'Once'
    -- directive is active for a single logic frame. An 'Ongoing' directive is
    -- active until it is explicitly cancelled or effectively cancelled with an
    -- 'Inactive' mode directive.
    type Directive_Mode is (Once, Ongoing, Inactive);

    type Directive_Modes is array (Directive_Mode range Once..Ongoing) of Directive_Bits;

end Directives;
