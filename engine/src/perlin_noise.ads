--
-- Adapted from Ken Perlin's Improved Noise reference implementation
--
-- Copyright 2002 Ken Perlin
--

with Objects;                           use Objects;

package Perlin_Noise is

    type Noise_Generator is new Object with private;
    type A_Noise_Generator is access all Noise_Generator'Class;

    function Create_Noise_Generator( repeat : Natural := 0 ) return A_Noise_Generator;

    function Noise( this : Noise_Generator'Class; x, y, z : Long_Float ) return Long_Float;

    function Octave_Noise( this        : Noise_Generator'Class;
                           x, y, z     : Long_Float;
                           octaves     : Positive;
                           persistence : Long_Float ) return Long_Float;

    procedure Delete( this : in out A_Noise_Generator );
    pragma Postcondition( this = null );

private

    type Int_Array is array(Natural range <>) of Integer;

    type Noise_Generator is new Object with
        record
            repeat : Natural := 0;
            p      : Int_Array(0..511);
        end record;

    procedure Construct( this : access Noise_Generator; repeat : Natural );

end Perlin_Noise;
