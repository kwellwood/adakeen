--
-- Copyright (c) 2016-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

generic
    type Element_Type is private;

package Ring_Buffers is

    type Buffer(size : Integer) is tagged private;

    -- Returns true if the buffer does not contain any data.
    function Is_Empty( buf : Buffer ) return Boolean;

    -- Returns true if the buffer is completely full.
    function Is_Full( buf : Buffer ) return Boolean;

    -- Returns the number of elements in the buffer.
    function Length( buf : Buffer ) return Natural;

    -- Returns element 'index' in the buffer. This does not change the contents.
    -- Indices always start at 1.
    function Peek( buf : Buffer; index : Integer ) return Element_Type;

    -- Updates element 'index' in the buffer. Indices always start at 1.
    procedure Poke( buf : in out Buffer; index : Integer; elem : Element_Type );

    -- Pops the first element off the buffer. The caller is responsible for
    -- ensuring the buffer is not empty.
    procedure Pop( buf : in out Buffer; elem : out Element_Type );

    -- Pushes 'elem' into the buffer as the last element. The caller is
    -- responsible for ensuring there is space avaiable.
    procedure Push( buf : in out Buffer; elem : Element_Type );

private

    type Element_Array is array (Integer range <>) of Element_Type;

    type Buffer(size : Integer) is tagged
        record
            elements : Element_Array(0..size);
            rpos     : Integer := 0;    -- first element waiting to be read
            wpos     : Integer := 0;    -- first element waiting to be written (one past data)
        end record;

end Ring_Buffers;
