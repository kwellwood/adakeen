--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Ada.Float_Text_IO;
with GNAT.OS_Lib;                       use GNAT.OS_Lib;
with GNAT.Random_Numbers;

#if WINDOWS'Defined then
with Support.Win;
#elsif OSX'Defined then
with Support.OSX;
#elsif LINUX'Defined then
with Support.Linux;
#end if;

package body Support is

    function Capitalize_First( str : String ) return String is
        result : String := str;
    begin
        if result'Length > 0 then
            result(result'First) := To_Upper( result(result'First) );
        end if;
        return result;
    end Capitalize_First;

    ----------------------------------------------------------------------------

    function Capitalize( str : String ) return String is
        result : String := To_Lower( str );
        capIt  : Boolean := True;
    begin
        for i in result'Range loop
            if Is_Alphanumeric( result(i) ) then
                if capIt then
                    result(i) := To_Upper( result(i) );
                    capIt := False;
                end if;
            else
                capIt := True;
            end if;
        end loop;
        return result;
    end Capitalize;

    ----------------------------------------------------------------------------

    function Constrain( val, min, max : Float ) return Float is
        realMin : constant Float := Float'Min( min, max );
        realMax : constant Float := Float'Max( min, max );
    begin
        if val < realMin then
            return realMin;
        elsif val > realMax then
            return realMax;
        else
            return val;
        end if;
    end Constrain;

    ----------------------------------------------------------------------------

    function Constrain( val, min, max : Integer ) return Integer is
        realMin : constant Integer := Integer'Min( min, max );
        realMax : constant Integer := Integer'Max( min, max );
    begin
        if val < realMin then
            return realMin;
        elsif val > realMax then
            return realMax;
        else
            return val;
        end if;
    end Constrain;

    ----------------------------------------------------------------------------

    function Div_Ceil( a, b : Integer ) return Integer is
        result : Integer := a / b;
    begin
        if a mod b /= 0 then
            result := result + 1;
        end if;
        return result;
    end Div_Ceil;

    ----------------------------------------------------------------------------

    function Fmod( x, y : Float ) return Float is
        x_y : constant Float := x / y;
    begin
        return (x_y - Float'Copy_Sign( Float'Floor(abs x_y), x_y )) * y;
    end Fmod;

    ----------------------------------------------------------------------------

    function Fmod( x, y : Long_Float ) return Long_Float is
        x_y : constant Long_Float := x / y;
    begin
        return (x_y - Long_Float'Copy_Sign( Long_Float'Floor(abs x_y), x_y )) * y;
    end Fmod;

    ----------------------------------------------------------------------------

    procedure Swap( a, b : in out Float ) is
        tmp : constant Float := a;
    begin
        a := b;
        b := tmp;
    end Swap;

    ----------------------------------------------------------------------------

    function EOL return String renames OS.EOL;

    ----------------------------------------------------------------------------

    function Ends_With( str : String; ending : String ) return Boolean
    is (ending'Length <= str'Length and then str(str'Last-ending'Length+1..str'Last) = ending);

    ----------------------------------------------------------------------------

    procedure Iterate_Words( phrase  : String;
                             examine : access procedure( word : String ) ) is
        start      : Integer := phrase'First;
        whitespace : Boolean := True;
    begin
        for c in phrase'Range loop
            if phrase(c) = ' ' and then not whitespace then
                -- finished a word
                whitespace := True;
                examine.all( phrase(start..c-1) );
            elsif whitespace and then phrase(c) /= ' ' then
                -- found a non-whitespace
                start := c;
                whitespace := False;
            end if;
        end loop;

        if not whitespace then
            examine.all( phrase(start..phrase'Last) );
        end if;
    end Iterate_Words;

    ----------------------------------------------------------------------------

    function Replace( str : String; from, to : Character ) return String is
        result : String := str;
    begin
        for i in result'Range loop
            if result(i) = from then
                result(i) := to;
            end if;
        end loop;
        return result;
    end Replace;

    ----------------------------------------------------------------------------

    function File_Length( path : String ) return Long_Integer is
        fd  : File_Descriptor;
        len : Long_Integer := -1;
    begin
        fd := Open_Read( path, Binary );
        if fd /= Invalid_FD then
            len := File_Length( fd );
            Close( fd );
        end if;
        return len;
    end File_Length;

    ----------------------------------------------------------------------------

    function Source_Ref_To_Unit_Name( ref : String ) return String is

        ------------------------------------------------------------------------

        -- Parses a string describing a protected source unit and returns a more
        -- readable name. Ex: "<my_task__L_2__B790b__startb2>" -> "My_Task.Start"
        function Protected_Name( name : String ) return String is
            uname : Unbounded_String := To_Unbounded_String( name(name'First+1..name'Last-1) );
            i, j  : Integer;
        begin
            -- convert "__" unit separators to "."
            i := Index( uname, "__" );
            while i > 0 loop
                Replace_Slice( uname, i, i + 1, "." );
                i := Index( uname, "__" );
            end loop;

            -- remove auto-generated units. their names begin with a capital letter.
            i := Index( uname, "." );
            while i > 0 loop
                if Is_Upper( Element( uname, i + 1 ) ) then
                    -- this unit is auto-generated; remove it and don't change i
                    -- because i will point to the next unit after the removal.
                    j := Index( uname, ".", i + 2 );
                    if j > i then
                        Replace_Slice( uname, i, j, "." );
                    end if;
                else
                    -- this unit is ok; move on to the next.
                    i := Index( uname, ".", i + 1);
                end if;
            end loop;

            -- remove the postfix. it is a series of capital letters optionally
            -- followed by a number.
            i := Length( uname );
            loop
                exit when i = 0 or else Is_Lower(Element( uname, i ) );
                i := i - 1;
            end loop;
            return Slice( uname, 1, i );
        end Protected_Name;

        ------------------------------------------------------------------------

        first : constant Integer := Index( ref, " in " ) + 4;
        last  : constant Integer := Index( ref, " at " ) - 1;
        unit  : constant String := ref(first..last);
    begin
        if unit(unit'First) = '<' then
            return Capitalize( Protected_Name( unit ) );
        else
            return Capitalize( unit );
        end if;
    end Source_Ref_To_Unit_Name;

    ----------------------------------------------------------------------------

    function Snap( point    : Float;
                   stepSize : Float;
                   centered : Boolean := False ) return Float is
        snapAmt : Float := 0.0;
    begin
        if centered then
            snapAmt := Fmod( (point + stepSize / 2.0), stepSize );
        else
            snapAmt := Fmod( point, stepSize );
        end if;
        if snapAmt <= stepSize / 2.0 then
            return point - snapAmt;
        else
            return point + (stepSize - snapAmt);
        end if;
    end Snap;

    ----------------------------------------------------------------------------

    function Image( f : Float; precision : Natural := 3 ) return String is
        txt : String(1..18) := (others => ' ');
    begin
        Ada.Float_Text_IO.Put( txt, f, Aft => precision, Exp => 0 );
        return Ada.Strings.Fixed.Trim( txt, Both );
    end Image;

    ----------------------------------------------------------------------------

    generator : GNAT.Random_Numbers.Generator;

    function Random_32 return Unsigned_32 is (Unsigned_32'(GNAT.Random_Numbers.Random( generator )));

    function Random_Float return Float is (Float'(GNAT.Random_Numbers.Random( generator )));

    ----------------------------------------------------------------------------

    function Read_String_16( stream : access Root_Stream_Type'Class ) return Unbounded_String is
        str : Ada.Strings.Unbounded.String_Access := new String(1..Integer(Unsigned_16'Input( stream )));
    begin
        if str'Length > 0 then
            String'Read( stream, str.all );
        end if;
        return result : Unbounded_String do
            result := To_Unbounded_String( str.all );
            Free( str );
        end return;
    end Read_String_16;

    ----------------------------------------------------------------------------

    function Read_String_32( stream : access Root_Stream_Type'Class ) return Unbounded_String is
        str : Ada.Strings.Unbounded.String_Access := new String(1..Natural'Input( stream ));
    begin
        if str'Length > 0 then
            String'Read( stream, str.all );
        end if;
        return result : Unbounded_String do
            result := To_Unbounded_String( str.all );
            Free( str );
        end return;
    end Read_String_32;

    ----------------------------------------------------------------------------

    procedure Write_String_16( stream : access Root_Stream_Type'Class; str : Unbounded_String ) is
    begin
        Unsigned_16'Output( stream, Unsigned_16(Length( str )) );
        if Length( str ) > 0 then
            String'Write( stream, To_String( str ) );
        end if;
    end Write_String_16;

    ----------------------------------------------------------------------------

    procedure Write_String_32( stream : access Root_Stream_Type'Class; str : Unbounded_String ) is
    begin
        Integer'Output( stream, Length( str ) );
        if Length( str ) > 0 then
            String'Write( stream, To_String( str ) );
        end if;
    end Write_String_32;

    --==========================================================================

    package body OS is

#if WINDOWS'Defined then
        package Impl renames Support.Win;
#elsif OSX'Defined then
        package Impl renames Support.OSX;
#elsif LINUX'Defined then
        package Impl renames Support.Linux;
#end if;

        function App_Data_Directory return String renames Impl.App_Data_Directory;

        procedure Auto_Open( filePath : String ) renames Impl.Auto_Open;

        function EOL return String renames Impl.EOL;

        function Executable_Extension return String renames Impl.Executable_Extension;

        function Executable_Path return String renames Impl.Executable_Path;

        function Execution_Directory return String renames Impl.Execution_Directory;

        function Home_Directory return String renames Impl.Home_Directory;

        procedure Reveal_Path( path : String ) renames Impl.Reveal_Path;

        function System_Font_Directory return String renames Impl.System_Font_Directory;

        function Temp_Directory return String renames Impl.Temp_Directory;

    end OS;

begin

    GNAT.Random_Numbers.Reset( generator );

end Support;
