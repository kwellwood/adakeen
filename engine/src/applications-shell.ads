--
-- Copyright (c) 2012-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Applications.Shell is

    -- An abstract command-line application class without a window.
    type Shell_Application is abstract new Application with private;
    type A_Shell_Application is access all Shell_Application'Class;

    -- Returns a string announcing the product. It is formatted as:
    -- [APPNAME] & " v[VERSION] - [COMPANY] Copyright (C) [BUILD_YEAR]
    function Get_Announce( this : not null access Shell_Application'Class ) return String;

private

    type Shell_Application is abstract new Application with null record;

    -- Calls Os_Exit to stop execution without returning.
    procedure Kill( this : access Shell_Application );

end Applications.Shell;
