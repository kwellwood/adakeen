--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Streams.Stream_IO;             use Ada.Streams.Stream_IO;
with Ada.Exceptions;                    use Ada.Exceptions;
with Applications;                      use Applications;
with Debugging;                         use Debugging;
with Events.Game;                       use Events.Game;
with Events.World;                      use Events.World;
with Games;                             use Games;
with Resources;                         use Resources;
with Resources.Scribble;                use Resources.Scribble;
with Scribble;                          use Scribble;
with Scribble.Compilers;                use Scribble.Compilers;
with Scribble.Parse_Errors;             use Scribble.Parse_Errors;
with Support;                           use Support;
with Support.Paths;                     use Support.Paths;
with Values.Casting;                    use Values.Casting;
with Values.Construction;               use Values.Construction;
with Values.Errors;                     use Values.Errors;
with Values.Indirects;
with Values.Lists;                      use Values.Lists;
with Values.Streaming;                  use Values.Streaming;
with Values.Strings;                    use Values.Strings;

package body Sessions is

    IDENTIFIER   : constant String := "galaxy:game";
    FILE_VERSION : constant Integer := 2 * 100_00 + Worlds.WRITE_VERSION;

    ----------------------------------------------------------------------------

    package Session_Refs is new Values.Indirects.Generics( Session );

    ----------------------------------------------------------------------------

    -- Handles a Load_World event to change the active world.
    procedure Handle_Load_World( this : not null access Session'Class;
                                 evt  : not null A_Load_World_Event;
                                 resp : out Response_Type );

    -- Handles a Game_Message event.
    procedure Handle_Message( this : not null access Session'Class;
                              evt  : not null A_Message_Event;
                              resp : out Response_Type );

    -- Handles a Pause_Game event to pause/resume gameplay.
    procedure Handle_Pause_Game( this : not null access Session'Class;
                                 evt  : not null A_Gameplay_Event );

    -- Handles a Run_Script event, compiling and executing the script's source.
    procedure Handle_Run_Script( this : not null access Session'Class;
                                 evt  : not null A_Script_Event;
                                 resp : out Response_Type );

    -- Handles a Start_Game event to restart the game session.
    procedure Handle_Start_Game( this : not null access Session'Class );

    -- Changes the active world to 'world', sending all the necessary events.
    -- If 'world' is not null, it will be stored in the world cache as 'filename'.
    procedure Set_Active_World( this     : not null access Session'Class;
                                filename : String;
                                world    : A_World );

    -- Cleans up after script thread 'thread' is no longer running, dumping a
    -- stack trace on error and queueing a Script_Done event as necessary. The
    -- thread 'thread' will be deleted.
    procedure Terminate_Script( this   : not null access Session'Class;
                                thread : in out A_Thread;
                                taskId : Integer );
    pragma Precondition( thread /= null );
    pragma Postcondition( thread = null );

    -- Updates the "tasks" session variable when a background task is added or
    -- removed.
    procedure Update_Tasks_Var( this : not null access Session'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    overriding
    procedure Construct( this : access Session ) is
    begin
        Object(this.all).Construct;
        this.worldController := Create_World_Controller;
        this.vars := Create_Map.Map;
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Session ) is
    begin
        pragma Assert( this.Get_World = null );
        pragma Assert( this.worlds.Is_Empty );
        pragma Assert( this.tasks.Is_Empty );

        Delete( this.worldController );

        Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Initialize( this : not null access Session'Class;
                          game : not null access Games.Game'Class ) is
    begin
        this.game := game;

        this.worldController.Initialize( this.game.Get_Corral );

        this.game.Get_Corral.Add_Listener( this, EVT_START_GAME );
        this.game.Get_Corral.Add_Listener( this, EVT_GAME_MESSAGE );
        this.game.Get_Corral.Add_Listener( this, EVT_LOAD_WORLD );
        this.game.Get_Corral.Add_Listener( this, EVT_PAUSE_GAME );
        this.game.Get_Corral.Add_Listener( this, EVT_RUN_SCRIPT );
        this.game.Attach( this );

        -- initialize the sub-class
        this.On_Initialize;
    end Initialize;

    ----------------------------------------------------------------------------

    procedure Finalize( this : not null access Session'Class ) is
    begin
        this.Pause;
        this.game.Get_Entity_System.Set_World( null );
        this.worldController.Set_World( null );

        this.game.Detach( this );
        this.game.Get_Corral.Remove_Listener( this );

        for w of this.worlds loop
            Delete( w.world );
        end loop;
        this.worlds.Clear;
        this.vars := Create_Map.Map;

        for t of this.tasks loop
            pragma Debug( Dbg( "Session: Aborted script " & t.thread.Get_First_Frame &
                               " [id " & Image( t.taskId ) & "]",
                               D_GAME or D_SCRIPT, Info ) );
            Delete( t.thread );
        end loop;
        this.tasks.Clear;

        -- finalize the sub-class
        this.On_Finalize;
    end Finalize;

    ----------------------------------------------------------------------------

    overriding
    function Get_Namespace_Name( this : access Session;
                                 name : String ) return Value is (this.Get_Var( name ));

    ----------------------------------------------------------------------------

    overriding
    function Get_Namespace_Ref( this : access Session;
                                name : String ) return Value
    is (Session_Refs.Create_Indirect( this, Get_Var'Access, Set_Var'Access, name ));

    ----------------------------------------------------------------------------

    function Get_Var( this : not null access Session'Class;
                      name : String ) return Value is
        val : constant Value := this.vars.Get( name );
    begin
        if val.Is_Null then
            Queue_Console_Text( "Session var '" & name & "' not found" );
            Dbg( "Session var '" & name & "' not found", D_GAME or D_SCRIPT, Error );
        end if;
        return val;
    end Get_Var;

    ----------------------------------------------------------------------------

    function Get_World( this : not null access Session'Class ) return A_World is (this.worldController.Get_World);

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Event( this : access Session;
                            evt  : in out A_Event;
                            resp : out Response_Type ) is
    begin
        Set_Response( resp, ST_SUCCESS );
        case evt.Get_Event_Id is
            when EVT_START_GAME   => this.Handle_Start_Game;
            when EVT_GAME_MESSAGE => this.Handle_Message( A_Message_Event(evt), resp );
            when EVT_LOAD_WORLD   => this.Handle_Load_World( A_Load_World_Event(evt), resp );
            when EVT_PAUSE_GAME   => this.Handle_Pause_Game( A_Gameplay_Event(evt) );
            when EVT_RUN_SCRIPT   => this.Handle_Run_Script( A_Script_Event(evt), resp );
            when others           => Set_Response( resp, ST_NONE );
        end case;
    end Handle_Event;

    ----------------------------------------------------------------------------

    procedure Handle_Load_World( this : not null access Session'Class;
                                 evt  : not null A_Load_World_Event;
                                 resp : out Response_Type ) is
    begin
        this.Request_Load_World( evt.Get_Filename );   -- may raise exception
        Set_Response( resp, ST_SUCCESS );
    exception
        when e : others =>
            -- Loading the world from disk failed. The current state of the
            -- session remains unchanged.
            Set_Response( resp, ST_FAILED, Create( Generic_Exception, Exception_Message( e ) ) );
    end Handle_Load_World;

    ----------------------------------------------------------------------------

    procedure Handle_Message( this : not null access Session'Class;
                              evt  : not null A_Message_Event;
                              resp : out Response_Type ) is
    begin
        -- LoadGame( filename : string )
        if evt.Get_Name = "LoadGame" then
            begin
                this.Request_Load( Cast_String( evt.Get_Args.Get( 1 ) ) );
            exception
                when e : others =>
                    -- Loading the session from disk failed. The state of the
                    -- current session remains unchanged.
                    Set_Response( resp, ST_FAILED, Create( Generic_Exception, Exception_Message( e ) ) );
            end;

        -- SaveGame( filename : string )
        elsif evt.Get_Name = "SaveGame" then
            this.Request_Save( Cast_String( evt.Get_Args.Get( 1 ) ) );

        -- handle all other Message events with scripts
        elsif evt.Get_Args.Valid then
            declare
                args : Value_Array(1..evt.Get_Args.Length);
            begin
                evt.Get_Args.To_Array( args );
                this.Handle_Message( evt.Get_Name, args );
            end;
        else
            this.Handle_Message( evt.Get_Name );
        end if;
    end Handle_Message;

    ----------------------------------------------------------------------------

    procedure Handle_Message( this : not null access Session'Class;
                              name : String;
                              args : Value_Array ) is
        handler : Function_Value;
        ignored : Value;
    begin
        -- dispatch the message to a Scribble hander synchronously
        handler := this.vars.Get( "On" & name ).Func;
        if handler.Valid then
            ignored := this.game.Get_Session.Run_Script( "game.On" & name, handler, args, taskId => 0 );
        end if;

        -- dispatch the message to an Ada handler implemented in the subclass
        this.On_Message( name, args );
    end Handle_Message;

    ----------------------------------------------------------------------------

    procedure Handle_Message( this : not null access Session'Class;
                              name : String ) is
        noArgs : Value_Array(1..0);
    begin
        this.Handle_Message( name, noArgs );
    end Handle_Message;

    ----------------------------------------------------------------------------

    procedure Handle_Pause_Game( this : not null access Session'Class;
                                 evt  : not null A_Gameplay_Event ) is
    begin
        -- pause gameplay
        if evt.Is_Paused then
            if this.playing then
                this.Request_Pause;
            end if;
        else -- resume gameplay
            if not this.playing and this.Get_World /= null then
                -- can't resume without a loaded world
                this.Request_Resume;
            end if;
        end if;
    end Handle_Pause_Game;

    ----------------------------------------------------------------------------

    procedure Handle_Run_Script( this : not null access Session'Class;
                                 evt  : not null A_Script_Event;
                                 resp : out Response_Type ) is
        prefix : constant String := "function() return (" & ASCII.LF;
        suffix : constant String := ASCII.LF & "); end function";
        source : constant Unbounded_String := prefix & evt.Get_Source & suffix;
        func   : Function_Value;
        args   : Value_Array(1..0);
        val    : Value;
        pragma Warnings( Off, val );
    begin
        -- compile it
        -- script has access to world, game, and engine namespaces
        begin
            func := this.game.Get_Script_VM.Get_Compiler.Compile( source, "", enableDebug => True ).Func;
        exception
            when e : Parse_Error =>
                val := Create( Compile_Error, Format_Parse_Error( Exception_Message( e ), source, omitLines => 1 ) );
                Set_Response( resp, ST_FAILED, val );
                if evt.Get_TaskId /= 0 then
                    Queue_Script_Done( val, evt.Get_TaskId );
                end if;
                return;
        end;

        -- run the script completely right now
        val := this.Run_Script( "expression", func, args, evt.Get_TaskId );
    end Handle_Run_Script;

    ----------------------------------------------------------------------------

    procedure Handle_Start_Game( this : not null access Session'Class ) is
    begin
        this.Request_Restart;
    end Handle_Start_Game;

    ----------------------------------------------------------------------------

    function Is_Editor( this : not null access Session'Class ) return Boolean is (this.isEditor);

    ----------------------------------------------------------------------------

    procedure Load( this : not null access Session'Class; filename : String ) is
        use Worlds_Cache;
        path   : constant String := Get_Application.Get_User_Directory & filename;
        file   : Ada.Streams.Stream_IO.File_Type;
        recg   : String(1..IDENTIFIER'Length);
        ver    : Integer := 0;
        header : Value;
        worlds : List_Value;
        index  : Integer := 0;
        vars   : Map_Value;
        cache  : Worlds_Cache.Map;
        item   : Cached_World;
        world  : A_World;
    begin
        pragma Debug( Dbg( "Loading game: " & path, D_GAME, Info ) );
        Open( file, In_File, path );                  -- may raise exception

        -- file identifier and version
        String'Read( Stream( file ), recg );
        if recg /= IDENTIFIER then
            raise READ_EXCEPTION with "Unrecognized file format";
        end if;

        ver := Integer'Input( Stream( file ) );
        if ver /= FILE_VERSION then
            raise READ_EXCEPTION with "Unsupported file version " & Image( ver ) &
                                      "; current version is " & Image( FILE_VERSION );
        end if;

        -- header info:
        --   list of open worlds
        --   current world
        --   session vars
        header := Value_Input( Stream( file ) );
        if not header.Is_Map then
            raise READ_EXCEPTION with "Error reading from file";
        end if;
        worlds := header.Map.Get( "worlds" ).Lst;
        vars := header.Map.Get( "vars" ).Map;
        index := header.Map.Get_Int( "world" );
        if not worlds.Valid or else
           not vars.Valid or else
           index not in 1..worlds.Length
        then
            raise READ_EXCEPTION with "Error reading from file";
        end if;

        -- the state of all cached worlds
        for i in 1..worlds.Length loop
            item.path := Read_String_16( Stream( file ) );
            item.world := Load_World( Stream( file ), worlds.Get_String( i ) );
            cache.Insert( worlds.Get_String( i ), item );
            item.world := null;
        end loop;

        Close( file );

        -- restart the game
        this.Restart;

        declare
            procedure Set_Var( name : String; val : Value ) is
            begin
                this.Set_Var( name, val );
            end Set_Var;
        begin
            vars.Iterate( Set_Var'Access );
        end;

        this.worlds := cache;
        cache.Clear;
        world := this.worlds.Element( worlds.Get_String( index ) ).world;
        this.Load_World( worlds.Get_String( index ), world );
    exception
        when others =>
            if Is_Open( file ) then
                for w of cache loop
                    Delete( w.world );
                end loop;
                Close( file );
            end if;
            raise;
    end Load;

    ----------------------------------------------------------------------------

    procedure Load_World( this : not null access Session'Class; filename : String ) is
        use Worlds_Cache;
        filenameExt : constant String := Add_Extension( filename, World_Extension );
        path        : Unbounded_String := To_Unbounded_String( filenameExt );
        wasPlaying  : constant Boolean := this.playing;
        crsr        : Worlds_Cache.Cursor;
        world       : A_World;
    begin
        -- stop gameplay, preventing the passage of game time. gameplay will
        -- resume if loading fails, or when the view unpauses gameplay after the
        -- load.
        this.Pause;

        -- loading of the world has begun
        Queue_Loading_Began;

        -- load the world from disk or retrieve its current state
        -- we only load the world if it's not in the cache yet, or it is in the
        -- cache but the contents should not be preserved ("preserve" property).
        crsr := this.worlds.Find( Get_Filename( filenameExt ) );
        if this.cacheEnabled and then Has_Element( crsr ) then
            -- if the cache already contains a world with the same filename,
            -- then we might be about to reload it if 'preserve' is False. if
            -- the world is to be reloaded, we want to use the same path that
            -- was originally used to load it so that we get the same file.
            --
            -- For example, Tinker may use an absolute path to load a world from
            -- a temp directory and the only way to reload it is to use the
            -- absolute path, instead of just the filename in the world.filename
            -- property that's available to scripts.
            --
            -- Another example would be loading an absolute path to a filename
            -- that matches a world in the media directory. When reloading the
            -- world, we don't want to fall back to locating the file with the
            -- Resources search algorithm because it will find the one in the
            -- media directory. We want to override the filename with the user's
            -- absolute path until the cache is cleared for a new game.
            path := Element( crsr ).path;
        end if;

        if not this.cacheEnabled or else
           not Has_Element( crsr ) or else
           not Element( crsr ).world.Get_Property( "preserve" ).To_Boolean
        then
            -- need to load the world from disk
            pragma Debug( Dbg( "Session: Loading world <" & To_String( path ) &
                               "> from disk", D_GAME, Info ) );

            begin
                world := Worlds.Load_World( To_String( path ) );   -- may raise exception
            exception
                when e : others =>
                    -- loading the world has ended in failure
                    Queue_Loading_Ended( False, Exception_Message( e ) );

                    -- resume gameplay if the session was in play before the load
                    if wasPlaying then
                        this.Resume;
                    end if;
                    raise;
            end;
        else
            -- use the existing world
            world := Element( crsr ).world;
            pragma Debug( Dbg( "Session: Activating cached world <" &
                               To_String( path ) & ">", D_GAME, Info ) );
        end if;

        this.Set_Active_World( To_String( path ), world );

        -- loading the world has successfully completed
        Queue_Loading_Ended;
    end Load_World;

    ----------------------------------------------------------------------------

    procedure Load_World( this     : not null access Session'Class;
                          filename : String;
                          world    : in out A_World ) is
    begin
        -- stop gameplay, preventing the passage of game time. gameplay will
        -- resume when the view unpauses gameplay after the load.
        this.Pause;

        -- loading of the world has begun
        Queue_Loading_Began;

        this.Set_Active_World( filename, world );
        world := null;

        -- loading the world has successfully completed (instantly)
        Queue_Loading_Ended;
    end Load_World;

    ----------------------------------------------------------------------------

    procedure Pause( this : not null access Session'Class ) is
    begin
        if this.playing then
            Dbg( "Session: Pausing gameplay", D_GAME, Info );
            this.game.Get_Entity_System.Stop_Gameplay;
            this.game.Detach( this.Get_World );
            this.game.Detach( this.game.Get_Entity_System );
            this.playing := False;
            Queue_Game_Paused( True );
        end if;
    end Pause;

    ----------------------------------------------------------------------------

    procedure Restart( this : not null access Session'Class ) is
    begin
        pragma Debug( Dbg( "Session: New game", D_GAME, Info ) );

        this.Pause;
        this.age := Time_Span_Zero;

        -- clean up the previous state
        this.game.Get_Entity_System.Set_World( null );
        this.worldController.Set_World( null );
        for w of this.worlds loop
            Delete( w.world );
        end loop;
        this.worlds.Clear;

        for t of this.tasks loop
            pragma Debug( Dbg( "Session: Aborted script " & t.thread.Get_First_Frame &
                               " [id " & Image( t.taskId ) & "]",
                               D_GAME or D_SCRIPT, Info ) );
            Delete( t.thread );
        end loop;
        this.tasks.Clear;

        Queue_Game_Started;

        -- initialize session variables
        this.vars := Create_Map.Map;
        this.Set_Var( "age", Create( Long_Float'(0.0) ) );
        this.Set_Var( "editor", Create( this.isEditor ) );
        this.Update_Tasks_Var;

        -- load initial game state from game.script
        declare
            procedure Set_Initial( name : String; val : Value ) is
            begin
                this.Set_Var( name, val );
            end Set_Initial;

            gameVars : Value;
        begin
            pragma Debug( Dbg( "Compiling 'game.script'", D_GAME or D_SCRIPT, Info ) );
            if not Load_Script( gameVars, "game.script", "scripts", this.game.Get_Script_VM.Get_Runtime ) then
                Dbg( "Script 'game.script' not loaded", D_GAME or D_SCRIPT, Error );
            else
                gameVars.Map.Get( "members" ).Map.Iterate( Set_Initial'Access );
            end if;
        end;
    end Restart;

    ----------------------------------------------------------------------------

    procedure Resume( this : not null access Session'Class ) is
    begin
        if not this.playing and this.Get_World /= null then
            Dbg( "Session: Resuming gameplay", D_GAME, Info );
            Queue_Game_Paused( False );
            this.playing := True;
            this.game.Attach( this.game.Get_Entity_System );
            this.game.Attach( this.Get_World );
            this.game.Get_Entity_System.Start_Gameplay;
        end if;
    end Resume;

    ----------------------------------------------------------------------------

    function Run_Script( this     : not null access Session'Class;
                         funcName : String;
                         func     : Functions.Function_Value;
                         args     : Value_Array;
                         taskId   : Integer := 0 ) return Value is
        thread : A_Thread;
        args2  : Value_Array(args'First..args'Last+1);
        result : Value;
    begin
        thread := Create_Thread;
        thread.Register_Namespace( ANONYMOUS_NAMESPACE, this.game.Get_Script_VM.Get_Globals );

        if func.Is_Member then
            -- insert the game as the implicit 'this' argument
            args2(args2'First) := GAME_ID;
            args2(args2'First+1..args2'Last) := args;

            -- may raise Runtime_Error
            thread.Load( func, args2, funcName );
        else
            -- may raise Runtime_Error
            thread.Load( func, args, funcName );
        end if;

        if taskId = 0 then
            -- run the script now, synchronously
            pragma Debug( Dbg( "Session: Running script " & thread.Get_First_Frame,
                               D_GAME or D_SCRIPT, Info ) );
            this.game.Get_Script_VM.Get_VM.Run( thread, Run_Complete );
            result := thread.Get_Result;
            this.Terminate_Script( thread, taskId );
        else
            -- schedule the script, asynchronously
            pragma Debug( Dbg( "Session: Scheduling script " & thread.Get_First_Frame &
                               " [id " & Image( taskId ) & "]",
                               D_GAME or D_SCRIPT, Info ) );
            this.tasks.Append( Script_Task'(taskId, thread) );
            this.Update_Tasks_Var;
            result := Null_Value;
        end if;

        return result;
    exception
        when e : others =>
            Dbg( "Session: Exception in script " & funcName & "(): ...", D_SCRIPT, Error );
            Dbg( e );
            Delete( thread );
            result := Create( Generic_Exception, Exception_Message( e ) );
            if taskId /= 0 then
                -- failed to start an asynchronous task
                Queue_Script_Done( result, taskId );
            end if;
            return result;
    end Run_Script;

    ----------------------------------------------------------------------------

    procedure Save( this : not null access Session'Class; filename : String ) is
        use Worlds_Cache;
        path   : constant String := Get_Application.Get_User_Directory & filename;
        header : constant Map_Value := Create_Map.Map;
        worlds : constant List_Value := Create_List.Lst;
        file   : Ada.Streams.Stream_IO.File_Type;
        index  : Integer := 1;
        crsr   : Worlds_Cache.Cursor;
    begin
        Create( file, Out_File, path );        -- raises exception on error

        -- file identifier and version
        String'Write( Stream( file ), IDENTIFIER );
        Integer'Output( Stream( file ), FILE_VERSION );

        -- header info:
        --   list of open worlds
        --   current world
        --   session vars
        crsr := this.worlds.First;
        while Has_Element( crsr ) loop
            worlds.Append( Create( Key( crsr ) ) );
            if this.Get_World = Element( crsr ).world then
                header.Set( "world", Create( index ) );
            end if;
            index := index + 1;
            Next( crsr );
        end loop;
        header.Set( "worlds", worlds, consume => True );
        header.Set( "vars", this.vars, consume => True );
        Value_Output( Stream( file ), header );

        -- the state of all cached worlds
        crsr := this.worlds.First;
        while Has_Element( crsr ) loop
            Write_String_16( Stream( file ), Element( crsr ).path );
            A_World'Output( Stream( file ), Element( crsr ).world );
            Next( crsr );
        end loop;

        Close( file );
        pragma Debug( Dbg( "Saved game: " & path, D_RES or D_GAME, Info ) );
    exception
        when WRITE_EXCEPTION =>
            -- file is not allowed to be overwritten
            raise;
        when Name_Error =>
            raise WRITE_EXCEPTION with "Bad filename: " & filename;
        when e : others =>
            raise WRITE_EXCEPTION with "Error writing " & path & ": " &
                                       Exception_Name( e ) & " - " &
                                       Exception_Message( e );
    end Save;

    ----------------------------------------------------------------------------

    procedure Set_Active_World( this     : not null access Session'Class;
                                filename : String;
                                world    : A_World ) is
        use Worlds_Cache;
        crsr      : Worlds_Cache.Cursor;
        prevWorld : A_World;
    begin
        if world = this.Get_World then
            return;
        end if;

        -- deactivate the old world
        if this.Get_World /= null then
            pragma Debug( Dbg( "Session: Deactivating world <" &
                               Image( this.Get_World.Get_Property( "filepath" ) ) & ">",
                               D_GAME, Info ) );
            this.game.Get_Entity_System.Set_World( null );
        end if;

        -- activate the new world (view begins loading too)
        prevWorld := this.Get_World;
        this.worldController.Set_World( world );
        this.game.Get_Entity_System.Set_World( world );
        pragma Debug( world /= null, Dbg( "Session: Activated world <" &
                                          Image( world.Get_Property( "filepath" ) ) & ">",
                                          D_GAME, Info ) );

        -- unload the previous world if necessary
        -- each world determines whether its state should be preserved across
        -- the game session, or if it should be reloaded each time the player
        -- enters it. when the world cache is disabled, it is always unloaded.
        if not this.cacheEnabled or (prevWorld /= null and then not prevWorld.Get_Property( "preserve" ).To_Boolean) then
            -- search by element instead of key because it's more robust. we
            -- need to guarantee there is no pointer to 'prevWorld' in the cache
            -- after deletion.
            crsr := this.worlds.First;
            while Has_Element( crsr ) loop
                if Element( crsr ).world = prevWorld then
                    this.worlds.Delete( crsr );
                end if;
                Next( crsr );
            end loop;
            Delete( prevWorld );
        end if;

        -- update the cache
        -- (even when disabled, the cache always contains the current world)
        if world /= null then
            crsr := this.worlds.Find( Get_Filename( filename ) );
            if not Has_Element( crsr ) then
                -- insert the new world
                this.worlds.Insert( Get_Filename( filename ),
                                    Cached_World'(path  => To_Unbounded_String( filename ),
                                                  world => this.Get_World) );
                pragma Debug( Dbg( "Session: Stored world <" & filename & "> in cache",
                                   D_GAME, Info ) );
            elsif world /= Element( crsr ).world then
                -- replace the old world
                Delete( this.worlds.Reference( crsr ).world );
                this.worlds.Reference( crsr ).world := this.Get_World;
                pragma Debug( Dbg( "Session: Replaced world <" & filename & "> in cache",
                                   D_GAME, Info ) );
            end if;
        end if;
    end Set_Active_World;

    ----------------------------------------------------------------------------

    procedure Set_Var( this : not null access Session'Class;
                       name : String;
                       val  : Value'Class ) is
        current     : constant Value := this.vars.Get( name );
        handlerName : constant String := Capitalize_First( name ) & "Changed";
    begin
        -- announce the session var change but don't clutter the log with "age" changes
        pragma Debug( name /= "age", Dbg( "Session: Var " & name & " := " & val.Image,
                                           D_GAME or D_SCRIPT, Info ) );
        if current /= Value(val) then
            this.vars.Set( name, val );            -- copies 'val'
            Queue_Game_Var_Changed( name, val );   -- copies 'val'

            -- run the var changed handler, when present
            this.Handle_Message( handlerName );
        end if;
    end Set_Var;

    ----------------------------------------------------------------------------

    procedure Terminate_Script( this   : not null access Session'Class;
                                thread : in out A_Thread;
                                taskId : Integer ) is
        pragma Unreferenced( this );
        result : Value;
    begin
        pragma Assert( not thread.Is_Running );
        if taskId /= 0 then
            null;
            pragma Debug( Dbg( "Session: Task " & thread.Get_First_Frame &
                               " [id " & Image( taskId ) & "] ended",
                               D_SCRIPT, Info ) );
        end if;
        result := thread.Get_Result;

        if taskId /= 0 then
            Queue_Script_Done( result, taskId );
        end if;

        if thread.Is_Errored then
            Queue_Console_Text( "Error in script " & thread.Get_First_Frame & ": " & Image( result ) );
            Dbg( "Session: Error in script " & thread.Get_First_Frame & ": " & Image( result ),
                 D_SCRIPT, Error );
            declare
                procedure Examine( str : String ) is
                begin
                    Queue_Console_Text( str );
                    Dbg( str, D_SCRIPT, Error );
                end Examine;
            begin
                thread.Iterate_Trace( Examine'Access );
            end;
        end if;

        Delete( thread );
    end Terminate_Script;

    ----------------------------------------------------------------------------

    overriding
    procedure Tick( this : access Session; time : Tick_Time ) is
        crsr : Task_Lists.Cursor := this.tasks.First;
        next : Task_Lists.Cursor;
        t    : Script_Task;
    begin
        -- increment the age of the session since the last restart
        this.age := this.age + time.elapsed;
        this.Set_Var( "age", Create( Long_Float(To_Duration( this.age )) * 1000.0 ) );

        -- run each background task until it yields
        while Task_Lists.Has_Element( crsr ) loop
            next := Task_Lists.Next( crsr );
            t := Task_Lists.Element( crsr );

            this.game.Get_Script_VM.Get_VM.Run( t.thread, Run_Yield );

            -- clean up and send a Script_Done event
            if not t.thread.Is_Running then
                this.Terminate_Script( t.thread, t.taskId );
                this.tasks.Delete( crsr );
                this.Update_Tasks_Var;
            end if;

            crsr := next;
        end loop;
    end Tick;

    ----------------------------------------------------------------------------

    procedure Update_Tasks_Var( this : not null access Session'Class ) is
        tasks : constant List_Value := Create_List.Lst;
    begin
        for t of this.tasks loop
            -- the "tasks" variable is just an informational list so it's not
            -- formatted for programmatic consumption. it could be formatted
            -- differently.
            --
            -- "[taskId] TaskName"
            tasks.Append( Create( "[" & Image( t.taskId ) & "] " & t.thread.Get_First_Frame ) );
        end loop;
        this.Set_Var( "tasks", tasks );
    end Update_Tasks_Var;

    ----------------------------------------------------------------------------

    not overriding
    procedure Request_Restart( this : access Session ) is
    begin
        this.Restart;
    end Request_Restart;

    ----------------------------------------------------------------------------

    not overriding
    procedure Request_Load( this : access Session; filename : String ) is
    begin
        this.Load( filename );            -- may raise exception
    end Request_Load;

    ----------------------------------------------------------------------------

    procedure Request_Save( this : not null access Session'Class; filename : String ) is
    begin
        this.Save( filename );
    end Request_Save;

    ----------------------------------------------------------------------------

    not overriding
    procedure Request_Pause( this : access Session ) is
    begin
        this.Pause;
    end Request_Pause;

    ----------------------------------------------------------------------------

    not overriding
    procedure Request_Resume( this : access Session ) is
    begin
        this.Resume;
    end Request_Resume;

    ----------------------------------------------------------------------------

    not overriding
    procedure Request_Load_World( this : access Session; filename : String ) is
    begin
        this.Load_World( filename );            -- may raise exception
    end Request_Load_World;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Session ) is
    begin
        Delete( A_Object(this) );
    end Delete;

end Sessions;
