--
-- Copyright (c) 2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Objects;                           use Objects;
with Scribble.Ast;                      use Scribble.Ast;
with Scribble.Ast.Expressions;          use Scribble.Ast.Expressions;
with Scribble.Ast.Expressions.Symbols;  use Scribble.Ast.Expressions.Symbols;
with Scribble.Ast.Script_Processors;    use Scribble.Ast.Script_Processors;
with Scribble.Ast.Scripts;              use Scribble.Ast.Scripts;
with Scribble.Ast.Statements;           use Scribble.Ast.Statements;
with Scribble.Value_Generators;         use Scribble.Value_Generators;
with Scribble.Runtimes;                 use Scribble.Runtimes;
with Values;                            use Values;
with Values.Lists;                      use Values.Lists;
with Values.Maps;                       use Values.Maps;

private package Scribble.Script_Generators is

    -- The Scribble script generator builds a Map value from a script's fully
    -- verified abstract syntax tree.
    type Script_Generator is new Limited_Object and Ast_Script_Processor with private;
    type A_Script_Generator is access all Script_Generator'Class;

    -- Creates a new instance of the Scribble script generator. The Scribble
    -- runtime provides information about the existence of directly exported
    -- Ada functions.
    function Create_Script_Generator( runtime : not null A_Scribble_Runtime ) return A_Script_Generator;
    pragma Postcondition( Create_Script_Generator'Result /= null );

    -- Generates and returns the Function defined by 'node'. If 'enableDebug' is
    -- True, debugging information will be included in the returned Function. A
    -- Parse_Error will be raised if an error is encountered during code
    -- generation (e.g. max program data size is exceeded.)
    function Generate( this        : not null access Script_Generator'Class;
                       node        : not null A_Ast_Script;
                       enableDebug : Boolean := False ) return Map_Value;
    pragma Postcondition( Generate'Result.Valid );

    procedure Delete( this : in out A_Script_Generator );
    pragma Postcondition( this = null );

private

    type Script_Generator is new Limited_Object and Ast_Script_Processor with
        record
            runtime     : A_Scribble_Runtime := null;
            generator   : A_Value_Generator := null;

            enableDebug : Boolean := False;
            members     : Map_Value;
            timers      : List_Value;
        end record;

    procedure Construct( this    : access Script_Generator;
                         runtime : not null A_Scribble_Runtime );

    procedure Delete( this : in out Script_Generator );

    -- script elements
    procedure Process_Required_Member( this : access Script_Generator; node : A_Ast_Required_Member ) is null;
    procedure Process_Script_Member( this : access Script_Generator; node : A_Ast_Script_Member );
    procedure Process_Timer( this : access Script_Generator; node : A_Ast_Timer );

end Scribble.Script_Generators;
