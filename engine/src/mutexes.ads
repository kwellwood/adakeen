--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Task_Identification;           use Ada.Task_Identification;
with Ada.Unchecked_Deallocation;

package Mutexes is

    pragma Preelaborate;

    -- A simple protected object used to implement a recursive mutex.
    protected type Mutex is

        -- Locks/acquires the mutex. The calling thread will block until the
        -- mutex is free, unless it already locked the mutex.
        entry Lock;

        -- Unlocks/releases the mutex. This must be called by the owning thread
        -- once for each call to Lock.
        procedure Unlock;

    private

        entry Waiting;

        owner : Task_Id := Null_Task_Id;
        count : Natural := 0;
    end Mutex;

    type A_Mutex is access all Mutex;

    -- Deletes a Mutex.
    procedure Delete is new Ada.Unchecked_Deallocation( Mutex, A_Mutex );

end Mutexes;
