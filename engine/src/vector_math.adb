--
-- Copyright (c) 2014-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
--with Debugging;                         use Debugging;
with Interfaces;                        use Interfaces;
with Support;                           use Support;

package body Vector_Math is

    -- Fast inverse square root approximation, algorithm from Quake 3
    function Finvsqrt( x : Float ) return Float is
        threeHalves : constant Float := 1.5;
        x2          : constant Float := x * 0.5;
        y           : aliased Float;
        yAsInt      : Unsigned_32;
        for yAsInt'Address use y'Address;
    begin
        y := x;
        yAsInt := 16#5f3759df# - (Shift_Right( yAsInt, 1 ));
        y := y * (threeHalves - (x2 * y * y));             -- first iteration
        return y;
    end Finvsqrt;

    ----------------------------------------------------------------------------

    function Angle_Of( p1, p2 : Vec2 ) return Float is (Arctan( p2.x - p1.x, p2.y - p1.y ));

    ----------------------------------------------------------------------------

    function Image( v : Vec2 ) return String is ("(" & Image( v.x ) & ", " & Image( v.y ) & ")");

    ----------------------------------------------------------------------------

    function Length( v : Vec2 ) return Float is (Sqrt( v.x * v.x + v.y * v.y ));

    ----------------------------------------------------------------------------

    function Normal( v : Vec2 ) return Vec2 is
        unit : constant Vec2 := Normalize( v );
    begin
        return (-unit.y, unit.x);
    end Normal;

    ----------------------------------------------------------------------------

    function Normalize( v : Vec2 ) return Vec2 is
        mag : constant Float := Finvsqrt( v.x * v.x + v.y * v.y );
    begin
        return Vec2'(v.x * mag, v.y * mag);
    end Normalize;

    ----------------------------------------------------------------------------

    procedure Normalize( x, y : in out Float ) is
        n : constant Vec2 := Normalize( Vec2'(x, y) );
    begin
        x := n.x;
        y := n.y;
    end Normalize;

    ----------------------------------------------------------------------------

    function Side_Of_Line( v1, v2 : Vec2; p : Vec2 ) return Float is
    begin
        return (v2.x - v1.x) * (v1.y - p.y) - (v1.y - v2.y) * (p.x - v1.x);
    end Side_Of_Line;

    ----------------------------------------------------------------------------

    function To_Vector( dir : Cardinal_Direction ) return Vec2
    is ((
        case dir is
            when Left  => (-1.0,  0.0),
            when Right => ( 1.0,  0.0),
            when Up    => ( 0.0, -1.0),
            when Down  => ( 0.0,  1.0)
    ));

    ----------------------------------------------------------------------------

    function To_Vector( angle : Float; length : Float := 1.0 ) return Vec2
    is (Vec2'(Sin( angle ) * length, Cos( angle ) * length));

    ----------------------------------------------------------------------------

    function Vec2_Input( stream : access Root_Stream_Type'Class ) return Vec2 is
    begin
        return v : Vec2 do
            v.x := Float'Input( stream );
            v.y := Float'Input( stream );
        end return;
    end Vec2_Input;

    ----------------------------------------------------------------------------

    procedure Vec2_Output( stream : access Root_Stream_Type'Class; v : Vec2 ) is
    begin
        Float'Output( stream, v.x );
        Float'Output( stream, v.y );
    end Vec2_Output;

    --==========================================================================

    function Image( r : Rectangle ) return String
    is ("[" & Image( r.x ) & ", " & Image( r.y ) & ", " & Image( r.width ) & ", " & Image( r.height ) & "]");

    ----------------------------------------------------------------------------

    function Intersect( a, b : Rectangle ) return Boolean
    is (not (a.x > (b.x + b.width) or (a.x + a.width) < b.x or a.y > (b.y + b.height) or (a.y + a.height) < b.y));

    ----------------------------------------------------------------------------

    procedure Intersect( p1, p2 : in out Vec2; area : Rectangle ) is
        ix    : Vec2;
        found : Boolean;
    begin
        Intersect_Full( (p1, p2),
                        ((area.x, -Float'Large), (area.x, Float'Large)),
                        ix, found );
        if found then
            if p1.x < p2.x then
                p1 := ix;
            else
                p2 := ix;
            end if;
        else
            p1.x := Float'Max( p1.x, area.x );
            p2.x := Float'Max( p2.x, area.x );
        end if;
        Intersect_Full( (p1, p2),
                        ((area.x + area.width, -Float'Large), (area.x + area.width, Float'Large)),
                        ix, found );
        if found then
            if p1.x < p2.x then
                p2 := ix;
            else
                p1 := ix;
            end if;
        else
            p1.x := Float'Min( p1.x, area.x + area.width );
            p2.x := Float'Min( p2.x, area.x + area.width );
        end if;
        Intersect_Full( (p1, p2),
                        ((-Float'Large, area.y), (Float'Large, area.y)),
                        ix, found );
        if found then
            if p1.y < p2.y then
                p1 := ix;
            else
                p2 := ix;
            end if;
        else
            p1.y := Float'Max( p1.y, area.y );
            p2.y := Float'Max( p2.y, area.y );
        end if;
        Intersect_Full( (p1, p2),
                        ((-Float'Large, area.y + area.height), (Float'Large, area.y + area.height)),
                        ix, found );
        if found then
            if p1.y < p2.y then
                p2 := ix;
            else
                p1 := ix;
            end if;
        else
            p1.y := Float'Min( p1.y, area.y + area.height );
            p2.y := Float'Min( p2.y, area.y + area.height );
        end if;
    end Intersect;

    ----------------------------------------------------------------------------

    function Contains( r : Rectangle; p : Vec2 ) return Boolean
    is (p.x >= r.x and p.x < r.x + r.width and p.y >= r.y and p.y < r.y + r.height);

    ----------------------------------------------------------------------------

    function Contains( r : Rectangle; x, y : Float ) return Boolean is (Contains( r, (x, y) ));

    --==========================================================================

    procedure Intersect( segA  : Segment;
                         segB  : Segment;
                         ix    : out Vec2;
                         found : out Boolean ) is
        uaNum    : constant Float := Cross( (segB.p2 - segB.p1), (segA.p1 - segB.p1) );
        ubNum    : constant Float := Cross( (segA.p2 - segA.p1), (segA.p1 - segB.p1) );
        denom    : constant Float := Cross( (segA.p2 - segA.p1), (segB.p2 - segB.p1) );
        ua, ub   : Float;
        pa1, pa2,
        pb1, pb2 : Vec2;
    begin
        ix := segA.p1;
        found := False;

        if denom /= 0.0 then
            ua := uaNum / denom;
            ub := ubNum / denom;
            -- true intersection happens when:
            -- (0.0 <= ua and then ua <= 1.0 and then 0.0 <= ub and then ub <= 1.0)
            -- but, by not including 0.0 and 1.0, we discount intersection with
            -- a line's endpoints, so two walls sharing an endpoint don't
            -- intersect. this is important when bisecting walls.
            if 0.0 < ua and ua < 1.0 and 0.0 < ub and ub < 1.0 then
                ix := segA.p1 + ua * (segA.p2 - segA.p1);
                found := True;
            end if;
        else
            -- if uaNum = 0.0 and ubNum = 0.0 then A and B are co-linear
            -- else A and B are parallel
            if uaNum = 0.0 and ubNum = 0.0 then
                -- use bounding boxes to check for overlapping
                if segA.p1.x <= segA.p2.x then
                    pa1.x := segA.p1.x;
                    pa2.x := segA.p2.x;
                else
                    pa1.x := segA.p2.x;
                    pa2.x := segA.p1.x;
                end if;
                if segA.p1.y <= segA.p2.y then
                    pa1.y := segA.p1.y;
                    pa2.y := segA.p2.y;
                else
                    pa1.y := segA.p2.y;
                    pa2.y := segA.p1.y;
                end if;
                if segB.p1.x <= segB.p2.x then
                    pb1.x := segB.p1.x;
                    pb2.x := segB.p2.x;
                else
                    pb1.x := segB.p2.x;
                    pb2.x := segB.p1.x;
                end if;
                if segB.p1.y <= segB.p2.y then
                    pb1.y := segB.p1.y;
                    pb2.y := segB.p2.y;
                else
                    pb1.y := segB.p2.y;
                    pb2.y := segB.p1.y;
                end if;

                -- the use of >= and <= means the bounding boxes must overlap,
                -- not just touch. this means sharing an endpoint doesn't count
                -- as overlapping.
                if not (pa1.x >= pb2.x or pa2.x <= pb1.x or pa1.y >= pb2.y or pa2.y <= pb1.y) then
                    found := True;
                end if;
            end if;
            null;
        end if;
    end Intersect;

    ----------------------------------------------------------------------------

    procedure Intersect_Full( segA  : Segment;
                              segB  : Segment;
                              ix    : out Vec2;
                              found : out Boolean ) is
        uaNum    : constant Float := Cross( (segB.p2 - segB.p1), (segA.p1 - segB.p1) );
        ubNum    : constant Float := Cross( (segA.p2 - segA.p1), (segA.p1 - segB.p1) );
        denom    : constant Float := Cross( (segA.p2 - segA.p1), (segB.p2 - segB.p1) );
        ua, ub   : Float;
        pa1, pa2,
        pb1, pb2 : Vec2;
    begin
        ix := segA.p1;
        found := False;

        if denom /= 0.0 then
            ua := uaNum / denom;
            ub := ubNum / denom;
            -- true intersection happens when:
            -- (0.0 <= ua and then ua <= 1.0 and then 0.0 <= ub and then ub <= 1.0)
            -- but, by not including 0.0 and 1.0, we discount intersection with
            -- a line's endpoints, so two walls sharing an endpoint don't
            -- intersect. this is important when bisecting walls.
            if 0.0 <= ua and ua <= 1.0 and 0.0 <= ub and ub <= 1.0 then
                ix := segA.p1 + ua * (segA.p2 - segA.p1);
                found := True;
            end if;
        else
            -- if uaNum = 0.0 and ubNum = 0.0 then A and B are co-linear
            -- else A and B are parallel
            if uaNum = 0.0 and ubNum = 0.0 then
                -- use bounding boxes to check for overlapping
                if segA.p1.x <= segA.p2.x then
                    pa1.x := segA.p1.x;
                    pa2.x := segA.p2.x;
                else
                    pa1.x := segA.p2.x;
                    pa2.x := segA.p1.x;
                end if;
                if segA.p1.y <= segA.p2.y then
                    pa1.y := segA.p1.y;
                    pa2.y := segA.p2.y;
                else
                    pa1.y := segA.p2.y;
                    pa2.y := segA.p1.y;
                end if;
                if segB.p1.x <= segB.p2.x then
                    pb1.x := segB.p1.x;
                    pb2.x := segB.p2.x;
                else
                    pb1.x := segB.p2.x;
                    pb2.x := segB.p1.x;
                end if;
                if segB.p1.y <= segB.p2.y then
                    pb1.y := segB.p1.y;
                    pb2.y := segB.p2.y;
                else
                    pb1.y := segB.p2.y;
                    pb2.y := segB.p1.y;
                end if;

                -- the use of > and < counts the bounding boxes as overlapping
                -- when they share an edge, so if co-linear segA and segB share
                -- an endpoint it still counts as an intersection.
                if not (pa1.x > pb2.x or pa2.x < pb1.x or pa1.y > pb2.y or pa2.y < pb1.y) then
                    found := True;
                end if;
            end if;
        end if;
    end Intersect_Full;

    ----------------------------------------------------------------------------

    function Left_Of( s : Segment; p : Vec2 ) return Boolean is (0.0 > ((s.p2.x - s.p1.x) * (p.y - s.p1.y) - (s.p2.y - s.p1.y) * (p.x - s.p1.x)));

end Vector_Math;
