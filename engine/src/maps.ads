--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Vectors;
with Ada.Streams;                       use Ada.Streams;
with Ada.Unchecked_Deallocation;
with Center_Arrays;
with Objects;                           use Objects;
with Values;                            use Values;
with Values.Maps;                       use Values.Maps;

package Maps is

    type Layer_Type is
    (
        Layer_Tiles,
        Layer_Scenery
    );
    for Layer_Type use
    (
        Layer_Tiles   => 0,     -- stored in world files; do not change the
        Layer_Scenery => 2      -- representations!
    );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Map_Listener is limited interface;
    type A_Map_Listener is access all Map_Listener'Class;

    procedure On_Layer_Added( this : not null access Map_Listener; layer : Integer ) is null;

    procedure On_Layer_Deleted( this : not null access Map_Listener; layer : Integer ) is null;

    procedure On_Layer_Property_Changed( this  : not null access Map_Listener;
                                         layer : Integer;
                                         name  : String ) is null;

    procedure On_Layers_Swapped( this   : not null access Map_Listener;
                                 layerA : Integer;
                                 layerB : Integer ) is null;

    procedure On_Tile_Changed( this     : not null access Map_Listener;
                               layer    : Integer;
                               col, row : Integer ) is null;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- A Map_Object is a layered 2D grid of tile ids. It's composed of a stack
    -- of Layers of different types and dimensions. To get and set tile ids in
    -- the map, locations are addressed by coordinates (layer, x, y).
    -- x, y coordinates start at ZERO. The smallest layer number is in the
    -- foreground and the largest layer number is in the background.
    type Map_Object is new Object with private;
    type A_Map is access all Map_Object'Class;

    -- Creates a new empty map with the given dimensions. All layers added to
    -- the map will share these dimensions. If the dimensions are too large,
    -- they will be constrained to the maximum.
    function Create_Map( width, height : Positive ) return A_Map;
    pragma Postcondition( Create_Map'Result /= null );

    -- Inserts a new layer of type 'layerType'. If the map already has the
    -- maximum number of layers, nothing will happen.
    --
    -- The layer will be inserted directly in front of layer 'layer'. If 'layer'
    -- is greater than the current background layer, the new layer will become
    -- the new background. If the map doesn't contain any layers yet, the new
    -- layer will be the middleground, layer 0.
    --
    -- 'properties' contains initial layer properties that will override the
    -- defaults for the layer's type; it cannot be null.
    --
    -- The optional 'layerData' map contains map data to initialize the layer,
    -- which will be copied from layer 0. Pass null to leave the layer empty.
    procedure Add_Layer( this       : not null access Map_Object'Class;
                         layer      : Integer;
                         layerType  : Layer_Type;
                         properties : Map_Value := Create_Map.Map;
                         layerData  : A_Map := null );
    pragma Precondition( properties.Valid );

    -- Adds a listener object to receive notifications when the map changes.
    procedure Add_Listener( this     : not null access Map_Object'Class;
                            listener : not null access Map_Listener'Class );

    -- Returns a copy of the tile data 'width' x 'height' in size, starting at
    -- 'col', 'row' in a new Map. The tile positions will be relative to 'col',
    -- 'row' in the original (this Map). The caller receives ownership of the
    -- returned Map.
    function Copy_Area( this          : not null access Map_Object'Class;
                        col, row      : Integer;
                        width, height : Positive ) return A_Map;

    -- Returns a copy of the data from layer 'layer' in a new Map at layer 0, or
    -- null if 'layer' is undefined. The caller receives ownership of the map.
    function Copy_Layer_Data( this  : not null access Map_Object'Class;
                              layer : Integer ) return A_Map;

    -- Deletes map layer 'layer' by index, if it exists. Layer 0 (middleground)
    -- cannot be deleted. Returns True if 'layer' was deleted, otherwise False.
    function Delete_Layer( this  : not null access Map_Object'Class;
                           layer : Integer ) return Boolean;

    -- Returns the index of the background layer.
    function Get_Background_Layer( this : not null access Map_Object'Class ) return Integer with Inline_Always;

    -- Returns the index of the foreground layer.
    function Get_Foreground_Layer( this : not null access Map_Object'Class ) return Integer with Inline_Always;

    -- Returns the map's height in tiles.
    function Get_Height( this : not null access Map_Object'Class ) return Positive with Inline_Always;

    -- Returns the number of layers in the map.
    function Get_Layer_Count( this : not null access Map_Object'Class ) return Natural with Inline_Always;

    -- Returns the properties of layer 'layer'. No copy of the value is made.
    -- See standard layer properties described below. If 'layer' does not exist,
    -- null will be returned.
    function Get_Layer_Properties( this  : not null access Map_Object'Class;
                                   layer : Integer ) return Map_Value;

    -- Returns the value of property 'name' in layer 'layer', or Null if the
    -- layer or property is undefined. No copy of the value is made. See
    -- standard layer properties described below.
    function Get_Layer_Property( this  : not null access Map_Object'Class;
                                 layer : Integer;
                                 name  : String ) return Value;

    -- Returns a set of the property definitions for layer 'layer' (by copy).
    -- Each property may have a set of attributes, called a definition, that
    -- determine how it is to be handled in an editor.
    function Get_Layer_Property_Definitions( this  : not null access Map_Object'Class;
                                             layer : Integer ) return Map_Value;

    -- Returns the immutable type of layer 'layer'.
    function Get_Layer_Type( this : not null access Map_Object'Class; layer : Integer ) return Layer_Type;

    -- Returns the id of a tile in the map. If the location is not on the map
    -- then 0 will be returned. 'col' and 'row' must be in tile coordinates. The
    -- upper-left tile in the map is at (0,0). Layers, background to foreground,
    -- start at 1.
    function Get_Tile_Id( this     : not null access Map_Object'Class;
                          layer    : Integer;
                          col, row : Integer ) return Integer with Inline;

    -- Returns the width of the map in tiles.
    function Get_Width( this : not null access Map_Object'Class ) return Positive with Inline_Always;

    -- Returns True if 'col,row' is within the map's area.
    function In_Map_Area( this : not null access Map_Object'Class; col, row : Integer ) return Boolean;

    -- Resizes this map into a new Map object of 'width,height' tiles, where the
    -- origin is located at 'x,y' in this Map. If the map grows, empty space
    -- will be filled with 0. If the map shrinks, it will be clipped to the new
    -- size. A null will be returned if the given dimensions are too large.
    function Resize( this   : not null access Map_Object'Class;
                     x, y   : Integer;
                     width,
                     height : Positive ) return A_Map;

    -- Sets the property 'name' to 'val' in layer 'layer'. 'val' will be copied.
    -- If the layer does not exist, nothing will happen. See standard layer
    -- properties described below.
    procedure Set_Layer_Property( this  : not null access Map_Object'Class;
                                  layer : Integer;
                                  name  : String;
                                  val   : Value'Class );

    -- Sets the tile id at a location in the map. If the location doesn't exist,
    -- nothing will happen. 'col' and 'row' must be in tile coordinates. The
    -- upper-left tile in the map is at (0,0). Layers, background to foreground,
    -- start at 1.
    procedure Set_Tile( this     : not null access Map_Object'Class;
                        layer    : Integer;
                        col, row : Integer;
                        tileId   : Integer );

    -- Swaps layers 'layer' and 'otherLayer', returning True on success or False
    -- if the same or invalid.
    function Swap_Layers( this       : not null access Map_Object'Class;
                          layer      : Integer;
                          otherLayer : Integer ) return Boolean;

    -- Returns a deep copy of the map.
    function Copy( src : A_Map ) return A_Map;
    pragma Postcondition( Copy'Result /= src or else src = null );

    -- Deletes the map.
    procedure Delete( this : in out A_Map );
    pragma Postcondition( this = null );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Maximum supported map dimensions
    MAX_WIDTH  : constant Integer := 2048;
    MAX_HEIGHT : constant Integer := 2048;
    MAX_LAYERS : constant Integer := 8;

    -- distance in Z depth between layers
    LAYER_Z_SPACING : constant := 16.0;

    -- Standard Layer Properties:

    -- All Layers                                          Type      Default Decription
    -------------                                          ----      ------- ----------
    PROP_TITLE     : constant String := "title";        -- String    ""      The human readable name of the layer
    PROP_DELETABLE : constant String := "deletable";    -- Boolean   False   Layer may be deleted in the editor?
    PROP_TINT      : constant String := "tint";         -- Color     White   Color tint ("#RRGGBB")
    PROP_OPACITY   : constant String := "opacity";      -- Float     1       Layer opacity

    -- Tile Layers
    --------------
    PROP_SOLID     : constant String := "solid";        -- Boolean   False   Tiles shapes used in collision?

    -- Scenery Layers
    -----------------
    PROP_STRETCH   : constant String := "stretch";      -- Boolean   False   Stretch to fill the viewport? (implies speed=0)
    PROP_SPEEDX    : constant String := "speedX";       -- Float     1       Horizontal scroll speed relative to middleground
    PROP_SPEEDY    : constant String := "speedY";       -- Float     1       Vertical scroll speed relative to middleground
    PROP_REPEATX   : constant String := "repeatX";      -- Boolean   True    Repeat the background horizontally?
    PROP_REPEATY   : constant String := "repeatY";      -- Boolean   True    Repeat the background vertically?
    PROP_MARGINT   : constant String := "marginTop";    -- Float     0       Starting distance from top edge of map
    PROP_MARGINB   : constant String := "marginBottom"; -- Float     0       Stopping distance from bottom of map
    PROP_MARGINL   : constant String := "marginLeft";   -- Float     0       Starting distance from left edge of map
    PROP_MARGINR   : constant String := "marginRight";  -- Float     0       Stopping distance from right edge of map
    PROP_OFFSETX   : constant String := "offsetX";      -- Float     0       Horizontal shift of repeating background
    PROP_OFFSETY   : constant String := "offsetY";      -- Float     0       Vertical shift of repeating background

private

    type Tile_Data is new Integer;

    type Tile_Array is array(Integer range <>) of Tile_Data;
    type A_Tile_Array is access all Tile_Array;

    type Map_Layer( layerType : Layer_Type; size : Integer ) is
        record
            data       : Tile_Array(0..size) := (others => 0);
            properties : Map_Value := Create_Map.Map;
        end record;
    type A_Map_Layer is access all Map_Layer;

    procedure Free is new Ada.Unchecked_Deallocation( Map_Layer, A_Map_Layer );

    package Map_Layers is new Center_Arrays( A_Map_Layer, "=" );

    package Listener_Lists is new Ada.Containers.Vectors( Positive, A_Map_Listener, "=" );

    ----------------------------------------------------------------------------

    type Map_Object is new Object with
        record
            width     : Positive := 1;
            height    : Positive := 1;
            layers    : Map_Layers.Center_Array;
            listeners : Listener_Lists.Vector;
        end record;

    procedure Adjust( this : access Map_Object );

    procedure Construct( this : access Map_Object; width, height : Positive );

    -- Creates a new layer of type 'layerType' and initializes applicable
    -- properties to default values.
    function Create_Layer( this      : not null access Map_Object'Class;
                           layerType : Layer_Type ) return A_Map_Layer;

    procedure Delete( this : in out Map_Object );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    function A_Map_Input( stream : access Root_Stream_Type'Class ) return A_Map;
    for A_Map'Input use A_Map_Input;

    procedure A_Map_Output( stream : access Root_Stream_Type'Class; map : A_Map );
    for A_Map'Output use A_Map_Output;

end Maps;
