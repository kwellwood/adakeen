--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Values.Construction;               use Values.Construction;
with Values.Streaming;                  use Values.Streaming;
with Values.Strings;                    use Values.Strings;

package body Maps is

    function Create_Map( width, height : Positive ) return A_Map is
        this : constant A_Map := new Map_Object;
    begin
        this.Construct( width, height );
        return this;
    end Create_Map;

    ----------------------------------------------------------------------------

    procedure Add_Layer( this       : not null access Map_Object'Class;
                         layer      : Integer;
                         layerType  : Layer_Type;
                         properties : Map_Value := Create_Map.Map;
                         layerData  : A_Map := null ) is
        actualLayer : Integer;
    begin
        if this.Get_Layer_Count = MAX_LAYERS then
            return;
        end if;

        actualLayer := this.layers.Insert_Symmetric( layer, this.Create_Layer( layerType ) );
        this.layers.Element( actualLayer ).properties.Merge( properties );

        if layerData /= null then
            pragma Assert( layerData.Get_Layer_Count = 1 );
            this.layers.Element( actualLayer ).data := layerData.layers.Element( 0 ).data;
        end if;

        for listener of this.listeners loop
            listener.On_Layer_Added( actualLayer );
        end loop;
    end Add_Layer;

    ----------------------------------------------------------------------------

    procedure Add_Listener( this     : not null access Map_Object'Class;
                            listener : not null access Map_Listener'Class ) is
    begin
        this.listeners.Append( A_Map_Listener(listener) );
    end Add_Listener;

    ----------------------------------------------------------------------------

    overriding
    procedure Adjust( this : access Map_Object ) is

        function Copy( layer : not null A_Map_Layer ) return A_Map_Layer is
            dup : constant A_Map_Layer := new Map_Layer(layer.layerType, layer.size);
        begin
            dup.properties := Clone( layer.properties ).Map;
            dup.data := layer.data;
            return dup;
        end Copy;

    begin
        for l in this.layers.First..this.layers.Last loop
            this.layers.Replace( l, Copy( this.layers.Element( l ) ) );
        end loop;
        this.listeners.Clear;
    end Adjust;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this   : access Map_Object;
                         width,
                         height : Positive ) is
    begin
        Object(this.all).Construct;
        this.width := Integer'Min( width, MAX_WIDTH );
        this.height := Integer'Min( height, MAX_HEIGHT );
    end Construct;

    ----------------------------------------------------------------------------

    function Copy_Area( this          : not null access Map_Object'Class;
                        col, row      : Integer;
                        width, height : Positive ) return A_Map is
        result : constant A_Map := new Map_Object;
    begin
        result.Construct( width, height );

        -- construct empty layers of the same type
        -- first construct the middle layer and then backgrounds
        -- then construct the foregrounds
        for layer in this.layers.First..this.layers.Last loop
            result.layers.Append( result.Create_Layer( this.layers.Element( layer ).layerType ) );
        end loop;
        result.layers.Set_Center( -this.layers.First );

        for layer in result.layers.First..result.layers.Last loop
            for r in 0..(height-1) loop
                for c in 0..(width-1) loop
                    result.Set_Tile( layer, c, r, this.Get_Tile_Id( layer, col + c, row + r ) );
                end loop;
            end loop;
        end loop;

        return result;
    end Copy_Area;

    ----------------------------------------------------------------------------

    function Copy_Layer_Data( this  : not null access Map_Object'Class;
                              layer : Integer ) return A_Map is
        result : constant A_Map := new Map_Object;
    begin
        result.Construct( this.width, this.height );
        result.layers.Insert_Symmetric( 0, result.Create_Layer( this.layers.Element( layer ).layerType ) );
        result.layers.Element( 0 ).data := this.layers.Element( layer ).data;
        return result;
    end Copy_Layer_Data;

    ----------------------------------------------------------------------------

    function Create_Layer( this      : not null access Map_Object'Class;
                           layerType : Layer_Type ) return A_Map_Layer is
        layer : A_Map_Layer;
    begin
        case layerType is

            when Layer_Tiles =>
                layer := new Map_Layer(layerType, this.width * this.height - 1);
                layer.properties.Set( PROP_SOLID, Create( False ) );

            when Layer_Scenery =>
                layer := new Map_Layer(layerType, 0);
                layer.properties.Set( PROP_STRETCH, Create( False ) );
                layer.properties.Set( PROP_SPEEDX,  Create( 1 ) );
                layer.properties.Set( PROP_SPEEDY,  Create( 1 ) );
                layer.properties.Set( PROP_REPEATX, Create( True ) );
                layer.properties.Set( PROP_REPEATY, Create( True ) );
                layer.properties.Set( PROP_MARGINT, Create( 0 ) );
                layer.properties.Set( PROP_MARGINB, Create( 0 ) );
                layer.properties.Set( PROP_MARGINL, Create( 0 ) );
                layer.properties.Set( PROP_MARGINR, Create( 0 ) );
                layer.properties.Set( PROP_OFFSETX, Create( 0 ) );
                layer.properties.Set( PROP_OFFSETY, Create( 0 ) );

        end case;

        -- common properties
        layer.properties.Set( PROP_TITLE,     Create( "" ) );
        layer.properties.Set( PROP_DELETABLE, Create( True ) );
        layer.properties.Set( PROP_TINT,    Create( "#FFFFFF" ) );
        layer.properties.Set( PROP_OPACITY, Create( 1 ) );

        return layer;
    end Create_Layer;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Map_Object ) is
        layer : A_Map_Layer;
    begin
        for l in this.layers.First..this.layers.Last loop
            layer := this.layers.Element( l );
            Free( layer );
        end loop;
        this.layers.Clear;
        Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Delete_Layer( this  : not null access Map_Object'Class;
                           layer : Integer ) return Boolean is
        mapLayer : A_Map_Layer;
    begin
        if layer = 0 or else
           not (layer in this.layers.First..this.layers.Last) or else
           not this.layers.Element( layer ).properties.Get_Boolean( PROP_DELETABLE )
        then
            return False;
        end if;

        mapLayer := this.layers.Element( layer );
        this.layers.Delete( layer );
        Free( mapLayer );

        for listener of this.listeners loop
            listener.On_Layer_Deleted( layer );
        end loop;

        return True;
    end Delete_Layer;

    ----------------------------------------------------------------------------

    function Get_Background_Layer( this : not null access Map_Object'Class ) return Integer is (this.layers.Last);

    ----------------------------------------------------------------------------

    function Get_Foreground_Layer( this : not null access Map_Object'Class ) return Integer is (this.layers.First);

    ----------------------------------------------------------------------------

    function Get_Height( this : not null access Map_Object'Class ) return Positive is (this.height);

    ----------------------------------------------------------------------------

    function Get_Layer_Count( this : not null access Map_Object'Class ) return Natural is (this.layers.Length);

    ----------------------------------------------------------------------------

    function Get_Layer_Properties( this  : not null access Map_Object'Class;
                                   layer : Integer ) return Map_Value is
    begin
        if layer in this.layers.First..this.layers.Last then
            return this.layers.Element( layer ).properties;
        end if;
        return Null_Value.Map;
    end Get_Layer_Properties;

    ----------------------------------------------------------------------------

    function Get_Layer_Property( this  : not null access Map_Object'Class;
                                 layer : Integer;
                                 name  : String ) return Value is
    begin
        if layer in this.layers.First..this.layers.Last then
            return this.layers.Element( layer ).properties.Get( name );
        end if;
        return Null_Value;
    end Get_Layer_Property;

    ----------------------------------------------------------------------------

    function Get_Layer_Property_Definitions( this  : not null access Map_Object'Class;
                                             layer : Integer ) return Map_Value is
        defs : Map_Value;

        ------------------------------------------------------------------------

        procedure Add_Definition( prop        : String;
                                  displayName : String;
                                  attributes  : Map_Value := Null_Value.Map ) is
            attrs : Map_Value;
        begin
            attrs := Create_Map( (Pair( "displayName",  Create( displayName ) ),
                                  Pair( "displayOrder", Create( defs.Size ) ),
                                  Pair( "visible", Create( True ) ),
                                  Pair( "readonly", Create( False ) )),
                                 consume => True ).Map;
            if attributes.Valid then
                attrs.Merge( attributes );
            end if;
            defs.Set( prop, attrs );
        end Add_Definition;

        ------------------------------------------------------------------------

    begin
        if layer in this.layers.First..this.layers.Last then
            defs := Create_Map.Map;

            Add_Definition( PROP_TITLE, "Title" );

            case this.layers.Element( layer ).layerType is
                when Layer_Tiles =>
                    Add_Definition( PROP_SOLID, "Solid" );

                when Layer_Scenery =>
                    Add_Definition( PROP_STRETCH, "Stretch" );
                    Add_Definition( PROP_SPEEDX,  "Speed X" );
                    Add_Definition( PROP_SPEEDY,  "Speed Y" );
                    Add_Definition( PROP_REPEATX, "Repeat X" );
                    Add_Definition( PROP_REPEATY, "Repeat Y" );
                    Add_Definition( PROP_MARGINT, "Margin Top" );
                    Add_Definition( PROP_MARGINB, "Margin Bottom" );
                    Add_Definition( PROP_MARGINL, "Margin Left" );
                    Add_Definition( PROP_MARGINR, "Margin Right" );
                    Add_Definition( PROP_OFFSETX, "Offset X" );
                    Add_Definition( PROP_OFFSETY, "Offset Y" );

            end case;

            Add_Definition( PROP_TINT,      "Tint" );
            Add_Definition( PROP_OPACITY,   "Opacity", Create_Map( (Pair( "control", Create( "slider" ) ),
                                                                    Pair( "min", Create( 0 ) ),
                                                                    Pair( "max", Create( 1 ) )) ).Map );
            Add_Definition( PROP_DELETABLE, "Deletable", Create_Map( (Pair( "visible", Create( False ) ),
                                                                      Pair( "readonly", Create( True ) )) ).Map );

        end if;
        return defs;
    end Get_Layer_Property_Definitions;

    ----------------------------------------------------------------------------

    function Get_Layer_Type( this  : not null access Map_Object'Class;
                             layer : Integer ) return Layer_Type is
    begin
        if layer in this.layers.First..this.layers.Last then
            return this.layers.Element( layer ).layerType;
        end if;
        return Layer_Tiles;
    end Get_Layer_Type;

    ----------------------------------------------------------------------------

    function Get_Tile_Id( this     : not null access Map_Object'Class;
                          layer    : Integer;
                          col, row : Integer ) return Integer is
    begin
        if layer in this.layers.First..this.layers.Last and this.In_Map_Area( col, row ) then
            case this.layers.Element( layer ).layerType is
                when Layer_Scenery =>
                    return Integer(this.layers.Element( layer ).data(0));
                when others =>
                    return Integer(this.layers.Element( layer ).data(row * this.width + col));
            end case;
        end if;
        return 0;
    end Get_Tile_Id;

    ----------------------------------------------------------------------------

    function Get_Width( this : not null access Map_Object'Class ) return Positive is (this.width);

    ----------------------------------------------------------------------------

    function In_Map_Area( this : not null access Map_Object'Class; col, row : Integer ) return Boolean
        is (col >= 0 and row >= 0 and col < this.width and row < this.height);

    ----------------------------------------------------------------------------

    function Resize( this   : not null access Map_Object'Class;
                     x, y   : Integer;
                     width,
                     height : Positive ) return A_Map is
        newMap     : constant A_Map := Create_Map( width, height );
        posNew,
        posThis    : Integer;
        copyWidth  : Integer;
        copyHeight : Integer;
    begin
        if newMap = null then
            return null;
        end if;

        -- create new layers of the same type
        for layer in this.layers.First..this.layers.Last loop
            newMap.layers.Append( newMap.Create_Layer( this.layers.Element( layer ).layerType ) );
            newMap.layers.Last_Element.properties := Clone( this.layers.Element( layer ).properties ).Map;
        end loop;
        newMap.layers.Set_Center( -this.layers.First );

        copyWidth := Integer'Min( newMap.width, this.width - x );
        copyHeight := Integer'Min( newMap.height, this.height - y );

        -- copy tile data
        for layer in this.layers.First..this.layers.Last loop
            posNew := newMap.layers.Element( layer ).data'First;
            case newMap.layers.Element( layer ).layerType is
                when Layer_Scenery =>
                    posThis := this.layers.Element( layer ).data'First;
                    newMap.layers.Element( layer ).data(posNew) := this.layers.Element( layer ).data(posThis);
                when others =>
                    posThis := this.layers.Element( layer ).data'First + (y * this.width) + x;
                    for row in 0..(copyHeight - 1) loop
                        newMap.layers.Element( layer ).data(posNew..(posNew + copyWidth - 1)) := this.layers.Element( layer ).data(posThis..(posThis + copyWidth - 1));
                        posNew := posNew + newMap.width;
                        posThis := posThis + this.width;
                    end loop;
            end case;
        end loop;

        return newMap;
    end Resize;

    ----------------------------------------------------------------------------

    procedure Set_Layer_Property( this  : not null access Map_Object'Class;
                                  layer : Integer;
                                  name  : String;
                                  val   : Value'Class ) is
    begin
        if layer in this.layers.First..this.layers.Last then
            this.layers.Element( layer ).properties.Set( name, val );

            for listener of this.listeners loop
                listener.On_Layer_Property_Changed( layer, name );
            end loop;
        end if;
    end Set_Layer_Property;

    ----------------------------------------------------------------------------

    procedure Set_Tile( this     : not null access Map_Object'Class;
                        layer    : Integer;
                        col, row : Integer;
                        tileId   : Integer ) is
    begin
        if layer in this.layers.First..this.layers.Last then
            case this.layers.Element( layer ).layerType is
                when Layer_Scenery =>
                    this.layers.Element( layer ).data(0) := Tile_Data(tileId);
                    for listener of this.listeners loop
                        listener.On_Tile_Changed( layer, 0, 0 );
                    end loop;
                when others =>
                    if this.In_Map_Area( col, row ) then
                        this.layers.Element( layer ).data(row * this.width + col) := Tile_Data(tileId);
                        for listener of this.listeners loop
                            listener.On_Tile_Changed( layer, col, row );
                        end loop;
                    end if;
            end case;
        end if;
    end Set_Tile;

    ----------------------------------------------------------------------------

    function Swap_Layers( this       : not null access Map_Object'Class;
                          layer      : Integer;
                          otherLayer : Integer ) return Boolean is
    begin
        if layer /= otherLayer and
           layer in this.layers.First..this.layers.Last and
           otherLayer in this.layers.First..this.layers.Last
        then
            this.layers.Swap( layer, otherLayer );
            for listener of this.listeners loop
                listener.On_Layers_Swapped( layer, otherLayer );
            end loop;
            return True;
        end if;
        return False;
    end Swap_Layers;

    --==========================================================================

    function A_Map_Input( stream : access Root_Stream_Type'Class ) return A_Map is

        ------------------------------------------------------------------------

        procedure Read_RLE( data : in out Tile_Array ) is
            offset   : Integer := data'First;
            needTile : Boolean := True;
            tile     : Tile_Data;
            val      : Integer;
        begin
            loop
                val := Integer'Input( stream );

                -- always accept tiles
                if val >= 0 then
                    tile := Tile_Data(val);
                    data(offset) := tile;
                    offset := offset + 1;
                    needTile := False;
                -- only accept a length if we don't need a tile
                elsif not needTile then
                   -- val is negative of length
                   for i in offset..Integer'Min( offset - val - 2, data'Last ) loop
                       data(offset) := tile;
                       offset := offset + 1;
                    end loop;
                    needTile := True;
                else
                    -- we needed a tile and didn't find one
                    raise Constraint_Error with "Invalid map data encoding";
                end if;
                exit when offset > data'Last;
            end loop;
        end Read_RLE;

        ------------------------------------------------------------------------

        map        : A_Map := null;
        width      : Integer;
        height     : Integer;
        layerType  : Layer_Type;
        layerCount : Integer;
        fgCount    : Integer;
        props      : Map_Value;
    begin
        -- map size
        width := Integer'Input( stream );
        height := Integer'Input( stream );
        layerCount := Integer'Input( stream );
        fgCount := Integer'Input( stream );

        map := Create_Map( width, height );
        if map = null then
            return null;
        end if;

        -- layered tile data
        for l in 1..layerCount loop
            layerType := Layer_Type'Input( stream );
            map.Add_Layer( l - 1, layerType );
            Read_RLE( map.layers.Last_Element.data );
        end loop;

        -- layer properties
        for l in map.layers.First..map.layers.Last loop
            props := Value_Input( stream ).Map;
            if props.Valid then
                -- merging these loaded properties into the default properties
                -- created by Add_Layer() allows news defaults to be added to
                -- stored map files.
                map.layers.Element( l ).properties.Merge( props );
            end if;
        end loop;

        map.layers.Set_Center( fgCount );

        return map;
    exception
        when others =>
            Delete( map );
            raise;
    end A_Map_Input;

    ----------------------------------------------------------------------------

    procedure A_Map_Output( stream : access Root_Stream_Type'Class; map : A_Map ) is

        ------------------------------------------------------------------------

        procedure Write_RLE( data : Tile_Array ) is
            offset : Integer := data'First;
            run    : Integer := 1;
        begin
            loop
                if run = 1 then
                    -- write out the tile id at the start of a run
                    Tile_Data'Output( stream, data(offset) );
                end if;
                if offset < data'Last and then data(offset) = data(offset + 1)
                then
                    -- the run will continue
                    if run < Integer'Last - 1 then
                        -- just increment length
                        run := run + 1;
                    else
                        -- we hit max run length. this will never happen.
                        -- to handle this case anyway, we just write out the
                        -- current run length and start a new run that includes
                        -- the current tile.
                        Integer'Output( stream, -run );
                        Tile_Data'Output( stream, data(offset) );
                        run := 2;
                    end if;
                elsif run > 1 then
                    -- the run has ended, write out the length
                    Integer'Output( stream, -run );
                    run := 1;
                end if;
                offset := offset + 1;
                exit when offset > data'Last;
            end loop;
        end Write_RLE;

        ------------------------------------------------------------------------

    begin
        -- map size
        Integer'Output( stream, map.width );
        Integer'Output( stream, map.height );
        Integer'Output( stream, map.Get_Layer_Count );
        Integer'Output( stream, -map.Get_Foreground_Layer );

        -- layer tile data
        for l in map.Get_Foreground_Layer..map.Get_Background_Layer loop
            Layer_Type'Output( stream, map.layers.Element( l ).layerType );
            Write_RLE( map.layers.Element( l ).data );
        end loop;

        -- layer properties
        for l in map.Get_Foreground_Layer..map.Get_Background_Layer loop
            Value_Output( stream, map.layers.Element( l ).properties );
        end loop;
    end A_Map_Output;

    ----------------------------------------------------------------------------

    function Copy( src : A_Map ) return A_Map is (A_Map(Copy( A_Object(src) )));

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Map ) is
    begin
        Delete( A_Object(this) );
    end Delete;

end Maps;
