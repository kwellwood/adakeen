--
-- Copyright (c) 2012-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Clipping is

    pragma Pure;

    -- This enumeration describes the physical bounding types of tiles, from
    -- fully empty to full solid and all the different kinds of slopes.
    type Clip_Type is (
        Passive,
        OneWay,
        Wall,

        --            2                    1---2
        --          / |                    | /
        --        1---3                    3
        Slope_45_Up_Floor,          Slope_45_Up_Ceiling,

        --        1                        1---2
        --        | \                        \ |
        --        3---2                        3
        Slope_45_Down_Floor,        Slope_45_Down_Ceiling,

        --                                   ..2
        --          ..2                    1---3
        --        1---3                    #####
        Slope_22_Up_Floor_Thin,     Slope_22_Up_Floor_Wide,

        --        #####                    1---2
        --        1---2                    3''
        --        3''
        Slope_22_Up_Ceiling_Wide,   Slope_22_Up_Ceiling_Thin,

        --        1..
        --        3---2                    1..
        --        #####                    3---2
        Slope_22_Down_Floor_Wide,   Slope_22_Down_Floor_Thin,

        --        1---2                    #####
        --          ''3                    1---2
        --                                   ''3
        Slope_22_Down_Ceiling_Thin, Slope_22_Down_Ceiling_Wide
    );

    -- Combines two Clip_Type values and returns the most solid/occlusive one.
    function Dominant( a, b : Clip_Type ) return Clip_Type;

    -- Returns the matching Clip_Type value for a string. The string should be
    -- the exact name of the enumeration value. 'valid' will return False if
    -- 'str' is not a valid Clip_Type value.
    procedure To_Clip_Type( str   : String;
                            clip  : out Clip_Type;
                            valid : out Boolean );

    -- Returns a general string describing the clippping type, like "Floor" or
    -- "Slope" or "Passive". Use Clip_Type'Image() to convert 'tile' to the
    -- exact enum string.
    function To_String( tile : Clip_Type ) return String;

    ----------------------------------------------------------------------------

    -- Returns True if the slope is shaped like the one to the          /|
    -- right, with the solid part underneath the slope.               /##|
    --                                                              /##A#|
    function Is_Slope_A( tile : Clip_Type ) return Boolean with Inline_Always;

    -- Returns True if the slope is shaped like the one to the      |\
    -- right, with the solid part underneath the slope.             |##\
    --                                                              |#B##\
    function Is_Slope_B( tile : Clip_Type ) return Boolean with Inline_Always;

    -- Returns True if the slope is shaped like the one to the      \##C#|
    -- right, with the solid part above the slope.                    \##|
    --                                                                  \|
    function Is_Slope_C( tile : Clip_Type ) return Boolean with Inline_Always;

    -- Returns True if the slope is shaped like the one to the      |#D##/
    -- right, with the solid part above the slope.                  |##/
    --                                                              |/
    function Is_Slope_D( tile : Clip_Type ) return Boolean with Inline_Always;

    -- Returns True if the tile has a solid top edge.
    function Solid_Bottom( tile : Clip_Type ) return Boolean with Inline_Always;
    function Solid_Left( tile : Clip_Type ) return Boolean with Inline_Always;
    function Solid_Right( tile : Clip_Type ) return Boolean with Inline_Always;
    function Solid_Top( tile : Clip_Type ) return Boolean with Inline_Always;

    -- Returns the Y value of the slope at the left edge of the tile, in pixel /
    -- world units relative to the tile (upper left is at 0,0). If the solid
    -- area is beneath the slope (A/B type slopes), the returned value will be
    -- positive, and if the solid area is above the slope (C/D type slopes), the
    -- returned value will be negative.
    function Slope_Left_Y( tile : Clip_Type; tileWidth : Positive ) return Integer with Inline_Always;

    -- Returns the Y value of the slope at the right edge of the tile, in pixel
    -- / world units relative to the tile (Y=0 at tope edge). If the solid
    -- area is beneath the slope (A/B type slopes), the returned value will be
    -- positive, and if the solid area is above the slope (C/D type slopes), the
    -- returned value will be negative.
    function Slope_Right_Y( tile : Clip_Type; tileWidth : Positive ) return Integer with Inline_Always;

    -- Returns the X coordinate of the edge of a slope tile 'tile', in world
    -- coordinates. 'y' (in world coordinates) is the Y location to find the
    -- slope at, the slope tile is located at ('col','row') on the map in tile
    -- coordinates, and 'tileWidth' is the width of a tile in pixels / world
    -- units.
    function Slope_X( tile      : Clip_Type;
                      y         : Float;
                      col, row  : Integer;
                      tileWidth : Positive ) return Float;

    -- Returns the Y coordinate of the edge of a slope tile 'tile', in world
    -- coordinates. 'x' (in world coordinates) is the X location to find the
    -- slope at, the slope tile is located at ('col','row') on the map in tile
    -- coordinates, and 'tileWidth' is the width of a tile in pixels / world
    -- units.
    function Slope_Y( tile      : Clip_Type;
                      x         : Float;
                      col, row  : Integer;
                      tileWidth : Positive ) return Float;

end Clipping;
