--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Values.Tagged_Ids;                 use Values.Tagged_Ids;

package Object_Ids is

    subtype Entity_Id    is Tagged_Id;
    subtype Component_Id is Tagged_Id;

    Entity_Tag    : constant Id_Tag := 0;
    Component_Tag : constant Id_Tag := 1;
    Game_Tag      : constant Id_Tag := 2;
    World_Tag     : constant Id_Tag := 3;

    -- Returns True if 'id' is a Component object.
    function Is_Component( id : Tagged_Id ) return Boolean is (Get_Tag( id ) = Component_Tag);

    -- Returns True if 'id' is an Entity object.
    function Is_Entity( id : Tagged_Id ) return Boolean is (Get_Tag( id ) = Entity_Tag);

    -- Returns True if 'id' is a Game object.
    function Is_Game( id : Tagged_Id ) return Boolean is (Get_Tag( id ) = Game_Tag);

    -- Returns True if 'id' is a World object.
    function Is_World( id : Tagged_Id ) return Boolean is (Get_Tag( id ) = World_Tag);

end Object_Ids;
