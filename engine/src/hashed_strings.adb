--
-- Copyright (c) 2012-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings.Hash;

package body Hashed_Strings is

    overriding
    function "="( l, r : Hashed_String ) return Boolean is (l.ht = r.ht);

    function "="( l : Hashed_String; r : String ) return Boolean is (To_String( l ) = r);

    function "="( l : String; r : Hashed_String ) return Boolean is (l = To_String( r ));

    ----------------------------------------------------------------------------

    function "<"( l, r : Hashed_String ) return Boolean is (l.ht < r.ht);

    ----------------------------------------------------------------------------

    function "&"( l : Hashed_String; r : String ) return String is (To_String( l ) & r);

    ----------------------------------------------------------------------------

    function "&"( l : String; r : Hashed_String ) return String is (l & To_String( r ));

    ----------------------------------------------------------------------------

    function "&"( l, r : Hashed_String ) return String is (To_String( l ) & To_String( r ));

    ----------------------------------------------------------------------------

    function Equivalent( l, r : Hashed_String ) return Boolean is (l.ht = r.ht and then l.str = r.str);

    ----------------------------------------------------------------------------

    function Hash( sh : Hashed_String ) return Hash_Type is (sh.ht);

    ----------------------------------------------------------------------------

    function To_Hashed_String( str : String ) return Hashed_String
    is (Hashed_String'(ht  => Ada.Strings.Hash( str ),
                       str => To_Unbounded_String( str )));

    function To_Hashed_String( str : Unbounded_String ) return Hashed_String
    is (Hashed_String'(ht  => Ada.Strings.Hash( To_String( str ) ),
                       str => str));

    ----------------------------------------------------------------------------

    function To_String( hs : Hashed_String ) return String is (To_String( hs.str ));

end Hashed_Strings;
