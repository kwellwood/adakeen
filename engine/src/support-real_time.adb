--
-- Copyright (c) 2012-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Ada.Long_Float_Text_IO;
with Ada.Long_Long_Float_Text_IO;

package body Support.Real_Time is

    function Elapsed( t : Timer_Type ) return Time_Span is
    begin
        if t.running then
            -- timer is currently running
            return Clock - t.started;
        elsif t.started > Time_First then
            -- timer is paused, return elapsed before pause
            return t.paused - t.started;
        else
            -- timer hasn't been started
            return Time_Span_Zero;
        end if;
    end Elapsed;

    ----------------------------------------------------------------------------

    function Is_Running( t : Timer_Type ) return Boolean is (t.running);

    ----------------------------------------------------------------------------

    procedure Pause( t : in out Timer_Type ) is
    begin
        if t.running then
            t.paused := Clock;
            t.running := False;
        end if;
    end Pause;

    ----------------------------------------------------------------------------

    procedure Restart( t : in out Timer_Type ) is
    begin
        t.running := False;         -- stop it
        t.started := Time_First;    -- clear it
        Start( t );                 -- start it again
    end Restart;

    ----------------------------------------------------------------------------

    procedure Start( t : in out Timer_Type ) is
    begin
        if not t.running then
            if t.started = Time_First then
                -- start timer for the first time
                t.started := Clock;
            else
                -- timer is paused, resume it
                t.started := Clock - (t.paused - t.started);
            end if;
            t.running := True;
        end if;
    end Start;

    --==========================================================================

    procedure End_Frame( t : in out Frame_Timer ) is
    begin
        t.elapsed := t.elapsed + (Clock - t.start);
        t.ticks := t.ticks + 1;
        if t.elapsed >= t.updatePeriod then
            t.fps := Float(t.ticks) / Float(To_Duration( t.elapsed ));
            t.ticks := 0;
            t.elapsed := Time_Span_Zero;
        end if;
    end End_Frame;

    ----------------------------------------------------------------------------

    function Get_Frame_Rate( t : Frame_Timer ) return Float is (t.fps);

    ----------------------------------------------------------------------------

    procedure Next_Frame( t : in out Frame_Timer ) is
        now : constant Time := Clock;
    begin
        if t.start > Time_First then
            t.elapsed := t.elapsed + (now - t.start);
            t.ticks := t.ticks + 1;
            if t.elapsed >= t.updatePeriod then
                t.fps := Float(t.ticks) / Float(To_Duration( t.elapsed ));
                t.ticks := 0;
                t.elapsed := Time_Span_Zero;
            end if;
        end if;
        t.start := now;
    end Next_Frame;

    ----------------------------------------------------------------------------

    procedure Reset( t : in out Frame_Timer ) is
    begin
        t.start := Time_First;
        t.ticks := 0;
        t.elapsed := Time_Span_Zero;
        t.fps := 0.0;
    end Reset;

    ----------------------------------------------------------------------------

    procedure Set_Update_Period( t : in out Frame_Timer; ts : Time_Span ) is
    begin
        t.updatePeriod := ts;
    end Set_Update_Period;

    ----------------------------------------------------------------------------

    procedure Start_Frame( t : in out Frame_Timer ) is
    begin
        t.start := Clock;
    end Start_Frame;

    --==========================================================================

    function Format( ts : Time_Span ) return String is
        secs : constant Integer := To_Seconds( ts );
    begin
        if secs < 1 then
            return Image( To_Milliseconds( ts ) ) & "[ms]";
        elsif secs < 60 then
            return Image( secs ) & "[s]";
        else
            return Image( To_Minutes( ts ) ) & "[min]";
        end if;
    end Format;

    ----------------------------------------------------------------------------

    function To_String( t : Time ) return String is
        secs : Seconds_Count;
        span : Time_Span;
        num  : Long_Long_Float;
        txt  : String(1..18) := (others => ' ');
    begin
        Split( t, secs, span );
        num := Long_Long_Float(secs) + Long_Long_Float(To_Duration( span ));
        Ada.Long_Long_Float_Text_IO.Put( txt, num, Aft => 3, Exp => 0 );
        return Ada.Strings.Fixed.Trim( txt, Both );
    end To_String;

    ----------------------------------------------------------------------------

    function To_String( ts : Time_Span; precision : Natural := 3 ) return String is
        num : Long_Float;
        txt : String(1..18) := (others => ' ');
    begin
        num := Long_Float(To_Duration( ts ));
        Ada.Long_Float_Text_IO.Put( txt, num, Aft => precision, Exp => 0 );
        return Ada.Strings.Fixed.Trim( txt, Both );
    end To_String;

    --==========================================================================

    function Time_Input( stream : access Root_Stream_Type'Class ) return Time is
    begin
        if Boolean'Input( stream ) then
            -- time is in the future
            return Clock + Time_Span'Input( stream );
        else
            -- time is in the past
            return Clock - Time_Span'Input( stream );
        end if;
    end Time_Input;

    ----------------------------------------------------------------------------

    procedure Time_Output( stream : access Root_Stream_Type'Class; t : Time ) is
        now    : constant Time := Clock;
        future : constant Boolean := t > now;
    begin
        Boolean'Output( stream, future );
        if future then
            Time_Span'Output( stream, t - now );
        else
            Time_Span'Output( stream, now - t );
        end if;
    end Time_Output;

end Support.Real_Time;
