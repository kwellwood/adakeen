--
-- Copyright (c) 2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Generic_Array_Sort;
with Ada.Containers.Vectors;
with Resources;                         use Resources;

package Tiles.Terrains is

    type Terrain_Tile is
        record
            layer   : Integer := 0;
            offsetX : Integer := 0;
            offsetY : Integer := 0;
            tileId  : Natural := 0;
        end record;

    function Gt( l, r : Terrain_Tile ) return Boolean is (l.layer > r.layer);

    -- Contains all the tiles in one slice or corner of terrain along a path,
    -- ordered by descending layer number (background to foreground).
    type Terrain_Slice is array (Integer range <>) of Terrain_Tile;
    type A_Terrain_Slice is access all Terrain_Slice;

    procedure Sort_Terrain_Tiles is new Ada.Containers.Generic_Array_Sort( Integer, Terrain_Tile, Terrain_Slice, Gt );

    -- Contains an array of alternate slices to use for a path edge. First index
    -- is 0. Index into the array using the position on the edge modulo'd by the
    -- length of the slices vector.
    package Terrain_Slices is new Ada.Containers.Vectors( Natural, A_Terrain_Slice, "=" );
    use Terrain_Slices;

    type Terrain_Edge is
        record
            angle  : Float := 0.0;
            slices : Terrain_Slices.Vector;
        end record;
    type A_Terrain_Edge is access all Terrain_Edge;

    package Terrain_Edges is new Ada.Containers.Vectors( Positive, A_Terrain_Edge, "=" );
    use Terrain_Edges;

    type Terrain_Corner is
        record
            angle1 : Float := 0.0;
            angle2 : Float := 0.0;
            slices : Terrain_Slices.Vector;
        end record;
    type A_Terrain_Corner is access all Terrain_Corner;

    package Terrain_Corners is new Ada.Containers.Vectors( Positive, A_Terrain_Corner, "=" );
    use Terrain_Corners;

    type Terrain_Definition is
        record
            name    : Unbounded_String;
            icon    : Natural := 0;
            edges   : Terrain_Edges.Vector;
            corners : Terrain_Corners.Vector;
        end record;
    type A_Terrain_Definition is access all Terrain_Definition;

    package Terrain_Vectors is new Ada.Containers.Vectors( Positive, A_Terrain_Definition, "=" );
    use Terrain_Vectors;

    ----------------------------------------------------------------------------

    -- Returns a copy of the Terrain_Definition.
    function Copy( src : A_Terrain_Definition ) return A_Terrain_Definition;
    pragma Postcondition( Copy'Result /= src or else src = null );

    -- Deletes the Terrain_Definition.
    procedure Delete( t : in out A_Terrain_Definition );
    pragma Postcondition( t = null );

    -- Deletes all terrains in the map and clears it.
    procedure Delete_All( terrains : in out Terrain_Vectors.Vector );

    -- Populates map 'terrains' using the terrains stored in the Scribble file
    -- 'res'. The format of the structure is expected to have already been
    -- validated. If an error occurs, no terrains will be returned.
    procedure Load_Terrains( res      : not null A_Resource_File;
                             terrains : in out Terrain_Vectors.Vector );

end Tiles.Terrains;
