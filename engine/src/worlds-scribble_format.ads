--
-- Copyright (c) 2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Worlds.Scribble_Format is

    -- Exports world 'world' and its entities to a text-based Scribble format in
    -- file 'filePath', returning True on success or False on failure.
    --
    -- This external format is useful for examining and tweaking the world
    -- outside of the editor, and for exporting and importing across different
    -- binary file format versions.
    --
    -- The Scribble file format contains fewer details than the native binary
    -- format, specifically in the area of entity state. Only an entity's public
    -- state (attributes, location, name, etc.) is stored, not individual
    -- components or their private state.
    --
    -- For this reason, an exported world file's information is incomplete. It
    -- should be, however, sufficient to recreate a likeness when imported
    -- again.
    function Export( world : not null A_World; filePath : String ) return Boolean;

    -- Imports an exported world file from 'filePath', returning the imported
    -- world on success or null on failure.
    function Import( filePath : String ) return A_World;

end Worlds.Scribble_Format;
