--
-- Copyright (c) 2014-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Assets.Scribble;                   use Assets.Scribble;
with Debugging;                         use Debugging;
with Scribble.Namespaces.Simple;        use Scribble.Namespaces.Simple;
with Values.Maps;                       use Values.Maps;

package body Script_VMs is

    function Create_Script_VM return A_Script_VM is
        this : constant A_Script_VM := new Script_VM;
    begin
        this.Construct;
        return this;
    end Create_Script_VM;

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access Script_VM ) is
    begin
        Limited_Object(this.all).Construct;
        this.runtime := Create_Scribble_Runtime;
        this.compiler := Create_Scribble_Compiler( this.runtime );
        this.vm := Create_Scribble_VM( this.runtime );
        this.globals := A_Scribble_Namespace(Create_Simple_Namespace);
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Script_VM ) is
    begin
        Delete( this.vm );
        Delete( this.compiler );
        Delete( this.runtime );
        Delete( A_Simple_Namespace(this.globals) );
        Limited_Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Get_Compiler( this : not null access Script_VM'Class ) return A_Scribble_Compiler is (this.compiler);

    ----------------------------------------------------------------------------

    function Get_Globals( this : not null access Script_VM'Class ) return A_Scribble_Namespace is (this.globals);

    ----------------------------------------------------------------------------

    function Get_Runtime( this : not null access Script_VM'Class ) return A_Scribble_Runtime is (this.runtime);

    ----------------------------------------------------------------------------

    function Get_VM( this : not null access Script_VM'Class ) return A_Scribble_VM is (this.vm);

    ----------------------------------------------------------------------------

    procedure Load_Globals( this : not null access Script_VM'Class; filename : String ) is
        globals : A_Scribble_Asset := null;
    begin
        pragma Debug( Dbg( "Loading '" & filename & "'", D_GAME or D_SCRIPT, Info ) );
        globals := Load_Value( filename, "scripts" );
        if not globals.Is_Loaded then
            null;    -- already already logged
        elsif not globals.Get_Value.Is_Map then
            Dbg( "Unexpected contents in script '" & filename & "'", D_GAME or D_SCRIPT, Error );
        else
            A_Simple_Namespace(this.globals).Set_Readonly( False );
            A_Simple_Namespace(this.globals).Add_Contents( globals.Get_Value.Map );
            A_Simple_Namespace(this.globals).Set_Readonly( True );
        end if;

        Delete( globals );
    end Load_Globals;

    ----------------------------------------------------------------------------

    procedure Set_Global( this    : not null access Script_VM'Class;
                          name    : String;
                          val     : Value'Class;
                          consume : Boolean := False ) is
    begin
        A_Simple_Namespace(this.globals).Set_Readonly( False );
        A_Simple_Namespace(this.globals).Set_Name( name, val, consume );
        A_Simple_Namespace(this.globals).Set_Readonly( True );
    end Set_Global;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Script_VM ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

    --==========================================================================

    function Check_Types( args : Value_Array; types : Type_Array ) return Boolean is
        pragma Warnings( Off, args );
        pragma Warnings( Off, types );
    begin
#if DEBUG then
        for i in args'Range loop
            if Get_Type( args(i) ) /= types(types'First + (i - args'First)) then
                -- allow 'null' to be passed to an id parameter
                if types(types'First + (i - args'First)) /= V_ID or else Get_Type( args(i) ) /= V_NULL then
                    return False;
                end if;
            end if;
        end loop;
#end if;
        return True;
    end Check_Types;

end Script_VMs;

