--
-- Copyright (c) 2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Scribble.Ast.Scripts;              use Scribble.Ast.Scripts;

package Scribble.Ast.Script_Processors is

    -- The Script_Ast_Processor interface class examines nodes in the abstract
    -- syntax tree for Scribble scripts, following the Visitor design pattern.
    -- Traversal must be implemented by the subclass.
    --
    -- Examination begins by passing an Ast_Processor to the Process() procedure
    -- of an Ast_Node.
    type Ast_Script_Processor is limited interface;
    type A_Ast_Script_Processor is access all Ast_Script_Processor'Class;

    -- script elements
    procedure Process_Required_Member( this : access Ast_Script_Processor; node : A_Ast_Required_Member ) is abstract;
    procedure Process_Script_Member( this : access Ast_Script_Processor; node : A_Ast_Script_Member ) is abstract;
    procedure Process_Timer( this : access Ast_Script_Processor; node : A_Ast_Timer ) is abstract;

end Scribble.Ast.Script_Processors;
