--
-- Copyright (C) 2001 Robert Penner
-- All Rights Reserved.
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- https://opensource.org/licenses/MIT
--
-- Source ported from https://github.com/warrenm/AHEasing
-- Retrieved 2018-05-05
--

package Easing is

    type Easing_Type is
    (
        Linear,
        Quad_In,    Quad_Out,    Quad_InOut,
        Cubic_In,   Cubic_Out,   Cubic_InOut,
        Quart_In,   Quart_Out,   Quart_InOut,
        Quint_In,   Quint_Out,   Quint_InOut,
        Sine_In,    Sine_Out,    Sine_InOut,
        Circ_In,    Circ_Out,    Circ_InOut,
        Expo_In,    Expo_Out,    Expo_InOut,
        Elastic_In, Elastic_Out, Elastic_InOut,
        Back_In,    Back_Out,    Back_InOut,
        Bounce_In,  Bounce_Out,  Bounce_InOut,
        Impulse_In, Impulse_Out, Impulse_InOut
    );

    type A_Easing_Function is access function( p : Float ) return Float;

    -- Returns the easing for type 't', given progress 'p'.
    function Ease( t : Easing_Type; p : Float ) return Float;

    -- Returns the opposite of easing 't'. InOut easings do not have opposites.
    function Opposite( t : Easing_Type ) return Easing_Type;

    -- Returns easing 't' as-is, while 'notInverted' is True. If 'notInverted'
    -- is passed as False, the opposite of 't' will be returned.
    function Use_Easing( t : Easing_Type; notReversed : Boolean := True ) return Easing_Type;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Linear interpolation
    function LinearInterpolation( p : Float ) return Float;

    -- Quadratic easing; p^2
    function QuadraticEaseIn( p : Float ) return Float;
    function QuadraticEaseOut( p : Float ) return Float;
    function QuadraticEaseInOut( p : Float ) return Float;

    -- Cubic easing; p^3
    function CubicEaseIn( p : Float ) return Float;
    function CubicEaseOut( p : Float ) return Float;
    function CubicEaseInOut( p : Float ) return Float;

    -- Quartic easing; p^4
    function QuarticEaseIn( p : Float ) return Float;
    function QuarticEaseOut( p : Float ) return Float;
    function QuarticEaseInOut( p : Float ) return Float;

    -- Quintic easing; p^5
    function QuinticEaseIn( p : Float ) return Float;
    function QuinticEaseOut( p : Float ) return Float;
    function QuinticEaseInOut( p : Float ) return Float;

    -- Sine wave easing; sin(p * PI/2)
    function SineEaseIn( p : Float ) return Float;
    function SineEaseOut( p : Float ) return Float;
    function SineEaseInOut( p : Float ) return Float;

    -- Circular easing; sqrt(1 - p^2)
    function CircularEaseIn( p : Float ) return Float;
    function CircularEaseOut( p : Float ) return Float;
    function CircularEaseInOut( p : Float ) return Float;

    -- Exponential easing, base 2
    function ExponentialEaseIn( p : Float ) return Float;
    function ExponentialEaseOut( p : Float ) return Float;
    function ExponentialEaseInOut( p : Float ) return Float;

    -- Exponentially-damped sine wave easing
    function ElasticEaseIn( p : Float ) return Float;
    function ElasticEaseOut( p : Float ) return Float;
    function ElasticEaseInOut( p : Float ) return Float;

    -- Overshooting cubic easing;
    function BackEaseIn( p : Float ) return Float;
    function BackEaseOut( p : Float ) return Float;
    function BackEaseInOut( p : Float ) return Float;

    -- Exponentially-decaying bounce easing
    function BounceEaseIn( p : Float ) return Float;
    function BounceEaseOut( p : Float ) return Float;
    function BounceEaseInOut( p : Float ) return Float;

    function ImpulseEaseIn( p : Float ) return Float;
    function ImpulseEaseOut( p : Float ) return Float;
    function ImpulseEaseInOut( p : Float ) return Float;

end Easing;
