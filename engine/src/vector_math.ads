--
-- Copyright (c) 2014-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Streams;                       use Ada.Streams;
with Directions;                        use Directions;

package Vector_Math is

    type Vec2 is
        record
            x, y : Float := 0.0;
        end record;

    -- Returns a normalized vector in the direction of 'dir'.
    function To_Vector( dir : Cardinal_Direction ) return Vec2 with Inline;

    -- Returns a vector in the direction of 'angle' with length 'length'.
    function To_Vector( angle : Float; length : Float := 1.0 ) return Vec2;

    function "-"( v : Vec2 ) return Vec2 is (Vec2'(-v.x, -v.y));

    function "-"( l, r : Vec2 ) return Vec2 is (Vec2'(l.x - r.x, l.y - r.y));

    function "+"( l, r : Vec2 ) return Vec2 is (Vec2'(l.x + r.x, l.y + r.y));

    function "*"( l : Vec2; r : Float ) return Vec2 is (Vec2'(l.x * r, l.y * r)) with Inline;

    function "*"( l : Float; r : Vec2 ) return Vec2 is (Vec2'(l * r.x, l * r.y)) with Inline;

    function "/"( l : Vec2; r : Float ) return Vec2 is (Vec2'(l.x / r, l.y / r)) with Inline;

    function "/"( l : Float; r : Vec2 ) return Vec2 is (Vec2'(l / r.x, l / r.y)) with Inline;

    -- Returns the angle of the line segment from point 'p1' to 'p2'.
    function Angle_Of( p1, p2 : Vec2 ) return Float;

    function Cross( p1, p2 : Vec2 ) return Float is (p1.x * p2.y - p1.y * p2.x) with Inline;

    function Dot( l, r : Vec2 ) return Float is (l.x * r.x + l.y * r.y) with Inline;

    -- Returns an image of the vector in the form: "(123.456, 1.23456)"
    function Image( v : Vec2 ) return String with Inline;

    -- Returns the length of vector 'v'.
    function Length( v : Vec2 ) return Float with Inline;

    -- Returns the Manhattan length of vector 'v'.
    function Manhattan_Length( v : Vec2 ) return Float is (abs v.x + abs v.y) with Inline;

    -- Returns the normal of vector 'v', length 1.
    function Normal( v : Vec2 ) return Vec2 with Inline;

    -- Returns a normalized vector in the same direction as 'v', length 1.
    function Normalize( v : Vec2 ) return Vec2 with Inline;

    -- Normalizes vector 'x,y' so that the new vector's length is 1.
    procedure Normalize( x, y : in out Float ) with Inline;

    -- Returns > 0 if 'p' is left of v1 -> v2, 0 if it is on the line, or < 0 if
    -- 'p' is right of v1 -> v2.
    function Side_Of_Line( v1, v2 : Vec2; p : Vec2 ) return Float;

    function Vec2_Input( stream : access Root_Stream_Type'Class ) return Vec2;
    for Vec2'Input use Vec2_Input;

    procedure Vec2_Output( stream : access Root_Stream_Type'Class; v : Vec2 );
    for Vec2'Output use Vec2_Output;

    ----------------------------------------------------------------------------

    type Rectangle is
        record
            x, y   : Float := 0.0;
            width,
            height : Float := 0.0;
        end record;

    -- Returns a string image of rectangle 'r', formatted as: "[X, Y, W, H]"
    function Image( r : Rectangle ) return String;

    -- Returns True if rectangles 'a' and 'b' intersect.
    function Intersect( a, b : Rectangle ) return Boolean;

    -- Modifies line segment 'p1' -> 'p2', shortening it to only the segment
    -- that lies within rectangle 'area'. If the given segment lies fully within
    -- 'area', it will not be modified. If it does not intersect 'area' at all,
    -- the behavior is undefined.
    procedure Intersect( p1, p2 : in out Vec2; area : Rectangle );

    -- Returns True if rectangle 'r' contains point 'p'. It is inclusive of the
    -- top and left edges, but exclusive of the bottom and right edges.
    function Contains( r : Rectangle; p : Vec2 ) return Boolean;

    -- Returns True if rectangle 'r' contains point 'x,y'. It is inclusive of
    -- the top and left edges, but exclusive of the bottom and right edges.
    function Contains( r : Rectangle; x, y : Float ) return Boolean;

    ----------------------------------------------------------------------------

    type Segment is
        record
            p1, p2 : Vec2;
        end record;

    -- Finds and returns the point of intersection between two line segments
    -- 'segA' and 'segB', excluding endpoints. If only an endpoint of one
    -- segment lies on the other, or they simply share an endpoint, then it does
    -- not count as an intersection.
    --
    -- 'found' will be returned True if the segments intersect, excluding
    -- endpoints, or they are co-incident, excluding endpoints. If the segments
    -- are co-incident then segA.p1 will be returned in 'ix'.
    --
    -- The way to differentiate between a single point of intersection and
    -- co-incident intersection (which both return 'found' as True) is to check
    -- the value of 'ix'. If 'ix' = 'segA.p1' then the segments are co-incident
    -- over some distance.
    --
    -- Equation from http://paulbourke.net/geometry/pointlineplane
    -- See the cached copy in the repo at docs/intersection/.
    procedure Intersect( segA  : Segment;
                         segB  : Segment;
                         ix    : out Vec2;
                         found : out Boolean );

    -- Finds and returns the point of intersection between two line segments
    -- segA and segB, including touching endpoints. If the segments are parallel
    -- or coincident then segA.p1 will be returned in 'ix'. 'found' is returned
    -- True if the segments are touching in any way.
    procedure Intersect_Full( segA  : Segment;
                              segB  : Segment;
                              ix    : out Vec2;
                              found : out Boolean );

    -- Returns True if point 'p' is left of (sits counter-clockwise of) the line
    -- segment 's', using a cross-product.
    function Left_Of( s : Segment; p : Vec2 ) return Boolean with Inline;

    function To_Segment( ax, ay, bx, by : Float ) return Segment is (((ax, ay), (bx, by)));

end Vector_Math;
