--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Streams.Buffers;                   use Streams.Buffers;
with Values.Lists;                      use Values.Lists;

package body Tiles.Matrices is

    procedure Delete_All( maps : in out Map_Vectors.Vector ) is
    begin
        for m of maps loop
            Delete( m );
        end loop;
        maps.Clear;
    end Delete_All;

    ----------------------------------------------------------------------------

    procedure Load_Matrices( res      : not null A_Resource_File;
                             matrices : in out Map_Vectors.Vector ) is
        stream : A_Buffer_Stream;
        count  : Integer;
        map    : A_Map;
    begin
        Delete_All( matrices );

        stream := res.Create_Stream;
        count := Integer'Input( stream );
        for m in 1..count loop
            map := A_Map'Input( stream );
            matrices.Append( map );
        end loop;
        Close( stream );
    exception
        when others =>
            Close( stream );
    end Load_Matrices;

end Tiles.Matrices;
