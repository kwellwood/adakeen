--
-- Copyright (c) 2014-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Exceptions;                    use Ada.Exceptions;
with Debugging;                         use Debugging;
with Resources.Plaintext;               use Resources.Plaintext;
with Scribble;                          use Scribble;
with Scribble.Compilers;                use Scribble.Compilers;
with Scribble.IO;                       use Scribble.IO;
with Scribble.Parse_Errors;             use Scribble.Parse_Errors;
with Scribble.Script_Compilers;         use Scribble.Script_Compilers;
with Support;                           use Support;
with Support.Paths;                     use Support.Paths;
with Values.Errors;                     use Values.Errors;

package body Resources.Scribble is

    function Compile_Value( val         : out Value;
                            resource    : not null A_Resource_File;
                            enableDebug : Boolean ) return Boolean is
        source   : Unbounded_String;
        runtime  : A_Scribble_Runtime;
        compiler : A_Scribble_Compiler;
    begin
        source := Read_Plaintext( resource );

        runtime := Create_Scribble_Runtime;
        compiler := Create_Scribble_Compiler( runtime );

        begin
            -- may raise Parse_Error
            val := compiler.Compile( source, resource.Get_Path, enableDebug );
        exception
            when e : Parse_Error =>
                Dbg( "Compilation failure" & ASCII.LF &
                     Format_Parse_Error( Exception_Message( e ) ),
                     D_RES or D_SCRIPT, Error );
                val := Create( Compile_Error, "Compiling '" & resource.Get_Filename & "' failed" );
        end;

        Delete( compiler );
        Delete( runtime );

        return not val.Is_Error;
    end Compile_Value;

    ----------------------------------------------------------------------------

    function Load_Scribble( val      : out Value;
                            resource : not null A_Resource_File ) return Boolean is
        stream  : A_Buffer_Stream;
        content : File_Content;
    begin
        stream := resource.Create_Stream;
        Standard.Scribble.IO.Read( stream, content );    -- does not raise an exception
        Close( stream );

        case content.status is
            when Unrecognized => val := Create( File_Error, "Format of '" & resource.Get_Filename & "' is unrecognized" );
            when Unsupported  => val := Create( File_Error, "Version of '" & resource.Get_Filename & "' is unsupported" );
            when Corrupt      => val := Create( File_Error, "File '" & resource.Get_Filename & "' is damaged" );
            when Loaded       => val := content.val;
            when None         => val := Null_Value;      -- should not happen
        end case;

        return not val.Is_Error;
    end Load_Scribble;

    ----------------------------------------------------------------------------

    function Load_Script( val         : out Value;
                          filePath    : String;
                          group       : String;
                          runtime     : not null A_Scribble_Runtime;
                          enableDebug : Boolean := False ) return Boolean is
        sourcePath : constant String := Locate_Resource( filePath, group );
        compiler   : A_Script_Compiler;
        success    : Boolean := False;
        res        : A_Resource_File := null;
    begin
        if sourcePath'Length > 0 and Case_Eq( Get_Filename( filePath ), Get_Filename( sourcePath ) ) then
            compiler := Create_Script_Compiler( runtime );
            begin
                val := Value(compiler.Compile_Script( sourcePath, enableDebug ));
                success := True;
            exception
                when e : Parse_Error =>
                    Dbg( "Compilation failure" & ASCII.LF &
                         Format_Parse_Error( Exception_Message( e ) ),
                         D_RES or D_SCRIPT, Error );
                    val := Create( Compile_Error, "Compiling '" & sourcePath & "' failed" );
            end;
            Delete( compiler );
        else
            -- 'filePath' not found; try to fall back to a compiled object file
            res := Load_Resource( filePath & ".sco", group );
            if res /= null then
                -- load a Scribble compiled object
                success := Load_Scribble( val, res );
                Delete( res );
            else
                val := Create( Not_Found, "File '" & filePath & "' not found" );
            end if;
        end if;

        return success;
    end Load_Script;

    ----------------------------------------------------------------------------

    function Load_Value( val         : out Value;
                         filePath    : String;
                         group       : String;
                         enableDebug : Boolean := False ) return Boolean is
        res     : A_Resource_File := null;
        success : Boolean := False;
    begin
        res := Load_Resource( filePath, group );
        if res = null then
            -- 'filePath' not found; try to fall back to a compiled object file
            res := Load_Resource( filePath & ".sco", group );
        end if;

        if res /= null then
            if Case_Eq( Get_Extension( res.Get_Filename ), "sco" ) then
                -- load a Scribble compiled object
                success := Load_Scribble( val, res );
            else
                -- compile a Scribble value
                success := Compile_Value( val, res, enableDebug );
            end if;
        else
            val := Create( Not_Found, "File '" & filePath & "' not found" );
            success := False;
        end if;

        Delete( res );
        return success;
    end Load_Value;

end Resources.Scribble;
