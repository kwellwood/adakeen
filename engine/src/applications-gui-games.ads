--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Game_Views;                        use Game_Views;
with Games;                             use Games;
with Semaphores;                        use Semaphores;
with Sessions;                          use Sessions;

package Applications.Gui.Games is

    -- An abstract application class that runs an event-based game.
    type Game_Application is abstract new Gui_Application with private;
    type A_Game_Application is access all Game_Application'Class;

    -- Returns a reference to the application's Game Logic System. The Game
    -- object is not created until the application is running.
    function Get_Game( this : not null access Game_Application'Class ) return A_Game;

    -- Returns the game logic update rate in Hertz. The game simulation thread
    -- uses a fixed time step.
    function Get_Game_Update_Hz( this : not null access Game_Application'Class ) return Positive;

    -- Returns a reference to the application's Game View (GUI, audio, etc). The
    -- Game_View object is not created until the application is initialized.
    function Get_View( this : not null access Game_Application'Class ) return A_Game_View;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Sets the concrete game Session instance used by the application. It will
    -- be consumed. This must be called before Run().
    procedure Set_Session( this    : not null access Game_Application'Class;
                           session : not null A_Session );

    -- Sets the concrete Game_View instance used by the application. It will be
    -- consumed. This must be called before Run().
    procedure Set_View( this : not null access Game_Application'Class;
                        view : not null A_Game_View );

    -- Gracefully terminates the running game application. This will stop the
    -- game logic and game view threadsand will cause the thread that called
    -- Run() to return.
    --
    -- 'errorCode' is the application's return code that will be returned by
    -- Run(). Use NO_ERROR for 'errorCode' during a normal shutdown.
    procedure Stop( this      : not null access Game_Application'Class;
                    errorCode : Integer := NO_ERROR );

private

    type Game_Application is abstract new Gui_Application with
        record
            exitSema     : Binary_Semaphore(False);
            errorCode    : Integer := NO_ERROR;       -- return code for the OS
            gameUpdateHz : Positive := 60;
            game         : A_Game := null;            -- game logic system
            session      : A_Session := null;
            view         : A_Game_View := null;       -- game view system
        end record;

    -- Sets the current working directory to the executable's directory.
    procedure Construct( this    : access Game_Application;
                         company : String;
                         name    : String;
                         userDir : String );

    -- Override to draw the loading screen displayed while the game is
    -- initializing. This is the only place where drawing can be done before the
    -- renderer is created in the game view.
    procedure Draw_Loading_Screen( this : not null access Game_Application'Class );

    -- Initializes the application and game systems. Returns True on success.
    --
    -- This function should NOT be overridden by a subclass. Games should
    -- override the On_Initialize() function instead.
    function Initialize( this : access Game_Application ) return Boolean;

    -- Finalizes the application and releases all resources. Do not call this if
    -- the application didn't successfully initialize.
    --
    -- Subclass implementations must call this last.
    procedure Finalize( this : access Game_Application );

    -- Gracefully terminates the application. This simply calls Stop().
    procedure Kill( this : access Game_Application );

    -- Runs the main event loop until Stop() is called.
    function Main( this : access Game_Application ) return Integer;

    -- Called as part of Initialize(), this procedure can be overridden to do
    -- game-specific initialization like preloading resources, defining game-
    -- specific built in Scribble functions, and registering game-specific
    -- entity components. Games.Scripting will be initialized already, but
    -- component definitions and entity templates will not be compiled yet. The
    -- loading screen will be displayed while On_Initialize executes.
    not overriding
    procedure On_Initialize( this : access Game_Application ) is null;

end Applications.Gui.Games;
