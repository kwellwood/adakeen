--
-- Copyright (c) 2012-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

private with Ada.Containers.Vectors;

package Bin_Packing is

    -- A Rect represents the size and location of a rectangle, packed in a Bin.
    type Rect is
        record
            x, y   : Integer := 0;
            width,
            height : Integer := 0;
        end record;

    -- A Bin represents a rectangular 2D space that can be filled with smaller
    -- 2D rectangles. Each rectangle added to the bin via Insert() receives a
    -- location in the bin where it resides, according to the bin packing
    -- algorithm. This class uses the MAXRECTS bin packing algorithm with the
    -- BSSF (Best Short Side Fit) heuristic.
    type Bin is tagged limited private;
    type A_Bin is access all Bin'Class;

    -- Creates a new bin of the specified size.
    function Create_Bin( width, height : Positive ) return A_Bin;

    -- Inserts a single rectangle into the bin.
    function Insert( this          : not null access Bin'Class;
                     width, height : Natural ) return Rect;

    -- Deletes the Bin.
    procedure Delete( this : in out A_Bin );

private

    package Rect_Vectors is new Ada.Containers.Vectors( Positive, Rect, "=" );

    type Bin is tagged limited
        record
            width,
            height    : Integer;
            freeRects : Rect_Vectors.Vector;
        end record;

    -- Determines the position for a new rectangle using the BSSF (Best Short
    -- Side Fit) heuristic.
    function Find_Position_BSSF( this          : not null access Bin'Class;
                                 width, height : Natural ) return Rect;

    -- Removes redundant rectangles from the free list.
    procedure Prune_Free_List( this : not null access Bin'Class );

    -- Splits 'freeNode' into smaller free rectangles, adding them to the free
    -- list, if 'usedNode' overlaps. Returns True if the node was split or
    -- False if the rectangles don't overlap.
    function Split_Free_Node( this     : not null access Bin'Class;
                              freeNode,
                              usedNode : Rect ) return Boolean;

end Bin_Packing;
