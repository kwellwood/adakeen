--
-- Copyright (c) 2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with GNAT.OS_Lib;                       use GNAT.OS_Lib;
with Mutexes;                           use Mutexes;
with Objects;                           use Objects;
with Values;                            use Values;
with Values.Maps;                       use Values.Maps;

package Assets is

    type Asset_Type is
    (
        Archive_Assets,
        Font_Assets,
        Library_Assets,
        Shader_Assets,
        Scribble_Assets,
        Script_Assets,
        Audio_Sample_Assets
    );

    function To_String( a : Asset_Type ) return String;

    -- Shuts down the asset system, ensuring that all assets have been unloaded.
    procedure Finalize;

    -- Queries the global asset cache, returning a list of all currently known
    -- assets. The result of 'assets' is of the form:
    --
    -- {
    --   id:      [id1, id12, ...]
    --   file:    [file1, file2, ...]
    --   group:   [group1, group2, ...]
    --   path:    [path1, path2, ...]
    --   loaded:  [loaded1, loaded2, ...]
    --   refs:    [count1, count2, ...]
    --   lastUse: [time1, time2, ...]
    -- }
    --
    -- Where:
    --   'id' is the asset's unique identifier (may be any type).
    --   'file' is the resource file path that was requested.
    --   'group' is the resource group of the file.
    --   'path' is the resolved path of the file containing the asset on disk.
    --   'loaded' is a Boolean indicating if the asset is currently loaded.
    --   'refs' is the number of outstanding references to the asset.
    --   'lastUse' is the number of seconds elapsed since the last use.
    procedure Query_Cache( stats : in out Map_Value );
    pragma Postcondition( stats.Valid );

    -- Unloads all assets of type 't'.
    procedure Unload_All( t : Asset_Type );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- A Asset represents a single game asset, loaded from a file. Each Asset
    -- subclass can load its resource file from disk into a readonly data
    -- structure that can be used directly.
    --
    -- Assets support hot-swapping / reloading on the fly, and a centralized
    -- asset management system.
    type Asset is abstract new Limited_Object with private;
    type A_Asset is access all Asset'Class;

    -- Returns the resource file path used to locate the asset's file. According
    -- to the standard resource search rules, the asset may have been loaded
    -- directly from disk or from an archive file.
    function Get_Path( this : not null access Asset'Class ) return String;

    -- Returns the asset's type, indicating which subclass it belongs to. Use
    -- this to avoid type-casting an Asset to the wrong subclass.
    function Get_Type( this : not null access Asset'Class ) return Asset_Type;

    -- Returns True if the asset is currently loaded for use, or False if the
    -- asset could not be loaded from disk.
    function Is_Loaded( this : not null access Asset'Class ) return Boolean;

    -- Creates and returns a new reference to asset 'this'.
    function Clone( this : access Asset'Class ) return access Asset'Class;

    -- Deletes/releases the reference to the asset. The asset may be shared with
    -- other clients and will not actually be unloaded and deleted until it is
    -- unused.
    procedure Delete( this : in out A_Asset );

private

    type Asset is abstract new Limited_Object with
        record
            -- constant for the life of the asset --
            typ          : Asset_Type;                   -- the asset's type
            id           : Value;                        -- unique id in the asset cache
            filePath     : Unbounded_String;             -- resource file path
            group        : Unbounded_String;             -- resource group
            trackChanges : Boolean := True;              -- track changes and reload automatically?
            dataLock     : Mutex;                        -- protects data loading/unloading

            -- protected by .dataLock --
            refreshable  : Boolean := True;              -- can the asset be reloaded?
            loaded       : Boolean := False;             -- asset is currently loaded?
            keepLoaded   : Boolean := False;             -- Prevent unloading due to refs
            diskFile     : Unbounded_String;             -- file to check for a timestamp change
            timestamp    : OS_Time := Invalid_Time;      -- file's last modification time
        end record;

    not overriding
    procedure Construct( this      : access Asset;
                         assetType : Asset_Type;
                         assetId   : Value'Class;
                         filePath  : String;
                         group     : String );
    pragma Precondition( filePath'Length > 0 );

    -- Attempts to load the asset; it must not already be loaded. The .loaded
    -- member will be True on success, or False on failure.
    procedure Load( this : not null access Asset'Class );

    -- Loads the asset into memory, returning True on success or False on
    -- failure. This will only be called when the asset is not already loaded.
    not overriding
    function Load_Data( this : access Asset ) return Boolean is (False);

    -- Checks if the asset's file in disk has changed since it was last loaded,
    -- reloading the newer file as necessary. The asset will be loaded if it is
    -- not currently loaded.
    --
    -- Call Is_Loaded() to determine if the asset has been loaded successfully.
    -- No indication is given if newer data has been reloaded from disk.
    procedure Refresh( this : not null access Asset'Class );

    -- Marks the asset as needing to refresh its data. It may have been unloaded
    -- by the cache, or the file on disk may have modifications that are safe
    -- to reload.
    procedure Set_Refreshable( this : not null access Asset'Class );

    -- Unloads the asset, if it is loaded. The .loaded member will be False on
    -- return. Assets that are flagged with .keepLoaded as True will not be
    -- unloaded unless 'force' is True. This is used to prevent assets from
    -- being unloaded until the asset cache is finalized.
    procedure Unload( this : not null access Asset'Class; force : Boolean := False );

    -- Completely unloads the asset from memory.
    not overriding
    procedure Unload_Data( this : access Asset ) is null;

    ----------------------------------------------------------------------------

    -- Searches for an asset by 'assetId', returning it if found, otherwise null.
    -- The reference count will be incremented.
    function Find_Asset( assetId : Value'Class ) return A_Asset;

    -- Stores 'asset' in the cache. It must not already exist.
    procedure Store_Asset( asset : in out A_Asset );

end Assets;
