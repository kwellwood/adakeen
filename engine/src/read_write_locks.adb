--
-- Copyright (c) 2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Read_Write_Locks is

    procedure Acquire_Read( this : in out Read_Write_Lock ) is
    begin
        this.orderQueue.Lock;
        this.countLock.Lock;
        if this.readCount = 0 then
            this.accessLock.Lock;
        end if;
        this.readCount := this.readCount + 1;    -- there's a new reader in town
        this.orderQueue.Unlock;
        this.countLock.Unlock;
    end Acquire_Read;

    ----------------------------------------------------------------------------

    procedure Acquire_Write( this : in out Read_Write_Lock ) is
    begin
        this.orderQueue.Lock;
        this.accessLock.Lock;
        this.orderQueue.Unlock;
    end Acquire_Write;

    ----------------------------------------------------------------------------

    procedure Release_Read( this : in out Read_Write_Lock ) is
    begin
        this.countLock.Lock;
        this.readCount := this.readCount - 1;
        if this.readCount = 0 then
            this.accessLock.Unlock;
        end if;
        this.countLock.Unlock;
    end Release_Read;

    ----------------------------------------------------------------------------

    procedure Release_Write( this : in out Read_Write_Lock ) is
    begin
        this.accessLock.Unlock;
    end Release_Write;

end Read_Write_Locks;
