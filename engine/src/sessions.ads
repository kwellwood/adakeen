--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Doubly_Linked_Lists;
with Ada.Containers.Indefinite_Ordered_Maps;
with Ada.Real_Time;                     use Ada.Real_Time;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Events;                            use Events;
with Events.Listeners;                  use Events.Listeners;
with Objects;                           use Objects;
with Object_Ids;                        use Object_Ids;
with Processes;                         use Processes;
with Scribble.Namespaces;               use Scribble.Namespaces;
with Scribble.VMs;                      use Scribble.VMs;
with Values;                            use Values;
with Values.Functions;                  use Values.Functions;
with Values.Maps;                       use Values.Maps;
with Values.Tagged_Ids;                 use Values.Tagged_Ids;
with Worlds;                            use Worlds;
with Worlds.Controllers;                use Worlds.Controllers;

limited with Games;

package Sessions is

    -- The Session object manages gameplay. It is responsible for setting up
    -- initial game state, storing and loading gameplay sessions, changing
    -- the active world, running game scripts, and pausing/resuming gameplay.
    --
    -- Only one Session object is instantiated by the Game class over the life
    -- of the application. To begin a new gameplay session, the game's Session
    -- is restarted. A game Session never really ends; gameplay just goes into
    -- an indefinite pause until the application terminates.

    type Session is abstract new Object and
                                 Scribble_Namespace and
                                 Event_Listener and
                                 Process with private;
    type A_Session is access all Session'Class;

    -- Initializes the session after it has been added to Game 'game'. The
    -- dispatching On_Initialize() method will be called to give the sub-class
    -- an opportunity to initialize before use.
    procedure Initialize( this : not null access Session'Class;
                          game : not null access Games.Game'Class );

    -- Stops gameplay and unloads the active world (if any). This is called
    -- before application shutdown.
    procedure Finalize( this : not null access Session'Class );

    -- Returns True if the Session is acting as a game editor. This flag is
    -- used by game logic to suppress certain events and messages that should
    -- not occur inside an editing environment. This flag does not change during
    -- the lifetime of the Session object.
    function Is_Editor( this : not null access Session'Class ) return Boolean;

    -- Returns the Process name of the Session object.
    overriding
    function Get_Process_Name( this : access Session ) return String is ("Session");

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Restarts the game session to the state of a session previously stored to
    -- disk as 'filename' in the config directory.
    --
    -- On success, the game session will be restarted and gameplay will not be
    -- running; it must be resumed. An exception will be raised on failure.
    procedure Load( this : not null access Session'Class; filename : String );

    -- Pauses gameplay, if it is running. No more game time will elapse until
    -- gameplay is resumed. If gameplay is already paused, nothing will change.
    procedure Pause( this : not null access Session'Class );

    -- Restarts the game session from the beginning. The session is responsible
    -- for setting up the initial game state. Upon restart, gameplay will not be
    -- running.
    procedure Restart( this : not null access Session'Class );

    -- Resumes gameplay, if it is paused and a world is active. Entities in the
    -- world will receive the "PlayStarted" message and game time will begin to
    -- elapse on each Tick. If gameplay is already running, nothing will change.
    procedure Resume( this : not null access Session'Class );

    -- Stores the current state of the game session to 'filename' in the config
    -- directory. If gameplay is running, it will not be paused.
    procedure Save( this : not null access Session'Class; filename : String );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Handles a message 'name', with positional list of arguments 'args', sent
    -- to the game. If defined, the associated handler script, name "On<Name>",
    -- will be run synchronously.
    procedure Handle_Message( this : not null access Session'Class;
                              name : String;
                              args : Value_Array );

    -- Handles message 'name' sent to the game, same as above, but without any
    -- arguments (for convenience).
    procedure Handle_Message( this : not null access Session'Class;
                              name : String );

    -- Returns the value of session variable 'name' (without copying). Null will
    -- be returned if 'name' has not been defined yet.
    function Get_Namespace_Name( this : access Session; name : String ) return Value;

    -- Returns a reference to session variable 'name', as a Scribble namespace.
    -- If 'name' has not been defined, it will be defined.
    function Get_Namespace_Ref( this : access Session;
                                name : String ) return Value;
    pragma Postcondition( Get_Namespace_Ref'Result.Is_Ref or else
                          Get_Namespace_Ref'Result.Is_Null );

    -- Returns the value of session variable 'name'.
    function Get_Var( this : not null access Session'Class;
                      name : String ) return Value;

    -- Returns a reference to the session's active world, or null if no world
    -- is active. The world is owned by this Session.
    function Get_World( this : not null access Session'Class ) return A_World;

    -- Runs function 'func' named 'funcName' with arguments 'args' in the context
    -- of the game script object. Returns the result of the function or an error.
    function Run_Script( this     : not null access Session'Class;
                         funcName : String;
                         func     : Functions.Function_Value;
                         args     : Value_Array;
                         taskId   : Integer := 0 ) return Value;

    -- Sets the value (by copy) of session variable 'name' to 'val', as a
    -- Scribble namespace. A Game_Var_Changed event will be queued if the value
    -- of 'name' is changed.
    procedure Set_Var( this : not null access Session'Class;
                       name : String;
                       val  : Value'Class );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    procedure Delete( this : in out A_Session );

    GAME_ID : constant Value := To_Id_Value( Create_Tagged_Id( Game_Tag, 1 ) );

private

    type Script_Task is
        record
            taskId : Integer := 0;
            thread : A_Thread := null;
        end record;

    package Task_Lists is new Ada.Containers.Doubly_Linked_Lists(Script_Task, "=");

    type Cached_World is
        record
            path  : Unbounded_String;
            world : A_World;
        end record;

    package Worlds_Cache is new Ada.Containers.Indefinite_Ordered_Maps( String, Cached_World, "<", "=" );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Session is abstract new Object and
                                 Scribble_Namespace and
                                 Event_Listener and
                                 Process with
        record
            isEditor        : Boolean := False;              -- acting as a game editor?
            game            : access Games.Game'Class := null;
            worldController : A_World_Controller := null;
            playing         : Boolean := False;              -- is gameplay running?
            vars            : Map_Value;                     -- game session vars
            age             : Time_Span := Time_Span_Zero;   -- time since last restart (includes pause time)
            tasks           : Task_Lists.List;               -- list of running tasks

            cacheEnabled    : Boolean := True;               -- world cached enabled?
            worlds          : Worlds_Cache.Map;              -- cache of all loaded and played worlds
        end record;

    procedure Construct( this : access Session );

    -- Destructor for a Session.
    procedure Delete( this : in out Session );

    procedure Handle_Event( this : access Session;
                            evt  : in out A_Event;
                            resp : out Response_Type );
    pragma Precondition( evt /= null );

    -- Finalizes the sub-class in preparation for deletion during application
    -- shutdown. This is called by Finalize() after gameplay has been paused,
    -- the active world has been unloaded, and the world cache has been emptied.
    not overriding
    procedure On_Finalize( this : access Session ) is null;

    -- Initializes the sub-class after the Session is initialized. This is
    -- called by Initialize().
    not overriding
    procedure On_Initialize( this : access Session ) is null;

    -- Called to allow the subclass to handle a message 'name', parameterized
    -- with a position list of arguments 'args'. This is called after the
    -- "On<name>" Scribble handler function, if defined, has run.
    not overriding
    procedure On_Message( this : access Session; name : String; args : Value_Array ) is null;

    -- Loads the world stored in 'filename' and sets it as the active world for
    -- gameplay. The view will be notified of the change via events. Upon
    -- successful completion, gameplay will not be running. The view must resume
    -- gameplay when it is ready.
    --
    -- If 'filename' is in the world cache because it was already loaded and set
    -- to preserve its state over the game session, then the cached world will
    -- be used instead of loading from disk.
    --
    -- If an error occurs, an exception will be raised but the state of gameplay
    -- will not change.
    procedure Load_World( this : not null access Session'Class; filename : String );

    -- Loads the world 'world' (consuming it) and sets it has the active world
    -- for gameplay. The view will be notified of the change via events. Upon
    -- successful completion, gameplay will not be running. The view must resume
    -- gameplay when it is ready.
    --
    -- The world will be stored in the world cache as 'filename', overwriting
    -- any existing world with the same filename. If 'world' has no filename
    -- because it was newly created, it is acceptable to pass an empty string
    -- for 'filename'.
    --
    -- Upon completion, gameplay will not be running. The view must resume
    -- gameplay when it is ready.
    procedure Load_World( this     : not null access Session'Class;
                          filename : String;
                          world    : in out A_World );
    pragma Precondition( world /= null );
    pragma Postcondition( world = null );

    -- Executes one frame of game logic, regardless of the running/paused state
    -- of gameplay.
    --
    -- If this procedure is overridden to provide behavior during gameplay and
    -- between (e.g. cutscenes), then this method must be called first.
    procedure Tick( this : access Session; time : Tick_Time );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- This is called when the Session receives an event to restart the game
    -- session.
    --
    -- Override this procedure to implement behavior that will be executed at
    -- the beginning of the new game, to set up the initial game state, etc.
    procedure Request_Restart( this : access Session );

    -- This is called when the Session receives a LoadGame message event to load
    -- a game session from disk and resume it. The default behavior is to call
    -- Load().
    --
    -- Override this procedure to implement alternate behavior.
    procedure Request_Load( this : access Session; filename : String );

    -- This is called when the Session receives a SaveGame message event to save
    -- the game session to disk. The default behavior is to call Save().
    --
    -- Override this procedure to implement alternate behavior.
    procedure Request_Save( this : not null access Session'Class; filename : String );

    -- This is called when the Session receives an event to pause gameplay. The
    -- default behavior is to call Pause().
    --
    -- Override this procedure to implement alternate behavior.
    procedure Request_Pause( this : access Session );

    -- This is called when the Session receives an event to resume gameplay. The
    -- default behavior is to call Resume().
    --
    -- Override this procedure to implement alternate behavior.
    procedure Request_Resume( this : access Session );

    -- This is called when the Session receives an event to change the current
    -- world. The default behavior is to call Load_World().
    --
    -- Override this procedure to implement alternate behavior.
    procedure Request_Load_World( this : access Session; filename : String );

end Sessions;
