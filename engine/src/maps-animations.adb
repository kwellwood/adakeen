--
-- Copyright (c) 2008-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Events;                            use Events;
with Events.World;                      use Events.World;
with Interfaces;                        use Interfaces;
with Tiles;                             use Tiles;
with Values.Casting;                    use Values.Casting;
with Values.Construction;               use Values.Construction;

package body Maps.Animations is

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Animated_Info_Input( stream : access Root_Stream_Type'Class ) return Animated_Info is
        info : Animated_Info;
    begin
        info.layer := Integer(Integer_8'Input( stream ));
        info.col := Natural(Unsigned_16'Input( stream ));
        info.row := Natural(Unsigned_16'Input( stream ));
        info.tileId := Natural'Input( stream );
        info.nextUpdate := Time_Span'Input( stream );
        return info;
    end Animated_Info_Input;

    ----------------------------------------------------------------------------

    procedure Animated_Info_Output( stream : access Root_Stream_Type'Class;
                                    info   : Animated_Info ) is
    begin
        Integer_8'Output( stream, Integer_8(info.layer) );
        Unsigned_16'Output( stream, Unsigned_16(info.col) );
        Unsigned_16'Output( stream, Unsigned_16(info.row) );
        Natural'Output( stream, info.tileId );
        Time_Span'Output( stream, (if info.nextUpdate >= Time_Span_Zero then info.nextUpdate else Time_Span_Zero) );
    end Animated_Info_Output;

    --==========================================================================

    function Create_Animation_Cache return A_Animation_Cache is
        this : constant A_Animation_Cache := new Animation_Cache;
    begin
        this.Construct;
        return this;
    end Create_Animation_Cache;

    ----------------------------------------------------------------------------

    procedure Initialize( this : not null access Animation_Cache'Class;
                          map  : A_Map;
                          lib  : Library_Ptr ) is
        pos     : Animated_Set.Cursor;
        nextPos : Animated_Set.Cursor;
        anmInfo : Animated_Info;
        tile    : A_Tile;
    begin
        this.map := map;
        this.lib := lib;

        this.map.Add_Listener( this );

        -- add animations for placed tiles that were not previously animated
        -- this occurs when animation is added to the tile library
        for layer in this.map.Get_Foreground_Layer..this.map.Get_Background_Layer loop
            for row in 0..this.map.Get_Height-1 loop
                for col in 0..this.map.Get_Width-1 loop
                    anmInfo := (layer, col, row, others => <>);
                    if not this.animated.Contains( anmInfo ) then
                        tile := this.lib.Get.Get_Tile( this.map.Get_Tile_Id( layer, col, row ) );
                        if tile /= null and then tile.Get_Delay > 0 then
                            anmInfo.tileId := tile.Get_Id;
                            anmInfo.nextUpdate := Milliseconds( tile.Get_Delay );
                            this.animated.Insert( anmInfo );
                        end if;
                    end if;
                end loop;
            end loop;
        end loop;

        -- cache animation frames from tile properties and cull animations for
        -- placed tiles that are not (no longer) animated
        pos := this.animated.First;
        while Animated_Set.Has_Element( pos ) loop
            nextPos := Animated_Set.Next( pos );

            anmInfo := Animated_Set.Element( pos );
            tile := this.lib.Get.Get_Tile( anmInfo.tileId );
            if tile /= null and then tile.Get_Delay > 0 then
                anmInfo.frameDelay := Milliseconds( tile.Get_Delay );
                anmInfo.frames := tile.Get_Attribute( "frames" ).Lst;
                if not anmInfo.frames.Valid or else anmInfo.frames.Length = 0 then
                    -- single-shot animation frame
                    if not anmInfo.frames.Valid then
                        anmInfo.frames := Create_List.Lst;
                    end if;
                    anmInfo.frames.Append( Create( Cast_Int( tile.Get_Attribute( "next" ), anmInfo.tileId ) ) );
                end if;
                this.animated.Include( anmInfo );
            else
                -- this tile isn't animated anymore; the tile library changed
                this.animated.Delete( pos );
            end if;

            pos := nextPos;
        end loop;
    end Initialize;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Layer_Added( this : not null access Animation_Cache; layer : Integer ) is
        anmInfo : Animated_Info;
        pos     : Animated_Set.Cursor;
        next    : Animated_Set.Cursor;
        updated : Animated_Set.Set;
    begin
        -- adjust the layer numbers of animations in other layers
        pos := this.animated.First;
        while Animated_Set.Has_Element( pos ) loop
            next := Animated_Set.Next( pos );
            anmInfo := Animated_Set.Element( pos );

            if anmInfo.layer < layer and layer <= 0 then
                -- layers in the foreground go further into the foreground
                anmInfo.layer := anmInfo.layer - 1;
                updated.Insert( anmInfo );
                this.animated.Delete( pos );

            elsif 0 < layer and layer <= anmInfo.layer then
                -- layers in the background go further into the background
                anmInfo.layer := anmInfo.layer + 1;
                updated.Insert( anmInfo );
                this.animated.Delete( pos );

            end if;
            pos := next;
        end loop;
        this.animated.Union( updated );
    end On_Layer_Added;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Layer_Deleted( this : not null access Animation_Cache; layer : Integer ) is
        anmInfo : Animated_Info;
        pos     : Animated_Set.Cursor;
        next    : Animated_Set.Cursor;
        updated : Animated_Set.Set;
    begin
        -- delete any animations that were in the layer and adjust the layer
        -- numbers of animations in other layers
        pos := this.animated.First;
        while Animated_Set.Has_Element( pos ) loop
            next := Animated_Set.Next( pos );
            anmInfo := Animated_Set.Element( pos );

            if anmInfo.layer = layer then
                -- delete animations in the deleted layer
                this.animated.Delete( pos );

            elsif anmInfo.layer < layer and layer < 0 then
                -- shift foreground layers toward the middleground
                anmInfo.layer := anmInfo.layer + 1;
                updated.Insert( anmInfo );
                this.animated.Delete( pos );

            elsif 0 < layer and layer < anmInfo.layer then
                -- shift background layers toward the middleground
                anmInfo.layer := anmInfo.layer - 1;
                updated.Insert( anmInfo );
                this.animated.Delete( pos );

            end if;
            pos := next;
        end loop;
        this.animated.Union( updated );
    end On_Layer_Deleted;

    ----------------------------------------------------------------------------

    function Get_Animated_Tile( this   : not null access Animation_Cache'Class;
                                layer  : Integer;
                                col    : Integer;
                                row    : Integer;
                                tileId : out Natural ) return Boolean is
        pos : constant Animated_Set.Cursor := this.animated.Find( (layer, col, row, others => <>) );
    begin
        if Animated_Set.Has_Element( pos ) then
            -- if there's an animation at this location, return the animated
            -- tile's id instead of whatever tile happens to be showing.
            tileId := this.animated.Constant_Reference( pos ).tileId;
            return True;
        end if;
        tileId := 0;
        return False;
    end Get_Animated_Tile;

    ----------------------------------------------------------------------------

    procedure Read( this   : not null access Animation_Cache'Class;
                    stream : access Root_Stream_Type'Class ) is
        count : Integer;
    begin
        count := Integer'Input( stream );
        for i in 1..count loop
            this.animated.Include( Animated_Info'Input( stream ) );
        end loop;
    end Read;

    ----------------------------------------------------------------------------

    procedure Resize( this      : not null access Animation_Cache'Class;
                      newMap    : not null A_Map;
                      colOffset : Integer;
                      rowOffset : Integer ) is
        pos,
        next    : Animated_Set.Cursor;
        anmInfo : Animated_Info;
    begin
        this.map := newMap;
        this.map.Add_Listener( this );

        pos := this.animated.First;
        while Animated_Set.Has_Element( pos ) loop
            next := Animated_Set.Next( pos );
            anmInfo := Animated_Set.Element( pos );

            anmInfo.col := anmInfo.col - colOffset;
            anmInfo.row := anmInfo.row - rowOffset;
            if (anmInfo.col in 0..this.map.Get_Width-1) and (anmInfo.row in 0..this.map.Get_Height-1) then
                -- update the new location
                this.animated.Replace_Element( pos, anmInfo );
            else
                -- no longer in the map
                this.animated.Delete( pos );
            end if;

            pos := next;
        end loop;
    end Resize;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Layers_Swapped( this   : not null access Animation_Cache;
                                 layerA : Integer;
                                 layerB : Integer ) is
        anmInfo : Animated_Info;
        pos     : Animated_Set.Cursor;
        next    : Animated_Set.Cursor;
        updated : Animated_Set.Set;
    begin
        -- find animations in the swapped layers and move them to a separate set
        pos := this.animated.First;
        while Animated_Set.Has_Element( pos ) loop
            next := Animated_Set.Next( pos );
            anmInfo := Animated_Set.Element( pos );

            if anmInfo.layer = layerA or anmInfo.layer = layerB then
                anmInfo.layer := (if anmInfo.layer = layerA then layerB else layerA);
                updated.Insert( anmInfo );
                this.animated.Delete( pos );
            end if;
            pos := next;
        end loop;
        this.animated.Union( updated );
    end On_Layers_Swapped;

    ----------------------------------------------------------------------------

    procedure Tick( this : not null access Animation_Cache'Class; time : Tick_Time ) is
        pos     : Animated_Set.Cursor;
        nextPos : Animated_Set.Cursor;
        anmInfo : Animated_Info;
        oldId   : Natural;
        newId   : Natural;
        nextId  : Value;
        tile    : A_Tile;
    begin
        this.counter := this.counter + time.elapsed;

        -- update animated tiles
        pos := this.animated.First;
        while Animated_Set.Has_Element( pos ) loop
            nextPos := Animated_Set.Next( pos );

            anmInfo := Animated_Set.Element( pos );
            oldId := this.map.Get_Tile_Id( anmInfo.layer, anmInfo.col, anmInfo.row );
            newId := oldId;

            if Is_Looping( anmInfo ) then
                -- looped animation:
                -- calculate frame using upTime modulus frame list size
                newId := Get_Frame( anmInfo, 1 + ((this.counter / anmInfo.frameDelay) mod anmInfo.frames.Length) );
            else
                -- single-shot animation frame:
                -- move to next frame if the frame delay expired
                if this.counter >= anmInfo.nextUpdate then
                    newId := anmInfo.frames.Get_Int( 1, default => oldId );
                    tile := this.lib.Get.Get_Tile( newId );
                    if tile /= null and then tile.Get_Delay > 0 then
                        -- update the Animated_Info with the next frame
                        if newId /= oldId then
                            anmInfo.tileId := newId;
                            anmInfo.frameDelay := Milliseconds( tile.Get_Delay );
                            anmInfo.frames := tile.Get_Attribute( "frames" ).Lst;
                            if not anmInfo.frames.Valid or else anmInfo.frames.Length = 0 then
                                if not anmInfo.frames.Valid then
                                    anmInfo.frames := Create_List.Lst;
                                end if;
                                nextId := tile.Get_Attribute( "next" );
                                if nextId.Is_Int then
                                    anmInfo.frames.Append( nextId );
                                else
                                    anmInfo.frames.Append( Create( newId ) );
                                end if;
                            end if;
                        end if;
                        anmInfo.nextUpdate := anmInfo.nextUpdate + anmInfo.frameDelay;
                        this.animated.Include( anmInfo );
                    else
                        -- the next frame isn't part of an animation; remove
                        -- the Animated_Info for this location
                        this.animated.Delete( pos );
                    end if;
                end if;
            end if;

            if newId /= oldId then
                -- begin ignoring changes in the On_Tile_Changed handler because
                -- we are changing the tile in the map to the next frame in the
                -- animation. we don't want to then react as though the user
                -- changed the tile in the editor to a different (and probably
                -- not animated) tile.
                this.ignoreChanges := True;
                this.map.Set_Tile( anmInfo.layer, anmInfo.col, anmInfo.row, newId );
                Queue_Tile_Changed( anmInfo.layer, anmInfo.col, anmInfo.row, newId );
                this.ignoreChanges := False;
            end if;

            pos := nextPos;
        end loop;
    end Tick;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Tile_Changed( this  : not null access Animation_Cache;
                               layer : Integer;
                               col   : Integer;
                               row   : Integer ) is
        id      : Natural;
        tile    : A_Tile;
        anmInfo : Animated_Info;
        pos     : Animated_Set.Cursor;
        next    : Value;
    begin
        if this.ignoreChanges then
            -- don't react to tile change notifications when Tick() is the one
            -- making the changes to the map.
            return;
        end if;

        id := this.map.Get_Tile_Id( layer, col, row );
        tile := this.lib.Get.Get_Tile( id );
        anmInfo := Animated_Info'(layer, col, row, others => <>);
        if tile /= null and then tile.Get_Delay > 0 then
            -- add/update animation
            anmInfo.tileId := id;
            anmInfo.frameDelay := Milliseconds( tile.Get_Delay );
            anmInfo.nextUpdate := this.counter + anmInfo.frameDelay;
            anmInfo.frames := tile.Get_Attribute( "frames" ).Lst;
            if not anmInfo.frames.Valid or else anmInfo.frames.Length = 0 then
                -- single-shot animation frame
                if not anmInfo.frames.Valid then
                    anmInfo.frames := Create_List.Lst;
                end if;
                next := tile.Get_Attribute( "next" );
                if next.Is_Int then
                    anmInfo.frames.Append( next );
                else
                    anmInfo.frames.Append( Create( id ) );
                end if;
            else
                -- looping animation
                pragma Assert( anmInfo.frames.Length > 1 );
            end if;
            this.animated.Include( anmInfo );
        else
            -- remove animation, if any
            pos := this.animated.Find( anmInfo );
            if Animated_Set.Has_Element( pos ) then
                this.animated.Delete( pos );
            end if;
        end if;
    end On_Tile_Changed;

    ----------------------------------------------------------------------------

    procedure Write( this   : not null access Animation_Cache'Class;
                     stream : access Root_Stream_Type'Class ) is
        anmCopy : Animated_Info;
    begin
        Natural'Output( stream, Natural(this.animated.Length) );
        for anmInfo of this.animated loop
            anmCopy := anmInfo;
            anmCopy.nextUpdate := (if anmCopy.nextUpdate > this.counter
                                   then anmCopy.nextUpdate - this.counter
                                   else Time_Span_Zero);
            Animated_Info'Output( stream, anmCopy );
        end loop;
    end Write;

end Maps.Animations;
