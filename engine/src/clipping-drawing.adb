--
-- Copyright (c) 2012-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Transformations;           use Allegro.Transformations;
with Drawing;                           use Drawing;
with Drawing.Primitives;                use Drawing.Primitives;

package body Clipping.Drawing is

    procedure Draw( cType   : Clip_Type;
                    x, y, z : Float;
                    size    : Positive;
                    color1,
                    color2  : Allegro_Color ) is
        left    : constant Float := x;
        right   : constant Float := x + Float(size - 1);
        top     : constant Float := y;
        bottom  : constant Float := y + Float(size - 1);
        backup,
        trans   : Allegro_Transform;
        leftY,
        rightY  : Float;
    begin
        Al_Copy_Transform( backup, Al_Get_Current_Transform.all );
        Al_Identity_Transform( trans );
        Al_Translate_Transform_3d( trans, 0.0, 0.0, z );
        Al_Compose_Transform( trans, backup );
        Al_Use_Transform( trans );

        -- left, bottom, right, top

        case cType is

            when Passive =>
                null;

            when OneWay =>
                Pixel_Line_V_Dotted( left, top, top + Float(size / 2 - 1), color1 );         -- left
                Pixel_Line_H_Dotted( left, right, top + Float(size / 2 - 1), color2 );       -- bottom
                Pixel_Line_V_Dotted( right, top, top + Float(size / 2 - 1), color2 );        -- right
                Pixel_Line( left, top, right, top, color1 );                                 -- top

            when Wall =>
                Pixel_Line( left, top, left, bottom, color1 );                               -- left
                Pixel_Line( left, bottom, right, bottom, color2 );                           -- bottom
                Pixel_Line( right, top, right, bottom, color2 );                             -- right
                Pixel_Line( left, top, right, top, color1 );                                 -- top

            when others =>
                leftY := Float(Slope_Left_Y( cType, size ));
                rightY := Float(Slope_Right_Y( cType, size ));

                if leftY >= 0.0 and then rightY >= 0.0 then
                    -- Floor slopes
                    --
                    -- slope A        slope B
                    --
                    --     2          1
                    --   / |          | \
                    -- 1---3          3---2
                    --
                    if top + leftY < bottom then
                        Pixel_Line_V_Dotted( left, top + leftY, bottom, color1 );            -- left
                    end if;
                    Pixel_Line_H_Dotted( left, right, bottom, color2 );                      -- bottom
                    if top + rightY < bottom then
                        Pixel_Line_V_Dotted( right, top + rightY, bottom, color2 );          -- right
                    end if;
                    Pixel_Line( left, top + leftY,                                           -- top (slope)
                                right, top + rightY,
                                color1 );
                else
                    -- Ceiling slopes
                    --
                    -- slope C        slope D
                    --
                    -- 1---2          1---2
                    --   \ |          | /
                    --     3          3
                    --
                    leftY := abs leftY;
                    rightY := abs rightY;
                    if leftY > 0.0 then
                        Pixel_Line_V_Dotted( left, top, top + leftY, color1 );               -- left
                    end if;
                    Pixel_Line( left, top + leftY,                                           -- bottom (slope)
                                right, top + rightY,
                                color2 );
                    if rightY > 0.0 then
                        Pixel_Line_V_Dotted( right,                                          -- right
                                             top,
                                             top + rightY,
                                             color2 );
                    end if;
                    Pixel_Line_H_Dotted( left, right, top, color1 );                         -- top
                end if;

        end case;

        Al_Use_Transform( backup );
    end Draw;

end Clipping.Drawing;
