--
-- Copyright (c) 2018-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Generic_Properties is

    package body Accessors is

        function Get_Owner( this : Accessor ) return access Base_Object'Class is (Base_Object(this.obj.all)'Access);

        ------------------------------------------------------------------------

        function Read( this : Accessor ) return Value_Type is (this.reader( this.obj ));

        ------------------------------------------------------------------------

        procedure Write( this : Accessor; val : Value_Type ) is
        begin
            this.writer( this.obj, val );
        end Write;

    end Accessors;

    --==========================================================================

    not overriding
    procedure Construct( this     : access Property;
                         pman     : not null A_Process_Manager;
                         accessor : Abstract_Accessor'Class ) is
    begin
        Limited_Object(this.all).Construct;
        this.sigFinished.Init( this );
        this.pman := pman;
        this.accessor.Replace_Element( accessor );
        this.target := accessor.Read;
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Property ) is
    begin
        Limited_Object(this).Delete;
        if this.duration > Time_Span_Zero then
            this.pman.Detach_Async( this'Unrestricted_Access );
        end if;
    end Delete;

    ----------------------------------------------------------------------------

    function Animating( this : not null access Property'Class ) return Boolean is (this.duration > Time_Span_Zero);

    ----------------------------------------------------------------------------

    function Finished( this : not null access Property'Class ) return access Signal'Class is (this.sigFinished'Access);

    ----------------------------------------------------------------------------

    function Get( this : not null access Property'Class ) return Value_Type is (this.accessor.Element.Read);

    ----------------------------------------------------------------------------

    function Get_Owner( this : not null access Property'Class ) return access Base_Object'Class is (this.accessor.Element.Get_Owner);

    ----------------------------------------------------------------------------

    function Get_Target( this : not null access Property'Class ) return Value_Type is (if this.Animating then this.target else this.Get);

    ----------------------------------------------------------------------------

    function Remaining( this : not null access Property'Class ) return Time_Span is (if this.elapsed < this.duration then this.duration - this.elapsed else Time_Span_Zero);

    ----------------------------------------------------------------------------

    procedure Set( this     : not null access Property'Class;
                   val      : Value_Type;
                   duration : Time_Span := Time_Span_Zero;
                   easing   : Easing_Type := Linear ) is
    begin
        this.target := val;
        this.start := this.accessor.Element.Read;
        this.easing := easing;

        if duration > Time_Span_Zero then
            -- apply the target value over time
            if this.duration = Time_Span_Zero then
                -- start the animation
                this.pman.Attach_Async( A_Process(this) );
            end if;

            this.duration := duration;
            this.elapsed := Time_Span_Zero;
        else
            -- apply the target value instantly
            if this.duration > Time_Span_Zero then
                -- stop the previous animation
                this.pman.Detach_Async( A_Process(this) );
            end if;

            this.duration := Time_Span_Zero;
            this.elapsed := Time_Span_Zero;
            this.accessor.Element.Write( this.target );
        end if;
    end Set;

    ----------------------------------------------------------------------------

    procedure Stop( this : not null access Property'Class ) is
    begin
        if this.Animating then
            this.duration := Time_Span_Zero;
            this.elapsed := Time_Span_Zero;
            this.pman.Detach_Async( this );
        end if;
    end Stop;

    ----------------------------------------------------------------------------

    overriding
    procedure Tick( this : access Property; time : Tick_Time ) is
        progress : Float;
    begin
        pragma Assert( this.duration > Time_Span_Zero );
        progress := Float'Min( 1.0, Float(To_Duration( this.elapsed )) / Float(To_Duration( this.duration )) );

        this.accessor.Element.Write( Interpolate( this.start, this.target, Ease( this.easing, progress ) ) );

        -- has the animation finished?
        if this.elapsed > this.duration then
            this.duration := Time_Span_Zero;
            this.elapsed := Time_Span_Zero;
            this.pman.Detach_Async( this );
            this.sigFinished.Emit;
        else
            this.elapsed := this.elapsed + time.elapsed;
        end if;
    end Tick;

end Generic_Properties;
