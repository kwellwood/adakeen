--
-- Copyright (c) 2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Interpolation is

    pragma Pure;

    function Interpolate( first, last : Boolean; progress : Float ) return Boolean
        is (0.5 <= Float(Boolean'Pos(first)) + Float(Boolean'Pos(last) - Boolean'Pos(first)) * progress);

    function Interpolate( first, last : Float; progress : Float ) return Float
        is (first + (last - first) * progress);

end Interpolation;
