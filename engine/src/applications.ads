--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Objects;                           use Objects;

package Applications is

    -- An Application is a global singleton object that implements an
    -- application's initialization, runtime, and shutdown behavior and provides
    -- some meta information about it.
    type Application is abstract new Limited_Object with private;
    type A_Application is access all Application'Class;

    -- Returns the name of the company or individual that produced this
    -- application.
    function Get_Company( this : not null access Application'Class ) return String;

    -- Returns the short name of the application. This can be used to determine
    -- the names of application-specific files, etc. Special characters should
    -- be avoided.
    function Get_Name( this : not null access Application'Class ) return String;

    -- Returns the path to the directory where the user's configuration and
    -- user-created data can safely be written. If it's not an absolute path,
    -- it is relative to the current working directory.
    function Get_User_Directory( this : not null access Application'Class ) return String;

    -- Returns True if the application uses a Display window for graphics.
    -- Command line / shell applications may support loading and saving images
    -- but do not have a Display.
    function Is_Display_Supported( this : access Application ) return Boolean is (False);

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Terminates the application execution as gracefully as possible. Execution
    -- may return from this method, depending on the implementation.
    procedure Kill( this : access Application ) is abstract;

    -- Runs application 'app' as the global application, from start to finish.
    -- It will call Initialize(), Main(), Finalize(), and then return a status
    -- code to be returned to the OS on exit.
    --
    -- If Initialize() raises an exception, Main() and Finalize() will not be
    -- called. If Main() raises an exception then Finalize() will be called to
    -- shutdown the application.
    function Run( this : not null access Application'Class ) return Integer;

    -- Displays a low-level error message to the user. The default
    -- implementation writes the text prefixed with "Error: " to the standard
    -- error stream.
    procedure Show_Error( this : access Application; text : String );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Deletes an Application object.
    procedure Delete( this : in out A_Application );
    pragma Postcondition( this = null );

    ----------------------------------------------------------------------------

    -- Returns a reference to the global application or null if it has not been
    -- created.
    function Get_Application return A_Application;

    -- Forces the application to shut down, displaying the given error message.
    -- If no application has been created, then the system level shutdown
    -- procedure will be called. This is only to be used in the case of a fatal
    -- error. Execution will return from this procedure.
    procedure Terminate_Application( error : String );

    ----------------------------------------------------------------------------

    -- Predefined values returned by Run
    NO_ERROR                   : constant := 0;
    ERROR_UNEXPECTED_EXCEPTION : constant := 1;

    -- This can be raised by initialization code in other packages called by
    -- Initialize() to indicate a failure. This exception should only be caught
    -- by Initialize() implementations, not raised, because Initialize() should
    -- never raise an exception.
    INIT_EXCEPTION : exception;

private

    type Application is abstract new Limited_Object with
        record
            -- the following fields are unprotected because they don't change
            company,                            -- developer's name
            name       : Unbounded_String;      -- app short name
            userDir    : Unbounded_String;      -- path of user's directory

            -- .configFile is the filename (not path) of the configuration file
            -- to load. It will be searched for in the current working directory
            -- first, then in .userDir. If no configuration file is found, it
            -- will be written to .userDir when the application exits. Defaults
            -- to "<.name>.cfg"
            configFile : Unbounded_String;
        end record;

    -- Constructs the application object.
    --
    -- 'name' is a short name for the application and should not contain any
    -- special characters. It is used to name configuration files.
    --
    -- 'userDir' is the path of a directory where the user's configuration and
    -- user-created files can safely be written. If a relative path is given, it
    -- will be assumed relative to the user's home directory. The string must
    -- end with a slash, unless it is empty.
    --
    -- Generally, 'userDir' should be relative and point to an application-
    -- specific or application suite-specific directory within the user's home
    -- directory (e.g.: "C:/Users/Joe User/Documents/MyCompany/MyApp/", where
    -- userDir = "MyCompany/MyApp/") because paths outside the user's home
    -- directory may not be writable when the application is installed. Files
    -- found in the current working directory will override those in 'userDir'.
    --
    -- This procedure should be called first by a subclass constructor.
    procedure Construct( this    : access Application;
                         company : String;
                         name    : String;
                         userDir : String );
    pragma Precondition( company'Length > 0 );
    pragma Precondition( name'Length > 0 );
    pragma Precondition( userDir'Length = 0 or else
                         userDir(userDir'Last) = '/' or else
                         userDir(userDir'Last) = '\' );

    -- Initializes the application, returning True on success. No exceptions
    -- should be raised.
    --
    -- Subclass implementations must call this first.
    function Initialize( this : access Application ) return Boolean;

    -- Finalizes the application, releasing all resources. No exceptions should
    -- be raised.
    --
    -- Subclass implementations must call this last.
    procedure Finalize( this : access Application );

    -- Subclasses implement this as their main procedure, called after successful
    -- application initialization.
    --
    -- Returns 0 for success, or any other integer to indicate an error to the
    -- OS on exit.
    not overriding
    function Main( this : access Application ) return Integer is (0);

end Applications;
