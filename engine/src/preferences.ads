--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Values;                            use Values;

package Preferences is

    pragma Elaborate_Body;

    -- Initializes the preference system. The prefences in file 'filename' will
    -- be loaded and the auto-save task will begin.
    procedure Initialize( filename : String );

    -- Finalizes the preference system. All non-default preferences will be
    -- written to the filename provided at initialization, and the auto-save
    -- task will end.
    procedure Finalize;

    ----------------------------------------------------------------------------

    -- These procedures return a value for the section.name preference. If the
    -- preference has never been set, a default value that has been previously
    -- set by calling Set_Default() will be returned. If Set_Default for the
    -- preference has not been called, then 'secondDefault' will be returned as
    -- a secondary default. If 'section' is not specified, "preference" will be
    -- used and the secondDefault value will be unavailable, to avoid abiguity.

    function Get_Pref( section, name : String;
                       secondDefault : Boolean := False ) return Boolean;

    function Get_Pref( section, name : String;
                       secondDefault : Float := 0.0 ) return Float;

    function Get_Pref( section, name : String;
                       secondDefault : Integer := 0 ) return Integer;

    function Get_Pref( section, name : String;
                       secondDefault : String := "" ) return String;

    function Get_Pref( section, name : String;
                       secondDefault : String := "" ) return Unbounded_String
    is (To_Unbounded_String( String'(Get_Pref( section, name, secondDefault )) ));

    function Get_Pref( section, name : String ) return Value;

    function Get_Pref( name : String ) return Boolean is (Get_Pref( "preference", name ));

    function Get_Pref( name : String ) return Float is (Get_Pref( "preference", name ));

    function Get_Pref( name : String ) return Integer is (Get_Pref( "preference", name ));

    function Get_Pref( name : String ) return String is (Get_Pref( "preference", name ));

    function Get_Pref( name : String ) return Unbounded_String is (Get_Pref( "preference", name ));

    ----------------------------------------------------------------------------

    -- These procedures set the value of the section.name preference. All set
    -- values will be saved to the preference file. If 'section' is not
    -- specified, "preference" will be used.

    procedure Set_Pref( name : String; value : Boolean );

    procedure Set_Pref( name : String; value : Float );

    procedure Set_Pref( name : String; value : Integer );

    procedure Set_Pref( name : String; value : String );

    procedure Set_Pref( section, name : String; value : Boolean );

    procedure Set_Pref( section, name : String; value : Float );

    procedure Set_Pref( section, name : String; value : Integer );

    procedure Set_Pref( section, name : String; value : String );

    procedure Set_Pref( section, name : String; val : Value'Class );

    ----------------------------------------------------------------------------

    -- These procedures set a default value for the section.name preference.
    -- Default values are not written to the preference file. If 'section' is
    -- not specified, "preference" will be used.

    procedure Set_Default( name : String; value : Boolean );

    procedure Set_Default( name : String; value : Float );

    procedure Set_Default( name : String; value : Integer );

    procedure Set_Default( name : String; value : String );

    procedure Set_Default( section, name : String; value : Boolean );

    procedure Set_Default( section, name : String; value : Float );

    procedure Set_Default( section, name : String; value : Integer );

    procedure Set_Default( section, name : String; value : String );

end Preferences;
