--
-- Copyright (c) 2012-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers;                    use Ada.Containers;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;

package Hashed_Strings is

    pragma Preelaborate;

    -- A Hashed_String is built from a fixed string using a case-sensitive hash
    -- function so that it can quickly be compared to other hashed strings. Note
    -- that the comparison isn't 100% accurate because hash collisions for
    -- differing strings are possible. Use a Hashed_String if you have a
    -- constant, case-sensitive string that needs to be compared with other
    -- constant strings frequently.
    type Hashed_String is private;

    -- Returns True if the Hashed_Strings match based on hash comparison. This
    -- isn't 100% accurate. See the comments for the Hashed_String type.
    function "="( l, r : Hashed_String ) return Boolean with Inline;
    function "="( l : Hashed_String; r : String ) return Boolean with Inline;
    function "="( l : String; r : Hashed_String ) return Boolean with Inline;

    -- Like the '=' operator, this function is based on hash comparison.
    function "<"( l, r : Hashed_String ) return Boolean with Inline;

    -- Returns a string from the concatenation of a Hashed_String with a fixed string.
    function "&"( l : Hashed_String; r : String ) return String with Inline;

    -- Returns a string from the concatenation of a fixed string with a Hashed_String.
    function "&"( l : String; r : Hashed_String ) return String with Inline;

    -- Returns a string from the concatenation of two Hashed_Strings.
    function "&"( l, r : Hashed_String ) return String with Inline;

    -- Returns True if the Hashed_Strings match exactly. This is slower than the
    -- '=' operator because the string contents will be compared if the hash
    -- values match.
    function Equivalent( l, r : Hashed_String ) return Boolean with Inline;

    -- Returns the hashed value of the internal string.
    function Hash( sh : Hashed_String ) return Hash_Type with Inline;

    -- Makes a Hashed_String from a fixed string.
    function To_Hashed_String( str : String ) return Hashed_String;
    function To_Hashed_String( str : Unbounded_String ) return Hashed_String;

    -- Returns the Hashed_String's internal string.
    function To_String( hs : Hashed_String ) return String with Inline;

private

    type Hashed_String is
        record
            ht  : Hash_Type;
            str : Unbounded_String;
        end record;

end Hashed_Strings;
