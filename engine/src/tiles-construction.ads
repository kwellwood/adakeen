--
-- Copyright (c) 2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Tiles.Atlases;                     use Tiles.Atlases;

package Tiles.Construction is

    -- Creates a new Tile with id 'id'. The id must be unique within the library
    -- that the tile will be added to.
    function Create_Tile( id : Natural ) return A_Tile;

    -- Sets the bitmap and white mask of tile 'tile'. The tile's bitmap is first
    -- located in 'atlas', if provided. If the bitmap is not in the atlas, then
    -- the separate bitmap 'original' is used. The tile's white mask will be
    -- automatically generated, if necessary, and added to 'atlas'.
    procedure Set_Tile_Bitmap( tile     : not null A_Tile;
                               atlas    : A_Atlas;
                               original : A_Allegro_Bitmap );

end Tiles.Construction;
