--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Text_IO;
pragma Warnings( Off, Ada.Text_IO );
with Allegro;                           use Allegro;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays.Monitors;         use Allegro.Displays.Monitors;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Joystick;                  use Allegro.Joystick;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.Mouse.Cursors;             use Allegro.Mouse.Cursors;
with Allegro.Native_Dialogs;            use Allegro.Native_Dialogs;
with Allegro.Shaders;                   use Allegro.Shaders;
with Allegro.State;                     use Allegro.State;
with Assets;                            use Assets;
with Assets.Fonts;
with Debugging;                         use Debugging;
with Interfaces;                        use Interfaces;
with Mouse.Cursors.Manager;
with Preferences;                       use Preferences;
with Support;                           use Support;

package body Applications.Gui is

    MIN_WIN_WIDTH  : constant := 64;
    MIN_WIN_HEIGHT : constant := 64;
    MIN_FS_WIDTH   : constant := 640;
    MIN_FS_HEIGHT  : constant := 400;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    logWindow : A_Allegro_Textlog := null;

    procedure Debug_Output( str : String );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    overriding
    function Initialize( this : access Gui_Application ) return Boolean is
        useLogWindow : Boolean := False;
    begin
        if not Application(this.all).Initialize then
            return False;
        end if;
        Enable_Debug_Channels( D_INIT );

        -- Initialize native dialogs and log window
        if Al_Init_Native_Dialog_Addon then
            for i in 1..Argument_Count loop
                if Argument( i ) = "-log" then
                    useLogWindow := True;
                    exit;
                end if;
            end loop;
            if useLogWindow then
                logWindow := Al_Open_Native_Text_Log( To_String( this.name ) & " log",
                                                      ALLEGRO_TEXTLOG_NO_CLOSE or ALLEGRO_TEXTLOG_MONOSPACE );
                if logWindow = null then
                    this.Show_Error( "Log window disabled - Failed to open log window" );
                end if;
            end if;
        else
            this.Show_Error( "Log window disabled - Failed to init native dialogs" );
        end if;
        Debugging.Set_Output( Debug_Output'Access );

        -- Install keyboard handler
        if not Al_Install_Keyboard then
            Dbg( "Install_Keyboard: Error " & Image( Al_Get_Errno ), D_APP, Error );
            this.Show_Error( "Error initializing keyboard support." );
            raise INIT_EXCEPTION with "Error initializing keyboard support";
        end if;

        -- Setup mouse driver and cursor
        if not Al_Install_Mouse then
            Dbg( "Install_Mouse: Error " & Image( Al_Get_Errno ), D_APP, Error );
            this.Show_Error( "Error initializing mouse support." );
            raise INIT_EXCEPTION with "Error initializing mouse support";
        end if;
        if this.useMouse then
            Al_Set_System_Mouse_Cursor( this.display, ALLEGRO_SYSTEM_MOUSE_CURSOR_ARROW );
        end if;

        -- Setup gamepad driver
        if not Al_Install_Joystick then
            Dbg( "Install_Joystick: Error " & Image( Al_Get_Errno ), D_APP, Error );
            this.Show_Error( "Error initializing gamepad support." );
            raise INIT_EXCEPTION with "Error initializing gamepad support";
        end if;

        -- Initialize the primitives drawing addon
        if not Al_Init_Primitives_Addon then
            Dbg( "Al_Init_Primitives_Addon: Error " & Image( Al_Get_Errno ), D_APP, Error );
            this.Show_Error( "Error initializing primitives drawing." );
            raise INIT_EXCEPTION with "Error initializing primitives drawing";
        end if;

        -- Suggest the display color depth
        declare
            bpp : Unsigned_32 := Unsigned_32(Get_Pref( "application", "bpp", this.defaultColorBits ));
        begin
            bpp := (if bpp = 15 or bpp = 16 or bpp = 24 or bpp = 32 then bpp else Unsigned_32(this.defaultColorBits));
            pragma Debug( Dbg( "Requesting color depth: " & Image( bpp ) & " bpp", D_APP, Info ) );
            Al_Set_New_Display_Option( ALLEGRO_COLOR_SIZE, bpp, ALLEGRO_SUGGEST );
        end;

        -- Set the display size
        --
        -- Use the width/height preferences constrained to the nearest allowed
        -- resolution: For windowed, it must be smaller than the desktop and
        -- larger than the minimum size. For fullscreen windowed (borderless
        -- fullscreen), it must be the desktop resolution. For true fullscreen,
        -- it must be one of the supported resolutions.
        --
        -- If no width/height preferences are given, use the default size,
        -- constrained to the nearest allowed resolution.
        declare
            displayFlags  : constant Display_Flags := ALLEGRO_PROGRAMMABLE_PIPELINE or ALLEGRO_OPENGL;
            monitor       : aliased Allegro_Monitor_Info;
            bpp           : Integer;
            importance    : Option_Importance;
            desktopWidth,
            desktopHeight : Integer := 0;
            displayWidth,
            displayHeight : Integer := 0;
        begin
            -- Detect the monitor to use (use the widest)
            -- Determine the desktop resolution
            for i in 0..Al_Get_Num_Video_Adapters loop
                if Al_Get_Monitor_Info( i, monitor'Access ) then
                    if monitor.x2 - monitor.x1 > desktopWidth then
                        Al_Set_New_Display_Adapter( i );
                        desktopWidth := monitor.x2 - monitor.x1;
                        desktopHeight := monitor.y2 - monitor.y1;
                    end if;
                end if;
            end loop;
            if desktopWidth = 0 then
                -- unable to get the desktop resolution!
                desktopWidth := this.defaultWidth;
                desktopHeight := this.defaultHeight;
                Dbg( "Unable to get desktop resolution", D_APP, Warning );
            end if;

            -- request vsync ON by default
            -- 0 = Default (whatever graphics driver wants)
            -- 1 = ON
            -- 2 = OFF
            Al_Set_New_Display_Option( ALLEGRO_VSYNC, Unsigned_32(Get_Pref( "application", "vsync", this.defaultVsync )), ALLEGRO_SUGGEST );

            -- enable the depth buffer
            Al_Set_New_Display_Option( ALLEGRO_FLOAT_DEPTH, 0, ALLEGRO_SUGGEST );
            Al_Set_New_Display_Option( ALLEGRO_DEPTH_SIZE, Unsigned_32(this.defaultDepthBits), ALLEGRO_SUGGEST );

            -- enable multisampling for drawing anti-aliased primitives
            Al_Set_New_Display_Option( ALLEGRO_SAMPLE_BUFFERS, 1, ALLEGRO_SUGGEST );
            Al_Set_New_Display_Option( ALLEGRO_SAMPLES, Unsigned_32(this.defaultSamples), ALLEGRO_SUGGEST );

            -- windowed display
            -- windowed mode will be used if:
            --   1. "windowed" = 1
            --   2. "windowed" is undefined but .defaultWindowed = True
            --   3. "fullscreen" = 0
            --   4. "fullscreen" and "windowed" are undefined but .defaultWindowed = False
            if Get_Pref( "application", "windowed", this.defaultWindowed ) or not Get_Pref( "application", "fullscreen", True ) then
                Set_Pref( "application", "windowed", True );

                -- use the width/height preferences to determine the display
                -- size, constrained to the size of the desktop.
                Al_Set_New_Display_Flags(    displayFlags
                                          or ALLEGRO_WINDOWED
                                          or (if this.resizableWindow then ALLEGRO_RESIZABLE else 0)
                                          or (if this.resizableWindow and Get_Pref( "application", "maximized" ) then ALLEGRO_MAXIMIZED else 0)
                                          or (if this.framelessWindow then ALLEGRO_FRAMELESS else 0) );
                displayWidth := Get_Pref( "application", "xres", 0 );
                displayHeight := Get_Pref( "application", "yres", 0 );
                if displayWidth > 0 and displayHeight > 0 then
                    -- found width/height preferences
                    --
                    -- use them as the display size. constrain the dimensions
                    -- independently to the desktop size. the proportions may
                    -- not be preserved.
                    displayWidth := Constrain( displayWidth, MIN_WIN_WIDTH, desktopWidth );
                    displayHeight := Constrain( displayHeight, MIN_WIN_HEIGHT, desktopHeight );

                    -- 1. attempt to create the display at the requested resolution
                    this.display := Al_Create_Display( displayWidth, displayHeight );
                end if;

                if this.display = null then
                   -- 2. attempt to create the display at default resolution
                   this.display := Al_Create_Display( Constrain( this.defaultWidth, MIN_WIN_WIDTH, desktopWidth ),
                                                      Constrain( this.defaultHeight, MIN_WIN_HEIGHT, desktopHeight ) );
                end if;

                if this.display /= null and Get_Pref( "application", "fullscreen" ) then
                    -- convert the window to fullscreen mode
                    --
                    -- creating it first without fullscreen mode allows the
                    -- display to have a windowed size to fall back to if
                    -- fullscreen mode is disabled later. if fullscreen mode
                    -- fails, don't worry about it; just keep windowed mode.
                    if not Al_Set_Display_Flag( this.display, ALLEGRO_FULLSCREEN_WINDOW, True ) then
                        Set_Pref( "application", "fullscreen", False );
                    end if;
                end if;

            -- fullscreen display
            else

                -- use the width/height preferences to determine the display
                -- size, constrained to a valid fullscreen resolution.
                Al_Set_New_Display_Flags( displayFlags or ALLEGRO_FULLSCREEN );
                Al_Get_New_Display_Option( ALLEGRO_COLOR_SIZE, bpp, importance );
                displayWidth := Get_Pref( "application", "xres", 0 );
                displayHeight := Get_Pref( "application", "yres", 0 );
                if displayWidth > 0 and displayHeight > 0 then
                    -- found width/height preferences
                    --
                    -- use them as the primary display size.
                    displayWidth := Constrain( displayWidth, MIN_FS_WIDTH, 3840 );
                    displayHeight := Constrain( displayHeight, MIN_FS_HEIGHT, 3840 );

                    -- 1. attempt to create the display at the requested resolution
                    this.display := Al_Create_Display( displayWidth, displayHeight );
                end if;

                if this.display = null then
                    -- 2. attempt to create the display at desktop resolution
                    this.display := Al_Create_Display( desktopWidth, desktopHeight );
                end if;
                if this.display = null then
                    -- 3. attempt to create the display at desktop resolution
                    --    as a fullscreen window.
                    Al_Set_New_Display_Flags( displayFlags or ALLEGRO_FULLSCREEN_WINDOW );
                    this.display := Al_Create_Display( desktopWidth, desktopHeight );
                end if;

            end if;

            if this.display /= null then
                displayWidth := Al_Get_Display_Width( this.display );
                displayHeight := Al_Get_Display_Height( this.display );

                -- if the requested depth buffer could not be created, try other sizes
                if Al_Get_Display_Option( this.display, ALLEGRO_DEPTH_SIZE ) < Get_Pref( "application", "depth", this.defaultDepthBits ) then
                    Al_Destroy_Display( this.display );

                    -- 16 bit depth buffer
                    pragma Debug( Dbg( "Requesting 16 bit depth buffer", D_APP, Info ) );
                    Al_Set_New_Display_Option( ALLEGRO_DEPTH_SIZE, 16, ALLEGRO_REQUIRE );
                    this.display := Al_Create_Display( displayWidth, displayHeight );
                    if this.display = null then
                        Al_Destroy_Display( this.display );

                        -- 24 bit depth buffer
                        pragma Debug( Dbg( "Requesting 24 bit depth buffer", D_APP, Info ) );
                        Al_Set_New_Display_Option( ALLEGRO_DEPTH_SIZE, 24, ALLEGRO_REQUIRE );
                        this.display := Al_Create_Display( displayWidth, displayHeight );
                        if this.display = null then

                            -- 32 bit depth buffer
                            pragma Debug( Dbg( "Requesting 32 bit depth buffer", D_APP, Info ) );
                            Al_Set_New_Display_Option( ALLEGRO_DEPTH_SIZE, 32, ALLEGRO_REQUIRE );
                            this.display := Al_Create_Display( displayWidth, displayHeight );
                            if this.display = null then

                                -- no depth buffer could be created. just recreate the
                                -- original display. drawing won't work properly but the
                                -- app can still run.
                                Al_Set_New_Display_Option( ALLEGRO_FLOAT_DEPTH, 0, ALLEGRO_DONTCARE );
                                Al_Set_New_Display_Option( ALLEGRO_DEPTH_SIZE, 0, ALLEGRO_DONTCARE );
                                this.display := Al_Create_Display( displayWidth, displayHeight );
                            end if;
                        end if;
                    end if;
                end if;

                if this.display /= null then
                    -- record the depth buffer size that was discovered so we
                    -- don't do discovery again next time. it makes the window
                    -- flash in a disconcerting way.
                    Set_Pref( "application", "depth", Al_Get_Display_Option( this.display, ALLEGRO_DEPTH_SIZE ) );
                end if;
            end if;

            if this.display = null then
                -- failed to create a display
                Dbg( "Failed to create display [" &
                     Image( displayWidth ) & "x" & Image( displayHeight ) &
                     "]: Error " & Image( Al_Get_Errno ),
                     D_APP, Error );
                this.Show_Error( "Unable to create graphics display window." );
                raise INIT_EXCEPTION with "Failed to create display [" &
                                          Image( displayWidth ) & "x" &
                                          Image( displayHeight ) & "]";
            end if;

            if Al_Get_Display_Option( this.display, ALLEGRO_DEPTH_SIZE ) = 0 then
                Dbg( "Depth buffer is not supported", D_APP, Warning );
            end if;
            if Al_Get_Display_Option( this.display, ALLEGRO_SAMPLES ) = 0 then
                Dbg( "Multisampling is not supported", D_APP, Warning );
            end if;

            -- release the display so it's not active for the startup thread
            Al_Set_Target_Bitmap( null );

            pragma Debug( Dbg( "Created display: " &
                               Image( Al_Get_Display_Width( this.display ) ) & "x" &
                               Image( Al_Get_Display_Height( this.display ) ) & ", " &
                               Image( Al_Get_Pixel_Format_Bits( Al_Get_Display_Format( this.display ) ) ) & "bpp color, " &
                               Image( Al_Get_Display_Option( this.display, ALLEGRO_DEPTH_SIZE ) ) & " bits depth, " &
                               Image( Al_Get_Display_Refresh_Rate( this.display ) ) & " Hz refresh, " &
                               "vsync " & (if Al_Get_Display_Option( this.display, ALLEGRO_VSYNC ) = 1 then "on"
                                           elsif Al_Get_Display_Option( this.display, ALLEGRO_VSYNC ) = 2 then "off"
                                           else "default") & ", " &
                               Image( Al_Get_Display_Option( this.display, ALLEGRO_SAMPLES ) ) & "x multisampling",
                               D_APP, Info ) );
        end;

        -- Set the initial window title
        Al_Set_Window_Title( this.display, To_String( this.name ) );

        -- Inhibit the screensaver
        if not Al_Inhibit_Screensaver( True ) then
            Dbg( "Failed to inhibit screensaver", D_APP, Warning );
        end if;

        Mouse.Cursors.Manager.Init_Mouse_Cursors;
        if not Assets.Fonts.Initialize_Fonts then
            Dbg( "Failed to initialize font support", D_APP, Error );
            this.Show_Error( "Error initializing font support." );
            raise INIT_EXCEPTION with "Error initializing font support";
        end if;

        Disable_Debug_Channels( D_INIT );
        return True;
    exception
        when e : others =>
            Dbg( "Gui_Application.Initialize Exception: ...", D_APP, Error );
            Dbg( e, D_APP, Error );
            Disable_Debug_Channels( D_INIT );

            this.Finalize;
            return False;
    end Initialize;

    ----------------------------------------------------------------------------

    overriding
    procedure Finalize( this : access Gui_Application ) is
    begin
        Assets.Unload_All( Font_Assets );
        Assets.Fonts.Finalize_Fonts;
        Mouse.Cursors.Manager.Unload_Mouse_Cursors;

        Al_Set_Target_Backbuffer( this.display );
        Al_Use_Shader( null );
        Assets.Unload_All( Shader_Assets );
        Al_Destroy_Display( this.display );

        Al_Uninstall_Joystick;
        Al_Uninstall_Mouse;
        Al_Uninstall_Keyboard;
        Al_Shutdown_Primitives_Addon;

        Al_Close_Native_Text_Log( logWindow );

        Application(this.all).Finalize;
    end Finalize;

    ----------------------------------------------------------------------------

    function Get_Display( this : not null access Gui_Application'Class ) return A_Allegro_Display is (this.display);

    ----------------------------------------------------------------------------

    function Is_Mouse_Enabled( this : not null access Gui_Application'Class ) return Boolean is (this.useMouse);

    ----------------------------------------------------------------------------

    procedure Show_Error( this    : not null access Gui_Application'Class;
                          title   : String;
                          heading : String;
                          text    : String ) is
    begin
        Al_Show_Native_Message_Box( this.display,
                                    title, heading, text,
                                    "", ALLEGRO_MESSAGEBOX_ERROR );
    end Show_Error;

    ----------------------------------------------------------------------------

    overriding
    procedure Show_Error( this : access Gui_Application; text : String ) is
    begin
        this.Show_Error( "Error!", "An error occurred", text );
    end Show_Error;

    --==========================================================================

    procedure Debug_Output( str : String ) is
    begin
        if logWindow /= null then
            Al_Append_Native_Text_Log( logWindow, str & ASCII.LF );
        end if;
        pragma Debug( Ada.Text_IO.Put_Line( str ) );
    end Debug_Output;

end Applications.Gui;
