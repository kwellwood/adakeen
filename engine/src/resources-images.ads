--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Streams;                       use Ada.Streams;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;

package Resources.Images is

    -- Returns True if the given format is supported. The format string is the
    -- image format file extension without a leading dot character.
    function Is_Format_Supported( format : String ) return Boolean;

    -- Loads an image of format 'format' from memory buffer 'data'. The format
    -- string is the image format file extension without a leading dot
    -- character. Returns null on error.
    function Load_Image( format : String;
                         data   : not null access Stream_Element_Array
                       ) return A_Allegro_Bitmap;

    -- Loads an image from resource file 'resource', if it's of a supported
    -- image format. The extension of the 'resource's filename will be used to
    -- determine the file format. Returns null on error.
    function Load_Image( resource : not null A_Resource_File ) return A_Allegro_Bitmap;

    -- Loads an image from 'filePath', if it's of a supported image format.
    -- If 'filePath' is relative, it will be located according to the standard
    -- rules for finding a resource file. Returns null on error.
    function Load_Image( filePath : String; group : String ) return A_Allegro_Bitmap;

end Resources.Images;
