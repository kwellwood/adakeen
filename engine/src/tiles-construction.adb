--
-- Copyright (c) 2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Locking;           use Allegro.Bitmaps.Locking;
with Allegro.Color;                     use Allegro.Color;
with Drawing;                           use Drawing;
with Interfaces;                        use Interfaces;
with Support;                           use Support;
with System.Storage_Elements;
with Values.Casting;                    use Values.Casting;

package body Tiles.Construction is

    function Create_Tile( id : Natural ) return A_Tile is
        tile : constant A_Tile := Create_Tile;
    begin
        tile.Set_Id( id );
        return tile;
    end Create_Tile;

    ----------------------------------------------------------------------------

    procedure Set_Tile_Bitmap( tile     : not null A_Tile;
                               atlas    : A_Atlas;
                               original : A_Allegro_Bitmap ) is

        ------------------------------------------------------------------------

        function Create_Mask( bmp : A_Allegro_Bitmap ) return A_Allegro_Bitmap is
            mask   : A_Allegro_Bitmap;
            region : A_Allegro_Locked_Region;
        begin
            mask := Al_Clone_Bitmap( bmp );

            -- this could be done with a shader but it's a lot more code to
            -- write the fragment and pixel shaders here and compile them and
            -- all that (to avoid an extra dependency on the Shaders package).
            -- this is quick and dirty and fast enough for now.
            region := Al_Lock_Bitmap( mask, ALLEGRO_PIXEL_FORMAT_RGBA_8888, ALLEGRO_LOCK_READWRITE );
            for y in 0..Al_Get_Bitmap_Height( mask )-1 loop
                declare
                    use System.Storage_Elements;
                    type Pixel_Data is array (Integer range 0..abs(region.pitch)-1) of Unsigned_8;
                    data : Pixel_Data;
                    for data'Address use region.data + Storage_Offset(y * region.pitch);
                begin
                    for x in 0..Al_Get_Bitmap_Width( mask )-1 loop
                        -- remember that RGBA means R is MSB and A is LSB of pixel
                        data(x * region.pixel_size + 3) := 255;   -- red
                        data(x * region.pixel_size + 2) := 255;   -- green
                        data(x * region.pixel_size + 1) := 255;   -- blue
                    end loop;
                end;
            end loop;
            Al_Unlock_Bitmap( mask );

            return mask;
        end Create_Mask;

        ------------------------------------------------------------------------

        x        : constant Integer := Cast_Int( tile.Get_Attribute( "x" ) );
        y        : constant Integer := Cast_Int( tile.Get_Attribute( "y" ) );
        wh       : constant Integer := Cast_Int( tile.Get_Attribute( "wh" ) );
        w        : constant Integer := Cast_Int( tile.Get_Attribute( "w" ), wh );
        h        : constant Integer := Cast_Int( tile.Get_Attribute( "h" ), wh );
        useAtlas : constant Boolean := Cast_Boolean( tile.Get_Attribute( "useAtlas" ), True );
        tileBmp  : A_Allegro_Bitmap;
        maskBmp  : A_Allegro_Bitmap;
    begin
        -- try to get an existing sub bitmap from the atlas
        if atlas /= null then
            tileBmp := atlas.Get_Bitmap( tile.Get_Name, x, y, w, h );
        end if;

        if tileBmp = null then
            if original = null then
                return;               -- no bitmap to set!
            end if;

            -- fall back to a copy of 'original' (the bitmap wasn't in the atlas)
            if x = 0 and y = 0 and w = 0 and h = 0 then
                -- copy the whole source image
                tileBmp := Al_Clone_Bitmap( original );
            else
                -- copy a region of the source image: create a new bitmap for
                -- the tile with the right size and blit the source image onto it.
                tileBmp := Al_Create_Bitmap( Integer'Min( (if w > 0 then w else Al_Get_Bitmap_Width( original )), Al_Get_Bitmap_Width( original ) - x ),
                                             Integer'Min( (if h > 0 then h else Al_Get_Bitmap_Height( original )), Al_Get_Bitmap_Height( original ) - y ) );
                Set_Target_Bitmap( tileBmp );
                Al_Draw_Bitmap_Region( original,
                                       Float(x), Float(y),
                                       Float(Al_Get_Bitmap_Width( tileBmp )),
                                       Float(Al_Get_Bitmap_Height( tileBmp )),
                                       0.0, 0.0,
                                       0 );
            end if;
        elsif not useAtlas then
            -- the tile's bitmap is in the atlas but we don't want to use it.
            -- this is most likely because it's tiled background scenery. copy
            -- it out to a separate bitmap.
            tileBmp := Al_Clone_Bitmap( tileBmp );
        end if;

        pragma Assert( tileBmp /= null );

        -- if necessary, create a white mask for the bitmap. it's created
        -- from the individual tile bitmap, instead of the source bitmap,
        -- so that we don't create a mask for a whole tile sheet, if that
        -- is where this tile came from.
        if tile.Is_Mask_Required then
            maskBmp := Create_Mask( tileBmp );
            if useAtlas and then atlas /= null then
                -- add the mask to the tile atlas and store the atlas bitmap in the tile
                if atlas.Add_Bitmap( tile.Get_Name & "-mask" & Image( tile.Get_Id ), maskBmp ) then
                    -- 'maskBmp' was added to the atlas; destroy the standalone
                    -- bitmap and retrieve a pointer to the atlas' copy.
                    Al_Destroy_Bitmap( maskBmp );
                    maskBmp := atlas.Get_Bitmap( tile.Get_Name & "-mask" & Image( tile.Get_Id ) );
                else
                    -- 'maskBmp' could not be added to the atlas
                    null;
                end if;
            end if;
        end if;

        tile.Set_Bitmap( tileBmp, maskBmp );
    end Set_Tile_Bitmap;

end Tiles.Construction;
