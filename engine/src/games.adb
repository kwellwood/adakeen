--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Real_Time;                     use Ada.Real_Time;
with Applications;                      use Applications;
with Applications.Gui.Games;            use Applications.Gui.Games;
with Component_Families;                use Component_Families;
with Debugging;                         use Debugging;
with Entities.Collidables;              use Entities.Collidables;
with Entities.Factory;                  use Entities.Factory;
with Entities.Solids;                   use Entities.Solids;
with Entities.Systems.Collision;        use Entities.Systems.Collision;
with Entities.Systems.Entity_Clipping;  use Entities.Systems.Entity_Clipping;
with Entities.Systems.Positioning;      use Entities.Systems.Positioning;
with Signals;                           use Signals;
with Support;                           use Support;

package body Games is

    package Connections is new Signals.Connections(Game);
    use Connections;

    instance : A_Game := null;

    ----------------------------------------------------------------------------

    -- Called at the start of each frame of the game thread, to synchronize the
    -- game and view threads. This prevents frame skipping.
    procedure On_Frame_Start( this : not null access Game'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Game( updateHz : Positive;
                          scriptVM : not null A_Script_VM;
                          session  : not null A_Session ) return A_Game is
    begin
        pragma Assert( instance = null, "Game already created" );
        instance := new Game;
        instance.Construct( updateHz, scriptVM, session );
        return instance;
    end Create_Game;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Game ) is
    begin
        if this = instance then
            instance := null;
        end if;
        Delete( A_Limited_Object(this) );
    end Delete;

    ----------------------------------------------------------------------------

    function Get_Game return A_Game is (instance);

    --==========================================================================

    not overriding
    procedure Construct( this     : access Game;
                         updateHz : Positive;
                         scriptVM : not null A_Script_VM;
                         session  : not null A_Session ) is
    begin
        Limited_Object(this.all).Construct;
        this.scriptVM := scriptVM;
        this.session := session;

        this.pman := Create_Process_Manager_Fixed_Step( "game",
                                                        Constrain( updateHz, 30, 1000 ),
                                                        eventChannel => GAME_CHANNEL );

        pragma Assert( Entities.Factory.Global = null );
        Entities.Factory.Global := Create_Entity_Factory;

        this.ecs := Create_Entity_System( this );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Game ) is
    begin
        Delete( this.session );
        Delete( this.ecs );
        Delete( this.pman );
        Delete( Entities.Factory.Global );
        Delete( this.scriptVM );

        Limited_Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Attach( this : not null access Game'Class;
                      proc : not null access Process'Class ) is
    begin
        this.pman.Attach_Async( proc );
    end Attach;

    ----------------------------------------------------------------------------

    procedure Detach( this : not null access Game'Class;
                      proc : not null access Process'Class ) is
    begin
        this.pman.Detach_Async( proc );
    end Detach;

    ----------------------------------------------------------------------------

    function Get_Corral( this : not null access Game'Class ) return A_Corral is (this.pman.Get_Corral);

    ----------------------------------------------------------------------------

    function Get_Entity_System( this : not null access Game'Class ) return A_Entity_System is (this.ecs);

    ----------------------------------------------------------------------------

    function Get_Process_Manager( this : not null access Game'Class ) return A_Process_Manager is (this.pman);

    ----------------------------------------------------------------------------

    function Get_Script_VM( this : not null access Game'Class ) return A_Script_VM is (this.scriptVM);

    ----------------------------------------------------------------------------

    function Get_Session( this : not null access Game'Class ) return A_Session is (this.session);

    ----------------------------------------------------------------------------

    procedure Initialize( this : not null access Game'Class ) is
    begin
        -- register standard component systems
        this.ecs.Register_System( SOLID_ID,      Create_Entity_Clipping_System );
        this.ecs.Register_System( COLLIDABLE_ID, Create_Collision_System );
        this.ecs.Register_System( LOCATION_ID,   Create_Positioning_System );

        -- register the view/game frame synchronization hook. the FrameStarted
        -- signal is emitted at the beginning of each execution frame.
        this.pman.FrameStarted.Connect( Slot( this, On_Frame_Start'Access ) );

        this.session.Initialize( this );
    end Initialize;

    ----------------------------------------------------------------------------

    procedure On_Frame_Start( this : not null access Game'Class ) is
        app            : constant A_Game_Application := A_Game_Application(Get_Application);
        viewTargetRate : constant Float := Float(app.Get_View.Get_Process_Manager.Get_Target_Rate);
        viewFrameRate  : constant Float := app.Get_View.Get_Process_Manager.Get_Frame_Rate;
        gameTargetRate : constant Float := Float(this.Get_Process_Manager.Get_Target_Rate);
        skipN          : constant Float := gameTargetRate / viewTargetRate;
    begin
        -- prevent frame duplication by waiting for the view to finish drawing
        -- a frame if the game is actually running (almost) as fast as the view
        -- is supposed to run, and if the view is actually running (almost) as
        -- fast as the game is actually running. There is a very small frame
        -- rate window in which the frames should be synchronized, otherwise the
        -- framerate will drop noticeably when frame synchronization kicks in.
        if this.Get_Process_Manager.Get_Frame_Rate * 1.1 > viewTargetRate * skipN and then
           this.Get_Process_Manager.Get_Frame_Rate       < viewFrameRate * skipN * 1.1
        then
            -- synchronize every Nth frame
            if not this.frameSyncOn then
                this.frameSyncOn := True;
                pragma Debug( Dbg( "Game frame sync is ON", D_PROCS, Info ) );
            end if;
            if this.frames mod Integer(Float'Max( 1.0, skipN ) ) = 0 then
                app.Get_View.Get_Corral.Wait_Dispatch_Start( Milliseconds( 500 ) );
            end if;
        else
            if this.frameSyncOn then
                this.frameSyncOn := False;
                pragma Debug( Dbg( "Game frame sync is OFF", D_PROCS, Info ) );
            end if;
        end if;
        this.frames := this.frames + 1;
    end On_Frame_Start;

    ----------------------------------------------------------------------------

    procedure Start( this : not null access Game'Class ) is
    begin
        pragma Debug( Dbg( "Starting game logic", D_GAME, Info ) );
        this.pman.Start;
    end Start;

    ----------------------------------------------------------------------------

    procedure Stop( this : not null access Game'Class ) is
    begin
        pragma Debug( Dbg( "Stopping game logic", D_GAME, Info ) );
        this.pman.Stop( timeout => 5.0 );
        this.session.Finalize;
        this.ecs.Shutdown;
    end Stop;

end Games;
