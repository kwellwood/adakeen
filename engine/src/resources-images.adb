--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Color;                     use Allegro.Color;
--with Debugging;                         use Debugging;
with Resources.Allegro_Files;           use Resources.Allegro_Files;
with Support;                           use Support;
with Support.Paths;                     use Support.Paths;

package body Resources.Images is

    function Is_Format_Supported( format : String ) return Boolean is
    begin
        return Case_Eq( format, "bmp" ) or else
               Case_Eq( format, "png" );
    end Is_Format_Supported;

    ----------------------------------------------------------------------------

    function Load_Image( format : String;
                         data   : not null access Stream_Element_Array
                       ) return A_Allegro_Bitmap is
        bmp  : A_Allegro_Bitmap := null;
        file : A_Allegro_File := null;
    begin
        file := Open_Memfile( data );
        bmp := Al_Load_Bitmap_f( file, '.' & To_Lower( format ) );
        Al_Fclose( file );
        if bmp /= null and Case_Eq( format, "bmp" ) then
            Al_Convert_Mask_To_Alpha( bmp, Al_Map_RGB( 255, 0, 255 ) );
        end if;
        return bmp;
    end Load_Image;

    ----------------------------------------------------------------------------

    function Load_Image( resource : not null A_Resource_File ) return A_Allegro_Bitmap is
        format : constant String := To_Lower( Get_Extension( resource.Get_Path ) );
        bmp    : A_Allegro_Bitmap := null;
        file   : A_Allegro_File := null;
    begin
        if resource.Size > 0 then
            file := resource.Create_Allegro_File;
            bmp := Al_Load_Bitmap_f( file, '.' & format );
            Al_Fclose( file );
            if bmp /= null and format = "bmp" then
                Al_Convert_Mask_To_Alpha( bmp, Al_Map_RGB( 255, 0, 255 ) );
            end if;
        end if;
        return bmp;
    end Load_Image;

    ----------------------------------------------------------------------------

    function Load_Image( filePath : String; group : String ) return A_Allegro_Bitmap is
        resource : A_Resource_File := null;
        bmp      : A_Allegro_Bitmap := null;
    begin
        resource := Load_Resource( filePath, group );
        if resource /= null then
            bmp := Load_Image( resource );
            Delete( resource );
        end if;
        return bmp;
    end Load_Image;

end Resources.Images;
