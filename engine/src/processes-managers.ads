--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Task_Identification;           use Ada.Task_Identification;
with Events;                            use Events;
with Events.Corrals;                    use Events.Corrals;
with Mutexes;                           use Mutexes;
with Objects;                           use Objects;
with Processes;                         use Processes;
with Signals;                           use Signals;
with Semaphores;                        use Semaphores;

private with Ada.Containers.Doubly_Linked_Lists;
private with Ada.Strings.Unbounded;
private with Ada.Unchecked_Deallocation;

package Processes.Managers is

    -- A Process_Manager implements a thread's main loop. Once its execution
    -- starts, a Process_Manager's internal thread repeatedly loops, updating
    -- each of its attached Processes once per frame.
    --
    -- The Process_Manager notifies Processes of elapsed time between frames in
    -- one of two modes, fixed step or real time:
    --
    --    In fixed step mode, the Process_Manager maintains an internal
    --    simulation time that updates in fixed steps between each frame,
    --    regardless of the real time that elapses between frames. This provides
    --    deterministic behavior, regardless of execution time. If the frame
    --    time budget is exceeded, simulation time will lag behind real time.
    --
    --    In real time mode, the Process_Manager notifies Processes of the real
    --    elapsed time between frames, allowing a Process to operate in real
    --    time, regardless of the frame rate.
    --
    -- Each Process_Manager maintains its own event Corral, allowing processes
    -- to listen for events dispatched by the application's global event manager.
    type Process_Manager is new Limited_Object with private;
    type A_Process_Manager is access all Process_Manager'Class;

    -- An access to a procedure that is responsible for destroying a Process.
    type A_Destructor is access procedure( process : in out A_Process );

    -- Creates a new Process_Manager that updates its Processes using a fixed
    -- simulation time step for deterministic behavior.
    --
    -- The thread will update no faster than 'targetRateHz' frames per second.
    -- If the attached Processes exceed the target time budget, then the update
    -- rate will drop below 'targetRateHz' frames per second in real time.
    -- However, the time step provided to Processes is fixed at 1 / 'targetRate'
    -- seconds. Simulation time will lag behind real time if the target frame
    -- rate cannot be achieved.
    --
    -- If Processes that will be attached to this Process_Manager queue events
    -- that should be framed (processed together in frames by the receiving
    -- threads), then set 'eventChannel' to the appropriate channel number. The
    -- Process_Manager will notify the global Event Manager when the event
    -- channel frame is complete.
    function Create_Process_Manager_Fixed_Step( name         : String;
                                                targetRateHz : Positive;
                                                eventChannel : Event_Channel := NO_CHANNEL
                                              ) return A_Process_Manager;
    pragma Postcondition( Create_Process_Manager_Fixed_Step'Result /= null );

    -- Creates a new Process_Manager that updates its Processes in real time,
    -- using a variable time step.
    --
    -- When 'maxRateHz' is set, the Processes will update no faster than
    -- 'maxRateHz' frames per second. When 'maxRateHz' is zero, the Processes
    -- will be updated as fast as possible. Processes will receive variable
    -- elapsed real time between frames, allowing their time scale to always
    -- track with real time.
    --
    -- If updating the Processes takes more than 1 / 'maxRateHz' seconds, the
    -- maximum frame rate will not be achieved. The Processes will then receive
    -- delta times greater than 1 / 'maxRateHz' per frame.
    --
    -- If Processes that will be attached to this Process_Manager queue events
    -- that should be framed (processed together in frames by the receiving
    -- threads), then set 'eventChannel' to the appropriate channel number. The
    -- Process_Manager will notify the global Event Manager when the event
    -- channel frame is complete.
    function Create_Process_Manager_Real_Time( name         : String;
                                               maxRateHz    : Natural;
                                               eventChannel : Event_Channel := NO_CHANNEL
                                             ) return A_Process_Manager;
    pragma Postcondition( Create_Process_Manager_Real_Time'Result /= null );

    -- Asynchronously attaches a process to the process manager for execution.
    -- It will be executed last in the rotation of attached processes. This can
    -- be called at any time before Stop. Constraint_Error will be raised if the
    -- process manager has been stopped.
    procedure Attach_Async( this : not null access Process_Manager'Class;
                            proc : not null access Process'Class );

    -- Asynchronously detaches a process from the process manager. This can be
    -- called at any time before Stop.
    --
    -- Note that if the calling thread is not the process manager's own thread,
    -- it is NOT safe to delete 'prop' immediately after detaching it (due to
    -- the asynchronous nature of the operation). While the process manager is
    -- running, it is not safe for an external thread to call Detach_Async() and
    -- then delete 'proc', without additional synchronization.
    procedure Detach_Async( this : not null access Process_Manager'Class;
                            proc : not null access Process'Class );

    -- Asynchronously detaches a Process from the process manager. This can be
    -- called at any time. The 'destructor' procedure (if provided) will be used
    -- to delete 'proc' after it is detached from the process manager. 'proc' is
    -- always consumed, but the process manager only takes ownership of it if a
    -- destructor is given. If caller does not want to destroy 'proc', it is
    -- simpler to call the Detach_Async() procedure above.
    procedure Detach_Async( this       : not null access Process_Manager'Class;
                            proc       : in out A_Process;
                            destructor : A_Destructor );
    pragma Postcondition( proc = null );

    -- This signal is emmitted at the start of each frame, on the process
    -- manager's own internal thread.
    function FrameStarted( this : not null access Process_Manager'Class ) return access Signal'Class;

    -- Returns the process manager's event corral, for adding and removing event
    -- listeners that run on the process manager's execution thread. Events are
    -- dispatched to listeners at the beginning of each execution frame, before
    -- the earliest attached process is ticked.
    function Get_Corral( this : not null access Process_Manager'Class ) return A_Corral;
    pragma Postcondition( Get_Corral'Result /= null );

    -- Returns the frame rate in Hz that the process manager is updating its
    -- processes. If the thread is not at max load, the rate returned may be
    -- faster that its actual rate, reflecting how fast the process manager
    -- COULD be updating if it was not sleeping between frames.
    function Get_Frame_Rate( this : not null access Process_Manager'Class ) return Float;

    -- Returns the duration of the previous complete frame, in real time. If the
    -- process manager falls behind real time, this will equal or exceed the
    -- target frame time.
    function Get_Frame_Time( this : not null access Process_Manager'Class ) return Time_Span;

    -- Returns the name of the process manager given at creation.
    function Get_Name( this : not null access Process_Manager'Class ) return String with Inline;

    -- Returns the target real time update rate of the process manager. Zero
    -- will be returned for real time process managers that are not rate limited.
    function Get_Target_Rate( this : not null access Process_Manager'Class ) return Natural;

    -- Asynchronously pauses or resumes the given Process. When paused, a
    -- Process's Tick procedure will not be called until the Process is resumed
    -- again. Nothing will happen if 'proc' is not attached to this process
    -- manager. Constraint_Error will be raised if the process manager's
    -- execution is stopped.
    procedure Pause_Async( this   : not null access Process_Manager'Class;
                           proc   : not null access Process'Class;
                           paused : Boolean );

    -- Runs the process manager on the calling thread. Process updates will
    -- begin and continue until execution returns. This procedure will not return
    -- until the process manager is stopped by calling Stop().
    --
    -- This can only be called once in the lifetime of the object.
    procedure Run( this : not null access Process_Manager'Class );

    -- Starts the process manager's internal thread. Process updates will begin.
    -- If execution has already been started, this will have no effect.
    procedure Start( this : not null access Process_Manager'Class );

    -- Stops the process manager's internal thread, preventing any further
    -- updates to the attached processes. Once execution has been stopped, it
    -- cannot be restarted during the object's lifetime. This procedure must NOT
    -- be called by the process manager's own thread.
    --
    -- If 'timeout' (seconds) is >= 0.1, then the calling thread will wait for
    -- the process manager's internal thread to terminate before returning.
    procedure Stop( this    : not null access Process_Manager'Class;
                    timeout : Duration := 1.0 );
    pragma Precondition( timeout >= 0.0 );

    -- Waits for the end of the current frame to complete. If 'timeout' is set
    -- to zero, the timeout period will be infinite.
    procedure Wait_Frame_End( this    : not null access Process_Manager'Class;
                              timeout : Time_Span );

    -- Deletes a process manager. Its attached processes are detached and left
    -- unchanged, as it does not own them. Take care to ensure that all
    -- remaining processes are deleted elsewhere.
    procedure Delete( this : in out A_Process_Manager );
    pragma Postcondition( this = null );

private

    use Ada.Strings.Unbounded;

    -- The execution context of a Process.
    type Execution is
        record
            process    : A_Process := null;
            firstTick  : Time := Time_First;
            lastTick   : Time := Time_First;
            paused     : Boolean := False;
            pauseTime  : Time := Time_First;
            detachMe   : Boolean := False;        -- to be detached by Ticker_Task
            destructor : A_Destructor := null;    -- optional destructor after detach
        end record;
    type A_Execution is access all Execution;

    function Eq( l, r : A_Execution ) return Boolean
    is (if    l = null then (r = null)
        elsif r = null then False
        else  l.process = r.process);

    procedure Delete is new Ada.Unchecked_Deallocation( Execution, A_Execution );

    package Execution_Lists is new Ada.Containers.Doubly_Linked_Lists(A_Execution, Eq);
    use Execution_Lists;

    ----------------------------------------------------------------------------

    -- An Operation represents a queued asynchronous operation involving a
    -- Process: Attach, Detach, Pause, etc. All of these operations are
    -- asynchronous to allow a Process to pause or detach itself during its Tick.
    type Operation is abstract tagged
        record
            process : A_Process := null;
        end record;
    type A_Operation is access all Operation'Class;

    -- Executes the Operation on the process manager's Process list. 'now' is
    -- the clock time of the operation.
    procedure Execute( this     : access Operation;
                       now      : Time;
                       execList : in out Execution_Lists.List ) is abstract;

    procedure Delete is new Ada.Unchecked_Deallocation( Operation'Class, A_Operation );

    package Operation_Lists is new Ada.Containers.Doubly_Linked_Lists( A_Operation, "=" );

    ----------------------------------------------------------------------------

    -- The process manager's internal task that updates its Processes.
    task type Ticker_Task is
        entry Init( pman : A_Process_Manager );
    end Ticker_Task;
    type A_Ticker_Task is access all Ticker_Task;

    procedure Delete is new Ada.Unchecked_Deallocation( Ticker_Task, A_Ticker_Task );

    ----------------------------------------------------------------------------

    type Process_Manager is new Limited_Object with
        record
            name            : Unbounded_String;
            targetHz        : Natural := 0;
            fixedStep       : Boolean := False;
            corral          : A_Corral := null;
            eventChannel    : Event_Channel := NO_CHANNEL;

            frameEnd        : Binary_Semaphore(True);
            sigFrameStarted : aliased Signal;

            lock            : Mutex;                   -- protects the fields below
            taskId          : Task_Id := Null_Task_Id; -- task running the manager
            started         : Boolean := False;
            stopped         : Boolean := False;
            opQueue         : Operation_Lists.List;
            ticker          : A_Ticker_Task := null;
            rate            : Float := 0.0;
            frameTime       : Time_Span := Milliseconds( 1 );
            frameStart      : Time := Clock;
        end record;

    procedure Construct( this         : access Process_Manager;
                         name         : String;
                         hertz        : Natural;
                         fixedStep    : Boolean;
                         eventChannel : Event_Channel );

    procedure Delete( this : in out Process_Manager );

    function Is_Stopped( this : not null access Process_Manager'Class ) return Boolean;

end Processes.Managers;
