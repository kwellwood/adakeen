--
-- Copyright (c) 2012-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Unchecked_Deallocation;
--with Debugging;                         use Debugging;

package body Bin_Packing is

    function Is_Contained_In( a, b : Rect ) return Boolean
    is (
        a.x >= b.x and then a.y >= b.y and then
        a.x + a.width <= b.x + b.width and then
        a.y + a.height <= b.y + b.height
    );

    --==========================================================================

    function Create_Bin( width, height : Positive ) return A_Bin is
        this : constant A_Bin := new Bin;
    begin
        this.width := width;
        this.height := height;
        this.freeRects.Append( Rect'(x => 0, y => 0, width => width, height => height) );
        return this;
    end Create_Bin;

    ----------------------------------------------------------------------------

    function Find_Position_BSSF( this          : not null access Bin'Class;
                                 width, height : Natural ) return Rect is
        pos              : Rect_Vectors.Cursor;
        bestNode,
        freeNode         : Rect;
        extraHoriz,
        extraVert        : Natural;
        shortSideFit     : Natural;
        bestShortSideFit : Natural := Natural'Last;
    begin
        pos := this.freeRects.First;
        while Rect_Vectors.Has_Element( pos ) loop
            freeNode := Rect_Vectors.Element( pos );

            if freeNode.width >= width and then freeNode.height >= height then
                extraHoriz := abs (freeNode.width - width);
                extraVert := abs (freeNode.height - height);
                shortSideFit := Natural'Min( extraHoriz, extraVert );

                if shortSideFit < bestShortSideFit then
                    bestNode.x := freeNode.x;
                    bestNode.y := freeNode.y;
                    bestNode.width := width;
                    bestNode.height := height;
                    bestShortSideFit := shortSideFit;

                    if bestShortSideFit = 0 then
                        -- can't improve on that and we have no tie breaker
                        -- heuristic (like best long side fit)
                        exit;
                    end if;
                end if;
            end if;

            Rect_Vectors.Next( pos );
        end loop;

        return bestNode;
    end Find_Position_BSSF;

    ----------------------------------------------------------------------------

    function Insert( this          : not null access Bin'Class;
                     width, height : Natural ) return Rect is
        use Rect_Vectors;

        newNode : Rect;
        i, last : Integer;
    begin
        newNode := this.Find_Position_BSSF( width, height );
        if newNode.height = 0 then
            -- the rectangle doesn't fit
            return newNode;
        end if;

        i := 1;
        last := Integer(this.freeRects.Length);
        while i <= last loop

            if this.Split_Free_Node( this.freeRects.Element( Positive(i) ), newNode ) then
                this.freeRects.Delete( Positive(i) );
                last := last - 1;
            else
                i := i + 1;
            end if;
        end loop;

        this.Prune_Free_List;

        return newNode;
    end Insert;

    ----------------------------------------------------------------------------

    procedure Prune_Free_List( this : not null access Bin'Class ) is
        i, j : Natural;
    begin
        i := 1;
        while i <= Natural(this.freeRects.Length) loop

            j := i + 1;
            while j <= Natural(this.freeRects.Length) loop

                if Is_Contained_In( this.freeRects.Element( i ), this.freeRects.Element( j ) ) then
                    this.freeRects.Delete( i );
                    i := i - 1;
                    exit;
                elsif Is_Contained_In( this.freeRects.Element( j ), this.freeRects.Element( i ) ) then
                    this.freeRects.Delete( j );
                else
                    j := j + 1;
                end if;

            end loop;

            i := i + 1;
        end loop;
    end Prune_Free_List;

    ----------------------------------------------------------------------------

    function Split_Free_Node( this     : not null access Bin'Class;
                              freeNode,
                              usedNode : Rect ) return Boolean is
        newNode : Rect;
    begin
        -- bail out if the rectangles do not intersect
        if usedNode.x >= freeNode.x + freeNode.width or else
           usedNode.x + usedNode.width <= freeNode.x or else
           usedNode.y >= freeNode.y + freeNode.height or else
           usedNode.y + usedNode.height <= freeNode.y
        then
            return False;
        end if;

        if usedNode.x < freeNode.x + freeNode.width and then usedNode.x + usedNode.width > freeNode.x then
            -- new free node at the top side of the used node
            if usedNode.y > freeNode.y and then usedNode.y < freeNode.y + freeNode.height then
                newNode := freeNode;
                newNode.height := usedNode.y - newNode.y;
                this.freeRects.Append( newNode );
            end if;

            -- new node at the bottom side of the used node
            if usedNode.y + usedNode.height < freeNode.y + freeNode.height then
                newNode := freeNode;
                newNode.y := usedNode.y + usedNode.height;
                newNode.height := freeNode.y + freeNode.height - newNode.y;
                this.freeRects.Append( newNode );
            end if;
        end if;

        if usedNode.y < freeNode.y + freeNode.height and then usedNode.y + usedNode.height > freeNode.y then
            -- new node at the left side of the used node
            if usedNode.x > freeNode.x and then usedNode.x < freeNode.x + freeNode.width then
                newNode := freeNode;
                newNode.width := usedNode.x - newNode.x;
                this.freeRects.Append( newNode );
            end if;

            -- new node at the right side of the used node
            if usedNode.x + usedNode.width < freeNode.x + freeNode.width then
                newNode := freeNode;
                newNode.x := usedNode.x + usedNode.width;
                newNode.width := freeNode.x + freeNode.width - newNode.x;
                this.freeRects.Append( newNode );
            end if;
        end if;

        return True;
    end Split_Free_Node;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Bin ) is
        procedure Free is new Ada.Unchecked_Deallocation( Bin'Class, A_Bin );
    begin
        this.freeRects.Clear;
        Free( this );
    end Delete;

end Bin_Packing;
