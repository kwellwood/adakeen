--
-- Copyright (c) 2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Mutexes;                           use Mutexes;
with Objects;                           use Objects;

package Read_Write_Locks is

    type Read_Write_Lock is new Limited_Object with
        record
            orderQueue : Mutex;
            accessLock : Mutex;
            countLock  : Mutex;
            readCount  : Natural := 0;
        end record;

    procedure Acquire_Read( this : in out Read_Write_Lock );

    procedure Acquire_Write( this : in out Read_Write_Lock );

    procedure Release_Read( this : in out Read_Write_Lock );

    procedure Release_Write( this : in out Read_Write_Lock );

end Read_Write_Locks;
