--
-- Copyright (c) 2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Objects;                           use Objects;
with Scribble.Runtimes;                 use Scribble.Runtimes;
with Values.Maps;                       use Values.Maps;

private with Scribble.Parsers.Scripts;
private with Scribble.Script_Analyzers;
private with Scribble.Script_Generators;

package Scribble.Script_Compilers is

    -- The Scribble script compiler compiles script source files into Map values
    -- containing the members of the script. Scripts are composed of member
    -- functions, member variables, and timers.
    type Script_Compiler is new Limited_Object with private;
    type A_Script_Compiler is access all Script_Compiler'Class;

    -- Creates a new Scribble compiler. The Scribble runtime provides
    -- information about the available Ada functions during compilation.
    function Create_Script_Compiler( runtime : not null A_Scribble_Runtime ) return A_Script_Compiler;
    pragma Postcondition( Create_Script_Compiler'Result /= null );

    -- Compiles the Scribble source code in file 'filePath' as a script. The
    -- script file may include other script files. The file path will be
    -- resolved using the standard Resources algorithm. Compiled functions will
    -- include debugging information if 'enableDebug' is True.
    --
    -- A Parse_Error exception will be raised if the source file cannot be
    -- opened, or a compilation error occurs.
    function Compile_Script( this        : not null access Script_Compiler'Class;
                             filePath    : String;
                             enableDebug : Boolean := False ) return Map_Value;
    pragma Postcondition( Compile_Script'Result.Valid );

    -- Deletes the compiler.
    procedure Delete( this : in out A_Script_Compiler );
    pragma Postcondition( this = null );

private

    use Scribble.Parsers.Scripts;
    use Scribble.Script_Analyzers;
    use Scribble.Script_Generators;

    type Script_Compiler is new Limited_Object with
        record
            runtime   : A_Scribble_Runtime := null;
            parser    : A_Script_Parser := null;
            semantics : A_Script_Analyzer := null;
            generator : A_Script_Generator := null;
        end record;

    procedure Construct( this    : access Script_Compiler;
                         runtime : not null A_Scribble_Runtime );

    procedure Delete( this : in out Script_Compiler );

end Scribble.Script_Compilers;
