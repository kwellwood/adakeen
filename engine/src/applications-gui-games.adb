--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Exceptions;                    use Ada.Exceptions;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Color;                     use Allegro.Color;
with Assets;                            use Assets;
with Assets.Libraries;
with Assets.Scripts;
with Debugging;                         use Debugging;
with Drawing;                           use Drawing;
with Drawing.Bitmaps;                   use Drawing.Bitmaps;
with Events.Manager;                    use Events.Manager;
with Game_Scripting;                    use Game_Scripting;
with GNAT.Directory_Operations;
with GNAT.Directory_Operations.Iteration;
with Palette;                           use Palette;
with Preferences;                       use Preferences;
with Resources;                         use Resources;
with Resources.Images;                  use Resources.Images;
with Script_VMs;                        use Script_VMs;
with Support.Paths;                     use Support.Paths;
with Values.Lists;                      use Values.Lists;

package body Applications.Gui.Games is

    overriding
    procedure Construct( this    : access Game_Application;
                         company : String;
                         name    : String;
                         userDir : String ) is
    begin
        Gui_Application(this.all).Construct( company, name, userDir );
        GNAT.Directory_Operations.Change_Dir( Execution_Directory );
    end Construct;

    ----------------------------------------------------------------------------

    procedure Draw_Loading_Screen( this : not null access Game_Application'Class ) is
        icons    : Allegro_Bitmap_Array(1..4) := (others => null);
        lastIcon : Natural := 0;

        procedure Load_Icon( filename : String ) is
        begin
            icons(lastIcon+1) := Load_Image( filename, "graphics" );
            if icons(lastIcon+1) /= null then
                lastIcon := lastIcon + 1;
            end if;
        end Load_Icon;

        loading : A_Allegro_Bitmap;
    begin
        Al_Acknowledge_Resize( this.display );       -- acknowledge a resize if the window is maximized
        Al_Set_Target_Backbuffer( this.display );    -- set the display active
        Clear_To_Color( Black );

        Load_Icon( To_String( this.name ) & "-16.png" );
        Load_Icon( To_String( this.name ) & "-32.png" );
        Load_Icon( To_String( this.name ) & "-64.png" );
        Load_Icon( To_String( this.name ) & "-128.png" );
        if lastIcon >= icons'First then
            Al_Set_Display_Icons( this.display, icons => icons(icons'First..lastIcon) );
            for i in icons'First..lastIcon loop
                Al_Destroy_Bitmap( icons(i) );
            end loop;
        end if;

        loading := Load_Image( "loading.png", "graphics" );
        if loading /= null then
            Clear_To_Color( Al_Map_RGB( 92, 92, 92 ) );
            Draw_Bitmap( loading,
                         Float(Get_Target_Width - Al_Get_Bitmap_Width( loading )) / 2.0,
                         Float(Get_Target_Height - Al_Get_Bitmap_Height( loading )) / 2.0 );
            Al_Destroy_Bitmap( loading );
        end if;

        Al_Flip_Display;
        Al_Set_Target_Backbuffer( null );            -- release the display
    end Draw_Loading_Screen;

    ----------------------------------------------------------------------------

    function Get_Game( this : not null access Game_Application'Class ) return A_Game is (this.game);

    ----------------------------------------------------------------------------

    function Get_Game_Update_Hz( this : not null access Game_Application'Class ) return Positive is (this.gameUpdateHz);

    ----------------------------------------------------------------------------

    function Get_View( this : not null access Game_Application'Class ) return A_Game_View is (this.view);

    ----------------------------------------------------------------------------

    overriding
    function Initialize( this : access Game_Application ) return Boolean is
        gameVM : A_Script_VM;
    begin
        if not Gui_Application(this.all).Initialize then
            return False;
        end if;
        Enable_Debug_Channels( D_INIT );

        -- helpful check: is the configured media folder empty?
        -- this occurs in a portable distribution or fresh repo clone when there
        -- is no config file.
        declare
            empty : Boolean := True;
            procedure Examine( path : String; index : Positive; quit : in out Boolean ) is
            begin
                empty := False;
                quit := True;
            end Examine;
            procedure Check_Empty is new GNAT.Directory_Operations.Iteration.Wildcard_Iterator( Examine );
        begin
            Check_Empty( Get_Pref( "application", "media" ) );
            if empty then
                Dbg( "Media directory is empty (missing config file?)", D_RES, Error );
            end if;
        end;

        Assets.Libraries.Initialize( this.display );

        -- Display the loading screen

        this.Draw_Loading_Screen;

        -- Initialize Game Systems

        Events.Manager.Global := Create_Event_Manager;

        pragma Debug( Dbg( "Initializing game scripting system...", D_APP, Info ) );
        gameVM := Create_Game_VM;
        Assets.Scripts.Initialize( gameVM.Get_Runtime );

        this.game := Create_Game( this.gameUpdateHz, gameVM, this.session );
        this.session := null;
        gameVM := null;
        this.game.Initialize;

        pragma Debug( Dbg( "App-specific initialization...", D_APP, Info ) );
        A_Game_Application(this).On_Initialize;

        -- Initialize View Systems

        this.view.Initialize( this.display );      -- may raise exception

        Disable_Debug_Channels( D_INIT );
        return True;
    exception
        when e : others =>
            Dbg( "Game_Application.Initialize Exception: ...", D_APP, Error );
            Dbg( e, D_APP, Error );
            Disable_Debug_Channels( D_INIT );

            Delete( gameVM );
            this.Finalize;
            return False;
    end Initialize;

    ----------------------------------------------------------------------------

    overriding
    procedure Finalize( this : access Game_Application ) is
    begin
        if this.view /= null then
            this.view.Finalize;
            Delete( this.view );
        end if;

        Assets.Unload_All( Script_Assets );
        Assets.Scripts.Finalize;

        Delete( this.game );

        Assets.Unload_All( Library_Assets );
        Assets.Libraries.Finalize;
        Assets.Finalize;

        Delete( Events.Manager.Global );

        Gui_Application(this.all).Finalize;
    end Finalize;

    ----------------------------------------------------------------------------

    overriding
    procedure Kill( this : access Game_Application ) is
    begin
        A_Game_Application(this).Stop( errorCode => 2 );
    end Kill;

    ----------------------------------------------------------------------------

    overriding
    function Main( this : access Game_Application ) return Integer is
    begin
        this.game.Start;
        this.view.Run;      -- runs until aborted by Stop()
        this.game.Stop;
        return this.errorCode;
    exception
        when e : others =>
            this.Show_Error( "Application Error: " & Exception_Message( e ) );
            if this.errorCode = 0 then
                this.errorCode := 1;
            end if;
            begin
                this.game.Stop;
            exception
                when others => null;
            end;
            return this.errorCode;
    end Main;

    ----------------------------------------------------------------------------

    procedure Set_Session( this    : not null access Game_Application'Class;
                           session : not null A_Session ) is
    begin
        pragma Assert( this.session = null );
        this.session := session;
    end Set_Session;

    ----------------------------------------------------------------------------

    procedure Set_View( this : not null access Game_Application'Class;
                        view : not null A_Game_View )  is
    begin
        pragma Assert( this.view = null );
        this.view := view;
    end Set_View;

    ----------------------------------------------------------------------------

    procedure Stop( this      : not null access Game_Application'Class;
                    errorCode : Integer := NO_ERROR ) is
    begin
        this.errorCode := errorCode;
        this.view.Stop;
    end Stop;

end Applications.Gui.Games;
