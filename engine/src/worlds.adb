--
-- Copyright (c) 2008-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Exceptions;                    use Ada.Exceptions;
with Ada.Streams.Stream_IO;             use Ada.Streams.Stream_IO;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Ada.Tags;                          use Ada.Tags;
with Component_Families;                use Component_Families;
with Debugging;                         use Debugging;
with Entities.Locations;                use Entities.Locations;
with Games;                             use Games;
with GNAT.OS_Lib;                       use GNAT.OS_Lib;
with Resources;                         use Resources;
with Sessions;                          use Sessions;
with Streams.Buffers;                   use Streams.Buffers;
with Support;                           use Support;
with Support.Paths;                     use Support.Paths;
with Values.Casting;                    use Values.Casting;
with Values.Construction;               use Values.Construction;
with Values.Indirects;
with Values.Streaming;                  use Values.Streaming;
with Values.Strings;                    use Values.Strings;

package body Worlds is

    package World_Refs is new Values.Indirects.Generics( World_Object );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Load_World( stream : access Root_Stream_Type'Class; path : String ) return A_World is
        world   : A_World;
        libName : Unbounded_String;
    begin
        world := A_World'Input( stream );      -- may raise exceptions

        -- load the world's tile library
        libName := Cast_Unbounded_String( world.Get_Property( "library" ) );
        world.lib := Load_Library( To_String( libName ), bitmaps => False );
        if not world.lib.Get.Is_Loaded then
            Delete( world );
            raise FILE_NOT_FOUND with "Unable to open library '" & To_String( libName ) & "'";
        end if;

        -- initialize the map info cache from tile properties
        world.mapInfo.Initialize( world.map, world.lib );

        -- initialize the animation cache from tile properties
        world.anmCache.Initialize( world.map, world.lib );

        world.Set_Property( "filename", Create( Get_Filename( path ) ) );
        world.Set_Property( "filepath", Create( path ) );

        return world;
    end Load_World;

    ----------------------------------------------------------------------------

    function Load_World( name : String ) return A_World is
        filename : constant String := Add_Extension( name, World_Extension );
        resource : A_Resource_File;
        stream   : A_Buffer_Stream;
        this     : A_World;
    begin
        pragma Debug( Dbg( "Loading world: " & filename, D_WORLD, Info ) );
        resource := Load_Resource( filename, "worlds" );
        if resource = null then
            raise FILE_NOT_FOUND with "File not found: " & filename;
        end if;

        begin
            stream := resource.Create_Stream;
            this := Load_World( stream, resource.Get_Path );        -- may raise exceptions
            Close( stream );
        exception
            when others =>
                Close( stream );
                Delete( resource );
                raise;
        end;

        this.Set_Property( "animated", Create( Get_Game /= null and then
                                               Get_Game.Get_Session /= null and then
                                               not Get_Game.Get_Session.Is_Editor ) );

        Delete( resource );
        return this;
    end Load_World;

    ----------------------------------------------------------------------------

    function Create_World( width,
                           height    : Positive;
                           tileWidth : Positive;
                           library   : String ) return A_World is
        this : A_World := null;
        lib  : Library_Ptr;
    begin
        lib := Load_Library( library, bitmaps => False );
        if lib.Get.Is_Loaded then
            this := new World_Object;
            this.Construct( width, height, tileWidth, lib );
        end if;
        return this;
    end Create_World;

    ----------------------------------------------------------------------------

    procedure Add_Layer( this       : not null access World_Object'Class;
                         layer      : Integer;
                         layerType  : Layer_Type;
                         properties : Map_Value := Create_Map.Map;
                         layerData  : A_Map := null ) is
        newLayer  : Integer := layer;
        autoTitle : String_Value;
        layerCopy : A_Map;
    begin
        if this.map.Get_Layer_Count = 0 then
            -- middle layer
            newLayer := 0;
        elsif newLayer < this.map.Get_Foreground_Layer - 1 then
            -- in front of most foreground layer
            newLayer := this.map.Get_Foreground_Layer - 1;
        elsif newLayer > this.map.Get_Background_Layer + 1 then
            -- behind most background layer
            newLayer := this.map.Get_Background_Layer + 1;
        end if;

        -- auto-generate a layer title if none was provided
        autoTitle := Create( (if newLayer <= 0 then "Foreground" else "Background") &
                             (if layerType = Layer_Scenery then " Scenery" else "") ).Str;

        if this.map.Get_Layer_Count < Maps.MAX_LAYERS then
            this.map.Add_Layer( newLayer,
                                layerType,
                                Create_Map( (1=>Pair( PROP_TITLE, autoTitle )   -- auto generate a "title" property
                                            ), consume => True ).Map
                                    .Merge( properties ).Map,                   -- 'properties' overrides it
                                layerData
                              );

            pragma Debug( Dbg( "Added layer " & Trim( newLayer'Img, Left ) & " [" &
                               layerType'Img & "] to map", D_WORLD, Info ) );
            if this.listener /= null then
                if layerData /= null then
                    this.listener.On_Layer_Created( newLayer, layerData );
                else
                    -- 'layerData' can be null to add an empty layer, so a
                    -- temporary copy of the new layer must be made for the
                    -- listener.
                    layerCopy := this.map.Copy_Layer_Data( newLayer );
                    this.listener.On_Layer_Created( newLayer, layerCopy );
                    Delete( layerCopy );
                end if;
            end if;
        end if;
    end Add_Layer;

    ----------------------------------------------------------------------------

    overriding
    procedure Adjust( this : access World_Object ) is
        pragma Unreferenced( this );
    begin
        raise COPY_NOT_ALLOWED;
    end Adjust;

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access World_Object ) is
    begin
        Object(this.all).Construct;
        this.mapInfo := Create_Map_Info_Cache;
        this.anmCache := Create_Animation_Cache;
    end Construct;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this      : access World_Object;
                         width     : Positive;
                         height    : Positive;
                         tileWidth : Positive;
                         lib       : Library_Ptr ) is
    begin
        this.Construct;

        this.lib := lib;
        this.Set_Property( "age",          Create( Long_Float(0.0) ) );
        this.Set_Property( "library",      Create( lib.Get.Get_Name ) );
        this.Set_Property( "filename",     Create( "" ) );
        this.Set_Property( "filepath",     Create( "" ) );
        this.Set_Property( "animated",     Create( Get_Game /= null and then
                                                   Get_Game.Get_Session /= null and then
                                                   not Get_Game.Get_Session.Is_Editor ) );
        this.Set_Property( "ambientLight", Create( "#ffffff" ) );
        this.Set_Property( "preserve",     Create( False ) );
        this.Set_Property( "tileWidth",    Create( tileWidth ) );
        this.tileWidth := tileWidth;

        this.map := Create_Map( width, height );
        this.mapInfo.Initialize( this.map, this.lib );
        this.anmCache.Initialize( this.map, this.lib );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out World_Object ) is
    begin
        Delete( A_Limited_Object(this.anmCache) );
        Delete( this.mapInfo );
        Delete( this.map );

        for e of this.entities.all loop
            Delete( e );
        end loop;
        Delete( this.entities );

        Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Delete_Layer( this : not null access World_Object'Class; layer : Integer ) is
    begin
        pragma Debug( Dbg( "Deleted layer " & Trim( layer'Img, Left ) & " from map", D_WORLD, Info ) );
        if this.map.Delete_Layer( layer ) then
            if this.listener /= null then
                this.listener.On_Layer_Deleted( layer );
            end if;
        end if;
    end Delete_Layer;

    ----------------------------------------------------------------------------

    procedure Add_Entity( this : not null access World_Object'Class; e : in out A_Entity ) is
    begin
        -- if the name isn't unique, re-generate another one
        if e.Get_Name'Length > 0 and then this.Find_Entity_By_Name( e.Get_Name ) /= null then
            this.Generate_Entity_Name( e );
        end if;

        this.entities.Insert( e.Get_Id, e );
        pragma Debug( Dbg( "Added entity " & To_String( e ) & " to world", D_ENTITY or D_WORLD, Info ) );

        if this.listener /= null then
            this.listener.On_Entity_Added( e );
        end if;
        e := null;
    end Add_Entity;

    ----------------------------------------------------------------------------

    function Find_Entity_By_Name( this : not null access World_Object'Class;
                                  name : String ) return A_Entity is
    begin
        if name'Length > 0 then
            for e of this.entities.all loop
                if e.Get_Name = name then
                    return e;
                end if;
            end loop;
        end if;
        return null;
    end Find_Entity_By_Name;

    ----------------------------------------------------------------------------

    procedure Generate_Entity_Name( this : not null access World_Object'Class; e : A_Entity ) is
        newName : Unbounded_String;
        i       : Integer := 0;
    begin
        loop
            i := i + 1;
            newName := To_Unbounded_String( e.Get_Template & Trim( Integer'Image( i ), Left ) );
            exit when this.Find_Entity_By_Name( To_String( newName ) ) = null;
        end loop;
        e.Set_Name( To_String( newName ) );
    end Generate_Entity_Name;

    ----------------------------------------------------------------------------

    function Get_Area( this : not null access World_Object'Class ) return Rectangle is ((0.0, 0.0, Float(this.Get_Width), Float(this.Get_Height)));

    function Get_Background_Layer( this : not null access World_Object'Class ) return Integer is (this.map.Get_Background_Layer);

    function Get_Background_Z( this : not null access World_Object'Class ) return Float is (this.Get_Layer_Z( this.Get_Background_Layer ));

    function Get_Clip_Type( this : not null access World_Object'Class;
                            x, y : Float ) return Clip_Type
        is (this.Get_Tile_Clip_Type( Integer(Float'Floor( x / Float(this.tileWidth) )),
                                     Integer(Float'Floor( y / Float(this.tileWidth) )) ));

    function Get_Entities( this : not null access World_Object'Class ) return A_Entity_Map is (this.entities);

    function Get_Foreground_Layer( this : not null access World_Object'Class ) return Integer is (this.map.Get_Foreground_Layer);

    function Get_Foreground_Z( this : not null access World_Object'Class ) return Float is (this.Get_Layer_Z( this.Get_Foreground_Layer ));

    function Get_Height( this : not null access World_Object'Class ) return Positive is (this.map.Get_Height * this.tileWidth);

    function Get_Layer_Count( this : not null access World_Object'Class ) return Natural is (this.map.Get_Layer_Count);

    function Get_Layer_Z( this  : not null access World_Object'Class;
                          layer : Integer ) return Float
        is (Float(layer) * Maps.LAYER_Z_SPACING);

    function Get_Layer_Property( this  : not null access World_Object'Class;
                                 layer : Integer;
                                 name  : String ) return Value
        is (this.map.Get_Layer_Property( layer, name ));

    function Get_Library( this : not null access World_Object'Class ) return Library_Ptr is (this.lib);

    function Get_Map_Columns( this : not null access World_Object'Class ) return Positive is (this.map.Get_Width);

    function Get_Map_Rows( this : not null access World_Object'Class ) return Positive is (this.map.Get_Height);

    ----------------------------------------------------------------------------

    overriding
    function Get_Namespace_Name( this : access World_Object;
                                 name : String ) return Value is (this.Get_Property( name ));

    ----------------------------------------------------------------------------

    overriding
    function Get_Namespace_Ref( this : access World_Object;
                                name : String ) return Value
        is (World_Refs.Create_Indirect( this, Get_Property'Access, Set_Property'Access, name ));

    ----------------------------------------------------------------------------

    overriding
    function Get_Process_Name( this : access World_Object ) return String
    is (
        "World('" & (
        if Cast_String( this.Get_Property( "filepath" ) )'Length > 0 then
            Cast_String( this.Get_Property( "filepath" ) )
        else
            "Untitled World"
        ) & "')"

    );

    ----------------------------------------------------------------------------

    function Get_Property( this : not null access World_Object'Class;
                           name : String ) return Value is
        val : constant Value := this.properties.Get( name );
    begin
        if val.Is_Null then
            Dbg( "World property '" & name & "' not found", D_WORLD or D_SCRIPT, Error );
        end if;
        return val;
    end Get_Property;

    ----------------------------------------------------------------------------

    function Get_Tile( this     : not null access World_Object'Class;
                       layer    : Integer;
                       col, row : Integer ) return A_Tile
        is (this.lib.Get.Get_Tile( this.Get_Tile_Id( layer, col, row ) ));

    ----------------------------------------------------------------------------

    function Get_Tile( this  : not null access World_Object'Class;
                       layer : Integer;
                       x, y  : Float ) return A_Tile
        is (this.Get_Tile( layer,
                           Integer(Float'Floor( x / Float(this.tileWidth) )),
                           Integer(Float'Floor( y / Float(this.tileWidth) )) ));

    ----------------------------------------------------------------------------

    function Get_Tile_Attribute( this     : not null access World_Object'Class;
                                 col, row : Integer;
                                 name     : String ) return Value is
        val : Value;
    begin
        for l in this.map.Get_Foreground_Layer..this.map.Get_Background_Layer loop
            val := this.Get_Tile( l, col, row ).Get_Attribute( name );
            if not val.Is_Null then
                return val;
            end if;
        end loop;
        return Null_Value;
    end Get_Tile_Attribute;

    ----------------------------------------------------------------------------

    function Get_Tile_Attribute( this : not null access World_Object'Class;
                                 x, y : Float;
                                 name : String ) return Value
        is (this.Get_Tile_Attribute( Integer(Float'Floor( x / Float(this.tileWidth) )),
                                     Integer(Float'Floor( y / Float(this.tileWidth) )),
                                     name ));

    ----------------------------------------------------------------------------

    function Get_Tile_Clip_Type( this     : not null access World_Object'Class;
                                 col, row : Integer ) return Clip_Type
        is (this.mapInfo.Get_Shape( col, row ));

    ----------------------------------------------------------------------------

    function Get_Tile_Clip_Type( this : not null access World_Object'Class;
                                 x, y : Float ) return Clip_Type
        is (this.Get_Tile_Clip_Type( Integer(Float'Floor( x / Float(this.tileWidth) )),
                                     Integer(Float'Floor( y / Float(this.tileWidth) )) ));

    ----------------------------------------------------------------------------

    function Get_Tile_Id( this     : not null access World_Object'Class;
                          layer    : Integer;
                          col, row : Integer ) return Natural is
        tileId : Natural;
    begin
        if this.anmCache.Get_Animated_Tile( layer, col, row, tileId ) then
            return tileId;
        end if;
        return this.map.Get_Tile_Id( layer, col, row );
    end Get_Tile_Id;

    ----------------------------------------------------------------------------

    function Get_Tile_Id( this  : not null access World_Object'Class;
                          layer : Integer;
                          x, y  : Float ) return Natural
        is (this.Get_Tile_Id( layer,
                              Integer(Float'Floor( x / Float(this.tileWidth) )),
                              Integer(Float'Floor( y / Float(this.tileWidth) )) ));

    ----------------------------------------------------------------------------

    function Get_Tile_Width( this : not null access World_Object'Class ) return Positive is (this.tileWidth);

    ----------------------------------------------------------------------------

    function Get_Width( this : not null access World_Object'Class ) return Positive is (this.map.Get_Width * this.tileWidth);

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out World_Object ) is
        count : Natural;
    begin
        obj.properties := Value_Input( stream ).Map;
        obj.map := A_Map'Input( stream );
        obj.tileWidth := obj.properties.Get_Int( "tileWidth", 1 );
        obj.anmCache.Read( stream );
        obj.age := Time_Span'Input( stream );
        obj.properties.Set( "age", Create( Long_Float(To_Duration( obj.age )) * 1000.0 ) );

        declare
            entity : A_Entity;
        begin
            count := Natural'Input( stream );
            for i in 1..count loop
                entity := A_Entity'Input( stream );
                pragma Assert( entity /= null, "Null pointer in world file" );
                obj.entities.Insert( entity.Get_Id, entity );
            end loop;
        end;
    end Object_Read;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : World_Object ) is
        properties : Map_Value;
    begin
        properties := Clone( obj.properties ).Map;
        properties.Set( "tileWidth", Create( obj.tileWidth ) );        -- overwrite any changes
        properties.Set( "library", Create( obj.lib.Get.Get_Name ) );   -- overwrite any changes
        properties.Remove( "filename" );                               -- not persisted
        properties.Remove( "filepath" );                               -- not persisted
        properties.Remove( "animated" );                               -- not persisted
        properties.Remove( "age" );                                    -- persisted separately
        Value_Output( stream, properties );

        A_Map'Output( stream, obj.map );
        obj.anmCache.Write( stream );
        Time_Span'Output( stream, obj.age );

        Natural'Output( stream, Natural(obj.entities.Length) );
        for e of obj.entities.all loop
            A_Entity'Output( stream, e );
        end loop;
    end Object_Write;

    ----------------------------------------------------------------------------

    procedure Resize( this          : not null access World_Object'Class;
                      col, row      : Integer;
                      width, height : Positive ) is

        ------------------------------------------------------------------------

        procedure Bound_Entities is
            loc : A_Location;
        begin
            for e of this.entities.all loop
                loc := A_Location(e.Get_Component( LOCATION_ID ));
                if loc /= null and then
                   (loc.Get_X > Float(this.Get_Width) or else
                    loc.Get_Y > Float(this.Get_Height))
                then
                    -- move the entity back inside the map boundaries
                    loc.Set_XY( Float'Min( loc.Get_X, Float(this.Get_Width) ),
                                Float'Min( loc.Get_Y, Float(this.Get_Height) ) );
                end if;
            end loop;
        end Bound_Entities;

        ------------------------------------------------------------------------

        newMap : A_Map;
    begin
        if width /= this.map.Get_Width or else height /= this.map.Get_Height then
            newMap := this.map.Resize( col, row, width, height );
            if newMap = null then
                return;
            end if;
            Delete( this.map );
            this.map := newMap;
            this.mapInfo.Initialize( this.map, this.lib );
            this.anmCache.Resize( this.map, col, row );
            Bound_Entities;
            if this.listener /= null then
                this.listener.On_World_Resized;
            end if;
        end if;
    end Resize;

    ----------------------------------------------------------------------------

    procedure Save( this      : not null access World_Object'Class;
                    path      : String;
                    overwrite : Boolean ) is
        filename : constant String := Add_Extension( path, World_Extension, force => True );
        file     : Ada.Streams.Stream_IO.File_Type;
        sa       : Stream_Access;
    begin
        if Is_Path_In_Archive( filename ) then
            raise WRITE_EXCEPTION with "Cannor write into an archive";
        elsif Is_Regular_File( filename ) and then not overwrite then
            raise WRITE_EXCEPTION with "File already exists";
        end if;

        Create( file, Out_File, filename );    -- raises exception on error
        sa := Stream( file );
        A_World'Output( sa, A_World(this) );
        pragma Debug( Dbg( "Wrote world to disk at: " &
                           Ada.Streams.Stream_IO.Name( file ), D_RES or D_WORLD, Info ) );
        Close( file );

        this.Set_Property( "filename", Create( Get_Filename( filename ) ) );
        this.Set_Property( "filepath", Create( filename ) );
    exception
        when WRITE_EXCEPTION =>
            -- file is not allowed to be overwritten
            raise;
        when Name_Error =>
            raise WRITE_EXCEPTION with "Bad filename: " & filename;
        when e : others =>
            raise WRITE_EXCEPTION with "Error writing " & filename & ": " &
                                       Exception_Name( e ) & " - " &
                                       Exception_Message( e );
    end Save;

    ----------------------------------------------------------------------------

    procedure Set_Layer_Property( this  : not null access World_Object'Class;
                                  layer : Integer;
                                  name  : String;
                                  val   : Value'Class ) is
    begin
        this.map.Set_Layer_Property( layer, name, val );    -- copies 'val'
        if this.listener /= null then
            this.listener.On_Layer_Property_Changed( layer, name, val );
        end if;
    end Set_Layer_Property;

    ----------------------------------------------------------------------------

    procedure Set_Listener( this     : not null access World_Object'Class;
                            listener : A_World_Listener ) is
    begin
        if listener /= this.listener then
            this.listener := listener;
            if this.listener /= null then
                this.listener.Initialize_World;
            end if;
        end if;
    end Set_Listener;

    ----------------------------------------------------------------------------

    procedure Set_Property( this : not null access World_Object'Class;
                            name : String;
                            val  : Value'Class ) is
    begin
        this.properties.Set( name, val );                   -- copies 'val'
        if this.listener /= null then
            this.listener.On_World_Property_Changed( name, val );
        end if;
    end Set_Property;

    ----------------------------------------------------------------------------

    procedure Set_Tile( this     : not null access World_Object'Class;
                        layer    : Integer;
                        col, row : Integer;
                        id       : Natural ) is
        tiles : Tile_Change_Lists.Vector;
    begin
        tiles.Append( Tile_Change'(layer, col, row, id) );
        this.Set_Tiles( tiles );
    end Set_Tile;

    ----------------------------------------------------------------------------

    procedure Set_Tile( this  : not null access World_Object'Class;
                        layer : Integer;
                        x, y  : Float;
                        id    : Natural ) is
    begin
        this.Set_Tile( layer,
                       Integer(Float'Floor( x / Float(this.tileWidth) )),
                       Integer(Float'Floor( y / Float(this.tileWidth) )), id );
    end Set_Tile;

    ----------------------------------------------------------------------------

    procedure Set_Tiles( this  : not null access World_Object'Class;
                         tiles : Tile_Change_Lists.Vector ) is

        ------------------------------------------------------------------------

        -- Updates the tile at 'col','row' in map layer 'layer' to tile id 'id',
        -- returning True on success or False if the location is not valid or the
        -- location already matches the id. No notification events are queued.
        function Set_Tile( tc : Tile_Change ) return Boolean is
        begin
            if (tc.col   < 0                             or tc.col   >= this.map.Get_Width           ) or else
               (tc.row   < 0                             or tc.row   >= this.map.Get_Height          ) or else
               (tc.layer < this.map.Get_Foreground_Layer or tc.layer >  this.map.Get_Background_Layer) or else
               tc.id = this.map.Get_Tile_Id( tc.layer, tc.col, tc.row )
            then
                return False;
            end if;

            this.map.Set_Tile( tc.layer, tc.col, tc.row, tc.id );
            return True;
        end Set_Tile;

        ------------------------------------------------------------------------

        changes : Tile_Change_Lists.Vector;
    begin
        changes.Reserve_Capacity( tiles.Capacity );
        for t of tiles loop
            if Set_Tile( t ) then
                changes.Append( t );
            end if;
        end loop;
        if this.listener /= null and not changes.Is_Empty then
            this.listener.On_Tiles_Changed( tiles );
        end if;
    end Set_Tiles;

    ----------------------------------------------------------------------------

    procedure Swap_Layers( this       : not null access World_Object'Class;
                           layer      : Integer;
                           otherLayer : Integer ) is
    begin
        if this.map.Swap_Layers( layer, otherLayer ) then
            if this.listener /= null then
                this.listener.On_Layers_Swapped( layer, otherLayer );
            end if;
        end if;
    end Swap_Layers;

    ----------------------------------------------------------------------------

    overriding
    procedure Tick( this : access World_Object; time : Tick_Time ) is
    begin
        -- increment the age of the world since create (gameplay time only)
        this.age := this.age + time.elapsed;
        this.Set_Property( "age", Create( Long_Float(To_Duration( this.age )) * 1000.0 ) );

        if this.properties.Get_Boolean( "animated" ) then
            this.anmCache.Tick( time );
        end if;
    end Tick;

    ----------------------------------------------------------------------------

    function Tile_Width( this : not null access World_Object'Class ) return Positive is (this.tileWidth);

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_World ) is
    begin
        Delete( A_Object(this) );
    end Delete;

    ----------------------------------------------------------------------------

    function A_World_Input( stream : access Root_Stream_Type'Class ) return A_World is
        world : A_World := null;
        recg  : constant String := IDENTIFIER & ":" & World_Extension;
        recg2 : String(1..recg'Length);
        ver   : Integer;
    begin
        String'Read( stream, recg2 );
        if recg2 = recg then
            ver := Integer'Input( stream );
            if ver = READ_VERSION then
                world := new World_Object;
                world.Construct;
                World_Object'Read( stream, world.all );
            else
                raise READ_EXCEPTION with "Unsupported file version " & Image( ver ) &
                                          "; current version is " & Image( READ_VERSION );
            end if;
        else
            raise READ_EXCEPTION with "Unrecognized file format";
        end if;
        return world;
    exception
        when e : Tag_Error =>
            declare
                msg : constant String := Exception_Message( e );
            begin
                raise READ_EXCEPTION with "Unknown world class " &
                    "<" & msg((Index( msg, ".", Backward ) + 1)..msg'Last) & ">";
            end;
        when others =>
            Delete( world );
            raise;
    end A_World_Input;

    ----------------------------------------------------------------------------

    procedure A_World_Output( stream : access Root_Stream_Type'Class; world : A_World ) is
    begin
        String'Write( stream, IDENTIFIER & ':' & World_Extension );
        Integer'Output( stream, WRITE_VERSION );
        World_Object'Write( stream, world.all );
    end A_World_Output;

end Worlds;
