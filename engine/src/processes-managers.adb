--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Exceptions;                    use Ada.Exceptions;
with Applications;                      use Applications;
with Debugging;                         use Debugging;
with Events.Manager;                    use Events.Manager;
with Support;                           use Support;
with Support.Real_Time;                 use Support.Real_Time;

package body Processes.Managers is

    -- Appends a process to the execution list.
    type Attach_Operation is new Operation with null record;

    procedure Execute( this     : access Attach_Operation;
                       now      : Time;
                       execList : in out Execution_Lists.List );

    function Create_Attach_Operation( process : not null A_Process ) return A_Operation
    is (new Attach_Operation'(process => process));

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Removes a process from the execution list.
    type Detach_Operation is new Operation with
        record
            destroy : A_Destructor := null;
        end record;

    procedure Execute( this     : access Detach_Operation;
                       now      : Time;
                       execList : in out Execution_Lists.List );

    -- If 'destructor' is not null, it will be used to destroy the process after
    -- it has been detached.
    function Create_Detach_Operation( process    : not null A_Process;
                                      destructor : A_Destructor ) return A_Operation
    is (new Detach_Operation'(process => process, destroy => destructor));

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Pauses the execution of a process before its next Tick.
    type Pause_Operation is new Operation with null record;

    procedure Execute( this     : access Pause_Operation;
                       now      : Time;
                       execList : in out Execution_Lists.List );

    function Create_Pause_Operation( process : not null A_Process ) return A_Operation
    is (new Pause_Operation'(process => process));

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Resumes the execution of a process on its next Tick.
    type Resume_Operation is new Operation with null record;

    procedure Execute( this     : access Resume_Operation;
                       now      : Time;
                       execList : in out Execution_Lists.List );

    function Create_Resume_Operation( process : not null A_Process ) return A_Operation
    is (new Resume_Operation'(process => process));

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Process_Manager_Fixed_Step( name         : String;
                                                targetRateHz : Positive;
                                                eventChannel : Event_Channel := NO_CHANNEL
                                              ) return A_Process_Manager is
        this : constant A_Process_Manager := new Process_Manager;
    begin
        this.Construct( name, targetRateHz, True, eventChannel );
        return this;
    end Create_Process_Manager_Fixed_Step;

    ----------------------------------------------------------------------------

    function Create_Process_Manager_Real_Time( name         : String;
                                               maxRateHz    : Natural;
                                               eventChannel : Event_Channel := NO_CHANNEL
                                             ) return A_Process_Manager is
        this : constant A_Process_Manager := new Process_Manager;
    begin
        this.Construct( name, maxRateHz, False, eventChannel );
        return this;
    end Create_Process_Manager_Real_Time;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this         : access Process_Manager;
                         name         : String;
                         hertz        : Natural;
                         fixedStep    : Boolean;
                         eventChannel : Event_Channel ) is
    begin
        Limited_Object(this.all).Construct;
        this.name := To_Unbounded_String( name );
        this.eventChannel := eventChannel;

        pragma Assert( Events.Manager.Global /= null, "Event manager is required" );
        this.corral := Create_Corral( Events.Manager.Global, name );

        pragma Assert( hertz > 0 or else not fixedStep );
        this.targetHz := hertz;
        this.fixedStep := fixedStep;

        this.Attach_Async( this.corral );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Process_Manager ) is
    begin
        this.Stop;
        for op of this.opQueue loop
            Delete( op );
        end loop;
        this.opQueue.Clear;
        Delete( this.corral );
        Delete( this.ticker );
        Limited_Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Attach_Async( this : not null access Process_Manager'Class;
                            proc : not null access Process'Class ) is
        use Operation_Lists;

        op     : A_Operation;
        opCrsr : Operation_Lists.Cursor;
    begin
        this.lock.Lock;
        if not this.stopped then
            -- before appending the Attach operation to the queue, check if a
            -- Detach operation for 'proc' is already queued. The two operations
            -- will cancel each other out.
            opCrsr := this.opQueue.Last;
            while Has_Element( opCrsr ) loop
                op := Element( opCrsr );
                if op.process = proc and then op.all in Detach_Operation'Class then
                    -- found a Detach operation on the queue for the same
                    -- process. Cancel it out. If a destructor was given for the
                    -- Detach then something is wrong because the caller
                    -- shouldn't still have a reference.
                    pragma Assert( Detach_Operation( op.all ).destroy = null,
                                   "Unexpected destructor given" );
                    pragma Debug( Dbg( "Attaching process <" & proc.Get_Process_Name &
                                       "> to manager <" & this.Get_Name & "> " &
                                       "cancelled a pending detach operation",
                                       D_PROCS, Info ) );
                    this.opQueue.Delete( opCrsr );       -- remove the Detach
                    Delete( op );                        -- delete the Detach
                    this.lock.Unlock;
                    return;               -- done; cancelled out a queued Detach
                else
                    Previous( opCrsr );
                end if;
            end loop;

            pragma Debug( Dbg( "Attaching process <" & proc.Get_Process_Name &
                                "> to manager <" & this.Get_Name & "> is queued",
                                D_PROCS, Info ) );
            this.opQueue.Append( Create_Attach_Operation( proc ) );
            this.lock.Unlock;
        else
            this.lock.Unlock;
            Delete( op );
            raise Constraint_Error with
                "Error: Attempted to attach process <" &
                proc.Get_Process_Name & "> to manager <" & this.Get_Name &
                "> after Stop";
        end if;
    end Attach_Async;

    ----------------------------------------------------------------------------

    procedure Detach_Async( this : not null access Process_Manager'Class;
                            proc : not null access Process'Class ) is
        proc2 : A_Process := proc;
    begin
        this.Detach_Async( proc2, null );
    end Detach_Async;

    ----------------------------------------------------------------------------

    procedure Detach_Async( this       : not null access Process_Manager'Class;
                            proc       : in out A_Process;
                            destructor : A_Destructor ) is
        use Operation_Lists;

        op      : A_Operation;
        opCrsr1 : Operation_Lists.Cursor;
        opCrsr2 : Operation_Lists.Cursor;
    begin
        this.lock.Lock;
        if not this.stopped then
            -- before appending the Detach operation to the queue, check if an
            -- Attach operation for 'proc' is already queued. The two operations,
            -- and any pause/resume inbetween, will all cancel out.
            opCrsr1 := this.opQueue.Last;
            while Has_Element( opCrsr1 ) loop
                op := Element( opCrsr1 );
                if op.process = proc and then op.all in Attach_Operation'Class then
                    -- found an Attach operation on the queue for the same
                    -- process. Cancel it out.
                    pragma Debug( Dbg( "Detaching process <" & proc.Get_Process_Name &
                                       "> from manager <" & this.Get_Name & "> " &
                                       "cancelled a pending attach operation",
                                       D_PROCS, Info ) );
                    opCrsr2 := Next( opCrsr1 );
                    this.opQueue.Delete( opCrsr1 );       -- remove the Attach
                    Delete( op );                         -- delete the Attach

                    -- Check forward and delete any Pause/Resume operation, for
                    -- the same proc, that might follow it.
                    while Has_Element( opCrsr2 ) loop
                        op := Element( opCrsr2 );
                        if op.process = proc then
                            -- if assert fails, was a new Operation class added?
                            pragma Assert( op.all in Pause_Operation or
                                           op.all in Resume_Operation,
                                           "Unknown Operation class" );
                            opCrsr1 := opCrsr2;
                            Next( opCrsr2 );
                            this.opQueue.Delete( opCrsr1 );     -- remove the Pause/Resume
                            Delete( op );                       -- delete the Pause/Resume
                        else
                            Next( opCrsr2 );
                        end if;
                    end loop;

                    this.lock.Unlock;

                    begin
                        if destructor /= null then
                            destructor.all( proc );    -- delete the Process
                        end if;
                    exception
                        when e : others =>
                            Dbg( "Exception destroying process on detach: " &
                                 Exception_Name( e ) & ": " & Exception_Message( e ),
                                 D_PROCS, Error );
                    end;
                    proc := null;
                    return;       -- done; cancelled out a queued Attach
                end if;
                Previous( opCrsr1 );
            end loop;

            -- queue the Detach operation
            pragma Debug( Dbg( "Detaching process <" & proc.Get_Process_Name &
                               "> from manager <" & this.Get_Name & "> " &
                               (if destructor /= null then "(with destructor)" else "") &
                               " is queued", D_PROCS, Info ) );
            this.opQueue.Append( Create_Detach_Operation( proc, destructor ) );
            this.lock.Unlock;
            proc := null;
        else
            this.lock.Unlock;

            -- no need to detach processes after Stop
            begin
                if destructor /= null then
                    destructor.all( proc );
                end if;
            exception
                when e : others =>
                    Dbg( "Exception destroying process on detach: " &
                         Exception_Name( e ) & ": " & Exception_Message( e ),
                         D_PROCS, Error );
            end;
            proc := null;
        end if;
    end Detach_Async;

    ----------------------------------------------------------------------------

    function FrameStarted( this : not null access Process_Manager'Class ) return access Signal'Class is (this.sigFrameStarted'Access);

    ----------------------------------------------------------------------------

    function Get_Corral( this : not null access Process_Manager'Class ) return A_Corral is (this.corral);

    ----------------------------------------------------------------------------

    function Get_Frame_Rate( this : not null access Process_Manager'Class ) return Float is
        elapsed : Time_Span;
        result  : Float;
    begin
        this.lock.Lock;

        elapsed := Clock - this.frameStart;
        if elapsed < Seconds( 1 ) then
            result := this.rate;
        else
            -- if the thread has not updated the rate in more than a second,
            -- then it has frozen and the last calculated rate is no longer
            -- valid. calculate a frame rate (<1 FPS) based on how long it has
            -- been since the last frame.
            result := 1.0 / Float(To_Duration( elapsed ));
        end if;

        this.lock.Unlock;
        return result;
    end Get_Frame_Rate;

    ----------------------------------------------------------------------------

    function Get_Frame_Time( this : not null access Process_Manager'Class ) return Time_Span is
    begin
        this.lock.Lock;
        return result : constant Time_Span := this.frameTime do
            this.lock.Unlock;
        end return;
    end Get_Frame_Time;

    ----------------------------------------------------------------------------

    function Get_Name( this : not null access Process_Manager'Class ) return String is (To_String( this.name ));

    ----------------------------------------------------------------------------

    function Get_Target_Rate( this : not null access Process_Manager'Class ) return Natural is (this.targetHz);

    ----------------------------------------------------------------------------

    function Is_Stopped( this : not null access Process_Manager'Class ) return Boolean is
    begin
        this.lock.Lock;
        return result : constant Boolean := this.stopped do
            this.lock.Unlock;
        end return;
    end Is_Stopped;

    ----------------------------------------------------------------------------

    procedure Pause_Async( this   : not null access Process_Manager'Class;
                           proc   : not null access Process'Class;
                           paused : Boolean )  is
        use Operation_Lists;

        op     : A_Operation;
        opCrsr : Operation_Lists.Cursor;
    begin
        this.lock.Lock;
        if not this.stopped then
            -- before appending the Pause/Resume operation to the queue, check
            -- if an opposite operation for 'proc' is already queued. The two
            -- operations will cancel each other out.
            opCrsr := this.opQueue.Last;
            while Has_Element( opCrsr ) loop
                op := Element( opCrsr );
                if op.process = proc and then
                   ((paused and then op.all in Resume_Operation'Class) or else
                    (not paused and then op.all in Pause_Operation'Class))
                then
                    -- Cancel it out
                    pragma Debug( paused,
                                  Dbg( "Resuming process <" & proc.Get_Process_Name &
                                       "> in manager <" & this.Get_Name & "> " &
                                       "cancelled a pending pause operation",
                                       D_PROCS, Info ) );
                    pragma Debug( not paused,
                                  Dbg( "Resuming process <" & proc.Get_Process_Name &
                                       "> in manager <" & this.Get_Name & "> " &
                                       "cancelled a pending pause operation",
                                       D_PROCS, Info ) );
                    this.opQueue.Delete( opCrsr );        -- remove the Detach
                    Delete( op );                         -- delete the Detach
                    this.lock.Unlock;
                    return;           -- done; cancelled out a queued Paused/Resume
                else
                    Previous( opCrsr );
                end if;
            end loop;

            pragma Debug( Dbg( (if paused then "Pausing" else "Resuming") &
                               " process <" & proc.Get_Process_Name &
                               "> in manager <" & this.Get_Name & "> is queued",
                               D_PROCS, Info ) );
            this.opQueue.Append( (if paused then Create_Pause_Operation( proc )
                                  else Create_Resume_Operation( proc )) );
            this.lock.Unlock;
        else
            this.lock.Unlock;
            Delete( op );
            raise Constraint_Error with
                "Error: Attempted to pause/resume process " &
                proc.Get_Process_Name & " in manager " & this.Get_Name &
                " after Stop";
        end if;
    end Pause_Async;

    ----------------------------------------------------------------------------

    procedure Run( this : not null access Process_Manager'Class ) is
        tickDelta : Time_Span := Time_Span_Zero;

        -- indicates a fatal exception occured in one of the processes and
        -- Terminate_Application() was called. Ticker_Task won't terminate, but
        -- it will stop ticking processes until Process_Manager.Stop is called
        -- by the application as part a graceful shutdown.
        fatalError : Boolean := False;

        ------------------------------------------------------------------------

#if DEBUG then
        nextUsageUpdate : Time := Clock;
        usageTotal      : Natural := 0;
        usageCount      : Natural := 0;

        -- Track the CPU time used by the process manager. The percentage used
        -- can be calculated by checking the difference between the requested
        -- time between frames and the amount of time remaining for sleep after
        -- ticking all processes in the frame. This code is debugging only so it
        -- doesn't slow down the release version. The average CPU use each
        -- second is sent to debugging output and can be seen with INFO level
        -- output for the PROCS system.
        procedure Update_CPU_Usage( frameStart : Time; tickDelta : Time_Span ) is
            now       : constant Time := Clock;
            remaining : Time_Span;
            usageNow  : Integer := 0;
        begin
            if now >= frameStart + tickDelta then
                usageNow := 100;
            else
                remaining := (frameStart + tickDelta) - now;
                usageNow := 100 - Integer(Float(100.0) * Float(To_Duration( remaining ) / To_Duration( tickDelta )));
                if usageNow < 0 then
                    usageNow := 0;
                end if;
            end if;
            usageTotal := usageTotal + usageNow;
            usageCount := usageCount + 1;

            if now >= nextUsageUpdate then
                Dbg( "Process manager <" & this.Get_Name & ">: " &
                     Image( this.rate ) & "fps (" &
                     Image( usageTotal / usageCount ) & "% load)",
                     D_PROCS, Info );
                usageTotal := 0;
                usageCount := 0;
                nextUsageUpdate := now + Seconds( 1 );
            end if;
        end Update_CPU_Usage;

        ------------------------------------------------------------------------

#else
        procedure Update_CPU_Usage( frameStart : Time; tickDelta : Time_Span ) is null;
#end if;

        ------------------------------------------------------------------------

        procedure Execute_Pending_Ops( execList : in out Execution_Lists.List;
                                       now      : Time ) is
        begin
            this.lock.Lock;
            for op of this.opQueue loop
                op.Execute( now, execList );
            end loop;
            this.opQueue.Clear;
            this.lock.Unlock;
        end Execute_Pending_Ops;

        ------------------------------------------------------------------------

        procedure Tick_Processes( execList  : in out Execution_Lists.List;
                                  now       : Time;
                                  tickDelta : Time_Span ) is

            procedure Tick_Process( exec : not null A_Execution ) is
            begin
                exec.process.Tick( Tick_Time'(total => now - exec.firstTick, elapsed => tickDelta) );
                exec.lastTick := now;
            exception
                when e : others =>
                    -- the process threw an unhandled exception. terminate the
                    -- application.
                    Dbg( "Exception in process <" & exec.process.Get_Process_Name &
                         "> of process manager <" & this.Get_Name & ">... ",
                         D_PROCS, Error );
                    Dbg( e, D_PROCS, Error );
                    Terminate_Application( "Exception in process <" &
                                           exec.process.Get_Process_Name & "> of " &
                                           "process manager <" & this.Get_Name & ">: " &
                                           Exception_Message( e ) );
                    fatalError := True;
            end Tick_Process;

            crsr  : Execution_Lists.Cursor;
            crsr2 : Execution_Lists.Cursor;
            exec  : A_Execution;
        begin
            -- execute any pending process management operations queued
            -- since the previous frame
            Execute_Pending_Ops( execList, now );

            -- execute each of the running processes
            crsr := execList.First;
            while Has_Element( crsr ) and not fatalError loop
                exec := Execution_Lists.Element( crsr );

                -- check if the process was previously flagged for detach
                if not exec.detachMe then
                    -- tick the process
                    if not exec.paused then
                        Tick_Process( exec );
                    end if;

                    -- execute any pending process management operations
                    -- this may cause any processes to be flagged for
                    -- detachment on their next tick.
                    Execute_Pending_Ops( execList, now );

                    -- move to the next process
                    Next( crsr );
                else
                    -- detach the process
                    crsr2 := Next( crsr );
                    execList.Delete( crsr );
                    crsr := crsr2;

                    -- delete it if necessary
                    if exec.destructor /= null then
                        exec.destructor( exec.process );
                    end if;
                    Delete( exec );
                end if;
            end loop;
        end Tick_Processes;

        ------------------------------------------------------------------------

        execList   : aliased Execution_Lists.List;
        frameTimer : aliased Frame_Timer;

        simStart      : Time;        -- start of the frame in simulation time
        realStart     : Time;        -- start of the frame in real time
        realWorkStart : Time;        -- start of the frame's work in real time (excludes overhead)
        realDelta     : Time_Span;   -- real time elapsed since previous frame
    begin
        -- run the process manager only once and only in one thread
        this.lock.Lock;
        if this.stopped or else (this.started and then this.ticker = null) then
            this.lock.Unlock;
            return;
        end if;
        this.started := True;
        this.lock.Unlock;

        pragma Debug( Dbg( "Process manager <" & this.Get_Name & "> starting", D_PROCS, Info ) );
        this.taskId := Current_Task;

        if this.targetHz > 0 then
            tickDelta := Seconds( 1 ) / this.targetHz;
        end if;

        frameTimer.Start_Frame;
        realStart := Clock;
        simStart := realStart;
        while not this.Is_Stopped loop
            realDelta := Clock - realStart;
            realStart := realStart + realDelta;
            frameTimer.Next_Frame;

            -- record previous frame duration and current frame start time
            this.lock.Lock;
            this.frameTime := realDelta;
            this.rate := frameTimer.Get_Frame_Rate;
            this.frameStart := Clock;
            this.lock.Unlock;

            this.sigFrameStarted.Emit;

            -- record the real time start of the frame's actual work so we can
            -- calculate CPU usage without bookkeeping and frame sync overhead.
            realWorkStart := Clock;

            -- execute all our processes (one frame)
            Tick_Processes( execList,
                            (if this.fixedStep then simStart else realStart),
                            (if this.fixedStep then tickDelta else realDelta) );

            -- track CPU usage (determined by now much of tickDelta we use)
            Update_CPU_Usage( realWorkStart, tickDelta );

            -- notify the event manager that all the events in the thread's
            -- event channel have been queued for the previous frame, allowing
            -- the event corrals to dispatch them.
            Events.Manager.Global.End_Frame( this.eventChannel );

            -- signal the end of frame semaphore for a synchronization peer that
            -- might be waiting to consume the events from this frame.
            this.frameEnd.Set;

            -- delay until the next frame time
            -- if this is a fixed rate update then this will not delay when
            -- simulation time is lagging behind real time; it will try to
            -- catch up. if this is variable rate then every frame will be
            -- as close to 'tickDelta' as possible and never shorter.
            delay until (if this.fixedStep then simStart else realStart) + tickDelta;

            simStart := simStart + tickDelta;
        end loop;
        pragma Debug( Dbg( "Process manager <" & this.Get_Name & "> stopping", D_PROCS, Info ) );

        -- Execute any remaining queued operations. This is necessary because
        -- they may flag certain Processes for detachment with a destructor,
        -- leaving us reponsible for deleting those Processes.
        Execute_Pending_Ops( execList, (if this.fixedStep then simStart else realStart) );

        -- Delete any processes flagged for detachment with a destructor
        for exec of execList loop
            if exec.detachMe and then exec.destructor /= null then
                exec.destructor( exec.process );
            end if;
        end loop;
        execList.Clear;

        pragma Debug( Dbg( "Process manager <" & this.Get_Name & "> complete", D_PROCS, Info ) );
    exception
        when e : others =>
            Dbg( "Process manager <" & this.Get_Name & "> Ticker_Task Exception: ...", D_PROCS, Error );
            Dbg( e, D_PROCS, Error );
            Terminate_Application( "Unexpected exception" );
    end Run;

    ----------------------------------------------------------------------------

    procedure Start( this : not null access Process_Manager'Class ) is
    begin
        this.lock.Lock;
        if not this.started and not this.stopped and this.ticker = null then
            this.started := True;
            this.ticker := new Ticker_Task;
            this.ticker.Init( A_Process_Manager(this) );
        end if;
        this.lock.Unlock;
    end Start;

    ----------------------------------------------------------------------------

    procedure Stop( this    : not null access Process_Manager'Class;
                    timeout : Duration := 1.0 ) is
    begin
        this.lock.Lock;
        if this.started and not this.stopped then
            this.stopped := True;

            -- the process manager's task needs the lock, so we must release it
            -- before attempting to rendezvous, to prevent a deadlock.
            this.lock.Unlock;

            -- wait for the thread running the process manager
            if Current_Task /= this.taskId then
                for count in 1..Integer(timeout * 10.0) loop
                    if Is_Terminated( this.taskId ) then
                        exit;
                    end if;
                    delay 0.1;
                end loop;
                if not Is_Terminated( this.taskId ) and then timeout >= 0.1 then
                    Dbg( "Process manager <" & this.Get_Name &
                         "> Ticker_Task failed to stop", D_PROCS, Error );
                end if;
            end if;
        else
            this.lock.Unlock;
        end if;
    end Stop;

    ----------------------------------------------------------------------------

    procedure Wait_Frame_End( this    : not null access Process_Manager'Class;
                              timeout : Time_Span ) is
    begin
        if timeout > Time_Span_Zero then
            select
                this.frameEnd.Wait_One;     -- only one thread is signalled per frame
            or
                delay To_Duration( timeout );
            end select;
        else
            this.frameEnd.Wait_One;
        end if;
    end Wait_Frame_End;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Process_Manager ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

    --==========================================================================

    overriding
    procedure Execute( this     : access Attach_Operation;
                       now      : Time;
                       execList : in out Execution_Lists.List ) is
        crsr : Execution_Lists.Cursor;
        exec : A_Execution := new Execution'(process   => this.process,
                                             firstTick => now,
                                             lastTick  => now,
                                             others    => <>);
    begin
        crsr := execList.Find( exec );
        if Has_Element( crsr ) then
            -- already attached; if the process is flagged for detach, clear
            -- the detach flag. otherwise, ignore the duplicate attach
            -- operation. this covers a special case where a detach and an
            -- attach could be queued back to back.
            Delete( exec );
            exec := Element( crsr );
            if exec.detachMe then
                exec.detachMe := False;
                exec.destructor := null;
            end if;
        else
            pragma Debug( Dbg( "Attached process <" &
                               exec.process.Get_Process_Name & ">",
                               D_PROCS, Info ) );
            execList.Append( exec );
        end if;
    end Execute;

    ----------------------------------------------------------------------------

    overriding
    procedure Execute( this     : access Detach_Operation;
                       now      : Time;
                       execList : in out Execution_Lists.List ) is
        pragma Unreferenced( now );
        exec : A_Execution := new Execution'(process => this.process, others => <>);
        crsr : Execution_Lists.Cursor;
    begin
        crsr := execList.Find( exec );
        if Execution_Lists.Has_Element( crsr ) then
            Delete( exec );
            exec := Execution_Lists.Element( crsr );
            exec.detachMe := True;
            exec.destructor := this.destroy;
        else
            Delete( exec );
        end if;
    end Execute;

    ----------------------------------------------------------------------------

    overriding
    procedure Execute( this     : access Pause_Operation;
                       now      : Time;
                       execList : in out Execution_Lists.List ) is
        exec : A_Execution := new Execution'(process => this.process, others => <>);
        crsr : Execution_Lists.Cursor;
    begin
        crsr := execList.Find( exec );
        if Execution_Lists.Has_Element( crsr ) then
            Delete( exec );
            exec := Execution_Lists.Element( crsr );
            exec.paused := True;
            exec.pauseTime := now;
            pragma Debug( Dbg( "Paused process <" &
                               exec.process.Get_Process_Name & ">",
                               D_PROCS, Info ) );
        else
            Delete( exec );
        end if;
    end Execute;

    ----------------------------------------------------------------------------

    overriding
    procedure Execute( this     : access Resume_Operation;
                       now      : Time;
                       execList : in out Execution_Lists.List ) is
        exec : A_Execution := new Execution'(process => this.process, others => <>);
        crsr : Execution_Lists.Cursor;
    begin
        crsr := execList.Find( exec );
        if Execution_Lists.Has_Element( crsr ) then
            Delete( exec );
            exec := Execution_Lists.Element( crsr );
            pragma Debug( Dbg( "Resuming process <" &
                               exec.process.Get_Process_Name & ">",
                               D_PROCS, Info ) );
            if exec.paused then
                exec.paused := False;
                exec.lastTick := now - (exec.pauseTime - exec.lastTick);
            end if;
        else
            Delete( exec );
        end if;
    end Execute;

    --==========================================================================

    task body Ticker_Task is
        this : A_Process_Manager;
    begin
        accept Init( pman : A_Process_Manager ) do
            this := pman;
            pragma Debug( Dbg( "Process manager <" & this.Get_Name & "> initialized", D_PROCS, Info ) );
        end Init;

        this.Run;
    end Ticker_Task;

end Processes.Managers;
