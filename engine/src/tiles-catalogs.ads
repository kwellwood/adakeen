--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers;                    use Ada.Containers;
with Ada.Containers.Indefinite_Ordered_Sets;
with Ada.Containers.Ordered_Maps;
with Ada.Containers.Vectors;
with Ada.Strings.Equal_Case_Insensitive;
with Ada.Strings.Less_Case_Insensitive;
with Ada.Unchecked_Conversion;

package Tiles.Catalogs is

    -- A Catalog indexes and organizes all the Tiles in a Tile_Library. It is
    -- stored inside a tile library as a catalog file, alongside all the bitmap
    -- files. In addition to indexing tiles, it indexes tile matrices for
    -- repeated tile patterns and large, multi-tile objects.
    type Catalog is new Limited_Object with private;
    type A_Catalog is access all Catalog'Class;

    -- Creates a new empty tile catalog.
    function Create_Catalog return A_Catalog;
    pragma Postcondition( Create_Catalog'Result /= null );

    -- Adds a new tile to the catalog. 'tile' is consumed. If 'tile' is passed
    -- as null, an empty tile slot is added to the tile list as a place holder.
    -- The exception DUPLICATE_TILE will be raised if a tile with the same id
    -- already exists in the catalog, unless the id = 'ANON_ID'.
    procedure Add_Tile( this : not null access Catalog'Class;
                        tile : in out A_Tile );
    pragma Postcondition( tile = null );

    -- Finds a tile with a name matching 'name' and returns a reference. The
    -- tile with the first matching name will be returned. If 'name' does not
    -- include a file extension, a matching .png file will be searched. Do not
    -- modify the tile; it belongs to the catalog. Null will be returned if no
    -- tile is not found.
    function Find_Tile( this : not null access Catalog'Class;
                        name : String ) return A_Tile;

    -- Returns a tile reference by id, base 1. Do not modify the tile; it
    -- belongs to the catalog. Null will be returned if the id is not found.
    function Get_Tile( this : not null access Catalog'Class;
                       id   : Natural ) return A_Tile;

    -- Iterates across all the tiles in the catalog by order of ascending id.
    procedure Iterate_By_Id( this    : not null access Catalog'Class;
                             examine : not null access procedure( tile : not null A_Tile ) );

    -- Iterates across all the tile slots in the catalog, beginning with 1. Not
    -- every slot contains a tile, so the 'tile' argument of 'examine' may be
    -- null.
    procedure Iterate_By_Slot( this    : not null access Catalog'Class;
                               examine : not null access procedure( slot : Positive;
                                                                    tile : A_Tile ) );

    -- Iterates across all unique filenames referenced by tiles in the catalog,
    -- alphabetically.
    procedure Iterate_Filenames( this    : not null access Catalog'Class;
                                 examine : not null access procedure( filename : String ) );

    -- Assigns tile ids to all tiles with ANON_ID ids. If all tile ids have been
    -- resolved, nothing will change.
    procedure Resolve_Tile_Ids( this : not null access Catalog'Class );

    -- Deletes the Catalog and all the Tiles it currently contains.
    procedure Delete( this : in out A_Catalog );
    pragma Postcondition( this = null );

    ----------------------------------------------------------------------------

    -- Returns the version number of the tile catalog's stream format. Different
    -- version numbers are incompatible.
    function Catalog_Version_Read return Natural;
    function Catalog_Version_Write return Natural;

    -- raised on attempt to add a tile with a duplicate id
    DUPLICATE_TILE : exception;

private

    package Tile_Maps is new Ada.Containers.Ordered_Maps( Natural, A_Tile, "<", "=" );
    package Tile_Vectors is new Ada.Containers.Vectors( Positive, A_Tile, "=" );
    package String_Sets is new Ada.Containers.Indefinite_Ordered_Sets( String,
                                                                       Ada.Strings.Less_Case_Insensitive,
                                                                       Ada.Strings.Equal_Case_Insensitive );

    type Catalog is new Limited_Object with
        record
            lock      : Mutex;                  -- protects only .idMap
            idMap     : Tile_Maps.Map;          -- mapping of id numbers to tiles
            slotArray : Tile_Vectors.Vector;    -- array of tiles as slots (sparse collection)
            filenames : String_Sets.Set;
        end record;

    procedure Delete( this : in out Catalog );

    -- Reads the contents of a Catalog from a stream.
    procedure Object_Read( stream : access Root_Stream_Type'Class;
                           obj    : out Catalog );
    for Catalog'Read use Object_Read;

    -- Writes the contents of a Catalog to a stream.
    procedure Object_Write( stream : access Root_Stream_Type'Class;
                            obj    : Catalog );
    for Catalog'Write use Object_Write;

    ----------------------------------------------------------------------------

    -- Verifies a file format header before reading the Catalog representation.
    -- If the header is invalid, READ_EXCEPTION will be raised. Raises an
    -- exception on streaming error.
    function A_Catalog_Input( stream : access Root_Stream_Type'Class ) return A_Catalog;
    for A_Catalog'Input use A_Catalog_Input;

    -- Writes a file format header and then the Catalog representation.
    procedure A_Catalog_Output( stream : access Root_Stream_Type'Class;
                                obj    : A_Catalog );
    for A_Catalog'Output use A_Catalog_Output;

end Tiles.Catalogs;
