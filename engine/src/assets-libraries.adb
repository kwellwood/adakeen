--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Ada.Containers.Indefinite_Ordered_Maps;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.State;                     use Allegro.State;
with Assets.Archives.Zip_Archives;
with Debugging;                         use Debugging;
with Resources;                         use Resources;
with Resources.Images;                  use Resources.Images;
with Streams.Buffers;                   use Streams.Buffers;
with Support;                           use Support;
with Support.Paths;                     use Support.Paths;
with Support.Real_Time;                 use Support.Real_Time;
with Tiles.Construction;                use Tiles.Construction;
with Values.Strings;                    use Values.Strings;

package body Assets.Libraries is

    ATLAS_MIN_SIZE  : constant := 256;
    ATLAS_INCREMENT : constant := 256;

    package Bitmap_Maps is new Ada.Containers.Indefinite_Ordered_Maps( String, A_Allegro_Bitmap, "<", "=" );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    procedure Initialize( display : A_Allegro_Display ) is
    begin
        pragma Debug( Dbg( "Initializing tile library assets", D_ASSET or D_TILES, Info ) );
        Assets.Libraries.display := display;

        -- register ".tlz" as a zip file format
        Assets.Archives.Register_Format( LIBRARY_EXTENSION, Assets.Archives.Zip_Archives.Load_Zip'Access );
    end Initialize;

    --==========================================================================

    procedure Finalize is
    begin
        pragma Debug( Dbg( "Finalizing tile library assets", D_ASSET or D_TILES, Info ) );
        Assets.Libraries.display := null;
    end Finalize;

    --==========================================================================

    function Create_Library( name : String ) return A_Tile_Library is
        filename : constant String := Add_Extension( To_Lower( name ), LIBRARY_EXTENSION );
        this     : constant A_Tile_Library := new Tile_Library;
    begin
        this.Construct( Library_Assets, Create( filename ), filename, "graphics" );
        this.catalog := Create_Catalog;
        return this;
    end Create_Library;

    ----------------------------------------------------------------------------

    function Load_Library( name : String; bitmaps : Boolean ) return Library_Ptr is
        lname    : constant String := To_Lower( name );
        filename : constant String := Add_Extension( lname, LIBRARY_EXTENSION );
        assetId  : Value;
        asset    : A_Asset := null;
        this     : A_Tile_Library := null;
    begin
        assetId := Create( filename );

        -- search for the archive in the asset cache
        asset := Find_Asset( assetId );
        if asset /= null then
            -- found in cache
            pragma Assert( asset.Get_Type = Library_Assets,
                           "Cached asset '" & lname & "' is not a tile library" );
            this := A_Tile_Library(asset);
        else
            -- create a new asset
            this := new Tile_Library;
            this.Construct( assetId, filename, bitmaps );
            Store_Asset( A_Asset(this) );
        end if;

        -- if bitmaps are needed and they haven't been loaded yet, then mark
        -- them as needed and load them from disk.
        if bitmaps and then this.Is_Loaded and then not this.Are_Bitmaps_Loaded then
            this.Load_Bitmaps;
        end if;

        return Library_Ptr'(Ada.Finalization.Controlled with target => this);
    end Load_Library;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this     : access Tile_Library;
                         assetId  : Value'Class;
                         filePath : String;
                         bitmaps  : Boolean ) is
    begin
        Asset(this.all).Construct( Library_Assets, assetId, filePath, "graphics" );
        this.name := To_Unbounded_String( Get_Basename( filePath ) );
        this.needBitmaps := bitmaps;
    end Construct;

    ----------------------------------------------------------------------------

    procedure Add_Tile( this : not null access Tile_Library'Class;
                        tile : in out A_Tile ) is
    begin
        if tile /= null then
            this.catalog.Add_Tile( tile );
        end if;
    end Add_Tile;

    ----------------------------------------------------------------------------

    function Are_Bitmaps_Loaded( this : not null access Tile_Library'Class ) return Boolean is
    begin
        this.loadLock.Lock;
        return result : constant Boolean := this.bmpLoaded do
            this.loadLock.Unlock;
        end return;
    end Are_Bitmaps_Loaded;

    ----------------------------------------------------------------------------

    function Get_Id( this : not null access Tile_Library'Class;
                     name : String ) return Natural is
        tile : constant A_Tile := this.catalog.Find_Tile( name );
    begin
        if tile /= null then
            return tile.Get_Id;
        end if;
        return 0;
    end Get_Id;

    ----------------------------------------------------------------------------

    function Get_Matrix( this  : not null access Tile_Library'Class;
                         index : Natural ) return A_Map is
    begin
        if index > 0 and then index <= Integer(this.matrices.Length) then
            return this.matrices.Element( index );
        end if;
        return null;
    end Get_Matrix;

    ----------------------------------------------------------------------------

    function Get_Matrix_Count( this : not null access Tile_Library'Class ) return Natural is (Natural(this.matrices.Length));

    ----------------------------------------------------------------------------

    function Get_Name( this : not null access Tile_Library'Class ) return String is (To_String( this.name ));

    ----------------------------------------------------------------------------

    function Get_Terrain( this  : not null access Tile_Library'Class;
                          index : Natural ) return A_Terrain_Definition is
    begin
        if index > 0 and then index <= Integer(this.terrains.Length) then
            return this.terrains.Element( index );
        end if;
        return null;
    end Get_Terrain;

    ----------------------------------------------------------------------------

    function Get_Terrain_Count( this : not null access Tile_Library'Class ) return Natural is (Integer(this.terrains.Length));

    ----------------------------------------------------------------------------

    function Get_Tile( this : not null access Tile_Library'Class;
                       id   : Natural ) return A_Tile
        is (if id > 0 then this.catalog.Get_Tile( id ) else null);

    ----------------------------------------------------------------------------

    procedure Iterate_By_Id( this    : not null access Tile_Library'Class;
                             examine : not null access procedure( tile : not null A_Tile ) ) is
    begin
        this.catalog.Iterate_By_Id( examine );
    end Iterate_By_Id;

    ----------------------------------------------------------------------------

    procedure Iterate_By_Slot( this    : not null access Tile_Library'Class;
                               examine : not null access procedure( slot : Positive;
                                                                    tile : A_Tile ) ) is
    begin
        this.catalog.Iterate_By_Slot( examine );
    end Iterate_By_Slot;

    ----------------------------------------------------------------------------

    procedure Load_Bitmaps( this : not null access Tile_Library'Class ) is
        use Bitmap_Maps;
        bitmaps : Bitmap_Maps.Map;

        ------------------------------------------------------------------------

        -- Loads image file "images/<filename>" from the tile library's archive
        -- into the 'bitmaps' map.
        procedure Load_Bitmap( filename : String ) is
            resource : A_Resource_File;
            bmp      : A_Allegro_Bitmap;
        begin
            resource := this.file.Load_File( "images/" & filename );
            if resource /= null then
                bmp := Load_Image( resource );
                Delete( resource );
            end if;
            if bmp /= null then
                bitmaps.Insert( filename, bmp );
            else
                Dbg( "Failed to load 'images/" & filename & "' from library '" &
                     To_String( this.filePath ) & "'",
                     D_ASSET or D_TILES, Error );
            end if;
        end Load_Bitmap;

        ------------------------------------------------------------------------

        -- Sets the bitmap and white mask of tile 'tile'
        procedure Set_Tile_Bitmap( tile : not null A_Tile ) is
            pos      : constant Bitmap_Maps.Cursor := bitmaps.Find( tile.Get_Name );
            original : A_Allegro_Bitmap;
        begin
            if Has_Element( pos ) then
                original := Element( pos );

                -- sets the tile's bitmap using 'this.atlas' or falling back to 'bmp'.
                -- a white mask will be created if necessary and added to the atlas.
                Set_Tile_Bitmap( tile, this.atlas, original );
            end if;

            if tile.Get_Bitmap = null then
                Dbg( "Missing 'images/" & tile.Get_Name & "' in library '" &
                     To_String( this.filePath ) & "'",
                     D_ASSET or D_TILES, Error );
            end if;
        end Set_Tile_Bitmap;

        ------------------------------------------------------------------------

        timer : Timer_Type;
        state : Allegro_State;
    begin
        Start( timer );

        this.loadLock.Lock;

        -- this remains True for the life of the asset, causing bitmaps to be
        -- automatically reloaded every time the tile library is reloaded from
        -- disk.
        this.needBitmaps := True;

        if not this.Is_Loaded or else this.bmpLoaded then
            this.loadLock.Unlock;
            return;
        end if;

        -- 1. load all image files from the library's archive
        Al_Store_State( state, ALLEGRO_STATE_TARGET_BITMAP );
        Al_Set_Target_Backbuffer( display );
        this.catalog.Iterate_Filenames( Load_Bitmap'Access );
        Al_Restore_State( state );

        -- 2. create a tile atlas and store all bitmaps into it
        if display /= null then
            declare
                pageWidth  : Positive := ATLAS_MIN_SIZE;
                pageHeight : Positive := ATLAS_MIN_SIZE;
                pixels     : Natural := 0;
                pos        : Bitmap_Maps.Cursor;
            begin
                -- enlarge the atlas size to fit the largest bitmap on a page
                for bmp of bitmaps loop
                    while pageWidth < Al_Get_Bitmap_Width( bmp ) loop
                        pageWidth := pageWidth + ATLAS_INCREMENT;
                    end loop;
                    while pageHeight < Al_Get_Bitmap_Height( bmp ) loop
                        pageHeight := pageHeight + ATLAS_INCREMENT;
                    end loop;
                    pixels := pixels + Al_Get_Bitmap_Width( bmp ) *
                                       Al_Get_Bitmap_Height( bmp );
                end loop;

                -- enlarge the page to fit all the pixels
                while (pixels * 6) / 5 > pageWidth * pageHeight loop
                    -- enlarge the shorter dimension until it's square, then
                    -- continue to enlarge it as a square.
                    if pageHeight >= pageWidth then
                        pageWidth := pageWidth + ATLAS_INCREMENT;
                    end if;
                    if pageWidth + ATLAS_INCREMENT >= pageHeight then
                        pageHeight := pageHeight + ATLAS_INCREMENT;
                    end if;
                end loop;

                this.atlas := Create_Atlas( display, pageWidth, pageHeight );

                -- store all bitmaps in the atlas
                pos := bitmaps.First;
                while Has_Element( pos ) loop
                    declare
                        bmp : A_Allegro_Bitmap := Element( pos );
                    begin
                        if this.atlas.Add_Bitmap( Key( pos ), bmp ) then
                            -- bitmap was copied into the atlas, destroy the original
                            Al_Destroy_Bitmap( bmp );
                            bitmaps.Replace_Element( pos, null );
                        end if;
                    end;
                    Next( pos );
                end loop;
            end;
        end if;

        -- 3. set each tile's bitmap
        declare
            state : aliased Allegro_State;
        begin
            Al_Store_State( state, ALLEGRO_STATE_ALL );
            this.catalog.Iterate_By_Id( Set_Tile_Bitmap'Access );
            Al_Restore_State( state );
        end;

        -- uncomment the following line to write the atlas's pages to disk for
        -- debugging purposes.
        --if this.atlas /= null then
        --    this.atlas.Save_Pages( To_String( this.name ) );
        --end if;

        -- 4. clean up any bitmaps not stored in the atlas
        for bmp of bitmaps loop
            Al_Destroy_Bitmap( bmp );
        end loop;
        bitmaps.Clear;

        pragma Debug( Dbg( "Loaded bitmaps from '" & To_String( this.filePath ) & "' - " &
                           Image( To_Milliseconds( Elapsed( timer ) ) ) & "[ms]",
                           D_TILES, Info ) );

        this.bmpLoaded := True;
        this.loadLock.Unlock;
    exception
        when e : others =>
            Dbg( "Failed to load bitmaps from library '" & To_String( this.filePath ) & "'", D_ASSET or D_TILES, Error );
            Dbg( e, D_ASSET or D_TILES, Error );
            this.bmpLoaded := True;
            this.loadLock.Unlock;
    end Load_Bitmaps;

    ----------------------------------------------------------------------------

    overriding
    function Load_Data( this : access Tile_Library ) return Boolean is
        res    : A_Resource_File := null;
        stream : A_Buffer_Stream := null;
    begin
        -- load the tile library's .tlz archive file
        this.file := Load_Archive( To_String( this.filePath ), To_String( this.group ), keepLoaded => False );
        if not this.file.Is_Loaded then
            Delete( this.file );
            return False;
        end if;

        -- load the catalog file
        begin
            if this.file /= null then
                res := this.file.Load_File( CATALOG_FILENAME );
                if res /= null then
                    stream := res.Create_Stream;
                    this.catalog := A_Catalog'Input( stream );  -- may raise exception
                    if this.catalog = null then
                        raise Constraint_Error with "Bad file format";
                    end if;
                    Close( stream );
                    Delete( res );
                else
                    Dbg( "Catalog not found in library '" &
                         To_String( this.filePath ) & "'", D_ASSET or D_TILES, Error );
                    Delete( this.file );
                end if;
            end if;
        exception
            when e : others =>
                -- unable to load catalog file (format error)
                Dbg( "Unable to load catalog in library '" &
                     To_String( this.filePath ) & "'", D_ASSET or D_TILES, Error );
                Dbg( e, D_ASSET or D_TILES, Error );
                Close( stream );
                Delete( res );
                Delete( this.file );
        end;

        -- load the tile matrices list
        if this.file /= null and then this.file.File_Exists( MATRIX_FILENAME ) then
            res := this.file.Load_File( MATRIX_FILENAME );
            Load_Matrices( res, this.matrices );
            Delete( res );
        end if;

        -- load the terrain set
        if this.file /= null and then this.file.File_Exists( TERRAIN_FILENAME ) then
            res := this.file.Load_File( TERRAIN_FILENAME );
            Load_Terrains( res, this.terrains );
            Delete( res );
        end if;

        -- load the bitmaps as necessary
        this.loadLock.Lock;
        if this.file /= null and then this.needBitmaps then
            this.Load_Bitmaps;
        end if;
        this.loadLock.Unlock;

        return this.file /= null;
    end Load_Data;

    ----------------------------------------------------------------------------

    overriding
    procedure Unload_Data( this : access Tile_Library ) is
    begin
        this.loadLock.Lock;
        this.bmpLoaded := False;
        this.loadLock.Unlock;

        Delete_All( this.terrains );
        Delete_All( this.matrices );
        Delete( this.catalog );
        Delete( this.atlas );
        Delete( this.file );
    end Unload_Data;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Tile_Library ) is
    begin
        -- 'this' is not in the asset cache, so it will be deleted by the cache
        -- when we try to release the reference.
        Delete( A_Asset(this) );
    end Delete;

    --==========================================================================

    overriding
    procedure Adjust( this : in out Library_Ptr ) is
        lib : A_Tile_Library;
    begin
        if this.target /= null then
            -- get another reference to this.target from the asset cache.
            -- BUT if this.target is not in the asset cache, then Clone() will
            -- return null; just put the reference back.
            lib := this.target;
            this.target := this.target.Clone;
            if this.target = null then
                this.target := lib;
            end if;
        end if;
    end Adjust;

    ----------------------------------------------------------------------------

    overriding
    procedure Finalize( this : in out Library_Ptr ) is
    begin
        Delete( A_Asset(this.target) );
    end Finalize;

    ----------------------------------------------------------------------------

    function "="( l, r : Library_Ptr ) return Boolean is (l.target = r.target);

    function Get( this : Library_Ptr ) return access Tile_Library'Class is (this.target);

    function Is_Null( this : Library_Ptr ) return Boolean is (this.target = null);

    function Not_Null( this : Library_Ptr ) return Boolean is (this.target /= null);

    ----------------------------------------------------------------------------

    procedure Set( this : in out Library_Ptr; target : A_Tile_Library ) is
    begin
        if this.target = target then
            -- the target isn't changing
            return;
        end if;

        if this.target /= null then
            this.Finalize;     -- decrement reference count
        end if;

        if target /= null then
            this.target := target;
            this.Adjust;       -- increment reference count
        end if;
    end Set;

    ----------------------------------------------------------------------------

    procedure Unset( this : in out Library_Ptr ) is
    begin
        this.Set( null );
    end Unset;

    --==========================================================================

end Assets.Libraries;
