--
-- Copyright (c) 2012 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Streams;                       use Ada.Streams;

-- This package provides an implementation for Allegro_Files backed by read-only
-- memory. It differs from the Allegro Memfile addon by optionally managing the
-- memory backing the file.
private package Resources.Allegro_Files is

    -- Returns a reference to a read-only Allegro_File backed by 'buffer'. If
    -- 'consume' is True, the Allegro_File assumes ownership of the buffer,
    -- freeing it when the file is closed. If 'buffer' is consumed, the caller
    -- should not reference 'buffer' again after calling this function.
    function Open_Memfile( buffer  : access Stream_Element_Array;
                           consume : Boolean := False ) return A_Allegro_File;

end Resources.Allegro_Files;
