--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Ada.Exceptions;                    use Ada.Exceptions;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Ada.Tags;                          use Ada.Tags;
with Debugging;                         use Debugging;
with Support;                           use Support;
with Support.Paths;                     use Support.Paths;

package body Tiles.Catalogs is

    -- A string that identifies the tile catalog file format. It is the first
    -- data found in the file.
    CATALOG_IDENTIFIER : constant String := "cat";

    -- The file format version number for tile catalogs.
    VERSION_READ  : constant := 8;
    VERSION_WRITE : constant := 8;

    --==========================================================================

    function Create_Catalog return A_Catalog is
        this : constant A_Catalog := new Catalog;
    begin
        this.Construct;
        return this;
    end Create_Catalog;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Catalog ) is
    begin
        -- note: there's what appears to be a threading bug that causes a cursor
        -- tampering exception in Delete when .idMap is cleared, unless it's
        -- protected by a lock. haven't been able to find the root cause of the
        -- cursor tampering. the other fields seem to be safe.
        this.lock.Lock;
        for t of this.idMap loop
            Delete( t );
        end loop;
        this.idMap.Clear;
        this.slotArray.Clear;
        Limited_Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Add_Tile( this : not null access Catalog'Class;
                        tile : in out A_Tile ) is
    begin
        if tile /= null then
            if tile.Get_Id /= NULL_TILE_ID then
                -- ANON_TILE_ID tiles have an id that will be assigned after all
                -- the other tiles have been added to the library. they cannot
                -- be added to .idMap until the id is known, but they are added
                -- to .slotArray.
                if tile.Get_Id /= ANON_TILE_ID then
                    this.lock.Lock;
                    if this.idMap.Contains( tile.Get_Id ) then
                        this.lock.Unlock;
                        raise DUPLICATE_TILE with
                              "Duplicate tile ID:" & Natural'Image( tile.Get_Id );
                    end if;
                    this.idMap.Insert( tile.Get_Id, tile );
                    this.lock.Unlock;
                end if;
                this.filenames.Include( To_Lower( tile.Get_Name ) );
            end if;
            this.slotArray.Append( tile );
            tile := null;
        else
            -- add a blank slot to the catalog's slot array
            this.slotArray.Append( null );
        end if;
    end Add_Tile;

    ----------------------------------------------------------------------------

    function Find_Tile( this : not null access Catalog'Class;
                        name : String ) return A_Tile is

        function Find_First( filename : String ) return A_Tile is
            lfilename : constant String := To_Lower( filename );
        begin
            for t of this.idMap loop
                if lfilename = To_Lower( t.Get_Name ) then
                    return t;
                end if;
            end loop;
            return null;
        end Find_First;

        tile : A_Tile := null;
    begin
        if name'Length > 0 then
            this.lock.Lock;
            if Get_Extension( name )'Length = 0 then
                tile := Find_First( Add_Extension( name, "png" ) );
            else
                tile := Find_First( name );
            end if;
            this.lock.Unlock;
        end if;
        return tile;
    end Find_Tile;

    ----------------------------------------------------------------------------

    function Get_Tile( this : not null access Catalog'Class;
                       id   : Natural ) return A_Tile is
        pos : Tile_Maps.Cursor;
    begin
        this.lock.Lock;
        pos := this.idMap.Find( id );
        if Tile_Maps.Has_Element( pos ) then
            return tile : constant A_Tile := Tile_Maps.Element( pos ) do
                this.lock.Unlock;
            end return;
        else
            this.lock.Unlock;
            return null;
        end if;
    end Get_Tile;

    ----------------------------------------------------------------------------

    procedure Iterate_By_Id( this    : not null access Catalog'Class;
                             examine : not null access procedure( tile : not null A_Tile ) ) is
    begin
        this.lock.Lock;
        for t of this.idMap loop
            examine.all( t );
        end loop;
        this.lock.Unlock;
    end Iterate_By_Id;

    ----------------------------------------------------------------------------

    procedure Iterate_By_Slot( this    : not null access Catalog'Class;
                               examine : not null access procedure( slot : Positive;
                                                                    tile : A_Tile ) ) is
    begin
        for i in 1..this.slotArray.Length loop
            examine.all( Positive(i), this.slotArray.Element( Positive(i) ) );
        end loop;
    end Iterate_By_Slot;

    ----------------------------------------------------------------------------

    procedure Iterate_Filenames( this    : not null access Catalog'Class;
                                 examine : not null access procedure( filename : String ) ) is
    begin
        for f of this.filenames loop
            examine.all( f );
        end loop;
    end Iterate_Filenames;

    ----------------------------------------------------------------------------

    not overriding
    procedure Object_Read( stream : access Root_Stream_Type'Class;
                           obj    : out Catalog ) is
        count : Natural;
        tile  : A_Tile;
    begin
        count := Natural'Input( stream );
        for i in 1..count loop
            tile := A_Tile'Input( stream );
            obj.Add_Tile( tile );
        end loop;
    end Object_Read;

    ----------------------------------------------------------------------------

    not overriding
    procedure Object_Write( stream : access Root_Stream_Type'Class;
                            obj    : Catalog ) is
    begin
        Natural'Output( stream, Natural(obj.slotArray.Length) );
        for t of obj.slotArray loop
            A_Tile'Output( stream, t );
        end loop;
    end Object_Write;

    ----------------------------------------------------------------------------

    procedure Resolve_Tile_Ids( this : not null access Catalog'Class ) is
        maxId : Natural := 0;
    begin
        for t of this.idMap loop
            maxId := Natural'Max( maxId, t.Get_Id );
        end loop;

        for t of this.slotArray loop
            if t.Get_Id = ANON_TILE_ID then
                maxId := maxId + 1;
                t.Set_Id( maxId );
                this.idMap.Insert( maxId, t );
            end if;
        end loop;
    end Resolve_Tile_Ids;

    --==========================================================================

    function A_Catalog_Input( stream : access Root_Stream_Type'Class ) return A_Catalog is
        obj     : A_Catalog := null;
        ident   : String(1..CATALOG_IDENTIFIER'Length);
        version : Integer := 0;
    begin
        -- verify the header info
        String'Read( stream, ident );
        if ident = CATALOG_IDENTIFIER then
            version := Integer'Input( stream );
            if version = Catalog_Version_Read then
                obj := new Catalog;
                obj.Construct;
                Catalog'Read( stream, obj.all );
            else
                -- unrecognized version
                raise Constraint_Error with "Unrecognized catalog file version: " &
                                            Image( version );
            end if;
        else
            -- invalid identifier
            raise Constraint_Error with "Unrecognized file format";
        end if;

        return obj;
    exception
        when Constraint_Error =>
            raise;
        when e : Tag_Error =>
            declare
                msg : constant String := Exception_Message( e );
            begin
                raise Constraint_Error with "Found unknown class " &
                      "<" & msg((Index( msg, ".", Backward ) + 1)..msg'Last) & ">";
            end;
        when e : others =>
            -- file too short
            -- this debug message is only extra info
            pragma Debug( Dbg( "Tiles.Catalogs Exception: " & Exception_Name( e ) &
                               " - " & Exception_Message( e ),
                               D_TILES, Info ) );
            raise Constraint_Error with "Unrecognized file format";
    end A_Catalog_Input;

    ----------------------------------------------------------------------------

    procedure A_Catalog_Output( stream : access Root_Stream_Type'Class;
                                obj    : A_Catalog ) is
    begin
        -- ensure no anonymous tiles remain
        obj.Resolve_Tile_Ids;

        -- write the header info
        String'Write( stream, CATALOG_IDENTIFIER );
        Integer'Output( stream, Catalog_Version_Write );
        Catalog'Write( stream, obj.all );
    end A_Catalog_Output;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Catalog ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

    --==========================================================================

    -- The Catalog file format uses Values, so the version number includes the
    -- current stream format version of Value classes.
    function Catalog_Version_Read return Natural is (VERSION_READ * 100 + Values.STREAM_VERSION_READ);
    function Catalog_Version_Write return Natural is (VERSION_WRITE * 100 + Values.STREAM_VERSION_WRITE);

end Tiles.Catalogs;
