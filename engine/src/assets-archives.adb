--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Ada.Containers.Indefinite_Ordered_Maps;
with Debugging;                         use Debugging;
with Support.Paths;                     use Support.Paths;
with Values.Strings;                    use Values.Strings;

package body Assets.Archives is

    type Archive_Format is
        record
            load : A_Archive_Loader;
        end record;

    package Format_Map is new Ada.Containers.Indefinite_Ordered_Maps( String,
                                                                      Archive_Format,
                                                                      "<", "=" );
    use Format_Map;

    formats : Format_Map.Map;

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    procedure Register_Format( ext : String; loader : not null A_Archive_Loader ) is
        lext : constant String := To_Lower( ext );
    begin
        if not formats.Contains( lext ) then
            formats.Insert( lext, (load => loader) );
        else
            Dbg( "Archive format '" & lext & "' already registered.", D_ASSET, Warning );
        end if;
    end Register_Format;

    --==========================================================================

    function Load_Archive( filePath : String; group : String; keepLoaded : Boolean ) return A_Archive is
        lpath   : constant String := To_Lower( filePath );
        pos     : Format_Map.Cursor;
        assetId : Value;
        asset   : A_Asset := null;
        this    : A_Archive := null;
    begin
        pos := formats.Find( To_Lower( Get_Extension( filePath ) ) );
        if Has_Element( pos ) then
            assetId := Create( (if group'Length > 0 then To_Lower( group ) & ":" else "") & lpath );

            -- search for the archive in the asset cache
            asset := Find_Asset( assetId );
            if asset /= null then
                -- found in cache
                pragma Assert( asset.Get_Type = Archive_Assets,
                               "Cached asset '" & filePath & "' is not an archive" );
                this := A_Archive(asset);
            else
                -- create a new asset
                this := Element( pos ).load( assetId, filePath, group );
                Store_Asset( A_Asset(this) );
            end if;
            this.dataLock.Lock;
            this.keepLoaded := this.keepLoaded or keepLoaded;
            this.dataLock.Unlock;
        else
            Dbg( "Archive extension unrecognized: " & filePath, D_ASSET, Warning );
        end if;
        return this;
    end Load_Archive;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this     : access Archive;
                         assetId  : Value'Class;
                         filePath : String;
                         group    : String ) is
    begin
        Asset(this.all).Construct( Archive_Assets, assetId, filePath, group );
    end Construct;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Archive ) is
    begin
        Delete( A_Asset(this) );
    end Delete;

end Assets.Archives;
