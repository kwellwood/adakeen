--
-- Copyright (c) 2018-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Ordered_Maps;
with Ada.Real_Time;                     use Ada.Real_Time;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Debugging;                         use Debugging;
with Resources;                         use Resources;
with Support.Paths;                     use Support.Paths;
with Values.Construction;               use Values.Construction;
with Values.Lists;                      use Values.Lists;
with Values.Strings;                    use Values.Strings;

package body Assets is

    type Asset_Info is
        record
            asset     : A_Asset;
            refs      : Natural := 1;
            idleDelay : Time_Span := Seconds( 30 );
            idleStart : Time := Clock;
        end record;
    type A_Asset_Info is access all Asset_Info;

    function Lt( l, r : Value ) return Boolean is (l < r);

    package Asset_Maps is new Ada.Containers.Ordered_Maps( Value, A_Asset_Info, Lt, "=" );
    use Asset_Maps;

    function To_String( a : Asset_Type ) return String is
        str : constant String := Asset_Type'Image( a );
    begin
        return str(str'First..Index( str, "_", Backward )-1);
    end To_String;

    ----------------------------------------------------------------------------

    -- The cache is implemented as a standard package, protected by a recursive
    -- Mutex, because protected types are not re-entrant on MacOS.
    package Cache is

        procedure Finalize;
        procedure Find_Asset( assetId : Value'Class; asset : out A_Asset );
        procedure Query( stats : in out Map_Value );
        procedure Release_Asset( asset : in out A_Asset );
        procedure Store_Asset( asset : in out A_Asset );
        procedure Unload_All( t : Asset_Type );

    private

        procedure Collect_Garbage;

        lock     : Mutex;
        idMap    : Asset_Maps.Map;
        gcTime   : Time := Clock;
        gcActive : Boolean := False;
    end Cache;

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    GARBAGE_INTERVAL : constant Time_Span := Seconds( 15 );

    package body Cache is

        procedure Finalize is
        begin
            lock.Lock;
            pragma Debug( Dbg( "Clearing asset cache", D_ASSET, Info ) );
            for info of idMap loop
                info.asset.Unload( force => True );
            end loop;
            lock.Unlock;
        end Finalize;

        ------------------------------------------------------------------------

        procedure Collect_Garbage is
            now : Time;
        begin
            lock.Lock;
            now := Clock;
            if gcActive or gcTime + GARBAGE_INTERVAL > now then
                lock.Unlock;
                return;
            end if;
            gcActive := True;

            for info of idMap loop
                if info.refs = 0 and info.idleStart + info.idleDelay <= now then
                    info.asset.Unload;
                end if;
            end loop;
            gcTime := Clock;
            gcActive := False;
            lock.Unlock;
        end Collect_Garbage;

        ------------------------------------------------------------------------

        procedure Find_Asset( assetId : Value'Class; asset : out A_Asset ) is
            pos  : Asset_Maps.Cursor;
            info : A_Asset_Info;
        begin
            lock.Lock;
            asset := null;
            pos := idMap.Find( Value(assetId) );
            if Has_Element( pos ) then
                info := Element( pos );
                info.refs := info.refs + 1;
                if info.refs = 1 then
                    -- the asset should be refreshed before use
                    info.asset.Set_Refreshable;
                end if;
                asset := info.asset;
            end if;

            Collect_Garbage;
            lock.Unlock;
        end Find_Asset;

        ------------------------------------------------------------------------

        procedure Query( stats : in out Map_Value ) is
            id       : constant List_Value := Create_List.Lst;
            file     : constant List_Value := Create_List.Lst;
            group    : constant List_Value := Create_List.Lst;
            path     : constant List_Value := Create_List.Lst;
            loaded   : constant List_Value := Create_List.Lst;
            refs     : constant List_Value := Create_List.Lst;
            idleTime : constant List_Value := Create_List.Lst;
        begin
            lock.Lock;
            Collect_Garbage;

            for info of idMap loop
                -- cache owned info
                refs.Append( Create( info.refs ) );
                idleTime.Append( (if info.refs > 0 then Create( 0 ) else Create( Long_Float(To_Duration( Clock - info.idleStart )) )) );

                -- asset owned info
                id.Append( info.asset.id );
                file.Append( Create( info.asset.filePath ) );
                group.Append( Create( info.asset.group ) );
                path.Append( Create( info.asset.diskFile ) );
                loaded.Append( Create( info.asset.Is_Loaded ) );
            end loop;
            lock.Unlock;

            stats := Create_Map.Map;
            stats.Set( "id"      , id      , consume => True );
            stats.Set( "file"    , file    , consume => True );
            stats.Set( "group"   , group   , consume => True );
            stats.Set( "path"    , path    , consume => True );
            stats.Set( "loaded"  , loaded  , consume => True );
            stats.Set( "refs"    , refs    , consume => True );
            stats.Set( "idleTime", idleTime, consume => True );
        end Query;

        ------------------------------------------------------------------------

        procedure Release_Asset( asset : in out A_Asset ) is
            pos  : Asset_Maps.Cursor;
            info : A_Asset_Info;
        begin
            lock.Lock;
            pos := idMap.Find( asset.id );

            if not Has_Element( pos ) then
                -- the asset is not in the cache, so we assume this is the only
                -- reference to it and free its resources.
                if asset.Is_Loaded then
                    asset.Unload( force => True );
                end if;
                Delete( A_Limited_Object(asset) );
                lock.Unlock;
                return;
            end if;

            info := Element( pos );

            pragma Assert( info.asset = asset );        -- must exist in cache
            pragma Assert( info.refs > 0 );             -- must have been referenced
            info.refs := info.refs - 1;
            if info.refs = 0 then
                info.idleStart := Clock;                -- asset is now idle
            end if;

            asset := null;

            Collect_Garbage;
            lock.Unlock;
        end Release_Asset;

        ------------------------------------------------------------------------

        procedure Store_Asset( asset : in out A_Asset ) is
            pos  : Asset_Maps.Cursor;
            info : A_Asset_Info;
        begin
            lock.Lock;
            pos := idMap.Find( asset.id );
            if not Has_Element( pos ) then
                -- store the asset
                pragma Debug( Dbg( "Creating asset [" & To_String( asset.typ ) & "] '" & To_String( asset.filePath ) & "'", D_ASSET, Debugging.Info ) );
                info := new Asset_Info'(asset => asset, others => <>);
                idMap.Insert( asset.id, info );
            else
                -- the asset already exists in the map
                -- another thread simultaneously constructed it and won the race
                -- to store it. throw away the loser and reference the existing
                -- asset instead.
                pragma Assert( not asset.loaded );              -- must not be loaded yet
                Delete( A_Limited_Object(asset) );              -- free Asset memory

                info := Element( pos );
                info.refs := info.refs + 1;                     -- new reference to existing asset
                asset := info.asset;
            end if;

            if info.refs = 1 then
                -- the asset should be refreshed before use
                info.asset.Set_Refreshable;
            end if;

            Collect_Garbage;
            lock.Unlock;
        end Store_Asset;

        ------------------------------------------------------------------------

        procedure Unload_All( t : Asset_Type ) is
        begin
            lock.Lock;
            for info of idMap loop
                if info.asset.Get_Type = t then
                    info.asset.Unload( force => True );
                end if;
            end loop;
            lock.Unlock;
        end Unload_All;

    end Cache;

    ----------------------------------------------------------------------------

    function Find_Asset( assetId : Value'Class ) return A_Asset is
        asset : A_Asset;
    begin
        Cache.Find_Asset( assetId, asset );
        if asset /= null then
            asset.Refresh;
        end if;
        return asset;
    end Find_Asset;

    ----------------------------------------------------------------------------

    procedure Store_Asset( asset : in out A_Asset ) is
    begin
        Cache.Store_Asset( asset );
        asset.Refresh;
    end Store_Asset;

    --==========================================================================

    procedure Finalize is
    begin
        Cache.Finalize;
    end Finalize;

    ----------------------------------------------------------------------------

    procedure Query_Cache( stats : in out Map_Value ) is
    begin
        Cache.Query( stats );
    end Query_Cache;

    ----------------------------------------------------------------------------

    procedure Unload_All( t : Asset_Type ) is
    begin
        Cache.Unload_All( t );
    end Unload_All;

    --==========================================================================

    not overriding
    procedure Construct( this      : access Asset;
                         assetType : Asset_Type;
                         assetId   : Value'Class;
                         filePath  : String;
                         group     : String ) is
    begin
        Limited_Object(this.all).Construct;
        this.typ := assetType;
        this.id := Clone( assetId );
        this.filePath := To_Unbounded_String( filePath );
        this.group := To_Unbounded_String( group );
        this.diskFile := To_Unbounded_String( Locate_Resource( filePath, group ) );
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Path( this : not null access Asset'Class ) return String is (To_String( this.filePath ));

    ----------------------------------------------------------------------------

    function Get_Type( this : not null access Asset'Class ) return Asset_Type is (this.typ);

    ----------------------------------------------------------------------------

    function Is_Loaded( this : not null access Asset'Class ) return Boolean is (this.loaded);

    ----------------------------------------------------------------------------

    procedure Load( this : not null access Asset'Class ) is
    begin
        -- NOTE: a write lock must be held
        pragma Assert( not this.loaded, "Asset '" & To_String( this.filePath ) & "' already loaded" );
        this.loaded := this.Load_Data;
        if not this.loaded then
            Dbg( "Failed to load asset [" & To_String( this.typ ) & "] '" & To_String( this.filePath ) & "'", D_ASSET, Error );
        end if;
    end Load;

    ----------------------------------------------------------------------------

    procedure Refresh( this : not null access Asset'Class ) is

        ------------------------------------------------------------------------

        -- Returns True if the Asset's data needs to be loaded, either because
        -- it isn't currently loaded or it is loaded but the data file on disk
        -- has been modified.
        function Data_Needs_Loading return Boolean is
            fileTime : OS_Time;
        begin
            if not this.loaded and then Is_Regular_File( Get_Outer_Path( To_String( this.diskFile ) ) ) then
                return True;
            end if;

            if this.trackChanges then
                -- data is already loaded but change tracking is enabled, so check
                -- the file's modification time
                fileTime := File_Time_Stamp( Get_Outer_Path( To_String( this.diskFile ) ) );

                -- data may need to be loaded if the file has been modified
                return (fileTime /= Invalid_Time) and then fileTime > this.timestamp;
            end if;

            return False;
        end Data_Needs_Loading;

        ------------------------------------------------------------------------

    begin
        -- NOTE: a read lock must be held
        if Length( this.diskFile ) = 0 then
            -- can't load the asset- the resource file doesn't exist
            return;
        end if;

        this.dataLock.Lock;
        if this.refreshable then
            if Data_Needs_Loading then
                pragma Debug( Dbg( "Loading asset [" & To_String( this.typ ) & "] '" & To_String( this.filePath ) & "'", D_ASSET, Info ) );

                -- unload the asset if necessary
                this.Unload( force => True );

                -- reload the asset from disk
                this.Load;
                if this.loaded then
                    this.timestamp := File_Time_Stamp( Get_Outer_Path( To_String( this.diskFile ) ) );
                end if;
            end if;
            this.refreshable := False;
        end if;
        this.dataLock.Unlock;
    end Refresh;

    ----------------------------------------------------------------------------

    procedure Set_Refreshable( this : not null access Asset'Class ) is
    begin
        this.dataLock.Lock;
        this.refreshable := True;
        this.dataLock.Unlock;
    end Set_Refreshable;

    ----------------------------------------------------------------------------

    procedure Unload( this : not null access Asset'Class; force : Boolean := False ) is
    begin
        this.dataLock.Lock;
        if this.loaded then
            if not this.keepLoaded or force then
                pragma Debug( Dbg( "Unloading asset [" & To_String( this.typ ) & "] '" & To_String( this.filePath ) & "'", D_ASSET, Info ) );
                this.Unload_Data;
                this.loaded := False;
                this.refreshable := True;
            end if;
        end if;
        this.dataLock.Unlock;
    end Unload;

    ----------------------------------------------------------------------------

    function Clone( this : access Asset'Class ) return access Asset'Class is
        result : A_Asset := null;
    begin
        if this /= null then
            Cache.Find_Asset( this.id, result );
        end if;
        return result;
    end Clone;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Asset ) is
    begin
        if this /= null then
            Cache.Release_Asset( this );        -- consumes 'this'
        end if;
    end Delete;

end Assets;
