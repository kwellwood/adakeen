--
-- Copyright (c) 2012 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Unchecked_Conversion;
with System;                            use System;

package body Streams.Buffers is

    procedure Open_Stream( buffer : in out A_SEA; stream : out A_Buffer_Stream ) is
    begin
        stream := new Buffer_Stream;
        stream.buffer := buffer;
        buffer := null;
        stream.readPos := stream.buffer'First;
        stream.freeBuf := True;
    end Open_Stream;

    ----------------------------------------------------------------------------

    function Stream( buffer : not null A_SEA ) return A_Buffer_Stream is
        strm : constant A_Buffer_Stream := new Buffer_Stream;
    begin
        strm.buffer := buffer;
        strm.readPos := strm.buffer'First;
        return strm;
    end Stream;

    ----------------------------------------------------------------------------

    function Stream( buffer : String ) return A_Buffer_Stream is
        function To_SE is new Ada.Unchecked_Conversion( Character, Stream_Element );

        strm : constant A_Buffer_Stream := new Buffer_Stream;
    begin
        strm.buffer := new Stream_Element_Array(Stream_Element_Offset(buffer'First)..Stream_Element_Offset(buffer'Last));
        strm.readPos := strm.buffer'First;
        for i in buffer'Range loop
            strm.buffer(Stream_Element_Offset(i)) := To_SE(buffer(i));
        end loop;
        strm.freeBuf := True;
        return strm;
    end Stream;

    ----------------------------------------------------------------------------

    procedure Close( stream : in out A_Buffer_Stream ) is
        procedure Free is new Ada.Unchecked_Deallocation( Buffer_Stream'Class, A_Buffer_Stream );
    begin
        if stream /= null then
            if stream.freeBuf then
                Delete( stream.buffer );
            end if;
            Free( stream );
        end if;
    end Close;

    ----------------------------------------------------------------------------

    overriding
    procedure Read( stream : in out Buffer_Stream;
                    item   : out Stream_Element_Array;
                    last   : out Stream_Element_Offset ) is

        procedure Memmove( dst, src : Address; len : Integer );
        pragma Import( C, Memmove, "memmove" );

        len : Integer;
    begin
        if stream.readPos <= stream.buffer'Last then
            if item'Length >= 256 then
                -- optimize for larger reads
                len := Integer'Min( item'Length, Integer(stream.buffer'Last - stream.readPos + 1) );
                Memmove( item(item'First)'Address,
                         stream.buffer(stream.readPos)'Address,
                         len );
                stream.readPos := stream.readPos + Stream_Element_Offset(len);
                last := item'First + Stream_Element_Offset(len) - 1;
            else
                -- standard byte-by-byte read
                last := item'First - 1;
                for i in item'Range loop
                    exit when stream.readPos > stream.buffer'Last;
                    item(i) := stream.buffer(stream.readPos);
                    last := i;
                    stream.readPos := stream.readPos + 1;
                end loop;
            end if;
        else
            last := item'First - 1;
        end if;
    end Read;

    ----------------------------------------------------------------------------

    overriding
    procedure Write( stream : in out Buffer_Stream;
                     item   : Stream_Element_Array ) is
        pragma Unreferenced( stream, item );
    begin
        raise STREAM_ERROR with "Writing to Buffer_Stream is not supported";
    end Write;

end Streams.Buffers;
