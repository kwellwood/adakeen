--
-- Copyright (c) 2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Color;                     use Allegro.Color;
with Generic_Properties;
with Interpolation;
with Palette;

package Animated_Properties is

    package Animated_Boolean is new Generic_Properties(Boolean, Interpolation.Interpolate);

    package Animated_Color is new Generic_Properties(Allegro_Color, Palette.Interpolate);

    package Animated_Float is new Generic_Properties(Float, Interpolation.Interpolate);

end Animated_Properties;
