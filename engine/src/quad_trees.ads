--
-- Copyright (c) 2015 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Ordered_Sets;
with Ada.Containers.Vectors;
with Ada.Finalization;                  use Ada.Finalization;

generic
   type Item_Type is private;
package Quad_Trees is

    type Tree is new Limited_Controlled with private;

    -- Sets the configuration for the quad tree. Its contents will be cleared if
    -- it's not empty. This must be called before the tree is used.
    --
    -- width, height is the spatial area covered by the tree.
    -- maxDepth is the maximum depth of the tree in nodes.
    -- maxChildren is the maximum number of children per leaf before subdivision,
    --     but it may be ignored if 'maxDepth' is reached.
    procedure Init( this          : in out Tree'Class;
                    width, height : Float;
                    maxDepth      : Positive;
                    maxChildren   : Natural );

    -- Clears the contents of the quad tree.
    procedure Clear( this : in out Tree'Class );

    -- Inserts line segment 'x1,y1-x2,y2' with attached 'item' into the tree.
    procedure Insert( this   : in out Tree'Class;
                      x1, y1 : Float;
                      x2, y2 : Float;
                      item   : Item_Type );

    -- Iterates over all segments intersecting rect 'x,y,w,h'. Calls 'examine'
    -- once for each item. Segments may be added or removed during iteration
    -- but they will not affect the remaining items during iteration.
    procedure Intersecting_Rect( this    : Tree'Class;
                                 x, y    : Float;
                                 w, h    : Float;
                                 examine : access procedure( x1, y1 : Float;
                                                             x2, y2 : Float;
                                                             item   : Item_Type ) );

    -- Iterates over all segments intersecting segment 'x1,y1-x2,y2'. Calls
    -- 'examine' once for each item. Segments may be added or removed during
    -- iteration but they will not affect the remaining items during iteration.
    procedure Intersecting_Segment( this    : Tree'Class;
                                    x1, y1  : Float;
                                    x2, y2  : Float;
                                    examine : access procedure( x1, y1 : Float;
                                                                x2, y2 : Float;
                                                                item   : Item_Type ) );

    -- Iterates over all segments in the tree, calling 'examine' once for each
    -- item. Segments may be added or removed during iteration but they will not
    -- affect the remaining items during iteration.
    procedure Iterate( this    : Tree'Class;
                       examine : access procedure( x1, y1 : Float;
                                                   x2, y2 : Float;
                                                   item   : Item_Type ) );

    -- Iterates over all leaves in the tree.
    procedure Iterate_Leaves( this    : Tree'Class;
                              examine : access procedure( x, y, w, h : Float ) );

    -- Removes all segments between 'x1,y1-x2,y2'.
    procedure Remove( this   : in out Tree'Class;
                      x1, y1 : Float;
                      x2, y2 : Float );

    -- Returns the number of elements in the quad tree.
    function Size( this : Tree'Class ) return Natural;

    -- Returns True if the tree is empty of items, otherwise False.
    function Is_Empty( this : Tree'Class ) return Boolean is (this.Size = 0);

private

    type Segment is
        record
            x1, y1 : Float;
            x2, y2 : Float;
        end record;

    type Rect is
        record
            x, y : Float := 0.0;
            w, h : Float := 0.0;
        end record;

    type Element is
        record
            seg  : Segment;
            item : Item_Type;
        end record;

    function "<"( l, r : Segment ) return Boolean;

    function "<"( l, r : Element ) return Boolean;

    function "="( l, r : Element ) return Boolean;

    package Element_Sets is new Ada.Containers.Ordered_Sets(Element, "<", "=");

    package Element_Vectors is new Ada.Containers.Vectors( Positive, Element, "=" );

    ----------------------------------------------------------------------------

    type Quad is (Top_Left, Bottom_Left, Top_Right, Bottom_Right);

    type Node;
    type A_Node is access all Node;

    type Node_Array;
    type A_Node_Array is access all Node_Array;

    ----------------------------------------------------------------------------

    type Node is
        record
            maxChildren : Natural := 0;
            depth       : Natural := Natural'Last;    -- remaining depth allowed
            bounds      : Rect := Rect'(0.0, 0.0, 1.0, 1.0);
            nodes       : A_Node_Array;
            children    : Element_Vectors.Vector;
        end record;

    procedure Delete( n : in out Node );

    -- Inserts line segment 'seg' with attached data 'item' into node 'n'.
    procedure Insert( n : in out Node; seg : Segment; item : Item_Type );

    -- Returns a unique set of segments intersecting rectangle 'r' in node 'n'.
    procedure Intersecting_Rect( n : Node; r : Rect; found : in out Element_Sets.Set );

    -- Returns a unique set of segments intersecting segment 'seg' in node 'n'.
    procedure Intersecting_Seg( n : Node; seg : Segment; found : in out Element_Sets.Set );

    -- Iterates over all elements in the tree.
    procedure Iterate( n : Node; found : in out Element_Sets.Set );

    -- Iterates over all leaves in the tree.
    procedure Iterate_Leaves( n       : Node;
                              examine : access procedure( x, y, w, h : Float ) );

    -- Removes all segments matching 'seg'.
    procedure Remove_Seg( n : in out Node; seg : Segment );

    -- Returns the number of children under this node.
    function Size( n : Node ) return Natural;

    -- Splits the node 'n' into four children with bounds of each of n's four
    -- quadrants.
    procedure Split( n : in out Node );

    type Node_Array is array (Quad) of Node;

    ----------------------------------------------------------------------------

    type Tree is new Limited_Controlled with
        record
            root : Node;
        end record;

    overriding
    procedure Initialize( this : in out Tree ) is null;

    overriding
    procedure Finalize( this : in out Tree );

end Quad_Trees;

