--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Mutexes is

    protected body Mutex is

        entry Lock when True is
        begin
            if owner = Lock'Caller then
                count := count + 1;
            else
                requeue Waiting with abort;
            end if;
        end Lock;

        ------------------------------------------------------------------------

        entry Waiting when count = 0 is
        begin
            owner := Waiting'Caller;
            count := 1;
        end Waiting;

        ------------------------------------------------------------------------

        procedure Unlock is
        begin
            pragma Assert( count > 0, "Mutex is not locked" );
            pragma Assert( owner = Current_Task, "Mutex owned by other thread" );
            count := count - 1;
        end Unlock;

    end Mutex;

end Mutexes;
