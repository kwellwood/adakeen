--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Doubly_Linked_Lists;
with Ada.Containers.Vectors;
with Ada.Unchecked_Deallocation;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Ada.Streams;                       use Ada.Streams;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Clipping;                          use Clipping;
with Mutexes;                           use Mutexes;
with Objects;                           use Objects;
with Values;                            use Values;
with Values.Maps;                       use Values.Maps;

package Tiles is

    type Fill_Mode is (Center, Stretch, StretchX, StretchY, Fit, Fill, Tiled);

    ----------------------------------------------------------------------------

    type Tile_Bits is mod 2**32;

    -- A special value for no tile bits set.
    TILE_INERT : constant Tile_Bits := 0;

    -- The tile blocks the camera from scrolling past it. These are used to
    -- bound the camera to a playing area.
    TILE_BLOCK_CAMERA : constant Tile_Bits := 2#00000000_00000000_0000000_00000001#;

    TILE_RESERVED1    : constant Tile_Bits := 2#00000000_00000000_0000000_00000010#;
    TILE_RESERVED2    : constant Tile_Bits := 2#00000000_00000000_0000000_00000100#;
    TILE_RESERVED3    : constant Tile_Bits := 2#00000000_00000000_0000000_00001000#;
    TILE_RESERVED4    : constant Tile_Bits := 2#00000000_00000000_0000000_00010000#;
    TILE_RESERVED5    : constant Tile_Bits := 2#00000000_00000000_0000000_00100000#;
    TILE_RESERVED6    : constant Tile_Bits := 2#00000000_00000000_0000000_01000000#;
    TILE_RESERVED7    : constant Tile_Bits := 2#00000000_00000000_0000000_10000000#;
    -- user type bits start at 2**8;

    ----------------------------------------------------------------------------

    -- An array of tile ids
    type Tile_Id_Array is array (Integer range <>) of Natural;
    type A_Tile_Id_Array is access all Tile_Id_Array;

    function A_Tile_Id_Array_Input( stream : access Root_Stream_Type'Class ) return A_Tile_Id_Array;
    for A_Tile_Id_Array'Input use A_Tile_Id_Array_Input;

    procedure A_Tile_Id_Array_Output( stream : access Root_Stream_Type'Class; tia : A_Tile_Id_Array );
    for A_Tile_Id_Array'Output use A_Tile_Id_Array_Output;

    -- Returns a copy of 'src'.
    function Copy( src : A_Tile_Id_Array ) return A_Tile_Id_Array;
    pragma Postcondition( Copy'Result /= src or else src = null );

    -- Deletes the Tile_Id_Array.
    procedure Delete is new Ada.Unchecked_Deallocation( Tile_Id_Array, A_Tile_Id_Array );

    package Tile_Id_Vectors is new Ada.Containers.Vectors( Positive, Natural, "=" );

    ----------------------------------------------------------------------------

    -- A Tile_Object represents a tile, the most basic element of a world map.
    -- It is uniquely identified within its library by an integer Id. Each tile
    -- is represented by a bitmap, can be animated using bitmaps from other
    -- tiles, and contains a set of attributes the define how entities interact
    -- with it in the world.
    type Tile_Object is new Limited_Object with private;
    type A_Tile is access all Tile_Object'Class;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- The following procedures should only be called during creation and
    -- configuration of the tile, not during gameplay when multiple threads may
    -- be reading the tile. These procedures are not thread-safe.

    -- Sets an attribute of the tile by name, by copy. While any attribute name
    -- can be used, the following have special meaning to the Oracle engine:
    --
    -- Reserved attributes:
    --
    --   Attribute(s)   Type      Description
    --   ------------   ----      -----------
    --   id             Number    The tile's id
    --   filename       String    The source image filename
    --
    -- Tilesheet attributes:
    --
    --   Attribute(s)   Type      Description
    --   ------------   ----      -----------
    --   x, y           Number    X and Y of tile's top left in source image
    --   w, h           Number    Width and height of tile in source image
    --   wh             Number    Width/height of square tile in source image
    --                            (overridden by 'w' and 'h' attributes)
    --
    -- Animation attributes: (map tiles, sprites, icons, etc.)
    --
    --   Attribute      Type      Description
    --   ---------      ----      -----------
    --   delay          Number    Animation frame delay (milliseconds)
    --   frames         List      List of tile ids in the animation loop
    --   next           Number    Tile id of next animation frame (single-shot)
    --
    -- Map tile attributes:
    --
    --   Attributes     Type      Description
    --   ----------     ----      -----------
    --   clip           String    Map clipping type (Clipping.Clip_Type enumeration)
    --   useAtlas       Boolean   Set False to disable tile atlas (for background scenery)
    --   typeBits       Number    Bitfield describing arbitrary binary properties
    --
    -- Sprite tile attributes:
    --
    --   Attributes     Type      Description
    --   ----------     ----      -----------
    --   offsetX        Number    X offset of sprite from entity (pixels)
    --   offsetY        Number    Y offset of sprite from entity (pixels)
    --   fillMode       String    Fill mode when frame doesn't match entity size
    --                            ("center" | "stretch" | "fill" | "fit" | "tiled")
    --   borderTop      Number    Width of sprite top border in source tile pixels (used for stretch and tiled fill modes)
    --   borderBottom   Number    Width of sprite bottom border in source tile pixels (used for stretch and tiled fill modes)
    --   borderLeft     Number    Width of sprite left border in source tile pixels (used for stretch and tiled fill modes)
    --   borderRight    Number    Width of sprite right border in source tile pixels (used for stretch and tiled fill modes)
    procedure Set_Attribute( this : not null access Tile_Object'Class;
                             name : String;
                             val  : Value'Class );
    pragma Precondition( name'Length > 0 );

    -- Sets the id of the tile. This should be called only once, during creation
    -- of the tile. Tile ids are unique within a library. Adding this tile to a
    -- library that already contains a tile with the same id will result in an
    -- error.
    procedure Set_Id( this : access Tile_Object; id : Natural );

    -- Sets the name (bitmap filename) of the tile. Tile names are not unique
    -- within a library.
    procedure Set_Name( this : access Tile_Object; name : String );
    pragma Precondition( name'Length > 0 );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- The following procedures are thread-safe and may be called at any time.

    -- Returns the value (by copy) of the 'name' attribute of the tile, or Null
    -- if the attribute has not been defined. See Set_Attribute() for
    -- descriptions of tile attributes with special meaning. This is safe to
    -- call with a null Tile pointer.
    function Get_Attribute( this : access Tile_Object'Class;
                            name : String ) return Value;

    -- Returns all of the tile's attributes (by copy). This is safe to call with
    -- a null Tile pointer. A map will always be returned, although it will be
    -- empty if 'this' is null. In addition to the standard attributes, the
    -- returned map will also contain "id" and "filename".
    function Get_Attributes( this : access Tile_Object'Class ) return Map_Value;
    pragma Postcondition( Get_Attributes'Result.Valid );

    -- Returns the tile's bitmap, or null if it hasn't been loaded yet. This is
    -- safe to call with a null Tile pointer.
    function Get_Bitmap( this : access Tile_Object'Class ) return A_Allegro_Bitmap;

    -- Returns the height of the tile's bitmap, or 0.0 if it hasn't been loaded
    -- yet. This is safe to call with a null Tile pointer.
    function Get_Height( this : access Tile_Object'Class ) return Float;

    -- Returns the id of the tile within its library or 0 if 'this' is null.
    -- This is safe to call with a null Tile pointer.
    function Get_Id( this : access Tile_Object'Class ) return Natural;

    -- Returns the tile's white mask bitmap. The mask will only exist if an
    -- attribute has been defined that implies a mask. Null will be returned if
    -- the tile doesn't have a mask or the tile's bitmap has not been loaded
    -- yet. This is safe to call with a null Tile pointer.
    function Get_Mask( this : access Tile_Object'Class ) return A_Allegro_Bitmap;

    -- Returns the name (bitmap filename) of the tile. Tile names are not unique
    -- within a library.
    function Get_Name( this : not null access Tile_Object'Class ) return String;

    -- Returns the width of the tile's bitmap, or 0.0 if it hasn't been loaded
    -- yet. This is safe to call with a null Tile pointer.
    function Get_Width( this : access Tile_Object'Class ) return Float;

    -- Returns True if the tile's bitmap has been loaded, successfully or
    -- otherwise.
    function Is_Loaded( this : not null access Tile_Object'Class ) return Boolean;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Cached Attributes (avoids tree lookup in Get_Attribute())

    -- Returns the border width along an edge of the tile. These are used when
    -- drawing tiles with certain fill modes.
    function Get_BorderBottom( this : not null access Tile_Object'Class ) return Float;
    function Get_BorderLeft  ( this : not null access Tile_Object'Class ) return Float;
    function Get_BorderRight ( this : not null access Tile_Object'Class ) return Float;
    function Get_BorderTop   ( this : not null access Tile_Object'Class ) return Float;

    -- Returns the tile's animation delay in milliseconds. The tile is animated
    -- when this is non-zero.
    function Get_Delay( this : not null access Tile_Object'Class ) return Integer;

    -- Returns the tile's fill mode- the way the tile is drawn when filling a
    -- larger space than the tile's native resolution.
    function Get_FillMode( this : not null access Tile_Object'Class ) return Fill_Mode;

    -- Returns the tile's emissive light level. All non-transparent pixels will
    -- emit this much light.
    function Get_Light( this : not null access Tile_Object'Class ) return Float;

    -- Returns the tile id of the tile's emissive light map, or zero if none.
    function Get_LightMap( this : not null access Tile_Object'Class ) return Integer;

    -- Returns the position offset to use when drawing the tile's emissive light
    -- map.
    function Get_LightMapX( this : not null access Tile_Object'Class ) return Integer;
    function Get_LightMapY( this : not null access Tile_Object'Class ) return Integer;

    -- Returns the position offset to use when drawing the tile.
    function Get_OffsetX( this : not null access Tile_Object'Class ) return Float;
    function Get_OffsetY( this : not null access Tile_Object'Class ) return Float;

    -- Returns the clipping shape of the tile when used in a solid map layer.
    function Get_Shape( this : access Tile_Object'Class ) return Clip_Type;

    -- Returns the typeBits bitmask of the tile.
    function Get_TypeBits( this : not null access Tile_Object'Class ) return Tile_Bits;

    -- Returns True if all 'bits' are set in the typeBits attribute.
    function Has_TypeBits( this : not null access Tile_Object'Class; bits : Tile_Bits ) return Boolean;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Deletes the Tile.
    procedure Delete( this : in out A_Tile );
    pragma Postcondition( this = null );

    ATTRIBUTE_ERROR : exception;

    -- NULL_TILE_ID designates an empty tile
    NULL_TILE_ID : constant := 0;

    -- ANON_TILE_ID designates tiles created by the tile library compiler as
    -- they are parsed from the tile index without an explicit id. the ids of
    -- ANON_TILE_ID tiles ids are assigned before writing the binary catalog
    -- file, after all other tile ids are known.
    ANON_TILE_ID : constant := Natural'Last;

private

    ----------------------------------------------------------------------------

    -- A Completion is a state object useful for synchronizing the start of an
    -- asynchronous operation and blocking threads until it has completed.
    protected type Completion is

        -- Atomically notifies the object that the thread would like to begin
        -- the associated operation, and returns a value indicating if the
        -- operation can begin.
        --
        -- If 'success' is True, the caller should now start the Completion's
        -- associated operation because no other thread has started it yet. If
        -- 'success' is False, the operation has already been started or
        -- completed, and cannot be started now.
        procedure Start( success : out Boolean );

        -- Notifies the object that the state is complete. This will unblock
        -- all threads blocked on the Wait entry. A prior call to Start must
        -- have been made, to indicate the operation had started.
        procedure Notify_Complete;

        -- Checks if the object's associated operation has been completed,
        -- without waiting for it to complete. Call Wait to block until the
        -- operation is complete.
        function Is_Complete return Boolean;

        -- Blocks the caller until the state becomes complete.
        entry Wait;

    private
        started  : Boolean := False;
        complete : Boolean := False;
    end Completion;

    ----------------------------------------------------------------------------

    type Tile_Object is new Limited_Object with
        record
            -- ** these fields are streamed **
            id         : Natural := 0;
            name       : Unbounded_String;
            attributes : Map_Value;
            -- ** end streamed fields **

            lock      : A_Mutex;                     -- protects .attributes
            loadState : Completion;                  -- protects .bmp and .mask
            bmp       : A_Allegro_Bitmap := null;
            mask      : A_Allegro_Bitmap := null;

            -- cached attributes
            borderTop    : Float := 0.0;
            borderBottom : Float := 0.0;
            borderLeft   : Float := 0.0;
            borderRight  : Float := 0.0;
            dlay         : Integer := 0;
            fillMode     : Fill_Mode := Center;
            light        : Float := 0.0;
            lightMap     : Integer := 0;
            lightMapX    : Integer := 0;
            lightMapY    : Integer := 0;
            offsetX      : Float := 0.0;
            offsetY      : Float := 0.0;
            shape        : Clip_Type := Passive;
            typeBits     : Tile_Bits := TILE_INERT;
        end record;

    -- Creates a new tile using register allocator. Returns null if no allocator
    -- has been registered.
    function Create_Tile return A_Tile;

    procedure Construct( this : access Tile_Object );

    procedure Delete( this : in out Tile_Object );

    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Tile_Object );
    for Tile_Object'Read use Object_Read;

    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Tile_Object );
    for Tile_Object'Write use Object_Write;

    -- Returns True if the tile has an attribute that requires a mask bitmap.
    function Is_Mask_Required( this : not null access Tile_Object'Class ) return Boolean;

    -- Sets the tile's bitmaps in a thread-safe manner. 'bmp' and 'mask' will be
    -- consumed.
    procedure Set_Bitmap( this : not null access Tile_Object'Class;
                          bmp  : in out A_Allegro_Bitmap;
                          mask : in out A_Allegro_Bitmap );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    function A_Tile_Input( stream : access Root_Stream_Type'Class ) return A_Tile;
    for A_Tile'Input use A_Tile_Input;

    procedure A_Tile_Output( stream : access Root_Stream_Type'Class; tile : A_Tile );
    for A_Tile'Output use A_Tile_Output;

    -- Defines a list of Tile objects.
    package Tile_Lists is new Ada.Containers.Doubly_Linked_Lists( A_Tile, "=" );

end Tiles;
