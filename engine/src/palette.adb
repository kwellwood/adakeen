--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Interpolation;                     use Interpolation;
with Support;                           use Support;

package body Palette is

    function Compare( x, y : Allegro_Color; tolerance : Natural := 0 ) return Boolean is
        xR, xG, xB, xA : Unsigned_8;
        yR, yG, yB, yA : Unsigned_8;
    begin
        if x = y then
            return True;
        end if;

        Al_Unmap_RGBA( x, xR, xG, xB, xA );
        Al_Unmap_RGBA( y, yR, yG, yB, yA );

        return abs (Integer(xR) - Integer(yR)) <= Integer(tolerance) and then
               abs (Integer(xG) - Integer(yG)) <= Integer(tolerance) and then
               abs (Integer(xB) - Integer(yB)) <= Integer(tolerance) and then
               abs (Integer(xA) - Integer(yA)) <= Integer(tolerance);
    end Compare;

    ----------------------------------------------------------------------------

    function Compare( x, y : Allegro_Color; tolerance : Float := 0.0 ) return Boolean is
        xR, xG, xB, xA : Float;
        yR, yG, yB, yA : Float;
    begin
        if x = y then
            return True;
        end if;

        Al_Unmap_RGBA_f( x, xR, xG, xB, xA );
        Al_Unmap_RGBA_f( y, yR, yG, yB, yA );

        return abs (xR - yR) <= tolerance and then
               abs (xG - yG) <= tolerance and then
               abs (xB - yB) <= tolerance and then
               abs (xA - yA) <= tolerance;
    end Compare;

    ----------------------------------------------------------------------------

    function Contrast( fg, bg : Allegro_Color; ratio : Float := 1.75 ) return Allegro_Color is

        ------------------------------------------------------------------------

        function Luminance( c : Allegro_Color ) return Float is
            cR, cG, cB, cA : Float;
        begin
            Al_Unmap_RGBA_f( c, cR, cG, cB, cA );
            return (0.2126 * (cR**2.2) +
                    0.7152 * (cG**2.2) +
                    0.0722 * (cB**2.2) + 0.05) / 1.05;
        end Luminance;

        ------------------------------------------------------------------------

        function Luminance_Adjust( c : Allegro_Color; val : Float ) return Allegro_Color is

            function Adjust( v : Float; c : Float ) return Float
            is (Float'Max(0.0, Float'Min(1.0, v - val * c)));

            r, g, b, a : Float;
        begin
            Al_Unmap_RGBA_f( c, r, g, b, a );
            return Al_Map_RGBA_f( Adjust( r, 0.2126 ),
                                  Adjust( g, 0.7152 ),
                                  Adjust( b, 0.0722 ),
                                  a );
        end Luminance_Adjust;

        ------------------------------------------------------------------------

        bgLum : constant Float := Luminance( bg );
        fgLum : constant Float := Luminance( fg );
    begin
        if Float'Max( bgLum, fgLum ) / Float'Min( bgLum, fgLum ) > ratio then
            return fg;
        end if;
        return Luminance_Adjust( fg, Float'Min( Float'Max( bgLum, fgLum ), 1.0 ) );
    end Contrast;

    ----------------------------------------------------------------------------

    function Get_Alpha( color : Allegro_Color ) return Unsigned_8 is
        r, g, b, a : Unsigned_8;
    begin
        Al_Unmap_RGBA( color, r, g, b, a );
        return a;
    end Get_Alpha;

    ----------------------------------------------------------------------------

    function Get_Alpha( color : Allegro_Color ) return Float is
        r, g, b, a : Float;
    begin
        Al_Unmap_RGBA_f( color, r, g, b, a );
        return a;
    end Get_Alpha;

    ----------------------------------------------------------------------------

    function Hard_Contrast( bg : Allegro_Color; preserveAlpha : Boolean := True ) return Allegro_Color is

        function Luminance( r, g, b : Float ) return Float is (0.3 * r + 0.59 * g + 0.11 * b);

        r, g, b, a : Float;
    begin
        Al_Unmap_RGBA_f( bg, r, g, b, a );
        if not preserveAlpha then
            a := 1.0;
        end if;
        return (if Luminance( r, g, b ) <= 0.5 then Al_Map_RGBA_f( 1.0, 1.0, 1.0, a ) else Al_Map_RGBA_f( 0.0, 0.0, 0.0, a ));
    end Hard_Contrast;

    ----------------------------------------------------------------------------

    function Html_To_Color( html : String; default : Allegro_Color ) return Allegro_Color is

        function From_Hex( hex : String ) return Unsigned_8 is
            result : Unsigned_8 := 0;
        begin
            case hex(hex'First) is
                when '0'..'9' => result := Shift_Left( Unsigned_8(0  + Character'Pos( hex(hex'First) ) - Character'Pos( '0' )), 4 );
                when 'A'..'F' => result := Shift_Left( Unsigned_8(10 + Character'Pos( hex(hex'First) ) - Character'Pos( 'A' )), 4 );
                when 'a'..'f' => result := Shift_Left( Unsigned_8(10 + Character'Pos( hex(hex'First) ) - Character'Pos( 'a' )), 4 );
                when others => return 255;
            end case;
            case hex(hex'First+1) is
                when '0'..'9' => result := result or Unsigned_8(0  + Character'Pos( hex(hex'First+1) ) - Character'Pos( '0' ));
                when 'A'..'F' => result := result or Unsigned_8(10 + Character'Pos( hex(hex'First+1) ) - Character'Pos( 'A' ));
                when 'a'..'f' => result := result or Unsigned_8(10 + Character'Pos( hex(hex'First+1) ) - Character'Pos( 'a' ));
                when others => return 255;
            end case;
            return result;
        end From_Hex;

        r, g, b, a : Unsigned_8 := 255;
    begin
        if html'Length > 0 and then html(html'First) /= '#' then
            if Case_Eq( html, "white" ) then
                return White;
            elsif Case_Eq( html, "black" ) then
                return Black;
            elsif Case_Eq( html, "transparent" ) then
                return Transparent;
            end if;
            return default;
        elsif html'Length /= 7 and then html'Length /= 9 then
            return default;
        end if;

        r := From_Hex( html(html'First+1..html'First+2) );
        g := From_Hex( html(html'First+3..html'First+4) );
        b := From_Hex( html(html'First+5..html'First+6) );
        if html'Length > 7 then
            a := From_Hex( html(html'First+7..html'First+8) );
        end if;
        return Al_Map_RGBA( r, g, b, a );
    end Html_To_Color;

    ----------------------------------------------------------------------------

    function Color_To_Html( color : Allegro_Color; alpha : Boolean := True ) return String is

        procedure To_Hex( str : in out String; channel : Unsigned_8 ) is
            hex : constant String := "0123456789ABCDEF";
        begin
            str(str'First) := hex(hex'First + Integer(Shift_Right( channel, 4 )));
            str(str'Last)  := hex(hex'First + Integer(channel and 16#0F#));
        end To_Hex;

        result     : String(1..9) := "#RRGGBBAA";
        r, g, b, a : Unsigned_8 := 255;
    begin
        Al_Unmap_RGBA( color, r, g, b, a );
        To_Hex( result(2..3), r );
        To_Hex( result(4..5), g );
        To_Hex( result(6..7), b );
        if not alpha or a = 255 then
            return result(1..7);
        end if;
        To_Hex( result(8..9), a );
        return result;
    end Color_To_Html;

    ----------------------------------------------------------------------------

    function Interpolate( first, last : Allegro_Color;
                          progress    : Float ) return Allegro_Color is
        r1, g1, b1, a1 : Float;
        r2, g2, b2, a2 : Float;
    begin
        Al_Unmap_RGBA_f( first, r1, g1, b1, a1 );
        Al_Unmap_RGBA_f( last, r2, g2, b2, a2 );
        return Al_Map_RGBA_f( Interpolate( r1, r2, progress ),
                              Interpolate( g1, g2, progress ),
                              Interpolate( b1, b2, progress ),
                              Interpolate( a1, a2, progress ) );
    end Interpolate;

    ----------------------------------------------------------------------------

    function Is_Opaque( color : Allegro_Color ) return Boolean is (Get_Alpha( color ) >= 1.0);

    ----------------------------------------------------------------------------

    function Lighten( color : Allegro_Color; factor : Float ) return Allegro_Color is
        r, g, b, a : Float;
    begin
        Al_Unmap_RGBA_f( color, r, g, b, a );
        return Al_Map_RGBA_f( Float'Min( r * factor, 1.0 ),
                              Float'Min( g * factor, 1.0 ),
                              Float'Min( b * factor, 1.0 ),
                              a );
    end Lighten;

    ----------------------------------------------------------------------------

    function Whiten( color : Allegro_Color; factor : Float ) return Allegro_Color
    is (Interpolate( color, Opacity( White, Get_Alpha( color ) ), factor ));

    ----------------------------------------------------------------------------

    function Make_Grey( brightness : Natural; alpha : Unsigned_8 := 255 ) return Allegro_Color is
        b : constant Unsigned_8 := Unsigned_8(Constrain( brightness, 0, 255 ));
    begin
        return Al_Map_RGBA( b, b, b, alpha );
    end Make_Grey;

    ----------------------------------------------------------------------------

    function Make_Grey( brightness : Float; alpha : Float := 1.0 ) return Allegro_Color is
        b : constant Float := Constrain( brightness, 0.0, 1.0 );
    begin
        return Al_Map_RGBA_f( b, b, b, alpha );
    end Make_Grey;

    ----------------------------------------------------------------------------

    function Multiply( a, b : Allegro_Color ) return Allegro_Color is
        r1, g1, b1, a1 : Float;
        r2, g2, b2, a2 : Float;
    begin
        Al_Unmap_RGBA_f( a, r1, g1, b1, a1 );
        Al_Unmap_RGBA_f( b, r2, g2, b2, a2 );
        return Al_Map_RGBA_f( r1 * r2, g1 * g2, b1 * b2, a1 * a2 );
    end Multiply;

    ----------------------------------------------------------------------------

    function Opacity( color : Allegro_Color; alpha : Float ) return Allegro_Color is
        r, g, b : Float;
    begin
        Al_Unmap_RGB_f( color, r, g, b );
        return Al_Map_RGBA_f( r, g, b, alpha );
    end Opacity;

end Palette;
