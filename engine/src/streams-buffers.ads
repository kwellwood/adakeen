--
-- Copyright (c) 2012 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Streams.Buffers is

    -- A Buffer_Stream is a stream implementation backed by a read-only buffer.
    -- The stream can be wrapped around an existing buffer to read its contents
    -- (use Stream constructor function), or a buffer can be consumed into the
    -- stream and freed automatically when the stream is closed (use Open_Stream
    -- constructor procedure).
    type Buffer_Stream is new Root_Stream_Type with private;
    type A_Buffer_Stream is access all Buffer_Stream'Class;

    -- Creates a Buffer_Stream backed by 'buffer', which is consumed by this
    -- procedure. Closing the stream will deallocate the buffer.
    procedure Open_Stream( buffer : in out A_SEA; stream : out A_Buffer_Stream );
    pragma Precondition( buffer /= null );
    pragma Postcondition( buffer = null );
    pragma Postcondition( stream /= null );

    -- Creates a Buffer_Stream wrapper around 'buffer'. The stream depends on
    -- the existance of buffer so be sure to call Close on the stream before
    -- deallocating the buffer.
    function Stream( buffer : not null A_SEA ) return A_Buffer_Stream;
    pragma Postcondition( Stream'Result /= null );

    -- Creates a Buffer_Stream wrapper around a copy of 'buffer'. It is
    -- recommended that only Characters be read from this type of buffer stream.
    function Stream( buffer : String ) return A_Buffer_Stream;
    pragma Postcondition( Stream'Result /= null );

    -- Closes the stream and deallocates its buffer (if it had ownership of it,
    -- given by calling Open_Stream.)
    procedure Close( stream : in out A_Buffer_Stream );
    pragma Postcondition( stream = null );

private

    type Buffer_Stream is new Root_Stream_Type with record
        buffer  : A_SEA := null;
        freeBuf : Boolean := False;  -- True if the stream's underlying
                                     -- buffer was consumed by the stream and
                                     -- should be freed when the stream is
                                     -- closed.
        readPos : Stream_Element_Offset;
    end record;

    -- 'last' will be set to item'First - 1 when the stream is empty.
    procedure Read( stream : in out Buffer_Stream;
                    item   : out Stream_Element_Array;
                    last   : out Stream_Element_Offset );

    -- Writing is not supported. Raises STREAM_ERROR.
    procedure Write( stream : in out Buffer_Stream;
                     item   : Stream_Element_Array );

end Streams.Buffers;
