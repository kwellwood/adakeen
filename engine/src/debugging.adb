--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Ada.Task_Identification;           use Ada.Task_Identification;
with Ada.Text_IO;
with GNAT.OS_Lib;
with Mutexes;                           use Mutexes;
with Version;

#if WINDOWS'Defined then
with GNAT.Traceback;                    use GNAT.Traceback;
with GNAT.Traceback.Symbolic;           use GNAT.Traceback.Symbolic;
#end if;

package body Debugging is

    type Channel_Verbosity is array (Verbosity_Level) of Debug_Channel;

    -- Protects all other package variables
    lock : Mutex;

    decoration : Decoration_Level := NONE;

    -- for RELEASE builds, all channels default to "Error" verbosity.
    -- for DEBUG builds, all channels default to "Warning" verbosity.
    -- the D_DEV channel defaults to maximum verbosity.
    verbosity : Channel_Verbosity := (Never   => D_DEV or ALL_CHANNELS,
                                      Error   => D_DEV or ALL_CHANNELS,
                                      Warning => D_DEV or (if Version.Is_Debug then ALL_CHANNELS else NO_CHANNELS),
                                      others  => D_DEV);

    -- additional channels to which all messages should be added
    extraChans : Debug_Channel := NO_CHANNELS;

    output  : Output_Proc := null;
    logPath : Unbounded_String;
    logFile : Ada.Text_IO.File_Type;

    -- caches the previous debugging output if no output procedure is set. the
    -- cache will be dumped to the output procedure when it is set.
    outputBuffer     : Unbounded_String;
    BUFFER_CAPACITY  : constant := 2048;

    ----------------------------------------------------------------------------

    procedure Write( str     : String;
                     channel : Debug_Channel;
                     level   : Verbosity_Level;
                     entity  : String );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    procedure Write( str     : String;
                     channel : Debug_Channel;
                     level   : Verbosity_Level;
                     entity  : String ) is
        ustr : Unbounded_String;
    begin
        -- check the verbosity filter
        if (verbosity(level) and (channel or extraChans)) /= NO_CHANNELS then

            -- prefix decorations
            if decoration >= TASK_IMAGE then
                ustr := To_Unbounded_String( '[' & Image( Current_Task ) & ']' );
                if decoration >= SOURCE_UNIT then
                    Append( ustr, ' ' & entity );
                end if;
                Append( ustr, ": " );
            end if;

            -- prefix "Error" / "Warning" label
            case level is
                when Error   => Append( ustr, "Error: " );
                when Warning => Append( ustr, "Warning: " );
                when others  => null;
            end case;
            Append( ustr, str );

            -- write to the output procedure
            if output /= null then
                output.all( To_String( ustr ) );
            else
                Append( outputBuffer, ustr );
                Append( outputBuffer, ASCII.LF );
                if Length( outputBuffer ) > BUFFER_CAPACITY then
                    Tail( outputBuffer, BUFFER_CAPACITY );
                end if;
            end if;

            -- write to the log file
            if Ada.Text_IO.Is_Open( logFile ) then
                Ada.Text_IO.Put_Line( logFile, To_String( ustr ) );
            end if;
        end if;
    end Write;

    ----------------------------------------------------------------------------

    procedure Dbg( str      : String;
                   channel  : Debug_Channel := D_DEV;
                   level    : Verbosity_Level := Never;
                   entity   : String := GNAT.Source_Info.Enclosing_Entity;
                   location : String := GNAT.Source_Info.Source_Location ) is
        pragma Unreferenced( location );
    begin
        lock.Lock;
        Write( str, channel, level, entity );
        lock.Unlock;
    end Dbg;

    ----------------------------------------------------------------------------

    procedure Dbg( e        : Exception_Occurrence;
                   channel  : Debug_Channel := D_DEV;
                   level    : Verbosity_Level := Never;
                   entity   : String := GNAT.Source_Info.Enclosing_Entity;
                   location : String := GNAT.Source_Info.Source_Location ) is
        pragma Warnings( Off, location );
    begin
        lock.Lock;
        Write( "Exception: " & Exception_Name( e ), channel, level, entity );
        Write( "Message  : " & Exception_Message( e ), channel, level, entity );
#if WINDOWS'Defined then
        Write( "Traceback: ...", channel, level, entity );
        Write( Symbolic_Traceback( e ), channel, level, entity );
#else
        -- Windows is the only compiler with a traceback, it seems
        Write( "Traceback: <unavailable>", channel, level, entity );
#end if;
        lock.Unlock;
    end Dbg;

    ----------------------------------------------------------------------------

    procedure Disable_Debug_Channels( channels : Debug_Channel ) is
    begin
        lock.Lock;
        extraChans := extraChans and not channels;
        lock.Unlock;
    end Disable_Debug_Channels;

    ----------------------------------------------------------------------------

    procedure Enable_Debug_Channels( channels : Debug_Channel ) is
    begin
        lock.Lock;
        extraChans := extraChans or channels;
        lock.Unlock;
    end Enable_Debug_Channels;

    ----------------------------------------------------------------------------

    procedure Set_Log_File( filePath : String ) is
    begin
        lock.Lock;
        if Ada.Text_IO.Is_Open( logFile ) then
            Ada.Text_IO.Close( logFile );
        end if;
        logPath := To_Unbounded_String( filePath );
        if Length( logPath ) > 0 then
            begin
                if GNAT.OS_Lib.Is_Regular_File( To_String( logPath ) ) then
                    Ada.Text_IO.Open( logFile,
                                      Ada.Text_IO.Append_File,
                                      To_String( logPath ) );
                else
                    Ada.Text_IO.Create( logFile,
                                        Ada.Text_IO.Append_File,
                                        To_String( logPath ) );
                end if;
                Dbg( "Writing log '" & To_String( logPath ) & "'",
                     D_LOG, Info,
                     GNAT.Source_Info.Enclosing_Entity,
                     GNAT.Source_Info.Source_Location );
            exception
                when others =>
                    Dbg( "Failed to create log '" & To_String( logPath ) & "'",
                         D_LOG, Error,
                         GNAT.Source_Info.Enclosing_Entity,
                         GNAT.Source_Info.Source_Location );
            end;
        end if;
        lock.Unlock;
    end Set_Log_File;

    ----------------------------------------------------------------------------

    procedure Set_Decoration( level : Decoration_Level ) is
    begin
        lock.Lock;
        decoration := level;
        lock.Unlock;
    end Set_Decoration;

    ----------------------------------------------------------------------------

    procedure Set_Output( proc : Output_Proc ) is
    begin
        lock.Lock;
        if output = null and proc /= null and Length( outputBuffer ) > 0 then
            proc.all( To_String( outputBuffer ) );
            outputBuffer := Null_Unbounded_String;
        end if;
        output := proc;
        lock.Unlock;
    end Set_Output;

    ----------------------------------------------------------------------------

    procedure Set_Verbosity( channels : Debug_Channel; level : Verbosity_Level ) is
    begin
        lock.Lock;
        for v in Verbosity_Level'Range loop
            if v <= level then
                -- enable 'channels' for this level of verbosity
                verbosity(v) := verbosity(v) or channels;
            else
                -- disable 'channels' because this level 'v' is too verbose
                verbosity(v) := verbosity(v) and not channels;
            end if;
        end loop;
        lock.Unlock;
    end Set_Verbosity;

end Debugging;
