--
-- Copyright (c) 2015-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Signals.Simple is

    procedure Connect( this : in out String_Signal'Class; slot : String_Connection'Class ) is
    begin
        Base_Signal(this).Connect( slot );
    end Connect;

    ----------------------------------------------------------------------------

    procedure Emit( this : String_Signal'Class; str : String ) is
        conns : constant Connection_Lists.List := this.conns.Copy;
    begin
        for c of conns loop
            String_Connection'Class(c).Emit( this.sender, str );
            exit when c.priority = Exclusive;
        end loop;
    end Emit;

    --==========================================================================

    package body Connections is

        function Slot( obj      : in out Target'Class;
                       method   : not null A_String_Slot;
                       priority : Priority_Type := Normal ) return String_Connection'Class is
            result : aliased Generic_String_Connection;
        begin
            result.Construct( priority );
            result.object := obj'Unchecked_Access;              -- UNSAFE!
            result.method := method;
            return result;
        end Slot;

        ----------------------------------------------------------------------------

        overriding
        procedure Emit( this   : Generic_String_Connection;
                        sender : access Base_Object'Class;
                        str    : String ) is
        begin
            this.object.Push_Signaller( sender );
            this.method( this.object, str );
            this.object.Pop_Signaller;
        end Emit;

        ----------------------------------------------------------------------------

        overriding
        function Eq( this : Generic_String_Connection; other : Base_Connection'Class ) return Boolean
            is (other in Generic_String_Connection'Class and then
                this.object = Generic_String_Connection(other).object and then
                this.method = Generic_String_Connection(other).method);

    end Connections;

end Signals.Simple;
