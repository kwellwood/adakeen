--
-- Copyright (c) 2015-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Palette;                           use Palette;
with Tiles;                             use Tiles;

package Drawing.Tiles is

    -- Draws 'tile' at the given location and size using the tile's fillMode
    -- attribute to determine how to fill the 'width,height' area.
    procedure Draw_Tile_Filled( tile          : A_Tile;
                                x, y, z       : Float;
                                width, height : Float;
                                tint          : Allegro_Color := White;
                                blendMode     : Blend_Mode := Current );

end Drawing.Tiles;
