--
-- Copyright (c) 2013-2015 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Styles is

    function Align_Horizontal( alignment      : Align_Type;
                               containerWidth : Float;
                               childWidth     : Float ) return Float is
    (
        case alignment is
            when Top_Left   | Left   | Bottom_Left   => 0.0,
            when Top_Center | Center | Bottom_Center => (containerWidth - childWidth) / 2.0,
            when Top_Right  | Right  | Bottom_Right  => (containerWidth - childWidth)
    );

    ----------------------------------------------------------------------------

    procedure Align_Rect( alignment                       : Align_Type;
                          containerWidth, containerHeight : Float;
                          childWidth, childHeight         : Float;
                          childX, childY                  : out Float ) is
    begin
        childX := Align_Horizontal( alignment, containerWidth, childWidth );
        childY := Align_Vertical( alignment, containerHeight, childHeight );
    end Align_Rect;

    ----------------------------------------------------------------------------

    function Align_Vertical( alignment       : Align_Type;
                             containerHeight : Float;
                             childHeight     : Float ) return Float is
    (
        case alignment is
            when Top_Left    | Top_Center    | Top_Right    => 0.0,
            when Center      | Left          | Right        => (containerHeight - childHeight) / 2.0,
            when Bottom_Left | Bottom_Center | Bottom_Right => (containerHeight - childHeight)
    );

end Styles;

