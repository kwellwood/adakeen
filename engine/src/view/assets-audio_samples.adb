--
-- Copyright (c) 2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Resources;                         use Resources;
with Allegro.File_IO;                   use Allegro.File_IO;
with Support.Paths;                     use Support.Paths;
with Values.Strings;                    use Values.Strings;

package body Assets.Audio_Samples is

    SAMPLE_EXTENSION : constant String := "wav";

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Load_Audio_Sample( filePath : String ) return A_Audio_Sample is
        lpath   : constant String := Add_Extension( To_Lower( filePath ), SAMPLE_EXTENSION );
        assetId : Value;
        asset   : A_Asset;
        this    : A_Audio_Sample;
    begin
        assetId := Create( lpath );
        asset := Find_Asset( assetId );
        if asset /= null then
            -- found in cache
            pragma Assert( asset.Get_Type = Audio_Sample_Assets,
                           "Cached asset '" & filePath & "' is not an audio sample" );
            this := A_Audio_Sample(asset);
        else
            -- create a new asset
            this := new Audio_Sample;
            this.Construct( assetId, lpath );
            Store_Asset( A_Asset(this) );
        end if;
        return this;
    end Load_Audio_Sample;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this     : access Audio_Sample;
                         assetId  : Value'Class;
                         filePath : String ) is
    begin
        Asset(this.all).Construct( Audio_Sample_Assets, assetId, filePath, "audio" );
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Allegro_Sample( this : not null access Audio_Sample'Class ) return A_Allegro_Sample is
    begin
        return this.alSample;
    end Get_Allegro_Sample;

    ----------------------------------------------------------------------------

    overriding
    function Load_Data( this : access Audio_Sample ) return Boolean is
        res    : A_Resource_File;
        alFile : A_Allegro_File;
    begin
        -- load the file resources
        res := Load_Resource( To_String( this.filePath ), To_String( this.group ) );
        if res /= null then
            -- load the audio from the file
            alFile := res.Create_Allegro_File;
            this.alSample := Al_Load_Sample_f( alFile, "." & Get_Extension( To_String( this.filePath ) ) );
            Al_Fclose( alFile );
            Delete( res );
        end if;
        return this.alSample /= null;
    end Load_Data;

    ----------------------------------------------------------------------------

    overriding
    procedure Unload_Data( this : access Audio_Sample ) is
    begin
        Al_Destroy_Sample( this.alSample );
    end Unload_Data;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Audio_Sample ) is
    begin
        Delete( A_Asset(this) );
    end Delete;

end Assets.Audio_Samples;
