--
-- Copyright (c) 2015-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Keyboard;                          use Keyboard;
with Mouse;                             use Mouse;

package Signals.Mouse is

    ----------------------------------------------------------------------------
    -- Mouse_Connection Class --

    type Mouse_Connection is abstract new Base_Connection with null record;

    type Mouse_Arguments is
        record
            x, y : Float;                       -- relative to the widget's content
        end record;

    not overriding
    procedure Emit( this   : Mouse_Connection;
                    sender : access Base_Object'Class;
                    args   : Mouse_Arguments ) is abstract;

    ----------------------------------------------------------------------------
    -- Mouse_Signal Class --

    type Mouse_Signal is new Base_Signal with private;

    procedure Connect( this : in out Mouse_Signal'Class; slot : Mouse_Connection'Class );

    procedure Emit( this : Mouse_Signal'Class; x, y : Float );

    ----------------------------------------------------------------------------
    -- Button_Connection Class --

    type Button_Connection is abstract new Base_Connection with
        record
            button    : Mouse_Button := Mouse_Any;
            modifiers : Modifiers_Pattern := MODIFIERS_ANY;
        end record;

    type Button_Arguments is
        record
            x, y      : Float;                  -- relative to the widget's content
            button    : Mouse_Button;           -- initiating mouse button
            modifiers : Modifiers_Array;        -- active modifiers
        end record;

    not overriding
    procedure Emit( this    : Button_Connection;
                    sender  : access Base_Object'Class;
                    args    : Button_Arguments;
                    handled : out Boolean ) is abstract;

    ----------------------------------------------------------------------------
    -- Button_Signal Class --

    type Button_Signal is new Base_Signal with private;

    procedure Connect( this : in out Button_Signal'Class; slot : Button_Connection'Class );

    procedure Emit( this      : Button_Signal'Class;
                    button    : Mouse_Button;
                    modifiers : Modifiers_Array;
                    x, y      : Float );

    ----------------------------------------------------------------------------
    -- Scroll_Connection Class --

    type Scroll_Connection is abstract new Base_Connection with null record;

    type Scroll_Arguments is
        record
            x, y      : Float;                  -- relative to the widget's content
            scrollAmt : Float;                  -- scroll distance (pixels)
        end record;

    not overriding
    procedure Emit( this    : Scroll_Connection;
                    sender  : access Base_Object'Class;
                    args    : Scroll_Arguments;
                    handled : out Boolean ) is abstract;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Scroll_Signal Class --

    type Scroll_Signal is new Base_Signal with private;

    procedure Connect( this : in out Scroll_Signal'Class; slot : Scroll_Connection'Class );

    procedure Emit( this      : Scroll_Signal'Class;
                    x, y      : Float;
                    scrollAmt : Float;
                    handled   : out Boolean );

    ----------------------------------------------------------------------------
    -- Signals.Mouse.Connections --

    generic
        type Target (<>) is abstract limited new Base_Object with private;
    package Connections is

        ------------------------------------------------------------------------
        -- Mouse_Connection

        type Mouse_Prototype is (Proto1);

        type A_Method1 is access
            procedure( object : not null access Target'Class;
                       mouse  : Mouse_Arguments );

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        function Slot( obj      : in out Target'Class;
                       method   : not null A_Method1;
                       priority : Priority_Type := Normal ) return Mouse_Connection'Class;

        function Slot( obj      : not null access Target'Class;
                       method   : not null A_Method1;
                       priority : Priority_Type := Normal ) return Mouse_Connection'Class is (Slot( obj.all, method, priority ));

        ------------------------------------------------------------------------
        -- Button_Connection

        type Button_Prototype is (Proto0, Proto3);

        type A_Method0 is access
            procedure( object : not null access Target'Class );

        type A_Method3 is access
            procedure( object : not null access Target'Class;
                       mouse  : Button_Arguments );

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        function Slot( obj       : in out Target'Class;
                       method    : not null A_Method0;
                       button    : Mouse_Button;
                       modifiers : Modifiers_Pattern := MODIFIERS_ANY;
                       priority  : Priority_Type := Normal ) return Button_Connection'Class;

        function Slot( obj       : not null access Target'Class;
                       method    : not null A_Method0;
                       button    : Mouse_Button;
                       modifiers : Modifiers_Pattern := MODIFIERS_ANY;
                       priority  : Priority_Type := Normal ) return Button_Connection'Class is (Slot( obj.all, method, button, modifiers, priority ));

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        function Slot( obj       : in out Target'Class;
                       method    : not null A_Method3;
                       button    : Mouse_Button := Mouse_Any;
                       modifiers : Modifiers_Pattern := MODIFIERS_ANY;
                       priority  : Priority_Type := Normal ) return Button_Connection'Class;

        function Slot( obj       : not null access Target'Class;
                       method    : not null A_Method3;
                       button    : Mouse_Button := Mouse_Any;
                       modifiers : Modifiers_Pattern := MODIFIERS_ANY;
                       priority  : Priority_Type := Normal ) return Button_Connection'Class is (Slot( obj.all, method, button, modifiers, priority ));

        ----------------------------------------------------------------------------
        -- Scroll_Connection

        type Scroll_Prototype is (Proto4);

        type A_Method4 is access
            procedure( object : not null access Target'Class;
                       mouse  : Scroll_Arguments );

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        function Slot( obj      : in out Target'Class;
                       method   : not null A_Method4;
                       priority : Priority_Type := Normal ) return Scroll_Connection'Class;

        function Slot( obj      : not null access Target'Class;
                       method   : not null A_Method4;
                       priority : Priority_Type := Normal ) return Scroll_Connection'Class is (Slot( obj.all, method, priority ));

    private

        type Generic_Mouse_Connection(proto : Mouse_Prototype) is new Mouse_Connection with
            record
                object : access Target'Class := null;
                case proto is
                    when Proto1 => method1 : A_Method1 := null;
                end case;
            end record;

        overriding
        procedure Emit( this   : Generic_Mouse_Connection;
                        sender : access Base_Object'Class;
                        args   : Mouse_Arguments );

        overriding
        function Eq( this : Generic_Mouse_Connection; other : Base_Connection'Class ) return Boolean;

        overriding
        function Get_Object( this : Generic_Mouse_Connection ) return access Base_Object'Class is (this.object);

        ------------------------------------------------------------------------

        type Generic_Button_Connection(proto : Button_Prototype) is new Button_Connection with
            record
                object : access Target'Class := null;
                case proto is
                    when Proto0 => method0 : A_Method0 := null;
                    when Proto3 => method3 : A_Method3 := null;
                end case;
            end record;

        overriding
        procedure Emit( this    : Generic_Button_Connection;
                        sender  : access Base_Object'Class;
                        args    : Button_Arguments;
                        handled : out Boolean );

        overriding
        function Eq( this : Generic_Button_Connection; other : Base_Connection'Class ) return Boolean;

        overriding
        function Get_Object( this : Generic_Button_Connection ) return access Base_Object'Class is (this.object);

        ------------------------------------------------------------------------

        type Generic_Scroll_Connection(proto : Scroll_Prototype) is new Scroll_Connection with
            record
                object : access Target'Class := null;
                case proto is
                    when Proto4 => method4 : A_Method4 := null;
                end case;
            end record;

        overriding
        procedure Emit( this    : Generic_Scroll_Connection;
                        sender  : access Base_Object'Class;
                        args    : Scroll_Arguments;
                        handled : out Boolean );

        overriding
        function Eq( this : Generic_Scroll_Connection; other : Base_Connection'Class ) return Boolean;

        overriding
        function Get_Object( this : Generic_Scroll_Connection ) return access Base_Object'Class is (this.object);

    end Connections;

private

    type Mouse_Signal is new Base_Signal with null record;

    type Button_Signal is new Base_Signal with null record;

    type Scroll_Signal is new Base_Signal with null record;

end Signals.Mouse;

