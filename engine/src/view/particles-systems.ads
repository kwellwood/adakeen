--
-- Copyright (c) 2015-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Indefinite_Ordered_Maps;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Ada.Unchecked_Deallocation;
with Events;                            use Events;
with Events.Listeners;                  use Events.Listeners;
with GNAT.Random_Numbers;               use GNAT.Random_Numbers;
with Lighting_Systems;                  use Lighting_Systems;
with Objects;                           use Objects;
with Processes;                         use Processes;
with Values.Lists;                      use Values.Lists;
with Values.Maps;                       use Values.Maps;
with Vector_Math;                       use Vector_Math;

limited with Game_Views;

package Particles.Systems is

    type Particle_System is new Limited_Object
                            and Process
                            and Event_Listener
                            with private;
    type A_Particle_System is access all Particle_System'Class;

    -- Creates and returns a new Particle System.
    function Create_Particle_System( view     : not null access Game_Views.Game_View'Class;
                                     capacity : Integer ) return A_Particle_System;

    -- Clears the state of the particle system, removing all particles, emitters
    -- and layers.
    procedure Clear( this : not null access Particle_System'Class );

    -- Draws the particles within 'bounds' at depth 'Z'.
    procedure Draw_Layer( this    : not null access Particle_System'Class;
                          z       : Float;
                          zOffset : Float;
                          bounds  : Rectangle;
                          pass    : Render_Pass );

    -- Freezes/unfreezes event updates to the particle system from the game. No
    -- state will change while it is frozen and no particles will update. Any
    -- events received while frozen will be ignored. Unfreeze it to resume
    -- handling new events.
    procedure Freeze_Updates( this : not null access Particle_System'Class; freeze : Boolean );

    -- Returns True if the particle system has been frozen, meaning it is
    -- ignoring particle system events and all particles are frozen.
    function Is_Frozen( this : not null access Particle_System'Class ) return Boolean;

    -- Returns a list of particle emitter names loaded from disk.
    function Get_Emitter_Names( this : not null access Particle_System'Class ) return List_Value;
    pragma Postcondition( Get_Emitter_Names'Result.Valid );

    procedure Delete( this : in out A_Particle_System );

private

    MAX_EMITTERS     : constant := 64;       -- max count of emitters
    MAX_LAYERS       : constant := 8;        -- max count of discrete Z depths
    INITIAL_CAPACITY : constant := 1024;     -- initial particle capacity of a layer
    MAX_CAPACITY     : constant := 16384;    -- max particle capacity per layer

    type Emitter_Info is
        record
            filePath : Unbounded_String;     -- definition source file
            def      : Emitter;              -- emitter definition
        end record;

    package Definition_Maps is new Ada.Containers.Indefinite_Ordered_Maps( String, Emitter_Info, "<", "=" );

    type Emitter_Array is array (Integer range <>) of Emitter;

    type Particle_Array is array (Integer range <>) of Particle;

    type Layer_Type(capacity : Integer) is
        record
            particles : Particle_Array(1..capacity);
            pCount    : Integer := 0;
            z         : Float := 0.0;
        end record;
    type A_Layer is access all Layer_Type;

    procedure Free is new Ada.Unchecked_Deallocation( Layer_Type, A_Layer );

    type Layer_Array is array (Integer range <>) of A_Layer;

    ----------------------------------------------------------------------------

    type Particle_System is new Limited_Object
                            and Process
                            and Event_Listener with
        record
            view        : access Game_Views.Game_View'Class;
            reloadDefs  : Boolean := False;         -- reload emitters on the fly?
            capacity    : Integer := 0;
            definitions : Definition_Maps.Map;
            frozen      : Boolean := False;
            eCount      : Natural := 0;
            emitters    : Emitter_Array(1..MAX_EMITTERS);
            gen         : Generator;
            layers      : Layer_Array(1..MAX_LAYERS);
        end record;

    -- Adds the emitter to particle system, using the following rules:
    --   * If the emitter is attached to a component id and one already exists,
    --     it will replace it.
    --   * Othewise, if no more emitter slots are free, the emitter with the
    --     shortest remaining lifespan will be replaced.
    --   * If no other emitters have a fixed lifespan (all attached to component
    --     ids), then the emitter will not be added.
    procedure Add_Emitter( this : not null access Particle_System'Class;
                           em   : Emitter );

    procedure Construct( this     : access Particle_System;
                         view     : not null access Game_Views.Game_View'Class;
                         capacity : Integer );

    -- Ensures that a layer with depth 'z' exists. Returns True on success, or
    -- False if adding the layer would create more than the maximum number of
    -- layers.
    function Add_Layer( this  : not null access Particle_System'Class;
                        z     : Float ) return Boolean;

    procedure Delete( this : in out Particle_System );

    procedure Draw_Layer( this    : not null access Particle_System'Class;
                          layer   : A_Layer;
                          bounds  : Rectangle;
                          zOffset : Float;
                          pass    : Render_Pass );

    -- Find and returns a particle emitter definition by 'name' in 'def'.
    -- 'found' will return True if a definition is found.
    procedure Get_Emitter_Definition( this  : not null access Particle_System'Class;
                                      name  : String;
                                      def   : out Emitter;
                                      found : out Boolean );

    overriding
    function Get_Process_Name( this : access Particle_System ) return String is ("Particle System");

    -- Handles an event received from the corral that the system is registered
    -- with. This is the same corral provided by the Game View system's
    -- Game_View object. If 'evt' is returned null then the event was consumed.
    procedure Handle_Event( this : access Particle_System;
                            evt  : in out A_Event;
                            resp : out Response_Type );

    -- Reloads the particle emitters library.
    procedure Load_Particles( this : not null access Particle_System'Class );

    -- Loads a particle emitter definition file at 'filePath'. Returns False if
    -- the file cannot be loaded, otherwise True.
    function Load_Particles( this     : not null access Particle_System'Class;
                             filePath : String ) return Boolean;

    -- Reserves capacity for 'count' additional active particles in layer
    -- 'layer'. If the current layer capacity is insufficient, the layer
    -- capacity will be doubled (capped at MAX_CAPACITY). 'layer' must be a
    -- valid layer number.
    procedure Reserve_Capacity( this  : not null access Particle_System'Class;
                                layer : in out A_Layer;
                                count : Integer );

    -- Sets the properties from map 'props' into emitter 'em'.
    procedure Set_Emitter_Properties( this  : not null access Particle_System'Class;
                                      em    : in out Emitter;
                                      props : Map_Value );

    -- Spawns 'count' particles from emitter 'em'.
    procedure Spawn_Particles( this  : not null access Particle_System'Class;
                               em    : Emitter;
                               count : Integer );

    procedure Tick( this : access Particle_System; time : Tick_Time );

end Particles.Systems;
