--
-- Copyright (c) 2012 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Mouse is

    pragma Pure;

    -- Represents the buttons on a three-button mouse.
    type Mouse_Button is (Mouse_Left, Mouse_Middle, Mouse_Right, Mouse_Any);

    type Mouse_Button_Array is array (Mouse_Left..Mouse_Right) of Boolean;

    -- Returns Mouse_Button 'mb' as a string: "Left", "Right", or "Middle".
    function To_String( mb : Mouse_Button ) return String
    is (
        case mb is
            when Mouse_Left   => "Left",
            when Mouse_Middle => "Middle",
            when Mouse_Right  => "Right",
            when Mouse_Any    => "Any"
    );

end Mouse;
