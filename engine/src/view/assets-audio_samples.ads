--
-- Copyright (c) 2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Audio;                     use Allegro.Audio;

package Assets.Audio_Samples is

    type Audio_Sample is new Asset with private;
    type A_Audio_Sample is access all Audio_Sample'Class;

    -- Loads the audio sample / sound effect from file 'filePath' in the "audio"
    -- resource group. If the file cannot be found or cannot be loaded, the
    -- returned asset will not be loaded.
    function Load_Audio_Sample( filePath : String ) return A_Audio_Sample;
    pragma Postcondition( Load_Audio_Sample'Result /= null );

    -- Returns a pointer to the Allegro sample, or null if the sample is not loaded.
    function Get_Allegro_Sample( this : not null access Audio_Sample'Class ) return A_Allegro_Sample;

    procedure Delete( this : in out A_Audio_Sample );

private

    type Audio_Sample is new Asset with
        record
            alSample : A_Allegro_Sample := null;
        end record;

    procedure Construct( this     : access Audio_Sample;
                         assetId  : Value'Class;
                         filePath : String );

    function Load_Data( this : access Audio_Sample ) return Boolean;

    procedure Unload_Data( this : access Audio_Sample );

end Assets.Audio_Samples;
