--
-- Copyright (c) 2015-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Signals.Keys is

    procedure Connect( this : in out Key_Signal'Class; slot : Key_Connection'Class ) is
    begin
        this.Connect( Base_Connection(slot) );
    end Connect;

    ----------------------------------------------------------------------------

    procedure Emit( this      : Key_Signal'Class;
                    key       : Integer;
                    char      : Character;
                    modifiers : Modifiers_Array;
                    handled   : out Boolean ) is
        conns : constant Connection_Lists.List := this.conns.Copy;
    begin
        handled := False;
        for c of conns loop
            if c in Key_Connection'Class then
                Key_Connection'Class(c).Emit( this.sender,
                                              Key_Arguments'(key,
                                                             char,
                                                             modifiers),
                                              handled );
                exit when handled;   -- Key_Connections filter by modifiers and key
            else
                Connection'Class(c).Emit( this.sender );
                handled := True;     -- Connections don't filter signals
                exit;
            end if;

            -- break early if the axis change signal was handled by an Exclusive
            -- priority connection; it should block lower priority connections.
            exit when c.priority = Exclusive;
        end loop;
    end Emit;

    --==========================================================================

    package body Connections is

        function Slot( obj       : in out Target'Class;
                       method    : not null A_Method0;
                       key       : Integer;
                       modifiers : Modifiers_Pattern := MODIFIERS_ANY;
                       priority  : Priority_Type := Normal ) return Key_Connection'Class is
            result : aliased Generic_Key_Connection(Proto0);
        begin
            result.Construct( priority );
            result.object := obj'Unchecked_Access;              -- UNSAFE!
            result.method0 := method;
            result.key := key;
            result.modifiers := modifiers;
            return result;
        end Slot;

        ------------------------------------------------------------------------

        function Slot( obj       : in out Target'Class;
                       method    : not null A_Method2;
                       key       : Integer;
                       modifiers : Modifiers_Pattern := MODIFIERS_ANY;
                       priority  : Priority_Type := Normal ) return Key_Connection'Class is
            result : aliased Generic_Key_Connection(Proto2);
        begin
            result.Construct( priority );
            result.object := obj'Unchecked_Access;              -- UNSAFE!
            result.method2 := method;
            result.key := key;
            result.modifiers := modifiers;
            return result;
        end Slot;

        ------------------------------------------------------------------------

        overriding
        procedure Emit( this    : Generic_Key_Connection;
                        sender  : access Base_Object'Class;
                        args    : Key_Arguments;
                        handled : out Boolean ) is
        begin
            handled := False;
            if this.modifiers = args.modifiers then
                if this.key = args.code or else
                   this.key = KEY_ANY or else
                   (this.key = KEY_ANY_CHARACTER and then ' ' <= args.char and then args.char <= '~')
                then
                    handled := True;
                    this.object.Push_Signaller( sender );
                    case this.proto is
                        when Proto0 => this.method0( this.object );
                        when Proto2 => this.method2( this.object, args );
                    end case;
                    this.object.Pop_Signaller;
                end if;
            end if;
        end Emit;

        ------------------------------------------------------------------------

        overriding
        function Eq( this : Generic_Key_Connection; other : Base_Connection'Class ) return Boolean is
        begin
            if other in Generic_Key_Connection'Class then
                return this.object    = Generic_Key_Connection(other).object and then
                       this.proto     = Generic_Key_Connection(other).proto and then
                       (case this.proto is
                           when Proto0 => (this.method0 = Generic_Key_Connection(other).method0),
                           when Proto2 => (this.method2 = Generic_Key_Connection(other).method2)) and then
                       ((this.key       = Generic_Key_Connection(other).key and then
                         this.modifiers = Generic_Key_Connection(other).modifiers) or else
                        (Generic_Key_Connection(other).key = KEY_ANY));
            end if;
            return False;
        end Eq;

    end Connections;

end Signals.Keys;
