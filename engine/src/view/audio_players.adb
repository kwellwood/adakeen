--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Audio.Codecs;              use Allegro.Audio.Codecs;
with Debugging;                         use Debugging;
with Events.Audio;                      use Events.Audio;
with Preferences;                       use Preferences;
with Support;                           use Support;
with Support.Paths;                     use Support.Paths;

package body Audio_Players is

    STREAM_BUFFER_COUNT   : constant := 4;
    STREAM_BUFFER_SAMPLES : constant := 2048;

    initialized : Boolean := False;
    audioOk     : Boolean := False;

    -- Initializes the audio driver, returning True on success. This only needs
    -- to be called once before using the audio system.
    function Initialize return Boolean;

    ----------------------------------------------------------------------------

    -- Pauses playing the currently loaded music in .musicStream.
    procedure Pause_Music( this : not null access Audio_Player'Class );

    -- Called when a Play_Music event is received to begin playing a music track.
    -- Only one music track can play at a time so this will stop any previously
    -- playing music. Music tracks will loop.
    procedure Play_Music( this : not null access Audio_Player'Class; name : String );
    pragma Precondition( name'Length > 0 );

    -- Called when a Play_Sound event is received to begin playing a sound once
    -- through. Multiple sounds can play simultaneously. If 'name' already
    -- started playing less than 'repeatDelay' ago, it will not be played.
    procedure Play_Sound( this        : not null access Audio_Player'Class;
                          name        : String;
                          repeatDelay : Time_Span := Time_Span_Zero );

    -- Starts/resumes playing the currently loaded music in .musicStream.
    procedure Resume_Music( this : not null access Audio_Player'Class );

    -- Called when a Stop_Music event is received. Any currently looping music
    -- will be stopped.
    procedure Stop_Music( this : not null access Audio_Player'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    -- Initializes the audio driver.
    function Initialize return Boolean is
    begin
        if not initialized then
            pragma Debug( Dbg( "Initializing audio system", D_AUDIO, Info ) );
            if Al_Install_Audio then
                if Al_Init_ACodec_Addon then
                    audioOk := True;
                else
                    Dbg( "Failed to initialize audio codecs", D_AUDIO, Error );
                end if;
            else
                Dbg( "Failed to install audio", D_AUDIO, Error );
            end if;

            initialized := True;
        end if;
        return audioOk;
    end Initialize;

    --==========================================================================

    function Create_Audio_Player( corral : not null A_Corral ) return A_Audio_Player is
        this : constant A_Audio_Player := new Audio_Player;
    begin
        this.Construct( corral );
        return this;
    end Create_Audio_Player;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Audio_Player; corral : not null A_Corral ) is
    begin
        Limited_Object(this.all).Construct;
        if Audio_Players.Initialize then
            this.voice := Al_Create_Voice( 44100,
                                           ALLEGRO_AUDIO_DEPTH_INT16,
                                           ALLEGRO_CHANNEL_CONF_2 );
            if this.voice /= null then
                this.mixer := Al_Create_Mixer( 44100,
                                               ALLEGRO_AUDIO_DEPTH_FLOAT32,
                                               ALLEGRO_CHANNEL_CONF_2 );
                if this.mixer /= null then
                    if not Al_Attach_Mixer_To_Voice( this.mixer, this.voice ) then
                        Dbg( "Failed to attach audio mixer to voice", D_AUDIO, Error );
                    end if;
                else
                    Dbg( "Failed to allocate audio mixer", D_AUDIO, Error );
                end if;
            else
                Dbg( "Failed to allocate audio voice", D_AUDIO, Error );
            end if;
        end if;
        this.corral := corral;
        this.corral.Add_Listener( this, EVT_PLAY_MUSIC );
        this.corral.Add_Listener( this, EVT_PLAY_SOUND );
        this.corral.Add_Listener( this, EVT_STOP_MUSIC );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Audio_Player ) is
    begin
        for snd of this.sounds loop
            Al_Detach_Sample_Instance( snd.instance );
            Al_Destroy_Sample_Instance( snd.instance );
            Delete( snd.sample );
        end loop;
        this.sounds.Clear;
        Al_Destroy_Mixer( this.mixer );
        Al_Destroy_Voice( this.voice );
        Limited_Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Event( this : access Audio_Player;
                            evt  : in out A_Event;
                            resp : out Response_Type ) is
        pragma Unreferenced( resp );
    begin
        case evt.Get_Event_Id is
            when EVT_PLAY_SOUND => this.Play_Sound( A_Play_Audio_Event(evt).Get_Audio_Name,
                                                    A_Play_Audio_Event(evt).Get_Repeat_Delay );
            when EVT_PLAY_MUSIC => this.Play_Music( A_Play_Audio_Event(evt).Get_Audio_Name );
            when EVT_STOP_MUSIC => this.Stop_Music;
            when others         => null;
        end case;
    end Handle_Event;

    ----------------------------------------------------------------------------

    procedure Pause_Music( this : not null access Audio_Player'Class ) is
    begin
        pragma Debug( Dbg( "Pausing music", D_AUDIO, Info ) );
        if this.musicStream /= null and this.musicPlaying then
            this.musicPlaying := not Al_Set_Audio_Stream_Playing( this.musicStream, False );
        end if;
    end Pause_Music;

    ----------------------------------------------------------------------------

    procedure Play_Music( this : not null access Audio_Player'Class; name : String ) is
        filename : constant String := Add_Extension( name, "ogg" );
    begin
        this.Stop_Music;
        pragma Debug( Dbg( "Playing_music <" & name & ">", D_AUDIO, Info ) );

        if this.voice /= null and this.mixer /= null then
            -- load the music file
            this.musicRes := Load_Resource( filename, "audio" );
            if this.musicRes /= null then
                -- create an audio stream backed by the resource file
                pragma Assert( this.musicStream = null );
                this.musicStream := Al_Load_Audio_Stream_f( this.musicRes.Create_Allegro_File,   -- consumes 'fp' argument
                                                            "." & Get_Extension( filename ),
                                                            STREAM_BUFFER_COUNT,
                                                            STREAM_BUFFER_SAMPLES );
            end if;

            if this.musicStream /= null then
                -- attach the audio stream to the audio mixer
                if Al_Attach_Audio_Stream_To_Mixer( this.musicStream, this.mixer ) and then
                   Al_Set_Audio_Stream_Playmode( this.musicStream, ALLEGRO_PLAYMODE_LOOP ) and then
                   Al_Set_Audio_Stream_Playing( this.musicStream, False )
                then
                    -- begin playing the music stream
                    if Get_Pref( "application", "music" ) then
                        this.Resume_Music;
                        if not this.musicPlaying then
                            Dbg( "Failed to play music file: " & filename, D_AUDIO, Error );
                            this.Stop_Music;   -- cleanup
                        end if;
                    end if;
                else
                    Dbg( "Failed to configure audio mixer", D_AUDIO, Error );
                    this.Stop_Music;   -- cleanup
                end if;
            else
                Dbg( "Failed to open music file: " & filename, D_AUDIO, Error );
                Delete( this.musicRes );
            end if;
        end if;
    end Play_Music;

    ----------------------------------------------------------------------------

    procedure Play_Sound( this        : not null access Audio_Player'Class;
                          name        : String;
                          repeatDelay : Time_Span := Time_Span_Zero ) is
        filename : constant String := Add_Extension( name, "wav" );
        now      : constant Time := Clock;
        sample   : A_Audio_Sample;
        instance : A_Allegro_Sample_Instance;
    begin
        pragma Debug( Dbg( "Playing_sound <" & name & ">", D_AUDIO, Info ) );

        if name'Length = 0 then
            return;   -- name is missing
        end if;

        if not Get_Pref( "application", "soundfx" ) or this.voice = null or this.mixer = null then
            return;   -- sound effects turned off or audio system not available
        end if;

        sample := Load_Audio_Sample( filename );
        if not sample.Is_Loaded then
            Delete( sample );
            Dbg( "Failed to open sound file: " & filename, D_AUDIO, Error );
            return;
        end if;

        -- ignore the play command if the sound already started playing more
        -- recently than the repeat delay. this.sounds is ordered by descending
        -- start time.
        for snd of this.sounds loop
            if snd.sample = sample then
                if now < snd.startTime + repeatDelay then
                    -- the repeat delay hasn't elapsed yet
                    Delete( sample );
                    return;
                end if;
                exit;
            end if;
        end loop;

        -- create and play a new instance of the sample
        instance := Al_Create_Sample_Instance( sample.Get_Allegro_Sample );
        if Al_Attach_Sample_Instance_To_Mixer( instance, this.mixer ) and then
           Al_Set_Sample_Instance_Playmode( instance, ALLEGRO_PLAYMODE_ONCE ) and then
           Al_Play_Sample_Instance( instance )
        then
            -- success! add it to the sound's instance list
            this.sounds.Prepend( Sound'(sample    => sample,
                                        instance  => instance,
                                        startTime => now) );
        else
            Dbg( "Failed to play sound <" & name & ">", D_AUDIO, Error );
            Al_Detach_Sample_Instance( instance );
            Al_Destroy_Sample_Instance( instance );
            Delete( sample );
        end if;
    end Play_Sound;

    ----------------------------------------------------------------------------

    procedure Resume_Music( this : not null access Audio_Player'Class ) is
    begin
        pragma Debug( Dbg( "Resuming music", D_AUDIO, Info ) );
        if this.musicStream /= null and not this.musicPlaying then
            this.musicPlaying := Al_Set_Audio_Stream_Playing( this.musicStream, True );
        end if;
    end Resume_Music;

    ----------------------------------------------------------------------------

    procedure Shutdown( this : not null access Audio_Player'Class ) is
    begin
        this.corral.Remove_Listener( this );

        this.Stop_Music;

        if this.mixer /= null then
            Al_Detach_Mixer( this.mixer );
        end if;
        if this.voice /= null then
            Al_Detach_Voice( this.voice );
        end if;
    end Shutdown;

    ----------------------------------------------------------------------------

    procedure Stop_Music( this : not null access Audio_Player'Class ) is
    begin
        pragma Debug( Dbg( "Stopping_music", D_AUDIO, Info ) );
        if this.musicStream /= null then
            Al_Detach_Audio_Stream( this.musicStream );
            Al_Destroy_Audio_Stream( this.musicStream );
            this.musicPlaying := False;
        end if;
        Delete( this.musicRes );
    end Stop_Music;

    ----------------------------------------------------------------------------

    overriding
    procedure Tick( this : access Audio_Player; time : Tick_Time ) is
        pragma Unreferenced( time );
        soundPos,
        soundPrev : Sound_Lists.Cursor;
        snd       : Sound;
    begin
        -- loop over all sounds, from oldest to newest
        -- remove each sound if it finished playing
        soundPos := this.sounds.Last;
        while Sound_Lists.Has_Element( soundPos ) loop
            snd := Sound_Lists.Element( soundPos );
            soundPrev := Sound_Lists.Previous( soundPos );

            -- check the playing state of each sample instance
            if not Al_Get_Sample_Instance_Playing( snd.instance ) then
                pragma Debug( Dbg( "Cleaning up sound <" & snd.sample.Get_Path & ">", D_AUDIO, Info ) );
                Al_Detach_Sample_Instance( snd.instance );
                Al_Destroy_Sample_Instance( snd.instance );
                Delete( snd.sample );
                this.sounds.Delete( soundPos );
            end if;

            soundPos := soundPrev;
        end loop;

        -- pause or resume music when the preference changes
        if this.musicStream /= null then
            if not this.musicPlaying and then Get_Pref( "application", "music" ) then
                this.Resume_Music;
            elsif this.musicPlaying and then not Get_Pref( "application", "music" ) then
                this.Pause_Music;
            end if;
        end if;
    end Tick;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Audio_Player ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

begin

    Set_Default( "application", "music", True );     -- music enabled?
    Set_Default( "application", "soundfx", True );   -- sound effects enabled?

end Audio_Players;
