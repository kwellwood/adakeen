--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Debugging;                         use Debugging;

pragma Warnings( Off, Debugging );

package body Drawing is

    -- setting an unaccelerated target
    unaccleratedTargetWarning : Boolean := False;
    pragma Warnings( Off, unaccleratedTargetWarning );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    procedure Set_Target_Bitmap( bmp : A_Allegro_Bitmap ) is
    begin
#if DEBUG then
        -- Notify the developer that a bitmap being drawn is not
        -- accelerated. It's likely that the bitmap was not created within
        -- the context of the currently active display. This is a
        -- development error and should be fixed immediately because
        -- unaccelerated drawing is extremely slow.
        if bmp /= null and then not Al_Is_Compatible_Bitmap( bmp ) and then not unaccleratedTargetWarning then
            Dbg( "Setting unaccelerated target bitmap!", D_GUI, Warning );
            unaccleratedTargetWarning := True;
        end if;
#end if;
        if bmp = Al_Get_Target_Bitmap then
            return;
        end if;
        Al_Set_Target_Bitmap( bmp );
    end Set_Target_Bitmap;

    ----------------------------------------------------------------------------

    function Get_Target_Bitmap return A_Allegro_Bitmap is (Al_Get_Target_Bitmap);

    ----------------------------------------------------------------------------

    function Get_Target_Height return Natural is (Al_Get_Bitmap_Height( Al_Get_Target_Bitmap ));

    ----------------------------------------------------------------------------

    function Get_Target_Width return Natural is (Al_Get_Bitmap_Width( Al_Get_Target_Bitmap ));

    ----------------------------------------------------------------------------

    procedure Clear_To_Color( color : Allegro_Color ) is
    begin
        Al_Clear_To_Color( color );
    end Clear_To_Color;

end Drawing;
