--
-- Copyright (c) 2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Allegro.Joystick;                  use Allegro.Joystick;
with Objects;                           use Objects;

package Gamepads is

    type Gamepad is new Limited_Object with private;
    type A_Gamepad is access all Gamepad'Class;

    function Create_Gamepad( device : not null A_Allegro_Joystick ) return A_Gamepad;

    -- Returns True if the Gamepad's device is active, or False if it has been
    -- unplugged.
    function Is_Active( this : not null access Gamepad'Class ) return Boolean;

    -- Sets button 'button' to 'pressed'. If the button state changes, a gamepad
    -- button pressed event will be queued.
    procedure Set_Button( this    : not null access Gamepad'Class;
                          button  : Natural;
                          pressed : Boolean );

    -- Sets the position of stick 'stick' to 'position' along axis 'axis'. If
    -- the stick position changes, a stick moved event will be queued.
    procedure Set_Position( this     : not null access Gamepad'Class;
                            stick    : Natural;
                            axis     : Natural;
                            position : Float );

    procedure Delete( this : in out A_Gamepad );
    pragma Postcondition( this = null );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Gamepad_Button is new Integer;
    type Gamepad_Axis is new Integer;

                                                     -- Generic Label              XBox            PlayStation        USB Gamepad (kiwitata)
                                                     -- -------                    ---------       -----------        -----------
    BUTTON_SOUTH : constant Gamepad_Button := 0;     -- South                      A (Green)       X      (Blue)      Y (north)
    BUTTON_EAST  : constant Gamepad_Button := 1;     -- East                       B (Red)         Circle (Red)       B (east)
    BUTTON_WEST  : constant Gamepad_Button := 2;     -- West                       X (Blue)        Square (Pink)      A (south
    BUTTON_NORTH : constant Gamepad_Button := 3;     -- North                      Y (Yellow)      North  (Triangle)  X (west)
    BUTTON_BACK  : constant Gamepad_Button := 4;     -- Back                       Back                               Select
    BUTTON_START : constant Gamepad_Button := 5;     -- Start                      Start                              Start
    BUTTON_L1    : constant Gamepad_Button := 6;     -- l1                         Left shoulder
    BUTTON_R1    : constant Gamepad_Button := 7;     -- r1                         Right shoulder
    BUTTON_LSB   : constant Gamepad_Button := 8;     -- lstick                     Left stick
    BUTTON_RSB   : constant Gamepad_Button := 9;     -- rstick                     Right stick
    BUTTON_DPADR : constant Gamepad_Button := 10;    -- D-Pad right
    BUTTON_DPADL : constant Gamepad_Button := 11;    -- D-Pad left
    BUTTON_DPADD : constant Gamepad_Button := 12;    -- D-Pad down
    BUTTON_DPADU : constant Gamepad_Button := 13;    -- D-Pad up
    BUTTON_NONE  : constant Gamepad_Button := -1;
    BUTTON_ANY   : constant Gamepad_Button := -2;

    AXIS_LXP   : constant Gamepad_Axis := 0;          -- Left thumbstick X+
    AXIS_LXN   : constant Gamepad_Axis := 1;          -- Left thumbstick X-
    AXIS_LYP   : constant Gamepad_Axis := 2;          -- Left thumbstick Y+
    AXIS_LYN   : constant Gamepad_Axis := 3;          -- Left thumbstick Y-
    AXIS_RXP   : constant Gamepad_Axis := 4;          -- Right thumbstick X+
    AXIS_RXN   : constant Gamepad_Axis := 5;          -- Right thumbstick X-
    AXIS_RYP   : constant Gamepad_Axis := 6;          -- Right thumbstick Y+
    AXIS_RYN   : constant Gamepad_Axis := 7;          -- Right thumbstick Y-
    AXIS_LTRIG : constant Gamepad_Axis := 8;          -- Left trigger
    AXIS_RTRIG : constant Gamepad_Axis := 9;          -- Right trigger
    AXIS_NONE  : constant Gamepad_Axis := -1;
    AXIS_ANY   : constant Gamepad_Axis := -2;

    -- Returns a short description of axis 'axis' for displaying to the user.
    function Get_Gamepad_Axis_Description( axis : Gamepad_Axis ) return String;

    -- Translates a gamepad axis label string to a gamepad axis number (one
    -- of the AXIS_* constants in the spec). Returns 'fallback' if the label is
    -- not one of the standard gamepad axis labels.
    function Get_Gamepad_Axis_From_Label( label : String; fallback : Gamepad_Axis ) return Gamepad_Axis;

    -- Returns a short description of button 'button' for displaying to the user.
    function Get_Gamepad_Button_Description( button : Gamepad_Button ) return String;

    -- Translates a gamepad button label string to a gamepad button number (one
    -- of the BUTTON_* constants in the spec). Returns 'fallback' if the label
    -- is not one of the standard gamepad button labels:
    function Get_Gamepad_Button_From_Label( label : String; fallback : Gamepad_Button ) return Gamepad_Button;

    AXIS_CENTERED_THRESHOLD : constant := 0.2;     -- near the center
    AXIS_SELECTED_THRESHOLD : constant := 0.8;     -- near the max
    AXIS_DEADZONE_THRESHOLD : constant := 0.125;

private

    MAX_GAMEPAD_BUTTONS : constant := 14;
    MAX_GAMEPAD_STICKS  : constant := 16;

    type Button_Type is
        record
            state : Boolean := False;                  -- pressed/released
            evt   : Gamepad_Button := BUTTON_NONE;     -- mapped event
        end record;

    type Button_Array is array (Integer range 0..MAX_GAMEPAD_BUTTONS-1) of Button_Type;

    type Axis_Type is
        record
            pos    : Float := 0.0;
            posEvt : Gamepad_Axis := AXIS_NONE;
            negEvt : Gamepad_Axis := AXIS_NONE;
        end record;

    type Axis_Array is array (0..2) of Axis_Type;

    type Stick_Type is
        record
            axisCount : Natural := 0;
            axes      : Axis_Array;
            deadzone  : Float := AXIS_DEADZONE_THRESHOLD;
        end record;

    type Stick_Array is array (Integer range 0..MAX_GAMEPAD_STICKS-1) of Stick_Type;

    ----------------------------------------------------------------------------

    type Gamepad is new Limited_Object with
        record
            device      : A_Allegro_Joystick := null;
            id          : Natural := 0;
            active      : Boolean := True;
            name        : Unbounded_String;
            buttonCount : Natural := 0;
            stickCount  : Natural := 0;
            buttons     : Button_Array;
            sticks      : Stick_Array;
        end record;

    procedure Construct( this : access Gamepad; device : A_Allegro_Joystick; id : Integer );

    procedure Delete( this : in out Gamepad );

end Gamepads;
