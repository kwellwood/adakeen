--
-- Copyright (c) 2017-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Indefinite_Ordered_Maps;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Mouse.Cursors;             use Allegro.Mouse.Cursors;
with Assets.Libraries;                  use Assets.Libraries;
with Debugging;                         use Debugging;
with Tiles;                             use Tiles;
with Values.Casting;                    use Values.Casting;

package body Mouse.Cursors.Manager is

    -- Maps a cursor name to a cursor instance.
    package Cursor_Maps is new Ada.Containers.Indefinite_Ordered_Maps( String, A_Mouse_Cursor, "<", "=" );

    registered : Cursor_Maps.Map;

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    procedure Init_Mouse_Cursors is
    begin
        registered.Insert( "NONE",        Create_Mouse_Cursor( ALLEGRO_SYSTEM_MOUSE_CURSOR_NONE ) );
        registered.Insert( "DEFAULT",     Create_Mouse_Cursor( ALLEGRO_SYSTEM_MOUSE_CURSOR_DEFAULT ) );
        registered.Insert( "ARROW",       Create_Mouse_Cursor( ALLEGRO_SYSTEM_MOUSE_CURSOR_ARROW ) );
        registered.Insert( "BUSY",        Create_Mouse_Cursor( ALLEGRO_SYSTEM_MOUSE_CURSOR_BUSY ) );
        registered.Insert( "QUESTION",    Create_Mouse_Cursor( ALLEGRO_SYSTEM_MOUSE_CURSOR_QUESTION ) );
        registered.Insert( "EDIT",        Create_Mouse_Cursor( ALLEGRO_SYSTEM_MOUSE_CURSOR_EDIT ) );
        registered.Insert( "MOVE",        Create_Mouse_Cursor( ALLEGRO_SYSTEM_MOUSE_CURSOR_MOVE ) );
        registered.Insert( "RESIZE_N",    Create_Mouse_Cursor( ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_N ) );
        registered.Insert( "RESIZE_W",    Create_Mouse_Cursor( ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_W ) );
        registered.Insert( "RESIZE_S",    Create_Mouse_Cursor( ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_S ) );
        registered.Insert( "RESIZE_E",    Create_Mouse_Cursor( ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_E ) );
        registered.Insert( "RESIZE_NW",   Create_Mouse_Cursor( ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_NW ) );
        registered.Insert( "RESIZE_SW",   Create_Mouse_Cursor( ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_SW ) );
        registered.Insert( "RESIZE_SE",   Create_Mouse_Cursor( ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_SE ) );
        registered.Insert( "RESIZE_NE",   Create_Mouse_Cursor( ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_NE ) );
        registered.Insert( "PROGRESS",    Create_Mouse_Cursor( ALLEGRO_SYSTEM_MOUSE_CURSOR_PROGRESS ) );
        registered.Insert( "PRECISION",   Create_Mouse_Cursor( ALLEGRO_SYSTEM_MOUSE_CURSOR_PRECISION ) );
        registered.Insert( "LINK",        Create_Mouse_Cursor( ALLEGRO_SYSTEM_MOUSE_CURSOR_LINK ) );
        registered.Insert( "ALT_SELECT",  Create_Mouse_Cursor( ALLEGRO_SYSTEM_MOUSE_CURSOR_ALT_SELECT ) );
        registered.Insert( "UNAVAILABLE", Create_Mouse_Cursor( ALLEGRO_SYSTEM_MOUSE_CURSOR_UNAVAILABLE ) );
    end Init_Mouse_Cursors;

    ----------------------------------------------------------------------------

    function Get_Mouse_Cursor( name : String ) return A_Mouse_Cursor is
        c      : constant Cursor_Maps.Cursor := registered.Find( name );
        result : A_Mouse_Cursor;
    begin
        if Cursor_Maps.Has_Element( c ) then
            result := Cursor_Maps.Element( c );
        else
            -- not found; register the cursor
            Register_Mouse_Cursor( name );
            result := registered.Element( name );
        end if;

        if result = null then
            result := registered.Element( "DEFAULT" );   -- this won't fail
        end if;
        return result;
    end Get_Mouse_Cursor;

    ----------------------------------------------------------------------------

    procedure Register_Mouse_Cursor( libAndTile : String ) is
        libName  : Unbounded_String;
        tileName : Unbounded_String;
        alCursor : A_Allegro_Mouse_Cursor;
        cursor   : A_Mouse_Cursor;
        lib      : Library_Ptr;
        tile     : A_Tile;
        colon    : Integer;
    begin
        if registered.Contains( libAndTile ) then
            return;
        end if;

        colon := Index( libAndTile, ":" );
        if colon > libAndTile'First then

            libName := To_Unbounded_String( Head( libAndTile, colon - 1 ) );
            lib := Load_Library( To_String( libName ), bitmaps => True );
            if lib.Get.Is_Loaded then

                tileName := To_Unbounded_String( Tail( libAndTile, libAndTile'Length - (Length( libName ) + 1) ) );
                tile := lib.Get.Get_Tile( lib.Get.Get_Id( To_String( tileName ) ) );
                if tile /= null then

                    if Al_Get_Bitmap_Width( tile.Get_Bitmap ) <= 32 and Al_Get_Bitmap_Height( tile.Get_Bitmap ) <= 32 then
                        alCursor := Al_Create_Mouse_Cursor( tile.Get_Bitmap,
                                                            Cast_Int( tile.Get_Attribute( "focusX" ), 0 ),
                                                            Cast_Int( tile.Get_Attribute( "focusY" ), 0 ) );
                        if alCursor /= null then
                            cursor := Create_Mouse_Cursor( alCursor );    -- success!
                        else
                            Dbg( "Failed to create mouse cursor <" & To_String( tileName ) & ">", D_GUI, Warning );
                        end if;
                    else
                        Dbg( "Cursor image <" & To_String( tileName ) & "> too large", D_GUI, Warning );
                    end if;
                else
                    Dbg( "Cursor image <" & To_String( tileName ) & "> not found in library <" & To_String( libName ) & ">", D_GUI, Warning );
                end if;
            else
                Dbg( "Cursor image <" & To_String( tileName ) & "> not available in missing library <" & To_String( libName ) & ">", D_GUI, Warning );
            end if;
        else
            Dbg( "Cursor name <" & libAndTile & "> missing library name", D_GUI, Warning );
        end if;

        -- if 'cursor' is null, insert it anyway to prevent repeated error messages
        registered.Insert( libAndTile, cursor );
    end Register_Mouse_Cursor;

    ----------------------------------------------------------------------------

    procedure Unload_Mouse_Cursors is
    begin
        for c of registered loop
            Delete( c );
        end loop;
    end Unload_Mouse_Cursors;

end Mouse.Cursors.Manager;
