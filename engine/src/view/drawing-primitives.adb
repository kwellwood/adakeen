--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.State;                     use Allegro.State;
with Allegro.Transformations;           use Allegro.Transformations;
with Drawing.Blending;                  use Drawing.Blending;

package body Drawing.Primitives is

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    procedure Line( x1, y1    : Float;
                    x2, y2    : Float;
                    color     : Allegro_Color;
                    thickness : Float := 1.0;
                    blendMode : Blend_Mode := Nonpremultiplied ) is
        state : Allegro_State;
    begin
        Set_Blend_Mode( state, blendMode );
        Al_Draw_Line( x1, y1, x2, y2, color, thickness );
        Restore_Blend_Mode( state );
    end Line;

    ----------------------------------------------------------------------------

    procedure Rect( x1, y1    : Float;
                    x2, y2    : Float;
                    color     : Allegro_Color;
                    thickness : Float := 1.0;
                    blendMode : Blend_Mode := Nonpremultiplied ) is
        state : Allegro_State;
    begin
        Set_Blend_Mode( state, blendMode );
        Al_Draw_Rectangle( x1, y1, x2, y2, color, thickness );
        Restore_Blend_Mode( state );
    end Rect;

    ----------------------------------------------------------------------------

    procedure Rectfill( x1, y1    : Float;
                        x2, y2    : Float;
                        z         : Float;
                        color     : Allegro_Color;
                        blendMode : Blend_Mode := Nonpremultiplied ) is
        backup : Allegro_Transform;
        trans  : Allegro_Transform;
        state  : Allegro_State;
    begin
        Set_Blend_Mode( state, blendMode );
        Al_Copy_Transform( backup, Al_Get_Current_Transform.all );
        if z /= 0.0 then
            Al_Identity_Transform( trans );
            Al_Translate_Transform_3d( trans, 0.0, 0.0, z );
            Al_Compose_Transform( trans, backup );
            Al_Use_Transform( trans );
        end if;

        Al_Draw_Filled_Rectangle( x1, y1, x2, y2, color );

        Al_Use_Transform( backup );
        Restore_Blend_Mode( state );
    end Rectfill;

    ----------------------------------------------------------------------------

    procedure Rectfill( x1, y1    : Float;
                        x2, y2    : Float;
                        color     : Allegro_Color;
                        blendMode : Blend_Mode := Nonpremultiplied ) is
        state : Allegro_State;
    begin
        Set_Blend_Mode( state, blendMode );
        Rectfill( x1, y1, x2, y2, 0.0, color, blendMode );
        Restore_Blend_Mode( state );
    end Rectfill;

    ----------------------------------------------------------------------------

    procedure Triangle( x1, y1    : Float;
                        x2, y2    : Float;
                        x3, y3    : Float;
                        color     : Allegro_Color;
                        thickness : Float := 1.0;
                        blendMode : Blend_Mode := Nonpremultiplied ) is
        state : Allegro_State;
    begin
        Set_Blend_Mode( state, blendMode );
        Al_Draw_Triangle( x1, y1, x2, y2, x3, y3, color, thickness );
        Restore_Blend_Mode( state );
    end Triangle;

    ----------------------------------------------------------------------------

    procedure Triangle_Filled( x1, y1    : Float;
                               x2, y2    : Float;
                               x3, y3    : Float;
                               color     : Allegro_Color;
                               blendMode : Blend_Mode := Nonpremultiplied ) is
        state : Allegro_State;
    begin
        Set_Blend_Mode( state, blendMode );
        Al_Draw_Filled_Triangle( x1, y1, x2, y2, x3, y3, color );
        Restore_Blend_Mode( state );
    end Triangle_Filled;

    --==========================================================================

    function From_Target_Pixels( width : Float ) return Float is
        inv  : Allegro_Transform;
        tmp  : Float := 0.0;
        a, b : Float;
    begin
        -- to back-transform from target bitmap pixels to drawing pixels, use
        -- the inverse of the current transform, since the current transform
        -- will convert from drawing pixels to target bitmap pixels.
        Al_Copy_Transform( inv, Al_Get_Current_Transform.all );
        Al_Invert_Transform( inv );

        -- we're assuming x and y axes are scaled the same amount, by using just
        -- a zoom scalar. create a vector of 'width' pixels in the x axis,
        -- convert it from screen pixels to drawing coordinates, and then test
        -- the length. a vector must be used instead of a single point to cancel
        -- out the translation component of the transformation and get just the
        -- scaling component.
        a := 0.0;
        b := 0.0 + width;
        Al_Transform_Coordinates( inv, a, tmp );
        Al_Transform_Coordinates( inv, b, tmp );
        return b - a;
    end From_Target_Pixels;

    ----------------------------------------------------------------------------

    procedure To_Target_Pixel_Center( x, y : in out Float ) is
        inv : Allegro_Transform;
    begin
        Al_Copy_Transform( inv, Al_Get_Current_Transform.all );
        Al_Invert_Transform( inv );

        -- transform to screen coordinates
        Al_Transform_Coordinates( Al_Get_Current_Transform.all, x, y );

        -- clamp to the middle of the pixel (so it's drawn as one pixel)
        x := Float'Floor( x ) + 0.5;
        y := Float'Floor( y ) + 0.5;

        -- transform back to drawing coordinates
        Al_Transform_Coordinates( inv, x, y );
    end To_Target_Pixel_Center;

    ----------------------------------------------------------------------------

    procedure To_Target_Pixel_Rounded( x, y : in out Float ) is
        inv : Allegro_Transform;
    begin
        Al_Copy_Transform( inv, Al_Get_Current_Transform.all );
        Al_Invert_Transform( inv );

        -- transform to screen coordinates
        Al_Transform_Coordinates( Al_Get_Current_Transform.all, x, y );

        x := Float'Rounding( x );
        y := Float'Rounding( y );

        -- transform back to drawing coordinates
        Al_Transform_Coordinates( inv, x, y );
    end To_Target_Pixel_Rounded;

    ----------------------------------------------------------------------------

    procedure Pixel_Line( x1, y1    : Float;
                          x2, y2    : Float;
                          color     : Allegro_Color;
                          thickness : Float := 1.0;
                          blendMode : Blend_Mode := Nonpremultiplied ) is
        state          : Allegro_State;
        a1, a2, b1, b2 : Float;
    begin
        Set_Blend_Mode( state, blendMode );

        -- for lines straight in X or straight in Y, make them on the half pixel
        -- so they fill exactly one pixel. if the line is angled, make sure it
        -- covers the full pixel in its travel dimension.

        a1 := Float'Floor( x1 ) + (if x1 < x2 then 0.0 else 0.5);
        b1 := Float'Floor( y1 ) + (if y1 < y2 then 0.0 else 0.5);
        a2 := Float'Floor( x2 ) + (if x2 > x1 then 1.0 else 0.5);
        b2 := Float'Floor( y2 ) + (if y2 > y1 then 1.0 else 0.5);
        Al_Draw_Line( a1, b1, a2, b2, color, thickness );

        Restore_Blend_Mode( state );
    end Pixel_Line;

    ----------------------------------------------------------------------------

    procedure Pixel_Line_H_Dotted( x1, x2    : Float;
                                   y         : Float;
                                   color     : Allegro_Color;
                                   thickness : Float := 1.0;
                                   blendMode : Blend_Mode := Nonpremultiplied ) is
        state : Allegro_State;
        yf    : constant Float := Float'Rounding( y ) + 0.5;
        first : Float := Float'Rounding( x1 );
        last  : Float := Float'Rounding( x2 );
        x     : Float := first;
    begin
        Set_Blend_Mode( state, blendMode );

        if last < first then
            first := Float'Rounding( x2 );
            last := Float'Rounding( x1 );
        end if;

        while x <= last loop
            Al_Draw_Line( x, yf, x + 2.0, yf, color, thickness );
            x := x + 4.0;
        end loop;

        Restore_Blend_Mode( state );
    end Pixel_Line_H_Dotted;

    ----------------------------------------------------------------------------

    procedure Pixel_Line_V_Dotted( x         : Float;
                                   y1, y2    : Float;
                                   color     : Allegro_Color;
                                   thickness : Float := 1.0;
                                   blendMode : Blend_Mode := Nonpremultiplied ) is
        state : Allegro_State;
        xf    : constant Float := Float'Rounding( x ) + 0.5;
        first : Float := Float'Rounding( y1 );
        last  : Float := Float'Rounding( y2 );
        y     : Float := first;
    begin
        Set_Blend_Mode( state, blendMode );

        if last < first then
            first := Float'Rounding( y2 );
            last := Float'Rounding( y1 );
        end if;

        while y <= last loop
            Al_Draw_Line( xf, y, xf, y + 2.0, color, thickness );
            y := y + 4.0;
        end loop;

        Restore_Blend_Mode( state );
    end Pixel_Line_V_Dotted;

    ----------------------------------------------------------------------------

    procedure Pixel_Rect( x1, y1    : Float;
                          x2, y2    : Float;
                          color     : Allegro_Color;
                          thickness : Float := 1.0;
                          blendMode : Blend_Mode := Nonpremultiplied ) is
        state      : Allegro_State;
        l, r, t, b : Float;
    begin
        Set_Blend_Mode( state, blendMode );

        if x1 <= x2 then
            l := Float'Rounding( x1 );
            r := Float'Rounding( x2 );
        else
            l := Float'Rounding( x2 );
            r := Float'Rounding( x1 );
        end if;
        if y1 <= y2 then
            t := Float'Rounding( y1 );
            b := Float'Rounding( y2 );
        else
            t := Float'Rounding( y2 );
            b := Float'Rounding( y1 );
        end if;

        -- draw a pixel-perfect rectangle that is inclusive of the coordinates
        Al_Draw_Rectangle( l + 0.5, t + 0.5,
                           r + 0.5, b + 0.5,
                           color,
                           thickness );

        Restore_Blend_Mode( state );
    end Pixel_Rect;

    ----------------------------------------------------------------------------

    procedure Pixel_Rectfill( x1, y1    : Float;
                              x2, y2    : Float;
                              z         : Float;
                              color     : Allegro_Color;
                              blendMode : Blend_Mode := Nonpremultiplied ) is
        state      : Allegro_State;
        backup     : Allegro_Transform;
        trans      : Allegro_Transform;
        l, r, t, b : Float;
    begin
        Set_Blend_Mode( state, blendMode );
        Al_Copy_Transform( backup, Al_Get_Current_Transform.all );
        if z /= 0.0 then
            Al_Identity_Transform( trans );
            Al_Translate_Transform_3d( trans, 0.0, 0.0, z );
            Al_Compose_Transform( trans, backup );
            Al_Use_Transform( trans );
        end if;

        if x1 <= x2 then
            l := x1;
            r := x2;
        else
            l := x2;
            r := x1;
        end if;
        if y1 <= y2 then
            t := y1;
            b := y2;
        else
            t := y2;
            b := y1;
        end if;

        -- draw a pixel-perfect rectangle that is inclusive of the coordinates
        Al_Draw_Filled_Rectangle( l, t,
                                  r + 1.0, b + 1.0,
                                  color );

        Al_Use_Transform( backup );
        Restore_Blend_Mode( state );
    end Pixel_Rectfill;

    ----------------------------------------------------------------------------

    procedure Pixel_Rectfill( x1, y1    : Float;
                              x2, y2    : Float;
                              color     : Allegro_Color;
                              blendMode : Blend_Mode := Nonpremultiplied ) is
    begin
        Pixel_Rectfill( x1, y1, x2, y2, 0.0, color, blendMode );
    end Pixel_Rectfill;

    ----------------------------------------------------------------------------

    procedure Rect_XYWH( x, y      : Float;
                         w, h      : Float;
                         color     : Allegro_Color;
                         blendMode : Blend_Mode := Nonpremultiplied ) is
        inv   : Allegro_Transform;
        tx1   : Float := x;
        ty1   : Float := y;
        tx2   : Float := x + w;
        ty2   : Float := y + h;
        state : Allegro_State;
    begin
        Set_Blend_Mode( state, blendMode );

        Al_Copy_Transform( inv, Al_Get_Current_Transform.all );
        Al_Invert_Transform( inv );

        -- transform to screen coordinates
        Al_Transform_Coordinates( Al_Get_Current_Transform.all, tx1, ty1 );
        Al_Transform_Coordinates( Al_Get_Current_Transform.all, tx2, ty2 );

        -- clamp to the middle of the pixel because the line width is 1.0
        tx1 := Float'Rounding( tx1 ) + 0.5;
        ty1 := Float'Rounding( ty1 ) + 0.5;
        tx2 := Float'Max( tx1, Float'Rounding( tx2 ) - 0.5 );
        ty2 := Float'Max( ty1, Float'Rounding( ty2 ) - 0.5 );

        -- transform back to drawing coordinates
        Al_Transform_Coordinates( inv, tx1, ty1 );
        Al_Transform_Coordinates( inv, tx2, ty2 );

        Al_Draw_Rectangle( tx1, ty1, tx2, ty2, color, From_Target_Pixels( 1.0 ) );
        Restore_Blend_Mode( state );
    end Rect_XYWH;

    ----------------------------------------------------------------------------

    procedure Rectfill_XYWH( x, y      : Float;
                             w, h      : Float;
                             color     : Allegro_Color;
                             blendMode : Blend_Mode := Nonpremultiplied ) is
        inv   : Allegro_Transform;
        tx1   : Float := x;
        ty1   : Float := y;
        tx2   : Float := x + w;
        ty2   : Float := y + h;
        state : Allegro_State;
    begin
        Set_Blend_Mode( state, blendMode );

        Al_Copy_Transform( inv, Al_Get_Current_Transform.all );
        Al_Invert_Transform( inv );

        -- transform to screen coordinates
        Al_Transform_Coordinates( Al_Get_Current_Transform.all, tx1, ty1 );
        Al_Transform_Coordinates( Al_Get_Current_Transform.all, tx2, ty2 );

        -- when drawing a filled rectangle, it will cover all the pixels between
        -- (tx1,ty1) and (tx2,ty2). think of it as a grid when coordinates are
        -- the lines between the pixels instead of the pixels themselves. A
        -- rectangle from (1,1) to (1,1) has a size of zero, but rectangle from
        -- (1.0,1.0) to (2.0,2.0) has a size of 1.0 and covers the pixel at (1,1).
        -- The rectangle will NOT be inclusive of the pixel at (x+w, y+h).
        tx1 := Float'Rounding( tx1 );
        ty1 := Float'Rounding( ty1 );
        tx2 := Float'Rounding( tx2 );
        ty2 := Float'Rounding( ty2 );

        -- transform back to drawing coordinates
        Al_Transform_Coordinates( inv, tx1, ty1 );
        Al_Transform_Coordinates( inv, tx2, ty2 );

        Al_Draw_Filled_Rectangle( tx1, ty1, tx2, ty2, color );
        Restore_Blend_Mode( state );
    end Rectfill_XYWH;

end Drawing.Primitives;

