--
-- Copyright (c) 2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Assets.Scribble;                   use Assets.Scribble;
with Debugging;                         use Debugging;
with Events.Input;                      use Events.Input;
with Support;                           use Support;
with Values.Maps;                       use Values.Maps;

package body Gamepads is

    -- Id to use for the next Gamepad instance. This is incremented every time a
    -- new Gamepad is created and a new Gamepad is created every time a new
    -- device is connected.
    nextId : Integer := 1;

    type Button_Label_Array is array (Gamepad_Button range <>) of String(1..6);
    type Axis_Label_Array is array (Gamepad_Axis range <>) of String(1..7);

    AXIS_LABELS : constant Axis_Label_Array(AXIS_LXP..AXIS_RTRIG) :=
    (
        AXIS_LXP   => "leftx+ ",
        AXIS_LXN   => "leftx- ",
        AXIS_LYP   => "lefty+ ",
        AXIS_LYN   => "lefty- ",
        AXIS_RXP   => "rightx+",
        AXIS_RXN   => "rightx-",
        AXIS_RYP   => "righty+",
        AXIS_RYN   => "righty-",
        AXIS_LTRIG => "ltrig  ",
        AXIS_RTRIG => "rtrig  "
    );

    BUTTON_LABELS : constant Button_Label_Array(BUTTON_SOUTH..BUTTON_DPADU) :=
    (
        BUTTON_SOUTH => "south ",
        BUTTON_EAST  => "east  ",
        BUTTON_WEST  => "west  ",
        BUTTON_NORTH => "north ",
        BUTTON_BACK  => "back  ",
        BUTTON_START => "start ",
        BUTTON_L1    => "left1 ",
        BUTTON_R1    => "right1",
        BUTTON_LSB   => "lstick",
        BUTTON_RSB   => "rstick",
        BUTTON_DPADR => "dpadr ",
        BUTTON_DPADL => "dpadl ",
        BUTTON_DPADD => "dpadd ",
        BUTTON_DPADU => "dpadu "
    );

    ----------------------------------------------------------------------------

    function Get_Gamepad_Axis_Description( axis : Gamepad_Axis ) return String is
    (case axis is
        when AXIS_NONE  => "(No Axis)",
        when AXIS_LXP   => "Left X+",
        when AXIS_LXN   => "Left X-",
        when AXIS_LYP   => "Left Y+",
        when AXIS_LYN   => "Left Y-",
        when AXIS_RXP   => "Right X+",
        when AXIS_RXN   => "Right X-",
        when AXIS_RYP   => "Right Y+",
        when AXIS_RYN   => "Right Y-",
        when AXIS_LTRIG => "Left Trigger",
        when AXIS_RTRIG => "Right Trigger",
        when AXIS_ANY   => "",
        when others     => "Axis" & axis'Img
    );

    ----------------------------------------------------------------------------

    -- Translates a gamepad axis label string to a gamepad axis number (one
    -- of the AXIS_* constants in the spec). Returns 'fallback' if the label is
    -- not one of the standard gamepad axis labels.
    function Get_Gamepad_Axis_From_Label( label : String; fallback : Gamepad_Axis ) return Gamepad_Axis is
    begin
        for i in AXIS_LABELS'Range loop
            if Trim( AXIS_LABELS(i), Right ) = label then
                return i;
            end if;
        end loop;
        return fallback;
    end Get_Gamepad_Axis_From_Label;

    ----------------------------------------------------------------------------

    function Get_Gamepad_Button_Description( button : Gamepad_Button ) return String is
    (case button is
        when BUTTON_NONE  => "(No Button)",
        when BUTTON_SOUTH => "South",
        when BUTTON_EAST  => "East",
        when BUTTON_WEST  => "West",
        when BUTTON_NORTH => "North",
        when BUTTON_BACK  => "Back",
        when BUTTON_START => "Start",
        when BUTTON_L1    => "L1",
        when BUTTON_R1    => "R1",
        when BUTTON_LSB   => "Left Stick",
        when BUTTON_RSB   => "Right Stick",
        when BUTTON_DPADR => "Right D-Pad",
        when BUTTON_DPADL => "Left D-Pad",
        when BUTTON_DPADD => "Down D-Pad",
        when BUTTON_DPADU => "Up D-Pad",
        when BUTTON_ANY   => "",
        when others       => "Button" & button'Img
    );

    ----------------------------------------------------------------------------

    -- Translates a gamepad button label string to a gamepad button number (one
    -- of the BUTTON_* constants in the spec). Returns 'fallback' if the label
    -- is not one of the standard gamepad button labels:
    function Get_Gamepad_Button_From_Label( label : String; fallback : Gamepad_Button ) return Gamepad_Button is
    begin
        for i in BUTTON_LABELS'Range loop
            if Trim( BUTTON_LABELS(i), Right ) = label then
                return i;
            end if;
        end loop;
        return fallback;
    end Get_Gamepad_Button_From_Label;

    ----------------------------------------------------------------------------

    -- Returns the standard gamepad label for gamepad axis 'axis', which is
    -- expected to be one of the AXIS_* constants in the spec. Returns an empty
    -- string if the axis is unrecognized.
    function Get_Label_For_Gamepad_Axis( axis : Gamepad_Axis ) return String is
    begin
        if axis in AXIS_LABELS'Range then
            return Trim( AXIS_LABELS(axis), Right );
        end if;
        return "";
    end Get_Label_For_Gamepad_Axis;

    ----------------------------------------------------------------------------

    -- Returns the standard gamepad label for gamepad button 'button', which is
    -- expected to be one of the BUTTON_* constants in the spec. Returns an
    -- empty string if the axis is unrecognized.
    function Get_Label_For_Gamepad_Button( button : Gamepad_Button ) return String is
    begin
        if button in BUTTON_LABELS'Range then
            return Trim( BUTTON_LABELS(button), Right );
        end if;
        return "";
    end Get_Label_For_Gamepad_Button;

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Gamepad( device : not null A_Allegro_Joystick ) return A_Gamepad is
        this : A_Gamepad;
    begin
        this := new Gamepad;
        this.Construct( device, nextId );
        nextId := nextId + 1;
        return this;
    end Create_Gamepad;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Gamepad; device : A_Allegro_Joystick; id : Integer ) is
        state   : Allegro_Joystick_State;
        mapFile : A_Scribble_Asset;
        mapping : Map_Value;
        axis    : Natural := 0;
    begin
        Limited_Object(this.all).Construct;
        this.device := device;
        this.id := id;

        this.name := To_Unbounded_String( Al_Get_Joystick_Name( this.device ) );
        Al_Get_Joystick_State( this.device, state );
        Dbg( "Detected new controller: " & To_String( this.name ), D_INPUT, Info );

        if Index( this.name, "XInput" ) > 0 then
            Delete( mapFile );
            mapFile := Assets.Scribble.Load_Value( "xinput.controller", "resources" );
            mapping := mapFile.Get_Value.Map;
        end if;
        if not mapping.Is_Map then
            Delete( mapFile );
            mapFile := Assets.Scribble.Load_Value( "default.controller", "resources" );
            mapping := mapFile.Get_Value.Map;
        end if;
        Dbg( "Using controller mapping: " & mapFile.Get_Path, D_INPUT, Info );

        -- use the controller mapping to translate raw button numbers to virtual
        -- gamepad buttons.
        if not mapping.Is_Map then
            Dbg( "No controller mapping found for " & To_String( this.name ), D_INPUT, Warning );
        end if;

        this.stickCount := Integer'Min( Al_Get_Joystick_Num_Sticks( this.device ), this.sticks'Last );
        for s in 0..this.stickCount-1 loop
            Dbg( "  Stick " & Image( s ), D_INPUT, Info );
            this.sticks(s).axisCount := Al_Get_Joystick_Num_Axes( this.device, s );
            for a in 0..this.sticks(s).axisCount-1 loop
                this.sticks(s).axes(a).pos := state.stick(s).axis(a);
                if mapping.Valid then
                    this.sticks(s).axes(a).posEvt := Get_Gamepad_Axis_From_Label( To_Lower( mapping.Get_String( "axis" & Image( axis ) & "p" ) ), this.sticks(s).axes(a).posEvt );
                    this.sticks(s).axes(a).negEvt := Get_Gamepad_Axis_From_Label( To_Lower( mapping.Get_String( "axis" & Image( axis ) & "n" ) ), this.sticks(s).axes(a).negEvt );
                end if;
                Dbg( "    Axis " & Image( a ) & "+ (""" & Al_Get_Joystick_Axis_Name( this.device, s, a ) & """) => " &
                     mapping.Get_String( "axis" & Image( axis ) & "p" ), D_INPUT, Info );
                Dbg( "    Axis " & Image( a ) & "- (""" & Al_Get_Joystick_Axis_Name( this.device, s, a ) & """) => " &
                     mapping.Get_String( "axis" & Image( axis ) & "n" ), D_INPUT, Info );
                axis := axis + 1;
            end loop;
        end loop;

        this.buttonCount := Integer'Min( Al_Get_Joystick_Num_Buttons( this.device ), this.buttons'Last + 1 );
        for b in 0..this.buttonCount-1 loop
            -- convert an analog button reading (15 bits) to a boolean button state
            this.buttons(b).state := state.button(b) >= 16384;
            if mapping.Valid then
                this.buttons(b).evt := Get_Gamepad_Button_From_Label( To_Lower( mapping.Get_String( "button" & Image( b ) ) ), this.buttons(b).evt );
            end if;
            Dbg( "  Button " & Image( b ) & " (""" & Al_Get_Joystick_Button_Name( this.device, b ) & """) => " &
                 mapping.Get_String( "button" & Image( b ) ), D_INPUT, Info );
        end loop;

    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Gamepad ) is
    begin
        this.device := null;           -- not owned, not destroyable
    end Delete;

    ----------------------------------------------------------------------------

    function Is_Active( this : not null access Gamepad'Class ) return Boolean is
    begin
        if not Al_Get_Joystick_Active( this.device ) then
            if this.active then
                -- force button release events when the gamepad is unplugged
                for i in 0..this.buttonCount-1 loop
                    if this.buttons(i).state then
                        this.Set_Button( i - this.buttons'First, pressed => False );
                    end if;
                end loop;

                Dbg( "Controller removed: " & To_String( this.name ), D_INPUT, Info );
                Queue_Gamepad_Removed( this.id );
            end if;
            return False;
        end if;
        return True;
    end Is_Active;

    ----------------------------------------------------------------------------

    procedure Set_Button( this    : not null access Gamepad'Class;
                          button  : Natural;
                          pressed : Boolean ) is
    begin
        if button < this.buttonCount then
            if pressed /= this.buttons(button).state then
                this.buttons(button).state := pressed;
                if this.buttons(button).evt >= 0 then
                    Dbg( "Gamepad" & this.id'Img & ": Button '" &
                         Get_Label_For_Gamepad_Button( this.buttons(button).evt ) &
                         "' " & (if pressed then "pressed" else "released"),
                         D_INPUT, Info );
                    if pressed then
                        Queue_Gamepad_Press( this.id, this.buttons(button).evt );
                    else
                        Queue_Gamepad_Release( this.id, this.buttons(button).evt );
                    end if;
                end if;
            end if;
        end if;
    end Set_Button;

    ----------------------------------------------------------------------------

    procedure Set_Position( this     : not null access Gamepad'Class;
                            stick    : Natural;
                            axis     : Natural;
                            position : Float ) is

        ------------------------------------------------------------------------

        procedure Queue_Event( axis : Gamepad_Axis; position : Float ) is
        begin
            Dbg( "Gamepad" & this.id'Img & ": Axis '" &
                 Get_Label_For_Gamepad_Axis( axis ) & "' = " & Image( position ),
                 D_INPUT, Info );
            if axis /= AXIS_NONE then
                Queue_Gamepad_Move( this.id, axis, position );
            end if;
        end Queue_Event;

        ------------------------------------------------------------------------

        position2 : Float := 0.0;
    begin
        if stick < this.stickCount then
            if axis < this.sticks(stick).axisCount then
                if (abs position) > this.sticks(stick).deadzone then
                    if position > 0.0 then
                        position2 := (position - this.sticks(stick).deadzone) / (1.0 - this.sticks(stick).deadzone);
                    else
                        position2 := (position + this.sticks(stick).deadzone) / (1.0 - this.sticks(stick).deadzone);
                    end if;
                end if;

                if position2 /= this.sticks(stick).axes(axis).pos then

                    -- when changing from a negative position to a non-negative,
                    -- or a positive position to a non-positive, pass through
                    -- the center.
                    if this.sticks(stick).axes(axis).pos < 0.0 and position2 >= 0.0 then
                        Queue_Event( this.sticks(stick).axes(axis).negEvt, 0.0 );
                    elsif this.sticks(stick).axes(axis).pos > 0.0 and position2 <= 0.0 then
                        Queue_Event( this.sticks(stick).axes(axis).posEvt, 0.0 );
                    end if;

                    if position2 > 0.0 then
                        Queue_Event( this.sticks(stick).axes(axis).posEvt, position2 );
                    elsif position2 < 0.0 then
                        Queue_Event( this.sticks(stick).axes(axis).negEvt, position2 );
                    end if;

                    this.sticks(stick).axes(axis).pos := position2;
                end if;
            end if;
        end if;
    end Set_Position;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Gamepad ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

end Gamepads;
