--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Ada.Containers;                    use Ada.Containers;
with Ada.Exceptions;                    use Ada.Exceptions;
with Allegro.Configuration;             use Allegro.Configuration;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Clipboard;                 use Allegro.Clipboard;
--with Allegro.Native_Dialogs;            use Allegro.Native_Dialogs;
with Allegro.Shaders;                   use Allegro.Shaders;
with Allegro.System;                    use Allegro.System;
with Applications;                      use Applications;
with Applications.Gui.Games;            use Applications.Gui.Games;
with Assets.Shaders;                    use Assets.Shaders;
with Debugging;                         use Debugging;
with Events.Application;                use Events.Application;
with Events.Game;                       use Events.Game;
with Gamepads;                          use Gamepads;
with Interfaces;                        use Interfaces;
with Keyboard;                          use Keyboard;
with Palette;                           use Palette;
with Preferences;                       use Preferences;
with Scribble;                          use Scribble;
with Scribble.Parse_Errors;             use Scribble.Parse_Errors;
with Support;                           use Support;
with Values.Casting;                    use Values.Casting;
with Values.Construction;               use Values.Construction;
with Values.Strings;                    use Values.Strings;
with Widget_Styles.Registry;

package body Game_Views is

    package Connections is new Signals.Connections(Game_View);
    use Connections;

    ----------------------------------------------------------------------------

    procedure Calculate_Frame_Extrapolation( this : not null access Game_View'Class );

    -- Called at the start of each frame of the view thread, to synchronize the
    -- view and game threads. This prevents frame skipping.
    procedure On_Frame_Start( this : not null access Game_View'Class );

    -- Populates the defined bindings with values previously stored to disk.
    -- Bindings that have not been defined yet will not be loaded from disk if a
    -- value has been saved.
    procedure Load_Bindings( this : not null access Game_View'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    not overriding
    procedure Construct( this : access Game_View; maxFps : Natural ) is
    begin
        Limited_Object(this.all).Construct;
        this.maxFps := Constrain( maxFps, 0, 1000 );

        this.sigInitializing   .Init( this );
        this.sigBindingsChanged.Init( this );
        this.sigFinalizing     .Init( this );
        this.sigQuitting       .Init( this );

        this.sigGameStarted    .Init( this );
        this.sigLoadingBegan   .Init( this );
        this.sigLoadingEnded   .Init( this );
        this.sigLoadingFailed  .Init( this );
        this.sigPausedChanged  .Init( this );

        -- Prevent Allegro automatically resizing the window when the menubar is
        -- enabled. See https://github.com/liballeg/allegro5/pull/861 introduced
        -- in Allegro 5.2.4. Allegro's logic maintains the same drawable area by
        -- increasing the window's height when the menubar is enabled.
        Al_Set_Config_Value( Al_Get_System_Config, "compatibility", "automatic_menu_display_resize", "false" );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Game_View ) is
    begin
        this.Delete_Pending_Widgets;

        Delete( this.renderer );
        if this.win /= null then
            while this.win.Is_Popup_Active loop
                this.win.Pop_Popup;
            end loop;
            this.win.Set_Modal( null );
            this.win.Visible.Set( False );
            Delete( this.win );
        end if;

        Delete( this.inhandler );
        Delete( this.audioPlayer );

        Delete( this.pman );

        -- warn of any leaked widgets
        if this.widgets.Length > 0 then
            Dbg( "Game_Views.Delete: " & Image( Integer(this.widgets.Length) ) &
                 " widgets were leaked",
                 D_GUI, Warning );
            for w of this.widgets loop
                Dbg( "  Widget ID = '" & w.Get_Id & "' : " & w.To_String, D_GUI, Warning );
            end loop;
        end if;

        Limited_Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Initializing( this : not null access Game_View'Class ) return access Signal'Class is (this.sigInitializing'Access);

    function BindingsChanged( this : not null access Game_View'Class ) return access Signal'Class is (this.sigBindingsChanged'Access);

    function Finalizing( this : not null access Game_View'Class ) return access Signal'Class is (this.sigFinalizing'Access);

    function GameStarted( this : not null access Game_View'Class ) return access Signal'Class is (this.sigGameStarted'Access);

    function LoadingBegan( this : not null access Game_View'Class ) return access Signal'Class is (this.sigLoadingBegan'Access);

    function LoadingEnded( this : not null access Game_View'Class ) return access Signal'Class is (this.sigLoadingEnded'Access);

    function LoadingFailed( this : not null access Game_View'Class ) return access Signal'Class is (this.sigLoadingFailed'Access);

    function PausedChanged( this : not null access Game_View'Class ) return access Signal'Class is (this.sigPausedChanged'Access);

    function Quitting( this : not null access Game_View'Class ) return access Signal'Class is (this.sigQuitting'Access);

    ----------------------------------------------------------------------------

    procedure Attach( this : not null access Game_View'Class;
                      proc : not null access Process'Class ) is
    begin
        this.pman.Attach_Async( proc );
    end Attach;

    ----------------------------------------------------------------------------

    procedure Calculate_Frame_Extrapolation( this : not null access Game_View'Class ) is
        -- the game's fixed time step per frame
        timeStep : constant Float := 1.0 / Float(A_Game_Application(Get_Application).Get_Game.Get_Process_Manager.Get_Target_Rate);

        -- the end time of the most recent game frame
        frameEnd : constant Time := this.pman.Get_Corral.Get_Frame_End( GAME_CHANNEL );

        -- the total game frame count, used for detecting skipped frames
        frameCount : constant Integer := this.pman.Get_Corral.Get_Frame_Count( GAME_CHANNEL );

        -- the number of game frames elapsed since the previous view frame
        elapsedFrames : Integer;

        -- the ratio of game simulation time to real time
        gameLagRatio  : Float;
    begin
        -- on the first frame, we don't have a previous frame
        if this.prevFrameEnd = Time_First then
            this.prevFrameEnd := frameEnd;
            this.prevFrameCount := frameCount;
        end if;

        -- if we have a new game frame, calculate the real time duration of it
        elapsedFrames := frameCount - this.prevFrameCount;
        if elapsedFrames > 0 then
            -- if multiple game frames occurred since .prevFrameEnd was updated
            -- (on the previous view frame) then use the average real time
            -- duration of the skipped frames.
            this.prevFrameElapsed := (frameEnd - this.prevFrameEnd) / elapsedFrames;
        end if;

        -- calculate the frame extrapolation factor
        --
        -- it's the number of whole and partial frames that should have been
        -- simulated in the game thread since the end of the most recently seen
        -- game frame, assuming the game thread is keeping up with real time
        -- (meeting processing time budget). Typically, this frame count will be
        -- less than one. If the game thread is over budget and lagging behind
        -- real time, we will account for that lag next.
        if not this.paused then
            this.frameExtrap := Float'Max( 0.0, Float(To_Duration( this.frameStart - frameEnd )) / timeStep );
        else
            this.frameExtrap := 0.0;
        end if;

        -- calculate the game lag ratio
        --
        -- this number corresponds with how far over budget the game update
        -- thread is running. if the game thread is under its time budget, then
        -- it keeps up with real time and the lag ratio is 1.0, meaning no lag:
        -- game time = real time. If it exceeds its time budget by a factor of
        -- 2 (for example), the lag ratio drops to 0.5, meaning 1 second of
        -- game time requires 2 seconds of real time to simulate.
        if Float(To_Duration( this.prevFrameElapsed )) > timeStep then
            gameLagRatio := timeStep / Float(To_Duration( this.prevFrameElapsed ));
        else
            gameLagRatio := 1.0;
        end if;

        -- due to small rounding and timing errors, the measured game frame time
        -- can be slightly more than the target, even when the game thread is
        -- under budget. correct for this by clamping the lag ratio to none if
        -- it's within a small threshold.
        if gameLagRatio >= 0.97 then
            gameLagRatio := 1.0;
        end if;

        -- convert the partial elapsed game frame(s) in real time to game
        -- simulation time.
        --
        -- this scales the frame extrapolation factor when the game thread is
        -- running over budget and lagging behind real time.
        this.frameExtrap := this.frameExtrap * gameLagRatio;

        --Dbg( "frame = " & Image( frameCount ) & ", extrap = " & Image( this.frameExtrap ) & ", lag = " & Image( gameLagRatio ) &
        --     ", frameElapsed = " & Image( Float(To_Duration( this.prevFrameElapsed )) ) &
        --     ", postFrameElapsed = " & Image( Float(To_Duration( this.frameStart - frameEnd )) ) &
        --     (if elapsedFrames = 0 then ", duplicate"
        --      elsif elapsedFrames > 1 then ", skipped" & Integer'Image( elapsedFrames )
        --      else "")
        --);

        this.prevFrameEnd := frameEnd;
        this.prevFrameCount := frameCount;
    end Calculate_Frame_Extrapolation;

    ----------------------------------------------------------------------------

    procedure Define_Binding( this     : not null access Game_View'Class;
                              action   : Input_Action;
                              name     : String;
                              defaults : Input_Binding ) is
    begin
        this.bindings.Include( action, (action  => action,
                                        name    => To_Unbounded_String( name ),
                                        binding => defaults) );
        Preferences.Set_Default( "bindings", To_Lower( name ) & ".key", defaults.keycode );
        Preferences.Set_Default( "bindings", To_Lower( name ) & ".axis", Integer(defaults.axis) );
        Preferences.Set_Default( "bindings", To_Lower( name ) & ".button", Integer(defaults.button) );
    end Define_Binding;

    ----------------------------------------------------------------------------

    procedure Delete_Pending_Widgets( this : not null access Game_View'Class ) is
    begin
        for w of this.deletedWidgets loop
            Delete( w );
        end loop;
        this.deletedWidgets.Clear;
    end Delete_Pending_Widgets;

    ----------------------------------------------------------------------------

    procedure Detach( this : not null access Game_View'Class;
                      proc : not null access Process'Class ) is
    begin
        this.pman.Detach_Async( proc );
    end Detach;

    ----------------------------------------------------------------------------

    procedure Detach( this : not null access Game_View'Class;
                      proc : Process'Class ) is
    begin
        this.Detach( proc'Unrestricted_Access );
    end Detach;

    ----------------------------------------------------------------------------

    function Get_Clipboard( this   : not null access Game_View'Class;
                            format : String ) return Value'Class is
        content : Value;
    begin
        if format = "text" then
            if Al_Clipboard_Has_Text( this.display ) then
                content := Create( Unbounded_String'(Al_Get_Clipboard_Text( this.display )) );
                if content.Str.Length = 0 then
                    -- an empty string is considered to be no content
                    content := Null_Value;
                end if;
            end if;
        elsif format = this.clipboardFormat then
            content := Clone( this.clipboard );
        end if;
        return content;
    end Get_Clipboard;

    ----------------------------------------------------------------------------

    function Get_Clipboard_Format( this : not null access Game_View'Class ) return String is (To_String( this.clipboardFormat ));

    ----------------------------------------------------------------------------

    function Get_Corral( this : not null access Game_View'Class ) return A_Corral is (this.pman.Get_Corral);

    ----------------------------------------------------------------------------

    function Get_Display( this : not null access Game_View'Class ) return A_Allegro_Display is (this.display);

    ----------------------------------------------------------------------------

    function Get_Frame_Extrapolation( this : not null access Game_View'Class ) return Float is (this.frameExtrap);

    ----------------------------------------------------------------------------

    function Get_Binding( this : not null access Game_View'Class; action : Input_Action ) return Input_Binding is
        pos    : constant Binding_Maps.Cursor := this.bindings.Find( action );
        result : Input_Binding;
    begin
        if Binding_Maps.Has_Element( pos ) then
            result := Binding_Maps.Element( pos ).binding;
        end if;
        return result;
    end Get_Binding;

    ----------------------------------------------------------------------------

    function Get_Binding_Name( this   : not null access Game_View'Class;
                               action : Input_Action ) return String is
        pos : constant Binding_Maps.Cursor := this.bindings.Find( action );
    begin
        if Binding_Maps.Has_Element( pos ) then
            return To_String( Binding_Maps.Element( pos ).name );
        end if;
        return "";
    end Get_Binding_Name;

    ----------------------------------------------------------------------------

    function Get_Input_Handler( this : not null access Game_View'Class ) return A_Input_Handler is (this.inhandler);

    ----------------------------------------------------------------------------

    function Get_Load_Error( this : not null access Game_View'Class ) return String is (To_String( this.loadError ));

    ----------------------------------------------------------------------------

    function Get_Process_Manager( this : not null access Game_View'Class ) return A_Process_Manager is (this.pman);

    ----------------------------------------------------------------------------

    function Get_Widget( this : not null access Game_View'Class;
                         id   : String ) return A_Widget is
        pos : Widget_Registry.Cursor;
    begin
        pos := this.widgets.Find( id );
        if Has_Element( pos ) then
            return Element( pos );
        else
            if id'Length > 0 then
                Dbg( "Widget id '" & id & "' not found", D_GUI, Warning );
                raise ID_NOT_FOUND with "Widget not found, id: " & id;
            else
                raise ID_NOT_FOUND with "Widget not found, no id given";
            end if;
        end if;
    end Get_Widget;

    ----------------------------------------------------------------------------

    function Get_Window( this : not null access Game_View'Class ) return A_Window is (this.win);

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Event( this : access Game_View;
                            evt  : in out A_Event;
                            resp : out Response_Type ) is
        pragma Unreferenced( resp );
        close          : Boolean := False;
        maximized      : Boolean := False;
        wasMaximized   : Boolean := False;
        fullWindow     : Boolean := False;
        menuBarChanged : Boolean := False;
        pragma Warnings( Off, menuBarChanged );
    begin
        case evt.Get_Event_Id is
            when EVT_GAME_PAUSED =>
                if this.paused /= A_Gameplay_Event(evt).Is_Paused then
                    this.paused := not this.paused;
                    this.PausedChanged.Emit;
                end if;

            when EVT_GAME_STARTED =>
                this.GameStarted.Emit;

            when EVT_LOADING_BEGAN =>
                this.loadError := To_Unbounded_String( "" );
                this.LoadingBegan.Emit;

            when EVT_LOADING_ENDED =>
                if A_Loading_Event(evt).Is_Success then
                    this.LoadingEnded.Emit;
                else
                    this.loadError := To_Unbounded_String( A_Loading_Event(evt).Get_Error );
                    this.LoadingFailed.Emit;
                end if;

            when EVT_VIEW_MESSAGE =>
                A_Game_View(this).On_Message( A_Message_Event(evt).Get_Name,
                                              A_Message_Event(evt).Get_Args );

            when EVT_CLOSE_WINDOW =>
                close := True;
                A_Game_View(this).On_Close_Window( close );
                if close then
                    this.Quit;
                end if;

            when EVT_EXIT_APPLICATION =>
                A_Game_Application(Get_Application).Stop;

            when EVT_WINDOW_RESIZED =>
                if A_Window_Event(evt).Get_Display = this.display then
                    -- Acknowledge the resize on the drawing thread because it
                    -- resets projections and drawing. Don't use the display
                    -- size in the event because it may already be out of date.
                    Al_Acknowledge_Resize( this.display );

#if WINDOWS'Defined then
                    -- On Windows, adding a menubar to a window eats into the
                    -- client area and sends a window resize event to notify the
                    -- application that its drawing area got smaller. We trap
                    -- that resize event here and record the height of the menu
                    -- bar so that the stored "application.yres" preference can
                    -- be appropriately adjusted. This stored yres is the total
                    -- height of the window (including the menubar) because it
                    -- is used at display creation, before a menubar is added.
                    if this.win.Has_Menubar and this.menubarHeight <= 0 then
                        this.menubarHeight := Integer'Max( this.initialHeight - Al_Get_Display_Height( this.display ), 0 );
                        menuBarChanged := this.menubarHeight > 0;
                    elsif not this.win.Has_Menubar and this.menubarHeight > 0 then
                        this.menubarHeight := 0;
                        menuBarChanged := True;
                    end if;
#end if;

                    -- TODO: Figure out how to receive the display resize event
                    -- from Allegro on this thread, because we could acknowledge
                    -- it sooner and make the drawing more responsive when
                    -- resizing. Basically skip the inter-frame delay and draw
                    -- again immediately when a resize is received. Trigger the
                    -- event instead of queueing it?

                    fullWindow := (Al_Get_Display_Flags( this.display ) and ALLEGRO_FULLSCREEN_WINDOW) = ALLEGRO_FULLSCREEN_WINDOW;
                    wasMaximized := Preferences.Get_Pref( "application", "maximized" );
                    maximized := (Al_Get_Display_Flags( this.display ) and ALLEGRO_MAXIMIZED) = ALLEGRO_MAXIMIZED;
                    Preferences.Set_Pref( "application", "maximized", maximized );

                    -- only update the persisted window size when it's not
                    -- maximized, to avoid overwriting the window size with the
                    -- display size when maximized.
                    if not maximized and not fullWindow then
                        -- the window constraints are set only when the window
                        -- goes from maximized to unmaximized. setting size
                        -- constraints will unmaximize it on Windows.
                        --
                        -- NOTE: There is a bug in Allegro 5.2.x on Windows that
                        -- does not take the menubar into account when calling
                        -- AdjustWindowRectEx() so the Window size jitters. This
                        -- bug was hand-corrected in the binaries accompanying
                        -- AlAda 5.2.2 through 5.2.5.
                        if wasMaximized or menuBarChanged then
                            -- only apply constraints when going from maximized
                            -- to unmaximized, or when adding/removing a menubar.
                            -- The Windows 10 April 2018 Update will stop resizing
                            -- the window at all, if it's resized too fast and
                            -- constaints are applied every time. (A race
                            -- condition bug in Windows 10?) We avoid the problem
                            -- be updating the constraints only when needed.
                            Al_Set_Window_Constraints( this.display, Integer(this.win.Get_Min_Width), Integer(this.win.Get_Min_Height), 0, 0 );
                            Al_Apply_Window_Constraints( this.display, True );
                        end if;
                        Preferences.Set_Pref( "application", "xres", Al_Get_Display_Width( this.display ) );
                        Preferences.Set_Pref( "application", "yres", Al_Get_Display_Height( this.display ) + this.menubarHeight );
                    end if;

                    this.win.Width.Set( Float(Al_Get_Display_Width( this.display )) );
                    this.win.Height.Set( Float(Al_Get_Display_Height( this.display )) );
                end if;

            when others => null;
        end case;
    end Handle_Event;

    ----------------------------------------------------------------------------

    procedure Initialize( this    : not null access Game_View'Class;
                          display : not null A_Allegro_Display ) is
    begin
        pragma Assert( this.display = null );

        -- set the application's display active on this thread so any bitmaps
        -- loaded by the view will be associated with it.
        this.display := display;
        Al_Set_Target_Backbuffer( this.display );

        this.pman := Create_Process_Manager_Real_Time( "view",
                                                       this.maxFps,
                                                       eventChannel => VIEW_CHANNEL );

        this.audioPlayer := Create_Audio_Player( this.pman.Get_Corral );
        this.inhandler := Create_Input_Handler( this.display, A_Game_Application(Get_Application).Is_Mouse_Enabled );
        this.Load_Bindings;

        this.pman.Attach_Async( this );
        this.pman.Get_Corral.Add_Listener( this, EVT_CLOSE_WINDOW );
        this.pman.Get_Corral.Add_Listener( this, EVT_EXIT_APPLICATION );
        this.pman.Get_Corral.Add_Listener( this, EVT_WINDOW_RESIZED );
        this.pman.Get_Corral.Add_Listener( this, EVT_GAME_PAUSED );
        this.pman.Get_Corral.Add_Listener( this, EVT_GAME_STARTED );
        this.pman.Get_Corral.Add_Listener( this, EVT_LOADING_BEGAN );
        this.pman.Get_Corral.Add_Listener( this, EVT_LOADING_ENDED );
        this.pman.Get_Corral.Add_Listener( this, EVT_VIEW_MESSAGE );

        this.pman.FrameStarted.Connect( Slot( this, On_Frame_Start'Access ) );

        this.pman.Attach_Async( this.audioPlayer );

        -- record the initial display size before a menubar is added
        this.initialWidth := Al_Get_Display_Width( this.display );
        this.initialHeight := Al_Get_Display_Height( this.display );

        -- load widget styles from disk
        begin
            Widget_Styles.Registry.Load_Style_Sheet( Get_Application.Get_Name & ".style" );
        exception
            when e : Parse_Error =>
                Dbg( "Stylesheet compilation failure" & ASCII.LF &
                     Format_Parse_Error( Exception_Message( e ) ),
                     D_VIEW or D_GUI, Error );
        end;

        this.win := Create_Window( this );
        this.win.Width.Set( Float(Al_Get_Display_Width( this.display )) );
        this.win.Height.Set( Float(Al_Get_Display_Height( this.display )) );

        this.renderer := Create_Renderer( this.display,
                                          A_Widget(this.win),
                                          Html_To_Color( Get_Pref( "application", "background" ), Black ) );
        this.pman.Attach_Async( this.renderer );

        this.Initializing.Emit;
    exception
        when e : others =>
            Al_Set_Target_Backbuffer( null );
            Dbg( "Exception initializing view: ...", D_VIEW, Error );
            Dbg( e, D_VIEW, Error );
            raise;
    end Initialize;

    ----------------------------------------------------------------------------

    procedure Finalize( this : not null access Game_View'Class ) is
    begin
        Al_Set_Target_Backbuffer( this.display );
        this.Finalizing.Emit;
        Al_Set_Target_Backbuffer( null );
    end Finalize;

    ----------------------------------------------------------------------------

    function Is_Paused( this : not null access Game_View'Class ) return Boolean is (this.paused);

    ----------------------------------------------------------------------------

    procedure Load_Bindings( this : not null access Game_View'Class ) is
    begin
        for b of this.bindings loop
            b.binding.keycode := Get_Pref( "bindings", To_Lower( To_String( b.name ) & ".key" ), b.binding.keycode );
            b.binding.axis := Gamepad_Axis(Get_Pref( "bindings", To_Lower( To_String( b.name ) ) & ".axis", Integer(b.binding.axis) ));
            b.binding.button := Gamepad_Button(Get_Pref( "bindings", To_Lower( To_String( b.name ) ) & ".button", Integer(b.binding.button) ));
        end loop;
    end Load_Bindings;

    ----------------------------------------------------------------------------

    procedure Load_Game( this : not null access Game_View'Class; filename : String ) is
    begin
        this.pauseCounts := 0;
        Queue_Game_Message( "LoadGame", (1 => Create( filename )) );
    end Load_Game;

    ----------------------------------------------------------------------------

    procedure On_Frame_Start( this : not null access Game_View'Class ) is
        app            : constant A_Game_Application := A_Game_Application(Get_Application);
        viewTargetRate : constant Float := Float(this.Get_Process_Manager.Get_Target_Rate);
        gameTargetRate : constant Float := Float(app.Get_Game.Get_Process_Manager.Get_Target_Rate);
        gameFrameRate  : constant Float := app.Get_Game.Get_Process_Manager.Get_Frame_Rate;
        skipN          : constant Float := viewTargetRate / gameTargetRate;
    begin
        -- prevent frame skipping by waiting for the game to finish generating a
        -- frame if the view is actually running (almost) as fast as the game
        -- is supposed to run, and if the game is actually running (almost) as
        -- fast as the view is actually running. There is a very small frame
        -- rate window in which the frames should be synchronized, otherwise the
        -- framerate will drop noticeably when frame synchronization kicks in.
        if this.Get_Process_Manager.Get_Frame_Rate * 1.1 > gameTargetRate * skipN and then
           this.Get_Process_Manager.Get_Frame_Rate       < gameFrameRate * skipN * 1.1
        then
            -- synchronize every Nth frame
            if not this.frameSyncOn then
                this.frameSyncOn := True;
                pragma Debug( Dbg( "View frame sync is ON", D_PROCS, Info ) );
            end if;
            if this.frames mod Integer(Float'Max( 1.0, skipN ) ) = 0 then
                app.Get_Game.Get_Process_Manager.Wait_Frame_End( Milliseconds( 500 ) );
            end if;
        else
            if this.frameSyncOn then
                this.frameSyncOn := False;
                pragma Debug( Dbg( "View frame sync is OFF", D_PROCS, Info ) );
            end if;
        end if;

        this.frames := this.frames + 1;
        this.frameStart := Clock;
    end On_Frame_Start;

    ----------------------------------------------------------------------------

    procedure Pause_Game( this : not null access Game_View'Class ) is
    begin
        if this.pauseCounts = 0 then
            Queue_Pause_Game( True );
        end if;
        this.pauseCounts := this.pauseCounts + 1;
        -- don't change .paused here, this is only where the command is issued.
        -- the state is changed by receiving a Game_Paused event.
    end Pause_Game;

    ----------------------------------------------------------------------------

    procedure Resume_Game( this : not null access Game_View'Class ) is
    begin
        this.pauseCounts := this.pauseCounts - 1;
        if this.pauseCounts <= 0 then
            this.pauseCounts := 0;
            Queue_Pause_Game( False );
        end if;
        -- don't change .paused here, this is only where the command is issued.
        -- the state is changed by receiving a Game_Paused event.
    end Resume_Game;

    ----------------------------------------------------------------------------

    procedure Quit( this : not null access Game_View'Class ) is
    begin
        this.Quitting.Emit;
        Queue_Exit_Application;
    end Quit;

    ----------------------------------------------------------------------------

    procedure Register( this  : not null access Game_View'Class;
                        widgt : not null access Widget'Class ) is
        id       : constant String := widgt.Get_Id;
        pos      : Widget_Registry.Cursor;
        inserted : Boolean;
    begin
        this.widgets.Insert( widgt.Get_Id, widgt, pos, inserted );
        if not inserted then
            raise DUPLICATE_ID with "Duplicate widget id: " & id ;
        end if;
    end Register;

    ----------------------------------------------------------------------------

    procedure Run( this : not null access Game_View'Class ) is
    begin
        pragma Debug( Dbg( "Starting game view", D_VIEW, Info ) );
        this.inhandler.Start;
        this.pman.Run;
    end Run;

    ----------------------------------------------------------------------------

    procedure Save_Game( this : not null access Game_View'Class; filename : String ) is
        pragma Unreferenced( this );
    begin
        Queue_Game_Message( "SaveGame", (1 => Create( filename )) );
    end Save_Game;

    ----------------------------------------------------------------------------

    procedure Schedule_Widget_Delete( this  : not null access Game_View'Class;
                                      widgt : in out A_Widget ) is
    begin
        pragma Assert( not this.deletedWidgets.Contains( widgt ),
                       "Scheduled deletion twice for " & widgt.To_String );
        pragma Assert( not this.widgets.Contains( widgt.Get_Id ) or else
                       this.widgets.Element( widgt.Get_Id ) /= widgt,
                       "Deleting a registered widget" );

        this.deletedWidgets.Append( widgt );
        widgt := null;
    end Schedule_Widget_Delete;

    ----------------------------------------------------------------------------

    procedure Set_Binding( this    : not null access Game_View'Class;
                           action  : Input_Action;
                           binding : Input_Binding ) is
        pos : constant Binding_Maps.Cursor := this.bindings.Find( action );
    begin
        if not Binding_Maps.Has_Element( pos ) then
            -- binding not defined
            return;
        end if;

        for b of this.bindings loop
            if b.action /= action then
                if b.binding.keycode = binding.keycode then
                    b.binding.keycode := KEY_ANY;
                    Set_Pref( "bindings", To_Lower( To_String( b.name ) ) & ".key", b.binding.keycode );
                end if;
                if b.binding.axis = binding.axis then
                    b.binding.axis := AXIS_NONE;
                    Set_Pref( "bindings", To_Lower( To_String( b.name ) ) & ".axis", Integer(b.binding.axis) );
                end if;
                if b.binding.button = binding.button then
                    b.binding.button := BUTTON_NONE;
                    Set_Pref( "bindings", To_Lower( To_String( b.name ) ) & ".button", Integer(b.binding.button) );
                end if;
            end if;
        end loop;

        this.bindings.Reference( pos ).binding := binding;
        Set_Pref( "bindings", To_Lower( To_String( this.bindings.Reference( pos ).name ) ) & ".key", binding.keycode );
        Set_Pref( "bindings", To_Lower( To_String( this.bindings.Reference( pos ).name ) ) & ".axis", Integer(binding.axis) );
        Set_Pref( "bindings", To_Lower( To_String( this.bindings.Reference( pos ).name ) ) & ".button", Integer(binding.button) );
        -- todo: save all bindings to disk

        this.sigBindingsChanged.Emit;
    end Set_Binding;

    ----------------------------------------------------------------------------

    procedure Set_Clipboard( this   : not null access Game_View'Class;
                             format : String;
                             val    : Value'Class ) is
    begin
        this.clipboardFormat := To_Unbounded_String( format );
        if format = "text" then
            Al_Set_Clipboard_Text( this.display, Cast_Unbounded_String( val ) );
        else
            this.clipboard := Clone( val );
            Al_Set_Clipboard_Text( this.display, "" );
        end if;
    end Set_Clipboard;

    ----------------------------------------------------------------------------

    procedure Start_Game( this : not null access Game_View'Class ) is
    begin
        this.pauseCounts := 0;
        Queue_Start_Game;
    end Start_Game;

    ----------------------------------------------------------------------------

    procedure Stop( this : not null access Game_View'Class ) is
    begin
        pragma Debug( Dbg( "Stopping game view", D_VIEW, Info ) );
        this.pman.Stop;
        this.inhandler.Stop;
        this.audioPlayer.Shutdown;
    end Stop;

    ----------------------------------------------------------------------------

    overriding
    procedure Tick( this : access Game_View; time : Tick_Time ) is
        pragma Unreferenced( time );
    begin
        -- set the display active on the view thread before any rendering
        if Al_Get_Target_Bitmap /= Al_Get_Backbuffer( this.display ) then
            Al_Set_Target_Backbuffer( this.display );
        end if;

        -- this must be calculated after events have been dispatched and before
        -- any other view processes have been ticked.
        this.Calculate_Frame_Extrapolation;
    end Tick;

    ----------------------------------------------------------------------------

    procedure Unregister( this : not null access Game_View'Class; id : String ) is
        pos : Widget_Registry.Cursor := this.widgets.Find( id );
    begin
        if Has_Element( pos ) then
            this.widgets.Delete( pos );
        end if;
    end Unregister;

    ----------------------------------------------------------------------------

    procedure Use_Shader( this : not null access Game_View'Class; name : String ) is
        pragma Unreferenced( this );
        shader : A_Shader := Load_Shader( name );
    begin
        Al_Use_Shader( shader.Get_Al_Shader );
        Delete( shader );      -- delete the reference (the Allegro_Shader remains valid)
    end Use_Shader;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Game_View ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

end Game_Views;
