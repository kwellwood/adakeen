--
-- Copyright (c) 2017-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Shaders;                   use Allegro.Shaders;

package Assets.Shaders is

    type Shader is new Asset with private;
    type A_Shader is access all Shader'Class;

    -- Loads GLSL named shader 'name' from the "graphics" resource group. The
    -- returned asset will not be loaded if an error occurs. Compilation errors
    -- will be logged to the debug system.
    function Load_Shader( name : String ) return A_Shader;
    pragma Postcondition( Load_Shader'Result /= null );

    -- Returns a pointer to the Allegro shader object. This Asset will not be
    -- unloaded until application exit, so it is safe to use the shader program
    -- after calling Delete() on this Shader reference.
    function Get_Al_Shader( this : not null access Shader'Class ) return A_Allegro_Shader;

    procedure Delete( this : in out A_Shader );

private

    type Shader is new Asset with
        record
            name     : Unbounded_String;
            alShader : A_Allegro_Shader := null;
        end record;

    procedure Construct( this    : access Shader;
                         assetId : Value'Class;
                         name    : String );

    function Load_Data( this : access Shader ) return Boolean;

    procedure Unload_Data( this : access Shader );

end Assets.Shaders;
