--
-- Copyright (c) 2015-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Ada.Containers.Vectors;
with Allegro.Color;                     use Allegro.Color;
with Allegro.State;                     use Allegro.State;
with Events;                            use Events;
with Events.Listeners;                  use Events.Listeners;
with Maps.Info_Caches;                  use Maps.Info_Caches;
with Object_Ids;                        use Object_Ids;
with Objects;                           use Objects;
with Processes;                         use Processes;
with Quad_Trees;
with Rendering_Options;                 use Rendering_Options;
with Values.Tagged_Ids;                 use Values.Tagged_Ids;
with Vector_Math;                       use Vector_Math;

limited with Game_Views;

package Lighting_Systems is

    type Render_Pass is
    (
        Render_Color,        -- Render raw colors (solid/alpha blending)
        Render_Shading,      -- Render light blocking (just depth)
        Render_Light         -- Render lighting (additive blending)
    );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Lighting_System is new Limited_Object
                            and Process
                            and Event_Listener
                           with private;
    type A_Lighting_System is access all Lighting_System'Class;

    -- Creates and returns a new Lighting System.
    function Create_Lighting_System( view : not null access Game_Views.Game_View'Class ) return A_Lighting_System;

    -- Clears the lighting system's state, clearing all lights and walls.
    procedure Clear( this : not null access Lighting_System'Class );

    -- Initializes the lighting system, clearing all previous lights and walls.
    -- The shadow casting geometry will be rebuilt from tile map 'map'. If 'map'
    -- is null, no shadow geometry will be built. The lighting system will need
    -- to be reinitialized with a map before it can be used.
    procedure Initialize( this      : not null access Lighting_System'Class;
                          mapInfo   : not null A_Map_Info_Cache;
                          tileWidth : Positive );

    -- Enables one or more rendering options in 'opt', adding them to the
    -- currently enabled options if 'enabled' is True. Otherwise, the options
    -- will be disabled; removed from the currently enabled options.
    procedure Enable_Option( this    : not null access Lighting_System'Class;
                             opt     : Render_Options;
                             enabled : Boolean := True );

    -- Returns all the currently set rendering options.
    function Get_Render_Options( this : not null access Lighting_System'Class ) return Render_Options;

    -- Returns True if rendering option 'opt' is enabled, or False if disabled.
    -- If multiple options are combined in 'opt', then True will only be returned
    -- if ALL of them are enabled.
    function Is_Enabled( this : not null access Lighting_System'Class; opt : Render_Options ) return Boolean;

    -- Updates the light blocking geometry, given a change in the shape of the
    -- map at 'col,row'. Existing walls will be bisected or removed, and new
    -- walls will be added as necessary.
    procedure Update_Geometry( this      : not null access Lighting_System'Class;
                               mapInfo   : A_Map_Info_Cache;
                               tileWidth : Positive;
                               col, row  : Integer );

    -- Freezes/unfreezes event updates to the lighting system from the game. No
    -- event-based state will change while it is frozen. Any events received
    -- while frozen will be ignored. Unfreeze it to resume handling new events.
    procedure Freeze_Updates( this : not null access Lighting_System'Class; freeze : Boolean );

    -- Returns True if the lighting system has been frozen, meaning it is
    -- ignoring lighting system events.
    function Is_Frozen( this : not null access Lighting_System'Class ) return Boolean;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Begins the next frame for the lighting system. 'sceneW,sceneH' is the
    -- geometry size of the Scene widget (not the viewport to its content).
    procedure Frame_Begin( this   : not null access Lighting_System'Class;
                           sceneW,
                           sceneH : Float );

    -- Reconfigures the drawing state for drawing pass 'pass' into the light buffer.
    procedure Pass_Begin( this : not null access Lighting_System'Class; pass : Render_Pass );
    pragma Precondition( pass = Render_Shading or pass = Render_Light );

    -- Draws all point and area light sources within the scene's visible area
    -- 'viewport', in world coordinates. This may only be called during a
    -- Render_Lighting pass.
    procedure Draw_Lights( this : not null access Lighting_System'Class; viewport : Rectangle );

    -- Ends the lighting frame, drawing it to the bitmap that was the target
    -- when the frame started, at 'x,y,z'. 'effectiveZoom' is the transformation
    -- from world units to display backbuffer pixels (or render target pixels,
    -- if rendering to a bitmap).
    procedure Frame_End( this          : not null access Lighting_System'Class;
                         x, y, z       : Float;
                         effectiveZoom : Float );

    procedure Delete( this : in out A_Lighting_System );

private

    type Endpoint is
        record
            x, y  : Float := 0.0;
            angle : Float := 0.0;
            start : Boolean := False;
            wall  : Natural := 0;
        end record;

    function "="( l, r : Endpoint ) return Boolean
    is (l.x = r.x and l.y = r.y and l.angle = r.angle and l.start = r.start);

    type Wall_Type is
        record
            p1, p2 : Endpoint;
        end record;

    package Wall_Vectors is new Ada.Containers.Vectors(Positive, Wall_Type, "=");
    use Wall_Vectors;

    package Endpoint_Vectors is new Ada.Containers.Vectors(Positive, Endpoint, "=");
    use Endpoint_Vectors;

    package Wall_Trees is new Quad_Trees( Integer );

    ----------------------------------------------------------------------------

    type Light_Type is (Point, Area);

    type Light_Source is abstract new Object with
        record
            typ         : Light_Type;
            system      : A_Lighting_System := null;
            componentId : Component_Id := NULL_ID;
            entityId    : Entity_Id := NULL_ID;
            x, y        : Float := 0.0;
            w, h        : Float := 0.0;
            z           : Float := 0.0;
            shadows     : Boolean := True;
            color       : Allegro_Color;          -- light color
            diffuse     : Float := 0.0;           -- diffuse intensity (unshadowed)
            angle       : Float := 0.0;
            arc         : Float := 180.0;
        end record;
    type A_Light_Source is access all Light_Source'Class;

    -- Draws the light source additively to the light accumulation buffer.
    not overriding
    procedure Draw( this : in out Light_Source; viewport : Rectangle ) is null;

    -- Draws debugging information for the individual light.
    not overriding
    procedure Draw_Debug( this : in out Light_Source ) is null;

    -- Returns True if the area affected by the light intersects the area of
    -- (x1,y1) -> (x2,y2). This abstract procedure may be overridden by
    -- subclasses.
    not overriding
    function Intersects( this : Light_Source; rect : Rectangle ) return Boolean;

    procedure Delete( this : in out A_Light_Source );
    pragma Postcondition( this = null );

    package Light_Vectors is new Ada.Containers.Vectors(Positive, A_Light_Source, "=");
    use Light_Vectors;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Point_Light is new Light_Source with
        record
            vertices : A_Allegro_Vertex_Array := null;
            indices  : A_Vertex_Index_Array := null;
        end record;

    -- Computes the light's geometry as an indexed triangle list of color
    -- 'color', filling .vertices and .indices. Returns the total number of
    -- triangles in the list (minimum is 4).
    --
    -- .vertices will contain ('Result+1) elements.
    --
    -- .indices will contain ('Result/2*3) elements.
    function Compute_Triangle_List( this  : in out Point_Light;
                                    color : Allegro_Color ) return Natural;

    procedure Delete( this : in out Point_Light );

    procedure Draw( this : in out Point_Light; viewport : Rectangle );

    procedure Draw_Debug( this : in out Point_Light );

    -- Finds the geometry affecting the light, given 'walls' as the map's entire
    -- set of walls.
    procedure Find_Geometry( this     : Point_Light'Class;
                             geometry : in out Wall_Vectors.Vector );

    -- Calculates the angle of each wall endpoint relative to the light and
    -- sorts them into vector 'endpoints'.
    procedure Sort_Endpoints( this      : Point_Light'Class;
                              walls     : in out Wall_Vectors.Vector;
                              endpoints : in out Endpoint_Vectors.Vector );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Area_Light is new Light_Source with null record;

    procedure Draw( this : in out Area_Light; viewport : Rectangle );

    procedure Draw_Debug( this : in out Area_Light );

    ----------------------------------------------------------------------------

    BLUR_RADIUS : constant Float := 1.0;   -- Gaussian blur radius

    type Lighting_System is new Limited_Object
                            and Process
                            and Event_Listener with
        record
            view     : access Game_Views.Game_View'Class;
            frozen   : Boolean := False;
            walls    : Wall_Trees.Tree;
            lights   : Light_Vectors.Vector;
            lightAcc : A_Allegro_Bitmap := null;
            litArea  : A_Allegro_Bitmap := null;
            ambient  : Allegro_Color;
            w, h     : Float := 0.0;   -- size of the lit area in scene widget coordinates
            zoom     : Float := 1.0;   -- scene zoom factor
            options  : Render_Options := 0;
            alState  : Allegro_State;
        end record;

    procedure Construct( this : access Lighting_System;
                         view : not null access Game_Views.Game_View'Class );

    procedure Delete( this : in out Lighting_System );

    overriding
    function Get_Process_Name( this : access Lighting_System ) return String is (A_Lighting_System(this).To_String);

    -- Handles an event received from the corral that the system is registered
    -- with. This is the same corral provided by the Game View system's
    -- Game_View object. If 'evt' is returned null then the event was consumed.
    procedure Handle_Event( this : access Lighting_System;
                            evt  : in out A_Event;
                            resp : out Response_Type );

    -- Enlarges the light accumulation buffer as necessary.
    procedure Set_Buffer_Size( this : not null access Lighting_System'Class;
                               w, h : Float );

    procedure Tick( this : access Lighting_System; time : Tick_Time );

end Lighting_Systems;
