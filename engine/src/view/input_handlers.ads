--
-- Copyright (c) 2012-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Mutexes;                           use Mutexes;
with Objects;                           use Objects;
with Support;                           use Support;

private with Ada.Unchecked_Deallocation;

package Input_Handlers is

    -- An Input_Handler object runs an internal task to listen from mouse and
    -- keyboard input from the OS and queue input events. Input_Handler objects
    -- may not be copied. Only one instance per application is necessary.
    type Input_Handler is new Limited_Object with private;
    type A_Input_Handler is access all Input_Handler'Class;

    -- Creates a new Input_Handler to receive mouse and keyboard input from the
    -- OS for the given display, generating appropriate events.
    function Create_Input_Handler( display      : not null A_Allegro_Display;
                                   mouseEnabled : Boolean ) return A_Input_Handler;
    pragma Postcondition( Create_Input_Handler'Result /= null );

    -- Returns True if 'key' is currently pressed down.
    function Is_Key_Down( this : not null access Input_Handler'Class;
                          key  : Integer ) return Boolean;

    -- Starts the Input_Handler's internal task. Input events will begin
    -- queueing after this is called. This can only be called once during the
    -- life of the object.
    procedure Start( this : not null access Input_Handler'Class );

    -- Stops the Input_Handler's task. It is called automatically during
    -- deletion.
    procedure Stop( this : not null access Input_Handler'Class );

    -- Deletes the Input_Handler. It will be stopped if it is currently running.
    procedure Delete( this : in out A_Input_Handler );
    pragma Postcondition( this = null );

private

    -- The life cycle of Input_Task is the following:
    -- "Init, Start, Stop" or "Init, Stop"
    task type Input_Task is

        -- Input_Task must be initialized after creation and before stopping.
        entry Init( this : A_Input_Handler );

        -- Starts polling hardware and sending events.
        entry Start;

        -- Stops operation and ends the task.
        entry Stop;

    end Input_Task;
    type A_Input_Task is access all Input_Task;

    -- Deletes the Input_Task.
    procedure Delete is new Ada.Unchecked_Deallocation( Input_Task, A_Input_Task );

    ----------------------------------------------------------------------------

    type Input_Handler is new Limited_Object with
        record
            display      : A_Allegro_Display := null;
            mouseEnabled : Boolean := False;
            process      : A_Input_Task := null;
            started      : Boolean := False;
            stopped      : Boolean := False;

            keyLock      : Mutex;
            keyStates    : Boolean_Array(1..ALLEGRO_KEY_MAX-1) := (others => False);
        end record;

    procedure Construct( this         : access Input_Handler;
                         display      : A_Allegro_Display;
                         mouseEnabled : Boolean );

    procedure Delete( this : in out Input_Handler );

end Input_Handlers;
