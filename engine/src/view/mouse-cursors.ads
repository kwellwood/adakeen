--
-- Copyright (c) 2013 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Mouse.Cursors;             use Allegro.Mouse.Cursors;
with Objects;                           use Objects;

package Mouse.Cursors is

    type Mouse_Cursor is abstract new Limited_Object with private;
    type A_Mouse_Cursor is access all Mouse_Cursor'Class;

    -- Creates a new mouse cursor from a custom bitmap. 'cursor' will be
    -- consumed even though the 'cursor' reference is not set to null.
    function Create_Mouse_Cursor( cursor : not null A_Allegro_Mouse_Cursor ) return A_Mouse_Cursor;

    -- Creates a new mouse cursor from an Allegro system cursor.
    function Create_Mouse_Cursor( cursor : Allegro_System_Mouse_Cursor ) return A_Mouse_Cursor;

    -- Sets this cursor as the active cursor for 'display'.
    procedure Set_Display_Cursor( this    : access Mouse_Cursor;
                                  display : not null A_Allegro_Display ) is abstract;

    procedure Delete( this : in out A_Mouse_Cursor );

private

    type Mouse_Cursor is abstract new Limited_Object with null record;

    ----------------------------------------------------------------------------

    type Custom_Cursor is new Mouse_Cursor with
        record
            custom : A_Allegro_Mouse_Cursor := null;
        end record;

    procedure Construct( this   : access Custom_Cursor;
                         custom : not null A_Allegro_Mouse_Cursor );

    procedure Delete( this : in out Custom_Cursor );

    procedure Set_Display_Cursor( this    : access Custom_Cursor;
                                  display : not null A_Allegro_Display );

    ----------------------------------------------------------------------------

    type System_Cursor is new Mouse_Cursor with
        record
            sysCursor : Allegro_System_Mouse_Cursor;
        end record;

    procedure Construct( this      : access System_Cursor;
                         sysCursor : Allegro_System_Mouse_Cursor );

    procedure Set_Display_Cursor( this    : access System_Cursor;
                                  display : not null A_Allegro_Display );

end Mouse.Cursors;
