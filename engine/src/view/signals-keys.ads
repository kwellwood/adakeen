--
-- Copyright (c) 2015-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Keyboard;                          use Keyboard;

package Signals.Keys is

    ----------------------------------------------------------------------------
    -- Key_Connection Class --

    type Key_Arguments is
        record
            code      : Integer;           -- key scan code
            char      : Character;         -- readable character (KeyTyped signals only)
            modifiers : Modifiers_Array;   -- active modifiers (KeyTyped signals only)
        end record;

    type Key_Connection is abstract new Base_Connection with
        record
            key       : Integer := KEY_ANY;
            modifiers : Modifiers_Pattern := MODIFIERS_ANY;
        end record;

    not overriding
    procedure Emit( this    : Key_Connection;
                    sender  : access Base_Object'Class;
                    args    : Key_Arguments;
                    handled : out Boolean ) is abstract;

    ----------------------------------------------------------------------------
    -- Key_Signal Class --

    type Key_Signal is new Base_Signal with private;

    procedure Emit( this      : Key_Signal'Class;
                    key       : Integer;
                    char      : Character;
                    modifiers : Modifiers_Array;
                    handled   : out Boolean );

    procedure Connect( this : in out Key_Signal'Class; slot : Key_Connection'Class );

    ----------------------------------------------------------------------------
    -- Signals.Keys.Connections --

    generic
        type Target (<>) is abstract limited new Base_Object with private;
    package Connections is

        type Prototype is (Proto0, Proto2);

        type A_Method0 is access
            procedure( object : not null access Target'Class );

        type A_Method2 is access
            procedure( object : not null access Target'Class;
                       sig    : Key_Arguments );

        ------------------------------------------------------------------------

        function Slot( obj       : in out Target'Class;
                       method    : not null A_Method0;
                       key       : Integer;
                       modifiers : Modifiers_Pattern := MODIFIERS_ANY;
                       priority  : Priority_Type := Normal ) return Key_Connection'Class;

        function Slot( obj       : not null access Target'Class;
                       method    : not null A_Method0;
                       key       : Integer;
                       modifiers : Modifiers_Pattern := MODIFIERS_ANY;
                       priority  : Priority_Type := Normal ) return Key_Connection'Class is (Slot( obj.all, method, key, modifiers, priority ));

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        function Slot( obj       : in out Target'Class;
                       method    : not null A_Method2;
                       key       : Integer;
                       modifiers : Modifiers_Pattern := MODIFIERS_ANY;
                       priority  : Priority_Type := Normal ) return Key_Connection'Class;

        function Slot( obj       : not null access Target'Class;
                       method    : not null A_Method2;
                       key       : Integer;
                       modifiers : Modifiers_Pattern := MODIFIERS_ANY;
                       priority  : Priority_Type := Normal ) return Key_Connection'Class is (Slot( obj.all, method, key, modifiers, priority ));

    private

        type Generic_Key_Connection(proto : Prototype) is new Key_Connection with
            record
                object : access Target'Class := null;
                case proto is
                    when Proto0 => method0 : A_Method0 := null;
                    when Proto2 => method2 : A_Method2 := null;
                end case;
            end record;

        overriding
        procedure Emit( this    : Generic_Key_Connection;
                        sender  : access Base_Object'Class;
                        args    : Key_Arguments;
                        handled : out Boolean );

        overriding
        function Eq( this : Generic_Key_Connection; other : Base_Connection'Class ) return Boolean;

        overriding
        function Get_Object( this : Generic_Key_Connection ) return access Base_Object'Class is (this.object);

    end Connections;

private

    type Key_Signal is new Signal with null record;

end Signals.Keys;
