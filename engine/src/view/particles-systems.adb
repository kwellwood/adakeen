--
-- Copyright (c) 2015-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Numerics;                      use Ada.Numerics;
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Assets.Scribble;                   use Assets.Scribble;
with Debugging;                         use Debugging;
with Drawing;                           use Drawing;
with Drawing.Bitmaps;                   use Drawing.Bitmaps;
with Drawing.Primitives;                use Drawing.Primitives;
with Events.Entities;                   use Events.Entities;
with Events.Entities.Locations;         use Events.Entities.Locations;
with Events.Particles;                  use Events.Particles;
with Game_Views;                        use Game_Views;
with Resources;                         use Resources;
with Support;                           use Support;
with Support.Paths;                     use Support.Paths;
with Values;                            use Values;
with Values.Casting;                    use Values.Casting;
with Values.Errors;                     use Values.Errors;
with Values.Strings;                    use Values.Strings;
with Version;

package body Particles.Systems is

    -- Handles an Entity_Deleted event by deleting all particle emitters
    -- attached to the entity.
    procedure Handle_Entity_Deleted( this : not null access Particle_System'Class;
                                     evt  : not null A_Entity_Event );

    -- Handles an Entity_Moved event by moving all particle emitters attached to
    -- the entity.
    procedure Handle_Entity_Moved( this : not null access Particle_System'Class;
                                   evt  : not null A_Entity_Moved_Event );

    procedure Handle_Particle_Burst( this : not null access Particle_System'Class;
                                     evt  : not null A_Particles_Event );

    procedure Handle_Start_Particles( this : not null access Particle_System'Class;
                                      evt  : not null A_Particles_Event );

    procedure Handle_Stop_Particles( this : not null access Particle_System'Class;
                                     evt  : not null A_Particles_Event );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Particle_System( view     : not null access Game_Views.Game_View'Class;
                                     capacity : Integer ) return A_Particle_System is
        this : constant A_Particle_System := new Particle_System;
    begin
        this.Construct( view, capacity );
        return this;
    end Create_Particle_System;

    ----------------------------------------------------------------------------

    procedure Add_Emitter( this : not null access Particle_System'Class;
                           em   : Emitter ) is
        shortestLife  : Time_Span := Time_Span_Last;
        shortestIndex : Integer := 0;
    begin
        -- 1. check if the emitter should replace one with the same component id
        if em.componentId /= NULL_ID then
            for i in 1..this.eCount loop
                if this.emitters(i).componentId /= NULL_ID then
                    if this.emitters(i).componentId = em.componentId then
                        -- replace an emitter with matching component id
                        this.emitters(i) := em;
                        return;
                    end if;
                elsif this.emitters(i).life < shortestLife then
                    shortestIndex := i;
                    shortestLife := this.emitters(i).life;
                end if;
            end loop;
        end if;

        -- 2. check if there's room in the emitters array
        if this.eCount < this.emitters'Length then
            this.eCount := this.eCount + 1;
            this.emitters(this.eCount) := em;
            return;
        end if;

        -- 3. find the shortest fixed lifespan emitter for replacement. if we
        -- didn't already find the shortest one, find it now.
        if em.componentId = NULL_ID then
            for i in 1..this.eCount loop
                if this.emitters(i).componentId = NULL_ID and then
                   this.emitters(i).life < shortestLife
                then
                    shortestIndex := i;
                    shortestLife := this.emitters(i).life;
                end if;
            end loop;
        end if;
        if shortestIndex > 0 then
            this.emitters(shortestIndex) := em;
            return;
        end if;

        -- no place to put the emitter!
    end Add_Emitter;

    ----------------------------------------------------------------------------

    function Add_Layer( this  : not null access Particle_System'Class;
                        z     : Float ) return Boolean is
    begin
        -- iterate from background to foreground
        for i in reverse this.layers'Range loop
            if this.layers(i) = null then
                -- found an empty slot for a foreground layer
                this.layers(i) := new Layer_Type(INITIAL_CAPACITY);
                this.layers(i).z := z;
                return True;
            elsif this.layers(i).z = z then
                -- found a layer with matching z depth
                return True;
            elsif this.layers(i).z < z then
                -- insert new layer before 'i'
                -- check if there is room (layer 'First will be filled last)
                if this.layers(this.layers'First) = null then
                    this.layers(this.layers'First..i-1) := this.layers(this.layers'First+1..i);
                    this.layers(i) := new Layer_Type(INITIAL_CAPACITY);
                    this.layers(i).z := z;
                    return True;
                else
                    return False;          -- no room
                end if;
            else
                null;                      -- keep looking
            end if;
        end loop;
        return False;
    end Add_Layer;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this     : access Particle_System;
                         view     : not null access Game_Views.Game_View'Class;
                         capacity : Integer ) is
    begin
        Limited_Object(this.all).Construct;
        this.view := view;
        this.capacity := capacity;
        Reset( this.gen );
        this.reloadDefs := Version.Is_Debug;

        this.view.Get_Corral.Add_Listener( this, EVT_ENTITY_DELETED );
        this.view.Get_Corral.Add_Listener( this, EVT_ENTITY_MOVED );
        this.view.Get_Corral.Add_Listener( this, EVT_PARTICLE_BURST );
        this.view.Get_Corral.Add_Listener( this, EVT_START_PARTICLES );
        this.view.Get_Corral.Add_Listener( this, EVT_STOP_PARTICLES );

        this.Load_Particles;
    end Construct;

    ----------------------------------------------------------------------------

    procedure Clear( this : not null access Particle_System'Class ) is
    begin
        this.eCount := 0;

        for i in this.layers'Range loop
            Free( this.layers(i) );
        end loop;
    end Clear;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Particle_System ) is
    begin
        this.view.Get_Corral.Remove_Listener( this'Unchecked_Access );

        this.Clear;
        Limited_Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Draw_Layer( this    : not null access Particle_System'Class;
                          z       : Float;
                          zOffset : Float;
                          bounds  : Rectangle;
                          pass    : Render_Pass ) is
    begin
        -- iterate from background to foreground
        for i in reverse this.layers'Range loop
            if this.layers(i) = null then
                exit;
            elsif this.layers(i).z = z then
                this.Draw_Layer( this.layers(i), bounds, zOffset, pass );
                exit;
            elsif this.layers(i).z < z then
                exit;        -- didn't find a matching Z layer
            end if;
        end loop;
    end Draw_Layer;

    ----------------------------------------------------------------------------

    procedure Draw_Layer( this    : not null access Particle_System'Class;
                          layer   : A_Layer;
                          bounds  : Rectangle;
                          zOffset : Float;
                          pass    : Render_Pass ) is
        pragma Unreferenced( this );
        halfSize  : Float;
        px, py    : Float;
        bmp       : A_Allegro_Bitmap;
        color     : Allegro_Color;
        blendMode : Blend_Mode;
    begin
        for i in 1..layer.pCount loop
            halfSize := layer.particles(i).size / 2.0;
            px := layer.particles(i).x - halfSize;
            py := layer.particles(i).y - halfSize;
            if Intersect( bounds, Rectangle'(px, py, layer.particles(i).size, layer.particles(i).size) ) then

                bmp := null;
                blendMode := Current;

                case pass is
                    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                    when Render_Color   |
                         Render_Shading =>

                        if pass = Render_Color and layer.particles(i).additive then
                            blendMode := Additive;
                        end if;
                        color := layer.particles(i).color;
                        bmp := layer.particles(i).texture.Get_Bitmap;

                    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                    when Render_Light =>

                        -- its brightness is controlled by the tint opacity
                        color := Make_Grey( layer.particles(i).light, 1.0 );
                        if layer.particles(i).light > 0.0 then
                            bmp := layer.particles(i).texture.Get_Mask;
                        end if;

                end case;

                if bmp /= null then
                    To_Target_Pixel_Rounded( px, py );
                    Draw_Bitmap_Stretched( bmp,
                                           px, py,
                                           layer.z + zOffset,
                                           layer.particles(i).size, layer.particles(i).size,
                                           Fit,
                                           layer.particles(i).rot,
                                           color,
                                           blendMode );
                end if;

            end if;
        end loop;
    end Draw_Layer;

    ----------------------------------------------------------------------------

    procedure Freeze_Updates( this : not null access Particle_System'Class; freeze : Boolean ) is
    begin
        this.frozen := freeze;
    end Freeze_Updates;

    ----------------------------------------------------------------------------

    procedure Get_Emitter_Definition( this  : not null access Particle_System'Class;
                                      name  : String;
                                      def   : out Emitter;
                                      found : out Boolean ) is
        use Definition_Maps;
        pos : Definition_Maps.Cursor;
    begin
        pos := this.definitions.Find( name );
        if Has_Element( pos ) then
            if this.reloadDefs then
                -- reload the particle definition for 'name'
                -- (and any others in the same file)
                if this.Load_Particles( To_String( Element( pos ).filePath ) ) then
                    pos := this.definitions.Find( name );
                    pragma Assert( Has_Element( pos ) );
                end if;
            end if;
            def := Element( pos ).def;                  -- copy the definition
            found := True;
        else
            found := False;
            Dbg( "Particle emitter definition '" & name & "' not found",
                 D_PARTICLES, Warning );
        end if;
    end Get_Emitter_Definition;

    ----------------------------------------------------------------------------

    function Get_Emitter_Names( this : not null access Particle_System'Class ) return List_Value is
        names : constant List_Value := Create_List.Lst;
        use Definition_Maps;
        pos   : Definition_Maps.Cursor;
    begin
        if this.reloadDefs then
            -- reload all particles, in case new particles become available
            this.Load_Particles;
        end if;

        pos := this.definitions.First;
        while Has_Element( pos ) loop
            names.Append( Create( Key( pos ) ) );
            Next( pos );
        end loop;
        return names;
    end Get_Emitter_Names;

    ----------------------------------------------------------------------------

    procedure Handle_Entity_Deleted( this : not null access Particle_System'Class;
                                     evt  : not null A_Entity_Event ) is
        eid : constant Entity_Id := evt.Get_Id;
        i   : Integer;
    begin
        i := this.emitters'First;
        while i < this.eCount loop
            if this.emitters(i).entityId = eid then
                this.emitters(i) := this.emitters(this.eCount);
                this.eCount := this.eCount - 1;
            else
                i := i + 1;
            end if;
        end loop;
        if i = this.eCount and then this.emitters(this.eCount).entityId = eid then
            this.eCount := this.eCount - 1;
        end if;
    end Handle_Entity_Deleted;

    ----------------------------------------------------------------------------

    procedure Handle_Entity_Moved( this : not null access Particle_System'Class;
                                   evt  : not null A_Entity_Moved_Event ) is
        eid : constant Entity_Id := evt.Get_Id;
    begin
        for i in 1..this.eCount loop
            if this.emitters(i).entityId = eid then
                this.emitters(i).prototype.x := this.emitters(i).prototype.x + (evt.Get_X - evt.Get_Prev_X);
                this.emitters(i).prototype.y := this.emitters(i).prototype.y + (evt.Get_Y - evt.Get_Prev_Y);
            end if;
        end loop;
    end Handle_Entity_Moved;

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Event( this : access Particle_System;
                            evt  : in out A_Event;
                            resp : out Response_Type ) is
        pragma Unreferenced( resp );
    begin
        if this.frozen then
            return;
        end if;
        case evt.Get_Event_Id is
            when EVT_ENTITY_MOVED    => this.Handle_Entity_Moved( A_Entity_Moved_Event(evt) );
            when EVT_ENTITY_DELETED  => this.Handle_Entity_Deleted( A_Entity_Event(evt) );
            when EVT_PARTICLE_BURST  => this.Handle_Particle_Burst( A_Particles_Event(evt) );
            when EVT_START_PARTICLES => this.Handle_Start_Particles( A_Particles_Event(evt) );
            when EVT_STOP_PARTICLES  => this.Handle_Stop_Particles( A_Particles_Event(evt) );
            when others              => null;
        end case;
    end Handle_Event;

    ----------------------------------------------------------------------------

    procedure Handle_Particle_Burst( this : not null access Particle_System'Class;
                                     evt  : not null A_Particles_Event ) is
        em    : Emitter;
        name  : String_Value;
        size  : Integer := 0;
        found : Boolean := False;
    begin
        -- look for a particle emitter definition name
        name := evt.Get_Emitter.Get( "name" ).Str;
        if name.Valid then
            size := evt.Get_Emitter.Size;
            this.Get_Emitter_Definition( name.To_String, em, found );
            if not found then
                Dbg( "Particle_Burst: Particle emitter definition '" &
                     name.To_String & "' not found",
                     D_PARTICLES, Warning );
                return;
            end if;
        end if;

        -- set/override the emitter's properties from the event
        -- don't bother if "name" was the only key in the event's emitter map
        if not name.Valid or else size > 1 then
            this.Set_Emitter_Properties( em, evt.Get_Emitter );
        end if;

        em.entityId := evt.Get_Entity_Id;
        em.z := evt.Get_Z;
        em.rate := evt.Get_Rate;
        em.life := evt.Get_Lifetime;
        em.prototype.x := evt.Get_X;
        em.prototype.y := evt.Get_Y;

        if this.Add_Layer( em.z ) then
            if em.life = Time_Span_Zero then
                this.Spawn_Particles( em, em.rate );
            else
                this.Add_Emitter( em );
            end if;
        end if;
    end Handle_Particle_Burst;

    ----------------------------------------------------------------------------

    procedure Handle_Start_Particles( this : not null access Particle_System'Class;
                                      evt  : not null A_Particles_Event ) is
        em   : Emitter;
        name : String_Value;
        size : Integer := 0;
        found : Boolean := False;
    begin
        -- look for a particle emitter definition name
        name := evt.Get_Emitter.Get( "name" ).Str;
        if name.Valid then
            size := evt.Get_Emitter.Size;
            this.Get_Emitter_Definition( name.To_String, em, found );
            if not found then
                Dbg( "Start_Particles: Particle emitter definition '" &
                     name.To_String & "' not found",
                     D_PARTICLES, Warning );
                return;
            end if;
        end if;

        -- set/override the emitter's properties from the event
        -- don't bother if "name" was the only key in the event's emitter map
        if not name.Valid or else size > 1 then
            this.Set_Emitter_Properties( em, evt.Get_Emitter );
        end if;

        em.componentId := evt.Get_Component_Id;
        em.entityId := evt.Get_Entity_Id;
        em.z := evt.Get_Z;
        em.rate := evt.Get_Rate;
        em.life := Time_Span_Zero;
        em.prototype.x := evt.Get_X;
        em.prototype.y := evt.Get_Y;

        if this.Add_Layer( em.z ) then
            this.Add_Emitter( em );
        end if;
    end Handle_Start_Particles;

    ----------------------------------------------------------------------------

    procedure Handle_Stop_Particles( this : not null access Particle_System'Class;
                                     evt  : not null A_Particles_Event ) is
    begin
        for i in 1..this.eCount loop
            if this.emitters(i).componentId = evt.Get_Component_Id then
                -- move the last emitter down to replace the dead one
                this.emitters(i) := this.emitters(this.eCount);
                this.eCount := this.eCount - 1;
                return;
            end if;
        end loop;
    end Handle_Stop_Particles;

    ----------------------------------------------------------------------------

    function Is_Frozen( this : not null access Particle_System'Class ) return Boolean is (this.frozen);

    ----------------------------------------------------------------------------

    function Load_Particles( this     : not null access Particle_System'Class;
                             filePath : String ) return Boolean is
        file  : A_Scribble_Asset;
        defs  : List_Value;
        name  : String_Value;
        props : Map_Value;
        em    : Emitter;
    begin
        file := Load_Value( filePath, "definitions" );
        if not file.Is_Loaded then
            Dbg( "Failed to open " & filePath, D_PARTICLES, Error );
            Delete( file );
            return False;
        elsif not file.Get_Value.Is_List and not file.Get_Value.Is_Map then
            Dbg( "Unexpected contents in " & filePath, D_PARTICLES, Error );
            Delete( file );
            return False;
        end if;

        defs := file.Get_Value.Lst;
        if defs.Is_Map then
            defs := Create_List.Lst;
            defs.Append( file.Get_Value, consume => True );   -- not a copy
        end if;

        for i in 1..defs.Length loop
            if defs.Get( i ).Is_Map then
                name := defs.Get( i ).Map.Get( "Emitter" ).Str;
                if name.Valid then
                    props := defs.Get( i ).Map.Get( "Properties" ).Map;
                    if props.Valid then
                        em := Emitter'(others => <>);
                        this.Set_Emitter_Properties( em, props );
                        if em.prototype.texture.Get_Bitmap /= null then
                            if em.prototype.lifespan > Time_Span_Zero or else
                                em.lifespanVar > Time_Span_Zero
                            then
                                pragma Debug( Dbg( "Loaded particle definition '" &
                                                   name.To_String & " from " & filePath,
                                                   D_PARTICLES, Info ) );
                                this.definitions.Include( name.To_String,
                                                          Emitter_Info'(filePath => To_Unbounded_String( filePath ),
                                                                        def      => em) );
                            else
                                Dbg( "Zero lifespan for particles of emitter '" &
                                     name.To_String & "' in " & filePath,
                                     D_PARTICLES, Warning );
                            end if;
                        else
                            Dbg( "Failed to load texture for emitter '" &
                                 name.To_String & "' in " & filePath,
                                 D_PARTICLES, Warning );
                        end if;
                    else
                        Dbg( "Missing properties of emitter '" &
                             name.To_String & "' in " & filePath,
                             D_PARTICLES, Warning );
                    end if;
                else
                    Dbg( "Unrecognized element #" &
                         Image( i ) & " in " & filePath,
                         D_PARTICLES, Warning );
                end if;
            else
                Dbg( "Unrecognized element #" & Image( i ) & " in " & filePath,
                     D_PARTICLES, Warning );
            end if;
        end loop;

        Delete( file );
        return True;
    end Load_Particles;

    ----------------------------------------------------------------------------

    procedure Load_Particles( this : not null access Particle_System'Class ) is
        files    : List_Value;
        compiled : List_Value;
        filename : Value;
    begin
        -- find all particle definitions in source format
        files := Search_Resources( "*.particle", "definitions" );

        -- find particle definitions that have been compiled
        -- add them to the list of files to load, without the ".sco" extension
        compiled := Search_Resources( "*.particle.sco", "definitions" );
        for i in 1..compiled.Length loop
            filename := Create( Remove_Extension( Cast_String( compiled.Get( i ) ) ) );
            if not files.Contains( filename ) then
                files.Append( filename );
            end if;
        end loop;

        -- load each file found, using only the ".particle" filename, whether or
        -- not it's actually compiled
        for i in 1..files.Length loop
            if not this.Load_Particles( Cast_String( files.Get( i ) ) ) then
                Dbg( "Failed to load particles from '" & Cast_String( files.Get( i ) ) & "'",
                     D_ENTITY, Error );
            end if;
        end loop;
    end Load_Particles;

    ----------------------------------------------------------------------------

    procedure Reserve_Capacity( this  : not null access Particle_System'Class;
                                layer : in out A_Layer;
                                count : Integer ) is
        pragma Unreferenced( this );
        newLayer : A_Layer;
        len      : Integer;
    begin
        if layer.pCount + count > layer.particles'Length and then
           layer.particles'Length < MAX_CAPACITY
        then
            len := layer.particles'Length;
            while len < layer.pCount + count and then len <= MAX_CAPACITY loop
                len := len * 2;
            end loop;
            newLayer := new Layer_Type(len);
            newLayer.particles(1..layer.pCount) := layer.particles(1..layer.pCount);
            newLayer.pCount := layer.pCount;
            newLayer.z := layer.z;
            Free( layer );
            layer := newLayer;
        end if;
    end Reserve_Capacity;

    ----------------------------------------------------------------------------

    procedure Set_Emitter_Properties( this  : not null access Particle_System'Class;
                                      em    : in out Emitter;
                                      props : Map_Value ) is
        pragma Unreferenced( this );
        texture : String_Value;
        temp    : Value;
    begin
        -- particle properties --
        texture := props.Get( "texture" ).Str;
        if texture.Valid then
            em.prototype.texture := Create_Icon( texture.To_String );
        end if;

        em.prototype.lifespan := Milliseconds( props.Get_Int( "lifespan", em.prototype.lifespan / Milliseconds( 1 ) ) );
        em.angle              := props.Get_Float( "angle",    em.angle * 180.0 / Pi ) / 180.0 * Pi;
        em.speed              := props.Get_Float( "speed",    em.speed );
        em.prototype.gravity  := props.Get_Float( "gravity",  em.prototype.gravity );
        em.prototype.radial   := props.Get_Float( "radial",   em.prototype.radial );
        em.prototype.tangent  := props.Get_Float( "tangent",  em.prototype.tangent );
        em.prototype.vTangent := props.Get_Float( "vTangent", em.prototype.vTangent );
        em.prototype.rot      := props.Get_Float( "rot",      em.prototype.rot * 180.0 / Pi ) / 180.0 * Pi;
        em.prototype.vRot     := props.Get_Float( "vRot",     em.prototype.vRot * 180.0 / Pi ) / 180.0 * Pi;

        em.prototype.size1    := props.Get_Float( "size1", em.prototype.size1 );
        if em.prototype.size1 <= 0.0 and then em.prototype.texture.Get_Bitmap /= null then
            -- use the texture size as the particle size
            em.prototype.size1 := Float(Integer'Max( em.prototype.texture.Get_Width, em.prototype.texture.Get_Height ));
        end if;
        em.prototype.size1    := Float'Max( 1.0, em.prototype.size1 );
        em.prototype.size2    := props.Get_Float( "size2", em.prototype.size2 );

        temp := props.Get( "color1" );
        if temp.Is_String then
            em.prototype.color1 := Html_To_Color( temp.Str.To_String, em.prototype.color1 );
        end if;
        temp := props.Get( "color2" );
        if temp.Is_String then
            em.prototype.color2 := Html_To_Color( temp.Str.To_String, em.prototype.color2 );
        end if;

        em.prototype.light1 := props.Get_Float( "light1", em.prototype.light1 );
        em.prototype.light2 := props.Get_Float( "light2", em.prototype.light2 );

        em.prototype.additive := props.Get_Boolean( "additive", em.prototype.additive );

        -- variation limits --
        em.lifespanVar := Milliseconds( props.Get_Int( "lifespanVar", em.lifespanVar / Milliseconds( 1 ) ) );
        em.radius      := props.Get_Float( "radius",      em.radius );
        em.radiusVar   := props.Get_Float( "radiusVar",   em.radiusVar );
        em.xVar        := props.Get_Float( "xVar",        em.xVar );
        em.yVar        := props.Get_Float( "yVar",        em.yVar );
        em.angleVar    := props.Get_Float( "angleVar",    em.angleVar * 180.0 / Pi ) / 180.0 * Pi;
        em.speedVar    := props.Get_Float( "speedVar",    em.speedVar );
        em.radialVar   := props.Get_Float( "radialVar",   em.radialVar );
        em.tangentVar  := props.Get_Float( "tangentVar",  em.tangentVar );
        em.vTangentVar := props.Get_Float( "vTangentVar", em.vTangentVar );
        em.rotVar      := props.Get_Float( "rotVar",      em.rotVar * 180.0 / Pi ) / 180.0 * Pi;
        em.vRotVar     := props.Get_Float( "vRotVar",     em.vRotVar * 180.0 / Pi ) / 180.0 * Pi;
        em.size1Var    := props.Get_Float( "size1Var",    em.size1Var );
        em.size2Var    := props.Get_Float( "size2Var",    em.size2Var );
        em.color1Var   := Html_To_Color( props.Get_String( "color1Var" ), em.color1Var );
        em.color2Var   := Html_To_Color( props.Get_String( "color2Var" ), em.color2Var );
        em.light1Var   := props.Get_Float( "light1Var",   em.light1Var );
        em.light2Var   := props.Get_Float( "light2Var",   em.light2Var );
    end Set_Emitter_Properties;

    ----------------------------------------------------------------------------

    procedure Spawn_Particles( this  : not null access Particle_System'Class;
                               em    : Emitter;
                               count : Integer ) is

        ------------------------------------------------------------------------

        -- Apply a variation to 'origin' such that origin = origin +/- var/2
        procedure Vary_Float( origin : in out Float; var : Float ) is
        begin
            if var > 0.0 then
                origin := origin + (var * Float'(Random( this.gen )) - var / 2.0);
            end if;
        end Vary_Float;
        pragma Inline_Always( Vary_Float );

        ------------------------------------------------------------------------

        procedure Vary_Time( origin : in out Time_Span; var : Time_Span ) is
        begin
            if var > Time_Span_Zero then
                origin := origin + To_Time_Span( To_Duration( var ) * Duration(Float'(Random( this.gen ))) - To_Duration(var) / 2.0 );
            end if;
        end Vary_Time;

        ------------------------------------------------------------------------

        -- Apply a color variation to 'origin' such that each of its RGBA
        -- components are varied by the corresponding components of 'var'.
        procedure Vary_Color( origin : in out Allegro_Color; var : Allegro_Color ) is
            r, g, b, a : Float;
            vr, vg, vb, va : Float;
        begin
            Al_Unmap_RGBA_f( origin, r, g, b, a );
            Al_Unmap_RGBA_f( var, vr, vg, vb, va );
            Vary_Float( r, vr );
            Vary_Float( g, vg );
            Vary_Float( b, vb );
            Vary_Float( a, va );
            r := Constrain( r, 0.0, 1.0 );
            g := Constrain( g, 0.0, 1.0 );
            b := Constrain( b, 0.0, 1.0 );
            a := Constrain( a, 0.0, 1.0 );
            origin := Al_Map_RGBA_f( r, g, b, a );
        end Vary_Color;

        ------------------------------------------------------------------------

        layer  : A_Layer := null;
        angle  : Float;
        speed  : Float;
        radius : Float;
    begin
        for i in reverse this.layers'Range loop
            if this.layers(i).z = em.z then
                this.Reserve_Capacity( this.layers(i), count );
                layer := this.layers(i);
                exit;
            end if;
        end loop;
        pragma Assert( layer /= null );

        for i in 1..count loop
            exit when layer.pCount = layer.particles'Length;

            layer.pCount := layer.pCount + 1;
            layer.particles(layer.pCount) := em.prototype;

            -- apply variations to all the initial values from the prototype
            Vary_Time(  layer.particles(layer.pCount).lifespan, em.lifespanVar );
            Vary_Float( layer.particles(layer.pCount).x,        em.xVar );
            Vary_Float( layer.particles(layer.pCount).y,        em.yVar );
            Vary_Float( layer.particles(layer.pCount).radial,   em.radialVar );
            Vary_Float( layer.particles(layer.pCount).tangent,  em.tangentVar );
            Vary_Float( layer.particles(layer.pCount).vTangent, em.vTangentVar );
            Vary_Float( layer.particles(layer.pCount).rot,      em.rotVar );
            Vary_Float( layer.particles(layer.pCount).vRot,     em.vRotVar );

            Vary_Float( layer.particles(layer.pCount).size1, em.size1Var );
            layer.particles(layer.pCount).size1 := Float'Max( 1.0, layer.particles(layer.pCount).size1 );
            if em.prototype.size2 = 0.0 then
                -- if the emitter doesn't have an explicit size2, use size1
                -- after is has been varied to this specific particle, which
                -- will produce no size change over the life of the particle
                -- (unless size2Var _has_ been specified).
                layer.particles(layer.pCount).size2 := layer.particles(layer.pCount).size1;
            end if;
            Vary_Float( layer.particles(layer.pCount).size2, em.size2Var );
            layer.particles(layer.pCount).size2 := Float'Max( 1.0, layer.particles(layer.pCount).size2 );

            if em.color1Var /= Transparent then
                Vary_Color( layer.particles(layer.pCount).color1, em.color1Var );
            end if;
            if em.prototype.color2 = No_Color then
                -- if the emitter doesn't have an explicit color2, use color1
                -- after it has been varied to this specific particle, which
                -- will produce no color change over the life of the particle
                -- (unless color2Var _has_ been specified).
                layer.particles(layer.pCount).color2 := layer.particles(layer.pCount).color1;
            end if;
            if em.color2Var /= Transparent then
                Vary_Color( layer.particles(layer.pCount).color2, em.color2Var );
            end if;

            Vary_Float( layer.particles(layer.pCount).light1, em.light1Var );
            layer.particles(layer.pCount).light1 := Float'Min( Float'Max( 0.0, layer.particles(layer.pCount).light1 ), 1.0 );
            if layer.particles(layer.pCount).light2 < 0.0 then
                -- if the emitter doesn't have an explicit light2, use light1
                -- after is has been varied to this specific particle, which
                -- will produce no light change over the life of the particle
                -- (unless light2Var _has_ been specified).
                layer.particles(layer.pCount).light2 := layer.particles(layer.pCount).light1;
            end if;
            Vary_Float( layer.particles(layer.pCount).light2, em.light2Var );
            layer.particles(layer.pCount).light2 := Float'Min( Float'Max( 0.0, layer.particles(layer.pCount).light2 ), 1.0 );

            -- set size, color and brightness to their initial states
            layer.particles(layer.pCount).size := layer.particles(layer.pCount).size1;
            layer.particles(layer.pCount).color := layer.particles(layer.pCount).color1;
            layer.particles(layer.pCount).light := layer.particles(layer.pCount).light1;

            -- calculate the velocity components from initial angle and speed
            angle := em.angle;
            speed := em.speed;
            Vary_Float( angle, em.angleVar );
            Vary_Float( speed, em.speedVar );
            layer.particles(layer.pCount).vx :=  Sin( angle ) * speed;
            layer.particles(layer.pCount).vy := -Cos( angle ) * speed;

            -- calculate distance from emitter if spawn radius is used
            if em.radius /= 0.0 or else em.radiusVar /= 0.0 then
                radius := em.radius;
                Vary_Float( radius, em.radiusVar );
                layer.particles(layer.pCount).x := layer.particles(layer.pCount).x + Sin( angle ) * radius;
                layer.particles(layer.pCount).y := layer.particles(layer.pCount).y - Cos( angle ) * radius;
            end if;

            -- record the emitter's position for radial & tangential motion
            layer.particles(layer.pCount).cx := em.prototype.x;
            layer.particles(layer.pCount).cy := em.prototype.y;
        end loop;
    end Spawn_Particles;

    ----------------------------------------------------------------------------

    procedure Tick( this : access Particle_System; time : Tick_Time ) is
        dt         : constant Float := Float(To_Duration( time.elapsed ));
        dt2        : Time_Span;
        layer      : A_Layer;
        i          : Integer;
        progress   : Float;
        radX, radY : Float;
        tanX, tanY : Float;
    begin
        if this.frozen then
            return;
        end if;

        -- update particles --
        for l in reverse this.layers'Range loop
            exit when this.layers(l) = null;

            layer := this.layers(l);
            i := 1;
            while i <= layer.pCount loop
                layer.particles(i).age := layer.particles(i).age + time.elapsed;
                if layer.particles(i).age <= layer.particles(i).lifespan then
                    progress := Float(To_Duration( layer.particles(i).age )) / Float(To_Duration( layer.particles(i).lifespan ));

                    -- acceleration
                    radX := 0.0;
                    radY := 0.0;
                    tanX := 0.0;
                    tanY := 0.0;
                    if (layer.particles(i).radial /= 0.0 or else layer.particles(i).tangent /= 0.0) and then
                       (layer.particles(i).x /= layer.particles(i).cx or else layer.particles(i).y /= layer.particles(i).cy)
                    then
                        -- don't apply radial acceleration when particle is on top of the emitter
                        radX := layer.particles(i).x - layer.particles(i).cx;
                        radY := layer.particles(i).y - layer.particles(i).cy;
                        Normalize( radX, radY );
                        tanX := -radY;
                        tanY := radX;
                        radX := radX * layer.particles(i).radial;
                        radY := radY * layer.particles(i).radial;
                        tanX := tanX * layer.particles(i).tangent;
                        tanY := tanY * layer.particles(i).tangent;
                    end if;
                    layer.particles(i).vx := layer.particles(i).vx + (radX + tanX) * dt;
                    layer.particles(i).vy := layer.particles(i).vy + (radY + tanY + layer.particles(i).gravity) * dt;

                    -- velocity
                    tanX := 0.0;
                    tanY := 0.0;
                    if layer.particles(i).vTangent /= 0.0 and then
                        (layer.particles(i).x /= layer.particles(i).cx or else layer.particles(i).y /= layer.particles(i).cy)
                    then
                        radX := layer.particles(i).x - layer.particles(i).cx;
                        radY := layer.particles(i).y - layer.particles(i).cy;
                        Normalize( radX, radY );
                        tanX := -radY;
                        tanY := radX;
                        tanX := tanX * layer.particles(i).vTangent;
                        tanY := tanY * layer.particles(i).vTangent;
                    end if;
                    layer.particles(i).x := layer.particles(i).x + (layer.particles(i).vx + tanX) * dt;
                    layer.particles(i).y := layer.particles(i).y + (layer.particles(i).vy + tanY) * dt;

                    -- rotation
                    layer.particles(i).rot := layer.particles(i).rot + layer.particles(i).vRot * dt;
                    if layer.particles(i).rot > 2.0 * Pi then
                        layer.particles(i).rot := layer.particles(i).rot - 2.0 * Pi;
                    elsif layer.particles(i).rot < -2.0 * Pi then
                        layer.particles(i).rot := layer.particles(i).rot + 2.0 * Pi;
                    end if;

                    -- size
                    layer.particles(i).size := Float'Max( 1.0, layer.particles(i).size1 + (layer.particles(i).size2 - layer.particles(i).size1) * progress );

                    -- color & opacity
                    layer.particles(i).color := Interpolate( layer.particles(i).color1, layer.particles(i).color2, progress );

                    -- emissive light brightness
                    layer.particles(i).light := layer.particles(i).light1 + (layer.particles(i).light2 - layer.particles(i).light1) * progress;

                    -- texture animation
                    layer.particles(i).texture.Update_Animation( layer.particles(i).age );

                    i := i + 1;
                else
                    -- remove the particle
                    if i < layer.pCount then
                        layer.particles(i) := layer.particles(layer.pCount);
                    end if;
                    layer.pCount := layer.pCount - 1;
                end if;
            end loop;
        end loop;

        -- update emitters --
        i := 1;
        while i <= this.eCount loop
            dt2 := time.elapsed;
            if this.emitters(i).componentId = NULL_ID then
                -- only emitters unattached to a component have a limited life
                if time.elapsed > this.emitters(i).life and then this.emitters(i).life > Time_Span_Zero then
                    -- when the emitter'd life expires between two ticks, only
                    -- count the lifetime that was remaining.
                    dt2 := this.emitters(i).life;
                end if;
                this.emitters(i).life := this.emitters(i).life - dt2;
            end if;
            if this.emitters(i).life >= Time_Span_Zero then
                -- calculate the fractional part of the emitter's age in seconds
                -- at the previous tick. we will then calculate the number of
                -- particles that were emitted, so far, in this second of the
                -- particle's life, and compare it to the number of particles
                -- that should be emitted up to this point. this allows for
                -- particle emission rates slower the tick rate.
                --
                -- NOTE: at this point, .age is the emitter's age at the
                -- previous tick.
                progress := Float(To_Duration(this.emitters(i).age - (Seconds( 1 ) * (this.emitters(i).age / Seconds( 1 )))));

                this.Spawn_Particles( this.emitters(i),
                                      Integer((progress + Float(To_Duration(dt2))) * this.emitters(i).rate) -     -- particles needed this second
                                      Integer(progress * this.emitters(i).rate) );                                -- particles emitted this second

                this.emitters(i).age := this.emitters(i).age + dt2;
                i := i + 1;
            else
                -- remove the emitter
                if i < this.eCount then
                    this.emitters(i) := this.emitters(this.eCount);
                end if;
                this.eCount := this.eCount - 1;
            end if;
        end loop;
    end Tick;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Particle_System ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

end Particles.Systems;
