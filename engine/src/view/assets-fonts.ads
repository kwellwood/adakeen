--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Color;                     use Allegro.Color;
with Allegro.Fonts;                     use Allegro.Fonts;
with Resources;                         use Resources;

package Assets.Fonts is

    -- Initializes the font system. Returns True on success or False on failure.
    function Initialize_Fonts return Boolean;

    -- Finalizes the font system. If Initialize has not been called, then this
    -- will have no effect.
    procedure Finalize_Fonts;

    -- Registers font name 'name' as an alias for the font at 'filePath'. This
    -- allows the font to be loaded using 'name' as the filePath argument to
    -- Load_Font().
    procedure Register_Font_Name( name : String; filePath : String );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- A Font represents a specific font of a specific size. Two Font instances
    -- are required to work with the same font in two different point sizes.
    type Font is abstract new Asset with private;
    type A_Font is access all Font'Class;

    -- Loads a font at a specific size. The file format will be automatically
    -- detected; it must be a supported font format. An exception will be raised
    -- on error.
    function Load_Font( filePath : String; size : Positive ) return A_Font;

    -- Renders a string onto the drawing target.
    procedure Draw_String( this  : not null access Font'Class;
                           str   : String;
                           x, y  : Float;
                           color : Allegro_Color );

    -- Returns the ascent height in pixels of the font at its given size. This
    -- is the height of the text from the baseline to the top, not including
    -- below the baseline (the descent) for letters like q, g, j, etc.
    function Ascent( this : not null access Font'Class ) return Natural;

    -- Returns the descent height in pixels of the font at its given size. This
    -- is the height of the text from the baseline to the bottom, not including
    -- above the baseline (the ascent). Letters like q, g, h, etc. use the
    -- descent area.
    function Descent( this : not null access Font'Class ) return Natural;

    -- Returns the line height in pixels of a string rendered with the font.
    -- Returns 10 if 'this' is null.
    function Line_Height( this : not null access Font'Class ) return Natural;

    -- Returns the length in pixels of a string rendered with the font.
    -- Returns str'Length if 'this' is null.
    function Text_Length( this : not null access Font'Class;
                          str  : String ) return Natural;

    -- Clears this reference to the asset. This must be called once for each
    -- call to Load_Font(). The asset is shared and will not be unloaded until
    -- no more reference remain.
    procedure Delete( this : in out A_Font );
    pragma Postcondition( this = null );

private

    type Font is new Asset with
        record
            res    : A_Resource_File := null;       -- backs the Allegro file
            alFont : A_Allegro_Font := null;
            size   : Positive := 1;

            -- the drawn flag is a workaround to ensure that the font is
            -- loaded on the drawing thread being being drawn. if the font has
            -- not been drawn yet, it will be reloaded by the Textout procedure
            -- immediately before drawing the first time.
            drawn : Boolean := False;
        end record;

    procedure Construct( this     : access Font;
                         assetId  : Value'Class;
                         filePath : String;
                         size     : Positive );

    function Load_Data( this : access Font ) return Boolean;

    procedure Unload_Data( this : access Font );

end Assets.Fonts;
