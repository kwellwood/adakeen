--
-- Copyright (c) 2012 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Keyboard is

    overriding
    function "="( l, r : Modifiers_Array ) return Boolean
    is (
        l(ALT)   = r(ALT)   and then
        l(CTRL)  = r(CTRL)  and then
        l(SHIFT) = r(SHIFT) and then
        l(CMD)   = r(CMD)
    );

    ----------------------------------------------------------------------------

    function None( mods : Modifiers_Array ) return Boolean
    is (
        not (mods(ALT)   or else
             mods(CTRL)  or else
             mods(SHIFT) or else
             mods(CMD))
    );

    ----------------------------------------------------------------------------

    function Only_Alt( mods : Modifiers_Array ) return Boolean
    is (
            mods(ALT)   and then
        not mods(CTRL)  and then
        not mods(SHIFT) and then
        not mods(CMD)
    );

    ----------------------------------------------------------------------------

    function Only_Ctrl( mods : Modifiers_Array ) return Boolean
    is (
        not mods(ALT)   and then
            mods(CTRL)  and then
        not mods(SHIFT) and then
        not mods(CMD)
    );

    ----------------------------------------------------------------------------

    function Only_Shift( mods : Modifiers_Array ) return Boolean
    is (
        not mods(ALT)   and then
        not mods(CTRL)  and then
            mods(SHIFT) and then
        not mods(CMD)
    );

    ----------------------------------------------------------------------------

    function Only_Cmd( mods : Modifiers_Array ) return Boolean
    is (
        not mods(ALT)   and then
        not mods(CTRL)  and then
        not mods(SHIFT) and then
            mods(CMD)
    );

    --==========================================================================

    function "="( l : Boolean; r : Trillian ) return Boolean
    is (
        r = Either           or else
        (l and then r = Yes) or else
        (not l and then r = No)
    );

    ----------------------------------------------------------------------------

    function "="( l : Trillian; r : Boolean ) return Boolean
    is (
        l = Either           or else
        (l = Yes and then r) or else
        (l = No and then not r)
    );

    --==========================================================================

    function "="( l : Modifiers_Array; r : Modifiers_Pattern ) return Boolean is
    begin
        for i in l'Range loop
            if l(i) /= r(i) then
                return False;
            end if;
        end loop;
        return True;
    end "=";

    ----------------------------------------------------------------------------

    function "="( l : Modifiers_Pattern; r : Modifiers_Array ) return Boolean is
    begin
        for i in l'Range loop
            if l(i) /= r(i) then
                return False;
            end if;
        end loop;
        return True;
    end "=";

end Keyboard;
