--
-- Copyright (c) 2012-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Color;                     use Allegro.Color;

package Drawing is

    -- Sets the drawing target bitmap.
    procedure Set_Target_Bitmap( bmp : A_Allegro_Bitmap );

    -- Returns the drawing target bitmap.
    function Get_Target_Bitmap return A_Allegro_Bitmap;

    -- Returns the width of the drawing target in pixels.
    function Get_Target_Width return Natural;

    -- Returns the height of the drawing target in pixels.
    function Get_Target_Height return Natural;

    -- Clears the entire drawing target to a single color.
    procedure Clear_To_Color( color : Allegro_Color );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Blend_Mode is
    (
        Current,           -- the thread's current blending mode (no change)
        Normal,            -- default blending mode, alpha is premultiplied
        Nonpremultiplied,  -- alpha is not premultiplied
        Additive,          -- source is added to destination
        Multiply,          -- source is multiplied with destination
        Overwrite          -- replace destination with source, including
    );

end Drawing;
