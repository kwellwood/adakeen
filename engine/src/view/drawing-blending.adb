--
-- Copyright (c) 2017-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Blending;                  use Allegro.Blending;

package body Drawing.Blending is

    procedure Set_Blend_Mode( blendMode : Blend_Mode ) is
    begin
        -- See http://liballeg.org/a5docs/trunk/graphics.html#blending-modes
        case blendMode is
            when Normal           => Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE,        ALLEGRO_INVERSE_ALPHA );
            when Nonpremultiplied => Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ALPHA,      ALLEGRO_INVERSE_ALPHA );
            when Additive         => Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ALPHA,      ALLEGRO_ONE           );
            when Multiply         => Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_DEST_COLOR, ALLEGRO_ZERO          );
            when Overwrite        => Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE,        ALLEGRO_ZERO          );
            when Current          => null;
        end case;
    end Set_Blend_Mode;

    ----------------------------------------------------------------------------

    procedure Set_Blend_Mode( state : in out Allegro_State; blendMode : Blend_Mode ) is
    begin
        Al_Store_State( state, ALLEGRO_STATE_BLENDER );
        Set_Blend_Mode( blendMode );
    end Set_Blend_Mode;

    ----------------------------------------------------------------------------

    procedure Restore_Blend_Mode( state : in out Allegro_State ) is
    begin
        Al_Restore_State( state );
    end Restore_Blend_Mode;

end Drawing.Blending;
