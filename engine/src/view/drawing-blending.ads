--
-- Copyright (c) 2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.State;                     use Allegro.State;

package Drawing.Blending is

    -- Sets the current blending mode to 'blendMode' immediately.
    procedure Set_Blend_Mode( blendMode : Blend_Mode );

    -- Stores the current blending state into 'state' and then sets the blending
    -- mode to 'blendMode'. Use Restore_Blend_Mode() or Al_Restore_State() to
    -- reapply the previous blending mode.
    procedure Set_Blend_Mode( state : in out Allegro_State; blendMode : Blend_Mode );

    -- Restores the blending mode previously stored in 'state' (along with any
    -- other Allegro state).
    procedure Restore_Blend_Mode( state : in out Allegro_State );

end Drawing.Blending;
