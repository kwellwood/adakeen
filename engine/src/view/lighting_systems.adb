--
-- Copyright (c) 2015-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers;                    use Ada.Containers;
with Ada.Numerics;                      use Ada.Numerics;
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Shaders;                   use Allegro.Shaders;
with Allegro.Transformations;           use Allegro.Transformations;
with Clipping;                          use Clipping;
--with Debugging;                         use Debugging;
with Drawing;                           use Drawing;
with Drawing.Bitmaps;                   use Drawing.Bitmaps;
with Drawing.Blending;                  use Drawing.Blending;
with Drawing.Primitives;                use Drawing.Primitives;
with Events.Entities;                   use Events.Entities;
with Events.Entities.Locations;         use Events.Entities.Locations;
with Events.Lighting;                   use Events.Lighting;
with Events.World;                      use Events.World;
with Interfaces;                        use Interfaces;
with Palette;                           use Palette;
with Palette.Tango;                     use Palette.Tango;
with Game_Views;                        use Game_Views;
with Renderers;
with Values;                            use Values;
with Values.Casting;                    use Values.Casting;

package body Lighting_Systems is

    package Point_Vectors is new Ada.Containers.Vectors(Positive, Vec2, "=");
    use Point_Vectors;

    ----------------------------------------------------------------------------

    -- Returns true if wall 'wallA' is in front of wall 'wallB', relative to
    -- the point 'p'.
    function In_Front_Of_Wall( wallA, wallB : Wall_Type; p : Vec2 ) return Boolean is

        ------------------------------------------------------------------------

        -- Returns the endpoint of a slightly shortened segment derived from
        -- points 'a' -> 'b'. This is used to avoid detecting intersections
        -- between walls that share an endpoint (common). The new segment would
        -- run from point 'a' to Shorten'Result.
        function Shorten( a, b : Vec2; fuzz : Float := 0.01 ) return Vec2 is ((a * (1.0 - fuzz)) + (b * fuzz));

        ------------------------------------------------------------------------

        a1, a2, a3 : Boolean;
        b1, b2, b3 : Boolean;
        q          : Vec2;      -- the shortened end of the segment to test
    begin
        -- Note: we slightly shorten the walls with Shorten so
        -- that intersections of the endpoints (common) don't count as
        -- wall intersections in this algorithm.

        q  := Shorten( (wallB.p1.x, wallB.p1.y), (wallB.p2.x, wallB.p2.y) );
        a1 := Left_Of( To_Segment( wallA.p2.x, wallA.p2.y, wallA.p1.x, wallA.p1.y ), q );

        q  := Shorten( (wallB.p2.x, wallB.p2.y), (wallB.p1.x, wallB.p1.y) );
        a2 := Left_Of( To_Segment( wallA.p2.x, wallA.p2.y, wallA.p1.x, wallA.p1.y ), q );
        a3 := Left_Of( To_Segment( wallA.p2.x, wallA.p2.y, wallA.p1.x, wallA.p1.y ), p );

        q  := Shorten( (wallA.p1.x, wallA.p1.y), (wallA.p2.x, wallA.p2.y) );
        b1 := Left_Of( To_Segment( wallB.p2.x, wallB.p2.y, wallB.p1.x, wallB.p1.y ), q );

        q  := Shorten( (wallA.p2.x, wallA.p2.y), (wallA.p1.x, wallA.p1.y) );
        b2 := Left_Of( To_Segment( wallB.p2.x, wallB.p2.y, wallB.p1.x, wallB.p1.y ), q );
        b3 := Left_Of( To_Segment( wallB.p2.x, wallB.p2.y, wallB.p1.x, wallB.p1.y ), p );

        -- Note: this algorithm is probably worthy of a short article
        -- but for now, draw it on paper to see how it works. Consider
        -- the line A1-A2. If both B1 and B2 are on one side and Light
        -- is on the other side, then A is in between the light and B.
        -- We can do the same with B1-B2: if A1 and A2 are on one side,
        -- and Light is on the other side, then B is in between the
        -- light and A.
        --
        -- See http://www.redblobgames.com/articles/visibility/segment-sorting.html
        -- See the cached copy in the repo at docs/segment sorting/.

        if b1 = b2 and b2 /= b3 then
            return True;
        end if;
        if a1 = a2 and a2  = a3 then
            return True;
        end if;
        if a1 = a2 and a2 /= a3 then
            return False;
        end if;
        if b1 = b2 and b2  = b3 then
            return False;
        end if;

        return False;
    end In_Front_Of_Wall;

    --==========================================================================

    procedure Handle_Entity_Moved( this : not null access Lighting_System'Class;
                                   evt  : not null A_Entity_Moved_Event );

    procedure Handle_Light_Created( this : not null access Lighting_System'Class;
                                    evt  : not null A_Light_Event );

    procedure Handle_Light_Updated( this : not null access Lighting_System'Class;
                                    evt  : not null A_Light_Event );

    procedure Handle_Light_Deleted( this : not null access Lighting_System'Class;
                                    evt  : not null A_Light_Event );

    procedure Handle_World_Property_Changed( this : not null access Lighting_System'Class;
                                             evt  : not null A_World_Property_Event );

    procedure Insert_Horizontal( this   : not null access Lighting_System'Class;
                                 x1, y1 : Float;
                                 x2, y2 : Float;
                                 faceUp : Boolean );

    procedure Insert_Vertical( this      : not null access Lighting_System'Class;
                               x         : Float;
                               y1, y2    : Float;
                               faceRight : Boolean );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    not overriding
    function Intersects( this : Light_Source; rect : Rectangle ) return Boolean
    is (Intersect( Rectangle'(this.x - this.w / 2.0, this.y - this.h / 2.0, this.w, this.h), rect ));

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Light_Source ) is
    begin
        Delete( A_Object(this) );
    end Delete;

    --==========================================================================

    procedure Compute_Triangles( this      : Point_Light'Class;
                                 walls     : Wall_Vectors.Vector;
                                 endpoints : Endpoint_Vectors.Vector;
                                 triangles : out Point_Vectors.Vector ) is

        ------------------------------------------------------------------------

        -- Adds a new triangle to the light's geometry, from 'angle1' to
        -- 'angle2', where 'wall' is the index of the intersecting wall in 'walls'.
        procedure Add_Triangle( angle1, angle2 : Float; wall : Integer ) is

            --------------------------------------------------------------------

            -- Returns the point of intersection between two intersecting
            -- lines a1-a2 and b1-b2.
            --
            -- This method differs from the more complete Intersect() method by
            -- assuming that 'segA' and 'segB' do intersect, allowing for speed
            -- optimizations.
            --
            -- Equation from http://paulbourke.net/geometry/lineline2d
            -- See the cached copy in the repo at docs/intersection/.
            function Intersection( segA, segB : Segment ) return Vec2 is
                s : Float := Cross( segA.p2 - segA.p1, segB.p2 - segB.p1 );
            begin
                -- guard against divide by zero
                if s /= 0.0 then
                    s := Cross( segB.p2 - segB.p1, segA.p1 - segB.p1 ) / s;
                end if;
                return segA.p1 + s * (segA.p2 - segA.p1);
            end Intersection;

            --------------------------------------------------------------------

            p1 : constant Vec2 := (this.x, this.y);
            p2 : Vec2 := (this.x + Cos( angle1 ), this.y + Sin( angle1 ));
            p3 : Vec2;
            p4 : Vec2;
            w  : Wall_Type;
        begin
            if wall > 0 then
                -- stop the triangle at the intersecting wall
                w := walls.Constant_Reference( wall );
                p3 := (w.p1.x, w.p1.y);
                p4 := (w.p2.x, w.p2.y);
            else
                -- stop the triangle at a fixed distance so it doesn't go
                -- out to infinity.
                p3 := (this.x + Cos( angle1 ) * this.w, this.y + Sin( angle1 ) * this.h);
                p4 := (this.x + Cos( angle2 ) * this.w, this.y + Sin( angle2 ) * this.h);
            end if;

            triangles.Append( Intersection( (p3, p4), (p1, p2) ) );     -- begin

            p2 := (this.x + Cos( angle2 ), this.y + Sin( angle2 ));

            triangles.Append( Intersection( (p3, p4), (p1, p2) ) );     -- end
        end Add_Triangle;

        ------------------------------------------------------------------------

        package Integer_Vectors is new Ada.Containers.Vectors(Positive, Integer, "=");
        use Integer_Vectors;

        open         : Integer_Vectors.Vector;
        inserted     : Boolean;
        currentAngle : Float := 0.0;
        prevWall     : Integer;
        nextWall     : Integer;
    begin
        for pass in 0..1 loop
            for p of endpoints loop
                prevWall := (if open.Is_Empty then 0 else open.First_Element);

                if p.start then
                    -- insert into the right place in the list
                    inserted := False;
                    for i in 1..Integer(open.Length) loop
                        if not In_Front_Of_Wall( walls.Constant_Reference( p.wall ),
                                                 walls.Constant_Reference( open.Constant_Reference( i ) ),
                                                 Vec2'(this.x, this.y) )
                        then
                            open.Insert( i, p.wall );
                            inserted := True;
                            exit;
                        end if;
                    end loop;
                    if not inserted then
                        open.Append( p.wall );
                    end if;
                else
                    for i in 1..Integer(open.Length) loop
                        if open.Constant_Reference( i ) = p.wall then
                            open.Delete( i );
                            exit;
                        end if;
                    end loop;
                end if;

                nextWall := (if open.Is_Empty then 0 else open.First_Element);

                if nextWall /= prevWall then
                    if pass = 1 then
                        -- triangle
                        Add_Triangle( currentAngle, p.angle, prevWall );
                    end if;
                    currentAngle := p.angle;
                end if;
            end loop;
        end loop;
    end Compute_Triangles;

    ----------------------------------------------------------------------------

    function Compute_Triangle_List( this  : in out Point_Light;
                                    color : Allegro_Color ) return Natural is
        geometry  : Wall_Vectors.Vector;
        endpoints : Endpoint_Vectors.Vector;
        triangles : Point_Vectors.Vector;
        p         : Vec2;
        index     : Integer;
        index2    : Integer;
    begin
        Find_Geometry( this, geometry );
        Sort_Endpoints( this, geometry, endpoints );
        Compute_Triangles( this, geometry, endpoints, triangles );

        if this.vertices = null or else this.vertices'Last < Integer(triangles.Length) then
            Delete( this.vertices );
            this.vertices := new Allegro_Vertex_Array(0..Integer(triangles.Length));
        end if;

        this.vertices(0).x := this.x;
        this.vertices(0).y := this.y;
        this.vertices(0).z := this.z;
        this.vertices(0).u := this.w / 2.0;
        this.vertices(0).v := this.h / 2.0;
        this.vertices(0).color := color;

        index := 1;
        while index <= Integer(triangles.Length) loop
            p := triangles.Constant_Reference( index );
            this.vertices(index).x := p.x;
            this.vertices(index).y := p.y;
            this.vertices(index).z := this.z;
            this.vertices(index).u := p.x - this.x + this.w / 2.0;
            this.vertices(index).v := p.y - this.y + this.h / 2.0;
            this.vertices(index).color := color;
            index := index + 1;
        end loop;

        -- no need to rebuild the vertex index list unless it's not long enough
        if this.indices = null or else this.indices'Length < (Integer(triangles.Length)/2)*3 then
            Delete( this.indices );
            this.indices := new Vertex_Index_Array(0..(Integer(triangles.Length)/2)*3-1);
            index := 1;
            index2 := this.indices'First;
            while index <= Integer(triangles.Length) loop
                this.indices(index2) := 0;
                this.indices(index2 + 1) := index;
                this.indices(index2 + 2) := index + 1;
                index := index + 2;
                index2 := index2 + 3;
            end loop;
        end if;

        return Natural(triangles.Length);
    end Compute_Triangle_List;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Point_Light ) is
    begin
        Delete( this.vertices );
        Delete( this.indices );
        Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw( this : in out Point_Light; viewport : Rectangle ) is
        pragma Unreferenced( viewport );
        diffuse : constant Float := (if this.shadows then this.diffuse else 1.0);
        count   : Natural;
    begin
        Al_Set_Shader_Float( "lightRadius", this.w / 2.0 );
        Al_Set_Shader_Float_Vector( "lightColor", this.color );
        Al_Set_Shader_Float( "lightAngle", this.angle * (Pi / 180.0) - Pi );
        Al_Set_Shader_Float( "lightArc", this.arc * (Pi / 180.0) );

        -- draw the shadowed light component, omitting shadowed areas
        if this.shadows then
            count := this.Compute_Triangle_List( Opacity( White, 1.0 - this.diffuse ) );

            -- draw the light geometry with shadows cut out
            Al_Draw_Indexed_Prim( this.vertices(this.vertices'First..this.vertices'First+count),
                                  null,
                                  this.indices(this.indices'First..this.indices'First+(count/2)*3-1),
                                  ALLEGRO_PRIM_TRIANGLE_LIST );
        end if;

        -- draw the diffuse light component (no shadows)
        if this.diffuse > 0.0 then
            if this.vertices = null or else this.vertices'Last < 3 then
                Delete( this.vertices );
                this.vertices := new Allegro_Vertex_Array(0..3);
            end if;

            -- this pair of triangles is structured like a quad, with texture
            -- coordinates ranging from 0.0 to this.w the "light" shader.
            this.vertices(0).x := this.x - this.w / 2.0;
            this.vertices(0).y := this.y - this.h / 2.0;
            this.vertices(0).z := this.z;
            this.vertices(0).u := 0.0;
            this.vertices(0).v := 0.0;
            this.vertices(0).color := Opacity( this.color, diffuse );

            this.vertices(1).x := this.x + this.w / 2.0;
            this.vertices(1).y := this.y - this.h / 2.0;
            this.vertices(1).z := this.z;
            this.vertices(1).u := this.w;
            this.vertices(1).v := 0.0;
            this.vertices(1).color := this.vertices(0).color;

            this.vertices(2).x := this.x - this.w / 2.0;
            this.vertices(2).y := this.y + this.h / 2.0;
            this.vertices(2).z := this.z;
            this.vertices(2).u := 0.0;
            this.vertices(2).v := this.h;
            this.vertices(2).color := this.vertices(0).color;

            this.vertices(3).x := this.x + this.w / 2.0;
            this.vertices(3).y := this.y + this.h / 2.0;
            this.vertices(3).z := this.z;
            this.vertices(3).u := this.w;
            this.vertices(3).v := this.h;
            this.vertices(3).color := this.vertices(0).color;

            Al_Draw_Indexed_Prim( this.vertices(0..3),
                                  null,
                                  (0, 1, 2, 3),
                                  ALLEGRO_PRIM_TRIANGLE_STRIP );
        end if;
    end Draw;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Debug( this : in out Point_Light) is
        lightX1 : constant Float := this.x - this.w / 2.0;
        lightY1 : constant Float := this.y - this.h / 2.0;

        ------------------------------------------------------------------------

        procedure Draw_Wall( x1, y1, x2, y2 : Float; item : Integer ) is
            pragma Unreferenced( item );
            sx1 : Float := x1;
            sy1 : Float := y1;
            sx2 : Float := x2;
            sy2 : Float := y2;
        begin
            To_Target_Pixel_Center( sx1, sy1 );
            To_Target_Pixel_Center( sx2, sy2 );

            -- draw each wall interacting with this light
            Line( sx1, sy1, sx2, sy2, Chameleon1, From_Target_Pixels( 1.0 ) );
        end Draw_Wall;

        ------------------------------------------------------------------------

        count : Natural;
    begin
        -- highlight all walls intersecting the lit area
        this.system.walls.Intersecting_Rect( lightX1, lightY1, this.w, this.h, Draw_Wall'Access );

        -- draw the light geometry wireframe
        if this.shadows then
            count := this.Compute_Triangle_List( Plum1 );

            for v in this.vertices'First..this.vertices'First+count loop
                To_Target_Pixel_Center( this.vertices(v).x, this.vertices(v).y );
                this.vertices(v).z := 0.0;   -- relative to scene widget depth
            end loop;

            -- draw the light geometry as a wireframe
            Al_Draw_Indexed_Prim( this.vertices(this.vertices'First..this.vertices'First+count),
                                  null,
                                  this.indices(this.indices'First..this.indices'First+(count/2)*3-1),
                                  ALLEGRO_PRIM_LINE_STRIP );
        else
            Rect_XYWH( lightX1, lightY1, this.w, this.h, Plum1 );
        end if;
    end Draw_Debug;

    ----------------------------------------------------------------------------

    procedure Find_Geometry( this     : Point_Light'Class;
                             geometry : in out Wall_Vectors.Vector ) is
        lightX1 : constant Float := this.x - this.w / 2.0;
        lightY1 : constant Float := this.y - this.h / 2.0;
        lightX2 : constant Float := this.x + this.w / 2.0;
        lightY2 : constant Float := this.y + this.h / 2.0;
        bisect         : Boolean := False;
        index          : Integer;
        wallA          : Wall_Type;
        wallB          : Wall_Type;
        ix             : Vec2;
        found          : Boolean;
        intersectFound : Boolean;
    begin
        -- find all walls intersecting the lit area
        declare
            procedure Examine( x1, y1, x2, y2 : Float; item : Integer ) is
                pragma Unreferenced( item );
            begin
                geometry.Append( Wall_Type'(p1 => Endpoint'(x1, y1, others => <>), p2 => Endpoint'(x2, y2, others => <>)) );

                -- if the wall extends beyond the light bounds, we will need to
                -- bisect the light boundary walls
                bisect := bisect or
                          (x1 < lightX1 or x2 < lightX1 or lightX2 < x1 or lightX2 < x2 or
                           y1 < lightY1 or y2 < lightY1 or lightY2 < y1 or lightY2 < y2);

                -- draw each wall interacting with this light (debugging)
                --Line( x1, y1, x2, y2, Chameleon1 );
            end Examine;
        begin
            this.system.walls.Intersecting_Rect( lightX1, lightY1,
                                                 this.w, this.h,
                                                 Examine'Access );
        end;

        -- light bounds (top, right, bottom, left)
        geometry.Append( Wall_Type'(p1 => Endpoint'(lightX1, lightY1, others => <>), p2 => Endpoint'(lightX2, lightY1, others => <>)) );
        geometry.Append( Wall_Type'(p1 => Endpoint'(lightX2, lightY1, others => <>), p2 => Endpoint'(lightX2, lightY2, others => <>)) );
        geometry.Append( Wall_Type'(p1 => Endpoint'(lightX2, lightY2, others => <>), p2 => Endpoint'(lightX1, lightY2, others => <>)) );
        geometry.Append( Wall_Type'(p1 => Endpoint'(lightX1, lightY2, others => <>), p2 => Endpoint'(lightX1, lightY1, others => <>)) );

        if not bisect then
            return;
        end if;

        -- bisect all walls that intersect the light bounds (last 4 walls)
        index := Integer(geometry.Length) - 3;
        while index <= Integer(geometry.Length) loop
            wallA := geometry.Constant_Reference( index );

            -- check for any intersections with 'wallA'
            intersectFound := False;
            for index2 in 1..Integer(geometry.Length) loop
                wallB := geometry.Constant_Reference( index2 );

                -- note: this limited parallel test is ok because the light
                -- bounds are strictly vertical and horizontal.
                if not (wallA.p1.x = wallA.p2.x and wallB.p1.x = wallB.p2.x) and   -- parallel vertical
                   not (wallA.p1.y = wallA.p2.y and wallB.p1.y = wallB.p2.y)       -- parallel horizontal
                then
                    -- check for a possible intersection and bisect as necessary
                    Intersect( Segment'((wallA.p1.x, wallA.p1.y), (wallA.p2.x, wallA.p2.y)),
                               Segment'((wallB.p1.x, wallB.p1.y), (wallB.p2.x, wallB.p2.y)),
                               ix,
                               found );

                    -- check for a single point of intersection. if ix = wallA.p1
                    -- then wallA and wallB are co-incident and can't be bisected.
                    if found and not (ix.x = wallA.p1.x and ix.y = wallA.p1.y) then

                        -- bisect 'wallA'
                        geometry.Append( Wall_Type'(p1 => Endpoint'(ix.x, ix.y, others => <>),
                                                    p2 => Endpoint'(wallA.p2.x, wallA.p2.y, others => <>)) );
                        wallA.p2.x := ix.x;
                        wallA.p2.y := ix.y;
                        geometry.Replace_Element( index, wallA );

                        -- bisect 'wallB'
                        geometry.Append( Wall_Type'(p1 => Endpoint'(ix.x, ix.y, others => <>),
                                                    p2 => Endpoint'(wallB.p2.x, wallB.p2.y, others => <>)) );
                        wallB.p2.x := ix.x;
                        wallB.p2.y := ix.y;
                        geometry.Replace_Element( index2, wallB );

                        intersectFound := True;
                        exit;
                    end if;
                end if;
            end loop;

            if not intersectFound then
                -- check for intersections against the next wall after all
                -- intersections with 'wallA' have been resolved.
                index := index + 1;
            end if;
        end loop;
    end Find_Geometry;

    ----------------------------------------------------------------------------

    procedure Sort_Endpoints( this      : Point_Light'Class;
                              walls     : in out Wall_Vectors.Vector;
                              endpoints : in out Endpoint_Vectors.Vector ) is

        function "<"( l, r : Endpoint ) return Boolean
        is (l.angle < r.angle or (l.angle = r.angle and l.start and not r.start));

        package Endpoint_Sorting is new Endpoint_Vectors.Generic_Sorting("<");

        index     : Integer := 1;
        angleDiff : Float;
    begin
        for w of walls loop
            -- calculate the angle of each of the wall's endpoints relative
            -- to the light source
            if w.p1.x /= this.x or w.p1.y /= this.y then
                w.p1.angle := Arctan( w.p1.y - this.y, w.p1.x - this.x );
            end if;
            if w.p2.x /= this.x or w.p2.y /= this.y then
                w.p2.angle := Arctan( w.p2.y - this.y, w.p2.x - this.x );
            end if;

            -- calculate which endpoint is the first to be encountered in a
            -- clockwise sweep
            angleDiff := w.p2.angle - w.p1.angle;
            if angleDiff <= -Pi then
                angleDiff := angleDiff + Pi * 2.0;
            end if;
            if angleDiff > Pi then
                angleDiff := angleDiff - Pi * 2.0;
            end if;
            w.p1.start := (angleDiff > 0.0);
            w.p2.start := not w.p1.start;

            -- store a reference to the parent wall
            w.p1.wall := index;
            w.p2.wall := index;

            endpoints.Append( w.p1 );
            endpoints.Append( w.p2 );

            index := index + 1;
        end loop;

        -- sort the endpoints by angle
        Endpoint_Sorting.Sort( endpoints );
    end Sort_Endpoints;

    --==========================================================================

    overriding
    procedure Draw( this : in out Area_Light; viewport : Rectangle ) is
        angle    : constant Float := this.angle * (Pi / 180.0);
        lightRay : constant Vec2 := (Cos( angle ), Sin( angle ));
        shadow   : constant Vec2 := (this.w + this.h) * lightRay;
        geometry : Wall_Vectors.Vector;
        vertices : Allegro_Vertex_Array(0..3);
        state    : Allegro_State;
        accTrans : Allegro_Transform;
        ident    : Allegro_Transform;
    begin
        Al_Copy_Transform( accTrans, Al_Get_Current_Transform.all );

        Al_Store_State( state, ALLEGRO_STATE_TARGET_BITMAP or ALLEGRO_STATE_BLENDER );
        Set_Target_Bitmap( this.system.litArea );
        this.system.view.Use_Shader( "default" );
        Set_Blend_Mode( Nonpremultiplied );

        -- use the same transform as the light accumulation buffer
        Al_Use_Transform( accTrans );

        Clear_To_Color( Black );
        Rectfill( this.x - this.w / 2.0, this.y - this.h / 2.0,
                  this.x + this.w / 2.0, this.y + this.h / 2.0,
                  this.color );

        -- debugging: show the direction of the shadows
        --Line( this.x, this.y, this.x + shadow.x, this.y + shadow.y, Scarlet2 );
        if this.shadows then
            -- find all walls intersecting the lit area
            declare
                procedure Examine( x1, y1, x2, y2 : Float; item : Integer ) is
                    pragma Unreferenced( item );
                begin
                    -- only add walls facing the light
                    if Side_Of_Line( (x1, y1), (x2, y2), (x1, y1) + lightRay ) > 0.0 then
                        geometry.Append( Wall_Type'(p1 => Endpoint'(x1, y1, others => <>), p2 => Endpoint'(x2, y2, others => <>)) );
                    end if;
                end Examine;
            begin
                this.system.walls.Intersecting_Rect( this.x - this.w / 2.0, this.y - this.h / 2.0,
                                                     this.w, this.h,
                                                     Examine'Access );
            end;

            for v in vertices'Range loop
                vertices(v).z := 0.0;
                vertices(v).u := 0.0;
                vertices(v).v := 0.0;
                vertices(v).color := Lighten( this.color, this.diffuse );
            end loop;

            for wall of geometry loop
                vertices(0).x := wall.p1.x;
                vertices(0).y := wall.p1.y;

                vertices(1).x := wall.p2.x;
                vertices(1).y := wall.p2.y;

                vertices(2).x := wall.p1.x - shadow.x;
                vertices(2).y := wall.p1.y - shadow.y;

                vertices(3).x := wall.p2.x - shadow.x;
                vertices(3).y := wall.p2.y - shadow.y;

                Al_Draw_Indexed_Prim( vertices(0..3),
                                      null,
                                      (0, 1, 2, 3),
                                      ALLEGRO_PRIM_TRIANGLE_STRIP );
            end loop;

            -- clear away shadows in the buffer that are outside the lit area
            if this.y - this.h / 2.0 > viewport.y then
                -- clear the area above the light (top left of light to top right of buffer
                Rectfill( this.x - this.w / 2.0, this.y - this.h / 2.0,
                          viewport.x + viewport.width, viewport.y,
                          Black );
            end if;
            if this.x + this.w / 2.0 < viewport.x + viewport.width then
                -- clear the area right of the light (top right of light to bottom right of buffer)
                Rectfill( this.x + this.w / 2.0, this.y - this.h / 2.0,
                          viewport.x + viewport.width, viewport.y + viewport.height,
                          Black );
            end if;
            if this.y + this.h / 2.0 < viewport.y + viewport.height then
                -- clear the area below the light (bottom left of buffer to bottom right of light)
                Rectfill( viewport.x, viewport.y + viewport.height,
                          this.x + this.w / 2.0, this.y + this.h / 2.0,
                          Black );
            end if;
            if this.x - this.w / 2.0 > viewport.x then
                -- clear the area left of the light (top left of buffer to bottom left of light)
                Rectfill( viewport.x, viewport.y,
                          this.x - this.w / 2.0, this.y + this.h / 2.0,
                          Black );
            end if;
        end if;

        Al_Restore_State( state );

        -- add the lit area to the light accumulation buffer. they are the same
        -- same size with the same transform, so just blit this one on top. be
        -- sure to scale the Z axis correctly.
        Al_Identity_Transform( ident );
        Al_Scale_Transform_3d( ident, 1.0, 1.0, Renderers.Z_SCALE );
        Al_Use_Transform( ident );
        Draw_Bitmap( this.system.litArea, 0.0, 0.0, this.z );

        -- restore the accumulation buffer's transform
        Al_Use_Transform( accTrans );
    end Draw;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Debug( this : in out Area_Light ) is
        angle     : constant Float := this.angle * (Pi / 180.0);
        lightRay  : constant Vec2 := (Cos( angle ), Sin( angle ));
        shadow    : constant Vec2 := (this.w + this.h) * (-lightRay);
        thickness : constant Float := From_Target_Pixels( 1.0 );

        ------------------------------------------------------------------------

        procedure Examine_Wall( x1, y1, x2, y2 : Float; item : Integer ) is
            pragma Unreferenced( item );
            type Vec_Array is array (Integer range <>) of Vec2;
            area     : constant Rectangle := (this.x - this.w / 2.0, this.y - this.h / 2.0, this.w, this.h);
            vertices : Vec_Array(0..3);
        begin
            -- only walls facing the light
            if Side_Of_Line( (x1, y1), (x2, y2), (x1, y1) + lightRay ) > 0.0 then
                To_Target_Pixel_Center( vertices(0).x, vertices(0).y );
                To_Target_Pixel_Center( vertices(1).x, vertices(1).y );
                Line( vertices(0).x, vertices(0).y, vertices(1).x, vertices(1).y, Chameleon1, thickness );

                -- constrain (0 -> 1)
                vertices(0) := (x1, y1);
                vertices(1) := (x2, y2);
                Intersect( vertices(0), vertices(1), area );

                -- constrain (0 -> 3)
                vertices(3) := vertices(0) + shadow;
                Intersect( vertices(0), vertices(3), area );

                -- constrain (1 -> 2)
                vertices(2) := vertices(1) + shadow;
                Intersect( vertices(1), vertices(2), area );

                for v in vertices'Range loop
                    To_Target_Pixel_Center( vertices(v).x, vertices(v).y );
                end loop;

                --        0----1   <-- wall
                --       /####/
                --      /####/     <-- projected shadow
                --     3####2

                Line( vertices(3).x, vertices(3).y, vertices(0).x, vertices(0).y, Plum1, thickness );
                Line( vertices(0).x, vertices(0).y, vertices(1).x, vertices(1).y, Plum1, thickness );
                Line( vertices(1).x, vertices(1).y, vertices(2).x, vertices(2).y, Plum1, thickness );
            end if;
        end Examine_Wall;

        ------------------------------------------------------------------------

        sx1, sy1, sx2, sy2 : Float;
        vec : Vec2;
    begin
        -- draw the lit area bounds
        Rect_XYWH( this.x - this.w / 2.0, this.y - this.h / 2.0,
                   this.w, this.h,
                   Plum1 );

        if this.shadows then
            -- show the direction of the shadows
            vec := (-lightRay) * (Float'Min( this.w, this.h ) * 0.25);
            sx1 := this.x;
            sy1 := this.y;
            sx2 := this.x + vec.x;
            sy2 := this.y + vec.y;
            To_Target_Pixel_Center( sx1, sy1 );
            To_Target_Pixel_Center( sx2, sy2 );
            Line( sx1, sy1, sx2, sy2, Orange1, thickness );

            -- draw shadow quads
            this.system.walls.Intersecting_Rect( this.x - this.w / 2.0, this.y - this.h / 2.0,
                                                 this.w, this.h,
                                                 Examine_Wall'Access );
        end if;
    end Draw_Debug;

    --==========================================================================

    function Create_Lighting_System( view : not null access Game_Views.Game_View'Class ) return A_Lighting_System is
        this : constant A_Lighting_System := new Lighting_System;
    begin
        this.Construct( view );
        return this;
    end Create_Lighting_System;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Lighting_System;
                         view : not null access Game_Views.Game_View'Class ) is
    begin
        Limited_Object(this.all).Construct;
        this.view := view;
        this.ambient := White;

        this.view.Get_Corral.Add_Listener( this, EVT_ENTITY_MOVED );
        this.view.Get_Corral.Add_Listener( this, EVT_LIGHT_CREATED );
        this.view.Get_Corral.Add_Listener( this, EVT_LIGHT_UPDATED );
        this.view.Get_Corral.Add_Listener( this, EVT_LIGHT_DELETED );
        this.view.Get_Corral.Add_Listener( this, EVT_WORLD_PROPERTY_CHANGED );
    end Construct;

    ----------------------------------------------------------------------------

    procedure Clear( this : not null access Lighting_System'Class ) is
    begin
        this.ambient := White;
        for light of this.lights loop
            Delete( light );
        end loop;
        this.lights.Clear;
        this.walls.Clear;
    end Clear;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Lighting_System ) is
    begin
        this.view.Get_Corral.Remove_Listener( this'Unchecked_Access );

        this.Clear;
        Al_Destroy_Bitmap( this.lightAcc );
        Al_Destroy_Bitmap( this.litArea );
    end Delete;

    ----------------------------------------------------------------------------

    procedure Initialize( this      : not null access Lighting_System'Class;
                          mapInfo   : not null A_Map_Info_Cache;
                          tileWidth : Positive ) is
        width  : constant Integer := mapInfo.Get_Map.Get_Width;
        height : constant Integer := mapInfo.Get_Map.Get_Height;
        x, x1  : Integer;
        y, y1  : Integer;
        top,
        top2,
        bottom : Boolean;
        left,
        left2,
        right  : Boolean;
        shape,
        shape2 : Clip_Type;
    begin
        this.Clear;

        this.walls.Init( Float(width * tileWidth), Float(height * tileWidth), 6, 8 );

        -- build horizontal walls
        for y in 0..height loop
            x := 0;
            while x <= width loop
                top := Solid_Bottom( mapInfo.Get_Shape( x, y - 1 ) );
                top2 := top;
                bottom := Solid_Top( mapInfo.Get_Shape( x, y ) );
                if top xor bottom then
                    x1 := x;
                    while top = top2 loop
                        x := x + 1;
                        top := Solid_Bottom( mapInfo.Get_Shape( x, y - 1) );
                        bottom := Solid_Top( mapInfo.Get_Shape( x, y ) );
                        exit when not (top xor bottom);
                    end loop;
                    if top2 then
                        -- this wall is a bottom edge for row 'y-1'
                        -- it runs right to left so its left hand side is facing down
                        -- (x,y) -> (x1,y)
                        this.walls.Insert( Float(x * tileWidth), Float(y * tileWidth), Float(x1 * tileWidth), Float(y * tileWidth), 0 );
                    else
                        -- this wall is a top edge for row 'y'
                        -- it runs left to right so its left hand side is facing up
                        -- (x1,y) -> (x,y)
                        this.walls.Insert( Float(x1 * tileWidth), Float(y * tileWidth), Float(x * tileWidth), Float(y * tileWidth), 0 );
                    end if;
                else
                    -- skip the tile
                    x := x + 1;
                end if;
            end loop;
        end loop;

        -- build vertical walls
        for x in 0..width loop
            y := 0;
            while y <= height loop
                left := Solid_Right( mapInfo.Get_Shape( x - 1, y ) );
                left2 := left;
                right := Solid_Left( mapInfo.Get_Shape( x, y ) );
                if left xor right then
                    y1 := y;
                    while left = left2 loop
                        y := y + 1;
                        left := Solid_Right( mapInfo.Get_Shape( x - 1, y ) );
                        right := Solid_Left( mapInfo.Get_Shape( x, y ) );
                        exit when not (left xor right);
                    end loop;
                    if left2 then
                        -- this wall is a right edge for column 'x-1'
                        -- it runs top to bottom so its left hand side is facing right
                        -- (x,y1) -> (x,y)
                        this.walls.Insert( Float(x * tileWidth), Float(y1 * tileWidth), Float(x * tileWidth), Float(y * tileWidth), 0 );
                    else
                        -- this wall is a left edge for column 'x'
                        -- it runs bottom to top so its left hand side is facing left
                        -- (x,y) -> (x,y1)
                        this.walls.Insert( Float(x * tileWidth), Float(y * tileWidth), Float(x * tileWidth), Float(y1 * tileWidth), 0 );
                    end if;
                else
                    -- skip the tile
                    y := y + 1;
                end if;
            end loop;
        end loop;

        for y in 0..mapInfo.Get_Map.Get_Height loop
            x := 0;
            while x <= mapInfo.Get_Map.Get_Width loop
                shape := mapInfo.Get_Shape( x, y );
                if (shape = Slope_45_Up_Floor or shape = Slope_45_Up_Ceiling) and then shape /= mapInfo.Get_Shape( x + 1, y - 1 ) then
                    -- follow the slope down and to the left
                    shape2 := shape;
                    x1 := x;
                    y1 := y;
                    while shape2 = shape loop
                        x1 := x1 - 1;
                        y1 := y1 + 1;
                        shape := mapInfo.Get_Shape( x1, y1 );
                    end loop;
                    if shape2 = Slope_45_Up_Floor then
                        --     /#
                        -- .  /##
                        --  \/###
                        --  /####
                        -- the left hand side of the slope faces up and left
                        -- (x1,y1) -> (x,y)
                        this.walls.Insert( Float((x1 + 1) * tileWidth), Float(y1 * tileWidth), Float((x + 1) * tileWidth), Float(y * tileWidth), 0 );
                    else -- shape2 = Slope_45_Up_Ceiling
                        --
                        -- ####/
                        -- ###/
                        -- ##/\
                        -- #/  '
                        -- the left hand side of the slope faces down and right
                        -- (x,y) -> (x1,y1)
                        this.walls.Insert( Float((x + 1) * tileWidth), Float(y * tileWidth), Float((x1 + 1) * tileWidth), Float(y1 * tileWidth), 0 );
                    end if;
                elsif (shape = Slope_45_Down_Floor or shape = Slope_45_Down_Ceiling) and then shape /= mapInfo.Get_Shape( x - 1, y - 1 ) then
                    -- follow the slope down and to the right
                    shape2 := shape;
                    x1 := x;
                    y1 := y;
                    while shape2 = shape loop
                        x1 := x1 + 1;
                        y1 := y1 + 1;
                        shape := mapInfo.Get_Shape( x1, y1 );
                    end loop;
                    if shape2 = Slope_45_Down_Floor then
                        -- #\  .
                        -- ##\/
                        -- ###\
                        -- ####\
                        -- the left hand side of the slope faces up and right
                        -- (x,y) -> (x1,y1)
                        this.walls.Insert( Float(x * tileWidth), Float(y * tileWidth), Float(x1 * tileWidth), Float(y1 * tileWidth), 0 );
                    else -- shape2 = Slope_45_Down_Ceiling
                        --  \####
                        --  /\###
                        -- '  \##
                        --     \#
                        -- the left hand side of the slope faces down and left
                        -- (x1,y1) -> (x,y)
                        this.walls.Insert( Float(x1 * tileWidth), Float(y1 * tileWidth), Float(x * tileWidth), Float(y * tileWidth), 0 );
                    end if;
                end if;
                x := x + 1;
            end loop;
        end loop;

        for y in 0..mapInfo.Get_Map.Get_Height loop
            x := 0;
            while x <= mapInfo.Get_Map.Get_Width loop
                shape := mapInfo.Get_Shape( x, y );
                if shape = Slope_22_Up_Floor_Wide and then mapInfo.Get_Shape( x + 1, y - 1 ) /= Slope_22_Up_Floor_Thin then
                    -- follow the slope down and to the left
                    x1 := x;
                    y1 := y;
                    while mapInfo.Get_Shape( x1 - 1, y1 ) = Slope_22_Up_Floor_Thin loop
                        x1 := x1 - 2;
                        y1 := y1 + 1;
                        exit when mapInfo.Get_Shape( x1, y1 ) /= Slope_22_Up_Floor_Wide;
                    end loop;
                    --     /#
                    -- .  /##
                    --  \/###
                    --  /####
                    -- the left hand side of the slope faces up and to the left
                    -- (x1,y1) -> (x,y)
                    this.walls.Insert( Float((x1 + 1) * tileWidth), Float(y1 * tileWidth), Float((x + 1) * tileWidth), Float(y * tileWidth), 0 );
                elsif shape = Slope_22_Down_Floor_Wide and then mapInfo.Get_Shape( x - 1, y - 1 ) /= Slope_22_Down_Floor_Thin then
                    -- follow the slope down and to the right
                    x1 := x;
                    y1 := y;
                    while mapInfo.Get_Shape( x1 + 1, y1 ) = Slope_22_Down_Floor_Thin loop
                        x1 := x1 + 2;
                        y1 := y1 + 1;
                        exit when mapInfo.Get_Shape( x1, y1 ) /= Slope_22_Down_Floor_Wide;
                    end loop;
                    -- #\  .
                    -- ##\/
                    -- ###\
                    -- ####\
                    -- the left hand side of the slope faces up and to the right
                    -- (x,y) -> (x1,y1)
                    this.walls.Insert( Float(x * tileWidth), Float(y * tileWidth), Float(x1 * tileWidth), Float(y1 * tileWidth), 0 );
                elsif shape = Slope_22_Up_Ceiling_Thin and then mapInfo.Get_Shape( x + 1, y - 1) /= Slope_22_Up_Ceiling_Wide then
                    -- follow the slope down and to the left
                    x1 := x;
                    y1 := y;
                    while mapInfo.Get_Shape( x1 - 1, y1 ) = Slope_22_Up_Ceiling_Wide loop
                        x1 := x1 - 2;
                        y1 := y1 + 1;
                        exit when mapInfo.Get_Shape( x1, y1 ) /= Slope_22_Up_Ceiling_Thin;
                    end loop;
                    -- ####/
                    -- ###/
                    -- ##/\
                    -- #/  '
                    -- the left hand side of the slope faces down and to the right
                    -- (x,y) -> (x1,y1)
                    this.walls.Insert( Float((x + 1) * tileWidth), Float(y * tileWidth), Float((x1 + 1) * tileWidth), Float(y1 * tileWidth), 0 );
                elsif shape = Slope_22_Down_Ceiling_Thin and then mapInfo.Get_Shape( x - 1, y - 1 ) /= Slope_22_Down_Ceiling_Wide then
                    -- follow the slope down and to the right
                    x1 := x;
                    y1 := y;
                    while mapInfo.Get_Shape( x1 + 1, y1 ) = Slope_22_Down_Ceiling_Wide loop
                        x1 := x1 + 2;
                        y1 := y1 + 1;
                        exit when mapInfo.Get_Shape( x1, y1 ) /= Slope_22_Down_Ceiling_Thin;
                    end loop;
                    -- \####
                    --  \###
                    --  /\##
                    -- '  \#
                    -- the left hand side of the slope faces down and to the left
                    -- (x1,y1) -> (x,y)
                    this.walls.Insert( Float(x1 * tileWidth), Float(y1 * tileWidth), Float(x * tileWidth), Float(y * tileWidth), 0 );
                end if;
                x := x + 1;
            end loop;
        end loop;
    end Initialize;

    ----------------------------------------------------------------------------

    procedure Draw_Lights( this     : not null access Lighting_System'Class;
                           viewport : Rectangle ) is
        usingLightShader : Boolean := False;
        light            : A_Light_Source;
        i                : Integer := 1;
    begin
        -- draw Point_Lights
        while i <= Integer(this.lights.Length) loop
            light := this.lights.Element( i );
            exit when light.typ /= Point;
            if light.Intersects( viewport ) then

                -- switch to the light shader only when necessary
                if not usingLightShader then
                    usingLightShader := True;
                    this.view.Use_Shader( "light" );
                end if;

                light.Draw( viewport );
            end if;
            i := i + 1;
        end loop;

        -- revert to the default shader for area lights
        if usingLightShader then
            this.view.Use_Shader( "default" );
        end if;

        -- draw Area_Lights
        while i <= Integer(this.lights.Length) loop
            light := this.lights.Element( i );
            exit when light.typ /= Area;
            if light.Intersects( viewport ) then
                light.Draw( viewport );
            end if;
            i := i + 1;
        end loop;
    end Draw_Lights;

    ----------------------------------------------------------------------------

    procedure Enable_Option( this    : not null access Lighting_System'Class;
                             opt     : Render_Options;
                             enabled : Boolean := True ) is
    begin
        if enabled then
            this.options := this.options or opt;
        else
            this.options := this.options and (not opt);
        end if;
    end Enable_Option;

    ----------------------------------------------------------------------------

    function Get_Render_Options( this : not null access Lighting_System'Class ) return Render_Options is (this.options);

    ----------------------------------------------------------------------------

    function Is_Enabled( this : not null access Lighting_System'Class; opt : Render_Options ) return Boolean is ((this.options and opt) = opt);

    ----------------------------------------------------------------------------

    procedure Frame_Begin( this   : not null access Lighting_System'Class;
                           sceneW,
                           sceneH : Float ) is
    begin
        this.Set_Buffer_Size( sceneW, sceneH );
        this.w := sceneW;
        this.h := sceneH;

        -- capture the color buffer and blending state before drawing lighting
        Al_Store_State( this.alState, ALLEGRO_STATE_TARGET_BITMAP or ALLEGRO_STATE_BLENDER );

        -- initialize the accumulated light buffer
        Set_Target_Bitmap( this.lightAcc );

        -- set the lighting shader
        this.view.Use_Shader( "default" );

        -- enable writes to color and depth so the buffer can be cleared
        Al_Set_Render_State( ALLEGRO_WRITE_MASK, ALLEGRO_MASK_RGBA or ALLEGRO_MASK_DEPTH );

        -- clear the color to ambient
        Clear_To_Color( this.ambient );

        -- clear the depth buffer to infinite depth
        Al_Clear_Depth_Buffer( 1.0 );
    end Frame_Begin;

    ----------------------------------------------------------------------------

    procedure Frame_End( this          : not null access Lighting_System'Class;
                         x, y, z       : Float;
                         effectiveZoom : Float ) is
        width  : constant Float := this.w / effectiveZoom;
        height : constant Float := this.h / effectiveZoom;

        ------------------------------------------------------------------------

        procedure Draw_Quad( x, y, w, h : Float ) is
        begin
            Rect_XYWH( x, y, w, h, Orange1 );
        end Draw_Quad;
        pragma Warnings( Off, Draw_Quad );

        ------------------------------------------------------------------------

        procedure Draw_Wall( x1, y1, x2, y2 : Float; item : Integer ) is
            pragma Unreferenced( item );
            sx1 : Float := x1;
            sy1 : Float := y1;
            sx2 : Float := x2;
            sy2 : Float := y2;
        begin
            To_Target_Pixel_Center( sx1, sy1 );
            To_Target_Pixel_Center( sx2, sy2 );
            Line( sx1, sy1, sx2, sy2, Scarlet2, From_Target_Pixels( 1.0 ) );
        end Draw_Wall;

        ------------------------------------------------------------------------

        trans : Allegro_Transform;
    begin
        Al_Set_Render_State( ALLEGRO_WRITE_MASK, ALLEGRO_MASK_RGBA or ALLEGRO_MASK_DEPTH );

        if this.Is_Enabled( SHOW_LIGHT_BLUR ) then
            -- draw the lighting with horizontal blurring from lightAcc to a secondary buffer
            Al_Set_Target_Bitmap( this.litArea );
            Al_Identity_Transform( trans );
            Al_Use_Transform( trans );
            this.view.Use_Shader( "blur5" );
            Al_Set_Shader_Float( "radius", BLUR_RADIUS );
            Al_Set_Shader_Float( "resolution", this.w );
            Al_Set_Shader_Float_Vector( "dir", 2, (1.0, 0.0) );

            Draw_Bitmap( this.lightAcc, 0.0, 0.0, 0.0, White, Nonpremultiplied );

            -- draw the lighting with vertical blurring from a secondary buffer to the target
            Al_Restore_State( this.alState );
            this.view.Use_Shader( "blur5" );
            Al_Set_Shader_Float( "radius", BLUR_RADIUS );
            Al_Set_Shader_Float( "resolution", this.h );
            Al_Set_Shader_Float_Vector( "dir", 2, (0.0, 1.0) );
            Draw_Bitmap_Region_Stretched( this.litArea,
                                          0.0, 0.0, this.w, this.h,
                                          x, y, z,
                                          width, height,
                                          0.0,
                                          White,
                                         (if this.Is_Enabled( SHOW_LIGHT_BUFFER ) then Nonpremultiplied else Multiply) );
            this.view.Use_Shader( "default" );
        else
            -- draw the light accumulation buffer directly, without blurring
            Al_Restore_State( this.alState );
            Draw_Bitmap_Region_Stretched( this.lightAcc,
                                          0.0, 0.0, this.w, this.h,
                                          x, y, z,
                                          width, height,
                                          0.0,
                                          White,
                                         (if this.Is_Enabled( SHOW_LIGHT_BUFFER ) then Nonpremultiplied else Multiply) );
        end if;

        -- draw the debugging information at the correct depth
        Al_Store_State( this.alState, ALLEGRO_STATE_TRANSFORM );
        Al_Identity_Transform( trans );
        Al_Translate_Transform_3d( trans, 0.0, 0.0, z );
        Al_Compose_Transform( trans, Al_Get_Current_Transform.all );
        Al_Use_Transform( trans );

        -- draw the quad tree divisions (debugging)
        if this.Is_Enabled( SHOW_LIGHT_QTREE ) then
            this.walls.Iterate_Leaves( Draw_Quad'Access );
        end if;

        -- draw shadow-casting walls (debugging)
        if this.Is_Enabled( SHOW_SHADOW_GEOM ) then
            this.walls.Iterate( Draw_Wall'Access );
        end if;

        -- draw individual light geometry (debugging)
        if this.Is_Enabled( SHOW_LIGHT_GEOM ) then
            for light of this.lights loop
                if light.Intersects( Rectangle'(x, y, width, height) ) then
                    light.Draw_Debug;
                end if;
            end loop;
        end if;

        Al_Restore_State( this.alState );
    end Frame_End;

    ----------------------------------------------------------------------------

    procedure Freeze_Updates( this : not null access Lighting_System'Class; freeze : Boolean ) is
    begin
        this.frozen := freeze;
    end Freeze_Updates;

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Event( this : access Lighting_System;
                            evt  : in out A_Event;
                            resp : out Response_Type ) is
        pragma Unreferenced( resp );
    begin
        if this.frozen then
            return;
        end if;
        case evt.Get_Event_Id is
            when EVT_ENTITY_MOVED           => this.Handle_Entity_Moved( A_Entity_Moved_Event(evt) );
            when EVT_LIGHT_CREATED          => this.Handle_Light_Created( A_Light_Event(evt) );
            when EVT_LIGHT_UPDATED          => this.Handle_Light_Updated( A_Light_Event(evt) );
            when EVT_LIGHT_DELETED          => this.Handle_Light_Deleted( A_Light_Event(evt) );
            when EVT_WORLD_PROPERTY_CHANGED => this.Handle_World_Property_Changed( A_World_Property_Event(evt) );
            when others                     => null;
        end case;
    end Handle_Event;

    ----------------------------------------------------------------------------

    procedure Handle_Entity_Moved( this : not null access Lighting_System'Class;
                                   evt  : not null A_Entity_Moved_Event ) is
        dx : constant Float := evt.Get_X - evt.Get_Prev_X;
        dy : constant Float := evt.Get_Y - evt.Get_Prev_Y;
    begin
        for l of this.lights loop
            if l.entityId = evt.Get_Id then
                 l.x := l.x + dx;
                 l.y := l.y + dy;
            end if;
        end loop;
    end Handle_Entity_Moved;

    ----------------------------------------------------------------------------

    procedure Handle_Light_Created( this : not null access Lighting_System'Class;
                                    evt  : not null A_Light_Event ) is
        shape : constant String := evt.Get_Properties.Get_String( "shape" );
        light : A_Light_Source;
    begin
        if shape = "point" then
            light := new Point_Light'(Object with typ => Point, others => <>);
        elsif shape = "area" then
            light := new Area_Light'(Object with typ => Area, others => <>);
        else
            return;
        end if;

        light.Construct;
        light.system      := A_Lighting_System(this);
        light.componentId := evt.Get_Component_Id;
        light.entityId    := evt.Get_Entity_Id;
        light.x           := evt.Get_Properties.Get_Float( "x", light.x );
        light.y           := evt.Get_Properties.Get_Float( "y", light.y );
        light.z           := evt.Get_Properties.Get_Float( "z", light.z );
        light.shadows     := evt.Get_Properties.Get_Boolean( "shadows", light.shadows );
        light.color       := Opacity( Html_To_Color( evt.Get_Properties.Get_String( "color" ), White ),
                                      evt.Get_Properties.Get_Float( "intensity", 1.0 ) );
        light.diffuse     := evt.Get_Properties.Get_Float( "diffuse", light.diffuse );
        light.angle       := evt.Get_Properties.Get_Float( "angle", light.angle );

        case light.typ is
            when Point =>
                light.w := evt.Get_Properties.Get_Float( "radius", light.w / 2.0 ) * 2.0;
                light.h := light.w;
                light.arc := evt.Get_Properties.Get_Float( "arc", light.arc );
            when Area =>
                light.w := evt.Get_Properties.Get_Float( "width", light.w );
                light.h := evt.Get_Properties.Get_Float( "height", light.h );
        end case;

        -- keep lights ordered by Light_Type for draw speed
        for i in 1..Integer(this.lights.Length) loop
            if light.typ <= this.lights.Element( i ).typ then
                this.lights.Insert( i, light );
                light := null;
                exit;
            end if;
        end loop;
        if light /= null then
            this.lights.Append( light );
        end if;
    end Handle_Light_Created;

    ----------------------------------------------------------------------------

    procedure Handle_Light_Updated( this : not null access Lighting_System'Class;
                                    evt  : not null A_Light_Event ) is
        light : A_Light_Source;
    begin
        for i in 1..Integer(this.lights.Length) loop
            light := this.lights.Element( i );
            if light.componentId = evt.Get_Component_Id then
                declare
                    procedure Update_Property( name : String; val : Value ) is
                    begin
                        if name = "x" then
                            light.x := Cast_Float( val, light.x );
                        elsif name = "y" then
                            light.y := Cast_Float( val, light.y );
                        elsif name = "width" then
                            light.w := Cast_Float( val, light.w );
                        elsif name = "height" then
                            light.h := Cast_Float( val, light.h );
                        elsif name = "radius" then
                            light.w := Cast_Float( val, light.w / 2.0 ) * 2.0;
                            light.h := light.w;
                        elsif name = "angle" then
                            light.angle := Cast_Float( val, light.angle );
                        elsif name = "arc" then
                            light.arc := Cast_Float( val, light.arc );
                        elsif name = "color" then
                            light.color := Opacity( Html_To_Color( Cast_String( val ), light.color ),
                                                    Get_Alpha( light.color ) );
                        elsif name = "intensity" then
                            light.color := Opacity( light.color, Cast_Float( val, Get_Alpha( light.color ) ) );
                        elsif name = "diffuse" then
                            light.diffuse := Cast_Float( val, light.diffuse );
                        elsif name = "z" then
                            light.z := Cast_Float( val, light.z );
                        elsif name = "shadows" then
                            light.shadows := Cast_Boolean( val, light.shadows );
                        end if;
                    end Update_Property;
                begin
                    evt.Get_Properties.Iterate( Update_Property'Access );
                end;
                exit;
            end if;
        end loop;
    end Handle_Light_Updated;

    ----------------------------------------------------------------------------

    procedure Handle_Light_Deleted( this : not null access Lighting_System'Class;
                                    evt  : not null A_Light_Event ) is
        light : A_Light_Source;
    begin
        for i in 1..Integer(this.lights.Length) loop
            light := this.lights.Element( i );
            if light.componentId = evt.Get_Component_Id then
                Delete( light );
                this.lights.Delete( i );
                exit;
            end if;
        end loop;
    end Handle_Light_Deleted;

    ----------------------------------------------------------------------------

    procedure Handle_World_Property_Changed( this : not null access Lighting_System'Class;
                                             evt  : not null A_World_Property_Event ) is
    begin
        if evt.Get_Property_Name = "ambientLight" then
            this.ambient := Opacity( Html_To_Color( Cast_String( evt.Get_Value, "#FFFFFF" ), White ), 1.0 );
        end if;
    end Handle_World_Property_Changed;

    ----------------------------------------------------------------------------

    procedure Insert_Horizontal( this   : not null access Lighting_System'Class;
                                 x1, y1 : Float;
                                 x2, y2 : Float;
                                 faceUp : Boolean ) is
        leftX  : Float := x1;
        leftY  : Float := y1;
        rightX : Float := x2;
        rightY : Float := y2;
    begin
        if rightX < leftX then
            leftX := x2;
            leftY := y2;
            rightX := x1;
            rightY := y1;
        end if;
        if faceUp then
            -- the wall runs left to right, left hand side facing up
            this.walls.Insert( leftX, leftY, rightX, rightY, item => 0 );
        else
            -- the wall runs right to left, left hand side facing down
            this.walls.Insert( rightX, rightY, leftX, leftY, item => 0 );
        end if;
    end Insert_Horizontal;

    ----------------------------------------------------------------------------

    procedure Insert_Vertical( this      : not null access Lighting_System'Class;
                               x         : Float;
                               y1, y2    : Float;
                               faceRight : Boolean ) is
    begin
        if faceRight then
            -- the wall runs top to bottom, left hand side facing right
            this.walls.Insert( x, Float'Min( y1, y2 ), x, Float'Max( y1, y2 ), item => 0 );
        else
            -- the wall runs bottom to top, left hand side facing left
            this.walls.Insert( x, Float'Max( y1, y2 ), x, Float'Min( y1, y2 ), item => 0 );
        end if;
    end Insert_Vertical;

    ----------------------------------------------------------------------------

    function Is_Frozen( this : not null access Lighting_System'Class ) return Boolean is (this.frozen);

    ----------------------------------------------------------------------------

    procedure Pass_Begin( this : not null access Lighting_System'Class; pass : Render_Pass ) is
        pragma Unreferenced( this );
    begin
        if pass = Render_Shading then
            -- draw depth values to the buffer, cull by depth. no color is drawn.
            Set_Blend_Mode( Nonpremultiplied );
            Al_Set_Render_State( ALLEGRO_WRITE_MASK, ALLEGRO_MASK_DEPTH );
        elsif pass = Render_Light then
            -- draw additive light to the buffer, culled by depth, without
            -- updating the existing depth values
            Set_Blend_Mode( Additive );
            Al_Set_Render_State( ALLEGRO_WRITE_MASK, ALLEGRO_MASK_RGBA );
        end if;
    end Pass_Begin;

    ----------------------------------------------------------------------------

    procedure Set_Buffer_Size( this : not null access Lighting_System'Class;
                               w, h : Float ) is
        newW,
        newH  : Natural;
        state : Allegro_State;
    begin
        if this.lightAcc = null or else
           Al_Get_Bitmap_Width( this.lightAcc ) < Integer(Float'Ceiling( w )) or else
           Al_Get_Bitmap_Height( this.lightAcc ) < Integer(Float'Ceiling( h ))
        then
            newW := Integer(Float'Ceiling( w ));
            newH := Integer(Float'Ceiling( h ));
            if this.lightAcc /= null then
                newW := Integer'Max( newW, Al_Get_Bitmap_Width( this.lightAcc ) );
                newH := Integer'Max( newH, Al_Get_Bitmap_Height( this.lightAcc ) );
            end if;

            Al_Destroy_Bitmap( this.lightAcc );
            Al_Destroy_Bitmap( this.litArea );

            Al_Store_State( state, ALLEGRO_STATE_BITMAP );

            -- linear filtering is used on the light map buffers to improve the
            -- smoothness of gaussian blur. For details, see the article at
            -- http://rastergrid.com/blog/2010/09/efficient-gaussian-blur-with-linear-sampling
            Al_Add_New_Bitmap_Flag( ALLEGRO_VIDEO_BITMAP or
                                    ALLEGRO_MIN_LINEAR or
                                    ALLEGRO_NO_PRESERVE_TEXTURE );

            Al_Set_New_Bitmap_Samples( Al_Get_Display_Option( this.view.Get_Display, ALLEGRO_SAMPLES ) );
            Al_Set_New_Bitmap_Depth( Al_Get_Display_Option( this.view.Get_Display, ALLEGRO_DEPTH_SIZE ) );
            this.lightAcc := Al_Create_Bitmap( newW, newH );
            Al_Set_New_Bitmap_Depth( 0 );

            this.litArea := Al_Create_Bitmap( newW, newH );
            Al_Set_New_Bitmap_Samples( 0 );

            Al_Restore_State( state );
        end if;
    end Set_Buffer_Size;

    ----------------------------------------------------------------------------

    overriding
    procedure Tick( this : access Lighting_System; time : Tick_Time ) is null;

    ----------------------------------------------------------------------------

    procedure Update_Geometry( this      : not null access Lighting_System'Class;
                               mapInfo   : A_Map_Info_Cache;
                               tileWidth : Positive;
                               col, row  : Integer ) is
        shape  : constant Clip_Type := mapInfo.Get_Shape( col, row );
        top    : constant Boolean := Solid_Top( mapInfo.Get_Shape( col, row ) ) xor Solid_Bottom( mapInfo.Get_Shape( col, row - 1 ) );
        bottom : constant Boolean := Solid_Bottom( mapInfo.Get_Shape( col, row ) ) xor Solid_Top( mapInfo.Get_Shape( col, row + 1 ) );
        left   : constant Boolean := Solid_Left( mapInfo.Get_Shape( col, row ) ) xor Solid_Right( mapInfo.Get_Shape( col - 1, row ) );
        right  : constant Boolean := Solid_Right( mapInfo.Get_Shape( col, row ) ) xor Solid_Left( mapInfo.Get_Shape( col + 1, row ) );
        slope  : Float;
    begin
        -- test for top and bottom walls and remove any slope
        declare
            hasTop    : Boolean := False;
            hasBottom : Boolean := False;

            -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

            procedure Examine_Horizontal_Wall( x1, y1, x2, y2 : Float; item : Integer ) is
                leftX  : Float := x1;
                leftY  : Float := y1;
                rightX : Float := x2;
                rightY : Float := y2;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

                procedure Remove_Horizontal_Wall( wallY : Float ) is
                begin
                    this.walls.Remove( x1, y1, x2, y2 );

                    if leftX = Float(col * tileWidth) then
                        if rightX > Float((col + 1) * tileWidth) then
                            -- replace with a shorter wall
                            this.Insert_Horizontal( Float((col + 1) * tileWidth), wallY, rightX, wallY, faceUp => x1 < x2 );
                        end if;
                    else -- leftX < Float(col * tileWidth)
                        if rightX = Float((col + 1) * tileWidth) then
                            -- replace with a shorter wall
                            this.Insert_Horizontal( leftX, wallY, Float(col * tileWidth), wallY, faceUp => x1 < x2 );
                        else
                            -- bisect the wall
                            -- 1. create a shorter wall to the left
                            this.Insert_Horizontal( leftX, wallY, Float(col * tileWidth), wallY, faceUp => x1 < x2 );
                            -- 2. create a shorter wall to the right
                            this.Insert_Horizontal( Float((col + 1) * tileWidth), wallY, rightX, wallY, faceUp => x1 < x2 );
                        end if;
                    end if;
                end Remove_Horizontal_Wall;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

                pragma Unreferenced( item );
            begin
                if x2 < x1 then
                    leftX := x2;
                    leftY := y2;
                    rightX := x1;
                    rightY := y1;
                end if;

                if y1 = y2 then
                    if y1 <= Float(row * tileWidth) then
                        -- found a top wall
                        hasTop := True;
                        if not top then
                            Remove_Horizontal_Wall( y1 );
                        end if;
                    elsif y1 >= Float((row + 1) * tileWidth) then
                        -- found a bottom wall
                        hasBottom := True;
                        if not bottom then
                            Remove_Horizontal_Wall( y2 );
                        end if;
                    end if;
                else
                    -- intersected a slope; remove it
                    -- we will add the new correct slope at the end
                    this.walls.Remove( x1, y1, x2, y2 );
                    slope := (rightY - leftY) / (rightX - leftX);

                    if leftX = Float(col * tileWidth) then
                        if rightX > Float((col + 1) * tileWidth) then
                            -- create a shorter slope to the right
                            this.Insert_Horizontal( Float((col + 1) * tileWidth), leftY + slope * (Float((col + 1) * tileWidth) - leftX),
                                                    rightX, rightY,
                                                    faceUp => x1 < x2 );
                        end if;
                    else -- leftX < Float(col * tileWidth)
                        if rightX = Float((col + 1) * tileWidth) then
                            -- create a shorter slope to the left
                            this.Insert_Horizontal( leftX, leftY,
                                                    Float(col * tileWidth), leftY + slope * (Float(col * tileWidth) - leftX),
                                                    faceUp => x1 < x2 );
                        else
                            -- bisect the wall
                            -- 1. create a shorter slope to the left
                            this.Insert_Horizontal( leftX, leftY,
                                                    Float(col * tileWidth), leftY + slope * (Float(col * tileWidth) - leftX),
                                                    faceUp => x1 < x2 );
                            -- 2. create a shorter slope to the right
                            this.Insert_Horizontal( Float((col + 1) * tileWidth), leftY + slope * (Float((col + 1) * tileWidth) - leftX),
                                                    rightX, rightY,
                                                    faceUp => x1 < x2 );
                        end if;
                    end if;
                end if;
            end Examine_Horizontal_Wall;

            -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        begin
            --  |     |
            --  |  |  |
            --  +-----+  <- top
            --  |  |  |
            --  |  |  |
            --  +-----+  <- bottom
            --  |  |  |
            --  |  ^ check walls intersecting this segment
            this.walls.Intersecting_Segment( Float(col * tileWidth + tileWidth / 2), Float(row * tileWidth - 1),
                                             Float(col * tileWidth + tileWidth / 2), Float(row * tileWidth + tileWidth + 1),
                                             Examine_Horizontal_Wall'Access );
            if top and not hasTop then
                -- a top wall is needed
                -- enhancement: extend an existing wall from the right or left
                this.Insert_Horizontal( Float(col * tileWidth),       Float(row * tileWidth),
                                        Float((col + 1) * tileWidth), Float(row * tileWidth),
                                        faceUp => Solid_Top( mapInfo.Get_Shape( col, row ) ) );
            end if;
            if bottom and not hasBottom then
                -- a bottom wall is needed
                -- enhancement: extend an existing wall from the right or left
                this.Insert_Horizontal( Float(col * tileWidth),       Float((row + 1) * tileWidth),
                                        Float((col + 1) * tileWidth), Float((row + 1) * tileWidth),
                                        faceUp => not Solid_Bottom( mapInfo.Get_Shape( col, row ) ) );
            end if;
        end;

        -- test for left and right walls
        declare
            hasLeft  : Boolean := False;
            hasRight : Boolean := False;

            -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

            procedure Examine_Vertical_Wall( x1, y1, x2, y2 : Float; item : Integer ) is
                pragma Unreferenced( item );
                topY : Float := y1;
                botY : Float := y2;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

                procedure Remove_Vertical_Wall( wallX : Float ) is
                begin
                    this.walls.Remove( x1, y1, x2, y2 );

                    if topY = Float(row * tileWidth) then
                        if botY > Float((row + 1) * tileWidth) then
                            -- replace with a shorter wall
                            this.Insert_Vertical( x => wallX, y1 => Float((row + 1) * tileWidth), y2 => botY, faceRight => y1 < y2 );
                        end if;
                    else -- topY < Float(row * tileWidth)
                        if botY = Float((row + 1) * tileWidth) then
                            -- replace with a shorter wall
                            this.Insert_Vertical( x => wallX, y1 => topY, y2 => Float(row * tileWidth), faceRight => y1 < y2 );
                        else
                            -- bisect the wall
                            -- 1. create a shorter wall above
                            this.Insert_Vertical( x => wallX, y1 => topY, y2 => Float(row * tileWidth), faceRight => y1 < y2 );
                            -- 2. create a shorter wall below
                            this.Insert_Vertical( x => wallX, y1 => Float((row + 1) * tileWidth), y2 => botY, faceRight => y1 < y2 );
                        end if;
                    end if;
                end Remove_Vertical_Wall;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

            begin
                if y2 < y1 then
                    topY := y2;
                    botY := y1;
                end if;

                if x1 <= Float(col * tileWidth) then
                    -- found a left wall
                    hasLeft := True;
                    if not left then
                        Remove_Vertical_Wall( x1 );
                    end if;
                elsif x1 >= Float((col + 1) * tileWidth) then
                    -- found a right wall
                    hasRight := True;
                     if not right then
                        Remove_Vertical_Wall( x2 );
                    end if;
                end if;
            end Examine_Vertical_Wall;

            -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        begin
            --  ----+----+----
            --      |    |
            --    --|----|--    <-- check walls intersecting this segment
            --      |    |
            --  ----+----+----
            -- left ^    ^ right
            this.walls.Intersecting_Segment( Float(col * tileWidth - 1),             Float(row * tileWidth + tileWidth / 2),
                                             Float(col * tileWidth + tileWidth + 1), Float(row * tileWidth + tileWidth / 2),
                                             Examine_Vertical_Wall'Access );
            if left and not hasLeft then
                -- a left wall is needed
                -- enhancement todo: extend an existing wall from the bottom or top
                this.Insert_Vertical( x => Float(col * tileWidth),
                                      y1 => Float(row * tileWidth), y2 => Float((row + 1) * tileWidth),
                                      faceRight => not Solid_Left( mapInfo.Get_Shape( col, row ) ) );
            end if;
            if right and not hasRight then
                -- a right wall is needed
                -- enhancement todo: extend an existing wall from the bottom or top
                this.Insert_Vertical( x => Float((col + 1) * tileWidth),
                                      y1 => Float(row * tileWidth), y2 => Float((row + 1) * tileWidth),
                                      faceRight => Solid_Right( mapInfo.Get_Shape( col, row ) ) );
            end if;
        end;

        -- create a slope wall, if necessary
        if Slope_Left_Y( shape, tileWidth ) /= Slope_Right_Y( shape, tileWidth ) then
            this.Insert_Horizontal( Float(col * tileWidth), Float(row * tileWidth + Slope_Left_Y( shape, tileWidth )),
                                    Float((col + 1) * tileWidth), Float(row * tileWidth + Slope_Right_Y( shape, tileWidth )),
                                    faceUp => Solid_Bottom( shape ) );
        end if;
    end Update_Geometry;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Lighting_System ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

end Lighting_Systems;
