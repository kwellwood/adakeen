--
-- Copyright (c) 2015-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Real_Time;                     use Ada.Real_Time;
with Allegro.Color;                     use Allegro.Color;
with Icons;                             use Icons;
with Object_Ids;                        use Object_Ids;
with Palette;                           use Palette;
with Values.Tagged_Ids;                 use Values.Tagged_Ids;

package Particles is

private

    type Particle is
        record
            age      : Time_Span := Time_Span_Zero;    -- current age
            lifespan : Time_Span := Time_Span_Zero;    -- total lifespan
            x, y     : Float := 0.0;        -- current location
            vx, vy   : Float := 0.0;        -- velocity
            gravity  : Float := 0.0;        -- gravitational acceleration
            radial   : Float := 0.0;        -- radial acceleration
            tangent  : Float := 0.0;        -- tangential acceleration
            vTangent : Float := 0.0;        -- tangential velocity
            cx, cy   : Float := 0.0;        -- center x,y for radial and tangent movement
            rot      : Float := 0.0;        -- current rotation
            vRot     : Float := 0.0;        -- current rotational velocity
            size     : Float := 0.0;        -- size (width or height)
            size1    : Float := 0.0;        -- initial size
            size2    : Float := 0.0;        -- final size
            color    : Allegro_Color;       -- current color
            color1   : Allegro_Color := White;      -- initial color (untinted)
            color2   : Allegro_Color := No_Color;   -- final color
            light    : Float := 0.0;        -- emissive light brightness (0.0 - 1.0)
            light1   : Float := 0.0;        -- initial emissive light brightness
            light2   : Float :=-1.0;        -- final emissive light brightness (<0 = use light1)
            texture  : Icon_Type;           -- bitmap to draw
            additive : Boolean := True;     -- use additive blending?
        end record;

    ----------------------------------------------------------------------------

    type Emitter is
        record
            -- Emitter fields
            componentId : Component_Id := NULL_ID;      -- originating component
            entityId    : Entity_Id := NULL_ID;         -- attached to entity
            z           : Float := 0.0;                 -- Z depth of the emitter
            rate        : Integer := 0;                 -- spawn rate (particles / sec)
            life        : Time_Span := Time_Span_Zero;  -- remaining life of the emitter
            age         : Time_Span := Time_Span_Zero;  -- age of the emitter

            -- Particle generation fields
            lifespanVar : Time_Span := Time_Span_Zero;  -- particle lifespan variation
            radius      : Float := 0.0;                 -- spawn distance from origin
            radiusVar   : Float := 0.0;                 -- spawn distance variation
            xVar, yVar  : Float := 0.0;                 -- spawn position variation
            angle       : Float := 0.0;                 -- initial angle of movement
            angleVar    : Float := 0.0;                 -- angle of movement variation
            speed       : Float := 0.0;                 -- initial speed of movement
            speedVar    : Float := 0.0;                 -- movement speed variation
            radialVar   : Float := 0.0;                 -- radial acceleration variation
            tangentVar  : Float := 0.0;                 -- tangential acceleration variation
            vTangentVar : Float := 0.0;                 -- tangential velocity variation
            rotVar      : Float := 0.0;                 -- rotation variation
            vRotVar     : Float := 0.0;                 -- rotational velocity variation
            size1Var    : Float := 0.0;                 -- initial size variation
            size2Var    : Float := 0.0;                 -- final size variation
            color1Var   : Allegro_Color;                -- initial color variation
            color2Var   : Allegro_Color;                -- final color variation
            light1Var   : Float := 0.0;                 -- initial brightness variation
            light2Var   : Float := 0.0;                 -- final brightness variation

            -- Initial particle state
            prototype   : Particle;
        end record;
    type A_Emitter is access all Emitter;

end Particles;
