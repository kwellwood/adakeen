--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Real_Time;                     use Ada.Real_Time;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Allegro.Color;                     use Allegro.Color;
with Animated_Properties;               use Animated_Properties;
with Easing;                            use Easing;
with Events;                            use Events;
with Events.Listeners;                  use Events.Listeners;
with Mouse;                             use Mouse;
with Objects;                           use Objects;
with Palette;                           use Palette;
with Processes;                         use Processes;
with Signals;                           use Signals;
with Signals.Gamepads;                  use Signals.Gamepads;
with Signals.Keys;                      use Signals.Keys;
with Signals.Mouse;                     use Signals.Mouse;
with Signals.Simple;                    use Signals.Simple;
with Values;                            use Values;
with Values.Maps;                       use Values.Maps;
with Vector_Math;                       use Vector_Math;
with Widget_Styles;                     use Widget_Styles;

limited with Game_Views;

private with Ada.Containers.Indefinite_Doubly_Linked_Lists;

limited private with Widgets.Windows;

package Widgets is

    pragma Warnings( Off, Allegro.Color );  -- not actually used here but used
    pragma Warnings( Off, Palette );        -- by almost all widget subclasses

    type Axis_Type is mod 8;

    X_AXIS    : constant Axis_Type := 2#001#;
    Y_AXIS    : constant Axis_Type := 2#010#;
    XY_AXES   : constant Axis_Type := X_AXIS or Y_AXIS;
    ZOOM_AXES : constant Axis_Type := 2#100# or XY_AXES;

    ----------------------------------------------------------------------------

    -- Widget subclasses implement the Animated interface to automatically run
    -- as a Process, when showing on the screen. Animated extends Process, so
    -- the implementor must have a Tick procedure which will be automatically
    -- called. The widget subclass does not need to attach itself to a
    -- Process_Manager; it is handled automatically.
    type Animated is limited interface and Process;

    ----------------------------------------------------------------------------

    type Anchor_Type is (Horizontal, Vertical, Center);

    type Anchor(typ : Anchor_Type) is tagged limited private;
    type A_Anchor is access all Anchor'Class;

    -- Attaches the anchor to another anchor 'target'. This anchor's widget will
    -- follow changes in size or position of the target anchor's widget. Setting
    -- 'margin' will offset this anchor's position from the target anchor.
    procedure Attach( this   : not null access Anchor'Class;
                      target : access Anchor'Class;
                      margin : Float := 0.0 );

    -- Disconnects the anchor's attachment.
    procedure Detach( this : not null access Anchor'Class );

    -- Returns the margin of this anchor from its attached anchor, if any.
    function Get_Margin( this : not null access Anchor'Class ) return Float;

    -- Returns True if the anchor is attached to another anchor.
    function Is_Attached( this : not null access Anchor'Class ) return Boolean;

    -- Sets the margin of this anchor from its attached anchor, if any.
    procedure Set_Margin( this : not null access Anchor'Class; margin : Float );

    ----------------------------------------------------------------------------

    -- A Widget is the most basic component of the GUI. A widget represents a
    -- control element, display element, or container in the interface. Widgets
    -- are organized as a tree, ultimately rooted at a Window widget, with each
    -- widget having one parent. Widgets communicate via a signals/slots based
    -- model, where signals are emitted to connected slots on that are listening
    -- for changes.
    type Widget is abstract new Limited_Object and Event_Listener with private;
    type A_Widget is access all Widget'Class;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Widget Signals

    function AttributeChanged( this : not null access Widget'Class ) return access String_Signal'Class;
    function Blurred( this : not null access Widget'Class ) return access Signal'Class;
    function Deleted( this : not null access Widget'Class ) return access Signal'Class;
    function Disabled( this : not null access Widget'Class ) return access Signal'Class;
    function Enabled( this : not null access Widget'Class ) return access Signal'Class;
    function Focused( this : not null access Widget'Class ) return access Signal'Class;
    function GamepadMoved( this : not null access Widget'Class ) return access Gamepad_Axis_Signal'Class;
    function GamepadPressed( this : not null access Widget'Class ) return access Gamepad_Button_Signal'Class;
    function GamepadReleased( this : not null access Widget'Class ) return access Gamepad_Button_Signal'Class;
    function Hidden( this : not null access Widget'Class ) return access Signal'Class;
    function KeyPressed( this : not null access Widget'Class ) return access Key_Signal'Class;
    function KeyReleased( this : not null access Widget'Class ) return access Key_Signal'Class;
    function KeyTyped( this : not null access Widget'Class ) return access Key_Signal'Class;
    function MouseClicked( this : not null access Widget'Class ) return access Button_Signal'Class;
    function MouseDoubleClicked( this : not null access Widget'Class ) return access Button_Signal'Class;
    function MouseEntered( this : not null access Widget'Class ) return access Signal'Class;
    function MouseExited( this : not null access Widget'Class ) return access Signal'Class;
    function MouseHeld( this : not null access Widget'Class ) return access Button_Signal'Class;
    function MouseMoved( this : not null access Widget'Class ) return access Mouse_Signal'Class;
    function MousePressed( this : not null access Widget'Class ) return access Button_Signal'Class;
    function MouseReleased( this : not null access Widget'Class ) return access Button_Signal'Class;
    function MouseScrolled( this : not null access Widget'Class ) return access Scroll_Signal'Class;
    function ParentChanged( this : not null access Widget'Class ) return access Signal'Class;
    function Resized( this : not null access Widget'Class ) return access Signal'Class;
    function Rooted( this : not null access Widget'Class ) return access Signal'Class;
    function Shown( this : not null access Widget'Class ) return access Signal'Class;
    function StyleChanged( this : not null access Widget'Class ) return access Signal'Class;
    function Unrooted( this : not null access Widget'Class ) return access Signal'Class;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Properties

    -- Sets whether the widget is visible within its parent. The visibility can
    -- be changed over a period of time using an easing function. An impulse
    -- style easing is recommended.
    function Visible( this : not null access Widget'Class ) return access Animated_Boolean.Property'Class;

    -- Sets the width or height of the widget within its parent's content area.
    -- The change in size can be animated over time, using an easing function.
    --
    -- Reading the width/height will return the widget's size within its parent's
    -- content area, either determined by anchors, or previously set with these
    -- properties.
    --
    -- Reading the size is undefined if the widget is not rooted inside the
    -- window's widget tree and it has not been previously set.
    function Height( this : not null access Widget'Class ) return access Animated_Float.Property'Class;
    function Width( this : not null access Widget'Class ) return access Animated_Float.Property'Class;

    -- Reads or sets the X/Y position of the widget within its parent's content
    -- area. The change in position can be animated over time, using an easing
    -- function.
    --
    -- Setting the position is incompatible with attaching the Left, Top, or
    -- Center anchors. If an X or Y position is set, it can be cleared with
    -- Unset_X() or Unset_Y().
    --
    -- Reading the position is undefined if the widget is not rooted inside the
    -- window's widget tree and it has not been previously set.
    function X( this : not null access Widget'Class ) return access Animated_Float.Property'Class;
    function Y( this : not null access Widget'Class ) return access Animated_Float.Property'Class;

    -- Reads or sets the content zoom (scaling) factor. Values > 1 scale the
    -- contents larger within the viewport (decreasing the visible content area),
    -- and values < 1 scale the contents smaller. The zoom factor does not
    -- affect the widget's viewport size within its container.
    function Zoom( this : not null access Widget'Class ) return access Animated_Float.Property'Class;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Anchors and Positioning

    function Left( this : not null access Widget'Class ) return access Anchor'Class;
    function Right( this : not null access Widget'Class ) return access Anchor'Class;
    function Top( this : not null access Widget'Class ) return access Anchor'Class;
    function Bottom( this : not null access Widget'Class ) return access Anchor'Class;
    function Horizontal_Center( this : not null access Widget'Class ) return access Anchor'Class;
    function Vertical_Center( this : not null access Widget'Class ) return access Anchor'Class;

    -- Helper procedure to center the widget within the area of 'target'. If
    -- 'width' or 'height' is greater than 0, it will be used. Otherwise, the
    -- widget's existing dimensions will be used. The target must be a parent or
    -- sibling of this widget. If 'target' is null, nothing will change.
    procedure Center( this   : not null access Widget'Class;
                      target : access Widget'Class;
                      width  : Float := 0.0;
                      height : Float := 0.0 );

    -- Helper procedure to horizontally center the widget within the width of
    -- 'target'. If 'width' is greater than 0, it will be used. Otherwise, the
    -- widget's existing width will be used. The target must be a parent or
    -- sibling of this widget. If 'target' is null, nothing will change.
    procedure Center_Horizontal( this   : not null access Widget'Class;
                                 target : access Widget'Class;
                                 width  : Float := 0.0 );

    -- Helper procedure to vertically center the widget within the height of
    -- 'target'. If 'height' is greater than 0, it will be used. Otherwise, the
    -- widget's existing height will be used. The target must be a parent or
    -- sibling of this widget. If 'target' is null, nothing will change.
    procedure Center_Vertical( this   : not null access Widget'Class;
                               target : access Widget'Class;
                               height : Float := 0.0 );

    -- Helper procedure to attach all of the widget's edge anchors to the
    -- corresponding anchors of 'target'. All other positioning (e.g. x, y) will
    -- be cleared. The target must be a parent or sibling of this widget.  If
    -- 'target' is null, nothing will change.
    procedure Fill( this         : not null access Widget'Class;
                    target       : access Widget'Class;
                    leftMargin   : Float := 0.0;
                    topMargin    : Float := 0.0;
                    rightMargin  : Float := 0.0;
                    bottomMargin : Float := 0.0 );

    -- Helper procedure to attach all of the widget's left and right anchors to
    -- the corresponding anchors of 'target'. The target must be a parent or
    -- sibling of this widget. If 'target' is null, nothing will change.
    procedure Fill_Horizontal( this        : not null access Widget'Class;
                               target      : access Widget'Class;
                               leftMargin  : Float := 0.0;
                               rightMargin : Float := 0.0 );

    -- Helper procedure to attach all of the widget's left and right anchors to
    -- the corresponding anchors of 'target'. The target must be a parent or
    -- sibling of this widget. If 'target' is null, nothing will change.
    procedure Fill_Vertical( this         : not null access Widget'Class;
                             target       : access Widget'Class;
                             topMargin    : Float := 0.0;
                             bottomMargin : Float := 0.0 );

    -- Returns the calculated geometry of the widget, describing its size and
    -- position relative to its parent. The geometry is updated by calls to
    -- Update_Geometry.
    function Get_Geometry( this : not null access Widget'Class ) return Rectangle;

    -- Returns a hint indicating the minimum width or height required to display
    -- the widget's content. This is implemented by subclasses.
    function Get_Min_Width( this : access Widget ) return Float;
    function Get_Min_Height( this : access Widget ) return Float;

    -- Returns a hint indicating the preferred width or height for displaying
    -- the widget's content. This hint can be set explicitly by calling
    -- Set_Preferred_Width() and Set_Preferred_Height(). If no preferred size is
    -- set, a layout widget will fall back to calculating the preferred size of
    -- its children, and otherwise returning its minimum size hint. The
    -- preferred size hint of a widget does not depend on its actual size.
    function Get_Preferred_Width( this : access Widget ) return Float;
    function Get_Preferred_Height( this : access Widget ) return Float;

    -- Returns the calculated viewport of the widget, describing the size and
    -- position of the visible area relative to the widget's own content area.
    function Get_Viewport( this : not null access Widget'Class ) return Rectangle;

    -- Sets the widget's preferred width or height hint.
    procedure Set_Preferred_Width( this : not null access Widget'Class; width : Float );
    procedure Set_Preferred_Height( this : not null access Widget'Class; height : Float );

    -- Cancels out any preferred width or height hint previously set with
    -- Set_Preferred_Width() or Set_Preferred_Height().
    procedure Unset_Preferred_Width( this : not null access Widget'Class );
    procedure Unset_Preferred_Height( this : not null access Widget'Class );

    -- Cancels out any width or height previously set with Set_Width() or Set_Height().
    procedure Unset_Width( this : not null access Widget'Class );
    procedure Unset_Height( this : not null access Widget'Class );

    -- Cancels out any X or Y previously set with X.Set() or Y.Set().
    procedure Unset_X( this : not null access Widget'Class );
    procedure Unset_Y( this : not null access Widget'Class );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Returns True if the widget type can accept focus in general.
    -- Call Is_Focusable() to check if the widget can receive focus right now.
    -- It will not accept focus if it's disabled or hidden.
    function Accepts_Focus( this : not null access Widget'Class ) return Boolean;

    -- Draws the widget and its children.
    procedure Draw( this : access Widget );

    -- Returns the value of attribute 'name'. If the attribute is not defined,
    -- Null will be returned. The returned value is not a copy; the caller is
    -- responsible for copying it as necessary.
    function Get_Attribute( this : not null access Widget'Class;
                            name : String ) return Value;

    -- Returns the map of all attributes (not a copy). If the widget has no
    -- attributes, an empty map will be returned.
    function Get_Attributes( this : not null access Widget'Class ) return Map_Value;
    pragma Postcondition( Get_Attributes'Result.Valid );

    -- Returns the location of the center of the widget within its container.
    function Get_Center_X( this : not null access Widget'Class ) return Float;
    function Get_Center_Y( this : not null access Widget'Class ) return Float;

    -- Returns the unique id of the widget
    function Get_Id( this : not null access Widget'Class ) return String;
    pragma Postcondition( Get_Id'Result'Length > 0 );

    -- Returns the name of the mouse cursor to show when the mouse is hovering
    -- over the widget.
    function Get_Mouse_Cursor( this : not null access Widget'Class ) return String;

    -- Returns the widget's parent, or null if it does not have a parent. A
    -- widget is owned by its parent.
    function Get_Parent( this : not null access Widget'Class ) return A_Widget;

    -- Returns a pointer to the Game_View controlling this widget. This is given
    -- at construction; the widget does not necessarily have a parent.
    function Get_View( this : not null access Widget'Class ) return access Game_Views.Game_View'Class;

    -- Returns the z depth of the widget relative to its parent.
    function Get_Z( this : not null access Widget'Class ) return Float;

    -- Returns True if the widget (and all its ancestors) are enabled. If False
    -- is returned, this widget or one of its ancestors is explicitly disabled.
    function Is_Enabled( this : not null access Widget'Class ) return Boolean;

    -- Returns True if the widget accepts input focus.
    function Is_Focusable( this : not null access Widget'Class ) return Boolean;

    -- Returns True if the widget has keyboard focus.
    function Is_Focused( this : not null access Widget'Class ) return Boolean;

     -- Returns True if the mouse is hovering over the widget.
    function Is_Hovered( this : not null access Widget'Class ) return Boolean;

    -- Returns True if the widget is showing on the screen. This means it is
    -- visible and its parent is also showing.
    function Is_Showing( this : not null access Widget'Class ) return Boolean;

    -- Moves the widget a relative distance within its container. The widget
    -- cannot be anchored in an axis that is being moved (ex: if 'dx' /= 0.0
    -- then the left, right, and centerX anchors cannot be in use. If 'dy' = 0.0
    -- then top, bottom, or centerY anchors can still be used.)
    procedure Move( this     : not null access Widget'Class;
                    dx, dy   : Float;
                    duration : Time_Span := Time_Span_Zero;
                    easing   : Easing_Type := Linear );

    -- Moves the widget to an absolute location with its container. The top left
    -- of the widget will be positioned at 'x,y'. The widget cannot be anchored.
    procedure Move_To( this     : not null access Widget'Class;
                       x, y     : Float;
                       duration : Time_Span := Time_Span_Zero;
                       easing   : Easing_Type := Linear );

    -- Requests focus for the widget from the window. If the widget is not
    -- rooted (within a window), the request will be ignored. If the widget does
    -- not accept focus, the next possible candidate will receive focus. See
    -- Window.Give_Focus() for details.
    procedure Request_Focus( this : not null access Widget'Class );

    -- Scrolls the widget's viewport by a relative distance 'dx,dy'. The scroll
    -- distance is in widget content coordinates.
    procedure Scroll( this : not null access Widget'Class; dx, dy : Float );

    -- Scrolls the widget's viewport to an abolute location in its content area.
    -- The top left corner of the viewport will scroll to 'x,y' in widget
    -- content coordinates.
    procedure Scroll_To( this : not null access Widget'Class; x, y : Float );

    -- Sets attribute 'name' to 'val'. Attributes are used for encapsulating
    -- minor additional data without extending a widget class. The given value
    -- will be copied unless 'consume' is True.
    procedure Set_Attribute( this    : not null access Widget'Class;
                             name    : String;
                             val     : Value'Class;
                             consume : Boolean := False );

    -- Sets attribute 'name' to 'val'.0
    procedure Set_Attribute( this : not null access Widget'Class; name : String; val : Boolean );
    procedure Set_Attribute( this : not null access Widget'Class; name : String; val : Integer );
    procedure Set_Attribute( this : not null access Widget'Class; name : String; val : Float );
    procedure Set_Attribute( this : not null access Widget'Class; name : String; val : Long_Float );
    procedure Set_Attribute( this : not null access Widget'Class; name : String; val : String );
    procedure Set_Attribute( this : not null access Widget'Class; name : String; val : Unbounded_String );

    -- Sets whether the widget is enabled or disabled. When disabled, the widget
    -- will ignore all user interaction. Its children are also implicitly
    -- disabled.
    procedure Set_Enabled( this : not null access Widget'Class; enable : Boolean );

    -- Sets whether the widget can accept focus or not.
    procedure Set_Focusable( this      : not null access Widget'Class;
                             focusable : Boolean );

    -- Sets the name of the mouse cursor to show when the mouse is hovering over
    -- the widget.
    procedure Set_Mouse_Cursor( this : not null access Widget'Class;
                                name : String );

    -- Sets the next widget to receive focus in the keyboard focus cycle.
    procedure Set_Next( this : not null access Widget'Class; id : String );

    -- Sets the previous widget to receive focus in the keyboard focus cycle.
    procedure Set_Prev( this : not null access Widget'Class; id : String );

    -- Sets the style of the widget by name. The 'SyleChanged' signal will be
    -- emitted on success. If style 'name' has not been defined for this widget
    -- type, or it can't be loaded, then the style will not change.
    procedure Set_Style( this : not null access Widget'Class; name : String );

    -- Sets the style property 'property' to 'val'. The format of the property
    -- string is "<elemname>.<propname>" where <elemname> is the visual element
    -- to modify, and <propname> is the property to set. The property is visible
    -- only when visual 'state' is active.
    procedure Set_Style_Property( this     : not null access Widget'Class;
                                  property : String;
                                  state    : Widget_State;
                                  val      : Value'Class );

    -- Sets the style property 'property' to 'val'. This is equivalent to calling
    -- the Set_Style_Property method above with state := DEFAULT_STATE.
    procedure Set_Style_Property( this     : not null access Widget'Class;
                                  property : String;
                                  val      : Value'Class );

    -- Sets the style of the widget, by copy. 'style' will remain under the
    -- ownership of the caller. The 'StyleChanged' signal will be emitted on
    -- success. If 'style' is null, the style will not change. It is important
    -- that 'style' be appropriately constructed for this widget's type. A Label
    -- style, for example, cannot be applied to a Button.
    procedure Set_Style( this : not null access Widget'Class; style : A_Widget_Style );

    -- Sets the 'z' / "draw depth" of the widget, relative to its parent.
    -- Greater than zero draws the widget behind its parent, and zero or less
    -- draws the widget in front of its parent.
    procedure Set_Z( this : not null access Widget'Class; z : Float );

    -- Returns a string representation of the widget for debugging purposes.
    function To_String( this : access Widget ) return String;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Deletes the Widget and all its children.
    procedure Delete( this : in out A_Widget );
    pragma Postcondition( this = null );

    -- Schedules the Widget and all its children for deferred deletion. The
    -- widget will be removed from the widget registry immediately, but it will
    -- not be deallocated until the stack is unwound back to Window.Event_Handler.
    -- This allows widgets that may be on the stack to be scheduled for deletion
    -- when they are no longer on the stack. Useful in situations such as a
    -- button that deletes itself on click.
    --
    -- If there is no Game_View or the widget isn't rooted, then it will be
    -- deleted immediately to avoid leaks.
    procedure Delete_Later( this : in out A_Widget );
    pragma Postcondition( this = null );

    WIDGET_NOT_FOUND : exception;

private

    -- Provides a linked list of widgets.
    package Widget_Lists is new Ada.Containers.Indefinite_Doubly_Linked_Lists( A_Widget, "=" );

    ----------------------------------------------------------------------------

    type Anchor(typ : Anchor_Type) is tagged limited
        record
            owner    : A_Widget := null;   -- the widget owning the anchor
            target   : A_Anchor := null;   -- the target anchor (if set)
            margin   : Float := 0.0;       -- the anchor's margin from its target
            innerPad : Float := 0.0;       -- inner padding for children

            resolved : Boolean := False;   -- .position has been resolved within .owner.parent's content
            position : Float := 0.0;       -- calculated from .target and .margin

            siblingObservers : Integer := 0;   -- count of sibling widgets anchored to this
        end record;

    -- Detaches the anchor if it is connected to an anchor on widget 'target'.
    -- Otherwise, nothing will change.
    procedure Detach( this : not null access Anchor'Class; target : access Widget'Class );

    -- Returns the position of the anchor, relative to its parent's content, if
    -- it is attached to another anchor.
    function Get_Position( this : not null access Anchor'Class ) return Float;

    -- If the anchor is attached to another, the target's position will be
    -- resolved by updating the geometry of the target anchor's widget.
    procedure Resolve_Attachment( this : not null access Anchor'Class );

    ----------------------------------------------------------------------------

    -- Enumeration for specifying the corner of a widget
    type Widget_Corner_Type is
    (
        Widget_Top_Left,
        Widget_Top_Right,
        Widget_Bottom_Left,
        Widget_Bottom_Right
    );

    ----------------------------------------------------------------------------

    type Widget is abstract new Limited_Object and Event_Listener with
        record
            view         : access Game_Views.Game_View'Class;
            id           : Unbounded_String;    -- unique id of the widget
            registered   : Boolean := False;    -- added to the registry?
            parent       : A_Widget := null;    -- parent/container widget
            children     : Widget_Lists.List;   -- list of child widgets

            z            : Float := 0.0;        -- z depth relative to parent
            posX         : Value := Null_Value;
            posY         : Value := Null_Value;
            posWidth     : Value := Null_Value;
            posHeight    : Value := Null_Value;
            prefWidth    : Value := Null_Value;
            prefHeight   : Value := Null_Value;
            resolvingX   : Boolean := False;
            resolvingY   : Boolean := False;
            geom         : Rectangle;           -- calculated location and size relative to its parent
            viewport     : Rectangle;           -- viewable area relative to own content
            zoomFactor   : Float := 1.0;        -- scaling factor (< 1 is zoomed out, > 1 is zoomed in)

            enable       : Boolean := True;     -- user interaction allowed?
            vis          : Boolean := True;     -- widget is visible
            hover        : Boolean := False;    -- mouse is over widget
            mouseButtons : Mouse_Button_Array := (others => False);
            acceptFocus  : Boolean := True;     -- widget accepts focus
            hasFocus     : Boolean := False;    -- widget is focused?
            nextId,                             -- next widget to focus
            prevId       : Unbounded_String;    -- previous widget to focus

            style        : A_Widget_Style := null;
            widgetType   : Unbounded_String;    -- the style widget type
            attributes   : Map_Value;           -- extensible attributes
            mouseCursor  : Unbounded_String := To_Unbounded_String( "DEFAULT" );

            propVisible  : aliased Animated_Boolean.Property;
            propX        : aliased Animated_Float.Property;
            propY        : aliased Animated_Float.Property;
            propHeight   : aliased Animated_Float.Property;
            propWidth    : aliased Animated_Float.Property;
            propZoom     : aliased Animated_Float.Property;

            ancLeft      : aliased Anchor(Vertical);
            ancRight     : aliased Anchor(Vertical);
            ancCenterX   : aliased Anchor(Vertical);
            ancTop       : aliased Anchor(Horizontal);
            ancBottom    : aliased Anchor(Horizontal);
            ancCenterY   : aliased Anchor(Horizontal);

            sigAttributeChanged : aliased String_Signal;
            sigBlurred          : aliased Signal;
            sigDeleted          : aliased Signal;
            sigDisabled         : aliased Signal;
            sigEnabled          : aliased Signal;
            sigFocused          : aliased Signal;
            sigHidden           : aliased Signal;
            sigResized          : aliased Signal;
            sigRooted           : aliased Signal;
            sigShown            : aliased Signal;
            sigStyleChanged     : aliased Signal;
            sigUnrooted         : aliased Signal;
            sigParentChanged    : aliased Signal;

            sigGamepadMoved     : aliased Gamepad_Axis_Signal;
            sigGamepadPressed   : aliased Gamepad_Button_Signal;
            sigGamepadReleased  : aliased Gamepad_Button_Signal;

            sigKeyPressed       : aliased Key_Signal;
            sigKeyReleased      : aliased Key_Signal;
            sigKeyTyped         : aliased Key_Signal;

            sigMouseClicked       : aliased Button_Signal;
            sigMouseDoubleClicked : aliased Button_Signal;
            sigMouseEntered       : aliased Signal;
            sigMouseExited        : aliased Signal;
            sigMouseHeld          : aliased Button_Signal;
            sigMouseMoved         : aliased Mouse_Signal;
            sigMousePressed       : aliased Button_Signal;
            sigMouseReleased      : aliased Button_Signal;
            sigMouseScrolled      : aliased Scroll_Signal;
        end record;

    -- Raises DUPLICATE_ID exception if 'id' has already been registered with
    -- the widget's view. Widget id strings must be unique per view. If 'id' is
    -- an empty string, a random id will be generated.
    procedure Construct( this       : access Widget;
                         view       : not null access Game_Views.Game_View'Class;
                         id         : String;
                         widgetType : String );

    procedure Delete( this : in out Widget );

    -- Activates a popup menu 'popup', placing widget corner 'corner' at 'x,y'
    -- in this widget's content coordinates. 'width' and 'height' specify the
    -- size of the popup widget. Passing zero in either dimension will size the
    -- popup to its reported minimum size.
    --
    -- The popup widget will become a child of the Window. It's actual position
    -- relative to the Window's content area will be translated from 'x,y' in
    -- this widget's content coordinates. It's actual size, 'width,height' must
    -- be given in Window content coordinates.
    --
    -- Example situations for specifying/omitting sizes are as follows:
    --   Select-enum choices popup : Specify width, omit height
    --   Pulldown menu             : Omit width and height
    --   Contextual popup menu     : Omit width and height
    procedure Activate_Popup( this   : not null access Widget'Class;
                              popup  : not null access Widget'Class;
                              corner : Widget_Corner_Type;
                              x, y   : Float;
                              width  : Float := 0.0;
                              height : Float := 0.0 );

    -- Adds 'child' as a child widget of this. The child's parent is set to this
    -- widget. This widget takes ownership of 'child'.
    procedure Add_Child( this  : not null access Widget'Class;
                         child : not null access Widget'Class );

    -- Brings this widget to the front of the drawing order, relative to its
    -- siblings, within the container.
    procedure Bring_To_Front( this : not null access Widget'Class );

    -- Deactivates popup menu 'popup' by popping popups from the window until it
    -- given popup has been deactivated. If the given popup menu isn't active,
    -- or this widget is not rooted, nothing will happen.
    procedure Deactivate_Popup( this  : not null access Widget'Class;
                                popup : not null access Widget'Class );

    -- Removes and deletes child widget 'child', if it belongs to this widget.
    -- If 'child' is not a child of this widget, then WIDGET_NOT_FOUND will be
    -- raised. 'child' will be consumed on success.
    procedure Delete_Child( this  : not null access Widget'Class;
                            child : in out A_Widget );
    pragma Postcondition( child = null );

    -- Removes and deletes all child widgets.
    procedure Delete_Children( this : not null access Widget'Class );

    -- Detaches any anchors targeting widget 'child', which is expected to be a
    -- child of this widget. This is used to detach all child widget anchors to
    -- a sibling when it is removed.
    procedure Detach_Anchors( this : not null access Widget'Class; child : access Widget'Class );

    -- Draws each of the visible children on top of the widget's content area,
    -- clipped to the widget's viewport. This is called by the default Draw()
    -- implementation after Draw_Content() and before Draw_Content_Foreground().
    procedure Draw_Children( this : access Widget );

    -- Draws the widget's background content behind its children. The default
    -- implementation of Draw() will call this first, clipping drawing to the
    -- widget's viewport.
    --
    -- Implement this procedure as necessary.
    procedure Draw_Content( this : access Widget ) is null;

    -- Draws the widget's foreground content in front of its children. The
    -- default implementation of Draw() will call this after Draw_Children(),
    -- clipping drawing to the widget's viewport.
    --
    -- Implement this procedure as necessary.
    procedure Draw_Content_Foreground( this : access Widget ) is null;

    -- Returns the descendant widget (including self) that contains location
    -- 'x,y' given in the widget's viewport coordinates (the parent widget's
    -- content coordinates.) The 'found' argument will return a pointer to the
    -- found widget, and 'wx,wy' will be returned as 'x,y' translated into the
    -- found widget's content coordinates. For a widget to be found, it must be
    -- visible and enabled.
    --
    -- If 'children' is True, children will be checked as well, otherwise they
    -- will be ignored.
    --
    -- If 'x,y' is not within an active, visible descendent's content area then
    -- 'found' will be returned null and 'wx,wy' will be set to 0.0.
    procedure Find_Widget_At( this     : access Widget;
                              x, y     : Float;
                              children : Boolean;
                              wx, wy   : out Float;
                              found    : out A_Widget );

     -- Attempts to focus the previous widget, or if it's disabled, its
     -- previous. Returns True on success.
    function Focus_Prev( this : not null access Widget'Class ) return Boolean;

     -- Attempts to focus the next widget, or if it's disabled- its next.
     -- Returns True on success.
    function Focus_Next( this : not null access Widget'Class ) return Boolean;

    -- Returns the mouse location in the widget's content coordinates. The mouse
    -- doesn't have to be over the widget; its location will be mapped from
    -- screen coordinates to the widget's content coordinates. The widget must
    -- be rooted within a Window before calling this, because it relies on
    -- asking its Window where the mouse is in Window coordinates.
    procedure Get_Mouse_Location( this   : not null access Widget'Class;
                                  mx, my : out Float );

    -- Returns the widget's unique id as its process name, in case a subclass
    -- implements Animated or Process.
    not overriding
    function Get_Process_Name( this : access Widget ) return String is (To_String( this.id ));

    -- Returns the current visual state of the widget for use with its style.
    function Get_Visual_State( this : access Widget ) return Widget_State is (DEFAULT_STATE);

    -- Returns the window containing this widget. If the widget is not rooted,
    -- then null will be returned.
    function Get_Window( this : not null access Widget'Class ) return access Widgets.Windows.Window'Class;

    -- Implements the Event_Listener interface. Override this to handle events
    -- for which the widget is listening.
    overriding
    procedure Handle_Event( this : access Widget;
                            evt  : in out A_Event;
                            resp : out Response_Type ) is null;

    -- Returns True if the widget is a descendent of 'ancestor'.
    function Is_Descendant_Of( this     : not null access Widget'Class;
                               ancestor : access Widget'Class ) return Boolean;

    -- Returns True if this widget is rooted within a Window. If it, or one of
    -- its ancestors, does not have a parent, then False will be returned.
    function Is_Rooted( this : not null access Widget'Class ) return Boolean;

    -- Returns True if the widget is visible within its container. This is used
    -- by the Visible property to read visibility. Callers outside the Widget
    -- package tree should use .Visible.Get instead.
    function Is_Visible( this : not null access Widget'Class ) return Boolean is (this.vis);

    -- Helper procedure adds the widget to the view's event corral as a listener
    -- for 'evtId' events. The widget will be automatically removed as a listener
    -- from all events when it is deleted.
    --
    -- This is equivalent to calling:
    -- this.Get_View.Get_Corral.Add_Listener( this, evtId );
    procedure Listen_For_Event( this : not null access Widget'Class; evtId : Event_Id );

    -- Removes 'child' from the widget without deleting it. Ownership of 'child'
    -- will be transferred to the caller. An exception will be raised if 'child'
    -- is not a child of this widget.
    procedure Remove_Child( this  : not null access Widget'Class;
                            child : not null access Widget'Class );

    -- Sends this widget to the back of the drawing order, relative to its
    -- siblings, within the container.
    procedure Send_To_Back( this : not null access Widget'Class );

    -- Notifies the widget that it has been focused or blurred within its
    -- window. The Focused or Blurred signal will be emitted. This should only
    -- be called by a Window widget, which tracks keyboard focus.
    procedure Set_Focused( this : not null access Widget'Class; focused : Boolean );

    -- Notifies the widget that its mouse hovering state is changing. The
    -- Entered or Exited signal will be emitted. This should only be called by a
    -- Window widget, which tracks the mouse.
    procedure Set_Hover( this : not null access Widget'Class; hover : Boolean );

    -- Sets widget 'parent' as the parent of this widget. Set 'parent' to null
    -- if the widget is being removed from its parent.
    --
    -- The Rooted/Unrooted and Shown/Hidden signals will be emitted as necessary.
    -- They will also be propagated to all applicable descendant widgets, since
    -- the descendents' rooted and shown states may also be changing.
    --
    -- NOTE: This procedure only updates the child in the parent/child widget
    -- relationship. The parent widget should be the only one calling this, as
    -- children are added to and removed from it.
    procedure Set_Parent( this   : not null access Widget'Class;
                          parent : access Widget'Class );

    procedure Set_Style_Property( this     : not null access Widget'Class;
                                  element  : Element_Index;
                                  property : String;
                                  state    : Widget_State;
                                  val      : Value'Class );

    -- Translates screen coordinates 'sx,sy' into the widget's content
    -- coordinates 'cx,cy'. If the widget is not rooted, the result is undefined.
    procedure Translate_To_Content( this   : not null access Widget'Class;
                                    sx, sy : Float;
                                    cx, cy : out Float );

    -- Translates the widget's content coordinates 'cx,cy' into window
    -- coordinates 'wx,wy'. If the widget is not rooted, the result is undefined.
    procedure Translate_To_Window( this   : not null access Widget'Class;
                                   cx, cy : Float;
                                   wx, wy : out Float );

    -- Helper procedure removes the widget from the view's event corral as a
    -- listener for 'evtId' events.
    --
    -- This is equivalent to calling:
    -- this.Get_View.Get_Corral.Remove_Listener( this, evtId );
    procedure Unlisten_For_Event( this : not null access Widget'Class; evtId : Event_Id := 0 );

    -- Updates the size and position of the widget, based on its anchors and
    -- positioning. Its siblings and children will be updated as necessary. If
    -- only the X axis / width needs to be updated, set 'axis' to X_AXIS.
    procedure Update_Geometry( this : not null access Widget'Class;
                               axis : Axis_Type := XY_AXES );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Compares widgets 'l' and 'r' by id; returns True if 'l' is less than 'r'.
    function Lt( l, r : A_Widget ) return Boolean;

end Widgets;
