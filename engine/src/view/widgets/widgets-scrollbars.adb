--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Drawing;                           use Drawing;
with Drawing.Primitives;                use Drawing.Primitives;
with Drawing.Tiles;                     use Drawing.Tiles;
with Widget_Styles.Registry;            use Widget_Styles.Registry;

package body Widgets.Scrollbars is

    package Mouse_Connections is new Signals.Mouse.Connections(Scrollbar);
    use Mouse_Connections;

    package Mouse_Connections_H is new Signals.Mouse.Connections(H_Scrollbar);
    use Mouse_Connections_H;

    package Mouse_Connections_V is new Signals.Mouse.Connections(V_Scrollbar);
    use Mouse_Connections_V;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Released( this : not null access Scrollbar'Class  );

    ----------------------------------------------------------------------------

    procedure On_Mouse_Held( this : not null access H_Scrollbar'Class );

    procedure On_Mouse_Moved( this  : not null access H_Scrollbar'Class;
                              mouse : Mouse_Arguments );

    procedure On_Mouse_Pressed( this  : not null access H_Scrollbar'Class;
                                mouse : Button_Arguments );

    ----------------------------------------------------------------------------

    procedure On_Mouse_Held( this : not null access V_Scrollbar'Class );

    procedure On_Mouse_Moved( this  : not null access V_Scrollbar'Class;
                              mouse : Mouse_Arguments );

    procedure On_Mouse_Pressed( this  : not null access V_Scrollbar'Class;
                                mouse : Button_Arguments );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    not overriding
    procedure Construct( this : access Scrollbar;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String ) is
    begin
        Widget(this.all).Construct( view, id, "Scrollbar" );
        this.MouseReleased.Connect( Slot( this, On_Mouse_Released'Access, Mouse_Left ) );
        this.Set_Focusable( False );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Scrollbar ) is
    begin
        this.client := null;
        Widget(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Content( this : access Scrollbar ) is
        state : constant Widget_State := this.Get_Visual_State;
        color : Allegro_Color;
        rect  : Rectangle;
    begin
        -- draw the bar background --
        color := this.style.Get_Color( BACKGROUND_ELEM, state );
        if color /= Transparent then
            Rectfill( 0.0, 0.0, this.viewport.width, this.viewport.height,
                      Lighten( color, this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );
        else
            Draw_Tile_Filled( this.style.Get_Image( BACKGROUND_ELEM, state ).Get_Tile,
                              0.0, 0.0, 0.0,
                              this.viewport.width, this.viewport.height,
                              Lighten( this.style.Get_Tint( BACKGROUND_ELEM, state ),
                                       this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );
        end if;

        if this.client /= null then
            rect := A_Scrollbar(this).Calculate_Button;

            -- draw the button background --
            color := this.style.Get_Color( BUTTON_ELEM, state );
            if color /= Transparent then
                Rectfill( rect.x, rect.y, rect.x + rect.width, rect.y + rect.height,
                          Lighten( color, this.style.Get_Shade( BUTTON_ELEM, state ) ) );
            else
                Draw_Tile_Filled( this.style.Get_Image( BUTTON_ELEM, state ).Get_Tile,
                                  rect.x, rect.y, 0.0,
                                  rect.width, rect.height,
                                  Lighten( this.style.Get_Tint( BUTTON_ELEM, state ),
                                           this.style.Get_Shade( BUTTON_ELEM, state ) ) );
            end if;

            -- draw the border --
            Draw_Tile_Filled( this.style.Get_Image( BORDER_ELEM, state ).Get_Tile,
                              rect.x, rect.y, 0.0,
                              rect.width, rect.height,
                              Lighten( this.style.Get_Tint( BORDER_ELEM, state ),
                                       this.style.Get_Shade( BORDER_ELEM, state ) ) );
        end if;
    end Draw_Content;

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Height( this : access Scrollbar ) return Float is (Float(this.style.Get_Image( BUTTON_ELEM ).Get_Height));

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Width( this : access Scrollbar ) return Float is (Float(this.style.Get_Image( BUTTON_ELEM ).Get_Width));

    ----------------------------------------------------------------------------

    overriding
    function Get_Visual_State( this : access Scrollbar ) return Widget_State is
    (
           (if not this.Is_Enabled then DISABLED_STATE else 0)
        or (if this.dragging       then PRESS_STATE    else 0)
        or (if this.hover          then HOVER_STATE    else 0)
    );

    ----------------------------------------------------------------------------

    procedure On_Mouse_Released( this : not null access Scrollbar'Class ) is
    begin
        this.pageUp := False;
        this.pageDown := False;
        this.dragging := False;
    end On_Mouse_Released;

    ----------------------------------------------------------------------------

    procedure Set_Client( this : not null access Scrollbar'Class; client : A_Widget ) is
    begin
        this.client := client;
    end Set_Client;

    --==========================================================================

    function Create_H_Scrollbar( view : not null access Game_Views.Game_View'Class;
                                 id   : String := "" ) return A_H_Scrollbar is
        this : A_H_Scrollbar := new H_Scrollbar;
    begin
        this.Construct( view, id );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_H_Scrollbar;

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access H_Scrollbar;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String ) is
    begin
        Scrollbar(this.all).Construct( view, id );
        this.MouseHeld.Connect( Slot( this, On_Mouse_Held'Access, Mouse_Left ) );
        this.MouseMoved.Connect( Slot( this, On_Mouse_Moved'Access ) );
        this.MousePressed.Connect( Slot( this, On_Mouse_Pressed'Access, Mouse_Left ) );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    function Calculate_Button( this : access H_Scrollbar ) return Rectangle is
        contentWidth : Float;
        rect         : Rectangle;
    begin
        rect.x := 0.0;
        rect.y := 0.0;
        rect.width := this.viewport.width;
        rect.height := this.viewport.height;
        if this.client /= null then
            contentWidth := this.client.Get_Preferred_Width;
            if contentWidth > 0.0 then
                rect.x := Float'Floor( this.viewport.width * (this.client.Get_Viewport.x / contentWidth) );
                rect.width := Float'Floor( this.viewport.width * Float'Min( this.client.Get_Viewport.width / contentWidth, 1.0 ) );
            end if;
        end if;
        return rect;
    end Calculate_Button;

    ----------------------------------------------------------------------------

    overriding
    function Get_Preferred_Height( this : access H_Scrollbar ) return Float is
        height : Float := 0.0;
    begin
        -- background
        if this.style.Get_Color( BACKGROUND_ELEM ) = Transparent then
            height := this.style.Get_Image( BACKGROUND_ELEM ).Get_Tile.Get_Height;
        end if;

        -- button
        if this.style.Get_Color( BUTTON_ELEM ) = Transparent then
            height := Float'Max( height, this.style.Get_Image( BUTTON_ELEM ).Get_Tile.Get_Height );
        end if;

        return height;
    end Get_Preferred_Height;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Held( this : not null access H_Scrollbar'Class ) is
    begin
        if this.pageUp then
            this.client.Scroll( -this.client.Get_Viewport.width, 0.0 );
        elsif this.pageDown then
            this.client.Scroll( this.client.Get_Viewport.width, 0.0 );
        end if;
    end On_Mouse_Held;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Moved( this  : not null access H_Scrollbar'Class;
                              mouse : Mouse_Arguments ) is
        scale : Float;
    begin
        if this.dragging and then this.client /= null then
            scale := this.client.Get_Preferred_Width / this.viewport.width;
            this.client.Scroll( Float'Rounding( (mouse.x - (this.Calculate_Button.x + this.dragPos)) * scale ), 0.0 );
        end if;
    end On_Mouse_Moved;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Pressed( this  : not null access H_Scrollbar'Class;
                                mouse : Button_Arguments ) is
        rect : Rectangle;
    begin
        if this.client /= null then
            rect := this.Calculate_Button;
            if mouse.x < rect.x then
                this.pageUp := True;
                this.client.Scroll( -this.client.viewport.width, 0.0 );
            elsif mouse.x >= rect.x + rect.width then
                this.pageDown := True;
                this.client.Scroll( this.client.viewport.width, 0.0 );
            else
                this.dragging := True;
                this.dragPos := mouse.x - rect.x;
            end if;
        end if;
    end On_Mouse_Pressed;

    --==========================================================================

    function Create_V_Scrollbar( view : not null access Game_Views.Game_View'Class;
                                 id   : String := "" ) return A_V_Scrollbar is
        this : A_V_Scrollbar := new V_Scrollbar;
    begin
        this.Construct( view, id );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_V_Scrollbar;

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access V_Scrollbar;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String ) is
    begin
        Scrollbar(this.all).Construct( view, id );
        this.MouseHeld.Connect( Slot( this, On_Mouse_Held'Access, Mouse_Left ) );
        this.MouseMoved.Connect( Slot( this, On_Mouse_Moved'Access ) );
        this.MousePressed.Connect( Slot( this, On_Mouse_Pressed'Access, Mouse_Left ) );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    function Calculate_Button( this : access V_Scrollbar ) return Rectangle is
        contentHeight : Float;
        rect          : Rectangle;
    begin
        rect.x := 0.0;
        rect.y := 0.0;
        rect.width := this.viewport.width;
        rect.height := this.viewport.height;
        if this.client /= null then
            contentHeight := this.client.Get_Preferred_Height;
            if contentHeight > 0.0 then
                rect.y := Float'Floor( this.viewport.height * (this.client.Get_Viewport.y / contentHeight) );
                rect.height := Float'Floor( this.viewport.height * Float'Min( this.client.Get_Viewport.height / contentHeight, 1.0 ) );
            end if;
        end if;
        return rect;
    end Calculate_Button;

    ----------------------------------------------------------------------------

    overriding
    function Get_Preferred_Width( this : access V_Scrollbar ) return Float is
        width : Float := 0.0;
    begin
        -- background
        if this.style.Get_Color( BACKGROUND_ELEM ) = Transparent then
            width := this.style.Get_Image( BACKGROUND_ELEM ).Get_Tile.Get_Width;
        end if;

        -- button
        if this.style.Get_Color( BUTTON_ELEM ) = Transparent then
            width := Float'Max( width, this.style.Get_Image( BUTTON_ELEM ).Get_Tile.Get_Width );
        end if;

        return width;
    end Get_Preferred_Width;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Held( this : not null access V_Scrollbar'Class ) is
    begin
        if this.pageUp then
            this.client.Scroll( 0.0, -this.client.viewport.height );
        elsif this.pageDown then
            this.client.Scroll( 0.0, this.client.viewport.height );
        end if;
    end On_Mouse_Held;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Moved( this  : not null access V_Scrollbar'Class;
                              mouse : Mouse_Arguments ) is
        scale : Float;
    begin
        if this.dragging and then this.client /= null then
            scale := this.client.Get_Preferred_Height / this.viewport.height;
            this.client.Scroll( 0.0, Float'Rounding( (mouse.y - (this.Calculate_Button.y + this.dragPos)) * scale ) );
        end if;
    end On_Mouse_Moved;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Pressed( this  : not null access V_Scrollbar'Class;
                                mouse : Button_Arguments ) is
        rect : Rectangle;
    begin
        if this.client /= null then
            rect := this.Calculate_Button;
            if mouse.y < rect.y then
                this.pageUp := True;
                this.client.Scroll( 0.0, -this.client.Get_Viewport.height );
            elsif mouse.y >= rect.y + rect.height then
                this.pageDown := True;
                this.client.Scroll( 0.0, this.client.Get_Viewport.height );
            else
                this.dragging := True;
                this.dragPos := mouse.y - rect.y;
            end if;
        end if;
    end On_Mouse_Pressed;

begin

    Register_Style( "Scrollbar",
                    (BACKGROUND_ELEM => To_Unbounded_String( "background" ),
                     BUTTON_ELEM     => To_Unbounded_String( "button" ),
                     BORDER_ELEM     => To_Unbounded_String( "border" )),
                    (BACKGROUND_ELEM => Area_Element,
                     BUTTON_ELEM     => Area_Element,
                     BORDER_ELEM     => Area_Element),
                    (0 => To_Unbounded_String( "disabled" ),
                     1 => To_Unbounded_String( "press" ),
                     2 => To_Unbounded_String( "hover" )) );

end Widgets.Scrollbars;
