--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Preferences;                       use Preferences;
with Support;                           use Support;
with Tiles;                             use Tiles;

package body Widgets.Scenes.Cameras is

    function Create_Camera( scene : not null A_Scene ) return A_Camera is
        this : constant A_Camera := new Camera;
    begin
        this.Construct( scene );
        return this;
    end Create_Camera;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Camera; scene : not null A_Scene ) is
    begin
        Limited_Object(this.all).Construct;
        this.scene := scene;
        this.camSpeed       := Constrain( Get_Pref( "development", "camera.speed",            192.0 ), 0.0, 4096.0 );
        this.camAccelDist   := Constrain( Get_Pref( "development", "camera.slowdownDistance",  32.0 ), 1.0, 256.0 );
        this.camAccelAmount := Constrain( Get_Pref( "development", "camera.slowdownPercent",    0.8 ), 0.0, 0.99 );
    end Construct;

    ----------------------------------------------------------------------------

    procedure Clip_To_Walls( this     : not null access Camera'Class;
                             vx1, vy1 : in out Float ) is

        function Is_Blocking( col, row : Integer ) return Boolean is (this.scene.mapInfo.Has_TypeBits( col, row, TILE_BLOCK_CAMERA ));

        ax1, ay1, ax2, ay2 : Integer;
        bx1, by1, bx2, by2 : Integer;
    begin
        -- the full clipping algorithm needs to run twice to cover both axes
        for iter in 1..2 loop

            -- current viewport in tile coordinates
            ax1 := Integer(Float'Floor( this.scene.Get_Viewport.x / Float(this.scene.Get_Tile_Width) ));
            ay1 := Integer(Float'Floor( this.scene.Get_Viewport.y / Float(this.scene.Get_Tile_Width) ));
            ax2 := Integer(Float'Floor( (this.scene.Get_Viewport.x + this.scene.Get_Viewport.width) / Float(this.scene.Get_Tile_Width) ));
            ay2 := Integer(Float'Floor( (this.scene.Get_Viewport.y + this.scene.Get_Viewport.height) / Float(this.scene.Get_Tile_Width) ));

            ax2 := Integer'Min( ax2 + (if Fmod( this.scene.Get_Viewport.x + this.scene.Get_Viewport.width, Float(this.scene.Get_Tile_Width) ) = 0.0 then -1 else 0), this.scene.map.Get_Width - 1 );
            ay2 := Integer'Min( ay2 + (if Fmod( this.scene.Get_Viewport.y + this.scene.Get_Viewport.height, Float(this.scene.Get_Tile_Width) ) = 0.0 then -1 else 0), this.scene.map.Get_Height - 1 );

            -- new viewport in tile coordinates
            bx1 := Integer(Float'Floor( vx1 / Float(this.scene.Get_Tile_Width) ));
            by1 := Integer(Float'Floor( vy1 / Float(this.scene.Get_Tile_Width) ));
            bx2 := Integer(Float'Floor( (vx1 + this.scene.Get_Viewport.width) / Float(this.scene.Get_Tile_Width) ));
            by2 := Integer(Float'Floor( (vy1 + this.scene.Get_Viewport.height) / Float(this.scene.Get_Tile_Width) ));

            bx2 := Integer'Min( bx2 + (if Fmod( vx1 + this.scene.Get_Viewport.width, Float(this.scene.Get_Tile_Width) ) = 0.0 then -1 else 0), this.scene.map.Get_Width - 1 );
            by2 := Integer'Min( by2 + (if Fmod( vy1 + this.scene.Get_Viewport.height, Float(this.scene.Get_Tile_Width) ) = 0.0 then -1 else 0), this.scene.map.Get_Height - 1 );

            -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

            -- check if the area moved by only one tile in any direction, which
            -- allows us to optimize the blocking tile checking by only looking
            -- at the edges of the viewport.
            if abs (ax1 - bx1) <= 1 and abs (ax2 - bx2) <= 1 and
               abs (ay1 - by1) <= 1 and abs (ay2 - by2) <= 1
            then
                if bx1 < ax1 then
                    -- scan the tiles to the left of the screen
                    for y in ay1..ay2 loop
                        if Is_Blocking( ax1 - 1, y ) then
                            vx1 := Float(ax1 * this.scene.Get_Tile_Width);
                            exit;
                        end if;
                    end loop;
                elsif bx2 > ax2 then
                    -- scan the tiles to the right of the screen
                    for y in ay1..ay2 loop
                        if Is_Blocking( ax2 + 1, y ) then
                            vx1 := Float((ax2 + 1) * this.scene.Get_Tile_Width) - this.scene.Get_Viewport.width;
                            exit;
                        end if;
                    end loop;
                end if;

                if by1 < ay1 then
                    -- scan the tiles to the top of the screen
                    for x in ax1..ax2 loop
                        if Is_Blocking( x, ay1 - 1 ) then
                            vy1 := Float(ay1 * this.scene.Get_Tile_Width);
                            exit;
                        end if;
                    end loop;
                elsif by2 > ay2 then
                    -- scan the tiles to the bottom of the screen
                    for x in ax1..ax2 loop
                        if Is_Blocking( x, ay2 + 1 ) then
                            vy1 := Float((ay2 + 1) * this.scene.Get_Tile_Width) - this.scene.Get_Viewport.height;
                            exit;
                        end if;
                    end loop;
                end if;

                -- break out of the 'iter' loop; the optimized clipping doesn't
                -- need to run twice to handle both axes.
                exit;
            end if;

            -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

            -- check the entire viewport for blocking tiles. this needs to run
            -- when the viewport jumps to a new location (player teleports),
            -- because the optimized edge checking can't cover all the cases.
            declare
                centerX : constant Integer := bx1 + (bx2 - bx1) / 2;
                centerY : constant Integer := by1 + (by2 - by1) / 2;
                nearX   : Integer := 0;
                nearY   : Integer := 0;
                nearest : Integer := abs (bx2 - bx1) + abs (by2 - by1) + 1;
                blocked : Boolean := False;
                dist    : Integer;
            begin
                -- locate the nearest blocking tile in view
                for y in by1..by2 loop
                    for x in bx1..bx2 loop
                        if Is_Blocking( x, y ) then
                            blocked := True;

                            -- calcaulate manhattan distance to the block from center
                            dist := abs (x - centerX) + abs (y - centerY);
                            if dist < nearest then
                                -- found a new nearest block
                                nearest := dist;
                                nearX := x - centerX;
                                nearY := y - centerY;
                            end if;
                        end if;
                    end loop;
                end loop;

                -- a blocking tile was found in view
                if blocked then
                    -- move the viewport in only once axis- the one that
                    -- requires the least movement (furthest from center)
                    if abs nearX > abs nearY then
                        if nearX >= 0 then
                            -- nearest block right of center; move left
                            vx1 := Float((centerX + nearX) * this.scene.Get_Tile_Width) - this.scene.Get_Viewport.width;
                        else
                            -- nearest block left of center; move right
                            vx1 := Float((centerX + nearX + 1) * this.scene.Get_Tile_Width);
                        end if;
                    else
                        if nearY >= 0 then
                            -- nearest block below center; move up
                            vy1 := Float((centerY + nearY) * this.scene.Get_Tile_Width) - this.scene.Get_Viewport.height;
                        else
                            -- nearest block above center; move down
                            vy1 := Float((centerY + nearY + 1) * this.scene.Get_Tile_Width);
                        end if;
                    end if;
                end if;
            end;

        end loop;  -- iter in 1..2
    end Clip_To_Walls;

    ----------------------------------------------------------------------------

    procedure Enable_Camera_Walls( this : not null access Camera'Class; enabled : Boolean ) is
    begin
        this.obeyCameraWalls := enabled;
    end Enable_Camera_Walls;

    ----------------------------------------------------------------------------

    procedure Enable_Loose_Tracking( this    : not null access Camera'Class;
                                     enabled : Boolean ) is
    begin
        this.looseTracking := enabled;
    end Enable_Loose_Tracking;

    ----------------------------------------------------------------------------

    function Get_FocusX( this : not null access Camera'Class ) return Float is (this.focusX);

    ----------------------------------------------------------------------------

    function Get_FocusX_To_Bounds( this : not null access Camera'Class ) return Float is
        viewport : constant Rectangle := this.scene.Get_Viewport;
        dist     : Float;
    begin
        -- check the left edge
        dist := this.focusX - (viewport.x + viewport.width / 2.0 + this.bounds.x);
        if dist < 0.0 then
            return dist;
        end if;

        -- check the right edge
        dist := this.focusX - (viewport.x + viewport.width / 2.0 + this.bounds.x + this.bounds.width);
        if dist > 0.0 then
            return dist;
        end if;

        return 0.0;
    end Get_FocusX_To_Bounds;

    ----------------------------------------------------------------------------

    function Get_FocusY( this : not null access Camera'Class ) return Float is (this.focusY);

    ----------------------------------------------------------------------------

    function Get_FocusY_To_Bounds( this : not null access Camera'Class ) return Float is
        viewport : constant Rectangle := this.scene.Get_Viewport;
        dist     : Float;
    begin
        -- check the top edge
        dist := this.focusY - (viewport.y + viewport.height / 2.0 + this.bounds.y);
        if dist < 0.0 then
            return dist;
        end if;

        -- check the bottom edge
        dist := this.focusY - (viewport.y + viewport.height / 2.0 + this.bounds.y + this.bounds.height);
        if dist > 0.0 then
            return dist;
        end if;

        return 0.0;
    end Get_FocusY_To_Bounds;

    ----------------------------------------------------------------------------

    procedure Scroll_Scene( this : not null access Camera'Class; x, y : Float ) is
    begin
        -- scroll the scene within the viewport by moving by 'x,y', but stop
        -- when any further movement would bring the focus outside of the camera
        -- bounds.
        this.Track_Focus( x, y, this.bounds );
    end Scroll_Scene;

    ----------------------------------------------------------------------------

    procedure Set_Bounds( this     : not null access Camera'Class;
                          boundsX1,
                          boundsY1,
                          boundsX2,
                          boundsY2 : Float ) is
    begin
        this.bounds.x := boundsX1;
        this.bounds.y := boundsY1;
        this.bounds.width := boundsX2 - boundsX1;
        this.bounds.height := boundsY2 - boundsY1;
        this.looseTrackingX := this.looseTracking and then this.Get_FocusX_To_Bounds /= 0.0;
        this.looseTrackingY := this.looseTracking and then this.Get_FocusY_To_Bounds /= 0.0;
        this.loosePrevDistX := 0.0;
        this.loosePrevDistY := 0.0;
    end Set_Bounds;

    ----------------------------------------------------------------------------

    procedure Set_Centering_Limit( this     : not null access Camera'Class;
                                   distance : Float ) is
    begin
        this.recenterLimit := distance;
    end Set_Centering_Limit;

    ----------------------------------------------------------------------------

    procedure Set_Focus( this   : not null access Camera'Class;
                         x, y   : Float;
                         center : Boolean := False ) is
        prevFocusX : constant Float := this.focusX;
        prevFocusY : constant Float := this.focusY;
        centerIt   : Boolean := center;
    begin
        if not this.scene.Is_Map_Loaded then
            return;
        end if;

        -- constrain the focus point to the map area
        this.focusX := Constrain( x, 0.0, Float(this.scene.map.Get_Width * this.scene.Get_Tile_Width) );
        this.focusY := Constrain( y, 0.0, Float(this.scene.map.Get_Height * this.scene.Get_Tile_Width) );

        -- if the focus moves by more than this limit, force a re-centering on
        -- the focal point. useful for recentering when the player teleports.
        centerIt := center
                    or abs (this.focusX - prevFocusX) > this.recenterLimit
                    or abs (this.focusY - prevFocusY) > this.recenterLimit;

        -- centering the focus disables loose tracking
        this.looseTrackingX := this.looseTrackingX and not centerIt;
        this.looseTrackingY := this.looseTrackingY and not centerIt;

        -- update the camera position based on the current focus and offset.
        -- if loose tracking mode is on, the camera only tracks in Tick().
        if not (this.looseTrackingX or this.looseTrackingY) then
            this.Track_Focus( 0.0, 0.0,
                              (if centerIt
                               then (x => this.bounds.x + this.bounds.width / 2.0,
                                     y => this.bounds.y + this.bounds.height / 2.0,
                                     width => 0.0,
                                     height => 0.0)
                               else this.bounds) );
        end if;
    end Set_Focus;

    ----------------------------------------------------------------------------

    procedure Tick( this : not null access Camera'Class; time : Tick_Time ) is
        viewport     : constant Rectangle := this.scene.Get_Viewport;
        dX, dY       : Float := 0.0;
        distX, distY : Float := 0.0;
        distSign     : Float;
    begin
        if not (this.looseTrackingX or this.looseTrackingY) then
            return;
        end if;

        -- determine how far .focusXY is outside of the camera bounds
        distX := this.Get_FocusX_To_Bounds;
        distY := this.Get_FocusY_To_Bounds;

        if this.looseTrackingX then
            if abs distX > this.camAccelDist then
                -- distX < -this.camAccelDist or this.camAccelDist < distX
                distSign := distX / abs distX;
                dX := distSign * this.camSpeed * Float(To_Duration( time.elapsed ));
            elsif abs distX > 1.0 then
                -- -this.camAccelDist <= distX < -1.0 or 1.0 < distX <= this.camAccelDist
                --
                -- (distX / this.camAccelDist) goes from 1 to 0, as a percentage
                -- of speed (1 - this.camAccelAmount) keeps the speed up so that
                -- speed doesn't drop quite to zero by the time distX is 0.
                -- (ex: when this.camAccelAmount = 0.8, the camera will slowdown
                -- by 80%; this terms adds the 20%). this prevents the camera
                -- speed from slowing to very small percentages of speed as
                -- distX goes to zero.
                --
                -- The term Float'Min(1, ...) caps the max speed at 100% of
                -- 'this.camSpeed' because we added in the
                -- (1.0 - this.camAccelAmount term to shift the speed up.
                distSign := distX / abs distX;
                dX := distSign * Float'Min( 1.0, ((abs distX) / this.camAccelDist) + (1.0 - this.camAccelAmount) ) * this.camSpeed * Float(To_Duration( time.elapsed ));
            else
                -- -1 <= distX <= 1.0
                dX := distX;
            end if;

            -- when the distance between the focus and the camera bounds changes
            -- signs, the focus skipped through the camera box in one frame.
            if (this.loosePrevDistX > 0.0 and dX < 0.0) or else
               (this.loosePrevDistX < 0.0 and dX > 0.0)
            then
                dX := distX;

            -- when the distance between the focus and the camera bounds is
            -- greater than the previous frame, then it's moving away from the
            -- camera bounds faster than we're chasing it. keep the same distance
            -- at least.
            elsif this.loosePrevDistX /= 0.0 and then abs this.loosePrevDistX < abs dX then
                dX := this.loosePrevDistX;
            end if;
        else
            dX := distX;
        end if;

        if this.looseTrackingY then
            if abs distY > this.camAccelDist then
                -- distY < -this.camAccelDist or this.camAccelDist < distY
                distSign := distY / abs distY;
                dY := distSign * this.camSpeed * Float(To_Duration( time.elapsed ));
            elsif abs distY > 1.0 then
                -- -this.camAccelDist <= distY < -1.0 or 1.0 < distY <= this.camAccelDist
                distSign := distY / abs distY;
                dY := distSign * Float'Min( 1.0, ((abs distY) / this.camAccelDist) + (1.0 - this.camAccelAmount) ) * this.camSpeed * Float(To_Duration( time.elapsed ));
            else
                -- -1 <= distY <= 1.0
                dY := distY;
            end if;

            if (this.loosePrevDistY > 0.0 and dY < 0.0) or else
               (this.loosePrevDistY < 0.0 and dY > 0.0)
            then
                dY := distY;
            elsif this.loosePrevDistY /= 0.0 and then abs this.loosePrevDistY < abs dY then
                dY := this.loosePrevDistY;
            end if;
        else
            dY := distY;
        end if;

        -- move the camera, bringing it back so the focus point is within bounds
        -- again. the bounds given here are enlarged to contain the focus so it
        -- doesn't get constrained.
        if dX /= 0.0 or dY /= 0.0 then
            this.Track_Focus( dX, dY,
                              (-viewport.width / 2.0, -viewport.height / 2.0,
                               viewport.width, viewport.height) );
        end if;

        -- turn off loose tracking when focus is back in camera bounds
        if this.looseTrackingX then
            this.loosePrevDistX := this.Get_FocusX_To_Bounds;
            if this.loosePrevDistX = 0.0 then
                this.looseTrackingX := False;
            end if;
        end if;
        if this.looseTrackingY then
            this.loosePrevDistY := this.Get_FocusY_To_Bounds;
            if this.loosePrevDistY = 0.0 then
                this.looseTrackingY := False;
            end if;
        end if;
    end Tick;

    ----------------------------------------------------------------------------

    procedure Track_Focus( this   : not null access Camera'Class;
                           shiftX : Float;
                           shiftY : Float;
                           bounds : Rectangle ) is
        viewport : constant Rectangle := this.scene.Get_Viewport;
        vx       : Float := viewport.x + shiftX;
        vy       : Float := viewport.y + shiftY;
    begin
        if this.focusX < vx + viewport.width / 2.0 + bounds.x then
            -- move the scene left
            vx := this.focusX - (viewport.width / 2.0 + bounds.x);
        elsif this.focusX > vx + viewport.width / 2.0 + bounds.x + bounds.width then
            -- move the scene right
            vx := this.focusX - (viewport.width / 2.0 + bounds.x + bounds.width);
        end if;

        if this.focusY < vy + viewport.height / 2.0 + bounds.y then
            -- move the scene up
            vy := this.focusY - (viewport.height / 2.0 + bounds.y);
        elsif this.focusY > vy + viewport.height / 2.0 + bounds.y + bounds.height then
            -- move the scene down
            vy := this.focusY - (viewport.height / 2.0 + bounds.y + bounds.height);
        end if;

        -- constrain to map area (left/top is the hard constraint)
        if this.scene.Is_Map_Loaded then
            vx := Float'Max( 0.0, Float'Min( vx, Float(this.scene.map.Get_Width * this.scene.Get_Tile_Width) - viewport.width) );
            vy := Float'Max( 0.0, Float'Min( vy, Float(this.scene.map.Get_Height * this.scene.Get_Tile_Width) - viewport.height) );
        end if;

        -- scroll based on the new calculated viewport origin
        if vx /= viewport.x or vy /= viewport.y then
            if this.obeyCameraWalls then
                this.Clip_To_Walls( vx, vy );
            end if;
            this.scene.Scroll_To( vx, vy );
        end if;
    end Track_Focus;

end Widgets.Scenes.Cameras;
