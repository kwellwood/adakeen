--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.State;                     use Allegro.State;
with Allegro.Transformations;           use Allegro.Transformations;
with Clipping;                          use Clipping;
with Component_Families;                use Component_Families;
with Debugging;                         use Debugging;
with Drawing;                           use Drawing;
with Drawing.Bitmaps;                   use Drawing.Bitmaps;
with Drawing.Primitives;                use Drawing.Primitives;
with Events.Entities;                   use Events.Entities;
with Events.Entities.Attributes;        use Events.Entities.Attributes;
with Events.Entities.Locations;         use Events.Entities.Locations;
with Events.Entities.Sprites;           use Events.Entities.Sprites;
with Events.World;                      use Events.World;
with Game_Views;                        use Game_Views;
with Interfaces;                        use Interfaces;
with Renderers;
with Tiles;                             use Tiles;
with Values.Allegro_Colors;             use Values.Allegro_Colors;
with Values.Casting;                    use Values.Casting;
with Values.Construction;               use Values.Construction;
with Widget_Styles.Registry;            use Widget_Styles.Registry;
with Widgets.Scenes.Cameras;            use Widgets.Scenes.Cameras;
with Widgets.Sprites;                   use Widgets.Sprites;

package body Widgets.Scenes is

    package Connections is new Signals.Connections(Scene);
    use Connections;

    ----------------------------------------------------------------------------

    -- Ensures the tile layer bitmaps have been created and are large enough to
    -- contain the visible tiles.
    procedure Create_Layer_Bitmaps( this : not null access Scene'Class );

    -- Handles an Entity_Attribute_Changed event by calling Set_Attribute on the
    -- affected Entity.
    procedure Handle_Entity_Attribute_Changed( this : not null access Scene'Class;
                                               evt  : not null A_Entity_Attribute_Event );

    -- Handles an Entity_Deleted event by deleting the corresponding Entity, if
    -- it is visible.
    procedure Handle_Entity_Deleted( this : not null access Scene'Class;
                                     evt  : not null A_Entity_Event );

    -- Handles an Entity_Moved event by moving the corresponding entity and
    -- focusing the viewport on it, if it's the scene's target.
    procedure Handle_Entity_Moved( this : not null access Scene'Class;
                                   evt  : not null A_Entity_Moved_Event );

    -- Handles an Entity_Resized event by resizing the corresponding entity.
    procedure Handle_Entity_Resized( this : not null access Scene'Class;
                                     evt  : not null A_Entity_Size_Event );

    -- Handles an Entity_Spawned event by creating and adding a corresponding
    -- scene entity. Calls Create_Entity_Widget() to create the entity's visible
    -- representation.
    procedure Handle_Entity_Spawned( this : not null access Scene'Class;
                                     evt  : not null A_Entity_Spawn_Event );

    -- Handles a Entity_Z_Changed event by changing the Z depth of the
    -- corresponding entity..
    procedure Handle_Entity_Z_Changed( this : not null access Scene'Class;
                                       evt  : not null A_Entity_Z_Event );

    -- Handles a Frame_Changed event by changing the image of the corresponding
    -- entity's Sprite widget.
    procedure Handle_Frame_Changed( this : not null access Scene'Class;
                                    evt  : not null A_Frame_Changed_Event );

    -- Handles a Layer_Created event by adding a new layer to the map.
    procedure Handle_Layer_Created( this : not null access Scene'Class;
                                    evt  : not null A_Layer_Created_Event );

    -- Handles a Layer_Deleted event by deleting a layer in the map.
    procedure Handle_Layer_Deleted( this : not null access Scene'Class;
                                    evt  : not null A_Layer_Event );

    -- Handles a Layer_Property_Changed event by updating the property in the
    -- scene's local map.
    procedure Handle_Layer_Property_Changed( this : not null access Scene'Class;
                                             evt  : not null A_Layer_Property_Event );

    -- Handles a Layers_Swapped event by swapping map layers.
    procedure Handle_Layers_Swapped( this : not null access Scene'Class;
                                     evt  : not null A_Layer_Swap_Event );

    -- Handles a Tile_Changed event by updating the specified tile in the
    -- scene's tile map. Override On_Tile_Changed to implement behavior in a
    -- subclass when a Tile_Changed event is received.
    procedure Handle_Tile_Changed( this : not null access Scene'Class;
                                   evt  : not null A_Tile_Event );

    -- Handles a World_Loaded event by clearing the scene's current map and all
    -- entities, and loading the new world's tile library and map. No entities
    -- will be added yet because the World_Loaded event does not contain entity
    -- information. A series of Entity_Spawned events will follow, to populate
    -- the world's entities in the scene.
    --
    -- Connect a slot to the MapChanged signal to implement behavior when a
    -- World_Loaded event is received.
    procedure Handle_World_Loaded( this : not null access Scene'Class;
                                   evt  : A_World_Loaded_Event );

    -- Handles a World_Property_Changed event.
    procedure Handle_World_Property_Changed( this : not null access Scene'Class;
                                             evt  : not null A_World_Property_Event );

    -- Called when the Scene widget has become disabled.
    procedure On_Disabled( this : not null access Scene'Class );

    -- Called when the Scene widget has become enabled again.
    procedure On_Enabled( this : not null access Scene'Class );

    -- Called when the Scene widget's geometry has been resized.
    procedure On_Resized( this : not null access Scene'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    not overriding
    procedure Construct( this : access Scene;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String ) is
    begin
        Widget(this.all).Construct( view, id, "Scene" );

        this.sigMapChanged.Init( this );
        this.sigWorldProp.Init( this );

        this.lights := Create_Lighting_System( view );
        this.options := this.options or this.lights.Get_Render_Options;

        this.particles := Create_Particle_System( view, 4096 );
        this.Get_View.Attach( this.particles );

        this.Listen_For_Event( EVT_FRAME_CHANGED );
        this.Listen_For_Event( EVT_ENTITY_ATTRIBUTE_CHANGED );
        this.Listen_For_Event( EVT_ENTITY_SPAWNED );
        this.Listen_For_Event( EVT_ENTITY_DELETED );
        this.Listen_For_Event( EVT_ENTITY_MOVED );
        this.Listen_For_Event( EVT_ENTITY_RESIZED );
        this.Listen_For_Event( EVT_ENTITY_Z_CHANGED );
        this.Listen_For_Event( EVT_WORLD_LOADED );
        this.Listen_For_Event( EVT_TILE_CHANGED );
        this.Listen_For_Event( EVT_WORLD_PROPERTY_CHANGED );
        this.Listen_For_Event( EVT_LAYER_CREATED );
        this.Listen_For_Event( EVT_LAYER_DELETED );
        this.Listen_For_Event( EVT_LAYER_PROPERTY_CHANGED );
        this.Listen_For_Event( EVT_LAYERS_SWAPPED );

        this.Enabled.Connect( Slot( this, On_Enabled'Access ) );
        this.Disabled.Connect( Slot( this, On_Disabled'Access ) );
        this.Resized.Connect( Slot( this, On_Resized'Access ) );

        this.worldProps := Create_Map.Map;
        this.mapInfo := Create_Map_Info_Cache;

        this.cam := Create_Camera( A_Scene(this) );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Scene ) is
        e2 : A_Entity;
    begin
        if this.view /= null then
            this.Get_View.Detach( this.particles );
        end if;
        Delete( this.particles );
        Delete( this.lights );
        for l in this.tileLayers.First..this.tileLayers.Last loop
            Al_Destroy_Bitmap( this.tileLayers.Reference( l ) );
        end loop;
        this.tileLayers.Clear;

        for e of this.entities loop
            e2 := e;
            Delete( e2 );
        end loop;
        this.entities.Clear;
        this.entityMap.Clear;

        Delete( this.mapInfo );
        Delete( this.map );

        Widget(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function MapChanged( this : not null access Scene'Class ) return access Signal'Class is (this.sigMapChanged'Access);

    function WorldPropertyChanged( this : not null access Scene'Class ) return access String_Signal'Class is (this.sigWorldProp'Access);

    ----------------------------------------------------------------------------

    procedure Add_Entity( this : not null access Scene'Class; entity : A_Entity ) is
    begin
        entity.Get_Widget.Visible.Set( True );
        this.entityMap.Insert( entity.Get_Entity_Id, entity );
        this.entities.Insert( entity );
        this.Add_Child( entity.Get_Widget );
        this.On_Entity_Added( entity );
    end Add_Entity;

    ----------------------------------------------------------------------------

    procedure Add_Widget( this  : not null access Scene'Class;
                          child : not null access Widget'Class ) is
    begin
        this.Add_Child( child );
    end Add_Widget;

    ----------------------------------------------------------------------------

    procedure Create_Layer_Bitmaps( this : not null access Scene'Class ) is
        state     : Allegro_State;
        bufWidth  : Float;
        bufHeight : Float;
    begin
        if not this.Is_Map_Loaded then
            return;
        end if;

        -- determine the buffer size in world units
        bufWidth := Float((this.tileX2 - this.tileX1 + 1) * this.tileWidth);
        bufHeight := Float((this.tileY2 - this.tileY1 + 1) * this.tileWidth);
        this.tileScaleX := 1.0;
        this.tileScaleY := 1.0;

        -- scale down the buffer by powers of two
        while bufWidth / 2.0 >= this.Get_Geometry.width loop
            bufWidth := bufWidth / 2.0;
            this.tileScaleX := this.tileScaleX * 2.0;
        end loop;

        while bufHeight / 2.0 >= this.Get_Geometry.height loop
            bufHeight := bufHeight / 2.0;
            this.tileScaleY := this.tileScaleY * 2.0;
        end loop;

        -- recreate a bitmap for each tile layer if necessary
        if this.tileLayers.Element( 0 ) = null or else
            Al_Get_Bitmap_Width( this.tileLayers.Element( 0 ) ) < Integer(Float'Ceiling( bufWidth )) or else
            Al_Get_Bitmap_Height( this.tileLayers.Element( 0 ) ) < Integer(Float'Ceiling( bufHeight ))
        then
            Al_Store_State( state, ALLEGRO_STATE_BITMAP );

            Al_Set_New_Bitmap_Flags( ALLEGRO_VIDEO_BITMAP or
                                     ALLEGRO_MIN_LINEAR or
                                     ALLEGRO_NO_PRESERVE_TEXTURE );

            for layer in this.tileLayers.First..this.tileLayers.Last loop
                Al_Destroy_Bitmap( this.tileLayers.Reference( layer ) );
                if this.map.Get_Layer_Type( layer ) = Layer_Tiles then
                    -- bitmaps are only needed for drawing tile layers
                    loop
                        this.tileLayers.Replace( layer, Al_Create_Bitmap( Integer(Float'Ceiling( bufWidth )),
                                                                          Integer(Float'Ceiling( bufHeight )) ) );
                        exit when this.tileLayers.Element( layer ) /= null;

                        -- try scaling down further in the largest dimension
                        if Float'Ceiling( bufWidth ) > Float'Ceiling( bufHeight ) then
                            this.tileScaleX := this.tileScaleX * 2.0;
                            bufWidth := bufWidth / 2.0;
                        else
                            this.tileScaleY := this.tileScaleY * 2.0;
                            bufHeight := bufHeight / 2.0;
                        end if;
                    end loop;
                    pragma Assert( this.tileLayers.Element( layer ) /= null, "Scene failed to create tile buffer" );
                    Set_Target_Bitmap( this.tileLayers.Element( layer ) );
                    this.view.Use_Shader( "default" );       -- set it just once
                end if;
            end loop;

            Al_Restore_State( state );
        end if;
    end Create_Layer_Bitmaps;

    ----------------------------------------------------------------------------

    not overriding
    function Create_Entity_Widget( this         : access Scene;
                                   entity       : not null A_Entity;
                                   initialState : Map_Value ) return A_Widget is
        vis : constant Map_Value := initialState.Get( Family_Name( VISIBLE_ID ) ).Map;
    begin
        if vis.Valid then
            if vis.Get_String( "render" ) = "sprite" then
                return A_Widget(Create_Sprite( this.Get_View, entity, vis.Get_String( "lib" ), vis.Get_Int( "frame" ) ));
            end if;
        end if;
        return null;
    end Create_Entity_Widget;

    ----------------------------------------------------------------------------

    procedure Delete_Entity( this : not null access Scene'Class; entity : in out A_Entity ) is
        widget : A_Widget;
    begin
        this.On_Entity_Removed( entity );
        entity.Get_Widget.Visible.Set( False );
        this.entities.Delete( entity );
        this.entityMap.Delete( entity.Get_Entity_Id );

        widget := entity.Get_Widget;
        this.Delete_Child( widget );
        Delete( entity );
    end Delete_Entity;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw( this : access Scene ) is
        state   : constant Widget_State := this.Get_Visual_State;
        alState : Allegro_State;
        trans   : Allegro_Transform;
    begin
        if not this.Is_Visible or this.viewport.width <= 0.0 or this.viewport.height <= 0.0 then
            return;
        end if;

        -- recalculate the render location of entities and the camera
        if this.Is_Enabled( SHOW_SPRITES ) then
            for e of this.entities loop
                e.Update_Render_Location;
                if e.Get_Entity_Id = this.target then
                    -- note: if the focus moves by more than the camera's centering
                    -- limit, it will automatically recenter on the target entity's
                    -- new position. example: the player teleports.
                    this.cam.Set_Focus( e.Get_Render_X, e.Get_Render_Y, False );
                    --Dbg( "Draw: ex=" & Image( e.Get_Entity_X ) &
                    --     ", rx=" & Image( e.Get_Render_X ) &
                    --     ", extrap=" & Image( this.Get_View.Get_Frame_Extrapolation ) &
                    --     ", delta=" & Image( e.Get_Render_X - prevX ) );
                end if;
            end loop;
        end if;

        -- before applying the zoom to translate widget coordinates to world
        -- coordinates, capture the scaling factor from display backbuffer pixels
        -- to widget pixels and calculate the total effective zoom, from world
        -- pixels to display backbuffer pixels. this is used to size the lighting
        -- buffer because its resolution is 1:1 with screen pixels.
        this.effectiveZoom := this.zoomFactor / From_Target_Pixels( 1.0 );

        Al_Store_State( alState, ALLEGRO_STATE_TRANSFORM );

        -- round viewport.x, viewport.y to the nearest pixel
        this.viewX := this.viewport.x;
        this.viewY := this.viewport.y;
        this.viewX := Float'Max( 0.0, Float'Rounding( this.viewX * this.effectiveZoom ) / this.effectiveZoom );    -- don't let it go negative or it will
        this.viewY := Float'Max( 0.0, Float'Rounding( this.viewY * this.effectiveZoom ) / this.effectiveZoom );    -- access tiles at row or col -1

        -- setup the world-to-viewport transformation, drawing relative to world
        -- coordinates (0,0) in layer 0, at the current zoom level.
        this.frontZ := this.Get_Layer_Z( this.Get_Foreground_Layer - 1 );
        Al_Identity_Transform( trans );
        Al_Translate_Transform_3d( trans, -this.viewX, -this.viewY, -this.frontZ );
        Al_Scale_Transform( trans, this.zoomFactor, this.zoomFactor );
        Al_Compose_Transform( trans, Al_Get_Current_Transform.all );
        Al_Use_Transform( trans );

        -- 1. Draw background color
        Rectfill( this.viewX, this.viewY,
                  this.viewX + this.viewport.width, this.viewY + this.viewport.height,
                  this.Get_Background_Z,
                  Lighten( this.style.Get_Color( BACKGROUND_ELEM, state ),
                           this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );

        -- 2. Draw world content
        if this.Is_Map_Loaded and then this.lib.Not_Null and then this.lib.Get.Is_Loaded then
            this.Draw_World;
        end if;

        -- back out the draw depth to be relative to the widget's Z depth. the
        -- following drawing is at the widget's native depth, in front of the
        -- world content.
        Al_Identity_Transform( trans );
        Al_Translate_Transform_3d( trans, 0.0, 0.0, this.frontZ );
        Al_Compose_Transform( trans, Al_Get_Current_Transform.all );
        Al_Use_Transform( trans );

        -- 3. Draw the widget's foreground in front
        A_Widget(this).Draw_Content_Foreground;

        -- 4. Draw child widgets (non-entities)
        A_Widget(this).Draw_Children;

        Al_Restore_State( alState );
    end Draw;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Children( this : access Scene ) is
        clipX, clipY,
        clipW, clipH : Integer;
        state        : Allegro_State;
        trans        : Allegro_Transform;
        winX1, winY1,
        winX2, winY2 : Float;
    begin
        Al_Get_Clipping_Rectangle( clipX, clipY, clipW, clipH );
        for child of this.children loop
            if not child.Get_Attribute( "eid" ).Is_Id and then Intersect( this.viewport, child.geom ) then
                -- the clipping rectangle of the child is calculated in window
                -- coordinates because it is not affected by transformations.
                child.Translate_To_Window( child.viewport.x, child.viewport.y, winX1, winY1 );
                child.Translate_To_Window( child.viewport.x + child.viewport.width, child.viewport.y + child.viewport.height, winX2, winY2 );

                -- the child's clipping rectangle is constrained to the parent's
                -- clipping rectangle.
                winX1 := Float(Integer'Max( clipX, Integer(winX1) ));
                winY1 := Float(Integer'Max( clipY, Integer(winY1) ));

                Al_Set_Clipping_Rectangle( Integer'Max( clipX, Integer(winX1) ),
                                           Integer'Max( clipY, Integer(winY1) ),
                                           Integer'Max( 0, Integer'Min( clipX + clipW - Integer(winX1), Integer(winX2 - winX1) ) ),
                                           Integer'Max( 0, Integer'Min( clipY + clipH - Integer(winY1), Integer(winY2 - winY1) ) ) );
                Al_Store_State( state, ALLEGRO_STATE_TRANSFORM );

                Al_Identity_Transform( trans );
                Al_Translate_Transform_3d( trans, child.geom.x, child.geom.y, child.z );
                Al_Compose_Transform( trans, Al_Get_Current_Transform.all );
                Al_Use_Transform( trans );

                child.Draw;

                Al_Restore_State( state );
            end if;
        end loop;
        Al_Set_Clipping_Rectangle( clipX, clipY, clipW, clipH );
    end Draw_Children;

    ----------------------------------------------------------------------------

    procedure Draw_Scenery_Layer( this  : not null access Scene'Class;
                                  layer : Integer ) is
        tile        : A_Tile;
        bmp         : A_Allegro_Bitmap;
        scenery     : Rectangle := this.viewport;
        speedX      : Float := 1.0;
        speedY      : Float := 1.0;
        repeatX     : Boolean := True;
        repeatY     : Boolean := True;
        marginTop   : Float;
        marginBot   : Float;
        marginLeft  : Float;
        marginRight : Float;
        offsetX     : Float := 0.0;
        offsetY     : Float := 0.0;
        vertices    : Allegro_Vertex_Array(0..3);
    begin
        -- - - - [ SETUP ] - - - - - - - - - - - - - - - - - - - - - - - - - - -

        tile := this.mapInfo.Get_Tile( layer, 0, 0 );
        if tile = null or else tile.Get_Bitmap = null then
            return;     -- nothing to draw
        end if;

        -- don't draw tiles from an atlas because the primitive drawing code
        -- will repeat across the whole atlas page, not just the selected tile
        -- within the atlas. set the "useAtlas" tile attribute False to ensure
        -- a tile can be used in a scenery layer.
        if Al_Is_Sub_Bitmap( tile.Get_Bitmap ) then
            return;
        end if;

        offsetX     := Cast_Float( this.map.Get_Layer_Property( layer, PROP_OFFSETX ), 0.0 );
        offsetY     := Cast_Float( this.map.Get_Layer_Property( layer, PROP_OFFSETY ), 0.0 );
        marginTop   := Cast_Float( this.map.Get_Layer_Property( layer, PROP_MARGINT ), 0.0 );
        marginBot   := Cast_Float( this.map.Get_Layer_Property( layer, PROP_MARGINB ), 0.0 );
        marginLeft  := Cast_Float( this.map.Get_Layer_Property( layer, PROP_MARGINL ), 0.0 );
        marginRight := Cast_Float( this.map.Get_Layer_Property( layer, PROP_MARGINR ), 0.0 );

        if not Cast_Boolean( this.map.Get_Layer_Property( layer, PROP_STRETCH ) ) then
            speedX  := Cast_Float( this.map.Get_Layer_Property( layer, PROP_SPEEDX    ), 1.0 );
            speedY  := Cast_Float( this.map.Get_Layer_Property( layer, PROP_SPEEDY    ), 1.0 );
            repeatX := Cast_Boolean( this.map.Get_Layer_Property( layer, PROP_REPEATX ), True );
            repeatY := Cast_Boolean( this.map.Get_Layer_Property( layer, PROP_REPEATY ), True );

            -- bounds of scenery drawing in world coordinates
            scenery.x := Float'Max( marginLeft, scenery.x );
            scenery.y := Float'Max( marginTop,  scenery.y );
            scenery.width  := Float'Min( scenery.width,  (Float(this.map.Get_Width  * this.tileWidth) - marginRight) - scenery.x );
            scenery.height := Float'Min( scenery.height, (Float(this.map.Get_Height * this.tileWidth) - marginBot  ) - scenery.y );
        end if;

        -- v0 ----- v1
        -- |       / |
        -- |     /   |    Vertices
        -- |   /     |    Orientation
        -- | /       |
        -- v2 ----- v3

        vertices(0).u := (scenery.x + this.viewport.width / 2.0) * speedX - this.viewport.width / 2.0 - offsetX - marginLeft;
        vertices(0).v := (scenery.y + this.viewport.height / 2.0) * speedY - this.viewport.height / 2.0 - offsetY - marginTop;
        vertices(0).x := scenery.x;
        vertices(0).y := scenery.y;
        vertices(0).z := this.Get_Layer_Z( layer );        -- relative to layer 0

        vertices(3).u := vertices(0).u + scenery.width;
        vertices(3).v := vertices(0).v + scenery.height;
        vertices(3).x := vertices(0).x + scenery.width;
        vertices(3).y := vertices(0).y + scenery.height;
        vertices(3).z := vertices(0).z;

        -- stretch the image to fill the viewport instead
        if Cast_Boolean( this.map.Get_Layer_Property( layer, PROP_STRETCH ) ) then
            vertices(0).u := -offsetX;
            vertices(0).v := -offsetY;
            vertices(0).x := scenery.x + marginLeft / this.zoomFactor;
            vertices(0).y := scenery.y + marginTop / this.zoomFactor;

            vertices(3).u := vertices(0).u + tile.Get_Width;
            vertices(3).v := vertices(0).v + tile.Get_Height;
            vertices(3).x := vertices(0).x + scenery.width - (marginLeft + marginRight) / this.zoomFactor;
            vertices(3).y := vertices(0).y + scenery.height - (marginTop + marginBot) / this.zoomFactor;
        else
            -- when drawing in non-repeating mode, we use the scrolled
            -- texture coordinates that we already calculated to determine
            -- where the screen coordinates of u,v = 0,0 would be instead.
            -- Notice that .x is calculated based on the scrolled .u, and .u
            -- is set to 0.
            if not repeatX and tile.Get_Width > 0.0 then
                vertices(0).x := marginLeft + (vertices(0).u - ((vertices(0).u / tile.Get_Width) * tile.Get_Width) * speedX);
                vertices(0).u := -offsetX;

                vertices(3).u := vertices(0).u + tile.Get_Width;
                vertices(3).x := vertices(0).x + tile.Get_Width;
            end if;
            if not repeatY and tile.Get_Height > 0.0 then
                vertices(0).y := marginTop + (vertices(0).v - ((vertices(0).v / tile.Get_Height) * tile.Get_Height) * speedY);
                vertices(0).v := -offsetY;

                vertices(3).v := vertices(0).v + tile.Get_Height;
                vertices(3).y := vertices(0).y + tile.Get_Height;
            end if;
        end if;

        vertices(1).u := vertices(3).u;
        vertices(1).v := vertices(0).v;
        vertices(1).x := vertices(3).x;
        vertices(1).y := vertices(0).y;
        vertices(1).z := vertices(0).z;

        vertices(2).u := vertices(0).u;
        vertices(2).v := vertices(3).v;
        vertices(2).x := vertices(0).x;
        vertices(2).y := vertices(3).y;
        vertices(2).z := vertices(0).z;

        -- - - - [ RENDER ] - - - - - - - - - - - - - - - - - - - - - - - - - -

        case this.renderPass is
            when Render_Color |
                 Render_Shading =>

                -- the tint doesn't matter for the shading pass
                vertices(0).color := this.Get_Layer_Tint( layer );
                bmp := tile.Get_Bitmap;

            when Render_Light =>

                if tile.Get_Light > 0.0 then
                    -- draw an auto-generated emissive light mask
                    vertices(0).color := Make_Grey( tile.Get_Light, 1.0 );
                    bmp := tile.Get_Mask;
                elsif tile.Get_LightMap > 0 then
                    -- draw a separate tile as an emissive light mask
                    vertices(0).color := White;
                    tile := this.lib.Get.Get_Tile( tile.Get_LightMap );
                    bmp := (if tile /= null then tile.Get_Bitmap else null);
                else
                    -- no lighting to draw
                    tile := null;
                    bmp := null;
                end if;

        end case;

        if tile /= null then
            vertices(1).color := vertices(0).color;
            vertices(2).color := vertices(0).color;
            vertices(3).color := vertices(0).color;

            -- draw the scenery rectangle
            Al_Draw_Indexed_Prim( vertices,
                                  bmp,
                                  (0, 1, 2, 3),
                                  ALLEGRO_PRIM_TRIANGLE_STRIP );
        end if;
    end Draw_Scenery_Layer;

    ----------------------------------------------------------------------------

    procedure Draw_Entity( this    : not null access Scene'Class;
                           entity  : not null A_Entity;
                           zOffset : Float ) is
        pragma Unreferenced( this );
        widget : constant A_Widget := entity.Get_Widget;
        left   : Float := widget.Get_Geometry.x;
        top    : Float := widget.Get_Geometry.y;
        backup : Allegro_Transform;
        trans  : Allegro_Transform;
    begin
        To_Target_Pixel_Rounded( left, top );

        Al_Copy_Transform( backup, Al_Get_Current_Transform.all );
        Al_Identity_Transform( trans );
        Al_Translate_Transform_3d( trans, left, top, entity.Get_Z + zOffset );
        Al_Compose_Transform( trans, backup );
        Al_Use_Transform( trans );
        widget.Draw;
        Al_Use_Transform( backup );
    end Draw_Entity;

    ----------------------------------------------------------------------------

    procedure Draw_Tile( this     : not null access Scene'Class;
                         layer    : Integer;
                         col, row : Integer ) is
        tile   : A_Tile;
        x, y   : Float;
        pad    : Float := 0.0;
        width,
        height : Float;
    begin
        tile := this.mapInfo.Get_Tile( layer, col, row );
        if tile = null then
            return;
        end if;

        x := Float(col * this.tileWidth);
        y := Float(row * this.tileWidth);

        case this.renderPass is
            when Render_Color |
                 Render_Shading =>

                -- draw the tile to the scene
                if tile.Get_Bitmap /= null then
                    Al_Draw_Bitmap( tile.Get_Bitmap, x, y, 0 );
                end if;

            when Render_Light =>

                if Float'Rounding( Float(this.tileWidth) * this.effectiveZoom ) / this.effectiveZoom /= Float(this.tileWidth) then
                    -- when the zoom level isn't an exact multiple/divisor, small
                    -- gaps <= 1 screen pixel in width can appear between tiles
                    -- when drawing to the light buffer. stretch the tiles by 1
                    -- screen pixel to cover the gaps.
                    pad := From_Target_Pixels( 1.0 );
                end if;

                -- draw an auto-generated light mask
                if tile.Get_Light > 0.0 then
                    if tile.Get_Mask /= null then
                        width := Float(Al_Get_Bitmap_Width( tile.Get_Mask ));
                        height := Float(Al_Get_Bitmap_Height( tile.Get_Mask ));
                        Al_Draw_Tinted_Scaled_Bitmap( tile.Get_Mask,
                                                      Make_Grey( tile.Get_Light, 1.0 ),
                                                      0.0, 0.0,
                                                      width, height,
                                                      x, y,
                                                      width + pad, height + pad,
                                                      0 );
                    end if;

                -- draw a separate tile as an emissive light mask
                elsif tile.Get_LightMap > 0 then
                    tile := this.lib.Get.Get_Tile( tile.Get_LightMap );
                    if tile /= null and then tile.Get_Bitmap /= null then
                        x := x + Float(tile.Get_LightMapX);
                        y := y + Float(tile.Get_LightMapY);
                        width := Float(Al_Get_Bitmap_Width( tile.Get_Bitmap ));
                        height := Float(Al_Get_Bitmap_Height( tile.Get_Bitmap ));
                        Al_Draw_Tinted_Scaled_Bitmap( tile.Get_Bitmap,
                                                      Make_Grey( tile.Get_Light, 1.0 ),
                                                      0.0, 0.0,
                                                      width, height,
                                                      x, y,
                                                      width + pad, height + pad,
                                                      0 );
                    end if;

                end if;

        end case;
    end Draw_Tile;

    ----------------------------------------------------------------------------

    procedure Draw_World( this : not null access Scene'Class ) is
        use Entity_Lists;
        viewXFloor : constant Float := Float'Floor( this.viewX );
        viewYFloor : constant Float := Float'Floor( this.viewY );
        state      : Allegro_State;
        trans      : Allegro_Transform;
        pos        : Entity_Lists.Cursor;
        z          : Float;
        nextZ      : Float;
        zOrder     : Integer;
        zOffset    : Float;
    begin
        -- area to be drawn in tile coordinates
        this.tileX1 := Integer(Float'Floor( viewXFloor / Float(this.tileWidth) ));
        this.tileY1 := Integer(Float'Floor( viewYFloor / Float(this.tileWidth) ));
        this.tileX2 := Integer'Min( Integer(Float'Floor( (this.viewX + this.viewport.width) / Float(this.tileWidth) )), this.map.Get_Width - 1 );
        this.tileY2 := Integer'Min( Integer(Float'Floor( (this.viewY + this.viewport.height) / Float(this.tileWidth) )), this.map.Get_Height - 1 );

        -- COLOR PASS - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        this.renderPass := Render_Color;

        -- draw tile layers to temporary buffers to avoid visible artifacts that
        -- would appear between tiles if they were all drawn to the target.
        Al_Store_State( state, ALLEGRO_STATE_TARGET_BITMAP );
        this.Create_Layer_Bitmaps;
        for layer in this.Get_Foreground_Layer..this.Get_Background_Layer loop
            if this.Is_Layer_Visible( layer ) and then this.map.Get_Layer_Type( layer ) = Layer_Tiles then
                Set_Target_Bitmap( this.tileLayers.Element( layer ) );
                Clear_To_Color( Transparent );

                Al_Identity_Transform( trans );
                Al_Scale_Transform( trans, 1.0 / this.tileScaleX, 1.0 / this.tileScaleY );
                Al_Use_Transform( trans );

                Al_Identity_Transform( trans );
                Al_Translate_Transform( trans, -viewXFloor, -viewYFloor );
                Al_Compose_Transform( trans, Al_Get_Current_Transform.all );
                Al_Use_Transform( trans );

                Al_Hold_Bitmap_Drawing( True );
                for row in this.tileY1..this.tileY2 loop
                    for col in this.tileX1..this.tileX2 loop
                        this.Draw_Tile( layer, col, row );
                    end loop;
                end loop;
                Al_Hold_Bitmap_Drawing( False );
            end if;
        end loop;
        Al_Restore_State( state );

        -- draw each layer to the target bitmap in front-to-back order to reduce
        -- overdraw. this prevents tiles from using translucency- pixels must be
        -- fully opque or fully transparent.
        for layer in this.Get_Foreground_Layer..this.Get_Background_Layer loop
            if this.Is_Layer_Visible( layer ) then
                case this.map.Get_Layer_Type( layer ) is

                    when Layer_Tiles =>
                        Draw_Bitmap_Stretched( this.tileLayers.Element( layer ),
                                               viewXFloor, viewYFloor,
                                               this.Get_Layer_Z( layer ),
                                               Float(Al_Get_Bitmap_Width( this.tileLayers.Element( layer ) )) * this.tileScaleX,
                                               Float(Al_Get_Bitmap_Height( this.tileLayers.Element( layer ) )) * this.tileScaleY,
                                               Stretch,
                                               0.0,
                                               this.Get_Layer_Tint( layer ) );

                    when Layer_Scenery =>
                        this.Draw_Scenery_Layer( layer );

                end case;
            end if;
        end loop;

        -- draw translucent things back to front
        -- entities and particles must be interleaved so they can both support
        -- translucency.
        pos := this.entities.First;
        for layer in reverse this.Get_Foreground_Layer..this.Get_Background_Layer loop
            z := this.Get_Layer_Z( layer );
            nextZ := this.Get_Layer_Z( layer - 1 );

            -- Draw editing overlay in this layer (may contain translucency)
            this.Draw_Layer_Overlay( layer );

            -- Draw entities in this layer
            zOffset := -Renderers.Z_PRECISION;
            zOrder := Integer'Last;
            if this.Is_Enabled( SHOW_SPRITES ) and not this.Is_Enabled( SHOW_SPRITES_FRONT ) then
                while Has_Element( pos ) and then nextZ < Element( pos ).Get_Z and then Element( pos ).Get_Z <= z loop
                    if Intersect( this.Get_Viewport, Element( pos ).Get_Widget.Get_Geometry ) then
                        if Element( pos ).Get_ZOrder < zOrder then
                            zOrder := Element( pos ).Get_ZOrder;
                            zOffset := zOffset - Renderers.Z_PRECISION;
                        end if;
                        this.Draw_Entity( Element( pos ), zOffset );
                    end if;
                    Next( pos );
                end loop;
            end if;

            -- Draw particles in this layer in front of entities
            if this.Is_Enabled( SHOW_PARTICLES ) then
                this.particles.Draw_Layer( z, zOffset - Renderers.Z_PRECISION, this.viewport, this.renderPass );
            end if;
        end loop;

        -- initialize the lighting system for this frame
        if this.Is_Enabled( SHOW_LIGHTING ) then

            this.lights.Frame_Begin( (this.viewport.width + 1.0) * this.effectiveZoom,
                                     (this.viewport.height + 1.0) * this.effectiveZoom );

            -- create the world transform to use for the light accumulation buffer
            Al_Identity_Transform( trans );
            Al_Scale_Transform_3d( trans, 1.0, 1.0, Renderers.Z_SCALE );
            Al_Translate_Transform_3d( trans, -this.viewX, -this.viewY, 0.0 );
            Al_Scale_Transform( trans, this.effectiveZoom, this.effectiveZoom );
            Al_Use_Transform( trans );

            -- SHADING PASS - - - - - - - - - - - - - - - - - - - - - - - - - -

            this.renderPass := Render_Shading;
            this.lights.Pass_Begin( this.renderPass );

            -- Draw each layer to the target bitmap in front-to-back order
            for layer in this.Get_Foreground_Layer..this.Get_Background_Layer loop
                if this.Is_Layer_Visible( layer ) then
                    case this.map.Get_Layer_Type( layer ) is

                        when Layer_Tiles =>
                            Draw_Bitmap( this.tileLayers.Element( layer ),
                                         viewXFloor, viewYFloor,
                                         this.Get_Layer_Z( layer ),
                                         Black );

                        when Layer_Scenery =>
                            this.Draw_Scenery_Layer( layer );

                    end case;
                end if;
            end loop;

            -- Draw entities and particles in back-to-front order
            pos := this.entities.First;
            for layer in reverse this.Get_Foreground_Layer..this.Get_Background_Layer loop
                z := this.Get_Layer_Z( layer );
                nextZ := this.Get_Layer_Z( layer - 1 );

                -- Draw entities in this layer
                zOffset := -Renderers.Z_PRECISION;
                zOrder := Integer'Last;
                if this.Is_Enabled( SHOW_SPRITES ) and not this.Is_Enabled( SHOW_SPRITES_FRONT ) then
                    while Has_Element( pos ) and then nextZ < Element( pos ).Get_Z and then Element( pos ).Get_Z <= z loop
                        if Intersect( this.Get_Viewport, Element( pos ).Get_Widget.Get_Geometry ) then
                            if Element( pos ).Get_ZOrder < zOrder then
                                zOrder := Element( pos ).Get_ZOrder;
                                zOffset := zOffset - Renderers.Z_PRECISION;
                            end if;
                            this.Draw_Entity( Element( pos ), zOffset );
                        end if;
                        Next( pos );
                    end loop;
                end if;

                -- Draw particles in front of entities in the same layer
                if this.Is_Enabled( SHOW_PARTICLES ) then
                    this.particles.Draw_Layer( z, zOffset - Renderers.Z_PRECISION, this.viewport, this.renderPass );
                end if;
            end loop;

            -- LIGHTING PASS - - - - - - - - - - - - - - - - - - - - - - - - - -

            this.renderPass := Render_Light;
            this.lights.Pass_Begin( this.renderPass );

            pos := this.entities.First;
            for layer in reverse this.Get_Foreground_Layer..this.Get_Background_Layer loop
                z := this.Get_Layer_Z( layer );
                nextZ := this.Get_Layer_Z( layer - 1 );

                -- Draw emissive map tiles in this layer
                if this.Is_Layer_Visible( layer ) then
                    case this.map.Get_Layer_Type( layer ) is

                        when Layer_Tiles =>
                            Al_Store_State( state, ALLEGRO_STATE_TRANSFORM );
                            Al_Identity_Transform( trans );
                            Al_Translate_Transform_3d( trans, 0.0, 0.0, z );
                            Al_Compose_Transform( trans, Al_Get_Current_Transform.all );
                            Al_Use_Transform( trans );

                            Al_Hold_Bitmap_Drawing( True );
                            for row in this.tileY1..this.tileY2 loop
                                for col in this.tileX1..this.tileX2 loop
                                    this.Draw_Tile( layer, col, row );
                                end loop;
                            end loop;
                            Al_Hold_Bitmap_Drawing( False );

                            Al_Restore_State( state );

                        when Layer_Scenery =>
                            this.Draw_Scenery_Layer( layer );

                    end case;
                end if;

                -- Draw all entities in this layer
                zOffset := -Renderers.Z_PRECISION;
                zOrder := Integer'Last;
                if this.Is_Enabled( SHOW_SPRITES ) and not this.Is_Enabled( SHOW_SPRITES_FRONT ) then
                    while Has_Element( pos ) and then nextZ < Element( pos ).Get_Z and then Element( pos ).Get_Z <= z loop
                        if Intersect( this.Get_Viewport, Element( pos ).Get_Widget.Get_Geometry ) then
                            if Element( pos ).Get_ZOrder < zOrder then
                                zOrder := Element( pos ).Get_ZOrder;
                                zOffset := zOffset - Renderers.Z_PRECISION;
                            end if;
                            this.Draw_Entity( Element( pos ), zOffset );
                        end if;
                        Next( pos );
                    end loop;
                end if;

                -- Draw emissive particles in this layer
                if this.Is_Enabled( SHOW_PARTICLES ) then
                    this.particles.Draw_Layer( z, zOffset - Renderers.Z_PRECISION, this.viewport, this.renderPass );
                end if;
            end loop;

            -- Draw point and area lights
            this.lights.Draw_Lights( Rectangle'(this.viewX, this.viewY,
                                                this.viewport.width, this.viewport.height) );

            -- Shade the world with the light buffer
            -- 'effectiveZoom' translates world units to screen pixels
            this.lights.Frame_End( this.viewX, this.viewY, this.frontZ, this.effectiveZoom );
        end if;

        -- draw all entities in front, so they are unaffected by light and cannot
        -- by hidden by tile layers. this is used by the editor.
        if this.Is_Enabled( SHOW_SPRITES_FRONT ) and this.Is_Enabled( SHOW_SPRITES ) then
            this.renderPass := Render_Color;
            pos := this.entities.First;
            while Has_Element( pos ) loop
                if Intersect( this.Get_Viewport, Element( pos ).Get_Widget.Get_Geometry ) then
                    this.Draw_Entity( Element( pos ),
                                      this.frontZ - Element( pos ).Get_Z );
                end if;
                Next( pos );
            end loop;
        end if;

        this.renderPass := Render_Color;
    end Draw_World;

    ----------------------------------------------------------------------------

    procedure Enable_Option( this    : not null access Scene'Class;
                             opt     : Render_Options;
                             enabled : Boolean := True ) is
    begin
        if enabled then
            this.options := this.options or opt;
        else
            this.options := this.options and (not opt);
        end if;

        -- pass the option on to the lighting system, in case it's a lighting option
        this.lights.Enable_Option( opt, enabled );
    end Enable_Option;

    ----------------------------------------------------------------------------

    function Find_Entity( this : not null access Scene'Class;
                          id   : Entity_Id ) return A_Entity is
        pos : constant Entity_Maps.Cursor := this.entityMap.Find( id );
    begin
        return (if Has_Element( pos ) then Element( pos ) else null);
    end Find_Entity;

    ----------------------------------------------------------------------------

    procedure Set_Focus( this   : not null access Scene'Class;
                         x, y   : Float;
                         center : Boolean := False ) is
    begin
        this.cam.Set_Focus( x, y, center );
    end Set_Focus;

    ----------------------------------------------------------------------------

    function Get_Background_Layer( this : not null access Scene'Class ) return Integer
        is (if this.Is_Map_Loaded then this.map.Get_Background_Layer else 0);

    ----------------------------------------------------------------------------

    function Get_Background_Z( this : not null access Scene'Class ) return Float
        is (if this.Is_Map_Loaded then this.Get_Layer_Z( this.map.Get_Background_Layer ) else 0.0);

    ----------------------------------------------------------------------------

    function Get_Foreground_Layer( this : not null access Scene'Class ) return Integer
        is (if this.Is_Map_Loaded then this.map.Get_Foreground_Layer else 0);

    ----------------------------------------------------------------------------

    function Get_Foreground_Z( this : not null access Scene'Class ) return Float
        is (if this.Is_Map_Loaded then this.Get_Layer_Z( this.map.Get_Foreground_Layer ) else 0.0);

    ----------------------------------------------------------------------------

    function Get_Height_Tiles( this : not null access Scene'Class ) return Natural
        is (if this.Is_Map_Loaded then this.map.Get_Height else 0);

    ----------------------------------------------------------------------------

    function Get_Layer_Count( this : not null access Scene'Class ) return Natural
        is (if this.Is_Map_Loaded then this.map.Get_Layer_Count else 0);

    ----------------------------------------------------------------------------

    function Get_Layer_Data( this  : not null access Scene'Class;
                             layer : Integer ) return A_Map is
    begin
        return this.map.Copy_Layer_Data( layer );
    end Get_Layer_Data;

    ----------------------------------------------------------------------------

    function Get_Layer_Properties( this  : not null access Scene'Class;
                                   layer : Integer ) return Map_Value
        is (this.map.Get_Layer_Properties( layer ));

    ----------------------------------------------------------------------------

    function Get_Layer_Property( this  : not null access Scene'Class;
                                 layer : Integer;
                                 name  : String ) return Value
        is (this.map.Get_Layer_Property( layer, name ));

    ----------------------------------------------------------------------------

    function Get_Layer_Tint( this : not null access Scene'Class; layer : Integer ) return Allegro_Color is
        tint : Allegro_Color := White;
    begin
        if this.map /= null then
            -- the layer's native (primary) tint
            tint := Coerce_Color( this.map.Get_Layer_Property( layer, PROP_TINT ), tint );

            -- set the opacity as the multiplication of the tint's alpha (default 1)
            -- and the opacity property (default 1).
            tint := Opacity( tint, Cast_Float( this.map.Get_Layer_Property( layer, PROP_OPACITY ), 1.0 ) * Get_Alpha( tint ) );

            if this.Is_Enabled( SHOW_LAYER_TINT ) then
                -- multiply the tint by the secondary tint as well
                tint := Multiply( tint, Cast_Color( this.map.Get_Layer_Property( layer, "_tint2" ), White ) );
            end if;
        end if;

        return tint;
    end Get_Layer_Tint;

    ----------------------------------------------------------------------------

    function Get_Layer_Type( this  : not null access Scene'Class;
                             layer : Integer ) return Layer_Type
        is (this.map.Get_Layer_Type( layer ));

    ----------------------------------------------------------------------------

    function Get_Layer_Z( this  : not null access Scene'Class;
                          layer : Integer ) return Float
        is (Float(layer) * Maps.LAYER_Z_SPACING);

    ----------------------------------------------------------------------------

    function Get_Library( this : not null access Scene'Class ) return String
        is (if this.lib.Not_Null then this.lib.Get.Get_Name else "");

    ----------------------------------------------------------------------------

    function Get_Map( this : not null access Scene'Class ) return A_Map is (this.map);

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Height( this : access Scene ) return Float is
    begin
        if this.Is_Map_Loaded then
            return Float(this.map.Get_Height * this.tileWidth);
        end if;
        return 0.0;
    end Get_Min_Height;

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Width( this : access Scene ) return Float is
    begin
        if this.Is_Map_Loaded then
            return Float(this.map.Get_Width * this.tileWidth);
        end if;
        return 0.0;
    end Get_Min_Width;

    ----------------------------------------------------------------------------

    function Get_Particle_System( this : not null access Scene'Class ) return A_Particle_System is (this.particles);

    ----------------------------------------------------------------------------

    function Get_Render_Options( this : not null access Scene'Class ) return Render_Options is (this.options);

    ----------------------------------------------------------------------------

    function Get_Render_Pass( this : not null access Scene'Class ) return Render_Pass is (this.renderPass);

    ----------------------------------------------------------------------------

    function Get_Tile_Width( this : not null access Scene'Class ) return Natural is (this.tileWidth);

    ----------------------------------------------------------------------------

    function Get_Width_Tiles( this : not null access Scene'Class ) return Natural
    is (if this.Is_Map_Loaded then this.map.Get_Width else 0);

    ----------------------------------------------------------------------------

    function Get_World_Property( this : not null access Scene'Class;
                                 name : String ) return Value is (this.worldProps.Get( name ));

    ----------------------------------------------------------------------------

    overriding
    function Get_Visual_State( this : access Scene ) return Widget_State is
    (
        (if not this.Is_Enabled then DISABLED_STATE else 0)
    );

    ----------------------------------------------------------------------------

    procedure Handle_Entity_Attribute_Changed( this : not null access Scene'Class;
                                               evt  : not null A_Entity_Attribute_Event ) is
        entity : A_Entity;
    begin
        entity := this.Find_Entity( evt.Get_Id );
        if entity /= null then
            entity.Set_Attribute( evt.Get_Attribute, evt.Get_Value, consume => False );
            -- the scene entity might not be found because the event was
            -- generated by an invisible game entity.
        end if;
    end Handle_Entity_Attribute_Changed;

    ----------------------------------------------------------------------------

    procedure Handle_Entity_Spawned( this : not null access Scene'Class;
                                     evt  : not null A_Entity_Spawn_Event ) is
        entity : A_Entity;
        widget : A_Widget;
    begin
        if evt.Get_State.Get( Family_Name( VISIBLE_ID ) ).Is_Map then

            -- create the scene entity that represents the game entity
            entity := Create_Entity( evt.Get_Id, evt.Get_Entity_Name, evt.Get_Template, evt.Get_State );

            -- create the visual representation widget
            widget := this.Create_Entity_Widget( entity, evt.Get_State );
            if widget /= null then
                widget.Set_Style( this.style.Get_Name );
                widget.Set_Attribute( "eid", To_Id_Value( evt.Get_Id ) );
            end if;

            -- add the widget to the entity and the entity to the scene
            entity.Set_Widget( widget );
            this.Add_Entity( entity );

            -- update camera position if this is the followed entity
            if evt.Get_Id = this.target then
                this.cam.Set_Focus( entity.Get_Render_X, entity.Get_Render_Y, center => True );
            end if;

            -- the player is not deletable
            if entity.Get_Entity_Id = To_Tagged_Id( this.Get_World_Property( "player" ) ) then
                entity.Set_Deletable( False );
            end if;
        end if;
    end Handle_Entity_Spawned;

    ----------------------------------------------------------------------------

    procedure Handle_Entity_Deleted( this : not null access Scene'Class;
                                     evt  : not null A_Entity_Event ) is
        entity : A_Entity := this.Find_Entity( evt.Get_Id );
    begin
        if entity /= null then
            this.Delete_Entity( entity );
        end if;
    end Handle_Entity_Deleted;

    ----------------------------------------------------------------------------

    procedure Handle_Entity_Moved( this : not null access Scene'Class;
                                   evt  : not null A_Entity_Moved_Event ) is
        entity : A_Entity;
    begin
        -- move the entity
        entity := this.Find_Entity( evt.Get_Id );
        if entity /= null then
            entity.Set_Entity_Location( evt.Get_X, evt.Get_Y, evt.Get_VX, evt.Get_VY );

            -- Note: Render location is not updated until the next frame
        end if;
    end Handle_Entity_Moved;

    ----------------------------------------------------------------------------

    procedure Handle_Entity_Resized( this : not null access Scene'Class;
                                     evt  : not null A_Entity_Size_Event ) is
        entity : constant A_Entity := this.Find_Entity( evt.Get_Id );
    begin
        if entity /= null and then entity.Is_Updatable then
            entity.Resize( Float(evt.Get_Width), Float(evt.Get_Height) );
        end if;
    end Handle_Entity_Resized;

    ----------------------------------------------------------------------------

    procedure Handle_Entity_Z_Changed( this : not null access Scene'Class;
                                       evt  : not null A_Entity_Z_Event ) is
        entity : constant A_Entity := this.Find_Entity( evt.Get_Id );
    begin
        if entity /= null and then (entity.Get_Z /= evt.Get_Z or entity.Get_ZOrder /= evt.Get_ZOrder) then
            -- the entity list is ordered by Z depth so the entity needs to be
            -- removed and re-inserted in the correct place.
            this.entities.Delete( entity );
            entity.Set_Z( evt.Get_Z );
            entity.Set_ZOrder( evt.Get_ZOrder );
            this.entities.Include( entity );
        end if;
    end Handle_Entity_Z_Changed;

    ----------------------------------------------------------------------------

    -- if evt is returned as null then the event was consumed
    overriding
    procedure Handle_Event( this : access Scene;
                            evt  : in out A_Event;
                            resp : out Response_Type ) is
        pragma Unreferenced( resp );
    begin
        if not this.Is_Enabled then
            return;
        end if;
        case evt.Get_Event_Id is
            when EVT_FRAME_CHANGED            => this.Handle_Frame_Changed( A_Frame_Changed_Event(evt) );
            when EVT_ENTITY_MOVED             => this.Handle_Entity_Moved( A_Entity_Moved_Event(evt) );
            when EVT_TILE_CHANGED             => this.Handle_Tile_Changed( A_Tile_Event(evt) );
            when EVT_ENTITY_ATTRIBUTE_CHANGED => this.Handle_Entity_Attribute_Changed( A_Entity_Attribute_Event(evt) );
            when EVT_ENTITY_SPAWNED           => A_Scene(this).Handle_Entity_Spawned( A_Entity_Spawn_Event(evt) );
            when EVT_ENTITY_DELETED           => this.Handle_Entity_Deleted( A_Entity_Event(evt) );
            when EVT_ENTITY_RESIZED           => this.Handle_Entity_Resized( A_Entity_Size_Event(evt) );
            when EVT_ENTITY_Z_CHANGED         => this.Handle_Entity_Z_Changed( A_Entity_Z_Event(evt) );
            when EVT_WORLD_PROPERTY_CHANGED   => this.Handle_World_Property_Changed( A_World_Property_Event(evt) );
            when EVT_WORLD_LOADED             => this.Handle_World_Loaded( A_World_Loaded_Event(evt) );
            when EVT_LAYER_CREATED            => this.Handle_Layer_Created( A_Layer_Created_Event(evt) );
            when EVT_LAYER_DELETED            => this.Handle_Layer_Deleted( A_Layer_Event(evt) );
            when EVT_LAYER_PROPERTY_CHANGED   => this.Handle_Layer_Property_Changed( A_Layer_Property_Event(evt) );
            when EVT_LAYERS_SWAPPED           => this.Handle_Layers_Swapped( A_Layer_Swap_Event(evt) );
            when others                       => null;
        end case;
    end Handle_Event;

    ----------------------------------------------------------------------------

    procedure Handle_Frame_Changed( this : not null access Scene'Class;
                                    evt  : not null A_Frame_Changed_Event ) is
        entity : constant A_Entity := this.Find_Entity( evt.Get_Id );
    begin
        if entity /= null and then entity.Is_Updatable then
            A_Sprite(entity.Get_Widget).Set_Frame( evt.Get_Frame );
        end if;
    end Handle_Frame_Changed;

    ----------------------------------------------------------------------------

    procedure Handle_Layer_Created( this : not null access Scene'Class;
                                    evt  : not null A_Layer_Created_Event ) is
    begin
        if this.map /= null then
            this.map.Add_Layer( evt.Get_Layer,
                                evt.Get_Type,
                                evt.Get_Properties,
                                evt.Get_Layer_Data );
            this.tileLayers.Insert_Symmetric( evt.Get_Layer, null );

            -- delete layer zero's tile bitmap to ensure the tile bitmaps will
            -- be refreshed on the next draw. this is a hack to create a tile
            -- layer bitmap for the new layer, if needed, in the correct size.
            Al_Destroy_Bitmap( this.tileLayers.Reference( 0 ) );
        end if;
    end Handle_Layer_Created;

    ----------------------------------------------------------------------------

    procedure Handle_Layer_Deleted( this : not null access Scene'Class;
                                    evt  : not null A_Layer_Event ) is
    begin
        if this.map /= null then
            if this.map.Delete_Layer( evt.Get_Layer ) then
                Al_Destroy_Bitmap( this.tileLayers.Reference( evt.Get_Layer ) );
                this.tileLayers.Delete( evt.Get_Layer );
            end if;
        end if;
    end Handle_Layer_Deleted;

    ----------------------------------------------------------------------------

    procedure Handle_Layer_Property_Changed( this : not null access Scene'Class;
                                             evt  : not null A_Layer_Property_Event ) is
    begin
        if this.map /= null then
            this.map.Set_Layer_Property( evt.Get_Layer, evt.Get_Property_Name, evt.Get_Value );
        end if;
    end Handle_Layer_Property_Changed;

    ----------------------------------------------------------------------------

    procedure Handle_Layers_Swapped( this : not null access Scene'Class;
                                     evt  : not null A_Layer_Swap_Event ) is
    begin
        if this.map /= null then
            if this.map.Swap_Layers( evt.Get_Layer, evt.Get_Other_Layer ) then
                this.tileLayers.Swap( evt.Get_Layer, evt.Get_Other_Layer );
            end if;
        end if;
    end Handle_Layers_Swapped;

    ----------------------------------------------------------------------------

    procedure Handle_Tile_Changed( this : not null access Scene'Class;
                                   evt  : not null A_Tile_Event ) is
        oldShape : Clip_Type;
    begin
        if this.Is_Map_Loaded then
            for t of evt.Get_List loop
                oldShape := this.mapInfo.Get_Shape( t.col, t.row );
                this.map.Set_Tile( t.layer, t.col, t.row, t.id );
                if oldShape /= this.mapInfo.Get_Shape( t.col, t.row ) then
                    this.lights.Update_Geometry( this.mapInfo, this.tileWidth, t.col, t.row );
                end if;
                A_Scene(this).On_Tile_Changed( t.layer, t.col, t.row );
            end loop;
        end if;
    end Handle_Tile_Changed;

    ----------------------------------------------------------------------------

    procedure Handle_World_Loaded( this : not null access Scene'Class;
                                   evt  : A_World_Loaded_Event ) is
        widget : A_Widget;
    begin
        for e of this.entityMap loop
            widget := e.Get_Widget;
            this.Delete_Child( widget );
            Delete( e );
        end loop;
        this.entityMap.Clear;
        this.entities.Clear;
        this.particles.Clear;
        this.lights.Clear;
        for l in this.tileLayers.First..this.tileLayers.Last loop
            Al_Destroy_Bitmap( this.tileLayers.Reference( l ) );
        end loop;
        this.tileLayers.Clear;

        this.tileWidth := 0;
        this.lib.Unset;
        this.worldProps := Create_Map.Map;
        Delete( this.map );

        if evt /= null then
            this.map := Copy( evt.Get_Map );

            this.lib := Load_Library( evt.Get_Library_Name, bitmaps => True );
            this.tileWidth := evt.Get_Tile_Width;

            this.mapInfo.Initialize( this.map, this.lib );
            this.lights.Initialize( this.mapInfo, this.tileWidth );

            for l in this.map.Get_Foreground_Layer..this.map.Get_Background_Layer loop
                this.tileLayers.Append( null );
            end loop;
            this.tileLayers.Set_Center( -this.map.Get_Foreground_Layer );
        end if;

        this.Set_Target( NULL_ID, 0.0, 0.0, 0.0, 0.0 );
        this.Zoom.Set( 1.0 );

        -- focus on the center of the world by default
        this.cam.Set_Focus( Float(this.Get_Width_Tiles * this.Get_Tile_Width) / 2.0,
                            Float(this.Get_Height_Tiles * this.Get_Tile_Width) / 2.0,
                            center => True );

        this.MapChanged.Emit;
    end Handle_World_Loaded;

    ----------------------------------------------------------------------------

    procedure Handle_World_Property_Changed( this : not null access Scene'Class;
                                             evt  : not null A_World_Property_Event ) is
    begin
        this.worldProps.Set( evt.Get_Property_Name, evt.Get_Value );
        this.sigWorldProp.Emit( evt.Get_Property_Name );
    end Handle_World_Property_Changed;

    ----------------------------------------------------------------------------

    function Is_Enabled( this : not null access Scene'Class; opt : Render_Options ) return Boolean is ((this.options and opt) = opt);

    ----------------------------------------------------------------------------

    function Is_Layer_Visible( this  : not null access Scene'Class;
                               layer : Integer ) return Boolean is
    begin
        if this.Is_Enabled( SHOW_ALL_LAYERS ) then
            return True;
        end if;

        return this.Is_Map_Loaded and then
               Cast_Boolean( this.map.Get_Layer_Property( layer, "_visible" ), True ) and then
               Cast_Boolean( this.map.Get_Layer_Property( layer, "_visible2" ), True );
    end Is_Layer_Visible;

    ----------------------------------------------------------------------------

    function Is_Map_Loaded( this : not null access Scene'Class ) return Boolean is (this.map /= null);

    ----------------------------------------------------------------------------

    function Is_Tile_Visible( this     : not null access Scene'Class;
                              layer    : Integer;
                              col, row : Integer ) return Boolean is
        tileX1, tileY1,
        tileX2, tileY2  : Integer;
    begin
        if this.Is_Layer_Visible( layer ) then
            tileX1 := Integer(Float'Floor( this.viewport.x / Float(this.tileWidth) ));
            tileY1 := Integer(Float'Floor( this.viewport.y / Float(this.tileWidth) ));
            tileX2 := Integer'Min( Integer(Float'Floor( (this.viewport.x + this.viewport.width) / Float(this.tileWidth) )), this.map.Get_Width - 1 );
            tileY2 := Integer'Min( Integer(Float'Floor( (this.viewport.y + this.viewport.height) / Float(this.tileWidth) )), this.map.Get_Height - 1 );

            return col >= tileX1 and then col <= tileX2 and then
                   row >= tileY1 and then row <= tileY2;
        end if;
        return False;
    end Is_Tile_Visible;

    ----------------------------------------------------------------------------

    procedure Iterate_Entities( this    : not null access Scene'Class;
                                examine : access procedure( entity : A_Entity ) ) is
        procedure Examine2( pos : Entity_Maps.Cursor ) is
        begin
            examine.all( Element( pos ) );
        end Examine2;
    begin
        this.entityMap.Iterate( Examine2'Access );
    end Iterate_Entities;

    ----------------------------------------------------------------------------

    procedure On_Disabled( this : not null access Scene'Class ) is
    begin
        this.lights.Freeze_Updates( True );
        this.particles.Freeze_Updates( True );
    end On_Disabled;

    ----------------------------------------------------------------------------

    procedure On_Enabled( this : not null access Scene'Class ) is
    begin
        this.lights.Freeze_Updates( False );
        this.particles.Freeze_Updates( False );
    end On_Enabled;

    ----------------------------------------------------------------------------

    procedure On_Resized( this : not null access Scene'Class ) is
    begin
        if this.Is_Map_Loaded then
            -- refocus on the focal point prior to the resize
            this.cam.Set_Focus( this.cam.Get_FocusX, this.cam.Get_FocusY, center => True );
        end if;
    end On_Resized;

    ----------------------------------------------------------------------------

    procedure Remove_Widget( this  : not null access Scene'Class;
                             child : not null access Widget'Class ) is
    begin
        child.Visible.Set( False );
        this.Remove_Child( child );
    end Remove_Widget;

    ----------------------------------------------------------------------------

    procedure Set_Layer_Secondary_Tint( this  : not null access Scene'Class;
                                        layer : Integer;
                                        tint  : Allegro_Color ) is
    begin
        if this.Is_Map_Loaded then
            this.map.Set_Layer_Property( layer, "_tint2", Create( tint ) );
        end if;
    end Set_Layer_Secondary_Tint;

    ----------------------------------------------------------------------------

    procedure Set_Layer_Secondary_Visible( this    : not null access Scene'Class;
                                           layer   : Integer;
                                           visible : Boolean ) is
    begin
        if this.Is_Map_Loaded then
            this.map.Set_Layer_Property( layer, "_visible2", Create( visible ) );
        end if;
    end Set_Layer_Secondary_Visible;

    ----------------------------------------------------------------------------

    procedure Set_Layer_Visible( this    : not null access Scene'Class;
                                 layer   : Integer;
                                 visible : Boolean ) is
    begin
        if this.Is_Map_Loaded then
            this.map.Set_Layer_Property( layer, "_visible", Create( visible ) );
        end if;
    end Set_Layer_Visible;

    ----------------------------------------------------------------------------

    procedure Set_Target( this     : not null access Scene'Class;
                          target   : Entity_Id;
                          boundsX1,
                          boundsY1,
                          boundsX2,
                          boundsY2 : Float ) is
        entity : A_Entity;
    begin
        this.cam.Set_Bounds( boundsX1, boundsY1, boundsX2, boundsY2 );
        if this.target /= target then
            pragma Debug( Dbg( "Scene: Target = " & Image( target ), D_GUI, Info ) );
        end if;

        entity := this.Find_Entity( target );
        if entity /= null then
            this.cam.Set_Focus( entity.Get_Render_X, entity.Get_Render_Y, center => (this.target /= target) );
        end if;
        this.target := target;
    end Set_Target;

    ----------------------------------------------------------------------------

    overriding
    procedure Tick( this : access Scene; time : Tick_Time ) is
    begin
        if this.Is_Map_Loaded then
            this.cam.Tick( time );
        end if;
    end Tick;

    ----------------------------------------------------------------------------

    procedure Unload_World( this : not null access Scene'Class ) is
    begin
        this.Handle_World_Loaded( null );
    end Unload_World;

begin

    Register_Style( "Scene",
                    (BACKGROUND_ELEM => To_Unbounded_String( "background" )),
                    (BACKGROUND_ELEM => Area_Element),
                    (0 => To_Unbounded_String( "disabled" )) );

end Widgets.Scenes;
