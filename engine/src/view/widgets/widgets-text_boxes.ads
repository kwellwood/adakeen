--
-- Copyright (c) 2014-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Indefinite_Doubly_Linked_Lists;

package Widgets.Text_Boxes is

    -- A Textbox is a multi-line text display widget. It does not support text
    -- editing via the gui.
    type Textbox is new Widget with private;
    type A_Textbox is access all Textbox'Class;

    -- Creates a new Textbox within 'view' with id 'id'.
    function Create_Textbox( view : not null access Game_Views.Game_View'Class;
                             id   : String := "" ) return A_Textbox;
    pragma Postcondition( Create_Textbox'Result /= null );

    -- Appends text to the Textbox's content.
    procedure Append( this : not null access Textbox'Class; text : String );
    procedure Append_Line( this : not null access Textbox'Class; text : String );

    -- Clears all text from the Textbox.
    procedure Clear( this : not null access Textbox'Class );

    -- Returns the Textbox's full text content.
    function Get_Text( this : not null access Textbox'Class ) return Unbounded_String;

    -- Prepends text to the Textbox's content.
    procedure Prepend( this : not null access Textbox'Class; text : String );
    procedure Prepend_Line( this : not null access Textbox'Class; text : String );

    -- Replaces the Textbox's full text content.
    procedure Set_Text( this : not null access Textbox'Class; text : Unbounded_String );

private

    BACKGROUND_ELEM : constant := 1;
    BORDER_ELEM     : constant := 2;
    TEXT_ELEM       : constant := 3;

    DISABLED_STATE : constant Widget_State := 2**0;

    package String_Lists is new Ada.Containers.Indefinite_Doubly_Linked_Lists( String, "=" );

    ----------------------------------------------------------------------------

    type Textbox is new Widget with
        record
            text          : Unbounded_String;
            displayLines  : String_Lists.List;
            contentWidth,
            contentHeight : Float := 0.0;    -- calculated size of the text plus padding
        end record;

    -- Scrolls the viewport to the top of the content if the text's vertical
    -- alignment is Top, or to the bottom of the content if the text's alignment
    -- is Bottom. The size of the viewport is not changed.
    procedure Adjust_Viewport( this : not null access Textbox'Class );

    procedure Construct( this : access Textbox;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );

    procedure Draw_Content( this : access Textbox );

    -- Returns the Textbox's minimum height.
    function Get_Min_Height( this : access Textbox ) return Float;

    -- Returns the Textbox's minimum width.
    function Get_Min_Width( this : access Textbox ) return Float;

    -- Returns the visual state of the text box, based on its enabled state.
    function Get_Visual_State( this : access Textbox ) return Widget_State;

    -- Recalculates the displayed text lines using the wrapping mode. The size
    -- of the widget's content may change.
    procedure Recalculate_Lines( this : not null access Textbox'Class );

end Widgets.Text_Boxes;
