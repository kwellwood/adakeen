--
-- Copyright (c) 2017-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Bitmaps;                   use Allegro.Bitmaps;
--with Debugging;                         use Debugging;
with Drawing.Bitmaps;                   use Drawing.Bitmaps;
with Drawing.Primitives;                use Drawing.Primitives;
with Drawing.Tiles;                     use Drawing.Tiles;
with Icons;                             use Icons;
with Widget_Styles.Registry;            use Widget_Styles.Registry;

package body Widgets.Split_Panes is

    package Mouse_Connections is new Signals.Mouse.Connections(Split_Pane);
    use Mouse_Connections;

    package Connections_V is new Signals.Connections(Vertical_Split_Pane);
    use Connections_V;

    package Mouse_Connections_V is new Signals.Mouse.Connections(Vertical_Split_Pane);
    use Mouse_Connections_V;

    package Connections_H is new Signals.Connections(Horizontal_Split_Pane);
    use Connections_H;

    package Mouse_Connections_H is new Signals.Mouse.Connections(Horizontal_Split_Pane);
    use Mouse_Connections_H;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Pressed( this  : not null access Split_Pane'Class;
                                mouse : Button_Arguments );

    procedure On_Mouse_Released( this : not null access Split_Pane'Class );

    procedure Set_Fill_Child( this    : not null access Split_Pane'Class;
                              child   : not null access Widget'Class;
                              minSize : Float := 0.0 );

    procedure Set_Fixed_Child( this    : not null access Split_Pane'Class;
                               child   : not null access Widget'Class;
                               minSize : Float := 0.0 );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    procedure On_Mouse_Moved( this  : not null access Horizontal_Split_Pane'Class;
                              mouse : Mouse_Arguments );

    procedure On_Resized( this : not null access Horizontal_Split_Pane'Class );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    procedure On_Mouse_Moved( this  : not null access Vertical_Split_Pane'Class;
                              mouse : Mouse_Arguments );

    procedure On_Resized( this : not null access Vertical_Split_Pane'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Split_Pane( view        : not null access Game_Views.Game_View'Class;
                                orientation : Orientation_Type;
                                fillSide    : Fill_Type ) return A_Split_Pane is
        this : A_Split_Pane;
    begin
        case orientation is
            when Horizontal =>
                this := new Horizontal_Split_Pane;
                A_Horizontal_Split_Pane(this).Construct( view, fillSide );
            when Vertical =>
                this := new Vertical_Split_Pane;
                A_Vertical_Split_Pane(this).Construct( view, fillSide );
        end case;
        return this;
    end Create_Split_Pane;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this       : access Split_Pane;
                         view       : not null access Game_Views.Game_View'Class;
                         widgetType : String;
                         fillSide   : Fill_Type ) is
    begin
        this.Construct( view, "", widgetType );
        this.fillSide := fillSide;
        this.MousePressed.Connect( Slot( this, On_Mouse_Pressed'Access, Mouse_Left ) );
        this.MouseReleased.Connect( Slot( this, On_Mouse_Released'Access, Mouse_Left ) );

        this.sigDragged.Init( this );
    end Construct;

    ----------------------------------------------------------------------------

    function Dragged( this : not null access Split_Pane'Class ) return access Signal'Class is (this.sigDragged'Access);

    ----------------------------------------------------------------------------

    procedure Enable_Resize( this : not null access Split_Pane'Class; enabled : Boolean ) is
    begin
        this.resizable := enabled;
        if this.resizable then
            this.mouseCursor := this.dragCursor;
        else
            this.mouseCursor := To_Unbounded_String( "NONE" );
        end if;
    end Enable_Resize;

    ----------------------------------------------------------------------------

    function Get_Left_Min_Size( this : not null access Split_Pane'Class ) return Float
    is ((if this.fillSide = Left_Side then this.minFillSize else this.minFixedSize));

    ----------------------------------------------------------------------------

    function Get_Right_Min_Size( this : not null access Split_Pane'Class ) return Float
    is ((if this.fillSide = Right_Side then this.minFillSize else this.minFixedSize));

    ----------------------------------------------------------------------------

    overriding
    function Get_Visual_State( this : access Split_Pane ) return Widget_State is
    (
           (if not this.Is_Enabled                              then DISABLED_STATE else 0)
        or (if this.dragging or (this.hover and this.resizable) then HOVER_STATE    else 0)
    );

    ----------------------------------------------------------------------------

    function Is_Resizable( this : not null access Split_Pane'Class ) return Boolean is (this.resizable);

    ----------------------------------------------------------------------------

    procedure On_Mouse_Pressed( this  : not null access Split_Pane'Class;
                                mouse : Button_Arguments ) is
    begin
        if this.resizable and this.leftChild /= null and this.rightChild /= null then
            this.dragging := True;
            this.dragX := mouse.x;
            this.dragY := mouse.y;
        end if;
    end On_Mouse_Pressed;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Released( this : not null access Split_Pane'Class ) is
    begin
        this.dragging := False;
        this.sigDragged.Emit;
    end On_Mouse_Released;

    ----------------------------------------------------------------------------

    procedure Set_Fill_Child( this    : not null access Split_Pane'Class;
                              child   : not null access Widget'Class;
                              minSize : Float := 0.0 ) is
    begin
        if this.fillChild /= null then
            this.Delete_Child( this.fillChild );
        end if;
        this.fillChild := A_Widget(child);
        this.minFillSize := minSize;
        this.Add_Child( child );
        this.Adjust_Layout;
    end Set_Fill_Child;

    ----------------------------------------------------------------------------

    procedure Set_Fixed_Child( this    : not null access Split_Pane'Class;
                               child   : not null access Widget'Class;
                               minSize : Float := 0.0 ) is
    begin
        if this.fixedChild /= null then
            this.Delete_Child( this.fixedChild );
        end if;
        this.fixedChild := A_Widget(child);
        this.minFixedSize := minSize;
        this.Add_Child( child );
        this.Adjust_Layout;
    end Set_Fixed_Child;

    ----------------------------------------------------------------------------

    procedure Set_Left( this    : not null access Split_Pane'Class;
                        child   : not null access Widget'Class;
                        minSize : Float := 0.0 ) is
    begin
        this.leftChild := A_Widget(child);
        case this.fillSide is
            when Left_Side  => this.Set_Fill_Child( child, minSize );
            when Right_Side => this.Set_Fixed_Child( child, minSize );
        end case;
    end Set_Left;

    ----------------------------------------------------------------------------

    procedure Set_Right( this    : not null access Split_Pane'Class;
                         child   : not null access Widget'Class;
                         minSize : Float := 0.0 ) is
    begin
        this.rightChild := A_Widget(child);
        case this.fillSide is
            when Left_Side  => this.Set_Fixed_Child( child, minSize );
            when Right_Side => this.Set_Fill_Child( child, minSize );
        end case;
    end Set_Right;

    --==========================================================================

    overriding
    procedure Adjust_Layout( this : access Horizontal_Split_Pane ) is
        barSize   : constant Float := this.style.Get_Spacing( BAR_ELEM );
        minHeight : Float;
    begin
        if this.leftChild /= null then
            this.leftChild.Fill_Horizontal( this );
            this.leftChild.Top.Attach( this.Top );
            this.leftChild.Bottom.Detach;
        end if;
        if this.rightChild /= null then
            this.rightChild.Fill_Horizontal( this );
            this.rightChild.Bottom.Attach( this.Bottom );
            this.rightChild.Top.Detach;
        end if;

        if this.leftChild = null or this.rightChild = null then
            return;
        end if;

        -- attach the fill widget to the fixed widget
        case this.fillSide is
            when Left_Side =>  this.leftChild.Bottom.Attach( this.rightChild.Top, this.style.Get_Spacing( BAR_ELEM ) );
            when Right_Side => this.rightChild.Top.Attach( this.leftChild.Bottom, this.style.Get_Spacing( BAR_ELEM ) );
        end case;

        -- constrain the initial child sizes
        minHeight := Float'Max( this.minFixedSize, this.fixedChild.Get_Min_Height );
        if minHeight > 0.0 then
            -- use the min height for the fixed child
            this.fixedChild.Height.Set( minHeight );
        else
            -- split the space evenly between the two children
            this.fixedChild.Height.Set( Float'Floor( (this.viewport.height - barSize) / 2.0 ) );

            -- ensure the fill child isn't too small (fixed child doesn't have a minimum height)
            minHeight := Float'Max( this.minFillSize, this.fillChild.Get_Min_Height );
            if this.fillChild.Height.Get < minHeight then
                this.fixedChild.Height.Set( Float'Max( 0.0, this.viewport.height - barSize - this.minFillSize ) );
            end if;
        end if;
    end Adjust_Layout;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this        : access Horizontal_Split_Pane;
                         view        : not null access Game_Views.Game_View'Class;
                         fillSide    : Fill_Type ) is
    begin
        Split_Pane(this.all).Construct( view, "HorizontalSplitPane", fillSide );
        this.dragCursor := To_Unbounded_String( "RESIZE_N" );
        this.mouseCursor := this.dragCursor;

        this.MouseMoved.Connect( Slot( this, On_Mouse_Moved'Access ) );
        this.Resized.Connect( Slot( this, On_Resized'Access ) );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Content( this : access Horizontal_Split_Pane ) is
        state   : constant Widget_State := this.Get_Visual_State;
        icon    : constant Icon_Type := this.style.Get_Image( HANDLE_ELEM, state );
        barSize : constant Float := this.style.Get_Spacing( BAR_ELEM );
        color   : Allegro_Color;
        barY    : Float := 0.0;
        iconX,
        iconY   : Float := 0.0;
    begin
        -- - - - Calculate Positions - - - --

        if this.leftChild /= null then
            barY := this.leftChild.Height.Get;
        end if;

        iconX := Float'Floor( (this.viewport.width - Float(icon.Get_Width)) / 2.0  );
        iconY := barY + Float'Floor( (barSize - Float(icon.Get_Height)) / 2.0 );

        -- - - - Draw Content - - - --

        color := this.style.Get_Color( BAR_ELEM, state );
        if color /= Transparent then
            Rectfill( 0.0, barY, this.viewport.width, barY + barSize,
                      Lighten( color, this.style.Get_Shade( BAR_ELEM, state ) ) );
        else
            Draw_Tile_Filled( this.style.Get_Image( BAR_ELEM, state ).Get_Tile,
                              0.0, barY, 0.0,
                              this.viewport.width, barSize,
                              Lighten( this.style.Get_Tint( BAR_ELEM, state ),
                                       this.style.Get_Shade( BAR_ELEM, state ) ) );
        end if;

        if icon.Get_Bitmap /= null then
            Draw_Bitmap_Stretched( icon.Get_Bitmap,
                                   iconX, iconY, 0.0,
                                   Float(icon.Get_Width), Float(icon.Get_Height),
                                   Fit,
                                   0.0,
                                   Lighten( this.style.Get_Tint( HANDLE_ELEM, state ),
                                            this.style.Get_Shade( HANDLE_ELEM, state ) ) );
        end if;
    end Draw_Content;

    ----------------------------------------------------------------------------

    overriding
    procedure Find_Widget_At( this     : access Horizontal_Split_Pane;
                              x, y     : Float;
                              children : Boolean;
                              wx, wy   : out Float;
                              found    : out A_Widget ) is
        -- translate viewport coordinates into content coordinates
        cx      : constant Float := this.viewport.x + x / this.zoomFactor;
        cy      : constant Float := this.viewport.y + y / this.zoomFactor;

        -- determine the bar's location
        barY    : constant Float := (if this.leftChild /= null then this.leftChild.Height.Get else 0.0);
        barSize : constant Float := this.style.Get_Spacing( BAR_ELEM );
    begin
        -- allow the bar to be twice as tall for interaction purposes
        if Contains( Rectangle'(0.0, barY - Float'Ceiling( barSize / 2.0 ), this.geom.width, barSize * 2.0), cx, cy ) then
            -- translate the viewport coordinates into content coordinates
            wx := cx;
            wy := cy;
            found := A_Widget(this);
            return;
        end if;

        -- operate normally
        Split_Pane(this.all).Find_Widget_At( x, y, children, wx, wy, found );
    end Find_Widget_At;

    ----------------------------------------------------------------------------

    overriding
    function Get_Left_Max_Size( this : access Horizontal_Split_Pane ) return Float
    is (this.geom.height - this.style.Get_Spacing( BAR_ELEM ) - this.Get_Right_Min_Size);

    ----------------------------------------------------------------------------

    overriding
    function Get_Right_Max_Size( this : access Horizontal_Split_Pane ) return Float
    is (this.geom.height - this.style.Get_Spacing( BAR_ELEM ) - this.Get_Left_Min_Size);

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Height( this : access Horizontal_Split_Pane ) return Float
    is (this.style.Get_Spacing( BAR_ELEM )
        + Float'Max( (if this.fillChild  /= null then this.fillChild.Get_Min_Height  else 0.0), this.minFillSize )
        + Float'Max( (if this.fixedChild /= null then this.fixedChild.Get_Min_Height else 0.0), this.minFixedSize ));

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Width( this : access Horizontal_Split_Pane ) return Float
    is (Float'Max( (if this.fillChild  /= null then this.fillChild.Get_Min_Width  else 0.0),
                   (if this.fixedChild /= null then this.fixedChild.Get_Min_Width else 0.0) ));

    ----------------------------------------------------------------------------

    overriding
    function Get_Preferred_Height( this : access Horizontal_Split_Pane ) return Float is
    begin
        if not this.prefHeight.Is_Null then
            return this.prefHeight.To_Float;
        end if;
        return this.style.Get_Spacing( BAR_ELEM )
               + Float'Max( (if this.fillChild  /= null then this.fillChild.Get_Preferred_Height  else 0.0), this.minFillSize )
               + Float'Max( (if this.fixedChild /= null then this.fixedChild.Get_Preferred_Height else 0.0), this.minFixedSize );
    end Get_Preferred_Height;

    ----------------------------------------------------------------------------

    overriding
    function Get_Preferred_Width( this : access Horizontal_Split_Pane ) return Float is
    begin
        if not this.prefWidth.Is_Null then
            return this.prefWidth.To_Float;
        end if;
        return Float'Max( (if this.fillChild  /= null then this.fillChild.Get_Preferred_Width  else 0.0),
                          (if this.fixedChild /= null then this.fixedChild.Get_Preferred_Width else 0.0) );
    end Get_Preferred_Width;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Moved( this  : not null access Horizontal_Split_Pane'Class;
                              mouse : Mouse_Arguments ) is
        minHeight : Float;
        moveDist  : Float;
    begin
        if not this.dragging then
            return;
        end if;

        -- when this.fillSide = ...
        --     Right_Side, moveDist will be positive when dragging down and negative when dragging up
        --     Left_Side,  moveDist will be negative when dragging down and positive when dragging up
        --
        -- moveDist is positive when the fixed size is expanding / fill side shrinking,
        -- and negative when fixed size is shrinking / fill side expanding

        moveDist := mouse.y - this.dragY;
        if this.fillSide = Left_Side then
            moveDist := -moveDist;
        end if;

        -- constrain to the fill widget's minimum height
        minHeight := Float'Max( this.minFillSize, this.fillChild.Get_Min_Height );
        if moveDist > 0.0 and this.fillChild.Height.Get - moveDist < minHeight then
            moveDist := this.fillChild.Height.Get - minHeight;
        end if;

        -- constrain to the fixed widget's minimum height (takes priority)
        minHeight := Float'Max( this.minFixedSize, this.fixedChild.Get_Min_Height );
        if moveDist < 0.0 and this.fixedChild.Height.Get + moveDist < minHeight then
            moveDist := minHeight - this.fixedChild.Height.Get;
        end if;

        this.fixedChild.Height.Set( this.fixedChild.Height.Get + moveDist );

        if this.fillSide = Left_Side then
            moveDist := -moveDist;
        end if;
        this.dragY := this.dragY + moveDist;
    end On_Mouse_Moved;

    ----------------------------------------------------------------------------

    procedure On_Resized( this : not null access Horizontal_Split_Pane'Class ) is
        minHeight : Float;
    begin
        if this.fillChild = null or this.fixedChild = null then
            return;
        end if;

        minHeight := Float'Max( this.minFillSize, this.fillChild.Get_Min_Height );

        -- if the split pane's height has lessened, check if the fill child
        -- (which got smaller) is still at least minimum height. if it's too
        -- small now, reduce the height of the fixed child until we meet its
        -- minimum. if the split_pane is smaller than the minimum height of both
        -- children, then it is the fill child that will get squished smaller
        -- than its minimum.
        if this.fillChild.Height.Get < minHeight then
            this.fixedChild.Height.Set( Float'Max( this.fixedChild.Height.Get - (minHeight - this.fillChild.Height.Get),
                                                   Float'Max( this.minFixedSize, this.fixedChild.Get_Min_Height ) ) );
        end if;
    end On_Resized;

    --==========================================================================

    overriding
    procedure Adjust_Layout( this : access Vertical_Split_Pane ) is
        barSize  : constant Float := this.style.Get_Spacing( BAR_ELEM );
        minWidth : Float;
    begin
        if this.leftChild /= null then
            this.leftChild.Fill_Vertical( this );
            this.leftChild.Left.Attach( this.Left );
            this.leftChild.Right.Detach;
        end if;
        if this.rightChild /= null then
            this.rightChild.Fill_Vertical( this );
            this.rightChild.Right.Attach( this.Right );
            this.rightChild.Left.Detach;
        end if;

        if this.leftChild = null or this.rightChild = null then
            return;
        end if;

        -- attach the fill widget to the fixed widget
        case this.fillSide is
            when Left_Side =>  this.leftChild.Right.Attach( this.rightChild.Left, this.style.Get_Spacing( BAR_ELEM ) );
            when Right_Side => this.rightChild.Left.Attach( this.leftChild.Right, this.style.Get_Spacing( BAR_ELEM ) );
        end case;

        -- constrain the initial child sizes
        minWidth := Float'Max( this.minFixedSize, this.fixedChild.Get_Min_Width );
        if minWidth > 0.0 then
            -- use the min width for the fixed child
            this.fixedChild.Width.Set( minWidth );
        else
            -- split the space evenly between the two children
            this.fixedChild.Width.Set( Float'Floor( (this.viewport.width - barSize) / 2.0 ) );

            -- ensure the fill child isn't too small (fixed child doesn't have a minimum width)
            minWidth := Float'Max( this.minFillSize, this.fillChild.Get_Min_Width );
            if this.fillChild.Width.Get < minWidth then
                this.fixedChild.Width.Set( Float'Max( 0.0, this.viewport.width - barSize - this.minFillSize ) );
            end if;
        end if;
    end Adjust_Layout;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this        : access Vertical_Split_Pane;
                         view        : not null access Game_Views.Game_View'Class;
                         fillSide    : Fill_Type ) is
    begin
        Split_Pane(this.all).Construct( view, "VerticalSplitPane", fillSide );
        this.dragCursor := To_Unbounded_String( "RESIZE_E" );
        this.mouseCursor := this.dragCursor;

        this.MouseMoved.Connect( Slot( this, On_Mouse_Moved'Access ) );
        this.Resized.Connect( Slot( this, On_Resized'Access ) );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Content( this : access Vertical_Split_Pane ) is
        state   : constant Widget_State := this.Get_Visual_State;
        icon    : constant Icon_Type := this.style.Get_Image( HANDLE_ELEM, state );
        barSize : constant Float := this.style.Get_Spacing( BAR_ELEM );
        color   : Allegro_Color;
        barX    : Float := 0.0;
        iconX,
        iconY   : Float := 0.0;
    begin
        -- - - - Calculate Positions - - - --

        if this.leftChild /= null then
            barX := this.leftChild.Width.Get;
        end if;

        iconX := barX + Float'Floor( (barSize - Float(icon.Get_Width)) / 2.0 );
        iconY := Float'Floor( (this.viewport.height - Float(icon.Get_Height)) / 2.0 );

        -- - - - Draw Content - - - --

        color := this.style.Get_Color( BAR_ELEM, state );
        if color /= Transparent then
            Rectfill( barX, 0.0, barX + barSize, this.viewport.height,
                      Lighten( color, this.style.Get_Shade( BAR_ELEM, state ) ) );
        else
            Draw_Tile_Filled( this.style.Get_Image( BAR_ELEM, state ).Get_Tile,
                              barX, 0.0, 0.0,
                              barSize, this.viewport.height,
                              Lighten( this.style.Get_Tint( BAR_ELEM, state ),
                                       this.style.Get_Shade( BAR_ELEM, state ) ) );
        end if;

        if icon.Get_Bitmap /= null then
            Draw_Bitmap_Stretched( icon.Get_Bitmap,
                                   iconX, iconY, 0.0,
                                   Float(icon.Get_Width), Float(icon.Get_Height),
                                   Fit,
                                   0.0,
                                   Lighten( this.style.Get_Tint( HANDLE_ELEM, state ),
                                            this.style.Get_Shade( HANDLE_ELEM, state ) ) );
        end if;
    end Draw_Content;

    ----------------------------------------------------------------------------

    overriding
    procedure Find_Widget_At( this     : access Vertical_Split_Pane;
                              x, y     : Float;
                              children : Boolean;
                              wx, wy   : out Float;
                              found    : out A_Widget ) is
        -- translate viewport coordinates into content coordinates
        cx      : constant Float := this.viewport.x + x / this.zoomFactor;
        cy      : constant Float := this.viewport.y + y / this.zoomFactor;

        -- determine the bar's location
        barX    : constant Float := (if this.leftChild /= null then this.leftChild.Width.Get else 0.0);
        barSize : constant Float := this.style.Get_Spacing( BAR_ELEM );
    begin
        -- allow the bar to be twice as wide for interaction purposes
        if Contains( Rectangle'(barX - Float'Ceiling( barSize / 2.0 ), 0.0, barSize * 2.0, this.geom.height), cx, cy ) then
            -- translate the viewport coordinates into content coordinates
            wx := cx;
            wy := cy;
            found := A_Widget(this);
            return;
        end if;

        -- operate normally
        Split_Pane(this.all).Find_Widget_At( x, y, children, wx, wy, found );
    end Find_Widget_At;

    ----------------------------------------------------------------------------

    overriding
    function Get_Left_Max_Size( this : access Vertical_Split_Pane ) return Float
    is (this.geom.width - this.style.Get_Spacing( BAR_ELEM ) - this.Get_Right_Min_Size);

    ----------------------------------------------------------------------------

    overriding
    function Get_Right_Max_Size( this : access Vertical_Split_Pane ) return Float
    is (this.geom.width - this.style.Get_Spacing( BAR_ELEM ) - this.Get_Left_Min_Size);

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Height( this : access Vertical_Split_Pane ) return Float
    is (Float'Max( (if this.fillChild  /= null then this.fillChild.Get_Min_Height  else 0.0),
                   (if this.fixedChild /= null then this.fixedChild.Get_Min_Height else 0.0) ));

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Width( this : access Vertical_Split_Pane ) return Float
    is (this.style.Get_Spacing( BAR_ELEM )
        + Float'Max( (if this.fillChild  /= null then this.fillChild.Get_Min_Width  else 0.0), this.minFillSize )
        + Float'Max( (if this.fixedChild /= null then this.fixedChild.Get_Min_Width else 0.0), this.minFixedSize ));

    ----------------------------------------------------------------------------

    overriding
    function Get_Preferred_Height( this : access Vertical_Split_Pane ) return Float is
    begin
        if not this.prefHeight.Is_Null then
            return this.prefHeight.To_Float;
        end if;
        return Float'Max( (if this.fillChild  /= null then this.fillChild.Get_Preferred_Height  else 0.0),
                          (if this.fixedChild /= null then this.fixedChild.Get_Preferred_Height else 0.0) );
    end Get_Preferred_Height;

    ----------------------------------------------------------------------------

    overriding
    function Get_Preferred_Width( this : access Vertical_Split_Pane ) return Float is
    begin
        if not this.prefWidth.Is_Null then
            return this.prefWidth.To_Float;
        end if;
        return this.style.Get_Spacing( BAR_ELEM )
               + Float'Max( (if this.fillChild  /= null then this.fillChild.Get_Preferred_Width  else 0.0), this.minFillSize )
               + Float'Max( (if this.fixedChild /= null then this.fixedChild.Get_Preferred_Width else 0.0), this.minFixedSize );
    end Get_Preferred_Width;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Moved( this  : not null access Vertical_Split_Pane'Class;
                              mouse : Mouse_Arguments ) is
        minWidth : Float;
        moveDist : Float;
    begin
        if not this.dragging then
            return;
        end if;

        -- when this.fillSide = ...
        --     Right_Side, moveDist will be positive when dragging right and negative when dragging left
        --     Left_Side,  moveDist will be negative when dragging right and positive when dragging left
        --
        -- moveDist is positive when the fixed size is expanding / fill side shrinking,
        -- and negative when fixed size is shrinking / fill side expanding

        moveDist := mouse.x - this.dragX;
        if this.fillSide = Left_Side then
            moveDist := -moveDist;
        end if;

        -- constrain to the fill widget's minimum width
        minWidth := Float'Max( this.minFillSize, this.fillChild.Get_Min_Width );
        if moveDist > 0.0 and this.fillChild.Width.Get - moveDist < minWidth then
            moveDist := this.fillChild.Width.Get - minWidth;
        end if;

        -- constrain to the fixed widget's minimum width (takes priority)
        minWidth := Float'Max( this.minFixedSize, this.fixedChild.Get_Min_Width );
        if moveDist < 0.0 and this.fixedChild.Width.Get + moveDist < minWidth then
            moveDist := minWidth - this.fixedChild.Width.Get;
        end if;

        this.fixedChild.Width.Set( this.fixedChild.Width.Get + moveDist );

        if this.fillSide = Left_Side then
            moveDist := -moveDist;
        end if;
        this.dragX := this.dragX + moveDist;
    end On_Mouse_Moved;

    ----------------------------------------------------------------------------

    procedure On_Resized( this : not null access Vertical_Split_Pane'Class ) is
        minWidth : Float;
    begin
        if this.fillChild = null or this.fixedChild = null then
            return;
        end if;

        minWidth := Float'Max( this.minFillSize, this.fillChild.Get_Min_Width );

        -- if the split pane's width has lessened, check if the fill child
        -- (which got smaller) is still at least minimum width. if it's too
        -- small now, reduce the width of the fixed child until we meet its
        -- minimum. if the split_pane is smaller than the minimum width of both
        -- children, then it is the fill child that will get squished smaller
        -- than its minimum.
        if this.fillChild.Width.Get < minWidth then
            this.fixedChild.Width.Set( Float'Max( this.fixedChild.Width.Get - (minWidth - this.fillChild.Width.Get),
                                                  Float'Max( this.minFixedSize, this.fixedChild.Get_Min_Width ) ) );
        end if;
    end On_Resized;

begin

    Register_Style( "HorizontalSplitPane",
                    (BAR_ELEM    => To_Unbounded_String( "bar" ),
                     HANDLE_ELEM => To_Unbounded_String( "handle" )),
                    (BAR_ELEM    => Area_Element,
                     HANDLE_ELEM => Icon_Element),
                    (0 => To_Unbounded_String( "disabled" ),
                     1 => To_Unbounded_String( "hover" )) );

    Register_Style( "VerticalSplitPane",
                    (BAR_ELEM    => To_Unbounded_String( "bar" ),
                     HANDLE_ELEM => To_Unbounded_String( "handle" )),
                    (BAR_ELEM    => Area_Element,
                     HANDLE_ELEM => Icon_Element),
                    (0 => To_Unbounded_String( "disabled" ),
                     1 => To_Unbounded_String( "hover" )) );

end Widgets.Split_Panes;
