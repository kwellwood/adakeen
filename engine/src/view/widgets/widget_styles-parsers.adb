--
-- Copyright (c) 2016-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Assertions;                    use Ada.Assertions;
with Ada.Exceptions;                    use Ada.Exceptions;
with Ada.Unchecked_Deallocation;
--with Debugging;                         use Debugging;
with Scribble;                          use Scribble;
with Scribble.Parse_Errors;             use Scribble.Parse_Errors;
with Scribble.String_Streams;           use Scribble.String_Streams;
with Values.Strings;                    use Values.Strings;
with Widget_Styles.Registry;            use Widget_Styles.Registry;

package body Widget_Styles.Parsers is

    use Tokens;

    --==========================================================================

    function Create_Parser return A_Parser is (new Parser'(new Scanner, others => <>));

    ----------------------------------------------------------------------------

    --
    -- File   ::= {Line}
    --   Line ::= Directive | Rule
    --
    procedure Scan_File( this     : not null access Parser'Class;
                         source   : Unbounded_String;
                         filepath : String ) is
        strm : A_String_Stream;
    begin
        this.source := source;
        strm := Stream( this.source );
        this.scanner.Set_Input( strm, filepath );

        loop
            exit when not this.Scan_Directive and then not this.Scan_Rule;
        end loop;
        this.scanner.Expect_Token( TK_EOF );

        this.scanner.Set_Input( null, "" );
        Close( strm );
        this.source := To_Unbounded_String( "" );
    exception
        when Parse_Error =>
            this.scanner.Set_Input( null );
            Close( strm );
            raise;
    end Scan_File;

    ----------------------------------------------------------------------------

    procedure Execute_Rule( this       : not null access Parser'Class;
                            element    : not null A_Element_Style;
                            state      : Widget_State;
                            properties : Map_Value'Class;
                            loc        : Token_Location ) is
        pragma Unreferenced( this );

        procedure Set_Property( property : String; val : Value ) is
        begin
            if not element.Set_Property( To_Property_Id( property ), state, val ) then
                Raise_Parse_Error( "Invalid property '" & property & "'", loc );
            end if;
        end Set_Property;

    begin
        properties.Iterate( Set_Property'Access );
    end Execute_Rule;

    ----------------------------------------------------------------------------

    --
    -- Declarations ::= '{' Declaration {';' Declaration} '}'
    --
    -- Declaration ::= Property ':' Value
    --   Property  ::= Identifier
    --   Value     ::= string | number | color | boolean
    --
    procedure Expect_Declarations( this         : not null access Parser'Class;
                                   declarations : in out Map_Value;
                                   location     : in out Token_Location ) is
        token    : Token_Ptr;
        val      : Value;
        property : Unbounded_String;
    begin
        declarations := Create_Map.Map;
        token := this.scanner.Expect_Token( TK_LBRACE );
        location := token.Get.Location;

        loop
            token := this.scanner.Accept_Token( TK_SYMBOL );
            exit when token.Is_Null;

            property := To_Unbounded_String( Symbol_Token(token.Get.all).Get_Name );
            this.scanner.Expect_Token( TK_COLON );

            -- parse the value: color, number, string, or boolean
            token := this.scanner.Expect_Token( TK_VALUE );
            val := Value_Token(token.Get.all).Get_Value;
            if not val.Is_String and
               not val.Is_Number and
               not val.Is_Color and
               not val.Is_Boolean
            then
                Raise_Parse_Error( "Invalid value type", token.Get.Location );
            end if;
            declarations.Set( property, val, consume => True );

            -- expect a trailing ';' unless this is the last declaration in the block
            if this.scanner.Accept_Token( TK_RBRACE ).Not_Null then
                this.scanner.Step_Back;     -- put the '}' back
                exit;
            end if;

            this.scanner.Expect_Token( TK_SEMICOLON );
        end loop;

        this.scanner.Expect_Token( TK_RBRACE );
    end Expect_Declarations;

    ----------------------------------------------------------------------------

    --
    -- Directive ::= '@' (Import | Copy) ';'
    --   Import  ::= 'import' string
    --   Copy    ::= 'copy' Identifier 'from' Identifier
    --
    function Scan_Directive( this : not null access Parser'Class ) return Boolean is
        dir      : Token_Ptr;
        filename : Token_Ptr;
        style1,
        style2   : Token_Ptr;
        val      : Value;
    begin
        if this.scanner.Accept_Token( TK_AT ).Is_Null then
            return False;
        end if;

        dir := this.scanner.Expect_One( (TK_IMPORT, TK_COPY) );

        -- Directive: @import "filename";
        if dir.Get.Get_Type = TK_IMPORT then
            filename := this.scanner.Expect_Token( TK_VALUE );
            this.scanner.Expect_Token( TK_SEMICOLON );

            val := Value_Token(filename.Get.all).Get_Value;
            if not val.Is_String then
                Raise_Parse_Error( "Expected string", filename.Get.Location );
            end if;

            begin
                Widget_Styles.Registry.Load_Style_Sheet( val.Str.To_String );
            exception
                when e : Parse_Error =>
                    Raise_Parse_Error( Exception_Message( e ), filename.Get.Location );
            end;

        -- Directive: @copy <style> from <style>
        elsif dir.Get.Get_Type = TK_COPY then
            style1 := this.scanner.Expect_Token( TK_SYMBOL );

            -- expect "from"
            style2 := this.scanner.Expect_Token( TK_FROM );

            -- expect Identifier of style to be copied
            style2 := this.scanner.Expect_Token( TK_SYMBOL );
            this.scanner.Expect_Token( TK_SEMICOLON );

            Widget_Styles.Registry.Copy_Named_Styles( Symbol_Token(style2.Get.all).Get_Name,
                                                      Symbol_Token(style1.Get.all).Get_Name );
        end if;

        return True;
    end Scan_Directive;

    ----------------------------------------------------------------------------

    --
    -- Rule ::= Selector Declarations
    --
    function Scan_Rule( this : not null access Parser'Class ) return Boolean is

        ------------------------------------------------------------------------

        -- Parses a list of state names into a combined Widget_State value.
        function Parse_States( definition  : A_Style_Definition;
                               stateTokens : Token_Lists.List
                             ) return Widget_State is
            state : Widget_State := 0;

            procedure Examine( pos : Token_Lists.Cursor ) is
                stateStr : constant String := Symbol_Token(Element( pos ).Get.all).Get_Name;
                aState   : Widget_State := 0;
            begin
                if not definition.Get_State( stateStr, aState ) then
                    Raise_Parse_Error( "Invalid state", Element( pos ).Get.Location );
                end if;
                state := state or aState;
            end Examine;

        begin
            stateTokens.Iterate( Examine'Access );
            return state;
        end Parse_States;

        ------------------------------------------------------------------------

        selector     : Selector_Type;
        declarations : Map_Value;
        decLoc       : Token_Location;
        definition   : A_Style_Definition;
        style        : A_Widget_Style;
        element      : Element_Index;
        state        : Widget_State := 0;
    begin
        -- parse selector
        if not this.Scan_Selector( selector ) then
            return False;
        end if;

        -- parse declarations block
        this.Expect_Declarations( declarations, decLoc );

        definition := Widget_Styles.Registry.Get_Definition( Symbol_Token(selector.widgetType.Get.all).Get_Name );
        if definition =  null then
            Raise_Parse_Error( "Invalid widget", selector.widgetType.Get.Location );
        end if;

        style := definition.Get_Style_Ref( To_String( selector.styleName ) );

        -- parse element name
        if not definition.Get_Element( Symbol_Token(selector.element.Get.all).Get_Name, element ) then
            Raise_Parse_Error( "Invalid element", selector.element.Get.Location );
        end if;

        if selector.subElement.Not_Null then
            if definition.Get_Sub_Style_Type( element )'Length = 0 then
                -- sub element provided but the widget type does not allow one
                Raise_Parse_Error( "Subelement not allowed", selector.subElement.Get.Location );
            end if;

            Assert( style.elements(element).all in Widget_Style'Class );

            definition := Widget_Styles.Registry.Get_Definition( definition.Get_Sub_Style_Type( element ) );
            Assert( definition /= null, "Invalid subelement type in style definition" );

            style := A_Widget_Style(style.elements(element));

            -- provided sub element name is not valid
            if not definition.Get_Element( Symbol_Token(selector.subElement.Get.all).Get_Name, element ) then
                Raise_Parse_Error( "Invalid subelement", selector.subElement.Get.Location );
            end if;
        end if;

        state := Parse_States( definition, selector.stateList );
        this.Execute_Rule( style.elements(element), state, declarations, decLoc );

        return True;
    end Scan_Rule;

    ----------------------------------------------------------------------------

    --
    -- Selector    ::= [StyleName] Widget '.' Element ['.' Element] {':' State}
    --   StyleName ::= Identifier
    --   Widget    ::= Identifier
    --   Element   ::= Identifier
    --   State     ::= Identifier
    --
    function Scan_Selector( this     : not null access Parser'Class;
                            selector : out Selector_Type ) return Boolean is
        token  : Token_Ptr;
        token2 : Token_Ptr;
    begin
        -- scan the style name or widget type
        token := this.scanner.Accept_Token( TK_SYMBOL );
        if token.Is_Null then
            return False;
        end if;
        token2 := this.scanner.Accept_Token( TK_SYMBOL );
        if token2.Not_Null then
            selector.styleName := To_Unbounded_String( Symbol_Token(token.Get.all).Get_Name );
            token := token2;
        end if;
        selector.widgetType := token;

        -- scan the required element
        this.scanner.Expect_Token( TK_DOT );
        selector.element := this.scanner.Expect_Token( TK_SYMBOL );

        -- scan the optional sub-element
        token := this.scanner.Accept_Token( TK_DOT );
        if token.Not_Null then
            selector.subElement := this.scanner.Expect_Token( TK_SYMBOL );
        end if;

        -- scan the optional visual states
        token := this.scanner.Accept_Token( TK_COLON );
        while token.Not_Null loop
            selector.stateList.Append( this.scanner.Expect_Token( TK_SYMBOL ) );
            token := this.scanner.Accept_Token( TK_COLON );
        end loop;

        return True;
    end Scan_Selector;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Parser ) is
        procedure Free is new Ada.Unchecked_Deallocation( Parser'Class, A_Parser );
    begin
        if this /= null then
            Delete( this.scanner );
            Free( this );
        end if;
    end Delete;

end Widget_Styles.Parsers;
