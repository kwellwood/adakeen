--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Doubly_Linked_Lists;
with Widgets.Game_Screens;              use Widgets.Game_Screens;

package Widgets.Screen_Managers is


    -- A Screen_Manager manages a collection of Game_Screens as a stack,
    -- displaying the front screen(s). A Game_Screen obscures the screens
    -- behind it unless it's not opaque, overlayed on the screen behind it.
    type Screen_Manager is new Widget and Animated with private;
    type A_Screen_Manager is access all Screen_Manager'Class;

    -- Creates a new, empty screen manager.
    function Create_Screen_Manager( view : not null access Game_Views.Game_View'Class;
                                    id   : String := "" ) return A_Screen_Manager;

    -- Adds a new screen to the back of the screen stack. If will only become
    -- immediately visible if the stack does not contain any opaque screens.
    -- 'screen' will become owned by this widget.
    procedure Add_Back( this   : not null access Screen_Manager'Class;
                        screen : not null access Game_Screen'Class );

    -- Adds a new screen to the front of the screen stack, showing it. If the
    -- screen is not opaque, then the screen(s) beneath it will remain visible.
    -- Otherwise 'screen' will obscure the currently visible screens and they
    -- will be hidden. 'screen' will become owned by this widget.
    procedure Add_Front( this   : not null access Screen_Manager'Class;
                         screen : not null access Game_Screen'Class );

    -- Returns True if 'screen' is on the stack, even if it's not currently visible.
    function Contains( this   : not null access Screen_Manager'Class;
                       screen : not null access Game_Screen'Class ) return Boolean;

    -- Pops and deletes the back of the screen stack.
    procedure Delete_Back( this : not null access Screen_Manager'Class );

    -- Pops and deletes the front of the screen stack.
    procedure Delete_Front( this : not null access Screen_Manager'Class );

    -- Returns the screen at the back of the stack, or null if empty.
    function Get_Back( this : not null access Screen_Manager'Class ) return A_Game_Screen;

    -- Returns the screen at the front of the stack, or null if empty.
    function Get_Front( this : not null access Screen_Manager'Class ) return A_Game_Screen;

    -- Returns the number of screens on the stack.
    function Get_Length( this : not null access Screen_Manager'Class ) return Natural;

    -- Pops and returns the back of the screen stack, transferring ownership to
    -- the caller. Returns null if the screen stack is empty.
    function Pop_Back( this : not null access Screen_Manager'Class ) return A_Game_Screen;

    -- Pops and returns the front of the screen stack, transferring ownership to
    -- the caller. Returns null if the screen stack is empty.
    function Pop_Front( this : not null access Screen_Manager'Class ) return A_Game_Screen;

private

    BACKGROUND_ELEM : constant := 1;

    DISABLED_STATE : constant Widget_State := 2**0;

    package Screen_Stacks is new Ada.Containers.Doubly_Linked_Lists( A_Game_Screen );
    use Screen_Stacks;

    ----------------------------------------------------------------------------

    type Screen_Manager is new Widget and Animated with
        record
            stack : Screen_Stacks.List;
        end record;

    procedure Construct( this : access Screen_Manager;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );

    procedure Draw_Content( this : access Screen_Manager );

    procedure Tick( this : access Screen_Manager; time : Tick_Time );

end Widgets.Screen_Managers;
