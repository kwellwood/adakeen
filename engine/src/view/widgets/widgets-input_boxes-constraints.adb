--
-- Copyright (c) 2012-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
--with Debugging;                         use Debugging;

package body Widgets.Input_Boxes.Constraints is

    function Basename_Only( curstr, newstr : String ) return String is
        illegal : constant String := ":*?""/\<>|#.";
    begin
        for c in reverse newstr'Range loop
            if Index( illegal, String'(1=>newstr(c)) ) >= illegal'First then
                return curstr;
            end if;
        end loop;
        return newstr;
    end Basename_Only;

    ----------------------------------------------------------------------------

    function Filename_Only( curstr, newstr : String ) return String is
        illegal : constant String := ":*?""/\<>|#";
    begin
        for c in reverse newstr'Range loop
            if Index( illegal, String'(1=>newstr(c)) ) >= illegal'First then
                return curstr;
            end if;
        end loop;
        return newstr;
    end Filename_Only;

    ----------------------------------------------------------------------------

    function Int_Only( curstr, newstr : String ) return String is
    begin
        for c in reverse newstr'Range loop
            if not Is_Digit( newstr(c) ) then
                if newstr(c) = '-' and c = newstr'First then
                    -- negative sign only allowed as first character
                    null;
                else
                    return curstr;
                end if;
            end if;
        end loop;
        return newstr;
    end Int_Only;

    ----------------------------------------------------------------------------

    function Int_Positive_Only( curstr, newstr : String ) return String is
    begin
        for c in reverse newstr'Range loop
            if not Is_Digit( newstr(c) ) then
                return curstr;
            end if;
        end loop;
        return newstr;
    end Int_Positive_Only;

    ----------------------------------------------------------------------------

    function Number_Only( curstr, newstr : String ) return String is
        dot : Boolean := False;
    begin
        if newstr'Length > 0 and then newstr(newstr'First) = '.' then
            -- can't start with a dot
            return curstr;
        end if;

        for c in reverse newstr'Range loop
            if not Is_Digit( newstr(c) ) then
                if newstr(c) = '.' and then not dot then
                    dot := True;
                elsif newstr(c) = '-' and c = newstr'First then
                    -- negative sign only allowed as first character
                    null;
                else
                    -- two dots or bad char
                    return curstr;
                end if;
            end if;
        end loop;
        return newstr;
    end Number_Only;

    ----------------------------------------------------------------------------

    function Number_Positive_Only( curstr, newstr : String ) return String is
        dot : Boolean := False;
    begin
        if newstr'Length > 0 and then newstr(newstr'First) = '.' then
            -- can't start with a dot
            return curstr;
        end if;

        for c in reverse newstr'Range loop
            if not Is_Digit( newstr(c) ) then
                if newstr(c) = '.' and then not dot then
                    dot := True;
                else
                    -- two dots or bad char
                    return curstr;
                end if;
            end if;
        end loop;
        return newstr;
    end Number_Positive_Only;

end Widgets.Input_Boxes.Constraints;
