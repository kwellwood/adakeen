--
-- Copyright (c) 2012-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Widgets.Popups is

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Popup( view : not null access Game_Views.Game_View'Class;
                           id   : String := "" ) return A_Popup is
        this : A_Popup := new Popup;
    begin
        this.Construct( view, id );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Popup;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Popup;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String ) is
    begin
        Widget(this.all).Construct( view, id, "" );
        this.Visible.Set( False );
        this.menu := Create_Popup_Menu( view, this.Get_Id & "-submenu" );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Popup ) is
    begin
        Delete( A_Widget(this.menu) );
        Widget(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Get_Menu( this : not null access Popup'Class ) return A_Sub_Menu is (this.menu);

    ----------------------------------------------------------------------------

    procedure Show( this : not null access Popup'Class; mouseX, mouseY : Float ) is
    begin
        this.menu.Show( mouseX, mouseY );
    end Show;

end Widgets.Popups;
