--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Game_Views;                        use Game_Views;

package body Widgets.Menubars is

    pragma Suppress( Elaboration_Check, Signals.Connections );
    pragma Suppress( Elaboration_Check, Signals.Mouse.Connections );

    package Connections is new Signals.Connections(Menubar);
    use Connections;

    ----------------------------------------------------------------------------

    procedure On_Rooted( this : not null access Menubar'Class );

    procedure On_Unrooted( this : not null access Menubar'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Menubar( view : not null access Game_Views.Game_View'Class;
                             id   : String ) return A_Menubar is
        this : A_Menubar := new Menubar;
    begin
        this.Construct( view, id );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Menubar;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Menubar;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String ) is
    begin
        Widget(this.all).Construct( view, id, "" );
        this.nativeMenu := Al_Create_Menu;
        this.Visible.Set( False );
        this.Set_Focusable( False );

        this.Rooted.Connect( Slot( this, On_Rooted'Access ) );
        this.Unrooted.Connect( Slot( this, On_Unrooted'Access ) );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Menubar ) is
    begin
        -- For some reason, destroying the menu at this point in the shutdown
        -- causes memory corruption and often results in a crash somewhere else.
        -- It may be that removing the menubar triggers a resize event that must
        -- be seen and acknowledged by the Input_Handler task, but by the time
        -- the Game_View's widgets are being destroyed, the Input_Handler has
        -- already been stopped.
        --
        -- Assuming the Menubar is being deleted because the application is
        -- being shut down, it's fine to leave the native menubar alone. Even
        -- if the menubar is being changed multiple times during the course of
        -- the application. The previous menubar is destroyed by the call to
        -- Al_Set_Display_Menu.
        this.nativeMenu := null;

        Widget(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Add_Menu( this : not null access Menubar'Class;
                        menu : in out A_Sub_Menu ) is
    begin
        this.Add_Child( menu );
        menu.Append_To_Native_Menu( this.nativeMenu );
        menu := null;
    end Add_Menu;

    ----------------------------------------------------------------------------

    procedure On_Rooted( this : not null access Menubar'Class ) is
    begin
        Al_Set_Display_Menu( this.view.Get_Display, this.nativeMenu );
    end On_Rooted;

    ----------------------------------------------------------------------------

    procedure On_Unrooted( this : not null access Menubar'Class ) is
        temp : A_Allegro_Menu;
    begin
        -- this is not called at shutdown; it's only triggered when a menubar is
        -- added to the Window and then removed at runtime.
        temp := Al_Remove_Display_Menu( this.view.Get_Display );
        pragma Assert( temp = this.nativeMenu );
    end On_Unrooted;

end Widgets.Menubars;
