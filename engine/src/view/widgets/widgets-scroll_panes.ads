--
-- Copyright (c) 2012-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Widgets.Buttons;                   use Widgets.Buttons;

private with Widgets.Scrollbars;

package Widgets.Scroll_Panes is

    -- A Scroll_Pane is a simple container widget with horizontal and
    -- vertical scrollbars that control a client widget's viewport. The client
    -- is displayed within the Scroll_Pane's content area as a child widget.
    -- Clients should implement the Scrollable interface to support scrolling.
    type Scroll_Pane is new Widget with private;
    type A_Scroll_Pane is access all Scroll_Pane'Class;

    -- Creates a new Scroll_Pane within 'view' with id 'id'.
    function Create_Scroll_Pane( view : not null access Game_Views.Game_View'Class;
                                 id   : String := "" ) return A_Scroll_Pane;
    pragma Postcondition( Create_Scroll_Pane'Result /= null );

    -- Shows or hides the horizontal scrollbar. If 'draw' is True, the scrollbar
    -- will be drawn. Toggling the draw state of the scrollbar will repack the
    -- scroll pane.
    procedure Draw_Hbar( this : not null access Scroll_Pane'Class; draw : Boolean );

    -- Shows or hides the vertical scrollbar. If 'draw' is True, the scrollbar
    -- will be drawn. Toggling the draw state of the scrollbar will repack the
    -- scroll pane.
    procedure Draw_Vbar( this : not null access Scroll_Pane'Class; draw : Boolean );

    -- Returns a pointer to the client widget, if any. The Scroll_Pane maintains
    -- ownership.
    function Get_Client( this : not null access Scroll_Pane'Class ) return A_Widget;

    -- Removes and returns the client widget, if any. Ownership is transferred
    -- to the caller.
    function Remove_Client( this : not null access Scroll_Pane'Class ) return A_Widget;

    -- Sets the single client widget of the scroll pane. The client widget should
    -- implement the Scrollable interface to support scrolling. 'client' will
    -- become owned by the Scroll_Pane. If the scroll pane already has a client,
    -- the old one will be deleted. To preserve it, call Remove_Client() first.
    procedure Set_Client( this   : not null access Scroll_Pane'Class;
                          client : access Widget'Class );

private

    use Widgets.Scrollbars;

    BACKGROUND_ELEM : constant := 1;
    UP_ELEM         : constant := 2;
    DOWN_ELEM       : constant := 3;
    LEFT_ELEM       : constant := 4;
    RIGHT_ELEM      : constant := 5;
    HSCROLL_ELEM    : constant := 6;
    VSCROLL_ELEM    : constant := 7;

    DISABLED_STATE : constant Widget_State := 2**0;

    ----------------------------------------------------------------------------

    type Scroll_Pane is new Widget with
        record
            client   : A_Widget := null;
            hscroll  : A_H_Scrollbar := null;
            vscroll  : A_V_Scrollbar := null;
            btnLeft,
            btnRight,
            btnUp,
            btnDown  : A_Button := null;
            drawHbar,
            drawVbar : Boolean := True;
        end record;

    -- Adjust the scroll pane's child layout when the viewport size changes or
    -- the scroll bars visibility changes, or the style changes.
    procedure Adjust_Layout( this : not null access Scroll_Pane'Class );

    procedure Construct( this : access Scroll_Pane;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );

    -- Draws the scroll pane's background, including the small square in the
    -- lower right that appears when both scroll bars are visible.
    procedure Draw_Content( this : access Scroll_Pane );

    -- Returns the client's scrolling increment, in pixels (client content
    -- coordinates), in the X dimension. A default scroll increment will be
    -- returned if the scroll pane does not have a client, or the client does
    -- not implement Scrollable.
    function Get_Inc_X( this : not null access Scroll_Pane'Class ) return Float;

    -- Returns the client's scrolling increment, in pixels (client content
    -- coordinates), in the Y dimension. A default scroll increment will be
    -- returned if the scroll pane does not have a client, or the client does
    -- not implement Scrollable.
    function Get_Inc_Y( this : not null access Scroll_Pane'Class ) return Float;

    -- Returns the visual state of the scroll pane, based on its enabled state.
    function Get_Visual_State( this : access Scroll_Pane ) return Widget_State;

end Widgets.Scroll_Panes;
