--
-- Copyright (c) 2017-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Values.Construction;               use Values.Construction;
with Values.Strings;                    use Values.Strings;
with Widgets.Buttons;                   use Widgets.Buttons;
with Widgets.Buttons.Toggles;           use Widgets.Buttons.Toggles;
with Widget_Styles.Registry;            use Widget_Styles.Registry;

package body Widgets.Accordions is

    package Connections is new Signals.Connections(Accordion);
    use Connections;

    -- This is called when a header button is toggled on (open) or off (closed).
    procedure On_Button_Toggled( this : not null access Accordion'Class );

    -- This is called when a content widget finishes sliding open or closed. The
    -- signaller is the Height property of the sliding content widget.
    procedure On_Slide_Finished( this : not null access Accordion'Class );

    -- Applies the accordion's style to the header widgets when the widget's
    -- style changes or the first content widget is added.
    procedure On_Style_Changed( this : not null access Accordion'Class );

    -- Starts closing widget 'content'.
    procedure Start_Close( this    : not null access Accordion'Class;
                           content : not null A_Widget );

    -- Starts opening widget 'content'.
    procedure Start_Open( this    : not null access Accordion'Class;
                          content : not null A_Widget );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Accordion( view : not null access Game_Views.Game_View'Class;
                              id   : String := "" ) return A_Accordion is
        this : constant A_Accordion := new Accordion;
    begin
        this.Construct( view, id );
        return this;
    end Create_Accordion;

    ----------------------------------------------------------------------------

    procedure Append( this    : not null access Accordion'Class;
                      content : not null access Widget'Class;
                      title   : String ) is
    begin
        this.Insert( this.Count + 1, content, title );
    end Append;

    ----------------------------------------------------------------------------

    procedure Clear( this : not null access Accordion'Class ) is
    begin
        if this.Count > 0 then
            this.column.Clear;
            this.On_Style_Changed;      -- recalculate the header height
        end if;
    end Clear;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Accordion;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String ) is
    begin
        Widget(this.all).Construct( view, id, "Accordion" );

        this.StyleChanged.Connect( Slot( this, On_Style_Changed'Access ) );

        this.column := Create_Column_Layout( view );
        this.Add_Child( this.column );
        this.column.Set_Style_Property( "background.spacing", Create( 0 ) );
        this.column.Set_Style_Property( "background.align", Create( "top_center" ) );
        this.column.Fill( this );
    end Construct;

    ----------------------------------------------------------------------------

    function Count( this : not null access Accordion'Class ) return Natural is (this.column.Count / 2);

    ----------------------------------------------------------------------------

    function Find( this  : not null access Accordion'Class;
                   title : String;
                   start : Positive := 1 ) return Natural is
    begin
        for i in start..this.Count loop
            if A_Button(this.column.Get_At( i * 2 - 1 )).Get_Text = title then
                return i;
            end if;
        end loop;
        return 0;
    end Find;

    ----------------------------------------------------------------------------

    function Get_Content( this : not null access Accordion'Class; index : Integer ) return A_Widget is (this.column.Get_At( index * 2 ));

    ----------------------------------------------------------------------------

    function Get_Expander( this : not null access Accordion'Class ) return Natural is
        expander : constant A_Widget := this.column.Get_Expander;
    begin
        if expander /= null then
            for i in 1..this.Count loop
                if expander = this.column.Get_At( i * 2 ) then
                    return i;
                end if;
            end loop;
        end if;
        return 0;
    end Get_Expander;

    ----------------------------------------------------------------------------

    overriding
    function Get_Preferred_Height( this : access Accordion ) return Float is
    begin
        if not this.prefHeight.Is_Null then
            return this.prefHeight.To_Float;
        end if;
        return A_Widget(this.column).Get_Preferred_Height;
    end Get_Preferred_Height;

    ----------------------------------------------------------------------------

    overriding
    function Get_Preferred_Width( this : access Accordion ) return Float is
    begin
        if not this.prefWidth.Is_Null then
            return this.prefWidth.To_Float;
        end if;
        return A_Widget(this.column).Get_Preferred_Width;
    end Get_Preferred_Width;

    ----------------------------------------------------------------------------

    function Get_Title( this : not null access Accordion'Class; index : Integer ) return String is
        button : constant A_Button := A_Button(this.column.Get_At( index * 2 - 1 ));
    begin
        if button /= null then
            return button.Get_Text;
        end if;
        return "";
    end Get_Title;

    ----------------------------------------------------------------------------

    procedure Insert( this    : not null access Accordion'Class;
                      index   : Integer;
                      content : not null access Widget'Class;
                      title   : String ) is
        newIndex : Integer;
        button   : A_Button;
    begin
        newIndex := Integer'Min( Integer'Max( 1, index ), this.Count + 1 );

        button := Create_Toggle_Button( this.Get_View );
        button.Set_Style( this.style.Get_Style( HEADER_ELEM ) );
        button.Set_State( True );
        button.Set_Text( title );
        button.Pressed.Connect( Slot( this, On_Button_Toggled'Access ) );
        button.Released.Connect( Slot( this, On_Button_Toggled'Access ) );

        -- widgets with a fixed height will maintain that height. if no fixed
        -- height is set, it will be sized by its preferred height.
        content.Set_Attribute( "fixedHeight", content.posHeight );

        content.Height.Finished.Connect( Slot( this, On_Slide_Finished'Access ) );
        content.Visible.Set( button.Get_State );

        this.column.Insert( newIndex * 2 - 1, content );     -- takes ownership of 'content'
        this.column.Insert( newIndex * 2 - 1, button );      -- takes ownership of 'button'

        if this.Count = 1 then
            this.On_Style_Changed;          -- update the header height
        end if;

        this.Update_Geometry;
    end Insert;

    ----------------------------------------------------------------------------

    procedure On_Button_Toggled( this : not null access Accordion'Class ) is
        content : A_Widget;
    begin
        for i in 1..this.Count loop
            if this.column.Get_At( i * 2 - 1 ) = A_Widget(this.Signaller) then
                content := this.column.Get_At( i * 2 );
                exit;
            end if;
        end loop;
        pragma Assert( content /= null );

        if A_Button(this.Signaller).Get_State then
            this.Start_Open( content );
        else
            this.Start_Close( content );
        end if;
    end On_Button_Toggled;

    ----------------------------------------------------------------------------

    procedure On_Slide_Finished( this : not null access Accordion'Class ) is
        content  : constant A_Widget := A_Widget(Animated_Float.Property(this.Signaller.all).Get_Owner);
        expander : constant Boolean := content.Get_Attribute( "expander" ) = Create( True );
        opened   : constant Boolean := content.Visible.Get;
    begin
        if expander then
            if opened then
                -- when the expander widget finishes sliding open, set it back
                -- to being the expander so to can react to changes in the
                -- accordion's height.
                content.Unset_Height;
                this.column.Set_Expander( content );
            end if;
        elsif content.Get_Attribute( "fixedHeight" ).Is_Null then
            -- the widget height is determined by its preferred height, so clear
            -- the fixed height set by the slide. this allows it to react to
            -- changes its contents' size.
            if opened then
                content.Unset_Height;
            end if;
        end if;
    end On_Slide_Finished;

    ----------------------------------------------------------------------------

    procedure On_Style_Changed( this : not null access Accordion'Class ) is
    begin
        -- apply the header style element to each button
        for i in 1..this.Count loop
            this.column.Get_At( i * 2 - 1 ).Set_Style( this.style.Get_Style( HEADER_ELEM ) );
        end loop;
    end On_Style_Changed;

    ----------------------------------------------------------------------------

    function Remove( this : not null access Accordion'Class; index : Integer ) return A_Widget is
        button  : A_Widget;
        content : A_Widget;
    begin
        if index < 1 or this.Count < index then
            return null;
        end if;

        content := this.column.Remove( index * 2 );
        button := this.column.Remove( index * 2 - 1 );
        Delete( button );

        -- clear attributes used by the Accordion
        content.Set_Attribute( "expander", Null_Value );
        content.Set_Attribute( "fixedHeight", Null_Value );

        return content;
    end Remove;

    ----------------------------------------------------------------------------

    function Replace( this    : not null access Accordion'Class;
                      index   : Integer;
                      content : A_Widget ) return A_Widget is
        removed : A_Widget;
    begin
        if content = null or index < 1 or this.Count < index then
            return null;
        end if;

        removed := this.column.Remove( index * 2 );
        content.Visible.Set( removed.Is_Visible );
        this.column.Insert( index * 2, content );

        -- clear attributes used by the Accordion
        removed.Set_Attribute( "expander", Null_Value );
        removed.Set_Attribute( "fixedHeight", Null_Value );

        return removed;
    end Replace;

    ----------------------------------------------------------------------------

    procedure Set_Expander( this  : not null access Accordion'Class;
                            index : Integer ) is
        content : constant A_Widget := this.column.Get_At( index * 2 );
        current : constant A_Widget := this.column.Get_Expander;
    begin
        if content /= current then
            if current /= null then
               current.Set_Attribute( "expander", Null_Value );
            end if;
            content.Set_Attribute( "expander", Create( True ) );
            content.Unset_Height;
            this.column.Set_Expander( content );
        end if;
    end Set_Expander;

    ----------------------------------------------------------------------------

    procedure Set_Title( this  : not null access Accordion'Class;
                         index : Integer;
                         title : String ) is
        button : constant A_Button := A_Button(this.column.Get_At( index * 2 - 1 ));
    begin
        if button /= null then
            button.Set_Text( title );
        end if;
    end Set_Title;

    ----------------------------------------------------------------------------

    procedure Start_Close( this    : not null access Accordion'Class;
                           content : not null A_Widget ) is
        expander : constant Boolean := content.Get_Attribute( "expander" ) = Create( True );
        height   : Float;
    begin
        -- Start the visible animation first, so it finishes first. this allows
        -- the On_Slide_Finished to know if the slide is opening or closing by
        -- checking the widget's visibility.
        content.Visible.Set( False, SLIDE_TIME, Impulse_In );

        if expander then
            -- capture the widget's actual height, then remove it as expander
            -- and set its fixed height for the start of the slide.
            height := content.Get_Geometry.height;
            this.column.Set_Expander( null );
            content.Height.Set( height );
        end if;

        if not content.Height.Animating then
            -- save the fixed height prior to closing the widget
            if content.Get_Attribute( "fixedHeight" ).Is_Number then
                content.Set_Attribute( "fixedHeight", content.posHeight );
            end if;
        end if;

        content.Height.Set( 0.0, SLIDE_TIME, Quad_In );
    end Start_Close;

    ----------------------------------------------------------------------------

    procedure Start_Open( this    : not null access Accordion'Class;
                          content : not null A_Widget ) is
        expander    : constant Boolean := content.Get_Attribute( "expander" ) = Create( True );
        startHeight : Float;
        openHeight  : Float;
    begin
        content.Visible.Set( True );

        if not expander then
            if content.Get_Attribute( "fixedHeight" ).Is_Number then
                -- return to the previous fixed height
                openHeight := content.Get_Attribute( "fixedHeight" ).To_Float;
            else
                openHeight := content.Get_Preferred_Height;
            end if;
        else
            startHeight := content.Get_Geometry.height;
            content.Unset_Height;

            -- set 'content' as the expander, to get its open height, then
            -- remove it as the expander and set its fixed height for the start
            -- of the slide.
            this.column.Set_Expander( content );
            openHeight := this.column.Get_Geometry.height;
            this.column.Set_Expander( null );
            content.Height.Set( startHeight );

            -- 'content' will be set as the expander when it finishes sliding
        end if;

        content.Height.Set( openHeight, SLIDE_TIME, Quad_Out );
    end Start_Open;

begin

    Register_Style( "Accordion",
                    (HEADER_ELEM => To_Unbounded_String( "header:Button" )),
                    (HEADER_ELEM => Widget_Element),
                    (0 => To_Unbounded_String( "disabled" )) );

end Widgets.Accordions;
