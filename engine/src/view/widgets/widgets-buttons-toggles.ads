--
-- Copyright (c) 2012-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

limited with Game_Views;

package Widgets.Buttons.Toggles is

    -- A Toggle_Button is a variation of a button that toggles its state when
    -- pressed with a mouse or activated with the keyboard. It's state remains
    -- until the mouse presses on it again to toggle the state back again.
    type Toggle_Button is new Button with private;
    type A_Toggle_Button is access all Toggle_Button'Class;

    -- Creates a new toggle button within 'view' with id 'id'. If 'id' is an
    -- empty string, one will be randomly generated. The default state of the
    -- new button is False.
    function Create_Toggle_Button( view : not null access Game_Views.Game_View'Class;
                                   id   : String := "" ) return A_Button;
    pragma Postcondition( Create_Toggle_Button'Result /= null );

    -- Creates a new toggle button within 'view' with id 'id'. 'text' is the
    -- button's text and 'icon' is an optional icon to display. The default
    -- state of the new button is False.
    function Create_Toggle_Button( view : not null access Game_Views.Game_View'Class;
                                   id   : String;
                                   text : String;
                                   icon : Icon_Type := NO_ICON ) return A_Button;
    pragma Postcondition( Create_Toggle_Button'Result /= null );

    -- Creates a new toggle button within 'view' with id 'id' and no text label.
    -- 'icon' is the icon to display.
    function Create_Toggle_Button( view : not null access Game_Views.Game_View'Class;
                                   id   : String;
                                   icon : Icon_Type ) return A_Button
    is (Create_Toggle_Button( view, id, "", icon ));
    pragma Postcondition( Create_Toggle_Button'Result /= null );

private

    type Toggle_Button is new Button with null record;

    procedure Construct( this : access Toggle_Button;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String;
                         text : String;
                         icon : Icon_Type );

end Widgets.Buttons.Toggles;
