--
-- Copyright (c) 2016-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Assertions;                    use Ada.Assertions;
with Resources;                         use Resources;
with Resources.Plaintext;               use Resources.Plaintext;
with Scribble;                          use Scribble;
with Scribble.Parse_Errors;             use Scribble.Parse_Errors;
with Scribble.Token_Locations;          use Scribble.Token_Locations;
with Support.Paths;                     use Support.Paths;
with Widget_Styles.Parsers;             use Widget_Styles.Parsers;

package body Widget_Styles.Registry is

    package Definition_Maps is new Ada.Containers.Indefinite_Ordered_Maps( String, A_Style_Definition, "<", "=" );
    use Definition_Maps;

    definitions : Definition_Maps.Map;

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    not overriding
    procedure Construct( this         : access Style_Definition;
                         widgetType   : String;
                         elementNames : String_Array;
                         elementTypes : Element_Type_Array;
                         stateNames   : String_Array ) is
        delim : Integer;
    begin
        Limited_Object(this.all).Construct;
        this.widgetType := To_Unbounded_String( widgetType );
        this.elements := new Element_Definition_Array(1..elementNames'Length);
        for i in this.elements'Range loop
            this.elements(i).elemType := elementTypes(i);
            delim := Index( elementNames(i), ":" );
            if elementTypes(i) = Widget_Element then
                -- the element name must be formatted as "ElemName:SubWidgetType"
                if delim < 1 then
                    raise Constraint_Error with "Sub widget type missing";
                end if;
                this.elements(i).name := Head( elementNames(i), delim - 1 );
                this.elements(i).widgetType := Unbounded_Slice( elementNames(i), delim+1, Length( elementNames(i) ) );
            else
                if delim >= 1 then
                    raise Constraint_Error with "Unexpected sub widget type";
                end if;
                this.elements(i).name := elementNames(i);
            end if;
        end loop;
        this.stateNames := new String_Array'(stateNames);
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Style_Definition ) is
    begin
        Free( this.elements );
        Free( this.stateNames );
        for i of this.instances loop
            Delete( i );
        end loop;
        this.instances.Clear;
        Limited_Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Create_Style( this : not null access Style_Definition'Class;
                           name : String ) return A_Widget_Style is
        style : A_Widget_Style;
        def   : A_Style_Definition;
    begin
        style := Create_Widget_Style( To_String( this.widgetType ), name, this.elements'Length );
        for i in style.elements'Range loop
            case this.elements(i).elemType is
                when Area_Element   => style.elements(i) := Create_Area_Style( 2 ** this.stateNames'Length );
                when Icon_Element   => style.elements(i) := Create_Icon_Style( 2 ** this.stateNames'Length );
                when Text_Element   => style.elements(i) := Create_Text_Style( 2 ** this.stateNames'Length );
                when Widget_Element =>
                    def := Get_Definition( To_String( this.elements(i).widgetType ) );
                    Assert( def /= null, "Invalid widget type '" & To_String( this.elements(i).widgetType ) & "'" );
                    style.elements(i) := A_Element_Style(def.Create_Style( "" ));
            end case;
        end loop;
        return style;
    end Create_Style;

    ----------------------------------------------------------------------------

    function Get_Element( this     : not null access Style_Definition'Class;
                          elemName : String;
                          element  : out Element_Index ) return Boolean is
    begin
        element := Element_Index'First;
        for i in this.elements'Range loop
            if elemName = this.elements(i).name then
                element := i;
                return True;
            end if;
        end loop;
        return False;
    end Get_Element;

    ----------------------------------------------------------------------------

    function Get_State( this      : not null access Style_Definition'Class;
                        stateName : String;
                        state     : out Widget_State ) return Boolean is
    begin
        state := 0;
        for i in this.stateNames'Range loop
            if stateName = this.stateNames(i) then
                state := Widget_State(2 ** i);
                return True;
            end if;
        end loop;
        return False;
    end Get_State;

    ----------------------------------------------------------------------------

    function Get_Style_Ref( this : not null access Style_Definition'Class;
                            name : String ) return A_Widget_Style is
        pos   : constant Style_Maps.Cursor := this.instances.Find( name );
        style : A_Widget_Style;
    begin
        if Has_Element( pos ) then
            style := Element( pos );
        else
            style := this.Create_Style( name );
            this.instances.Insert( name, style );
        end if;
        return style;
    end Get_Style_Ref;

    ----------------------------------------------------------------------------

    function Get_Sub_Style_Type( this    : not null access Style_Definition'Class;
                                 element : Element_Index ) return String
    is (To_String( this.elements(element).widgetType ));

    --==========================================================================

    procedure Copy_Named_Styles( fromName, toName : String ) is
        fromPos,
        toPos   : Style_Maps.Cursor;
    begin
        for def of definitions loop
            fromPos := def.instances.Find( fromName );
            if Has_Element( fromPos ) then
                toPos := def.instances.Find( toName );
                if Has_Element( toPos ) then
                    Element( toPos ).Merge_From( Element( fromPos ) );
                else
                    def.instances.Insert( toName, Clone( Element( fromPos ) ) );
                end if;
            end if;
        end loop;
    end Copy_Named_Styles;

    ----------------------------------------------------------------------------

    function Get_Definition( widgetType : String ) return A_Style_Definition is
        pos : constant Definition_Maps.Cursor := definitions.Find( widgetType );
    begin
        if Has_Element( pos ) then
            return Element( pos );
        end if;
        return null;
    end Get_Definition;

    ----------------------------------------------------------------------------

    function Get_Style( widgetType : String; styleName : String ) return A_Widget_Style is
        definition : constant A_Style_Definition := Get_Definition( widgetType );
    begin
        if definition /= null then
            return Clone( definition.Get_Style_Ref( styleName ) );
        end if;
        return null;
    end Get_Style;

    ----------------------------------------------------------------------------

    procedure Load_Style_Sheet( filename : String ) is
        content : Unbounded_String;
         parser : A_Parser;
    begin
        -- load the file from disk
        if not Load_Plaintext( filename, "resources", content ) then
            Raise_Parse_Error( "File not found", Token_Location'(0, 0, 0, To_Unbounded_String( filename ) ) );
        end if;

        -- parse and load the file
        --
        -- Scan_File() uses the filepath passed to it for two purposes:
        --
        --   1. displaying error information on a parser failure.
        --   2. determining the base directory to use for resolving relative
        --      paths in @import statements.
        --
        -- if the game assets are packaged into archives named after their
        -- resource groups, then Locate_Resource() will actually return a path
        -- to the archive containing 'filename' instead of the style file. so,
        -- for purpose #2, the folder of the archive is used and for purpose #1,
        -- the style filename replaces the archive name. if the style file isn't
        -- in an archive, then the expression below will resolve normally to the
        -- style file's path anyway.
        parser := Create_Parser;
        parser.Scan_File( content, Get_Directory( Locate_Resource( filename, "resources" ) ) & filename );   -- may raise Parse_Error
        Delete( parser );
    exception
        when Parse_Error =>
            Delete( parser );
            raise;
    end Load_Style_Sheet;

    ----------------------------------------------------------------------------

    procedure Register_Style( widgetType   : String;
                              elementNames : String_Array;
                              elementTypes : Element_Type_Array;
                              stateNames   : String_Array ) is
        definition : A_Style_Definition;
    begin
        Assert( not definitions.Contains( widgetType ), "Duplicate style definition for widget type: " & widgetType );
        definition := new Style_Definition;
        definition.Construct( widgetType, elementNames, elementTypes, stateNames );
        definitions.Insert( widgetType, definition );
    end Register_Style;

end Widget_Styles.Registry;
