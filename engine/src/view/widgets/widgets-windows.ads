--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Keyboard;                  use Allegro.Keyboard;
with Widgets.Menubars;                  use Widgets.Menubars;

package Widgets.Windows is

    -- A Window is the root of the widget tree within the application window.
    --
    -- All interface input events are delivered to the Window and then
    -- dispatched by the Window to its descendent widgets. Widget events are
    -- handled in a bottom-up approach, being delivered to the focused widget
    -- first and then dispatching up the tree, toward the Window, until the
    -- event is captured and handled.
    --
    -- Input focus, or just "focus", is tracked by the Window. Exactly one
    -- widget in the Window tree will have focus at all times. If no other
    -- widget is focused, the Window will be focused by default. The focused
    -- widget will receive all keyboard events.
    type Window is new Widget with private;
    type A_Window is access all Window'Class;

    -- Creates a new Window within 'view', filling the view's display size. Each
    -- Game_View has one Window widget at the root.
    function Create_Window( view : access Game_Views.Game_View'Class;
                            id   : String := "" ) return A_Window;
    pragma Postcondition( Create_Window'Result /= null );

    -- Adds a child widget to the Window. The Window will own 'child' after this
    -- call and will delete it upon destruction.
    procedure Add_Widget( this  : not null access Window'Class; child : not null access Widget'Class );

    -- Returns the mouse location relative to the widget content of 'requestor'.
    procedure Get_Mouse_Location( this      : not null access Window'Class;
                                  requestor : not null access Widget'Class;
                                  mx, my    : out Float );

    -- Attempts to transfer keyboard input focus to 'target'. If 'target'
    -- doesn't accept focus, then focus will be given to its "next focus" widget
    -- (what happens when you press the Tab key). If no "next focus" widget
    -- exists or accepts focus, then focus will be given to the widget's nearest
    -- ancestor that accepts it. Changing the input focus will prevent any
    -- potential key held and key release events from being sent to the old
    -- focused widget that received the key press.
    procedure Give_Focus( this   : not null access Window'Class;
                          target : not null access Widget'Class );

    -- This is called when a 'descendant' of the window becomes shown or hidden
    -- on the screen. Keyboard focus and mouse events are handled when a focused
    -- or mouse-pressed widget disappears under the mouse. 'descendant' must be
    -- rooted within this window.
    procedure Handle_Widget_Hidden( this       : not null access Window'Class;
                                    descendant : not null access Widget'Class );

    -- Returns True if the window has a menubar attached to it, otherwise False.
    function Has_Menubar( this : not null access Window'Class ) return Boolean;

    -- Returns True if a modal dialog is active in the window.
    function Is_Modal_Active( this : not null access Window'Class ) return Boolean;

    -- Returns True if a popup widget is visible in the window.
    function Is_Popup_Active( this : not null access Window'Class ) return Boolean;

    -- Pops the top-most widget off the popup stack. If there are no active
    -- popup widgets, nothing will happen.
    procedure Pop_Popup( this : not null access Window'Class );

    -- Pops all widgets from the popups stack until the given popup widget has
    -- been popped. If the widget isn't an active popup menu, nothing will
    -- happen.
    procedure Pop_Popup( this  : not null access Window'Class;
                         popup : not null access Widget'Class );

    -- Pushes a popup widget onto the top of the popup stack, making it visible.
    -- Popups are the highest in the Z-order, so they will be displayed in front
    -- of any modal widget, most recent on top. The widget will temporarily
    -- become a child of the window while its on the popup stack.
    procedure Push_Popup( this  : not null access Window'Class;
                          popup : not null access Widget'Class );

    -- Removes the widget from the window.
    procedure Remove_Widget( this : not null access Window'Class; child : not null access Widget'Class );

    -- Sets the Window's menu bar widget. The menu bar is optional and there can
    -- be only one per Window. 'menu' will be consumed. If a menu bar has
    -- already been set, the old one will be deleted.
    procedure Set_Menubar( this : not null access Window'Class; menu : in out A_Menubar );
    pragma Postcondition( menu = null );

    -- Sets the minimum allowed size of the window in display pixels when resizing.
    procedure Set_Min_Size( this   : not null access Window'Class;
                            width  : Integer;
                            height : Integer );

    -- Sets the modal widget drawn on top of all others in the Window. This
    -- widget and its descendents will receive all input events or prevent them
    -- from being sent to any others, while it is visible. Set 'modal' to null
    -- to clear the modal widget. 'modal' will be set visible when it's set and
    -- invisible when another modal widget is set or it's unset. The modal
    -- modal widget tree will be drawn in front of all other Window children and
    -- behind any active popup widgets.
    procedure Set_Modal( this : not null access Window'Class; modal : access Widget'Class );

    -- Sets the title at the top of the application window.
    procedure Set_Title( this : not null access Window'Class; title : String );

    -- Notifies the window that widget 'requestor' would like to show its mouse
    -- cursor. The window will determine if 'requestor' is the owner of the
    -- mouse cursor before changing the displayed cursor.
    --
    -- If the left mouse button is held down, the widget that accepted the left
    -- mouse press is the owner of the mouse cursor. Otherwise, the owner is the
    -- widget currently under the mouse. If no widget is under the mouse, the
    -- window itself is the owner.
    procedure Show_Mouse_Cursor( this      : not null access Window'Class;
                                 requestor : access Widget'Class );

    -- Deletes the Window.
    procedure Delete( this : in out A_Window );
    pragma Postcondition( this = null );

private

    -- an array of widgets by mouse button; used for tracking mouse button
    -- history relative to widgets.
    type Mouse_Widget_Array is array (Mouse_Button) of A_Widget;

    -- an array of widgets by key scancode. used for tracking key history
    -- relative to widgets.
    type Key_Widget_Array is array (1..ALLEGRO_KEY_MAX-1) of A_Widget;

    ----------------------------------------------------------------------------

    type Window is new Widget with
        record
            deleting       : Boolean := False;           -- true only in Delete()

            minWidth       : Natural := 0;               -- minimum allowed size hint
            minHeight      : Natural := 0;               -- in display pixels

            mouseX,
            mouseY         : Float := 0.0;
            mouseOver      : A_Widget := null;     -- widget mouse is over, or the one it was over when a mouse button was pressed
            mousePressedOn : Mouse_Widget_Array;
            mouseClickedOn : Mouse_Widget_Array;
            keyPressSentTo : Key_Widget_Array := Key_Widget_Array'(others=>null);

            menu           : A_Menubar := null;
            focus          : A_Widget := null;     -- keyboard focus
            modal          : A_Widget := null;     -- widget with modality in window
            popups         : Widget_Lists.List;    -- stack of active popup widgets
        end record;

    procedure Construct( this : access Window;
                         view : access Game_Views.Game_View'Class;
                         id   : String );

    procedure Delete( this : in out Window );

    -- Draws the whole Window, all its children, and then the popup widget stack
    -- in back-to-front order. The modal widget, if set, will be the front-most
    -- child. The popup widgets, if any are active, will still be drawn in front
    -- of the modal widget tree and menu bar. This procedure is used by the
    -- renderer to draw the whole OS window.
    procedure Draw( this : access Window );

    -- find a widget at the given screen coordinates (sx, sy) and return the
    -- corresponding widget content coordinates if a widget is found (wx, wy).
    procedure Find_Widget_At( this     : access Window;
                              sx, sy   : Float;
                              children : Boolean;
                              wx, wy   : out Float;
                              found    : out A_Widget );

    -- Returns the minimum allowed window height in display pixels. This is only
    -- used if the window is resizable.
    function Get_Min_Height( this : access Window ) return Float;

    -- Returns the minimum allowed window width in display pixels. This is only
    -- used if the window is resizable.
    function Get_Min_Width( this : access Window ) return Float;

    -- Handles all input events that the Window is registered to receive.
    procedure Handle_Event( this : access Window;
                            evt  : in out A_Event;
                            resp : out Response_Type );
    pragma Precondition( evt /= null );

end Widgets.Windows;
