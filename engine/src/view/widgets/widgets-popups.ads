--
-- Copyright (c) 2012-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Widgets.Menu_Items.Sub_Menus;      use Widgets.Menu_Items.Sub_Menus;

package Widgets.Popups is

    -- A Popup widget is a context-sensitive menu widget.
    type Popup is new Widget with private;
    type A_Popup is access all Popup'Class;

    -- Creates a new Popup menu within 'view' with id 'id'.
    function Create_Popup( view : not null access Game_Views.Game_View'Class;
                           id   : String := "" ) return A_Popup;
    pragma Postcondition( Create_Popup'Result /= null );

    -- Returns the popup's menu for adding and removing items.
    function Get_Menu( this : not null access Popup'Class ) return A_Sub_Menu;

    -- Shows the popup menu on the screen at the mouse location 'mouseX,mouseY'.
    -- The mouse will be warped to the given location, which causes the popup
    -- menu to appear there too.
    procedure Show( this : not null access Popup'Class; mouseX, mouseY : Float );

private

    type Popup is new Widget with
        record
            menu : A_Sub_Menu := null;
        end record;

    procedure Construct( this : access Popup;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );

    procedure Delete( this : in out Popup );

end Widgets.Popups;
