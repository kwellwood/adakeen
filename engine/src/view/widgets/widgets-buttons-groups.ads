--
-- Copyright (c) 2012-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

private with Ada.Containers.Doubly_Linked_Lists;

package Widgets.Buttons.Groups is

    -- A Button_Group is a logical control object that allows buttons to be
    -- linked. A button group is used to create a radio-button-like behavior
    -- with two or more toggleable buttons. Button_Group is not actually a
    -- widget and an instance must be explicitly owned by another object to
    -- prevent leaks. The Button_Group object doesn't maintain ownership of any
    -- buttons added to its membership.
    type Button_Group is new Limited_Object with private;
    type A_Button_Group is access all Button_Group'Class;

    -- Creates a new empty button group.
    function Create_Button_Group return A_Button_Group;
    pragma Postcondition( Create_Button_Group'Result /= null );

    -- Button Group Signals

    function Cleared( this : not null access Button_Group'Class ) return access Signal'Class;  -- emitted when selection is cleared
    function Set    ( this : not null access Button_Group'Class ) return access Signal'Class;  -- emitted when the selection changes (not cleared)

    -- Adds a button to the group's membership. If the button is currently
    -- pressed, it will become the group's new selection. If the group is empty
    -- and it must have a selected button, then 'button' will be pressed before
    -- it is added.
    procedure Add( this   : not null access Button_Group'Class;
                   button : not null A_Button );

    -- Removes all buttons from the group's membership.
    procedure Clear( this : not null access Button_Group'Class );

    -- Returns the currently selected button, if a button is selected. Otherwise,
    -- null will be returned.
    function Get_Selected( this : not null access Button_Group'Class ) return A_Button;

    -- Changes whether or not the active button in the group is allowed to be
    -- unselected. If 'keep' is set True (the default behavior) then the active
    -- button on a tool group, once pressed, can't be unpressed. This behavior
    -- will only take effect after a button in the group has been selected.
    procedure Set_Keep_Selected( this : not null access Button_Group'Class;
                                 keep : Boolean );

    -- Unpresses the active button in the group, if there is one. This will have
    -- no effect if the 'keep selected' option has been set unless 'force' is
    -- set True.
    procedure Unset( this  : not null access Button_Group'Class;
                     force : Boolean := False );

    -- Deletes the Button_Group.
    procedure Delete( this : in out A_Button_Group );
    pragma Postcondition( this = null );

private

    -- Provides a list of Buttons.
    package Button_Collection is new Ada.Containers.Doubly_Linked_Lists( A_Button, "=" );
    use Button_Collection;

    type Button_Group is new Limited_Object with
        record
            buttons      : Button_Collection.List;
            selected     : A_Button := null;   -- current selection (null if none)
            prevSelected : A_Button := null;   -- previous selection (may not be valid pointer)
            keepSelected : Boolean := True;    -- force a button to remain selected
            sigCleared   : aliased Signal;
            sigSet       : aliased Signal;
        end record;

    procedure Construct( this : access Button_Group );

    procedure Delete( this : in out Button_Group );

    -- Returns a string representation of a button group for debugging purposes.
    function To_String( this : access Button_Group ) return String;

end Widgets.Buttons.Groups;
