--
-- Copyright (c) 2012-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Icons;                             use Icons;

package Widgets.Panels.Dialogs is

    -- A Dialog is an abstract container widget for modal dialogs. All of the
    -- dialog's controls are added as children and a title can be set using the
    -- inherited operations from Panel. The Dialog class extends the Panel's
    -- functionality by adding Hide and Show procedures to hide and show the
    -- dialog as a modal widget in the window. If the dialog has a title bar, it
    -- can also be dragged with the mouse. Events to pause and resume gameplay
    -- will be queued when the dialog is shown and hidden, respectively, because
    -- it is modal.
    type Dialog is abstract new Panel with private;
    type A_Dialog is access all Dialog'Class;

    -- Returns the dialog's icon.
    function Get_Icon( this : not null access Dialog'Class ) return Icon_Type;

    -- Returns the dialog's title text.
    function Get_Title( this : not null access Dialog'Class ) return String;

    -- Hides the dialog. It remains a child of the window.
    procedure Hide( this : not null access Dialog'Class );

    -- Sets the icon on the title header to 'icon'.
    procedure Set_Icon( this : not null access Dialog'Class; icon : Icon_Type );

    -- Sets the dialog's title text.
    procedure Set_Title( this : not null access Dialog'Class; title : String );

    -- Shows the modal dialog in the window. The dialog must have been added
    -- to the window before this is called.
    procedure Show( this : not null access Dialog'Class );

private

    BACKGROUND_ELEM : constant := 1;
    BORDER_ELEM     : constant := 2;
    HEADER_ELEM     : constant := 3;
    ICON_ELEM       : constant := 4;
    TITLE_ELEM      : constant := 5;

    DISABLED_STATE : constant Widget_State := 2**0;

    ----------------------------------------------------------------------------

    type Dialog is abstract new Panel with
        record
            icon         : Icon_Type;
            title        : Unbounded_String;
            headerHeight : Float := 0.0;      -- height of the header text, icon and padding
            dragStartX,
            dragStartY   : Float := -1.0;
        end record;

    procedure Construct( this  : access Dialog;
                         view  : not null access Game_Views.Game_View'Class;
                         id    : String;
                         title : String;
                         icon  : Icon_Type );

    -- Draws the Dialog's background and border.
    procedure Draw_Content( this : access Dialog );

    function Get_Min_Height( this : access Dialog ) return Float;

    function Get_Min_Width( this : access Dialog ) return Float;

end Widgets.Panels.Dialogs;
