--
-- Copyright (c) 2017-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Widgets.Row_Layouts;            use Widgets.Row_Layouts;

package Widgets.Accordions is

    -- An Accordion represents a column of individually titled and expandable or
    -- collapsible sections. Each header / title bar of each section collapses
    -- and expands the client widget beneath it.
    type Accordion is new Widget with private;
    type A_Accordion is access all Accordion'Class;

    -- Creates a new Accordion within 'view' with id 'id'.
    function Create_Accordion( view : not null access Game_Views.Game_View'Class;
                               id   : String := "" ) return A_Accordion;
    pragma Postcondition( Create_Accordion'Result /= null );

    -- Appends a new section 'content', identified by 'title' on its header. The
    -- Accordion will own 'content' after this call and will delete it upon
    -- destruction.
    procedure Append( this    : not null access Accordion'Class;
                      content : not null access Widget'Class;
                      title   : String );

    -- Removes and deletes all sections.
    procedure Clear( this : not null access Accordion'Class );

    -- Returns the number of sections.
    function Count( this : not null access Accordion'Class ) return Natural;

    -- Returns the index of the first section matching 'title', starting at
    -- index 'start'. If no match is found, 0 will be returned.
    function Find( this  : not null access Accordion'Class;
                   title : String;
                   start : Positive := 1 ) return Natural;

    -- Returns a pointer to the section's content at index 'index' (starting at
    -- 1). If the index does not exist, null will be returned.
    function Get_Content( this : not null access Accordion'Class; index : Integer ) return A_Widget;

    -- Returns the index of the expanding content, if set. Otherwise, zero will
    -- be returned.
    function Get_Expander( this : not null access Accordion'Class ) return Natural;

    -- Returns the title of the section at index 'index' (starting at 1). If the
    -- index does not exist, an empty string will be returned.
    function Get_Title( this : not null access Accordion'Class; index : Integer ) return String;

    -- Inserts a section before index 'index' (starting at 1), identified by
    -- 'title' its header. This is equivalent to appending the section if
    -- 'index' is greater than the tab count. The Accordion will own 'content'
    -- after this call and will delete it upon destruction.
    procedure Insert( this    : not null access Accordion'Class;
                      index   : Integer;
                      content : not null access Widget'Class;
                      title   : String );

    -- Removes and returns the content of the section at index 'index' (starting
    -- at 1). The caller receives ownership of the returned widget. If the index
    -- does not exist, null will be returned.
    function Remove( this : not null access Accordion'Class; index : Integer ) return A_Widget;

    -- Replaces the content of the section at index 'index' (starting at 1) with
    -- widget 'content', and returns the current content. If the index does not
    -- exist, null will be returned and 'content' will not be consumed. If
    -- 'content' is null, null will be returned and nothing will change.
    function Replace( this    : not null access Accordion'Class;
                      index   : Integer;
                      content : A_Widget ) return A_Widget;

    -- Sets the index that expands and shrinks its content to fill extra space
    -- after all other visible content widgets have been given their preferred
    -- space. If 'index' is not a valid index, then the current expander index
    -- will be cleared.
    procedure Set_Expander( this  : not null access Accordion'Class;
                            index : Integer );

    -- Sets the title of the section at index 'index' to 'title'. If the index
    -- is not valid, nothing will change.
    procedure Set_Title( this  : not null access Accordion'Class;
                         index : Integer;
                         title : String );

private

    HEADER_ELEM : constant := 1;           -- a header button

    DISABLED_STATE : constant Widget_State := 2**0;

    SLIDE_TIME : constant Time_Span := Milliseconds( 250 );

    ----------------------------------------------------------------------------

    type Accordion is new Widget with
        record
            column : A_Column_Layout;      -- odd indices are headers, even are content
        end record;

    procedure Construct( this : access Accordion;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );

    overriding
    function Get_Min_Height( this : access Accordion ) return Float is (A_Widget(this.column).Get_Min_Height);

    overriding
    function Get_Min_Width( this : access Accordion ) return Float is (A_Widget(this.column).Get_Min_Width);

    function Get_Preferred_Height( this : access Accordion ) return Float;

    function Get_Preferred_Width( this : access Accordion ) return Float;

end Widgets.Accordions;
