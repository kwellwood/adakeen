--
-- Copyright (c) 2016-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Doubly_Linked_Lists;
with Scribble.Generic_Tokens;
with Scribble.Scanners;
with Scribble.Token_Locations;          use Scribble.Token_Locations;
with Scribble.Tokenizers;
with Values.Maps;                       use Values.Maps;
with Widget_Styles.Token_Types;         use Widget_Styles.Token_Types;

package Widget_Styles.Parsers is

    -- STYLE FILE GRAMMAR
    ----------------------------------------------------------------------------
    -- TOP-LEVEL
    --
    -- File   ::= {Line}
    --   Line ::= Directive | Rule
    --
    -- Directive ::= '@' (Import | Copy) ';'
    --   Import  ::= 'import' string
    --   Copy    ::= 'copy' Identifier 'from' Identifier
    --
    -- Rule ::= Selector Declarations
    --
    ----------------------------------------------------------------------------
    -- SELECTORS
    --
    -- Selector    ::= [StyleName] Widget '.' Element ['.' Element] {':' State}
    --   StyleName ::= Identifier
    --   Widget    ::= Identifier
    --   Element   ::= Identifier
    --   State     ::= Identifier
    --
    -- Identifier     ::= symbol-token
    --   symbol-token ::= letter {letter | digit | '_'}
    --
    ----------------------------------------------------------------------------
    -- DECLARATIONS
    --
    -- Declarations ::= '{' Declaration {';' Declaration} '}'
    --
    -- Declaration ::= Property ':' Value
    --   Property  ::= Identifier
    --   Value     ::= string | number | color | boolean
    --
    ----------------------------------------------------------------------------
    type Parser is tagged limited private;
    type A_Parser is access all Parser'Class;

    -- Creates and returns a new style file parser.
    function Create_Parser return A_Parser;

    -- Parses and loads widget styles from 'source' using the style file syntax.
    -- Comments are ignored. See the syntax above.
    --
    -- A Parse_Error will be raised if an error occurs during parsing. If
    -- additional unrelated text follows, not including comments and whitespace,
    -- a Parse_Error will be raised.
    procedure Scan_File( this     : not null access Parser'Class;
                         source   : Unbounded_String;
                         filepath : String );

    procedure Delete( this : in out A_Parser );

private

    package Tokens is new Scribble.Generic_Tokens(Token_Type, Name, To_Token_Type, TK_SYMBOL, TK_VALUE, TK_EOL, TK_EOF, Is_Delimiter);
    package Tokenizers is new Scribble.Tokenizers(Tokens);
    package Scanners is new Scribble.Scanners(Tokenizers);
    use Scanners;

    package Token_Lists is new Ada.Containers.Doubly_Linked_Lists(Tokens.Token_Ptr, Tokens."=");
    use Token_Lists;

    type Selector_Type is
        record
            styleName  : Unbounded_String;
            widgetType : Tokens.Token_Ptr;
            element    : Tokens.Token_Ptr;
            subElement : Tokens.Token_Ptr;
            stateList  : Token_Lists.List;
        end record;

    ----------------------------------------------------------------------------

    type Parser is tagged limited
        record
            scanner : A_Scanner := null;
            path    : Unbounded_String;
            source  : Unbounded_String;
        end record;

    -- Executes a rule, setting all properties in 'properties' for style
    -- element 'element' in visual state 'state'. 'loc' is the lexical location
    -- of the property declarations in the source code, only used to raise a
    -- Parse_Error exception if one of the properties is invalid.
    procedure Execute_Rule( this       : not null access Parser'Class;
                            element    : not null A_Element_Style;
                            state      : Widget_State;
                            properties : Map_Value'Class;
                            loc        : Token_Location );

    -- Parses a block of declarations, returning them as a map. If a declaration
    -- block is not found, a Parse_Error will be raised.
    procedure Expect_Declarations( this         : not null access Parser'Class;
                                   declarations : in out Map_Value;
                                   location     : in out Token_Location );

    function Scan_Directive( this : not null access Parser'Class ) return Boolean;

    function Scan_Rule( this : not null access Parser'Class ) return Boolean;

    -- Reads a selector into 'selector', returning True, if the next tokens in
    -- the stream are a selector. If no selector is encountered, False will be
    -- returned.
    function Scan_Selector( this     : not null access Parser'Class;
                            selector : out Selector_Type ) return Boolean;

end Widget_Styles.Parsers;

