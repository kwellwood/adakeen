--
-- Copyright (c) 2014-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Assets.Fonts;                      use Assets.Fonts;
--with Debugging;                         use Debugging;
with Drawing;                           use Drawing;
with Drawing.Bitmaps;                   use Drawing.Bitmaps;
with Drawing.Primitives;                use Drawing.Primitives;
with Drawing.Tiles;                     use Drawing.Tiles;
with Game_Views;                        use Game_Views;
with Icons;                             use Icons;
with Keyboard;                          use Keyboard;
with Styles;                            use Styles;
with Support;                           use Support;
with Values.Construction;               use Values.Construction;
with Widget_Styles.Registry;            use Widget_Styles.Registry;
with Widgets.Enumerations.Popups;       use Widgets.Enumerations.Popups;

package body Widgets.Enumerations is

    package Connections is new Signals.Connections(Enumeration);
    use Connections;

    package Key_Connections is new Signals.Keys.Connections(Enumeration);
    use Key_Connections;

    package Mouse_Connections is new Signals.Mouse.Connections(Enumeration);
    use Mouse_Connections;

    POPUP_MENU : constant String := ":popup";
    MENU_ITEM  : constant String := ":item";

    ----------------------------------------------------------------------------

    procedure Build_Popup( this : not null access Enumeration'Class );

    procedure Clicked_Option( this : not null access Enumeration'Class );

    procedure On_Key_Down( this : not null access Enumeration'Class );

    procedure On_Key_Up( this : not null access Enumeration'Class );

    -- Handles the left mouse button to open/close the enumeration.
    procedure On_Mouse_Pressed( this : not null access Enumeration'Class );

    procedure On_Style_Changed( this : not null access Enumeration'Class );

    procedure Popup_Hidden( this : not null access Enumeration'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Enum( view : not null access Game_Views.Game_View'Class;
                          id   : String := "" ) return A_Enumeration is
        this : A_Enumeration := new Enumeration;
    begin
        this.Construct( view, id );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Enum;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Enumeration;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String  ) is
    begin
        Widget(this.all).Construct( view, id, "Enumeration" );
        this.sigSelChanged.Init( this );

        this.KeyTyped.Connect( Slot( this, On_Key_Down'Access, ALLEGRO_KEY_DOWN, (others=>No) ) );
        this.KeyTyped.Connect( Slot( this, On_Key_Up'Access, ALLEGRO_KEY_UP, (others=>No) ) );
        this.MousePressed.Connect( Slot( this, On_Mouse_Pressed'Access, Mouse_Left ) );
        this.StyleChanged.Connect( Slot( this, On_Style_Changed'Access ) );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Enumeration ) is
    begin
        if this.popupMenu /= null then
            this.popupMenu.Hidden.Disconnect( Slot( this, Popup_Hidden'Access ) );
            Delete( this.popupMenu );
        end if;
        Widget(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function SelectionChanged( this : not null access Enumeration'Class ) return access Signal'Class is (this.sigSelChanged'Access);

    ----------------------------------------------------------------------------

    procedure Add_Item( this : not null access Enumeration'Class;
                        text : String;
                        val  : Value'Class ) is
    begin
        this.items.Append( Item_Record'(To_Unbounded_String( text ), Clone( val )) );
        if this.Item_Count = 1 then
            -- automatically select the first item
            this.Set_Index( 1 );
        end if;
    end Add_Item;

    ----------------------------------------------------------------------------

    procedure Build_Popup( this : not null access Enumeration'Class ) is
        menuItem : A_Enum_Item;
    begin
        if this.popupMenu = null and then not this.items.Is_Empty then
            this.popupMenu := A_Widget(Create_Popup( this.Get_View, To_String( this.id & POPUP_MENU ) ));
            this.popupMenu.Set_Style( this.style.Get_Style( MENU_ELEM ) );
            this.popupMenu.Hidden.Connect( Slot( this, Popup_Hidden'Access ) );
            for i in 1..this.Item_Count loop
                menuItem := Create_Enum_Item( this.Get_View,
                                              this.popupMenu.Get_Id & MENU_ITEM & Image( i ),
                                              To_String( this.items.Element( i ).text ) );
                menuItem.Set_Attribute( "item", i );
                menuItem.Set_Selected( i = this.index );
                menuItem.Set_Style( this.style.Get_Style( ITEM_ELEM ) );
                menuItem.Clicked.Connect( Slot( this, Clicked_Option'Access ) );
                A_Enum_Popup(this.popupMenu).Add_Item( menuItem );
            end loop;
        end if;
    end Build_Popup;

    ----------------------------------------------------------------------------

    procedure Clicked_Option( this : not null access Enumeration'Class ) is
        index : Value;
    begin
        index := A_Widget(this.Signaller).Get_Attribute( "item" );
        if index.Is_Number then
            this.Set_Open( False );
            this.Set_Index( index.To_Int );
        end if;
    end Clicked_Option;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Content( this : access Enumeration ) is
        state         : constant Widget_State := this.Get_Visual_State;
        align         : constant Align_Type := this.style.Get_Align( BACKGROUND_ELEM, state );
        text          : constant String := this.Get_Text;
        icon          : constant Icon_Type := this.style.Get_Image( ICON_ELEM, state );
        color         : Allegro_Color;
        iconWidth,
        iconHeight    : Float;
        iconX, iconY  : Float;
        textWidth     : Float := 0.0;
        textHeight    : Float := 0.0;
        textX, textY  : Float;
        contentWidth,
        contentHeight : Float;
        contentX,
        contentY      : Float;
        font          : A_Font;
    begin
        -- - - - Calculate Positions - - - --

        -- calculate icon size --
        iconWidth := Float'Min( Float(icon.Get_Width), this.viewport.width );
        iconHeight := Float'Min( Float(icon.Get_Height), this.viewport.height );

        -- calculate text size --
        if text'Length > 0 then
            font := this.style.Get_Font( TEXT_ELEM, state );
            textHeight := Float(font.Line_Height);
            textWidth := Float(font.Text_Length( text ));
        end if;

        -- calculate total content size --
        contentWidth := textWidth;
        contentHeight := Float'Max( iconHeight, textHeight );

        -- calculate alignment --
        Align_Rect( align,
                    this.viewport.width - (this.viewport.height +
                                           this.style.Get_Pad_Left( BACKGROUND_ELEM, state ) +
                                           this.style.Get_Pad_Right( BACKGROUND_ELEM, state )),
                    this.viewport.height - (this.style.Get_Pad_Top( BACKGROUND_ELEM, state ) +
                                            this.style.Get_Pad_Bottom( BACKGROUND_ELEM, state )),
                    contentWidth, contentHeight,
                    contentX, contentY );
        contentX := this.style.Get_Pad_Left( BACKGROUND_ELEM, state ) + contentX;
        contentY := this.style.Get_Pad_Top( BACKGROUND_ELEM, state ) + contentY;
        iconX := Float'Rounding( this.viewport.width - (this.viewport.height / 2.0) - (iconWidth / 2.0) + this.style.Get_Offset_X( ICON_ELEM, state ) );
        iconY := Float'Rounding( contentY + Align_Vertical( align, contentHeight, iconHeight ) + this.style.Get_Offset_Y( ICON_ELEM, state ) );
        textX := contentX + this.style.Get_Offset_X( TEXT_ELEM, state );
        textY := contentY + Align_Vertical( align, contentHeight, textHeight ) + this.style.Get_Offset_Y( TEXT_ELEM, state );

        -- - - - Draw Content - - - --

        -- draw the text background --
        color := this.style.Get_Color( BACKGROUND_ELEM, state );
        if color /= Transparent then
            Rectfill( 0.0, 0.0,
                      this.viewport.width - this.viewport.height, this.viewport.height,
                      Lighten( color, this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );
        else
            Draw_Tile_Filled( this.style.Get_Image( BACKGROUND_ELEM, state ).Get_Tile,
                              0.0, 0.0, 0.0,
                              this.viewport.width - this.viewport.height, this.viewport.height,
                              Lighten( this.style.Get_Tint( BACKGROUND_ELEM, state ),
                                       this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );
        end if;

        -- draw the button background --
        color := this.style.Get_Color( BUTTON_ELEM, state );
        if color /= Transparent then
            Rectfill(this.viewport.width - this.viewport.height, 0.0,
                      this.viewport.width, this.viewport.height,
                      Lighten( color, this.style.Get_Shade( BUTTON_ELEM, state ) ) );
        else
            Draw_Tile_Filled( this.style.Get_Image( BUTTON_ELEM, state ).Get_Tile,
                              this.viewport.width - this.viewport.height, 0.0, 0.0,
                              this.viewport.height, this.viewport.height,
                              Lighten( this.style.Get_Tint( BUTTON_ELEM, state ),
                                       this.style.Get_Shade( BUTTON_ELEM, state ) ) );
        end if;

        -- draw the border --
        Draw_Tile_Filled( this.style.Get_Image( BORDER_ELEM, state ).Get_Tile,
                          0.0, 0.0, 0.0,
                          this.viewport.width, this.viewport.height,
                          Lighten( this.style.Get_Tint( BORDER_ELEM, state ),
                                   this.style.Get_Shade( BORDER_ELEM, state ) ) );

        -- draw the content --
        if icon.Get_Bitmap /= null then
            Draw_Bitmap_Stretched( icon.Get_Bitmap,
                                   iconX, iconY, 0.0,
                                   iconWidth, iconHeight,
                                   Fit,
                                   0.0,
                                   Lighten( this.style.Get_Tint( ICON_ELEM, state ),
                                            this.style.Get_Shade( ICON_ELEM, state ) ) );
        end if;
        if font /= null then
            font.Draw_String( text,
                              textX, textY,
                              Lighten( this.style.Get_Color( TEXT_ELEM, state ),
                                       this.style.Get_Shade( TEXT_ELEM, state ) ) );
        end if;
    end Draw_Content;

    ----------------------------------------------------------------------------

    function Get_Index( this : not null access Enumeration'Class ) return Natural is (this.index);

    ----------------------------------------------------------------------------

    function Get_Text( this : not null access Enumeration'Class ) return String is
    begin
        if this.index > 0 then
            return To_String( this.items.Element( this.index ).text );
        end if;
        return "";
    end Get_Text;

    ----------------------------------------------------------------------------

    function Get_Value( this : not null access Enumeration'Class ) return Value is
    begin
        if this.index > 0 then
            return this.items.Element( this.index ).val;    -- not a copy
        end if;
        return Null_Value;
    end Get_Value;

    ----------------------------------------------------------------------------

    overriding
    function Get_Visual_State( this : access Enumeration ) return Widget_State is
    (
           (if not this.Is_Enabled then DISABLED_STATE else 0)
        or (if this.open           then OPEN_STATE     else 0)
        or (if this.Is_Focused     then FOCUS_STATE    else 0)
        or (if this.hover          then HOVER_STATE    else 0)
    );

    ----------------------------------------------------------------------------

    function Item_Count( this : not null access Enumeration'Class ) return Natural is (Natural(this.items.Length));

    ----------------------------------------------------------------------------

    procedure On_Key_Down( this : not null access Enumeration'Class ) is
    begin
        this.Set_Index( this.index + 1 );
    end On_Key_Down;

    ----------------------------------------------------------------------------

    procedure On_Key_Up( this : not null access Enumeration'Class ) is
    begin
        this.Set_Index( this.index - 1 );
    end On_Key_Up;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Pressed( this : not null access Enumeration'Class ) is
    begin
         this.Set_Open( not this.open );
    end On_Mouse_Pressed;

    ----------------------------------------------------------------------------

    procedure On_Style_Changed( this : not null access Enumeration'Class ) is
    begin
        if this.popupMenu /= null then
            this.popupMenu.Set_Style( this.style.Get_Style( MENU_ELEM ) );
        end if;

        -- repack because the style may change the font or icon size, which will
        -- change the minimum size hint
        this.Update_Geometry;
    end On_Style_Changed;

    ----------------------------------------------------------------------------

    procedure Popup_Hidden( this : not null access Enumeration'Class ) is
    begin
        this.Set_Open( False );
    end Popup_Hidden;

    ----------------------------------------------------------------------------

    procedure Select_Text( this : not null access Enumeration'Class; text : String ) is
    begin
        for i in 1..this.Item_Count loop
            if this.items.Element( i ).text = text then
                this.Set_Index( i );
                exit;
            end if;
        end loop;
    end Select_Text;

    ----------------------------------------------------------------------------

    procedure Select_Value( this : not null access Enumeration'Class; val : Value'Class ) is
    begin
        for i in 1..this.Item_Count loop
            if this.items.Element( i ).val.Compare( val ) = 0 then
                this.Set_Index( i );
                exit;
            end if;
        end loop;
    end Select_Value;

    ----------------------------------------------------------------------------

    procedure Set_Index( this : not null access Enumeration'Class; index : Natural ) is
    begin
        if index = this.index then
            return;
        end if;

        if index >= 1 and then index <= this.Item_Count then
            this.Set_Open( False );
            this.index := index;
            if this.popupMenu /= null then
                A_Enum_Popup(this.popupMenu).Set_Selected( this.index );
            end if;
            this.sigSelChanged.Emit;
        end if;
    end Set_Index;

    ----------------------------------------------------------------------------

    procedure Set_Open( this : not null access Enumeration'Class; open : Boolean ) is
    begin
        if open = this.open or else (open and then this.items.Is_Empty) then
            return;
        end if;

        this.open := open;
        if this.open then
            this.Build_Popup;
            this.Activate_Popup( this.popupMenu, Widget_Top_Left,
                                 0.0, this.viewport.height, this.geom.width, 0.0 );
        else
            this.Deactivate_Popup( this.popupMenu );
        end if;
    end Set_Open;

begin

    Register_Style( "Enumeration",
                    (BACKGROUND_ELEM => To_Unbounded_String( "background" ),
                     BUTTON_ELEM     => To_Unbounded_String( "button" ),
                     BORDER_ELEM     => To_Unbounded_String( "border" ),
                     ICON_ELEM       => To_Unbounded_String( "expander" ),
                     TEXT_ELEM       => To_Unbounded_String( "text" ),
                     MENU_ELEM       => To_Unbounded_String( "menu:EnumPopup" ),
                     ITEM_ELEM       => To_Unbounded_String( "item:EnumItem" )),
                    (BACKGROUND_ELEM => Area_Element,
                     BUTTON_ELEM     => Area_Element,
                     BORDER_ELEM     => Area_Element,
                     ICON_ELEM       => Icon_Element,
                     TEXT_ELEM       => Text_Element,
                     MENU_ELEM       => Widget_Element,
                     ITEM_ELEM       => Widget_Element),
                    (0 => To_Unbounded_String( "disabled" ),
                     1 => To_Unbounded_String( "open" ),
                     2 => To_Unbounded_String( "focus" ),
                     3 => To_Unbounded_String( "hover" )) );

end Widgets.Enumerations;
