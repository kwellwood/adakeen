--
-- Copyright (c) 2014-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Vectors;

package Widgets.Enumerations is

    -- todo:
    -- Add an Enum_Listener that notifies of selection actions
    -- Allow items to be added/removed/updated

    -- An enumeration widget displays a list of selectable options. Clicking on
    -- the list brings up a downdown menu.
    type Enumeration is new Widget with private;
    type A_Enumeration is access all Enumeration'Class;

    -- Creates a new Enumeration within 'view' with id 'id'. If the label's icon needs
    -- to be stretched to a specific size, use Set_Icon to set it instead.
    function Create_Enum( view : not null access Game_Views.Game_View'Class;
                          id   : String := "" ) return A_Enumeration;
    pragma Postcondition( Create_Enum'Result /= null );

    -- Enumeration Signals
    function SelectionChanged( this : not null access Enumeration'Class ) return access Signal'Class;

    -- Adds a new item to the enumeration.
    procedure Add_Item( this : not null access Enumeration'Class;
                        text : String;
                        val  : Value'Class );

    -- Returns the index of the selection, starting at 1. Zero will be returned
    -- if the enumeration is empty.
    function Get_Index( this : not null access Enumeration'Class ) return Natural with Inline;

    -- Returns the text of the current selection. An empty string will be
    -- returned if the enumeration is empty.
    function Get_Text( this : not null access Enumeration'Class ) return String;

    -- Returns the value (by reference) of the current selection. A null pointer
    -- will be returned if the enumeration is empty.
    function Get_Value( this : not null access Enumeration'Class ) return Value;

    -- Returns the number of items in the enumeration.
    function Item_Count( this : not null access Enumeration'Class ) return Natural with Inline;

    -- Selects the item with text 'text' in the enumeration. If the text exists
    -- in multiple places, the first matching index will be selected.
    procedure Select_Text( this : not null access Enumeration'Class; text : String );

    -- Selects the item with value 'val' in the enumeration. If the value exists
    -- in multiple places, the first matching index will be selected.
    procedure Select_Value( this : not null access Enumeration'Class; val : Value'Class );

    -- Sets the currently selected index of the enumeration. If the index is not
    -- valid, it will be ignored.
    procedure Set_Index( this : not null access Enumeration'Class; index : Natural );

private

    BACKGROUND_ELEM : constant := 1;
    BUTTON_ELEM     : constant := 2;
    BORDER_ELEM     : constant := 3;
    ICON_ELEM       : constant := 4;
    TEXT_ELEM       : constant := 5;
    MENU_ELEM       : constant := 6;
    ITEM_ELEM       : constant := 7;

    DISABLED_STATE  : constant Widget_State := 2**0;
    OPEN_STATE      : constant Widget_State := 2**1;
    FOCUS_STATE     : constant Widget_State := 2**2;
    HOVER_STATE     : constant Widget_State := 2**3;

    ----------------------------------------------------------------------------

    type Item_Record is
        record
            text : Unbounded_String;
            val  : Value;
        end record;

    package Item_Vectors is new Ada.Containers.Vectors( Positive, Item_Record );

    type Enumeration is new Widget with
        record
            popupMenu     : A_Widget := null;     -- Enum_Popup menu (belongs to this)
            items         : Item_Vectors.Vector;
            index         : Natural := 0;
            open          : Boolean := False;
            sigSelChanged : aliased Signal;
        end record;

    procedure Construct( this : access Enumeration;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );

    procedure Delete( this : in out Enumeration );

    procedure Draw_Content( this : access Enumeration );

    -- Returns the visual state of the enumeration, based on its enabled, opened,
    -- focused, and hovered states.
    function Get_Visual_State( this : access Enumeration ) return Widget_State;

    -- Sets the enumeration's menu open or closed.
    procedure Set_Open( this : not null access Enumeration'Class; open : Boolean );

end Widgets.Enumerations;
