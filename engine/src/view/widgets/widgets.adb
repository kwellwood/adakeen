--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Ada.Unchecked_Conversion;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.State;                     use Allegro.State;
with Allegro.Transformations;           use Allegro.Transformations;
with Debugging;                         use Debugging;
with Game_Views;                        use Game_Views;
with GNAT.Random_Numbers;               use GNAT.Random_Numbers;
with Interfaces;                        use Interfaces;
with Keyboard;                          use Keyboard;
with Processes.Managers;                use Processes.Managers;
with Support;                           use Support;
with Values.Construction;               use Values.Construction;
with Values.Strings;                    use Values.Strings;
with Widget_Styles.Registry;            use Widget_Styles.Registry;
with Widgets.Windows;                   use Widgets.Windows;

package body Widgets is

    use Widget_Lists;

    package Connections is new Signals.Connections(Widget);
    use Connections;

    package Key_Connections is new Signals.Keys.Connections(Widget);
    use Key_Connections;

    package Widget_Boolean_Property is new Animated_Boolean.Accessors(Widget);
    package Widget_Float_Property is new Animated_Float.Accessors(Widget);

    ----------------------------------------------------------------------------

    POS_BIT_LEFT   : constant Unsigned_8 := 2#10000#;
    POS_BIT_RIGHT  : constant Unsigned_8 := 2#01000#;
    POS_BIT_CENTER : constant Unsigned_8 := 2#00100#;
    POS_BIT_X      : constant Unsigned_8 := 2#00010#;
    POS_BIT_WIDTH  : constant Unsigned_8 := 2#00001#;

    type U8_Array is array (Integer range <>) of Unsigned_8;

                                             -- Valid Anchors + Position:
                                             --
    VALID_POSITIONING : constant U8_Array := -- Left   Right   Center    X    Width
    (                                        -- ----   -----   ------   ---   -----
        -- No positioning yet
        0,                                   --

        -- Partially specified position
        POS_BIT_LEFT,                        --  *
        POS_BIT_RIGHT,                       --          *
        POS_BIT_CENTER,                      --                  *
        POS_BIT_X,                           --                          *
        POS_BIT_WIDTH,                       --                                 *

        -- Fully specified position
        POS_BIT_LEFT   or POS_BIT_RIGHT,     --  *       *
        POS_BIT_LEFT   or POS_BIT_WIDTH,     --  *                              *
        POS_BIT_RIGHT  or POS_BIT_WIDTH,     --          *                      *
        POS_BIT_RIGHT  or POS_BIT_X,         --          *               *
        POS_BIT_X      or POS_BIT_WIDTH,     --                          *      *
        POS_BIT_CENTER or POS_BIT_WIDTH      --                  *              *
    );

    -- Checks if a combination of anchors and positioning is valid.
    function Is_Valid_Positioning( posCombo : Unsigned_8 ) return Boolean is
    begin
        for i in VALID_POSITIONING'Range loop
            if posCombo = VALID_POSITIONING(i) then
                return True;
            end if;
        end loop;
        return False;
    end Is_Valid_Positioning;

    ----------------------------------------------------------------------------

    -- Returns the previously specified width or height of the widget within its
    -- parent's content area, either set explicitly or determined implicitly
    -- by anchors. If the width or height has not been specified and the widget
    -- has not been rooted inside the window's widget tree, then the result is
    -- undefined.
    function Get_Width( this : not null access Widget'Class ) return Float;
    function Get_Height( this : not null access Widget'Class ) return Float;

    -- Returns the previously specified X or Y position of the widget within its
    -- parent's content area. If X or Y has not been specified and the widget
    -- has not been rooted inside the window's widget tree, the result is
    -- undefined.
    function Get_X( this : not null access Widget'Class ) return Float;
    function Get_Y( this : not null access Widget'Class ) return Float;

    -- Returns the zoom factor of the widget. Numbers greater than one mean the
    -- view of the widget is zoomed in.
    function Get_Zoom( this : not null access Widget'Class ) return Float;

    -- Detaches the widget from the view's process manager if it's animated.
    procedure On_Hidden( this : not null access Widget'Class );

    -- Called when the Tab key is pressed to move focus to the next or previous
    -- focusable widget. This is only called when a next or previous focusable
    -- widget has been set.
    procedure On_Key_Tab( this : not null access Widget'Class;
                          key  : Key_Arguments );

    -- Attaches the widget to the view's process manager if it's animated.
    procedure On_Shown( this : not null access Widget'Class );

    -- This is called with the widget becomes enabled or disabled. Because the
    -- enabled state is inherited from ancestors, the enabled state of
    -- descendants also changes when the enabled state of an ancestor changes.
    -- Each widget emits a Enabled or Disable signal. The Enabled signals
    -- propagate downward and the Disabled signals propagate upward, so that
    -- leaf widgets are never enabled when their ancestors are not.
    procedure Propagate_Enabled( this : not null access Widget'Class; enabled : Boolean );

    -- This is called when the widget becomes rooted or unrooted in a Window.
    -- "Rooted" means the widget has the Window as an ancestor. The Rooted
    -- signals for descendants of a newly rooted/unrooted widget propagate from
    -- the leaves upward to the common ancestor, emitting Rooted or Unrooted
    -- signals from each affected descendant.
    procedure Propagate_Rooted( this : not null access Widget'Class; rooted : Boolean );

    -- This is called when the widget becomes shown or unshown on the screen.
    -- For a widget to be shown, it must be rooted and visible. If it becomes
    -- unrooted, or not visible (one of its ancestors may have become unrooted
    -- or not visible), the widget will be hidden. Essentially, this procedure
    -- is called every time the result of the Is_Showing function changes.
    --
    -- Shown/Hidden signals are propagated from the leaves upward, to the widget
    -- that caused the visibility change. As each of the children emit
    -- shown/hidden signals, they also each start a propagation of descendant
    -- shown/hidden signals that propagate upward toward the root. For each
    -- hidden/shown widget under the initiator, its own shown/hidden signal is
    -- emitted before its descendent shown/hidden signals are propagated.
    procedure Propagate_Shown( this : not null access Widget'Class; shown : Boolean );

    -- Sets whether the widget is visible within its container.
    procedure Set_Visible( this : not null access Widget'Class; visible : Boolean );

    -- Updates the private .posWidth and .posHeight positioning values and
    -- recalculates geometry as necessary. These procedures are used by the
    -- Width and Height properties.
    procedure Set_Width( this : not null access Widget'Class; width : Float );
    procedure Set_Height( this : not null access Widget'Class; height : Float );

    -- Updates the private .posX and .posY positioning values and recalculates
    -- geometry as necessary. These procedures are used by the X and Y properties.
    procedure Set_X( this : not null access Widget'Class; x : Float );
    procedure Set_Y( this : not null access Widget'Class; y : Float );

    -- Sets the content zoom (scaling) factor. Values > 1 scale the contents
    -- larger within the viewport (decreasing the visible content area), and
    -- values < 1 scale the contents smaller. The zoom factor does not affect
    -- the widget's viewport size within its container.
    procedure Set_Zoom( this : not null access Widget'Class; zoom : Float );

    -- Updates the positions and sizes of the widget's children. This assumes
    -- that the widget is rooted.
    procedure Update_Geometry_Children( this : not null access Widget'Class;
                                        axis : Axis_Type );

    -- Updates the position and width of the widget, based on its anchors and
    -- position. Its anchors' targets and its children will be updated as
    -- necessary. This procedure differs from Update_Geometry() in that it does
    -- not explicitly recalculate its siblings' geometry, only its own and its
    -- children's, and it assumes the widget is rooted.
    procedure Update_Geometry_Single( this           : not null access Widget'Class;
                                      axis : Axis_Type );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    not overriding
    procedure Construct( this       : access Widget;
                         view       : not null access Game_Views.Game_View'Class;
                         id         : String;
                         widgetType : String ) is
        gen : Generator;
    begin
        Limited_Object(this.all).Construct;
        this.view := A_Game_View(view);
        if id'Length > 0 then
            this.id := To_Unbounded_String( id );
        else
            Reset( gen );
            this.id := To_Unbounded_String( "W" & Image( Unsigned_32'(Random( gen )) ) );
        end if;
        this.attributes := Create_Map.Map;
        this.widgetType := To_Unbounded_String( widgetType );
        if widgetType'Length > 0 then
            this.style := Widget_Styles.Registry.Get_Style( widgetType, "" );
        end if;

        this.ancLeft.owner    := A_Widget(this);
        this.ancRight.owner   := A_Widget(this);
        this.ancTop.owner     := A_Widget(this);
        this.ancBottom.owner  := A_Widget(this);
        this.ancCenterX.owner := A_Widget(this);
        this.ancCenterY.owner := A_Widget(this);

        this.propVisible.Construct( view.Get_Process_Manager, Widget_Boolean_Property.Accessor'(this, Is_Visible'Access, Set_Visible'Access) );
        this.propWidth.Construct( view.Get_Process_Manager, Widget_Float_Property.Accessor'(this, Get_Width'Access, Set_Width'Access) );
        this.propHeight.Construct( view.Get_Process_Manager, Widget_Float_Property.Accessor'(this, Get_Height'Access, Set_Height'Access) );
        this.propX.Construct( view.Get_Process_Manager, Widget_Float_Property.Accessor'(this, Get_X'Access, Set_X'Access) );
        this.propY.Construct( view.Get_Process_Manager, Widget_Float_Property.Accessor'(this, Get_Y'Access, Set_Y'Access) );
        this.propZoom.Construct( view.Get_Process_Manager, Widget_Float_Property.Accessor'(this, Get_Zoom'Access, Set_Zoom'Access) );

        this.sigAttributeChanged  .Init( this );
        this.sigBlurred           .Init( this );
        this.sigDeleted           .Init( this );
        this.sigDisabled          .Init( this );
        this.sigEnabled           .Init( this );
        this.sigFocused           .Init( this );
        this.sigHidden            .Init( this );
        this.sigResized           .Init( this );
        this.sigRooted            .Init( this );
        this.sigShown             .Init( this );
        this.sigStyleChanged      .Init( this );
        this.sigUnrooted          .Init( this );
        this.sigParentChanged     .Init( this );

        this.sigGamepadMoved      .Init( this );
        this.sigGamepadPressed    .Init( this );
        this.sigGamepadReleased   .Init( this );

        this.sigKeyPressed        .Init( this );
        this.sigKeyReleased       .Init( this );
        this.sigKeyTyped          .Init( this );

        this.sigMouseClicked      .Init( this );
        this.sigMouseDoubleClicked.Init( this );
        this.sigMouseHeld         .Init( this );
        this.sigMouseMoved        .Init( this );
        this.sigMousePressed      .Init( this );
        this.sigMouseReleased     .Init( this );
        this.sigMouseScrolled     .Init( this );

        this.Hidden.Connect( Slot( this, On_Hidden'Access, priority => High ) );
        this.Shown.Connect( Slot( this, On_Shown'Access, priority => High ) );

        view.Register( this );        -- raises exception on duplicate id
        this.registered := True;
    end Construct;

    ----------------------------------------------------------------------------

    function AttributeChanged( this : not null access Widget'Class ) return access String_Signal'Class is (this.sigAttributeChanged'Access);

    function Blurred( this : not null access Widget'Class ) return access Signal'Class is (this.sigBlurred'Access);

    function Deleted( this : not null access Widget'Class ) return access Signal'Class is (this.sigDeleted'Access);

    function Disabled( this : not null access Widget'Class ) return access Signal'Class is (this.sigDisabled'Access);

    function Enabled( this : not null access Widget'Class ) return access Signal'Class is (this.sigEnabled'Access);

    function Focused( this : not null access Widget'Class ) return access Signal'Class is (this.sigFocused'Access);

    function GamepadMoved( this : not null access Widget'Class ) return access Gamepad_Axis_Signal'Class is (this.sigGamepadMoved'Access);

    function GamepadPressed( this : not null access Widget'Class ) return access Gamepad_Button_Signal'Class is (this.sigGamepadPressed'Access);

    function GamepadReleased( this : not null access Widget'Class ) return access Gamepad_Button_Signal'Class is (this.sigGamepadReleased'Access);

    function Hidden( this : not null access Widget'Class ) return access Signal'Class is (this.sigHidden'Access);

    function KeyPressed( this : not null access Widget'Class ) return access Key_Signal'Class is (this.sigKeyPressed'Access);

    function KeyReleased( this : not null access Widget'Class ) return access Key_Signal'Class is (this.sigKeyReleased'Access);

    function KeyTyped( this : not null access Widget'Class ) return access Key_Signal'Class is (this.sigKeyTyped'Access);

    function MouseClicked( this : not null access Widget'Class ) return access Button_Signal'Class is (this.sigMouseClicked'Access);

    function MouseDoubleClicked( this : not null access Widget'Class ) return access Button_Signal'Class is (this.sigMouseDoubleClicked'Access);

    function MouseEntered( this : not null access Widget'Class ) return access Signal'Class is (this.sigMouseEntered'Access);

    function MouseExited( this : not null access Widget'Class ) return access Signal'Class is (this.sigMouseExited'Access);

    function MouseHeld( this : not null access Widget'Class ) return access Button_Signal'Class is (this.sigMouseHeld'Access);

    function MouseMoved( this : not null access Widget'Class ) return access Mouse_Signal'Class is (this.sigMouseMoved'Access);

    function MousePressed( this : not null access Widget'Class ) return access Button_Signal'Class is (this.sigMousePressed'Access);

    function MouseReleased( this : not null access Widget'Class ) return access Button_Signal'Class is (this.sigMouseReleased'Access);

    function MouseScrolled( this : not null access Widget'Class ) return access Scroll_Signal'Class is (this.sigMouseScrolled'Access);

    function ParentChanged( this : not null access Widget'Class ) return access Signal'Class is (this.sigParentChanged'Access);

    function Resized( this : not null access Widget'Class ) return access Signal'Class is (this.sigResized'Access);

    function Rooted( this : not null access Widget'Class ) return access Signal'Class is (this.sigRooted'Access);

    function Shown( this : not null access Widget'Class ) return access Signal'Class is (this.sigShown'Access);

    function StyleChanged( this : not null access Widget'Class ) return access Signal'Class is (this.sigStyleChanged'Access);

    function Unrooted( this : not null access Widget'Class ) return access Signal'Class is (this.sigUnrooted'Access);

    ----------------------------------------------------------------------------

    function Visible( this : not null access Widget'Class ) return access Animated_Boolean.Property'Class is (this.propVisible'Access);

    function Width( this : not null access Widget'Class ) return access Animated_Float.Property'Class is (this.propWidth'Access);

    function Height( this : not null access Widget'Class ) return access Animated_Float.Property'Class is (this.propHeight'Access);

    function X( this : not null access Widget'Class ) return access Animated_Float.Property'Class is (this.propX'Access);

    function Y( this : not null access Widget'Class ) return access Animated_Float.Property'Class is (this.propY'Access);

    function Zoom( this : not null access Widget'Class ) return access Animated_Float.Property'Class is (this.propZoom'Access);

    ----------------------------------------------------------------------------

    function Left( this : not null access Widget'Class ) return access Anchor'Class is (this.ancLeft'Access);

    function Right( this : not null access Widget'Class ) return access Anchor'Class is (this.ancRight'Access);

    function Top( this : not null access Widget'Class ) return access Anchor'Class is (this.ancTop'Access);

    function Bottom( this : not null access Widget'Class ) return access Anchor'Class is (this.ancBottom'Access);

    function Horizontal_Center( this : not null access Widget'Class ) return access Anchor'Class is (this.ancCenterX'Access);

    function Vertical_Center( this : not null access Widget'Class ) return access Anchor'Class is (this.ancCenterY'Access);

    ----------------------------------------------------------------------------

    function Accepts_Focus( this : not null access Widget'Class ) return Boolean is (this.acceptFocus);

    ----------------------------------------------------------------------------

    procedure Activate_Popup( this   : not null access Widget'Class;
                              popup  : not null access Widget'Class;
                              corner : Widget_Corner_Type;
                              x, y   : Float;
                              width  : Float := 0.0;
                              height : Float := 0.0 ) is
        winX, winY  : Float;            -- window content coordinates for the corner
        winViewport : Rectangle;
        realCorner  : Widget_Corner_Type := corner;
        realWidth   : Float := width;   -- the popup's width at time of activation
        realHeight  : Float := height;  -- the popup's height at time of activation
        setWidth    : Boolean := True;  -- set popup's width?
        setHeight   : Boolean := True;  -- set popup's height?
    begin
        -- determine the location of the specified corner of the popup
        this.Translate_To_Window( x, y, winX, winY );
        winX := winX - this.Get_Window.Get_Geometry.x;
        winY := winY - this.Get_Window.Get_Geometry.y;

        -- determines the visible area of the Window the popup will go in
        winViewport := this.Get_Window.Get_Viewport;

        -- determine the size of the popup ('width,hight' in window coordinates)
        if width <= 0.0 then
            -- use the popup's own specified width, if it has one. otherwise,
            -- fallback to the popup's preferred width.
            if not popup.posWidth.Is_Null then
                realWidth := popup.Get_Width;
            else
                -- use popup's preferred width. we still need to determine the
                -- popup's width at the time of activation so that the widget's
                -- corner position can be properly calculated. when 'setWidth'
                -- is False, the popup's fixed width will not be set. this
                -- allows the widget's width to change later while it's visible,
                -- if its preferred width changes.
                setWidth := False;
                realWidth := popup.Get_Preferred_Width;
            end if;
        end if;
        if height <= 0.0 then
            if not popup.posHeight.Is_Null then
                realHeight := popup.Get_Height;
            else
                setHeight := False;
                realHeight := popup.Get_Preferred_Height;
            end if;
        end if;

        -- determine if the specified popup corner can be placed at x, y without
        -- the popup going off the edge of its parent.
        if realCorner = Widget_Top_Left or else realCorner = Widget_Bottom_Left then
            -- check for widget going off the right side
            if winX + realWidth > winViewport.x + winViewport.width then
                if realCorner = Widget_Top_Left then
                    realCorner := Widget_Top_Right;
                else
                    realCorner := Widget_Bottom_Right;
                end if;
            end if;
        else
            -- check for widget going off the left side
            if winX - realWidth < winViewport.x then
                if realCorner = Widget_Top_Right then
                    realCorner := Widget_Top_Left;
                else
                    realCorner := Widget_Bottom_Left;
                end if;
            end if;
        end if;
        if realCorner = Widget_Top_Left or else realCorner = Widget_Top_Right then
            -- check for widget going off the bottom side
            if winY + realHeight > winViewport.y + winViewport.height then
                if realCorner = Widget_Top_Left then
                    realCorner := Widget_Bottom_Left;
                else
                    realCorner := Widget_Bottom_Right;
                end if;
            end if;
        else
            -- check for widget going off the top side
            if winY - realHeight < winViewport.y then
                if realCorner = Widget_Bottom_Left then
                    realCorner := Widget_Top_Left;
                else
                    realCorner := Widget_Top_Right;
                end if;
            end if;
        end if;

        -- push the popup widget to the window
        this.Get_Window.Push_Popup( popup );

        -- apply the calculated layout to the popup
        if setWidth then
            popup.Set_Width( realWidth );
        end if;
        if setHeight then
            popup.Set_Height( realHeight );
        end if;
        case realCorner is
            when Widget_Top_Left =>
                popup.Set_X( winX );
                popup.Set_Y( winY );
            when Widget_Top_Right =>
                popup.Set_X( winX - realWidth );
                popup.Set_Y( winY );
            when Widget_Bottom_Left =>
                popup.Set_X( winX );
                popup.Set_Y( winY - realHeight );
            when Widget_Bottom_Right =>
                popup.Set_X( winX - realWidth );
                popup.Set_Y( winY - realHeight );
        end case;
    end Activate_Popup;

    ----------------------------------------------------------------------------

    procedure Add_Child( this  : not null access Widget'Class;
                         child : not null access Widget'Class ) is
    begin
        pragma Assert( child.parent = null, "Widget already has a parent" );
        pragma Assert( A_Widget(child) /= A_Widget(this), "Adding to self" );
        this.children.Append( child );
        child.Set_Parent( this );
    end Add_Child;

    ----------------------------------------------------------------------------

    procedure Bring_To_Front( this : not null access Widget'Class ) is
        pos : Widget_Lists.Cursor;
    begin
        if this.parent /= null then
            pos := this.parent.children.Find( this );
            pragma Assert( Has_Element( pos ) );
            this.parent.children.Delete( pos );
            this.parent.children.Append( this );     -- draw it last
        end if;
    end Bring_To_Front;

    ----------------------------------------------------------------------------

    procedure Center( this   : not null access Widget'Class;
                      target : access Widget'Class;
                      width  : Float := 0.0;
                      height : Float := 0.0 ) is
    begin
        this.Center_Horizontal( target, width );
        this.Center_Vertical( target, height );
    end Center;

    ----------------------------------------------------------------------------

    procedure Center_Horizontal( this   : not null access Widget'Class;
                                 target : access Widget'Class;
                                 width  : Float := 0.0 ) is
    begin
        if target /= null then
            this.Unset_X;
            this.Left.Detach;
            this.Right.Detach;
            this.Horizontal_Center.Attach( target.Horizontal_Center );
            if width > 0.0 then
                this.Set_Width( width );
            end if;
        end if;
    end Center_Horizontal;

    ----------------------------------------------------------------------------

    procedure Center_Vertical( this   : not null access Widget'Class;
                               target : access Widget'Class;
                               height : Float := 0.0 ) is
    begin
        if target /= null then
            this.Unset_Y;
            this.Top.Detach;
            this.Bottom.Detach;
            this.Vertical_Center.Attach( target.Vertical_Center );
            if height > 0.0 then
                this.Set_Height( height );
            end if;
        end if;
    end Center_Vertical;

    ----------------------------------------------------------------------------

    procedure Deactivate_Popup( this  : not null access Widget'Class;
                                popup : not null access Widget'Class ) is
    begin
        this.Get_Window.Pop_Popup( popup );
    end Deactivate_Popup;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Widget ) is
    begin
        -- every widget should be removed from its parent before it is deleted
        pragma Assert( this.parent = null, "Deleting widget with a parent" );

        if this.view /= null then
            this.Get_View.Get_Corral.Remove_Listener( this'Unchecked_Access );
        end if;

        this.Delete_Children;
        this.sigDeleted.Emit;      -- notify others that this widget is going away

        if this.registered then
            this.Get_View.Unregister( this.Get_Id );
            this.registered := False;
        end if;
        Delete( this.style );

        -- observer counts are allowed to be negative here because Delete_Children
        -- hijacks the reference counting and sets the count to zero, which can
        -- result in negative counts upon deletion.
        pragma Assert( this.ancLeft.siblingObservers <= 0, "Left anchor has sibling observers" );
        pragma Assert( this.ancRight.siblingObservers <= 0, "Right anchor has sibling observers" );
        pragma Assert( this.ancCenterX.siblingObservers <= 0, "Horizontal center anchor has sibling observers" );
        pragma Assert( this.ancTop.siblingObservers <= 0, "Top anchor has sibling observers" );
        pragma Assert( this.ancBottom.siblingObservers <= 0, "Bottom anchor has sibling observers" );
        pragma Assert( this.ancCenterY.siblingObservers <= 0, "Vertical center anchor has sibling observers" );

        Limited_Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Delete_Child( this  : not null access Widget'Class;
                            child : in out A_Widget ) is
        pos : Widget_Lists.Cursor;
    begin
        if child /= null then
            pos := this.children.Find( child );
            if Has_Element( pos ) then
                child.Set_Visible( False );
                this.Remove_Child( child );
                Delete( child );
            else
                raise WIDGET_NOT_FOUND with "Widget is not a child";
            end if;
        end if;
    end Delete_Child;

    ----------------------------------------------------------------------------

    procedure Delete_Children( this : not null access Widget'Class ) is
        w : A_Widget;
    begin
        -- manually clear the .siblingObserver counts for all siblings so the
        -- observer count assertion in Delete() doesn't fail.
        --
        -- there is a potential bug in GNAT 2016 that crashes if the widgets are
        -- removed and deleted one at a time. it only crashes outside the
        -- debugger and sometimes reports 'heap exhausted'.
        for child of this.children loop
            child.ancLeft.siblingObservers := 0;
            child.ancRight.siblingObservers := 0;
            child.ancCenterX.siblingObservers := 0;
            child.ancTop.siblingObservers := 0;
            child.ancBottom.siblingObservers := 0;
            child.ancCenterY.siblingObservers := 0;
        end loop;

        while not this.children.Is_Empty loop
            w := this.children.Last_Element;
            w.Set_Visible( False );
            this.Remove_Child( w );
            Delete( w );
        end loop;
    end Delete_Children;

    ----------------------------------------------------------------------------

    procedure Detach_Anchors( this : not null access Widget'Class; child : access Widget'Class ) is
    begin
        if child.parent /= this then
            -- the target widget isn't a child of this, so it isn't a sibling to
            -- the children, so none of the children are anchored to it.
            return;
        end if;

        for sibling of this.children loop
            sibling.ancLeft.Detach( child );
            sibling.ancRight.Detach( child );
            sibling.ancCenterX.Detach( child );
            sibling.ancTop.Detach( child );
            sibling.ancBottom.Detach( child );
            sibling.ancCenterY.Detach( child );
        end loop;
    end Detach_Anchors;

    ----------------------------------------------------------------------------

    not overriding
    procedure Draw( this : access Widget ) is
        state : Allegro_State;
        trans : Allegro_Transform;
    begin
        if not this.Is_Visible or this.viewport.width <= 0.0 or this.viewport.height <= 0.0 then
            return;
        end if;

        Al_Store_State( state, ALLEGRO_STATE_TRANSFORM );

        Al_Identity_Transform( trans );
        Al_Translate_Transform( trans, -this.viewport.x, -this.viewport.y );
        Al_Scale_Transform( trans, this.zoomFactor, this.zoomFactor );
        Al_Compose_Transform( trans, Al_Get_Current_Transform.all );
        Al_Use_Transform( trans );

        A_Widget(this).Draw_Content;
        A_Widget(this).Draw_Children;
        A_Widget(this).Draw_Content_Foreground;

        Al_Restore_State( state );
    end Draw;

    ----------------------------------------------------------------------------

    not overriding
    procedure Draw_Children( this : access Widget ) is
        state        : Allegro_State;
        clipX, clipY,
        clipW, clipH : Integer;
        trans        : Allegro_Transform;
        winX1, winY1,
        winX2, winY2 : Float;
    begin
        Al_Get_Clipping_Rectangle( clipX, clipY, clipW, clipH );
        for child of this.children loop
            if Intersect( this.viewport, child.geom ) then
                -- the clipping rectangle of the child is calculated in window
                -- coordinates because it is not affected by transformations.
                child.Translate_To_Window( child.viewport.x, child.viewport.y, winX1, winY1 );
                child.Translate_To_Window( child.viewport.x + child.viewport.width, child.viewport.y + child.viewport.height, winX2, winY2 );

                -- the child's clipping rectangle is constrained to the parent's
                -- clipping rectangle.
                winX1 := Float(Integer'Max( clipX, Integer(winX1) ));
                winY1 := Float(Integer'Max( clipY, Integer(winY1) ));

                Al_Set_Clipping_Rectangle( Integer'Max( clipX, Integer(winX1) ),
                                           Integer'Max( clipY, Integer(winY1) ),
                                           Integer'Max( 0, Integer'Min( clipX + clipW - Integer(winX1), Integer(winX2 - winX1) ) ),
                                           Integer'Max( 0, Integer'Min( clipY + clipH - Integer(winY1), Integer(winY2 - winY1) ) ) );

                Al_Store_State( state, ALLEGRO_STATE_TRANSFORM );

                Al_Identity_Transform( trans );
                Al_Translate_Transform_3d( trans, child.geom.x, child.geom.y, child.z );
                Al_Compose_Transform( trans, Al_Get_Current_Transform.all );
                Al_Use_Transform( trans );

                child.Draw;

                Al_Restore_State( state );
            end if;
        end loop;
        Al_Set_Clipping_Rectangle( clipX, clipY, clipW, clipH );
    end Draw_Children;

    ----------------------------------------------------------------------------

    procedure Fill( this         : not null access Widget'Class;
                    target       : access Widget'Class;
                    leftMargin   : Float := 0.0;
                    topMargin    : Float := 0.0;
                    rightMargin  : Float := 0.0;
                    bottomMargin : Float := 0.0 ) is
    begin
        this.Fill_Horizontal( target, leftMargin, rightMargin );
        this.Fill_Vertical( target, topMargin, bottomMargin );
    end Fill;

    ----------------------------------------------------------------------------

    procedure Fill_Horizontal( this        : not null access Widget'Class;
                               target      : access Widget'Class;
                               leftMargin  : Float := 0.0;
                               rightMargin : Float := 0.0 ) is
    begin
        if target /= null then
            this.Unset_X;
            this.Unset_Width;
            this.Horizontal_Center.Detach;

            this.Left.Attach( target.Left, leftMargin );
            this.Right.Attach( target.Right, rightMargin );
        end if;
    end Fill_Horizontal;

    ----------------------------------------------------------------------------

    procedure Fill_Vertical( this         : not null access Widget'Class;
                             target       : access Widget'Class;
                             topMargin    : Float := 0.0;
                             bottomMargin : Float := 0.0 ) is
    begin
        if target /= null then
            this.Unset_Y;
            this.Unset_Height;
            this.Vertical_Center.Detach;

            this.Top.Attach( target.Top, topMargin );
            this.Bottom.Attach( target.Bottom, bottomMargin );
        end if;
    end Fill_Vertical;

    ----------------------------------------------------------------------------

    not overriding
    procedure Find_Widget_At( this     : access Widget;
                              x, y     : Float;
                              children : Boolean;
                              wx, wy   : out Float;
                              found    : out A_Widget ) is
        -- translate viewport coordinates into content coordinates
        cx : constant Float := this.viewport.x + x / this.zoomFactor;
        cy : constant Float := this.viewport.y + y / this.zoomFactor;
    begin
        wx := 0.0;
        wy := 0.0;
        found := null;

        if not this.Is_Visible or else not this.enable then
            return;
        end if;

        -- iterate in reverse to check children in Z-order, front to back
        if children then
            for child of reverse this.children loop
                if Contains( child.geom, cx, cy ) then
                    child.Find_Widget_At( cx - child.geom.x, cy - child.geom.y, True, wx, wy, found );
                    if found /= null then
                        return;
                    end if;
                end if;
            end loop;
        end if;

        -- verify x, y is within the viewport
        if Contains( Rectangle'(0.0, 0.0, this.geom.width, this.geom.height), x, y ) then
            -- translate the viewport coordinates into content coordinates
            wx := cx;
            wy := cy;
            found := A_Widget(this);
        end if;
    end Find_Widget_At;

    ----------------------------------------------------------------------------

    function Focus_Prev( this : not null access Widget'Class ) return Boolean is
        wdgt : A_Widget := null;
    begin
        if Length( this.prevId ) = 0 then
            return False;       -- no prev widget
        end if;
        -- raises ID_NOT_FOUND if prev widget not found
        wdgt := this.Get_View.Get_Widget( To_String( this.prevId ) );
        while wdgt /= A_Widget(this) loop
            -- loop through the list of prev widgets until one of
            -- them accepts focus or we get back to this widget.
            if wdgt.Is_Focusable then
                wdgt.Request_Focus;
                return True;
            end if;
            -- raises ID_NOT_FOUND if no prev widget
            wdgt := this.Get_View.Get_Widget( To_String( wdgt.prevId ) );
        end loop;
        -- the prev widget is this widget. return that it has been focused.
        return True;
    exception
        when ID_NOT_FOUND =>
            return False;
    end Focus_Prev;

    ----------------------------------------------------------------------------

    function Focus_Next( this : not null access Widget'Class ) return Boolean is
        wdgt : A_Widget := null;
    begin
        if Length( this.nextId ) = 0 then
            return False;       -- no next widget
        end if;
        -- raises ID_NOT_FOUND if next widget not found
        wdgt := this.Get_View.Get_Widget( To_String( this.nextId ) );
        while wdgt /= A_Widget(this) loop
            -- loop through the list of next widgets until one of
            -- them accepts focus or we get back to this widget.
            if wdgt.Is_Focusable then
                wdgt.Request_Focus;
                return True;
            end if;
            if Length( wdgt.nextId ) = 0 then
                return False;   -- no next widget
            end if;
            -- raises ID_NOT_FOUND if next widget not found
            wdgt := this.Get_View.Get_Widget( To_String( wdgt.nextId ) );
        end loop;
        -- the next widget is this widget. return that it has been focused.
        return True;
    exception
        when ID_NOT_FOUND =>
            -- the widget referenced by .nextId was not found. the value may
            -- been set incorrectly, or the value is stale and the widget is
            -- gone, or whatever. there's no next focusable widget.
            return False;
    end Focus_Next;

    ----------------------------------------------------------------------------

    function Get_Attribute( this : not null access Widget'Class;
                            name : String ) return Value
    is (this.attributes.Get( name ));

    ----------------------------------------------------------------------------

    function Get_Attributes( this : not null access Widget'Class ) return Map_Value is (this.attributes);

    ----------------------------------------------------------------------------

    function Get_Center_X( this : not null access Widget'Class ) return Float is (this.geom.x + (this.geom.width / 2.0));

    function Get_Center_Y( this : not null access Widget'Class ) return Float is (this.geom.y + (this.geom.height / 2.0));

    ----------------------------------------------------------------------------

    function Get_Geometry( this : not null access Widget'Class ) return Rectangle is (this.geom);

    ----------------------------------------------------------------------------

    function Get_Height( this : not null access Widget'Class ) return Float is (if this.posHeight.Is_Null then this.geom.height else this.posHeight.To_Float);

    ----------------------------------------------------------------------------

    function Get_Id( this : not null access Widget'Class ) return String is (To_String( this.id ));

    ----------------------------------------------------------------------------

    not overriding
    function Get_Min_Height( this : access Widget ) return Float is (0.0);

    ----------------------------------------------------------------------------

    not overriding
    function Get_Min_Width( this : access Widget ) return Float is (0.0);

    ----------------------------------------------------------------------------

    function Get_Mouse_Cursor( this : not null access Widget'Class ) return String is (To_String(this.mouseCursor));

    ----------------------------------------------------------------------------

    procedure Get_Mouse_Location( this   : not null access Widget'Class;
                                  mx, my : out Float ) is
    begin
        this.Get_Window.Get_Mouse_Location( this, mx, my );
    end Get_Mouse_Location;

    ----------------------------------------------------------------------------

    function Get_Parent( this : not null access Widget'Class ) return A_Widget is (this.parent);

    ----------------------------------------------------------------------------

    not overriding
    function Get_Preferred_Height( this : access Widget ) return Float is
    begin
        if not this.prefHeight.Is_Null then
            return this.prefHeight.To_Float;
        end if;
        return A_Widget(this).Get_Min_Height;
    end Get_Preferred_Height;

    ----------------------------------------------------------------------------

    not overriding
    function Get_Preferred_Width( this : access Widget ) return Float is
    begin
        if not this.prefWidth.Is_Null then
            return this.prefWidth.To_Float;
        end if;
        return A_Widget(this).Get_Min_Width;
    end Get_Preferred_Width;

    ----------------------------------------------------------------------------

    function Get_View( this : not null access Widget'Class ) return access Game_Views.Game_View'Class is (this.view);

    ----------------------------------------------------------------------------

    function Get_Viewport( this : not null access Widget'Class ) return Rectangle is (this.viewport);

    ----------------------------------------------------------------------------

    function Get_Width( this : not null access Widget'Class ) return Float is (if this.posWidth.Is_Null then this.geom.width else this.posWidth.To_Float);

    ----------------------------------------------------------------------------

    function Get_Window( this : not null access Widget'Class ) return access Widgets.Windows.Window'Class is
        next : A_Widget := A_Widget(this);
    begin
        while next /= null loop
            if next.all in Window'Class then
                return A_Window(next);
            end if;
            next := next.parent;
        end loop;
        return null;
    end Get_Window;

    ----------------------------------------------------------------------------

    function Get_X( this : not null access Widget'Class ) return Float is ((if this.posX.Is_Null then this.geom.x else this.posX.To_Float));

    function Get_Y( this : not null access Widget'Class ) return Float is ((if this.posY.Is_Null then this.geom.y else this.posY.To_Float));

    function Get_Z( this : not null access Widget'Class ) return Float is (this.z);

    ----------------------------------------------------------------------------

    function Get_Zoom( this : not null access Widget'Class ) return Float is (this.zoomFactor);

    ----------------------------------------------------------------------------

    function Is_Descendant_Of( this     : not null access Widget'Class;
                               ancestor : access Widget'Class ) return Boolean is
        next : A_Widget := A_Widget(this);
    begin
        if ancestor = null then
            return False;
        end if;
        loop
            if next = ancestor then
                return True;
            end if;
            exit when next.parent = null;
            next := next.parent;
        end loop;
        return False;
    end Is_Descendant_Of;

    ----------------------------------------------------------------------------

    function Is_Enabled( this : not null access Widget'Class ) return Boolean is
        next : A_Widget := A_Widget(this);
    begin
        loop
            if not next.enable then
                return False;
            end if;
            next := next.parent;
            exit when next = null;
        end loop;
        return True;
    end Is_Enabled;

    ----------------------------------------------------------------------------

    function Is_Focusable( this : not null access Widget'Class ) return Boolean
    is (this.acceptFocus and then this.Is_Enabled and then this.Is_Showing);

    ----------------------------------------------------------------------------

    function Is_Focused( this : not null access Widget'Class ) return Boolean is (this.hasFocus);

    ----------------------------------------------------------------------------

    function Is_Hovered( this : not null access Widget'Class ) return Boolean is (this.hover);

    ----------------------------------------------------------------------------

    function Is_Rooted( this : not null access Widget'Class ) return Boolean is
        next : A_Widget := A_Widget(this);
    begin
        loop
            if next.all in Window'Class then
                return True;
            end if;
            next := next.parent;
            exit when next = null;
        end loop;
        return False;
    end Is_Rooted;

    ----------------------------------------------------------------------------

    function Is_Showing( this : not null access Widget'Class ) return Boolean is
        next : A_Widget := A_Widget(this);
    begin
        loop
            exit when not next.Is_Visible;
            if next.all in Window'Class then
                return True;
            end if;
            next := next.parent;
            exit when next = null;
        end loop;
        return False;    -- self or an ancestor was not visible or not rooted
    end Is_Showing;

    ----------------------------------------------------------------------------

    procedure Listen_For_Event( this : not null access Widget'Class; evtId : Event_Id ) is
    begin
        this.Get_View.Get_Corral.Add_Listener( this, evtId );
    end Listen_For_Event;

    ----------------------------------------------------------------------------

    procedure Move( this     : not null access Widget'Class;
                    dx, dy   : Float;
                    duration : Time_Span := Time_Span_Zero;
                    easing   : Easing_Type := Linear ) is
    begin
        if dx /= 0.0 then
            this.X.Set( this.Get_X + dx, duration, easing );
        end if;
        if dy /= 0.0 then
            this.Y.Set( this.Get_Y + dy, duration, easing );
        end if;
    end Move;

    ----------------------------------------------------------------------------

    procedure Move_To( this     : not null access Widget'Class;
                       x, y     : Float;
                       duration : Time_Span := Time_Span_Zero;
                       easing   : Easing_Type := Linear ) is
    begin
        this.X.Set( x, duration, easing );
        this.Y.Set( y, duration, easing );
    end Move_To;

    ----------------------------------------------------------------------------

    procedure On_Hidden( this : not null access Widget'Class ) is
    begin
        if this.all in Animated'Class then
            this.Get_View.Detach( A_Process(this) );
        end if;
    end On_Hidden;

    ----------------------------------------------------------------------------

    procedure On_Key_Tab( this : not null access Widget'Class;
                          key  : Key_Arguments ) is
    begin
        if key.modifiers(SHIFT) then
            if this.Focus_Prev then
                null;
            end if;
        else
            if this.Focus_Next then
                null;
            end if;
        end if;
    end On_Key_Tab;

    ----------------------------------------------------------------------------

    procedure On_Shown( this : not null access Widget'Class ) is
    begin
        if this.all in Animated'Class then
            this.Get_View.Attach( A_Process(this) );
        end if;
    end On_Shown;

    ---------------------------------------------------------------------------

    procedure Propagate_Enabled( this : not null access Widget'Class; enabled : Boolean ) is
    begin
        if enabled then
            -- enabled propagates down the tree
            this.Enabled.Emit;
        end if;

        for child of this.children loop
            child.Propagate_Enabled( enabled );
        end loop;

        if not enabled then
            -- disabled propagates up the tree
            this.Disabled.Emit;
        end if;
    end Propagate_Enabled;

    ----------------------------------------------------------------------------

    procedure Propagate_Rooted( this : not null access Widget'Class; rooted : Boolean ) is
    begin
        for child of this.children loop
            child.Propagate_Rooted( rooted );
        end loop;
        if rooted then
            this.Rooted.Emit;
        else
            this.Unrooted.Emit;
        end if;
    end Propagate_Rooted;

    ----------------------------------------------------------------------------

    procedure Propagate_Shown( this : not null access Widget'Class; shown : Boolean ) is
    begin
        -- propagate the signal from the leaves upward by traversing to the
        -- children first.
        for child of this.children loop
            if child.Is_Visible then
                child.Propagate_Shown( shown );
            end if;
        end loop;

        if shown then
            this.Shown.Emit;
        else
            this.Hidden.Emit;
            if this.Get_Window /= null then
                -- allow the window to transition keyboard focus and mouse presses
                this.Get_Window.Handle_Widget_Hidden( this );
            end if;
        end if;
    end Propagate_Shown;

    ----------------------------------------------------------------------------

    procedure Remove_Child( this  : not null access Widget'Class;
                            child : not null access Widget'Class ) is
        pos : Widget_Lists.Cursor := this.children.Find( child );
    begin
        if Has_Element( pos ) then
            if child.Is_Showing then
                -- In order for the GUI to clean up when a child is removed, it
                -- must be hidden first. All of the cleanup is performed when a
                -- descendent of the Window becomes hidden. The caller should
                -- have taken care of this before calling Remove_Child().
                Dbg( "Widget " & child.To_String & " removed while visible",
                     D_GUI, Warning );
                child.Set_Visible( False );
            end if;
            child.Set_Parent( null );
            this.children.Delete( pos );
        else
            raise WIDGET_NOT_FOUND with "Widget is not a child";
        end if;
    end Remove_Child;

    ----------------------------------------------------------------------------

    procedure Request_Focus( this : not null access Widget'Class ) is
    begin
        if this.Is_Rooted then
            this.Get_Window.Give_Focus( this );
        end if;
    end Request_Focus;

    ----------------------------------------------------------------------------

    procedure Scroll( this : not null access Widget'Class; dx, dy : Float ) is
    begin
        this.Scroll_To( this.viewport.x + dx, this.viewport.y + dy );
    end Scroll;

    ----------------------------------------------------------------------------

    procedure Scroll_To( this : not null access Widget'Class; x, y : Float ) is
        prefWidth  : constant Float := this.Get_Preferred_Width;
        prefHeight : constant Float := this.Get_Preferred_Height;
    begin
        this.viewport.x := x;
        this.viewport.y := y;

        -- if the widget implements a preferred size hint, use it as the
        -- scrollable area bounds for the viewport.
        if prefWidth > 0.0 then
            if this.viewport.x + this.viewport.width > this.Get_Preferred_Width then
                this.viewport.x := prefWidth - this.viewport.width;
            end if;
            if this.viewport.x < 0.0 then
                this.viewport.x := 0.0;
            end if;
        end if;
        if prefHeight > 0.0 then
            if this.viewport.y + this.viewport.height > this.Get_Preferred_Height then
                this.viewport.y := prefHeight - this.viewport.height;
            end if;
            if this.viewport.y < 0.0 then
                this.viewport.y := 0.0;
            end if;
        end if;
    end Scroll_To;

    ----------------------------------------------------------------------------

    procedure Send_To_Back( this : not null access Widget'Class ) is
        pos : Widget_Lists.Cursor;
    begin
        if this.parent /= null then
            pos := this.parent.children.Find( this );
            pragma Assert( Has_Element( pos ) );
            this.parent.children.Delete( pos );
            this.parent.children.Prepend( this );     -- draw it first
        end if;
    end Send_To_Back;

    ----------------------------------------------------------------------------

    procedure Set_Attribute( this    : not null access Widget'Class;
                             name    : String;
                             val     : Value'Class;
                             consume : Boolean := False ) is
    begin
        this.attributes.Set( name, val, consume => consume );
        this.AttributeChanged.Emit( name );
    end Set_Attribute;

    procedure Set_Attribute( this : not null access Widget'Class; name : String; val : Boolean ) is
    begin
        this.Set_Attribute( name, Create( val ), consume => True );
    end Set_Attribute;

    procedure Set_Attribute( this : not null access Widget'Class; name : String; val : Integer ) is
    begin
        this.Set_Attribute( name, Create( val ), consume => True );
    end Set_Attribute;

    procedure Set_Attribute( this : not null access Widget'Class; name : String; val : Float ) is
    begin
        this.Set_Attribute( name, Create( val ), consume => True );
    end Set_Attribute;

    procedure Set_Attribute( this : not null access Widget'Class; name : String; val : Long_Float ) is
    begin
        this.Set_Attribute( name, Create( val ), consume => True );
    end Set_Attribute;

    procedure Set_Attribute( this : not null access Widget'Class; name : String; val : String ) is
    begin
        this.Set_Attribute( name, Create( val ), consume => True );
    end Set_Attribute;

    procedure Set_Attribute( this : not null access Widget'Class; name : String; val : Unbounded_String ) is
    begin
        this.Set_Attribute( name, Create( val ), consume => True );
    end Set_Attribute;

    ----------------------------------------------------------------------------

    procedure Set_Enabled( this : not null access Widget'Class; enable : Boolean ) is
        wasEnabled : constant Boolean := this.Is_Enabled;
        isEnabled  : Boolean;
    begin
        if enable /= this.enable then
            this.enable := enable;
            isEnabled := this.Is_Enabled;
            if not wasEnabled and then isEnabled then
                this.Propagate_Enabled( True );
            elsif wasEnabled and then not isEnabled then
                this.Propagate_Enabled( False );
            end if;
        end if;
    end Set_Enabled;

    ----------------------------------------------------------------------------

    procedure Set_Focusable( this      : not null access Widget'Class;
                             focusable : Boolean ) is
    begin
        if focusable /= this.acceptFocus then
            this.acceptFocus := focusable;
        end if;
    end Set_Focusable;

    ----------------------------------------------------------------------------

    procedure Set_Focused( this : not null access Widget'Class; focused : Boolean ) is
    begin
        if this.hasFocus /= focused then
            this.hasFocus := focused;
            if this.hasFocus then
                this.Focused.Emit;
            else
                this.Blurred.Emit;
            end if;
        end if;
    end Set_Focused;

    ----------------------------------------------------------------------------

    procedure Set_Height( this : not null access Widget'Class; height : Float ) is
        oldHeight : constant Value := this.posHeight;
    begin
        if height >= 0.0 then
            this.posHeight := Create( height );
        else
            this.posHeight := Null_Value;
        end if;
        if this.posHeight /= oldHeight then
            this.Update_Geometry( Y_AXIS );
        end if;
    end Set_Height;

    ----------------------------------------------------------------------------

    procedure Set_Hover( this : not null access Widget'Class; hover : Boolean ) is
    begin
        if this.hover /= hover then
            this.hover := hover;
            if this.hover then
                this.MouseEntered.Emit;
            else
                this.MouseExited.Emit;
            end if;
        end if;
    end Set_Hover;

    ----------------------------------------------------------------------------

    procedure Set_Mouse_Cursor( this : not null access Widget'Class;
                                name : String ) is
    begin
        if name /= this.mouseCursor then
            this.mouseCursor := To_Unbounded_String( name );
            if this.Get_Window /= null then
                this.Get_Window.Show_Mouse_Cursor( this );
            end if;
        end if;
    end Set_Mouse_Cursor;

    ----------------------------------------------------------------------------

    procedure Set_Next( this : not null access Widget'Class; id : String ) is
    begin
        if this.nextId /= id then
            if Length( this.nextId ) = 0 and then id'Length > 0 then
                this.KeyTyped.Connect( Slot( this, On_Key_Tab'Access, ALLEGRO_KEY_TAB, (others=>No), priority => Low ) );
            elsif Length( this.nextId ) > 0 and then id'Length = 0 then
                this.KeyTyped.Disconnect( Slot( this, On_Key_Tab'Access, ALLEGRO_KEY_TAB, (others=>No), priority => Low ) );
            end if;
            this.nextId := To_Unbounded_String( id );
        end if;
    end Set_Next;

    ----------------------------------------------------------------------------

    procedure Set_Parent( this   : not null access Widget'Class;
                          parent : access Widget'Class ) is
        wasRooted : Boolean;
        wasShown  : Boolean;
    begin
        if parent = this.parent then
            return;
        end if;

        wasRooted := this.Is_Rooted;
        wasShown := wasRooted and then this.Is_Showing;

        -- if the parent has already been set, remove it from its widget tree
        -- before setting its new parent.
        if this.parent /= null then
            -- detach anchors to parent and siblings
            this.Left.Detach;
            this.Right.Detach;
            this.Horizontal_Center.Detach;
            this.Top.Detach;
            this.Bottom.Detach;
            this.Vertical_Center.Detach;

            -- detach siblings' anchors to this
            this.parent.Detach_Anchors( this );

            this.parent := null;
            if parent = null then
                this.ParentChanged.Emit;
            end if;
            if wasRooted then
                this.Propagate_Rooted( False );
                if wasShown then
                    this.Propagate_Shown( False );
                end if;
            end if;
        end if;

        -- set the widget's new parent
        if parent /= null then
            this.parent := A_Widget(parent);
            if this.Is_Rooted then
                this.Update_Geometry( XY_AXES );
                this.ParentChanged.Emit;
                this.Propagate_Rooted( True );
                if this.Is_Showing then
                    this.Propagate_Shown( True );
                end if;
            else
                this.ParentChanged.Emit;
            end if;
        end if;
    end Set_Parent;

    ----------------------------------------------------------------------------

    procedure Set_Preferred_Height( this : not null access Widget'Class; height : Float ) is
    begin
        if height > 0.0 then
            this.prefHeight := Create( height );
        else
            this.Unset_Preferred_Height;
        end if;
    end Set_Preferred_Height;

    ----------------------------------------------------------------------------

    procedure Set_Preferred_Width( this : not null access Widget'Class; width : Float ) is
    begin
        if width > 0.0 then
            this.prefWidth := Create( width );
        else
            this.Unset_Preferred_Width;
        end if;
    end Set_Preferred_Width;

    ----------------------------------------------------------------------------

    procedure Set_Prev( this : not null access Widget'Class; id : String ) is
    begin
        if this.prevId /= id then
            if Length( this.prevId ) = 0 and then id'Length > 0 then
                this.KeyTyped.Connect( Slot( this, On_Key_Tab'Access, ALLEGRO_KEY_TAB, (SHIFT=>Yes, others=>No), priority => Low ) );
            elsif Length( this.prevId ) > 0 and then id'Length = 0 then
                this.KeyTyped.Disconnect( Slot( this, On_Key_Tab'Access, ALLEGRO_KEY_TAB, (SHIFT=>Yes, others=>No), priority => Low ) );
            end if;
            this.prevId := To_Unbounded_String( id );
        end if;
    end Set_Prev;

    ----------------------------------------------------------------------------

    procedure Set_Style( this : not null access Widget'Class; name : String ) is
        style,
        style2 : A_Widget_Style;
        first  : Integer := name'First;
        last   : Integer := Index( name, " " ) - 1;
    begin
        -- always apply the base style first
        style := Get_Style( To_String( this.widgetType ), "" );

        -- apply styles from left first to right last
        if name'Length > 0 then
            while last >= first loop
                style2 := Widget_Styles.Registry.Get_Style( To_String( this.widgetType ), name(first..last) );
                pragma Debug( style2 = null, Dbg( "Style '" & name(first..last) & "' is undefined", D_GUI, Warning ) );
                if style /= null then
                    style.Merge_From( style2 );
                    Delete( style2 );
                else
                    style := style2;
                end if;

                first := last + 2;
                last := Index( name, " ", first ) - 1;
            end loop;

            style2 := Widget_Styles.Registry.Get_Style( To_String( this.widgetType ), name(first..name'Last) );
            pragma Debug( style2 = null, Dbg( "Style '" & name(first..name'Last) & "' is undefined", D_GUI, Warning ) );
            if style /= null then
                style.Merge_From( style2 );
                Delete( style2 );
            else
                style := style2;
            end if;
        end if;

        this.Set_Style( style );      -- copies 'style'
        Delete( style );
    end Set_Style;

    ----------------------------------------------------------------------------

    procedure Set_Style( this : not null access Widget'Class; style : A_Widget_Style ) is
    begin
        if style /= null then
            this.style.Copy_From( style );
            this.StyleChanged.Emit;
        end if;
    end Set_Style;

    ----------------------------------------------------------------------------

    procedure Set_Style_Property( this     : not null access Widget'Class;
                                  element  : Element_Index;
                                  property : String;
                                  state    : Widget_State;
                                  val      : Value'Class ) is
    begin
        if this.style.Set_Property( element, To_Property_Id( property ), state, val ) then
            this.StyleChanged.Emit;
        else
            Dbg( "No style property '" & property & "' in element" &
                 Element_Index'Image( element ) & " of " & this.To_String,
                 D_GUI, Warning );
        end if;
    end Set_Style_Property;

    ----------------------------------------------------------------------------

    procedure Set_Style_Property( this     : not null access Widget'Class;
                                  property : String;
                                  state    : Widget_State;
                                  val      : Value'Class ) is
        -- property is in the form "elemName.propName"
        dot  : constant Integer := Index( property, "." );

        def  : A_Style_Definition;
        elem : Element_Index;
    begin
        if dot > property'First and dot < property'Last then
            -- lookup the style's definition so we can translate an element name
            -- to an index.
            def := Get_Definition( this.style.Get_Widget_Type );
            if def /= null then
                if def.Get_Element( property(property'First..dot-1), elem ) then
                    this.Set_Style_Property( elem, property(dot+1..property'Last), state, val );
                else
                    Dbg( "No element named " & property(property'First..dot-1) );
                end if;
            else
            Dbg( "No def for " & this.style.Get_Name );
            end if;
        end if;
    end Set_Style_Property;

    ----------------------------------------------------------------------------

    procedure Set_Style_Property( this     : not null access Widget'Class;
                                  property : String;
                                  val      : Value'Class ) is
    begin
        this.Set_Style_Property( property, DEFAULT_STATE, val );
    end Set_Style_Property;

    ----------------------------------------------------------------------------

    procedure Set_Visible( this : not null access Widget'Class; visible : Boolean ) is
        wasShown : Boolean;
    begin
        if visible = this.vis then
            return;
        end if;

        wasShown := this.Is_Showing;

        this.vis := visible;
        if this.Is_Showing then
            -- the widget became shown
            this.Propagate_Shown( True );
        elsif wasShown then
            -- the widget became unshown
            this.Propagate_Shown( False );
        end if;
    end Set_Visible;

    ----------------------------------------------------------------------------

    procedure Set_Width( this : not null access Widget'Class; width : Float ) is
        oldWidth : constant Value := this.posWidth;
    begin
        if width >= 0.0 then
            this.posWidth := Create( width );
        else
            this.posWidth := Null_Value;
        end if;
        if this.posWidth /= oldWidth then
            this.Update_Geometry( X_AXIS );
        end if;
    end Set_Width;

    ----------------------------------------------------------------------------

    procedure Set_X( this : not null access Widget'Class; x : Float ) is
    begin
        if Create( x ) /= this.posX then
            this.posX := Create( x );
            this.Update_Geometry( X_AXIS );
        end if;
    end Set_X;

    ----------------------------------------------------------------------------

    procedure Set_Y( this : not null access Widget'Class; y : Float ) is
    begin
        if Create( y ) /= this.posY then
            this.posY := Create( y );
            this.Update_Geometry( Y_AXIS );
        end if;
    end Set_Y;

    ----------------------------------------------------------------------------

    procedure Set_Z( this : not null access Widget'Class; z : Float ) is
    begin
        this.z := z;
    end Set_Z;

    ----------------------------------------------------------------------------

    procedure Set_Zoom( this : not null access Widget'Class; zoom : Float ) is
    begin
        if zoom /= 0.0 and this.zoomFactor /= zoom then
            this.zoomFactor := zoom;
            this.Update_Geometry( ZOOM_AXES );
        end if;
    end Set_Zoom;

    ----------------------------------------------------------------------------

    overriding
    function To_String( this : access Widget ) return String
    is ("<" & this.Get_Class_Name & " : id=" & To_String( this.id ) & ">");

    ----------------------------------------------------------------------------

    procedure Translate_To_Content( this   : not null access Widget'Class;
                                    sx, sy : Float;
                                    cx, cy : out Float ) is
    begin
        if this.parent /= null then
            this.parent.Translate_To_Content( sx, sy, cx, cy );
        else
            cx := sx;
            cy := sy;
        end if;
        cx := this.viewport.x + (cx - this.geom.x) / this.zoomFactor;
        cy := this.viewport.y + (cy - this.geom.y) / this.zoomFactor;
    end Translate_To_Content;

    ----------------------------------------------------------------------------

    procedure Translate_To_Window( this   : not null access Widget'Class;
                                   cx, cy : Float;
                                   wx, wy : out Float ) is
        parentX, parentY : Float;
    begin
        -- translate the content coordinates of this widget to the content
        -- coordinates of the parent
        wx := this.geom.x + this.zoomFactor * (cx - this.viewport.x);
        wy := this.geom.y + this.zoomFactor * (cy - this.viewport.y);

        -- translate the content coordinates of the parent to screen coordinates
        if this.parent /= null then
            parentX := wx;
            parentY := wy;
            this.parent.Translate_To_Window( parentX, parentY, wx, wy );
        end if;
    end Translate_To_Window;

    ----------------------------------------------------------------------------

    procedure Unlisten_For_Event( this : not null access Widget'Class; evtId : Event_Id := 0 ) is
    begin
        this.Get_View.Get_Corral.Remove_Listener( this, evtId );
    end Unlisten_For_Event;

    ----------------------------------------------------------------------------

    procedure Unset_Height( this : not null access Widget'Class ) is
    begin
        if this.posHeight /= Null_Value then
            this.posHeight := Null_Value;
            this.Update_Geometry( Y_AXIS );
        end if;
    end Unset_Height;

    ----------------------------------------------------------------------------

    procedure Unset_Preferred_Height( this : not null access Widget'Class ) is
    begin
        this.prefHeight := Null_Value;
    end Unset_Preferred_Height;

    ----------------------------------------------------------------------------

    procedure Unset_Preferred_Width( this : not null access Widget'Class ) is
    begin
        this.prefWidth := Null_Value;
    end Unset_Preferred_Width;

    ----------------------------------------------------------------------------

    procedure Unset_Width( this : not null access Widget'Class ) is
    begin
        if this.posWidth /= Null_Value then
            this.posWidth := Null_Value;
            this.Update_Geometry( X_AXIS );
        end if;
    end Unset_Width;

    ----------------------------------------------------------------------------

    procedure Unset_X( this : not null access Widget'Class ) is
    begin
        if not this.posX.Is_Null then
            this.posX := Null_Value;
            this.Update_Geometry( X_AXIS );
        end if;
    end Unset_X;

    ----------------------------------------------------------------------------

    procedure Unset_Y( this : not null access Widget'Class ) is
    begin
        if not this.posY.Is_Null then
            this.posY := Null_Value;
            this.Update_Geometry( Y_AXIS );
        end if;
    end Unset_Y;

    ----------------------------------------------------------------------------

    procedure Update_Geometry( this : not null access Widget'Class;
                               axis : Axis_Type := XY_AXES ) is
        siblingXobservers : Integer;
        siblingYobservers : Integer;
    begin
        if not this.Is_Rooted then
            return;
        end if;

        siblingXobservers := this.ancLeft.siblingObservers + this.ancRight.siblingObservers + this.ancCenterX.siblingObservers;
        siblingYobservers := this.ancTop.siblingObservers + this.ancBottom.siblingObservers + this.ancCenterY.siblingObservers;

        if ((axis and X_AXIS) > 0 and (siblingXobservers > 0)) or ((axis and Y_AXIS) > 0 and (siblingYobservers > 0)) then
            -- if the widget has any siblings anchored to it, then they must be
            -- updated too
            this.parent.Update_Geometry_Children( axis );
        else
            this.Update_Geometry_Single( axis );
        end if;
    end Update_Geometry;

    ----------------------------------------------------------------------------

    procedure Update_Geometry_Children( this : not null access Widget'Class;
                                        axis : Axis_Type ) is
    begin
        -- all the children's anchors are marked unresolved, and then they
        -- are resolved in order of dependency, as siblings anchor to each
        -- other.
        for child of this.children loop
            if (axis and X_AXIS) > 0 then
                child.ancLeft.resolved := False;
                child.ancRight.resolved := False;
                child.ancCenterX.resolved := False;
            end if;
            if (axis and Y_AXIS) > 0 then
                child.ancTop.resolved := False;
                child.ancBottom.resolved := False;
                child.ancCenterY.resolved := False;
            end if;
        end loop;

        for child of this.children loop
            child.Update_Geometry_Single( axis );
        end loop;
    end Update_Geometry_Children;

    ----------------------------------------------------------------------------

    procedure Update_Geometry_Single( this : not null access Widget'Class;
                                      axis : Axis_Type ) is
        oldGeom  : constant Rectangle := this.geom;
        size     : Float;
        posCombo : Unsigned_8 := 0;
    begin
        -- verify that the set of specified X, width, and anchors is valid
        if (axis and X_AXIS) > 0 then
            posCombo := (if this.ancLeft.Is_Attached then POS_BIT_LEFT else 0) or
                        (if this.ancRight.Is_Attached then POS_BIT_RIGHT else 0) or
                        (if this.ancCenterX.Is_Attached then POS_BIT_CENTER else 0) or
                        (if this.posX.Is_Number then POS_BIT_X else 0) or
                        (if this.posWidth.Is_Number then POS_BIT_WIDTH else 0);
            if not Is_Valid_Positioning( posCombo ) then
                raise Constraint_Error with "Invalid X positioning (" & Image( posCombo ) & ") for " & this.To_String;
            end if;
        end if;

        -- verify that the set of specified Y, height, and anchors is valid
        if (axis and Y_AXIS) > 0 then
            posCombo := (if this.ancTop.Is_Attached then POS_BIT_LEFT else 0) or
                        (if this.ancBottom.Is_Attached then POS_BIT_RIGHT else 0) or
                        (if this.ancCenterY.Is_Attached then POS_BIT_CENTER else 0) or
                        (if this.posY.Is_Number then POS_BIT_X else 0) or
                        (if this.posHeight.Is_Number then POS_BIT_WIDTH else 0);
            if not Is_Valid_Positioning( posCombo ) then
                raise Constraint_Error with "Invalid Y positioning (" & Image( posCombo ) & ") for " & this.To_String;
            end if;
        end if;

        -- begin resolving the X positions of own anchors. if we end up back
        -- here through recursive calls, then there is anchor circularity among
        -- siblings.
        if (axis and X_AXIS) > 0 then
            if this.resolvingX then
                raise Constraint_Error with "Widget positioning circularity in X axis at " & this.To_String;
            end if;
            this.resolvingX := True;

            -- resolve own anchors to parent and siblings
            this.ancLeft.Resolve_Attachment;
            this.ancRight.Resolve_Attachment;
            this.ancCenterX.Resolve_Attachment;

            -- update geometry X position --
            if this.ancLeft.Is_Attached then
                -- if left anchor in use, x is based on left anchor
                this.geom.x := this.ancLeft.Get_Position + this.ancLeft.margin * this.zoomFactor;
            elsif this.posX.Is_Number then
                -- else use the specified X
                this.geom.x := this.posX.To_Float;
                if this.parent /= null then
                    this.geom.x := this.geom.x + this.parent.ancLeft.innerPad;
                end if;
            elsif this.ancRight.Is_Attached then
                -- else if right anchor in use, x is based on right anchor and width
                if this.posWidth.Is_Number then
                    -- use the specified width when available
                    size := this.posWidth.To_Float;
                else
                    -- fall back to preferred width hint if no width is specified
                    size := this.Get_Preferred_Width;
                end if;
                this.geom.x := this.ancRight.Get_Position - this.ancRight.margin * this.zoomFactor - size;
            elsif this.ancCenterX.Is_Attached then
                -- else if the center X anchor in use, x is based on center anchor and width
                if this.posWidth.Is_Number then
                    -- use the specified width when available
                    size := this.posWidth.To_Float;
                else
                    -- fall back to preferred width hint if no width is specified
                    size := this.Get_Preferred_Width;
                end if;
                this.geom.x := Float'Floor( this.ancCenterX.Get_Position + this.ancCenterX.margin * this.zoomFactor - (size / 2.0) );
            else
                -- no way to calculate the left edge
                this.geom.x := 0.0;
                if this.parent /= null then
                    this.geom.x := this.geom.x + this.parent.ancLeft.innerPad;
                end if;
            end if;

            -- update geometry width --
            if this.ancRight.Is_Attached then
                -- if right anchor is in use, width is based on stretching
                this.geom.width := this.ancRight.Get_Position - this.ancRight.margin * this.zoomFactor - this.geom.x;
            elsif this.posWidth.Is_Number then
                -- else use the specified width
                this.geom.width := this.posWidth.To_Float;
            else
                -- else fall back to the preferred width
                this.geom.width := this.Get_Preferred_Width;
            end if;
            this.geom.width := Float'Max( 0.0, this.geom.width );

            this.ancLeft.position := this.geom.x;
            this.ancRight.position := this.geom.x + this.geom.width;
            this.ancCenterX.position := this.geom.x + (this.geom.width / 2.0);

            -- NOTE: should this be done instead? calculate the horizontal center
            -- anchor line, taking this widget's inner padding into account.
            --this.ancCenterX.position := this.geom.x + this.ancLeft.innerPad + (this.geom.width - (this.ancLeft.innerPad - this.ancRight.innerPad)) / 2.0;

            this.ancLeft.resolved := True;
            this.ancRight.resolved := True;
            this.ancCenterX.resolved := True;
            this.resolvingX := False;
        end if;

        -- begin resolving the Y positions of own anchors. if we end up back
        -- here through recursive calls, then there is anchor circularity among
        -- siblings.
        if (axis and Y_AXIS) > 0 then
            if this.resolvingY then
                raise Constraint_Error with "Widget positioning circularity in Y axis at " & this.To_String;
            end if;
            this.resolvingY := True;

            -- resolve own anchors to parent and siblings
            this.ancTop.Resolve_Attachment;
            this.ancBottom.Resolve_Attachment;
            this.ancCenterY.Resolve_Attachment;

            -- update geometry Y position --
            if this.ancTop.Is_Attached then
                -- if top anchor in use, y is based on top anchor
                this.geom.y := this.ancTop.Get_Position + this.ancTop.margin * this.zoomFactor;
            elsif this.posY.Is_Number then
                -- else use the specified Y
                this.geom.y := this.posY.To_Float;
                if this.parent /= null then
                    this.geom.y := this.geom.y + this.parent.ancTop.innerPad;
                end if;
            elsif this.ancBottom.Is_Attached then
                -- else if bottom anchor in use, y is based on bottom anchor and height
                if this.posHeight.Is_Number then
                    -- use the specified height when available
                    size := this.posHeight.To_Float;
                else
                    -- fall back to preferred height hint if no height is specified
                    size := this.Get_Preferred_Height;
                end if;
                this.geom.y := this.ancBottom.Get_Position - this.ancBottom.margin * this.zoomFactor - size;
            elsif this.ancCenterY.Is_Attached then
                -- else if the center Y anchor in use, x is based on center anchor and height
                if this.posHeight.Is_Number then
                    -- use the specified height when available
                    size := this.posHeight.To_Float;
                else
                    -- fall back to preferred height hint if no height is specified
                    size := this.Get_Preferred_Height;
                end if;
                this.geom.y := Float'Floor( this.ancCenterY.Get_Position + this.ancCenterY.margin * this.zoomFactor - (size / 2.0) );
            else
                -- no way to calculate the top edge
                this.geom.y := 0.0;
                if this.parent /= null then
                    this.geom.y := this.geom.y + this.parent.ancTop.innerPad;
                end if;
            end if;

            -- update geometry height
            if this.ancBottom.Is_Attached then
                -- if bottom anchor is in use, height is based on stretching
                this.geom.height := this.ancBottom.Get_Position - this.ancBottom.margin * this.zoomFactor - this.geom.y;
            elsif this.posHeight.Is_Number then
                -- else use the specified height
                this.geom.height := this.posHeight.To_Float;
            else
                -- else fall back to the preferred height
                this.geom.height := this.Get_Preferred_Height;
            end if;
            this.geom.height := Float'Max( 0.0, this.geom.height );

            this.ancTop.position := this.geom.y;
            this.ancBottom.position := this.geom.y + this.geom.height;
            this.ancCenterY.position := this.geom.y + (this.geom.height / 2.0);

            -- NOTE: should this be done instead? calculate the vertical center
            -- anchor line, taking this widget's inner padding into account.
            -- this.ancCenterY.position := this.geom.y + this.ancTop.innerPad + (this.geom.height - (this.ancTop.innerPad - this.ancBottom.innerPad)) / 2.0;

            this.ancTop.resolved := True;
            this.ancBottom.resolved := True;
            this.ancCenterY.resolved := True;
            this.resolvingY := False;
        end if;

        -- check if either the size changed or the zoom changed. the viewport
        -- and children geometry need to be updated when the zoom changes, even
        -- if the widget's geometry size didn't change.
        if (this.geom.width /= oldGeom.width) or (this.geom.height /= oldGeom.height) or ((axis and ZOOM_AXES) = ZOOM_AXES) then
            this.viewport.width := this.geom.width / this.zoomFactor;
            this.viewport.height := this.geom.height / this.zoomFactor;

            -- update only the X axis if only the width changed. update only the
            -- Y axis if only the height changed. always update both axes if the
            -- zoom changed.
            if (this.geom.width /= oldGeom.width and this.geom.height /= oldGeom.height) or ((ZOOM_AXES and axis) = ZOOM_AXES) then
                this.Update_Geometry_Children( XY_AXES );
            elsif this.geom.width /= oldGeom.width then
                this.Update_Geometry_Children( X_AXIS );
            else
                this.Update_Geometry_Children( Y_AXIS );
            end if;

            this.Resized.Emit;
        end if;
    end Update_Geometry_Single;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Widget ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

    ----------------------------------------------------------------------------

    procedure Delete_Later( this : in out A_Widget ) is
        view : A_Game_View;

        -- Recursively remove all widgets from the view's widget id registry
        procedure Unregister( w : not null A_Widget ) is
        begin
            for child of w.children loop
                Unregister( child );
            end loop;
            if w.registered then
                w.Get_View.Unregister( w.Get_Id );
                w.registered := False;
            end if;
        end Unregister;

    begin
        if this /= null then
            view := this.Get_View;
            if view /= null then
                -- the widget must have been removed from its parent already
                pragma Assert( this.parent = null );

                -- unregister all widgets in the tree and schedule delete for
                -- the root. the children will be deleted with it.
                Unregister( this );
                view.Schedule_Widget_Delete( this );
            else
                Delete( this );
            end if;
        end if;
    end Delete_Later;

    ----------------------------------------------------------------------------

    function Lt( l, r : A_Widget ) return Boolean is
    begin
        if l = null then
            return r /= null;
        elsif r /= null then
            return l.id < r.id;
        end if;

        -- l /= null and r = null
        return False;
    end Lt;

    --==========================================================================

    procedure Attach( this   : not null access Anchor'Class;
                      target : access Anchor'Class;
                      margin : Float := 0.0 ) is
    begin
        -- validate the anchor type
        if target.typ /= this.typ then
            raise Constraint_Error with "Expected " & this.typ'Img & " anchor";
        end if;

        if this.target = A_Anchor(target) then
            -- not updating the target, just the margin
            this.Set_Margin( margin );
            return;
        end if;

        if this.target /= null and then this.target.owner.parent = this.owner.parent then
            this.target.siblingObservers := this.target.siblingObservers - 1;
        end if;

        if target = null then
            -- anchoring to nothing
            null;
        elsif target.owner.parent = this.owner.parent then
            -- anchoring to sibling
            target.siblingObservers := target.siblingObservers + 1;
        elsif target.owner /= this.owner.parent then
            -- not anchoring to parent
            raise Constraint_Error with "Anchor isn't to parent or sibling";
        end if;

        this.target := A_Anchor(target);
        this.margin := (if target /= null then margin else 0.0);
            this.resolved := False;
            if this.typ = Vertical then
                this.owner.Update_Geometry( X_AXIS );
            else
                this.owner.Update_Geometry( Y_AXIS );
            end if;
    end Attach;

    ----------------------------------------------------------------------------

    procedure Detach( this : not null access Anchor'Class; target : access Widget'Class ) is
    begin
        if this.target /= null and then this.target.owner = A_Widget(target) then
            this.Detach;
        end if;
    end Detach;

    ----------------------------------------------------------------------------

    procedure Detach( this : not null access Anchor'Class ) is
    begin
        if this.target /= null then
            if this.target.owner.parent = this.owner.parent then
                -- was anchored to sibling
                this.target.siblingObservers := this.target.siblingObservers - 1;
            end if;

            this.target := null;
            this.margin := 0.0;
            this.resolved := False;
            if this.typ = Vertical then
                this.owner.Update_Geometry( X_AXIS );
            else
                this.owner.Update_Geometry( Y_AXIS );
            end if;
        end if;
    end Detach;

    ----------------------------------------------------------------------------

    function Get_Margin( this : not null access Anchor'Class ) return Float is (this.margin);

    ----------------------------------------------------------------------------

    function Get_Position( this : not null access Anchor'Class ) return Float is
        pos : Float := this.target.position;
    begin
        if this.target.owner = this.owner.parent then
            if this.typ = Vertical then
                pos := pos - this.owner.parent.geom.x + this.target.innerPad;
            else
                pos := pos - this.owner.parent.geom.y + this.target.innerPad;
            end if;

            -- convert to the parent's content coordinates from the grandparent's
            -- content coodinates (the parent's .geom coordinates).
            pos := pos / this.owner.parent.zoomFactor;
        end if;
        return pos;
    end Get_Position;

    ----------------------------------------------------------------------------

    function Is_Attached( this : not null access Anchor'Class ) return Boolean is (this.target /= null);

    ----------------------------------------------------------------------------

    procedure Resolve_Attachment( this : not null access Anchor'Class ) is
    begin
        -- NOTE: Calling Update_Geometry_Single() here may change the size of
        -- the target widget when it resolves its anchors, which would cause it
        -- to update its own children too.

        if this.Is_Attached and then not this.target.resolved then
            if this.typ = Vertical then
                this.target.owner.Update_Geometry_Single( X_AXIS );
            else
                this.target.owner.Update_Geometry_Single( Y_AXIS );
            end if;
        end if;
    end Resolve_Attachment;

    ----------------------------------------------------------------------------

    procedure Set_Margin( this : not null access Anchor'Class; margin : Float ) is
    begin
        if this.Is_Attached then
            if this.margin /= margin then
                this.margin := margin;
                if this.typ = Vertical then
                    this.owner.Update_Geometry( X_AXIS );
                else
                    this.owner.Update_Geometry( Y_AXIS );
                end if;
            end if;
        end if;
    end Set_Margin;

end Widgets;
