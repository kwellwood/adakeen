--
-- Copyright (c) 2017-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Drawing.Primitives;                use Drawing.Primitives;
with Drawing.Tiles;                     use Drawing.Tiles;
with Styles;                            use Styles;
with Values.Construction;               use Values.Construction;
with Values.Strings;                    use Values.Strings;
with Widgets.Buttons;                   use Widgets.Buttons;
with Widgets.Buttons.Toggles;           use Widgets.Buttons.Toggles;
with Widget_Styles.Registry;            use Widget_Styles.Registry;

package body Widgets.Tab_Views is

    package Connections is new Signals.Connections(Tab_View);
    use Connections;

    ----------------------------------------------------------------------------

    procedure On_Button_Pressed( this : not null access Tab_View'Class );

    procedure On_Index_Changed( this : not null access Tab_View'Class );

    procedure On_Style_Changed( this : not null access Tab_View'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Tab_View( view : not null access Game_Views.Game_View'Class;
                              id   : String := "" ) return A_Tab_View is
        this : constant A_Tab_View := new Tab_View;
    begin
        this.Construct( view, id );
        return this;
    end Create_Tab_View;

    ----------------------------------------------------------------------------

    procedure Append( this    : not null access Tab_View'Class;
                      content : not null access Widget'Class;
                      title   : String;
                      icon    : Icon_Type := NO_ICON ) is
    begin
        this.Insert( this.Count + 1, content, title, icon );
    end Append;

    ----------------------------------------------------------------------------

    procedure Clear( this : not null access Tab_View'Class ) is
    begin
        if this.Count > 0 then
            this.buttons.Clear;
            this.stack.Clear;
            this.On_Style_Changed;      -- recalculate the header height
        end if;
    end Clear;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Tab_View;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String ) is
    begin
        Widget(this.all).Construct( view, id, "TabView" );
        this.sigTabChanged.Init( this );

        this.StyleChanged.Connect( Slot( this, On_Style_Changed'Access ) );

        this.buttons := Create_Row_Layout( view );
        this.Add_Child( this.buttons );

        this.stack := Create_Stack_Layout( view );
        this.Add_Child( this.stack );
        this.stack.Fill( this );
        this.stack.IndexChanged.Connect( Slot( this, On_Index_Changed'Access ) );

        this.buttonGroup := Create_Button_Group;
        this.buttonGroup.Set_Keep_Selected( True );
        this.buttonGroup.Set.Connect( Slot( this, On_Button_Pressed'Access ) );

        -- layout the stack and buttons based on the style
        this.On_Style_Changed;
    end Construct;

    ----------------------------------------------------------------------------

    function Count( this : not null access Tab_View'Class ) return Natural is (this.buttons.Count);

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Tab_View ) is
    begin
        Delete( this.buttonGroup );
        Widget(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Content( this : access Tab_View ) is
        color : Allegro_Color;
    begin
        -- draw the header
        color := this.style.Get_Color( HEADER_ELEM );
        if color /= Transparent then
            Rectfill( 0.0, 0.0, this.viewport.width, this.headerHeight,
                      Lighten( color, this.style.Get_Shade( HEADER_ELEM ) ) );
        else
            Draw_Tile_Filled( this.style.Get_Image( HEADER_ELEM ).Get_Tile,
                              0.0, 0.0, 0.0,
                              this.viewport.width, this.headerHeight,
                              Lighten( this.style.Get_Tint( HEADER_ELEM ),
                                       this.style.Get_Shade( HEADER_ELEM ) ) );
        end if;

        -- draw the background --
        color := this.style.Get_Color( BACKGROUND_ELEM );
        if color /= Transparent then
            Rectfill( 0.0, this.headerHeight,
                      this.viewport.width, this.viewport.height,
                      Lighten( color, this.style.Get_Shade( BACKGROUND_ELEM ) ) );
        else
            Draw_Tile_Filled( this.style.Get_Image( BACKGROUND_ELEM ).Get_Tile,
                              0.0, this.headerHeight, 0.0,
                              this.viewport.width, this.viewport.height - this.headerHeight,
                              Lighten( this.style.Get_Tint( BACKGROUND_ELEM ),
                                       this.style.Get_Shade( BACKGROUND_ELEM ) ) );
        end if;

        -- draw the border --
        Draw_Tile_Filled( this.style.Get_Image( BORDER_ELEM ).Get_Tile,
                          0.0, 0.0, 0.0,
                          this.viewport.width, this.viewport.height,
                          Lighten( this.style.Get_Tint( BORDER_ELEM ),
                                   this.style.Get_Shade( BORDER_ELEM ) ) );
    end Draw_Content;

    ----------------------------------------------------------------------------

    function Find( this  : not null access Tab_View'Class;
                   title : String;
                   start : Positive := 1 ) return Natural is
    begin
        for i in start..this.Count loop
            if A_Button(this.buttons.Get_At( i )).Get_Text = title then
                return i;
            end if;
        end loop;
        return 0;
    end Find;

    ----------------------------------------------------------------------------

    function Get_Content( this : not null access Tab_View'Class; index : Integer ) return A_Widget is (this.stack.Get_At( index ));

    ----------------------------------------------------------------------------

    function Get_Icon( this : not null access Tab_View'Class; index : Integer ) return Icon_Type is
        button : constant A_Button := A_Button(this.buttons.Get_At( index ));
    begin
        if button /= null then
            return button.Get_Icon;
        end if;
        return NO_ICON;
    end Get_Icon;

    ----------------------------------------------------------------------------

    function Get_Index( this : not null access Tab_View'Class ) return Natural is
    begin
        for i in 1..this.buttons.Count loop
            if A_Button(this.buttons.Get_At( i )).Get_State then
                return i;
            end if;
        end loop;
        return 0;
    end Get_Index;

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Height( this : access Tab_View ) return Float is
        contentHeight : Float := 0.0;
    begin
        for i in 1..this.stack.Count loop
            contentHeight := Float'Max( contentHeight, this.stack.Get_At( i ).Get_Min_Height );
        end loop;

        return this.headerHeight +
               this.style.Get_Pad_Top( BACKGROUND_ELEM ) +
               contentHeight +
               this.style.Get_Pad_Bottom( BACKGROUND_ELEM );
    end Get_Min_Height;

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Width( this : access Tab_View ) return Float is
        tabsWidth    : Float := 0.0;
        contentWidth : Float := 0.0;
    begin
        for i in 1..this.Count loop
            contentWidth := Float'Max( contentWidth, this.stack.Get_At( i ).Get_Min_Width );
        end loop;

        tabsWidth := this.style.Get_Pad_Left( HEADER_ELEM ) +
                     A_Widget(this.buttons).Get_Min_Width +
                     this.style.Get_Pad_Right( HEADER_ELEM );

        contentWidth := this.style.Get_Pad_Left( BACKGROUND_ELEM ) +
                        contentWidth +
                        this.style.Get_Pad_Right( BACKGROUND_ELEM );

        return Float'Max( tabsWidth, contentWidth );
    end Get_Min_Width;

    ----------------------------------------------------------------------------

    overriding
    function Get_Preferred_Height( this : access Tab_View ) return Float is
        contentHeight : Float := 0.0;
    begin
        if not this.prefHeight.Is_Null then
            return this.prefHeight.To_Float;
        end if;

        for i in 1..this.stack.Count loop
            contentHeight := Float'Max( contentHeight, this.stack.Get_At( i ).Get_Preferred_Height );
        end loop;

        return this.headerHeight +
               this.style.Get_Pad_Top( BACKGROUND_ELEM ) +
               contentHeight +
               this.style.Get_Pad_Bottom( BACKGROUND_ELEM );
    end Get_Preferred_Height;

    ----------------------------------------------------------------------------

    overriding
    function Get_Preferred_Width( this : access Tab_View ) return Float is
        tabsWidth    : Float := 0.0;
        contentWidth : Float := 0.0;
    begin
        if not this.prefWidth.Is_Null then
            return this.prefWidth.To_Float;
        end if;

        for i in 1..this.Count loop
            contentWidth := Float'Max( contentWidth, this.stack.Get_At( i ).Get_Preferred_Width );
        end loop;

        tabsWidth := this.style.Get_Pad_Left( HEADER_ELEM ) +
                     A_Widget(this.buttons).Get_Preferred_Width +
                     this.style.Get_Pad_Right( HEADER_ELEM );

        contentWidth := this.style.Get_Pad_Left( BACKGROUND_ELEM ) +
                        contentWidth +
                        this.style.Get_Pad_Right( BACKGROUND_ELEM );

        return Float'Max( tabsWidth, contentWidth );
    end Get_Preferred_Width;

    ----------------------------------------------------------------------------

    function Get_Title( this : not null access Tab_View'Class; index : Integer ) return String is
        button : constant A_Button := A_Button(this.buttons.Get_At( index ));
    begin
        if button /= null then
            return button.Get_Text;
        end if;
        return "";
    end Get_Title;

    ----------------------------------------------------------------------------

    overriding
    function Get_Visual_State( this : access Tab_View ) return Widget_State is
    ((if not this.Is_Enabled then DISABLED_STATE else 0));

    ----------------------------------------------------------------------------

    procedure Insert( this    : not null access Tab_View'Class;
                      index   : Integer;
                      content : not null access Widget'Class;
                      title   : String;
                      icon    : Icon_Type := NO_ICON ) is
        newIndex : Integer;
        button   : A_Button;
    begin
        newIndex := Integer'Min( Integer'Max( 1, index ), this.Count + 1 );

        button := Create_Toggle_Button( this.Get_View );
        button.Set_Style( this.style.Get_Style( TAB_ELEM ) );
        button.Set_Text( title );
        button.Set_Icon( icon );
        this.buttons.Insert( newIndex, button );    -- takes ownership of 'button'
        this.stack.Insert( newIndex, content );     -- takes ownership of 'content'
        this.buttonGroup.Add( button );

        if this.Count = 1 then
            this.On_Style_Changed;          -- update the header height
        end if;
    end Insert;

    ----------------------------------------------------------------------------

    procedure On_Button_Pressed( this : not null access Tab_View'Class ) is
        button : constant A_Button := this.buttonGroup.Get_Selected;
    begin
        for i in 1..this.Count loop
            if button = A_Button(this.buttons.Get_At( i )) then
                this.stack.Set_Index( i );
                return;
            end if;
        end loop;
    end On_Button_Pressed;

    ----------------------------------------------------------------------------

    procedure On_Index_Changed( this : not null access Tab_View'Class ) is
    begin
        this.sigTabChanged.Emit;
    end On_Index_Changed;

    ----------------------------------------------------------------------------

    procedure On_Style_Changed( this : not null access Tab_View'Class ) is
    begin
        -- apply the tab style element to each button
        for i in 1..this.buttons.Count loop
            this.buttons.Get_At( i ).Set_Style( this.style.Get_Style( TAB_ELEM ) );
        end loop;

        -- apply header padding to the Row_Layout positioning
        this.buttons.Top.Attach( this.Top, this.style.Get_Pad_Top( HEADER_ELEM ) );
        this.buttons.Left.Attach( this.Left, this.style.Get_Pad_Left( HEADER_ELEM ) );
        this.buttons.Right.Attach( this.Right, this.style.Get_Pad_Right( HEADER_ELEM ) );
        this.buttons.Height.Set( A_Widget(this.buttons).Get_Preferred_Height );

        -- apply tab spacing from the header element to the Row_Layout
        this.buttons.Set_Style_Property( "background.spacing", Create( this.style.Get_Spacing( HEADER_ELEM ) ) );
        this.buttons.Set_Style_Property( "background.align", Create( Align_Type'Image( this.style.Get_Align( HEADER_ELEM ) ) ) );

        this.headerHeight := this.style.Get_Pad_Top( HEADER_ELEM ) +
                             A_Widget(this.buttons).Get_Preferred_Height +
                             this.style.Get_Pad_Bottom( HEADER_ELEM );

        -- layout the content stack
        this.stack.Top.Set_Margin( this.headerHeight + this.style.Get_Pad_Top( HEADER_ELEM ) );
        this.stack.Bottom.Set_Margin( this.style.Get_Pad_Bottom( BACKGROUND_ELEM ) );
        this.stack.Left.Set_Margin( this.style.Get_Pad_Left( BACKGROUND_ELEM ) );
        this.stack.Right.Set_Margin( this.style.Get_Pad_Right( BACKGROUND_ELEM ) );
    end On_Style_Changed;

    ----------------------------------------------------------------------------

    function Remove( this : not null access Tab_View'Class; index : Integer ) return A_Widget is
        button : A_Widget;
    begin
        if index < 1 or this.Count < index then
            return null;
        end if;

        button := this.buttons.Remove( index );
        Delete( button );

        return this.stack.Remove( index );
    end Remove;

    ----------------------------------------------------------------------------

    procedure Set_Icon( this  : not null access Tab_View'Class;
                        index : Integer;
                        icon  : Icon_Type ) is
        button : constant A_Button := A_Button(this.buttons.Get_At( index ));
    begin
        if button /= null then
            button.Set_Icon( icon );        -- size of header may have changed
            this.Update_Geometry;
        end if;
    end Set_Icon;

    ----------------------------------------------------------------------------

    procedure Set_Index( this  : not null access Tab_View'Class;
                         index : Integer ) is
        button : A_Button;
    begin
        button := A_Button(this.buttons.Get_At( index ));
        if button /= null then
            button.Set_State( True );
        end if;
    end Set_Index;

    ----------------------------------------------------------------------------

    procedure Set_Title( this  : not null access Tab_View'Class;
                         index : Integer;
                         title : String ) is
        button : constant A_Button := A_Button(this.buttons.Get_At( index ));
    begin
        if button /= null then
            button.Set_Text( title );
            this.Update_Geometry;           -- size of header may have changed
        end if;
    end Set_Title;

begin

    Register_Style( "TabView",
                    (BACKGROUND_ELEM => To_Unbounded_String( "background" ),
                     BORDER_ELEM     => To_Unbounded_String( "border" ),
                     HEADER_ELEM     => To_Unbounded_String( "header" ),
                     TAB_ELEM        => To_Unbounded_String( "tab:Button" )),
                    (BACKGROUND_ELEM => Area_Element,
                     BORDER_ELEM     => Area_Element,
                     HEADER_ELEM     => Area_Element,
                     TAB_ELEM        => Widget_Element),
                    (0 => To_Unbounded_String( "disabled" )) );

end Widgets.Tab_Views;
