--
-- Copyright (c) 2017-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Keyboard;                  use Allegro.Keyboard;
with Keyboard;                          use Keyboard;
with Styles;                            use Styles;
with Support;                           use Support;
with Widgets.Buttons.Pushes;            use Widgets.Buttons.Pushes;
with Widgets.Input_Boxes.Constraints;   use Widgets.Input_Boxes.Constraints;
with Widget_Styles.Registry;            use Widget_Styles.Registry;

package body Widgets.Spinners is

    package Connections is new Signals.Connections(Spinner);
    use Connections;

    package Key_Connections is new Signals.Keys.Connections(Spinner);
    use Key_Connections;

    ----------------------------------------------------------------------------

    -- Returns a value 'val' constrained according to the current settings.
    function Constrain_Value( this : not null access Spinner'Class; val : Float ) return Float;

    -- The down button was clicked.
    procedure On_Clicked_Down( this : not null access Spinner'Class );

    -- The up button was clicked.
    procedure On_Clicked_Up( this : not null access Spinner'Class );

    -- A new value was entered into the input box.
    procedure On_Entered_Value( this : not null access Spinner'Class );

    -- The user pressed ESC in the input box.
    procedure On_Input_Escaped( this : not null access Spinner'Class );

    -- Adjusts the child layout after a resize.
    procedure On_Resized( this : not null access Spinner'Class );

    -- Updates the child widgets' styles after a style change and then adjusts
    -- the layout.
    procedure On_Style_Changed( this : not null access Spinner'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Spinner( view : not null access Game_Views.Game_View'Class;
                             id   : String := "" ) return A_Spinner is
        this : A_Spinner := new Spinner;
    begin
        this.Construct( view, id );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Spinner;

    ----------------------------------------------------------------------------

    procedure Adjust_Layout( this : not null access Spinner'Class ) is
    begin
        this.btnUp.Set_Preferred_Width( this.geom.height );
        this.btnDown.Set_Preferred_Width( this.geom.height );
        if this.style.Get_Align( BACKGROUND_ELEM ) /= Center then
            this.input.Left.Attach( this.Left, 0.0 );
            this.input.Right.Set_Margin( 0.0 );
            this.btnUp.Bottom.Attach( this.Vertical_Center );
            this.btnDown.Top.Attach( this.btnUp.Bottom );
            this.btnDown.Left.Detach;
            this.btnDown.Right.Attach( this.Right );
        else
            this.btnUp.Bottom.Attach( this.Bottom );
            this.btnDown.Top.Attach( this.Top );
            this.btnDown.Right.Detach;
            this.btnDown.Left.Attach( this.Left );
            this.input.Left.Attach( this.btnDown.Right, this.style.Get_Spacing( BACKGROUND_ELEM ) );
            this.input.Right.Set_Margin( this.style.Get_Spacing( BACKGROUND_ELEM ) );
        end if;
    end Adjust_Layout;

    ----------------------------------------------------------------------------

    function Changed( this : not null access Spinner'Class ) return access Signal'Class is (this.sigChanged'Access);

    ----------------------------------------------------------------------------

    function Constrain_Value( this : not null access Spinner'Class; val : Float ) return Float is
        result : Float;
    begin
        result := Constrain( val, this.min, this.max );
        if this.discrete then
            -- apply the discrete value constraint, rounding to the nearest step
            result := this.min + (this.incr * Float'Rounding( (result - this.min) / this.incr ));
        end if;
        return result;
    end Constrain_Value;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Spinner;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String ) is
    begin
        Widget(this.all).Construct( view, id, "Spinner" );
        this.sigChanged.Init( this );

        this.Resized.Connect( Slot( this, On_Resized'Access ) );
        this.StyleChanged.Connect( Slot( this, On_Style_Changed'Access ) );

        -- the Up button is on the right side, either above the Down button,
        -- sharing the top half of the vertical space, or filling the vertical
        -- area.
        this.btnUp := Create_Push_Button( view );
        this.Add_Child( this.btnUp );
        this.btnUp.Top.Attach( this.Top );
        this.btnUp.Right.Attach( this.Right );
        this.btnUp.Set_Style( this.style.Get_Style( UP_ELEM ) );
        this.btnUp.Set_Focusable( False );
        this.btnUp.Pressed.Connect( Slot( this, On_Clicked_Up'Access ) );
        this.btnUp.Held.Connect( Slot( this, On_Clicked_Up'Access ) );

        -- the Down button is either on the right side underneath the Up button,
        -- sharing the bottom half of the vertical space, or filling the
        -- vertical area on the left side.
        this.btnDown := Create_Push_Button( view );
        this.Add_Child( this.btnDown );
        this.btnDown.Bottom.Attach( this.Bottom );
        this.btnDown.Set_Style( this.style.Get_Style( DOWN_ELEM ) );
        this.btnDown.Set_Focusable( False );
        this.btnDown.Pressed.Connect( Slot( this, On_Clicked_Down'Access ) );
        this.btnDown.Held.Connect( Slot( this, On_Clicked_Down'Access ) );

        -- the input box is either centered between the Down and Up buttons, or
        -- to the left of them, filling most of the horizontal space.
        this.input := Create_Input_Box( view );
        this.Add_Child( this.input );
        this.input.Top.Attach( this.Top );
        this.input.Bottom.Attach( this.Bottom );
        this.input.Right.Attach( this.btnUp.Left );
        this.input.Set_Constraint( Number_Only'Access );
        this.input.Set_Style( this.style.Get_Style( INPUT_ELEM ) );
        this.input.Accepted.Connect( Slot( this, On_Entered_Value'Access ));
        this.input.KeyPressed.Connect( Slot( this, On_Input_Escaped'Access, ALLEGRO_KEY_ESCAPE, (others => No) ) );
        this.input.Blurred.Connect( Slot( this, On_Entered_Value'Access ) );

         -- init styles of child widgets and adjust layout
        this.On_Style_Changed;
    end Construct;

    ----------------------------------------------------------------------------

    procedure Enable_Discrete( this : not null access Spinner'Class; enabled : Boolean ) is
    begin
        this.discrete := enabled;
    end Enable_Discrete;

    ----------------------------------------------------------------------------

    function Get_Min_Height( this : access Spinner ) return Float is
    begin
        if this.style.Get_Align( BACKGROUND_ELEM ) = Center then
            return Float'Max( this.btnUp.Get_Min_Height, this.btnDown.Get_Min_Height );
        end if;
        return this.btnUp.Get_Min_Height + this.btnDown.Get_Min_Width;
    end Get_Min_Height;

    ----------------------------------------------------------------------------

    function Get_Min_Width( this : access Spinner ) return Float is
    begin
        if this.style.Get_Align( BACKGROUND_ELEM ) = Center then
            return this.btnDown.Get_Min_Width + this.input.Get_Min_Width + this.btnUp.Get_Min_Width;
        end if;
        return this.input.Get_Min_Width + this.btnUp.Get_Min_Width;
    end Get_Min_Width;

    ----------------------------------------------------------------------------

    function Get_Precision( this : not null access Spinner'Class ) return Natural is (this.precision);

    ----------------------------------------------------------------------------

    function Get_Increment( this : not null access Spinner'Class ) return Float is (this.incr);

    ----------------------------------------------------------------------------

    function Get_Value( this : not null access Spinner'Class ) return Float is (this.val);

    ----------------------------------------------------------------------------

    function Is_Discrete( this : not null access Spinner'Class ) return Boolean is (this.discrete);

    ----------------------------------------------------------------------------

    procedure On_Clicked_Down( this : not null access Spinner'Class ) is
    begin
        this.Set_Value( this.Get_Value - this.Get_Increment );
    end On_Clicked_Down;

    ----------------------------------------------------------------------------

    procedure On_Clicked_Up( this : not null access Spinner'Class ) is
    begin
        this.Set_Value( this.Get_Value + this.Get_Increment );
    end On_Clicked_Up;

    ----------------------------------------------------------------------------

    procedure On_Entered_Value( this : not null access Spinner'Class ) is
        val : Float;
    begin
        if this.input.Get_Text'Length > 0 then
            val := Float'Value( this.input.Get_Text );
            this.Set_Value( this.Constrain_Value( val ) );
        end if;
    exception
        when others =>
            null;
    end On_Entered_Value;

    ----------------------------------------------------------------------------

    procedure On_Input_Escaped( this : not null access Spinner'Class ) is
    begin
        this.input.Set_Text( Image( this.val, this.precision ) );
    end On_Input_Escaped;

    ----------------------------------------------------------------------------

    procedure On_Resized( this : not null access Spinner'Class ) is
    begin
        this.Adjust_Layout;
    end On_Resized;

    ----------------------------------------------------------------------------

    procedure On_Style_Changed( this : not null access Spinner'Class ) is
    begin
        this.btnUp.Set_Style( this.style.Get_Style( UP_ELEM ) );
        this.btnDown.Set_Style( this.style.Get_Style( DOWN_ELEM ) );
        this.input.Set_Style( this.style.Get_Style( INPUT_ELEM ) );

        -- padding surrounds the client area and the scroll bars/buttons
        this.Top.innerPad := this.style.Get_Pad_Top( BACKGROUND_ELEM );
        this.Left.innerPad := this.style.Get_Pad_Left( BACKGROUND_ELEM );
        this.Right.innerPad := this.style.Get_Pad_Right( BACKGROUND_ELEM );
        this.Bottom.innerPad := this.style.Get_Pad_Bottom( BACKGROUND_ELEM );

        this.Adjust_Layout;
    end On_Style_Changed;

    ----------------------------------------------------------------------------

    procedure Set_Precision( this : not null access Spinner'Class; count : Natural ) is
    begin
        this.precision := count;
        this.Set_Value( this.val );
    end Set_Precision;

    ----------------------------------------------------------------------------

    procedure Set_Range( this : not null access Spinner'Class; min, max : Float ) is
    begin
        if min < max then
            this.min := min;
            this.max := max;
        end if;
    end Set_Range;

    ----------------------------------------------------------------------------

    procedure Set_Increment( this : not null access Spinner'Class; amount : Float ) is
    begin
        this.incr := amount;
    end Set_Increment;

    ----------------------------------------------------------------------------

    procedure Set_Value( this : not null access Spinner'Class; val : Float ) is
    begin
        if val /= this.val or else this.input.Get_Text /= Image( this.val, this.precision ) then
            this.val := this.Constrain_Value( val );
            this.input.Set_Text( Image( this.val, this.precision ) );
            this.Changed.Emit;
        end if;
    end Set_Value;

begin

    Register_Style( "Spinner",
                    (BACKGROUND_ELEM => To_Unbounded_String( "background" ),
                     INPUT_ELEM      => To_Unbounded_String( "input:InputBox" ),
                     UP_ELEM         => To_Unbounded_String( "up:Button" ),
                     DOWN_ELEM       => To_Unbounded_String( "down:Button" )),
                    (BACKGROUND_ELEM => Area_Element,
                     INPUT_ELEM      => Widget_Element,
                     UP_ELEM         => Widget_Element,
                     DOWN_ELEM       => Widget_Element),
                    (0 => To_Unbounded_String( "disabled" )) );

end Widgets.Spinners;
