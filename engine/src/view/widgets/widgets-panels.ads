--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Widgets.Panels is

    -- A Panel is a simple container for multiple widgets. Panels are useful for
    -- visually grouping objects together so that a group of them can be moved
    -- or shown/hidden simultaneously by performing the operation once on their
    -- Panel.
    --
    -- The Panel class is also extended for more specialized uses as a modal
    -- dialog and a context-sensitive popup.
    type Panel is new Widget with private;
    type A_Panel is access all Panel'Class;

    -- Creates a new Panel within 'view' with id 'id'.
    function Create_Panel( view : not null access Game_Views.Game_View'Class;
                           id   : String := "" ) return A_Panel;
    pragma Postcondition( Create_Panel'Result /= null );

    -- Adds a child widget to the Panel. The Panel will own 'child' after this
    -- call and will delete it upon destruction.
    procedure Add_Widget( this  : not null access Panel'Class;
                          child : not null access Widget'Class );

    -- Removes and deletes all child widgets within the panel.
    procedure Clear_Widgets( this : not null access Panel'Class );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Properties

    -- Reads or sets the background color of the widget.
    function BackgroundColor( this : not null access Panel'Class ) return access Animated_Color.Property'Class;

private

    BACKGROUND_ELEM : constant := 1;
    BORDER_ELEM     : constant := 2;

    DISABLED_STATE : constant Widget_State := 2**0;

    ----------------------------------------------------------------------------

    type Panel is new Widget with
        record
            propBackgroundColor : aliased Animated_Color.Property;
        end record;

    procedure Construct( this       : access Panel;
                         view       : not null access Game_Views.Game_View'Class;
                         id         : String;
                         widgetType : String );

    -- Draws the Panel's background and border.
    procedure Draw_Content( this : access Panel );

end Widgets.Panels;
