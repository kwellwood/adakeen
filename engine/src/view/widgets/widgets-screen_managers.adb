--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Debugging;                         use Debugging;
with Drawing.Primitives;                use Drawing.Primitives;
with Drawing.Tiles;                     use Drawing.Tiles;
with Game_Views;                        use Game_Views;
with Widget_Styles.Registry;            use Widget_Styles.Registry;

package body Widgets.Screen_Managers is

    package Connections is new Signals.Connections(Screen_Manager);
    use Connections;

    procedure On_Focused( this : not null access Screen_Manager'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Screen_Manager( view : not null access Game_Views.Game_View'Class;
                                    id   : String := "" ) return A_Screen_Manager is
        this : A_Screen_Manager := new Screen_Manager;
    begin
        this.Construct( view, id );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Screen_Manager;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Screen_Manager;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String ) is
    begin
        Widget(this.all).Construct( view, id, "ScreenStack" );
        this.Focused.Connect( Slot( this, On_Focused'Access ) );
    end Construct;

    ----------------------------------------------------------------------------

    procedure Add_Back( this   : not null access Screen_Manager'Class;
                        screen : not null access Game_Screen'Class ) is
        visible : Boolean;
    begin
        -- the screen will only be visible if the stack is empty or the stack
        -- does not contain any opaque screens (e.g. the last screen is visible
        -- but it's not opaque).
        visible := this.stack.Is_Empty or else
                   (not this.stack.Last_Element.Is_Opaque and this.stack.Last_Element.Is_Visible);

        screen.Visible.Set( False );
        this.stack.Append( screen );
        this.Add_Child( screen );
        screen.Send_To_Back;
        screen.Fill( this );
        screen.Visible.Set( visible );

        pragma Debug( Dbg( "Added screen " & screen.To_String & " to back", D_GUI, Info ) );
    end Add_Back;

    ----------------------------------------------------------------------------

    procedure Add_Front( this   : not null access Screen_Manager'Class;
                         screen : not null access Game_Screen'Class ) is
    begin
        pragma Debug( Dbg( "Adding screen " & screen.To_String & " to front", D_GUI, Info ) );

        -- adding the child here will make it visible and rooted to the window.
        -- it may want to focus one of its control elements.
        screen.Visible.Set( False );
        this.stack.Prepend( screen );
        this.Add_Child( screen );
        screen.Fill( this );
        screen.Visible.Set( True );

        -- if this is an opaque screen, the screens behind it can now be hidden.
        -- if the new screen didn't request focus and one of the hidden screens
        -- contained the focused widget, the screen manager will be focused.
        if screen.Is_Opaque then
            for scr of this.stack loop
                if scr /= screen then
                    exit when not scr.Is_Visible;
                    scr.Visible.Set( False );
                end if;
            end loop;
        end if;

        pragma Debug( Dbg( "Added screen " & screen.To_String & " to front", D_GUI, Info ) );
    end Add_Front;

    ----------------------------------------------------------------------------

    function Contains( this   : not null access Screen_Manager'Class;
                       screen : not null access Game_Screen'Class ) return Boolean is
    begin
        return this.stack.Contains( screen );
    end Contains;

    ----------------------------------------------------------------------------

    procedure Delete_Back( this : not null access Screen_Manager'Class ) is
        screen : A_Game_Screen := this.Pop_Back;
    begin
        if this.view /= null then
            Delete_Later( A_Widget(screen) );
        else
            Delete( A_Widget(screen) );
        end if;
    end Delete_Back;

    ----------------------------------------------------------------------------

    procedure Delete_Front( this : not null access Screen_Manager'Class ) is
        screen : A_Game_Screen := this.Pop_Front;
    begin
        if this.view /= null then
            Delete_Later( A_Widget(screen) );
        else
            Delete( A_Widget(screen) );
        end if;
    end Delete_Front;

    ----------------------------------------------------------------------------

    procedure On_Focused( this : not null access Screen_Manager'Class ) is
    begin
        -- if the screen manager receives focus, try to pass it to the front screen
        if not this.stack.Is_Empty and then this.stack.First_Element.Accepts_Focus then
            this.stack.First_Element.Request_Focus;
        end if;
    end On_Focused;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Content( this : access Screen_Manager ) is
        state : constant Widget_State := this.Get_Visual_State;
        color : Allegro_Color;
    begin
        if this.stack.Is_Empty or else
           (not this.stack.Last_Element.Is_Opaque and this.stack.Last_Element.Is_Visible)
        then
            color := this.style.Get_Color( BACKGROUND_ELEM, state );
            if color /= Transparent then
                Rectfill( 0.0, 0.0, this.viewport.width, this.viewport.height,
                          Lighten( color, this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );
            end if;
            Draw_Tile_Filled( this.style.Get_Image( BACKGROUND_ELEM, state ).Get_Tile,
                              0.0, 0.0, 0.0,
                              this.viewport.width, this.viewport.height,
                              Lighten( this.style.Get_Tint( BACKGROUND_ELEM, state ),
                                       this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );
        end if;
    end Draw_Content;

    ----------------------------------------------------------------------------

    function Get_Back( this : not null access Screen_Manager'Class ) return A_Game_Screen is
    begin
        if not this.stack.Is_Empty then
            return this.stack.Last_Element;
        end if;
        return null;
    end Get_Back;

    ----------------------------------------------------------------------------

    function Get_Front( this : not null access Screen_Manager'Class ) return A_Game_Screen is
    begin
        if not this.stack.Is_Empty then
            return this.stack.First_Element;
        end if;
        return null;
    end Get_Front;

    ----------------------------------------------------------------------------

    function Get_Length( this : not null access Screen_Manager'Class ) return Natural is (Natural(this.stack.Length));

    ----------------------------------------------------------------------------

    function Pop_Back( this : not null access Screen_Manager'Class ) return A_Game_Screen is
        back : A_Game_Screen := null;
    begin
        pragma Debug( Dbg( "Popping back screen", D_GUI, Info ) );

        if not this.stack.Is_Empty then
            back := this.stack.Last_Element;
            this.stack.Delete_Last;
            back.Visible.Set( False );
            this.Remove_Child( back );
            back.Exited.Emit;
        end if;
        return back;
    end Pop_Back;

    ----------------------------------------------------------------------------

    function Pop_Front( this : not null access Screen_Manager'Class ) return A_Game_Screen is
        front : A_Game_Screen := null;
    begin
        pragma Debug( Dbg( "Popping front screen", D_GUI, Info ) );

        if not this.stack.Is_Empty then
            front := this.stack.First_Element;
            this.stack.Delete_First;

            if front.Is_Opaque then
                -- show the screen(s) hidden behind it
                for scr of this.stack loop
                    scr.Visible.Set( True );
                    exit when scr.Is_Opaque;
                end loop;
            end if;

            front.Visible.Set( False );
            this.Remove_Child( front );
            front.Exited.Emit;
        end if;
        return front;
    end Pop_Front;

    ----------------------------------------------------------------------------

    overriding
    procedure Tick( this : access Screen_Manager; time : Tick_Time ) is
        state    : constant Widget_State := this.Get_Visual_State;
        crsr     : Screen_Stacks.Cursor;
        crsrNext : Screen_Stacks.Cursor;
        crsr2    : Screen_Stacks.Cursor;
        screen   : A_Game_Screen;
    begin
        this.style.Update_Animation( BACKGROUND_ELEM, state, time.total );

        crsr := this.stack.First;
        while Has_Element( crsr ) loop
            crsrNext := Next( crsr );
            screen := Element( crsr );

            if screen.Is_Exiting then
                pragma Debug( Dbg( "Exiting screen " & screen.To_String, D_GUI, Info ) );

                this.stack.Delete( crsr );

                if screen.Is_Visible and screen.Is_Opaque then
                    -- show the screen(s) hidden behind it
                    crsr2 := crsrNext;
                    while Has_Element( crsr2 ) loop
                        Element( crsr2 ).Visible.Set( True );
                        exit when Element( crsr2 ).Is_Opaque;
                        crsr2:= Next( crsr2 );
                    end loop;
                end if;

                screen.Visible.Set( False );
                this.Remove_Child( screen );
                screen.Exited.Emit;
                Delete( A_Widget(screen) );
            end if;

            crsr := crsrNext;
        end loop;
    end Tick;

begin

    Register_Style( "ScreenStack",
                    (BACKGROUND_ELEM => To_Unbounded_String( "background" )),
                    (BACKGROUND_ELEM => Area_Element),
                    (0 => To_Unbounded_String( "disabled" )) );

end Widgets.Screen_Managers;
