--
-- Copyright (c) 2012-2015 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Scrollables is

    pragma Preelaborate;

    -- An interface for scrollable widgets. A widget implementing this interface
    -- declares that its viewport can be scrolled if it does not display the
    -- widget's entire content region.
    type Scrollable is limited interface;
    type A_Scrollable is access all Scrollable'Class;

    -- Returns the widget's increment for scrolling in the X direction.
    function Get_Scroll_Inc_X( this : access Scrollable ) return Float is abstract;

    -- Returns the widget's increment for scrollign in the Y direction.
    function Get_Scroll_Inc_Y( this : access Scrollable ) return Float is abstract;

end Scrollables;
