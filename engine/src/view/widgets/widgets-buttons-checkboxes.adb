--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Assets.Fonts;                      use Assets.Fonts;
--with Debugging;                         use Debugging;
with Drawing;                           use Drawing;
with Drawing.Bitmaps;                   use Drawing.Bitmaps;
with Drawing.Primitives;                use Drawing.Primitives;
with Drawing.Tiles;                     use Drawing.Tiles;
with Game_Views;                        use Game_Views;
with Keyboard;                          use Keyboard;
with Styles;                            use Styles;
with Values.Strings;                    use Values.Strings;
with Widget_Styles.Registry;            use Widget_Styles.Registry;

package body Widgets.Buttons.Checkboxes is

    package Key_Connections is new Signals.Keys.Connections(Checkbox);
    use Key_Connections;

    package Mouse_Connections is new Signals.Mouse.Connections(Checkbox);
    use Mouse_Connections;

    ----------------------------------------------------------------------------

    procedure On_Key_Enter( this : not null access Checkbox'Class );

    -- Handles a left mouse button press to toggle the state of the checkbox.
    -- The other mouse buttons are not used.
    procedure On_Mouse_Pressed( this : not null access Checkbox'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Checkbox( view : not null access Game_Views.Game_View'Class;
                              id   : String := "" ) return A_Checkbox is
        this : A_Checkbox := new Checkbox;
    begin
        this.Construct( view, id, "", NO_ICON );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Checkbox;

    ----------------------------------------------------------------------------

    function Create_Checkbox( view : not null access Game_Views.Game_View'Class;
                              id   : String;
                              text : String;
                              icon : Icon_Type := NO_ICON ) return A_Checkbox is
        this : A_Checkbox := new Checkbox;
    begin
        this.Construct( view, id, text, icon );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Checkbox;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Checkbox;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String;
                         text : String;
                         icon : Icon_Type ) is
    begin
        Button(this.all).Construct( view, id, "Checkbox", text, icon );
        this.style.Set_Property( BACKGROUND_ELEM, STYLE_ALIGN, DEFAULT_STATE, Create( "left" ) );

        this.KeyTyped.Connect( Slot( this, On_Key_Enter'Access, ALLEGRO_KEY_SPACE, (others=>No) ) );
        this.KeyTyped.Connect( Slot( this, On_Key_Enter'Access, ALLEGRO_KEY_ENTER, (others=>No) ) );
        this.KeyTyped.Connect( Slot( this, On_Key_Enter'Access, ALLEGRO_KEY_PAD_ENTER, (others=>No) ) );

        this.MousePressed.Connect( Slot( this, On_Mouse_Pressed'Access, Mouse_Left ) );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Content( this : access Checkbox ) is
        state         : constant Widget_State := this.Get_Visual_State;
        text          : constant String := To_String( this.text );
        icon          : constant Icon_Type := this.style.Get_Image( ICON_ELEM, state );
        checkbox      : constant Icon_Type := this.style.Get_Image( CHECKBOX_ELEM, state );
        align         : constant Align_Type := this.style.Get_Align( BACKGROUND_ELEM, state );
        spacing       : constant Float := this.style.Get_Spacing( BACKGROUND_ELEM, state );
        color         : Allegro_Color;
        font          : A_Font;
        boxWidth      : Float := 0.0;
        boxHeight     : Float := 0.0;
        iconWidth     : Float := 0.0;
        iconHeight    : Float := 0.0;
        textWidth     : Float := 0.0;
        textHeight    : Float := 0.0;
        contentWidth  : Float := 0.0;
        contentHeight : Float := 0.0;
        contentX      : Float := 0.0;
        contentY      : Float := 0.0;
        boxX, boxY    : Float;
        iconX, iconY  : Float;
        textX, textY  : Float;
    begin
        -- - - - Calculate Positions - - - --

        -- calculate checkbox skin size --
        boxWidth := Float'Min( this.viewport.width, Float(checkbox.Get_Width) );
        boxHeight := Float'Min( this.viewport.height, Float(checkbox.Get_Height) );

        -- calculate icon size --
        iconWidth := Float'Min( this.viewport.width, (if this.style.Get_Width( ICON_ELEM, state ) > 0.0 then this.style.Get_Width( ICON_ELEM, state ) else Float(icon.Get_Width)) );
        iconHeight := Float'Min( this.viewport.height, (if this.style.Get_Height( ICON_ELEM, state ) > 0.0 then this.style.Get_Height( ICON_ELEM, state ) else Float(icon.Get_Height)) );

        -- calculate text size --
        if text'Length > 0 then
            font := this.style.Get_Font( TEXT_ELEM, state );
            textHeight := Float(font.Line_Height);
            textWidth := Float(font.Text_Length( text ));
        end if;

        -- calculate total content size --
        if iconWidth > 0.0 and textWidth > 0.0 then
            contentWidth := boxWidth + spacing + iconWidth + spacing + textWidth;
        elsif textWidth > 0.0 then
            contentWidth := boxWidth + spacing + textWidth;
        elsif iconWidth > 0.0 then
            contentWidth := boxWidth + spacing + iconWidth;
        else
            contentWidth := boxWidth;
        end if;
        contentHeight := Float'Max( boxHeight, Float'Max( iconHeight, textHeight ) );

        -- calculate alignment --
        Align_Rect( align,
                    this.viewport.width - (this.style.Get_Pad_Left( BACKGROUND_ELEM, state ) + this.style.Get_Pad_Right( BACKGROUND_ELEM, state )),
                    this.viewport.height - (this.style.Get_Pad_Top( BACKGROUND_ELEM, state ) + this.style.Get_Pad_Bottom( BACKGROUND_ELEM, state )),
                    contentWidth, contentHeight,
                    contentX, contentY );
        contentX := this.style.Get_Pad_Left( BACKGROUND_ELEM, state ) + contentX;
        contentY := this.style.Get_Pad_Top( BACKGROUND_ELEM, state ) + contentY;
        boxX := Float'Rounding( contentX );
        boxY := Float'Rounding( contentY + Align_Vertical( align, contentHeight, boxHeight ) );
        iconX := Float'Rounding( boxX + spacing );
        iconY := Float'Rounding( contentY + Align_Vertical( align, contentHeight, iconHeight ) );
        textX := contentX + contentWidth - textWidth;
        textY := contentY + Align_Vertical( align, contentHeight, textHeight );

        -- - - - Draw Content - - - --

        -- draw the background --
        color := this.style.Get_Color( BACKGROUND_ELEM, state );
        if color /= Transparent then
            Rectfill( 0.0, 0.0, this.viewport.width, this.viewport.height,
                      Lighten( color, this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );
        else
            Draw_Tile_Filled( this.style.Get_Image( BACKGROUND_ELEM, state ).Get_Tile,
                              0.0, 0.0, 0.0,
                              this.viewport.width, this.viewport.height,
                              Lighten( this.style.Get_Tint( BACKGROUND_ELEM, state ),
                                       this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );
        end if;

        -- draw the border --
        Draw_Tile_Filled( this.style.Get_Image( BORDER_ELEM, state ).Get_Tile,
                          0.0, 0.0, 0.0,
                          this.viewport.width, this.viewport.height,
                          Lighten( this.style.Get_Tint( BORDER_ELEM, state ),
                                   this.style.Get_Shade( BORDER_ELEM, state ) ) );

        -- draw the content --
        Draw_Bitmap( checkbox.Get_Bitmap, boxX, boxY );

        if icon.Get_Bitmap /= null then
            if iconWidth = Float(icon.Get_Width) and then iconHeight = Float(icon.Get_Height) then
                Draw_Bitmap( icon.Get_Bitmap, iconX, iconY );
            else
                Draw_Bitmap_Stretched( icon.Get_Bitmap, iconX, iconY, 0.0, iconWidth, iconHeight, Fit );
            end if;
        end if;

        if font /= null then
            font.Draw_String( text,
                              textX, textY,
                              Lighten( this.style.Get_Color( TEXT_ELEM, state ),
                                       this.style.Get_Shade( TEXT_ELEM, state ) ) );
        end if;
    end Draw_Content;

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Height( this : access Checkbox ) return Float is
        padTop     : constant Float := this.style.Get_Pad_Top( BACKGROUND_ELEM );
        padBottom  : constant Float := this.style.Get_Pad_Bottom( BACKGROUND_ELEM );
        boxHeight  : Float := 0.0;
        iconHeight : Float := 0.0;
        textHeight : Float := 0.0;
    begin
        boxHeight := Float(this.style.Get_Image( CHECKBOX_ELEM ).Get_Height);

        iconHeight := (if this.style.Get_Height( ICON_ELEM ) > 0.0
                       then this.style.Get_Height( ICON_ELEM )
                       else Float(this.style.Get_Image( ICON_ELEM ).Get_Height));

        if Length( this.text ) > 0 then
            textHeight := Float(this.style.Get_Font( TEXT_ELEM ).Line_Height);
        end if;

        return padTop + Float'Max( boxHeight, Float'Max( iconHeight, textHeight) ) + padBottom;
    end Get_Min_Height;

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Width( this : access Checkbox ) return Float is
        padLeft   : constant Float := this.style.Get_Pad_Left( BACKGROUND_ELEM );
        padRight  : constant Float := this.style.Get_Pad_Right( BACKGROUND_ELEM );
        spacing   : constant Float := this.style.Get_Spacing( BACKGROUND_ELEM );
        boxWidth  : Float := 0.0;
        iconWidth : Float := 0.0;
        textWidth : Float := 0.0;
    begin
        boxWidth := Float(this.style.Get_Image( CHECKBOX_ELEM ).Get_Width);

        iconWidth := (if this.style.Get_Width( ICON_ELEM ) > 0.0
                      then this.style.Get_Width( ICON_ELEM )
                      else Float(this.style.Get_Image( ICON_ELEM ).Get_Width));

        if Length( this.text ) > 0 then
            textWidth := Float(this.style.Get_Font( TEXT_ELEM ).Text_Length( To_String( this.text ) ));
        end if;

        if iconWidth > 0.0 and then textWidth > 0.0 then
            return padLeft + boxWidth + spacing + iconWidth + spacing + textWidth + padRight;
        elsif textWidth > 0.0 then
            return padLeft + boxWidth + spacing + textWidth + padRight;
        elsif iconWidth > 0.0 then
            return padLeft + boxWidth + spacing + iconWidth + padRight;
        else
            return padLeft + boxWidth + padRight;
        end if;
    end Get_Min_Width;

    ----------------------------------------------------------------------------

    procedure On_Key_Enter( this : not null access Checkbox'Class ) is
    begin
        this.Toggle_State;
    end On_Key_Enter;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Pressed( this : not null access Checkbox'Class ) is
    begin
        this.Toggle_State;
    end On_Mouse_Pressed;

begin

    Register_Style( "Checkbox",
                    (BACKGROUND_ELEM => To_Unbounded_String( "background" ),
                     BORDER_ELEM     => To_Unbounded_String( "border" ),
                     ICON_ELEM       => To_Unbounded_String( "icon" ),
                     TEXT_ELEM       => To_Unbounded_String( "text" ),
                     CHECKBOX_ELEM   => To_Unbounded_String( "checkbox")),
                    (BACKGROUND_ELEM => Area_Element,
                     BORDER_ELEM     => Area_Element,
                     ICON_ELEM       => Icon_Element,
                     TEXT_ELEM       => Text_Element,
                     CHECKBOX_ELEM   => Icon_Element),
                    (0 => To_Unbounded_String( "disabled" ),
                     1 => To_Unbounded_String( "press" ),
                     2 => To_Unbounded_String( "focus" ),
                     3 => To_Unbounded_String( "hover" )) );

end Widgets.Buttons.Checkboxes;
