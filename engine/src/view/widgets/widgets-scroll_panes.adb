--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Drawing;                           use Drawing;
with Drawing.Primitives;                use Drawing.Primitives;
with Drawing.Tiles;                     use Drawing.Tiles;
with Game_Views;                        use Game_Views;
with Icons;                             use Icons;
with Scrollables;                       use Scrollables;
with Widget_Styles.Registry;            use Widget_Styles.Registry;
with Widgets.Buttons.Pushes;            use Widgets.Buttons.Pushes;

package body Widgets.Scroll_Panes is

    DEFAULT_SCROLL_INCREMENT : constant := 16.0;

    package Connections is new Signals.Connections(Scroll_Pane);
    use Connections;

    package Mouse_Connections is new Signals.Mouse.Connections(Scroll_Pane);
    use Mouse_Connections;

    ----------------------------------------------------------------------------

    -- Handles mouse scroll wheel actions to scroll vertically, if the client
    -- doesn't handle them.
    procedure On_Mouse_Scrolled( this  : not null access Scroll_Pane'Class;
                                 mouse : Scroll_Arguments );

    -- Adjusts the child layout after a resize.
    procedure On_Resized( this : not null access Scroll_Pane'Class );

    -- Updates the child widgets' styles after a style change and then adjusts
    -- the layout.
    procedure On_Style_Changed( this : not null access Scroll_Pane'Class );

    procedure Scroll_Down( this : not null access Scroll_Pane'Class );

    procedure Scroll_Left( this : not null access Scroll_Pane'Class );

    procedure Scroll_Right( this : not null access Scroll_Pane'Class );

    procedure Scroll_Up( this : not null access Scroll_Pane'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Scroll_Pane( view : not null access Game_Views.Game_View'Class;
                                 id   : String := "" ) return A_Scroll_Pane is
        this : A_Scroll_Pane := new Scroll_Pane;
    begin
        this.Construct( view, id );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Scroll_Pane;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Scroll_Pane;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String ) is
    begin
        Widget(this.all).Construct( view, id, "ScrollPane" );
        this.MouseScrolled.Connect( Slot( this, On_Mouse_Scrolled'Access ) );
        this.Resized.Connect( Slot( this, On_Resized'Access ) );
        this.StyleChanged.Connect( Slot( this, On_Style_Changed'Access ) );

        this.btnUp := Create_Push_Button( view, this.Get_Id & ":btnUp" );
        this.Add_Child( this.btnUp );
        this.btnUp.Right.Attach( this.Right );
        this.btnUp.Top.Attach( this.Top );
        this.btnUp.Set_Style( this.style.Get_Style( UP_ELEM ) );
        this.btnUp.Set_Focusable( False );
        this.btnUp.Pressed.Connect( Slot( this, Scroll_Up'Access ) );
        this.btnUp.Held.Connect( Slot( this, Scroll_Up'Access ) );

        this.btnDown := Create_Push_Button( view, this.Get_Id & ":btnDown" );
        this.Add_Child( this.btnDown );
        this.btnDown.Right.Attach( this.Right );
        this.btnDown.Bottom.Attach( this.Bottom );
        this.btnDown.Set_Style( this.style.Get_Style( DOWN_ELEM ) );
        this.btnDown.Set_Focusable( False );
        this.btnDown.Pressed.Connect( Slot( this, Scroll_Down'Access ) );
        this.btnDown.Held.Connect( Slot( this, Scroll_Down'Access ) );

        this.btnLeft := Create_Push_Button( view, this.Get_Id & ":btnLeft" );
        this.Add_Child( this.btnLeft );
        this.btnLeft.Left.Attach( this.Left );
        this.btnLeft.Bottom.Attach( this.Bottom );
        this.btnLeft.Set_Style( this.style.Get_Style( LEFT_ELEM ) );
        this.btnLeft.Set_Focusable( False );
        this.btnLeft.Pressed.Connect( Slot( this, Scroll_Left'Access ) );
        this.btnLeft.Held.Connect( Slot( this, Scroll_Left'Access ) );

        this.btnRight := Create_Push_Button( this.Get_View, this.Get_Id & ":btnRight" );
        this.Add_Child( this.btnRight );
        this.btnRight.Right.Attach( this.Right );
        this.btnRight.Bottom.Attach( this.Bottom );
        this.btnRight.Set_Style( this.style.Get_Style( RIGHT_ELEM ) );
        this.btnRight.Set_Focusable( False );
        this.btnRight.Pressed.Connect( Slot( this, Scroll_Right'Access ) );
        this.btnRight.Held.Connect( Slot( this, Scroll_Right'Access ) );

        this.hscroll := Create_H_Scrollbar( view );
        this.Add_Child( this.hscroll );
        this.hscroll.Set_Style( this.style.Get_Style( HSCROLL_ELEM ) );
        this.hscroll.Left.Attach( this.btnLeft.Right );
        this.hscroll.Right.Attach( this.btnRight.Left );
        this.hscroll.Bottom.Attach( this.Bottom );

        -- update the horizontal scrollbar buttons to match the maximum height
        -- of all horizontal scroll controls
        this.btnLeft.Top.Attach( this.hscroll.Top );
        this.btnRight.Top.Attach( this.hscroll.Top );

        this.vscroll := Create_V_Scrollbar( view );
        this.Add_Child( this.vscroll );
        this.vscroll.Set_Style( this.style.Get_Style( VSCROLL_ELEM ) );
        this.vscroll.Top.Attach( this.btnUp.Bottom );
        this.vscroll.Bottom.Attach( this.btnDown.Top );
        this.vscroll.Right.Attach( this.Right );

        -- update the Get_Width scrollbar buttons to match the maximum width of
        -- all vertical scroll controls
        this.btnUp.Left.Attach( this.vscroll.Left );
        this.btnDown.Left.Attach( this.vscroll.Left );

        this.Set_Focusable( False );

         -- init styles of child widgets and adjust layout
        this.On_Style_Changed;
    end Construct;

    ----------------------------------------------------------------------------

    procedure Adjust_Layout( this : not null access Scroll_Pane'Class ) is
        h2, w2        : Float;
        preferredSize : Float;
    begin
        -- update the buttons' anchors when scrollbars are enabled or disabled
        if this.drawVbar then
            this.btnRight.Right.Attach( this.btnDown.Left );
        else
            this.btnRight.Right.Attach( this.Right );
        end if;
        if this.drawHbar then
            this.btnDown.Bottom.Attach( this.btnRight.Top );
        else
            this.btnDown.Bottom.Attach( this.Bottom );
        end if;

        -- hide the horizonal scrollbar and adjust left/right if not wide enough
        if this.drawHbar then
            -- if the scroll pane isn't wide enough for the left/right buttons,
            -- or if it isn't wide enough for the left/right buttons while the
            -- vertical scrolling buttons are visible...
            if (this.viewport.width < this.btnLeft.Get_Preferred_Width + this.btnRight.Get_Preferred_Width) or else
               (this.drawVbar and then this.viewport.width - this.btnDown.Get_Preferred_Width < this.btnLeft.Get_Preferred_Width + this.btnRight.Get_Preferred_Width)
            then
                -- hide the horizontal scrollbar and squeeze the left/right buttons to fit
                w2 := Float'Floor( (if this.drawVbar then this.viewport.width - this.btnDown.Get_Preferred_Width else this.viewport.width) / 2.0 );
                this.btnRight.Width.Set( w2 );
                this.btnLeft.Right.Attach( this.btnRight.Left );
                this.hscroll.Visible.Set( False );
            else
                this.btnRight.Unset_Width;         -- use preferred size hint
                this.btnLeft.Right.Detach;
                this.hscroll.Visible.Set( True );
            end if;
        end if;

        -- hide the vertical scrollbar and adjust up/down if not tall enough
        if this.drawVbar then
            -- if the scroll pane isn't tall enough for the up/down buttons, or
            -- if it isn't tall enough for the up/down buttons while the
            -- horizontal scrolling buttons are visible...
            if (this.viewport.height < this.btnUp.Get_Preferred_Height + this.btnDown.Get_Preferred_Height) or else
               (this.drawHbar and then this.viewport.height - this.btnRight.Get_Preferred_Height < this.btnUp.Get_Preferred_Height + this.btnDown.Get_Preferred_Height)
            then
                -- hide the vertical scrollbar and squeeze the up/down buttons to fit
                h2 := Float'Floor( (if this.drawHbar then this.viewport.height - this.btnRight.Get_Preferred_Height else this.Height.Get) / 2.0 );
                this.btnDown.Height.Set( h2 );
                this.btnUp.Bottom.Attach( this.btnDown.Top );
                this.vscroll.Visible.Set( False );
            else
                this.btnDown.Unset_Height;         -- use preferred size hint
                this.btnUp.Bottom.Detach;
                this.vscroll.Visible.Set( True );
            end if;
        end if;

        -- adjust the client widget's layout
        if this.client /= null then
            this.client.Fill( this );
            this.client.Right.Set_Margin( (if this.drawVbar then this.vscroll.Width.Get else 0.0) );
            this.client.Bottom.Set_Margin( (if this.drawHbar then this.hscroll.Height.Get else 0.0) );

            -- if the preferred size in one of the dimensions changes due to the
            -- resize, make sure that its viewport doesn't go off the edge of
            -- the content if it doesn't have to. Example: A widget whose
            -- preferred height is based on actual width.
            if this.vscroll.Is_Visible then
                preferredSize := this.client.Get_Preferred_Height;
                if preferredSize < (this.client.Get_Viewport.y + this.client.Get_Viewport.height) and this.client.Get_Viewport.height <= preferredSize then
                    this.client.Scroll_To( this.client.Get_Viewport.x, preferredSize - this.client.Get_Viewport.height );
                end if;
            end if;
            if this.hscroll.Is_Visible then
                preferredSize := this.client.Get_Preferred_Width;
                if preferredSize < (this.client.Get_Viewport.x + this.client.Get_Viewport.width) and this.client.Get_Viewport.width <= preferredSize then
                    this.client.Scroll_To( preferredSize - this.client.Get_Viewport.width, this.client.Get_Viewport.y );
                end if;
            end if;
        end if;
    end Adjust_Layout;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Content( this : access Scroll_Pane ) is
        state : constant Widget_State := this.Get_Visual_State;
        color : Allegro_Color;
    begin
        color := this.style.Get_Color( BACKGROUND_ELEM, state );
        if color /= Transparent then
            Rectfill( 0.0, 0.0, this.viewport.width, this.viewport.height,
                      Lighten( color, this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );
        else
            Draw_Tile_Filled( this.style.Get_Image( BACKGROUND_ELEM, state ).Get_Tile,
                              0.0, 0.0, 0.0,
                              this.viewport.width, this.viewport.height,
                              Lighten( this.style.Get_Tint( BACKGROUND_ELEM, state ),
                                       this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );
        end if;
    end Draw_Content;

    ----------------------------------------------------------------------------

    procedure Draw_Hbar( this : not null access Scroll_Pane'Class; draw : Boolean ) is
    begin
        if draw /= this.drawHbar then
            this.drawHbar := draw;
            this.btnLeft.Visible.Set( this.drawHbar );
            this.hscroll.Visible.Set( this.drawHbar );
            this.btnRight.Visible.Set( this.drawHbar );
            this.Adjust_Layout;
        end if;
    end Draw_Hbar;

    ----------------------------------------------------------------------------

    procedure Draw_Vbar( this : not null access Scroll_Pane'Class; draw : Boolean ) is
    begin
        if draw /= this.drawVbar then
            this.drawVbar := draw;
            this.btnUp.Visible.Set( this.drawVbar );
            this.vscroll.Visible.Set( this.drawVbar );
            this.btnDown.Visible.Set( this.drawVbar );
            this.Adjust_Layout;
        end if;
    end Draw_Vbar;

    ----------------------------------------------------------------------------

    function Get_Client( this : not null access Scroll_Pane'Class ) return A_Widget is (this.client);

    ----------------------------------------------------------------------------

    function Get_Inc_X( this : not null access Scroll_Pane'Class ) return Float
    is (
        if this.client /= null and then this.client.all in Scrollable'Class then
            A_Scrollable(this.client).Get_Scroll_Inc_X
        else
            DEFAULT_SCROLL_INCREMENT
    );

    ----------------------------------------------------------------------------

    function Get_Inc_Y( this : not null access Scroll_Pane'Class ) return Float
    is (
        if this.client /= null and then this.client.all in Scrollable'Class then
            A_Scrollable(this.client).Get_Scroll_Inc_Y
        else
            DEFAULT_SCROLL_INCREMENT
    );

    ----------------------------------------------------------------------------

    overriding
    function Get_Visual_State( this : access Scroll_Pane ) return Widget_State is
    ((if not this.Is_Enabled then DISABLED_STATE else 0));

    ----------------------------------------------------------------------------

    procedure On_Mouse_Scrolled( this  : not null access Scroll_Pane'Class;
                                 mouse : Scroll_Arguments ) is
    begin
        -- Was the scroll actually effective?
        -- handled := this.client /= null and then
        --            this.client.Get_Content_Height > this.client.Get_Height;

        if this.client /= null then
            -- vertical scrolling on OS X is reversed to match its paradigm of
            -- scrolling up means moving the content up, instead of the view.
#if OSX'Defined then
            this.client.Scroll( 0.0, mouse.scrollAmt * this.Get_Inc_Y );
#else
            this.client.Scroll( 0.0, (-mouse.scrollAmt) * this.Get_Inc_Y );
#end if;
        end if;
    end On_Mouse_Scrolled;

    ----------------------------------------------------------------------------

    procedure On_Resized( this : not null access Scroll_Pane'Class ) is
    begin
        this.Adjust_Layout;
    end On_Resized;

    ----------------------------------------------------------------------------

    procedure On_Style_Changed( this : not null access Scroll_Pane'Class ) is
    begin
        this.btnUp.Set_Style( this.style.Get_Style( UP_ELEM ) );
        this.btnDown.Set_Style( this.style.Get_Style( DOWN_ELEM ) );
        this.btnLeft.Set_Style( this.style.Get_Style( LEFT_ELEM ) );
        this.btnRight.Set_Style( this.style.Get_Style( RIGHT_ELEM ) );
        this.hscroll.Set_Style( this.style.Get_Style( HSCROLL_ELEM ) );
        this.vscroll.Set_Style( this.style.Get_Style( VSCROLL_ELEM ) );

        -- padding surrounds the client area and the scroll bars/buttons
        this.Top.innerPad := this.style.Get_Pad_Top( BACKGROUND_ELEM );
        this.Left.innerPad := this.style.Get_Pad_Left( BACKGROUND_ELEM );
        this.Right.innerPad := this.style.Get_Pad_Right( BACKGROUND_ELEM );
        this.Bottom.innerPad := this.style.Get_Pad_Bottom( BACKGROUND_ELEM );

        this.hscroll.Height.Set( Float'Max( this.btnLeft.Get_Preferred_Height,
                                 Float'Max( this.btnRight.Get_Preferred_Height,
                                            this.hscroll.Get_Preferred_Height ) ) );

        this.vscroll.Width.Set( Float'Max( this.btnUp.Get_Preferred_Width,
                                Float'Max( this.btnDown.Get_Preferred_Width,
                                           this.vscroll.Get_Preferred_Width ) ) );

        this.Adjust_Layout;
    end On_Style_Changed;

    ----------------------------------------------------------------------------

    function Remove_Client( this : not null access Scroll_Pane'Class ) return A_Widget is
        client : constant A_Widget := this.client;
    begin
        if this.client /= null then
            this.hscroll.Set_Client( null );
            this.vscroll.Set_Client( null );
            this.client.Visible.Set( False );
            this.Remove_Child( this.client );
            this.client := null;
            this.Adjust_Layout;
        end if;
        return client;
    end Remove_Client;

    ----------------------------------------------------------------------------

    procedure Scroll_Down( this : not null access Scroll_Pane'Class ) is
    begin
        if this.client /= null then
            this.client.Scroll( 0.0, this.Get_Inc_Y );
        end if;
    end Scroll_Down;

    ----------------------------------------------------------------------------

    procedure Scroll_Left( this : not null access Scroll_Pane'Class ) is
    begin
        if this.client /= null then
            this.client.Scroll( -this.Get_Inc_X, 0.0 );
        end if;
    end Scroll_Left;

    ----------------------------------------------------------------------------

    procedure Scroll_Right( this : not null access Scroll_Pane'Class ) is
    begin
        if this.client /= null then
            this.client.Scroll( this.Get_Inc_X, 0.0 );
        end if;
    end Scroll_Right;

    ----------------------------------------------------------------------------

    procedure Scroll_Up( this : not null access Scroll_Pane'Class ) is
    begin
        if this.client /= null then
            this.client.Scroll( 0.0, -this.Get_Inc_Y );
        end if;
    end Scroll_Up;

    ----------------------------------------------------------------------------

    procedure Set_Client( this   : not null access Scroll_Pane'Class;
                          client : access Widget'Class ) is
        oldClient : A_Widget;
    begin
        if A_Widget(client) /= this.client then
            oldClient := this.Remove_Client;
            Delete( oldClient );
            this.client := A_Widget(client);
            if this.client /= null then
                this.client.Visible.Set( True );
                this.Add_Child( this.client );
                this.hscroll.Set_Client( this.client );
                this.vscroll.Set_Client( this.client );
            end if;
            this.Adjust_Layout;
        end if;
    end Set_Client;

begin

    Register_Style( "ScrollPane",
                    (BACKGROUND_ELEM => To_Unbounded_String( "background" ),
                     UP_ELEM         => To_Unbounded_String( "up:Button" ),
                     DOWN_ELEM       => To_Unbounded_String( "down:Button" ),
                     LEFT_ELEM       => To_Unbounded_String( "left:Button" ),
                     RIGHT_ELEM      => To_Unbounded_String( "right:Button" ),
                     HSCROLL_ELEM    => To_Unbounded_String( "hbar:Scrollbar" ),
                     VSCROLL_ELEM    => To_Unbounded_String( "vbar:Scrollbar" )),
                    (BACKGROUND_ELEM => Area_Element,
                     UP_ELEM         => Widget_Element,
                     DOWN_ELEM       => Widget_Element,
                     LEFT_ELEM       => Widget_Element,
                     RIGHT_ELEM      => Widget_Element,
                     HSCROLL_ELEM    => Widget_Element,
                     VSCROLL_ELEM    => Widget_Element),
                    (0 => To_Unbounded_String( "disabled" )) );

end Widgets.Scroll_Panes;
