--
-- Copyright (c) 2016-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Widget_Style_Properties is

    procedure Clone( this : in out Property'Class; from : Property'Class ) is
    begin
        this.defined := from.defined;
        this.default := from.default;
        this.stateCount := from.stateCount;
        if from.states /= null then
            if this.states = null or else this.states'Length /= from.states'Length then
                Free( this.states );
                this.states := new Prop_Type_Array(from.states'Range);
            end if;
            this.states.all := from.states.all;
        else
            Free( this.states );
        end if;
    end Clone;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out Property'Class ) is
    begin
        Free( this.states );
    end Delete;

    ----------------------------------------------------------------------------

    function Get_Default( this : Property'Class ) return Prop_Type is (this.default);

    ----------------------------------------------------------------------------

    function Get_Value( this : Property'Class; state : Natural; val : out Prop_Type ) return Boolean is
    begin
        if state = 0 or this.states = null then
            val := this.default;
            return True;
        elsif (this.defined and 2**state) /= 0 then
            val := this.states(state);
            return True;
        end if;
        return False;
    end Get_Value;

    ----------------------------------------------------------------------------

    procedure Merge_From_Property( this : in out Property'Class; from : Property'Class ) is
    begin
        -- handle the default value specially because it's not in the .states array
        if (from.defined and (2**0)) /= 0 then
            this.default := from.default;
            this.defined := this.defined or (2**0);
        end if;

        if this.states = null and from.states /= null then
            this.states := new Prop_Type_Array(from.states'First..from.states'Last);
        end if;

        for state in 1..from.stateCount loop
            if (from.defined and 2**state) /= 0 then
                this.states(state) := from.states(state);
                this.defined := this.defined or (2**state);
            end if;
        end loop;
    end Merge_From_Property;

    ----------------------------------------------------------------------------

    procedure Set_Default( this : in out Property'Class; val : Prop_Type ) is
    begin
        if (this.defined and (2**0)) = 0 then
            this.default := val;
        end if;
    end Set_Default;

    ----------------------------------------------------------------------------

    procedure Set_State_Count( this : in out Property'Class; count : Natural ) is
    begin
        this.stateCount := count;
    end Set_State_Count;

    ----------------------------------------------------------------------------

    procedure Set_Value( this : in out Property'Class; state : Natural; val : Prop_Type ) is
    begin
        if state = 0 then
            this.default := val;
        else
            if this.states = null then
                this.states := new Prop_Type_Array(1..this.stateCount-1);
            end if;
            this.states(state) := val;
        end if;
        this.defined := this.defined or (2**state);
    end Set_Value;

end Widget_Style_Properties;
