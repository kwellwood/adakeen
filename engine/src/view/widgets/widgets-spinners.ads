--
-- Copyright (c) 2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Widgets.Buttons;                   use Widgets.Buttons;
with Widgets.Input_Boxes;               use Widgets.Input_Boxes;

package Widgets.Spinners is

    -- A Spinner widget is control widget that combines an input box with up
    -- and down increment/decrement buttons.
    type Spinner is new Widget with private;
    type A_Spinner is access all Spinner'Class;

    -- Creates a new Spinner within 'view' with id 'id'.
    function Create_Spinner( view : not null access Game_Views.Game_View'Class;
                             id   : String := "" ) return A_Spinner;
    pragma Postcondition( Create_Spinner'Result /= null );

    -- Spinner Signals
    function Changed( this : not null access Spinner'Class ) return access Signal'Class;

    -- Enables/disables the discrete value constraint. When enabled, only the
    -- min value plus multiples of the increment are allowed. Manually entered
    -- values will be rounded to the nearest step.
    procedure Enable_Discrete( this : not null access Spinner'Class; enabled : Boolean );

    -- Returns the number of digits of precision after the decimal point.
    function Get_Precision( this : not null access Spinner'Class ) return Natural;

    -- The amount that the value is incremented or decremented by the buttons.
    function Get_Increment( this : not null access Spinner'Class ) return Float;

    -- Returns the numeric value entered in the input box.
    function Get_Value( this : not null access Spinner'Class ) return Float;

    -- Returns True if the discrete value constraint is enabled.
    function Is_Discrete( this : not null access Spinner'Class ) return Boolean;

    -- Sets the number of digits of precision after the decimal point.
    procedure Set_Precision( this : not null access Spinner'Class; count : Natural );

    -- Sets the spinners's range of values. If 'min' >= 'max' then nothing will
    -- happen. The value will not be constrained to the range or to a discrete
    -- step of the range.
    procedure Set_Range( this : not null access Spinner'Class; min, max : Float );

    -- Sets the amount for incrementing and decrementing. Steps are relative
    -- to the minimum value.
    procedure Set_Increment( this : not null access Spinner'Class; amount : Float );

    -- Sets the input box's text value. The value will not be constrained.
    procedure Set_Value( this : not null access Spinner'Class; val : Float );

private

    BACKGROUND_ELEM : constant := 1;
    INPUT_ELEM      : constant := 2;
    UP_ELEM         : constant := 3;
    DOWN_ELEM       : constant := 4;

    DISABLED_STATE : constant Widget_State := 2**0;

    ----------------------------------------------------------------------------

    type Spinner is new Widget with
        record
            btnUp      : A_Button := null;
            btnDown    : A_Button := null;
            input      : A_Input_Box := null;
            val        : Float := 0.0;
            min        : Float := 0.0;
            max        : Float := 1.0;
            incr       : Float := 1.0;
            discrete   : Boolean := False;
            precision  : Natural := 0;
            sigChanged : aliased Signal;
        end record;

    -- Adjust the input ears' child layout when the viewport size changes or the
    -- style changes.
    procedure Adjust_Layout( this : not null access Spinner'Class );

    procedure Construct( this : access Spinner;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );

    function Get_Min_Height( this : access Spinner ) return Float;

    function Get_Min_Width( this : access Spinner ) return Float;

end Widgets.Spinners;
