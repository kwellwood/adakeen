--
-- Copyright (c) 2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Values.Allegro_Colors;             use Values.Allegro_Colors;
with Values.Casting;                    use Values.Casting;
with Values.Construction;               use Values.Construction;

package body Widget_Styles.Property_Accessors is

    overriding
    function Get_Owner( this : Style_Color_Accessor ) return access Base_Object'Class
        is (Base_Object(this.style.all)'Access);

    ----------------------------------------------------------------------------

    overriding
    function Read( this : Style_Color_Accessor ) return Allegro_Color
        is (Cast_Color( this.style.Get_Property( this.element, this.property, this.state ) ));

    ----------------------------------------------------------------------------

    overriding
    procedure Write( this : Style_Color_Accessor; val : Allegro_Color ) is
    begin
        this.style.Set_Property( this.element, this.property, this.state, Create( val ) );
    end Write;

    --==========================================================================

    overriding
    function Get_Owner( this : Style_Float_Accessor ) return access Base_Object'Class
        is (Base_Object(this.style.all)'Access);

    ----------------------------------------------------------------------------

    overriding
    function Read( this : Style_Float_Accessor ) return Float
        is (Cast_Float( this.style.Get_Property( this.element, this.property, this.state ) ));

    ----------------------------------------------------------------------------

    overriding
    procedure Write( this : Style_Float_Accessor; val : Float ) is
    begin
        this.style.Set_Property( this.element, this.property, this.state, Create( val ) );
    end Write;

end Widget_Styles.Property_Accessors;
