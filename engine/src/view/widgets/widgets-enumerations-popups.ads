--
-- Copyright (c) 2016-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

private package Widgets.Enumerations.Popups is

    -- An Enum_Item is a selection in an enumeration pulldown menu. Enum items
    -- can only be placed within an enum menu and they fire a Clicked signal
    -- when selected.
    type Enum_Item is new Widget with private;
    type A_Enum_Item is access all Enum_Item'Class;

    -- Creates a new Enum_Item within 'view' with 'id'. 'text' is the text of
    -- the item in the menu.
    function Create_Enum_Item( view : not null access Game_Views.Game_View'Class;
                               id   : String;
                               text : String ) return A_Enum_Item;
    pragma Postcondition( Create_Enum_Item'Result /= null );

    -- Enum_Item Signals
    function Clicked( this : not null access Enum_Item'Class ) return access Signal'Class;

    -- Returns the enum item's text.
    function Get_Text( this : not null access Enum_Item'Class ) return String;

    -- Notifies the enum item that it is selected. Only one item is selected per
    -- Enumeration widget.
    procedure Set_Selected( this : not null access Enum_Item'Class; selected : Boolean );

    ----------------------------------------------------------------------------

    -- An Enum_Popup is the popup menu of choices presented when clicking on an
    -- Enumeration widget. It's minimum size is calculated from the minimum size
    -- of its children, so its size is computed automatically. The popup widget
    -- registers itself as a listener for Enum_Items added to it, and hides
    -- automatically when one is clicked.
    type Enum_Popup is new Widget with private;
    type A_Enum_Popup is access all Enum_Popup'Class;

    -- Creates a new Enum_Popup within 'view' with id 'id'.
    function Create_Popup( view : not null access Game_Views.Game_View'Class;
                           id   : String := "" ) return A_Enum_Popup;
    pragma Postcondition( Create_Popup'Result /= null );

    -- The Clicked signal is emitted when an item in the popup menu is clicked.
    function Clicked( this : not null access Enum_Popup'Class ) return access Signal'Class;

    -- Appends an item to the menu. 'item' will be consumed by this popup.
    procedure Add_Item( this : not null access Enum_Popup'Class;
                        item : not null A_Enum_Item );

    -- Sets the item 'index' as selected, unselecting the previously selected
    -- item. Valid indexes start at 1. If the index is not valid, only the
    -- currently selected item will be unselected.
    procedure Set_Selected( this : not null access Enum_Popup'Class; index : Natural );

private

    BACKGROUND_ELEM : constant := 1;
    BORDER_ELEM     : constant := 2;
    TEXT_ELEM       : constant := 3;

    DISABLED_STATE  : constant Widget_State := 2**0;
    SELECTED_STATE  : constant Widget_State := 2**1;
    PRESS_STATE     : constant Widget_State := 2**2;
    HOVER_STATE     : constant Widget_State := 2**3;

    ----------------------------------------------------------------------------

    type Enum_Item is new Widget with
        record
            text       : Unbounded_String;
            selected   : Boolean := False;
            sigClicked : aliased Signal;
        end record;

    procedure Construct( this : access Enum_Item;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String;
                         text : String );

    -- Draws the enum item.
    procedure Draw_Content( this : access Enum_Item );

    -- Returns the widget's minimum height.
    function Get_Min_Height( this : access Enum_Item ) return Float;

    -- Returns the widget's minimum width.
    function Get_Min_Width( this : access Enum_Item ) return Float;

    function Get_Visual_State( this : access Enum_Item ) return Widget_State;

    -- Returns a string representation of the enum item for debugging purposes.
    function To_String( this : access Enum_Item ) return String;

    ----------------------------------------------------------------------------

    type Enum_Popup is new Widget with
        record
            sigClicked : aliased Signal;
        end record;

    procedure Construct( this : access Enum_Popup;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );

    procedure Draw_Content( this : access Enum_Popup );

    -- Returns the minimum height of the popup menu, calculated as a sum of the
    -- heights of the menu's items.
    function Get_Min_Height( this : access Enum_Popup ) return Float;

    -- Returns the minimum width of the popup menu, calculated as the maximum
    -- width of the menu's items.
    function Get_Min_Width( this : access Enum_Popup ) return Float;

    -- Returns the preferred height of the popup menu, calculated as a sum of
    -- the preferred heights of the menu's items.
    function Get_Preferred_Height( this : access Enum_Popup ) return Float;

    -- Returns the preferred width of the popup menu, calculated as the maximum
    -- preferred width of the menu's items.
    function Get_Preferred_Width( this : access Enum_Popup ) return Float;

end Widgets.Enumerations.Popups;
