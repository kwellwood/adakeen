--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Assets.Libraries;                  use Assets.Libraries;
with Scene_Entities;                    use Scene_Entities;

limited with Game_Views;

package Widgets.Sprites is

    -- A Sprite is a visible representation of an Entity. It is a partially
    -- transparent image, drawn as a tile from a tile library, that is added to
    -- a Scene widget.
    type Sprite is new Widget with private;
    type A_Sprite is access all Sprite'Class;

    -- Creates a new Sprite. 'eid' and 'name' uniquely identify the entity it
    -- represents, within the world. 'template' is the name of the entity
    -- template that created the entity.
    --
    -- 'initialState' describes the entity's public state at creation. The
    -- "Location" and "Visible" fields contain position and visual info.
    function Create_Sprite( view    : not null access Game_Views.Game_View'Class;
                            entity  : not null A_Entity;
                            libName : String;
                            frame   : Natural ) return A_Sprite;

    function FrameChanged( this : not null access Sprite'Class ) return access Signal'Class;

    -- Sets the tile id of the Sprite's current frame.
    procedure Set_Frame( this : not null access Sprite'Class; frame : Natural );

private

    FRAME_ELEM  : constant := 1;
    BORDER_ELEM : constant := 2;

    DISABLED_STATE  : constant Widget_State := 2**0;
    RESIZABLE_STATE : constant Widget_State := 2**1;
    HOVER_STATE     : constant Widget_State := 2**2;
    SELECTED_STATE  : constant Widget_State := 2**3;

    ----------------------------------------------------------------------------

    type Sprite is new Widget with
        record
            entity  : A_Entity;
            lib     : Library_Ptr;
            frame   : Natural := 0;
            offsetX,
            offsetY : Float := 0.0;

            sigFrameChanged : aliased Signal;
        end record;

    procedure Construct( this    : access Sprite;
                         view    : not null access Game_Views.Game_View'Class;
                         entity  : not null A_Entity;
                         libName : String;
                         frame   : Natural );

    procedure Draw_Content( this : access Sprite );

    function Get_Frame_Height( this : not null access Sprite'Class ) return Natural;

    function Get_Frame_Width( this : not null access Sprite'Class ) return Natural;

    function Get_Min_Height( this : access Sprite ) return Float;

    function Get_Min_Width( this : access Sprite ) return Float;

    function Get_Visual_State( this : access Sprite ) return Widget_State;

end Widgets.Sprites;
