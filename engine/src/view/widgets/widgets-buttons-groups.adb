--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;

package body Widgets.Buttons.Groups is

    package Connections is new Signals.Connections(Button_Group);
    use Connections;

    procedure Deleted_Button( this : not null access Button_Group'Class );

    procedure Pressed_Button( this : not null access Button_Group'Class );

    procedure Released_Button( this : not null access Button_Group'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Button_Group return A_Button_Group is
        this : A_Button_Group := new Button_Group;
    begin
        this.Construct;
        return this;
    exception
        when others =>
            Delete( this );
            raise;
    end Create_Button_Group;

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access Button_Group ) is
    begin
        Limited_Object(this.all).Construct;
        this.sigCleared.Init( this );
        this.sigSet.Init( this );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Button_Group ) is
    begin
        this.selected := null;         -- avoid emitting a 'Cleared' signal
        this.prevSelected := null;     --
        this.Clear;
        Limited_Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Cleared( this : not null access Button_Group'Class ) return access Signal'Class is (this.sigCleared'Access);

    function Set( this : not null access Button_Group'Class ) return access Signal'Class is (this.sigSet'Access);

    ----------------------------------------------------------------------------

    procedure Add( this   : not null access Button_Group'Class;
                   button : not null A_Button ) is
    begin
        -- check if the button needs to become selected
        if this.selected = null then
            if this.keepSelected then
                button.Set_State( True );
            end if;
        end if;

        -- check if the button is replacing the current selection
        if button.Get_State then
            this.prevSelected := this.selected;
            this.selected := button;
        end if;

        button.Pressed.Connect( Slot( this, Pressed_Button'Access ) );
        button.Released.Connect( Slot( this, Released_Button'Access ) );
        button.Deleted.Connect( Slot( this, Deleted_Button'Access ) );
        this.buttons.Append( button );

        if this.selected = button then
            -- unpress the previous selection, if any
            if this.prevSelected /= null then
                this.prevSelected.Set_State( False );
            end if;

            -- emit a selection changed signal
            this.Set.Emit;
        end if;
    end Add;

    ----------------------------------------------------------------------------

    procedure Clear( this : not null access Button_Group'Class ) is
    begin
        for button of this.buttons loop
            button.Pressed.Disconnect( Slot( this, Pressed_Button'Access ) );
            button.Released.Disconnect( Slot( this, Released_Button'Access ) );
            button.Deleted.Disconnect( Slot( this, Deleted_Button'Access ) );
        end loop;
        this.buttons.Clear;
        this.prevSelected := this.selected;
        this.selected := null;
        if this.selected /= this.prevSelected then
            this.Cleared.Emit;
        end if;
    end Clear;

    ----------------------------------------------------------------------------

    function Get_Selected( this : not null access Button_Group'Class ) return A_Button is (this.selected);

    ----------------------------------------------------------------------------

    procedure Deleted_Button( this : not null access Button_Group'Class ) is
        source : constant A_Button := A_Button(this.Signaller);
        pos    : Button_Collection.Cursor := this.buttons.Find( source );
    begin
        if Has_Element( pos ) then
            this.buttons.Delete( pos );

            if this.prevSelected = source then
                this.prevSelected := null;
            end if;

            -- if it was the selected button, unselect it and potentially select
            -- a different one
            if this.selected = source then
                this.selected := null;
                this.prevSelected := null;
                if this.keepSelected and not this.buttons.Is_Empty then
                    this.selected := this.buttons.First_Element;
                    this.buttons.First_Element.Set_State( True );
                    this.Set.Emit;
                else
                    this.Cleared.Emit;
                end if;
            end if;
        end if;
    end Deleted_Button;

    ----------------------------------------------------------------------------

    procedure Pressed_Button( this : not null access Button_Group'Class ) is
    begin
        if A_Button(this.Signaller) /= this.selected then
            this.prevSelected := this.selected;
            this.selected := A_Button(this.Signaller);
            if this.prevSelected /= null then
                this.prevSelected.Set_State( False );
            end if;
            this.Set.Emit;
        end if;
    end Pressed_Button;

    ----------------------------------------------------------------------------

    procedure Released_Button( this : not null access Button_Group'Class ) is
    begin
        if A_Button(this.Signaller) = this.selected then
            if this.keepSelected then
                this.selected.Set_State( True );
            else
                this.prevSelected := this.selected;
                this.selected := null;
                this.Cleared.Emit;
            end if;
        end if;
    end Released_Button;

    ----------------------------------------------------------------------------

    procedure Set_Keep_Selected( this : not null access Button_Group'Class;
                                 keep : Boolean ) is
    begin
        this.keepSelected := keep;
    end Set_Keep_Selected;

    ----------------------------------------------------------------------------

    overriding
    function To_String( this : access Button_Group ) return String is

        function Get_Button_Names return String is
            names : Unbounded_String;
        begin
            for button of this.buttons loop
                if Length( names ) > 0 then
                    names := names & ", ";
                end if;
                names := names & "'" & button.Get_Text & "'";
            end loop;
            return To_String( names );
        end Get_Button_Names;

    begin
        if not this.buttons.Is_Empty then
            return "<" & this.Get_Class_Name & " : " & Get_Button_Names & ">";
        else
            return "<" & this.Get_Class_Name & " : [Empty]>";
        end if;
    end To_String;

    ----------------------------------------------------------------------------

    procedure Unset( this  : not null access Button_Group'Class;
                     force : Boolean := False ) is
        keepSelected : constant Boolean := this.keepSelected;
    begin
        if this.selected /= null then
            if force then
                this.keepSelected := False;
            end if;
            this.selected.Set_State( False );   -- emits 'Cleared' if button was pressed
            this.keepSelected := keepSelected;
        end if;
    end Unset;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Button_Group ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

end Widgets.Buttons.Groups;
