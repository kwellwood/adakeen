--
-- Copyright (c) 2016-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Indefinite_Ordered_Maps;
with Ada.Unchecked_Deallocation;
with Values.Maps;                       use Values.Maps;

package Widget_Styles.Registry is

    type Style_Definition is new Limited_Object with private;
    type A_Style_Definition is access all Style_Definition'Class;

    -- Returns the visual element of the widget's style named 'elemName' in
    -- 'element'. Returns True on success, or False if 'elemName' is invalid.
    -- When False is returned, the value of 'element' is undefined.
    function Get_Element( this     : not null access Style_Definition'Class;
                          elemName : String;
                          element  : out Element_Index ) return Boolean;

    -- Returns the visual state named 'stateName' in 'state'. Returns True on
    -- success, or False if 'elemName' is invalid. When False is returned, the
    -- value of 'state' is undefined.
    function Get_State( this      : not null access Style_Definition'Class;
                        stateName : String;
                        state     : out Widget_State ) return Boolean;

    -- Returns a pointer to a style object by name 'name'. If the named style
    -- doesn't exist yet, an empty one will be created.
    --
    -- This does not return a copy.
    function Get_Style_Ref( this : not null access Style_Definition'Class;
                            name : String ) return A_Widget_Style;
    pragma Postcondition( Get_Style_Ref'Result /= null );

    -- If element 'element' is a sub-widget, its widgetType be returned.
    -- Otherwise, an empty string will be returned.
    function Get_Sub_Style_Type( this    : not null access Style_Definition'Class;
                                 element : Element_Index ) return String;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type String_Array is array (Integer range <>) of Unbounded_String;

    type Element_Type_Array is array (Integer range <>) of Element_Type;

    -- Copies the style named 'fromName' for all widget types to 'toName' for
    -- all widget types. This allows one named style to extend another named
    -- style. Any conflicts will be overridden by style 'fromName'.
    procedure Copy_Named_Styles( fromName, toName : String );

    -- Returns a reference to the style definition for widgets of type
    -- 'widgetType'. Null will be returned if 'widgetType' has not been
    -- registered.
    function Get_Definition( widgetType : String ) return A_Style_Definition;

    -- Returns a copy of the style named 'styleName' for widgets of type
    -- 'widgetType'. Null will only be returned if 'widgetType' has not been
    -- registered.
    function Get_Style( widgetType : String; styleName : String ) return A_Widget_Style;

    -- Loads a widget style sheet from 'filename'. This will populate all style
    -- objects in the registry so a widget can retrieve its style information.
    -- The file will be located according to the standard resource algorithm in
    -- the "resouces" resource group. Any other files imported by the file will
    -- also be loaded. Raises Parse_Error if an error occurs loading or parsing
    -- the file.
    procedure Load_Style_Sheet( filename : String );

    -- Registers a style definition for a widget type at elaboration time. When
    -- a widget of 'widgetType' is constructed, an appropriate Widget_Style
    -- object will be constructed using the definition below.
    procedure Register_Style( widgetType   : String;
                              elementNames : String_Array;
                              elementTypes : Element_Type_Array;
                              stateNames   : String_Array );
    pragma Precondition( elementNames'Length = elementTypes'Length );

private

    package Style_Maps is new Ada.Containers.Indefinite_Ordered_Maps( String, A_Widget_Style, "<", "=" );
    use Style_Maps;

    type A_String_Array is access all String_Array;

    procedure Free is new Ada.Unchecked_Deallocation( String_Array, A_String_Array );

    type Element_Definition is
        record
            name       : Unbounded_String;
            elemType   : Element_Type;
            widgetType : Unbounded_String;      -- only for Widget_Elements
        end record;

    type Element_Definition_Array is array (Integer range <>) of Element_Definition;
    type A_Element_Definition_Array is access all Element_Definition_Array;

    procedure Free is new Ada.Unchecked_Deallocation( Element_Definition_Array, A_Element_Definition_Array );

    type Style_Definition is new Limited_Object with
        record
            widgetType : Unbounded_String;
            elements   : A_Element_Definition_Array;
            stateNames : A_String_Array;
            instances  : Style_Maps.Map;
        end record;

    procedure Construct( this         : access Style_Definition;
                         widgetType   : String;
                         elementNames : String_Array;
                         elementTypes : Element_Type_Array;
                         stateNames   : String_Array );
    pragma Precondition( elementNames'Length = elementTypes'Length );

    procedure Delete( this : in out Style_Definition );

    function Create_Style( this : not null access Style_Definition'Class;
                           name : String ) return A_Widget_Style;

end Widget_Styles.Registry;
