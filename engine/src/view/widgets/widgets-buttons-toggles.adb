--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Keyboard;                  use Allegro.Keyboard;
--with Debugging;                         use Debugging;
with Keyboard;                          use Keyboard;

package body Widgets.Buttons.Toggles is

    package Key_Connections is new Signals.Keys.Connections(Toggle_Button);
    use Key_Connections;

    package Mouse_Connections is new Signals.Mouse.Connections(Toggle_Button);
    use Mouse_Connections;

    procedure On_Key_Enter( this : not null access Toggle_Button'Class );

    -- Handles a left mouse button press to toggle the state of the button.
    procedure On_Mouse_Pressed( this : not null access Toggle_Button'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Toggle_Button( view : not null access Game_Views.Game_View'Class;
                                   id   : String := "" ) return A_Button
        is (Create_Toggle_Button( view, id, "", NO_ICON ));

    ----------------------------------------------------------------------------

    function Create_Toggle_Button( view : not null access Game_Views.Game_View'Class;
                                   id   : String;
                                   text : String;
                                   icon : Icon_Type := NO_ICON ) return A_Button is
        this : A_Toggle_Button := new Toggle_Button;
    begin
        this.Construct( view, id, text, icon );
        return A_Button(this);
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Toggle_Button;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Toggle_Button;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String;
                         text : String;
                         icon : Icon_Type ) is
    begin
        Button(this.all).Construct( view, id, "Button", text, icon );

        this.KeyPressed.Connect( Slot( this, On_Key_Enter'Access, ALLEGRO_KEY_ENTER, (others=>No), priority => Low ) );
        this.KeyPressed.Connect( Slot( this, On_Key_Enter'Access, ALLEGRO_KEY_PAD_ENTER, (others=>No), priority => Low ) );
        this.KeyPressed.Connect( Slot( this, On_Key_Enter'Access, ALLEGRO_KEY_SPACE, (others=>No), priority => Low ) );

        this.MousePressed.Connect( Slot( this, On_Mouse_Pressed'Access, Mouse_Left, priority => Low ) );
    end Construct;

    ----------------------------------------------------------------------------

    procedure On_Key_Enter( this : not null access Toggle_Button'Class ) is
    begin
        this.Toggle_State;
    end On_Key_Enter;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Pressed( this : not null access Toggle_Button'Class ) is
    begin
        A_Button(this).Toggle_State;
    end On_Mouse_Pressed;

end Widgets.Buttons.Toggles;
