--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Bitmaps;                   use Allegro.Bitmaps;
--with Debugging;                         use Debugging;
with Drawing;                           use Drawing;
with Drawing.Bitmaps;                   use Drawing.Bitmaps;
with Drawing.Tiles;                     use Drawing.Tiles;
with Game_Views;                        use Game_Views;
with Lighting_Systems;                  use Lighting_Systems;
with Tiles;                             use Tiles;
with Widget_Styles.Registry;            use Widget_Styles.Registry;
with Widgets.Scenes;                    use Widgets.Scenes;

package body Widgets.Sprites is

    package Connections is new Signals.Connections(Sprite);
    use Connections;

    ----------------------------------------------------------------------------

    -- Called when the widget's entity is resized or moved to reposition the
    -- widget within the scene.
    procedure On_Entity_Location_Changed( this : not null access Sprite'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Sprite( view    : not null access Game_Views.Game_View'Class;
                            entity  : not null A_Entity;
                            libName : String;
                            frame   : Natural ) return A_Sprite is
        this : constant A_Sprite := new Sprite;
    begin
        this.Construct( view, entity, libName, frame );
        return this;
    end Create_Sprite;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this    : access Sprite;
                         view    : not null access Game_Views.Game_View'Class;
                         entity  : not null A_Entity;
                         libName : String;
                         frame   : Natural ) is
    begin
        Widget(this.all).Construct( view, "", "Sprite" );
        this.sigFrameChanged.Init( this );

        this.entity := entity;
        this.lib := Load_Library( libName, bitmaps => True );
        this.Set_Frame( frame );           -- size and position the sprite

        -- the Sprite will be the first handler of these signals
        this.entity.Moved.Connect( Slot( this, On_Entity_Location_Changed'Access ) );
        this.entity.Resized.Connect( Slot( this, On_Entity_Location_Changed'Access ) );
    end Construct;

    ----------------------------------------------------------------------------

    function FrameChanged( this : not null access Sprite'Class ) return access Signal'Class is (this.sigFrameChanged'Access);

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Content( this : access Sprite ) is
        tile  : A_Tile;
        tile2 : A_Tile;
    begin
        tile := this.lib.Get.Get_Tile( this.frame );

        pragma Assert( this.parent.all in Scene'Class, "Sprite can only be added to Scene" );
        case A_Scene(this.parent).Get_Render_Pass is

            when Render_Color |
                 Render_Shading =>

                -- draw the tile, filling the content area using its native fill mode
                Draw_Tile_Filled( tile,
                                  0.0, 0.0, 0.0,
                                  this.viewport.width, this.viewport.height );

                -- Display a box showing the physical edges of the sprite's entity
                --Rect_XYWH( (this.viewport.width - this.entity.Get_Entity_Width) / 2.0 - this.offsetX,
                --           (this.viewport.height - this.entity.Get_Entity_Height) / 2.0 - this.offsetY,
                --           this.entity.Get_Entity_Width, this.entity.Get_Entity_Height,
                --           Lighten( this.style.Get_Color( BORDER_ELEM, state ), this.style.Get_Shade( BORDER_ELEM, state ) ) );

            when Render_Light =>

                if tile /= null then

                    -- draw the sprite's emissive light
                    if tile.Get_Light > 0.0 then
                        -- emissive tiles don't support any fillmode except Center
                        -- (because Draw_Tile_Filled accepts a Tile, not a bitmap).
                        Draw_Bitmap( tile.Get_Mask,
                                     Float(tile.Get_LightMapX), Float(tile.Get_LightMapY), 0.0,
                                     Make_Grey( tile.Get_Light, 1.0 ) );
                    elsif tile.Get_LightMap > 0 then
                        tile2 := this.lib.Get.Get_Tile( tile.Get_LightMap );
                        if tile2 /= null then
                            -- draw the emissive tile, filling the content area using its native fill mode
                            Draw_Tile_Filled( tile2,
                                              Float(tile.Get_LightMapX), Float(tile.Get_LightMapY), 0.0,
                                              this.viewport.width,
                                              this.viewport.height );
                        end if;
                    end if;

                end if;

        end case;
    end Draw_Content;

    ----------------------------------------------------------------------------

    function Get_Frame_Height( this : not null access Sprite'Class ) return Natural is
        bmp : A_Allegro_Bitmap;
    begin
        bmp := this.lib.Get.Get_Tile( this.frame ).Get_Bitmap;
        return (if bmp /= null then Al_Get_Bitmap_Height( bmp ) else Integer(Float'Floor( this.entity.Get_Entity_Height )));
    end Get_Frame_Height;

    ----------------------------------------------------------------------------

    function Get_Frame_Width( this : not null access Sprite'Class ) return Natural is
        bmp : A_Allegro_Bitmap;
    begin
        bmp := this.lib.Get.Get_Tile( this.frame ).Get_Bitmap;
        return (if bmp /= null then Al_Get_Bitmap_Width( bmp ) else Integer(Float'Floor( this.entity.Get_Entity_Width )));
    end Get_Frame_Width;

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Height( this : access Sprite ) return Float
    is (Float'Max( Float(this.Get_Frame_Height), this.entity.Get_Entity_Height ));

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Width( this : access Sprite ) return Float
    is (Float'Max( Float(this.Get_Frame_Width), this.entity.Get_Entity_Width ));

    ----------------------------------------------------------------------------

    overriding
    function Get_Visual_State( this : access Sprite ) return Widget_State is
    (
       (if not this.Is_Enabled         then DISABLED_STATE  else 0)
        or (if this.entity.Is_Selected then SELECTED_STATE  else 0)
        or (if this.hover              then HOVER_STATE     else 0)
    );

    ----------------------------------------------------------------------------

    procedure On_Entity_Location_Changed( this : not null access Sprite'Class ) is
        newWidth  : constant Float := Float'Max( this.Get_Min_Width, this.entity.Get_Entity_Width );
        newHeight : constant Float := Float'Max( this.Get_Min_Height, this.entity.Get_Entity_Height );
    begin
        this.X.Set( this.entity.Get_Render_X - (newWidth / 2.0) + this.offsetX );
        this.Y.Set( this.entity.Get_Render_Y - (newHeight / 2.0) + this.offsetY );
        this.Width.Set( newWidth );
        this.Height.Set( newHeight );
    end On_Entity_Location_Changed;

    ----------------------------------------------------------------------------

    procedure Set_Frame( this : not null access Sprite'Class; frame : Natural ) is
        newWidth,
        newHeight : Float;
        tile      : A_Tile;
    begin
        if frame /= this.frame then
            this.frame := frame;

            tile := this.lib.Get.Get_Tile( this.frame );
            if tile /= null then
                this.offsetX := tile.Get_OffsetX;
                this.offsetY := tile.Get_OffsetY;
            end if;

            newWidth := Float'Max( this.Get_Min_Width, this.entity.Get_Entity_Width );
            newHeight := Float'Max( this.Get_Min_Height, this.entity.Get_Entity_Height );
            this.X.Set( this.entity.Get_Render_X - (newWidth / 2.0) + this.offsetX );
            this.Y.Set( this.entity.Get_Render_Y - (newHeight / 2.0) + this.offsetY );
            this.Width.Set( newWidth );
            this.Height.Set( newHeight );

            this.FrameChanged.Emit;
        end if;
    end Set_Frame;

begin

    Register_Style( "Sprite",
                    (FRAME_ELEM  => To_Unbounded_String( "frame" ),
                     BORDER_ELEM => To_Unbounded_String( "border" )),
                    (FRAME_ELEM  => Area_Element,
                     BORDER_ELEM => Area_Element),
                    (0 => To_Unbounded_String( "disabled" ),
                     1 => To_Unbounded_String( "resizable" ),
                     2 => To_Unbounded_String( "selected" ),
                     3 => To_Unbounded_String( "hover" )) );

end Widgets.Sprites;
