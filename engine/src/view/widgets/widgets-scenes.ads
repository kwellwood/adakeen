--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Ordered_Maps;
with Ada.Containers.Ordered_Sets;
with Ada.Unchecked_Deallocation;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Assets.Libraries;                  use Assets.Libraries;
with Center_Arrays;
with Lighting_Systems;                  use Lighting_Systems;
with Maps;                              use Maps;
with Maps.Info_Caches;                  use Maps.Info_Caches;
with Object_Ids;                        use Object_Ids;
with Particles.Systems;                 use Particles.Systems;
with Rendering_Options;                 use Rendering_Options;
with Scene_Entities;                    use Scene_Entities;
with Values.Tagged_Ids;                 use Values.Tagged_Ids;

limited with Widgets.Scenes.Cameras;

package Widgets.Scenes is

    -- A Scene displays a graphical view of a game world. It contains layered
    -- scenery, entities, lights and particles. Each scene entity corresponds to
    -- a visible entity in the game world. Each light corresponds with a light
    -- component attached to a game entity. Particles exist and are simulated
    -- solely in the scene. The Scene's viewport shows only a portion of the
    -- world at a time, but its view can be zoomed or scrolled, manually or by
    -- following an entity.
    type Scene is abstract new Widget and Animated with private;
    type A_Scene is access all Scene'Class;

    -- Scene Signals
    function MapChanged( this : not null access Scene'Class ) return access Signal'Class;
    function WorldPropertyChanged( this : not null access Scene'Class ) return access String_Signal'Class;

    -- Adds a child widget to the Scene. The Scene will own 'child' after this
    -- call and will delete it upon destruction. This procedure is not for
    -- adding widgets for rendering entities. It is only for adding GUI elements
    -- and controls.
    procedure Add_Widget( this  : not null access Scene'Class;
                          child : not null access Widget'Class );

    -- Removes a child widget from the Scene. The caller will receive ownership
    -- of 'child'. This procedure is not for removing widgets rendering entities,
    -- it is for removing GUI elements and controls only.
    procedure Remove_Widget( this  : not null access Scene'Class;
                             child : not null access Widget'Class );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Drawing Options

    -- Enables one or more rendering options in 'opt', adding them to the
    -- currently enabled options if 'enabled' is True. Otherwise, the options
    -- will be disabled; removed from the currently enabled options.
    procedure Enable_Option( this    : not null access Scene'Class;
                             opt     : Render_Options;
                             enabled : Boolean := True );

    -- Returns True if rendering option 'opt' is enabled, or False if disabled.
    -- If multiple options are combined in 'opt', then True will only be returned
    -- if ALL of them are enabled.
    function Is_Enabled( this : not null access Scene'Class; opt : Render_Options ) return Boolean;

    -- Returns all the currently set rendering options.
    function Get_Render_Options( this : not null access Scene'Class ) return Render_Options;

    -- Return the current rendering pass during Scene drawing.
    function Get_Render_Pass( this : not null access Scene'Class ) return Render_Pass;

    -- Returns True if layer 'layer' is currently visible.
    function Is_Layer_Visible( this  : not null access Scene'Class;
                               layer : Integer ) return Boolean;

    -- Sets the secondary color tint used when drawing layer 'layer'. It will be
    -- multiplied with the layer's native tint. This layer property is not
    -- persisted.
    procedure Set_Layer_Secondary_Tint( this  : not null access Scene'Class;
                                        layer : Integer;
                                        tint  : Allegro_Color );

    -- Sets the secondary visibility for layer 'layer'. It will be AND-ed with
    -- the layer's primary visibility, so if either Set_Layer_Visible() or
    -- Set_Layer_Secondary_Visible() is used to make it invisible, it will not
    -- be drawn. This layer property is not persisted.
    procedure Set_Layer_Secondary_Visible( this    : not null access Scene'Class;
                                           layer   : Integer;
                                           visible : Boolean );

    -- Sets the visibility for a layer. This layer property is not persisted.
    procedure Set_Layer_Visible( this    : not null access Scene'Class;
                                 layer   : Integer;
                                 visible : Boolean );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- World Map

    -- Returns a pointer to the currently loaded world's map. DO NOT modify it.
    function Get_Map( this : not null access Scene'Class ) return A_Map;

    -- Returns the index of the background layer, or 0 if no no background layers.
    function Get_Background_Layer( this : not null access Scene'Class ) return Integer;

    -- Returns the index of the foreground layer, or 0 if no no foreground layers.
    function Get_Foreground_Layer( this : not null access Scene'Class ) return Integer;

    -- Returns the number of layers in the map, or 0 if none is loaded.
    function Get_Layer_Count( this : not null access Scene'Class ) return Natural;

    -- Returns the data from layer 'layer' in a new Map at layer 0, or null if
    -- 'layer' is undefined. The caller receives ownership of the new map.
    function Get_Layer_Data( this  : not null access Scene'Class;
                             layer : Integer ) return A_Map;

    -- Returns a reference to the properties of layer 'layer'. (This is not a
    -- copy; DO NOT modify it). Returns null if 'layer' is undefined.
    function Get_Layer_Properties( this  : not null access Scene'Class;
                                   layer : Integer ) return Map_Value;

    -- Returns the property 'name' of map layer 'layer', if it has been defined.
    -- Returns Null if the property has not been set or no world is loaded.
    function Get_Layer_Property( this  : not null access Scene'Class;
                                 layer : Integer;
                                 name  : String ) return Value;

    -- Returns the immutable type of layer 'layer', or Layer_Tiles if 'layer' is
    -- undefined.
    function Get_Layer_Type( this  : not null access Scene'Class;
                             layer : Integer ) return Layer_Type;

    -- Returns the Z depth of layer 'layer' relative to the Scene widget's Z.
    function Get_Layer_Z( this  : not null access Scene'Class;
                          layer : Integer ) return Float;

    -- Returns the name of the tile library used by the current world, or an
    -- empty string if no world is loaded.
    function Get_Library( this : not null access Scene'Class ) return String;

    -- Returns the height of the scene's map in tiles.
    function Get_Height_Tiles( this : not null access Scene'Class ) return Natural;

    -- Returns the width of the scene's map in tiles.
    function Get_Width_Tiles( this : not null access Scene'Class ) return Natural;

    -- Returns the width of a tile in world coordinates. Returns zero if no map
    -- is loaded.
    function Get_Tile_Width( this : not null access Scene'Class ) return Natural;

    -- Returns the world property 'name', if it has been defined for the current
    -- world. Returns Null if the property has not been set or no world is
    -- loaded.
    function Get_World_Property( this : not null access Scene'Class;
                                 name : String ) return Value;

    -- Returns True if a map is loaded in the scene.
    function Is_Map_Loaded( this : not null access Scene'Class ) return Boolean;

    -- Unloads the current world from the Scene, if one is loaded. This does not
    -- affect game state.
    procedure Unload_World( this : not null access Scene'Class );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Entities

    -- Returns a reference to an entity. null will be returned if 'id' is not
    -- recognized.
    function Find_Entity( this : not null access Scene'Class;
                          id   : Entity_Id ) return A_Entity;

    -- Iterates over all Entities.
    procedure Iterate_Entities( this    : not null access Scene'Class;
                                examine : access procedure( entity : A_Entity ) );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Camera

    procedure Set_Focus( this   : not null access Scene'Class;
                         x, y   : Float;
                         center : Boolean := False );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Returns a pointer to the scene's Particle_System.
    function Get_Particle_System( this : not null access Scene'Class ) return A_Particle_System;

    ----------------------------------------------------------------------------

    function Lt_Id( l, r : A_Entity ) return Boolean is (l.Get_Entity_Id  < r.Get_Entity_Id);

    -- Provides an ordered Set of entity references.
    package Entity_Sets is new Ada.Containers.Ordered_Sets( A_Entity, Lt_Id, "=" );

    SCENE_DEFAULTS : constant Render_Options := SHOW_LAYER_TINT
                                             or SHOW_SPRITES
                                             or SHOW_PARTICLES
                                             or SHOW_LIGHTING;

private

    pragma Inline( Get_Background_Layer );
    pragma Inline( Get_Foreground_Layer );
    pragma Inline( Get_Layer_Count );
    pragma Inline( Get_Layer_Data );
    pragma Inline( Get_Layer_Properties );
    pragma Inline( Get_Layer_Property );
    pragma Inline( Get_Layer_Type );
    pragma Inline( Get_Layer_Z );
    pragma Inline( Get_Library );
    pragma Inline( Get_Map );
    pragma Inline( Get_Particle_System );
    pragma Inline( Get_Render_Options );
    pragma Inline( Get_Render_Pass );
    pragma Inline( Get_Tile_Width );
    pragma Inline( Get_Width_Tiles );
    pragma Inline( Get_World_Property );
    pragma Inline( Is_Map_Loaded );

    BACKGROUND_ELEM : constant := 1;

    DISABLED_STATE : constant Widget_State := 2**0;

    ----------------------------------------------------------------------------

    package Bitmap_Arrays is new Center_Arrays( A_Allegro_Bitmap, "=" );

    -- Provides a Map to map entity id values to entities.
    package Entity_Maps is new Ada.Containers.Ordered_Maps( Entity_Id, A_Entity, "<", "=" );
    use Entity_Maps;

    package Entity_Lists is new Ada.Containers.Ordered_Sets( A_Entity, Entity_Z_Order, "=" );
    type A_Entity_List is access all Entity_Lists.Set;

    procedure Delete is new Ada.Unchecked_Deallocation( Entity_Lists.Set, A_Entity_List );

    type Boolean_Array is array (Natural range <>) of Boolean;
    type A_Boolean_Array is access all Boolean_Array;

    type Color_Array is array (Natural range <>) of Allegro_Color;
    type A_Color_Array is access all Color_Array;

    ----------------------------------------------------------------------------

    type Scene is abstract new Widget and Animated with
        record
            sigMapChanged   : aliased Signal;
            sigWorldProp    : aliased String_Signal;

            renderPass      : Render_Pass := Render_Color;
            options         : Render_Options := SCENE_DEFAULTS;
            lights          : A_Lighting_System := null;
            particles       : A_Particle_System := null;

            tileLayers      : Bitmap_Arrays.Center_Array;
            tileScaleX      : Float := 1.0;         -- Power-of-two scaling when drawing .tileLayers[]
            tileScaleY      : Float := 1.0;         -- keeps buffer size down when view area is large

            lib             : Library_Ptr;
            map             : A_Map := null;
            mapInfo         : A_Map_Info_Cache := null;
            entityMap       : Entity_Maps.Map;      -- maps entity id to entity
            entities        : Entity_Lists.Set;     -- all entities
            worldProps      : Map_Value;
            tileWidth       : Natural := 0;         -- width of a tile in world coordinates

            cam             : access Cameras.Camera := null;
            target          : Entity_Id := NULL_ID; -- target entity to follow

            effectiveZoom   : Float := 1.0;         -- total zoom from widget pixels to screen pixels
            viewX, viewY    : Float := 0.0;
            tileX1, tileY1,                         -- tile bounds in view this
            tileX2, tileY2  : Integer := 0;         --   frame
            frontZ          : Float := 0.0;         -- the frontmost Z depth of the scene,
                                                    --   relative to layer 0
        end record;

    procedure Construct( this : access Scene;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );

    procedure Delete( this : in out Scene );

    -- Adds an entity to the scene, updating the entity id map. The scene will
    -- take ownership of 'entity'.
    procedure Add_Entity( this : not null access Scene'Class; entity : A_Entity );

    -- Creates and returns a new renderable widget for entity 'eid'. By default,
    -- a Sprite widget will be created. Subclasses can override this method to
    -- instantiate additional widget types.
    function Create_Entity_Widget( this         : access Scene;
                                   entity       : not null A_Entity;
                                   initialState : Map_Value ) return A_Widget;

    -- Deletes entity 'entity', updating the id map and selection set as
    -- necessary. 'entity' will be consumed.
    procedure Delete_Entity( this : not null access Scene'Class; entity : in out A_Entity );

    -- Draws the scene's background, its map layers, its children, and its
    -- overlay layer. This overriding procedure dispatches to Draw_Content,
    -- Draw_Tiles_Layer, and Draw_Content_Foreground. Sprite children as drawn
    -- interleaved between the map layers at their approriate Z depths, but
    -- other children are drawn on top, as usual.
    procedure Draw( this : access Scene );

    -- Draws non-entity child widgets only. Entity widgets are handled specially.
    overriding
    procedure Draw_Children( this : access Scene );

    -- Draws the scene's foreground in front of all map layers and entities, and
    -- in front of the overlay layer. This does nothing by default but could be
    -- overridden to draw something like a HUD.
    overriding
    procedure Draw_Content_Foreground( this : access Scene ) is null;

    -- Draws the widget representing entity 'entity'.
    procedure Draw_Entity( this    : not null access Scene'Class;
                           entity  : not null A_Entity;
                           zOffset : Float );

    -- Draws after each layer 'layer' has been drawn, allowing additional effects
    -- to be added per layer. Drawing is performed in world coordinates (x,y are
    -- relative to world 0,0 and z is relative to layer 0).
    not overriding
    procedure Draw_Layer_Overlay( this : access Scene; layer : Integer ) is null;

    -- Draws layer 'layer' as parallax scrolling scenery. This procedure is
    -- called once per scenery layer (background to foreground), per frame.
    procedure Draw_Scenery_Layer( this  : not null access Scene'Class;
                                  layer : Integer );

    -- Draws the tile in map layer 'layer' at 'col','row'. The tile will be drawn
    -- relative to the world origin (0,0) at Z depth 0.0.
    procedure Draw_Tile( this     : not null access Scene'Class;
                         layer    : Integer;
                         col, row : Integer );

    -- Draws all world content in the scene. A map must be loaded. All drawing
    -- is relative to world coordinates (0,0) at layer 0.
    procedure Draw_World( this : not null access Scene'Class );
    pragma Precondition( this.map /= null );

    -- Returns the world Z depth of the background map layer relative to layer
    -- 0, or 0.0 if no world loaded.
    function Get_Background_Z( this : not null access Scene'Class ) return Float;

    -- Returns the world Z depth of the foreground map layer relative to laser
    -- 0, or 0.0 if no world loaded.
    function Get_Foreground_Z( this : not null access Scene'Class ) return Float;

    -- Returns the tint of layer 'layer' based on its primary and secondary tints
    -- and opacity.
    function Get_Layer_Tint( this : not null access Scene'Class; layer : Integer ) return Allegro_Color;

    -- Returns the minimum height of the scene based on the height of the map.
    function Get_Min_Height( this : access Scene ) return Float;

    -- Returns the minimum width of the scene based on the width of the map.
    function Get_Min_Width( this : access Scene ) return Float;

    -- Returns the visual state of the scene, based on its enabled and loaded
    -- state.
    function Get_Visual_State( this : access Scene ) return Widget_State;

    -- Handles an event received from the corral that the scene is registered
    -- with. This is the same corral provided by the Game View system's
    -- Game_View object. If 'evt' is returned null then the event was consumed.
    procedure Handle_Event( this : access Scene;
                            evt  : in out A_Event;
                            resp : out Response_Type );
    pragma Precondition( evt /= null );

    -- Returns True if the tile at 'col','row' (tile coordinates) in layer number
    -- 'layer' of the scene's map is currently visible in the scene's viewport.
    function Is_Tile_Visible( this     : not null access Scene'Class;
                              layer    : Integer;
                              col, row : Integer ) return Boolean;

    -- Called when a new entity is added, just after it has been added to the
    -- entities map.
    not overriding
    procedure On_Entity_Added( this : access Scene; entity : not null A_Entity ) is null;

    -- Called when an entity is being removed, just before it is removed from
    -- the entities map.
    not overriding
    procedure On_Entity_Removed( this : access Scene; entity : not null A_Entity ) is null;

    -- Called just after a tile has been updated in the map. Override this
    -- procedure as necessary.
    not overriding
    procedure On_Tile_Changed( this     : access Scene;
                               layer    : Integer;
                               col, row : Integer ) is null;

    -- Sets the scene's target entity, 'target', by Entity_Id. The scene will
    -- automatically scroll its viewport to keep the target entity in sight,
    -- constrained within a bounding box 'boundsX1', 'boundsY1' to 'boundsX2',
    -- 'boundsY2' in world coordinates, relative to viewport center. If 'target'
    -- is a null id then the scene will not follow an entity.
    procedure Set_Target( this     : not null access Scene'Class;
                          target   : Entity_Id;
                          boundsX1,
                          boundsY1,
                          boundsX2,
                          boundsY2 : Float );

    procedure Tick( this : access Scene; time : Tick_Time );

    ----------------------------------------------------------------------------

    -- Deletes a Boolean_Array.
    procedure Delete is new Ada.Unchecked_Deallocation( Boolean_Array, A_Boolean_Array );

    -- Deletes a Color_Array.
    procedure Delete is new Ada.Unchecked_Deallocation( Color_Array, A_Color_Array );

end Widgets.Scenes;
