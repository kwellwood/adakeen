--
-- Copyright (c) 2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Widget_Styles.Registry;            use Widget_Styles.Registry;

package body Widgets.Aspect_Layouts is

    package Connections is new Signals.Connections(Aspect_Layout);
    use Connections;

    ----------------------------------------------------------------------------

    procedure On_Parent_Changed( this : not null access Aspect_Layout'Class );

    procedure On_Style_Changed( this : not null access Aspect_Layout'Class );

    procedure Update_Size( this : not null access Aspect_Layout'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Aspect_Layout( view : not null access Game_Views.Game_View'Class;
                                   id   : String := "" ) return A_Aspect_Layout is
        this : A_Aspect_Layout := new Aspect_Layout;
    begin
        this.Construct( view, id );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Aspect_Layout;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Aspect_Layout;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String ) is
    begin
        Widget(this.all).Construct( view, id, "Aspect" );
        this.StyleChanged.Connect( Slot( this, On_Style_Changed'Access ) );
        this.ParentChanged.Connect( Slot( this, On_Parent_Changed'Access ) );

        -- set the inner anchor offsets from the background padding in the style
        this.On_Style_Changed;
    end Construct;

    ----------------------------------------------------------------------------

    procedure Add_Widget( this  : not null access Aspect_Layout'Class;
                          child : not null access Widget'Class ) is
    begin
        this.Add_Child( child );
    end Add_Widget;

    ----------------------------------------------------------------------------

    procedure Clear_Widgets( this : not null access Aspect_Layout'Class ) is
    begin
        this.Delete_Children;
    end Clear_Widgets;

    ----------------------------------------------------------------------------

    procedure Enable_Auto_Zoom( this    : not null access Aspect_Layout'Class;
                                enabled : Boolean ) is
    begin
        this.autoZoom := enabled;
        this.Update_Size;
    end Enable_Auto_Zoom;

    ----------------------------------------------------------------------------

    procedure Enable_Integer_Scaling( this    : not null access Aspect_Layout'Class;
                                      enabled : Boolean ) is
    begin
        this.intScaling := enabled;
        this.Update_Size;
    end Enable_Integer_Scaling;

    ----------------------------------------------------------------------------

    procedure On_Parent_Changed( this : not null access Aspect_Layout'Class ) is
    begin
        if this.prevParent /= null then
            this.prevParent.Resized.Disconnect_All( this );
        end if;
        if this.Get_Parent /= null then
            this.Get_Parent.Resized.Connect( Slot( this, Update_Size'Access ) );
            this.Update_Size;
            this.Center( this.Get_Parent );
        end if;
        this.prevParent := this.Get_Parent;
    end On_Parent_Changed;

    ----------------------------------------------------------------------------

    procedure On_Style_Changed( this : not null access Aspect_Layout'Class ) is
    begin
        this.ancLeft.innerPad := this.style.Get_Pad_Left( BACKGROUND_ELEM );
        this.ancTop.innerPad := this.style.Get_Pad_Top( BACKGROUND_ELEM );
        this.ancRight.innerPad := -this.style.Get_Pad_Right( BACKGROUND_ELEM );
        this.ancBottom.innerPad := -this.style.Get_Pad_Bottom( BACKGROUND_ELEM );

        this.Update_Geometry;
    end On_Style_Changed;

    ----------------------------------------------------------------------------

    procedure Remove_Widget( this  : not null access Aspect_Layout'Class;
                             child : not null access Widget'Class ) is
    begin
        this.Remove_Child( child );
    end Remove_Widget;

    ----------------------------------------------------------------------------

    procedure Set_Base_Size( this   : not null access Aspect_Layout'Class;
                             width  : Float;
                             height : Float ) is
    begin
        this.baseWidth := width;
        this.baseHeight := height;
        this.Update_Size;
    end Set_Base_Size;

    ----------------------------------------------------------------------------

    procedure Update_Size( this : not null access Aspect_Layout'Class ) is
        parentWidth  : constant Float := this.Get_Parent.Width.Get;
        parentHeight : constant Float := this.Get_Parent.Height.Get;
        dstWidth     : Float;
        dstHeight    : Float;
        srcRatio     : Float;
        dstRatio     : Float;
        scaling      : Float;
    begin
        if this.Get_Parent = null or this.baseWidth <= 0.0 or this.baseHeight <= 0.0 then
            return;
        end if;

        if this.intScaling then
            scaling := Float'Min( Float'Floor( parentWidth / this.baseWidth ),
                                  Float'Floor( parentHeight / this.baseHeight ) );
            dstWidth := Float'Rounding( this.baseWidth * scaling );
            dstHeight := Float'Rounding( this.baseHeight * scaling );
        else
            dstRatio := parentWidth / parentHeight;
            srcRatio  := this.baseWidth / this.baseHeight;
            if srcRatio = dstRatio then
                -- proportions match
                dstWidth := parentWidth;
                dstHeight := parentHeight;
            elsif srcRatio < dstRatio then
                -- bmp is taller; adjust width and shift right
                dstWidth := Float'Rounding( this.baseWidth * parentHeight / this.baseHeight );
                dstHeight := parentHeight;
            else
                -- bmp is wider; adjust height and shift down
                dstWidth := parentWidth;
                dstHeight := Float'Rounding( this.baseHeight * parentWidth / this.baseWidth );
            end if;
        end if;

        this.Width.Set( dstWidth );
        this.Height.Set( dstHeight );

        if this.autoZoom then
            this.Zoom.Set( dstWidth / this.baseWidth );
        end if;
    end Update_Size;

begin

    Register_Style( "Aspect",
                    (BACKGROUND_ELEM => To_Unbounded_String( "background" )),
                    (BACKGROUND_ELEM => Area_Element),
                    (0 => To_Unbounded_String( "disabled" )) );

end Widgets.Aspect_Layouts;
