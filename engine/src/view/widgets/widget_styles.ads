--
-- Copyright (c) 2016-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Real_Time;                     use Ada.Real_Time;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Assets.Fonts;                      use Assets.Fonts;
with Allegro.Color;                     use Allegro.Color;
with Icons;                             use Icons;
with Objects;                           use Objects;
with Styles;                            use Styles;
with Values;                            use Values;
with Widget_Style_Properties;

package Widget_Styles is

    type Widget_State is mod 2**16;

    INVALID_STATE : constant Widget_State := Widget_State'Last;
    DEFAULT_STATE : constant Widget_State := Widget_State'First;

    subtype Element_Index is Natural;

    type Element_Style is abstract new Limited_Object with private;

    type Element_Type is (Area_Element, Icon_Element, Text_Element, Widget_Element);

    subtype Property_Id is Integer;

    -- Style Property Ids                                   -- NAME
    STYLE_PROP_UNKNOWN : constant Property_Id :=  0;
    STYLE_ALIGN        : constant Property_Id :=  1;        -- align
    STYLE_COLOR        : constant Property_Id :=  2;        -- color
    STYLE_FONT_NAME    : constant Property_Id :=  3;        -- fontName
    STYLE_FONT_SIZE    : constant Property_Id :=  4;        -- fontSize
    STYLE_HEIGHT       : constant Property_Id :=  5;        -- height
    STYLE_IMAGE        : constant Property_Id :=  6;        -- image
    STYLE_OFFSET_X     : constant Property_Id :=  7;        -- offsetX
    STYLE_OFFSET_Y     : constant Property_Id :=  8;        -- offsetY
    STYLE_PAD_BOTTOM   : constant Property_Id :=  9;        -- padBottom
    STYLE_PAD_LEFT     : constant Property_Id := 10;        -- padLeft
    STYLE_PAD_RIGHT    : constant Property_Id := 11;        -- padRight
    STYLE_PAD_TOP      : constant Property_Id := 12;        -- padTop
    STYLE_SHADE        : constant Property_Id := 13;        -- shade
    STYLE_SPACING      : constant Property_Id := 14;        -- spacing
    STYLE_TINT         : constant Property_Id := 15;        -- tint
    STYLE_WIDTH        : constant Property_Id := 16;        -- width
    STYLE_WRAP         : constant Property_Id := 17;        -- wrap

    -- Returns the integer property id for a property name string 'propertyName',
    -- or 0 if it is not recognized.
    function To_Property_Id( propertyName : String ) return Property_Id;

    ----------------------------------------------------------------------------

    type Widget_Style(elementCount : Natural) is new Element_Style with private;
    type A_Widget_Style is access all Widget_Style'Class;

    function Create_Widget_Style( widgetType   : String;
                                  name         : String;
                                  elementCount : Natural ) return A_Widget_Style;

    -- Copies 'source' to this style, overwriting everything.
    procedure Copy_From( this   : in out Widget_Style'Class;
                         source : A_Widget_Style );

    -- Returns the name of the style that defined the Widget_Style.
    function Get_Name( this : Widget_Style'Class ) return String;

    function Get_Style( this    : Widget_Style'Class;
                        element : Element_Index ) return A_Widget_Style;

    -- Returns the widget type that this style is intended for.
    function Get_Widget_Type( this : Widget_Style'Class ) return String;

    -- Merges the contents of 'source' into this style, overriding as necessary.
    procedure Merge_From( this   : in out Widget_Style'Class;
                          source : A_Widget_Style );

    -- Sets property 'property' of element 'element' for widget state 'state' to
    -- value 'val', returning True on success. Returns False if the element or
    -- property do not exist for this style. The value will be converted to the
    -- appropriate internal representation.
    function Set_Property( this     : in out Widget_Style'Class;
                           element  : Element_Index;
                           property : Property_Id;
                           state    : Widget_State;
                           val      : Value'Class ) return Boolean;

    -- Same as Set_Property() above, ignoring the return value.
    procedure Set_Property( this     : in out Widget_Style'Class;
                            element  : Element_Index;
                            property : Property_Id;
                            state    : Widget_State;
                            val      : Value'Class );

    procedure Update_Animation( this    : in out Widget_Style'Class;
                                element : Element_Index;
                                state   : Widget_State;
                                now     : Time_Span );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Returns the value of property 'property' in style element 'element' for
    -- widget state 'state', converted to a generic Value type. If the element
    -- or property does not exist for this style, null will be returned.
    --
    -- This corresponds with Set_Property(element, property, state, val).
    function Get_Property( this     : Widget_Style'Class;
                           element  : Element_Index;
                           property : Property_Id;
                           state    : Widget_State := DEFAULT_STATE ) return Value;

    function Get_Align( this    : Widget_Style'Class;
                        element : Element_Index;
                        state   : Widget_State := DEFAULT_STATE ) return Align_Type;

    function Get_Color( this    : Widget_Style'Class;
                        element : Element_Index;
                        state   : Widget_State := DEFAULT_STATE ) return Allegro_Color;

    function Get_Font( this    : Widget_Style'Class;
                       element : Element_Index;
                       state   : Widget_State := DEFAULT_STATE ) return A_Font;

    function Get_Font_Name( this    : Widget_Style'Class;
                            element : Element_Index;
                            state   : Widget_State := DEFAULT_STATE ) return String;

    function Get_Font_Size( this    : Widget_Style'Class;
                            element : Element_Index;
                            state   : Widget_State := DEFAULT_STATE ) return Integer;

    function Get_Height( this    : Widget_Style'CLass;
                         element : Element_Index;
                         state   : Widget_State := DEFAULT_STATE ) return Float;

    function Get_Image( this    : Widget_Style'Class;
                        element : Element_Index;
                        state   : Widget_State := DEFAULT_STATE ) return Icon_Type;

    function Get_Image_Name( this    : Widget_Style'Class;
                             element : Element_Index;
                             state   : Widget_State := DEFAULT_STATE ) return String;

    function Get_Offset_X( this    : Widget_Style'Class;
                           element : Element_Index;
                           state   : Widget_State := DEFAULT_STATE ) return Float;

    function Get_Offset_Y( this    : Widget_Style'Class;
                           element : Element_Index;
                           state   : Widget_State := DEFAULT_STATE ) return Float;

    function Get_Pad_Bottom( this    : Widget_Style'Class;
                             element : Element_Index;
                             state   : Widget_State := DEFAULT_STATE ) return Float;

    function Get_Pad_Left( this    : Widget_Style'Class;
                           element : Element_Index;
                           state   : Widget_State := DEFAULT_STATE ) return Float;

    function Get_Pad_Right( this    : Widget_Style'Class;
                            element : Element_Index;
                            state   : Widget_State := DEFAULT_STATE ) return Float;

    function Get_Pad_Top( this    : Widget_Style'Class;
                          element : Element_Index;
                          state   : Widget_State := DEFAULT_STATE ) return Float;

    function Get_Shade( this    : Widget_Style'Class;
                        element : Element_Index;
                        state   : Widget_State := DEFAULT_STATE ) return Float;

    function Get_Spacing( this    : Widget_Style'Class;
                          element : Element_Index;
                          state   : Widget_State := DEFAULT_STATE ) return Float;

    function Get_Tint( this    : Widget_Style'Class;
                       element : Element_Index;
                       state   : Widget_State := DEFAULT_STATE ) return Allegro_Color;

    function Get_Width( this    : Widget_Style'Class;
                        element : Element_Index;
                        state   : Widget_State := DEFAULT_STATE ) return Float;

    function Get_Wrap( this    : Widget_Style'Class;
                       element : Element_Index;
                       state   : Widget_State := DEFAULT_STATE ) return Wrap_Mode;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    function Clone( this : A_Widget_Style ) return A_Widget_Style;

    procedure Delete( this : in out A_Widget_Style );

private

    type Element_Style is abstract new Limited_Object with
        record
            stateCount : Natural := 0;
        end record;
    type A_Element_Style is access all Element_Style'Class;

    procedure Construct( this : access Element_Style; stateCount : Natural );

    function Clone( this : Element_Style ) return A_Element_Style is (null);

    procedure Copy_From_Element( this   : in out Element_Style;
                                 source : Element_Style'Class ) is null;

    procedure Get_Align( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Align_Type );

    procedure Get_Color( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Allegro_Color );

    procedure Get_Font( this : in out Element_Style; state : Widget_State; defined : out Boolean; property : in out A_Font );

    procedure Get_Font_Name( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Unbounded_String );

    procedure Get_Font_Size( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Integer );

    procedure Get_Height( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Float );

    procedure Get_Image( this : in out Element_Style; state : Widget_State; defined : out Boolean; property : in out Icon_Type );

    procedure Get_Image_Name( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Unbounded_String );

    procedure Get_Offset_X( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Float );

    procedure Get_Offset_Y( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Float );

    procedure Get_Pad_Bottom( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Float );

    procedure Get_Pad_Left( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Float );

    procedure Get_Pad_Right( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Float );

    procedure Get_Pad_Top( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Float );

    procedure Get_Shade( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Float );

    procedure Get_Spacing( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Float );

    procedure Get_Tint( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Allegro_Color );

    procedure Get_Width( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Float );

    procedure Get_Wrap( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Wrap_Mode );

    procedure Merge_From_Element( this   : in out Element_Style;
                                  source : Element_Style'Class ) is null;

    function Set_Property( this     : in out Element_Style;
                           property : Property_Id;
                           state    : Widget_State;
                           val      : Value'Class ) return Boolean is (False);

    procedure Update_Animation( this  : in out Element_Style;
                                state : Widget_State;
                                now   : Time_Span ) is null;

    ----------------------------------------------------------------------------

    package Align_Properties   is new Widget_Style_Properties(Align_Type);
    package Color_Properties   is new Widget_Style_Properties(Allegro_Color);
    package Float_Properties   is new Widget_Style_Properties(Float);
    package Integer_Properties is new Widget_Style_Properties(Integer);
    package String_Properties  is new Widget_Style_Properties(Unbounded_String);
    package Wrap_Properties    is new Widget_Style_Properties(Wrap_Mode);

    ----------------------------------------------------------------------------

    type Element_Style_Array is array (Integer range <>) of A_Element_Style;

    type Widget_Style(elementCount : Natural) is new Element_Style with
        record
            elements   : Element_Style_Array(1..elementCount);
            widgetType : Unbounded_String;
            name       : Unbounded_String;
        end record;

    procedure Construct( this : access Widget_Style; widgetType : String; name : String );

    function Clone( this : Widget_Style ) return A_Element_Style;

    overriding procedure Copy_From_Element( this   : in out Widget_Style;
                                            source : Element_Style'Class );

    procedure Delete( this : in out Widget_Style );

    overriding procedure Merge_From_Element( this   : in out Widget_Style;
                                             source : Element_Style'Class );

    ----------------------------------------------------------------------------

    function Create_Area_Style(stateCount : Natural) return A_Element_Style;

    type Area_Style is new Element_Style with
        record
            color     : Color_Properties.Property;
            align     : Align_Properties.Property;
            image     : String_Properties.Property;
            icon      : Icon_Type := NO_ICON;
            iconState : Widget_State := INVALID_STATE;
            shade     : Float_Properties.Property;
            spacing   : Float_Properties.Property;
            padBottom : Float_Properties.Property;
            padLeft   : Float_Properties.Property;
            padRight  : Float_Properties.Property;
            padTop    : Float_Properties.Property;
            tint      : Color_Properties.Property;
        end record;
    type A_Area_Style is access all Area_Style'Class;

    function Clone( this : Area_Style ) return A_Element_Style;

    procedure Construct( this : access Area_Style; stateCount : Natural );

    overriding procedure Copy_From_Element( this   : in out Area_Style;
                                            source : Element_Style'Class );

    procedure Delete( this : in out Area_Style );

    overriding procedure Get_Align( this : Area_Style; state : Widget_State; defined : out Boolean; property : in out Align_Type );

    overriding procedure Get_Color( this : Area_Style; state : Widget_State; defined : out Boolean; property : in out Allegro_Color );

    overriding procedure Get_Image( this : in out Area_Style; state : Widget_State; defined : out Boolean; property : in out Icon_Type );

    overriding procedure Get_Image_Name( this : Area_Style; state : Widget_State; defined : out Boolean; property : in out Unbounded_String );

    overriding procedure Get_Pad_Bottom( this : Area_Style; state : Widget_State; defined : out Boolean; property : in out Float );

    overriding procedure Get_Pad_Left( this : Area_Style; state : Widget_State; defined : out Boolean; property : in out Float );

    overriding procedure Get_Pad_Right( this : Area_Style; state : Widget_State; defined : out Boolean; property : in out Float );

    overriding procedure Get_Pad_Top( this : Area_Style; state : Widget_State; defined : out Boolean; property : in out Float );

    overriding procedure Get_Shade( this : Area_Style; state : Widget_State; defined : out Boolean; property : in out Float );

    overriding procedure Get_Spacing( this : Area_Style; state : Widget_State; defined : out Boolean; property : in out Float );

    overriding procedure Get_Tint( this : Area_Style; state : Widget_State; defined : out Boolean; property : in out Allegro_Color );

    overriding procedure Merge_From_Element( this   : in out Area_Style;
                                             source : Element_Style'Class );

    overriding function Set_Property( this     : in out Area_Style;
                                      property : Property_Id;
                                      state    : Widget_State;
                                      val      : Value'Class ) return Boolean;

    overriding procedure Update_Animation( this  : in out Area_Style;
                                           state : Widget_State;
                                           now   : Time_Span );

    ----------------------------------------------------------------------------

    function Create_Icon_Style(stateCount : Natural) return A_Element_Style;

    type Icon_Style is new Element_Style with
        record
            height    : Float_Properties.Property;
            image     : String_Properties.Property;
            icon      : Icon_Type := NO_ICON;
            iconState : Widget_State := INVALID_STATE;
            offsetX   : Float_Properties.Property;
            offsetY   : Float_Properties.Property;
            shade     : Float_Properties.Property;
            tint      : Color_Properties.Property;
            width     : Float_Properties.Property;
        end record;
    type A_Icon_Style is access all Icon_Style'Class;

    function Clone( this : Icon_Style ) return A_Element_Style;

    procedure Construct( this : access Icon_Style; stateCount : Natural );

    overriding procedure Copy_From_Element( this   : in out Icon_Style;
                                            source : Element_Style'Class );

    procedure Delete( this : in out Icon_Style );

    overriding procedure Get_Height( this : Icon_Style; state : Widget_State; defined : out Boolean; property : in out Float );

    overriding procedure Get_Image( this : in out Icon_Style; state : Widget_State; defined : out Boolean; property : in out Icon_Type );

    overriding procedure Get_Image_Name( this : Icon_Style; state : Widget_State; defined : out Boolean; property : in out Unbounded_String );

    overriding procedure Get_Offset_X( this : Icon_Style; state : Widget_State; defined : out Boolean; property : in out Float );

    overriding procedure Get_Offset_Y( this : Icon_Style; state : Widget_State; defined : out Boolean; property : in out Float );

    overriding procedure Get_Shade( this : Icon_Style; state : Widget_State; defined : out Boolean; property : in out Float );

    overriding procedure Get_Tint( this : Icon_Style; state : Widget_State; defined : out Boolean; property : in out Allegro_Color );

    overriding procedure Get_Width( this : Icon_Style; state : Widget_State; defined : out Boolean; property : in out Float );

    overriding procedure Merge_From_Element( this   : in out Icon_Style;
                                             source : Element_Style'Class );

    overriding function Set_Property( this     : in out Icon_Style;
                                      property : Property_Id;
                                      state    : Widget_State;
                                      val      : Value'Class ) return Boolean;

    overriding procedure Update_Animation( this  : in out Icon_Style;
                                           state : Widget_State;
                                           now   : Time_Span );

    ----------------------------------------------------------------------------

    function Create_Text_Style( stateCount : Natural ) return A_Element_Style;

    type Text_Style is new Element_Style with
        record
            color     : Color_Properties.Property;
            fontName  : String_Properties.Property;
            fontSize  : Integer_Properties.Property;
            offsetX   : Float_Properties.Property;
            offsetY   : Float_Properties.Property;
            shade     : Float_Properties.Property;
            wrap      : Wrap_Properties.Property;
            font      : A_Font := null;
            fontState : Widget_State := INVALID_STATE;
        end record;
    type A_Text_Style is access all Text_Style'Class;

    function Clone( this : Text_Style ) return A_Element_Style;

    procedure Construct( this : access Text_Style; stateCount : Natural );

    overriding procedure Copy_From_Element( this   : in out Text_Style;
                                            source : Element_Style'Class );

    procedure Delete( this : in out Text_Style );

    overriding procedure Get_Color( this : Text_Style; state : Widget_State; defined : out Boolean; property : in out Allegro_Color );

    overriding procedure Get_Font( this : in out Text_Style; state : Widget_State; defined : out Boolean; property : in out A_Font );

    overriding procedure Get_Font_Name( this : Text_Style; state : Widget_State; defined : out Boolean; property : in out Unbounded_String );

    overriding procedure Get_Font_Size( this : Text_Style; state : Widget_State; defined : out Boolean; property : in out Integer );

    overriding procedure Get_Offset_X( this : Text_Style; state : Widget_State; defined : out Boolean; property : in out Float );

    overriding procedure Get_Offset_Y( this : Text_Style; state : Widget_State; defined : out Boolean; property : in out Float );

    overriding procedure Get_Shade( this : Text_Style; state : Widget_State; defined : out Boolean; property : in out Float );

    overriding procedure Get_Wrap( this : Text_Style; state : Widget_State; defined : out Boolean; property : in out Wrap_Mode );

    overriding procedure Merge_From_Element( this   : in out Text_Style;
                                             source : Element_Style'Class );

    overriding function Set_Property( this     : in out Text_Style;
                                      property : Property_Id;
                                      state    : Widget_State;
                                      val      : Value'Class ) return Boolean;

end Widget_Styles;
