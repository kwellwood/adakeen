--
-- Copyright (c) 2017-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Styles;                            use Styles;
with Widget_Styles.Registry;            use Widget_Styles.Registry;

package body Widgets.Row_Layouts is

    package Connections is new Signals.Connections(Row_Layout);
    use Connections;

    procedure On_Style_Changed( this : not null access Row_Layout'Class );

    procedure On_Child_Hidden( this : not null access Row_Layout'Class );

    procedure On_Child_Shown( this : not null access Row_Layout'Class );

    function Visible_Count( this : not null access Row_Layout'Class ) return Natural is (Integer(this.visibles.Length));

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Row_Layout( view : not null access Game_Views.Game_View'Class;
                                id   : String := "" ) return A_Row_Layout is
        this : constant A_Row_Layout := new Row_Layout;
    begin
        this.Construct( view, id, "RowLayout" );
        return this;
    end Create_Row_Layout;

    ----------------------------------------------------------------------------

    procedure Append( this  : not null access Row_Layout'Class;
                      child : not null access Widget'Class ) is
    begin
        this.Insert( this.Count + 1, child );
    end Append;

    ----------------------------------------------------------------------------

    procedure Clear( this : not null access Row_Layout'Class ) is
    begin
        this.expander := null;
        this.Delete_Children;
        this.elements.Clear;
        this.visibles.Clear;
    end Clear;

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this       : access Row_Layout;
                         view       : not null access Game_Views.Game_View'Class;
                         id         : String;
                         widgetType : String ) is
    begin
        Widget(this.all).Construct( view, id, widgetType );
        this.StyleChanged.Connect( Slot( this, On_Style_Changed'Access ) );
        this.acceptFocus := False;

        -- set the inner anchor offsets from the background padding in the style
        this.On_Style_Changed;
    end Construct;

    ----------------------------------------------------------------------------

    function Count( this : not null access Row_Layout'Class ) return Natural is (Natural(this.elements.Length));

    ----------------------------------------------------------------------------

    procedure Insert( this  : not null access Row_Layout'Class;
                      index : Integer;
                      child : not null access Widget'Class ) is
        newIndex : Integer;
        visIndex : Integer := 0;
    begin
        newIndex := Integer'Min( Integer'Max( 1, index ), this.Count + 1 );

        this.Add_Child( child );
        this.elements.Insert( newIndex, A_Widget(child) );

        child.Shown.Connect( Slot( this, On_Child_Shown'Access ) );
        child.Hidden.Connect( Slot( this, On_Child_Hidden'Access ) );
        if child.Is_Visible then
            visIndex := 1;
            for w of this.elements loop
                exit when w = child;
                if w.Is_Visible then
                    visIndex := visIndex + 1;
                end if;
            end loop;
            this.visibles.Insert( visIndex, child );
        end if;

        child.Unset_X;
        child.Unset_Y;

        this.Hook_Aligned_Edges( A_Widget(child) );

        if child.Is_Visible then
            this.Hook_Adjacent_Edges( visIndex );
        end if;
    end Insert;

    ----------------------------------------------------------------------------

    function Get_At( this : not null access Row_Layout'Class; index : Integer ) return A_Widget is
    begin
        if index >= 1 and index <= this.Count then
            return this.elements.Element( index );
        end if;
        return null;
    end Get_At;

    ----------------------------------------------------------------------------

    function Get_Expander( this : not null access Row_Layout'Class ) return A_Widget is (this.expander);

    ----------------------------------------------------------------------------

    function Get_First( this : not null access Row_Layout'Class ) return A_Widget is (this.Get_At( 1 ));

    ----------------------------------------------------------------------------

    function Get_Last( this : not null access Row_Layout'Class ) return A_Widget is (this.Get_At( this.Count ));

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Height( this : access Row_Layout ) return Float is
        maxHeight : Float := 0.0;
    begin
        for child of this.visibles loop
            maxHeight := Float'Max( maxHeight, (if child.posHeight.Is_Null then child.Get_Preferred_Height else child.posHeight.To_Float) );
        end loop;

        return this.style.Get_Pad_Top( BACKGROUND_ELEM ) +
               maxHeight +
               this.style.Get_Pad_Bottom( BACKGROUND_ELEM );
    end Get_Min_Height;

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Width( this : access Row_Layout ) return Float is
        sumWidths : Float := 0.0;
    begin
        for child of this.visibles loop
            sumWidths := sumWidths + (if child.posWidth.Is_Null then child.Get_Preferred_Width else child.posWidth.To_Float);
        end loop;

        return this.style.Get_Pad_Left( BACKGROUND_ELEM ) +
               sumWidths +
               Float'Max( 0.0, (Float(this.Visible_Count) - 1.0) * this.style.Get_Spacing( BACKGROUND_ELEM ) ) +
               this.style.Get_Pad_Right( BACKGROUND_ELEM );
    end Get_Min_Width;

    ----------------------------------------------------------------------------

    not overriding
    function Get_Negative_Edge( this     : access Row_Layout;
                                child    : access Widget'Class;
                                inverted : Boolean ) return A_Anchor is
        align       : constant Align_Type := this.style.Get_Align( BACKGROUND_ELEM );
        normalOrder : constant Boolean := not (align = Top_Right or align = Right or align = Bottom_Right);
    begin
        return (if (not normalOrder) xor inverted then child.ancRight'Access else child.ancLeft'Access);
    end Get_Negative_Edge;

    ----------------------------------------------------------------------------

    not overriding
    procedure Hook_Aligned_Edges( this : access Row_Layout; child : A_Widget ) is
        align : constant Align_Type := this.style.Get_Align( BACKGROUND_ELEM );
    begin
        case align is
            when Top_Left | Top_Center | Top_Right =>
                child.Top.Attach( this.Top );
            when Left | Center | Right =>
                if child.posHeight.Is_Null then
                    -- if no height is explicitly set, fill the row's height
                    child.Fill_Vertical( this );
                else
                    child.Vertical_Center.Attach( this.Vertical_Center );
                end if;
            when Bottom_Left | Bottom_Center | Bottom_Right =>
                child.Bottom.Attach( this.Bottom );
        end case;
    end Hook_Aligned_Edges;

    ----------------------------------------------------------------------------

    procedure Hook_Adjacent_Edges( this     : not null access Row_Layout'Class;
                                   visIndex : Positive ) is

        ------------------------------------------------------------------------

        -- Neg_Edge() and Pos_Edge() return the negative or positive anchor of a
        -- child widget, depending on which direction the row is ordered. The
        -- negative edge of a child at index 'N' is the one nearest child 'N-1'
        -- and its positive edge is the one nearest child 'N+1'.
        --
        -- Hooking Up Edges: (Normal ordering, No Expander)
        --
        --     Children are hooked together such that the negative edge of the
        --     child at index 'N' is hooked to the positive edge of child 'N-1'.
        --
        --     | +-----+ +------+ +---+ +----+
        --     |<|  1  |<|  2   |<| 3 |<| 4  |
        --     | +-----+ +------+ +---+ +----+
        --     N N     P N      P N   P N    P                   <- negative or positive edge
        --     ^
        --     Parent (this)
        --
        -- Hooking Up Edges: (Normal ordering, With Expander)
        --
        --     If a child is set as the expander widget (this.expander) then the
        --     direction of the connections must be swapped, starting with the
        --     positive edge of the expander, such that the positive edge of
        --     child 'N' is hooked to the negative edge of child 'N+1'. To
        --     accomplish this, the polarity of Neg_Edge() and Pos_Edge() is
        --     swapped for indexes greater than the index of the expander.
        --
        --     | +-----+ +-------+ +---------+ +---+ +----+ |
        --     |<|  1  |<|   2   |<|    3    |>| 4 |>| 5  |>|
        --     | +-----+ +-------+ +---------+ +---+ +----+ |
        --     N N     P N       P N         p n   p n    p p    <- negative or positive edge
        --     ^                        ^    N P   N P    N N    <- swapped polarity
        --     |                        |                   ^
        --     Parent (this)            Expander            Parent (this)

        function Neg_Edge( child : access Widget'Class; inverted : Boolean ) return A_Anchor
        is (this.Get_Negative_Edge( child, inverted ))
        with Inline;

        function Pos_Edge( child : access Widget'Class; inverted : Boolean ) return A_Anchor
        is (this.Get_Negative_Edge( child, not inverted ))
        with Inline;

        ------------------------------------------------------------------------

        spacing   : constant Float := this.style.Get_Spacing( BACKGROUND_ELEM );
        child     : constant A_Widget := this.visibles.Element( visIndex );
        expIndex  : Natural;
        attachDir : Integer;
        polarity  : Boolean;
    begin
        -- find the index of the expanding child in the visible child list.
        -- if there isn't a visible expanding child, default to Visible_Count + 1
        -- for normal behavior.
        expIndex := this.Visible_Count + 1;
        if this.expander /= null and then this.expander.Is_Visible then
            for i in 1..this.Visible_Count loop
                if this.visibles.Element( i ) = this.expander then
                    expIndex := i;
                    exit;
                end if;
            end loop;
        end if;

        if visIndex = expIndex then
            -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            -- Expander child --

            -- both its negative and positive edges attach to siblings in the
            -- negative and positive directions. use a loop to attach the negative
            -- edge to one sibling, then invert polarity to attach the other
            -- edge to the other sibling.
            for polarity in Boolean'Range loop
                attachDir := (if polarity then 1 else -1);
                if (visIndex + attachDir) in 1..this.Visible_Count then
                    -- attach to a sibling attachee in the attachDir direction
                    Neg_Edge( child, polarity ).Attach( Pos_Edge( this.visibles.Element( visIndex + attachDir ), polarity ), spacing );
                else
                    -- attach to the parent edge instead
                    Neg_Edge( child, polarity ). Attach( Neg_Edge( this, polarity ) );
                end if;
            end loop;

        else
            -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            -- Normal child --

            -- polarity is False (normal case) when child is before the expander
            -- or there is no expander. It is True (inverted polarity) when
            -- there is an expander and child is after it.
            polarity := visIndex > expIndex;

            -- - - - Outgoing attachment - - - --

            -- attachDir is the direction from visIndex that the outgoing anchor
            -- attachment will go. -1 is the normal case (index N attaches to N-1),
            -- and +1 if visIndex is after an expander.
            attachDir := (if polarity then 1 else -1);

            -- if there is a sibling attachee in the direction of 'attachDir',
            -- attach the negative edge of child to the positive edge of it.
            if (visIndex + attachDir) in 1..this.Visible_Count then
                Neg_Edge( child, polarity ).Attach( Pos_Edge( this.visibles.Element( visIndex + attachDir ), polarity ), spacing );

            -- if child is at the beginning of the list it connects to the parent
            elsif (visIndex + attachDir) < 1 then
                Neg_Edge( child, polarity ).Attach( Neg_Edge( this, polarity ) );

            -- if child is at the end of the list, and only if there is an
            -- expander, then it connects to the parent
            elsif (visIndex + attachDir) > this.Visible_Count and polarity then
                Neg_Edge( child, True ).Attach( Neg_Edge( this, True ) );
            end if;

            -- - - - Incoming attachment - - - --

            -- attachDir is now the direction of the incoming attaching. +1 is
            -- the normal case (index N+1 attaches to N), and -1 if visIndex is
            -- after an expander.
            attachDir := -attachDir;

            -- if there is a sibling attacher in the direction of 'attachDir',
            -- attach the negative edge of it to the positive edge of child.
            if (visIndex + attachDir) in 1..this.Visible_Count then
                Neg_Edge( this.visibles.Element( visIndex + attachDir ), polarity ).Attach( Pos_Edge( child, polarity ), spacing );
            end if;
        end if;
    end Hook_Adjacent_Edges;

    ----------------------------------------------------------------------------

    procedure On_Child_Hidden( this : not null access Row_Layout'Class ) is
        source   : constant A_Widget := A_Widget(this.Signaller);
        visIndex : Integer := 0;
    begin
        if source.Is_Visible then
            return;      -- filter out Hidden signals caused by ancestors
        end if;

        for w of this.visibles loop
            visIndex := visIndex + 1;
            exit when w = source;
        end loop;

        if source = this.expander then
            -- when the expander is hidden, the anchors need to be refreshed
            this.visibles.Delete( Index => visIndex );
            this.Refresh_Anchors;
        else
            this.Unhook_Adjacent_Edges( visIndex );
            this.visibles.Delete( Index => visIndex );
        end if;
    end On_Child_Hidden;

    ----------------------------------------------------------------------------

    procedure On_Child_Shown( this : not null access Row_Layout'Class ) is
        child    : constant A_Widget := A_Widget(this.Signaller);
        visIndex : Integer := 1;
    begin
        for w of this.elements loop
            exit when w = child;
            if w.Is_Visible then
                visIndex := visIndex + 1;
            end if;
        end loop;
        if visIndex <= this.Visible_Count and then this.visibles.Element( visIndex ) = child then
            return;         -- filter out Shown signals caused by ancestors
        end if;
        this.visibles.Insert( visIndex, child );

        if child = this.expander then
            -- when the expander is shown, the anchors need to be refreshed
            this.Refresh_Anchors;
        else
            this.Hook_Adjacent_Edges( visIndex );
        end if;
    end On_Child_Shown;

    ----------------------------------------------------------------------------

    procedure On_Style_Changed( this : not null access Row_Layout'Class ) is
    begin
        this.ancLeft.innerPad := this.style.Get_Pad_Left( BACKGROUND_ELEM );
        this.ancTop.innerPad := this.style.Get_Pad_Top( BACKGROUND_ELEM );
        this.ancRight.innerPad := -this.style.Get_Pad_Right( BACKGROUND_ELEM );
        this.ancBottom.innerPad := -this.style.Get_Pad_Bottom( BACKGROUND_ELEM );

        this.Refresh_Anchors;
    end On_Style_Changed;

    ----------------------------------------------------------------------------

    procedure Refresh_Anchors( this : not null access Row_Layout'Class ) is
    begin
        for child of this.elements loop
            child.Top.Detach;
            child.Bottom.Detach;
            child.Left.Detach;
            child.Right.Detach;
            child.Vertical_Center.Detach;
        end loop;

        for child of this.elements loop
            this.Hook_Aligned_Edges( child );
        end loop;

        for i in 1..this.Visible_Count loop
            this.Hook_Adjacent_Edges( i );
        end loop;
    end Refresh_Anchors;

    ----------------------------------------------------------------------------

    function Remove( this : not null access Row_Layout'Class; index : Integer ) return A_Widget is
        visIndex : Integer := 0;
        child    : A_Widget;
    begin
        if index < 1 or this.Count < index then
            return null;
        end if;

        child := this.Get_At( index );
        if child.Is_Visible then
            for w of this.visibles loop
                visIndex := visIndex + 1;
                exit when w = child;
            end loop;
            pragma Assert( visIndex > 0 );

            if child = this.expander then
                this.expander := null;
                this.visibles.Delete( Index => visIndex );
                this.Refresh_Anchors;
            else
                this.Unhook_Adjacent_Edges( visIndex );
                this.visibles.Delete( Index => visIndex );
            end if;
        elsif child = this.expander then
            this.expander := null;
        end if;

        child.Shown.Disconnect( Slot( this, On_Child_Shown'Access ) );
        child.Hidden.Disconnect( Slot( this, On_Child_Hidden'Access ) );

        child.Left.Detach;
        child.Right.Detach;
        child.Top.Detach;
        child.Bottom.Detach;
        child.Horizontal_Center.Detach;
        child.Vertical_Center.Detach;

        this.elements.Delete( index );
        this.Remove_Child( child );

        return child;
    end Remove;

    ----------------------------------------------------------------------------

    procedure Set_Expander( this  : not null access Row_Layout'Class;
                            child : access Widget'Class ) is
    begin
        if child /= this.expander then
            if child = null or else this.children.Contains( child ) then
                this.expander := A_Widget(child);
                this.Refresh_Anchors;
            end if;
        end if;
    end Set_Expander;

    ----------------------------------------------------------------------------

    procedure Swap( this   : not null access Row_Layout'Class;
                    indexA : Integer;
                    indexB : Integer ) is
        childA, childB     : A_Widget;
        visibleA, visibleB : Boolean;
    begin
        if indexA in 1..Integer(this.elements.Length) and indexB in 1..Integer(this.elements.Length) then

            -- be sure to hide the child elements before removing them
            visibleA := this.elements.Element( indexA ).Visible.Get;
            visibleB := this.elements.Element( indexB ).Visible.Get;
            this.elements.Element( indexA ).Visible.Set( False );
            this.elements.Element( indexB ).Visible.Set( False );

            -- swap the elements
            if indexA < indexB then
                childB := this.Remove( indexB );
                childA := this.Remove( indexA );
                this.Insert( indexA, childB );
                this.Insert( indexB, childA );
            elsif indexA > indexB then
                childA := this.Remove( indexA );
                childB := this.Remove( indexB );
                this.Insert( indexB, childA );
                this.Insert( indexA, childB );
            end if;

            -- restore widget visibility
            this.elements.Element( indexA ).Visible.Set( visibleA );
            this.elements.Element( indexB ).Visible.Set( visibleB );
        end if;
    end Swap;

    ----------------------------------------------------------------------------

    procedure Unhook_Adjacent_Edges( this     : not null access Row_Layout'Class;
                                     visIndex : Positive ) is

        ------------------------------------------------------------------------

        function Neg_Edge( child : access Widget'Class; inverted : Boolean ) return A_Anchor
        is (this.Get_Negative_Edge( child, inverted ))
        with Inline;

        function Pos_Edge( child : access Widget'Class; inverted : Boolean ) return A_Anchor
        is (this.Get_Negative_Edge( child, not inverted ))
        with Inline;

        ------------------------------------------------------------------------

        child     : constant A_Widget := this.visibles( visIndex );
        expIndex  : Integer;
        polarity  : Boolean;
        attachDir : Integer;
    begin
        -- find the index of the expanding child in the visible child list.
        -- if there isn't a visible expanding child, default to Visible_Count + 1
        -- for normal behavior.
        expIndex := this.Visible_Count + 1;
        if this.expander /= null and then this.expander.Is_Visible then
            for i in 1..this.Visible_Count loop
                if this.visibles.Element( i ) = this.expander then
                    expIndex := i;
                    exit;
                end if;
            end loop;
        end if;

        pragma Assert( visIndex /= expIndex );

        -- polarity is False (normal case) when child is before the expander
        -- or there is no expander. It is True (inverted polarity) when
        -- there is an expander and child is after it.
        polarity := visIndex > expIndex;

        -- attachDir is the direction of the child whose negative edge was
        -- anchored to 'child' and which will now be anchored to the positive
        -- edge of the child on the other side.
        attachDir := (if polarity then -1 else 1);

        if (visIndex + attachDir) in 1..this.Visible_Count then
            Neg_Edge( this.visibles.Element( visIndex + attachDir ), polarity ).Attach( Neg_Edge( child, polarity ).target,
                                                                                        Neg_Edge( child, polarity ).margin );
        end if;

        -- detach anchors to other children. (only one of them is actually
        -- attached to something.)
        Neg_Edge( child, polarity ).Detach;
        Pos_Edge( child, polarity ).Detach;
    end Unhook_Adjacent_Edges;

    --==========================================================================

    function Create_Column_Layout( view : not null access Game_Views.Game_View'Class;
                                   id   : String := "" ) return A_Column_Layout is
        this : constant A_Column_Layout := new Column_Layout;
    begin
        this.Construct( view, id, "ColumnLayout" );
        return this;
    end Create_Column_Layout;

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Height( this : access Column_Layout ) return Float is
        sumHeights : Float := 0.0;
    begin
        for child of this.visibles loop
            sumHeights := sumHeights + (if child.posHeight.Is_Null then child.Get_Preferred_Height else child.posHeight.To_Float);
        end loop;

        return this.style.Get_Pad_Top( BACKGROUND_ELEM ) +
               sumHeights +
               Float'Max( 0.0, (Float(this.Visible_Count) - 1.0) * this.style.Get_Spacing( BACKGROUND_ELEM ) ) +
               this.style.Get_Pad_Bottom( BACKGROUND_ELEM );
    end Get_Min_Height;

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Width( this : access Column_Layout ) return Float is
        maxWidth : Float := 0.0;
    begin
        for child of this.visibles loop
            maxWidth := Float'Max( maxWidth, (if child.posWidth.Is_Null then child.Get_Preferred_Width else child.posWidth.To_Float) );
        end loop;

        return this.style.Get_Pad_Left( BACKGROUND_ELEM ) +
               maxWidth +
               this.style.Get_Pad_Right( BACKGROUND_ELEM );
    end Get_Min_Width;

    ----------------------------------------------------------------------------

    overriding
    function Get_Negative_Edge( this     : access Column_Layout;
                                child    : access Widget'Class;
                                inverted : Boolean ) return A_Anchor is
        align       : constant Align_Type := this.style.Get_Align( BACKGROUND_ELEM );
        normalOrder : constant Boolean := not (align = Bottom_Left or align = Bottom_Center or align = Bottom_Right);
    begin
        return (if (not normalOrder) xor inverted then child.ancBottom'Access else child.ancTop'Access);
    end Get_Negative_Edge;

    ----------------------------------------------------------------------------

    overriding
    procedure Hook_Aligned_Edges( this : access Column_Layout; child : A_Widget ) is
        align : constant Align_Type := this.style.Get_Align( BACKGROUND_ELEM );
    begin
        case align is
            when Top_Left | Left | Bottom_Left =>
                child.Left.Attach( this.Left );
            when Top_Center | Center | Bottom_Center =>
                if child.posWidth.Is_Null then
                    -- if no width is explicitly set, fill the column's width
                    child.Fill_Horizontal( this );
                else
                    child.Horizontal_Center.Attach( this.Horizontal_Center );
                end if;
            when Top_Right | Right | Bottom_Right =>
                child.Right.Attach( this.Right );
        end case;
    end Hook_Aligned_Edges;

begin

    Register_Style( "RowLayout",
                    (BACKGROUND_ELEM => To_Unbounded_String( "background" )),
                    (BACKGROUND_ELEM => Area_Element),
                    (0 => To_Unbounded_String( "disabled" )) );

    Register_Style( "ColumnLayout",
                    (BACKGROUND_ELEM => To_Unbounded_String( "background" )),
                    (BACKGROUND_ELEM => Area_Element),
                    (0 => To_Unbounded_String( "disabled" )) );

end Widgets.Row_Layouts;
