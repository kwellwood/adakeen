--
-- Copyright (c) 2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Vectors;

package Widgets.Stack_Layouts is

    -- A Stack_Layout is a simple layout widget that stacks a set of widgets in
    -- the same space and then displays only one at a time. It has no visible
    -- style components. Padding is determined by its widget style.
    type Stack_Layout is new Widget with private;
    type A_Stack_Layout is access all Stack_Layout'Class;

    -- Stack_Layout Signals
    function IndexChanged( this : not null access Stack_Layout'Class ) return access Signal'Class;

    -- Creates a new Stack_Layout within 'view' with id 'id'.
    function Create_Stack_Layout( view : not null access Game_Views.Game_View'Class;
                                  id   : String := "" ) return A_Stack_Layout;
    pragma Postcondition( Create_Stack_Layout'Result /= null );

    -- Appends a widget to the stack. The Stack_Layout will own 'child' after
    -- this call and will delete it upon destruction.
    procedure Append( this  : not null access Stack_Layout'Class;
                      child : not null access Widget'Class );

    -- Removes and deletes all child widgets in the layout.
    procedure Clear( this : not null access Stack_Layout'Class );

    -- Returns the number of widgets in the layout.
    function Count( this : not null access Stack_Layout'Class ) return Natural;

    -- Returns a pointer to the widget at index 'index' (starting at 1). If the
    -- index does not exist, null will be returned.
    function Get_At( this : not null access Stack_Layout'Class; index : Integer ) return A_Widget;

    -- Returns the index of the currently visible item. Returns 0 when empty.
    function Get_Index( this : not null access Stack_Layout'Class ) return Integer;

    -- Inserts a widget into the stack before index 'index' (starting at 1).
    -- This is equivalent to appending the widget if 'index' is greater than the
    -- child count. The Row_Layout will own 'child' after this call and will
    -- delete it upon destruction.
    procedure Insert( this  : not null access Stack_Layout'Class;
                      index : Integer;
                      child : not null access Widget'Class );

    -- Removes and returns the widget at index 'index' (starting at 1). If the
    -- index does not exist, null will be returned.
    function Remove( this : not null access Stack_Layout'Class; index : Integer ) return A_Widget;

    -- Changes the selected index in the stack, displaying the widget at index
    -- 'index' instead. If 'index' is not a valid index, nothing will happen.
    procedure Set_Index( this : not null access Stack_Layout'CLass; index : Integer );

private

    BACKGROUND_ELEM : constant := 1;

    DISABLED_STATE : constant Widget_State := 2**0;

    package Widget_Vectors is new Ada.Containers.Vectors( Positive, A_Widget, "=" );

    ----------------------------------------------------------------------------

    type Stack_Layout is new Widget with
        record
            order : Widget_Vectors.Vector;
            index : Integer := 0;

            sigIndexChanged : aliased Signal;
        end record;

    procedure Construct( this : access Stack_Layout;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );

    function Get_Min_Height( this : access Stack_Layout ) return Float;

    function Get_Min_Width( this : access Stack_Layout ) return Float;

    function Get_Preferred_Height( this : access Stack_Layout ) return Float;

    function Get_Preferred_Width( this : access Stack_Layout ) return Float;

end Widgets.Stack_Layouts;
