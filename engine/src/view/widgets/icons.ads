--
-- Copyright (c) 2013-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Real_Time;                     use Ada.Real_Time;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Assets.Libraries;                  use Assets.Libraries;
with Tiles;                             use Tiles;
with Values.Lists;                      use Values.Lists;

package Icons is

    type Icon_Type is tagged private;

    -- Creates an Icon using the 'iconName' named tile in library 'libName'. If
    -- the icon can't be loaded and 'required' is True, a warning will be
    -- reported. Returns NO_ICON if the icon can't be loaded.
    function Create_Icon( libName  : String;
                          iconName : String;
                          required : Boolean := True ) return Icon_Type;

    -- Creates an Icon using the tile id 'iconId' in library 'libName'. If the
    -- icon can't be loaded and 'required' is True, a warning will be reported.
    -- Returns NO_ICON if the icon can't be loaded.
    function Create_Icon( libName  : String;
                          iconId   : Natural;
                          required : Boolean := True ) return Icon_Type;

    -- Creates an Icon using the tile id 'iconId' in library 'lib'. If the icon
    -- can't be loaded and 'required' is True, a warning will be reported.
    -- Returns NO_ICON if the icon can't be loaded.
    function Create_Icon( lib      : Library_Ptr;
                          iconId   : Natural;
                          required : Boolean := True ) return Icon_Type;

    -- Creates an Icon using the 'iconName' named tile in library 'lib'. If the
    -- icon can't be loaded and 'required' is True, a warning will be reported.
    -- Returns NO_ICON if the icon can't be loaded.
    function Create_Icon( lib      : Library_Ptr;
                          iconName : String;
                          required : Boolean := True ) return Icon_Type;

    -- Creates an Icon from a single combined "icon id" string. 'iconId' must be
    -- formatted as "libName:tileId", where "libName" is the tile library name,
    -- and "tileId" is either a tile id number or filename in the library. If
    -- the icon can't be loaded and 'required' is True, a warning will be
    -- reported. Returns NO_ICON if the icon can't be loaded.
    function Create_Icon( iconId   : String;
                          required : Boolean := True ) return Icon_Type;

    -- Returns a reference to the icon's bitmap, if any. If the icon is animated,
    -- the current frame's bitmap will be returned. If the icon could not be
    -- loaded, null will be returned.
    function Get_Bitmap( this : Icon_Type ) return A_Allegro_Bitmap;

    -- Returns the height of the icon.
    function Get_Height( this : Icon_Type ) return Natural;

    -- Returns a reference to the icon's tile library. This reference belongs to
    -- the Icon.
    function Get_Library( this : Icon_Type ) return Library_Ptr;

    -- Returns a reference to the icon's mask bitmap, if any. If the icon could
    -- not be loaded, null will be returned.
    function Get_Mask( this : Icon_Type ) return A_Allegro_Bitmap;

    -- Returns the width of the icon.
    function Get_Width( this : Icon_Type ) return Natural;

    -- Returns a reference to the icon's tile. If the icon has a looping
    -- animation, the tile originally used to create the icon will be returned.
    -- If the tile has a one-shot animation, the tile of the current frame will
    -- be returned. If the icon could not be loaded, null will be returned.
    function Get_Tile( this : Icon_Type ) return A_Tile;

    -- Updates the icon's animation, if it is animated, using 'now' as the
    -- current clock time for the animation. If the icon is using a one-shot
    -- animation, it may proceed to the next frame but it will not skip frames
    -- even if more than the frame delay has passed.
    procedure Update_Animation( this : in out Icon_Type; now : Time_Span );

    function "="( l, r : Icon_Type ) return Boolean;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Icon_Array is array (Integer range <>) of Icon_Type;

    function NO_ICON return Icon_Type;

private

    type Icon_Type is tagged
       record
           lib    : Library_Ptr;
           tile   : A_Tile := null;
           width  : Integer := 0;
           height : Integer := 0;

            -- for animation --
            anmStart   : Time_Span := Time_Span_Zero;  -- start time of the animation
            frameDelay : Time_Span := Time_Span_Zero;  -- delay between frames
            frameLoop  : List_Value;                   -- list of frames in loop
       end record;

    -- this is a function instead of a constant because it avoids elaboration
    -- issues related to initialization of the internal Library_Ptr smart pointer.
    function NO_ICON return Icon_Type is (Icon_Type'(others => <>));

end Icons;
