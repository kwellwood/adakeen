--
-- Copyright (c) 2012-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Widgets.Input_Boxes.Constraints is

    -- Returns 'newstr' only if it contains the basename of a file, without an
    -- extension or path. If 'newstr' contains any invalid filename characters,
    -- a path, or a dot for an extension, then 'curstr' will be returned.
    function Basename_Only( curstr, newstr : String ) return String;

    -- Returns 'newstr' only if it contains a filename, without a path. If
    -- 'newstr' contains any invalid filename characters or a path, then
    -- 'curstr' will be returned.
    function Filename_Only( curstr, newstr : String ) return String;

    -- Returns 'newstr' only if it contains an integer or is an empty string.
    -- If 'newstr' contains any non-digit characters then 'curstr' will be
    -- returned.
    function Int_Only( curstr, newstr : String ) return String;

    -- Returns 'newstr' only if it contains a non-negative integer or is an
    -- empty string. If 'newstr' contains any non-digit characters then 'curstr'
    -- will be returned.
    function Int_Positive_Only( curstr, newstr : String ) return String;

    -- Returns 'newstr' only if it is empty or a valid decimal number, otherwise
    -- 'curstr' will be returned.
    function Number_Only( curstr, newstr : String ) return String;

    -- Returns 'newstr' only if it is empty or a valid non-negative decimal
    -- number, otherwise 'curstr' will be returned.
    function Number_Positive_Only( curstr, newstr : String ) return String;

end Widgets.Input_Boxes.Constraints;
