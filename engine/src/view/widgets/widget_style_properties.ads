--
-- Copyright (c) 2016-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Unchecked_Deallocation;

generic
    type Prop_Type is private;
package Widget_Style_Properties is

    type Property is tagged limited private;

    -- Copies Property 'from'.
    procedure Clone( this : in out Property'Class; from : Property'Class );

    -- Frees any memory allocated for the Property.
    procedure Delete( this : in out Property'Class );

    -- Returns the value of the property for the default state (0).
    function Get_Default( this : Property'Class ) return Prop_Type;

    -- Returns the value of the property for state 'state', if it is defined. If
    -- the property only has a value defined for state 0 (the default state),
    -- then that will be returned instead. Returns True if a value was returned
    -- in 'val'.
    function Get_Value( this : Property'Class; state : Natural; val : out Prop_Type ) return Boolean;

    -- Merges all defined values from property 'from' into this. The conflicting
    -- values in 'from' will override in this.
    procedure Merge_From_Property( this : in out Property'Class; from : Property'Class );

    -- Sets the default value for this property without officially defining it.
    -- The default (state 0) value can only override when merged into another
    -- Property if it has been officially defined.
    procedure Set_Default( this : in out Property'Class; val : Prop_Type );

    -- Sets the maximum number of states the property can be defined in. This
    -- must be called before Set_Value or it will have no effect.
    procedure Set_State_Count( this : in out Property'Class; count : Natural );

    -- Sets the value of the property for state 'state' to value 'val'.
    procedure Set_Value( this : in out Property'Class; state : Natural; val : Prop_Type );

private

    type Defined_States is mod 2**32;

    type Prop_Type_Array is array (Positive range <>) of Prop_Type;
    type A_Prop_Type_Array is access all Prop_Type_Array;

    procedure Free is new Ada.Unchecked_Deallocation(Prop_Type_Array, A_Prop_Type_Array);

    ----------------------------------------------------------------------------

    type Property is tagged limited
        record
            defined    : Defined_States := 0;
            default    : Prop_Type;
            states     : A_Prop_Type_Array;
            stateCount : Natural := 0;
        end record;

end Widget_Style_Properties;
