--
-- Copyright (c) 2016-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Palette;                           use Palette;
with Values.Allegro_Colors;             use Values.Allegro_Colors;
with Values.Casting;                    use Values.Casting;
with Values.Construction;               use Values.Construction;
with Values.Strings;                    use Values.Strings;

package body Widget_Styles is

    function To_Property_Id( propertyName : String ) return Property_Id is
    begin
        if    propertyName = "align" then
            return STYLE_ALIGN;
        elsif propertyName = "color" then
            return STYLE_COLOR;
        elsif propertyName = "fontName" then
            return STYLE_FONT_NAME;
        elsif propertyName = "fontSize" then
            return STYLE_FONT_SIZE;
        elsif propertyName = "height" then
            return STYLE_HEIGHT;
        elsif propertyName = "image" then
            return STYLE_IMAGE;
        elsif propertyName = "offsetX" then
            return STYLE_OFFSET_X;
        elsif propertyName = "offsetY" then
            return STYLE_OFFSET_Y;
        elsif propertyName = "padBottom" then
            return STYLE_PAD_BOTTOM;
        elsif propertyName = "padLeft" then
            return STYLE_PAD_LEFT;
        elsif propertyName = "padRight" then
            return STYLE_PAD_RIGHT;
        elsif propertyName = "padTop" then
            return STYLE_PAD_TOP;
        elsif propertyName = "shade" then
            return STYLE_SHADE;
        elsif propertyName = "spacing" then
            return STYLE_SPACING;
        elsif propertyName = "tint" then
            return STYLE_TINT;
        elsif propertyName = "width" then
            return STYLE_WIDTH;
        elsif propertyName = "wrap" then
            return STYLE_WRAP;
        end if;
        return STYLE_PROP_UNKNOWN;
    end To_Property_Id;

    --==========================================================================

    function Create_Widget_Style( widgetType   : String;
                                  name         : String;
                                  elementCount : Natural ) return A_Widget_Style is
        this : constant A_Widget_Style := new Widget_Style(elementCount);
    begin
        this.Construct( widgetType, name );
        return this;
    end Create_Widget_Style;

    ----------------------------------------------------------------------------

    overriding
    function Clone( this : Widget_Style ) return A_Element_Style is
        copy : constant A_Widget_Style := Create_Widget_Style( To_String( this.widgetType ),
                                                               To_String( this.name ),
                                                               this.elementCount );
    begin
        for i in this.elements'Range loop
            copy.elements(i) := this.elements(i).all.Clone;
        end loop;
        return A_Element_Style(copy);
    end Clone;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Widget_Style; widgetType : String; name : String ) is
    begin
        Element_Style(this.all).Construct;
        this.widgetType := To_Unbounded_String( widgetType );
        this.name := To_Unbounded_String( name );
    end Construct;

    ----------------------------------------------------------------------------

    procedure Copy_From( this   : in out Widget_Style'Class;
                         source : A_Widget_Style ) is
    begin
        if source = null or else source.elementCount /= this.elementCount then
            return;
        end if;
        pragma Assert( source.elementCount = this.elementCount );
        pragma Assert( source.widgetType = this.widgetType );
        this.Copy_From_Element( source.all );
    end Copy_From;

    ----------------------------------------------------------------------------

    overriding
    procedure Copy_From_Element( this   : in out Widget_Style;
                                 source : Element_Style'Class ) is
    begin
        this.name := Widget_Style(source).name;
        for e in this.elements'Range loop
            this.elements(e).Copy_From_Element( Widget_Style(source).elements(e).all );
        end loop;
    end Copy_From_Element;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Widget_Style ) is
    begin
        for i in this.elements'Range loop
            Delete( A_Limited_Object(this.elements(i)) );
        end loop;
        Limited_Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Get_Property( this     : Widget_Style'Class;
                           element  : Element_Index;
                           property : Property_Id;
                           state    : Widget_State := DEFAULT_STATE ) return Value is
    begin
        case property is
            when STYLE_ALIGN      => return Create( Integer(this.Get_Align( element, state )'Enum_Rep) );
            when STYLE_COLOR      => return Create( this.Get_Color( element, state ) );
            when STYLE_FONT_NAME  => return Create( this.Get_Font_Name( element, state ) );
            when STYLE_FONT_SIZE  => return Create( this.Get_Font_Size( element, state ) );
            when STYLE_HEIGHT     => return Create( this.Get_Height( element, state ) );
            when STYLE_IMAGE      => return Create( this.Get_Image_Name( element, state ) );
            when STYLE_OFFSET_X   => return Create( this.Get_Offset_X( element, state ) );
            when STYLE_OFFSET_Y   => return Create( this.Get_Offset_Y( element, state ) );
            when STYLE_PAD_BOTTOM => return Create( this.Get_Pad_Bottom( element, state ) );
            when STYLE_PAD_LEFT   => return Create( this.Get_Pad_Left( element, state ) );
            when STYLE_PAD_RIGHT  => return Create( this.Get_Pad_Right( element, state ) );
            when STYLE_PAD_TOP    => return Create( this.Get_Pad_Top( element, state ) );
            when STYLE_SHADE      => return Create( this.Get_Shade( element, state ) );
            when STYLE_SPACING    => return Create( this.Get_Spacing( element, state ) );
            when STYLE_TINT       => return Create( this.Get_Tint( element, state ) );
            when STYLE_WIDTH      => return Create( this.Get_Width( element, state ) );
            when STYLE_WRAP       => return Create( Integer(this.Get_Wrap( element, state )'Enum_Rep) );
            when others           => return Null_Value;
        end case;
    end Get_Property;

    ----------------------------------------------------------------------------

    function Get_Align( this    : Widget_Style'Class;
                        element : Element_Index;
                        state   : Widget_State := DEFAULT_STATE ) return Align_Type is
        result  : Align_Type := Left;
        defined : Boolean;
    begin
        for s in reverse 0..state loop
            if (s and state) = s or s = DEFAULT_STATE then
                this.elements(element).Get_Align( s, defined, result );
                exit when defined;
            end if;
        end loop;
        return result;
    end Get_Align;

    ----------------------------------------------------------------------------

    function Get_Color( this    : Widget_Style'Class;
                        element : Element_Index;
                        state   : Widget_State := DEFAULT_STATE ) return Allegro_Color is
        result  : Allegro_Color := White;
        defined : Boolean;
    begin
        for s in reverse 0..state loop
            if (s and state) = s or s = DEFAULT_STATE then
                this.elements(element).Get_Color( s, defined, result );
                exit when defined;
            end if;
        end loop;
        return result;
    end Get_Color;

    ----------------------------------------------------------------------------

    function Get_Font( this    : Widget_Style'Class;
                       element : Element_Index;
                       state   : Widget_State := DEFAULT_STATE ) return A_Font is
        result  : A_Font := null;
        defined : Boolean;
    begin
        for s in reverse 0..state loop
            if (s and state) = s or s = DEFAULT_STATE then
                this.elements(element).Get_Font( s, defined, result );
                exit when defined;
            end if;
        end loop;
        return result;
    end Get_Font;

    ----------------------------------------------------------------------------

    function Get_Font_Name( this    : Widget_Style'Class;
                            element : Element_Index;
                            state   : Widget_State := DEFAULT_STATE ) return String is
        result  : Unbounded_String := To_Unbounded_String( "standard" );
        defined : Boolean;
    begin
        for s in reverse 0..state loop
            if (s and state) = s or s = DEFAULT_STATE then
                this.elements(element).Get_Font_Name( s, defined, result );
                exit when defined;
            end if;
        end loop;
        return To_String( result );
    end Get_Font_Name;

    ----------------------------------------------------------------------------

    function Get_Font_Size( this    : Widget_Style'Class;
                            element : Element_Index;
                            state   : Widget_State := DEFAULT_STATE ) return Integer is
        result  : Integer := 12;
        defined : Boolean;
    begin
        for s in reverse 0..state loop
            if (s and state) = s or s = DEFAULT_STATE then
                this.elements(element).Get_Font_Size( s, defined, result );
                exit when defined;
            end if;
        end loop;
        return result;
    end Get_Font_Size;

    ----------------------------------------------------------------------------

    function Get_Height( this    : Widget_Style'Class;
                         element : Element_Index;
                         state   : Widget_State := DEFAULT_STATE ) return Float is
        result  : Float := 0.0;
        defined : Boolean;
    begin
        for s in reverse 0..state loop
            if (s and state) = s or s = DEFAULT_STATE then
                this.elements(element).Get_Height( s, defined, result );
                exit when defined;
            end if;
        end loop;
        return result;
    end Get_Height;

    ----------------------------------------------------------------------------

    function Get_Image( this    : Widget_Style'Class;
                        element : Element_Index;
                        state   : Widget_State := DEFAULT_STATE ) return Icon_Type is
        result  : Icon_Type := NO_ICON;
        defined : Boolean;
    begin
        for s in reverse 0..state loop
            if (s and state) = s or s = DEFAULT_STATE then
                this.elements(element).Get_Image( s, defined, result );
                exit when defined;
            end if;
        end loop;
        return result;
    end Get_Image;

    ----------------------------------------------------------------------------

    function Get_Image_Name( this    : Widget_Style'Class;
                             element : Element_Index;
                             state   : Widget_State := DEFAULT_STATE ) return String is
        result  : Unbounded_String;
        defined : Boolean;
    begin
        for s in reverse 0..state loop
            if (s and state) = s or s = DEFAULT_STATE then
                this.elements(element).Get_Image_Name( s, defined, result );
                exit when defined;
            end if;
        end loop;
        return To_String( result );
    end Get_Image_Name;

    ----------------------------------------------------------------------------

    function Get_Name( this : Widget_Style'Class ) return String is (To_String( this.name ));

    ----------------------------------------------------------------------------

    function Get_Offset_X( this    : Widget_Style'Class;
                           element : Element_Index;
                           state   : Widget_State := DEFAULT_STATE ) return Float is
        result  : Float := 0.0;
        defined : Boolean;
    begin
        for s in reverse 0..state loop
            if (s and state) = s or s = DEFAULT_STATE then
                this.elements(element).Get_Offset_X( s, defined, result );
                exit when defined;
            end if;
        end loop;
        return result;
    end Get_Offset_X;

    ----------------------------------------------------------------------------

    function Get_Offset_Y( this    : Widget_Style'Class;
                           element : Element_Index;
                           state   : Widget_State := DEFAULT_STATE ) return Float is
        result  : Float := 0.0;
        defined : Boolean;
    begin
        for s in reverse 0..state loop
            if (s and state) = s or s = DEFAULT_STATE then
                this.elements(element).Get_Offset_Y( s, defined, result );
                exit when defined;
            end if;
        end loop;
        return result;
    end Get_Offset_Y;

    ----------------------------------------------------------------------------

    function Get_Pad_Bottom( this    : Widget_Style'Class;
                             element : Element_Index;
                             state   : Widget_State := DEFAULT_STATE ) return Float is
        result  : Float := 0.0;
        defined : Boolean;
    begin
        for s in reverse 0..state loop
            if (s and state) = s or s = DEFAULT_STATE then
                this.elements(element).Get_Pad_Bottom( s, defined, result );
                exit when defined;
            end if;
        end loop;
        return result;
    end Get_Pad_Bottom;

    ----------------------------------------------------------------------------

    function Get_Pad_Left( this    : Widget_Style'Class;
                           element : Element_Index;
                           state   : Widget_State := DEFAULT_STATE ) return Float is
        result  : Float := 0.0;
        defined : Boolean;
    begin
        for s in reverse 0..state loop
            if (s and state) = s or s = DEFAULT_STATE then
                this.elements(element).Get_Pad_Left( s, defined, result );
                exit when defined;
            end if;
        end loop;
        return result;
    end Get_Pad_Left;

    ----------------------------------------------------------------------------

    function Get_Pad_Right( this    : Widget_Style'Class;
                            element : Element_Index;
                            state   : Widget_State := DEFAULT_STATE ) return Float is
        result  : Float := 0.0;
        defined : Boolean;
    begin
        for s in reverse 0..state loop
            if (s and state) = s or s = DEFAULT_STATE then
                this.elements(element).Get_Pad_Right( s, defined, result );
                exit when defined;
            end if;
        end loop;
        return result;
    end Get_Pad_Right;

    ----------------------------------------------------------------------------

    function Get_Pad_Top( this    : Widget_Style'Class;
                          element : Element_Index;
                          state   : Widget_State := DEFAULT_STATE ) return Float is
        result  : Float := 0.0;
        defined : Boolean;
    begin
        for s in reverse 0..state loop
            if (s and state) = s or s = DEFAULT_STATE then
                this.elements(element).Get_Pad_Top( s, defined, result );
                exit when defined;
            end if;
        end loop;
        return result;
    end Get_Pad_Top;

    ----------------------------------------------------------------------------

    function Get_Shade( this    : Widget_Style'Class;
                        element : Element_Index;
                        state   : Widget_State := DEFAULT_STATE ) return Float is
        result  : Float := 1.0;
        defined : Boolean;
    begin
        for s in reverse 0..state loop
            if (s and state) = s or s = DEFAULT_STATE then
                this.elements(element).Get_Shade( s, defined, result );
                exit when defined;
            end if;
        end loop;
        return result;
    end Get_Shade;

    ----------------------------------------------------------------------------

    function Get_Spacing( this    : Widget_Style'Class;
                          element : Element_Index;
                          state   : Widget_State := DEFAULT_STATE ) return Float is
        result  : Float := 0.0;
        defined : Boolean;
    begin
        for s in reverse 0..state loop
            if (s and state) = s or s = DEFAULT_STATE then
                this.elements(element).Get_Spacing( s, defined, result );
                exit when defined;
            end if;
        end loop;
        return result;
    end Get_Spacing;

    ----------------------------------------------------------------------------

    function Get_Style( this    : Widget_Style'Class;
                        element : Element_Index ) return A_Widget_Style is (A_Widget_Style(this.elements(element)));

    ----------------------------------------------------------------------------

    function Get_Tint( this    : Widget_Style'Class;
                       element : Element_Index;
                       state   : Widget_State := DEFAULT_STATE ) return Allegro_Color is
        result  : Allegro_Color := White;
        defined : Boolean;
    begin
        for s in reverse 0..state loop
            if (s and state) = s or s = DEFAULT_STATE then
                this.elements(element).Get_Tint( s, defined, result );
                exit when defined;
            end if;
        end loop;
        return result;
    end Get_Tint;

    ----------------------------------------------------------------------------

    function Get_Widget_Type( this : Widget_Style'Class ) return String is (To_String( this.widgetType ));

    ----------------------------------------------------------------------------

    function Get_Width( this    : Widget_Style'Class;
                        element : Element_Index;
                        state   : Widget_State := DEFAULT_STATE ) return Float is
        result  : Float := 0.0;
        defined : Boolean;
    begin
        for s in reverse 0..state loop
            if (s and state) = s or s = DEFAULT_STATE then
                this.elements(element).Get_Width( s, defined, result );
                exit when defined;
            end if;
        end loop;
        return result;
    end Get_Width;

    ----------------------------------------------------------------------------

    function Get_Wrap( this    : Widget_Style'Class;
                       element : Element_Index;
                       state   : Widget_State := DEFAULT_STATE ) return Wrap_Mode is
        result  : Wrap_Mode := None;
        defined : Boolean;
    begin
        for s in reverse 0..state loop
            if (s and state) = s or s = DEFAULT_STATE then
                this.elements(element).Get_Wrap( s, defined, result );
                exit when defined;
            end if;
        end loop;
        return result;
    end Get_Wrap;

    ----------------------------------------------------------------------------

    procedure Merge_From( this   : in out Widget_Style'Class;
                          source : A_Widget_Style ) is
    begin
        if source = null or else source.elementCount /= this.elementCount then
            return;
        end if;

        -- append the name of the source style
        this.Merge_From_Element( source.all );
        if Length( source.name ) > 0 then
            if Length( this.name ) > 0 then
                Append( this.name, " " );
            end if;
            Append( this.name, source.name );
        end if;
    end Merge_From;

    ----------------------------------------------------------------------------

    overriding
    procedure Merge_From_Element( this   : in out Widget_Style;
                                  source : Element_Style'Class ) is
    begin
        for e in this.elements'Range loop
            this.elements(e).Merge_From_Element( Widget_Style(source).elements(e).all );
        end loop;
    end Merge_From_Element;

    ----------------------------------------------------------------------------

    function Set_Property( this     : in out Widget_Style'Class;
                           element  : Element_Index;
                           property : Property_Id;
                           state    : Widget_State;
                           val      : Value'Class ) return Boolean
    is (this.elements(element).Set_Property( property, state, val ));

    ----------------------------------------------------------------------------

    procedure Set_Property( this     : in out Widget_Style'Class;
                            element  : Element_Index;
                            property : Property_Id;
                            state    : Widget_State;
                            val      : Value'Class ) is
        ignored : Boolean;
        pragma Warnings( Off, ignored );
    begin
        ignored := this.Set_Property( element, property, state, val );
    end Set_Property;

    ----------------------------------------------------------------------------

    procedure Update_Animation( this    : in out Widget_Style'Class;
                                element : Element_Index;
                                state   : Widget_State;
                                now     : Time_Span ) is
    begin
        this.elements(element).Update_Animation( state, now );
    end Update_Animation;

    ----------------------------------------------------------------------------

    function Clone( this : A_Widget_Style ) return A_Widget_Style is
    begin
        if this /= null then
            return A_Widget_Style(this.all.Clone);
        end if;
        return null;
    end Clone;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Widget_Style ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

    --==========================================================================

    not overriding
    procedure Construct( this : access Element_Style; stateCount : Natural ) is
    begin
        Limited_Object(this.all).Construct;
        this.stateCount := stateCount;
    end Construct;

    ----------------------------------------------------------------------------

    not overriding
    procedure Get_Align( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Align_Type ) is
        pragma Unreferenced( this, state, property );
    begin
        defined := False;
    end Get_Align;

    ----------------------------------------------------------------------------

    not overriding
    procedure Get_Color( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Allegro_Color ) is
        pragma Unreferenced( this, state, property );
    begin
        defined := False;
    end Get_Color;

    ----------------------------------------------------------------------------

    not overriding
    procedure Get_Font( this : in out Element_Style; state : Widget_State; defined : out Boolean; property : in out A_Font ) is
        pragma Unreferenced( this, state, property );
    begin
        defined := False;
    end Get_Font;

    ----------------------------------------------------------------------------

    not overriding
    procedure Get_Font_Name( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Unbounded_String ) is
        pragma Unreferenced( this, state, property );
    begin
        defined := False;
    end Get_Font_Name;

    ----------------------------------------------------------------------------

    not overriding
    procedure Get_Font_Size( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Integer ) is
        pragma Unreferenced( this, state, property );
    begin
        defined := False;
    end Get_Font_Size;

    ----------------------------------------------------------------------------

    not overriding
    procedure Get_Height( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Float ) is
        pragma Unreferenced( this, state, property );
    begin
        defined := False;
    end Get_Height;

    ----------------------------------------------------------------------------

    not overriding
    procedure Get_Image( this : in out Element_Style; state : Widget_State; defined : out Boolean; property : in out Icon_Type ) is
        pragma Unreferenced( this, state, property );
    begin
        defined := False;
    end Get_Image;

    ----------------------------------------------------------------------------

    not overriding
    procedure Get_Image_Name( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Unbounded_String ) is
        pragma Unreferenced( this, state, property );
    begin
        defined := False;
    end Get_Image_Name;

    ----------------------------------------------------------------------------

    not overriding
    procedure Get_Offset_X( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Float ) is
        pragma Unreferenced( this, state, property );
    begin
        defined := False;
    end Get_Offset_X;

    ----------------------------------------------------------------------------

    not overriding
    procedure Get_Offset_Y( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Float ) is
        pragma Unreferenced( this, state, property );
    begin
        defined := False;
    end Get_Offset_Y;

    ----------------------------------------------------------------------------

    not overriding
    procedure Get_Pad_Bottom( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Float ) is
        pragma Unreferenced( this, state, property );
    begin
        defined := False;
    end Get_Pad_Bottom;

    ----------------------------------------------------------------------------

    not overriding
    procedure Get_Pad_Left( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Float ) is
        pragma Unreferenced( this, state, property );
    begin
        defined := False;
    end Get_Pad_Left;

    ----------------------------------------------------------------------------

    not overriding
    procedure Get_Pad_Right( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Float ) is
        pragma Unreferenced( this, state, property );
    begin
        defined := False;
    end Get_Pad_Right;

    ----------------------------------------------------------------------------

    not overriding
    procedure Get_Pad_Top( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Float ) is
        pragma Unreferenced( this, state, property );
    begin
        defined := False;
    end Get_Pad_Top;

    ----------------------------------------------------------------------------

    not overriding
    procedure Get_Shade( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Float ) is
        pragma Unreferenced( this, state, property );
    begin
        defined := False;
    end Get_Shade;

    ----------------------------------------------------------------------------

    not overriding
    procedure Get_Spacing( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Float ) is
        pragma Unreferenced( this, state, property );
    begin
        defined := False;
    end Get_Spacing;

    ----------------------------------------------------------------------------

    not overriding
    procedure Get_Tint( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Allegro_Color ) is
        pragma Unreferenced( this, state, property );
    begin
        defined := False;
    end Get_Tint;

    ----------------------------------------------------------------------------

    not overriding
    procedure Get_Width( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Float ) is
        pragma Unreferenced( this, state, property );
    begin
        defined := False;
    end Get_Width;

    ----------------------------------------------------------------------------

    not overriding
    procedure Get_Wrap( this : Element_Style; state : Widget_State; defined : out Boolean; property : in out Wrap_Mode ) is
        pragma Unreferenced( this, state, property );
    begin
        defined := False;
    end Get_Wrap;

    --==========================================================================

    function Create_Area_Style( stateCount : Natural ) return A_Element_Style is
        this : constant A_Element_Style := new Area_Style;
    begin
        this.Construct( stateCount );
        return this;
    end Create_Area_Style;

    ----------------------------------------------------------------------------

    overriding
    function Clone( this : Area_Style ) return A_Element_Style is
        copy : constant A_Area_Style := A_Area_Style(Create_Area_Style( this.stateCount ));
    begin
        copy.Construct( this.stateCount );

        copy.align.Clone(this.align );
        copy.color.Clone( this.color );
        copy.image.Clone( this.image );
        copy.padBottom.Clone( this.padBottom );
        copy.padLeft.Clone( this.padLeft );
        copy.padRight.Clone( this.padRight );
        copy.padTop.Clone( this.padTop );
        copy.shade.Clone( this.shade );
        copy.spacing.Clone( this.spacing );
        copy.tint.Clone( this.tint );

        return A_Element_Style(copy);
    end Clone;

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access Area_Style; stateCount : Natural ) is
    begin
        Element_Style(this.all).Construct( stateCount );

        this.align.Set_State_Count( this.stateCount );
        this.color.Set_State_Count( this.stateCount );
        this.image.Set_State_Count( this.stateCount );
        this.padBottom.Set_State_Count( this.stateCount );
        this.padLeft.Set_State_Count( this.stateCount );
        this.padRight.Set_State_Count( this.stateCount );
        this.padTop.Set_State_Count( this.stateCount );
        this.shade.Set_State_Count( this.stateCount );
        this.spacing.Set_State_Count( this.stateCount );
        this.tint.Set_State_Count( this.stateCount );

        this.align.Set_Default( Left );
        this.color.Set_Default( Transparent );
        this.image.Set_Default( To_Unbounded_String( "" ) );
        this.padBottom.Set_Default( 0.0 );
        this.padLeft.Set_Default( 0.0 );
        this.padRight.Set_Default( 0.0 );
        this.padTop.Set_Default( 0.0 );
        this.shade.Set_Default( 1.0 );
        this.spacing.Set_Default( 2.0 );
        this.tint.Set_Default( White );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Copy_From_Element( this   : in out Area_Style;
                                 source : Element_Style'Class ) is
    begin
        pragma Assert( source in Area_Style'Class );
        this.color.Clone( Area_Style(source).color );
        this.align.Clone( Area_Style(source).align );
        this.image.Clone( Area_Style(source).image );
        this.icon := NO_ICON;
        this.iconState := INVALID_STATE;
        this.shade.Clone( Area_Style(source).shade );
        this.spacing.Clone( Area_Style(source).spacing );
        this.padBottom.Clone( Area_Style(source).padBottom );
        this.padLeft.Clone( Area_Style(source).padLeft );
        this.padRight.Clone( Area_Style(source).padRight );
        this.padTop.Clone( Area_Style(source).padTop );
        this.tint.Clone( Area_Style(source).tint );
    end Copy_From_Element;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Area_Style ) is
    begin
        this.align.Delete;
        this.color.Delete;
        this.image.Delete;
        this.padBottom.Delete;
        this.padLeft.Delete;
        this.padRight.Delete;
        this.padTop.Delete;
        this.shade.Delete;
        this.spacing.Delete;
        this.tint.Delete;

        Element_Style(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    overriding
    procedure Get_Align( this : Area_Style; state : Widget_State; defined : out Boolean; property : in out Align_Type ) is
    begin
        defined := this.align.Get_Value( Natural(state), property );
    end Get_Align;

    ----------------------------------------------------------------------------

    overriding
    procedure Get_Color( this : Area_Style; state : Widget_State; defined : out Boolean; property : in out Allegro_Color ) is
    begin
        defined := this.color.Get_Value( Natural(state), property );
    end Get_Color;

    ----------------------------------------------------------------------------

    overriding
    procedure Get_Image( this : in out Area_Style; state : Widget_State; defined : out Boolean; property : in out Icon_Type ) is
        image : Unbounded_String;
    begin
        this.Get_Image_Name( state, defined, image );
        if defined then
            if state /= this.iconState then
                if Length( image ) > 0 then
                    this.icon := Create_Icon( To_String( image ) );
                else
                    this.icon := NO_ICON;
                end if;
                this.iconState := state;
            end if;
            property := this.icon;
        end if;
    end Get_Image;

    ----------------------------------------------------------------------------

    overriding
    procedure Get_Image_Name( this : Area_Style; state : Widget_State; defined : out Boolean; property : in out Unbounded_String ) is
    begin
        defined := this.image.Get_Value( Natural(state), property );
    end Get_Image_Name;

    ----------------------------------------------------------------------------

    overriding
    procedure Get_Pad_Bottom( this : Area_Style; state : Widget_State; defined : out Boolean; property : in out Float ) is
    begin
        defined := this.padBottom.Get_Value( Natural(state), property );
    end Get_Pad_Bottom;

    ----------------------------------------------------------------------------

    overriding
    procedure Get_Pad_Left( this : Area_Style; state : Widget_State; defined : out Boolean; property : in out Float ) is
    begin
        defined := this.padLeft.Get_Value( Natural(state), property );
    end Get_Pad_Left;

    ----------------------------------------------------------------------------

    overriding
    procedure Get_Pad_Right( this : Area_Style; state : Widget_State; defined : out Boolean; property : in out Float ) is
    begin
        defined := this.padRight.Get_Value( Natural(state), property );
    end Get_Pad_Right;

    ----------------------------------------------------------------------------

    overriding
    procedure Get_Pad_Top( this : Area_Style; state : Widget_State; defined : out Boolean; property : in out Float ) is
    begin
        defined := this.padTop.Get_Value( Natural(state), property );
    end Get_Pad_Top;

    ----------------------------------------------------------------------------

    overriding
    procedure Get_Shade( this : Area_Style; state : Widget_State; defined : out Boolean; property : in out Float ) is
    begin
        defined := this.shade.Get_Value( Natural(state), property );
    end Get_Shade;

    ----------------------------------------------------------------------------

    overriding
    procedure Get_Spacing( this : Area_Style; state : Widget_State; defined : out Boolean; property : in out Float ) is
    begin
        defined := this.spacing.Get_Value( Natural(state), property );
    end Get_Spacing;

    ----------------------------------------------------------------------------

    overriding
    procedure Get_Tint( this : Area_Style; state : Widget_State; defined : out Boolean; property : in out Allegro_Color ) is
    begin
        defined := this.tint.Get_Value( Natural(state), property );
    end Get_Tint;

    ----------------------------------------------------------------------------

    overriding
    function Set_Property( this     : in out Area_Style;
                           property : Property_Id;
                           state    : Widget_State;
                           val      : Value'Class ) return Boolean is

        function Cast_Align is new Cast_Enum(Align_Type);

        success : Boolean := True;
    begin
        case property is
            when STYLE_ALIGN      => this.align.Set_Value( Natural(state), Cast_Align( val, this.align.Get_Default ) );
            when STYLE_COLOR      => this.color.Set_Value( Natural(state), Coerce_Color( val, this.color.Get_Default ) );
            when STYLE_IMAGE      => this.image.Set_Value( Natural(state), Cast_Unbounded_String( val, this.image.Get_Default ) );
                                     this.iconState := INVALID_STATE;
            when STYLE_PAD_BOTTOM => this.padBottom.Set_Value( Natural(state), Cast_Float( val, this.padBottom.Get_Default ) );
            when STYLE_PAD_LEFT   => this.padLeft.Set_Value( Natural(state), Cast_Float( val, this.padLeft.Get_Default ) );
            when STYLE_PAD_RIGHT  => this.padRight.Set_Value( Natural(state), Cast_Float( val, this.padRight.Get_Default ) );
            when STYLE_PAD_TOP    => this.padTop.Set_Value( Natural(state), Cast_Float( val, this.padTop.Get_Default ) );
            when STYLE_SHADE      => this.shade.Set_Value( Natural(state), Cast_Float( val, this.shade.Get_Default ) );
            when STYLE_SPACING    => this.spacing.Set_Value( Natural(state), Cast_Float( val, this.spacing.Get_Default ) );
            when STYLE_TINT       => this.tint.Set_Value( Natural(state), Coerce_Color( val, this.tint.Get_Default ) );
            when others           => success := False;
        end case;
        return success;
    end Set_Property;

    ----------------------------------------------------------------------------

    overriding
    procedure Merge_From_Element( this   : in out Area_Style;
                                  source : Element_Style'Class ) is
    begin
        this.color.Merge_From_Property( Area_Style(source).color );
        this.align.Merge_From_Property( Area_Style(source).align );
        this.image.Merge_From_Property( Area_Style(source).image );
        this.icon := NO_ICON;
        this.iconState := INVALID_STATE;
        this.shade.Merge_From_Property( Area_Style(source).shade );
        this.spacing.Merge_From_Property( Area_Style(source).spacing );
        this.padBottom.Merge_From_Property( Area_Style(source).padBottom );
        this.padLeft.Merge_From_Property( Area_Style(source).padLeft );
        this.padRight.Merge_From_Property( Area_Style(source).padRight );
        this.padTop.Merge_From_Property( Area_Style(source).padTop );
        this.tint.Merge_From_Property( Area_Style(source).tint );
    end Merge_From_Element;

    ----------------------------------------------------------------------------

    overriding
    procedure Update_Animation( this  : in out Area_Style;
                                state : Widget_State;
                                now   : Time_Span ) is
    begin
        if state = this.iconState then
            this.icon.Update_Animation( now );
        end if;
    end Update_Animation;

    --==========================================================================

    function Create_Icon_Style( stateCount : Natural ) return A_Element_Style is
        this : constant A_Element_Style := new Icon_Style;
    begin
        this.Construct( stateCount );
        return this;
    end Create_Icon_Style;

    ----------------------------------------------------------------------------

    overriding
    function Clone( this : Icon_Style ) return A_Element_Style is
        copy : constant A_Icon_Style := A_Icon_Style(Create_Icon_Style( this.stateCount ));
    begin
        copy.Construct( this.stateCount );

        copy.height.Clone( this.height );
        copy.image.Clone( this.image );
        copy.offsetX.Clone( this.offsetX );
        copy.offsetY.Clone( this.offsetY );
        copy.shade.Clone( this.shade );
        copy.tint.Clone( this.tint );
        copy.width.Clone( this.width );

        return A_Element_Style(copy);
    end Clone;

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access Icon_Style; stateCount : Natural ) is
    begin
        Element_Style(this.all).Construct( stateCount );

        this.height.Set_State_Count( this.stateCount );
        this.image.Set_State_Count( this.stateCount );
        this.offsetX.Set_State_Count( this.stateCount );
        this.offsetY.Set_State_Count( this.stateCount );
        this.shade.Set_State_Count( this.stateCount );
        this.tint.Set_State_Count( this.stateCount );
        this.width.Set_State_Count( this.stateCount );

        this.height.Set_Default( 0.0 );
        this.image.Set_Default( To_Unbounded_String( "" ) );
        this.offsetX.Set_Default( 0.0 );
        this.offsetY.Set_Default( 0.0 );
        this.shade.Set_Default( 1.0 );
        this.tint.Set_Default( White );
        this.width.Set_Default( 0.0 );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Copy_From_Element( this   : in out Icon_Style;
                                 source : Element_Style'Class ) is
    begin
        pragma Assert( source in Icon_Style'Class );
        this.height.Clone( Icon_Style(source).height );
        this.image.Clone( Icon_Style(source).image );
        this.icon := NO_ICON;
        this.iconState := INVALID_STATE;
        this.offsetX.Clone( Icon_Style(source).offsetX );
        this.offsetY.Clone( Icon_Style(source).offsetY );
        this.shade.Clone( Icon_Style(source).shade );
        this.tint.Clone( Icon_Style(source).tint );
        this.width.Clone( Icon_Style(source).width );
    end Copy_From_Element;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Icon_Style ) is
    begin
        this.height.Delete;
        this.image.Delete;
        this.offsetX.Delete;
        this.offsetY.Delete;
        this.shade.Delete;
        this.tint.Delete;
        this.width.Delete;

        Element_Style(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    overriding
    procedure Get_Height( this : Icon_Style; state : Widget_State; defined : out Boolean; property : in out Float ) is
    begin
        defined := this.height.Get_Value( Natural(state), property );
    end Get_Height;

    ----------------------------------------------------------------------------

    overriding
    procedure Get_Image( this : in out Icon_Style; state : Widget_State; defined : out Boolean; property : in out Icon_Type ) is
        image : Unbounded_String;
    begin
        this.Get_Image_Name( state, defined, image );
        if defined then
            if state /= this.iconState then
                if Length( image ) > 0 then
                    this.icon := Create_Icon( To_String( image ) );
                else
                    this.icon := NO_ICON;
                end if;
                this.iconState := state;
            end if;
            property := this.icon;
        end if;
    end Get_Image;

    ----------------------------------------------------------------------------

    overriding
    procedure Get_Image_Name( this : Icon_Style; state : Widget_State; defined : out Boolean; property : in out Unbounded_String ) is
    begin
        defined := this.image.Get_Value( Natural(state), property );
    end Get_Image_Name;

    ----------------------------------------------------------------------------

    overriding
    procedure Get_Offset_X( this : Icon_Style; state : Widget_State; defined : out Boolean; property : in out Float ) is
    begin
        defined := this.offsetX.Get_Value( Natural(state), property );
    end Get_Offset_X;

    ----------------------------------------------------------------------------

    overriding
    procedure Get_Offset_Y( this : Icon_Style; state : Widget_State; defined : out Boolean; property : in out Float ) is
    begin
        defined := this.offsetY.Get_Value( Natural(state), property );
    end Get_Offset_Y;

    ----------------------------------------------------------------------------

    overriding
    procedure Get_Shade( this : Icon_Style; state : Widget_State; defined : out Boolean; property : in out Float ) is
    begin
        defined := this.shade.Get_Value( Natural(state), property );
    end Get_Shade;

    ----------------------------------------------------------------------------

    overriding
    procedure Get_Tint( this : Icon_Style; state : Widget_State; defined : out Boolean; property : in out Allegro_Color ) is
    begin
        defined := this.tint.Get_Value( Natural(state), property );
    end Get_Tint;

    ----------------------------------------------------------------------------

    overriding
    procedure Get_Width( this : Icon_Style; state : Widget_State; defined : out Boolean; property : in out Float ) is
    begin
        defined := this.width.Get_Value( Natural(state), property );
    end Get_Width;

    ----------------------------------------------------------------------------

    overriding
    procedure Merge_From_Element( this   : in out Icon_Style;
                                  source : Element_Style'Class ) is
    begin
        pragma Assert( source in Icon_Style'Class );
        this.height.Merge_From_Property( Icon_Style(source).height );
        this.image.Merge_From_Property( Icon_Style(source).image );
        this.icon := NO_ICON;
        this.iconState := INVALID_STATE;
        this.offsetX.Merge_From_Property( Icon_Style(source).offsetX );
        this.offsetY.Merge_From_Property( Icon_Style(source).offsetY );
        this.shade.Merge_From_Property( Icon_Style(source).shade );
        this.tint.Merge_From_Property( Icon_Style(source).tint );
        this.width.Merge_From_Property( Icon_Style(source).width );
    end Merge_From_Element;

    ----------------------------------------------------------------------------

    overriding
    function Set_Property( this     : in out Icon_Style;
                           property : Property_Id;
                           state    : Widget_State;
                           val      : Value'Class ) return Boolean is
        success : Boolean := True;
    begin
        case property is
            when STYLE_HEIGHT   => this.height.Set_Value( Natural(state), Cast_Float( val, this.height.Get_Default ) );
            when STYLE_IMAGE    => this.image.Set_Value( Natural(state), Cast_Unbounded_String( val, this.image.Get_Default ) );
                                   this.iconState := INVALID_STATE;
            when STYLE_OFFSET_X => this.offsetX.Set_Value( Natural(state), Cast_Float( val, this.offsetX.Get_Default ) );
            when STYLE_OFFSET_Y => this.offsetY.Set_Value( Natural(state), Cast_Float( val, this.offsetY.Get_Default ) );
            when STYLE_SHADE    => this.shade.Set_Value( Natural(state), Cast_Float( val, this.shade.Get_Default ) );
            when STYLE_TINT     => this.tint.Set_Value( Natural(state), Coerce_Color( val, this.tint.Get_Default ) );
            when STYLE_WIDTH    => this.width.Set_Value( Natural(state), Cast_Float( val, this.width.Get_Default ) );
            when others         => success := False;
        end case;
        return success;
    end Set_Property;

    ----------------------------------------------------------------------------

    overriding
    procedure Update_Animation( this  : in out Icon_Style;
                                state : Widget_State;
                                now   : Time_Span ) is
    begin
        if state = this.iconState then
            this.icon.Update_Animation( now );
        end if;
    end Update_Animation;

    --==========================================================================

    function Create_Text_Style( stateCount : Natural ) return A_Element_Style is
        this : constant A_Element_Style := new Text_Style;
    begin
        this.Construct( stateCount );
        return this;
    end Create_Text_Style;

    ----------------------------------------------------------------------------

    overriding
    function Clone( this : Text_Style ) return A_Element_Style is
        copy : constant A_Text_Style := A_Text_Style(Create_Text_Style( this.stateCount ));
    begin
        copy.Construct( this.stateCount );
        copy.color.Clone( this.color );
        copy.fontName.Clone( this.fontName );
        copy.fontSize.Clone( this.fontSize );
        copy.offsetX.Clone( this.offsetX );
        copy.offsetY.Clone( this.offsetY );
        copy.shade.Clone( this.shade );
        copy.wrap.Clone( this.wrap );
        return A_Element_Style(copy);
    end Clone;

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access Text_Style; stateCount : Natural ) is
    begin
        Element_Style(this.all).Construct( stateCount );

        this.color.Set_State_Count( this.stateCount );
        this.fontName.Set_State_Count( this.stateCount );
        this.fontSize.Set_State_Count( this.stateCount );
        this.offsetX.Set_State_Count( this.stateCount );
        this.offsetY.Set_State_Count( this.stateCount );
        this.shade.Set_State_Count( this.stateCount );
        this.wrap.Set_State_Count( this.stateCount );

        this.color.Set_Default( Black );
        this.fontName.Set_Default( To_Unbounded_String( "standard" ) );
        this.fontSize.Set_Default( 12 );
        this.offsetX.Set_Default( 0.0 );
        this.offsetY.Set_Default( 0.0 );
        this.shade.Set_Default( 1.0 );
        this.wrap.Set_Default( None );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Copy_From_Element( this   : in out Text_Style;
                                 source : Element_Style'Class ) is
    begin
        pragma Assert( source in Text_Style'Class );
        this.color.Clone( Text_Style(source).color );
        this.fontName.Clone( Text_Style(source).fontName );
        this.fontSize.Clone( Text_Style(source).fontSize );
        Delete( this.font );
        this.fontState := INVALID_STATE;
        this.offsetX.Clone( Text_Style(source).offsetX );
        this.offsetY.Clone( Text_Style(source).offsetY );
        this.shade.Clone( Text_Style(source).shade );
        this.wrap.Clone( Text_Style(source).wrap );
    end Copy_From_Element;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Text_Style ) is
    begin
        this.color.Delete;
        Delete( this.font );
        this.fontName.Delete;
        this.fontSize.Delete;
        this.offsetX.Delete;
        this.offsetY.Delete;
        this.shade.Delete;
        this.wrap.Delete;
        Element_Style(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    overriding
    procedure Get_Color( this : Text_Style; state : Widget_State; defined : out Boolean; property : in out Allegro_Color ) is
    begin
        defined := this.color.Get_Value( Natural(state), property );
    end Get_Color;

    ----------------------------------------------------------------------------

    overriding
    procedure Get_Font( this : in out Text_Style; state : Widget_State; defined : out Boolean; property : in out A_Font ) is
        name : Unbounded_String;
        size : Integer := 1;
    begin
        defined := this.fontName.Get_Value( Natural(state), name ) and then
                   this.fontSize.Get_Value( Natural(state), size );
        if defined then
            if state /= this.fontState then
                Delete( this.font );
                this.font := Load_Font( To_String( name ), size );
                this.fontState := state;
            end if;
            property := this.font;
        end if;
    end Get_Font;

    ----------------------------------------------------------------------------

    overriding
    procedure Get_Font_Name( this : Text_Style; state : Widget_State; defined : out Boolean; property : in out Unbounded_String ) is
    begin
        defined := this.fontName.Get_Value( Natural(state), property );
    end Get_Font_Name;

    ----------------------------------------------------------------------------

    overriding
    procedure Get_Font_Size( this : Text_Style; state : Widget_State; defined : out Boolean; property : in out Integer ) is
    begin
        defined := this.fontSize.Get_Value( Natural(state), property );
    end Get_Font_Size;

    ----------------------------------------------------------------------------

    overriding
    procedure Get_Offset_X( this : Text_Style; state : Widget_State; defined : out Boolean; property : in out Float ) is
    begin
        defined := this.offsetX.Get_Value( Natural(state), property );
    end Get_Offset_X;

    ----------------------------------------------------------------------------

    overriding
    procedure Get_Offset_Y( this : Text_Style; state : Widget_State; defined : out Boolean; property : in out Float ) is
    begin
        defined := this.offsetY.Get_Value( Natural(state), property );
    end Get_Offset_Y;

    ----------------------------------------------------------------------------

    overriding
    procedure Get_Shade( this : Text_Style; state : Widget_State; defined : out Boolean; property : in out Float ) is
    begin
        defined := this.shade.Get_Value( Natural(state), property );
    end Get_Shade;

    ----------------------------------------------------------------------------

    overriding
    procedure Get_Wrap( this : Text_Style; state : Widget_State; defined : out Boolean; property : in out Wrap_Mode ) is
    begin
        defined := this.wrap.Get_Value( Natural(state), property );
    end Get_Wrap;

    ----------------------------------------------------------------------------

    overriding
    procedure Merge_From_Element( this   : in out Text_Style;
                                  source : Element_Style'Class ) is
    begin
        pragma Assert( source in Text_Style'Class );
        this.color.Merge_From_Property( Text_Style(source).color );
        this.fontName.Merge_From_Property( Text_Style(source).fontName );
        this.fontSize.Merge_From_Property( Text_Style(source).fontSize );
        Delete( this.font );
        this.fontState := INVALID_STATE;
        this.offsetX.Merge_From_Property( Text_Style(source).offsetX );
        this.offsetY.Merge_From_Property( Text_Style(source).offsetY );
        this.shade.Merge_From_Property( Text_Style(source).shade );
        this.wrap.Merge_From_Property( Text_Style(source).wrap );
    end Merge_From_Element;

    ----------------------------------------------------------------------------

    overriding
    function Set_Property( this     : in out Text_Style;
                           property : Property_Id;
                           state    : Widget_State;
                           val      : Value'Class ) return Boolean is

        function Cast_Wrap is new Cast_Enum(Wrap_Mode);

        success : Boolean := True;
    begin
        case property is
            when STYLE_COLOR     => this.color.Set_Value( Natural(state), Coerce_Color( val, this.color.Get_Default ) );
            when STYLE_FONT_NAME => this.fontName.Set_Value( Natural(state), Cast_Unbounded_String( val, this.fontName.Get_Default ) );
                                    this.fontState := INVALID_STATE;
            when STYLE_FONT_SIZE => this.fontSize.Set_Value( Natural(state), Cast_Int( val, this.fontSize.Get_Default ) );
                                    this.fontState := INVALID_STATE;
            when STYLE_OFFSET_X  => this.offsetX.Set_Value( Natural(state), Cast_Float( val, this.offsetX.Get_Default ) );
            when STYLE_OFFSET_Y  => this.offsetY.Set_Value( Natural(state), Cast_Float( val, this.offsetY.Get_Default ) );
            when STYLE_SHADE     => this.shade.Set_Value( Natural(state), Cast_Float( val, this.shade.Get_Default ) );
            when STYLE_WRAP      => this.wrap.Set_Value( Natural(state), Cast_Wrap( val, this.wrap.Get_Default ) );
            when others          => success := False;
        end case;
        return success;
    end Set_Property;

end Widget_Styles;
