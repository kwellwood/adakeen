--
-- Copyright (c) 2014-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Displays;                  use Allegro.Displays;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Assets.Fonts;                      use Assets.Fonts;
--with Debugging;                         use Debugging;
with Drawing.Primitives;                use Drawing.Primitives;
with Drawing.Tiles;                     use Drawing.Tiles;
with Game_Views;                        use Game_Views;
with Styles;                            use Styles;
with Widget_Styles.Registry;            use Widget_Styles.Registry;

package body Widgets.Text_Boxes is

    package Connections is new Signals.Connections(Textbox);
    use Connections;

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Textbox( view : not null access Game_Views.Game_View'Class;
                             id   : String := "" ) return A_Textbox is
        this : A_Textbox := new Textbox;
    begin
        this.Construct( view, id );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Textbox;

    ----------------------------------------------------------------------------

    procedure Clear( this : not null access Textbox'Class ) is
    begin
        this.Set_Text( To_Unbounded_String( "" ) );
    end Clear;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Textbox;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String ) is
    begin
        Widget(this.all).Construct( view, id, "TextBox" );
        this.StyleChanged.Connect( Slot( this, Recalculate_Lines'Access ) );  -- style change affects line wrap
        this.Resized.Connect( Slot( this, Recalculate_Lines'Access ) );       -- viewport resize affects line wrap
        this.Set_Focusable( False );
    end Construct;

    ----------------------------------------------------------------------------

    procedure Adjust_Viewport( this : not null access Textbox'Class ) is
        align : constant Align_Type := this.style.Get_Align( BACKGROUND_ELEM );
    begin
        if align = Top_Left or align = Top_Center or align = Top_Right then
            this.viewport.y := 0.0;
        elsif align = Bottom_Left or align = Bottom_Center or align = Bottom_Right then
            if this.viewport.height > 0.0 then
                this.viewport.y := Float'Max( this.Get_Preferred_Height, this.contentHeight ) - this.viewport.height;
                if this.viewport.y < 0.0 then
                    this.viewport.y := 0.0;
                end if;
            end if;
        end if;
    end Adjust_Viewport;

    ----------------------------------------------------------------------------

    procedure Append( this : not null access Textbox'Class; text : String ) is
    begin
        Append( this.text, text );
        this.Recalculate_Lines;
    end Append;

    ----------------------------------------------------------------------------

    procedure Append_Line( this : not null access Textbox'Class; text : String ) is
    begin
        this.Append( text & ASCII.LF );
    end Append_Line;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Content( this : access Textbox ) is
        state         : constant Widget_State := this.Get_Visual_State;
        align         : constant Align_Type := this.style.Get_Align( BACKGROUND_ELEM, state );
        font          : constant A_Font := this.style.Get_Font( TEXT_ELEM, state );
        contentWidth  : constant Float := Float'Max( this.viewport.width, this.contentWidth );
        contentHeight : constant Float := Float'Max( this.viewport.height, this.contentHeight );
        lineWidth     : constant Float := contentWidth -
                                          this.style.Get_Pad_Left( BACKGROUND_ELEM, state ) -
                                          this.style.Get_Pad_Right( BACKGROUND_ELEM, state );
        lineHeight    : constant Float := Float(font.Line_Height);
        lineSpacing   : constant Float := this.style.Get_Spacing( BACKGROUND_ELEM, state );
        padLeft       : constant Float := this.style.Get_Pad_Left( BACKGROUND_ELEM, state );
        color         : Allegro_Color;
        textX, textY  : Float;
    begin
        -- draw the background --
        color := this.style.Get_Color( BACKGROUND_ELEM, state );
        if color /= Transparent then
            Rectfill( 0.0, 0.0, contentWidth, contentHeight,
                      Lighten( color, this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );
        else
            Draw_Tile_Filled( this.style.Get_Image( BACKGROUND_ELEM, state ).Get_Tile,
                              0.0, 0.0, 0.0,
                              contentWidth, contentHeight,
                              Lighten( this.style.Get_Tint( BACKGROUND_ELEM, state ),
                                       this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );
        end if;

        -- draw the border --
        Draw_Tile_Filled( this.style.Get_Image( BORDER_ELEM, state ).Get_Tile,
                          0.0, 0.0, 0.0,
                          contentWidth, contentHeight,
                          Lighten( this.style.Get_Tint( BORDER_ELEM, state ),
                                   this.style.Get_Shade( BORDER_ELEM, state ) ) );

        -- draw the text lines within the viewport's vertical range
        Al_Hold_Bitmap_Drawing( True );
        color := Lighten( this.style.Get_Color( TEXT_ELEM, state ),
                          this.style.Get_Shade( TEXT_ELEM, state ) );
        if align = Bottom_Left or align = Bottom_Center or align = Bottom_Right then
            textY := contentHeight - this.style.Get_Pad_Bottom( BACKGROUND_ELEM, state ) - lineHeight;
            for line of reverse this.displayLines loop
                exit when textY + lineHeight <= this.viewport.y;             -- bottom of this line is above the viewport
                if textY < this.viewport.y + this.viewport.height then
                    if line'Length > 0 then
                        textX := padLeft + Align_Horizontal( align,
                                                             lineWidth,
                                                             Float(font.Text_Length( line )) );
                        font.Draw_String( line, textX, textY, color );
                    end if;
                end if;
                textY := textY - (lineHeight + lineSpacing);
            end loop;
        else
            textY := this.style.Get_Pad_Top( BACKGROUND_ELEM, state );
            for line of this.displayLines loop
                exit when textY >= this.viewport.y + this.viewport.height;   -- top of this line is below the viewport
                if textY + lineHeight > this.viewport.y then
                    if line'Length > 0 then
                        textX := padLeft + Align_Horizontal( align,
                                                             lineWidth,
                                                             Float(font.Text_Length( line )) );
                        font.Draw_String( line, textX, textY, color );
                    end if;
                end if;
                textY := textY + (lineHeight + lineSpacing);
            end loop;
        end if;
        Al_Hold_Bitmap_Drawing( False );
    end Draw_Content;

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Height( this : access Textbox ) return Float is
    begin
        -- always assume atleast one line of text
        return this.style.Get_Pad_Top( BACKGROUND_ELEM )
               + Float'Max( 1.0, Float(this.displayLines.Length)) * Float(this.style.Get_Font( TEXT_ELEM ).Line_Height)
               + Float'Max( Float(this.displayLines.Length) - 1.0, 0.0 ) * this.style.Get_Spacing( BACKGROUND_ELEM )
               + this.style.Get_Pad_Bottom( BACKGROUND_ELEM );
    end Get_Min_Height;

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Width( this : access Textbox ) return Float is
        font      : A_Font;
        lineWidth : Integer := 0;
        maxWidth  : Integer := 0;
    begin
        font := this.style.Get_Font( TEXT_ELEM );
        lineWidth := font.Text_Length( "X" );     -- assume atleast one letter
        for line of this.displayLines loop
            lineWidth := font.Text_Length( line );
            if lineWidth > maxWidth then
                maxWidth := lineWidth;
            end if;
        end loop;
        return this.style.Get_Pad_Left( BACKGROUND_ELEM )
               + Float(maxWidth)
               + this.style.Get_Pad_Right( BACKGROUND_ELEM );
    end Get_Min_Width;

    ----------------------------------------------------------------------------

    function Get_Text( this : not null access Textbox'Class ) return Unbounded_String is (this.text);

    ----------------------------------------------------------------------------

    overriding
    function Get_Visual_State( this : access Textbox ) return Widget_State is ((if not this.Is_Enabled then DISABLED_STATE else DEFAULT_STATE));

    ----------------------------------------------------------------------------

    procedure Prepend( this : not null access Textbox'Class; text : String ) is
    begin
        this.text := text & this.text;
        this.Recalculate_Lines;
    end Prepend;

    ----------------------------------------------------------------------------

    procedure Prepend_Line( this : not null access Textbox'Class; text : String ) is
    begin
        this.Prepend( text & ASCII.LF );
    end Prepend_Line;

    ----------------------------------------------------------------------------

    procedure Recalculate_Lines( this : not null access Textbox'Class ) is
        text      : constant String := To_String( this.text );
        font      : constant A_Font := this.style.Get_Font( TEXT_ELEM );
        wrapMode  : constant Wrap_Mode := this.style.Get_Wrap( TEXT_ELEM );
        wrapWidth : constant Float := this.viewport.width -
                                      this.style.Get_Pad_Left( BACKGROUND_ELEM ) -
                                      this.style.Get_Pad_Right( BACKGROUND_ELEM );
        lineWidth : Integer := 0;
        maxWidth  : Integer := 0;
        lineStart : Integer;
        lineEnd   : Integer;
        wrapped   : Boolean;
        trimEnd   : Trim_End := Right;
    begin
        this.displayLines.Clear;

        lineStart := text'First;
        lineEnd := lineStart - 1;
        for i in text'Range loop
            -- ignore CR characters; break at LF only
            if text(i) = ASCII.CR then
                null;

            -- wrap due to end of line
            elsif text(i) = ASCII.LF then
                lineEnd := i - 1;
                this.displayLines.Append( Trim( text(lineStart..lineEnd), trimEnd ) );
                trimEnd := Right;      -- stop trimming the left end of incomplete lines now
                lineStart := i + 1;

            -- wrap due to text length
            elsif wrapMode /= None and then wrapWidth > 0.0 and then Float(font.Text_Length( text(lineStart..i) )) > wrapWidth then
                if wrapMode = Hard then
                    -- hard line wrapping
                    lineEnd := i - 1;
                    this.displayLines.Append( Trim( text(lineStart..lineEnd), trimEnd ) );
                    lineStart := i;
                else
                    -- soft line wrapping
                    -- look backward for the start of this word
                    wrapped := False;
                    for j in reverse lineStart..i loop
                        if text(j) = ' ' or else text(j) = ASCII.HT then
                            lineEnd := j - 1;
                            this.displayLines.Append( Trim( text(lineStart..lineEnd), trimEnd ) );
                            lineStart := j + 1;
                            wrapped := True;
                            exit;
                        end if;
                    end loop;

                    -- soft line wrapping not possible; hard wrap
                    if not wrapped then
                        lineEnd := i - 1;
                        this.displayLines.Append( Trim( text(lineStart..lineEnd), trimEnd ) );
                        lineStart := i;
                    end if;
                end if;
                trimEnd := Both;  -- wrapped an incomplete line; the next line
                                  -- needs to be trimmed on the left too, to
                                  -- remove any whitespace from the wrap.
            end if;

            if not this.displayLines.Is_Empty then
                lineWidth := font.Text_Length( this.displayLines.Last_Element );
                if lineWidth > maxWidth then
                    maxWidth := lineWidth;
                end if;
            end if;
        end loop;

        -- add the last line
        if lineStart <= text'Last then
            this.displayLines.Append( Trim( text(lineStart..text'Last), trimEnd ) );
            lineWidth := font.Text_Length( this.displayLines.Last_Element );
            if lineWidth > maxWidth then
                maxWidth := lineWidth;
            end if;
        end if;

        -- recalculate content size --
        this.contentWidth := this.style.Get_Pad_Left( BACKGROUND_ELEM )
                             + Float(maxWidth)
                             + this.style.Get_Pad_Right( BACKGROUND_ELEM );
        this.contentHeight := this.style.Get_Pad_Top( BACKGROUND_ELEM )
                              + Float(Integer(this.displayLines.Length) * font.Line_Height)
                              + (Float(this.displayLines.Length) - 1.0) * this.style.Get_Spacing( BACKGROUND_ELEM )
                              + this.style.Get_Pad_Bottom( BACKGROUND_ELEM );

        this.Adjust_Viewport;
    end Recalculate_Lines;

    ----------------------------------------------------------------------------

    procedure Set_Text( this : not null access Textbox'Class; text : Unbounded_String ) is
    begin
        if this.text /= text then
            this.text := text;
            this.Recalculate_Lines;
        end if;
    end Set_Text;

begin

    Register_Style( "TextBox",
                    (BACKGROUND_ELEM => To_Unbounded_String( "background" ),
                     BORDER_ELEM     => To_Unbounded_String( "border" ),
                     TEXT_ELEM       => To_Unbounded_String( "text" )),
                    (BACKGROUND_ELEM => Area_Element,
                     BORDER_ELEM     => Area_Element,
                     TEXT_ELEM       => Text_Element),
                    (0 => To_Unbounded_String( "disabled" )) );

end Widgets.Text_Boxes;
