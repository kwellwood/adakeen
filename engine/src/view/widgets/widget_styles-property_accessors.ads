--
-- Copyright (c) 2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Animated_Properties;               use Animated_Properties;

package Widget_Styles.Property_Accessors is

    type Style_Color_Accessor is new Animated_Color.Abstract_Accessor with
        record
            style    : access Widget_Style'Class;
            element  : Element_Index;
            property : Property_Id;
            state    : Widget_State;
        end record;

    function Get_Owner( this : Style_Color_Accessor ) return access Base_Object'Class;
    function Read( this : Style_Color_Accessor ) return Allegro_Color;
    procedure Write( this : Style_Color_Accessor; val : Allegro_Color );

    ----------------------------------------------------------------------------

    type Style_Float_Accessor is new Animated_Float.Abstract_Accessor with
        record
            style    : access Widget_Style'Class;
            element  : Element_Index;
            property : Property_Id;
            state    : Widget_State;
        end record;

    function Get_Owner( this : Style_Float_Accessor ) return access Base_Object'Class;
    function Read( this : Style_Float_Accessor ) return Float;
    procedure Write( this : Style_Float_Accessor; val : Float );

end Widget_Styles.Property_Accessors;
