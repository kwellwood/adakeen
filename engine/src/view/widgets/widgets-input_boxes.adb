--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Assets.Fonts;                      use Assets.Fonts;
--with Debugging;                         use Debugging;
with Drawing;                           use Drawing;
with Drawing.Primitives;                use Drawing.Primitives;
with Drawing.Tiles;                     use Drawing.Tiles;
with Game_Views;                        use Game_Views;
with Keyboard;                          use Keyboard;
with Values.Casting;                    use Values.Casting;
with Values.Strings;                    use Values.Strings;
with Widget_Styles.Registry;            use Widget_Styles.Registry;

package body Widgets.Input_Boxes is

    package Connections is new Signals.Connections(Input_Box);
    use Connections;

    package Key_Connections is new Signals.Keys.Connections(Input_Box);
    use Key_Connections;

    package Mouse_Connections is new Signals.Mouse.Connections(Input_Box);
    use Mouse_Connections;

    SCROLL_DELAY : constant Time_Span := Milliseconds( 200 );

    function Unconstrained( curStr, newStr : String ) return String is (newStr);

    ----------------------------------------------------------------------------

    procedure On_Blurred( this : not null access Input_Box'Class );

    procedure On_Focused( this : not null access Input_Box'Class );

    -- Adds a readable character to the input string when one is typed.
    procedure On_Key_Character( this : not null access Input_Box'Class;
                                key  : Key_Arguments );

    -- Moves the cursor to the end of the input when the End key is typed.
    procedure On_Key_End( this : not null access Input_Box'Class;
                          key  : Key_Arguments );

    -- Moves the cursor to the beginning of the input when the Home key is typed.
    procedure On_Key_Home( this : not null access Input_Box'Class;
                           key  : Key_Arguments );

    -- Moves the cursor one space to the left, if possible, when the left key is
    -- typed.
    procedure On_Key_Left( this : not null access Input_Box'Class;
                           key  : Key_Arguments );

    -- Moves the cursor one space to the right, if possible, when the right key
    -- is typed.
    procedure On_Key_Right( this : not null access Input_Box'Class;
                            key  : Key_Arguments );

    procedure On_Mouse_Clicked( this : not null access Input_Box'Class );

    procedure On_Mouse_Doubleclicked( this  : not null access Input_Box'Class;
                                      mouse : Button_Arguments );

    procedure On_Mouse_Moved( this  : not null access Input_Box'Class;
                              mouse : Mouse_Arguments );

    procedure On_Mouse_Pressed( this  : not null access Input_Box'Class;
                                mouse : Button_Arguments );

    procedure On_Resized( this : not null access Input_Box'Class );

    procedure On_Style_Changed( this : not null access Input_Box'Class );

    -- Sets the cursor position if 'pos' changes it, and recalculates the
    -- visible area if the cursor moves outside it. Set 'textChanged' as True
    -- to force the visible area to be recalculated because the text itself
    -- changed.
    procedure Set_Cursor_Pos( this        : not null access Input_Box'Class;
                              pos         : Integer;
                              textChanged : Boolean := False );

    -- Sets the .text field to 'text' using the constraint function, and updates
    -- the undo history. If 'undoable' is False, the undo history will be cleared
    -- instead of updated.
    procedure Set_Text_Internal( this     : not null access Input_Box'Class;
                                 text     : String;
                                 undoable : Boolean := True );

    -- Ensures the cursor is visible by resetting its blink interval.
    procedure Show_Cursor( this : not null access Input_Box'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Input_Box( view : not null access Game_Views.Game_View'Class;
                               id   : String := "" ) return A_Input_Box is
        this : A_Input_Box := new Input_Box;
    begin
        this.Construct( view, id );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Input_Box;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Input_Box;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String ) is
    begin
        Widget(this.all).Construct( view, id, "InputBox" );
        this.sigAccepted.Init( this );

        this.StyleChanged.Connect( Slot( this, On_Style_Changed'Access ) );
        this.Resized.Connect( Slot( this, On_Resized'Access ) );
        this.Focused.Connect( Slot( this, On_Focused'Access ) );
        this.Blurred.Connect( Slot( this, On_Blurred'Access ) );

        -- move the cursor
        this.KeyTyped.Connect( Slot( this, On_Key_End'Access, ALLEGRO_KEY_END, (SHIFT=>Either, others=>No) ) );
        this.KeyTyped.Connect( Slot( this, On_Key_Home'Access, ALLEGRO_KEY_HOME, (SHIFT=>Either, others=>No) ) );
        this.KeyTyped.Connect( Slot( this, On_Key_Left'Access, ALLEGRO_KEY_LEFT, (SHIFT=>Either, others=>No) ) );
        this.KeyTyped.Connect( Slot( this, On_Key_Right'Access, ALLEGRO_KEY_RIGHT, (SHIFT=>Either, others=>No) ) );

        -- delete characters
        this.KeyTyped.Connect( Slot( this, Do_Backspace'Access, ALLEGRO_KEY_BACKSPACE, (SHIFT=>Either, others=>No) ) );
        this.KeyTyped.Connect( Slot( this, Do_Delete'Access, ALLEGRO_KEY_DELETE, (SHIFT=>Either, others=>No) ) );

        -- select all
        this.KeyTyped.Connect( Slot( this, Select_All'Access, ALLEGRO_KEY_A, (CTRL=>Yes, others=>No) ) );

        -- forward/backward through history of accepted strings
        this.KeyTyped.Connect( Slot( this, Do_History_Forward'Access, ALLEGRO_KEY_DOWN, (SHIFT=>Either, others=>No) ) );
        this.KeyTyped.Connect( Slot( this, Do_History_Back'Access, ALLEGRO_KEY_UP, (SHIFT=>Either, others=>No) ) );

        -- accept the current string
        this.KeyTyped.Connect( Slot( this, Do_Enter'Access, ALLEGRO_KEY_ENTER, (SHIFT=>Either, others=>No) ) );
        this.KeyTyped.Connect( Slot( this, Do_Enter'Access, ALLEGRO_KEY_PAD_ENTER, (SHIFT=>Either, others=>No) ) );

        -- record readable characters
        this.KeyTyped.Connect( Slot( this, On_Key_Character'Access, KEY_ANY_CHARACTER, (SHIFT=>Either, others=>No) ) );

        -- undo/redo
        this.KeyTyped.Connect( Slot( this, Do_Undo'Access, ALLEGRO_KEY_Z, (CTRL=>Yes, others=>No) ) );
        this.KeyTyped.Connect( Slot( this, Do_Redo'Access, ALLEGRO_KEY_Y, (CTRL=>Yes, others=>No) ) );

        -- cut/copy/paste
        this.KeyTyped.Connect( Slot( this, Do_Copy'Access, ALLEGRO_KEY_C, (CTRL=>Yes, others=>No) ) );
        this.KeyTyped.Connect( Slot( this, Do_Cut'Access, ALLEGRO_KEY_X, (CTRL=>Yes, others=>No) ) );
        this.KeyTyped.Connect( Slot( this, Do_Paste'Access, ALLEGRO_KEY_V, (CTRL=>Yes, others=>No) ) );

        this.MouseMoved.Connect( Slot( this, On_Mouse_Moved'Access ) );
        this.MousePressed.Connect( Slot( this, On_Mouse_Pressed'Access, Mouse_Left ) );
        this.MouseClicked.Connect( Slot( this, On_Mouse_Clicked'Access, Mouse_Left ) );
        this.MouseDoubleClicked.Connect( Slot( this, On_Mouse_Doubleclicked'Access, Mouse_Left ) );

        this.Set_Mouse_Cursor( "EDIT" );
        this.constrain := Unconstrained'Access;
        this.Set_Cursor_Pos( Length( this.text ), textChanged => True );
        this.blinkTime := Clock;
        this.scrollTime := Clock;
    end Construct;

    ----------------------------------------------------------------------------

    function Accepted( this : not null access Input_Box'Class ) return access Signal'Class is (this.sigAccepted'Access);

    ----------------------------------------------------------------------------

    procedure Add_Character( this : not null access Input_Box'Class;
                             char : Character ) is
        len   : constant Natural := Length( this.text );
        left  : constant String := Slice( this.text, 1, this.cursorPos );
        right : constant String := Slice( this.text, this.cursorPos + 1, len );
    begin
        this.selectOnClick := False;
        if this.selectPos /= this.cursorPos then
            -- replace the selection (at least 1 char) with 1 char
            this.Replace_Selection( String'(1=>char) );
        else
            if len < this.maxLen then
                this.Set_Text_Internal( left & char & right );
            end if;
            if len < Length( this.text ) then
                -- a character was indeed added
                this.Set_Cursor_Pos( this.cursorPos + 1, textChanged => True );
                this.selectPos := this.cursorPos;
            end if;
        end if;
    end Add_Character;

    ----------------------------------------------------------------------------

    procedure Calculate_Visible_Text( this : not null access Input_Box'Class ) is
        font     : constant A_Font := this.style.Get_Font( TEXT_ELEM );
        padLeft  : constant Float := this.style.Get_Pad_Left( BACKGROUND_ELEM );
        padRight : constant Float := this.style.Get_Pad_Right( BACKGROUND_ELEM );
        len      : Float := 0.0;
    begin
        -- if the input box is not yet attached to the window then don't compute
        -- visible because: 1. It's not necessary, and 2. the visible character
        -- range will be scrunched to nothing.
        if not this.Is_Rooted then
            return;
        end if;

        -- if the cursor scrolled left out of the visible text, jump the visible
        -- text window left such that the cursor is visible again, 5 characters
        -- from the left edge.
        if this.cursorPos < this.viewFirst then
            this.viewFirst := Integer'Max( 1, this.cursorPos - 5 );
        end if;

        -- starting at the left of the view, count the number characters that
        -- fit within the visible area
        this.viewLast := this.viewFirst - 1;
        for i in this.viewFirst..Length( this.text ) loop
            len := Float(font.Text_Length( Slice( this.text, this.viewFirst, i ) ));
            if len >= this.viewport.width - (padLeft + padRight) then
                exit;
            end if;
            this.viewLast := i;
        end loop;

        -- if the cursor scrolled right out of the visible text, jump the visible
        -- text window right such that the cursor is visible again, 5 characters
        -- from the right edge. the left of the visible window will need to be
        -- re-calculated, starting from the right this time.
        if this.cursorPos >= this.viewLast then
            this.viewLast := Integer'Min( this.cursorPos + 5, Length( this.text ) );
            this.viewFirst := this.viewLast + 1;
            for i in reverse 1..this.viewLast loop
                len := Float(font.Text_Length( Slice( this.text, i, this.viewLast ) ));
                if len > this.viewport.width - (padLeft + padRight) then
                    exit;
                end if;
                this.viewFirst := i;
            end loop;
        end if;
    end Calculate_Visible_Text;

    ----------------------------------------------------------------------------

    procedure Clear_Selection( this : not null access Input_Box'Class ) is
    begin
        this.selectPos := this.cursorPos;
    end Clear_Selection;

    ----------------------------------------------------------------------------

    procedure Do_Backspace( this : not null access Input_Box'Class ) is
        origLen : constant Natural := Length( this.text );
    begin
        this.selectOnClick := False;
        if this.selectPos /= this.cursorPos then
            -- delete the selected text
            this.Replace_Selection( "" );
        elsif this.cursorPos > 0 then
            -- delete the character left of the cursor
            this.Set_Text_Internal( Slice( this.text, 1, this.cursorPos - 1 ) &
                                    Slice( this.text,
                                           this.cursorPos + 1,
                                           Length( this.text ) ) );

            if Length( this.text ) < origLen then
                -- a character was indeed removed
                this.Set_Cursor_Pos( this.cursorPos - 1, textChanged => True );
                this.selectPos := this.cursorPos;
            end if;
        end if;
        this.Show_Cursor;
    end Do_Backspace;

    ----------------------------------------------------------------------------

    procedure Do_Copy( this : not null access Input_Box'Class ) is
        selectedFirst,
        selectedLast : Integer;
    begin
        if this.cursorPos = this.selectPos then
            return;
        end if;

        selectedFirst := Integer'Min( this.cursorPos, this.selectPos ) + 1;
        selectedLast := Integer'Max( this.cursorPos, this.selectPos );

        this.Get_View.Set_Clipboard( "text", Create( Unbounded_Slice( this.text, selectedFirst, selectedLast ) ) );
    end Do_Copy;

    ----------------------------------------------------------------------------

    procedure Do_Cut( this : not null access Input_Box'Class ) is
    begin
        if this.cursorPos = this.selectPos then
            return;
        end if;

        this.Do_Copy;
        this.Replace_Selection( "" );
    end Do_Cut;

    ----------------------------------------------------------------------------

    procedure Do_Delete( this : not null access Input_Box'Class ) is
        len : constant Natural := Length( this.text );
    begin
        this.selectOnClick := False;
        if this.selectPos /= this.cursorPos then
            -- delete the selected text
            this.Replace_Selection( "" );
        elsif this.cursorPos < len then
            -- delete the character right of the cursor
            this.Set_Text_Internal( Slice( this.text, 1, this.cursorPos ) &
                                    Slice( this.text, this.cursorPos + 2, len ) );
            if len > Length( this.text ) then
                -- a character was indeed removed
                --this.cursorPos doesn't change
                this.selectPos := this.cursorPos;
                this.Calculate_Visible_Text;
            end if;
        end if;
        this.Show_Cursor;
    end Do_Delete;

    ----------------------------------------------------------------------------

    procedure Do_Enter( this : not null access Input_Box'Class ) is
    begin
        this.selectOnClick := False;
        this.selectPos := this.cursorPos;
        if this.histEnabled then
            if Length( this.text ) > 0 then
                if this.history.Is_Empty or else this.history.Last_Element /= this.text then
                    this.history.Append( this.text );
                end if;
            end if;
            this.histIndex := Integer(this.history.Length) + 1;
        end if;
        this.Show_Cursor;
        this.Accepted.Emit;
    end Do_Enter;

    ----------------------------------------------------------------------------

    procedure Do_History_Back( this : not null access Input_Box'Class ) is
    begin
        if not this.histEnabled then
            return;
        end if;

        if this.histIndex > 1 then
            this.histIndex := this.histIndex - 1;
            this.Set_Text( To_String( this.history.Element( this.histIndex ) ) );
        end if;
    end Do_History_Back;

    ----------------------------------------------------------------------------

    procedure Do_History_Forward( this : not null access Input_Box'Class ) is
    begin
        if not this.histEnabled then
            return;
        end if;

        if this.histIndex < Integer(this.history.Length) then
            this.histIndex := this.histIndex + 1;
            this.Set_Text( To_String( this.history.Element( this.histIndex ) ) );
        else
            if this.histIndex = Integer(this.history.Length) then
                this.histIndex := this.histIndex + 1;
            end if;
            this.Set_Text( "" );
        end if;
    end Do_History_Forward;

    ----------------------------------------------------------------------------

    procedure Do_Paste( this : not null access Input_Box'Class ) is
        str   : Unbounded_String := Cast_Unbounded_String( this.Get_View.Get_Clipboard( "text" ) );
        left,
        right : Unbounded_String;
    begin
        -- only take the first line of text
        for i in 1..Length( str ) loop
            if Element( str, i ) = ASCII.CR or Element( str, i ) = ASCII.LF then
                str := Head( str, i - 1 );
                exit;
            end if;
        end loop;
        if Length( str ) = 0 then
            return;
        end if;

        if this.cursorPos /= this.selectPos then
            -- replace the selection (checks max length)
            this.Replace_Selection( To_String( str ) );
        else
            -- insert at cursor and check max length
            left := Unbounded_Slice( this.text, 1, this.cursorPos );
            right := Unbounded_Slice( this.text, this.cursorPos + 1, Length( this.text ) );
            if Length( str ) > this.maxLen - (Length( left ) + Length( right)) then
                -- truncate the pasted text to fit the maximum input length
                str := Head( str, this.maxLen - (Length( left ) + Length( right)) );
            end if;

            this.Set_Text_Internal( To_String( left & str & right ) );
            this.Set_Cursor_Pos( this.cursorPos + Length( str ), textChanged => True );
            this.selectPos := this.cursorPos;
        end if;
    end Do_Paste;

    ----------------------------------------------------------------------------

    procedure Do_Redo( this : not null access Input_Box'Class ) is
    begin
        if this.undoIndex < Integer(this.undoHistory.Length) then
            this.undoIndex := this.undoIndex + 1;

            this.text := this.undoHistory.Constant_Reference( this.undoIndex ).afterText;
            if this.undoIndex < Integer(this.undoHistory.Length) then
                -- set the cursor to the before position of the next change,
                -- since we don't have an after position for each change.
                this.Set_Cursor_Pos( this.undoHistory.Constant_Reference( this.undoIndex + 1 ).cursorPos, textChanged => True );
            else
                -- we don't know what the cursor position would have been for
                -- the latest change, so default to the string's tail.
                this.Set_Cursor_Pos( Length( this.text ), textChanged => True );
            end if;
            this.selectPos := this.cursorPos;
        end if;
    end Do_Redo;

    ----------------------------------------------------------------------------

    procedure Do_Undo( this : not null access Input_Box'Class ) is
    begin
        if this.undoIndex > 0 then
            this.text := this.undoHistory.Constant_Reference( this.undoIndex ).beforeText;
            this.Set_Cursor_Pos( this.undoHistory.Constant_Reference( this.undoIndex ).cursorPos, textChanged => True );
            this.selectPos := this.cursorPos;

            this.undoIndex := this.undoIndex - 1;
        end if;
    end Do_Undo;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Content( this : access Input_Box ) is
        state         : constant Widget_State := this.Get_Visual_State;
        font          : constant A_Font := this.style.Get_Font( TEXT_ELEM, state );
        textColor     : constant Allegro_Color := Lighten( this.style.Get_Color( TEXT_ELEM, state ),
                                                           this.style.Get_Shade( TEXT_ELEM, state ) );
        color         : Allegro_Color;
        textX, textY  : Float;
        selectedFirst : Integer := this.viewLast + 1;
        selectedLast  : Integer := selectedFirst - 1;
        selectedX1    : Float := -1.0;
        selectedX2    : Float := -1.0;
    begin
        -- - - - Calculate Positions - - - --

        textX := this.style.Get_Pad_Left( BACKGROUND_ELEM, state );
        textY := this.style.Get_Pad_Top( BACKGROUND_ELEM, state ) +
                 (this.viewport.height - this.style.Get_Pad_Top( BACKGROUND_ELEM, state ) -
                                         this.style.Get_Pad_Bottom( BACKGROUND_ELEM, state )) / 2.0 -
                 Float(font.Line_Height) / 2.0;

        -- - - - Draw Content - - - --

        -- draw the background --
        color := this.style.Get_Color( BACKGROUND_ELEM, state );
        if color /= Transparent then
            Rectfill( 0.0, 0.0, this.viewport.width, this.viewport.height,
                      Lighten( color, this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );
        else
            Draw_Tile_Filled( this.style.Get_Image( BACKGROUND_ELEM, state ).Get_Tile,
                              0.0, 0.0, 0.0,
                              this.viewport.width, this.viewport.height,
                              Lighten( this.style.Get_Tint( BACKGROUND_ELEM, state ),
                                       this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );
        end if;

        -- draw the border --
        Draw_Tile_Filled( this.style.Get_Image( BORDER_ELEM, state ).Get_Tile,
                          0.0, 0.0, 0.0,
                          this.viewport.width, this.viewport.height,
                          Lighten( this.style.Get_Tint( BORDER_ELEM, state ),
                                   this.style.Get_Shade( BORDER_ELEM, state ) ) );

        -- draw the text
        if this.viewFirst <= this.viewLast then

            -- set up the selected range if there's a selection
            if this.selectPos > this.cursorPos then
                -- selected right to left
                selectedFirst := this.cursorPos + 1;
                selectedLast := this.selectPos;
            elsif this.selectPos < this.cursorPos then
                -- selected left to right
                selectedFirst := this.selectPos + 1;
                selectedLast := this.cursorPos;
            end if;

            -- check if the selected range is in the view range
            if (this.viewFirst <= selectedFirst and selectedFirst <= this.viewLast) or
               (this.viewFirst <= selectedLast and selectedLast <= this.viewLast)
            then
                -- selected range is at least partially in the view range
                -- constrain it to be fully within the view range
                selectedFirst := Integer'Max( selectedFirst, this.viewFirst );
                selectedLast := Integer'Min( selectedLast, this.viewLast );
            else
                -- selected range is not in the view range (null range)
                selectedFirst := this.viewLast + 1;
                selectedLast := selectedFirst - 1;
            end if;

            -- draw the unselected text in the view range before the selected range
            font.Draw_String( Slice( this.text, this.viewFirst, Integer'Min( selectedFirst - 1, this.viewLast ) ),
                              textX, textY,
                              textColor );

            textX := textX + Float(font.Text_Length( Slice( this.text, this.viewFirst, Integer'Min( selectedFirst - 1, this.viewLast ) ) ));

            -- draw the selected text in the view range
            if selectedFirst <= this.viewLast then
                -- draw the background behind the selected text
                selectedX1 := textX;
                selectedX2 := selectedX1 + Float(font.Text_Length( Slice( this.text, selectedFirst, selectedLast ) )) - 1.0;
                Pixel_Rectfill( selectedX1, this.style.Get_Pad_Top( BACKGROUND_ELEM, state ),
                                selectedX2, this.viewport.height - this.style.Get_Pad_Bottom( BACKGROUND_ELEM, state ),
                                Lighten( this.style.Get_Color( SELAREA_ELEM ),
                                         this.style.Get_Shade( SELAREA_ELEM ) ) );

                -- draw the selected text
                font.Draw_String( Slice( this.text, selectedFirst, selectedLast ),
                                  textX, textY,
                                  Lighten( this.style.Get_Color( SELTEXT_ELEM, state ),
                                           this.style.Get_Shade( SELTEXT_ELEM, state ) ) );
                textX := selectedX2 + 1.0;

                -- draw the unselected text in the view range after the selected range
                if selectedLast < this.viewLast then
                    font.Draw_String( Slice( this.text, selectedLast + 1, this.viewLast ),
                                      textX, textY,
                                      textColor );
                end if;
            end if;
        end if;

        -- draw the cursor
        if this.hasFocus then
            declare
                textH  : constant Float := Float(font.Line_Height);
                height : constant Float := textH * 0.8;
                cx     : constant Float := this.style.Get_Pad_Left( BACKGROUND_ELEM ) + Float(font.Text_Length( Slice( this.text, this.viewFirst, this.cursorPos ) ));
                cy     : constant Float := this.style.Get_Pad_Top( BACKGROUND_ELEM, state ) +
                                           (this.viewport.height - this.style.Get_Pad_Top( BACKGROUND_ELEM, state ) -
                                           this.style.Get_Pad_Bottom( BACKGROUND_ELEM, state )) / 2.0 -
                                           Float(font.Line_Height) / 2.0 +
                                           1.0;
            begin
                color := textColor;
                if ((Clock - this.blinkTime) / Milliseconds( 500 )) mod 2 = 0 then
                    if selectedX1 <= cx and cx <= selectedX2 then
                        color := Lighten( this.style.Get_Color( SELTEXT_ELEM, state ),
                                          this.style.Get_Shade( SELTEXT_ELEM, state ) );
                    end if;
                    Pixel_Line( cx, cy, cx, cy + height, color );
                end if;
            end;
        end if;
    end Draw_Content;

    ----------------------------------------------------------------------------

    procedure Enable_History( this : not null access Input_Box'Class; enabled : Boolean ) is
    begin
        this.histEnabled := enabled;
        if not this.histEnabled then
            this.history.Clear;
            this.histIndex := 1;
        end if;
    end Enable_History;

    ----------------------------------------------------------------------------

    procedure Enable_Select_All_On_Focus( this    : not null access Input_Box'Class;
                                          enabled : Boolean ) is
    begin
        this.selectAllEnabled := enabled;
        this.selectOnClick := False;
    end Enable_Select_All_On_Focus;

    ----------------------------------------------------------------------------

    function Find_Pos( this   : not null access Input_Box'Class;
                       mouseX : Float ) return Natural is
        font      : constant A_Font := this.style.Get_Font( TEXT_ELEM );
        padLeft   : constant Float := this.style.Get_Pad_Left( BACKGROUND_ELEM );
        x         : Float := padLeft;
        nextX     : Float;
    begin
        for i in this.viewFirst..this.viewLast loop
            if mouseX <= x then
                return i - 1;
            end if;

            nextX := padLeft + Float(font.Text_Length( Slice( this.text, this.viewFirst, i ) ));
            if mouseX < nextX then
                -- the mouse is on the 'i' index character
                if (mouseX - x) < (nextX - mouseX) then
                    -- the mouse is on the left half of the 'i' character
                    return i - 1;
                end if;
            end if;

            x := nextX;
        end loop;

        return this.viewLast;   -- after the last visible character
    end Find_Pos;

    ----------------------------------------------------------------------------

    function Get_Text( this : not null access Input_Box'Class ) return String is (To_String( this.text ));

    ----------------------------------------------------------------------------

    function Get_Visual_State( this : access Input_Box ) return Widget_State is
    (
           (if not this.Is_Enabled then DISABLED_STATE else 0)
        or (if this.Is_Focused     then FOCUS_STATE    else 0)
        or (if this.hover          then HOVER_STATE    else 0)
    );

    ----------------------------------------------------------------------------

    function Is_Enabled_Select_All_On_Focus( this : not null access Input_Box'Class ) return Boolean is (this.selectAllEnabled);

    ----------------------------------------------------------------------------

    procedure Move_Cursor( this     : not null access Input_Box'Class;
                           dir      : Move_Dir;
                           doSelect : Boolean ) is
    begin
        this.selectOnClick := False;

        case dir is
            when Go_First => this.Set_Cursor_Pos( 0 );
            when Go_Last  => this.Set_Cursor_Pos( Length( this.text ) );

            when Go_Left  =>
                if this.cursorPos = this.selectPos or doSelect then
                    -- move the cursor left one character normally
                    this.Set_Cursor_Pos( Integer'Max( this.cursorPos - 1, 0 ) );
                else
                    -- a selection exists and it isn't being extended with Shift,
                    -- so clear the selection and move the cursor to its left side
                    this.Set_Cursor_Pos( Integer'Min( this.selectPos, this.cursorPos ) );
                    this.selectPos := this.cursorPos;
                end if;

            when Go_Right =>
                if this.cursorPos = this.selectPos or doSelect then
                    this.Set_Cursor_Pos( Integer'Min( this.cursorPos + 1, Length( this.text ) ) );
                else
                    this.Set_Cursor_Pos( Integer'Max( this.selectPos, this.cursorPos ) );
                    this.selectPos := this.cursorPos;
                end if;

        end case;

        if not doSelect then
            this.selectPos := this.cursorPos;
        end if;
        this.Show_Cursor;
    end Move_Cursor;

    ----------------------------------------------------------------------------

    procedure On_Blurred( this : not null access Input_Box'Class ) is
    begin
        this.Clear_Selection;
    end On_Blurred;

    ----------------------------------------------------------------------------

    procedure On_Focused( this : not null access Input_Box'Class ) is
    begin
        if this.selectAllEnabled then
            -- This only has an effect when the focus was gained by pressed 'tab'
            -- to focus the next/previous widget. That's because when clicking
            -- on the widget, the On_Mouse_Pressed handler sets the cursor and
            -- selection, overriding whatever is done here.
            this.Select_All;
            this.selectOnClick := True;
        end if;
    end On_Focused;

    ----------------------------------------------------------------------------

    procedure On_Key_Character( this : not null access Input_Box'Class;
                                key  : Key_Arguments ) is
    begin
        this.Add_Character( key.char );
        this.Show_Cursor;
    end On_Key_Character;

    ----------------------------------------------------------------------------

    procedure On_Key_End( this : not null access Input_Box'Class;
                          key  : Key_Arguments ) is
    begin
        this.Move_Cursor( Go_Last, doSelect => key.modifiers(SHIFT) );
    end On_Key_End;

    ----------------------------------------------------------------------------

    procedure On_Key_Home( this : not null access Input_Box'Class;
                           key  : Key_Arguments ) is
    begin
        this.Move_Cursor( Go_First, doSelect => key.modifiers(SHIFT) );
    end On_Key_Home;

    ----------------------------------------------------------------------------

    procedure On_Key_Left( this : not null access Input_Box'Class;
                           key  : Key_Arguments ) is
    begin
        this.Move_Cursor( Go_Left, doSelect => key.modifiers(SHIFT) );
    end On_Key_Left;

    ----------------------------------------------------------------------------

    procedure On_Key_Right( this : not null access Input_Box'Class;
                            key  : Key_Arguments ) is
    begin
        this.Move_Cursor( Go_Right, doSelect => key.modifiers(SHIFT) );
    end On_Key_Right;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Clicked( this : not null access Input_Box'Class ) is
    begin
        -- only "select all" on the first click after focused, and only if the
        -- user hasn't done any typing with the keyboard yet.
        if this.selectOnClick then
            this.Select_All;
            this.selectOnClick := False;
        end if;
    end On_Mouse_Clicked;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Doubleclicked( this  : not null access Input_Box'Class;
                                      mouse : Button_Arguments ) is
        selectPos : Integer;
        cursorPos : Integer;
    begin
        if Length( this.text ) = 0 then
            return;
        end if;

        selectPos := this.Find_Pos( mouse.x );   -- returns 0..length
        if selectPos = Length( this.text ) then
            return;
        end if;

        while selectPos > 0 and then Is_Alphanumeric( Element( this.text, selectPos ) ) loop
            selectPos := selectPos - 1;
        end loop;

        cursorPos := selectPos;
        while cursorPos < Length( this.text ) and then (Is_Alphanumeric( Element( this.text, cursorPos + 1 ) ) or Element( this.text, cursorPos + 1 ) = '_') loop
            cursorPos := cursorPos + 1;
        end loop;

        this.Set_Cursor_Pos( cursorPos );
        this.selectPos := selectPos;
    end On_Mouse_Doubleclicked;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Moved( this  : not null access Input_Box'Class;
                              mouse : Mouse_Arguments ) is
        cursorPos : Integer;
    begin
        if not this.mouseButtons(Mouse_Left) then
            return;
        end if;

        -- don't allow the next click after a press-and-drag to perform the
        -- "select all" behavior. it's clear this was the user's real intent.
        this.selectOnClick := False;
        cursorPos := this.Find_Pos( mouse.x );

        -- automatically scroll the view range
        if Clock - this.scrollTime >= SCROLL_DELAY then
            if mouse.x < this.style.Get_Pad_Left( BACKGROUND_ELEM ) then
                cursorPos := Integer'Max( cursorPos - 1, 0 );
                this.scrollTime := Clock;
            elsif mouse.x > this.viewport.width - this.style.Get_Pad_Right( BACKGROUND_ELEM ) then
                cursorPos := Integer'Min( cursorPos + 1, Length( this.text ) );
                this.scrollTime := Clock;
            end if;
        end if;

        this.Set_Cursor_Pos( cursorPos );
    end On_Mouse_Moved;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Pressed( this  : not null access Input_Box'Class;
                                mouse : Button_Arguments ) is
    begin
        this.Set_Cursor_Pos( this.Find_Pos( mouse.x ) );
        if not mouse.modifiers(SHIFT) then
            this.selectPos := this.cursorPos;
        end if;
    end On_Mouse_Pressed;

    ----------------------------------------------------------------------------

    procedure On_Style_Changed( this : not null access Input_Box'Class ) is
    begin
        -- repack because the style may change the font or padding, which may
        -- change the minimum size hint
        this.Update_Geometry;
    end On_Style_Changed;

    ----------------------------------------------------------------------------

    procedure On_Resized( this : not null access Input_Box'Class ) is
    begin
        this.Calculate_Visible_Text;
    end On_Resized;

    ----------------------------------------------------------------------------

    procedure Replace_Selection( this : not null access Input_Box'class;
                                 str  : String ) is
        newText       : Unbounded_String;
        selectedFirst,
        selectedLast  : Integer;
    begin
        pragma Assert( this.selectPos /= this.cursorPos, "No selection to replace" );

        selectedFirst := Integer'Min( this.cursorPos, this.selectPos ) + 1;
        selectedLast := Integer'Max( this.cursorPos, this.selectPos );

        newText := To_Unbounded_String(
                       Slice( this.text, 1, selectedFirst - 1 ) &
                       str &
                       Slice( this.text, selectedLast + 1, Length( this.text ) ) );

        if Length( newText ) <= this.maxLen then
            this.Set_Text_Internal( To_String( newText ) );
        end if;

        if this.text = newText then
            -- replacement succeeded
            this.Set_Cursor_Pos( selectedFirst - 1 + str'Length, textChanged => True );
            this.selectPos := this.cursorPos;
        end if;
    end Replace_Selection;

    ----------------------------------------------------------------------------

    procedure Select_All( this : not null access Input_Box'Class ) is
    begin
        this.selectPos := 0;
        this.Set_Cursor_Pos( Length( this.text ) );
        this.Show_Cursor;
    end Select_All;

    ----------------------------------------------------------------------------

    procedure Set_Constraint( this       : not null access Input_Box'Class;
                              constraint : A_Constrain_Func ) is
        len : constant Natural := Length( this.text );
    begin
        this.constrain := constraint;
        if this.constrain = null then
            this.constrain := Unconstrained'Access;
        end if;
        this.text := To_Unbounded_String( "" );
        this.Set_Text_Internal( To_String( this.text ), undoable => False );
        this.Set_Cursor_Pos( Integer'Min( this.cursorPos, len ), textChanged => True );
        this.selectPos := this.cursorPos;
        this.history.Clear;
        this.histIndex := 1;
    end Set_Constraint;

    ----------------------------------------------------------------------------

    procedure Set_Cursor_Pos( this        : not null access Input_Box'Class;
                              pos         : Integer;
                              textChanged : Boolean := False ) is
    begin
        if pos /= this.cursorPos then
            this.cursorPos := pos;
        end if;

        -- if 'textChanged' is set, then the visible text has to be recalculated
        -- because either the text or the viewport changed.
        if textChanged or this.cursorPos < this.viewFirst or this.cursorPos >= this.viewLast then
            this.Calculate_Visible_Text;
        end if;
    end Set_Cursor_Pos;

    ----------------------------------------------------------------------------

    procedure Set_Max_Length( this : not null access Input_Box'Class; max : Positive ) is
    begin
        this.maxLen := max;
        if Length( this.text ) > this.maxLen then
            this.Set_Text_Internal( Slice( this.text, 1, this.maxLen ), undoable => False );
            this.Set_Cursor_Pos( Integer'Min( this.cursorPos, Length( this.text ) ), textChanged => True );
            this.selectPos := this.cursorPos;
        end if;
        this.history.Clear;
        this.histIndex := 1;
    end Set_Max_Length;

    ----------------------------------------------------------------------------

    procedure Set_Text( this     : not null access Input_Box'Class;
                        text     : String;
                        acceptIt : Boolean := False ) is
    begin
        this.Set_Text_Internal( text(text'First..Integer'Min( text'Last, text'First + this.maxLen - 1 )), undoable => False );
        this.Set_Cursor_Pos( Length( this.text ), textChanged => True );
        this.selectPos := this.cursorPos;
        if acceptIt then
            this.Do_Enter;
        end if;
    end Set_Text;

    ----------------------------------------------------------------------------

    procedure Set_Text_Internal( this     : not null access Input_Box'Class;
                                 text     : String;
                                 undoable : Boolean := True ) is
        prev : constant Unbounded_String := this.text;
        now  : constant Time := Clock;
    begin
        this.text := To_Unbounded_String( this.constrain( To_String( this.text ), text ) );
        if undoable then
            -- discard history that was left undone
            while this.undoIndex < Integer(this.undoHistory.Length) loop
                this.undoHistory.Delete_Last;
            end loop;

            if Integer(this.undoHistory.Length) < 1 or else this.undoHistory.Last_Element.timestamp + Seconds( 1 ) < now then
                this.undoHistory.Append( Input_State'(beforeText => prev,
                                                      cursorPos  => this.cursorPos,
                                                      afterText  => this.text,
                                                      timestamp  => now) );
            else
                -- combine states that occur within 1 second of each other
                this.undoHistory.Replace_Element( this.undoHistory.Last,
                                                  Input_State'(beforeText => this.undoHistory.Last_Element.beforeText,
                                                               cursorPos  => this.undoHistory.Last_Element.cursorPos,
                                                               afterText  => this.text,
                                                               timestamp  => now) );
            end if;
        else
            this.undoHistory.Clear;
        end if;
        this.undoIndex := Integer(this.undoHistory.Length);
    end Set_Text_Internal;

    ----------------------------------------------------------------------------

    procedure Show_Cursor( this : not null access Input_Box'Class ) is
    begin
        -- make sure the cursor is visible by resetting the blink time
        this.blinkTime := Clock;
    end Show_Cursor;

begin

    Register_Style( "InputBox",
                    (BACKGROUND_ELEM => To_Unbounded_String( "background" ),
                     BORDER_ELEM     => To_Unbounded_String( "border" ),
                     TEXT_ELEM       => To_Unbounded_String( "text" ),
                     SELAREA_ELEM    => To_Unbounded_String( "selection" ),
                     SELTEXT_ELEM    => To_Unbounded_String( "selectedText")),
                    (BACKGROUND_ELEM => Area_Element,
                     BORDER_ELEM     => Area_Element,
                     TEXT_ELEM       => Text_Element,
                     SELAREA_ELEM    => Text_Element,
                     SELTEXT_ELEM    => Text_Element),
                    (0 => To_Unbounded_String( "disabled" ),
                     1 => To_Unbounded_String( "focus" ),
                     2 => To_Unbounded_String( "hover" )) );

end Widgets.Input_Boxes;
