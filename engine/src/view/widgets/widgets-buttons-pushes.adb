--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Keyboard;                  use Allegro.Keyboard;
with Gamepads;                          use Gamepads;

package body Widgets.Buttons.Pushes is

    package Connections is new Signals.Connections(Push_Button);
    use Connections;

    package Gamepad_Connections is new Signals.Gamepads.Connections(Push_Button);
    use Gamepad_Connections;

    package Key_Connections is new Signals.Keys.Connections(Push_Button);
    use Key_Connections;

    package Mouse_Connections is new Signals.Mouse.Connections(Push_Button);
    use Mouse_Connections;

    ----------------------------------------------------------------------------

    procedure On_Blurred( this : not null access Push_Button'Class );

    procedure On_Gamepad_Pressed( this    : not null access Push_Button'Class;
                                  gamepad : Gamepad_Button_Arguments );

    procedure On_Gamepad_Released( this    : not null access Push_Button'Class;
                                   gamepad : Gamepad_Button_Arguments );

    procedure On_Key_Enter_Pressed( this : not null access Push_Button'Class );

    procedure On_Key_Enter_Released( this : not null access Push_Button'Class );

    -- Handles the left mouse button, emitting the button.held signal while the
    -- left mouse button is held.
    procedure On_Mouse_Held( this : not null access Push_Button'Class );

    -- Handles the left mouse button to press the button.
    procedure On_Mouse_Pressed( this : not null access Push_Button'Class );

    -- Handles the left mouse button to release the button. A Click action
    -- occurs only if the mouse is released while hovering on the button.
    -- Otherwise the click is considered aborted. The state still changes to
    -- False and a Release action still occurs.
    procedure On_Mouse_Released( this  : not null access Push_Button'Class;
                                 mouse : Button_Arguments );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Push_Button( view : not null access Game_Views.Game_View'Class;
                                 id   : String := "" ) return A_Button
        is (Create_Push_Button( view, id, "", NO_ICON ));

    ----------------------------------------------------------------------------

    function Create_Push_Button( view : not null access Game_Views.Game_View'Class;
                                 id   : String;
                                 text : String;
                                 icon : Icon_Type := NO_ICON ) return A_Button is
        this : A_Push_Button := new Push_Button;
    begin
        this.Construct( view, id, text, icon );
        return A_Button(this);
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Push_Button;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Push_Button;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String;
                         text : String;
                         icon : Icon_Type ) is
    begin
        Button(this.all).Construct( view, id, "Button", text, icon );

        this.Blurred.Connect( Slot( this, On_Blurred'Access ) );

        this.GamepadPressed.Connect( Slot( this, On_Gamepad_Pressed'Access, BUTTON_SOUTH, priority => Low ) );
        this.GamepadReleased.Connect( Slot( this, On_Gamepad_Released'Access, BUTTON_SOUTH, priority => Low ) );

        this.KeyPressed.Connect( Slot( this, On_Key_Enter_Pressed'Access, ALLEGRO_KEY_SPACE, priority => Low ) );
        this.KeyPressed.Connect( Slot( this, On_Key_Enter_Pressed'Access, ALLEGRO_KEY_ENTER, priority => Low ) );
        this.KeyPressed.Connect( Slot( this, On_Key_Enter_Pressed'Access, ALLEGRO_KEY_PAD_ENTER, priority => Low ) );
        this.KeyReleased.Connect( Slot( this, On_Key_Enter_Released'Access, ALLEGRO_KEY_SPACE, priority => Low ) );
        this.KeyReleased.Connect( Slot( this, On_Key_Enter_Released'Access, ALLEGRO_KEY_ENTER, priority => Low ) );
        this.KeyReleased.Connect( Slot( this, On_Key_Enter_Released'Access, ALLEGRO_KEY_PAD_ENTER, priority => Low ) );

        this.MouseHeld.Connect( Slot( this, On_Mouse_Held'Access, Mouse_Left, priority => Low ) );
        this.MousePressed.Connect( Slot( this, On_Mouse_Pressed'Access, Mouse_Left, priority => Low ) );
        this.MouseReleased.Connect( Slot( this, On_Mouse_Released'Access, Mouse_Left, priority => Low ) );
    end Construct;

    ----------------------------------------------------------------------------

    procedure On_Blurred( this : not null access Push_Button'Class ) is
    begin
        this.Set_State( False );
        this.gamepadId := 0;
    end On_Blurred;

    ----------------------------------------------------------------------------

    procedure On_Gamepad_Pressed( this    : not null access Push_Button'Class;
                                  gamepad : Gamepad_Button_Arguments ) is
    begin
        this.Set_State( True );
        this.gamepadId := gamepad.id;
    end On_Gamepad_Pressed;

    ----------------------------------------------------------------------------

    procedure On_Gamepad_Released( this    : not null access Push_Button'Class;
                                   gamepad : Gamepad_Button_Arguments ) is
    begin
        this.Set_State( False );
        if this.gamepadId = gamepad.id then
            this.Clicked.Emit;
        end if;
        this.gamepadId := 0;
    end On_Gamepad_Released;

    ----------------------------------------------------------------------------

    procedure On_Key_Enter_Pressed( this : not null access Push_Button'Class ) is
    begin
        this.Set_State( True );
    end On_Key_Enter_Pressed;

    ----------------------------------------------------------------------------

    procedure On_Key_Enter_Released( this : not null access Push_Button'Class ) is
    begin
        this.Set_State( False );
        this.Clicked.Emit;
    end On_Key_Enter_Released;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Held( this : not null access Push_Button'Class ) is
    begin
        this.Held.Emit;
    end On_Mouse_Held;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Pressed( this : not null access Push_Button'Class ) is
    begin
        this.Set_State( True );
    end On_Mouse_Pressed;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Released( this  : not null access Push_Button'Class;
                                 mouse : Button_Arguments ) is
    begin
        this.Set_State( False );
        if mouse.x >= 0.0 and then mouse.y >= 0.0 and then
           mouse.x < this.Width.Get and then mouse.y < this.Height.Get
        then
            this.Clicked.Emit;
        end if;
    end On_Mouse_Released;

    ----------------------------------------------------------------------------

    overriding
    procedure Toggle_State( this : access Push_Button ) is
    begin
        -- Change the state twice because it's a push button; it's not supposed
        -- to stay pressed down.
        this.Set_State( not this.on );
        this.Set_State( not this.on );
    end Toggle_State;

end Widgets.Buttons.Pushes;
