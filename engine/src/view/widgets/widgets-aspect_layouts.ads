--
-- Copyright (c) 2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Widgets.Aspect_Layouts is

    -- An Aspect_Layout is a layout widget designed to maintain an aspect ratio
    -- within its parent. Its own size will maintain the aspect ratio of its
    -- base size, growing to fill its parent and shrinking no smaller that its
    -- minimum size.
    --
    -- The layout can optionally scale its size in integer multiples of its base
    -- size. It can optionally zoom its content proportionately to its scaling,
    -- allowing it to maintain a fixed content size regardless of its visual
    -- size.
    --
    -- Padding is controlled by its widget style; it has no visual components.
    type Aspect_Layout is new Widget with private;
    type A_Aspect_Layout is access all Aspect_Layout'Class;

    -- Creates a new Aspect_Layout within 'view' with id 'id'.
    function Create_Aspect_Layout( view : not null access Game_Views.Game_View'Class;
                                   id   : String := "" ) return A_Aspect_Layout;
    pragma Postcondition( Create_Aspect_Layout'Result /= null );

    -- Adds a child widget to the layout. The Aspect_Layout will own 'child'
    -- after this call and will delete it upon destruction.
    procedure Add_Widget( this  : not null access Aspect_Layout'Class;
                          child : not null access Widget'Class );

    -- Removes and deletes all child widgets in the layout.
    procedure Clear_Widgets( this : not null access Aspect_Layout'Class );

    -- Enable/disable auto-zoom to keep the layout's content size fixed at the
    -- base size as the widget grows or shrinks.
    procedure Enable_Auto_Zoom( this    : not null access Aspect_Layout'Class;
                                enabled : Boolean );

    -- Enable/disable scaling in whole integer multiples of the base size.
    procedure Enable_Integer_Scaling( this    : not null access Aspect_Layout'Class;
                                      enabled : Boolean );

    -- Removes child widget 'child' from the layout.
    procedure Remove_Widget( this  : not null access Aspect_Layout'Class;
                             child : not null access Widget'Class );

    -- Sets the base size for the layout. The aspect ratio of this size will be
    -- preserved.
    procedure Set_Base_Size( this   : not null access Aspect_Layout'Class;
                             width  : Float;
                             height : Float );

private

    BACKGROUND_ELEM : constant := 1;

    DISABLED_STATE : constant Widget_State := 2**0;

    ----------------------------------------------------------------------------

    type Aspect_Layout is new Widget with
        record
            baseWidth  : Float := 0.0;
            baseHeight : Float := 0.0;
            intScaling : Boolean := False;       -- resize only in integer multiples of base size
            autoZoom   : Boolean := False;       -- zoom to keep content a fixed size
            prevParent : A_Widget := null;
        end record;

    procedure Construct( this : access Aspect_Layout;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );

end Widgets.Aspect_Layouts;
