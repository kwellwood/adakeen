--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Widgets.Scenes.Cameras is

    type Camera is new Limited_Object with private;
    type A_Camera is access all Camera'Class;

    function Create_Camera( scene : not null A_Scene ) return A_Camera;
    pragma Postcondition( Create_Camera'Result /= null );

    -- Enables/disables camera wall tiles blocking the camera. When enabled, its
    -- movement will be constrained to keep tiles with the "BlockCamera" property
    -- out of the scene.
    procedure Enable_Camera_Walls( this    : not null access Camera'Class;
                                   enabled : Boolean );

    -- Enables/disables loose tracking, allowing the scene to accelerate toward
    -- the focus if the bounds change, causing the focus to be out of bounds.
    procedure Enable_Loose_Tracking( this    : not null access Camera'Class;
                                     enabled : Boolean );

    -- Returns the current focus point of the camera in world coordinates.
    function Get_FocusX( this : not null access Camera'Class ) return Float;
    function Get_FocusY( this : not null access Camera'Class ) return Float;

    -- Scrolls the viewport by 'x', 'y' in world coordinates. If the scene is
    -- following an entity, then scrolling will be constrained so as to keep the
    -- camera within the current camera bounds.
    procedure Scroll_Scene( this : not null access Camera'Class; x, y : Float );

    -- Updates the bounds of the camera box containing the focus point. The
    -- bounds are in world coordinates, relative to the center of the viewport.
    -- If the focus point is outside of the new camera box, loose tracking will
    -- be turned on to allow the camera to accelerate to where it needs to be to
    -- get the focus point into the new camera bounding box.
    procedure Set_Bounds( this     : not null access Camera'Class;
                          boundsX1,
                          boundsY1,
                          boundsX2,
                          boundsY2 : Float );

    -- Sets the distance (in world units) that the focus can move before the
    -- scene is forced to recenter on it. Infinite by default.
    procedure Set_Centering_Limit( this     : not null access Camera'Class;
                                   distance : Float );

    -- Moves the focus to 'x,y' in world coordinates. The scene will be scrolled
    -- to keep the focus within bounds unless it is constrained by map edges or
    -- camera blocking tiles. If 'center' is True then the camera will be
    -- positioned such that 'x,y' is in the center of the bounds.
    --
    -- When centering the focus point, map edges will still constrain the camera
    -- position, but blocking tiles do not. Think of centering the camera as
    -- teleporting it to a new location, instead of smoothly tracking a point as
    -- it moves.
    procedure Set_Focus( this   : not null access Camera'Class;
                         x, y   : Float;
                         center : Boolean := False );

    -- Updates the location of the camera, scrolling it to keep the focus within
    -- the camera bounding box.
    procedure Tick( this : not null access Camera'Class; time : Tick_Time );

private

    type Camera is new Limited_Object with
        record
            scene           : A_Scene := null;      -- the scene to control

            -- focus point (world units) to keep within bounds by moving viewport
            focusX, focusY  : Float := 0.0;

            -- force centering if the focus changes by more than this (world units)
            recenterLimit   : Float := 1.0e9;

            -- bounds to constrain the focus (world units), relative to the
            -- center of the viewport
            bounds          : Rectangle := (0.0, 0.0, 0.0, 0.0);

            obeyCameraWalls : Boolean := False;     -- camera wall tiles block the camera's edge?
            looseTracking   : Boolean := False;     -- allow loose tracking?
            looseTrackingX,                         -- follow the focux loosely in X
            looseTrackingY  : Boolean := False;     --   or Y, with acceleration
            loosePrevDistX,                         -- when loose tracking, previous frame
            loosePrevDistY  : Float := 0.0;         -- distance from .focusXY to camera bounds

            camSpeed        : Float := 0.0;         -- camera movement constants
            camAccelDist    : Float := 0.0;         -- used for loose tracking
            camAccelAmount  : Float := 0.0;         --
        end record;

    procedure Construct( this : access Camera; scene : not null A_Scene );

    -- Clips the edges of the camera to camera walls. 'vx1', 'vy1' is the next
    -- desired location of the viewport. It will be constrained according to map
    -- tiles with the "blockCamera" attribute. This procedure only has an effect
    -- on 'vx1', 'vy1' if the .obeyCameraWalls field is true.
    procedure Clip_To_Walls( this     : not null access Camera'Class;
                             vx1, vy1 : in out Float );

    -- Returns 0.0 if .focusX is currently within the camera bounding box on the
    -- X axis, otherwise the distance that the center point X would have to move
    -- to catch up with the focus X.
    function Get_FocusX_To_Bounds( this : not null access Camera'Class ) return Float;

    -- Returns 0.0 if .focusY is currently within the camera bounding box on the
    -- Y axis, otherwise the distance that the center point Y would have to move
    -- to catch up with the focus Y.
    function Get_FocusY_To_Bounds( this : not null access Camera'Class ) return Float;

    -- Updates the Scene's viewport to keep the focus within 'bounds'. After
    -- updating the viewport to follow the focus point, the viewport will be
    -- further shifted by 'shiftX,shiftY' in world coordinates.
    --
    -- The movement of the Scene's viewport will be constrained by the following:
    --
    -- * If a map is loaded, the Scene's viewport will remain fully on the map
    --   unless the map is smaller than the viewport. The upper left of the map
    --   will always be in the viewport.
    -- * If .obeyCameraWalls is true, the edge of the viewport will be blocked
    --   by camera blocking tiles that are off screen.
    procedure Track_Focus( this   : not null access Camera'Class;
                           shiftX : Float;
                           shiftY : Float;
                           bounds : Rectangle );

end Widgets.Scenes.Cameras;
