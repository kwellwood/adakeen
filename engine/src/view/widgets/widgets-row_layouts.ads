--
-- Copyright (c) 2017-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Vectors;

package Widgets.Row_Layouts is

    -- A Row_Layout is a simple layout widget that aligns a series of widgets
    -- into a row. It has no visible style components. Padding, spacing and
    -- alignment are controlled by its widget style.
    type Row_Layout is new Widget with private;
    type A_Row_Layout is access all Row_Layout'Class;

    -- Creates a new Row_Layout within 'view' with id 'id'.
    function Create_Row_Layout( view : not null access Game_Views.Game_View'Class;
                                id   : String := "" ) return A_Row_Layout;
    pragma Postcondition( Create_Row_Layout'Result /= null );

    -- Appends a widget to the row. The Row_Layout will own 'child' after this
    -- call and will delete it upon destruction.
    procedure Append( this  : not null access Row_Layout'Class;
                      child : not null access Widget'Class );

    -- Removes and deletes all child widgets in the layout.
    procedure Clear( this : not null access Row_Layout'Class );

    -- Returns the number of widgets in the layout.
    function Count( this : not null access Row_Layout'Class ) return Natural;

    -- Inserts a widget into the row before index 'index' (starting at 1). This
    -- is equivalent to appending the widget if 'index' is greater than the
    -- child count. The Row_Layout will own 'child' after this call and will
    -- delete it upon destruction.
    procedure Insert( this  : not null access Row_Layout'Class;
                      index : Integer;
                      child : not null access Widget'Class );

    -- Returns a pointer to the widget at index 'index' (starting at 1). If the
    -- index does not exist, null will be returned.
    function Get_At( this : not null access Row_Layout'Class; index : Integer ) return A_Widget;

    -- Returns a pointer to the expander widget, if it has been set. Otherwise,
    -- null will be returned.
    function Get_Expander( this : not null access Row_Layout'Class ) return A_Widget;

    -- Returns a pointer to the first widget, or null if the layout is empty.
    function Get_First( this : not null access Row_Layout'Class ) return A_Widget;

    -- Returns a pointer to the last widget, or null if the layout is empty.
    function Get_Last( this : not null access Row_Layout'Class ) return A_Widget;

    -- Removes and returns the widget at index 'index' (starting at 1). If the
    -- index does not exist, null will be returned.
    function Remove( this : not null access Row_Layout'Class; index : Integer ) return A_Widget;

    -- Sets the child that expands and shrinks to fill extra space after all
    -- children have been given their preferred space. If 'child' is not already
    -- a child of the layout, nothing will change. Otherwise, if 'child' is null,
    -- then the expander will be cleared.
    procedure Set_Expander( this  : not null access Row_Layout'Class;
                            child : access Widget'Class );

    -- Swaps the widgets at index 'indexA' and 'indexB'. If either index is not
    -- valid, nothing will change.
    procedure Swap( this   : not null access Row_Layout'Class;
                    indexA : Integer;
                    indexB : Integer );

    ----------------------------------------------------------------------------

    -- A Column_Layout is a simple layout widget that aligns a series of widgets
    -- into a column. It has no visible style components. Padding, spacing and
    -- alignment are controlled by its widget style.
    type Column_Layout is new Row_Layout with private;
    type A_Column_Layout is access all Column_Layout'Class;

    -- Creates a new Column_Layout within 'view' with id 'id'.
    function Create_Column_Layout( view : not null access Game_Views.Game_View'Class;
                                   id   : String := "" ) return A_Column_Layout;
    pragma Postcondition( Create_Column_Layout'Result /= null );

private

    BACKGROUND_ELEM : constant := 1;

    DISABLED_STATE : constant Widget_State := 2**0;

    package Widget_Vectors is new Ada.Containers.Vectors( Positive, A_Widget, "=" );

    ----------------------------------------------------------------------------

    type Row_Layout is new Widget with
        record
            elements : Widget_Vectors.Vector;      -- all elements, ordered
            visibles : Widget_Vectors.Vector;      -- only visible elements, ordered
            expander : A_Widget := null;
        end record;

    procedure Construct( this       : access Row_Layout;
                         view       : not null access Game_Views.Game_View'Class;
                         id         : String;
                         widgetType : String );

    function Get_Min_Height( this : access Row_Layout ) return Float;

    function Get_Min_Width( this : access Row_Layout ) return Float;

    -- Returns the negative edge anchor of child 'child', or the positive edge
    -- anchor if 'inverted' is True. The negative edge is the edge between child
    -- 'N-1' and child 'N', and the positive edge is the edge between child 'N'
    -- and child 'N+1'.
    --
    -- This function also accounts for alignment. If the horizontal alignment is
    -- leftified or centered, then the child ordering is left-to-right. Thus,
    -- Left is the negative edge. But if the horizontal alignment is right, then
    -- child ordering is right to left and so Right is the negative edge.
    function Get_Negative_Edge( this     : access Row_Layout;
                                child    : access Widget'Class;
                                inverted : Boolean ) return A_Anchor;

    procedure Hook_Adjacent_Edges( this     : not null access Row_Layout'Class;
                                   visIndex : Positive );

    procedure Hook_Aligned_Edges( this : access Row_Layout; child : A_Widget );

    procedure Refresh_Anchors( this : not null access Row_Layout'Class );

    procedure Unhook_Adjacent_Edges( this     : not null access Row_Layout'Class;
                                     visIndex : Positive );

    ----------------------------------------------------------------------------

    type Column_Layout is new Row_Layout with null record;

    function Get_Min_Height( this : access Column_Layout ) return Float;

    function Get_Min_Width( this : access Column_Layout ) return Float;

    function Get_Negative_Edge( this     : access Column_Layout;
                                child    : access Widget'Class;
                                inverted : Boolean ) return A_Anchor;

    procedure Hook_Aligned_Edges( this : access Column_Layout; child : A_Widget );

end Widgets.Row_Layouts;
