--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Assets.Fonts;                      use Assets.Fonts;
--with Debugging;                         use Debugging;
with Drawing;                           use Drawing;
with Drawing.Bitmaps;                   use Drawing.Bitmaps;
with Drawing.Primitives;                use Drawing.Primitives;
with Drawing.Tiles;                     use Drawing.Tiles;
with Game_Views;                        use Game_Views;
with Styles;                            use Styles;
with Support;                           use Support;
with Values.Strings;                    use Values.Strings;
with Widget_Styles.Registry;            use Widget_Styles.Registry;

package body Widgets.Labels is

    package Connections is new Signals.Connections(Label);
    use Connections;

    ----------------------------------------------------------------------------

    procedure On_Style_Changed( this : not null access Label'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Label( view : not null access Game_Views.Game_View'Class;
                           id   : String := "" ) return A_Label is
        this : A_Label := new Label;
    begin
        this.Construct( view, id, "", NO_ICON );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Label;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Label;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String;
                         text : String;
                         icon : Icon_Type ) is
    begin
        Widget(this.all).Construct( view, id, "Label" );
        this.StyleChanged.Connect( Slot( this, On_Style_Changed'Access ) );

        this.Set_Focusable( False );
        this.Set_Text( text );
        this.Set_Icon( (if icon /= NO_ICON then icon else this.style.Get_Image( ICON_ELEM )) );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Content( this : access Label ) is
        state         : constant Widget_State := this.Get_Visual_State;
        text          : constant String := To_String( this.text );
        align         : constant Align_Type := this.style.Get_Align( BACKGROUND_ELEM, state );
        icon          : constant Icon_Type := this.style.Get_Image( ICON_ELEM, state );
        color         : Allegro_Color;
        font          : A_Font;
        iconWidth     : Float := 0.0;
        iconHeight    : Float := 0.0;
        textWidth     : Float := 0.0;
        textHeight    : Float := 0.0;
        contentWidth  : Float := 0.0;
        contentHeight : Float := 0.0;
        contentX      : Float;
        contentY      : Float;
        iconX, iconY  : Float;
        textX, textY  : Float;
        stretchIcon   : Boolean := True;    -- stretch the icon or fit it?
    begin
        -- - - - Calculate Positions - - - --

        -- calculate icon size --
        if icon.Get_Bitmap /= null then
            iconWidth := this.style.Get_Width( ICON_ELEM, state );
            if iconWidth = 0.0 then
                iconWidth := Float(icon.Get_Width);
                stretchIcon := False;
            end if;
            iconWidth := Float'Min( iconWidth, this.viewport.width );
            iconHeight := this.style.Get_Height( ICON_ELEM, state );
            if iconHeight = 0.0 then
                iconHeight := Float(icon.Get_Height);
                stretchIcon := False;
            end if;
            iconHeight := Float'Min( iconHeight, this.viewport.height );
        end if;

        -- calculate text size --
        if text'Length > 0 then
            font := this.style.Get_Font( TEXT_ELEM, state );
            textWidth := Float(font.Text_Length( text ));
            textHeight := Float(font.Line_Height);
        end if;

        -- calculate total content size --
        if iconWidth > 0.0 and textWidth > 0.0 then
            contentWidth := iconWidth + this.style.Get_Spacing( BACKGROUND_ELEM, state ) + textWidth;
        elsif textWidth > 0.0 then
            contentWidth := textWidth;
        else
            contentWidth := iconWidth;
        end if;
        contentHeight := Float'Max( iconHeight, textHeight );

        -- calculate alignment --
        Align_Rect( align,
                    this.viewport.width - (this.style.Get_Pad_Left( BACKGROUND_ELEM, state ) + this.style.Get_Pad_Right( BACKGROUND_ELEM, state )),
                    this.viewport.height - (this.style.Get_Pad_Top( BACKGROUND_ELEM, state ) + this.style.Get_Pad_Bottom( BACKGROUND_ELEM, state )),
                    contentWidth, contentHeight,
                    contentX, contentY );
        contentX := this.style.Get_Pad_Left( BACKGROUND_ELEM, state ) + contentX;
        contentY := this.style.Get_Pad_Top( BACKGROUND_ELEM, state ) + contentY;
        iconX := Float'Rounding( contentX + this.style.Get_Offset_X( ICON_ELEM, state ) );
        iconY := Float'Rounding( contentY + Align_Vertical( align, contentHeight, iconHeight ) + this.style.Get_Offset_Y( ICON_ELEM, state ) );
        textX := contentX + (contentWidth - textWidth) + this.style.Get_Offset_X( TEXT_ELEM, state );
        textY := contentY + Align_Vertical( align, contentHeight, textHeight ) + this.style.Get_Offset_Y( TEXT_ELEM, state );

        -- - - - Draw Content - - - --

        -- draw the background --
        color := this.style.Get_Color( BACKGROUND_ELEM, state );
        if color /= Transparent then
            Rectfill( 0.0, 0.0, this.viewport.width, this.viewport.height,
                      Lighten( color, this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );
        else
            Draw_Tile_Filled( this.style.Get_Image( BACKGROUND_ELEM, state ).Get_Tile,
                              0.0, 0.0, 0.0,
                              this.viewport.width, this.viewport.height,
                              Lighten( this.style.Get_Tint( BACKGROUND_ELEM, state ),
                                       this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );
        end if;

        -- draw the border --
        Draw_Tile_Filled( this.style.Get_Image( BORDER_ELEM, state ).Get_Tile,
                          0.0, 0.0, 0.0,
                          this.viewport.width, this.viewport.height,
                          Lighten( this.style.Get_Tint( BORDER_ELEM, state ),
                                   this.style.Get_Shade( BORDER_ELEM, state ) ) );

        -- draw the content --
        if icon.Get_Bitmap /= null then
            Draw_Bitmap_Stretched( icon.Get_Bitmap,
                                   iconX, iconY, 0.0,
                                   iconWidth, iconHeight,
                                   (if stretchIcon then Stretch else Fit),
                                   0.0,
                                   Lighten( this.style.Get_Tint( ICON_ELEM, state ),
                                            this.style.Get_Shade( ICON_ELEM, state ) ) );
        end if;
        if font /= null then
            font.Draw_String( text,
                              textX, textY,
                              Lighten( this.style.Get_Color( TEXT_ELEM, state ),
                                       this.style.Get_Shade( TEXT_ELEM, state ) ) );
        end if;
    end Draw_Content;

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Height( this : access Label ) return Float is
        padTop     : constant Float := this.style.Get_Pad_Top( BACKGROUND_ELEM );
        padBottom  : constant Float := this.style.Get_Pad_Bottom( BACKGROUND_ELEM );
        icon       : constant Icon_Type := this.style.Get_Image( ICON_ELEM );
        iconHeight : Float := 0.0;
        textHeight : Float := 0.0;
    begin
        if icon.Get_Bitmap /= null then
            iconHeight := (if this.style.Get_Height( ICON_ELEM ) > 0.0
                           then this.style.Get_Height( ICON_ELEM )
                           else Float(icon.Get_Height));
        end if;

        if Length( this.text ) > 0 then
            -- Note: Reserve "Line Height" in space, not "Ascent"
            textHeight := Float(this.style.Get_Font( TEXT_ELEM ).Line_Height);
        end if;

        return padTop + Float'Max( iconHeight, textHeight ) + padBottom;
    end Get_Min_Height;

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Width( this : access Label ) return Float is
        padLeft   : constant Float := this.style.Get_Pad_Left( BACKGROUND_ELEM );
        padRight  : constant Float := this.style.Get_Pad_Right( BACKGROUND_ELEM );
        icon      : constant Icon_Type := this.style.Get_Image( ICON_ELEM );
        spacing   : constant Float := this.style.Get_Spacing( BACKGROUND_ELEM );
        iconWidth : Float := 0.0;
        textWidth : Float := 0.0;
    begin
        if icon.Get_Bitmap /= null then
            iconWidth := (if this.style.Get_Width( ICON_ELEM ) > 0.0
                          then this.style.Get_Width( ICON_ELEM )
                          else Float(icon.Get_Width));
        end if;

        if Length( this.text ) > 0 then
            textWidth := Float(this.style.Get_Font( TEXT_ELEM ).Text_Length( To_String( this.text ) ));
        end if;

        if iconWidth > 0.0 and then textWidth > 0.0 then
            return padLeft + iconWidth + spacing + textWidth + padRight;
        end if;

        -- one of these widths is zero
        return padLeft + Float'Max( iconWidth, textWidth ) + padRight;
    end Get_Min_Width;

    ----------------------------------------------------------------------------

    function Get_Icon( this : not null access Label'Class ) return Icon_Type is (this.icon);

    ----------------------------------------------------------------------------

    function Get_Text( this : not null access Label'Class ) return String is (To_String( this.text ));

    ----------------------------------------------------------------------------

    overriding
    function Get_Visual_State( this : access Label ) return Widget_State is
    ((if not this.Is_Enabled then DISABLED_STATE else 0));

    ----------------------------------------------------------------------------

    procedure On_Style_Changed( this : not null access Label'Class ) is
    begin
        -- don't allow a style that doesn't define an icon to remove the
        -- existing one. set the icon back again.
        if this.icon /= NO_ICON and then this.style.Get_Image( ICON_ELEM ) = NO_ICON then
            this.Set_Icon( this.icon );                -- keep the same icon
        else
            this.Set_Icon( this.style.Get_Image( ICON_ELEM ) );
        end if;
    end On_Style_Changed;

    ----------------------------------------------------------------------------

    procedure Set_Icon( this : not null access Label'Class;
                        icon : Icon_Type ) is
        tileId : constant Natural := icon.Get_Tile.Get_Id;
    begin
        this.icon := icon;
        this.style.Set_Property( ICON_ELEM, STYLE_IMAGE, DEFAULT_STATE,
                                 Create( (if tileId > 0
                                          then icon.Get_Library.Get.Get_Name & ":" & Image( tileId )
                                          else "") ) );
        this.Update_Geometry;
    end Set_Icon;

    ----------------------------------------------------------------------------

    procedure Set_Text( this : not null access Label'Class; text : String ) is
    begin
        this.text := To_Unbounded_String( text );
        this.Update_Geometry( X_AXIS );   -- text only affects the minimum width hint
    end Set_Text;

    ----------------------------------------------------------------------------

    overriding
    procedure Tick( this : access Label; time : Tick_Time ) is
    begin
        this.style.Update_Animation( BACKGROUND_ELEM, this.Get_Visual_State, time.total );
        this.style.Update_Animation( BORDER_ELEM, this.Get_Visual_State, time.total );
        this.style.Update_Animation( ICON_ELEM, this.Get_Visual_State, time.total );
    end Tick;

    ----------------------------------------------------------------------------

    overriding
    function To_String( this : access Label ) return String
    is (
        "<" & this.Get_Class_Name & " " &
            ": id=" & To_String( this.id ) &
            ", text='" & To_String( this.text ) &
        "'>"
    );

begin

    Register_Style( "Label",
                    (BACKGROUND_ELEM => To_Unbounded_String( "background" ),
                     BORDER_ELEM     => To_Unbounded_String( "border" ),
                     ICON_ELEM       => To_Unbounded_String( "icon" ),
                     TEXT_ELEM       => To_Unbounded_String( "text" )),
                    (BACKGROUND_ELEM => Area_Element,
                     BORDER_ELEM     => Area_Element,
                     ICON_ELEM       => Icon_Element,
                     TEXT_ELEM       => Text_Element),
                    (0 => To_Unbounded_String( "disabled" )) );

end Widgets.Labels;
