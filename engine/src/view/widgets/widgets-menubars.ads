--
-- Copyright (c) 2012-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Native_Dialogs;            use Allegro.Native_Dialogs;
with Widgets.Menu_Items.Sub_Menus;      use Widgets.Menu_Items.Sub_Menus;

limited with Game_Views;

package Widgets.Menubars is

    -- A Menubar spans the top of a Window and contains pulldown menus.
    type Menubar is new Widget with private;
    type A_Menubar is access all Menubar'Class;

    -- Creates a new Menubar within 'view' with widget id 'id'. Each window has
    -- at most one menu bar.
    function Create_Menubar( view : not null access Game_Views.Game_View'Class;
                             id   : String ) return A_Menubar;
    pragma Precondition( id'Length > 0 );
    pragma Postcondition( Create_Menubar'Result /= null );

    -- Adds a menu to the menu bar. 'menu' will be consumed.
    procedure Add_Menu( this : not null access Menubar'Class;
                        menu : in out A_Sub_Menu );
    pragma Precondition( menu /= null );
    pragma Postcondition( menu = null );

private

    type Menubar is new Widget with
        record
            nativeMenu : A_Allegro_Menu := null;
        end record;

    procedure Construct( this : access Menubar;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );
    pragma Precondition( id'Length > 0 );

    procedure Delete( this : in out Menubar );

end Widgets.Menubars;
