--
-- Copyright (c) 2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Icons;                             use Icons;
with Widgets.Buttons.Groups;            use Widgets.Buttons.Groups;
with Widgets.Row_Layouts;               use Widgets.Row_Layouts;
with Widgets.Stack_Layouts;             use Widgets.Stack_Layouts;

package Widgets.Tab_Views is

    -- A Tab_View represents a standard "Tabs" interface element, where a row of
    -- controls along the edge of a stack layout is used to choose the visible
    -- content area between a set of child widgets.
    type Tab_View is new Widget with private;
    type A_Tab_View is access all Tab_View'Class;

    -- Creates a new Tab_View within 'view' with id 'id'.
    function Create_Tab_View( view : not null access Game_Views.Game_View'Class;
                              id   : String := "" ) return A_Tab_View;
    pragma Postcondition( Create_Tab_View'Result /= null );

    -- Appends a new tab 'content', identified by 'title' and 'icon' on its tab.
    -- The Tab_View will own 'content' after this call and will delete it upon
    -- destruction.
    procedure Append( this    : not null access Tab_View'Class;
                      content : not null access Widget'Class;
                      title   : String;
                      icon    : Icon_Type := NO_ICON );

    -- Removes and deletes all tabs.
    procedure Clear( this : not null access Tab_View'Class );

    -- Returns the number of tabs.
    function Count( this : not null access Tab_View'Class ) return Natural;

    -- Returns the index of the first tab matching 'title', starting at index
    -- 'start'. If no match is found, 0 will be returned.
    function Find( this  : not null access Tab_View'Class;
                   title : String;
                   start : Positive := 1 ) return Natural;

    -- Returns a pointer to the tab content at index 'index' (starting at 1). If
    -- the index does not exist, null will be returned.
    function Get_Content( this : not null access Tab_View'Class; index : Integer ) return A_Widget;

    -- Returns the icon of the tab at index 'index' (starting at 1). If the
    -- index does not exist, NO_ICON will be returned.
    function Get_Icon( this : not null access Tab_View'Class; index : Integer ) return Icon_Type;

    -- Returns the index of the currently active tab, or 0 if no tabs exist.
    function Get_Index( this : not null access Tab_View'Class ) return Natural;

    -- Returns the title of the tab at index 'index' (starting at 1). If the
    -- index does not exist, an empty string will be returned.
    function Get_Title( this : not null access Tab_View'Class; index : Integer ) return String;

    -- Inserts a tab before index 'index' (starting at 1), identified by 'title'
    -- and 'icon' on its tab. This is equivalent to appending the tab if 'index'
    -- is greater than the tab count. The Tab_View will own 'content' after this
    -- call and will delete it upon destruction.
    procedure Insert( this    : not null access Tab_View'Class;
                      index   : Integer;
                      content : not null access Widget'Class;
                      title   : String;
                      icon    : Icon_Type := NO_ICON );

    -- Removes and returns the content of the tab at index 'index' (starting at
    -- 1). The caller receives ownership of the returned widget. If the index
    -- does not exist, null will be returned.
    function Remove( this : not null access Tab_View'Class; index : Integer ) return A_Widget;

    -- Sets the icon of the tab at index 'index' to 'icon'. If the index is not
    -- valid, nothing will change.
    procedure Set_Icon( this  : not null access Tab_View'Class;
                        index : Integer;
                        icon  : Icon_Type );

    -- Sets the current tab by index. If 'index' is not a valid index, nothing
    -- will change.
    procedure Set_Index( this  : not null access Tab_View'Class;
                         index : Integer );

    -- Sets the title of the tab at index 'index' to 'title'. If the index is
    -- not valid, nothing will change.
    procedure Set_Title( this  : not null access Tab_View'Class;
                         index : Integer;
                         title : String );

private

    BACKGROUND_ELEM : constant := 1;           -- content background
    BORDER_ELEM     : constant := 2;           -- content border
    HEADER_ELEM     : constant := 3;           -- header area / tab background
    TAB_ELEM        : constant := 4;           -- a tab button

    DISABLED_STATE : constant Widget_State := 2**0;

    ----------------------------------------------------------------------------

    type Tab_View is new Widget with
        record
            buttons       : A_Row_Layout;
            buttonGroup   : A_Button_Group;    -- ensures single-selection behavior
            stack         : A_Stack_Layout;
            headerHeight  : Float := 0.0;
            sigTabChanged : aliased Signal;
        end record;

    procedure Construct( this : access Tab_View;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );

    procedure Delete( this : in out Tab_View );

    -- Draws the tab view's background and header areas.
    procedure Draw_Content( this : access Tab_View );

    function Get_Min_Height( this : access Tab_View ) return Float;

    function Get_Min_Width( this : access Tab_View ) return Float;

    function Get_Preferred_Height( this : access Tab_View ) return Float;

    function Get_Preferred_Width( this : access Tab_View ) return Float;

    -- Returns the visual state of the tab view, based on its enabled state.
    function Get_Visual_State( this : access Tab_View ) return Widget_State;

end Widgets.Tab_Views;
