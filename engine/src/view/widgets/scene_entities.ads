--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Object_Ids;                        use Object_Ids;
with Objects;                           use Objects;
with Signals;                           use Signals;
with Signals.Simple;                    use Signals.Simple;
with Values;                            use Values;
with Values.Maps;                       use Values.Maps;
with Values.Tagged_Ids;                 use Values.Tagged_Ids;
with Widgets;                           use Widgets;

package Scene_Entities is

    -- A scene entity is the visible representation of a game Entity. It is
    -- managed by the Scene widget.
    type Entity is new Limited_Object with private;
    type A_Entity is access all Entity'Class;

    -- Creates a new scene entity corresponding with game entity 'eid'.
    function Create_Entity( eid          : Entity_Id;
                            name         : String;
                            template     : String;
                            initialState : Map_Value ) return A_Entity;
    pragma Postcondition( Create_Entity'Result /= null );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    function AttributeChanged( this : not null access Entity'Class ) return access String_Signal'Class;

    -- Called just after the entity has moved, when an entity moved event is
    -- received from the game. The widget's layout has been updated accordingly.
    function Moved( this : not null access Entity'Class ) return access Signal'Class;

    -- Emitted just after the widget's entity's physical size has changed. The
    -- widget's layout has already been updated as necessary.
    function Resized( this : not null access Entity'Class ) return access Signal'Class;

    -- The entity was added to or removed from the current selection.
    function Selected( this : not null access Entity'Class ) return access Signal'Class;
    function Unselected( this : not null access Entity'Class ) return access Signal'Class;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Creates and returns a Value encapsulting a copy of the scene entity.
    function Create_Copy( this : not null access Entity'Class ) return Value;

    -- Freezes updates to the entity from the game, making Is_Updatable() return
    -- False. Unfreeze the entity once for each time that it is frozen.
    procedure Freeze_Updates( this : not null access Entity'Class; freeze : Boolean );

    -- Returns the value of entity attribute 'name', or null if not defined.
    function Get_Attribute( this : not null access Entity'Class; name : String ) return Value with Inline;

    -- Returns a map of all of the entity's attributes.
    function Get_Attributes( this : not null access Entity'Class ) return Map_Value with Inline;
    pragma Postcondition( Get_Attributes'Result.Valid );

    -- Returns the Entity_Id of the game entity.
    function Get_Entity_Id( this : not null access Entity'Class ) return Entity_Id with Inline;

    -- Returns the unique name of the entity.
    function Get_Entity_Name( this : not null access Entity'Class ) return String with Inline;

    -- Returns the entity's physical height in world units.
    function Get_Entity_Height( this : not null access Entity'Class ) return Float with Inline;

    -- Returns the entity's physical width in world units.
    function Get_Entity_Width( this : not null access Entity'Class ) return Float with Inline;

    -- Returns the current render location of the widget's entity, based on
    -- extrapolation from the latest game frame. This is the center of the
    -- widget's layout area.
    function Get_Render_X( this : not null access Entity'Class ) return Float with Inline;
    function Get_Render_Y( this : not null access Entity'Class ) return Float with Inline;

    -- Returns the name of the entity template that created the widget's entity.
    function Get_Template( this : not null access Entity'Class ) return String with Inline;

    -- Returns the widget that represents the entity in the Scene.
    function Get_Widget( this : not null access Entity'Class ) return A_Widget with Inline;

    -- Returns the location of the widget's entity, based on the latest game
    -- frame.
    function Get_Entity_X( this : not null access Entity'Class ) return Float with Inline;
    function Get_Entity_Y( this : not null access Entity'Class ) return Float with Inline;

    -- Returns the Z depth of the entity. Larger numbers are in the background.
    function Get_Z( this : not null access Entity'Class ) return Float with Inline;

    -- Returns the Z-Order of the widget relative to others with the same Z
    -- depth. Larger numbers are in the background.
    function Get_ZOrder( this : not null access Entity'Class ) return Integer with Inline;

    -- Returns a hint indicating that the entity should not be deletable by the
    -- user in a game editor environment. The Scene is responsible for respecting
    -- this hint.
    function Is_Deletable( this : not null access Entity'Class ) return Boolean with Inline;

    -- Returns True if the widget is selected.
    function Is_Selected( this : not null access Entity'Class ) return Boolean with Inline;

    -- Returns True if the widget should accept updates from the game Entity.
    -- This doesn't restrict any user functionality, it simply indicates that
    -- all entity events should be ignored until the entity is updatable again.
    function Is_Updatable( this : not null access Entity'Class ) return Boolean with Inline;

    -- Moves the rendered location of the entity by 'dx', 'dy' in world
    -- coordinates. The location of the entity's widget will NOT be updated.
    procedure Move_Render_Location( this   : not null access Entity'Class;
                                    dx, dy : Float );

    -- Resizes the entity to 'width', 'height' while keeping the widget centered
    -- at its entity's location. If 'width' or 'height' = 0, the value will not
    -- be changed. (ex: width => 10, height => 0 will change only the width.)
    procedure Resize( this    : not null access Entity'Class;
                      width,
                      height : Float );

    -- Sets entity attribute 'name' to 'val'. The given value will be copied
    -- unless 'consume' is True.
    procedure Set_Attribute( this    : not null access Entity'Class;
                             name    : String;
                             val     : Value'Class;
                             consume : Boolean := False );

    -- Sets the 'Is_Deletable' hint indicating whether or not the user should be
    -- allowed to delete this entity in a game editor environment.
    procedure Set_Deletable( this : not null access Entity'Class; allow : Boolean );

    -- Sets the location of the game entity. This called by update events from
    -- the game logic.
    --
    -- The widget's rendered location (affecting widget layout) is not updated
    -- here; it is updated later in Extrapolate_Render_Location(), based on
    -- extrapolation between game logic frames. If the entity's current velocity
    -- is zero, motion extrapolation will be zeroed until the next frame because
    -- it isn't in motion.
    procedure Set_Entity_Location( this   : not null access Entity'Class;
                                   x, y   : Float;
                                   vx, vy : Float := 0.0 );

    -- Notifies the widget of its selected state. This doesn't actually
    -- select/unselect the widget; that can only be done by its Window.
    procedure Set_Selected( this : not null access Entity'Class; selected : Boolean );

    -- Sets the widget 'w' that visually represents the entity. This may be
    -- called only once, just after creation and before use.
    procedure Set_Widget( this : not null access Entity'Class; w : access Widget'Class );

    -- Sets the 'z' / "draw depth" of the entity relative the middleground layer.
    -- Lower numbers are in the foreground.
    procedure Set_Z( this : not null access Entity'Class; z : Float );

    -- Sets the "z-order", or drawing order relative to other entities with the
    -- same Z depth. Lower numbers are in the foreground.
    procedure Set_ZOrder( this : not null access Entity'Class; zOrder : Integer );

    -- Recalculates the rendered location (widget layout) of the widget by
    -- extrapolating the entity's location between game logic frames. If the
    -- widget is not currently updatable, then nothing will change.
    procedure Update_Render_Location( this : not null access Entity'Class );

    -- Returns True if entity 'l' should be drawn before entity 'r'. Entities
    -- are drawn background first within the scene.
    function Entity_Z_Order( this : A_Entity; other : A_Entity ) return Boolean
    is (this.Get_Z > other.Get_Z or
           (this.Get_Z = other.Get_Z and (this.Get_ZOrder > other.Get_ZOrder or
               (this.Get_ZOrder = other.Get_ZOrder and this.Get_Entity_Id < other.Get_Entity_Id))));

    procedure Delete( this : in out A_Entity );
    pragma Postcondition( this = null );

private

    type Entity is new Limited_Object with
        record
            widget        : A_Widget;           -- widget that renders the entity

            sigAttributeChanged : aliased String_Signal;
            sigMoved            : aliased Signal;
            sigResized          : aliased Signal;
            sigSelected         : aliased Signal;
            sigUnselected       : aliased Signal;

            eid           : Entity_Id := NULL_ID;
            name          : Unbounded_String;
            template      : Unbounded_String;   -- template that created entity
            attrs         : Map_Value;          -- entity attributes

            x, y          : Float := 0.0;       -- entity location
            prevX, prevY  : Float := 0.0;       -- previous entity location
            renderX,                            -- entity's render location,
            renderY       : Float := 0.0;       -- extrapolated from previous frames
            z             : Float := 0.0;       -- entity's visual z depth
            zOrder        : Integer := 0;       -- relative order within the same z depth
            width,
            height        : Float := 0.0;       -- entity's size

            frozen        : Integer := 0;       -- no event updates when > 0
            isSelected    : Boolean := False;
            deletable     : Boolean := True;    -- user can delete entity in editor?
        end record;

    -- 'eid' and 'name' uniquely identify the entity within the world. 'template'
    -- is the name of the template that created the entity.
    --
    -- 'initialState' describes the entity's public state at creation. The
    -- "Location" and "Visible" fields contain position and visual info.
    procedure Construct( this         : access Entity;
                         eid          : Entity_Id;
                         name         : String;
                         template     : String;
                         initialState : Map_Value );

end Scene_Entities;
