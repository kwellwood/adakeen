--
-- Copyright (c) 2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Widgets.Split_Panes is

    type Fill_Type is (Left_Side, Right_Side);

    type Orientation_Type is (Horizontal, Vertical);

    -- A Split_Pane is a two widget container split by an adjustable divider.
    -- The divider can be dragged by the mouse.
    type Split_Pane is abstract new Widget with private;
    type A_Split_Pane is access all Split_Pane'Class;

    -- Split Pane Signals
    function Dragged( this : not null access Split_Pane'Class ) return access Signal'Class;

    -- Creates a new Panel within 'view', with the divider oriented as
    -- 'orientation'. If the orientation is vertical, the divider will be
    -- vertical and the widgets will be on the left and right. Otherwise, for
    -- a horizontal orientation, the widgets will be on the top and bottom.
    -- 'fillSide' determines which child will fill the Split_Pane's area when it
    -- is resized Right_Side => right or bottom, Left_Side => left or top. The
    -- other side will remain a fixed size when resized.
    function Create_Split_Pane( view        : not null access Game_Views.Game_View'Class;
                                orientation : Orientation_Type;
                                fillSide    : Fill_Type ) return A_Split_Pane;
    pragma Postcondition( Create_Split_Pane'Result /= null );

    -- Enables and disables resizing of the fixed-size pane. Default behavior is
    -- enabled.
    procedure Enable_Resize( this : not null access Split_Pane'Class; enabled : Boolean );

    -- Returns the maximum possible size of the left/top widget, based on the
    -- Split_Pane's current size, the size of the divider area, and the minimum
    -- size constraint of the right/bottom widget.
    function Get_Left_Max_Size( this : access Split_Pane ) return Float is abstract;

    -- Returns the minimum size constraint of the left/top widget. This is the
    -- size provided to Set_Left(), not the child widget's own minimum size hint.
    function Get_Left_Min_Size( this : not null access Split_Pane'Class ) return Float;

    -- Returns the maximum possible size of the right/bottom widget, based on
    -- the Split_Pane's current size, the size of the divider area, and the
    -- minimum size constraint of the left/top widget.
    function Get_Right_Max_Size( this : access Split_Pane ) return Float is abstract;

    -- Returns the minimum size constraint of the right/bottom widget. This is
    -- the size constraint provided to Set_Right(), not the child widget's own
    -- minimum size hint.
    function Get_Right_Min_Size( this : not null access Split_Pane'Class ) return Float;

    -- Returns True if the fixed-size pane is resizable. The behavior can be
    -- toggled with Enable_Resize().
    function Is_Resizable( this : not null access Split_Pane'Class ) return Boolean;

    -- Sets the left (or top) child of the Split_Pane. The Panel will own 'child'
    -- after this call and will delete it upon replacement or destruction.
    procedure Set_Left( this    : not null access Split_Pane'Class;
                        child   : not null access Widget'Class;
                        minSize : Float := 0.0 );

    -- Sets the right (or bottom) child of the Split_Pane. The Panel will own
    -- 'child' after this call and will delete it upon replacement or
    -- destruction.
    procedure Set_Right( this    : not null access Split_Pane'Class;
                         child   : not null access Widget'Class;
                         minSize : Float := 0.0 );

private

    BAR_ELEM    : constant := 1;
    HANDLE_ELEM : constant := 2;

    DISABLED_STATE : constant Widget_State := 2**0;
    HOVER_STATE    : constant Widget_State := 2**1;

    ----------------------------------------------------------------------------

    type Split_Pane is abstract new Widget with
        record
            orientation  : Orientation_Type := Vertical;
            dragCursor   : Unbounded_String;
            fillSide     : Fill_Type := Right_Side;
            leftChild    : A_Widget;            -- left or top widget
            rightChild   : A_Widget;            -- right or bottom widget
            fillChild    : A_Widget;            -- the widget that expands automatically
            fixedChild   : A_Widget;            -- the widget that does not auto-expand
            minFillSize  : Float := 0.0;        -- min size of .fillChild
            minFixedSize : Float := 0.0;        -- min size of .fixedChild
            resizable    : Boolean := True;     -- dragging is allowed?
            dragging     : Boolean := False;    -- true when dragging with left mouse
            dragX, dragY : Float := 0.0;        -- widget content point under the mouse

            sigDragged   : aliased Signal;      -- the handle was dragged
        end record;

    procedure Adjust_Layout( this : access Split_Pane ) is null;

    procedure Construct( this       : access Split_Pane;
                         view       : not null access Game_Views.Game_View'Class;
                         widgetType : String;
                         fillSide   : Fill_Type );

    function Get_Visual_State( this : access Split_Pane ) return Widget_State;

    ----------------------------------------------------------------------------

    type Horizontal_Split_Pane is new Split_Pane with null record;
    type A_Horizontal_Split_Pane is access all Horizontal_Split_Pane'Class;

    procedure Adjust_Layout( this : access Horizontal_Split_Pane );

    procedure Construct( this     : access Horizontal_Split_Pane;
                         view     : not null access Game_Views.Game_View'Class;
                         fillSide : Fill_Type );

    -- Draws the Split_Pane's divider.
    procedure Draw_Content( this : access Horizontal_Split_Pane );

    procedure Find_Widget_At( this     : access Horizontal_Split_Pane;
                              x, y     : Float;
                              children : Boolean;
                              wx, wy   : out Float;
                              found    : out A_Widget );

    function Get_Left_Max_Size( this : access Horizontal_Split_Pane ) return Float;

    function Get_Right_Max_Size( this : access Horizontal_Split_Pane ) return Float;

    function Get_Min_Height( this : access Horizontal_Split_Pane ) return Float;

    function Get_Min_Width( this : access Horizontal_Split_Pane ) return Float;

    function Get_Preferred_Height( this : access Horizontal_Split_Pane ) return Float;

    function Get_Preferred_Width( this : access Horizontal_Split_Pane ) return Float;

    ----------------------------------------------------------------------------

    type Vertical_Split_Pane is new Split_Pane with null record;
    type A_Vertical_Split_Pane is access all Vertical_Split_Pane'Class;

    procedure Adjust_Layout( this : access Vertical_Split_Pane );

    procedure Construct( this     : access Vertical_Split_Pane;
                         view     : not null access Game_Views.Game_View'Class;
                         fillSide : Fill_Type );

    -- Draws the Split_Pane's divider.
    procedure Draw_Content( this : access Vertical_Split_Pane );

    procedure Find_Widget_At( this     : access Vertical_Split_Pane;
                              x, y     : Float;
                              children : Boolean;
                              wx, wy   : out Float;
                              found    : out A_Widget );

    function Get_Left_Max_Size( this : access Vertical_Split_Pane ) return Float;

    function Get_Right_Max_Size( this : access Vertical_Split_Pane ) return Float;

    function Get_Min_Height( this : access Vertical_Split_Pane ) return Float;

    function Get_Min_Width( this : access Vertical_Split_Pane ) return Float;

    function Get_Preferred_Height( this : access Vertical_Split_Pane ) return Float;

    function Get_Preferred_Width( this : access Vertical_Split_Pane ) return Float;

end Widgets.Split_Panes;
