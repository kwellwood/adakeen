--
-- Copyright (c) 2012-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Widgets.Scrollbars is

    -- An abstract scrollbar widget. A scrollbar has a client widget that it is
    -- responsible for scrolling. The amount that can be scrolled is the
    -- difference between the client's content region and its viewport.
    type Scrollbar is abstract new Widget with private;

    -- Calculates where the scroll button will be drawn on the scrollbar, based
    -- on the size of the client widget and the location and size of its
    -- viewport.
    function Calculate_Button( this : access Scrollbar ) return Rectangle is abstract;

    -- Sets the client widget of the scrollbar; ie: the widget that will be
    -- controlled. 'client' will not belong to the scrollbar. The client of a
    -- scrollbar should be removed before its deleted. If 'client' is null, the
    -- scrollbar will have no client.
    procedure Set_Client( this : not null access Scrollbar'Class; client : A_Widget );

    ----------------------------------------------------------------------------

    -- A horizontal scrollbar widget.
    type H_Scrollbar is new Scrollbar with private;
    type A_H_Scrollbar is access all H_Scrollbar'Class;

    -- Creates a horizontal scrollbar within 'view' with widget id 'id'.
    function Create_H_Scrollbar( view : not null access Game_Views.Game_View'Class;
                                 id   : String := "" ) return A_H_Scrollbar;
    pragma Postcondition( Create_H_Scrollbar'Result /= null );

    function Get_Preferred_Height( this : access H_Scrollbar ) return Float;

    ----------------------------------------------------------------------------

    -- A vertical scrollbar widget.
    type V_Scrollbar is new Scrollbar with private;
    type A_V_Scrollbar is access all V_Scrollbar'Class;

    -- Creates a vertical scrollbar within 'view' with widget id 'id'.
    function Create_V_Scrollbar( view : not null access Game_Views.Game_View'Class;
                                 id   : String := "" ) return A_V_Scrollbar;
    pragma Postcondition( Create_V_Scrollbar'Result /= null );

    function Get_Preferred_Width( this : access V_Scrollbar ) return Float;

private

    BACKGROUND_ELEM : constant := 1;
    BUTTON_ELEM     : constant := 2;
    BORDER_ELEM     : constant := 3;

    DISABLED_STATE : constant Widget_State := 2**0;
    PRESS_STATE    : constant Widget_State := 2**1;
    HOVER_STATE    : constant Widget_State := 2**2;

    ----------------------------------------------------------------------------

    type Scrollbar is abstract new Widget with
        record
            client   : A_Widget := null;    -- widget to scroll
            pageUp,                         -- true when paging up
            pageDown,                       -- true when paging down
            dragging : Boolean := False;    -- true when button is dragged
            dragPos  : Float := 0.0;
        end record;
    type A_Scrollbar is access all Scrollbar'Class;

    procedure Construct( this : access Scrollbar;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );

    procedure Delete( this : in out Scrollbar );

    procedure Draw_Content( this : access Scrollbar );

    function Get_Min_Height( this : access Scrollbar ) return Float;

    function Get_Min_Width( this : access Scrollbar ) return Float;

    function Get_Visual_State( this : access Scrollbar ) return Widget_State;

    ----------------------------------------------------------------------------

    type H_Scrollbar is new Scrollbar with null record;

    procedure Construct( this : access H_Scrollbar;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );

    function Calculate_Button( this : access H_Scrollbar ) return Rectangle;

    ----------------------------------------------------------------------------

    type V_Scrollbar is new Scrollbar with null record;

    procedure Construct( this : access V_Scrollbar;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );

    function Calculate_Button( this : access V_Scrollbar ) return Rectangle;

end Widgets.Scrollbars;
