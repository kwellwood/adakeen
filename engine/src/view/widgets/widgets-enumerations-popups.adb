--
-- Copyright (c) 2016-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Assets.Fonts;                      use Assets.Fonts;
--with Debugging;                         use Debugging;
with Drawing;                           use Drawing;
with Drawing.Primitives;                use Drawing.Primitives;
with Drawing.Tiles;                     use Drawing.Tiles;
with Game_Views;                        use Game_Views;
with Widget_Styles.Registry;            use Widget_Styles.Registry;

package body Widgets.Enumerations.Popups is

    pragma Suppress( Elaboration_Check, Signals.Connections );
    pragma Suppress( Elaboration_Check, Signals.Mouse.Connections );

    package Item_Mouse_Connections is new Signals.Mouse.Connections(Enum_Item);
    use Item_Mouse_Connections;

    ----------------------------------------------------------------------------

    -- Clicks the enum item if the right mouse button is released on the widget.
    procedure On_Mouse_Released( this  : not null access Enum_Item'Class;
                                 mouse : Button_Arguments );

    ----------------------------------------------------------------------------

    package Popup_Connections is new Signals.Connections(Enum_Popup);
    use Popup_Connections;

    procedure Clicked_Item( this : not null access Enum_Popup'Class );

    procedure On_Resized( this : not null access Enum_Popup'Class );

    procedure On_Style_Changed( this : not null access Enum_Popup'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Enum_Item( view : not null access Game_Views.Game_View'Class;
                               id   : String;
                               text : String ) return A_Enum_Item is
        this : A_Enum_Item := new Enum_Item;
    begin
        this.Construct( view, id, text );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Enum_Item;

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access Enum_Item;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String;
                         text : String ) is
    begin
        Widget(this.all).Construct( view, id, "EnumItem" );
        this.sigClicked.Init( this );

        this.MouseReleased.Connect( Slot( this, On_Mouse_Released'Access, Mouse_Left ) );

        this.Set_Focusable( False );
        this.text := To_Unbounded_String( text );
    end Construct;

    ----------------------------------------------------------------------------

    function Clicked( this : not null access Enum_Item'Class ) return access Signal'Class is (this.sigClicked'Access);

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Content( this : access Enum_Item ) is
        state        : constant Widget_State := this.Get_Visual_State;
        font         : constant A_Font := this.style.Get_Font( TEXT_ELEM, state );
        padTop       : constant Float := this.style.Get_Pad_Top( BACKGROUND_ELEM, state );
        padBottom    : constant Float := this.style.Get_Pad_Bottom( BACKGROUND_ELEM, state );
        textX, textY : Float;
        color        : Allegro_Color;
    begin
        -- - - - Calculate Positions - - - --

        textX := this.style.Get_Pad_Left( BACKGROUND_ELEM );
        textY := padTop + (this.viewport.height - (padTop + padBottom) - Float(font.Line_Height)) / 2.0;

        -- - - - Draw Content - - - --

        -- draw the background --
        color := this.style.Get_Color( BACKGROUND_ELEM, state );
        if color /= Transparent then
            Rectfill( 0.0, 0.0, this.viewport.width, this.viewport.height,
                      Lighten( color, this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );
        else
            Draw_Tile_Filled( this.style.Get_Image( BACKGROUND_ELEM, state ).Get_Tile,
                              0.0, 0.0, 0.0,
                              this.viewport.width, this.viewport.height,
                              Lighten( this.style.Get_Tint( BACKGROUND_ELEM, state ),
                                       this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );
        end if;

        -- draw the border --
        Draw_Tile_Filled( this.style.Get_Image( BORDER_ELEM, state ).Get_Tile,
                          0.0, 0.0, 0.0,
                          this.viewport.width, this.viewport.height,
                          Lighten( this.style.Get_Tint( BORDER_ELEM, state ),
                                   this.style.Get_Shade( BORDER_ELEM, state ) ) );

        -- draw the text --
        font.Draw_String( To_String( this.text ), textX, textY,
                          Lighten( this.style.Get_Color( TEXT_ELEM, state ),
                                   this.style.Get_Shade( TEXT_ELEM, state ) ) );
    end Draw_Content;

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Height( this : access Enum_Item ) return Float is
    begin
        return this.style.Get_Pad_Top( BACKGROUND_ELEM ) +
               Float(this.style.Get_Font( TEXT_ELEM ).Line_Height) +
               this.style.Get_Pad_Bottom( BACKGROUND_ELEM );
    end Get_Min_Height;

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Width( this : access Enum_Item ) return Float is
    (
        this.style.Get_Pad_Left( BACKGROUND_ELEM ) +
        Float(this.style.Get_Font( TEXT_ELEM ).Text_Length( To_String( this.text ) )) +
        this.style.Get_Pad_Right( BACKGROUND_ELEM )
    );

    ----------------------------------------------------------------------------

    function Get_Text( this : not null access Enum_Item'Class ) return String is (To_String( this.text ));

    ----------------------------------------------------------------------------

    overriding
    function Get_Visual_State( this : access Enum_Item ) return Widget_State is
    (
           (if not this.Is_Enabled           then DISABLED_STATE else 0)
        or (if this.selected                 then SELECTED_STATE else 0)
        or (if this.mouseButtons(Mouse_Left) then PRESS_STATE    else 0)
        or (if this.hover                    then HOVER_STATE    else 0)
    );

    ----------------------------------------------------------------------------

    procedure On_Mouse_Released( this  : not null access Enum_Item'Class;
                                 mouse : Button_Arguments ) is
    begin
        if mouse.x >= 0.0 and then mouse.y >= 0.0 and then
           mouse.x < this.Width.Get and then mouse.y < this.Height.Get
        then
            this.Clicked.Emit;
        end if;
    end On_Mouse_Released;

    ----------------------------------------------------------------------------

    procedure Set_Selected( this : not null access Enum_Item'Class; selected : Boolean ) is
    begin
        this.selected := selected;
    end Set_Selected;

    ----------------------------------------------------------------------------

    overriding
    function To_String( this : access Enum_Item ) return String
    is ("<" & this.Get_Class_Name & " '" & To_String( this.text ) & "'>");

    --==========================================================================

    function Create_Popup( view : not null access Game_Views.Game_View'Class;
                           id   : String := "" ) return A_Enum_Popup is
        this : A_Enum_Popup := new Enum_Popup;
    begin
        this.Construct( view, id );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Popup;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Enum_Popup;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String ) is
    begin
        Widget(this.all).Construct( view, id, "EnumPopup" );
        this.sigClicked.Init( this );

        this.StyleChanged.Connect( Slot( this, On_Style_Changed'Access ) );
        this.Resized.Connect( Slot( this, On_Resized'Access ) );

        this.Visible.Set( False );
    end Construct;

    ----------------------------------------------------------------------------

    procedure Add_Item( this : not null access Enum_Popup'Class; item : not null A_Enum_Item ) is
    begin
        -- high priority because the menu must be closed before servicing
        -- whatever else is connected to the menu item.
        item.Clicked.Connect( Slot( this, Clicked_Item'Access, priority => High ) );
        this.Add_Child( item );

        item.Fill_Horizontal( item.Get_Parent );
        item.Y.Set( Float(this.children.Length) * item.Get_Preferred_Height );
        item.Height.Set( item.Get_Preferred_Height );

        -- recalculate the width of the popup. the new item may have increased it.
        this.Width.Set( this.Get_Preferred_Width );
    end Add_Item;

    ----------------------------------------------------------------------------

    function Clicked( this : not null access Enum_Popup'Class ) return access Signal'Class is (this.sigClicked'Access);

    ----------------------------------------------------------------------------

    procedure Clicked_Item( this : not null access Enum_Popup'Class ) is
    begin
        -- Emitting "Clicked" here will trigger the Enumeration to close the
        -- popup menu and emit its own signals that the selection has changed.
        this.Clicked.Emit;
    end Clicked_Item;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Content( this : access Enum_Popup ) is
        state : constant Widget_State := this.Get_Visual_State;
        color : Allegro_Color;
    begin
        -- draw the background --
        color := this.style.Get_Color( BACKGROUND_ELEM, state );
        if color /= Transparent then
            Rectfill( 0.0, 0.0, this.viewport.width, this.viewport.height,
                      Lighten( color, this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );
        else
            Draw_Tile_Filled( this.style.Get_Image( BACKGROUND_ELEM, state ).Get_Tile,
                              0.0, 0.0, 0.0,
                              this.viewport.width, this.viewport.height,
                              Lighten( this.style.Get_Tint( BACKGROUND_ELEM, state ),
                                       this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );
        end if;

        -- draw the border --
        Draw_Tile_Filled( this.style.Get_Image( BORDER_ELEM, state ).Get_Tile,
                          0.0, 0.0, 0.0,
                          this.viewport.width, this.viewport.height,
                          Lighten( this.style.Get_Tint( BORDER_ELEM, state ),
                                   this.style.Get_Shade( BORDER_ELEM, state ) ) );
    end Draw_Content;

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Height( this : access Enum_Popup ) return Float is
        height : Float := 0.0;
    begin
        for child of this.children loop
            height := height + child.Get_Min_Height;
        end loop;
        return this.style.Get_Pad_Top( BACKGROUND_ELEM ) + height + this.style.Get_Pad_Bottom( BACKGROUND_ELEM );
    end Get_Min_Height;

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Width( this : access Enum_Popup ) return Float is
        width : Float := 0.0;
    begin
        for child of this.children loop
            width := Float'Max( A_Enum_Item(child).Get_Min_Width, width );
        end loop;
        return this.style.Get_Pad_Left( BACKGROUND_ELEM ) + width + this.style.Get_Pad_Right( BACKGROUND_ELEM );
    end Get_Min_Width;

    ----------------------------------------------------------------------------

    overriding
    function Get_Preferred_Height( this : access Enum_Popup ) return Float is
        height : Float := 0.0;
    begin
        if not this.prefHeight.Is_Null then
            return this.prefHeight.To_Float;
        end if;
        for child of this.children loop
            height := height + child.Get_Preferred_Height;
        end loop;
        return this.style.Get_Pad_Top( BACKGROUND_ELEM ) + height + this.style.Get_Pad_Bottom( BACKGROUND_ELEM );
    end Get_Preferred_Height;

    ----------------------------------------------------------------------------

    overriding
    function Get_Preferred_Width( this : access Enum_Popup ) return Float is
        width : Float := 0.0;
    begin
        if not this.prefWidth.Is_Null then
            return this.prefWidth.To_Float;
        end if;
        for child of this.children loop
            width := Float'Max( A_Enum_Item(child).Get_Preferred_Width, width );
        end loop;
        return this.style.Get_Pad_Left( BACKGROUND_ELEM ) + width + this.style.Get_Pad_Right( BACKGROUND_ELEM );
    end Get_Preferred_Width;

    ----------------------------------------------------------------------------

    procedure On_Style_Changed( this : not null access Enum_Popup'Class ) is
    begin
        -- because the padding may have changed
        this.ancLeft.innerPad := this.style.Get_Pad_Left( BACKGROUND_ELEM );
        this.ancTop.innerPad := this.style.Get_Pad_Top( BACKGROUND_ELEM );
        this.ancRight.innerPad := -this.style.Get_Pad_Right( BACKGROUND_ELEM );
        this.ancBottom.innerPad := -this.style.Get_Pad_Bottom( BACKGROUND_ELEM );

        this.Update_Geometry;
    end On_Style_Changed;

    ----------------------------------------------------------------------------

    procedure On_Resized( this : not null access Enum_Popup'Class ) is
        top : Float := 0.0;
    begin
        for child of this.children loop
            child.Y.Set( top );
            top := top + child.Get_Preferred_Height;
        end loop;
    end On_Resized;

    ----------------------------------------------------------------------------

    procedure Set_Selected( this : not null access Enum_Popup'Class; index : Natural ) is
        i : Integer := 1;
    begin
        for child of this.children loop
            A_Enum_Item(child).Set_Selected( index = i );
            i := i + 1;
        end loop;
    end Set_Selected;

begin

    Register_Style( "EnumPopup",
                    (BACKGROUND_ELEM => To_Unbounded_String( "background" ),
                     BORDER_ELEM     => To_Unbounded_String( "border" )),
                    (BACKGROUND_ELEM => Area_Element,
                     BORDER_ELEM     => Area_Element),
                    (0 => To_Unbounded_String( "disabled" )) );

    Register_Style( "EnumItem",
                    (BACKGROUND_ELEM => To_Unbounded_String( "background" ),
                     BORDER_ELEM     => To_Unbounded_String( "border" ),
                     TEXT_ELEM       => To_Unbounded_String( "text" )),
                    (BACKGROUND_ELEM => Area_Element,
                     BORDER_ELEM     => Area_Element,
                     TEXT_ELEM       => Text_Element),
                    (0 => To_Unbounded_String( "disabled" ),
                     1 => To_Unbounded_String( "selected" ),
                     2 => To_Unbounded_String( "press" ),
                     3 => To_Unbounded_String( "hover" )) );

end Widgets.Enumerations.Popups;

