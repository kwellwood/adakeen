--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Vectors;

package Widgets.Input_Boxes is

    -- An access to a string constraint function. A constraint function returns
    -- 'newstr' if it meets the constraint, otherwise 'curstr' will be returned.
    type A_Constrain_Func is
        access function( curstr, newstr : String ) return String;

    ----------------------------------------------------------------------------

    -- An Input_Box widget allows a single line of text to be edited with the
    -- keyboard. The text of the input box can be larger than the width of the
    -- width; the portion of the text near the cursor will be displayed.
    type Input_Box is new Widget with private;
    type A_Input_Box is access all Input_Box'Class;

    -- Creates a new Input_Box within 'view' with id 'id'.
    function Create_Input_Box( view : not null access Game_Views.Game_View'Class;
                               id   : String := "" ) return A_Input_Box;
    pragma Postcondition( Create_Input_Box'Result /= null );

    -- Input Box Signals
    function Accepted( this : not null access Input_Box'Class ) return access Signal'Class;

    -- Clears the current selection, if any.
    procedure Clear_Selection( this : not null access Input_Box'Class );

    -- Enables/disables the command history. Previous commands can be retyped by
    -- pressing the up/down arrows to browse through previous history. New lines
    -- are added to the input history when an 'Accepted' action is fired.
    procedure Enable_History( this : not null access Input_Box'Class; enabled : Boolean );

    -- Returns the input box's text.
    function Get_Text( this : not null access Input_Box'Class ) return String;

    -- Returns True when the behavior to select all text when focused is enabled.
    function Is_Enabled_Select_All_On_Focus( this : not null access Input_Box'Class ) return Boolean;

    -- Selects all the text, putting the cursor at the end.
    procedure Select_All( this : not null access Input_Box'Class );

    -- When enabled, the input box will automatically select all text when focused.
    procedure Enable_Select_All_On_Focus( this    : not null access Input_Box'Class;
                                          enabled : Boolean );

    -- Sets a constraint function to validate the input box's text. If
    -- 'constraint' is null, any text can be added. No text can be entered into
    -- the input box that is not validated by the constraint function.
    procedure Set_Constraint( this       : not null access Input_Box'Class;
                              constraint : A_Constrain_Func );

    -- Sets the maximum length of the input box's text in characters.
    procedure Set_Max_Length( this : not null access Input_Box'Class; max : Positive );

    -- Sets the input box's text value. The value will remain unchanged if
    -- 'text' is not accepted by the current constraint function. If 'acceptIt'
    -- is True, then the input box will accept the text as well, emitting the
    -- .Accepted action. This is equivalent to the user pressing the Enter key
    -- after typing 'text'.
    procedure Set_Text( this     : not null access Input_Box'Class;
                        text     : String;
                        acceptIt : Boolean := False );

private

    BACKGROUND_ELEM : constant := 1;
    BORDER_ELEM     : constant := 2;
    TEXT_ELEM       : constant := 3;
    SELAREA_ELEM    : constant := 4;
    SELTEXT_ELEM    : constant := 5;

    DISABLED_STATE : constant Widget_State := 2**0;
    FOCUS_STATE    : constant Widget_State := 2**1;
    HOVER_STATE    : constant Widget_State := 2**2;

    -- direction to move the cursor on a key press
    type Move_Dir is (Go_First, Go_Left, Go_Right, Go_Last);

    package String_Vectors is new Ada.Containers.Vectors( Positive, Unbounded_String, "=" );

    type Input_State is
        record
            beforeText : Unbounded_String;
            cursorPos  : Integer;
            afterText  : Unbounded_String;
            timestamp  : Time;
        end record;

    package State_Vectors is new Ada.Containers.Vectors( Positive, Input_State, "=" );

    ----------------------------------------------------------------------------

    type Input_Box is new Widget with
        record
            text        : Unbounded_String;
            cursorPos   : Integer := 0;  -- cursor position between chars (1 = between 1 and 2)
                                         --   valid range is 0..length
            selectPos   : Integer := 0;  -- selection start between chars (1 = between 1 and 2)
                                         --   valid range is 0..length; selected
                                         --   range is (selectPos+1)..cursorPos
                                         --   or (cursorPos+1)..selectPos; no
                                         --   selection if selectPos = cursorPos
            viewFirst   : Natural := 1;  -- index of the first character to draw
            viewLast    : Natural := 0;  -- index of last character to draw
            constrain   : A_Constrain_Func := null;
            maxLen      : Positive := 1024;
            blinkTime   : Time;
            scrollTime  : Time;
            histEnabled : Boolean := False;
            history     : String_Vectors.Vector;
            histIndex   : Integer := 0;
            undoHistory : State_Vectors.Vector;
            undoIndex   : Integer := 0;

            selectAllEnabled : Boolean := False;   -- select all text when focused
            selectOnClick    : Boolean := False;   -- select all on next click

            sigAccepted : aliased Signal;
        end record;

    procedure Construct( this : access Input_Box;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );

    -- Inserts 'char' into the input box's value to the right of the cursor
    -- position. If the updated text exceeds the max length, or it does not
    -- validate with the input box's constraint function, the character will not
    -- be added.
    procedure Add_Character( this : not null access Input_Box'Class;
                             char : Character );

    -- Calculates the range of text that is visible in the viewport, based on
    -- the cursor location.
    procedure Calculate_Visible_Text( this : not null access Input_Box'Class );

    -- Deletes the character to the left of the cursor, if there is one, and if
    -- the resulting text doesn't violate the constraint function.
    procedure Do_Backspace( this : not null access Input_Box'Class );

    -- Copies the current selection to the clipboard, if a selection exists.
    procedure Do_Copy( this : not null access Input_Box'Class );

    -- Copies the current selection to the clipboard and then deletes it, if a
    -- selection exists.
    procedure Do_Cut( this : not null access Input_Box'Class );

    -- Deletes the character to the right of the cursor, if there is one, and if
    -- the resulting text doesn't violate the constraint function.
    procedure Do_Delete( this : not null access Input_Box'Class );

    -- Enters the current text, firing the 'Accepted' signal if the text is
    -- accepted by the constraint function. This is called when the Enter key is
    -- pressed.
    procedure Do_Enter( this : not null access Input_Box'Class );

    -- Moves backward in the command history, overwriting the text box's current
    -- content.
    procedure Do_History_Back( this : not null access Input_Box'Class );

    -- Moves forward in the command history, overwriting the text box's current
    -- content. If the end of the command history is passed, the text box will
    -- be cleared.
    procedure Do_History_Forward( this : not null access Input_Box'Class );

    -- Inserts the contents of the clipboard at the cursor's location. If a
    -- selection exists, it will be replaced.
    procedure Do_Paste( this : not null access Input_Box'Class );

    -- Reapplies a previously undone change.
    procedure Do_Redo( this : not null access Input_Box'Class );

    -- Undoes the previous change, restoring a value from the history.
    procedure Do_Undo( this : not null access Input_Box'Class );

    procedure Draw_Content( this : access Input_Box );

    -- Returns the position (between characters, e.g. for the cursor) of a
    -- pixel location 'mouseX', given in content coordinates. This is used to
    -- position the cursor on mouse events.
    function Find_Pos( this : not null access Input_Box'Class; mouseX : Float ) return Natural;
    pragma Postcondition( Find_Pos'Result <= Length( this.text ) );

    -- Returns the visual state of the input box, based on its enabled, focused,
    -- and hovered states.
    function Get_Visual_State( this : access Input_Box ) return Widget_State;

    -- Moves the cursor in the direction 'dir', if possible. If 'doSelect' is
    -- True, then a selection will be created (if one does not exist) from the
    -- cursor's current position to the cursor's new position, if it moves. If
    -- 'doSelect' is False, any existing selection will be cleared. The visible
    -- portion of the text will be recalculated, to keep the cursor in view.
    procedure Move_Cursor( this     : not null access Input_Box'Class;
                           dir      : Move_Dir;
                           doSelect : Boolean );

    -- Replaces the selected text with 'str'. If the string is empty, the
    -- selection will be deleted. The contraint function will be applied to
    -- validate the replacement.
    procedure Replace_Selection( this : not null access Input_Box'class;
                                 str  : String );
    pragma Precondition( this.selectPos /= this.cursorPos );

end Widgets.Input_Boxes;
