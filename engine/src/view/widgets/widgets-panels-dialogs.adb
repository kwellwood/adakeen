--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Assets.Fonts;                      use Assets.Fonts;
--with Debugging;                         use Debugging;
with Drawing.Bitmaps;                   use Drawing.Bitmaps;
with Drawing.Primitives;                use Drawing.Primitives;
with Drawing.Tiles;                     use Drawing.Tiles;
with Game_Views;                        use Game_Views;
with Keyboard;                          use Keyboard;
with Styles;                            use Styles;
with Support;                           use Support;
with Values.Strings;                    use Values.Strings;
with Widget_Styles.Registry;            use Widget_Styles.Registry;
with Widgets.Windows;                   use Widgets.Windows;

package body Widgets.Panels.Dialogs is

    package Connections is new Signals.Connections(Dialog);
    use Connections;

    package Key_Connections is new Signals.Keys.Connections(Dialog);
    use Key_Connections;

    package Mouse_Connections is new Signals.Mouse.Connections(Dialog);
    use Mouse_Connections;

    ----------------------------------------------------------------------------

    function Get_Header_Height( this : not null access Dialog'Class ) return Float;

    -- Traps key actions, preventing them from being propagated to the dialog's
    -- window. This is to prevent the user from calling shortcuts on the window
    -- that are unrelated to this dialog.
    procedure Ignore_Keys( this : not null access Dialog'Class ) is null;

    -- Moves the dialog around its parent if its being dragged.
    procedure On_Mouse_Moved( this  : not null access Dialog'Class;
                              mouse : Mouse_Arguments );

    -- Detects if the dialog should be dragged when the left mouse is pressed.
    procedure On_Mouse_Pressed( this  : not null access Dialog'Class;
                                mouse : Button_Arguments );

    procedure On_Style_Changed( this : not null access Dialog'Class );

    procedure Recalculate_Child_Padding( this : not null access Dialog'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    not overriding
    procedure Construct( this  : access Dialog;
                         view  : not null access Game_Views.Game_View'Class;
                         id    : String;
                         title : String;
                         icon  : Icon_Type ) is
    begin
        Panel(this.all).Construct( view, id, "Dialog" );

        this.StyleChanged.Connect( Slot( this, On_Style_Changed'Access ) );

        -- capture all key events not explicitly handled by the dialog so they
        -- don't propagate further up.
        this.KeyPressed.Connect( Slot( this.all, Ignore_Keys'Access, KEY_ANY, priority => Low ) );
        this.KeyReleased.Connect( Slot( this.all, Ignore_Keys'Access, KEY_ANY, priority => Low ) );
        this.KeyTyped.Connect( Slot( this.all, Ignore_Keys'Access, KEY_ANY, priority => Low ) );

        this.MouseMoved.Connect( Slot( this, On_Mouse_Moved'Access ) );
        this.MousePressed.Connect( Slot( this, On_Mouse_Pressed'Access, Mouse_Left ) );

        this.Visible.Set( False );
        this.Set_Title( title );
        this.Set_Icon( (if icon /= NO_ICON then icon else this.style.Get_Image( ICON_ELEM )) );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Content( this : access Dialog ) is
        state         : constant Widget_State := this.Get_Visual_State;
        text          : constant String := To_String( this.title );
        icon          : constant Icon_Type := this.icon;
        align         : constant Align_Type := this.style.Get_Align( HEADER_ELEM, state );
        color         : Allegro_Color;
        font          : A_Font;
        iconWidth     : Float := 0.0;
        iconHeight    : Float := 0.0;
        textWidth     : Float := 0.0;
        textHeight    : Float := 0.0;
        contentWidth  : Float := 0.0;
        contentHeight : Float := 0.0;
        contentX      : Float;
        contentY      : Float;
        iconX, iconY  : Float;
        textX, textY  : Float;
    begin
        -- - - - Calculate Positions - - - --

        -- calculate icon size --
        iconWidth := this.style.Get_Width( ICON_ELEM, state );
        if iconWidth = 0.0 then
            iconWidth := Float(icon.Get_Width);
        end if;
        iconWidth := Float'Min( iconWidth, this.viewport.width );
        iconHeight := this.style.Get_Height( ICON_ELEM, state );
        if iconHeight = 0.0 then
            iconHeight := Float(icon.Get_Height);
        end if;
        iconHeight := Float'Min( iconHeight, this.viewport.height );

        -- calculate text size --
        if text'Length > 0 then
            font := this.style.Get_Font( TITLE_ELEM, state );
            textWidth := Float(font.Text_Length( text ));
            textHeight := Float(font.Line_Height);
        end if;

        -- calculate total content size --
        if iconWidth > 0.0 and textWidth > 0.0 then
            contentWidth := iconWidth + this.style.Get_Spacing( HEADER_ELEM, state ) + textWidth;
        elsif textWidth > 0.0 then
            contentWidth := textWidth;
        else
            contentWidth := iconWidth;
        end if;
        contentHeight := Float'Max( iconHeight, textHeight );

        -- calculate alignment --
        contentX := this.style.Get_Pad_Left( HEADER_ELEM, state ) +
                    Align_Horizontal( align,
                                      this.viewport.width - (this.style.Get_Pad_Left( HEADER_ELEM, state ) + this.style.Get_Pad_Right( HEADER_ELEM, state )),
                                      contentWidth );
        contentY := this.style.Get_Pad_Top( HEADER_ELEM, state );
        iconX := Float'Rounding( contentX );
        iconY := Float'Rounding( contentY + Align_Vertical( align, contentHeight, iconHeight ) );
        textX := contentX + (contentWidth - textWidth);
        textY := contentY + Align_Vertical( align, contentHeight, textHeight );

        -- - - - Draw Content - - - --

        -- draw the header
        color := this.style.Get_Color( HEADER_ELEM, state );
        if color /= Transparent then
            Rectfill( 0.0, 0.0, this.viewport.width, this.headerHeight,
                      Lighten( color, this.style.Get_Shade( HEADER_ELEM, state ) ) );
        else
            Draw_Tile_Filled( this.style.Get_Image( HEADER_ELEM, state ).Get_Tile,
                              0.0, 0.0, 0.0,
                              this.viewport.width, this.headerHeight,
                              Lighten( this.style.Get_Tint( HEADER_ELEM, state ),
                                       this.style.Get_Shade( HEADER_ELEM, state ) ) );
        end if;

        -- draw the header content --
        if icon.Get_Bitmap /= null then
            Draw_Bitmap_Stretched( icon.Get_Bitmap,
                                   iconX, iconY, 0.0,
                                   iconWidth, iconHeight,
                                   Fit,
                                   0.0,
                                   Lighten( this.style.Get_Tint( ICON_ELEM, state ),
                                            this.style.Get_Shade( ICON_ELEM, state ) ) );
        end if;
        if font /= null then
            font.Draw_String( text,
                              textX, textY,
                              Lighten( this.style.Get_Color( TITLE_ELEM, state ),
                                       this.style.Get_Shade( TITLE_ELEM, state ) ) );
        end if;

        -- draw the background --
        color := this.style.Get_Color( BACKGROUND_ELEM, state );
        if color /= Transparent then
            Rectfill( 0.0, this.headerHeight,
                      this.viewport.width, this.viewport.height,
                      Lighten( color, this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );
        else
            Draw_Tile_Filled( this.style.Get_Image( BACKGROUND_ELEM, state ).Get_Tile,
                              0.0, this.headerHeight, 0.0,
                              this.viewport.width, this.viewport.height - this.headerHeight,
                              Lighten( this.style.Get_Tint( BACKGROUND_ELEM, state ),
                                       this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );
        end if;

        -- draw the border --
        Draw_Tile_Filled( this.style.Get_Image( BORDER_ELEM, state ).Get_Tile,
                          0.0, 0.0, 0.0,
                          this.viewport.width, this.viewport.height,
                          Lighten( this.style.Get_Tint( BORDER_ELEM, state ),
                                   this.style.Get_Shade( BORDER_ELEM, state ) ) );
    end Draw_Content;

    ----------------------------------------------------------------------------

    function Get_Header_Height( this : not null access Dialog'Class ) return Float is
        iconHeight : Float;
    begin
        iconHeight := Float'Max( this.style.Get_Height( ICON_ELEM ),
                                 Float(this.style.Get_Image( ICON_ELEM ).Get_Height) );

        return this.style.Get_Pad_Top( HEADER_ELEM ) +
               Float'Max( iconHeight,
                          Float(this.style.Get_Font( TITLE_ELEM ).Line_Height) ) +
               this.style.Get_Pad_Bottom( HEADER_ELEM );
    end Get_Header_Height;

    ----------------------------------------------------------------------------

    function Get_Icon( this : not null access Dialog'Class ) return Icon_Type is (this.icon);

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Height( this : access Dialog ) return Float
    is (this.ancTop.innerPad - this.ancBottom.innerPad);

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Width( this : access Dialog ) return Float is
        iconWidth : Float;
    begin
        iconWidth := Float'Max( this.style.Get_Width( ICON_ELEM ),
                                Float(this.style.Get_Image( ICON_ELEM ).Get_Width) );
        if iconWidth > 0.0 then
            iconWidth := iconWidth + this.style.Get_Spacing( HEADER_ELEM );
        end if;

        -- take the wider of: 1) inner padding (left & right), or
        --                    2) width of header area (title & padding)
        return Float'Max( this.ancTop.innerPad - this.ancBottom.innerPad,
                          this.style.Get_Pad_Left( HEADER_ELEM ) +
                          iconWidth +
                          Float(this.style.Get_Font( TITLE_ELEM ).Text_Length( To_String( this.title ) )) +
                          this.style.Get_Pad_Right( HEADER_ELEM ) );
    end Get_Min_Width;

    ----------------------------------------------------------------------------

    function Get_Title( this : not null access Dialog'Class ) return String is (To_String( this.title ));

    ----------------------------------------------------------------------------

    procedure Hide( this : not null access Dialog'Class ) is
    begin
        if this.Is_Visible then
            this.Get_Window.Set_Modal( null );
        end if;
    end Hide;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Moved( this  : not null access Dialog'Class;
                              mouse : Mouse_Arguments ) is
    begin
        if this.mouseButtons(Mouse_Left) and then
           (this.dragStartX >= 0.0 and then this.dragStartY >= 0.0) and then
           (mouse.x /= this.dragStartX or else mouse.y /= this.dragStartY)
        then
            -- keep the dialog within its parent's viewport
            this.Move( Constrain( mouse.x - this.dragStartX,
                                  -this.geom.x,
                                  this.Get_Parent.viewport.width - (this.geom.x + this.geom.width) ),
                       Constrain( mouse.y - this.dragStartY,
                                  -this.geom.y,
                                  this.Get_Parent.viewport.height - (this.geom.y + this.geom.height) ) );
        end if;
    end On_Mouse_Moved;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Pressed( this  : not null access Dialog'Class;
                                mouse : Button_Arguments ) is
    begin
        if mouse.y < this.headerHeight and then Length( this.title ) > 0 then
            this.dragStartX := mouse.x;
            this.dragStartY := mouse.y;
        else
            this.dragStartX := -1.0;
            this.dragStartY := -1.0;
        end if;
    end On_Mouse_Pressed;

    ----------------------------------------------------------------------------

    procedure On_Style_Changed( this : not null access Dialog'Class ) is
    begin
        this.Set_Icon( this.icon );                -- keep the same icon
    end On_Style_Changed;

    ----------------------------------------------------------------------------

    procedure Recalculate_Child_Padding( this : not null access Dialog'Class ) is
    begin
        this.headerHeight := this.Get_Header_Height;

        this.ancLeft.innerPad := this.style.Get_Pad_Left( BACKGROUND_ELEM );
        this.ancTop.innerPad := this.headerHeight + this.style.Get_Pad_Top( BACKGROUND_ELEM );
        this.ancRight.innerPad := -this.style.Get_Pad_Right( BACKGROUND_ELEM );
        this.ancBottom.innerPad := -this.style.Get_Pad_Bottom( BACKGROUND_ELEM );

        this.Update_Geometry;
    end Recalculate_Child_Padding;

    ----------------------------------------------------------------------------

    procedure Set_Icon( this : not null access Dialog'Class; icon : Icon_Type ) is
        tileId : constant Natural := icon.Get_Tile.Get_Id;
    begin
        this.icon := icon;
        this.style.Set_Property( ICON_ELEM, STYLE_IMAGE, DEFAULT_STATE,
                                 Create( (if tileId > 0
                                          then icon.Get_Library.Get.Get_Name & ":" & Image( tileId )
                                          else "") ) );
        this.Recalculate_Child_Padding;
    end Set_Icon;

    ----------------------------------------------------------------------------

    procedure Set_Title( this : not null access Dialog'Class; title : String ) is
    begin
        this.title := To_Unbounded_String( title );
        this.Recalculate_Child_Padding;
    end Set_Title;

    ----------------------------------------------------------------------------

    procedure Show( this : not null access Dialog'Class ) is
    begin
        this.Get_Window.Set_Modal( this );
        this.X.Set( Float'Floor( (this.Get_Parent.Get_Viewport.width - this.Width.Get) / 2.0 ) );
        this.Y.Set( Float'Floor( (this.Get_Parent.Get_Viewport.height - this.Height.Get) / 2.0 ) );
    end Show;

begin

    Register_Style( "Dialog",
                    (BACKGROUND_ELEM => To_Unbounded_String( "background" ),
                     HEADER_ELEM     => To_Unbounded_String( "header" ),
                     BORDER_ELEM     => To_Unbounded_String( "border" ),
                     ICON_ELEM       => To_Unbounded_String( "icon" ),
                     TITLE_ELEM      => To_Unbounded_String( "title" )),
                    (BACKGROUND_ELEM => Area_Element,
                     HEADER_ELEM     => Area_Element,
                     BORDER_ELEM     => Area_Element,
                     ICON_ELEM       => Icon_Element,
                     TITLE_ELEM      => Text_Element),
                    (0 => To_Unbounded_String( "disabled" )) );

end Widgets.Panels.Dialogs;
