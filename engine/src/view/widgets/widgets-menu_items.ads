--
-- Copyright (c) 2012-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Native_Dialogs;            use Allegro.Native_Dialogs;
with Icons;                             use Icons;
with Interfaces;                        use Interfaces;

package Widgets.Menu_Items is

    -- A Menu_Item is a selection in a pulldown menu. Menu items can only be
    -- placed within a pulldown menu and they fire a Menu_Action when clicked.
    type Menu_Item is new Widget with private;
    type A_Menu_Item is access all Menu_Item'Class;

    -- Creates a new Menu_Item within 'view' with 'id'. 'text' is the text of
    -- the item in the menu. 'icon' is the optional icon to draw to the left of
    -- the text.
    function Create_Menu_Item( view      : not null access Game_Views.Game_View'Class;
                               id        : String;
                               text      : String;
                               icon      : Icon_Type := NO_ICON ) return A_Menu_Item;
    pragma Postcondition( Create_Menu_Item'Result /= null );

    function Create_Menu_Item( view : not null access Game_Views.Game_View'Class;
                               text : String;
                               icon : Icon_Type := NO_ICON ) return A_Menu_Item;
    pragma Postcondition( Create_Menu_Item'Result /= null );

    -- Same as Create_Menu_Item except the item has a checkbox that can be toggled.
    function Create_Menu_Checkbox( view : not null access Game_Views.Game_View'Class;
                                   id   : String;
                                   text : String;
                                   icon : Icon_Type := NO_ICON ) return A_Menu_Item;
    pragma Postcondition( Create_Menu_Checkbox'Result /= null );

    function Create_Menu_Checkbox( view : not null access Game_Views.Game_View'Class;
                                   text : String;
                                   icon : Icon_Type := NO_ICON ) return A_Menu_Item;
    pragma Postcondition( Create_Menu_Checkbox'Result /= null );

    -- Creates a separator menu item that visual separates items above and below.
    function Create_Menu_Separator( view : not null access Game_Views.Game_View'Class ) return A_Menu_Item;
    pragma Postcondition( Create_Menu_Separator'Result /= null );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Menu Item Signals
    function Clicked( this : not null access Menu_Item'Class ) return access Signal'Class;
    function Selected( this : not null access Menu_Item'Class ) return access Signal'Class;
    function Unselected( this : not null access Menu_Item'Class ) return access Signal'Class;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Appends the item to Allegro native menu 'parent'. The menu item's root
    -- menu pointer will change.
    procedure Append_To_Native_Menu( this : not null access Menu_Item'Class; parent : A_Allegro_Menu );

    -- Returns the menu item's icon.
    function Get_Icon( this : not null access Menu_Item'Class ) return Icon_Type;

    -- Returns the menu item's shortcut text.
    function Get_Shortcut( this : not null access Menu_Item'Class ) return String;

    -- Returns the menu item's text.
    function Get_Text( this : not null access Menu_Item'Class ) return String;

    -- Returns true if the item is checked (must be checkable).
    function Is_Checked( this : not null access Menu_Item'Class ) return Boolean;

    -- Sets the checked state of the item. It must have been created as a
    -- checkable item. If the checked state changes, the Selected or Unselected
    -- signal will be emitted.
    procedure Set_Checked( this    : not null access Menu_Item'Class;
                           checked : Boolean );

    -- Sets the menu item's icon. The icon will be stretched to 16x16.
    procedure Set_Icon( this : not null access Menu_Item'Class; icon : Icon_Type );

    -- Sets the shortcut text to be displayed.
    procedure Set_Shortcut( this : not null access Menu_Item'Class; text : String );

    -- Sets the text for the menu item.
    procedure Set_Text( this : not null access Menu_Item'Class; text : String );

private

    type Menu_Item is new Widget with
        record
            parentMenu    : A_Allegro_Menu := null;
            childMenu     : A_Allegro_Menu := null;
            nativeId      : Unsigned_16 := 0;
            text          : Unbounded_String;
            shortcut      : Unbounded_String;
            icon          : Icon_Type;
            checkable     : Boolean := False;    -- does it have a checkbox?
            checked       : Boolean := False;    -- state of the checkbox

            sigClicked    : aliased Signal;
            sigSelected   : aliased Signal;
            sigUnselected : aliased Signal;
        end record;

    procedure Construct( this      : access Menu_Item;
                         view      : not null access Game_Views.Game_View'Class;
                         id        : String;
                         text      : String;
                         icon      : Icon_Type;
                         checkable : Boolean );

    function Get_Native_Flags( this : not null access Menu_Item'Class ) return Menu_Item_Flags;

    procedure Handle_Event( this : access Menu_Item;
                            evt  : in out A_Event;
                            resp : out Response_Type );

    -- Returns a string representation of the menu item for debugging purposes.
    function To_String( this : access Menu_Item ) return String;

end Widgets.Menu_Items;
