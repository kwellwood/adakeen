--
-- Copyright (c) 2012-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Widgets.Buttons.Checkboxes is

    -- A Checkbox is a variation of a button that toggles its state when
    -- pressed with a mouse or activated with the keyboard. It differs from a
    -- toggle button in that it has a checkbox icon that changes with the state,
    -- not a pressed/released look. It's state remains until the mouse presses
    -- on it again to toggle the state back again.
    type Checkbox is new Button with private;
    type A_Checkbox is access all Checkbox'Class;

    -- Creates a new checkbox within 'view' with id 'id'. If 'id' is an empty
    -- string, an id will be randomly generated.
    function Create_Checkbox( view : not null access Game_Views.Game_View'Class;
                              id   : String := "" ) return A_Checkbox;
    pragma Postcondition( Create_Checkbox'Result /= null );

    -- Creates a new checkbox within 'view' with id 'id'. 'text' is the
    -- checkbox's text and 'icon' is the icon to display next to the text. The
    -- default state of the new checkbox is False. Call Set_Checkbox_Icons to
    -- override the default icons used for the checkbox itself.
    function Create_Checkbox( view : not null access Game_Views.Game_View'Class;
                              id   : String;
                              text : String;
                              icon : Icon_Type := NO_ICON ) return A_Checkbox;
    pragma Postcondition( Create_Checkbox'Result /= null );

private

    -- added to the elements specified in Widgets.Buttons
    CHECKBOX_ELEM : constant := 5;

    type Checkbox is new Button with null record;

    procedure Construct( this : access Checkbox;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String;
                         text : String;
                         icon : Icon_Type );

    -- Draws the checkbox widget.
    procedure Draw_Content( this : access Checkbox );

    -- Returns the minimum height of the checkbox, based on its icon and text.
    function Get_Min_Height( this : access Checkbox ) return Float;

    -- Returns the minimum width of the button, based on its icon and text.
    function Get_Min_Width( this : access Checkbox ) return Float;

end Widgets.Buttons.Checkboxes;
