--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Icons;                             use Icons;

package Widgets.Game_Screens is

    -- A Game_Screen is a simple abstract container for multiple widgets,
    -- intended to be used with a Screen_Manager object. It fills the entire
    -- window and is intended to represent one "state" of a game's GUI, managing
    -- input and logic, and transitions to other screens. A screen can have an
    -- optional background image or color or it can have a transparent
    -- background.
    --
    -- This class is abstract with the intention that it be instantiated
    -- specifically for each screen in a game's interface (e.g. title screen,
    -- main menu, loading screen, game play, etc.)
    --
    -- A screen populates its child widgets at construction and also handles all
    -- Actions dispatched by its children.
    type Game_Screen is abstract new Widget with private;
    type A_Game_Screen is access all Game_Screen'Class;

    -- Screen Signals
    function Exited( this : not null access Game_Screen'Class ) return access Signal'Class;

    -- Indicates the screen will now begin exiting. On the screen manager's next
    -- tick after this screen has finished exiting, the screen will be removed
    -- and destroyed. Deactivate will be called just before the screen is
    -- removed from its manager.
    procedure Exit_Screen( this : not null access Game_Screen'Class );

    -- Returns the current background image, or NO_ICON if none.
    function Get_Background( this : not null access Game_Screen'Class ) return Icon_Type;

    -- Returns True if the screen is in the processing of exiting.
    function Is_Exiting( this : not null access Game_Screen'Class ) return Boolean;

    -- Returns True if the game screen is opaque, obscuring screens behind it.
    function Is_Opaque( this : not null access Game_Screen'Class ) return Boolean;

private

    BACKGROUND_ELEM : constant := 1;

    DISABLED_STATE : constant Widget_State := 2**0;

    ----------------------------------------------------------------------------

    type Game_Screen is abstract new Widget with
        record
            background : Icon_Type;         -- background image
            opaque     : Boolean := True;   -- screen is opaque?
            exiting    : Boolean := False;  -- in the process of exiting?

            sigExited  : aliased Signal;
        end record;

    procedure Construct( this     : access Game_Screen;
                         view     : not null access Game_Views.Game_View'Class;
                         id       : String;
                         isOpaque : Boolean );

    -- Draws the screen's background image, if it has one, or background color
    -- if it's not transparent.
    procedure Draw_Content( this : access Game_Screen );

    -- Returns the visual state of the screen, based on its enabled and active
    -- state.
    function Get_Visual_State( this : access Game_Screen ) return Widget_State;

    -- Sets the background image name to display behind all the child widgets.
    -- If the background image is not found or is not given, the background will
    -- be transparent.
    procedure Set_Background( this       : not null access Game_Screen'Class;
                              background : Icon_Type );

end Widgets.Game_Screens;
