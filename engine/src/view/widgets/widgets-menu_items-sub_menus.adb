--
-- Copyright (c) 2016-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Native_Dialogs;            use Allegro.Native_Dialogs;
with Allegro.Mouse;                     use Allegro.Mouse;
--with Debugging;                         use Debugging;
with Game_Views;                        use Game_Views;

package body Widgets.Menu_Items.Sub_Menus is

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Popup_Menu( view : not null access Game_Views.Game_View'Class;
                                id   : String := "" ) return A_Sub_Menu is
        this : constant A_Sub_Menu := new Sub_Menu;
    begin
        this.Construct( view, id, "", NO_ICON, popup => True );
        return this;
    end Create_Popup_Menu;

    ----------------------------------------------------------------------------

    function Create_Sub_Menu( view : not null access Game_Views.Game_View'Class;
                              id   : String;
                              text : String;
                              icon : Icon_Type := NO_ICON ) return A_Sub_Menu is
        this : constant A_Sub_Menu := new Sub_Menu;
    begin
        this.Construct( view, id, text, icon, popup => False );
        return this;
    end Create_Sub_Menu;

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this  : access Sub_Menu;
                         view  : not null access Game_Views.Game_View'Class;
                         id    : String;
                         text  : String;
                         icon  : Icon_Type;
                         popup : Boolean ) is
    begin
        Menu_Item(this.all).Construct( view, id, text, icon, checkable => False );
        this.popup := popup;
        this.childMenu := (if this.popup then Al_Create_Popup_Menu else Al_Create_Menu);
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Sub_Menu ) is
    begin
        if this.parentMenu = null then
            -- this is the top level menu (popup or pulldown); delete the whole tree
            Al_Destroy_Menu( this.childMenu );
        end if;
        Menu_Item(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Add_Menu_Item( this : not null access Sub_Menu'Class;
                             item : in out A_Menu_Item ) is
    begin
        this.Add_Child( item );
        item.Append_To_Native_Menu( this.childMenu );
        item := null;
    end Add_Menu_Item;

    ----------------------------------------------------------------------------

    procedure Clear( this : not null access Sub_Menu'Class ) is
    begin
        for child of this.children loop
            Al_Remove_Menu_Item( this.childMenu, Integer(A_Menu_Item(child).nativeId) );
        end loop;
        this.Delete_Children;
    end Clear;

    ----------------------------------------------------------------------------

    procedure Show( this : not null access Sub_Menu'Class; x, y : Float ) is
    begin
        if not this.popup then
            return;
        end if;

        -- try to warp the mouse to the given location
        if Al_Set_Mouse_XY( this.Get_View.Get_Display,
                            Integer(Float'Rounding( x )),
                            Integer(Float'Rounding( y )) )
        then
            null;
        end if;

        -- the popup will appear at the mouse's location
        Al_Popup_Menu( this.childMenu, this.Get_View.Get_Display );
    end Show;

end Widgets.Menu_Items.Sub_Menus;
