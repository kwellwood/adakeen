--
-- Copyright (c) 2012-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Icons;                             use Icons;

package Widgets.Buttons is

    -- A Button is a simple control widget with two states: on and off. A button
    -- emits Pressed, Released, Held and Clicked signals.
    --
    -- This abstract class is intended to be extended by a subclass that
    -- implements a specific behavior, like toggling, or a push button that goes
    -- back to Off as soon as the mouse is released.
    type Button is abstract new Widget and Animated with private;
    type A_Button is access all Button'Class;

    -- Button Signals
    function Pressed( this : not null access Button'Class ) return access Signal'Class;
    function Released( this : not null access Button'Class ) return access Signal'Class;
    function Held( this : not null access Button'Class ) return access Signal'Class;
    function Clicked( this : not null access Button'Class ) return access Signal'Class;

    -- Returns the button's icon.
    function Get_Icon( this : not null access Button'Class ) return Icon_Type;

    -- Returns the state of the button: True if it's on and False if it's off.
    function Get_State( this : not null access Button'Class ) return Boolean;

    -- Returns the button's text.
    function Get_Text( this : not null access Button'Class ) return String;

    -- Sets the button's icon. Leave 'width' and 'height' as 0 to use the icon's
    -- native size, otherwise the icon will be stretched to the size specified.
    procedure Set_Icon( this : not null access Button'Class;
                        icon : Icon_Type );

    -- Sets the state of the button. If the state changes, a Pressed or Released
    -- signal will be emitted accordingly.
    procedure Set_State( this : not null access Button'Class; on : Boolean );

    -- Sets the button's text.
    procedure Set_Text( this : access Button; text : String );

    -- Toggles the state of the button to the opposite of the current state. A
    -- Pressed or Released signal will be emitted accordingly.
    procedure Toggle_State( this : access Button );

private

    BACKGROUND_ELEM : constant := 1;
    BORDER_ELEM     : constant := 2;
    ICON_ELEM       : constant := 3;
    TEXT_ELEM       : constant := 4;

    DISABLED_STATE : constant Widget_State := 2**0;
    PRESS_STATE    : constant Widget_State := 2**1;
    FOCUS_STATE    : constant Widget_State := 2**2;
    HOVER_STATE    : constant Widget_State := 2**3;

    ----------------------------------------------------------------------------

    type Button is abstract new Widget and Animated with
        record
            text        : Unbounded_String;
            icon        : Icon_Type;
            on          : Boolean := False;

            sigPressed  : aliased Signal;
            sigReleased : aliased Signal;
            sigHeld     : aliased Signal;
            sigClicked  : aliased Signal;
        end record;

    procedure Construct( this       : access Button;
                         view       : not null access Game_Views.Game_View'Class;
                         id         : String;
                         widgetType : String;
                         text       : String;
                         icon       : Icon_Type );

    -- Draws the button.
    procedure Draw_Content( this : access Button );

    -- Returns the minimum height of the button, based on its icon and text.
    function Get_Min_Height( this : access Button ) return Float;

    -- Returns the minimum width of the button, based on its icon and text.
    function Get_Min_Width( this : access Button ) return Float;

    -- Returns the visual state of the button, based on its enabled, pressed,
    -- focused, and hovered states.
    function Get_Visual_State( this : access Button ) return Widget_State;

    -- Updates the button's icon, if it's animated.
    procedure Tick( this : access Button; time : Tick_Time );

    -- Returns a string representation of the widget for debugging purposes.
    function To_String( this : access Button ) return String;

end Widgets.Buttons;

