--
-- Copyright (c) 2012-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Icons;                             use Icons;

package Widgets.Labels is

    -- A Label widget is a simple text string with an optional icon to the left
    -- of the text.
    type Label is new Widget and Animated with private;
    type A_Label is access all Label'Class;

    -- Creates a new Label within 'view'. If 'id' is an empty string, a random
    -- id will be generated.
    function Create_Label( view : not null access Game_Views.Game_View'Class;
                           id   : String := "" ) return A_Label;
    pragma Postcondition( Create_Label'Result /= null );

    -- Returns the label's icon.
    function Get_Icon( this : not null access Label'Class ) return Icon_Type;

    -- Returns the label's text.
    function Get_Text( this : not null access Label'Class ) return String;

    -- Sets the label's icon. Leave 'width' and 'height' as 0 to use the icon's
    -- native size, otherwise the icon will be proportionately scaled to fit the
    -- specified size.
    procedure Set_Icon( this : not null access Label'Class;
                        icon : Icon_Type );

    -- Sets the label's text.
    procedure Set_Text( this : not null access Label'Class; text : String );

private

    BACKGROUND_ELEM : constant := 1;
    BORDER_ELEM     : constant := 2;
    ICON_ELEM       : constant := 3;
    TEXT_ELEM       : constant := 4;

    DISABLED_STATE : constant Widget_State := 2**0;

    ----------------------------------------------------------------------------

    type Label is new Widget and Animated with
        record
            icon : Icon_Type;
            text : Unbounded_String;
        end record;

    procedure Construct( this : access Label;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String;
                         text : String;
                         icon : Icon_Type );

    procedure Draw_Content( this : access Label );

    -- Returns the label's minimum height.
    function Get_Min_Height( this : access Label ) return Float;

    -- Returns the label's minimum width.
    function Get_Min_Width( this : access Label ) return Float;

    -- Returns the visual state of the label, based on its enabled state.
    function Get_Visual_State( this : access Label ) return Widget_State;

    -- Updates the label's icon, if it's animated.
    procedure Tick( this : access Label; time : Tick_Time );

    function To_String( this : access Label ) return String;

end Widgets.Labels;
