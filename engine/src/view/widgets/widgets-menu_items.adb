--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.State;                     use Allegro.State;
--with Debugging;                         use Debugging;
with Drawing;                           use Drawing;
with Events.Application;                use Events.Application;
with Game_Views;                        use Game_Views;

package body Widgets.Menu_Items is

    pragma Suppress( Elaboration_Check, Signals.Mouse.Connections );

    package Connections is new Signals.Connections(Menu_Item);
    use Connections;

    ----------------------------------------------------------------------------

    -- Safely copies 'source' to a new memory bitmap of the same size. Unlike
    -- Al_Clone_Bitmap, this allows 'source' to be a sub bitmap.
    function Copy_To_Memory_Bitmap( source : A_Allegro_Bitmap ) return A_Allegro_Bitmap is
        state : Allegro_State;
        copy  : A_Allegro_Bitmap;
    begin
        if source = null then
            return null;
        end if;

        Al_Store_State( state, ALLEGRO_STATE_BITMAP );

        Al_Set_New_Bitmap_Flags( ALLEGRO_MEMORY_BITMAP );
        copy := Al_Create_Bitmap( Al_Get_Bitmap_Width( source ), Al_Get_Bitmap_Height( source ) );
        Al_Set_Target_Bitmap( copy );
        Clear_To_Color( Transparent );
        Al_Draw_Bitmap( source, 0.0, 0.0, 0 );

        Al_Restore_State( state );
        return copy;
    end Copy_To_Memory_Bitmap;

    ----------------------------------------------------------------------------

    nextId : Unsigned_16 := 1;

    function Get_Next_Id return Unsigned_16 is

    begin
        return next : constant Unsigned_16 := nextId do
            nextId := nextId + 1;
            if nextId = 0 then
                nextId := 1;
            end if;
        end return;
    end Get_Next_Id;

    ----------------------------------------------------------------------------

    procedure On_Enabled( this : not null access Menu_Item'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Menu_Item( view : not null access Game_Views.Game_View'Class;
                               id   : String;
                               text : String;
                               icon : Icon_Type := NO_ICON ) return A_Menu_Item is
        this : A_Menu_Item := new Menu_Item;
    begin
        this.Construct( view, id, text, icon, False );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Menu_Item;

    ----------------------------------------------------------------------------

    function Create_Menu_Item( view : not null access Game_Views.Game_View'Class;
                               text : String;
                               icon : Icon_Type := NO_ICON ) return A_Menu_Item
    is (Create_Menu_Item( view, "", text, icon ));

    ----------------------------------------------------------------------------

    function Create_Menu_Checkbox( view : not null access Game_Views.Game_View'Class;
                                   id   : String;
                                   text : String;
                                   icon : Icon_Type := NO_ICON ) return A_Menu_Item is
        this : A_Menu_Item := new Menu_Item;
    begin
        this.Construct( view, id, text, icon, True );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Menu_Checkbox;

    ----------------------------------------------------------------------------

    function Create_Menu_Checkbox( view : not null access Game_Views.Game_View'Class;
                                   text : String;
                                   icon : Icon_Type := NO_ICON ) return A_Menu_Item
    is (Create_Menu_Checkbox( view, "", text, icon ));

    ----------------------------------------------------------------------------

    function Create_Menu_Separator( view : not null access Game_Views.Game_View'Class ) return A_Menu_Item is (Create_Menu_Item( view, "", "" ));

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this      : access Menu_Item;
                         view      : not null access Game_Views.Game_View'Class;
                         id        : String;
                         text      : String;
                         icon      : Icon_Type;
                         checkable : Boolean ) is
    begin
        Widget(this.all).Construct( view, id, "" );
        this.sigClicked   .Init( this );
        this.sigSelected  .Init( this );
        this.sigUnselected.Init( this );

        this.nativeId := Get_Next_Id;
        this.checkable := checkable;

        this.Visible.Set( False );
        this.Set_Focusable( False );
        this.Set_Text( text );
        this.Set_Icon( icon );

        this.Listen_For_Event( EVT_MENU_CLICKED );

        this.Enabled.Connect( Slot( this, On_Enabled'Access ) );
        this.Disabled.Connect( Slot( this, On_Enabled'Access ) );   -- On_Enabled() works for both
    end Construct;

    ----------------------------------------------------------------------------

    function Clicked( this : not null access Menu_Item'Class ) return access Signal'Class is (this.sigClicked'Access);

    function Selected( this : not null access Menu_Item'Class ) return access Signal'Class is (this.sigSelected'Access);

    function Unselected( this : not null access Menu_Item'Class ) return access Signal'Class is (this.sigUnselected'Access);

    ----------------------------------------------------------------------------

    procedure Append_To_Native_Menu( this : not null access Menu_Item'Class; parent : A_Allegro_Menu ) is
    begin
        pragma Assert( this.parentMenu = null );
        this.parentMenu := parent;

        -- copy the icon to use in the menu. it will assume ownership.
        -- we can't simply use Al_Clone_Bitmap because it is most likely a sub
        -- bitmap from a tile atlas.
        Al_Append_Menu_Item( this.parentMenu,
                             this.Get_Text,
                             this.nativeId,
                             this.Get_Native_Flags,
                             Copy_To_Memory_Bitmap( this.Get_Icon.Get_Bitmap ),  -- consumed into 'parent'
                             this.childMenu );
    end Append_To_Native_Menu;

    ----------------------------------------------------------------------------

    function Get_Icon( this : not null access Menu_Item'Class ) return Icon_Type is (this.icon);

    ----------------------------------------------------------------------------

    function Get_Native_Flags( this : not null access Menu_Item'Class ) return Menu_Item_Flags is
    (
        (if this.enable then ALLEGRO_MENU_ITEM_ENABLED else ALLEGRO_MENU_ITEM_DISABLED) or
        (if this.checkable then ALLEGRO_MENU_ITEM_CHECKBOX else 0) or
        (if this.checked then ALLEGRO_MENU_ITEM_CHECKED else 0)
    );

    ----------------------------------------------------------------------------

    function Get_Shortcut( this : not null access Menu_Item'Class ) return String is (To_String( this.shortcut ));

    ----------------------------------------------------------------------------

    function Get_Text( this : not null access Menu_Item'Class ) return String is (To_String( this.text ));

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Event( this : access Menu_Item;
                            evt  : in out A_Event;
                            resp : out Response_Type ) is
        pragma Unreferenced( resp );
    begin
        if evt.Get_Event_Id = EVT_MENU_CLICKED then
            if A_Menu_Event(evt).Get_Menu_Id = Integer(this.nativeId) then
                if this.checkable then
                    this.Set_Checked( not this.checked );
                end if;
                this.Clicked.Emit;
                Delete( evt );       -- no one else needs this event
            end if;
        end if;
    end Handle_Event;

    ----------------------------------------------------------------------------

    function Is_Checked( this : not null access Menu_Item'Class ) return Boolean is (this.checked);

    ----------------------------------------------------------------------------

    procedure On_Enabled( this : not null access Menu_Item'Class ) is
    begin
        if this.parentMenu /= null and then Length( this.text ) > 0 then
            Al_Set_Menu_Item_Flags( this.parentMenu, Integer(this.nativeId), this.Get_Native_Flags );
        end if;
    end On_Enabled;

    ----------------------------------------------------------------------------

    procedure Set_Checked( this    : not null access Menu_Item'Class;
                           checked : Boolean ) is
    begin
        if this.checkable and checked /= this.checked then
            this.checked := checked;
            if this.checked then
                this.Selected.Emit;
            else
                this.Unselected.Emit;
            end if;
        end if;
    end Set_Checked;

    ----------------------------------------------------------------------------

    procedure Set_Icon( this : not null access Menu_Item'Class; icon : Icon_Type ) is
    begin
        this.icon := icon;
        if this.parentMenu /= null then
            Al_Set_Menu_Item_Icon( this.parentMenu,
                                   Integer(this.nativeId),
                                   Copy_To_Memory_Bitmap( this.icon.Get_Bitmap ) );
        end if;
    end Set_Icon;

    ----------------------------------------------------------------------------

    procedure Set_Shortcut( this : not null access Menu_Item'Class; text : String ) is
    begin
        this.shortcut := To_Unbounded_String( text );
    end Set_Shortcut;

    ----------------------------------------------------------------------------

    procedure Set_Text( this : not null access Menu_Item'Class; text : String ) is
    begin
        this.text := To_Unbounded_String( text );
        if this.parentMenu /= null then
            Al_Set_Menu_Item_Caption( this.parentMenu, Integer(this.nativeId), text );
        end if;
    end Set_Text;

    ----------------------------------------------------------------------------

    overriding
    function To_String( this : access Menu_Item ) return String
    is ("<" & this.Get_Class_Name & " '" & To_String( this.text ) & "'>");

end Widgets.Menu_Items;
