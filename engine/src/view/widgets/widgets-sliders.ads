--
-- Copyright (c) 2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Widgets.Sliders is

    -- A Slider is a control widget that represents a numeric value in a range.
    -- The value can be constrained to one of a set of discrete intermediate
    -- values, or it can unconstrained. A slider emits Changed signals to
    -- indicate its value has been changed as the user drags the slider or
    -- increments it with a keypress.
    type Slider is new Widget with private;
    type A_Slider is access all Slider'Class;

    -- Creates a new Slider within 'view'. If 'id' is an empty string, a random
    -- id will be generated.
    function Create_Slider( view : not null access Game_Views.Game_View'Class;
                            id   : String := "" ) return A_Slider;
    pragma Postcondition( Create_Slider'Result /= null );

    -- Slider Signals
    function Changed( this : not null access Slider'Class ) return access Signal'Class;

    -- Enable continuous change, which fires the 'Changed' signal for every
    -- value change while dragging the scroll bar. Otherwise, the signal is
    -- fired only when the user releases the handle.
    procedure Enable_Continuous_Change( this : not null access Slider'Class; enabled : Boolean );

    -- Returns the value at the slider's left side.
    function Get_Left( this : not null access Slider'Class ) return Float;

    -- Returns the value at the slider's right side.
    function Get_Right( this : not null access Slider'Class ) return Float;

    -- Returns the number of discrete steps between left and right values, or 0
    -- if the constraint is disabled.
    function Get_Steps( this : not null access Slider'Class ) return Natural;

    -- Returns the steps hint, or 0 if none.
    function Get_Steps_Hint( this : not null access Slider'Class ) return Natural;

    -- Returns the currently selected value.
    function Get_Value( this : not null access Slider'Class ) return Float;

    -- Sets the slider's range of values, from left to right. If 'left' = 'right'
    -- then nothing will happen. The value will not be constrained to the range
    -- or to a discrete step of the range.
    procedure Set_Range( this  : not null access Slider'Class;
                         left,
                         right : Float );

    -- Sets the number of discrete steps between the left and right values. The
    -- current value will not be constrained. If 'count' < 2 then the constraint
    -- will be disabled.
    procedure Set_Steps( this : not null access Slider'Class; count : Natural );

    -- Sets a hint for how far to increment/decrement the slider when pressing
    -- the left and right arrows but the steps constraint is not specified.
    procedure Set_Steps_Hint( this : not null access Slider'Class; count : Natural );

    -- Sets the value of the slider. If the slider is constrained to discrete
    -- increments, the nearest possible value will be set.
    procedure Set_Value( this : not null access Slider'Class; val : Float );

    -- Sets the value of the slider, moving it by 'stepCount' steps.
    procedure Step_Value( this : not null access Slider'Class; stepCount : Integer );

    -- Updates the value of the slider to reflect a new value, but without
    -- emitting the Changed signal. This is useful to prevent update cycles where
    -- the slider controls a value, but also updates to reflect changes to that
    -- value made by other controls.
    procedure Update_Value( this : not null access Slider'Class; val : Float );

private

    BACKGROUND_ELEM : constant := 1;
    BORDER_ELEM     : constant := 2;
    TRACK_ELEM      : constant := 3;
    HANDLE_ELEM     : constant := 4;

    DISABLED_STATE : constant Widget_State := 2**0;
    PRESS_STATE    : constant Widget_State := 2**1;
    FOCUS_STATE    : constant Widget_State := 2**2;
    HOVER_STATE    : constant Widget_State := 2**3;

    ----------------------------------------------------------------------------

    type Slider is new Widget with
        record
            val        : Float := 0.0;           -- selected value
            leftSide   : Float := 0.0;           -- left hand side value
            rightSide  : Float := 1.0;           -- right hand side value
            steps      : Natural := 0;           -- < 2 means no constraint
            stepsHint  : Natural := 0;           -- < 2 means no hint
            dragging   : Boolean := False;       -- left mouse pressed on slider?
            dragX      : Float := 0.0;
            preDragVal : Float := 0.0;           -- value when dragging started
            allChanges : Boolean := False;       -- emit Changed while dragging?
            sigChanged : aliased Signal;
        end record;

    procedure Construct( this : access Slider;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );

    -- Draws the slider.
    procedure Draw_Content( this : access Slider );

    -- Returns the minimum height of the slider.
    function Get_Min_Height( this : access Slider ) return Float;

    -- Returns the minimum width of the slider.
    function Get_Min_Width( this : access Slider ) return Float;

    -- Returns the visual state of the slider, based on its enabled, pressed,
    -- focused, and hovered states.
    function Get_Visual_State( this : access Slider ) return Widget_State;

    -- Sets the value of the slider to 'val'. Set 'final' to True if this is the
    -- final value being set (versus an intermediate value while the slider is
    -- being dragged). Intermediate value changes will not be signalled unless
    -- continuous changes are enabled. Set 'suppress' to True to completely
    -- suppress signals.
    procedure Set_Value( this : not null access Slider'Class; val : Float; final : Boolean; suppress : Boolean );

    -- Returns a string representation of the widget for debugging purposes.
    function To_String( this : access Slider ) return String;

end Widgets.Sliders;
