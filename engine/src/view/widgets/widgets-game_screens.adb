--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Debugging;                         use Debugging;
with Drawing;                           use Drawing;
with Drawing.Primitives;                use Drawing.Primitives;
with Drawing.Tiles;                     use Drawing.Tiles;
with Game_Views;                        use Game_Views;
with Support;                           use Support;
with Values.Strings;                    use Values.Strings;
with Widget_Styles.Registry;            use Widget_Styles.Registry;

package body Widgets.Game_Screens is

    package Connections is new Signals.Connections(Game_Screen);
    use Connections;

    ----------------------------------------------------------------------------

    procedure On_Hidden( this : not null access Game_Screen'Class );

    procedure On_Shown( this : not null access Game_Screen'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    not overriding
    procedure Construct( this     : access Game_Screen;
                         view     : not null access Game_Views.Game_View'Class;
                         id       : String;
                         isOpaque : Boolean ) is
    begin
        Widget(this.all).Construct( view, id, "Screen" );

        this.sigExited.Init( this );

        this.Shown.Connect( Slot( this, On_Shown'Access ) );
        this.Hidden.Connect( Slot( this, On_Hidden'Access ) );
        this.opaque := isOpaque;
    end Construct;

    ----------------------------------------------------------------------------

    function Exited( this : not null access Game_Screen'Class ) return access Signal'Class is (this.sigExited'Access);

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Content( this : access Game_Screen ) is
        state : constant Widget_State := this.Get_Visual_State;
        color : Allegro_Color;
    begin
        color := this.style.Get_Color( BACKGROUND_ELEM, state );
        if color /= Transparent then
            Rectfill( 0.0, 0.0, this.viewport.width, this.viewport.height,
                      Lighten( color, this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );
        end if;
        Draw_Tile_Filled( this.style.Get_Image( BACKGROUND_ELEM, state ).Get_Tile,
                          0.0, 0.0, 0.0,
                          this.viewport.width, this.viewport.height,
                          Lighten( this.style.Get_Tint( BACKGROUND_ELEM, state ),
                                   this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );
    end Draw_Content;

    ----------------------------------------------------------------------------

    procedure Exit_Screen( this : not null access Game_Screen'Class ) is
    begin
        this.exiting := True;
    end Exit_Screen;

    ----------------------------------------------------------------------------

    function Get_Background( this : not null access Game_Screen'Class ) return Icon_Type is
        icon : constant Icon_Type := this.style.Get_Image( BACKGROUND_ELEM );
    begin
        if icon /= NO_ICON then
            return icon;
        end if;
        return this.background;
    end Get_Background;

    ----------------------------------------------------------------------------

    overriding
    function Get_Visual_State( this : access Game_Screen ) return Widget_State is
    (
        (if not this.Is_Enabled then DISABLED_STATE else 0)
    );

    ----------------------------------------------------------------------------

    function Is_Exiting( this : not null access Game_Screen'Class ) return Boolean is (this.exiting);

    ----------------------------------------------------------------------------

    function Is_Opaque( this : not null access Game_Screen'Class ) return Boolean is (this.opaque);

    ----------------------------------------------------------------------------

    procedure On_Hidden( this : not null access Game_Screen'Class ) is
    begin
        pragma Debug( Dbg( "Hiding screen " & this.To_String, D_GUI, Info ) );
    end On_Hidden;

    ----------------------------------------------------------------------------

    procedure On_Shown( this : not null access Game_Screen'Class ) is
    begin
        pragma Debug( Dbg( "Showing screen " & this.To_String, D_GUI, Info ) );
        this.Request_Focus;
    end On_Shown;

    ----------------------------------------------------------------------------

    procedure Set_Background( this       : not null access Game_Screen'Class;
                              background : Icon_Type ) is
        tileId : constant Natural := background.Get_Tile.Get_Id;
    begin
        this.background := background;
        this.style.Set_Property( BACKGROUND_ELEM, STYLE_IMAGE, DEFAULT_STATE,
                                 Create( (if tileId > 0
                                          then background.Get_Library.Get.Get_Name & ":" & Image( tileId )
                                          else "") ) );
    end Set_Background;

begin

    Register_Style( "Screen",
                    (BACKGROUND_ELEM => To_Unbounded_String( "background" )),
                    (BACKGROUND_ELEM => Area_Element),
                    (0 => To_Unbounded_String( "disabled" )) );

end Widgets.Game_Screens;
