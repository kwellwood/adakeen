--
-- Copyright (c) 2017-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Widget_Styles.Registry;            use Widget_Styles.Registry;

package body Widgets.Stack_Layouts is

    package Connections is new Signals.Connections(Stack_Layout);
    use Connections;

    procedure On_Style_Changed( this : not null access Stack_Layout'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Stack_Layout( view : not null access Game_Views.Game_View'Class;
                                  id   : String := "" ) return A_Stack_Layout is
        this : constant A_Stack_Layout := new Stack_Layout;
    begin
        this.Construct( view, id );
        return this;
    end Create_Stack_Layout;

    ----------------------------------------------------------------------------

    procedure Append( this  : not null access Stack_Layout'Class;
                      child : not null access Widget'Class ) is
    begin
        this.Insert( this.Count + 1, child );
    end Append;

    ----------------------------------------------------------------------------

    procedure Clear( this : not null access Stack_Layout'Class ) is
    begin
        this.Delete_Children;
        this.order.Clear;
        if this.index > 0 then
            this.index := 0;
            this.sigIndexChanged.Emit;
        end if;
    end Clear;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Stack_Layout;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String ) is
    begin
        Widget(this.all).Construct( view, id, "StackLayout" );
        this.sigIndexChanged.Init( this );

        this.StyleChanged.Connect( Slot( this, On_Style_Changed'Access ) );

        -- set the inner anchor offsets from the background padding in the style
        this.On_Style_Changed;
    end Construct;

    ----------------------------------------------------------------------------

    function Count( this : not null access Stack_Layout'Class ) return Natural is (Natural(this.order.Length));

    ----------------------------------------------------------------------------

    function Get_At( this : not null access Stack_Layout'Class; index : Integer ) return A_Widget is
    begin
        if index >= 1 and index <= this.Count then
            return this.order.Element( index );
        end if;
        return null;
    end Get_At;

    ----------------------------------------------------------------------------

    function Get_Index( this : not null access Stack_Layout'Class ) return Integer is (this.index);

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Height( this : access Stack_Layout ) return Float is
        maxHeight : Float := 0.0;
    begin
        for child of this.order loop
            maxHeight := Float'Max( maxHeight, child.Get_Min_Height );
        end loop;

        return this.style.Get_Pad_Top( BACKGROUND_ELEM ) +
               maxHeight +
               this.style.Get_Pad_Bottom( BACKGROUND_ELEM );
    end Get_Min_Height;

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Width( this : access Stack_Layout ) return Float is
        maxWidth : Float := 0.0;
    begin
        for child of this.order loop
            maxWidth := Float'Max( maxWidth, child.Get_Min_Width );
        end loop;

        return this.style.Get_Pad_Left( BACKGROUND_ELEM ) +
               maxWidth +
               this.style.Get_Pad_Right( BACKGROUND_ELEM );
    end Get_Min_Width;

    ----------------------------------------------------------------------------

    overriding
    function Get_Preferred_Height( this : access Stack_Layout ) return Float is
        maxHeight : Float := 0.0;
    begin
        if not this.prefHeight.Is_Null then
            return this.prefHeight.To_Float;
        end if;

        for child of this.order loop
            maxHeight := Float'Max( maxHeight, child.Get_Preferred_Height );
        end loop;

        return this.style.Get_Pad_Top( BACKGROUND_ELEM ) +
               maxHeight +
               this.style.Get_Pad_Bottom( BACKGROUND_ELEM );
    end Get_Preferred_Height;

    ----------------------------------------------------------------------------

    overriding
    function Get_Preferred_Width( this : access Stack_Layout ) return Float is
        maxWidth : Float := 0.0;
    begin
        if not this.prefWidth.Is_Null then
            return this.prefWidth.To_Float;
        end if;

        for child of this.order loop
            maxWidth := Float'Max( maxWidth, child.Get_Preferred_Width );
        end loop;

        return this.style.Get_Pad_Left( BACKGROUND_ELEM ) +
               maxWidth +
               this.style.Get_Pad_Right( BACKGROUND_ELEM );
    end Get_Preferred_Width;

    ----------------------------------------------------------------------------

    function IndexChanged( this : not null access Stack_Layout'Class ) return access Signal'Class is (this.sigIndexChanged'Access);

    ----------------------------------------------------------------------------

    procedure Insert( this  : not null access Stack_Layout'Class;
                      index : Integer;
                      child : not null access Widget'Class ) is
        newIndex : Integer;
    begin
        newIndex := Integer'Min( Integer'Max( 1, index ), this.Count + 1 );

        this.Add_Child( child );
        this.order.Insert( newIndex, A_Widget(child) );

        child.Unset_Width;
        child.Unset_Height;
        child.Unset_X;
        child.Unset_Y;
        child.Fill( this );

        child.Visible.Set( this.index = 0 );

        if this.index <= 0 then
            -- first widget into the stack becomes visible
            this.index := newIndex;
            this.sigIndexChanged.Emit;
        elsif newIndex <= this.index then
            -- increment the selected index to keep the same widget selected
            this.index := this.index + 1;
        end if;
    end Insert;

    ----------------------------------------------------------------------------

    procedure On_Style_Changed( this : not null access Stack_Layout'Class ) is
    begin
        this.ancLeft.innerPad := this.style.Get_Pad_Left( BACKGROUND_ELEM );
        this.ancTop.innerPad := this.style.Get_Pad_Top( BACKGROUND_ELEM );
        this.ancRight.innerPad := -this.style.Get_Pad_Right( BACKGROUND_ELEM );
        this.ancBottom.innerPad := -this.style.Get_Pad_Bottom( BACKGROUND_ELEM );

        this.Update_Geometry;
    end On_Style_Changed;

    ----------------------------------------------------------------------------

    function Remove( this : not null access Stack_Layout'Class; index : Integer ) return A_Widget is
        child : A_Widget;
    begin
        child := this.Get_At( index );
        if child /= null then
            child.Visible.Set( False );
            this.order.Delete( index );
            this.Remove_Child( child );
            if index < this.index then
                -- decrement the selected index to keep the same widget selected
                this.index := this.index - 1;
            elsif index = this.index then
                if index > this.Count then
                    -- removed the last one
                    this.index := this.index - 1;
                end if;
                if this.index > 0 then
                    this.Get_At( this.index ).Visible.Set( True );
                end if;
                this.sigIndexChanged.Emit;
            end if;
        end if;
        return child;
    end Remove;

    ----------------------------------------------------------------------------

    procedure Set_Index( this : not null access Stack_Layout'Class; index : Integer ) is
        current : A_Widget;
    begin
        -- the new index must be valid
        if index < 1 or this.Count < index then
            return;
        end if;

        if this.index /= index then
            current := this.Get_At( this.index );
            if current /= null then
                current.Visible.Set( False );
            end if;

            this.index := index;
            this.Get_At( this.index ).Visible.Set( True );
            this.sigIndexChanged.Emit;
        end if;
    end Set_Index;

begin

    Register_Style( "StackLayout",
                    (BACKGROUND_ELEM => To_Unbounded_String( "background" )),
                    (BACKGROUND_ELEM => Area_Element),
                    (0 => To_Unbounded_String( "disabled" )) );

end Widgets.Stack_Layouts;
