--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Debugging;                         use Debugging;
with Support;                           use Support;
with Values;                            use Values;
with Values.Casting;                    use Values.Casting;

package body Icons is

    function Create_Icon( libName  : String;
                          iconName : String;
                          required : Boolean := True ) return Icon_Type is
        lib : constant Library_Ptr := Load_Library( libName, bitmaps => True );
    begin
        if lib.Get.Is_Loaded then
            return Create_Icon( lib, iconName, required );
        elsif required then
            Dbg( "Failed to load library '" & libName & "' for icon '" & iconName & "'",
                 D_TILES or D_GUI, Error );
        end if;
        return Icon_Type'(others => <>);
    end Create_Icon;

    ----------------------------------------------------------------------------

    function Create_Icon( libName  : String;
                          iconId   : Natural;
                          required : Boolean := True ) return Icon_Type is
        lib  : constant Library_Ptr := Load_Library( libName, bitmaps => True );
    begin
        if lib.Get.Is_Loaded then
            return Create_Icon( lib, iconId, required );
        elsif required then
            Dbg( "Create_Icon: Failed to load library '" & libName & "' for icon" & iconId'Img,
                 D_TILES or D_GUI, Error );
        end if;
        return Icon_Type'(others => <>);
    end Create_Icon;

    ----------------------------------------------------------------------------

    function Create_Icon( lib      : Library_Ptr;
                          iconId   : Natural;
                          required : Boolean := True ) return Icon_Type is
        tile : A_Tile;
    begin
        if lib.Not_Null and then lib.Get.Is_Loaded then
            tile := lib.Get.Get_Tile( iconId );
            if tile /= null and then tile.Get_Bitmap /= null then
                return Icon_Type'(lib    => lib,
                                  tile   => tile,
                                  width  => Al_Get_Bitmap_Width( tile.Get_Bitmap ),
                                  height => Al_Get_Bitmap_Height( tile.Get_Bitmap ),
                                  frameDelay => Milliseconds( Cast_Int( tile.Get_Attribute( "delay" ) ) ),
                                  frameLoop  => tile.Get_Attribute( "frames" ).Lst,
                                  others     => <>);
            elsif iconId > 0 and then required then
                Dbg( "Create_Icon: Tile id " & Image( iconId ) &
                     " not found in library <" & lib.Get.Get_Name & ">",
                     D_TILES or D_GUI, Error );
            end if;
        elsif required then
            Dbg( "Create_Icon: Null library", D_GUI, Error );
        end if;
        return Icon_Type'(others => <>);
    end Create_Icon;

    ----------------------------------------------------------------------------

    function Create_Icon( lib      : Library_Ptr;
                          iconName : String;
                          required : Boolean := True ) return Icon_Type is
        tile : A_Tile;
    begin
        if lib.Not_Null and then lib.Get.Is_Loaded then
            tile := lib.Get.Get_Tile( lib.Get.Get_Id( iconName ) );
            if tile /= null and then tile.Get_Bitmap /= null then
                return Icon_Type'(lib        => lib,
                                  tile       => tile,
                                  width      => Al_Get_Bitmap_Width( tile.Get_Bitmap ),
                                  height     => Al_Get_Bitmap_Height( tile.Get_Bitmap ),
                                  frameDelay => Milliseconds( Cast_Int( tile.Get_Attribute( "delay" ) ) ),
                                  frameLoop  => tile.Get_Attribute( "frames" ).Lst,
                                  others     => <>);
            elsif required then
                Dbg( "Failed to load icon '" & iconName &
                     "' from library <" & lib.Get.Get_Name & ">",
                     D_TILES or D_GUI, Error );
            end if;
        elsif required then
            Dbg( "Create_Icon: Null library", D_GUI, Error );
        end if;
        return Icon_Type'(others => <>);
    end Create_Icon;

    ----------------------------------------------------------------------------

    function Create_Icon( iconId   : String;
                          required : Boolean := True ) return Icon_Type is
        delim : constant Natural := Index( iconId, ":" );
        isId  : Boolean := True;
    begin
        if delim > iconId'First and then delim < iconId'Last then
            -- Check if the icon name could be a tile id
            if iconId'Length - delim <= 6 then
                for i in (delim+1)..iconId'Last loop
                    if not Is_Digit( iconId(i) ) then
                        isId := False;
                        exit;
                    end if;
                end loop;
            else
                -- too long to be an id
                isId := False;
            end if;

            if isId then
                -- Load the icon as a tile id
                return Create_Icon( iconId(iconId'First..delim-1),
                                    Natural'Value( iconId(delim+1..iconId'Last) ),
                                    required );
            else
                -- Load the icon as a filename
                return Create_Icon( iconId(iconId'First..delim-1),
                                    iconId(delim+1..iconId'Last),
                                    required );
            end if;
        elsif iconId'Length > 0 and then required then
            Dbg( "Create_Icon: Failed to parse icon string '" & iconId & "'", D_GUI, Error );
        end if;
        return NO_ICON;
    end Create_Icon;

    ----------------------------------------------------------------------------

    function Get_Bitmap( this : Icon_Type ) return A_Allegro_Bitmap is ((if this.tile /= null then this.tile.Get_Bitmap else null));

    function Get_Height( this : Icon_Type ) return Natural is (this.height);

    function Get_Library( this : Icon_Type ) return Library_Ptr is (this.lib);

    function Get_Mask( this : Icon_Type ) return A_Allegro_Bitmap is ((if this.tile /= null then this.tile.Get_Mask else null));

    function Get_Width( this : Icon_Type ) return Natural is (this.width);

    function Get_Tile( this : Icon_Type ) return A_Tile is (this.tile);

    ----------------------------------------------------------------------------

    procedure Update_Animation( this : in out Icon_Type; now : Time_Span ) is
    begin
        if this.lib.Is_Null or else not this.lib.Get.Is_Loaded then
            return;
        end if;

        if this.anmStart = Time_Span_Zero then
            this.anmStart := now;
        end if;

        -- update the icon if it's animated
        if this.frameDelay > Time_Span_Zero then
            if this.frameLoop.Valid then
                -- next frame in a looping animation
                this.tile := this.lib.Get.Get_Tile( this.frameLoop.Get_Int( 1 + (((now - this.anmStart) / this.frameDelay) mod this.frameLoop.Length) ) );

            elsif (now - this.anmStart) >= this.frameDelay then
                -- next frame in a one-shot animation
                this.anmStart := now;
                this.tile := this.lib.Get.Get_Tile( Cast_Int( this.tile.Get_Attribute( "next" ) ) );
                this.frameDelay := (if this.tile /= null then Milliseconds( Cast_Int( this.tile.Get_Attribute( "delay" ), 0 ) ) else Time_Span_Zero);
                this.frameLoop := (if this.tile /= null then this.tile.Get_Attribute( "frames" ).Lst else Null_Value.Lst);
            end if;
        end if;
    end Update_Animation;

    --==========================================================================

    function "="( l, r : Icon_Type ) return Boolean is (l.lib = r.lib and then l.tile = r.tile);

end Icons;
