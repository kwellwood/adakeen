--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

limited with Game_Views;

package Widgets.Buttons.Pushes is

    -- A Push_Button is a variation of a button that is on when it's being
    -- pressed with the mouse or keyboard, and immediately returns to off when
    -- the mouse or keyboard is released. It does not stay on until clicked
    -- again.
    type Push_Button is new Button with private;
    type A_Push_Button is access all Push_Button'Class;

    -- Creates a new push button within 'view' with id 'id'. If 'id' is an empty
    -- string, an id will be randomly generated.
    function Create_Push_Button( view : not null access Game_Views.Game_View'Class;
                                 id   : String := "" ) return A_Button;
    pragma Postcondition( Create_Push_Button'Result /= null );

    -- Creates a new push button within 'view' with id 'id'. 'text' is the
    -- button's text and 'icon' is the optional icon to display.
    function Create_Push_Button( view : not null access Game_Views.Game_View'Class;
                                 id   : String;
                                 text : String;
                                 icon : Icon_Type := NO_ICON ) return A_Button;
    pragma Postcondition( Create_Push_Button'Result /= null );

    -- Creates a new push button within 'view' with id 'id' and no text label.
    -- 'icon' is the icon to display.
    function Create_Push_Button( view : not null access Game_Views.Game_View'Class;
                                 id   : String;
                                 icon : Icon_Type ) return A_Button
    is (Create_Push_Button( view, id, "", icon ));

private

    type Push_Button is new Button with
        record
            gamepadId : Integer := -1;
        end record;

    procedure Construct( this : access Push_Button;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String;
                         text : String;
                         icon : Icon_Type );

    -- Toggles the state of the button and then toggles it back again. (Push
    -- buttons don't stay on!) Both Press and Release actions will occur.
    procedure Toggle_State( this : access Push_Button );

end Widgets.Buttons.Pushes;
