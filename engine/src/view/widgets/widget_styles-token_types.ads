--
-- Copyright (c) 2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Widget_Styles.Token_Types is

    type Token_Type is
    (
        TK_IMPORT,          -- "import"
        TK_COPY,            -- "copy"
        TK_FROM,            -- "from"

        TK_AT,              -- "@"
        TK_LBRACE,          -- "{"
        TK_RBRACE,          -- "}"
        TK_COLON,           -- ":"
        TK_SEMICOLON,       -- ";"
        TK_DOT,             -- "."

        TK_VALUE,           -- any literal value
        TK_SYMBOL,          -- any symbol

        TK_EOL,             -- ASCII line feed

        TK_EOF              -- end of file
    );

    -- Returns the syntactic representation of the token type. For token types
    -- that don't have a syntactic representation (e.g. TK_VALUE, TK_SYMBOL, etc.)
    -- an empty string will be returned.
    function Image( t : Token_Type ) return String;

    -- Returns the name of the Token_Type value in readable form. Used for error
    -- messages.
    function Name( t : Token_Type ) return String;

    -- Converts a string 'img' to a Token_Type, returning 'valid' as True on
    -- success, or False on failure. This procedure does not support tokens that
    -- do not map directly to a single string.
    procedure To_Token_Type( img : String; t : out Token_Type; valid : out Boolean );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Returns true if 'c' is a delimiter character, otherwise false.
    function Is_Delimiter( c : Character ) return Boolean;

end Widget_Styles.Token_Types;
