--
-- Copyright (c) 2016-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Widgets.Menu_Items.Sub_Menus is

    -- A Sub_Menu is a container for menu items that is itself a menu item. It
    -- can be used in menu bars and popup menus, and other menus.
    type Sub_Menu is new Menu_Item with private;
    type A_Sub_Menu is access all Sub_Menu'Class;

    -- Creates a new top-level popup context menu within 'view' with id 'id'.
    -- This Sub_Menu cannot be used in the menu bar.
    function Create_Popup_Menu( view : not null access Game_Views.Game_View'Class;
                                id   : String := "" ) return A_Sub_Menu;

    -- Creates a new menu within 'view' with id 'id. 'text' and 'icon' will be
    -- displayed in the parent menu. The Sub_Menu can be used within a popup
    -- context menu or the menu bar.
    function Create_Sub_Menu( view : not null access Game_Views.Game_View'Class;
                              id   : String;
                              text : String;
                              icon : Icon_Type := NO_ICON ) return A_Sub_Menu;
    pragma Postcondition( Create_Sub_Menu'Result /= null );

    -- Adds an item to the bottom of the menu. 'item' will be consumed.
    procedure Add_Menu_Item( this : not null access Sub_Menu'Class;
                             item : in out A_Menu_Item );
    pragma Precondition( item /= null );
    pragma Postcondition( item = null );

    -- Removes all menu items.
    procedure Clear( this : not null access Sub_Menu'Class );

    -- Shows the menu as a popup on the screen at window coordinates 'x', 'y'.
    -- The mouse will be warped to the given location because that's currently
    -- the only mechanism for positioning the native menu.
    --
    -- If the menu was not created as a popup, nothing will happen.
    procedure Show( this : not null access Sub_Menu'Class; x, y : Float );

private

    type Sub_Menu is new Menu_Item with
        record
            popup : Boolean := False;
        end record;

    procedure Construct( this  : access Sub_Menu;
                         view  : not null access Game_Views.Game_View'Class;
                         id    : String;
                         text  : String;
                         icon  : Icon_Type;
                         popup : Boolean );

    procedure Delete( this : in out Sub_Menu );

end Widgets.Menu_Items.Sub_Menus;
