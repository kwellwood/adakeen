--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Drawing;                           use Drawing;
with Drawing.Primitives;                use Drawing.Primitives;
with Drawing.Tiles;                     use Drawing.Tiles;
with Game_Views;                        use Game_Views;
with Icons;                             use Icons;
with Widget_Styles.Property_Accessors;  use Widget_Styles.Property_Accessors;
with Widget_Styles.Registry;            use Widget_Styles.Registry;

package body Widgets.Panels is

    package Connections is new Signals.Connections(Panel);
    use Connections;

    ----------------------------------------------------------------------------

    procedure On_Style_Changed( this : not null access Panel'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Panel( view : not null access Game_Views.Game_View'Class;
                           id   : String := "" ) return A_Panel is
        this : A_Panel := new Panel;
    begin
        this.Construct( view, id, "Panel" );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Panel;

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this       : access Panel;
                         view       : not null access Game_Views.Game_View'Class;
                         id         : String;
                         widgetType : String ) is
    begin
        Widget(this.all).Construct( view, id, widgetType );
        this.StyleChanged.Connect( Slot( this, On_Style_Changed'Access ) );

        this.propBackgroundColor.Construct( view.Get_Process_Manager, Style_Color_Accessor'(this.style, BACKGROUND_ELEM, STYLE_COLOR, DEFAULT_STATE) );

        -- set the inner anchor offsets from the background padding in the style
        this.On_Style_Changed;
    end Construct;

    ----------------------------------------------------------------------------

    function BackgroundColor( this : not null access Panel'Class ) return access Animated_Color.Property'Class is (this.propBackgroundColor'Access);

    ----------------------------------------------------------------------------

    procedure Add_Widget( this  : not null access Panel'Class;
                          child : not null access Widget'Class ) is
    begin
        this.Add_Child( child );
    end Add_Widget;

    ----------------------------------------------------------------------------

    procedure Clear_Widgets( this : not null access Panel'Class ) is
    begin
        this.Delete_Children;
    end Clear_Widgets;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Content( this : access Panel ) is
        state : constant Widget_State := this.Get_Visual_State;
        color : Allegro_Color;
    begin
        -- draw the background --
        color := this.style.Get_Color( BACKGROUND_ELEM, state );
        if color /= Transparent then
            Rectfill( this.viewport.x, this.viewport.y,
                      this.viewport.x + this.viewport.width, this.viewport.y + this.viewport.height,
                      Lighten( color, this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );
        else
            Draw_Tile_Filled( this.style.Get_Image( BACKGROUND_ELEM, state ).Get_Tile,
                              this.viewport.x, this.viewport.y, 0.0,
                              this.viewport.width, this.viewport.height,
                              Lighten( this.style.Get_Tint( BACKGROUND_ELEM, state ),
                                       this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );
        end if;

        -- draw the border --
        Draw_Tile_Filled( this.style.Get_Image( BORDER_ELEM, state ).Get_Tile,
                          this.viewport.x, this.viewport.y, 0.0,
                          this.viewport.width, this.viewport.height,
                          Lighten( this.style.Get_Tint( BORDER_ELEM, state ),
                                   this.style.Get_Shade( BORDER_ELEM, state ) ) );
    end Draw_Content;

    ----------------------------------------------------------------------------

    procedure On_Style_Changed( this : not null access Panel'Class ) is
    begin
        this.ancLeft.innerPad := this.style.Get_Pad_Left( BACKGROUND_ELEM );
        this.ancTop.innerPad := this.style.Get_Pad_Top( BACKGROUND_ELEM );
        this.ancRight.innerPad := -this.style.Get_Pad_Right( BACKGROUND_ELEM );
        this.ancBottom.innerPad := -this.style.Get_Pad_Bottom( BACKGROUND_ELEM );

        this.Update_Geometry;
    end On_Style_Changed;

begin

    Register_Style( "Panel",
                    (BACKGROUND_ELEM => To_Unbounded_String( "background" ),
                     BORDER_ELEM     => To_Unbounded_String( "border" )),
                    (BACKGROUND_ELEM => Area_Element,
                     BORDER_ELEM     => Area_Element),
                    (0 => To_Unbounded_String( "disabled" )) );

end Widgets.Panels;
