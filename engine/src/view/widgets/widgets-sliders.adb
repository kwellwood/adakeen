--
-- Copyright (c) 2017-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Keyboard;                  use Allegro.Keyboard;
--with Debugging;                         use Debugging;
with Drawing.Bitmaps;                   use Drawing.Bitmaps;
with Drawing.Primitives;                use Drawing.Primitives;
with Drawing.Tiles;                     use Drawing.Tiles;
with Support;                           use Support;
with Tiles;                             use Tiles;
with Widget_Styles.Registry;            use Widget_Styles.Registry;

package body Widgets.Sliders is

    package Key_Connections is new Signals.Keys.Connections(Slider);
    use Key_Connections;

    package Mouse_Connections is new Signals.Mouse.Connections(Slider);
    use Mouse_Connections;

    ----------------------------------------------------------------------------

    -- Calculates the area of the slider handle from the current value. It is
    -- constrained to the slider track when the current value is out of range.
    procedure Calculate_Handle( this : not null access Slider'Class; rect : out Rectangle );

    -- Jumps to the right value.
    procedure On_Key_End_Pressed( this : not null access Slider'Class );

    -- Jumps to the left value.
    procedure On_Key_Home_Pressed( this : not null access Slider'Class );

    -- Jumps one tick closer to the left.
    procedure On_Key_Left_Pressed( this : not null access Slider'Class );

    -- Jumps one tick closer to the right.
    procedure On_Key_Right_Pressed( this : not null access Slider'Class );

    -- Handles dragging with the left mouse button. If the mouse is dragging the
    -- handle, it will move. Otherwise, it will be ignored.
    procedure On_Mouse_Moved( this  : not null access Slider'Class;
                              mouse : Mouse_Arguments );

    -- Handles the left mouse button to grab the handle or jump it to a specific
    -- value. If the mouse is pressed on the handle, it will be grabbed for
    -- dragging.
    procedure On_Mouse_Pressed( this  : not null access Slider'Class;
                                mouse : Button_Arguments );

    -- Handles the left mouse button to release the handle, if it was dragging.
    procedure On_Mouse_Released( this  : not null access Slider'Class );

    -- Handles mouse vertical scroll wheel.
    procedure On_Mouse_Scrolled( this  : not null access Slider'Class;
                                 mouse : Scroll_Arguments );

    -- Calculates the handle X (left edge) corresponding with a value 'val'. It
    -- is constrained by steps and the left/right endpoints.
    function Translate_Value_To_X( this : not null access Slider'Class; val : Float ) return Float;

    -- Calculates the slider value given a handle X (left edge of the handle).
    -- The value will NOT be constrained.
    function Translate_X_To_Value( this : not null access Slider'Class; x : Float ) return Float;

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Slider( view : not null access Game_Views.Game_View'Class;
                            id   : String := "" ) return A_Slider is
        this : constant A_Slider := new Slider;
    begin
        this.Construct( view, id );
        return this;
    end Create_Slider;

    ----------------------------------------------------------------------------

    procedure Calculate_Handle( this : not null access Slider'Class; rect : out Rectangle ) is
        padTop    : constant Float := this.style.Get_Pad_Top( BACKGROUND_ELEM );
        padBottom : constant Float := this.style.Get_Pad_Bottom( BACKGROUND_ELEM );
        handle    : constant A_Tile := this.style.Get_Image( HANDLE_ELEM ).Get_Tile;
    begin
        -- constrain the handle position in case the value is out of range
        rect.x := this.Translate_Value_To_X( this.val );
        rect.y := padTop + (this.viewport.height - (padTop + handle.Get_Height + padBottom)) / 2.0;
        rect.width := handle.Get_Width;
        rect.height := handle.Get_Height;
    end Calculate_Handle;

    ----------------------------------------------------------------------------

    function Changed( this : not null access Slider'Class ) return access Signal'Class is (this.sigChanged'Access);

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Slider;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String ) is
    begin
        Widget(this.all).Construct( view, id, "Slider" );
        this.sigChanged.Init( this );

        this.KeyPressed.Connect( Slot( this, On_Key_End_Pressed'Access, ALLEGRO_KEY_END ) );
        this.KeyPressed.Connect( Slot( this, On_Key_Home_Pressed'Access, ALLEGRO_KEY_HOME ) );
        this.KeyPressed.Connect( Slot( this, On_Key_Left_Pressed'Access, ALLEGRO_KEY_LEFT ) );
        this.KeyPressed.Connect( Slot( this, On_Key_Right_Pressed'Access, ALLEGRO_KEY_RIGHT ) );

        this.MouseMoved.Connect( Slot( this, On_Mouse_Moved'Access ) );
        this.MousePressed.Connect( Slot( this, On_Mouse_Pressed'Access, Mouse_Left ) );
        this.MouseReleased.Connect( Slot( this, On_Mouse_Released'Access, Mouse_Left ) );
        this.MouseScrolled.Connect( Slot( this, On_Mouse_Scrolled'Access ) );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Content( this : access Slider ) is
        state      : constant Widget_State := this.Get_Visual_State;
        padTop     : constant Float := this.style.Get_Pad_Top( BACKGROUND_ELEM );
        padBottom  : constant Float := this.style.Get_Pad_Bottom( BACKGROUND_ELEM );
        padLeft    : constant Float := this.style.Get_Pad_Left( BACKGROUND_ELEM );
        padRight   : constant Float := this.style.Get_Pad_Right( BACKGROUND_ELEM );
        handle     : constant A_Tile := this.style.Get_Image( HANDLE_ELEM, state ).Get_Tile;
        handleRect : Rectangle;
        color      : Allegro_Color;
    begin
        -- - - - Calculate Positions - - - --

        this.Calculate_Handle( handleRect );

        -- - - - Draw Content - - - --

        -- draw the background --
        color := this.style.Get_Color( BACKGROUND_ELEM, state );
        if color /= Transparent then
            Rectfill( 0.0, 0.0, this.viewport.width, this.viewport.height,
                      Lighten( color, this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );
        else
            Draw_Tile_Filled( this.style.Get_Image( BACKGROUND_ELEM, state ).Get_Tile,
                              0.0, 0.0, 0.0,
                              this.viewport.width, this.viewport.height,
                              Lighten( this.style.Get_Tint( BACKGROUND_ELEM, state ),
                                       this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );
        end if;

        -- draw the border --
        Draw_Tile_Filled( this.style.Get_Image( BORDER_ELEM, state ).Get_Tile,
                          0.0, 0.0, 0.0,
                          this.viewport.width, this.viewport.height,
                          Lighten( this.style.Get_Tint( BORDER_ELEM, state ),
                                   this.style.Get_Shade( BORDER_ELEM, state ) ) );

        -- draw the track --
        -- fixme: if the viewport's height is even and the track's height is odd,
        -- then it will draw it on a half pixel in Y and blur it.
        Draw_Tile_Filled( this.style.Get_Image( TRACK_ELEM, state ).Get_Tile,
                          padLeft, padTop, 0.0,
                          this.viewport.width - (padLeft + padRight), this.viewport.height - (padTop + padBottom),
                          Lighten( this.style.Get_Tint( TRACK_ELEM, state ),
                                   this.style.Get_Shade( TRACK_ELEM, state ) ) );

        -- draw the handle --
        Draw_Bitmap( handle.Get_Bitmap,
                     Float'Rounding( handleRect.x ), Float'Rounding( handleRect.y ), 0.0,
                     Lighten( this.style.Get_Tint( HANDLE_ELEM, state ),
                              this.style.Get_Shade( HANDLE_ELEM, state ) ) );
    end Draw_Content;

    ----------------------------------------------------------------------------

    procedure Enable_Continuous_Change( this : not null access Slider'Class; enabled : Boolean ) is
    begin
        this.allChanges := enabled;
    end Enable_Continuous_Change;

    ----------------------------------------------------------------------------

    function Get_Left( this : not null access Slider'Class ) return Float is (this.leftSide);

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Height( this : access Slider ) return Float is
        padTop     : constant Float := this.style.Get_Pad_Top( BACKGROUND_ELEM );
        padBottom  : constant Float := this.style.Get_Pad_Bottom( BACKGROUND_ELEM );
    begin
        return padTop +
               Float'Max( Float(this.style.Get_Image( TRACK_ELEM ).Get_Height),
                          Float(this.style.Get_Image( HANDLE_ELEM ).Get_Height) ) +
               padBottom;
    end Get_Min_Height;

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Width( this : access Slider ) return Float is
        padLeft  : constant Float := this.style.Get_Pad_Left( BACKGROUND_ELEM );
        padRight : constant Float := this.style.Get_Pad_Right( BACKGROUND_ELEM );
    begin
        return padLeft +
               Float(this.style.Get_Image( HANDLE_ELEM ).Get_Height) +
               padRight;
    end Get_Min_Width;

    ----------------------------------------------------------------------------

    function Get_Right( this : not null access Slider'Class ) return Float is (this.rightSide);

    ----------------------------------------------------------------------------

    overriding
    function Get_Visual_State( this : access Slider ) return Widget_State is
    (
           (if not this.Is_Enabled then DISABLED_STATE             else 0)
        or (if this.dragging       then PRESS_STATE or HOVER_STATE else 0)
        or (if this.Is_Focused     then FOCUS_STATE                else 0)
        or (if this.hover          then HOVER_STATE                else 0)
    );

    ----------------------------------------------------------------------------

    function Get_Steps( this : not null access Slider'Class ) return Natural is (this.steps);

    ----------------------------------------------------------------------------

    function Get_Steps_Hint( this : not null access Slider'Class ) return Natural is (this.stepsHint);

    ----------------------------------------------------------------------------

    function Get_Value( this : not null access Slider'Class ) return Float is (this.val);

    ----------------------------------------------------------------------------

    procedure On_Key_End_Pressed( this : not null access Slider'Class ) is
    begin
        this.Set_Value( this.rightSide );
    end On_Key_End_Pressed;

    ----------------------------------------------------------------------------

    procedure On_Key_Home_Pressed( this : not null access Slider'Class ) is
    begin
        this.Set_Value( this.leftSide );
    end On_Key_Home_Pressed;

    ----------------------------------------------------------------------------

    procedure On_Key_Left_Pressed( this : not null access Slider'Class ) is
    begin
        this.Step_Value( -1 );
    end On_Key_Left_Pressed;

    ----------------------------------------------------------------------------

    procedure On_Key_Right_Pressed( this : not null access Slider'Class ) is
    begin
        this.Step_Value( 1 );
    end On_Key_Right_Pressed;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Moved( this  : not null access Slider'Class;
                              mouse : Mouse_Arguments ) is
        oldX : Float;
        newX : Float;
    begin
        if this.dragging then
            oldX := this.Translate_Value_To_X( this.val );
            this.Set_Value( this.Translate_X_To_Value( oldX + (mouse.x - this.dragX) ), final => False, suppress => False );

            -- update the drag position by how much the handle moved. it may not
            -- have moved at all if a constraint prevented it, it may have moved
            -- freely with the mouse, or it may have jumped from one step to another.
            newX := this.Translate_Value_To_X( this.val );
            this.dragX := this.dragX + (newX - oldX);
        end if;
    end On_Mouse_Moved;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Pressed( this  : not null access Slider'Class;
                                mouse : Button_Arguments ) is
        handleRect : Rectangle;
    begin
        this.Calculate_Handle( handleRect );
        if Contains( handleRect, mouse.x, mouse.y ) then
            this.dragging := True;
            this.preDragVal := this.val;
            this.dragX := mouse.x;
        else
            this.Set_Value( this.Translate_X_To_Value( mouse.x - handleRect.width / 2.0 ) );

            -- begin dragging from the new value. don't do this for sliders with
            -- steps because the new handle position may not be under the mouse
            -- and it doesn't slide smoothly anyway.
            if this.steps = 0 then
                this.dragging := True;
                this.preDragVal := this.val;
                this.dragX := mouse.x;
            end if;
        end if;
    end On_Mouse_Pressed;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Released( this  : not null access Slider'Class ) is
        final : Float;
    begin
        this.dragging := False;

        -- if continuous changes enabled, then signals were already emitted
        if not this.allChanges then
            final := this.val;
            this.val := this.preDragVal;
            this.Set_Value( final, final => True, suppress => False );    -- emit Changed if the value changed
        end if;
    end On_Mouse_Released;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Scrolled( this  : not null access Slider'Class;
                                 mouse : Scroll_Arguments ) is
    begin
        this.Step_Value( Integer(Float'Rounding( mouse.scrollAmt )) );
    end On_Mouse_Scrolled;

    ----------------------------------------------------------------------------

    procedure Set_Range( this  : not null access Slider'Class;
                         left,
                         right : Float ) is
    begin
        if left /= right then
            this.leftSide := left;
            this.rightSide := right;
        end if;
    end Set_Range;

    ----------------------------------------------------------------------------

    procedure Set_Steps( this : not null access Slider'Class; count : Natural ) is
    begin
        this.steps := count;
        if this.steps < 2 then
            this.steps := 0;
        end if;
    end Set_Steps;

    ----------------------------------------------------------------------------

    procedure Set_Steps_Hint( this : not null access Slider'Class; count : Natural ) is
    begin
        this.stepsHint := count;
        if this.stepsHint < 2 then
            this.stepsHint := 0;
        end if;
    end Set_Steps_Hint;

    ----------------------------------------------------------------------------

    procedure Set_Value( this : not null access Slider'Class; val : Float ) is
    begin
        -- don't allow the value to be set while dragging because the final value
        -- hasn't been determined yet. this may be called by a slot that receives
        -- the Changed signal while dragging with continuous change enabled.
        if not this.dragging then
            this.Set_Value( val, final => True, suppress => False );
        end if;
    end Set_Value;

    ----------------------------------------------------------------------------

    procedure Set_Value( this : not null access Slider'Class; val : Float; final : Boolean; suppress : Boolean ) is
        actual   : Float;
        stepSize : Float;
    begin
        -- constrain to the range
        actual := Constrain( val, this.leftSide, this.rightSide );

        -- constrain by rounding to the nearest step
        if this.steps > 1 then
            stepSize := (this.rightSide - this.leftSide) / Float(this.steps - 1);
            actual := this.leftSide + Float'Rounding( (actual - this.leftSide) / stepSize ) * stepSize;

            -- in case of floating point inaccuracy
            actual := Constrain( actual, this.leftSide, this.rightSide );
        end if;

        if actual /= this.val then
            this.val := actual;
            if (final or this.allChanges) and not suppress then
                this.sigChanged.Emit;
            end if;
        end if;
    end Set_Value;

    ----------------------------------------------------------------------------

    procedure Step_Value( this : not null access Slider'Class; stepCount : Integer ) is
        steps : constant Integer := (if this.steps > 1 then this.steps
                                     elsif this.stepsHint > 1 then this.stepsHint
                                     else 5);
    begin
        this.Set_Value( this.val + Float(stepCount) * (this.rightSide - this.leftSide) / Float(steps - 1) );
    end Step_Value;

    ----------------------------------------------------------------------------

    overriding
    function To_String( this : access Slider ) return String is
    (
        "<" & this.Get_Class_Name & " " &
            ": id=" & To_String( this.id ) &
            ", range=" & Image( this.leftSide ) & ".." & Image( this.rightSide ) &
            ", val=" & Image( this.val ) &
        "'>"
    );

    ----------------------------------------------------------------------------

    function Translate_Value_To_X( this : not null access Slider'Class; val : Float ) return Float is
        padLeft    : constant Float := this.style.Get_Pad_Left( BACKGROUND_ELEM );
        padRight   : constant Float := this.style.Get_Pad_Right( BACKGROUND_ELEM );
        handle     : constant A_Tile := this.style.Get_Image( HANDLE_ELEM ).Get_Tile;
        trackRange : constant Float := this.viewport.width - (padLeft + handle.Get_Width + padRight);
        valueRange : constant Float := this.rightSide - this.leftSide;
    begin
        return padLeft + Constrain( (val - this.leftSide) / valueRange * trackRange, 0.0, trackRange );
    end Translate_Value_To_X;

    ----------------------------------------------------------------------------

    function Translate_X_To_Value( this : not null access Slider'Class; x : Float ) return Float is
        padLeft    : constant Float := this.style.Get_Pad_Left( BACKGROUND_ELEM );
        padRight   : constant Float := this.style.Get_Pad_Right( BACKGROUND_ELEM );
        handle     : constant A_Tile := this.style.Get_Image( HANDLE_ELEM ).Get_Tile;
        trackRange : constant Float := this.viewport.width - (padLeft + handle.Get_Width + padRight);
        valueRange : constant Float := this.rightSide - this.leftSide;
    begin
        return this.leftSide + Constrain( (x - padLeft) / trackRange * valueRange, 0.0, valueRange );
    end Translate_X_To_Value;

    ----------------------------------------------------------------------------

    procedure Update_Value( this : not null access Slider'Class; val : Float ) is
    begin
        this.Set_Value( val, final => True, suppress => True );
    end Update_Value;

begin

    Register_Style( "Slider",
                    (BACKGROUND_ELEM => To_Unbounded_String( "background" ),
                     BORDER_ELEM     => To_Unbounded_String( "border" ),
                     TRACK_ELEM      => To_Unbounded_String( "track" ),
                     HANDLE_ELEM     => To_Unbounded_String( "handle" )),
                    (BACKGROUND_ELEM => Area_Element,
                     BORDER_ELEM     => Area_Element,
                     TRACK_ELEM      => Area_Element,
                     HANDLE_ELEM     => Icon_Element),
                    (0 => To_Unbounded_String( "disabled" ),
                     1 => To_Unbounded_String( "press" ),
                     2 => To_Unbounded_String( "focus" ),
                     3 => To_Unbounded_String( "hover" )) );

end Widgets.Sliders;
