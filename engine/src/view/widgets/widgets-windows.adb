--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.State;                     use Allegro.State;
with Allegro.Transformations;           use Allegro.Transformations;
with Debugging;                         use Debugging;
with Events.Input;                      use Events.Input;
with Game_Views;                        use Game_Views;
with Interfaces;                        use Interfaces;
with Keyboard;                          use Keyboard;
with Mouse.Cursors;                     use Mouse.Cursors;
with Mouse.Cursors.Manager;

package body Widgets.Windows is

    pragma Suppress( Elaboration_Check, Signals.Keys.Connections );

    package Connections is new Signals.Connections(Window);
    use Connections;

    package Key_Connections is new Signals.Keys.Connections(Window);
    use Key_Connections;

    use Widget_Lists;

    ----------------------------------------------------------------------------

    -- Dispatches a mouse click event to the widget that the mouse clicked on.
    -- If the button release occurred over a different widget than the press,
    -- then the click will be ignored.
    procedure Dispatch_Click( this : not null access Window'Class;
                              evt  : not null A_Mouse_Button_Event );

    -- Dispatches a mouse double click event to the widget that the mouse
    -- clicked on. If the button release occurred over a different widget than
    -- the press, then the click will be ignored.
    procedure Dispatch_Doubleclick( this : not null access Window'Class;
                                    evt  : not null A_Mouse_Button_Event );

    -- Dispatches a gamepad axis positioning event to the focused widget.
    procedure Dispatch_Gamepad_Move( this    : not null access Window'Class;
                                     evt     : not null A_Gamepad_Event;
                                     handled : in out Boolean );

    -- Dispatches a gamepad button press event to the focused widget.
    procedure Dispatch_Gamepad_Press( this    : not null access Window'Class;
                                      evt     : not null A_Gamepad_Event;
                                      handled : in out Boolean );

    -- Dispatches a gamepad button release event to the focused widget. Unlike
    -- the keyboard, a button release is not necessarily sent to the same widget
    -- that received the earlier button press event. All gamepad events are sent
    -- directly to the focused widget.
    procedure Dispatch_Gamepad_Release( this    : not null access Window'Class;
                                        evt     : not null A_Gamepad_Event;
                                        handled : in out Boolean );

    -- Dispatches a key press event to the focused widget.
    procedure Dispatch_Key_Press( this    : not null access Window'Class;
                                  evt     : not null A_Key_Event;
                                  handled : in out Boolean );

    -- Dispatches a key release event to the widget that received the key press
    -- event, regardless of input focus.
    procedure Dispatch_Key_Release( this    : not null access Window'Class;
                                    evt     : not null A_Key_Event;
                                    handled : in out Boolean );

    -- Dispatches a key typed event to the focused widget, if the key was
    -- pressed after the widget received focus.
    procedure Dispatch_Key_Typed( this    : not null access Window'Class;
                                  evt     : not null A_Key_Typed_Event;
                                  handled : in out Boolean );

    -- Dispatches a mouse held event to the widget that the mouse was over at
    -- the time the mouse button was pressed.
    procedure Dispatch_Mouse_Held( this : not null access Window'Class;
                                   evt  : not null A_Mouse_Button_Event );

    -- Dispatches a mouse move event. If the mouse is over a different widget
    -- than last move then widget exit and widget enter events are dispatched
    -- appropriately first.
    --
    -- If the left mouse button is being held down, a mouse move event is sent
    -- to the widget that the mouse was over when the left button was pressed.
    -- If the mouse is currently over a widget that isn't the one that the left
    -- mouse button was pressed over, then that widget will also receive a mouse
    -- move event.
    procedure Dispatch_Mouse_Move( this : not null access Window'Class;
                                   evt  : not null A_Mouse_Event );

    -- Dispatches a mouse press event to the widget the mouse is currently over.
    -- If a popup widget (or stack of popup widgets) is being displayed, then
    -- the popup widgets will be popped off the stack and hidden unless they are
    -- under the mouse. This allows context menus, etc, to be hidden when the
    -- user clicks somewhere else in the Window. If a modal dialog widget is
    -- being displayed, then the press will only be dispatched to descendents of
    -- the modal widget.
    procedure Dispatch_Mouse_Press( this : not null access Window'Class;
                                    evt  : not null A_Mouse_Button_Event );

    -- Dispatches a mouse release event to the widget the mouse is over.
    procedure Dispatch_Mouse_Release( this : not null access Window'Class;
                                      evt  : not null A_Mouse_Button_Event );

    -- Dispatches a mouse release event of button 'btn' to the widget on the
    -- screen at 'x', 'y'.
    procedure Dispatch_Mouse_Release( this : not null access Window'Class;
                                      x, y : Float;
                                      btn  : Mouse_Button );

    -- Dispatches a scroll wheel event to the widget the mouse is over.
    procedure Dispatch_Mouse_Scroll( this    : not null access Window'Class;
                                     evt     : not null A_Mouse_Scroll_Event;
                                     handled : out Boolean );

    -- Called when a Mouse_Leave event is received, to null out the mouse over
    -- widget.
    procedure Handle_Mouse_Leave( this : not null access Window'Class );

    -- Handles a widget that is deleted while references to it from the Window
    -- remain.
    procedure On_Deleted( this : not null access Window'Class );

    -- Closes any open popups when Escape is typed.
    procedure On_Key_Escape( this : not null access Window'Class );

    -- Sets/changes a widget reference 'reference' from the Window to 'widget'.
    -- References include the focused widget, the mouse-over widget, and widgets
    -- that will receive a mouse release or key press when the event occurs.
    --
    -- This procedure will disconnect from the previously referenced widget's
    -- Deleted signal if it's no longer referenced, and connect to 'widget'.
    -- This signal is used to detect when referenced widgets are deleted out from
    -- under the Window, so it can clean up dangling references.
    procedure Set_Reference( this      : not null access Window'Class;
                             reference : in out A_Widget;
                             widget    : A_Widget );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Window( view : access Game_Views.Game_View'Class;
                            id   : String := "" ) return A_Window is
        this : A_Window := new Window;
    begin
        this.Construct( view, id );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Window;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Window;
                         view : access Game_Views.Game_View'Class;
                         id   : String ) is
    begin
        Widget(this.all).Construct( view, id, "" );
        this.KeyTyped.Connect( Slot( this, On_Key_Escape'Access, ALLEGRO_KEY_ESCAPE, (others=>No) ) );

        this.hasFocus := True;
        this.focus := A_Widget(this);
        this.modal := A_Widget(this);

        this.Listen_For_Event( EVT_KEY_PRESS );
        this.Listen_For_Event( EVT_KEY_RELEASE );
        this.Listen_For_Event( EVT_KEY_TYPED );
        this.Listen_For_Event( EVT_GAMEPAD_PRESS );
        this.Listen_For_Event( EVT_GAMEPAD_RELEASE );
        this.Listen_For_Event( EVT_GAMEPAD_MOVE );
        this.Listen_For_Event( EVT_MOUSE_HELD );
        this.Listen_For_Event( EVT_MOUSE_MOVE );
        this.Listen_For_Event( EVT_MOUSE_PRESS );
        this.Listen_For_Event( EVT_MOUSE_RELEASE );
        this.Listen_For_Event( EVT_MOUSE_CLICK );
        this.Listen_For_Event( EVT_MOUSE_DOUBLECLICK );
        this.Listen_For_Event( EVT_MOUSE_SCROLL );
        this.Listen_For_Event( EVT_MOUSE_LEAVE );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Window ) is
    begin
        this.deleting := True;

        -- clear the widget references so hiding the child widgets before
        -- deleting them won't cause us to reference widgets that were deleted
        -- earlier in the child traversal.
        this.Set_Reference( this.mouseOver, null );
        for i in this.mousePressedOn'Range loop
            this.Set_Reference( this.mousePressedOn(i), null );
        end loop;
        for i in this.mouseClickedOn'Range loop
            this.Set_Reference( this.mouseClickedOn(i), null );
        end loop;
        this.Set_Reference( this.focus, null );

        -- there shouldn't be any popups active during deletion or they will be
        -- dropped on the floor
        this.popups.Clear;

        -- the modal widget should have been cleared before deletion
        this.modal := null;

        Widget(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Add_Widget( this  : not null access Window'Class;
                          child : not null access Widget'Class ) is
    begin
        this.Add_Child( child );
    end Add_Widget;

    ----------------------------------------------------------------------------

    procedure Dispatch_Click( this : not null access Window'Class;
                              evt  : not null A_Mouse_Button_Event ) is
        btn    : constant Mouse_Button := evt.Get_Button;
        cx, cy : Float;
    begin
        -- only send the click to the widget if a press and release occurred
        -- over the same widget
        if this.mouseClickedOn(btn) /= null then
            this.mouseClickedOn(btn).Translate_To_Content( Float(evt.Get_X), Float(evt.Get_Y), cx, cy );
            this.mouseClickedOn(btn).MouseClicked.Emit( btn, evt.Get_Modifiers, cx, cy );
        end if;
    end Dispatch_Click;

    ----------------------------------------------------------------------------

    procedure Dispatch_Doubleclick( this : not null access Window'Class;
                                    evt  : not null A_Mouse_Button_Event ) is
        btn    : constant Mouse_Button := evt.Get_Button;
        cx, cy : Float;
    begin
        -- only send the click to the widget if a press and release occurred
        -- over the same widget
        if this.mouseClickedOn(btn) /= null then
            this.mouseClickedOn(btn).Translate_To_Content( Float(evt.Get_X), Float(evt.Get_Y), cx, cy );
            this.mouseClickedOn(btn).MouseDoubleClicked.Emit( btn, evt.Get_Modifiers, cx, cy );
        end if;
    end Dispatch_Doubleclick;

    ----------------------------------------------------------------------------

    procedure Dispatch_Gamepad_Move( this    : not null access Window'Class;
                                     evt     : not null A_Gamepad_Event;
                                     handled : in out Boolean ) is
        wdgt : A_Widget;
    begin
        -- propagate the event upward from the focused widget
        wdgt := this.focus;
        while wdgt /= null loop
            wdgt.GamepadMoved.Emit( evt.Get_Gamepad_Id, evt.Get_Axis, evt.Get_Position, handled );
            exit when handled;
            wdgt := wdgt.parent;
        end loop;
    end Dispatch_Gamepad_Move;

    ----------------------------------------------------------------------------

    procedure Dispatch_Gamepad_Press( this    : not null access Window'Class;
                                      evt     : not null A_Gamepad_Event;
                                      handled : in out Boolean ) is
        wdgt : A_Widget;
    begin
        -- propagate the event upward from the focused widget
        wdgt := this.focus;
        while wdgt /= null loop
            wdgt.GamepadPressed.Emit( evt.Get_Gamepad_Id, evt.Get_Button, handled );
            exit when handled;
            wdgt := wdgt.parent;
        end loop;
    end Dispatch_Gamepad_Press;

    ----------------------------------------------------------------------------

    procedure Dispatch_Gamepad_Release( this    : not null access Window'Class;
                                        evt     : not null A_Gamepad_Event;
                                        handled : in out Boolean ) is
        wdgt : A_Widget;
    begin
        -- propagate the event upward from the focused widget
        wdgt := this.focus;
        while wdgt /= null loop
            wdgt.GamepadReleased.Emit( evt.Get_Gamepad_Id, evt.Get_Button, handled );
            exit when handled;
            wdgt := wdgt.parent;
        end loop;
    end Dispatch_Gamepad_Release;

    ----------------------------------------------------------------------------

    procedure Dispatch_Key_Press( this    : not null access Window'Class;
                                  evt     : not null A_Key_Event;
                                  handled : in out Boolean ) is
        key  : constant Integer := evt.Get_Key;
        wdgt : A_Widget;
    begin
        if key in this.keyPressSentTo'Range then
            -- todo: this.keyPressSentTo(key) should be marked as the actual
            -- widget that handled the first signal, so that the release is sent
            -- to the same exact widget, with propagation. However, the KeyTyped
            -- signal must be sent to the focused widget as well, and it doesn't
            -- have to be handled by the same widget as the KeyPressed signal.
            -- care must be take in Give_Focus to send the focus to the correct
            -- event.
            this.Set_Reference( this.keyPressSentTo(key), this.focus );

            -- propagate the event upward from the focused widget
            wdgt := this.keyPressSentTo(key);
            while wdgt /= null loop
                wdgt.KeyPressed.Emit( key, ASCII.NUL, MODIFIERS_NONE, handled );
                exit when handled;
                wdgt := wdgt.parent;
            end loop;
        end if;
    end Dispatch_Key_Press;

    ----------------------------------------------------------------------------

    procedure Dispatch_Key_Release( this    : not null access Window'Class;
                                    evt     : not null A_Key_Event;
                                    handled : in out Boolean ) is
        key  : constant Integer := evt.Get_Key;
        wdgt : A_Widget;
    begin
        if key in this.keyPressSentTo'Range then
            if this.keyPressSentTo(key) /= null then

                -- propagate the event upward from the focused widget
                wdgt := this.keyPressSentTo(key);
                while wdgt /= null loop
                    wdgt.KeyReleased.Emit( key, ASCII.NUL, MODIFIERS_NONE, handled );
                    exit when handled;
                    wdgt := wdgt.parent;
                end loop;

                this.Set_Reference( this.keyPressSentTo(key), null );
            end if;
        end if;
    end Dispatch_Key_Release;

    ----------------------------------------------------------------------------

    procedure Dispatch_Key_Typed( this    : not null access Window'Class;
                                  evt     : not null A_Key_Typed_Event;
                                  handled : in out Boolean ) is
        key       : constant Integer := evt.Get_Key;
        modifiers : Modifiers_Array;
        wdgt      : A_Widget;
    begin
        evt.Get_Modifiers( modifiers );
        if key in this.keyPressSentTo'Range then
            if this.keyPressSentTo(key) /= null then
                -- propagate the event upward from the focused widget
                wdgt := this.keyPressSentTo(key);
                while wdgt /= null loop
                    wdgt.KeyTyped.Emit( key, evt.Get_Char, modifiers, handled );
                    exit when handled;
                    wdgt := wdgt.parent;
                end loop;
            end if;
        end if;
    end Dispatch_Key_Typed;

    ----------------------------------------------------------------------------

    procedure Dispatch_Mouse_Held( this : not null access Window'Class;
                                   evt  : not null A_Mouse_Button_Event ) is
        btn    : constant Mouse_Button := evt.Get_Button;
        cx, cy : Float;
    begin
        -- if the mouse button was pressed on a widget, then we send a held
        -- event to that widget, even if the mouse isn't currently over it.
        if this.mousePressedOn(btn) /= null then
            this.mousePressedOn(btn).Translate_To_Content( Float(evt.Get_X), Float(evt.Get_Y), cx, cy );
            this.mousePressedOn(btn).MouseHeld.Emit( btn, evt.Get_Modifiers, cx, cy );
        end if;
    end Dispatch_Mouse_Held;

    ----------------------------------------------------------------------------

    procedure Dispatch_Mouse_Move( this : not null access Window'Class;
                                   evt  : not null A_Mouse_Event ) is
        current : A_Widget;
        cx, cy  : Float;
        target  : A_Widget;
    begin
        this.mouseX := Float(evt.Get_X);
        this.mouseY := Float(evt.Get_Y);

        this.Find_Widget_At( Float(evt.Get_X), Float(evt.Get_Y), True, cx, cy, current );

        -- handle the exit and enter events when a mouse button isn't being held
        -- on a specific widget
        if this.mousePressedOn(Mouse_Left) = null and
           this.mousePressedOn(Mouse_Middle) = null and
           this.mousePressedOn(Mouse_Right) = null
        then
            if this.mouseOver /= current then
                -- the mouse hover widget has changed, notify the previous hovered
                -- widget and the new hovered widget.
                if this.mouseOver /= null then
                    this.mouseOver.Set_Hover( False );
                end if;
                if current /= null then
                    current.Set_Hover( True );
                end if;
                this.Set_Reference( this.mouseOver, current );
                this.Show_Mouse_Cursor( this.mouseOver );
            end if;
        end if;

        -- send the mouse moved event to the mouse hovered widget. if a mouse
        -- button is being held, this may be different than the widget the mouse
        -- is actually over.
        target := this.mouseOver;
        if target /= null and then target.Is_Showing then
            target.Translate_To_Content( this.mouseX, this.mouseY, cx, cy );
            target.MouseMoved.Emit( cx, cy );
        end if;
    end Dispatch_Mouse_Move;

    ----------------------------------------------------------------------------

    procedure Dispatch_Mouse_Press( this : not null access Window'Class;
                                    evt  : not null A_Mouse_Button_Event ) is
        btn       : constant Mouse_Button := evt.Get_Button;
        mouseOver : A_Widget := null;
        cx, cy    : Float := 0.0;
        suppress  : Boolean := False;
    begin
        if this.mouseOver = null then
            this.Find_Widget_At( Float(evt.Get_X), Float(evt.Get_Y), True, cx, cy, mouseOver );
            this.Set_Reference( this.mouseOver, mouseOver );
        end if;
        this.Set_Reference( this.mousePressedOn(btn), this.mouseOver );

        if this.mousePressedOn(btn) /= null then
            -- iterate through the popup stack, popping each off until we find
            -- a widget that is an ancestor of mousePressedOn(btn). this clears
            -- popup windows when the mouse clicks away from them.
            while not this.popups.Is_Empty loop
                exit when this.mousePressedOn(btn).Is_Descendant_Of( this.popups.Last_Element );
                this.Pop_Popup;
            end loop;

            -- if the widget the mouse was pressed on is not within the current
            -- modal widget then don't dispatch the press event.
            if not this.mousePressedOn(btn).Is_Descendant_Of( this.modal ) then
                -- however, if the mouse was pressed on a widget that is a
                -- descendant of a widget still on the popup stack, then it's ok
                -- to dispatch the press event. (see: Find_Widget_At)
                suppress := True;
                for popup of reverse this.popups loop
                    if this.mousePressedOn(btn).Is_Descendant_Of( popup ) then
                        suppress := False;
                        exit;
                    end if;
                end loop;
                if suppress then
                    -- suppress the press event, it shouldn't be sent to a
                    -- widget "behind" a modal or popup widget.
                    this.Set_Reference( this.mousePressedOn(btn), null );
                    this.Set_Reference( this.mouseOver, null );
                    return;
                end if;
            end if;
        end if;

        -- send the press to the widget the mouse is currently over
        if this.mousePressedOn(btn) /= null then
            -- change the focused widget
            this.Give_Focus( this.mousePressedOn(btn) );
            this.mousePressedOn(btn).Translate_To_Content( Float(evt.Get_X), Float(evt.Get_Y), cx, cy );
            this.mousePressedOn(btn).mouseButtons(btn) := True;
            this.mousePressedOn(btn).MousePressed.Emit( btn, evt.Get_Modifiers, cx, cy );
        end if;
    end Dispatch_Mouse_Press;

    ----------------------------------------------------------------------------

    procedure Dispatch_Mouse_Release( this : not null access Window'Class;
                                      evt  : not null A_Mouse_Button_Event ) is
    begin
        this.Dispatch_Mouse_Release( Float(evt.Get_X), Float(evt.Get_Y), evt.Get_Button );
    end Dispatch_Mouse_Release;

    ----------------------------------------------------------------------------

    procedure Dispatch_Mouse_Release( this : not null access Window'Class;
                                      x, y : Float;
                                      btn  : Mouse_Button ) is
        cx, cy : Float;   -- widget content coordinates
    begin
        -- send the release event to the same widget that received the press
        if this.mousePressedOn(btn) /= null then
            this.mousePressedOn(btn).Translate_To_Content( x, y, cx, cy );
            this.mousePressedOn(btn).mouseButtons(btn) := False;
            this.mousePressedOn(btn).MouseReleased.Emit( btn, MODIFIERS_NONE, cx, cy );
        end if;

        -- indicate that the last press and release were over the same widget.
        -- this does not mean an actual click occurred because the release
        -- could have delayed too long.
        if this.mousePressedOn(btn) = this.mouseOver then
            this.Set_Reference( this.mouseClickedOn(btn), this.mouseOver );
        else
            this.Set_Reference( this.mouseClickedOn(btn), null );
        end if;

        this.Set_Reference( this.mousePressedOn(btn), null );

        -- when the left mouse button is released, the owner of the mouse cursor
        -- has potentially changed. the new owner would be the widget currently
        -- under the mouse.
        if btn = Mouse_Left then
            this.Show_Mouse_Cursor( this.mouseOver );
        end if;
    end Dispatch_Mouse_Release;

    ----------------------------------------------------------------------------

    procedure Dispatch_Mouse_Scroll( this    : not null access Window'Class;
                                     evt     : not null A_Mouse_Scroll_Event;
                                     handled : out Boolean ) is
        wdgt   : A_Widget := this.mouseOver;
        cx, cy : Float;
    begin
        handled := False;
        while wdgt /= null loop

            -- translate screen coordinates to wdgt content coordinates
            wdgt.Translate_To_Content( Float(evt.Get_X), Float(evt.Get_Y), cx, cy );
            wdgt.MouseScrolled.Emit( cx, cy, Float(evt.Get_Amount), handled );
            if handled then
                exit;
            end if;

            -- give the widget's parent an opportunity to handle the event
            wdgt := wdgt.parent;
        end loop;
    end Dispatch_Mouse_Scroll;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw( this : access Window ) is
        clipX, clipY,
        clipW, clipH : Integer;

        ------------------------------------------------------------------------

        procedure Draw_Child( child : not null A_Widget ) is
            childGeom    : constant Rectangle := child.Get_Geometry;
            state        : Allegro_State;
            winX1, winY1 : Float;
            winX2, winY2 : Float;
            trans        : Allegro_Transform;
        begin
            -- the clipping rectangle of the child is calculated in window
            -- coordinates because it is not affected by transformations.
            child.Translate_To_Window( child.viewport.x, child.viewport.y, winX1, winY1 );
            child.Translate_To_Window( child.viewport.x + child.viewport.width, child.viewport.y + child.viewport.height, winX2, winY2 );

            -- the child's clipping rectangle is constrained to the parent's
            -- clipping rectangle.
            winX1 := Float(Integer'Max( clipX, Integer(winX1) ));
            winY1 := Float(Integer'Max( clipY, Integer(winY1) ));

            Al_Set_Clipping_Rectangle( Integer'Max( clipX, Integer(winX1) ),
                                       Integer'Max( clipY, Integer(winY1) ),
                                       Integer'Max( 0, Integer'Min( clipX + clipW - Integer(winX1), Integer(winX2 - winX1) ) ),
                                       Integer'Max( 0, Integer'Min( clipY + clipH - Integer(winY1), Integer(winY2 - winY1) ) ) );

            Al_Store_State( state, ALLEGRO_STATE_TRANSFORM );

            Al_Identity_Transform( trans );
            Al_Translate_Transform_3d( trans, childGeom.x, childGeom.y, child.z );
            Al_Compose_Transform( trans, Al_Get_Current_Transform.all );
            Al_Use_Transform( trans );

            child.Draw;

            Al_Restore_State( state );
        end Draw_Child;

        ------------------------------------------------------------------------

        state : Allegro_State;
        trans : Allegro_Transform;
    begin
        if not this.Is_Visible or this.geom.width <= 0.0 or this.geom.height <= 0.0 then
            return;
        end if;

        Al_Store_State( state, ALLEGRO_STATE_TRANSFORM );

        -- drawing to bmp, the drawing must be translated by
        -- (win.vx1, win.vy1) in case the display is larger than the
        -- scaled window content.
        Al_Identity_Transform( trans );
        Al_Translate_Transform( trans, -this.viewport.x, -this.viewport.y );
        Al_Scale_Transform( trans, this.zoomFactor, this.zoomFactor );
        Al_Compose_Transform( trans, Al_Get_Current_Transform.all );
        Al_Use_Transform( trans );

        -- draw all the visible children (except those in the popups list)
        Al_Get_Clipping_Rectangle( clipX, clipY, clipW, clipH );
        for child of this.children loop
            if child.Is_Visible and then child /= A_Widget(this.menu) and then not this.popups.Contains( child ) then
                Draw_Child( child );
            end if;
        end loop;
        Al_Set_Clipping_Rectangle( clipX, clipY, clipW, clipH );

        -- draw the menubar widget at the top
        if this.menu /= null then
            A_Widget(this.menu).Draw;
        end if;

        -- draw the popup widgets
        -- these are drawn in front of the menubar, so they are drawn after,
        -- instead of being drawn earlier, with all the other children.
        Al_Get_Clipping_Rectangle( clipX, clipY, clipW, clipH );
        for child of this.popups loop
            Draw_Child( child );
        end loop;
        Al_Set_Clipping_Rectangle( clipX, clipY, clipW, clipH );

        Al_Restore_State( state );
    end Draw;

    ----------------------------------------------------------------------------

    overriding
    procedure Find_Widget_At( this     : access Window;
                              sx, sy   : Float;
                              children : Boolean;
                              wx, wy   : out Float;
                              found    : out A_Widget ) is
        winX     : constant Float := (sx - this.geom.x) / this.zoomFactor;
        winY     : constant Float := (sy - this.geom.y) / this.zoomFactor;
        suppress : Boolean;
    begin
        found := null;
        Widget(this.all).Find_Widget_At( winX, winY, children, wx, wy, found );
        if found /= null then
            -- if we have a modal widget and the widget we found is not a
            -- descendant of it, then forget that we found anything.
            if not found.Is_Descendant_Of( this.modal ) then
                -- however, the modal widget may push popups onto the window
                -- too (e.g. popup menus or enumeration widgets), so we need to
                -- check if 'found' is a descendant of any popups, too.
                suppress := True;
                for popup of reverse this.popups loop
                    if found.Is_Descendant_Of( popup ) then
                        -- the widget we found is a descendent of a popup on the
                        -- window's popup stack. allow it to be returned.
                        suppress := False;
                        exit;
                    end if;
                end loop;
                if suppress then
                    -- forget we found a widget because it's not a descendant of
                    -- the active modal widget, nor a descendant of any popups.
                    found := null;
                end if;
            end if;
        end if;
    end Find_Widget_At;

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Height( this : access Window ) return Float is (Float(this.minHeight));

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Width( this : access Window ) return Float is (Float(this.minWidth));

    ----------------------------------------------------------------------------

    procedure Get_Mouse_Location( this      : not null access Window'Class;
                                  requestor : not null access Widget'Class;
                                  mx, my    : out Float ) is
    begin
        requestor.Translate_To_Content( this.mouseX, this.mouseY, mx, my );
    end Get_Mouse_Location;

    ----------------------------------------------------------------------------

    procedure Give_Focus( this   : not null access Window'Class;
                          target : not null access Widget'Class ) is
        tryFocus : A_Widget := null;
        release  : A_Key_Event;
        tmp      : Boolean := False;
    begin
        -- don't change focus if the Window is being deleted- the target may
        -- have been deleted already.
        if target /= this.focus and not this.deleting then
            tryFocus := A_Widget(target);
            loop
                -- check if tryFocus accepts focus. if it can't accept and it
                -- doesn't have a parent (because it's the window) then it will
                -- get focus anyway.
                if tryFocus.Is_Focusable or else tryFocus.parent = null then
                    -- check if the new focus widget is actually different
                    if this.focus /= tryFocus then
                        -- find any keys pressed on the old focus widget and
                        -- clear their hooks so that future held and release
                        -- events of those keys will not be sent to any widget
                        -- without another key press event.
                        for i in this.keyPressSentTo'Range loop
                            if this.keyPressSentTo(i) = this.focus then
                                -- generate a Key_Release event for the widget
                                -- that is losing focus.
                                tmp := False;
                                release := Generate_Key_Release( i );
                                pragma Debug( Dbg( "Dispatching generated key release " &
                                                   A_Object(release).To_String & " to " &
                                                   this.focus.To_String,
                                                   D_INPUT, Info ) );
                                this.Dispatch_Key_Release( release, tmp );
                                Delete( A_Event(release) );
                            end if;
                        end loop;

                        -- blur the old widget and focus the new widget
                        this.focus.Set_Focused( False );
                        this.Set_Reference( this.focus, tryFocus );
                        pragma Debug( Dbg( "Focused " & this.focus.To_String, D_GUI or D_FOCUS, Info ) );
                        this.focus.Set_Focused( True );
                    end if;
                    exit;
                end if;

                -- This widget doesn't accept focus, try to give it to its "next"
                -- (if it has one that will accept focus), otherwise try giving
                -- focus to its parent. if we run out of parents, the window
                -- will have to take focus.
                if Length( tryFocus.nextId ) > 0 then
                    declare
                        next : A_Widget;
                    begin
                        next := this.Get_View.Get_Widget( To_String( tryFocus.nextId ) );
                        if next.Is_Focusable then
                            tryFocus := next;
                        else
                            -- 'next' widget doesn't accept focus
                            tryFocus := tryFocus.parent;
                        end if;
                    exception
                        when others =>
                            -- 'next' widget doesn't exist
                            tryFocus := tryFocus.parent;
                    end;
                else
                    -- no 'next' widget specified
                    tryFocus := tryFocus.parent;
                end if;
            end loop;
        end if;
    end Give_Focus;

    ----------------------------------------------------------------------------

    procedure Handle_Widget_Hidden( this       : not null access Window'Class;
                                    descendant : not null access Widget'Class ) is
    begin
        if this.mouseOver /= null and then
           this.mouseOver.Is_Descendant_Of( descendant )
        then
            -- mouse was over the widget that was hidden, or a descendant
            -- of the widget that was hidden
            pragma Debug( Dbg( "Widget " & this.mouseOver.To_String &
                               " losing hover because widget " & descendant.To_String &
                               " was hidden",
                               D_GUI, Info ) );
            this.mouseOver.Set_Hover( False );
            this.Set_Reference( this.mouseOver, null );
            this.Show_Mouse_Cursor( this.mouseOver );
        end if;
        for i in this.mousePressedOn'Range loop
            if this.mousePressedOn(i) /= null and then
               this.mousePressedOn(i).Is_Descendant_Of( descendant )
            then
                -- a mouse button was pressed on the widget that was hidden, or
                -- on a descendant of the widget that was hidden. fake a mouse
                -- button release event. handling the event will cause
                -- this.mousePressedOn(i) to be set to null.
                pragma Debug( Dbg( "Widget " & this.mousePressedOn(i).To_String &
                                   " receiving " & To_String( i ) & " mouse release " &
                                   " because widget " & descendant.To_String &
                                   " was hidden",
                                   D_GUI, Info ) );
                this.Dispatch_Mouse_Release( -1.0, -1.0, i );
            end if;
        end loop;
        for i in this.mouseClickedOn'Range loop
            if this.mouseClickedOn(i) /= null and then
               this.mouseClickedOn(i).Is_Descendant_Of( descendant )
            then
                -- a mouse button was clicked on the widget that was hidden, or
                -- on a descendant of the widget that was hidden. we don't do
                -- anything special here because widgets don't have any special
                -- state for this case.
                this.Set_Reference( this.mouseClickedOn(i), null );
            end if;
        end loop;

        if this.modal = descendant then
            -- the modal widget was hidden.
            pragma Debug( Dbg( "Modal widget " & this.modal.To_String & " hidden", D_GUI, Info ) );
            this.Set_Modal( null );
        elsif this.focus /= null and then
              this.focus.Is_Descendant_Of( descendant )
        then
            -- the widget that was hidden had focus, or a descendant of it had
            -- focus, so we give focus to the parent of the widget that was
            -- hidden because all widgets between the focused and the descendant
            -- (if any) are being hidden as well.
            pragma Debug( Dbg( "Widget " & this.focus.To_String &
                               " losing focus because widget " & descendant.To_String &
                               " was hidden; focusing widget " &
                               descendant.parent.To_String,
                               D_GUI or D_FOCUS, Info ) );
            this.Give_Focus( descendant.parent );
        end if;
    end Handle_Widget_Hidden;

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Event( this : access Window;
                            evt  : in out A_Event;
                            resp : out Response_Type ) is
        pragma Unreferenced( resp );
        handled : Boolean := False;
    begin
        case evt.Get_Event_Id is
            when EVT_KEY_PRESS =>
                this.Dispatch_Key_Press( A_Key_Event(evt), handled );

            when EVT_KEY_RELEASE =>
                this.Dispatch_Key_Release( A_Key_Event(evt), handled );

            when EVT_KEY_TYPED =>
                this.Dispatch_Key_Typed( A_Key_Typed_Event(evt), handled );

            when EVT_GAMEPAD_PRESS =>
                this.Dispatch_Gamepad_Press( A_Gamepad_Event(evt), handled );

            when EVT_GAMEPAD_RELEASE =>
                this.Dispatch_Gamepad_Release( A_Gamepad_Event(evt), handled );

            when EVT_GAMEPAD_MOVE =>
                this.Dispatch_Gamepad_Move( A_Gamepad_Event(evt), handled );

            when EVT_MOUSE_HELD =>
                this.Dispatch_Mouse_Held( A_Mouse_Button_Event(evt) );
                handled := True;

            when EVT_MOUSE_MOVE =>
                this.Dispatch_Mouse_Move( A_Mouse_Event(evt) );
                handled := True;

            when EVT_MOUSE_PRESS =>
                this.Dispatch_Mouse_Press( A_Mouse_Button_Event(evt) );
                handled := True;

            when EVT_MOUSE_RELEASE =>
                this.Dispatch_Mouse_Release( A_Mouse_Button_Event(evt) );
                handled := True;

            when EVT_MOUSE_CLICK =>
                this.Dispatch_Click( A_Mouse_Button_Event(evt) );
                handled := True;

            when EVT_MOUSE_DOUBLECLICK =>
                this.Dispatch_Doubleclick( A_Mouse_Button_Event(evt) );
                handled := True;

            when EVT_MOUSE_SCROLL =>
                this.Dispatch_Mouse_Scroll( A_Mouse_Scroll_Event(evt), handled );

            when EVT_MOUSE_LEAVE =>
                this.Handle_Mouse_Leave;
                handled := True;

            when others => null;
        end case;

        if handled then
            -- consume the input event. there should not be any other listeners.
            Delete( evt );
        end if;

        -- clean up any widgets that may have been deleted during this event
        this.view.Delete_Pending_Widgets;
    end Handle_Event;

    ----------------------------------------------------------------------------

    procedure Handle_Mouse_Leave( this : not null access Window'Class ) is
    begin
        if this.mouseOver /= null then
            this.mouseOver.Set_Hover( False );
            this.Set_Reference( this.mouseOver, null );
        end if;
        Mouse.Cursors.Manager.Get_Mouse_Cursor( this.Get_Mouse_Cursor ).Set_Display_Cursor( this.Get_View.Get_Display );
    end Handle_Mouse_Leave;

    ----------------------------------------------------------------------------

    function Has_Menubar( this : not null access Window'Class ) return Boolean is (this.menu /= null);

    ----------------------------------------------------------------------------

    function Is_Modal_Active( this : not null access Window'Class ) return Boolean is (this.modal /= A_Widget(this));

    ----------------------------------------------------------------------------

    function Is_Popup_Active( this : not null access Window'Class ) return Boolean is (not this.popups.Is_Empty);

    ----------------------------------------------------------------------------

    procedure On_Deleted( this : not null access Window'Class ) is
        source : constant A_Widget := A_Widget(this.Signaller);
        name   : constant String := "<" & source.Get_Class_Name & ", id: " & source.Get_Id & ">";
    begin
        for mb in Mouse_Widget_Array'Range loop
            if this.mousePressedOn(mb) = source then
                if source /= A_Widget(this) then
                    Dbg( "Widget " & name & " deleted while " &
                         To_String( mb ) & " Mouse pressed",
                         D_GUI, Warning );
                end if;
                this.mousePressedOn(mb) := null;
            end if;
            if this.mouseClickedOn(mb) = source then
                if source /= A_Widget(this) then
                    Dbg( "Widget " & name & " deleted while " &
                         To_String( mb ) & " Mouse clicked",
                         D_GUI, Warning );
                end if;
                this.mouseClickedOn(mb) := null;
            end if;
        end loop;

        for k in Key_Widget_Array'Range loop
            if this.keyPressSentTo(k) = source then
                if source /= A_Widget(this) then
                    Dbg( "Widget " & name & " deleted while " &
                         Al_Keycode_To_Name( k ) & " key held",
                         D_GUI, Warning );
                end if;
                this.keyPressSentTo(k) := null;
            end if;
        end loop;

        if source = this.mouseOver then
            if source /= A_Widget(this) then
                Dbg( "Widget " & name & " deleted while mouse over", D_GUI, Warning );
            end if;
            this.mouseOver := null;
            this.Show_Mouse_Cursor( this.mouseOver );
        end if;

        if source = this.focus then
            if source /= A_Widget(this) then
                Dbg( "Widget " & name & " deleted while focused", D_GUI or D_FOCUS, Warning );
            end if;
            this.focus := A_Widget(this);
            pragma Debug( Dbg( "Focused " & this.focus.To_String & " due to deletion", D_GUI or D_FOCUS, Info ) );
            this.focus.Set_Focused( True );
        end if;
    end On_Deleted;

    ----------------------------------------------------------------------------

    procedure On_Key_Escape( this : not null access Window'Class ) is
    begin
        -- if nothing else has handled the ESC key, default behavior of the
        -- window is to pop the top popup widget.
        if not this.popups.Is_Empty then
            this.Pop_Popup;
        end if;
    end On_Key_Escape;

    ----------------------------------------------------------------------------

    procedure Pop_Popup( this : not null access Window'Class ) is
        popup : A_Widget;
    begin
        if not this.popups.Is_Empty then
            -- hide then delete the top popup
            popup := this.popups.Last_Element;
            this.popups.Delete_Last;
            popup.Visible.Set( False );
            this.Remove_Child( popup );
            -- todo: decide what to do with the popup
            --Delete( A_Object(popup) );

            -- give focus to the next popup, the modal widget, or the window
            if not this.popups.Is_Empty then
                this.Give_Focus( this.popups.Last_Element );
            else
                -- if no modal dialog is active, the window will be the modal widget
                this.Give_Focus( this.modal );
            end if;
        end if;
    end Pop_Popup;

    ----------------------------------------------------------------------------

    procedure Pop_Popup( this  : not null access Window'Class;
                         popup : not null access Widget'Class ) is
    begin
        if this.popups.Contains( popup ) then
            loop
                if this.popups.Last_Element = popup then
                    this.Pop_Popup;
                    exit;
                end if;
                this.Pop_Popup;
            end loop;
        end if;
    end Pop_Popup;

    ----------------------------------------------------------------------------

    procedure Push_Popup( this  : not null access Window'Class;
                          popup : not null access Widget'Class ) is
    begin
        popup.Visible.Set( True );
        this.popups.Append( popup );
        this.Add_Widget( popup );
        this.Give_Focus( popup );
    end Push_Popup;

    ----------------------------------------------------------------------------

    procedure Remove_Widget( this  : not null access Window'Class;
                             child : not null access Widget'Class ) is
    begin
        this.Remove_Child( child );
    end Remove_Widget;

    ----------------------------------------------------------------------------

    procedure Set_Menubar( this : not null access Window'Class; menu : in out A_Menubar ) is
    begin
        if this.menu /= null then
            this.Delete_Child( A_Widget(this.menu) );
        end if;
        if menu.all in Menubar'Class then
            this.menu := menu;
            this.Add_Widget( A_Widget(menu) );
        else
            Delete( A_Widget(menu) );
        end if;
        menu := null;
    end Set_Menubar;

    ----------------------------------------------------------------------------

    procedure Set_Min_Size( this   : not null access Window'Class;
                            width  : Integer;
                            height : Integer ) is
    begin
        this.minWidth := Integer'Max( 0, width );
        this.minHeight := Integer'Max( 0, height );

        -- for a resizable window that is not maximized, apply the window size
        -- constraints now that they are known.
        if (Al_Get_Display_Flags( this.Get_View.Get_Display ) and (ALLEGRO_RESIZABLE or ALLEGRO_MAXIMIZED)) = ALLEGRO_RESIZABLE then
            Al_Set_Window_Constraints( this.Get_View.Get_Display, this.minWidth, this.minHeight, 0, 0 );
            Al_Apply_Window_Constraints( this.Get_View.Get_Display, True );
        end if;
    end Set_Min_Size;

    ----------------------------------------------------------------------------

    procedure Set_Modal( this : not null access Window'Class; modal : access Widget'Class ) is
    begin
        if this.modal /= A_Widget(this) and then this.modal.Is_Visible then
            -- if we already have a modal widget and it's visible then hide it
            -- before setting the new modal.
            this.modal.Visible.Set( False );
        end if;

        -- set the new modal widget
        if modal /= null then
            -- make it visible and give it focus
            this.modal := A_Widget(modal);
            this.Give_Focus( this.modal );
            this.modal.Bring_To_Front;
            this.modal.Visible.Set( True );
        else
            -- window is now the modal; give it focus if nothing else has it.
            -- the previous modal widget may have set the focus on hide, and we
            -- don't want to step on that.
            this.modal := A_Widget(this);
            if not this.focus.Is_Showing then
                this.Give_Focus( A_Widget(this) );
            end if;
        end if;

        -- clear the whole mouse state
        if this.mouseOver /= null then
            this.mouseOver.Set_Hover( False );
            this.Set_Reference( this.mouseOver, null );
        end if;
        for i in Mouse_Widget_Array'Range loop
            this.Set_Reference( this.mousePressedOn(i), null );
            this.Set_Reference( this.mouseClickedOn(i), null );
        end loop;
        this.Show_Mouse_Cursor( this.mouseOver );
    end Set_Modal;

    ----------------------------------------------------------------------------

    procedure Set_Reference( this      : not null access Window'Class;
                             reference : in out A_Widget;
                             widget    : A_Widget ) is

        ------------------------------------------------------------------------

        -- Check if 'widget' is currently being referenced by the Window.
        -- References include .focus, .mouseOver, .mousePressedOn(),
        -- .mouseClickedOn(), and .keyPressSentTo().
        function Is_Referencing( widget : not null A_Widget ) return Boolean is
        begin
            -- check the standard stuff: focused widget or mouse-over widget
            if this.focus = widget or this.mouseOver = widget then
                return True;
            end if;

            -- check widgets that are receiving a mouse press
            for mb in Mouse_Widget_Array'Range loop
                if this.mousePressedOn(mb) = widget or this.mouseClickedOn(mb) = widget then
                    return True;
                end if;
            end loop;

            -- check widgets that will receive a key release
            for k in Key_Widget_Array'Range loop
                if this.keyPressSentTo(k) = widget then
                    return True;
                end if;
            end loop;

            return False;
        end Is_Referencing;

        ------------------------------------------------------------------------

        previous : constant A_Widget := reference;
    begin
        -- update the reference to the new widget
        reference := widget;
        if reference /= null then
            -- connect to its Deleted signal
            reference.Deleted.Connect( Slot( this, On_Deleted'Access ) );
        end if;

        -- if the previously referenced widget is no longer referenced,
        -- disconnect from its Deleted signal
        if previous /= null and then not Is_Referencing( previous ) then
            previous.Deleted.Disconnect( Slot( this, On_Deleted'Access ) );
        end if;
    end Set_Reference;

    ----------------------------------------------------------------------------

    procedure Set_Title( this : not null access Window'Class; title : String ) is
    begin
        Al_Set_Window_Title( this.Get_View.Get_Display, title );
    end Set_Title;

    ----------------------------------------------------------------------------

    procedure Show_Mouse_Cursor( this      : not null access Window'Class;
                                 requestor : access Widget'Class ) is
    begin
        if this.mousePressedOn(Mouse_Left) = requestor or else
           (requestor = this.mouseOver and then this.mousePressedOn(Mouse_Left) = null)
        then
            Mouse.Cursors.Manager.Get_Mouse_Cursor( (if requestor /= null
                                                     then requestor.Get_Mouse_Cursor
                                                     else this.Get_Mouse_Cursor)
                                                    ).Set_Display_Cursor( this.Get_View.Get_Display );
        end if;
    end Show_Mouse_Cursor;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Window ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

end Widgets.Windows;
