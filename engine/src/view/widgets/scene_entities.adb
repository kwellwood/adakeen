--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Component_Families;                use Component_Families;
--with Debugging;                         use Debugging;
with Game_Views;                        use Game_Views;
with Values.Construction;               use Values.Construction;
with Values.Lists;                      use Values.Lists;
with Values.Strings;                    use Values.Strings;

package body Scene_Entities is

    function Create_Entity( eid          : Entity_Id;
                            name         : String;
                            template     : String;
                            initialState : Map_Value ) return A_Entity is
        this : constant A_Entity := new Entity;
    begin
        this.Construct( eid, name, template, initialState );
        return this;
    end Create_Entity;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this         : access Entity;
                         eid          : Entity_Id;
                         name         : String;
                         template     : String;
                         initialState : Map_Value ) is
        loc   : constant Map_Value := initialState.Get( Family_Name( LOCATION_ID ) ).Map;
        vis   : constant Map_Value := initialState.Get( Family_Name( VISIBLE_ID ) ).Map;
        attrs : constant Map_Value := initialState.Get( Family_Name( ATTRIBUTES_ID ) ).Map;
    begin
        Limited_Object(this.all).Construct;

        this.sigAttributeChanged.Init( this );
        this.sigMoved           .Init( this );
        this.sigResized         .Init( this );
        this.sigSelected        .Init( this );
        this.sigUnselected      .Init( this );

        this.eid := eid;
        this.name := To_Unbounded_String( name );
        this.template := To_Unbounded_String( template );
        this.attrs := Clone( attrs ).Map;
        if not this.attrs.Valid then
            this.attrs := Create_Map.Map;
        end if;

        this.x := loc.Get_Float( "x" );
        this.y := loc.Get_Float( "y" );
        this.prevX := this.x;
        this.prevY := this.y;
        this.renderX := this.x;
        this.renderY := this.y;
        this.width := loc.Get_Float( "width" );
        this.height := loc.Get_Float( "height" );
        this.z := vis.Get_Float( "z" );
        this.zOrder := vis.Get_Int( "zOrder" );
    end Construct;

    ----------------------------------------------------------------------------

    function AttributeChanged( this : not null access Entity'Class ) return access String_Signal'Class is (this.sigAttributeChanged'Access);

    function Moved( this : not null access Entity'Class ) return access Signal'Class is (this.sigMoved'Access);

    function Resized( this : not null access Entity'Class ) return access Signal'Class is (this.sigResized'Access);

    function Selected( this : not null access Entity'Class ) return access Signal'Class is (this.sigSelected'Access);

    function Unselected( this : not null access Entity'Class ) return access Signal'Class is (this.sigUnselected'Access);

    ----------------------------------------------------------------------------

    function Create_Copy( this : not null access Entity'Class ) return Value is
        attributes : Map_Value;
        anames     : List_Value;
        id         : Tagged_Id;
    begin
        attributes := Clone( this.Get_Attributes ).Map;

        -- remove connections array (if present) because it contains references
        -- to other existing entities. a new copy of an entity should not share
        -- any connections with the original entity.
        attributes.Remove( "connections" );

        -- remove entity attributes that reference other entities and components
        anames := attributes.Get_Keys.Lst;
        for i in 1..anames.Length loop
            if attributes.Get( anames.Get_String( i ) ).Is_Id then
                id := To_Tagged_Id( attributes.Get( anames.Get_String( i ) ) );
                if Is_Entity( id ) or Is_Component( id ) then
                    attributes.Remove( anames.Get_String( i ) );
                end if;
            end if;
        end loop;

        return Create_Map( (Pair( "template",   Create( this.Get_Template      ) ),
                            Pair( "x",          Create( this.Get_Entity_X      ) ),
                            Pair( "y",          Create( this.Get_Entity_Y      ) ),
                            Pair( "w",          Create( this.Get_Entity_Width  ) ),
                            Pair( "h",          Create( this.Get_Entity_Height ) ),
                            Pair( "name",       Create( this.Get_Entity_Name   ) ),
                            Pair( "attributes", attributes                     )) );
    end Create_Copy;

    ----------------------------------------------------------------------------

    procedure Freeze_Updates( this : not null access Entity'Class; freeze : Boolean ) is
    begin
        this.frozen := this.frozen + (if freeze then 1 else -1);
        if this.frozen < 0 then
            this.frozen := 0;
        end if;
    end Freeze_Updates;

    ----------------------------------------------------------------------------

    function Get_Attribute( this : not null access Entity'Class; name : String ) return Value is (this.attrs.Get( name ));

    function Get_Attributes( this : not null access Entity'Class ) return Map_Value is (this.attrs);

    function Get_Entity_Id( this : not null access Entity'Class ) return Entity_Id is (this.eid);

    function Get_Entity_Name( this : not null access Entity'Class ) return String is (To_String( this.name ));

    function Get_Entity_Height( this : not null access Entity'Class ) return Float is (this.height);

    function Get_Entity_Width( this : not null access Entity'Class ) return Float is (this.width);

    function Get_Entity_X( this : not null access Entity'Class ) return Float is (this.x);

    function Get_Entity_Y( this : not null access Entity'Class ) return Float is (this.y);

    function Get_Render_X( this : not null access Entity'Class ) return Float is (this.renderX);

    function Get_Render_Y( this : not null access Entity'Class ) return Float is (this.renderY);

    function Get_Template( this : not null access Entity'Class ) return String is (To_String( this.template ));

    function Get_Widget( this : not null access Entity'Class ) return A_Widget is (this.widget);

    function Get_Z( this : not null access Entity'Class ) return Float is (this.z);

    function Get_ZOrder( this : not null access Entity'Class ) return Integer is (this.zOrder);

    function Is_Deletable( this : not null access Entity'Class ) return Boolean is (this.deletable);

    function Is_Selected( this : not null access Entity'Class ) return Boolean is (this.isSelected);

    function Is_Updatable( this : not null access Entity'Class ) return Boolean is (this.frozen = 0);

    ----------------------------------------------------------------------------

    procedure Move_Render_Location( this   : not null access Entity'Class;
                                    dx, dy : Float ) is
    begin
        if dx /= 0.0 or dy /= 0.0 then
            this.renderX := this.renderX + dx;
            this.renderY := this.renderY + dy;
            this.sigMoved.Emit;
        end if;
    end Move_Render_Location;

    ----------------------------------------------------------------------------

    procedure Resize( this   : not null access Entity'Class;
                      width,
                      height : Float ) is
    begin
        if width /= this.width or else height /= this.height then
            this.width := width;
            this.height := height;
            this.sigResized.Emit;
        end if;
    end Resize;

    ----------------------------------------------------------------------------

    procedure Set_Attribute( this    : not null access Entity'Class;
                             name    : String;
                             val     : Value'Class;
                             consume : Boolean := False ) is
    begin
        this.attrs.Set( name, val, consume => consume );
        this.AttributeChanged.Emit( name );
    end Set_Attribute;

    ----------------------------------------------------------------------------

    procedure Set_Deletable( this : not null access Entity'Class; allow : Boolean ) is
    begin
        this.deletable := allow;
    end Set_Deletable;

    ----------------------------------------------------------------------------

    procedure Set_Entity_Location( this   : not null access Entity'Class;
                                   x, y   : Float;
                                   vx, vy : Float := 0.0 ) is
    begin
        this.prevX := this.x;
        this.prevY := this.y;
        this.x := x;
        this.y := y;

        -- disable position extrapolation if:
        --   the entity has no velocity
        --   the entity teleported
        --   the entity's rendering location is not currently updatable
        if (abs (this.x - this.prevX) >= 32.0 or abs (this.y - this.prevY) >= 32.0) or
           (vx = 0.0 and then vy = 0.0) or
           not this.Is_Updatable
        then
            this.prevX := this.x;
            this.prevY := this.y;
        end if;

        if this.Is_Updatable then
            this.renderX := this.x;
            this.renderY := this.y;
            this.sigMoved.Emit;
        end if;
    end Set_Entity_Location;

    ----------------------------------------------------------------------------

    procedure Set_Selected( this : not null access Entity'Class; selected : Boolean ) is
    begin
        if selected /= this.isSelected then
            this.isSelected := selected;
            if this.isSelected then
                this.sigSelected.Emit;
            else
                this.sigUnselected.Emit;
            end if;
        end if;
    end Set_Selected;

    ----------------------------------------------------------------------------

    procedure Set_Widget( this : not null access Entity'Class; w : access Widget'Class ) is
    begin
        pragma Assert( this.widget = null, "Widget can only be set once" );
        this.widget := A_Widget(w);
    end Set_Widget;

    ----------------------------------------------------------------------------

    procedure Set_Z( this : not null access Entity'Class; z : Float ) is
    begin
        this.z := z;
        this.widget.Set_Z( z );
    end Set_Z;

    ----------------------------------------------------------------------------

    procedure Set_ZOrder( this : not null access Entity'Class; zOrder : Integer ) is
    begin
        this.zOrder := zOrder;
    end Set_ZOrder;

    ----------------------------------------------------------------------------

    procedure Update_Render_Location( this : not null access Entity'Class ) is
        prevRenderX : constant Float := this.renderX;
        prevRenderY : constant Float := this.renderY;
    begin
        if this.Is_Updatable then
            this.renderX := this.x + (this.x - this.prevX) * this.widget.Get_View.Get_Frame_Extrapolation;
            this.renderY := this.y + (this.y - this.prevY) * this.widget.Get_View.Get_Frame_Extrapolation;

            if this.renderX /= prevRenderX or this.renderY /= prevRenderY then
                --Dbg( "Prev:" & Image( this.prevX ) & "," & Image( this.prevY ) &
                --     ", XY:" & Image( this.x ) & "," & Image( this.y ) &
                --     ", Render:" & Image( this.renderX ) & "," & Image( this.renderY ) );
                this.sigMoved.Emit;
            end if;
        end if;
    end Update_Render_Location;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Entity ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

end Scene_Entities;
