--
-- Copyright (c) 2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Rendering_Options is

    type Render_Options is mod 2**32;

    SHOW_ALL_LAYERS    : constant Render_Options := 2**0;     -- draw all layers, regardless of visibility
    SHOW_LAYER_TINT    : constant Render_Options := 2**1;     -- draw layers with given color tints
    SHOW_SPRITES       : constant Render_Options := 2**2;     -- draw sprites
    SHOW_SPRITES_FRONT : constant Render_Options := 2**3;     -- draw all sprites in front (if SHOW_SPRITES_TOP)
    SHOW_PARTICLES     : constant Render_Options := 2**4;     -- draw particles
    SHOW_LIGHTING      : constant Render_Options := 2**5;     -- draw light and shadows

    SHOW_LIGHT_BUFFER  : constant Render_Options := 2**8;     -- draw the lighting buffer as solid (if SHOW_LIGHTING)
    SHOW_LIGHT_QTREE   : constant Render_Options := 2**9;     -- draw the lighting quad tree (if SHOW_LIGHTING)
    SHOW_SHADOW_GEOM   : constant Render_Options := 2**10;    -- draw the shadow geometry (if SHOW_LIGHTING)
    SHOW_LIGHT_BLUR    : constant Render_Options := 2**11;    -- blur the lighting / shadows (doesn't work well) (if SHOW_LIGHTING)
    SHOW_LIGHT_GEOM    : constant Render_Options := 2**12;    -- draw individual light geometry (if SHOW_LIGHTING)

end Rendering_Options;
