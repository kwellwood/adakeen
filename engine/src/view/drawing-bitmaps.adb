--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.State;                     use Allegro.State;
with Allegro.Transformations;           use Allegro.Transformations;
with Drawing.Blending;                  use Drawing.Blending;

#if DEBUG then
with Allegro.Displays;                  use Allegro.Displays;
with Debugging;                         use Debugging;
#end if;

package body Drawing.Bitmaps is

    -- drawing an unaccelerated bitmap
    unaccleratedDrawWarning : Boolean := False;
    pragma Warnings( Off, unaccleratedDrawWarning );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    procedure Draw_Bitmap( bmp       : A_Allegro_Bitmap;
                           x, y      : Float;
                           z         : Float := 0.0;
                           tint      : Allegro_Color := White;
                           blendMode : Blend_Mode := Current ) is
    begin
        if bmp = null then
            return;
        end if;

        Draw_Bitmap_Region( bmp,
                            0.0, 0.0,
                            Float(Al_Get_Bitmap_Width( bmp )), Float(Al_Get_Bitmap_Height( bmp )),
                            x, y, z,
                            tint,
                            blendMode );
    end Draw_Bitmap;

    ----------------------------------------------------------------------------

    procedure Draw_Bitmap_Region( bmp       : A_Allegro_Bitmap;
                                  srcX,
                                  srcY      : Float;
                                  srcWidth,
                                  srcHeight : Float;
                                  destX,
                                  destY     : Float;
                                  destZ     : Float := 0.0;
                                  tint      : Allegro_Color := White;
                                  blendMode : Blend_Mode := Current ) is
        state  : Allegro_State;
        trans,
        backup : Allegro_Transform;
    begin
        if bmp = null then
            return;
        end if;

#if DEBUG then
        -- Notify the developer that a bitmap being drawn is not
        -- accelerated. It's likely that the bitmap was not created within
        -- the context of the currently active display. This is a
        -- development error and should be fixed immediately because
        -- unaccelerated drawing is extremely slow.
        if not Al_Is_Compatible_Bitmap( bmp ) and then not unaccleratedDrawWarning then
            Dbg( "Drawing unaccelerated bitmaps!", D_GUI, Warning );
            unaccleratedDrawWarning := True;
        end if;
#end if;

        if blendMode /= Current then
            Set_Blend_Mode( state, blendMode );
        end if;
        if destZ /= 0.0 then
            Al_Copy_Transform( backup, Al_Get_Current_Transform.all );
            Al_Identity_Transform( trans );
            Al_Translate_Transform_3d( trans, 0.0, 0.0, destZ );
            Al_Compose_Transform( trans, backup );
            Al_Use_Transform( trans );
        end if;

        Al_Draw_Tinted_Bitmap_Region( bmp,
                                      tint,
                                      srcX, srcY,
                                      srcWidth, srcHeight,
                                      destX, destY,
                                      0 );

        if destZ /= 0.0 then
            Al_Use_Transform( backup );
        end if;
        if blendMode /= Current then
            Restore_Blend_Mode( state );
        end if;
    end Draw_Bitmap_Region;

    ----------------------------------------------------------------------------

    procedure Draw_Bitmap_Stretched( bmp           : A_Allegro_Bitmap;
                                     x, y, z       : Float;
                                     width, height : Float;
                                     stretchMode   : Stretch_Mode := Stretch;
                                     rotation      : Float := 0.0;
                                     tint          : Allegro_Color := White;
                                     blendMode     : Blend_Mode := Current ) is
        state               : Allegro_State;
        srcRatio, dstRatio  : Float;
        srcWidth, srcHeight : Float;
        dstWidth, dstHeight : Float;
        dstX, dstY          : Float;
        trans,
        backup              : Allegro_Transform;
    begin
        if bmp = null or width <= 0.0 or height <= 0.0 then
            return;
        end if;

        Set_Blend_Mode( state, blendMode );

        srcWidth := Float(Al_Get_Bitmap_Width( bmp ));
        srcHeight := Float(Al_Get_Bitmap_Height( bmp ));

        case stretchMode is
            when Stretch =>
                dstWidth := width;
                dstHeight := height;
                dstX := x;
                dstY := y;

            when Fit =>
                dstRatio := width / height;
                srcRatio  := srcWidth / srcHeight;
                if srcRatio = dstRatio then
                    -- proportions match
                    dstWidth := width;
                    dstHeight := height;
                    dstX := x;
                    dstY := y;

                elsif srcRatio < dstRatio then
                    -- bmp is taller; adjust width and shift right
                    dstWidth := srcWidth * height / srcHeight;
                    dstHeight := height;
                    dstX := x + (width - dstWidth) / 2.0;
                    dstY := y;

                else
                    -- bmp is wider; adjust height and shift down
                    dstWidth := width;
                    dstHeight := srcHeight * width / srcWidth;
                    dstX := x;
                    dstY := y + (height - dstHeight) / 2.0;

                end if;

            when Fill =>
                dstRatio := width / height;
                srcRatio  := srcWidth / srcHeight;
                if srcRatio = dstRatio then
                    -- proportions match
                    dstWidth := width;
                    dstHeight := height;
                    dstX := x;
                    dstY := y;

                elsif srcRatio < dstRatio then
                    -- bmp is taller; adjust height and shift up
                    dstWidth := width;
                    dstHeight := srcHeight * width / srcWidth;
                    dstX := x;
                    dstY := y + (height - dstHeight) / 2.0;

                else
                    -- bmp is wider; adjust width and shift right
                    dstWidth := srcWidth * height / srcHeight;
                    dstHeight := height;
                    dstX := x + (width - dstWidth) / 2.0;
                    dstY := y;

                end if;

        end case;

        Al_Copy_Transform( backup, Al_Get_Current_Transform.all );
        Al_Identity_Transform( trans );
        if rotation /= 0.0 then
            -- translate the center of the source (center of the source rect)
            -- to the origin before rotation and scaling
            Al_Translate_Transform( trans, -srcWidth / 2.0, -srcHeight / 2.0 );
        end if;
        Al_Scale_Transform( trans, dstWidth / srcWidth, dstHeight / srcHeight );
        Al_Rotate_Transform( trans, rotation );
        Al_Translate_Transform_3d( trans, dstX, dstY, z );
        Al_Compose_Transform( trans, backup );
        Al_Use_Transform( trans );

        Al_Draw_Tinted_Bitmap_Region( bmp,
                                      tint,
                                      0.0, 0.0,
                                      srcWidth,
                                      srcHeight,
                                      0.0, 0.0,
                                      0 );

        Al_Use_Transform( backup );
        Restore_Blend_Mode( state );
    end Draw_Bitmap_Stretched;

    ----------------------------------------------------------------------------

    procedure Draw_Bitmap_Region_Stretched( bmp        : A_Allegro_Bitmap;
                                            srcX,
                                            srcY       : Float;
                                            srcWidth,
                                            srcHeight  : Float;
                                            destX,
                                            destY,
                                            destZ      : Float;
                                            destWidth,
                                            destHeight : Float;
                                            rotation   : Float := 0.0;
                                            tint       : Allegro_Color := White;
                                            blendMode  : Blend_Mode := Current ) is
        state  : Allegro_State;
        trans,
        backup : Allegro_Transform;
    begin
        if bmp = null or srcWidth <= 0.0 or srcHeight <= 0.0 then
            return;
        end if;

        Set_Blend_Mode( state, blendMode );

        Al_Copy_Transform( backup, Al_Get_Current_Transform.all );
        Al_Identity_Transform( trans );
        if rotation /= 0.0 then
            Al_Translate_Transform( trans, -(srcX + srcWidth / 2.0), -(srcY + srcHeight / 2.0) );
        end if;
        Al_Scale_Transform( trans, destWidth / srcWidth, destHeight / srcHeight );
        Al_Rotate_Transform( trans, rotation );
        Al_Translate_Transform_3d( trans, destX, destY, destZ );
        Al_Compose_Transform( trans, backup );
        Al_Use_Transform( trans );

        Al_Draw_Tinted_Bitmap_Region( bmp,
                                      tint,
                                      srcX, srcY,
                                      srcWidth,
                                      srcHeight,
                                      0.0, 0.0,
                                      0 );

        Al_Use_Transform( backup );
        Restore_Blend_Mode( state );
    end Draw_Bitmap_Region_Stretched;

end Drawing.Bitmaps;
