--
-- Copyright (c) 2015-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Signals.Mouse is

    procedure Connect( this : in out Mouse_Signal'Class; slot : Mouse_Connection'Class ) is
    begin
        Base_Signal(this).Connect( slot );
    end Connect;

    ----------------------------------------------------------------------------

    procedure Emit( this : Mouse_Signal'Class; x, y : Float ) is
        conns : constant Connection_Lists.List := this.conns.Copy;
    begin
        for c of conns loop
            Mouse_Connection'Class(c).Emit( this.sender,
                                            Mouse_Arguments'(x, y) );
            exit when c.priority = Exclusive;
        end loop;
    end Emit;

    --==========================================================================

    procedure Connect( this : in out Button_Signal'Class; slot : Button_Connection'Class ) is
    begin
        Base_Signal(this).Connect( slot );
    end Connect;

    ----------------------------------------------------------------------------

    procedure Emit( this      : Button_Signal'Class;
                    button    : Mouse_Button;
                    modifiers : Modifiers_Array;
                    x, y      : Float ) is
        conns   : constant Connection_Lists.List := this.conns.Copy;
        handled : Boolean := False;
    begin
        for c of conns loop
            Button_Connection'Class(c).Emit( this.sender,
                                             Button_Arguments'(x, y,
                                                               button,
                                                               modifiers),
                                             handled );
            -- Button_Connections filter by mouse button and modifiers. if
            -- 'c' is an exclusive priority that filters out the signal and
            -- and doesn't handle it, then lower priority Connections can
            -- still handle it. Example: an Exclusive priority connection to
            -- a MousePressed signal for Mouse_Right won't prevent a Normal
            -- priority connection for Mouse_Any from handling a signal when
            -- Mouse_Left is pressed.
            exit when handled and c.priority = Exclusive;
        end loop;
    end Emit;

    --==========================================================================

    procedure Connect( this : in out Scroll_Signal'Class; slot : Scroll_Connection'Class ) is
    begin
        Base_Signal(this).Connect( slot );
    end Connect;

    ----------------------------------------------------------------------------

    procedure Emit( this      : Scroll_Signal'Class;
                    x, y      : Float;
                    scrollAmt : Float;
                    handled   : out Boolean ) is
        conns : constant Connection_Lists.List := this.conns.Copy;
    begin
        -- 'handled' is used to track when any connection receives the signal so
        -- that the scroll action is not passed on to the parent widget. it
        -- will be consumed by the deepest widget that handles it.
        handled := False;

        for c of conns loop
            Scroll_Connection'Class(c).Emit( this.sender,
                                             Scroll_Arguments'(x, y,
                                                               scrollAmt),
                                             handled );
            -- Scroll_Connections don't actually filter signals right now
            -- but the Emit() prototype allows it to filter
            exit when handled;
        end loop;
    end Emit;

    --==========================================================================

    package body Connections is

        function Slot( obj      : in out Target'Class;
                       method   : not null A_Method1;
                       priority : Priority_Type := Normal ) return Mouse_Connection'Class is
            result : aliased Generic_Mouse_Connection(Proto1);
        begin
            result.Construct( priority );
            result.object := obj'Unchecked_Access;              -- UNSAFE!
            result.method1 := method;
            return result;
        end Slot;

        ------------------------------------------------------------------------

        overriding
        procedure Emit( this   : Generic_Mouse_Connection;
                        sender : access Base_Object'Class;
                        args   : Mouse_Arguments ) is
        begin
            this.object.Push_Signaller( sender );
            case this.proto is
                when Proto1 => this.method1( this.object, args );
            end case;
            this.object.Pop_Signaller;
        end Emit;

        ------------------------------------------------------------------------

        overriding
        function Eq( this : Generic_Mouse_Connection; other : Base_Connection'Class ) return Boolean is
        begin
            if other in Generic_Mouse_Connection'Class then
                return this.object = Generic_Mouse_Connection(other).object and then
                       this.proto  = Generic_Mouse_Connection(other).proto and then
                       (case this.proto is
                           when Proto1 => (this.method1 = Generic_Mouse_Connection(other).method1));
            end if;
            return False;
        end Eq;

        --======================================================================

        function Slot( obj       : in out Target'Class;
                       method    : not null A_Method0;
                       button    : Mouse_Button;
                       modifiers : Modifiers_Pattern := MODIFIERS_ANY;
                       priority  : Priority_Type := Normal ) return Button_Connection'Class is
            result : aliased Generic_Button_Connection(Proto0);
        begin
            result.Construct( priority );
            result.object := obj'Unchecked_Access;              -- UNSAFE!
            result.method0 := method;
            result.button := button;
            result.modifiers := modifiers;
            return result;
        end Slot;

        ------------------------------------------------------------------------

        function Slot( obj       : in out Target'Class;
                       method    : not null A_Method3;
                       button    : Mouse_Button := Mouse_Any;
                       modifiers : Modifiers_Pattern := MODIFIERS_ANY;
                       priority  : Priority_Type := Normal ) return Button_Connection'Class is
            result : aliased Generic_Button_Connection(Proto3);
        begin
            result.Construct( priority );
            result.object := obj'Unchecked_Access;              -- UNSAFE!
            result.method3 := method;
            result.button := button;
            result.modifiers := modifiers;
            return result;
        end Slot;

        ------------------------------------------------------------------------

        overriding
        procedure Emit( this    : Generic_Button_Connection;
                        sender  : access Base_Object'Class;
                        args    : Button_Arguments;
                        handled : out Boolean ) is
        begin
            handled := False;
            if this.modifiers = args.modifiers then
                if this.button = args.button or else
                   this.button = Mouse_Any
                then
                    handled := True;
                    this.object.Push_Signaller( sender );
                    case this.proto is
                        when Proto0 => this.method0( this.object );
                        when Proto3 => this.method3( this.object, args );
                    end case;
                    this.object.Pop_Signaller;
                end if;
            end if;
        end Emit;

        ------------------------------------------------------------------------

        overriding
        function Eq( this : Generic_Button_Connection; other : Base_Connection'Class ) return Boolean is
        begin
            if other in Generic_Button_Connection'Class then
                return this.object    = Generic_Button_Connection(other).object and then
                       this.proto     = Generic_Button_Connection(other).proto and then
                       (case this.proto is
                           when Proto0 => (this.method0 = Generic_Button_Connection(other).method0),
                           when Proto3 => (this.method3 = Generic_Button_Connection(other).method3)) and then
                       this.button    = Generic_Button_Connection(other).button and then
                       this.modifiers = Generic_Button_Connection(other).modifiers;
            end if;
            return False;
        end Eq;

        --======================================================================

        function Slot( obj      : in out Target'Class;
                       method   : not null A_Method4;
                       priority : Priority_Type := Normal ) return Scroll_Connection'Class is
            result : aliased Generic_Scroll_Connection(Proto4);
        begin
            result.Construct( priority );
            result.object := obj'Unchecked_Access;              -- UNSAFE!
            result.method4 := method;
            return result;
        end Slot;

        ------------------------------------------------------------------------

        overriding
        procedure Emit( this    : Generic_Scroll_Connection;
                        sender  : access Base_Object'Class;
                        args    : Scroll_Arguments;
                        handled : out Boolean ) is
        begin
            handled := True;
            this.object.Push_Signaller( sender );
            case this.proto is
                when Proto4 => this.method4( this.object, args );
            end case;
            this.object.Pop_Signaller;
        end Emit;

        ------------------------------------------------------------------------

        overriding
        function Eq( this : Generic_Scroll_Connection; other : Base_Connection'Class ) return Boolean is
        begin
            if other in Generic_Scroll_Connection'Class then
                return this.object = Generic_Scroll_Connection(other).object and then
                       this.proto  = Generic_Scroll_Connection(other).proto and then
                       (case this.proto is
                           when Proto4 => (this.method4 = Generic_Scroll_Connection(other).method4));
            end if;
            return False;
        end Eq;

    end Connections;

end Signals.Mouse;
