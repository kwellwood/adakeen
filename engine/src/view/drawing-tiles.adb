--
-- Copyright (c) 2015-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Drawing.Bitmaps;                   use Drawing.Bitmaps;
with Support;                           use Support;

package body Drawing.Tiles is

    procedure Draw_Tile_Filled( tile          : A_Tile;
                                x, y, z       : Float;
                                width, height : Float;
                                tint          : Allegro_Color := White;
                                blendMode     : Blend_Mode := Current ) is
        drawX        : Float := x;
        drawY        : Float := y;
        drawW        : Float := width;
        drawH        : Float := height;
        bmp          : A_Allegro_Bitmap;
        borderTop    : Float;
        borderBottom : Float;
        borderLeft   : Float;
        borderRight  : Float;
        tileX        : Float;
        tileY        : Float;
        tileWidth    : Float;
        tileHeight   : Float;
        fracWidth    : Float;
        fracHeight   : Float;
    begin
        if tile = null then
            return;
        end if;

        bmp := tile.Get_Bitmap;
        if bmp = null then
            return;
        end if;

        case tile.Get_FillMode is
            -- center the bitmap in the area, no scaling
            -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            when Center =>
                Draw_Bitmap( bmp,
                             x + (width - Float(Al_Get_Bitmap_Width( bmp ))) / 2.0,
                             y + (height - Float(Al_Get_Bitmap_Height( bmp ))) / 2.0,
                             z,
                             tint, blendMode );

            -- stretch to fill the area. if borders are defined, stretch the
            -- borders as well. top and bottom borders will be stretched
            -- horizontally. left and right borders will be stretched
            -- vertically.
            -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            when Stretch | StretchX | StretchY =>

                if tile.Get_FillMode = StretchX then
                    drawH := Float(Al_Get_Bitmap_Height( bmp ));
                    drawY := y + (height - drawH) / 2.0;
                elsif tile.Get_FillMode = StretchY then
                    drawW := Float(Al_Get_Bitmap_Width( bmp ));
                    drawX := x + (width - drawW) / 2.0;
                end if;

                borderTop := tile.Get_BorderTop;
                borderBottom := tile.Get_BorderBottom;
                borderLeft := tile.Get_BorderLeft;
                borderRight := tile.Get_BorderRight;

                -- - - - - DRAW CORNERS - - - - --
                if borderTop > 0.0 then
                    if borderLeft > 0.0 then
                        -- top left corner
                        Draw_Bitmap_Region( bmp,
                                            0.0, 0.0,
                                            borderLeft, borderTop,
                                            drawX, drawY, z,
                                            tint, blendMode );
                    end if;
                    if borderRight > 0.0 then
                        -- top right corner
                        Draw_Bitmap_Region( bmp,
                                            Float(Al_Get_Bitmap_Width( bmp )) - borderRight, 0.0,
                                            borderRight, borderTop,
                                            drawX + drawW - borderRight, drawY, z,
                                            tint, blendMode );
                    end if;
                end if;
                if borderBottom > 0.0 then
                    if borderLeft > 0.0 then
                        -- bottom left corner
                        Draw_Bitmap_Region( bmp,
                                            0.0, Float(Al_Get_Bitmap_Height( bmp )) - borderBottom,
                                            borderLeft, borderBottom,
                                            drawX, drawY + drawH - borderBottom, z,
                                            tint, blendMode );
                    end if;
                    if borderRight > 0.0 then
                        -- bottom right corner
                        Draw_Bitmap_Region( bmp,
                                            Float(Al_Get_Bitmap_Width( bmp )) - borderRight, Float(Al_Get_Bitmap_Height( bmp )) - borderBottom,
                                            borderRight, borderBottom,
                                            drawX + drawW - borderRight, drawY + drawH - borderBottom, z,
                                            tint, blendMode );
                    end if;
                end if;

                -- - - - - DRAW HORIZONTAL BORDERS - - - - --
                tileWidth := Float(Al_Get_Bitmap_Width( bmp )) - (borderLeft + borderRight);
                if tileWidth > 0.0 then
                    if borderTop > 0.0 then
                        Draw_Bitmap_Region_Stretched( bmp,
                                                      borderLeft, 0.0,
                                                      tileWidth, borderTop,
                                                      drawX + borderLeft, drawY, z,
                                                      drawW - (borderLeft + borderRight), borderTop,
                                                      0.0,
                                                      tint, blendMode );
                    end if;
                    if borderBottom > 0.0 then
                        Draw_Bitmap_Region_Stretched( bmp,
                                                      borderLeft, Float(Al_Get_Bitmap_Height( bmp )) - borderBottom,
                                                      tileWidth, borderBottom,
                                                      drawX + borderLeft, drawY + drawH - borderBottom, z,
                                                      drawW - (borderLeft + borderRight), borderBottom,
                                                      0.0,
                                                      tint, blendMode );
                    end if;
                end if;

                -- - - - - DRAW VERTICAL BORDERS - - - - --
                tileHeight := Float(Al_Get_Bitmap_Height( bmp )) - (borderTop + borderBottom);
                if tileHeight > 0.0 then
                    if borderLeft > 0.0 then
                        Draw_Bitmap_Region_Stretched( bmp,
                                                      0.0, borderTop,
                                                      borderLeft, tileHeight,
                                                      drawX, drawY + borderTop, z,
                                                      borderLeft, drawH - (borderTop + borderBottom),
                                                      0.0,
                                                      tint, blendMode );
                    end if;
                    if borderRight > 0.0 then
                        Draw_Bitmap_Region_Stretched( bmp,
                                                      Float(Al_Get_Bitmap_Width( bmp )) - borderRight, borderTop,
                                                      borderRight, tileHeight,
                                                      drawX + drawW - borderRight, drawY + borderTop, z,
                                                      borderRight, drawH - (borderTop + borderBottom),
                                                      0.0,
                                                      tint, blendMode );
                    end if;
                end if;

                -- - - - - DRAW MIDDLE CONTENT - - - - --
                if tileWidth > 0.0 and then tileHeight > 0.0 then
                    Draw_Bitmap_Region_Stretched( bmp,
                                                  borderLeft, borderTop,
                                                  tileWidth, tileHeight,
                                                  drawX + borderLeft, drawY + borderTop, z,
                                                  drawW - (borderLeft + borderRight), drawH - (borderTop + borderBottom),
                                                  0.0,
                                                  tint, blendMode );
                end if;

            -- proportionately fit to the area
            -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            when Fit =>
                Draw_Bitmap_Stretched( bmp,
                                       x, y, z,
                                       width, height,
                                       Fit,
                                       0.0,
                                       tint, blendMode );

            -- proportionately fill the area
            -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            when Fill =>
                Draw_Bitmap_Stretched( bmp,
                                       x, y, z,
                                       width, height,
                                       Fill,
                                       0.0,
                                       tint, blendMode );

            -- tile the content across the area. if borders are defined, tile
            -- the borders as well, from left to right and top to bottom.
            -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            when Tiled =>
                borderTop := tile.Get_BorderTop;
                borderBottom := tile.Get_BorderBottom;
                borderLeft := tile.Get_BorderLeft;
                borderRight := tile.Get_BorderRight;

                -- - - - - DRAW CORNERS - - - - --
                if borderTop > 0.0 then
                    if borderLeft > 0.0 then
                        -- top left corner
                        Draw_Bitmap_Region( bmp,
                                            0.0, 0.0,
                                            borderLeft, borderTop,
                                            x, y, z,
                                            tint, blendMode );
                    end if;
                    if borderRight > 0.0 then
                        -- top right corner
                        Draw_Bitmap_Region( bmp,
                                            Float(Al_Get_Bitmap_Width( bmp )) - borderRight, 0.0,
                                            borderRight, borderTop,
                                            x + width - borderRight, y, z,
                                            tint, blendMode );
                    end if;
                end if;
                if borderBottom > 0.0 then
                    if borderLeft > 0.0 then
                        -- bottom left corner
                        Draw_Bitmap_Region( bmp,
                                            0.0, Float(Al_Get_Bitmap_Height( bmp )) - borderBottom,
                                            borderLeft, borderBottom,
                                            x, y + height - borderBottom, z,
                                            tint, blendMode );
                    end if;
                    if borderRight > 0.0 then
                        -- bottom right corner
                        Draw_Bitmap_Region( bmp,
                                            Float(Al_Get_Bitmap_Width( bmp )) - borderRight, Float(Al_Get_Bitmap_Height( bmp )) - borderBottom,
                                            borderRight, borderBottom,
                                            x + width - borderRight, y + height - borderBottom, z,
                                            tint, blendMode );
                    end if;
                end if;

                -- - - - - DRAW HORIZONTAL BORDERS - - - - --
                tileWidth := Float(Al_Get_Bitmap_Width( bmp )) - (borderLeft + borderRight);
                if tileWidth > 0.0 then
                    fracWidth := Fmod( width - (borderLeft + borderRight), tileWidth );
                    -- horizontally tile the top and bottom borders
                    tileX := borderLeft;
                    while tileX + tileWidth <= width - borderRight loop
                        if borderTop > 0.0 then
                            Draw_Bitmap_Region( bmp,
                                                borderLeft, 0.0,
                                                tileWidth, borderTop,
                                                x + tileX, y, z,
                                                tint, blendMode );
                        end if;
                        if borderBottom > 0.0 then
                            Draw_Bitmap_Region( bmp,
                                                borderLeft, Float(Al_Get_Bitmap_Height( bmp )) - borderBottom,
                                                tileWidth, borderBottom,
                                                x + tileX, y + height - borderBottom, z,
                                                tint, blendMode );
                        end if;
                        tileX := tileX + tileWidth;
                    end loop;
                    -- draw the fractional tile on the right
                    if fracWidth > 0.0 then
                        if borderTop > 0.0 then
                            Draw_Bitmap_Region( bmp,
                                                borderLeft, 0.0,
                                                fracWidth, borderTop,
                                                x + tileX, y, z,
                                                tint, blendMode );
                        end if;
                        if borderBottom > 0.0 then
                            Draw_Bitmap_Region( bmp,
                                                borderLeft, Float(Al_Get_Bitmap_Height( bmp )) - borderBottom,
                                                fracWidth, borderBottom,
                                                x + tileX, y + height - borderBottom, z,
                                                tint, blendMode );
                        end if;
                    end if;
                end if;

                -- - - - - DRAW VERTICAL BORDERS - - - - --
                tileHeight := Float(Al_Get_Bitmap_Height( bmp )) - (borderTop + borderBottom);
                if tileHeight > 0.0 then
                    fracHeight := Fmod( (height - (borderTop + borderBottom)), tileHeight );
                    -- vertically tile the left and right borders
                    tileY := borderTop;
                    while tileY + tileHeight <= height - borderBottom loop
                        if borderLeft > 0.0 then
                            Draw_Bitmap_Region( bmp,
                                                0.0, borderTop,
                                                borderLeft, tileHeight,
                                                x, y + tileY, z,
                                                tint, blendMode );
                        end if;
                        if borderRight > 0.0 then
                            Draw_Bitmap_Region( bmp,
                                                Float(Al_Get_Bitmap_Width( bmp )) - borderRight, borderTop,
                                                borderRight, tileHeight,
                                                x + width - borderRight, y + tileY, z,
                                                tint, blendMode );
                        end if;
                        tileY := tileY + tileHeight;
                    end loop;
                    -- draw the fractional tile on the bottom
                    if fracHeight > 0.0 then
                        if borderLeft > 0.0 then
                            Draw_Bitmap_Region( bmp,
                                                0.0, borderTop,
                                                borderLeft, fracHeight,
                                                x, y + tileY, z,
                                                tint, blendMode );
                        end if;
                        if borderRight > 0.0 then
                            Draw_Bitmap_Region( bmp,
                                                Float(Al_Get_Bitmap_Width( bmp )) - borderRight, borderTop,
                                                borderRight, fracHeight,
                                                x + width - borderRight, y + tileY, z,
                                                tint, blendMode );
                        end if;
                    end if;
                end if;

                -- - - - - DRAW MIDDLE CONTENT - - - - --
                if tileWidth > 0.0 and then tileHeight > 0.0 then
                    -- draw whole tiles
                    tileY := borderTop;
                    while tileY + tileHeight <= height - borderBottom loop
                        tileX := borderLeft;
                        while tileX + tileWidth <= width - borderRight loop
                            Draw_Bitmap_Region( bmp,
                                                borderLeft, borderTop,
                                                tileWidth, tileHeight,
                                                x + tileX, y + tileY, z,
                                                tint, blendMode );
                            tileX := tileX + tileWidth;
                        end loop;
                        if fracWidth > 0.0 then
                            -- draw fractional width tile
                            Draw_Bitmap_Region( bmp,
                                                borderLeft, borderTop,
                                                fracWidth, tileHeight,
                                                x + tileX, y + tileY, z,
                                                tint, blendMode );
                        end if;
                        tileY := tileY + tileHeight;
                    end loop;
                    if fracHeight > 0.0 then
                        -- draw fractional height tiles
                        tileX := borderLeft;
                        while tileX + tileWidth <= width - borderRight loop
                            Draw_Bitmap_Region( bmp,
                                                borderLeft, borderTop,
                                                tileWidth, fracHeight,
                                                x + tileX, y + tileY, z,
                                                tint, blendMode );
                            tileX := tileX + tileWidth;
                        end loop;
                        if fracWidth > 0.0 then
                            -- draw fractional fractional height and fractional width tile
                            Draw_Bitmap_Region( bmp,
                                                borderLeft, borderTop,
                                                fracWidth, fracHeight,
                                                x + tileX, y + tileY, z,
                                                tint, blendMode );
                        end if;
                    end if;
                end if;

        end case;
    end Draw_Tile_Filled;

end Drawing.Tiles;
