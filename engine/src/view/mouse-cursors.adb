--
-- Copyright (c) 2013 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Mouse.Cursors is

    function Create_Mouse_Cursor( cursor : not null A_Allegro_Mouse_Cursor ) return A_Mouse_Cursor is
        this : constant A_Mouse_Cursor := new Custom_Cursor;
    begin
        Custom_Cursor(this.all).Construct( cursor );
        return this;
    end Create_Mouse_Cursor;

    ----------------------------------------------------------------------------

    function Create_Mouse_Cursor( cursor : Allegro_System_Mouse_Cursor ) return A_Mouse_Cursor is
        this : constant A_Mouse_Cursor := new System_Cursor;
    begin
        System_Cursor(this.all).Construct( cursor );
        return this;
    end Create_Mouse_Cursor;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Mouse_Cursor ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

    --==========================================================================

    not overriding
    procedure Construct( this   : access Custom_Cursor;
                         custom : not null A_Allegro_Mouse_Cursor ) is
    begin
        Mouse_Cursor(this.all).Construct;
        this.custom := custom;
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Custom_Cursor ) is
    begin
        Al_Destroy_Mouse_Cursor( this.custom );
        Mouse_Cursor(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    overriding
    procedure Set_Display_Cursor( this    : access Custom_Cursor;
                                  display : not null A_Allegro_Display ) is
    begin
        Al_Set_Mouse_Cursor( display, this.custom );
    end Set_Display_Cursor;

    --==========================================================================

    procedure Construct( this      : access System_Cursor;
                         sysCursor : Allegro_System_Mouse_Cursor ) is
    begin
        Mouse_Cursor(this.all).Construct;
        this.sysCursor := sysCursor;
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Set_Display_Cursor( this    : access System_Cursor;
                                  display : not null A_Allegro_Display ) is
    begin
        Al_Set_System_Mouse_Cursor( display, this.sysCursor );
    end Set_Display_Cursor;

end Mouse.Cursors;
