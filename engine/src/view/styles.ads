--
-- Copyright (c) 2013-2015 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Styles is

    type Align_Type is
    (
        Top_Left,    Top_Center,    Top_Right,
        Left,        Center,        Right,
        Bottom_Left, Bottom_Center, Bottom_Right
    );

    -- Returns the horizontal alignment of 'child' within 'container'. The X
    -- value returned is relative to the container's left side.
    function Align_Horizontal( alignment      : Align_Type;
                               containerWidth : Float;
                               childWidth     : Float ) return Float;

    -- Returns the alignment of rectangle 'child' within rectangle 'container'.
    -- The coordinates returned are relative to the container's top left corner.
    procedure Align_Rect( alignment                       : Align_Type;
                          containerWidth, containerHeight : Float;
                          childWidth, childHeight         : Float;
                          childX, childY                  : out Float );

    -- Returns the vertical alignment of 'child' within 'container'. The Y value
    -- returned is relative to the container's top.
    function Align_Vertical( alignment       : Align_Type;
                             containerHeight : Float;
                             childHeight     : Float ) return Float;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- 'None' will prevent any line wrapping.
    -- 'Soft' wrapping will wrap text on whitespace, if at all possible.
    -- 'Hard' wrapping will wrap text on whatever character won't fit.
    type Wrap_Mode is (None, Soft, Hard);

end Styles;
