--
-- Copyright (c) 2012 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Keyboard is

    pragma Pure;

    -- Represents special keys used to modify key presses or mouse clicks
    type Modifiers_Type is (ALT, CTRL, SHIFT, CMD);
    type Modifiers_Array is array (Modifiers_Type) of Boolean;

    -- A Modifiers_Array value with no modifiers set
    MODIFIERS_NONE : constant Modifiers_Array := Modifiers_Array'(others=>False);

    -- Returns True if all modifiers in 'l' and 'r' match exactly.
    function "="( l, r : Modifiers_Array ) return Boolean with Inline;

    -- Returns True if no modifiers are set.
    function None( mods : Modifiers_Array ) return Boolean with Inline;

    -- Returns True if only the Alt modifier is set.
    function Only_Alt( mods : Modifiers_Array ) return Boolean with Inline;

    -- Returns True if only the Ctrl modifier is set.
    function Only_Ctrl( mods : Modifiers_Array ) return Boolean with Inline;

    -- Returns True if only the Shift modifier is set.
    function Only_Shift( mods : Modifiers_Array ) return Boolean with Inline;

    -- Returns True if only the Command modifier is set.
    function Only_Cmd( mods : Modifiers_Array ) return Boolean with Inline;

    ----------------------------------------------------------------------------

    -- Represents a trinary logic value, where the third value is undefined.
    type Trillian is (Yes, No, Either);

    -- Returns True if 'l' and 'r' match or if 'r' is Either.
    function "="( l : Boolean; r : Trillian ) return Boolean with Inline;

    -- Returns True if 'l' and 'r' match or if 'l' is Either.
    function "="( l : Trillian; r : Boolean ) return Boolean with Inline;

    ----------------------------------------------------------------------------

    -- An array of trinary modifier states- on, off, either. A Modifiers_Pattern
    -- value is used to match a Modifiers_Array by setting definite values for
    -- the modifiers you want to match, and using the Either value to match
    -- anything else.
    type Modifiers_Pattern is array (Modifiers_Type) of Trillian;

    -- All modifiers with a value of Either: this will match any Modifiers_Array.
    MODIFIERS_ANY : constant Modifiers_Pattern := Modifiers_Pattern'(others=>Either);

    -- Returns True if the pattern in 'r' matches the definite state in 'l'.
    function "="( l : Modifiers_Array; r : Modifiers_Pattern ) return Boolean with Inline;

    -- Returns True if the pattern in 'l' matches the definite state in 'r'.
    function "="( l : Modifiers_Pattern; r : Modifiers_Array ) return Boolean with Inline;

    ----------------------------------------------------------------------------

    KEY_ANY           : constant Integer := 0;
    KEY_ANY_CHARACTER : constant Integer := -1;

end Keyboard;
