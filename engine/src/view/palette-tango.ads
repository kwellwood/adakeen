--
-- Copyright (c) 2014 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Palette.Tango is

    -- This is the Tango color theme
    -- http://tango.freedesktop.org/
    --
    -- Adobe Kuler
    -- http://kuler.adobe.com/

    function Butter1 return Allegro_Color is (Al_Map_RGB( 16#FC#, 16#E9#, 16#4F# )) with Inline;
    function Butter2 return Allegro_Color is (Al_Map_RGB( 16#ED#, 16#D4#, 16#00# )) with Inline;
    function Butter3 return Allegro_Color is (Al_Map_RGB( 16#C4#, 16#A0#, 16#00# )) with Inline;

    function Orange1 return Allegro_Color is (Al_Map_RGB( 16#FC#, 16#AF#, 16#3E# )) with Inline;
    function Orange2 return Allegro_Color is (Al_Map_RGB( 16#F5#, 16#79#, 16#00# )) with Inline;
    function Orange3 return Allegro_Color is (Al_Map_RGB( 16#CE#, 16#5C#, 16#00# )) with Inline;

    function Chocolate1 return Allegro_Color is (Al_Map_RGB( 16#E9#, 16#B9#, 16#6E# )) with Inline;
    function Chocolate2 return Allegro_Color is (Al_Map_RGB( 16#C1#, 16#7D#, 16#11# )) with Inline;
    function Chocolate3 return Allegro_Color is (Al_Map_RGB( 16#8F#, 16#59#, 16#02# )) with Inline;

    function Chameleon1 return Allegro_Color is (Al_Map_RGB( 16#8A#, 16#E2#, 16#34# )) with Inline;
    function Chameleon2 return Allegro_Color is (Al_Map_RGB( 16#73#, 16#D2#, 16#16# )) with Inline;
    function Chameleon3 return Allegro_Color is (Al_Map_RGB( 16#4E#, 16#9A#, 16#06# )) with Inline;

    function Skyblue1 return Allegro_Color is (Al_Map_RGB( 16#72#, 16#9F#, 16#CF# )) with Inline;
    function Skyblue2 return Allegro_Color is (Al_Map_RGB( 16#34#, 16#65#, 16#A4# )) with Inline;
    function Skyblue3 return Allegro_Color is (Al_Map_RGB( 16#20#, 16#4A#, 16#87# )) with Inline;

    function Plum1 return Allegro_Color is (Al_Map_RGB( 16#AD#, 16#7F#, 16#A8# )) with Inline;
    function Plum2 return Allegro_Color is (Al_Map_RGB( 16#75#, 16#50#, 16#7B# )) with Inline;
    function Plum3 return Allegro_Color is (Al_Map_RGB( 16#5C#, 16#35#, 16#66# )) with Inline;

    function Scarlet1 return Allegro_Color is (Al_Map_RGB( 16#EF#, 16#29#, 16#29# )) with Inline;
    function Scarlet2 return Allegro_Color is (Al_Map_RGB( 16#CC#, 16#00#, 16#00# )) with Inline;
    function Scarlet3 return Allegro_Color is (Al_Map_RGB( 16#A4#, 16#00#, 16#00# )) with Inline;

    function Chrome1 return Allegro_Color is (Al_Map_RGB( 16#EE#, 16#EE#, 16#EC# )) with Inline;
    function Chrome2 return Allegro_Color is (Al_Map_RGB( 16#D3#, 16#D7#, 16#CF# )) with Inline;
    function Chrome3 return Allegro_Color is (Al_Map_RGB( 16#BA#, 16#BD#, 16#B6# )) with Inline;
    function Chrome4 return Allegro_Color is (Al_Map_RGB( 16#88#, 16#8A#, 16#85# )) with Inline;
    function Chrome5 return Allegro_Color is (Al_Map_RGB( 16#55#, 16#57#, 16#53# )) with Inline;
    function Chrome6 return Allegro_Color is (Al_Map_RGB( 16#2E#, 16#34#, 16#36# )) with Inline;

end Palette.Tango;
