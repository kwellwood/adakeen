--
-- Copyright (c) 2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Signals.Gamepads is

    procedure Connect( this : in out Gamepad_Axis_Signal'Class; slot : Gamepad_Axis_Connection'Class ) is
    begin
        Base_Signal(this).Connect( slot );
    end Connect;

    ----------------------------------------------------------------------------

    procedure Emit( this      : Gamepad_Axis_Signal'Class;
                    gamepadId : Integer;
                    axis      : Gamepad_Axis;
                    position  : Float;
                    handled   : out Boolean ) is
        conns : constant Connection_Lists.List := this.conns.Copy;
    begin
        handled := False;
        for c of conns loop
            Gamepad_Axis_Connection'Class(c).Emit( this.sender,
                                                   Gamepad_Axis_Arguments'(gamepadId,
                                                                           axis,
                                                                           position),
                                                   handled );
            exit when handled;   -- Gamepad_Axis_Connections filter by axis
        end loop;
    end Emit;

    --==========================================================================

    procedure Connect( this : in out Gamepad_Button_Signal'Class; slot : Gamepad_Button_Connection'Class ) is
    begin
        Base_Signal(this).Connect( slot );
    end Connect;

    ----------------------------------------------------------------------------

    procedure Emit( this      : Gamepad_Button_Signal'Class;
                    gamepadId : Integer;
                    button    : Gamepad_Button;
                    handled   : out Boolean ) is
        conns : constant Connection_Lists.List := this.conns.Copy;
    begin
        handled := False;
        for c of conns loop
            Gamepad_Button_Connection'Class(c).Emit( this.sender,
                                                     Gamepad_Button_Arguments'(gamepadId,
                                                                               button),
                                                     handled );
            exit when handled;   -- Gamepad_Button_Connections filter by button
        end loop;
    end Emit;

    --==========================================================================

    package body Connections is

        function Slot( obj      : in out Target'Class;
                       method   : not null A_Axis_Method_0;
                       axis     : Gamepad_Axis;
                       priority : Priority_Type := Normal ) return Gamepad_Axis_Connection'Class is
            result : aliased Generic_Gamepad_Axis_Connection(AxisProto0);
        begin
            result.Construct( priority );
            result.object := obj'Unchecked_Access;              -- UNSAFE!
            result.method0 := method;
            result.axis := axis;
            return result;
        end Slot;

        ------------------------------------------------------------------------

        function Slot( obj      : in out Target'Class;
                       method   : not null A_Axis_Method_1;
                       axis     : Gamepad_Axis := AXIS_ANY;
                       priority : Priority_Type := Normal ) return Gamepad_Axis_Connection'Class is
            result : aliased Generic_Gamepad_Axis_Connection(AxisProto1);
        begin
            result.Construct( priority );
            result.object := obj'Unchecked_Access;              -- UNSAFE!
            result.method1 := method;
            result.axis := axis;
            return result;
        end Slot;

        ------------------------------------------------------------------------

        overriding
        procedure Emit( this    : Generic_Gamepad_Axis_Connection;
                        sender  : access Base_Object'Class;
                        args    : Gamepad_Axis_Arguments;
                        handled : out Boolean ) is
        begin
            handled := False;
            if this.axis = args.axis or this.axis = AXIS_ANY then
                handled := True;
                this.object.Push_Signaller( sender );
                case this.proto is
                    when AxisProto0 => this.method0( this.object, args.id, args.position );
                    when AxisProto1 => this.method1( this.object, args );
                end case;
                this.object.Pop_Signaller;
            end if;
        end Emit;

        ------------------------------------------------------------------------

        overriding
        function Eq( this : Generic_Gamepad_Axis_Connection; other : Base_Connection'Class ) return Boolean is
        begin
            if other in Generic_Gamepad_Axis_Connection'Class then
                return this.object = Generic_Gamepad_Axis_Connection(other).object and then
                       this.proto  = Generic_Gamepad_Axis_Connection(other).proto and then
                       (case this.proto is
                           when AxisProto0 => (this.method0 = Generic_Gamepad_Axis_Connection(other).method0),
                           when AxisProto1 => (this.method1 = Generic_Gamepad_Axis_Connection(other).method1)) and then
                       this.axis = Generic_Gamepad_Axis_Connection(other).axis;
            end if;
            return False;
        end Eq;

        --======================================================================

        function Slot( obj      : in out Target'Class;
                       method   : not null A_Button_Method_0;
                       button   : Gamepad_Button;
                       priority : Priority_Type := Normal ) return Gamepad_Button_Connection'Class is
            result : aliased Generic_Gamepad_Button_Connection(ButtonProto0);
        begin
            result.Construct( priority );
            result.object := obj'Unchecked_Access;              -- UNSAFE!
            result.method0 := method;
            result.button := button;
            return result;
        end Slot;

        ------------------------------------------------------------------------

        function Slot( obj      : in out Target'Class;
                       method   : not null A_Button_Method_1;
                       button   : Gamepad_Button;
                       priority : Priority_Type := Normal ) return Gamepad_Button_Connection'Class is
            result : aliased Generic_Gamepad_Button_Connection(ButtonProto1);
        begin
            result.Construct( priority );
            result.object := obj'Unchecked_Access;              -- UNSAFE!
            result.method1 := method;
            result.button := button;
            return result;
        end Slot;

        ------------------------------------------------------------------------

        function Slot( obj      : in out Target'Class;
                       method   : not null A_Button_Method_2;
                       button   : Gamepad_Button := BUTTON_ANY;
                       priority : Priority_Type := Normal ) return Gamepad_Button_Connection'Class is
            result : aliased Generic_Gamepad_Button_Connection(ButtonProto2);
        begin
            result.Construct( priority );
            result.object := obj'Unchecked_Access;              -- UNSAFE!
            result.method2 := method;
            result.button := button;
            return result;
        end Slot;

        ------------------------------------------------------------------------

        overriding
        procedure Emit( this    : Generic_Gamepad_Button_Connection;
                        sender  : access Base_Object'Class;
                        args    : Gamepad_Button_Arguments;
                        handled : out Boolean ) is
        begin
            handled := False;
            if this.button = args.button or this.button = BUTTON_ANY then
                handled := True;
                this.object.Push_Signaller( sender );
                case this.proto is
                    when ButtonProto0 => this.method0( this.object );
                    when ButtonProto1 => this.method1( this.object, args.id );
                    when ButtonProto2 => this.method2( this.object, args );
                end case;
                this.object.Pop_Signaller;
            end if;
        end Emit;

        ------------------------------------------------------------------------

        overriding
        function Eq( this : Generic_Gamepad_Button_Connection; other : Base_Connection'Class ) return Boolean is
        begin
            if other in Generic_Gamepad_Button_Connection'Class then
                return this.object = Generic_Gamepad_Button_Connection(other).object and then
                       this.proto  = Generic_Gamepad_Button_Connection(other).proto and then
                       (case this.proto is
                           when ButtonProto0 => (this.method0 = Generic_Gamepad_Button_Connection(other).method0),
                           when ButtonProto1 => (this.method1 = Generic_Gamepad_Button_Connection(other).method1),
                           when ButtonProto2 => (this.method2 = Generic_Gamepad_Button_Connection(other).method2)) and then
                       this.button = Generic_Gamepad_Button_Connection(other).button;
            end if;
            return False;
        end Eq;

    end Connections;

end Signals.Gamepads;
