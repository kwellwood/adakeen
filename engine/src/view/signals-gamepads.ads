--
-- Copyright (c) 2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Gamepads;                          use Gamepads;

package Signals.Gamepads is

    ----------------------------------------------------------------------------
    -- Gamepad_Axis_Connection Class --

    type Gamepad_Axis_Arguments is
        record
            id       : Integer := 0;                  -- gamepad id
            axis     : Gamepad_Axis := AXIS_NONE;     -- axis that changed
            position : Float := 0.0;                  -- position on the axis (-1.0 to +1.0)
        end record;

    type Gamepad_Axis_Connection is abstract new Base_Connection with
        record
            axis : Gamepad_Axis := AXIS_NONE;               -- <0 = any axis
        end record;

    not overriding
    procedure Emit( this    : Gamepad_Axis_Connection;
                    sender  : access Base_Object'Class;
                    args    : Gamepad_Axis_Arguments;
                    handled : out Boolean ) is abstract;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Gamepad_Axis_Signal Class --

    type Gamepad_Axis_Signal is new Base_Signal with private;

    procedure Emit( this      : Gamepad_Axis_Signal'Class;
                    gamepadId : Integer;
                    axis      : Gamepad_Axis;
                    position  : Float;
                    handled   : out Boolean );

    procedure Connect( this : in out Gamepad_Axis_Signal'Class; slot : Gamepad_Axis_Connection'Class );

    ----------------------------------------------------------------------------
    -- Gamepad_Button_Connection Class --

    type Gamepad_Button_Arguments is
        record
            id     : Integer := 0;             -- gamepad id
            button : Gamepad_Button := BUTTON_NONE;
        end record;

    type Gamepad_Button_Connection is abstract new Base_Connection with
        record
            button : Gamepad_Button := BUTTON_NONE;         -- <0 = any button
        end record;

    not overriding
    procedure Emit( this    : Gamepad_Button_Connection;
                    sender  : access Base_Object'Class;
                    args    : Gamepad_Button_Arguments;
                    handled : out Boolean ) is abstract;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Gamepad_Button_Signal Class --

    type Gamepad_Button_Signal is new Base_Signal with private;

    procedure Emit( this      : Gamepad_Button_Signal'Class;
                    gamepadId : Integer;
                    button    : Gamepad_Button;
                    handled   : out Boolean );

    procedure Connect( this : in out Gamepad_Button_Signal'Class; slot : Gamepad_Button_Connection'Class );

    ----------------------------------------------------------------------------
    -- Signals.Gamepads.Connections --

    generic
        type Target (<>) is abstract limited new Base_Object with private;
    package Connections is

        ------------------------------------------------------------------------
        -- Gamepad_Axis_Connection

        type Axis_Prototype is (AxisProto0, AxisProto1);

        type A_Axis_Method_0 is access
            procedure( object    : not null access Target'Class;
                       gamepadId : Integer;
                       position  : Float );

        type A_Axis_Method_1 is access
            procedure( object  : not null access Target'Class;
                       gamepad : Gamepad_Axis_Arguments );

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        function Slot( obj      : in out Target'Class;
                       method   : not null A_Axis_Method_0;
                       axis     : Gamepad_Axis;
                       priority : Priority_Type := Normal ) return Gamepad_Axis_Connection'Class;

        function Slot( obj      : not null access Target'Class;
                       method   : not null A_Axis_Method_0;
                       axis     : Gamepad_Axis;
                       priority : Priority_Type := Normal ) return Gamepad_Axis_Connection'Class is (Slot( obj.all, method, axis, priority ));

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        function Slot( obj      : in out Target'Class;
                       method   : not null A_Axis_Method_1;
                       axis     : Gamepad_Axis := AXIS_ANY;
                       priority : Priority_Type := Normal ) return Gamepad_Axis_Connection'Class;

        function Slot( obj      : not null access Target'Class;
                       method   : not null A_Axis_Method_1;
                       axis     : Gamepad_Axis := AXIS_ANY;
                       priority : Priority_Type := Normal ) return Gamepad_Axis_Connection'Class is (Slot( obj.all, method, axis, priority ));

        ------------------------------------------------------------------------
        -- Gamepad_Button_Connection

        type Button_Prototype is (ButtonProto0, ButtonProto1, ButtonProto2);

        type A_Button_Method_0 is access
            procedure( object : not null access Target'Class );

        type A_Button_Method_1 is access
            procedure( object    : not null access Target'Class;
                       gamepadId : Integer );

        type A_Button_Method_2 is access
            procedure( object  : not null access Target'Class;
                       gamepad : Gamepad_Button_Arguments );

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        function Slot( obj      : in out Target'Class;
                       method   : not null A_Button_Method_0;
                       button   : Gamepad_Button;
                       priority : Priority_Type := Normal ) return Gamepad_Button_Connection'Class;

        function Slot( obj      : not null access Target'Class;
                       method   : not null A_Button_Method_0;
                       button   : Gamepad_Button;
                       priority : Priority_Type := Normal ) return Gamepad_Button_Connection'Class is (Slot( obj.all, method, button, priority ));

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        function Slot( obj      : in out Target'Class;
                       method   : not null A_Button_Method_1;
                       button   : Gamepad_Button;
                       priority : Priority_Type := Normal ) return Gamepad_Button_Connection'Class;

        function Slot( obj      : not null access Target'Class;
                       method   : not null A_Button_Method_1;
                       button   : Gamepad_Button;
                       priority : Priority_Type := Normal ) return Gamepad_Button_Connection'Class is (Slot( obj.all, method, button, priority ));

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        function Slot( obj      : in out Target'Class;
                       method   : not null A_Button_Method_2;
                       button   : Gamepad_Button := BUTTON_ANY;
                       priority : Priority_Type := Normal ) return Gamepad_Button_Connection'Class;

        function Slot( obj      : not null access Target'Class;
                       method   : not null A_Button_Method_2;
                       button   : Gamepad_Button := BUTTON_ANY;
                       priority : Priority_Type := Normal ) return Gamepad_Button_Connection'Class is (Slot( obj.all, method, button, priority ));

    private

        type Generic_Gamepad_Axis_Connection(proto : Axis_Prototype) is new Gamepad_Axis_Connection with
            record
                object : access Target'Class := null;
                case proto is
                    when AxisProto0 => method0 : A_Axis_Method_0 := null;
                    when AxisProto1 => method1 : A_Axis_Method_1 := null;
                end case;
            end record;

        overriding
        procedure Emit( this    : Generic_Gamepad_Axis_Connection;
                        sender  : access Base_Object'Class;
                        args    : Gamepad_Axis_Arguments;
                        handled : out Boolean );

        overriding
        function Eq( this : Generic_Gamepad_Axis_Connection; other : Base_Connection'Class ) return Boolean;

        overriding
        function Get_Object( this : Generic_Gamepad_Axis_Connection ) return access Base_Object'Class is (this.object);

        ------------------------------------------------------------------------

        type Generic_Gamepad_Button_Connection(proto : Button_Prototype) is new Gamepad_Button_Connection with
            record
                object : access Target'Class := null;
                case proto is
                    when ButtonProto0 => method0 : A_Button_Method_0 := null;
                    when ButtonProto1 => method1 : A_Button_Method_1 := null;
                    when ButtonProto2 => method2 : A_Button_Method_2 := null;
                end case;
            end record;

        overriding
        procedure Emit( this    : Generic_Gamepad_Button_Connection;
                        sender  : access Base_Object'Class;
                        args    : Gamepad_Button_Arguments;
                        handled : out Boolean );

        overriding
        function Eq( this : Generic_Gamepad_Button_Connection; other : Base_Connection'Class ) return Boolean;

        overriding
        function Get_Object( this : Generic_Gamepad_Button_Connection ) return access Base_Object'Class is (this.object);

    end Connections;

private

    type Gamepad_Axis_Signal is new Signal with null record;

    type Gamepad_Button_Signal is new Signal with null record;

end Signals.Gamepads;
