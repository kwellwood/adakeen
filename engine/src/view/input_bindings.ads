--
-- Copyright (c) 2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Ordered_Maps;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Gamepads;                          use Gamepads;
with Keyboard;                          use Keyboard;

package Input_Bindings is

    -- This type describes a unique input semantic (e.g. "Jump" or "Shoot")
    type Input_Action is new Integer;

    type Input_Actions is array (Positive range <>) of Input_Action;

    -- Describes the key and game controller inputs bound to an input action.
    type Input_Binding is
        record
            keycode : Integer := KEY_ANY;
            axis    : Gamepad_Axis := AXIS_NONE;
            button  : Gamepad_Button := BUTTON_NONE;
        end record;

    -- Encapsulates a binding and descriptive meta-info.
    type Binding_Info is
        record
            action  : Input_Action := -1;
            name    : Unbounded_String;
            binding : Input_Binding;
        end record;

    -- Maps semantic input actions to bindings, for use by the class that
    -- manages input bindings.
    package Binding_Maps is new Ada.Containers.Ordered_Maps( Input_Action, Binding_Info, "<", "=" );

end Input_Bindings;
