--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Doubly_Linked_Lists;
with Ada.Containers.Indefinite_Hashed_Maps;
with Ada.Real_Time;                     use Ada.Real_Time;
with Ada.Strings.Hash_Case_Insensitive;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Audio_Players;                     use Audio_Players;
with Allegro.Displays;                  use Allegro.Displays;
with Events;                            use Events;
with Events.Corrals;                    use Events.Corrals;
with Events.Listeners;                  use Events.Listeners;
with Input_Bindings;                    use Input_Bindings;
with Input_Handlers;                    use Input_Handlers;
with Objects;                           use Objects;
with Processes;                         use Processes;
with Processes.Managers;                use Processes.Managers;
with Renderers;                         use Renderers;
with Signals;                           use Signals;
with Values;                            use Values;
with Values.Lists;                      use Values.Lists;
with Widgets;                           use Widgets;
with Widgets.Windows;                   use Widgets.Windows;

package Game_Views is

    -- The Game_View object is the root object in the View system, which is
    -- responsible for managing all user interactions with the game. Input
    -- handling, UI widgets, rendering, and audio are all subsystems managed by
    -- the Game_View class.
    --
    -- The entire view system runs within the context of a Process_Manager
    -- provided by the Game_View instance. The Game_View itself executes as a
    -- Process and Event_Listener.
    type Game_View is abstract new Limited_Object and
                                   Event_Listener and
                                   Process with private;
    type A_Game_View is access all Game_View'Class;
    pragma No_Strict_Aliasing( A_Game_View );

    -- Deletes the Game_View.
    procedure Delete( this : in out A_Game_View );
    pragma Postcondition( this = null );

    -- Initializes the view with its display, creating its renderer, widgets, etc.
    -- The Initializing signal will be emitted to allow the application to build
    -- the GUI widgets.
    procedure Initialize( this    : not null access Game_View'Class;
                          display : not null A_Allegro_Display );

    -- Performs all finalization before shutdown, emitting the Finalizing the
    -- signal. This must be called just before deleting the object.
    procedure Finalize( this : not null access Game_View'Class );

    -- Runs the game view. This procedure only returns after Stop() is called.
    procedure Run( this : not null access Game_View'Class );

    -- Stops running the game view, causing Run() to return.
    procedure Stop( this : not null access Game_View'Class );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Signals

    function Initializing( this : not null access Game_View'Class ) return access Signal'Class;
    function BindingsChanged( this : not null access Game_View'Class ) return access Signal'Class;
    function Finalizing( this : not null access Game_View'Class ) return access Signal'Class;
    function Quitting( this : not null access Game_View'Class ) return access Signal'Class;

    function GameStarted( this : not null access Game_View'Class ) return access Signal'Class;
    function LoadingBegan( this : not null access Game_View'Class ) return access Signal'Class;
    function LoadingEnded( this : not null access Game_View'Class ) return access Signal'Class;
    function LoadingFailed( this : not null access Game_View'Class ) return access Signal'Class;
    function PausedChanged( this : not null access Game_View'Class ) return access Signal'Class;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Queues an event to restart the game session from the beginning. The game
    -- in progress will be discarded.
    procedure Start_Game( this : not null access Game_View'Class );

    -- Queues an event to load a saved game session 'filename' and resume play.
    -- The game in progress will be discarded.
    procedure Load_Game( this : not null access Game_View'Class; filename : String );

    -- Queues an event to asynchronously pause game play.
    --
    -- The view will be notified of the change in state later, then emit the
    -- .PausedChanged signal.
    procedure Pause_Game( this : not null access Game_View'Class );

    -- Queues an event to asynchronously resume game play.
    --
    -- The view will be notified of the change in state later, then emit the
    -- .PausedChanged signal.
    procedure Resume_Game( this : not null access Game_View'Class );

    -- Queues and event to asynchronously save the current game session to
    -- 'filename' on disk for later. Game play will not change state.
    procedure Save_Game( this : not null access Game_View'Class; filename : String );

    -- Begins exiting the application, emitting the Queueing signal and then
    -- queueing an event to shutdown the Game_View thread on the next frame.
    -- Call this to exit the application.
    procedure Quit( this : not null access Game_View'Class );

    -- Returns True if gameplay is paused.
    function Is_Paused( this : not null access Game_View'Class ) return Boolean;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Attaches a Process to the view's Process_Manager.
    procedure Attach( this : not null access Game_View'Class;
                      proc : not null access Process'Class );

    -- Detaches a Process from the view's Process_Manager.
    procedure Detach( this : not null access Game_View'Class;
                      proc : not null access Process'Class );

    -- Detaches a Process from the view's Process_Manager.
    procedure Detach( this : not null access Game_View'Class;
                      proc : Process'Class );

    -- Returns an access to view's Corral for registering/unregistering for
    -- event notifications.
    function Get_Corral( this : not null access Game_View'Class ) return A_Corral;
    pragma Postcondition( Get_Corral'Result /= null );

    -- Returns the view's process manager.
    function Get_Process_Manager( this : not null access Game_View'Class ) return A_Process_Manager;
    pragma Postcondition( Get_Process_Manager'Result /= null );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Returns the view's display.
    function Get_Display( this : not null access Game_View'Class ) return A_Allegro_Display;

    -- Returns the rendering position extrapolation factor. This is used to
    -- extrapolate what the current locations of entities are likely to be,
    -- based on their motion between the last two game logic frames.
    --
    -- X(now) = X(curr) + (X(curr) - X(prev)) * extrapolation;
    --
    -- Entity positions can be extrapolated to produce visually smooth movement
    -- even when the game and view frame loops are not completely phase locked.
    function Get_Frame_Extrapolation( this : not null access Game_View'Class ) return Float;

    -- Returns the error generated by the most recent load failure, or an empty
    -- string if the most recent load was successful.
    function Get_Load_Error( this : not null access Game_View'Class ) return String;

    -- Returns the view's Window widget. This will return null if the window has
    -- not yet been set.
    function Get_Window( this : not null access Game_View'Class ) return A_Window;

    -- Begins using shader 'name' for drawing to the target bitmap. Reverts to
    -- the default shader if the shader can't be loaded.
    procedure Use_Shader( this : not null access Game_View'Class; name : String );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Returns the view's input handler.
    function Get_Input_Handler( this : not null access Game_View'Class ) return A_Input_Handler;

    -- Defines a new semantic input action with a default input binding.
    procedure Define_Binding( this     : not null access Game_View'Class;
                              action   : Input_Action;
                              name     : String;
                              defaults : Input_Binding );

    -- Returns the keybord/gamepad button inputs bound to input action 'action'.
    function Get_Binding( this   : not null access Game_View'Class;
                          action : Input_Action ) return Input_Binding;

    -- Returns the name for defined input action 'action'. An empty string will
    -- be returned if the action has not been defined.
    function Get_Binding_Name( this   : not null access Game_View'Class;
                               action : Input_Action ) return String;

    -- Sets the keyboard/gamepad button inputs bound to input action 'action'
    -- and emits the BindingsChanged signal, to notify listeners. If an input is
    -- currently bound to another action, it will be changed to 'action',
    -- exclusively. If 'action' has not been defined, nothing will happen. Input
    -- bindings will be automatically saved to disk on change.
    procedure Set_Binding( this    : not null access Game_View'Class;
                           action  : Input_Action;
                           binding : Input_Binding );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Returns the contents of the clipboard if its format matches 'format'.
    -- If it does not match, Null will be returned.
    function Get_Clipboard( this   : not null access Game_View'Class;
                            format : String ) return Value'Class;

    -- Returns the format of the clipboard's contents.
    function Get_Clipboard_Format( this : not null access Game_View'Class ) return String;

    -- Sets the clipboard contents to 'val' of type 'format'.
    procedure Set_Clipboard( this   : not null access Game_View'Class;
                             format : String;
                             val    : Value'Class );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Returns a widget in the registry by id. Raises exception ID_NOT_FOUND if
    -- the widget does not exist.
    function Get_Widget( this : not null access Game_View'Class;
                         id   : String ) return A_Widget;
    pragma Postcondition( Get_Widget'Result /= null );

    -- Adds a widget to the view's widget registry. Raises exception
    -- DUPLICATE_ID if a widget with the same id is already registered.
    procedure Register( this  : not null access Game_View'Class;
                        widgt : not null access Widget'Class );

    -- Removes a widget from the view's registry. If the widget is not
    -- registered, nothing happens.
    procedure Unregister( this : not null access Game_View'Class; id : String );
    pragma Precondition( id'Length > 0 );

    -- Deletes all widgets scheduled for deletion with Schedule_Widget_Delete(),
    -- including their children.
    procedure Delete_Pending_Widgets( this : not null access Game_View'Class );

    -- Schedules a widget 'widgt' for deletion by adding it to a pending queue.
    -- The widget and its children must have been unregistered already. All
    -- widgets scheduled for deletion (and any children they may have) will be
    -- deleted when Delete_Pending_Widgets is called. 'widgt' will be consumed.
    -- Neither it, nor any of its children, may be referenced from anywhere
    -- else.
    procedure Schedule_Widget_Delete( this  : not null access Game_View'Class;
                                      widgt : in out A_Widget );

    ----------------------------------------------------------------------------

    -- raised on attempt to register a widget with a duplicate id
    DUPLICATE_ID : exception;

    -- raised on attempt to access an registered widget by id
    ID_NOT_FOUND : exception;

private

    -- A Widget_Registry maps widget ids to widget references. The registry will
    -- contain references to all created widgets, whether they are parented or
    -- not.
    package Widget_Registry is new Ada.Containers.Indefinite_Hashed_Maps(
        String, A_Widget, Ada.Strings.Hash_Case_Insensitive, "=", "=" );
    use Widget_Registry;

    package Widget_Lists is new Ada.Containers.Doubly_Linked_Lists( A_Widget, "=" );
    use Widget_Lists;

    -----------------------------------------------------------------------------

    type Game_View is abstract new Limited_Object and
                                   Event_Listener and
                                   Process with
        record
            display          : A_Allegro_Display := null;
            maxFps           : Natural := 60;

            initialWidth,
            initialHeight    : Integer := 0;
            menubarHeight    : Integer := 0;

            win              : A_Window := null;
            widgets          : Widget_Registry.Map;
            deletedWidgets   : Widget_Lists.List;   -- scheduled for deletion

            pman             : A_Process_Manager := null;
            renderer         : A_Renderer := null;
            audioPlayer      : A_Audio_Player := null;
            inhandler        : A_Input_Handler := null;
            bindings         : Binding_Maps.Map;

            frames           : Integer := 0;                 -- total number of view games
            frameSyncOn      : Boolean := False;             -- frame synchronization active? (debugging only)
            frameStart       : Time := Time_First;           -- start time of current frame (real time)
            frameExtrap      : Float := 0.0;                 -- frame extrapolation factor
            prevFrameCount   : Integer := 0;                 -- last game frame count
            prevFrameEnd     : Time := Time_First;           -- end time of last game frame (real time)
            prevFrameElapsed : Time_Span := Time_Span_Zero;  -- duration of last game frame (real time)

            pauseCounts      : Integer := 0;        -- pause attempt count (like a counting semaphore)
            paused           : Boolean := False;    -- game is paused?

            clipboardFormat  : Unbounded_String;    -- format of the clipboard
            clipboard        : Value;               -- contents of the clipboard

            loadError        : Unbounded_String;    -- error message of most recent load

            sigInitializing    : aliased Signal;
            sigBindingsChanged : aliased Signal;
            sigFinalizing      : aliased Signal;
            sigQuitting        : aliased Signal;

            sigGameStarted     : aliased Signal;
            sigLoadingBegan    : aliased Signal;
            sigLoadingEnded    : aliased Signal;
            sigLoadingFailed   : aliased Signal;
            sigPausedChanged   : aliased Signal;
        end record;

    -- 'maxFps' is the maximum frame rate at which the view thread will run, or
    -- zero for no frame limit.
    procedure Construct( this : access Game_View; maxFps : Natural );

    procedure Delete( this : in out Game_View );

    overriding
    function Get_Process_Name( this : access Game_View ) return String is ("Game View");

    -- Handles all events that the Game_View is registered to receive.
    procedure Handle_Event( this : access Game_View;
                            evt  : in out A_Event;
                            resp : out Response_Type );
    pragma Precondition( evt /= null );

    -- Called when a request to close the window is received by the Game_View.
    -- On_Close_Window() should be overridden if some action, like showing a
    -- confirmation dialog, should be performed instead. Set 'allowed' to False
    -- prevent the application from exiting.
    procedure On_Close_Window( this    : access Game_View;
                               allowed : in out Boolean ) is null;

    -- Called when an EVT_VIEW_MESSAGE event is received by the Game_View. Each
    -- message event with the same name will have the same number and types of
    -- arguments in 'args'. On_Message() should be overridden by the subclass to
    -- examine messages and act appropriately.
    procedure On_Message( this : access Game_View;
                          name : String;
                          args : List_Value'Class ) is null;

    -- Executes the Game_View logic. 'time' is the amount of time elapsed since
    -- the previous tick. Overriding implementations must call this first.
    procedure Tick( this : access Game_View; time : Tick_Time );

end Game_Views;
