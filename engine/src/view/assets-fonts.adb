--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Ada.Containers.Indefinite_Ordered_Maps;
with Allegro.Fonts.TTF;                 use Allegro.Fonts.TTF;
with Allegro.State;                     use Allegro.State;
with Debugging;                         use Debugging;
with Drawing;                           use Drawing;
with Drawing.Blending;                  use Drawing.Blending;
with Interfaces;                        use Interfaces;
with Support.Paths;                     use Support.Paths;
with Values.Construction;               use Values.Construction;
with Values.Lists;                      use Values.Lists;
with Values.Strings;                    use Values.Strings;

package body Assets.Fonts is

    FONT_EXTENSION : constant String := "ttf";

    -- Maps a font name to a file path.
    package Font_Paths is new Ada.Containers.Indefinite_Ordered_Maps( String, String, "<", "=" );
    use Font_Paths;

    aliases : Font_Paths.Map;

    initialized : Boolean := False;
    finalized   : Boolean := False;

    --==========================================================================

    function Initialize_Fonts return Boolean is
    begin
        if not initialized then
            pragma Debug( Dbg( "Initializing font system", D_FONT, Info ) );
            if not Al_Init_Font_Addon then
                Dbg( "Allegro font addon failed to initialize", D_FONT, Error );
                return False;
            elsif not Al_Init_TTF_Addon then
                Dbg( "Allegro TTF addon failed to initialize", D_FONT, Error );
                return False;
            end if;
            initialized := True;
        end if;
        return True;
    end Initialize_Fonts;

    ----------------------------------------------------------------------------

    procedure Finalize_Fonts is
    begin
        if initialized and then not finalized then
            pragma Debug( Dbg( "Finalizing font system", D_FONT, Info ) );
            Al_Shutdown_TTF_Addon;
            Al_Shutdown_Font_Addon;
            finalized := True;
        end if;
    end Finalize_Fonts;

    ----------------------------------------------------------------------------

    procedure Register_Font_Name( name : String; filePath : String ) is
        lname : constant String := To_Lower( name );
    begin
        if aliases.Contains( lname ) then
            Dbg( "Overriding font alias '" & lname & "' with '" & filePath & "'", D_FONT, Warning );
            aliases.Delete( lname );
        end if;
        aliases.Insert( lname, Add_Extension( filePath, FONT_EXTENSION ) );
    end Register_Font_Name;

    --==========================================================================

    function Load_Font( filePath : String; size : Positive ) return A_Font is
        lpath   : constant String := Add_Extension( filePath, FONT_EXTENSION );
        path    : Unbounded_String;
        pos     : Font_Paths.Cursor;
        assetId : Value;
        asset   : A_Asset;
        this    : A_Font;
    begin
        pos := aliases.Find( To_Lower( filePath ) );
        if Has_Element( pos ) then
            -- use the alias instead
            -- it's already lowercase and has a file extension
            path := To_Unbounded_String( Element( pos ) );
        else
            path := To_Unbounded_String( lpath );
        end if;

        assetId := Create_List( (1 => Create( path ), 2 => Create( size )) );

        asset := Find_Asset( assetId );
        if asset /= null then
            -- found in cache
            pragma Assert( asset.Get_Type = Font_Assets,
                           "Cached asset '" & filePath & "' is not an archive" );
            this := A_Font(asset);
        else
            -- create a new asset
            this := new Font;
            this.Construct( assetId, To_String( path ), size );
            Store_Asset( A_Asset(this) );
        end if;
        return this;
    end Load_Font;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this     : access Font;
                         assetId  : Value'Class;
                         filePath : String;
                         size     : Positive ) is
    begin
        Asset(this.all).Construct( Font_Assets, assetId, filePath, "resources" );
        this.size := size;
    end Construct;

    ----------------------------------------------------------------------------

    procedure Draw_String( this  : not null access Font'Class;
                           str   : String;
                           x, y  : Float;
                           color : Allegro_Color ) is
        state : Allegro_State;
    begin
        if this.loaded then
            if not this.drawn then
                -- if this font has not been drawn yet, load it again on this thread
                -- that is doing the drawing. this works around a limitation in
                -- Allegro 5.0 that only supports image loading on the rendering
                -- thread. It should be fixed by Allegro 5.1 in the future.
                Al_Destroy_Font( this.alFont );
                this.alFont := Al_Load_TTF_Font_f( this.res.Create_Allegro_File,    -- consumes 'file' argument
                                                   this.res.Get_Filename,
                                                   this.size,
                                                   0 );
                this.drawn := True;
            end if;
            Set_Blend_Mode( state, Normal );
            Al_Draw_Text( this.alFont,
                          color,
                          x, y,
                          ALLEGRO_ALIGN_LEFT or ALLEGRO_ALIGN_INTEGER,
                          str );
            Restore_Blend_Mode( state );
        end if;
    end Draw_String;

    ----------------------------------------------------------------------------

    function Ascent( this : not null access Font'Class ) return Natural is
    begin
        if this.loaded then
            return Al_Get_Font_Ascent( this.alFont );
        end if;
        return 10;
    end Ascent;

    ----------------------------------------------------------------------------

    function Descent( this : not null access Font'Class ) return Natural is
    begin
        if this.loaded then
            return Al_Get_Font_Descent( this.alFont );
        end if;
        return 0;
    end Descent;

    ----------------------------------------------------------------------------

    function Line_Height( this : not null access Font'Class ) return Natural is
    begin
        if this.loaded then
            return Al_Get_Font_Line_Height( this.alFont );
        end if;
        return 10;
    end Line_Height;

    ----------------------------------------------------------------------------

    overriding
    function Load_Data( this : access Font ) return Boolean is
    begin
        this.res := Load_Resource( To_String( this.filePath ), To_String( this.group ) );
        if this.res /= null then
            -- load the font initially, to verify it can be loaded properly. the
            -- font must be loaded on the drawing thread, but this might not be
            -- it. so, the font will be soft-reloaded before drawing it the
            -- first time to ensure it is loaded on the drawing thread. This is
            -- due to a limitation in Allegro 5.0
            this.alFont := Al_Load_TTF_Font_f( this.res.Create_Allegro_File,    -- consumes 'file' argument
                                               this.res.Get_Filename,
                                               this.size,
                                               0 );
            if this.alFont = null then
                Delete( this.res );
            end if;
        end if;

        return this.alFont /= null;
    end Load_Data;

    ----------------------------------------------------------------------------

    function Text_Length( this : not null access Font'Class;
                          str  : String ) return Natural is
    begin
        if this.loaded then
            if str'Length > 0 then
                return Al_Get_Text_Width( this.alFont, str );
            end if;
            return 0;
        end if;
        return str'Length;
    end Text_Length;

    ----------------------------------------------------------------------------

    overriding
    procedure Unload_Data( this : access Font ) is
    begin
        Al_Destroy_Font( this.alFont );
        Delete( this.res );
    end Unload_Data;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Font ) is
    begin
        Delete( A_Asset(this) );
    end Delete;

end Assets.Fonts;
