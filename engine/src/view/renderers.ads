--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Maps;
with Objects;                           use Objects;
with Processes;                         use Processes;
with Widgets;                           use Widgets;

package Renderers is

    -- A Renderer is a member of the Game View system and is responsible for
    -- drawing a Window widget to the screen. Only one renderer exists per
    -- application. It implements the Process interface for execution. The
    -- renderer uses double-buffering for updating frames.
    type Renderer is new Limited_Object and Process with private;
    type A_Renderer is access all Renderer'Class;

    -- Create a Renderer object that draws Window 'win' to display 'display'.
    --
    -- 'background' sets the color of the outer border that surrounds the
    -- window, if the window widget doesn't fill the entire display. For
    -- applications that use a fixed window size that scales to fill the
    -- display, the display resolutions in between exact multiples of the window
    -- size will show this border color between the edges of the window widget
    -- and the display.

    function Create_Renderer( display    : A_Allegro_Display;
                              win        : not null A_Widget;
                              background : Allegro_Color ) return A_Renderer;
    pragma Postcondition( Create_Renderer'Result /= null );

    -- Renders one frame of the renderer's window widget to its display window.
    -- This can only be called from the rendering thread.
    procedure Draw( this : not null access Renderer'Class );

    function Get_Process_Name( this : access Renderer ) return String is ("Renderer");

    -- Updates the screen.
    procedure Tick( this : access Renderer; time : Tick_Time );

    -- Deletes the Renderer.
    procedure Delete( this : in out A_Renderer );
    pragma Postcondition( this = null );

    -- scale Z coordinates such that -256.0 is foreground and +256.0 is background.
    -- this scaling is primarily to support the depth used by the Scene widget,
    -- which supports up to MAX_LAYERS, each spaced LAYER_Z_SPACING apart. the
    -- the 2x is not necessary; it just gives some margin.
    Z_RANGE : constant := Float(Maps.MAX_LAYERS) * Maps.LAYER_Z_SPACING * 2.0;
    Z_SCALE : constant := 1.0 / Z_RANGE;

    -- this is the smallest precision supported by the 16bit depth buffer when
    -- the Z range is scaled from -Z_SCALE to +Z_SCALE (where Z_SCALE = 1/256).
    --
    -- Z_RANGE * (1.0 / Z_PRECISION) = (1.0 / 2^15)
    Z_PRECISION : constant := 1.0 / (2.0 ** 15 / Z_RANGE);

private

    -- an array of video bitmaps for double/triple-buffering
    type Page_Array is array(Natural range <>) of A_Allegro_Bitmap;
    type A_Page_Array is access all Page_Array;

    ----------------------------------------------------------------------------

    type Renderer is new Limited_Object and Process with
        record
            display    : A_Allegro_Display := null;
            win        : A_Widget := null;
            background : Allegro_Color;
        end record;

    procedure Construct( this       : access Renderer;
                         display    : A_Allegro_Display;
                         win        : not null A_Widget;
                         background : Allegro_Color );

end Renderers;
