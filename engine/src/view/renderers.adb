--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.State;                     use Allegro.State;
with Allegro.Transformations;           use Allegro.Transformations;
--with Debugging;                         use Debugging;
with Drawing;                           use Drawing;
with Game_Views;                        use Game_Views;
with Interfaces;                        use Interfaces;

package body Renderers is

    function Create_Renderer( display    : A_Allegro_Display;
                              win        : not null A_Widget;
                              background : Allegro_Color ) return A_Renderer is
        this : constant A_Renderer := new Renderer;
    begin
        this.Construct( display, win, background );
        return this;
    end Create_Renderer;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this       : access Renderer;
                         display    : A_Allegro_Display;
                         win        : not null A_Widget;
                         background : Allegro_Color ) is
    begin
        Limited_Object(this.all).Construct;
        this.display := display;
        this.win := win;
        this.background := background;
    end Construct;

    ----------------------------------------------------------------------------

    procedure Draw( this : not null access Renderer'Class ) is
        state : Allegro_State;
        trans : Allegro_Transform;
    begin
        Al_Set_Target_Backbuffer( this.display );

        -- greater depths are more distant. for things at the same depth, the
        -- last item drawn wins.
        Al_Set_Render_State( ALLEGRO_DEPTH_TEST, 1 );
        Al_Set_Render_State( ALLEGRO_DEPTH_FUNCTION, ALLEGRO_RENDER_LESS_EQUAL );
        Al_Set_Render_State( ALLEGRO_WRITE_MASK, ALLEGRO_MASK_RGBA or ALLEGRO_MASK_DEPTH );

        -- color of any pixels not drawn by the window
        Clear_To_Color( this.background );

        -- infinite depth (-1.0 is foreground, 1.0 is background)
        Al_Clear_Depth_Buffer( 1.0 );

        -- start drawing with the default shader
        this.win.Get_View.Use_Shader( "default" );

        Al_Set_Clipping_Rectangle( 0, 0, Al_Get_Display_Width( this.display ), Al_Get_Display_Height( this.display ) );

        Al_Store_State( state, ALLEGRO_STATE_TRANSFORM );

        Al_Identity_Transform( trans );
        Al_Scale_Transform_3d( trans, 1.0, 1.0, Z_SCALE );
        Al_Compose_Transform( trans, Al_Get_Current_Transform.all );
        Al_Use_Transform( trans );

        -- exceptions raised by drawing will be caught by the Process_Manager
        -- and terminate the application.
        this.win.Draw;

        Al_Restore_State( state );

        Al_Flip_Display;
    exception
        when others =>
            Al_Restore_State( state );
            raise;
    end Draw;

    ----------------------------------------------------------------------------

    overriding
    procedure Tick( this : access Renderer; time : Tick_Time ) is
        pragma Unreferenced( time );
    begin
        this.Draw;
    end Tick;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Renderer ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

end Renderers;
