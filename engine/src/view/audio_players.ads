--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Doubly_Linked_Lists;
with Ada.Real_Time;                     use Ada.Real_Time;
with Allegro.Audio;                     use Allegro.Audio;
with Assets.Audio_Samples;              use Assets.Audio_Samples;
with Events;                            use Events;
with Events.Corrals;                    use Events.Corrals;
with Events.Listeners;                  use Events.Listeners;
with Objects;                           use Objects;
with Processes;                         use Processes;
with Resources;                         use Resources;

package Audio_Players is

    -- Audio_Player objects listen for certain audio events and play sound
    -- effects or music on command. The player contains an internal task for
    -- playing audio in the background.
    type Audio_Player is new Limited_Object and Process and Event_Listener with private;
    type A_Audio_Player is access all Audio_Player'Class;

    -- Creates a new Audio_Player that will listen for audio events in 'corral'.
    function Create_Audio_Player( corral : not null A_Corral ) return A_Audio_Player;
    pragma Postcondition( Create_Audio_Player'Result /= null );

    overriding
    function Get_Process_Name( this : access Audio_Player ) return String is ("Audio Player");

    -- Handles Play_Music, Stop_Music, and Play_Sound events.
    procedure Handle_Event( this : access Audio_Player;
                            evt  : in out A_Event;
                            resp : out Response_Type );
    pragma Precondition( evt /= null );

    -- Shuts down the audio player, stopping the music and sound effects, and
    -- removing the player from the event corral. Call this before deleting the
    -- object.
    procedure Shutdown( this : not null access Audio_Player'Class );

    procedure Tick( this : access Audio_Player; time : Tick_Time );

    -- Deletes the Audio_Player.
    procedure Delete( this : in out A_Audio_Player );
    pragma Postcondition( this = null );

private

    type Sound is
        record
            sample    : A_Audio_Sample;
            instance  : A_Allegro_Sample_Instance;
            startTime : Time;
        end record;

    package Sound_Lists is new Ada.Containers.Doubly_Linked_Lists( Sound, "=" );

    ----------------------------------------------------------------------------

    type Audio_Player is new Limited_Object and Process and Event_Listener with
        record
            voice        : A_Allegro_Voice := null;
            mixer        : A_Allegro_Mixer := null;
            corral       : A_Corral := null;
            musicRes     : A_Resource_File := null;
            musicStream  : A_Allegro_Audio_Stream := null;
            musicPlaying : Boolean := False;
            sounds       : Sound_Lists.List;
        end record;

    procedure Construct( this : access Audio_Player; corral : not null A_Corral );

    procedure Delete( this : in out Audio_Player );

end Audio_Players;
