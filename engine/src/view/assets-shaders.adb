--
-- Copyright (c) 2017-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Debugging;                         use Debugging;
with Resources;                         use Resources;
with Resources.Plaintext;               use Resources.Plaintext;
with Support.Paths;                     use Support.Paths;
with Values.Strings;                    use Values.Strings;

package body Assets.Shaders is

    function Load_Shader( name : String ) return A_Shader is
        lname   : constant String := To_Lower( name ) & ".glsl";
        assetId : constant Value := Create( lname );
        asset   : A_Asset := null;
        this    : A_Shader := null;
        created : Boolean := False;
    begin
        asset := Find_Asset( assetId );
        if asset /= null then
            -- found in cache
            pragma Assert( asset.Get_Type = Shader_Assets,
                           "Cached asset '" & lname & "' is not a shader" );
            this := A_Shader(asset);
        else
            -- create a new asset
            this := new Shader;
            this.Construct( assetId, To_Lower( name ) );
            created := True;
            Store_Asset( A_Asset(this) );
        end if;
        if created and not this.loaded then
            Dbg( "Failed to load shader <" & name & ">", D_ASSET or D_RES, Error );
        end if;
        return this;
    end Load_Shader;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this    : access Shader;
                         assetId : Value'Class;
                         name    : String ) is
    begin
        Asset(this.all).Construct( Shader_Assets, assetId, Add_Extension( name, "frag.glsl" ), "graphics" );
        this.name := To_Unbounded_String( name );
        this.keepLoaded := True;
        this.trackChanges := False;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Al_Shader( this : not null access Shader'Class ) return A_Allegro_Shader is
    begin
        return this.alShader;
    end Get_Al_Shader;

    ----------------------------------------------------------------------------

    overriding
    function Load_Data( this : access Shader ) return Boolean is
        vertPath   : constant String := Add_Extension( To_String( this.name ), "vert.glsl" );
        fragPath   : constant String := Add_Extension( To_String( this.name ), "frag.glsl" );
        vertSource : Unbounded_String;
        fragSource : Unbounded_String;
    begin
        this.alShader := Al_Create_Shader( ALLEGRO_SHADER_GLSL );
        if this.alShader /= null then
            if Load_Plaintext( vertPath, To_String( this.group ), vertSource ) then
                if Load_Plaintext( fragPath, To_String( this.group ), fragSource ) then
                    if Al_Attach_Shader_Source( this.alShader, ALLEGRO_VERTEX_SHADER, To_String( vertSource ) ) then
                        if Al_Attach_Shader_Source( this.alShader, ALLEGRO_PIXEL_SHADER, To_String( fragSource ) ) then
                            if Al_Build_Shader( this.alShader ) then
                                -- success!
                                return True;
                            else
                                Dbg( "Failed to build shader <" & To_String( this.name ) & ">", D_GUI, Error );
                            end if;
                        else
                            Dbg( "Failed to compile " & fragPath, D_GUI, Error );
                        end if;
                    else
                        Dbg( "Failed to compile " & vertPath, D_GUI, Error );
                    end if;

                    -- display errors from the shader log
                    Dbg( Al_Get_Shader_Log( this.alShader ), D_GUI, Error );
                else
                    Dbg( "File not found: " & fragPath, D_ASSET or D_RES, Error );
                end if;
            else
                Dbg( "File not found: " & vertPath, D_ASSET or D_RES, Error );
            end if;

            Al_Destroy_Shader( this.alShader );
        else
            Dbg( "Failed to create shader <" & To_String( this.name ) & ">", D_GUI, Error );
        end if;
        return False;
    end Load_Data;

    ----------------------------------------------------------------------------

    overriding
    procedure Unload_Data( this : access Shader ) is
    begin
        -- this is why individual shaders cannot be unloaded. the shader could
        -- be in use on the current bitmap or another bitmap.
        Al_Use_Shader( null );

        Al_Destroy_Shader( this.alShader );
    end Unload_Data;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Shader ) is
    begin
        Delete( A_Asset(this) );
    end Delete;

end Assets.Shaders;
