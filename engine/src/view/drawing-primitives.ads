--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Drawing.Primitives is

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Scalable Drawing

    -- The following procedures operate using exact fractional (non inclusive)
    -- coordinates. If a scaling transform may be applied, these procedures
    -- should be preferred over the pixel-perfect procedures below.

    -- Draws a line between two points.
    procedure Line( x1, y1    : Float;
                    x2, y2    : Float;
                    color     : Allegro_Color;
                    thickness : Float := 1.0;
                    blendMode : Blend_Mode := Nonpremultiplied );

    -- Draws a rectangle outline.
    procedure Rect( x1, y1    : Float;
                    x2, y2    : Float;
                    color     : Allegro_Color;
                    thickness : Float := 1.0;
                    blendMode : Blend_Mode := Nonpremultiplied );

    -- Draws a filled rectangle. By default, the z is 0.0.
    procedure Rectfill( x1, y1    : Float;
                        x2, y2    : Float;
                        color     : Allegro_Color;
                        blendMode : Blend_Mode := Nonpremultiplied );

    -- Draws a filled rectangle with a 'z' plane coordinate.
    procedure Rectfill( x1, y1    : Float;
                        x2, y2    : Float;
                        z         : Float;
                        color     : Allegro_Color;
                        blendMode : Blend_Mode := Nonpremultiplied );

    -- Draws a triangle outline.
    procedure Triangle( x1, y1    : Float;
                        x2, y2    : Float;
                        x3, y3    : Float;
                        color     : Allegro_Color;
                        thickness : Float := 1.0;
                        blendMode : Blend_Mode := Nonpremultiplied );

    -- Draws a filled triangle.
    procedure Triangle_Filled( x1, y1    : Float;
                               x2, y2    : Float;
                               x3, y3    : Float;
                               color     : Allegro_Color;
                               blendMode : Blend_Mode := Nonpremultiplied );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Pixel-Perfect Drawing

    -- Converts a width 'width' in pixels on the target drawing bitmap to the
    -- current drawing coordinates.
    function From_Target_Pixels( width : Float ) return Float;

    -- Rounds 'x,y' in drawing coordinates to the middle of a pixel on the
    -- target drawing bitmap.
    procedure To_Target_Pixel_Center( x, y : in out Float );

    -- Rounds 'x,y' in drawing coordinates to the upper left corner of a pixel
    -- on the target drawing bitmap.
    procedure To_Target_Pixel_Rounded( x, y : in out Float );

    -- The procedures below draw pixel-perfect primitives, offsetting the given
    -- coordinates such that they line up on pixel centers. All coordinates are
    -- inclusive. These methods should only be used when the scaling transform
    -- is an integer, not fractional.

    -- Draws a line between the centers of two pixels.
    procedure Pixel_Line( x1, y1    : Float;
                          x2, y2    : Float;
                          color     : Allegro_Color;
                          thickness : Float := 1.0;
                          blendMode : Blend_Mode := Nonpremultiplied );

    -- Draws a horizontal dotted line. (2 on, 2 off)
    procedure Pixel_Line_H_Dotted( x1, x2    : Float;
                                   y         : Float;
                                   color     : Allegro_Color;
                                   thickness : Float := 1.0;
                                   blendMode : Blend_Mode := Nonpremultiplied );

    -- Draws a vertical dotted line. (2 on, 2 off)
    procedure Pixel_Line_V_Dotted( x         : Float;
                                   y1, y2    : Float;
                                   color     : Allegro_Color;
                                   thickness : Float := 1.0;
                                   blendMode : Blend_Mode := Nonpremultiplied );

    -- Draws a rectangle outline.
    procedure Pixel_Rect( x1, y1    : Float;
                          x2, y2    : Float;
                          color     : Allegro_Color;
                          thickness : Float := 1.0;
                          blendMode : Blend_Mode := Nonpremultiplied );

    -- Draws a filled rectangle. By default, the z is 0.0.
    procedure Pixel_Rectfill( x1, y1    : Float;
                              x2, y2    : Float;
                              color     : Allegro_Color;
                              blendMode : Blend_Mode := Nonpremultiplied );

    -- Draws a filled rectangle with a 'z' plane coordinate.
    procedure Pixel_Rectfill( x1, y1    : Float;
                              x2, y2    : Float;
                              z         : Float;
                              color     : Allegro_Color;
                              blendMode : Blend_Mode := Nonpremultiplied );

    -- Draws a rectangle with the top left at 'x,y' and 'w,h' in size. The edges
    -- of the rectangle are always 1px thick on the target bitmap, regardless of
    -- scaling, and they are aligned to the nearest whole pixel  if the target
    -- bitmap, so they are sharp and clear.
    --
    -- Unlike Rect() and Pixel_Rect(), the rectangle drawn is inclusive of (x,y)
    -- but NOT inclusive of (x+w, y+h).
    procedure Rect_XYWH( x, y      : Float;
                         w, h      : Float;
                         color     : Allegro_Color;
                         blendMode : Blend_Mode := Nonpremultiplied );

    -- Draws a filled rectangle with the top left at 'x,y' and 'w,h' in size.
    -- The edges of the rectangle are aligned to the nearest whole pixel of the
    -- target bitmap, so they are sharp and clear.
    --
    -- Unlike Rect() and Pixel_Rect(), the rectangle drawn is inclusive of (x,y)
    -- but NOT inclusive of (x+w, y+h).
    procedure Rectfill_XYWH( x, y      : Float;
                             w, h      : Float;
                             color     : Allegro_Color;
                             blendMode : Blend_Mode := Nonpremultiplied );

end Drawing.Primitives;
