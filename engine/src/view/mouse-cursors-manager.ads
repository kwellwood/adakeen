--
-- Copyright (c) 2017-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Mouse.Cursors.Manager is

    -- Initializes the mouse cursor registry before first use.
    procedure Init_Mouse_Cursors;

    -- Retrieves a mouse cursor by name. 'name' should be either a previously
    -- registered custom cursor, or one of the following system cursor names:
    --
    -- "NONE", "DEFAULT", "ARROW", "BUSY", "QUESTION", "EDIT", "MOVE"
    -- "RESIZE_N", "RESIZE_W", "RESIZE_S", "RESIZE_E"
    -- "RESIZE_NW", "RESIZE_SW", "RESIZE_SE", "RESIZE_NE"
    -- "PROGRESS", "PRECISION", "LINK", "ALT_SELECT", "UNAVAILABLE"
    --
    -- If 'name' does not match a registered cursor, the cursor will be
    -- registered automatically as 'name' (see Register_Mouse_Cursor for the
    -- expected cursor name format). If the cursor cannot be registered, a
    -- default cursor will be returned.
    function Get_Mouse_Cursor( name : String ) return A_Mouse_Cursor;

    -- Registers a custom mouse cursor from a tile by name in a tile library.
    -- The format for libAndTile is "<lib>:<tile>", where <lib> is a tile
    -- library name and <tile> is a filename in the library. If 'libAndTile'
    -- does not exist, or if the image is larger than 32 pixels to a side, then
    -- it will not be registered. If a cursor of the same 'libAndTile' has
    -- already been registered, it will not be registered again.
    procedure Register_Mouse_Cursor( libAndTile : String );

    -- Deletes all registered cursors, unloading the registry for shutdown.
    procedure Unload_Mouse_Cursors;

end Mouse.Cursors.Manager;
