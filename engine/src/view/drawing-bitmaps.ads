--
-- Copyright (c) 2012-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Palette;                           use Palette;

package Drawing.Bitmaps is

    type Stretch_Mode is
    (
        -- Stretches the source bitmap to cover the destination area without
        -- regard for its original proportions.
        Stretch,

        -- Stretches the source bitmap to fit the narrowest dimension of the
        -- destination area, preserving the bitmap's original proportions. It
        -- will be centered within the widest dimension, leaving empty space in
        -- the margins.
        Fit,

        -- Stretches the source bitmap to fill the widest dimension of the
        -- destination area, preserving the bitmap's original proportions. No
        -- clipping is performed, so pixels outside of the destination area may
        -- be drawn in the narrowest dimension. Take care to set the clipping
        -- rectangle, if necessary, before drawing a bitmap with Fill.
        Fill
    );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Draws bitmap 'bmp', using 'x,y' as the top left corner.
    procedure Draw_Bitmap( bmp       : A_Allegro_Bitmap;
                           x, y      : Float;
                           z         : Float := 0.0;
                           tint      : Allegro_Color := White;
                           blendMode : Blend_Mode := Current );

    -- Draws a region of bitmap 'bmp'.
    procedure Draw_Bitmap_Region( bmp       : A_Allegro_Bitmap;
                                  srcX,
                                  srcY      : Float;
                                  srcWidth,
                                  srcHeight : Float;
                                  destX,
                                  destY     : Float;
                                  destZ     : Float := 0.0;
                                  tint      : Allegro_Color := White;
                                  blendMode : Blend_Mode := Current );

    -- Draws bitmap 'bmp', stretching it to 'width' x 'height'. The bitmap will
    -- be stretched to the destination area using 'mode'.
    --
    -- If 'rotation' is specified, then the bitmap will be rotated around its
    -- center and drawn such that the center of 'bmp' is at 'x,y' in the
    -- destination bitmap. No clipping will be performed.
    procedure Draw_Bitmap_Stretched( bmp           : A_Allegro_Bitmap;
                                     x, y, z       : Float;
                                     width, height : Float;
                                     stretchMode   : Stretch_Mode := Stretch;
                                     rotation      : Float := 0.0;
                                     tint          : Allegro_Color := White;
                                     blendMode     : Blend_Mode := Current );

    -- Draws a region of bitmap 'bmp', stretching it to the specified dimensions
    -- without respect to the original proportions.
    --
    -- If 'rotation' is specified, then the bitmap will be rotated around the
    -- center of 'bmp' and then drawn so the center is at 'x,y'.
    procedure Draw_Bitmap_Region_Stretched( bmp        : A_Allegro_Bitmap;
                                            srcX,
                                            srcY       : Float;
                                            srcWidth,
                                            srcHeight  : Float;
                                            destX,
                                            destY,
                                            destZ      : Float;
                                            destWidth,
                                            destHeight : Float;
                                            rotation   : Float := 0.0;
                                            tint       : Allegro_Color := White;
                                            blendMode  : Blend_Mode := Current );

end Drawing.Bitmaps;
