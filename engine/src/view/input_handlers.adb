--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Ordered_Maps;
with Ada.Real_Time;                     use Ada.Real_Time;
with Ada.Task_Identification;           use Ada.Task_Identification;
with Ada.Unchecked_Conversion;
with Allegro;                           use Allegro;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Joystick;                  use Allegro.Joystick;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.Native_Dialogs;            use Allegro.Native_Dialogs;
with Allegro.Time;
with Debugging;                         use Debugging;
with Events.Application;                use Events.Application;
with Events.Input;                      use Events.Input;
with Gamepads;                          use Gamepads;
with Interfaces;                        use Interfaces;
with Interfaces.C;                      use Interfaces.C;
with Keyboard;                          use Keyboard;
with Mouse;                             use Mouse;
with System;                            use System;
with System.Address_To_Access_Conversions;

package body Input_Handlers is

    function Lt( l, r : A_Allegro_Joystick ) return Boolean is
        type Uint_Addr is mod 2 ** Standard'Address_Size;
        function From_Addr is new Ada.Unchecked_Conversion( Address, Uint_Addr );
    begin
        return From_Addr( l.all'Address ) < From_Addr( r.all'Address );
    end Lt;

    package Joystick_Maps is new Ada.Containers.Ordered_Maps(A_Allegro_Joystick, A_Gamepad, Lt, "=");

    ----------------------------------------------------------------------------

    function Is_Visible_Keycode( keycode : Integer ) return Boolean is
    begin
        case keycode is
            when ALLEGRO_KEY_A |
                 ALLEGRO_KEY_B |
                 ALLEGRO_KEY_C |
                 ALLEGRO_KEY_D |
                 ALLEGRO_KEY_E |
                 ALLEGRO_KEY_F |
                 ALLEGRO_KEY_G |
                 ALLEGRO_KEY_H |
                 ALLEGRO_KEY_I |
                 ALLEGRO_KEY_J |
                 ALLEGRO_KEY_K |
                 ALLEGRO_KEY_L |
                 ALLEGRO_KEY_M |
                 ALLEGRO_KEY_N |
                 ALLEGRO_KEY_O |
                 ALLEGRO_KEY_P |
                 ALLEGRO_KEY_Q |
                 ALLEGRO_KEY_R |
                 ALLEGRO_KEY_S |
                 ALLEGRO_KEY_T |
                 ALLEGRO_KEY_U |
                 ALLEGRO_KEY_V |
                 ALLEGRO_KEY_W |
                 ALLEGRO_KEY_X |
                 ALLEGRO_KEY_Y |
                 ALLEGRO_KEY_Z |
                 ALLEGRO_KEY_0 |
                 ALLEGRO_KEY_1 |
                 ALLEGRO_KEY_2 |
                 ALLEGRO_KEY_3 |
                 ALLEGRO_KEY_4 |
                 ALLEGRO_KEY_5 |
                 ALLEGRO_KEY_6 |
                 ALLEGRO_KEY_7 |
                 ALLEGRO_KEY_8 |
                 ALLEGRO_KEY_9 |
                 ALLEGRO_KEY_PAD_0 |
                 ALLEGRO_KEY_PAD_1 |
                 ALLEGRO_KEY_PAD_2 |
                 ALLEGRO_KEY_PAD_3 |
                 ALLEGRO_KEY_PAD_4 |
                 ALLEGRO_KEY_PAD_5 |
                 ALLEGRO_KEY_PAD_6 |
                 ALLEGRO_KEY_PAD_7 |
                 ALLEGRO_KEY_PAD_8 |
                 ALLEGRO_KEY_PAD_9 |
                 ALLEGRO_KEY_ESCAPE |
                 ALLEGRO_KEY_TILDE |
                 ALLEGRO_KEY_MINUS |
                 ALLEGRO_KEY_EQUALS |
                 ALLEGRO_KEY_OPENBRACE |
                 ALLEGRO_KEY_CLOSEBRACE |
                 ALLEGRO_KEY_SEMICOLON |
                 ALLEGRO_KEY_QUOTE |
                 ALLEGRO_KEY_BACKSLASH |
                 ALLEGRO_KEY_BACKSLASH2 |
                 ALLEGRO_KEY_COMMA |
                 ALLEGRO_KEY_FULLSTOP |
                 ALLEGRO_KEY_SLASH |
                 ALLEGRO_KEY_SPACE |
                 ALLEGRO_KEY_PAD_SLASH |
                 ALLEGRO_KEY_PAD_ASTERISK |
                 ALLEGRO_KEY_PAD_MINUS |
                 ALLEGRO_KEY_PAD_PLUS |
                 ALLEGRO_KEY_AT |
                 ALLEGRO_KEY_CIRCUMFLEX |
                 ALLEGRO_KEY_COLON2 |
                 ALLEGRO_KEY_PAD_EQUALS |
                 ALLEGRO_KEY_BACKQUOTE |
                 ALLEGRO_KEY_SEMICOLON2 =>
                     return True;
             when others =>
                 return False;
         end case;
    end Is_Visible_Keycode;

    ----------------------------------------------------------------------------

    type Click_Count is mod 2;

    type MB_State is
        record
            pressed   : Boolean := False;
            pressTime : Ada.Real_Time.Time := Clock;
            pressX,
            pressY    : Integer := 0;
            lastHeld  : Ada.Real_Time.Time := Clock;
            clickTime : Ada.Real_Time.Time := Clock;
            clickX,
            clickY    : Integer := 0;
            clicks    : Click_Count := 0;
        end record;

    type MB_States is array (Mouse_Button) of MB_State;

    ----------------------------------------------------------------------------

    -- allowed mouse drift during click
    CLICK_DRIFT : constant := 10;

    -- delay between press and release
    CLICK_DELAY : constant Time_Span := Milliseconds( 400 );

    -- delay between 1st click and 2nd click

    DOUBLECLICK_DELAY : constant Time_Span := Milliseconds( 600 );

    -- delay between mouse held events
    HELD_REPEAT_DELAY : constant Time_Span := Milliseconds( 1000 / 50 );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Input_Handler( display      : not null A_Allegro_Display;
                                   mouseEnabled : Boolean ) return A_Input_Handler is
        this : constant A_Input_Handler := new Input_Handler;
    begin
        this.Construct( display, mouseEnabled );
        return this;
    end Create_Input_Handler;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this         : access Input_Handler;
                         display      : A_Allegro_Display;
                         mouseEnabled : Boolean ) is
    begin
        Limited_Object(this.all).Construct;
        this.display := display;
        this.mouseEnabled := mouseEnabled;
        this.process := new Input_Task;
        this.process.Init( A_Input_Handler(this) );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Input_Handler ) is
    begin
        this.Stop;
        Delete( this.process );
        Limited_Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Is_Key_Down( this : not null access Input_Handler'Class;
                          key  : Integer ) return Boolean is
    begin
        this.keyLock.Lock;
        return result : constant Boolean := this.keyStates(key) do
            this.keyLock.Unlock;
        end return;
    end Is_Key_Down;

    ----------------------------------------------------------------------------

    procedure Start( this : not null access Input_Handler'Class ) is
    begin
        if not this.started and then not this.stopped then
            this.started := True;
            this.process.Start;
        end if;
    end Start;

    ----------------------------------------------------------------------------

    procedure Stop( this : not null access Input_Handler'Class ) is
    begin
        if not this.stopped then
            pragma Debug( Dbg( "Stopping input handler...", D_INPUT, Info ) );
            this.stopped := True;
            this.process.Stop;

            -- wait one second for the process to shut down
            for count in 1..10 loop
                if Is_Terminated( this.process'Identity ) then
                    exit;
                end if;
                delay 0.1;
            end loop;
            if not Is_Terminated( this.process'Identity ) then
                Dbg( "Input_Handler.Stop: Input_Task failed to stop", D_INPUT, Error );
            end if;
        end if;
    exception
        when e : others =>
            Dbg( "Input_Handler.Stop Exception: ...", D_INPUT, Error );
            Dbg( e, D_INPUT, Error );
    end Stop;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Input_Handler ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

    --==========================================================================

    task body Input_Task is
        handler     : A_Input_Handler;
        mbtn        : MB_States;
        modifiers   : Modifiers_Array := MODIFIERS_NONE;
        controllers : Joystick_Maps.Map;

        ------------------------------------------------------------------------

        procedure Update_Gamepads is
            count : Natural := 0;
            joy   : A_Allegro_Joystick;
            pos   : Joystick_Maps.Cursor;
            use Joystick_Maps;
        begin
            -- check for disconnected controllers
            for c of controllers loop
                if c /= null and then not c.Is_Active then
                    Delete( c );
                end if;
            end loop;

            -- check for newly added controllers
            count := Al_Get_Num_Joysticks;
            for i in 1..count loop
                joy := Al_Get_Joystick( i - 1 );
                pos := controllers.Find( joy );
                if not Has_Element( pos ) or else Element( pos ) = null then
                    controllers.Include( joy, Create_Gamepad( joy ) );
                end if;
            end loop;
        end Update_Gamepads;

        ------------------------------------------------------------------------

        -- Processes all waiting input events.
        procedure Process_Events( queue : A_Allegro_Event_Queue ) is
            now : Ada.Real_Time.Time;

            --------------------------------------------------------------------

            -- send mouse button held events for all buttons held down
            procedure Check_Mouse_Held is
                mstate : Allegro_Mouse_State;
            begin
                Al_Get_Mouse_State( mstate );
                for mb in mbtn'Range loop
                    if mbtn(mb).pressed and then
                       now - mbtn(mb).pressTime >= CLICK_DELAY and then
                       now - mbtn(mb).lastHeld >= HELD_REPEAT_DELAY
                    then
                        -- event: mouse button still held
                        Queue_Mouse_Held( mstate.x, mstate.y, mb );
                        mbtn(mb).lastHeld := now;
                    end if;
                end loop;
            end Check_Mouse_Held;

            --------------------------------------------------------------------

            start   : constant Ada.Real_Time.Time := Clock;
            event   : aliased Allegro_Event;
            timeout : Allegro.Time.Allegro_Timeout;
        begin
            -- process all events waiting since the last input frame
            Allegro.Time.Al_Init_Timeout( timeout, 0.01 );
            while Al_Wait_For_Event_Until( queue, event'Access, timeout ) loop
                now := Clock;

                case event.any.typ is
                    when ALLEGRO_EVENT_KEY_DOWN =>
                        -- a single key was pressed down
                        if event.keyboard.keycode in 1..(ALLEGRO_KEY_MAX-1) then
                            Queue_Key_Press( event.keyboard.keycode );
                            case event.keyboard.keycode is
                                when ALLEGRO_KEY_ALT | ALLEGRO_KEY_ALTGR => modifiers(ALT) := True;
                                when ALLEGRO_KEY_LCTRL | ALLEGRO_KEY_RCTRL => modifiers(CTRL) := True;
                                when ALLEGRO_KEY_LSHIFT | ALLEGRO_KEY_RSHIFT => modifiers(SHIFT) := True;
                                when ALLEGRO_KEY_LWIN | ALLEGRO_KEY_RWIN | ALLEGRO_KEY_COMMAND => modifiers(CMD) := True;
                                when others => null;
                            end case;
                            handler.keyLock.Lock;
                            handler.keyStates(event.keyboard.keycode) := True;
                            handler.keyLock.Unlock;
                        end if;

                    when ALLEGRO_EVENT_KEY_UP =>
                        -- a single key was pressed down
                        if event.keyboard.keycode in 1..(ALLEGRO_KEY_MAX-1) then
                            Queue_Key_Release( event.keyboard.keycode );
                            case event.keyboard.keycode is
                                when ALLEGRO_KEY_ALT | ALLEGRO_KEY_ALTGR => modifiers(ALT) := False;
                                when ALLEGRO_KEY_LCTRL | ALLEGRO_KEY_RCTRL => modifiers(CTRL) := False;
                                when ALLEGRO_KEY_LSHIFT | ALLEGRO_KEY_RSHIFT => modifiers(SHIFT) := False;
                                when ALLEGRO_KEY_LWIN | ALLEGRO_KEY_RWIN | ALLEGRO_KEY_COMMAND => modifiers(CMD) := False;
                                when others => null;
                            end case;
                            handler.keyLock.Lock;
                            handler.keyStates(event.keyboard.keycode) := False;
                            handler.keyLock.Unlock;
                        end if;

                    when ALLEGRO_EVENT_KEY_CHAR =>
                        -- a character was typed on the keyboard or auto-repeated
                        declare
                            -- the Allegro event contains the modifiers
                            modifiers : Modifiers_Array := MODIFIERS_NONE;
                        begin
                            if event.keyboard.keycode in 1..(ALLEGRO_KEY_MAX-1) then
                                modifiers(ALT) := (event.keyboard.modifiers and (KEYMOD_ALT or KEYMOD_ALTGR)) /= 0;
                                modifiers(CTRL) := (event.keyboard.modifiers and KEYMOD_CTRL) /= 0;
                                modifiers(SHIFT) := (event.keyboard.modifiers and KEYMOD_SHIFT) /= 0;
                                modifiers(CMD) := (event.keyboard.modifiers and (KEYMOD_LWIN or KEYMOD_RWIN or KEYMOD_COMMAND)) /= 0;
                                if Is_Visible_Keycode( event.keyboard.keycode ) then
                                    -- visible UTF characters outside the 8 bit ASCII
                                    -- range can be typed on MacOS. Check explicitly,
                                    if 0 < event.keyboard.unichar and event.keyboard.unichar < 128 then
                                        Queue_Key_Typed( event.keyboard.keycode,
                                                         Character'Val(event.keyboard.unichar),
                                                         modifiers );
                                    end if;
                                else
                                    Queue_Key_Typed( event.keyboard.keycode, ASCII.NUL, modifiers );
                                end if;
                            end if;
                        end;

                    when ALLEGRO_EVENT_JOYSTICK_AXIS =>
                        controllers.Element( event.joystick.id ).Set_Position( event.joystick.stick, event.joystick.axis, event.joystick.pos );

                    when ALLEGRO_EVENT_JOYSTICK_BUTTON_DOWN =>
                        controllers.Element( event.joystick.id ).Set_Button( event.joystick.button, pressed => True );

                    when ALLEGRO_EVENT_JOYSTICK_BUTTON_UP =>
                        controllers.Element( event.joystick.id ).Set_Button( event.joystick.button, pressed => False );

                    when ALLEGRO_EVENT_JOYSTICK_CONFIGURATION =>
                        Dbg( "Reconfiguring controllers", D_INPUT, Info );
                        if Al_Reconfigure_Joysticks then
                            Update_Gamepads;
                        else
                            Dbg( "Controller reconfiguration failed", D_INPUT, Error );
                        end if;

                    when ALLEGRO_EVENT_MOUSE_AXES =>
                        if event.mouse.dz /= 0 then
                            Queue_Mouse_Scroll( event.mouse.x, event.mouse.y, event.mouse.dz );
                        end if;
                        if event.mouse.dx /= 0 or else event.mouse.dy /= 0 then
                            Queue_Mouse_Move( event.mouse.x, event.mouse.y );
                        end if;

                    when ALLEGRO_EVENT_MOUSE_BUTTON_DOWN =>
                        if event.mouse.button = 1 then
                            -- event: left mouse press
                            Queue_Mouse_Press( event.mouse.x, event.mouse.y, Mouse_Left, modifiers );
                            mbtn(Mouse_Left).pressed := True;
                            mbtn(Mouse_Left).pressTime := now;
                            mbtn(Mouse_Left).pressX := event.mouse.x;
                            mbtn(Mouse_Left).pressY := event.mouse.y;
                            mbtn(Mouse_Left).lastHeld := now;
                        elsif event.mouse.button = 2 then
                            -- event: right mouse press
                            Queue_Mouse_Press( event.mouse.x, event.mouse.y, Mouse_Right, modifiers );
                            mbtn(Mouse_Right).pressed := True;
                            mbtn(Mouse_Right).pressTime := now;
                            mbtn(Mouse_Right).pressX := event.mouse.x;
                            mbtn(Mouse_Right).pressY := event.mouse.y;
                            mbtn(Mouse_Right).lastHeld := now;
                        elsif event.mouse.button = 3 then
                            -- event: middle mouse press
                            Queue_Mouse_Press( event.mouse.x, event.mouse.y, Mouse_Middle, modifiers );
                            mbtn(Mouse_Middle).pressed := True;
                            mbtn(Mouse_Middle).pressTime := now;
                            mbtn(Mouse_Middle).pressX := event.mouse.x;
                            mbtn(Mouse_Middle).pressY := event.mouse.y;
                            mbtn(Mouse_Middle).lastHeld := now;
                        end if;

                    when ALLEGRO_EVENT_MOUSE_BUTTON_UP =>
                        if event.mouse.button = 1 then
                            -- event: left mouse release
                            Queue_Mouse_Release( event.mouse.x, event.mouse.y, Mouse_Left );
                            mbtn(Mouse_Left).pressed := False;
                            if now - mbtn(Mouse_Left).pressTime < CLICK_DELAY and then
                               abs( mbtn(Mouse_Left).pressX - event.mouse.x ) < CLICK_DRIFT and then
                               abs( mbtn(Mouse_Left).pressY - event.mouse.y ) < CLICK_DRIFT
                            then
                                -- event: left mouse click
                                Queue_Mouse_Click( event.mouse.x, event.mouse.y, Mouse_Left );
                                if mbtn(Mouse_Left).clicks = 1 then
                                    if now - mbtn(Mouse_Left).clickTime < DOUBLECLICK_DELAY and then
                                       abs( mbtn(Mouse_Left).clickX - event.mouse.x ) < CLICK_DRIFT and then
                                       abs( mbtn(Mouse_Left).clickY - event.mouse.y ) < CLICK_DRIFT
                                    then
                                        -- event: left mouse double click
                                        Queue_Mouse_Doubleclick( event.mouse.x, event.mouse.y, Mouse_Left );
                                        mbtn(Mouse_Left).clicks := 0;                          -- reset the clicks count
                                    end if;
                                else
                                    mbtn(Mouse_Left).clicks := mbtn(Mouse_Left).clicks + 1;    -- increase the clicks count
                                end if;
                                mbtn(Mouse_Left).clickTime := now;
                                mbtn(Mouse_Left).clickX := event.mouse.x;
                                mbtn(Mouse_Left).clickY := event.mouse.y;
                            end if;

                        elsif event.mouse.button = 2 then
                            -- event: right mouse release
                            Queue_Mouse_Release( event.mouse.x, event.mouse.y, Mouse_Right );
                            mbtn(Mouse_Right).pressed := False;
                            if now - mbtn(Mouse_Right).pressTime < CLICK_DELAY and then
                               abs( mbtn(Mouse_Right).pressX - event.mouse.x ) < CLICK_DRIFT and then
                               abs( mbtn(Mouse_Right).pressY - event.mouse.y ) < CLICK_DRIFT
                            then
                                -- event: right mouse click
                                Queue_Mouse_Click( event.mouse.x, event.mouse.y, Mouse_Right );
                                if mbtn(Mouse_Right).clicks = 1 then
                                    if now - mbtn(Mouse_Right).clickTime < DOUBLECLICK_DELAY and then
                                       abs( mbtn(Mouse_Right).clickX - event.mouse.x ) < CLICK_DRIFT and then
                                       abs( mbtn(Mouse_Right).clickY - event.mouse.y ) < CLICK_DRIFT
                                    then
                                        -- event: right mouse double click
                                        Queue_Mouse_Doubleclick( event.mouse.x, event.mouse.y, Mouse_Right );
                                        mbtn(Mouse_Right).clicks := 0;                         -- reset the clicks count
                                    end if;
                                else
                                    mbtn(Mouse_Right).clicks := mbtn(Mouse_Right).clicks + 1;  -- increase the clicks count
                                end if;
                                mbtn(Mouse_Right).clickTime := now;
                                mbtn(Mouse_Right).clickX := event.mouse.x;
                                mbtn(Mouse_Right).clickY := event.mouse.y;
                            end if;

                        elsif event.mouse.button = 3 then
                            -- event: middle mouse release
                            Queue_Mouse_Release( event.mouse.x, event.mouse.y, Mouse_Middle );
                            mbtn(Mouse_Middle).pressed := False;
                            if now - mbtn(Mouse_Middle).pressTime < CLICK_DELAY and then
                               abs( mbtn(Mouse_Middle).pressX - event.mouse.x ) < CLICK_DRIFT and then
                               abs( mbtn(Mouse_Middle).pressY - event.mouse.y ) < CLICK_DRIFT
                            then
                                -- event: middle mouse click
                                Queue_Mouse_Click( event.mouse.x, event.mouse.y, Mouse_Middle );
                                if mbtn(Mouse_Middle).clicks = 1 then
                                    if now - mbtn(Mouse_Middle).clickTime < DOUBLECLICK_DELAY and then
                                       abs( mbtn(Mouse_Middle).clickX - event.mouse.x ) < CLICK_DRIFT and then
                                       abs( mbtn(Mouse_Middle).clickY - event.mouse.y ) < CLICK_DRIFT
                                    then
                                        -- event: middle mouse double click
                                        Queue_Mouse_Doubleclick( event.mouse.x, event.mouse.y, Mouse_Middle );
                                        mbtn(Mouse_Middle).clicks := 0;                          -- reset the clicks count
                                    end if;
                                else
                                    mbtn(Mouse_Middle).clicks := mbtn(Mouse_Middle).clicks + 1;  -- increase the clicks count
                                end if;
                                mbtn(Mouse_Middle).clickTime := now;
                                mbtn(Mouse_Middle).clickX := event.mouse.x;
                                mbtn(Mouse_Middle).clickY := event.mouse.y;
                            end if;

                        end if;

                    when ALLEGRO_EVENT_MOUSE_LEAVE_DISPLAY => Queue_Mouse_Leave;

                    when ALLEGRO_EVENT_DISPLAY_CLOSE => Queue_Close_Window;

                    when ALLEGRO_EVENT_DISPLAY_SWITCH_OUT =>
                        -- reset the modifiers in case a button was being held
                        -- during the switch out.
                        modifiers := MODIFIERS_NONE;
                        handler.keyLock.Lock;
                        handler.keyStates(ALLEGRO_KEY_ALT) := False;
                        handler.keyStates(ALLEGRO_KEY_ALTGR) := False;
                        handler.keyStates(ALLEGRO_KEY_LCTRL) := False;
                        handler.keyStates(ALLEGRO_KEY_RCTRL) := False;
                        handler.keyStates(ALLEGRO_KEY_LSHIFT) := False;
                        handler.keyStates(ALLEGRO_KEY_RSHIFT) := False;
                        handler.keyStates(ALLEGRO_KEY_LWIN) := False;
                        handler.keyStates(ALLEGRO_KEY_RWIN) := False;
                        handler.keyLock.Unlock;
                        Queue_App_Blur;

                    when ALLEGRO_EVENT_DISPLAY_SWITCH_IN => Queue_App_Focus;

                    when ALLEGRO_EVENT_DISPLAY_RESIZE =>
                        -- TODO: Acknowledge the resize event from Allegro on
                        -- the same thread that receives it. See Game_View.
                        Queue_Window_Resized( event.display.source,
                                              event.display.x, event.display.y,
                                              event.display.width, event.display.height );

                    when ALLEGRO_EVENT_MENU_CLICK =>
                        declare
                            package Display_Conversion is new System.Address_To_Access_Conversions(Allegro_Display);
                            use Display_Conversion;

                            function To_Size_t is new Ada.Unchecked_Conversion( Address, size_t );
                            function To_Allegro_Display is new Ada.Unchecked_Conversion( Object_Pointer, A_Allegro_Display );
                        begin
                            Queue_Menu_Clicked( To_Allegro_Display(To_Pointer( event.user.data2 )),
                                                Integer(To_Size_t(event.user.data1)) );
                        end;

                    when others =>
                        null;
                end case;

                Check_Mouse_Held;

                -- break out of the Allegro event processing loop to check for
                -- the Stop rendezvous.
                if now - start > Milliseconds( 100 ) then
                    exit;
                end if;
            end loop;

            now := Clock;
            Check_Mouse_Held;
        end Process_Events;

        ------------------------------------------------------------------------

        done  : Boolean := False;
        queue : A_Allegro_Event_Queue;
    begin
        pragma Debug( Dbg( "Input_Handlers.Input_Task created", D_INPUT, Info ) );

        accept Init( this : A_Input_Handler ) do
            handler := this;
        end Init;

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        -- create an event queue for listening to input events
        queue := Al_Create_Event_Queue;
        if queue = null then
            raise Constraint_Error with "Event queue creation failed";
        end if;

        -- register for keyboard events
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );

        -- register for gamepad events
        Al_Register_Event_Source( queue, Al_Get_Joystick_Event_Source );
        Update_Gamepads;

        -- register for mouse events
        if handler.mouseEnabled then
            declare
                source : constant A_Allegro_Event_Source := Al_Get_Mouse_Event_Source;
            begin
                if source /= null then
                    Al_Register_Event_Source( queue, source );
                end if;
            end;
        end if;

        -- register for window events
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( handler.display ) );

        -- register for menu events
        Al_Register_Event_Source( queue, Al_Get_Default_Menu_Event_Source );

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        select
            accept Start do
                null;
                pragma Debug( Dbg( "Input_Handlers.Input_Task starting", D_INPUT, Info ) );
            end Start;
        or
            accept Stop do
                pragma Debug( Dbg( "Input_Handlers.Input_Task stopping", D_INPUT, Info ) );
                done := True;
            end Stop;
        end select;

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        -- Process_Events runs repeatedly to process incoming Allegro events,
        -- breaking to check for the Stop rendezvous at least every 100 ms.
        while not done loop
            select
                accept Stop do
                    pragma Debug( Dbg( "Input_Handlers.Input_Task stopping", D_INPUT, Info ) );
                    done := True;
                end Stop;
            else
                -- Processes all pending Allegro events, waiting up to 100ms for
                -- the first one, if none are queued.
                Process_Events( queue );
            end select;
        end loop;

        Al_Destroy_Event_Queue( queue );

        for c of controllers loop
            Delete( c );
        end loop;
        controllers.Clear;

        pragma Debug( Dbg( "Input_Handlers.Input_Task complete", D_INPUT, Info ) );
    exception
        when e : others =>
            Dbg( "Input_Handlers.Input_Task Exception: ...", D_INPUT, Error );
            Dbg( e, D_INPUT, Error );
            Al_Destroy_Event_Queue( queue );
    end Input_Task;

end Input_Handlers;
