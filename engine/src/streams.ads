--
-- Copyright (c) 2012-2014 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Streams;                       use Ada.Streams;
with Ada.Unchecked_Deallocation;

package Streams is

    -- A named access type for Stream_Element_Arrays.
    type A_SEA is access all Stream_Element_Array;

    -- Deletes the Stream_Element_Array.
    procedure Delete is new Ada.Unchecked_Deallocation( Stream_Element_Array, A_SEA );

    subtype SEO is Stream_Element_Offset;

    -- raised by streams on error
    STREAM_ERROR : exception;

end Streams;
