--
-- Copyright (c) 2018-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Debugging;                         use Debugging;
with Resources;                         use Resources;
with Resources.Scribble;
with Support.Paths;                     use Support.Paths;
with Values.Errors;                     use Values.Errors;
with Values.Strings;                    use Values.Strings;
with Version;

package body Assets.Scribble is

    -- compile with debug symbols?
    COMPILE_DEBUG : constant Boolean := Version.Is_Debug;

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Load_Value( filePath : String; group : String ) return A_Scribble_Asset is
        lpath   : constant String := Add_Extension( To_Lower( filePath ), "sv" );
        assetId : Value;
        asset   : A_Asset;
        this    : A_Scribble_Asset;
    begin
        assetId := Create( To_Lower( group ) & ":" & lpath );

        asset := Find_Asset( assetId );
        if asset /= null then
            -- found in cache
            pragma Assert( asset.Get_Type = Scribble_Assets,
                           "Cached asset '" & filePath & "' is not Scribble" );
            this := A_Scribble_Asset(asset);
        else
            -- create a new asset
            this := new Scribble_Asset;
            this.Construct( assetId, lpath, group );
            Store_Asset( A_Asset(this) );
        end if;
        return this;
    end Load_Value;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this     : access Scribble_Asset;
                         assetId  : Value'Class;
                         filePath : String;
                         group    : String ) is
    begin
        Asset(this.all).Construct( Scribble_Assets, assetId, filePath, group );
        if Length( this.diskFile ) = 0 then
            -- source file is not found; fall back to a compiled object file
            this.diskFile := To_Unbounded_String( Locate_Resource( To_String( this.filePath ) & ".sco", To_String( this.group ) ) );
        end if;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Value( this : not null access Scribble_Asset'Class ) return Value is
    begin
        if this.loaded then
           return this.val;
        end if;
        return Create( Null_Reference, "Asset not loaded" );
    end Get_Value;

    ----------------------------------------------------------------------------

    overriding
    function Load_Data( this : access Scribble_Asset ) return Boolean is
    begin
        if not Resources.Scribble.Load_Value( this.val,
                                              To_String( this.filePath ),
                                              To_String( this.group ),
                                              enableDebug => COMPILE_DEBUG )
        then
            Dbg( this.val.Err.Get_Message, D_ASSET or D_SCRIPT, Error );
            return False;
        end if;
        return True;
    end Load_Data;

    ----------------------------------------------------------------------------

    overriding
    procedure Unload_Data( this : access Scribble_Asset ) is
    begin
        this.val := Null_Value;
    end Unload_Data;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Scribble_Asset ) is
    begin
        Delete( A_Asset(this) );
    end Delete;

end Assets.Scribble;
