--
-- Copyright (c) 2018-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers;                    use Ada.Containers;
with Ada.Containers.Hashed_Sets;
with Ada.Real_Time;                     use Ada.Real_Time;
with Assets.Libraries;                  use Assets.Libraries;
with Processes;                         use Processes;
with Values.Lists;                      use Values.Lists;

package Maps.Animations is

    type Animation_Cache is new Limited_Object and Map_Listener with private;
    type A_Animation_Cache is access all Animation_Cache'Class;

    function Create_Animation_Cache return A_Animation_Cache;

    function Get_Animated_Tile( this   : not null access Animation_Cache'Class;
                                layer  : Integer;
                                col    : Integer;
                                row    : Integer;
                                tileId : out Natural ) return Boolean;

    procedure Initialize( this : not null access Animation_Cache'Class;
                          map  : A_Map;
                          lib  : Library_Ptr );

    procedure Read( this   : not null access Animation_Cache'Class;
                    stream : access Root_Stream_Type'Class );

    procedure Resize( this      : not null access Animation_Cache'Class;
                      newMap    : not null A_Map;
                      colOffset : Integer;
                      rowOffset : Integer );

    procedure Tick( this : not null access Animation_Cache'Class; time : Tick_Time );

    procedure Write( this   : not null access Animation_Cache'Class;
                     stream : access Root_Stream_Type'Class );

private

    -- There are two kinds of tile animations: looping animations and single-shot
    -- animation frames.
    --
    -- Looping animations have two or more frames and a frame delay shared by
    -- all frames. Looping animations are also globally synchronized, so all the
    -- locations in the map that use the same animated tile will simultaneously
    -- show the same frame.
    --
    -- Single-shot animations are broken up into individual frames and act more
    -- like a series of automated tile changes. A single-shot animated tile has
    -- a frame delay and a next tile to change to, upon expiration. The
    -- animation delay starts when a map tile is set to an animated frame, and
    -- there is no global synronization between frames, like looping animations.
    -- A single-shot animation can transition back to the first frame, or to a
    -- looping animation tile, or to a static tile, which ends the animation.
    --
    -- If the .frames field of the Animated_Info record has two or more frames,
    -- then the animation type is looping. Otherwise, the type is a single-shot
    -- animation and the next frame is the first element in .frames.
    type Animated_Info is
        record
            layer      : Integer;                       -- layer of animated tile
            col, row   : Integer;                       -- coordinates of animated tile
            tileId     : Natural := 0;                  -- id of animated tile
            nextUpdate : Time_Span := Time_Span_Zero;   -- next time to update the frame
            frameDelay : Time_Span := Time_Span_Zero;   -- animation frame delay
            frames     : List_Value := Null_Value.Lst;  -- frame list
        end record;

    function Animated_Info_Input( stream : access Root_Stream_Type'Class ) return Animated_Info;
    for Animated_Info'Input use Animated_Info_Input;

    procedure Animated_Info_Output( stream : access Root_Stream_Type'Class; info : Animated_Info );
    for Animated_Info'Output use Animated_Info_Output;

    -- Returns True if 'l' and 'r' share the same map location.
    function Equivalent( l, r : Animated_Info ) return Boolean
        is (l.layer = r.layer and then l.col = r.col and then l.row = r.row)
        with Inline_Always;

    -- Returns the tile id of the frame at 'index' in the animation's frame
    -- loop, or 0 if the animation isn't loop or doesn't have index 'index'.
    -- Indexes start at 1.
    function Get_Frame( ai : Animated_Info; index : Integer ) return Natural
        is (ai.frames.Get_Int( index, default => ai.tileId ))
        with Inline_Always;

    -- Returns a hash value of the Animated_Info record. Its layer may be
    -- negative, so bias the calculation to avoid underflow when casting to
    -- Hash_Type.
    function Hash( ai : Animated_Info ) return Hash_Type
        is (Hash_Type(Integer'Last / 2 + (ai.layer * MAX_WIDTH * MAX_HEIGHT) + (ai.row * MAX_WIDTH) + ai.col))
        with Inline_Always;

    -- Returns True if the animation is played as a loop. A frame list with
    -- multiple frames is looped. If the frame list is a single item, then the
    -- animation is a single-shot.
    function Is_Looping( ai : Animated_Info ) return Boolean is (ai.frames.Length > 1) with Inline_Always;

    package Animated_Set is new Ada.Containers.Hashed_Sets( Animated_Info, Hash, Equivalent, "=" );

    ----------------------------------------------------------------------------

    type Animation_Cache is new Limited_Object and Map_Listener with
        record
            map           : A_Map;
            lib           : Library_Ptr;
            counter       : Time_Span := Time_Span_Zero;
            animated      : Animated_Set.Set;
            ignoreChanges : Boolean := False;
        end record;

    procedure On_Layer_Added( this : not null access Animation_Cache; layer : Integer );

    procedure On_Layer_Deleted( this : not null access Animation_Cache; layer : Integer );

    procedure On_Layers_Swapped( this   : not null access Animation_Cache;
                                 layerA : Integer;
                                 layerB : Integer );

    procedure On_Tile_Changed( this  : not null access Animation_Cache;
                               layer : Integer;
                               col   : Integer;
                               row   : Integer );

end Maps.Animations;
