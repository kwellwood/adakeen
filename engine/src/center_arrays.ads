--
-- Copyright (c) 2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Vectors;

generic
   type Element_Type is private;

   with function "="( l, r : Element_Type ) return Boolean is <>;

package Center_Arrays is

    type Center_Array is tagged private;

    -- Appends element 'item' to the back of the array.
    procedure Append( this : in out Center_Array'Class; item : Element_Type );

    -- Clears the array, removing all elements.
    procedure Clear( this : in out Center_Array'Class );

    -- Deletes the element at 'index', decreasing the array's length and moving
    -- other elements toward center index 0, if necessary. Nothing will change
    -- if 'index' is not valid.
    procedure Delete( this : in out Center_Array'Class; index : Integer );

    -- Returns the element in the array at 'index' by copy. Raises an exception
    -- if 'index' is not valid.
    function Element( this : Center_Array'Class; index : Integer ) return Element_Type;

    -- Returns the index of the first element in the array.
    function First( this : Center_Array'Class ) return Integer;

    -- Returns the first element in the array by copy. Raises an exception if
    -- the array is empty.
    function First_Element( this : Center_Array'Class ) return Element_Type;

    -- Inserts 'item' into the array, in front of any item at 'index'. The
    -- functional form of this method returns the index of 'item' after insertion.
    --
    -- This behavior is not symmetrical. When inserting before index -1, for
    -- example, the item will enter the array at index -2 and push the previous
    -- element at -2 away from index 0. However, when inserting in front of a
    -- positive index, +1 for example, the new item will enter the array at
    -- index +1 and elements at greater indices will be pushed away from 0.
    --
    -- Example:      Insert 'X' before 1
    --
    --   [A][B][C][D]        ==>        [A][B][X][C][D]
    --   -1  0  1  2                    -1  0  1  2  3
    --                                         ^
    --
    -- Example:      Insert 'Y' before -1
    --
    --   [A][B][C][D]        ==>        [Y][A][B][C][D]
    --   -1  0  1  2                    -2 -1  0  1  2
    --                                   *
    --
    -- The first item inserted into an empty array will always be at index 0.
    -- Once index 0 has been populated, it cannot be displaced by subsequent
    -- insertions; the special case of attempting to insert again before index 0
    -- will insert in front of it, into index -1.
    function Insert_Before( this : in out Center_Array'Class; before : Integer; item : Element_Type ) return Integer;
    procedure Insert_Before( this : in out Center_Array'Class; before : Integer; item : Element_Type );

    -- Inserts 'item' into the array at index 'index', increasing the array's
    -- length and pushing other elements away from center index 0, if necessary.
    -- This insertion method provides symmetrical behavior, so inserting at
    -- index 1 puts the item at index 1, and inserting at index -1 puts the item
    -- at index -1. The functional form of this method returns the index of
    -- 'item' after insertion.
    --
    -- Example:        Insert 'X' at 1
    --
    --   [A][B][C][D]        ==>        [A][B][X][C][D]
    --   -1  0  1  2                    -1  0  1  2  3
    --                                         ^
    --
    -- Example:        Insert 'Y' at -1
    --
    --   [A][B][C][D]        ==>        [A][Y][B][C][D]
    --   -1  0  1  2                    -2 -1  0  1  2
    --                                      *
    --
    -- The first item inserted into an empty array will always be at index 0.
    -- Once index 0 has been populated, it cannot be displaced by subsequent
    -- insertions; the special case of attempting to insert again at index 0
    -- will instead insert in front of it, at index -1.
    --
    -- Inserting an item before First or after Last will prepend or append the
    -- item, respectively.
    function Insert_Symmetric( this : in out Center_Array'Class; index : Integer; item : Element_Type ) return Integer;
    procedure Insert_Symmetric( this : in out Center_Array'Class; index : Integer; item : Element_Type );

    -- Returns True when the array has a length of zero.
    function Is_Empty( this : Center_Array'Class ) return Boolean;

    -- Returns the index of the last element in the array. Returns -1 when empty.
    function Last( this : Center_Array'Class ) return Integer;

    -- Returns the last element in the array by copy. Raises an exception if the
    -- array is empty.
    function Last_Element( this : Center_Array'Class ) return Element_Type;

    -- Returns the length of the array, in elements.
    function Length( this : Center_Array'Class ) return Natural;

    -- Prepends 'item' to the front of the array.
    procedure Prepend( this : in out Center_Array'Class; item : Element_Type );

    type Reference_Type(element : not null access Element_Type) is private
        with Implicit_Dereference => element;

    -- Returns a reference to the element at index 'index'. Raises an exception
    -- if the index is not valid.
    function Reference( this : aliased in out Center_Array; index : Integer ) return Reference_Type;

    -- Replaces the element at index 'index' with item. Raises an exception if
    -- 'index' is not valid.
    procedure Replace( this : in out Center_Array'Class; index : Integer; item : Element_Type );

    -- Moves the center of the array (index 0) to what is currently at 'index'.
    -- If 'index' is not valid, nothing will change.
    procedure Set_Center( this : in out Center_Array'Class; index : Integer );

    -- Swaps the element at 'indexA' with the element at 'indexB'. Nothing will
    -- change if either index is invalid.
    procedure Swap( this : in out Center_Array'Class; indexA, indexB : Integer );

private

    pragma Inline( Append );
    pragma Inline( Element );
    pragma Inline( First );
    pragma Inline( First_Element );
    pragma Inline( Is_Empty );
    pragma Inline( Last );
    pragma Inline( Last_Element );
    pragma Inline( Prepend );
   pragma Inline (Reference);

    package Item_Vectors is new Ada.Containers.Vectors( Natural, Element_Type, "=" );

    type Center_Array is tagged
        record
            items  : Item_Vectors.Vector;
            middle : Integer := 0;
        end record;

    type Reference_Type( element : not null access Element_Type ) is null record;

end Center_Arrays;
