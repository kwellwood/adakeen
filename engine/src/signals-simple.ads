--
-- Copyright (c) 2015-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Signals.Simple is

    ----------------------------------------------------------------------------
    -- String_Connection Class --

    type String_Connection is abstract new Base_Connection with null record;

    not overriding
    procedure Emit( this   : String_Connection;
                    sender : access Base_Object'Class;
                    str    : String ) is abstract;

    ----------------------------------------------------------------------------
    -- String_Signal Class --

    type String_Signal is new Base_Signal with null record;

    procedure Connect( this : in out String_Signal'Class; slot : String_Connection'Class );

    procedure Emit( this : String_Signal'Class; str : String );

    ----------------------------------------------------------------------------
    -- Signals.Simple.Connections --

    generic
        type Target (<>) is abstract limited new Base_Object with private;
    package Connections is

        ------------------------------------------------------------------------
        -- String_Connection

        type A_String_Slot is access
            procedure( object : not null access Target'Class; str : String );

        function Slot( obj      : in out Target'Class;
                       method   : not null A_String_Slot;
                       priority : Priority_Type := Normal ) return String_Connection'Class;

        function Slot( obj      : not null access Target'Class;
                       method   : not null A_String_Slot;
                       priority : Priority_Type := Normal ) return String_Connection'Class is (Slot( obj.all, method, priority ));

    private

        ------------------------------------------------------------------------
        -- String_Connection

        type Generic_String_Connection is new String_Connection with
            record
                object : access Target'Class := null;
                method : A_String_Slot := null;
            end record;

        overriding
        procedure Emit( this    : Generic_String_Connection;
                        sender : access Base_Object'Class;
                        str    : String );

        overriding
        function Eq( this : Generic_String_Connection; other : Base_Connection'Class ) return Boolean;

        overriding
        function Get_Object( this : Generic_String_Connection ) return access Base_Object'Class is (this.object);

    end Connections;

end Signals.Simple;
