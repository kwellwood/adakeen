--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;
with Ada.Streams;                       use Ada.Streams;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Equal_Case_Insensitive;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Interfaces;                        use Interfaces;

package Support is

    function "*"( i : Integer; f : Float ) return Float is (Float(i) * f) with Inline;

    function "*"( f : Float; i : Integer ) return Float is (Float(i) * f) with Inline;

    function "/"( i : Integer; f : Float ) return Float is (Float(i) / f) with Inline;

    function "/"( f : Float; i : Integer ) return Float is (f / Float(i)) with Inline;

    function "-"( f : Float; i : Integer ) return Float is (f - Float(i)) with Inline;

    function "-"( i : Integer; f : Float ) return Float is (Float(i) - f) with Inline;

    function "+"( f : Float; i : Integer ) return Float is (f + Float(i)) with Inline;

    function "+"( i : Integer; f : Float ) return Float is (Float(i) + f) with Inline;

    function "**"( l, r : Long_Float ) return Long_Float;

    function Constrain( val, min, max : Float ) return Float with Inline;

    function Constrain( val, min, max : Integer ) return Integer with Inline;

    function Div_Ceil( a, b : Integer ) return Integer with Inline;

    -- Returns the value of 'point' as it would be snapped to a series of
    -- 'stepSize' size. If 'centered' is True, the snap will occur in the middle
    -- a step instead of on them.
    function Snap( point    : Float;
                   stepSize : Float;
                   centered : Boolean := False ) return Float with Inline;

    function Trunc( x : Float ) return Integer is (Integer(Float'Truncation( x ))) with Inline;

    function Fmod( x, y : Float ) return Float with Inline;

    function Fmod( x, y : Long_Float ) return Long_Float with Inline;

    function Max( a, b : Integer ) return Integer is (Integer'Max( a, b )) with Inline;

    function Max( a, b : Float ) return Float is (Float'Max( a, b )) with Inline;

    function Min( a, b : Integer ) return Integer is (Integer'Min( a, b )) with Inline;

    function Min( a, b : Float ) return Float is (Float'Min( a, b )) with Inline;

    procedure Swap( a, b : in out Float );

    ----------------------------------------------------------------------------

    function "&"( l : String; r : Unbounded_String ) return String is (l & To_String( r )) with Inline;

    function "&"( l : Unbounded_String; r : String ) return String is (To_String( l ) & r) with Inline;

    -- Capitalizes the first character in 'str'.
    function Capitalize_First( str : String ) return String;
    pragma Postcondition( Capitalize_First'Result'Length = str'Length );

    -- Capitalizes the words in the string using a set of delimiters.
    function Capitalize( str : String ) return String;
    pragma Postcondition( Capitalize'Result'Length = str'Length );

    -- Compares two strings; case insensitive.
    function Case_Eq( l, r : String ) return Boolean renames Ada.Strings.Equal_Case_Insensitive;

    -- Compares two strings; case insensitive.
    function Case_Eq( l, r : Unbounded_String ) return Boolean is (Case_Eq( To_String( l ), To_String( r ) )) with Inline;

    -- Compares two strings; case insensitive.
    function Case_Eq( l : Unbounded_String; r : String ) return Boolean is (Case_Eq( To_String( l ), r )) with Inline;

    -- Compares two strings; case insensitive.
    function Case_Eq( l : String; r : Unbounded_String ) return Boolean is (Case_Eq( l, To_String( r ) )) with Inline;

    -- Returns the native platform's end-of-line sequence.
    function EOL return String;

    -- Returns True if 'str' ends with 'ending'. Comparison is case sensitive.
    function Ends_With( str : String; ending : String ) return Boolean with Inline;

    -- Iterate over words in a string separated by whitespace.
    procedure Iterate_Words( phrase  : String;
                             examine : access procedure( word : String ) );

    -- Replaces all 'from' characters in a string with 'to' characters.
    function Replace( str : String; from, to : Character ) return String;

    function Lower( str : String ) return String renames Ada.Characters.Handling.To_Lower;

    function Upper( str : String ) return String renames Ada.Characters.Handling.To_Upper;

    ----------------------------------------------------------------------------

    -- Returns -1 if the file does not exist.
    function File_Length( path : String ) return Long_Integer;
    pragma Postcondition( File_Length'Result >= -1 );

    ----------------------------------------------------------------------------

    -- Extracts a readable Ada unit name from a source reference line. If the
    -- source reference doesn't contain symbols then an empty string will be
    -- returned because the unit name can't be determined.
    function Source_Ref_To_Unit_Name( ref : String ) return String;

    ----------------------------------------------------------------------------

    -- Returns a string representation of 'i' without whitespace.
    function Image( i : Integer ) return String is (Trim( Integer'Image( i ), Left )) with Inline;
    pragma Postcondition( Image'Result'Length > 0 );

    -- Returns a string representation of 'i' without whitespace.
    function Image( i : Long_Long_Integer ) return String is (Trim( Long_Long_Integer'Image( i ), Left )) with Inline;
    pragma Postcondition( Image'Result'Length > 0 );

    -- Returns a string representation of 'u' without whitespace.
    function Image( u : Unsigned_8 ) return String is (Trim( Unsigned_8'Image( u ), Left )) with Inline;
    pragma Postcondition( Image'Result'Length > 0 );

    -- Returns a string representation of 'u' without whitespace.
    function Image( u : Unsigned_16 ) return String is (Trim( Unsigned_16'Image( u ), Left )) with Inline;
    pragma Postcondition( Image'Result'Length > 0 );

    -- Returns a string representation of 'u' without whitespace.
    function Image( u : Unsigned_32 ) return String is (Trim( Unsigned_32'Image( u ), Left )) with Inline;
    pragma Postcondition( Image'Result'Length > 0 );

    -- Returns a string representation of 'u' without whitespace.
    function Image( u : Unsigned_64 ) return String is (Trim( Unsigned_64'Image( u ), Left )) with Inline;
    pragma Postcondition( Image'Result'Length > 0 );

    -- Returns a string image of a floating point number, where 'precision' is
    -- the number of places after the decimal to render.
    function Image( f : Float; precision : Natural := 3 ) return String with Inline;
    pragma Postcondition( Image'Result'Length > 0 );

    ----------------------------------------------------------------------------

    -- Returns a random unsigned 32-bit number.
    function Random_32 return Unsigned_32;

    -- Returns a random number between 0 and 1.
    function Random_Float return Float;

    ----------------------------------------------------------------------------

    type Boolean_Array is array (Positive range <>) of Boolean;

    ----------------------------------------------------------------------------

    -- Reads a string written with Write_String16 without overflowing the stack
    -- for large strings. The maximum length of the string is described with a
    -- 16-bit integer.
    function Read_String_16( stream : access Root_Stream_Type'Class ) return Unbounded_String;

    -- Reads a string written with Write_String32 without overflowing the stack
    -- for large strings. The maximum length of the string is described with a
    -- 32-bit integer.
    function Read_String_32( stream : access Root_Stream_Type'Class ) return Unbounded_String;

    -- Writes a string to a stream with only the length preceeding it, instead
    -- of the full bounds. The length of the string is described with a 16-bit
    -- integer.
    procedure Write_String_16( stream : access Root_Stream_Type'Class; str : Unbounded_String );

    -- Writes a string to a stream with only the length preceeding it, instead
    -- of the full bounds. The length of the string is described with a 32-bit
    -- integer.
    procedure Write_String_32( stream : access Root_Stream_Type'Class; str : Unbounded_String );

private

    -- A package for functions with OS-specific implementations
    package OS is

        -- Returns the path of the directory where system-wide
        -- application-specific data files can be read and written.
        function App_Data_Directory return String;

        -- Attempts to open the file 'filePath' with the file type's default
        -- application.
        procedure Auto_Open( filePath : String );

        -- Returns the native end-of-line sequence.
        function EOL return String;

        -- Returns the file extension for executable files, not including a dot.
        function Executable_Extension return String;

        -- Returns the path of the application's executable file.
        function Executable_Path return String;

        -- Returns the directory containing the executable file, or, if the executable is
        -- running on OS X inside an .app bundle, the bundle's Resources directory.
        function Execution_Directory return String;

        -- Returns the path of the user's home directory.
        function Home_Directory return String;

        -- Opens 'path' in the OS GUI and selects it if it points to an
        -- existing file. Nothing happens if the directory of 'path' does not
        -- exist.
        procedure Reveal_Path( path : String );

        -- Returns the directory for system fonts on the local machine.
        function System_Font_Directory return String;

        -- Returns the current platform's temporary directory.
        function Temp_Directory return String;

    end OS;

    pragma Import( C, "**", "pow" );

end Support;
