--
-- Copyright (c) 2012-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Color;                     use Allegro.Color;

package Clipping.Drawing is

    -- Draws the clipping shape of a tile at x, y on the drawing target. The
    -- clipping shape is determined by the tile's clipping type in 'cType'.
    -- 'size' is the width/height of the tile in pixels.
    procedure Draw( cType   : Clip_Type;
                    x, y, z : Float;
                    size    : Positive;
                    color1,
                    color2  : Allegro_Color );

end Clipping.Drawing;
