--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Resources;                         use Resources;

package Assets.Archives is

    -- An Archive is a data file containing individually addressable files. The
    -- Archive class should be subclassed for each archive file format supported
    -- by the application.
    type Archive is abstract new Asset with private;
    type A_Archive is access all Archive'Class;

    -- Loads an archive file from 'filePath' in resource group 'group', located
    -- according to the standard resource search rules. If 'keepLoaded' is True,
    -- the archive will remain loaded for future use until the application
    -- exits.
    --
    -- An Archive will always be returned, but it will not be loaded if the file
    -- format is damaged or unrecognized.
    function Load_Archive( filePath : String; group : String; keepLoaded : Boolean ) return A_Archive;

    -- Returns true if file 'filePath' exists in the Archive.
    not overriding
    function File_Exists( this     : access Archive;
                          filePath : String ) return Boolean is abstract;

    -- Reads file 'filePath' from the Archive and returns it as a Resource file.
    -- The caller takes ownership of the returned object; closing the archive
    -- does not affect it. Returns null if 'filePath' does not exist or cannot
    -- be read.
    not overriding
    function Load_File( this : access Archive; filePath : String ) return A_Resource_File is abstract;

    -- Searches the archive for entry names matching 'wildcard', calling
    -- 'process' once for each matching entry.
    not overriding
    procedure Search( this     : access Archive;
                      wildcard : String;
                      process  : access procedure( entryName : String ) ) is abstract;

    -- Deletes the Archive.
    procedure Delete( this : in out A_Archive );
    pragma Postcondition( this = null );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Prototype for a function that loads an archive. The archive file will be
    -- located according to the standard resource search rules.
    --
    -- This will always return an Archive instance, but it will not be loaded if
    -- an error occurred.
    type A_Archive_Loader is
        access function( id : Value; filePath : String; group : String ) return A_Archive;

    -- Registers support for an archive file format by file extension. 'ext' is
    -- not case sensitive, should not contain a leading dot character, and may
    -- only be registered once.
    --
    -- Once a file extension has been registered, Load_Archive() will be able to
    -- load archive files with the given extension.
    procedure Register_Format( ext : String; loader : not null A_Archive_Loader );

private

    type Archive is abstract new Asset with null record;

    procedure Construct( this     : access Archive;
                         assetId  : Value'Class;
                         filePath : String;
                         group    : String );

end Assets.Archives;
