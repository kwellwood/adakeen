--
-- Copyright (C) 2001 Robert Penner
-- All Rights Reserved.
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- https://opensource.org/licenses/MIT
--
-- Source ported from https://github.com/warrenm/AHEasing
-- Retrieved 2018-05-05
--

with Ada.Numerics;                      use Ada.Numerics;
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;

package body Easing is

    type Easing_Array is array (Easing_Type) of A_Easing_Function;

    functions : constant Easing_Array :=
        (
            Linear        => LinearInterpolation'Access,
            Quad_In       => QuadraticEaseIn'Access,
            Quad_Out      => QuadraticEaseOut'Access,
            Quad_InOut    => QuadraticEaseInOut'Access,
            Cubic_In      => CubicEaseIn'Access,
            Cubic_Out     => CubicEaseOut'Access,
            Cubic_InOut   => CubicEaseInOut'Access,
            Quart_In      => QuarticEaseIn'Access,
            Quart_Out     => QuarticEaseOut'Access,
            Quart_InOut   => QuarticEaseInOut'Access,
            Quint_In      => QuinticEaseIn'Access,
            Quint_Out     => QuinticEaseOut'Access,
            Quint_InOut   => QuinticEaseInOut'Access,
            Sine_In       => SineEaseIn'Access,
            Sine_Out      => SineEaseOut'Access,
            Sine_InOut    => SineEaseInOut'Access,
            Circ_In       => CircularEaseIn'Access,
            Circ_Out      => CircularEaseOut'Access,
            Circ_InOut    => CircularEaseInOut'Access,
            Expo_In       => ExponentialEaseIn'Access,
            Expo_Out      => ExponentialEaseOut'Access,
            Expo_InOut    => ExponentialEaseInOut'Access,
            Elastic_In    => ElasticEaseIn'Access,
            Elastic_Out   => ElasticEaseOut'Access,
            Elastic_InOut => ElasticEaseInOut'Access,
            Back_In       => BackEaseIn'Access,
            Back_Out      => BackEaseOut'Access,
            Back_InOut    => BackEaseInOut'Access,
            Bounce_In     => BounceEaseIn'Access,
            Bounce_Out    => BounceEaseOut'Access,
            Bounce_InOut  => BounceEaseInOut'Access,
            Impulse_In    => ImpulseEaseIn'Access,
            Impulse_Out   => ImpulseEaseOut'Access,
            Impulse_InOut => ImpulseEaseInOut'Access
        );

    function Ease( t : Easing_Type; p : Float ) return Float is (functions(t).all( p ));

    ----------------------------------------------------------------------------

    type Easing_Type_Array is array (Easing_Type) of Easing_Type;

    opposites : constant Easing_Type_Array :=
        (
            Linear        => Linear,
            Quad_In       => Quad_Out,
            Quad_Out      => Quad_In,
            Quad_InOut    => Quad_InOut,
            Cubic_In      => Cubic_Out,
            Cubic_Out     => Cubic_In,
            Cubic_InOut   => Cubic_InOut,
            Quart_In      => Quart_Out,
            Quart_Out     => Quart_In,
            Quart_InOut   => Quart_InOut,
            Quint_In      => Quint_Out,
            Quint_Out     => Quint_In,
            Quint_InOut   => Quint_InOut,
            Sine_In       => Sine_Out,
            Sine_Out      => Sine_In,
            Sine_InOut    => Sine_InOut,
            Circ_In       => Circ_Out,
            Circ_Out      => Circ_In,
            Circ_InOut    => Circ_InOut,
            Expo_In       => Expo_Out,
            Expo_Out      => Expo_In,
            Expo_InOut    => Expo_InOut,
            Elastic_In    => Elastic_Out,
            Elastic_Out   => Elastic_In,
            Elastic_InOut => Elastic_InOut,
            Back_In       => Back_Out,
            Back_Out      => Back_In,
            Back_InOut    => Back_InOut,
            Bounce_In     => Bounce_Out,
            Bounce_Out    => Bounce_In,
            Bounce_InOut  => Bounce_InOut,
            Impulse_In    => Impulse_Out,
            Impulse_Out   => Impulse_In,
            Impulse_InOut => Impulse_InOut
        );

    function Opposite( t : Easing_Type ) return Easing_Type is (opposites(t));

    ----------------------------------------------------------------------------

    function Use_Easing( t : Easing_Type; notReversed : Boolean := True ) return Easing_Type is (if notReversed then t else Opposite( t ));

    ----------------------------------------------------------------------------

    -- Modeled after the line y = x
    function LinearInterpolation( p : Float ) return Float is (p);

    ----------------------------------------------------------------------------

    -- Modeled after the parabola y = x^2
    function QuadraticEaseIn( p : Float ) return Float is (p * p);

    ----------------------------------------------------------------------------

    -- Modeled after the parabola y = -x^2 + 2x
    function QuadraticEaseOut( p : Float ) return Float is (-(p * (p - 2.0)));

    ----------------------------------------------------------------------------

    -- Modeled after the piecewise quadratic
    -- y = (1/2)((2x)^2)             ; [0, 0.5)
    -- y = -(1/2)((2x-1)*(2x-3) - 1) ; [0.5, 1]
    function QuadraticEaseInOut( p : Float ) return Float is
    begin
        if p < 0.5 then
            return 2.0 * p * p;
        else
            return (-2.0 * p * p) + (4.0 * p) - 1.0;
        end if;
    end QuadraticEaseInOut;

    ----------------------------------------------------------------------------

    -- Modeled after the cubic x^3
    function CubicEaseIn( p : Float ) return Float is (p * p * p);

    ----------------------------------------------------------------------------

    -- Modeled after the cubic y = (x - 1)^3 + 1
    function CubicEaseOut( p : Float ) return Float is
        f : constant Float := p - 1.0;
    begin
        return f * f * f + 1.0;
    end CubicEaseOut;

    ----------------------------------------------------------------------------

    function CubicEaseInOut( p : Float ) return Float is
        f : Float;
    begin
        if p < 0.5 then
            return 4.0 * p * p * p;
        else
            f := (2.0 * p) - 2.0;
            return 0.5 * f * f * f + 1.0;
        end if;
    end CubicEaseInOut;

    ----------------------------------------------------------------------------

    -- Modeled after the quartic x^4
    function QuarticEaseIn( p : Float ) return Float is (p * p * p * p);

    ----------------------------------------------------------------------------

    -- Modeled after the quartic y = 1 - (x - 1)^4
    function QuarticEaseOut( p : Float ) return Float is
        f : constant Float := p - 1.0;
    begin
        return f * f * f * (1.0 - p) + 1.0;
    end QuarticEaseOut;

    ----------------------------------------------------------------------------

    -- Modeled after the piecewise quartic
    -- y = (1/2)((2x)^4)        ; [0, 0.5)
    -- y = -(1/2)((2x-2)^4 - 2) ; [0.5, 1]
    function QuarticEaseInOut( p : Float ) return Float is
        f : Float;
    begin
        if p < 0.5 then
            return 8.0 * p * p * p * p;
        else
            f := p - 1.0;
            return -8.0 * f * f * f * f + 1.0;
        end if;
    end QuarticEaseInOut;

    ----------------------------------------------------------------------------

    -- Modeled after the quintic y = x^5
    function QuinticEaseIn( p : Float ) return Float is (p * p * p * p * p);

    ----------------------------------------------------------------------------

    -- Modeled after the quintic y = (x - 1)^5 + 1
    function QuinticEaseOut( p : Float ) return Float is
        f : constant Float := p - 1.0;
    begin
        return f * f * f * f * f + 1.0;
    end QuinticEaseOut;

    ----------------------------------------------------------------------------

    -- Modeled after the piecewise quintic
    -- y = (1/2)((2x)^5)       ; [0, 0.5)
    -- y = (1/2)((2x-2)^5 + 2) ; [0.5, 1]
    function QuinticEaseInOut( p : Float ) return Float is
        f : Float;
    begin
        if p < 0.5 then
            return 16.0 * p * p * p * p * p;
        else
            f := (2.0 * p) - 2.0;
            return  0.5 * f * f * f * f * f + 1.0;
        end if;
    end QuinticEaseInOut;

    ----------------------------------------------------------------------------

    -- Modeled after quarter-cycle of sine wave
    function SineEaseIn( p : Float ) return Float is (Sin( (p - 1.0) * (Pi / 2.0) ) + 1.0);

    ----------------------------------------------------------------------------

    -- Modeled after quarter-cycle of sine wave (different phase)
    function SineEaseOut( p : Float ) return Float is (Sin( p * (Pi / 2.0) ));

    ----------------------------------------------------------------------------

    -- Modeled after half sine wave
    function SineEaseInOut( p : Float ) return Float is (0.5 * (1.0 - Cos( p * Pi )));

    ----------------------------------------------------------------------------

    -- Modeled after shifted quadrant IV of unit circle
    function CircularEaseIn( p : Float ) return Float is ( 1.0 - Sqrt( 1.0 - (p * p) ));

    ----------------------------------------------------------------------------

    -- Modeled after shifted quadrant II of unit circle
    function CircularEaseOut( p : Float ) return Float is (Sqrt( (2.0 - p) * p ));

    ----------------------------------------------------------------------------

    -- Modeled after the piecewise circular function
    -- y = (1/2)(1 - sqrt(1 - 4x^2))           ; [0, 0.5)
    -- y = (1/2)(sqrt(-(2x - 3)*(2x - 1)) + 1) ; [0.5, 1]
    function CircularEaseInOut( p : Float ) return Float is
    begin
        if p < 0.5 then
            return 0.5 * (1.0 - Sqrt( 1.0 - 4.0 * (p * p) ));
        else
            return 0.5 * (Sqrt( -((2.0 * p) - 3.0) * ((2.0 * p) - 1.0) ) + 1.0);
        end if;
    end CircularEaseInOut;

    ----------------------------------------------------------------------------

    -- Modeled after the exponential function y = 2^(10(x - 1))
    function ExponentialEaseIn( p : Float ) return Float is
    begin
        if p = 0.0 then
            return p;
        else
            return 2.0 ** (10.0 * (p - 1.0));
        end if;
    end ExponentialEaseIn;

    ----------------------------------------------------------------------------

    -- Modeled after the exponential function y = -2^(-10x) + 1
    function ExponentialEaseOut( p : Float ) return Float is
    begin
        if p = 1.0 then
            return p;
        else
            return 1.0 - 2.0 ** (-10.0 * p);
        end if;
    end ExponentialEaseOut;

    ----------------------------------------------------------------------------

    -- Modeled after the piecewise exponential
    -- y = (1/2)2^(10(2x - 1))         ; [0,0.5)
    -- y = -(1/2)*2^(-10(2x - 1))) + 1 ; [0.5,1]
    function ExponentialEaseInOut( p : Float ) return Float is
    begin
        if p = 0.0 or p = 1.0 then
            return p;
        end if;

        if p < 0.5 then
            return 0.5 * (2.0 ** ((20.0 * p) - 10.0));
        else
            return -0.5 * (2.0 ** ((-20.0 * p) + 10.0)) + 1.0;
        end if;
    end ExponentialEaseInOut;

    ----------------------------------------------------------------------------

    -- Modeled after the damped sine wave y = sin(13pi/2*x)*pow(2, 10 * (x - 1))
    function ElasticEaseIn( p : Float ) return Float is (Sin( 13.0 * (Pi / 2.0) * p ) * (2.0 ** (10.0 * (p - 1.0))));

    ----------------------------------------------------------------------------

    -- Modeled after the damped sine wave y = sin(-13pi/2*(x + 1))*pow(2, -10x) + 1
    function ElasticEaseOut( p : Float ) return Float
    is (Sin( -13.0 * (Pi / 2.0) * (p + 1.0) ) * (2.0 ** (-10.0 * p)) + 1.0);

    ----------------------------------------------------------------------------

    -- Modeled after the piecewise exponentially-damped sine wave:
    -- y = (1/2)*sin(13pi/2*(2*x))*pow(2, 10 * ((2*x) - 1))      ; [0,0.5)
    -- y = (1/2)*(sin(-13pi/2*((2x-1)+1))*pow(2,-10(2*x-1)) + 2) ; [0.5, 1]
    function ElasticEaseInOut( p : Float ) return Float is
    begin
        if p < 0.5 then
            return 0.5 * Sin( 13.0 * (Pi / 2.0) * (2.0 * p) ) * (2.0 ** (10.0 * ((2.0 * p) - 1.0)));
        else
            return 0.5 * (Sin( -13.0 * (Pi / 2.0) * ((2.0 * p - 1.0) + 1.0) ) * (2.0 ** (-10.0 * (2.0 * p - 1.0))) + 2.0);
        end if;
    end ElasticEaseInOut;

    ----------------------------------------------------------------------------

    -- Modeled after the overshooting cubic y = x^3-x*sin(x*pi)
    function BackEaseIn( p : Float ) return Float is (p * p * p - p * Sin( p * Pi ));

    ----------------------------------------------------------------------------

    -- Modeled after overshooting cubic y = 1-((1-x)^3-(1-x)*sin((1-x)*pi))
    function BackEaseOut( p : Float ) return Float is
        f : constant Float := 1.0 - p;
    begin
        return 1.0 - (f * f * f - f * Sin( f * Pi ));
    end BackEaseOut;

    ----------------------------------------------------------------------------

    -- Modeled after the piecewise overshooting cubic function:
    -- y = (1/2)*((2x)^3-(2x)*sin(2*x*pi))           ; [0, 0.5)
    -- y = (1/2)*(1-((1-x)^3-(1-x)*sin((1-x)*pi))+1) ; [0.5, 1]
    function BackEaseInOut( p : Float ) return Float is
        f : Float;
    begin
        if p < 0.5 then
            f := 2.0 * p;
            return 0.5 * (f * f * f - f * Sin( f * Pi ));
        else
            f := 1.0 - (2.0 * p - 1.0);
            return 0.5 * (1.0 - (f * f * f - f * Sin( f * Pi ))) + 0.5;
        end if;
    end BackEaseInOut;

    ----------------------------------------------------------------------------

    function BounceEaseIn( p : Float ) return Float is (1.0 - BounceEaseOut( 1.0 - p ));

    ----------------------------------------------------------------------------

    function BounceEaseOut( p : Float ) return Float is
    begin
        if p < 4.0 / 11.0 then
            return (121.0 * p * p) / 16.0;
        elsif p < 8.0 / 11.0 then
            return (363.0 / 40.0 * p * p) - (99.0 / 10.0 * p) + 17.0 / 5.0;
        elsif p < 9.0 / 10.0 then
            return (4356.0 / 361.0 * p * p) - (35442.0 / 1805.0 * p) + 16061.0 / 1805.0;
        else
            return (54.0 / 5.0 * p * p) - (513.0 / 25.0 * p) + 268.0 / 25.0;
        end if;
    end BounceEaseOut;

    ----------------------------------------------------------------------------

    function BounceEaseInOut( p : Float ) return Float is
    begin
        if p < 0.5 then
            return 0.5 * BounceEaseIn( p * 2.0 );
        else
            return 0.5 * BounceEaseOut( p * 2.0 - 1.0 ) + 0.5;
        end if;
    end BounceEaseInOut;

    ----------------------------------------------------------------------------

    -- Written by Kevin Wellwood
    function ImpulseEaseIn( p : Float ) return Float is
    begin
        return Float'Floor( p );
    end ImpulseEaseIn;

    ----------------------------------------------------------------------------

    -- Written by Kevin Wellwood
    function ImpulseEaseOut( p : Float ) return Float is
    begin
        return Float'Ceiling( p );
    end ImpulseEaseOut;

    ----------------------------------------------------------------------------

    -- Written by Kevin Wellwood
    function ImpulseEaseInOut( p : Float ) return Float is
    begin
        return Float'Rounding( p );
    end ImpulseEaseInOut;

end Easing;
