--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Indefinite_Ordered_Maps;
with Ada.Containers.Vectors;
with Allegro.Displays;                  use Allegro.Displays;
with Bin_Packing;                       use Bin_Packing;

package Tiles.Atlases is

    type Atlas is new Limited_Object with private;
    type A_Atlas is access all Atlas'Class;

    -- Creates a new Atlas with pages of the specified size but no larger than
    -- the largest texture size supported by the display.
    function Create_Atlas( display    : not null A_Allegro_Display;
                           pageWidth,
                           pageHeight : Positive ) return A_Atlas;
    pragma Postcondition( Create_Atlas'Result /= null );

    -- Adds a bitmap 'bmp' to the Atlas, loaded from unique file 'filename'.
    -- True will be returned on success, or false if 'bmp' is too large to fit
    -- or 'filename' has already been added.
    function Add_Bitmap( this     : not null access Atlas'Class;
                         filename : String;
                         bmp      : not null A_Allegro_Bitmap ) return Boolean;

    -- Returns a bitmap, or a portion of a bitmap, cached as 'filename'.
    function Get_Bitmap( this          : not null access Atlas'Class;
                         filename      : String;
                         x, y          : Integer := 0;
                         width, height : Integer := 0 ) return A_Allegro_Bitmap;

    -- Saves each page of the atlas as a bitmap in the current directory, using
    -- 'name' as the filename prefix and appending the page number. The bitmaps
    -- will be saved in .png format.
    procedure Save_Pages( this : not null access Atlas'Class; name : String );

    -- Deletes the Atlas. Make sure all sub-bitmap references previously
    -- returned from the atlas have been destroyed first or they will become
    -- invalid.
    procedure Delete( this : in out A_Atlas );

private

    type Page is
        record
            bmp  : A_Allegro_Bitmap;
            bin  : A_Bin;
            full : Boolean := False;
        end record;
    type A_Page is access all Page;

    package Page_Lists is new Ada.Containers.Vectors( Positive, A_Page, "=" );

    type Bitmap_Pos is
        record
            page : Integer;
            x, y : Integer;
            w, h : Integer;
        end record;

    package Bitmap_Maps is new Ada.Containers.Indefinite_Ordered_Maps( String, Bitmap_Pos, "<", "=" );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Atlas is new Limited_Object with
        record
            display    : A_Allegro_Display := null;
            pageWidth,
            pageHeight : Natural := 0;
            pages      : Page_Lists.Vector;
            filenames  : Bitmap_Maps.Map;
        end record;

    -- Constructs the Atlas with pages of 'pageWidth' x 'pageHeight' but no
    -- larger than the largest texture size supported by the display.
    procedure Construct( this       : in out Atlas;
                         display    : not null A_Allegro_Display;
                         pageWidth,
                         pageHeight : Positive );

    procedure Delete( this : in out Atlas );

    -- Adds a new Page to the Atlas.
    procedure Add_Page( this : not null access Atlas'Class );

end Tiles.Atlases;
