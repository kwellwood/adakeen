--
-- Copyright (c) 2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Color;                     use Allegro.Color;
with Palette;                           use Palette;

package Values.Allegro_Colors is

    -- Casts 'val' to an Allegro_Color, or returns 'default' if 'val' is not a
    -- color value.
    function Cast_Color( val : Value'Class; default : Allegro_Color := Black ) return Allegro_Color with Inline;

    -- Attempts to coerce 'val' into a color. Supported value types are colors
    -- and strings (in html #rrggbb[aa]" format). 'default' will be returned if
    -- 'val' could not be coerced.
    function Coerce_Color( val : Value'Class; default : Allegro_Color := Black ) return Allegro_Color;

    -- Creates a new color value from Allegro_Color 'color'.
    function Create( color : Allegro_Color ) return Value;

    -- Returns the value as an Allegro_Color.
    function To_Color( this : Value'Class ) return Allegro_Color with Inline;
    pragma Precondition( this.Is_Color );

end Values.Allegro_Colors;
