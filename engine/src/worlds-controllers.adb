--
-- Copyright (c) 2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Events.Manager;                    use Events.Manager;
with Events.World;                      use Events.World;
with Values.Casting;                    use Values.Casting;

package body Worlds.Controllers is

    -- Handles a Create_Layer event, creating a new foreground or background
    -- layer in the world map.
    procedure Handle_Create_Layer( this : not null access World_Controller'Class;
                                   evt  : not null A_Layer_Create_Event );

    -- Handles a Delete_Layer event, deleting a layer in the world map.
    procedure Handle_Delete_Layer( this : not null access World_Controller'Class;
                                   evt  : not null A_Layer_Event );

    -- Handles a Set_Layer_Property event, changing the value of a named property
    -- on a layer in the world map.
    procedure Handle_Set_Layer_Property( this : not null access World_Controller'Class;
                                         evt  : not null A_Layer_Property_Event );

    -- Handles a Set_Tile event, changing a single tile in the world map.
    procedure Handle_Set_Tile( this : not null access World_Controller'Class;
                               evt  : not null A_Tile_Event );

    -- Handles a Set_World_Property event, changing the value of a named property
    -- of the world.
    procedure Handle_Set_World_Property( this : not null access World_Controller'Class;
                                         evt  : not null A_World_Property_Event );

    -- Handles a Swap_Layers event, swapping the order of two layers in the
    -- world map.
    procedure Handle_Swap_Layers( this : not null access World_Controller'Class;
                                  evt  : not null A_Layer_Swap_Event );

    --==========================================================================

    function Create_World_Controller return A_World_Controller is
        this : constant A_World_Controller := new World_Controller;
    begin
        this.Construct;
        return this;
    end Create_World_Controller;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out World_Controller ) is
    begin
        if this.corral /= null then
            this.corral.Remove_Listener( this'Unchecked_Access );
        end if;
        Limited_Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Get_World( this : not null access World_Controller'Class ) return A_World is (this.world);

    ----------------------------------------------------------------------------

    procedure Handle_Create_Layer( this : not null access World_Controller'Class;
                                   evt  : not null A_Layer_Create_Event ) is
    begin
        this.world.Add_Layer( evt.Get_Layer,
                              evt.Get_Type,
                              evt.Get_Properties,
                              evt.Get_Layer_Data );
    end Handle_Create_Layer;

    ----------------------------------------------------------------------------

    procedure Handle_Delete_Layer( this : not null access World_Controller'Class;
                                   evt  : not null A_Layer_Event ) is
    begin
        this.world.Delete_Layer( evt.Get_Layer );
    end Handle_Delete_Layer;

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Event( this : access World_Controller;
                            evt  : in out A_Event;
                            resp : out Response_Type ) is
    begin
        Set_Response( resp, ST_SUCCESS );
        case evt.Get_Event_Id is
            when EVT_CREATE_LAYER       => this.Handle_Create_Layer( A_Layer_Create_Event(evt) );
            when EVT_DELETE_LAYER       => this.Handle_Delete_Layer( A_Layer_Event(evt) );
            when EVT_SWAP_LAYERS        => this.Handle_Swap_Layers( A_Layer_Swap_Event(evt) );
            when EVT_SET_TILE           => this.Handle_Set_Tile( A_Tile_Event(evt) );
            when EVT_SET_WORLD_PROPERTY => this.Handle_Set_World_Property( A_World_Property_Event(evt) );
            when EVT_SET_LAYER_PROPERTY => this.Handle_Set_Layer_Property( A_Layer_Property_Event(evt) );
            when others                 => Set_Response( resp, ST_NONE );
        end case;
    end Handle_Event;

    ----------------------------------------------------------------------------

    procedure Handle_Set_Layer_Property( this : not null access World_Controller'Class;
                                         evt  : not null A_Layer_Property_Event ) is
    begin
        this.world.Set_Layer_Property( evt.Get_Layer, evt.Get_Property_Name, evt.Get_Value );
    end Handle_Set_Layer_Property;

    ----------------------------------------------------------------------------

    procedure Handle_Set_Tile( this : not null access World_Controller'Class;
                               evt  : not null A_Tile_Event ) is
    begin
        this.world.Set_Tiles( evt.Get_List );
    end Handle_Set_Tile;

    ----------------------------------------------------------------------------

    procedure Handle_Set_World_Property( this : not null access World_Controller'Class;
                                         evt  : not null A_World_Property_Event ) is
    begin
        this.world.Set_Property( evt.Get_Property_Name, evt.Get_Value );
    end Handle_Set_World_Property;

    ----------------------------------------------------------------------------

    procedure Handle_Swap_Layers( this : not null access World_Controller'Class;
                                  evt  : not null A_Layer_Swap_Event ) is
    begin
        this.world.Swap_Layers( evt.Get_Layer, evt.Get_Other_Layer );
    end Handle_Swap_Layers;

    ----------------------------------------------------------------------------

    procedure Initialize( this : not null access World_Controller'Class; corral : not null A_Corral ) is
    begin
        pragma Assert( this.corral = null );
        this.corral := corral;

        this.corral.Add_Listener( this, EVT_CREATE_LAYER );
        this.corral.Add_Listener( this, EVT_DELETE_LAYER );
        this.corral.Add_Listener( this, EVT_SWAP_LAYERS );
        this.corral.Add_Listener( this, EVT_SET_TILE );
        this.corral.Add_Listener( this, EVT_SET_WORLD_PROPERTY );
        this.corral.Add_Listener( this, EVT_SET_LAYER_PROPERTY );
    end Initialize;

    ----------------------------------------------------------------------------

    overriding
    procedure Initialize_World( this : access World_Controller ) is
        evt : A_Event;
    begin
        Queue_World_Loaded( this.world.map,
                            this.world.Get_Tile_Width,
                            Cast_String( this.world.Get_Property( "library" ) ) );

        declare
            procedure Property_Changed( key : String; val : Value ) is
            begin
                Queue_World_Property_Changed( key, val );
            end Property_Changed;
        begin
            this.world.properties.Iterate( Property_Changed'Access );
        end;

        for e of this.world.Get_Entities.all loop
            evt := e.Create_Spawned_Event;
            Events.Manager.Global.Queue_Event( evt, GAME_CHANNEL );
        end loop;
    end Initialize_World;

    ----------------------------------------------------------------------------

    procedure Modified( this : not null access World_Controller'Class; worldModified : Boolean ) is
    begin
        if this.world /= null then
            if worldModified then
                Queue_World_Modified;
            else
                Queue_World_Unmodified;
            end if;
        end if;
    end Modified;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Entity_Added( this : access World_Controller; e : not null A_Entity ) is
        evt : A_Event := e.Create_Spawned_Event;
    begin
        Events.Manager.Global.Queue_Event( evt, GAME_CHANNEL );
        this.Modified( True );
    end On_Entity_Added;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Layer_Created( this  : access World_Controller;
                                layer : Integer;
                                data  : not null A_Map ) is
    begin
        Queue_Layer_Created( layer      => layer,
                             layerType  => this.world.map.Get_Layer_Type( layer ),
                             layerData  => data,
                             properties => this.world.map.Get_Layer_Properties( layer ),
                             propDefs   => this.world.map.Get_Layer_Property_Definitions( layer ) );
        this.Modified( True );
    end On_Layer_Created;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Layer_Deleted( this : access World_Controller; layer : Integer ) is
    begin
        Queue_Layer_Deleted( layer );
        this.Modified( True );
    end On_Layer_Deleted;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Layer_Property_Changed( this  : access World_Controller;
                                         layer : Integer;
                                         name  : String;
                                         val   : Value'Class ) is
    begin
        Queue_Layer_Property_Changed( layer, name, val );
        this.Modified( True );
    end On_Layer_Property_Changed;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Layers_Swapped( this   : access World_Controller;
                                 layerA : Integer;
                                 layerB : Integer ) is
    begin
        Queue_Layers_Swapped( layerA, layerB );
        this.Modified( True );
    end On_Layers_Swapped;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Tiles_Changed( this  : access World_Controller;
                                tiles : Tile_Change_Lists.Vector ) is
    begin
        Queue_Tiles_Changed( tiles );
        this.Modified( True );
    end On_Tiles_Changed;

    ----------------------------------------------------------------------------

    overriding
    procedure On_World_Property_Changed( this : access World_Controller;
                                         name : String;
                                         val  : Value'Class ) is
    begin
        Queue_World_Property_Changed( name, val );
        if name = "filename" or     -- "filename" isn't persisted so it's not a modification
           name = "filepath" or     -- "filepath" isn't persisted so it's not a modification
           name = "animated" or     -- "animated" isn't persisted so it's not a modification
           name = "age"             -- aging is not a modification
        then
            null;
        else
            this.Modified( True );
        end if;
    end On_World_Property_Changed;

    ----------------------------------------------------------------------------

    overriding
    procedure On_World_Resized( this : access World_Controller ) is
    begin
        this.Initialize_World;
        this.Modified( True );
    end On_World_Resized;

    ----------------------------------------------------------------------------

    procedure Set_World( this : not null access World_Controller'Class; world : A_World ) is
    begin
        if world = this.world then
            return;
        end if;

        if this.world /= null then
            this.world.Set_Listener( null );
        end if;

        this.world := world;
        if this.world /= null then
            this.world.Set_Listener( this );
        end if;
    end Set_World;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_World_Controller ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

end Worlds.Controllers;
