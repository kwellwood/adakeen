--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

private package Support.OSX is

    -- Returns the path of the directory where system-wide application-specific
    -- data files can be read and written on an OS X system.
    function App_Data_Directory return String is ("/Library/Application Support/");

    -- Attempts to open the file 'filePath' with the file type's default
    -- application.
    procedure Auto_Open( filePath : String );

    -- Returns the end-of-line sequence for OS X.
    function EOL return String is (String'(1 => ASCII.LF));

    -- Returns the file extension of executables on OS X without a leading dot
    -- character.
    function Executable_Extension return String is ("");

    -- Returns the path of the application's executable file.
    function Executable_Path return String;

    -- Returns the absolute path of the directory containing the executable
    -- file, or, if the executable is running inside an .app bundle, the
    -- bundle's Resources directory.
    function Execution_Directory return String;
    pragma Postcondition( Execution_Directory'Result'Length > 0 );

    -- Returns the path of the user's home directory.
    function Home_Directory return String;

    -- Opens a Finder window at the directory specified by 'path'. If 'path'
    -- is a file, the file will be selected in the window.
    procedure Reveal_Path( path : String );

    -- Returns the path of the OS X system font directory.
    function System_Font_Directory return String is ("/Library/Fonts/");

    -- Returns the current platform's temporary directory.
    function Temp_Directory return String;

end Support.OSX;
