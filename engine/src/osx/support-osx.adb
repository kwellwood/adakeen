--
-- Copyright (c) 2012-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with GNAT.OS_Lib;                       use GNAT.OS_Lib;
with Support.Paths;                     use Support.Paths;
with System;                            use System;

package body Support.OSX is

    function Get_Env_Var( name : String ) return String is
        str : GNAT.OS_Lib.String_Access := Getenv( name );
    begin
        if str /= null then
            declare
                result : String(1..Integer'Min(1024, str'Length));
            begin
                result := str(str'First..str'First + Integer'Min(1024, str'Length) - 1);
                Free( str );
                return result;
            end;
        else
            return "";
        end if;
    end Get_Env_Var;

    ----------------------------------------------------------------------------

    procedure Auto_Open( filePath : String ) is
        args : Argument_List(1..1);
        pid  : Process_Id;
        pragma Warnings( Off, pid );
    begin
        if Is_Regular_File( filePath ) then
            args(1) := new String'(filePath);
            pid := GNAT.OS_Lib.Non_Blocking_Spawn( "/usr/bin/open", args );
            for i in args'Range loop
                Free( args(i) );
            end loop;
        end if;
    end Auto_Open;

    ----------------------------------------------------------------------------

    function Executable_Path return String is

        function NSGetExecutablePath( buf     : Address;
                                      bufsize : access Unsigned_32 ) return Integer;
        pragma Import( C, NSGetExecutablePath, "_NSGetExecutablePath" );

        len  : aliased Unsigned_32 := 1024;
        path : aliased String(1..Integer(len)) := (others=>ASCII.NUL);
    begin
        if NSGetExecutablePath( path(path'First)'Address, len'Access ) = 0 then
            for i in path'Range loop
                if path(i) = ASCII.NUL then
                    return Normalize_Pathname( path(1..i-1) );
                end if;
            end loop;
            return path;
        end if;

        -- the executable's path length is greater than 512
        return Normalize_Pathname( "." );
    end Executable_Path;

    ----------------------------------------------------------------------------

    function Execution_Directory return String is
        dir : constant String := Get_Directory( Executable_Path );
    begin
        if Index( dir, ".app/Contents/MacOS", Backward ) > dir'First then
            -- Inside an app bundle, the execution directory is the bundle's
            -- Resources directory.
            return Normalize_Pathname( dir & "../Resources/" );
        else
            -- Not inside an app bundle; the execution directory is the one
            -- that contains the executable.
            return dir;
        end if;
    end Execution_Directory;

    ----------------------------------------------------------------------------

    function Home_Directory return String is (Get_Env_Var( "HOME" ) & "/");

    ----------------------------------------------------------------------------

    procedure Reveal_Path( path : String ) is
        args : Argument_List(1..2);
        pid  : Process_Id;
        pragma Warnings( Off, pid );
    begin
        args(1) := new String'("-R");
        args(2) := new String'(path);
        pid := GNAT.OS_Lib.Non_Blocking_Spawn( "/usr/bin/open", args );
        for i in args'Range loop
            Free( args(i) );
        end loop;
    end Reveal_Path;

    ----------------------------------------------------------------------------

    function Temp_Directory return String is (Get_Env_Var( "TMPDIR" ));

end Support.OSX;
