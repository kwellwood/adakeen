--
-- Copyright (c) 2014-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Scribble.Runtimes;                 use Scribble.Runtimes;
with Values;                            use Values;

package Resources.Scribble is

    -- Compiles the source code in file 'resource' into a single Value 'val',
    -- returning True on success or False on failure. If compilation fails, a
    -- descriptive Error value will be returned in 'val' and compilation error
    -- details will be output to the debugging system. If 'enableDebug' is True,
    -- debugging info will be embedded in compiled functions.
    function Compile_Value( val         : out Value;
                            resource    : not null A_Resource_File;
                            enableDebug : Boolean ) return Boolean;

    -- Loads a compiled Scribble object (.sco) from file 'resource' into 'val',
    -- returning True on success or False on failure. On failure, a descriptive
    -- Error will be returned in 'val'.
    function Load_Scribble( val      : out Value;
                            resource : not null A_Resource_File ) return Boolean;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Compiles or loads Scribble script file 'filePath' in resource group
    -- 'group' into Value 'val', returning True on success or False on failure.
    -- On success, 'val' will contain a Map value. On failure, 'val' will
    -- contain a descriptive Error value  (ex: File not found, compilation
    -- failure, or bad file format). Compilation error details will be output to
    -- the debugging system.
    --
    -- If the file extension of 'filePath' is ".script" and the file can be
    -- located, it will be compiled with runtime 'runtime'. Set 'enableDebug' to
    -- True to embed debug info in compiled functions.
    --
    -- If the file cannot be located, then a file path with ".sco" appended to
    -- the filename (ex: "myfile.script.sco") will be located and loaded as a
    -- Scribble compiled object file.
    --
    -- This fallback mechanism allows script files to be referenced consistently
    -- as "*.script" while also supporting game distribution with only object
    -- files, for speedier load times.
    function Load_Script( val         : out Value;
                          filePath    : String;
                          group       : String;
                          runtime     : not null A_Scribble_Runtime;
                          enableDebug : Boolean := False ) return Boolean;

    -- Compiles or loads Scribble file 'filePath' in resource group 'group' into
    -- Value 'val', returning True on success or False on failure. On failure,
    -- a descriptive Error will be returned in 'val' (ex: File not found,
    -- compilation failure, or bad file format). Compilation error details will
    -- be output to the debugging system.
    --
    -- If the file extension of 'filePath' is ".sv" and the file can be loaded,
    -- it will be compiled. Set 'enableDebug' to True to embed debug info in any
    -- compiled functions. If the file cannot be located, then a file path with
    -- ".sco" appended to the filename (ex: "myfile.sv.sco") will be located and
    -- loaded as a Scribble compiled object file.
    --
    -- This fallback mechanism allows Scribble value files to be referenced
    -- consistently as "*.sv" and "*.sp" while also supporting game distribution
    -- with object files only (no source), for speedier load times.
    function Load_Value( val         : out Value;
                         filePath    : String;
                         group       : String;
                         enableDebug : Boolean := False ) return Boolean;

end Resources.Scribble;

