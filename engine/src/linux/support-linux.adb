--
-- Copyright (c) 2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with GNAT.OS_Lib;                       use GNAT.OS_Lib;
with Support.Paths;                     use Support.Paths;

package body Support.Linux is

    function Get_Env_Var( name : String ) return String is
        str : GNAT.OS_Lib.String_Access := Getenv( name );
    begin
        if str /= null then
            declare
                result : String(1..Integer'Min(1024, str'Length));
            begin
                result := str(str'First..str'First + Integer'Min(1024, str'Length) - 1);
                Free( str );
                return result;
            end;
        else
            return "";
        end if;
    end Get_Env_Var;

    ----------------------------------------------------------------------------

    procedure Auto_Open( filePath : String ) is
        args : Argument_List(1..1);
        pid  : Process_Id;
        pragma Warnings( Off, pid );
    begin
        if Is_Regular_File( filePath ) then
            args(1) := new String'(filePath);
            pid := GNAT.OS_Lib.Non_Blocking_Spawn( "/usr/bin/xdg-open", args );
            for i in args'Range loop
                Free( args(i) );
            end loop;
        end if;
    end Auto_Open;

    ----------------------------------------------------------------------------

    function Executable_Path return String is (Normalize_Pathname( "/proc/self/exe" ));

    ----------------------------------------------------------------------------

    function Execution_Directory return String is (Get_Directory( Executable_Path ));

    ----------------------------------------------------------------------------

    function Home_Directory return String is (Get_Env_Var( "HOME" ) & "/");

    ----------------------------------------------------------------------------

    procedure Reveal_Path( path : String ) is
        args : Argument_List(1..1);
        pid  : Process_Id;
        pragma Warnings( Off, pid );
    begin
        -- Note: This doesn't actually select a file after opening a browser to
        -- a folder location.
        args(1) := new String'((if Is_Regular_File( path ) then Get_Directory( path ) else path));
        pid := GNAT.OS_Lib.Non_Blocking_Spawn( "/usr/bin/xdg-open", args );
        for i in args'Range loop
            Free( args(i) );
        end loop;
    end Reveal_Path;

end Support.Linux;
