--
-- Copyright (c) 2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

private package Support.Linux is

    -- Returns the path of the directory where system-wide application-specific
    -- data files can be read and written on a Linux system.
    function App_Data_Directory return String is ("/usr/local/");

    -- Attempts to open the file 'filePath' with the file type's default
    -- application.
    procedure Auto_Open( filePath : String );

    -- Returns the end-of-line sequence for Linux.
    function EOL return String is (String'(1 => ASCII.LF));

    -- Returns the file extension of executables on Linux without a leading dot
    -- character.
    function Executable_Extension return String is ("");

    -- Returns the path of the application's executable file.
    function Executable_Path return String;

    -- Returns the absolute path of the directory containing the executable
    -- file.
    function Execution_Directory return String;
    pragma Postcondition( Execution_Directory'Result'Length > 0 );

    -- Returns the path of the user's home directory.
    function Home_Directory return String;

    -- Opens a Finder window at the directory specified by 'path'. If 'path'
    -- is a file, the file should be selected in the window but Linux doesn't
    -- currently support this in a compatible way.
    procedure Reveal_Path( path : String );

    -- Returns the path of the system font directory.
    function System_Font_Directory return String is ("/usr/share/fonts/");

    -- Returns the current platform's temporary directory.
    function Temp_Directory return String is ("/tmp/");

end Support.Linux;
