--
-- Copyright (c) 2012 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Real_Time;                     use Ada.Real_Time;

package Support.Real_Time is

    -- A simple generic timer used to calculate elapsed time.
    type Timer_Type is limited private;

    -- Return the elapsed time counted by the timer.
    function Elapsed( t : Timer_Type ) return Time_Span;

    -- Returns True if the timer has been started and isn't paused.
    function Is_Running( t : Timer_Type ) return Boolean;

    -- Pauses the timer. Elapsed time will not accumulate until the timer
    -- is started again.
    procedure Pause( t : in out Timer_Type );

    -- Clears elapsed time and starts the timer again.
    procedure Restart( t : in out Timer_Type );

    -- Starts or resumes the timer. This will have no effect if the timer is
    -- already running.
    procedure Start( t : in out Timer_Type );

    ----------------------------------------------------------------------------

    -- A timer used specifically for calculating frame rates.
    type Frame_Timer is tagged limited private;

    -- Notifies the frame timer of the end of the frame. The frame rate is only
    -- calculated once per period (by default, one second).
    procedure End_Frame( t : in out Frame_Timer );

    -- Returns the current frame rate.
    function Get_Frame_Rate( t : Frame_Timer ) return Float;

    -- Atomically ends the previous frame and starts the next one. This is
    -- nearly equivalent to "End_Frame( t ); Start_Frame( t );" except the Clock
    -- will only be read once.
    procedure Next_Frame( t : in out Frame_Timer );

    -- Resets the timer. The frame rate will be reset to zero.
    procedure Reset( t : in out Frame_Timer );

    -- Sets the minimum time to collect ticks before recalculating the rate.
    -- The default is one second.
    procedure Set_Update_Period( t : in out Frame_Timer; ts : Time_Span );

    -- Notifies the frame timer of the start of the frame.
    procedure Start_Frame( t : in out Frame_Timer );

    ----------------------------------------------------------------------------

    -- Returns a string representation of 'ts' with the nearest applicable units.
    -- Example: "10[ms]", "10[s]", "10[min]"
    function Format( ts : Time_Span ) return String;
    pragma Postcondition( Format'Result'Length > 0 );

    -- Returns 'ts' as a Long_Float, based in seconds.
    function To_Float( ts : Time_Span ) return Long_Float is (Long_Float(To_Duration( ts )));

    -- Returns 'ts' rounded to the nearest microsecond.
    function To_Microseconds( ts : Time_Span ) return Natural is (Natural(To_Duration( ts ) * 1_000_000.0));

    -- Returns 'ts' rounded to the nearest millisecond.
    function To_Milliseconds( ts : Time_Span ) return Natural is (Natural(To_Duration( ts ) * 1_000.0));

    -- Returns 'ts' rounded to the nearest minute.
    function To_Minutes( ts : Time_Span ) return Natural is (Natural(To_Duration( ts ) / 60.0));

    -- Returns 'ts' rounded to the nearest second.
    function To_Seconds( ts : Time_Span ) return Natural is (Natural(To_Duration( ts )));

    -- Returns a string representation of Time 't' as a floating point number
    -- of seconds since the epoch (typically machine boot), with three digits
    -- to the right of the decimal.
    function To_String( t : Time ) return String;

    -- Returns a string representation of 'ts' as a floating point number in
    -- seconds, with 'precision' number of digits to the right of the decimal.
    function To_String( ts : Time_Span; precision : Natural := 3 ) return String;
    pragma Postcondition( To_String'Result'Length > 0 );

    ----------------------------------------------------------------------------

    -- Reads a Time record from a stream.
    function Time_Input( stream : access Root_Stream_Type'Class ) return Time;

    -- Writes a Time record to a stream.
    procedure Time_Output( stream : access Root_Stream_Type'Class; t : Time );

private

    type Timer_Type is limited
        record
            started : Time := Time_First;   -- start time, changes when unpaused
            paused  : Time := Time_First;   -- time of pause (if paused)
            running : Boolean := False;     -- time is running
        end record;

    type Frame_Timer is tagged limited
        record
            ticks        : Natural := 0;                 -- ticks during this period
            updatePeriod : Time_Span := Seconds( 1 );    -- minimum time between fps updates
            start        : Time := Clock;                -- start time of the frame
            elapsed      : Time_Span := Time_Span_Zero;  -- time elapsed during period
            fps          : Float := 0.0;                 -- calculated fps
        end record;

end Support.Real_Time;
