--
-- Copyright (c) 2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Indefinite_Ordered_Maps;
with Objects;                           use Objects;
with Scribble.Ast;                      use Scribble.Ast;
with Scribble.Ast.Expressions;          use Scribble.Ast.Expressions;
with Scribble.Ast.Expressions.Symbols;  use Scribble.Ast.Expressions.Symbols;
with Scribble.Ast.Script_Processors;    use Scribble.Ast.Script_Processors;
with Scribble.Ast.Scripts;              use Scribble.Ast.Scripts;
with Scribble.Ast.Statements;           use Scribble.Ast.Statements;
with Scribble.Runtimes;                 use Scribble.Runtimes;
with Scribble.Semantics;                use Scribble.Semantics;

--
-- Semantic rules enforced by the Script Semantic_Analyzer:
--
-- 1.  Member names cannot be match any reserved name in the Script component API.
-- 2.  Required members must be defined later.
-- 3.  Defined members cannot be required later.
-- 4.  Defined members cannot be marked default later.
-- 5.  Members can only override a member defined as default.
--
-- 6.  Timer names must match a member- required or declared.
-- 7.  Timer periods must be positive.
--
--     Add numbered rules to the bottom of this list as they are implemented.
--
private package Scribble.Script_Analyzers is

    type Script_Analyzer is new Limited_Object and Ast_Script_Processor with private;
    type A_Script_Analyzer is access all Script_Analyzer'Class;

    -- Creates a new instance of the Scribble Script semantic analyzer. The
    -- Scribble runtime provides information about the existence of directly
    -- exported Ada functions.
    function Create_Script_Analyzer( runtime : not null A_Scribble_Runtime ) return A_Script_Analyzer;
    pragma Postcondition( Create_Script_Analyzer'Result /= null );

    -- Analyzes AST 'script' for conformance with Scribble script semantic rules
    -- and builds variable definition tables in the given AST. If the AST does
    -- not conform to all semantic rules of the language, a Parse_Error will be
    -- raised.
    procedure Analyze( this   : not null access Script_Analyzer'Class;
                       script : not null A_Ast_Script );

    procedure Delete( this : in out A_Script_Analyzer );
    pragma Postcondition( this = null );

private

    type Member_Rec is
        record
            required   : A_Ast_Required_Member := null;
            definition : A_Ast_Script_Member := null;
        end record;

    package Member_Maps is new Ada.Containers.Indefinite_Ordered_Maps(String, Member_Rec, "<", "=");
    use Member_Maps;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Script_Analyzer is new Limited_Object and Ast_Script_Processor with
        record
            runtime : A_Scribble_Runtime := null;
            child   : A_Semantic_Analyzer := null;     -- for analyzing elements

            members : Member_Maps.Map;
        end record;

    procedure Construct( this    : access Script_Analyzer;
                         runtime : not null A_Scribble_Runtime );

    procedure Delete( this : in out Script_Analyzer );

    -- script elements
    procedure Process_Required_Member( this : access Script_Analyzer; node : A_Ast_Required_Member );
    procedure Process_Script_Member( this : access Script_Analyzer; node : A_Ast_Script_Member );
    procedure Process_Timer( this : access Script_Analyzer; node : A_Ast_Timer );

end Scribble.Script_Analyzers;
