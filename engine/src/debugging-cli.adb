--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;

package body Debugging.CLI is

    procedure Parse_Arguments is

        ------------------------------------------------------------------------

        function To_Channel( str : String ) return Debug_Channel is
            lstr : constant String := To_Lower( str );
        begin
            if lstr = "log" then
                return D_LOG;
            elsif lstr = "audio" then
                return D_AUDIO;
            elsif lstr = "entity" then
                return D_ENTITY;
            elsif lstr = "script" then
                return D_SCRIPT;
            elsif lstr = "events" then
                return D_EVENTS;
            elsif lstr = "game" then
                return D_GAME;
            elsif lstr = "gui" then
                return D_GUI;
            elsif lstr = "init" then
                return D_INIT;
            elsif lstr = "input" then
                return D_INPUT;
            elsif lstr = "physics" then
                return D_PHYSICS;
            elsif lstr = "prefs" then
                return D_PREFS;
            elsif lstr = "procs" then
                return D_PROCS;
            elsif lstr = "res" then
                return D_RES;
            elsif lstr = "tiles" then
                return D_TILES;
            elsif lstr = "view" then
                return D_VIEW;
            elsif lstr = "app" then
                return D_APP;
            elsif lstr = "world" then
                return D_WORLD;
            elsif lstr = "font" then
                return D_FONT;
            elsif lstr = "particles" then
                return D_PARTICLES;
            elsif lstr = "asset" then
                return D_ASSET;
            elsif lstr = "focus" then
                return D_FOCUS;
            end if;
            return NO_CHANNELS;
        end To_Channel;

        ------------------------------------------------------------------------

    begin
        for i in 1..Argument_Count loop
            if Argument( i )'Length >= 5 then
                declare
                    arg  : constant String := Argument( i );
                    eq   : Integer;
                    chan : Debug_Channel;
                begin
                    if arg(arg'First..arg'First+2) = "-DV" then
                        eq := Index( arg, "=", arg'First + 3 );
                        if eq > arg'First + 3 and then eq < arg'Last then
                            -- Argument is '-DVxxx=y'
                            -- where 'xxx' is the channel and 'y' is the verbosity
                            chan := To_Channel( arg(arg'First+3..eq-1) );
                            if arg(eq+1..arg'Last) = "e" then
                                Set_Verbosity( chan, Error );
                            elsif arg(eq+1..arg'Last) = "w" then
                                Set_Verbosity( chan, Warning );
                            elsif arg(eq+1..arg'Last) = "i" then
                                Set_Verbosity( chan, Info );
                            end if;
                        elsif eq = arg'First + 3 then
                            -- Argument is '-DV=y' where 'y' is global verbosity
                            if arg(eq+1..arg'Last) = "s" then
                                Set_Verbosity( ALL_CHANNELS, Never );
                            elsif arg(eq+1..arg'Last) = "e" then
                                Set_Verbosity( ALL_CHANNELS, Error );
                            elsif arg(eq+1..arg'Last) = "w" then
                                Set_Verbosity( ALL_CHANNELS, Warning );
                            elsif arg(eq+1..arg'Last) = "i" then
                                Set_Verbosity( ALL_CHANNELS, Info );
                            end if;
                        end if;
                    elsif arg(arg'First..arg'First+2) = "-DD" then
                        eq := Index( arg, "=", arg'First + 3 );
                        if eq = arg'First + 3 then
                            -- Argument is '-DD=y' where 'y' is global decoration
                            if arg(eq+1..arg'Last) = "0" then
                                Set_Decoration( NONE );
                            elsif arg(eq+1..arg'Last) = "1" then
                                Set_Decoration( TASK_IMAGE );
                            elsif arg(eq+1..arg'Last) = "2" then
                                Set_Decoration( SOURCE_UNIT );
                            end if;
                        end if;
                    elsif arg(arg'First..arg'First+2) = "-DF" then
                        eq := Index( arg, "=", arg'First + 3 );
                        if eq = arg'First + 3 then
                            -- Argument is '-DF=y' where 'y' is the log filename
                            Set_Log_File( arg(eq+1..arg'Last) );
                        end if;
                    end if;
                end;
            end if;
        end loop;
    end Parse_Arguments;

end Debugging.CLI;
