--
-- Copyright (c) 2016-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Resources.Plaintext is

    -- Reads the contents of file 'resource' into a string as plaintext.
    function Read_Plaintext( resource : not null A_Resource_File ) return Unbounded_String;

    -- Locates and loads file 'filePath' into a string as plaintext. The file
    -- will be located according to the standard resource file search order.
    -- Returns True on success, or False if the file cannot be found or read.
    -- When False is returned, 'text' will be an empty string.
    function Load_Plaintext( filePath : String; group : String; text : out Unbounded_String ) return Boolean;

    -- Locates and loads file 'filePath' into a string as plaintext. The file
    -- will be located according to the standard resource file search order.
    -- Returns an empty string if the file cannot be found or read.
    function Load_Plaintext( filePath : String; group : String ) return Unbounded_String;

end Resources.Plaintext;
