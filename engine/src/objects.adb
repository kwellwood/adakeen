--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Ada.Tags;                          use Ada.Tags;
with Ada.Unchecked_Deallocation;
with System;                            use System;
with System.Address_Image;

package body Objects is

    function Get_Class_Name( this : not null access Base_Object'Class;
                             full : Boolean := False ) return String is
        tag : constant String := External_Tag( this.all'Tag );
        dot : constant Integer := Index( tag, ".", Backward );
    begin
        if not full and then dot > tag'First then
            return To_Lower( tag(dot+1..tag'Last) );
        end if;
        return To_Lower( tag );
    end Get_Class_Name;

    --==========================================================================

    overriding
    function To_String( this : access Object ) return String
    is ("<" & this.Get_Class_Name & " @" & Address_Image( this.all'Address ) & ">");

    ----------------------------------------------------------------------------

    function Copy( src : access Object'Class ) return A_Object is
        procedure Free is new Ada.Unchecked_Deallocation( Object'Class, A_Object );
        dest : A_Object := null;
    begin
        if src /= null then
            dest := new Object'Class'(src.all);
            begin
                dest.Adjust;
            exception
                when COPY_NOT_ALLOWED =>
                    -- don't execute the deconstructor because it wasn't constructed
                    Free( dest );
                    raise;
            end;
        end if;
        return dest;
    end Copy;

    ----------------------------------------------------------------------------

    overriding
    procedure Pop_Signaller( this : access Object ) is
        procedure Free is new Ada.Unchecked_Deallocation( Object_List, A_Object_List );
        temp : A_Object_List;
    begin
        pragma Assert( this.signallerCount > 0, "Signaller push/pop mismatch" );
        if this.signallerCount > 1 then
            -- more than one sender on the stack; pop the head
            temp := this.signallers.next;
            this.signallers := temp.all;
            Free( temp );
        else
            -- only one sender (potentially) on the stack
            this.signallers.elem := null;
        end if;
        this.signallerCount := this.signallerCount - 1;
    end Pop_Signaller;

    ----------------------------------------------------------------------------

    overriding
    procedure Push_Signaller( this   : access Object;
                              sender : access Base_Object'Class ) is
    begin
        if this.signallerCount > 0 then
            -- already a sender on the stack; push a new head
            this.signallers.next := new Object_List'(this.signallers);
        end if;
        this.signallers.elem := sender;
        this.signallerCount := this.signallerCount + 1;
    end Push_Signaller;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Object ) is
        procedure Free is new Ada.Unchecked_Deallocation( Object'Class, A_Object );
    begin
        if this /= null then
            this.Delete;
            Free( this );
        end if;
    end Delete;

    --==========================================================================

    overriding
    function To_String( this : access Limited_Object ) return String
    is ("<" & this.Get_Class_Name & " @" & Address_Image( this.all'Address ) & ">");

    ----------------------------------------------------------------------------

    overriding
    procedure Pop_Signaller( this : access Limited_Object ) is
        procedure Free is new Ada.Unchecked_Deallocation( Object_List, A_Object_List );
        temp : A_Object_List;
    begin
        pragma Assert( this.signallerCount > 0, "Signaller push/pop mismatch" );
        if this.signallerCount > 1 then
            -- more than one sender on the stack; pop the head
            temp := this.signallers.next;
            this.signallers := temp.all;
            Free( temp );
        else
            -- only one sender (potentially) on the stack
            this.signallers.elem := null;
        end if;
        this.signallerCount := this.signallerCount - 1;
    end Pop_Signaller;

    ----------------------------------------------------------------------------

    overriding
    procedure Push_Signaller( this   : access Limited_Object;
                              sender : access Base_Object'Class ) is
    begin
        if this.signallerCount > 0 then
            -- already a sender on the stack; push a new head
            this.signallers.next := new Object_List'(this.signallers);
        end if;
        this.signallers.elem := sender;
        this.signallerCount := this.signallerCount + 1;
    end Push_Signaller;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Limited_Object ) is
        procedure Free is new Ada.Unchecked_Deallocation( Limited_Object'Class, A_Limited_Object );
    begin
        if this /= null then
            this.Delete;
            Free( this );
        end if;
    end Delete;

end Objects;
