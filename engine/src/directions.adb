--
-- Copyright (c) 2012 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Directions is

    function "+"( l : Direction_Type; r : Cardinal_Direction ) return Direction_Type is
        result : Direction_Type := l;
    begin
        case r is
            when Left  => result.x := -1;
            when Right => result.x :=  1;
            when Up    => result.y := -1;
            when Down  => result.y :=  1;
        end case;
        return result;
    end "+";

    ----------------------------------------------------------------------------

    function "-"( l : Direction_Type; r : Cardinal_Direction ) return Direction_Type is
        result : Direction_Type := l;
    begin
        case r is
            when Left =>
                if result.x < 0 and result.y /= 0 then
                    result.x := 0;
                end if;
            when Right =>
                if result.x > 0 and result.y /= 0 then
                    result.x := 0;
                end if;
            when Up =>
                if result.y < 0 and result.x /= 0 then
                    result.y := 0;
                end if;
            when Down =>
                if result.y > 0 and result.x /= 0 then
                    result.y := 0;
                end if;
        end case;
        return result;
    end "-";

    ----------------------------------------------------------------------------

    function "and"( l : Direction_Type; r : Cardinal_Direction ) return Boolean is
    begin
        case r is
            when Left  => return l.x < 0;
            when Right => return l.x > 0;
            when Up    => return l.y < 0;
            when Down  => return l.y > 0;
        end case;
    end "and";

    ----------------------------------------------------------------------------

    function To_D8( dir : Direction_Type ) return Direction_8 is
    begin
        case dir.y is
            when -1 =>
                case dir.x is
                    when -1 => return D8_Up_Left;
                    when  0 => return D8_Up;
                    when  1 => return D8_Up_Right;
                end case;
            when  0 =>
                case dir.x is
                    when -1 => return D8_Left;
                    when  0 => raise Constraint_Error with "Invalid direction";
                    when  1 => return D8_Right;
                end case;
            when  1 =>
                case dir.x is
                    when -1 => return D8_Down_Left;
                    when  0 => return D8_Down;
                    when  1 => return D8_Down_Right;
                end case;
        end case;
    end To_D8;

    ----------------------------------------------------------------------------

    function To_D8( dir : Cardinal_Direction ) return Direction_8
    is (
        case dir is
            when Left  => D8_Left,
            when Right => D8_Right,
            when Up    => D8_Up,
            when Down  => D8_Down
    );

    ----------------------------------------------------------------------------

    function To_X( dir : Direction_Type ) return Direction_8
    is (if dir.x > 0 then D8_Right else D8_Left);

    ----------------------------------------------------------------------------

    function To_Y( dir : Direction_Type ) return Direction_8
    is (if dir.y > 0 then D8_Down else D8_Up);

    ----------------------------------------------------------------------------

    function To_Y( dir : Direction_Type ) return Cardinal_Direction
    is (if dir.y > 0 then Down else Up);

    ----------------------------------------------------------------------------

    function "not"( db : Direction_Booleans ) return Boolean is
    begin
        for i in db'Range loop
            if db(i) then
                return False;
            end if;
        end loop;
        return True;
    end "not";

    ----------------------------------------------------------------------------

    function Opposite( dir : Cardinal_Direction ) return Cardinal_Direction
    is (
        case dir is
            when Left  => Right,
            when Right => Left,
            when Up    => Down,
            when Down  => Up
    );

    ----------------------------------------------------------------------------

    function To_Direction( dir : Cardinal_Direction ) return Direction_Type
    is (
        case dir is
            when Left  => Dir_Left,
            when Right => Dir_Right,
            when Up    => Dir_Up,
            when Down  => Dir_Down
    );

    ----------------------------------------------------------------------------

    function To_Direction( dir : Direction_8 ) return Direction_Type
    is (
        case dir is
            when D8_Left       => Dir_Left,
            when D8_Right      => Dir_Right,
            when D8_Up         => Dir_Up,
            when D8_Down       => Dir_Down,
            when D8_Up_Left    => Dir_Up_Left,
            when D8_Up_Right   => Dir_Up_Right,
            when D8_Down_Left  => Dir_Down_Left,
            when D8_Down_Right => Dir_Down_Right
    );

    ----------------------------------------------------------------------------

    function To_String( dir : Direction_Type ) return String
    is (
        "[" & Axis_Direction'Image( dir.x ) & "," &
        Axis_Direction'Image( dir.y ) & "]"
    );

end Directions;
