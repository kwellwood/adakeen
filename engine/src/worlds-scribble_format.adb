--
-- Copyright (c) 2018-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Text_IO;
with Component_Families;                use Component_Families;
with Debugging;                         use Debugging;
with Entities.Entity_Attributes;        use Entities.Entity_Attributes;
with Entities.Factory;                  use Entities.Factory;
with Entities.Locations;                use Entities.Locations;
with Resources.Scribble;
with Values.Construction;               use Values.Construction;
with Values.Errors;                     use Values.Errors;
with Values.Lists;                      use Values.Lists;
with Values.Strings;                    use Values.Strings;

package body Worlds.Scribble_Format is

    function Export( world : not null A_World; filePath : String ) return Boolean is

        ------------------------------------------------------------------------

        function Export_Layer( l : Integer ) return Map_Value is
            layer : constant Map_Value := Create_Map.Map;
            tiles : List_Value;
            row   : List_Value;
        begin
            layer.Set( "layer", Create( l ) );
            layer.Set( "type", Create( Layer_Type'Pos( world.map.Get_Layer_Type( l ) ) ) );
            layer.Set( "properties", world.map.Get_Layer_Properties( l ) );

            case world.map.Get_Layer_Type( l ) is
                when Layer_Tiles =>
                    tiles := Create_List.Lst;
                    for r in 0..world.map.Get_Height-1 loop
                        row := Create_List.Lst;
                        for c in 0..world.map.Get_Width-1 loop
                            row.Append( Create( world.Get_Tile_Id( l, c, r ) ) );
                        end loop;
                        tiles.Append( row, consume => True );
                    end loop;
                    layer.Set( "tiles", tiles, consume => True );
                when Layer_Scenery =>
                    layer.Set( "scenery", Create( world.Get_Tile_Id( l, 0, 0 ) ) );
            end case;

            return layer;
        end Export_Layer;

        ------------------------------------------------------------------------

        file     : Ada.Text_IO.File_Type;
        content  : constant Map_Value := Create_Map.Map;
        props    : Map_Value;
        entities : List_Value;
        entity   : Map_Value;
        loc      : A_Location;
        layers   : List_Value;
    begin
        -- world properties
        props := Clone( world.properties ).Map;
        props.Remove( "filename" );                         -- not stored
        props.Remove( "filepath" );                         -- not stored
        props.Remove( "animated" );                         -- not stored
        props.Set( "width", Create( world.map.Get_Width ) );
        props.Set( "height", Create( world.map.Get_Height ) );
        content.Set( "properties", props, consume => True );

        entities := Create_List.Lst;
        for e of world.entities.all loop
            entity := Create_Map.Map;
            entity.Set( "id",         To_Id_Value( e.Get_Id ) );
            entity.Set( "name",       Create( e.Get_Name ) );
            entity.Set( "template",   Create( e.Get_Template ) );
            entity.Set( "attributes", e.Get_Attributes.Get_Public_Values );

            loc := A_Location(e.Get_Component( LOCATION_ID ));
            if loc /= null then
                entity.Set( "x", Create( loc.Get_X ) );
                entity.Set( "y", Create( loc.Get_Y ) );
                entity.Set( "width", Create( loc.Get_Width ) );
                entity.Set( "height", Create( loc.Get_Height ) );
            end if;

            entities.Append( entity, consume => True );
        end loop;
        content.Set( "entities", entities, consume => True );

        layers := Create_List.Lst;
        if world.map.Get_Layer_Count >  0 then
            -- NOTE: Layers must be exported in this order due to the way layers
            -- are imported. When recreating the layers on import, layer 0 must
            -- be added first. Then, the foreground layers are prepended and the
            -- background layers are appended, away from the middle in either
            -- direction.
            layers.Append( Export_Layer( 0 ), consume => True );
            for l in reverse world.map.Get_Foreground_Layer..-1 loop
                layers.Append( Export_Layer( l ), consume => True );
            end loop;
            for l in 1..world.map.Get_Background_Layer loop
                layers.Append( Export_Layer( l ), consume => True );
            end loop;
        end if;
        content.Set( "map", layers, consume => True );

        -- write the structure to disk --
        Ada.Text_IO.Create( file, Ada.Text_IO.Out_File, filePath );
        Ada.Text_IO.Put_Line( file, Image( content ) );
        Ada.Text_IO.Close( file );

        return True;
    exception
        when others =>
            -- failed to create
            return False;
    end Export;

    ----------------------------------------------------------------------------

    function Import( filePath : String ) return A_World is

        ------------------------------------------------------------------------

        function To_Layer_Type( pos : Integer; result : in out Layer_Type ) return Boolean is
        begin
            result := Layer_Type'Val( pos );
            return True;
        exception when others => return False;
        end To_Layer_Type;

        ------------------------------------------------------------------------

        fac        : constant A_Entity_Factory := Entities.Factory.Global;
        world      : A_World;
        file       : Value;
        content    : Map_Value;
        props      : Map_Value;
        tileWidth  : Integer := 16;
        width      : Integer := 1;
        height     : Integer := 1;
        layerList  : List_Value;
        layer      : Map_Value;
        layerIndex : Integer;
        layerType  : Layer_Type := Layer_Type'First;
        tiles      : List_Value;
        row        : List_Value;
        entityList : List_Value;
        entity     : Map_Value;
        e          : A_Entity;
        attrs      : A_Attributes;
    begin
        if not Resources.Scribble.Load_Value( file, filePath, "" ) then
            Dbg( "Failed to load " & filePath & ": " & file.Err.Get_Message,
                 D_RES or D_SCRIPT or D_WORLD, Error );
            return null;       -- file could not be loaded
        end if;

        content := file.Map;
        if not content.Valid or else not content.Contains( "properties" ) then
            return null;       -- bad file format
        end if;

        props := content.Get( "properties" ).Map;
        if not props.Valid then
            return null;       -- missing required world properties
        end if;

        -- - - - World Dimensions - - - --
        tileWidth := props.Get_Int( "tileWidth", tileWidth );
        width := props.Get_Int( "width", 0 );
        height := props.Get_Int( "height", 0 );
        props.Remove( "width" );
        props.Remove( "height" );
        if tileWidth <= 0 or width <= 0 or height <= 0 then
            return null;       -- missing world map size
        end if;

        world := Create_World( width, height, tileWidth, props.Get_String( "library" ) );

        -- - - - World Properties - - - --
        declare
            procedure Set_Property( name : String; val : Value ) is
            begin
                world.Set_Property( name, val );
            end Set_Property;
        begin
            props.Iterate( Set_Property'Access );
        end;

        -- - - - Map Layers - - - --
        layerList := content.Get( "map" ).Lst;
        for i in 1..(if layerList.Valid then layerList.Length else 0) loop
            layer := layerList.Get( i ).Map;
            if layer.Valid then
                layerIndex := layer.Get_Int( "layer", 0 );
                if To_Layer_Type( layer.Get_Int( "type", 0 ), layerType ) then
                    world.map.Add_Layer( layerIndex, layerType, layer.Get( "properties" ).Map );
                    case layerType is

                        -- - - - Scenery Layer - - - --
                        when Layer_Scenery =>
                            world.Set_Tile( layerIndex, 0, 0, Integer'Max( layer.Get_Int( "scenery" ), 0 ) );

                        -- - - - Tiles Layer - - - --
                        when Layer_Tiles =>
                            tiles := layer.Get( "tiles" ).Lst;
                            for r in 1..(if tiles.Valid then tiles.Length else 0) loop
                                row := tiles.Get( r ).Lst;
                                for c in 1..(if row.Valid then row.Length else 0) loop
                                    world.Set_Tile( layerIndex, c - 1, r - 1, Integer'Max( row.Get_Int( c ), 0 ) );
                                end loop;
                            end loop;

                    end case;
                end if;
            end if;
        end loop;

        -- - - - Entities - - - --
        entityList := content.Get( "entities" ).Lst;
        for i in 1..(if entityList.Valid then entityList.Length else 0) loop
            entity := entityList.Get( i ).Map;
            if entity.Valid then
                e := fac.Create_Entity( entity.Get_String( "template" ),
                                        Create_Map( (Pair( "id", entity.Get( "id" ) ),
                                                     Pair( "name", entity.Get( "name" ) ),
                                                     Pair( "Location",
                                                           Create_Map( (Pair( "x", entity.Get( "x" ) ),
                                                                        Pair( "y", entity.Get( "y" ) ),
                                                                        Pair( "width", entity.Get( "width" ) ),
                                                                        Pair( "height", entity.Get( "height" ) )) )
                                                         ))
                                                  ).Map
                                      );
                if e /= null then
                    props := entity.Get( "attributes" ).Map;
                    attrs := e.Get_Attributes;
                    world.Add_Entity( e );

                    -- - - - Entity Attributes - - - --
                    if props.Valid and attrs /= null then
                        declare
                            procedure Set_Attribute( name : String; val : Value ) is
                            begin
                                attrs.Set_Value( name, val );
                            end Set_Attribute;
                        begin
                            props.Iterate( Set_Attribute'Access );
                        end;
                    end if;

                end if;
            end if;
        end loop;

        return world;
    end Import;

end Worlds.Scribble_Format;
