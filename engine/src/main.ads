--
-- Copyright (c) 2012-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

-- These restrictions prevent the 'abort' statement from being used in order to
-- reduce runtime overhead.
-- http://docs.adacore.com/gnat-unw-docs.html/gnat_ugn_8.html#SEC88
pragma Restrictions( No_Abort_Statements );
pragma Restrictions( Max_Asynchronous_Select_Nesting => 0 );

package Main is

    pragma Elaborate_Body;

end Main;
