--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Ada.Calendar;                      use Ada.Calendar;
with Ada.Calendar.Formatting;
with Ada.Calendar.Time_Zones;

package body Version is

    function Release return String
#if VERSION'Defined then
    is ($VERSION);
#else
    is ("0.0.0");
#end if;

    ----------------------------------------------------------------------------

    function Copyright_Year return String is
    begin
        -- $BUILD_TIME is the build time in UTC
        return "2008-" &
            Trim(
                Year_Number'Image(
                    Ada.Calendar.Formatting.Year(
#if BUILD_TIME'Defined then
                        Time'(Ada.Calendar.Formatting.Value( $BUILD_TIME )),
#else
                        Ada.Calendar.Clock,
#end if;
                        Time_Zone => Ada.Calendar.Time_Zones.UTC_Time_Offset
                    )
                ),
                Left
            );
    end Copyright_Year;

    ----------------------------------------------------------------------------

    function Is_Debug return Boolean
#if DEBUG then
    is (True);
#else
    is (False);
#end if;

end Version;
