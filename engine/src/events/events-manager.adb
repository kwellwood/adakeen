--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Debugging;                         use Debugging;
with Events;

package body Events.Manager is

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Event_Manager return A_Event_Manager is
        this : constant A_Event_Manager := new Event_Manager;
    begin
        this.Construct;
        return this;
    end Create_Event_Manager;

    ----------------------------------------------------------------------------

    procedure End_Frame( this : not null access Event_Manager'Class;
                         chan : Event_Channel ) is
        listeners : Corral_Maps.Map;
        listener  : Corral_Maps.Cursor;
    begin
        if chan = NO_CHANNEL then
            return;
        end if;

        this.lock.Lock;
        listeners := Copy( this.corralRefs );
        this.lock.Unlock;

        listener := listeners.First;
        while Has_Element( listener ) loop
            Key( listener ).End_Frame( chan );
            Next( listener );
        end loop;
    end End_Frame;

    ----------------------------------------------------------------------------

    procedure Queue_Event( this : not null access Event_Manager'Class;
                           evt  : in out A_Event;
                           chan : Event_Channel := NO_CHANNEL ) is
        idPos     : Event_Id_Maps.Cursor;
        remaining : Natural;
        e         : A_Event;
    begin
        evt.evtChan := chan;

        this.lock.Lock;
        -- find the list of corrals registered for the event id
        idPos := this.idMap.Find( evt.Get_Event_Id );
        if Has_Element( idPos ) then
            remaining := Natural(this.idMap.Constant_Reference( idPos ).Length);
            if remaining > 0 then
                -- iterate over the registered corrals
                for corral of this.idMap.Constant_Reference( idPos ).Element.all loop
                    -- sending the event to N corrals requires N-1 copies.
                    -- the last corral in the list gets the actual object, not a copy
                    e := (if remaining > 1 then Copy( evt ) else evt);
                    corral.Dispatch_Async( e );         -- consumes 'e'
                    remaining := remaining - 1;
                end loop;
                evt := null;    -- 'evt' was consumed
            else
                Dbg( "No listeners for queued event " & evt.To_String, D_EVENTS, Warning );
            end if;
        end if;
        Delete( evt );
        this.lock.Unlock;
    end Queue_Event;

    ----------------------------------------------------------------------------

    procedure Register_Corral( this   : not null access Event_Manager'Class;
                               corral : not null A_Corral;
                               evtId  : Event_Id ) is
        idPos     : Event_Id_Maps.Cursor;
        refPos    : Corral_Maps.Cursor;
        emptyList : Corral_Vectors.Vector;
        inserted  : Boolean;
    begin
        this.lock.Lock;

        -- find the list of corrals already registered for the event id
        idPos := this.idMap.Find( evtId );
        if not Has_Element( idPos ) then
            -- insert an empty listener list if the event id not found
            this.idMap.Insert( evtId, emptyList, idPos, inserted );
        end if;
        -- add 'corral' to the listeners of the event id
        if not this.idMap.Constant_Reference( idPos ).Contains( corral ) then
            this.idMap.Reference( idPos ).Append( corral );
            refPos := this.corralRefs.Find( corral );
            if Has_Element( refPos ) then
                -- increment the corral's reference count
                this.corralRefs.Replace_Element( refPos, Element( refPos ) + 1 );
            else
                -- add the first reference
                this.corralRefs.Insert( corral, 1 );
            end if;
        else
            Dbg( "Corral <" & corral.Get_Name &
                 "> already registered for event id" & Event_Id'Image( evtId ),
                 D_EVENTS, Warning );
        end if;

        this.lock.Unlock;
    end Register_Corral;

    ----------------------------------------------------------------------------

    procedure Trigger_Event( this     : not null access Event_Manager'Class;
                             evt      : in out A_Event;
                             response : out Response_Type ) is
        idPos       : Event_Id_Maps.Cursor;
        listeners : Corral_Vectors.Vector;
    begin
        pragma Debug( Dbg( "Triggering " & To_String( evt ), D_EVENTS, Info ) );

        this.lock.Lock;
        -- find the list of corrals already registered for the event id
        idPos := this.idMap.Find( evt.Get_Event_Id );
        if Has_Element( idPos ) then
            -- make a copy of the list, to avoid holding the event manager lock
            listeners := Copy( this.idMap.Constant_Reference( idPos ) );
        end if;
        this.lock.Unlock;

        -- try to dispatch the event to each corral in the listener list
        -- until one of them consumes it
        for corral of listeners loop
            --pragma Debug( Dbg( "Triggering " & To_String( evt ) &
            --                   " on corral " & corral.Get_Name,
            --                   D_EVENTS, Info ) );
            corral.Dispatch_Sync( evt, response );
            exit when evt = null;
        end loop;

        if evt /= null then
            Dbg( "Trigger_Event: No responding listener for " & To_String( evt ), D_EVENTS, Warning );
        end if;
        Delete( evt );
    end Trigger_Event;

    ----------------------------------------------------------------------------

    procedure Unregister_Corral( this   : not null access Event_Manager'Class;
                                 corral : not null A_Corral;
                                 evtId  : Event_Id ) is
        idPos  : Event_Id_Maps.Cursor;
        refPos : Corral_Maps.Cursor;
        lPos   : Corral_Vectors.Cursor;
    begin
        this.lock.Lock;

        refPos := this.corralRefs.Find( corral );
        if not Has_Element( refPos ) then
            this.lock.Unlock;
            Dbg( "Unable to unregister corral <" & corral.Get_Name &
                 "> for event id" & Event_Id'Image( evtId ) & ": Corral not registered",
                 D_EVENTS, Warning );
            return;
        end if;

        -- find the list of corrals already registered for the event id
        idPos := this.idMap.Find( evtId );
        if Has_Element( idPos ) then
            -- find 'corral' within the list of listeners for 'evtId'
            lPos := this.idMap.Reference( idPos ).Find( corral );
        end if;
        if Has_Element( lPos ) then
            -- found; remove it from the list
            this.idMap.Reference( idPos ).Delete( lPos );

            -- decrement the reference count
            if Element( refPos ) > 1 then
                this.corralRefs.Replace_Element( refPos, Element( refPos ) - 1 );
            else
                this.corralRefs.Delete( refPos );
            end if;
        else
            Dbg( "Unable to unregister corral <" & corral.Get_Name &
                 "> for event id" & Event_Id'Image( evtId ) & ": Corral not registered",
                 D_EVENTS, Warning );
        end if;

        this.lock.Unlock;
    end Unregister_Corral;

    ----------------------------------------------------------------------------

    procedure Unregister_Corral( this   : not null access Event_Manager'Class;
                                 corral : not null A_Corral ) is
        refPos : Corral_Maps.Cursor;
        lPos   : Corral_Vectors.Cursor;
    begin
        this.lock.Lock;
        -- remove the corral completely from the reference count list
        refPos := this.corralRefs.Find( corral );
        if not Has_Element( refPos ) then
            this.lock.Unlock;
            return;
        end if;
        this.corralRefs.Delete( refPos );

        -- iterate over the listener lists of all events
        for list of this.idMap loop
            -- find 'corral' in the listener list for this particular event
            lPos := list.Find( corral );
            if Has_Element( lPos ) then
                -- found; remove it and continue to other event ids
                list.Delete( lPos );
            end if;
        end loop;
        this.lock.Unlock;
    end Unregister_Corral;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Event_Manager ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

end Events.Manager;
