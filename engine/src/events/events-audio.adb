--
-- Copyright (c) 2012-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Events.Manager;

package body Events.Audio is

    not overriding
    procedure Construct( this        : access Play_Audio_Event;
                         evtId       : Event_Id;
                         audioName   : String;
                         repeatDelay : Time_Span ) is
    begin
        Event(this.all).Construct( evtId );
        this.audioName := To_Unbounded_String( audioName );
        this.repeatDelay := repeatDelay;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Audio_Name( this : not null access Play_Audio_Event'Class ) return String is (To_String( this.audioName ));

    function Get_Repeat_Delay( this : not null access Play_Audio_Event'Class ) return Time_Span is (this.repeatDelay);

    overriding
    function To_String( this : access Play_Audio_Event ) return String
    is ("<Audio_Event - name: '" & this.Get_Audio_Name & "'>");

    --==========================================================================

    procedure Queue_Play_Music( name : String ) is
        evt : A_Play_Audio_Event := new Play_Audio_Event;
    begin
        evt.Construct( EVT_PLAY_MUSIC, name, Time_Span_Zero );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_Play_Music;

    ----------------------------------------------------------------------------

    procedure Queue_Play_Sound( name : String; repeatDelay : Time_Span := Time_Span_Zero ) is
        evt : A_Play_Audio_Event := new Play_Audio_Event;
    begin
        evt.Construct( EVT_PLAY_SOUND, name, repeatDelay );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_Play_Sound;

    ----------------------------------------------------------------------------

    procedure Queue_Stop_Music is
        evt : A_Event := new Event;
    begin
        evt.Construct( EVT_STOP_MUSIC );
        Events.Manager.Global.Queue_Event( evt, GAME_CHANNEL );
    end Queue_Stop_Music;

end Events.Audio;
