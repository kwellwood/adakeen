--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Events.Manager;
with Values.Construction;               use Values.Construction;
with Values.Strings;                    use Values.Strings;

package body Events.Game is

    not overriding
    procedure Construct( this   : access Gameplay_Event;
                         evtId  : Event_Id;
                         paused : Boolean ) is
    begin
        Event(this.all).Construct( evtId );
        this.paused := paused;
    end Construct;

    ----------------------------------------------------------------------------

    function Is_Paused( this : not null access Gameplay_Event'Class ) return Boolean is (this.paused);

    overriding
    function To_String( this : access Gameplay_Event ) return String
    is ("<Gameplay_Event - paused: " & Boolean'Image( this.paused ) & ">");

    --==========================================================================

    overriding
    procedure Adjust( this : access Game_Var_Changed_Event ) is
    begin
        Event(this.all).Adjust;
        this.val := Clone( this.val );
    end Adjust;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Game_Var_Changed_Event;
                         var  : String;
                         val  : Value'Class ) is
    begin
        Event(this.all).Construct( EVT_GAME_VAR_CHANGED );
        this.var := To_Unbounded_String( var );
        this.val := Clone( val );
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Value( this : not null access Game_Var_Changed_Event'Class ) return Value is (this.val);

    function Get_Var( this : not null access Game_Var_Changed_Event'Class ) return String is (To_String( this.var ));

    overriding
    function To_String( this : access Game_Var_Changed_Event ) return String
    is (
        "<Game_Var_Changed_Event - var: " & To_String( this.var ) &
        ", val: " & this.val.Image & ">"
    );

    --==========================================================================

    not overriding
    procedure Construct( this    : access Loading_Event;
                         eId     : Event_Id;
                         success : Boolean;
                         error   : String ) is
    begin
        Event(this.all).Construct( eId );
        this.success := success;
        this.error := To_Unbounded_String( error );
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Error( this : not null access Loading_Event'Class ) return String is (To_String( this.error ));

    function Is_Success( this : not null access Loading_Event'Class ) return Boolean is (this.success);

    overriding
    function To_String( this : access Loading_Event ) return String
    is (
        "<Loading_Event - success: " & Boolean'Image( this.success ) &
        ", error: """ & To_String( this.error ) & """>"
    );

    --==========================================================================

    overriding
    procedure Adjust( this : access Script_Event ) is
    begin
        Event(this.all).Adjust;
        this.result := Clone( this.result );
    end Adjust;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this   : access Script_Event;
                         evtId  : Event_Id;
                         taskId : Integer;
                         source : Unbounded_String;
                         result : Value'Class ) is
    begin
        Event(this.all).Construct( evtId );
        this.taskId := taskId;
        this.source := source;
        this.result := Clone( result );
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Result( this : not null access Script_Event'Class ) return Value is (this.result);

    function Get_Source( this : not null access Script_Event'Class ) return Unbounded_String is (this.source);

    function Get_TaskId( this : not null access Script_Event'Class ) return Integer is (this.taskId);

    overriding
    function To_String( this : access Script_Event ) return String
    is (
        "<Script_Event - evtId:" & Event_Id'Image( this.evtId ) &
        ", taskId:" & Integer'Image( this.taskId ) & ">"
    );

    --==========================================================================

    overriding
    procedure Adjust( this : access Message_Event ) is
    begin
        Event(this.all).Adjust;
        this.args := Clone( this.args ).Lst;
    end Adjust;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this  : access Message_Event;
                         evtId : Event_Id;
                         name  : String;
                         args  : Value_Array ) is
    begin
        Event(this.all).Construct( evtId );
        this.name := To_Unbounded_String( name );
        this.args := Create_List( args ).Lst;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Args( this : not null access Message_Event'Class ) return List_Value is (this.args);

    function Get_Name( this : not null access Message_Event'Class ) return String is (To_String( this.name ));

    --==========================================================================

    procedure Queue_Message( evtId   : Event_Id;
                             channel : Event_Channel;
                             name    : String;
                             args    : Value_Array ) is
        evt : A_Message_Event := new Message_Event;
    begin
        evt.Construct( evtId, name, args );
        Events.Manager.Global.Queue_Event( A_Event(evt), channel );
    end Queue_Message;

    --==========================================================================

    procedure Queue_Console_Text( text : String ) is
        evt : A_Script_Event := new Script_Event;
    begin
        evt.Construct( EVT_CONSOLE_TEXT, 0, Null_Unbounded_String, Create( text ) );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_Console_Text;

    ----------------------------------------------------------------------------

    procedure Queue_Game_Message( name : String ) is
        args : Value_Array(1..0);
    begin
        Queue_Message( EVT_GAME_MESSAGE, VIEW_CHANNEL, name, args );       -- from view to game
    end Queue_Game_Message;

    ----------------------------------------------------------------------------

    procedure Queue_Game_Message( name : String; args : Value_Array ) is
    begin
        Queue_Message( EVT_GAME_MESSAGE, VIEW_CHANNEL, name, args );       -- from view to game
    end Queue_Game_Message;

    ----------------------------------------------------------------------------

    procedure Queue_Game_Paused( paused : Boolean ) is
        evt : A_Gameplay_Event := new Gameplay_Event;
    begin
        evt.Construct( EVT_GAME_PAUSED, paused );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_Game_Paused;

    ----------------------------------------------------------------------------

    procedure Queue_Game_Started is
        evt : A_Event := new Event;
    begin
        evt.Construct( EVT_GAME_STARTED );
        Events.Manager.Global.Queue_Event( evt, GAME_CHANNEL );
    end Queue_Game_Started;

    ----------------------------------------------------------------------------

    procedure Queue_Game_Var_Changed( var : String; val : Value'Class ) is
        evt : A_Game_Var_Changed_Event := new Game_Var_Changed_Event;
    begin
        evt.Construct( var, val );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_Game_Var_Changed;

    ----------------------------------------------------------------------------

    procedure Queue_Loading_Began is
        evt : A_Event := new Event;
    begin
        -- keep this special event in-order with game channel events but send it
        -- out-of-band so it arrives ASAP. this is done by flushing the events
        -- in the game channel and then sending it out-of-band. the following
        -- game channel events will still be framed.
        Events.Manager.Global.End_Frame( GAME_CHANNEL );
        evt.Construct( EVT_LOADING_BEGAN );
        Events.Manager.Global.Queue_Event( evt, NO_CHANNEL );
    end Queue_Loading_Began;

    ----------------------------------------------------------------------------

    procedure Queue_Loading_Ended( success : Boolean := True; errorMsg : String := "" ) is
        evt : A_Loading_Event := new Loading_Event;
    begin
        evt.Construct( EVT_LOADING_ENDED, success, errorMsg );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_Loading_Ended;

    ----------------------------------------------------------------------------

    procedure Queue_Pause_Game( paused : Boolean ) is
        evt : A_Gameplay_Event := new Gameplay_Event;
    begin
        evt.Construct( EVT_PAUSE_GAME, paused );

        -- NOTE: Pause_Game events need to be sent out-of-band because the view
        -- may need to pause the game to show a native dialog without returning
        -- to the event loop. In this case, the VIEW_CHANNEL frame won't be
        -- completed until after the dialog has already been opened and closed.
        Events.Manager.Global.Queue_Event( A_Event(evt), NO_CHANNEL );
    end Queue_Pause_Game;

    ----------------------------------------------------------------------------

    procedure Queue_Run_Script( source : Unbounded_String; taskId : Integer := 0 ) is
        evt : A_Script_Event := new Script_Event;
    begin
        evt.Construct( EVT_RUN_SCRIPT, taskId, source, Null_Value );
        Events.Manager.Global.Queue_Event( A_Event(evt), VIEW_CHANNEL );
    end Queue_Run_Script;

    ----------------------------------------------------------------------------

    procedure Queue_Script_Done( result : Value'Class; taskId : Integer ) is
        evt : A_Script_Event := new Script_Event;
    begin
        evt.Construct( EVT_SCRIPT_DONE, taskId, Null_Unbounded_String, result );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_Script_Done;

    ----------------------------------------------------------------------------

    procedure Queue_Start_Game is
        evt : A_Event := new Event;
    begin
        evt.Construct( EVT_START_GAME );
        Events.Manager.Global.Queue_Event( evt, VIEW_CHANNEL );
    end Queue_Start_Game;

    ----------------------------------------------------------------------------

    procedure Queue_View_Message( name : String ) is
        args : Value_Array(1..0);
    begin
        Queue_Message( EVT_VIEW_MESSAGE, GAME_CHANNEL, name, args );       -- from game to view
    end Queue_View_Message;

    ----------------------------------------------------------------------------

    procedure Queue_View_Message( name : String; args : Value_Array ) is
    begin
        Queue_Message( EVT_VIEW_MESSAGE, GAME_CHANNEL, name, args );       -- from game to view
    end Queue_View_Message;

end Events.Game;
