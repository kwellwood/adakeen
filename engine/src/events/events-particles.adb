--
-- Copyright (c) 2015-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Events.Manager;                    use Events.Manager;
with Support;                           use Support;
with Values.Construction;               use Values.Construction;

package body Events.Particles is

    overriding
    procedure Adjust( this : access Particles_Event ) is
    begin
        Event(this.all).Adjust;
        this.emitter := Clone( this.emitter ).Map;
    end Adjust;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this        : access Particles_Event;
                         evtId       : Event_Id;
                         componentId : Component_Id;
                         emitter     : Map_Value;
                         x, y, z     : Float;
                         rate        : Integer;
                         lifetime    : Time_Span;
                         entityId    : Entity_Id ) is
    begin
        Event(this.all).Construct( evtId );
        this.componentId := componentId;
        this.emitter := Clone( emitter ).Map;
        this.x := x;
        this.y := y;
        this.z := z;
        this.rate := rate;
        this.lifetime := lifetime;
        this.entityId := entityId;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Component_Id( this : not null access Particles_Event'Class ) return Component_Id is (this.componentId);

    function Get_Emitter( this : not null access Particles_Event'Class ) return Map_Value is (this.emitter);

    function Get_Entity_Id( this : not null access Particles_Event'Class ) return Entity_Id is (this.entityId);

    function Get_Lifetime( this : not null access Particles_Event'Class ) return Time_Span is (this.lifetime);

    function Get_Rate( this : not null access Particles_Event'Class ) return Integer is (this.rate);

    function Get_X( this : not null access Particles_Event'Class ) return Float is (this.x);

    function Get_Y( this : not null access Particles_Event'Class ) return Float is (this.y);

    function Get_Z( this : not null access Particles_Event'Class ) return Float is (this.z);

    ----------------------------------------------------------------------------

    overriding
    function To_String( this : access Particles_Event ) return String
    is ("<Particles_Event - x: " & Image( this.x ) & ", y: " & Image( this.y ) & ">");

    --==========================================================================

    procedure Queue_Particle_Burst( emitter : Value'Class;
                                    x, y, z : Float;
                                    count   : Integer ) is
        evt : A_Particles_Event;
        em  : Map_Value;
    begin
        if emitter.Is_String then
            em := Create( (1 => Pair( "name", emitter )) ).Map;
        elsif emitter.Is_Map then
            em := emitter.Map;
        else
            return;
        end if;

        evt := new Particles_Event;
        evt.Construct( EVT_PARTICLE_BURST, NULL_ID, em, x, y, z, count, Milliseconds( 0 ), NULL_ID );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_Particle_Burst;

    ----------------------------------------------------------------------------

    procedure Queue_Particle_Burst( emitter  : Value'Class;
                                    x, y, z  : Float;
                                    rate     : Integer;
                                    lifetime : Time_Span;
                                    entityId : Entity_Id := NULL_ID ) is
        evt : A_Particles_Event;
        em  : Map_Value;
    begin
        if emitter.Is_String then
            em := Create( (1 => Pair( "name", emitter )) ).Map;
        elsif emitter.Is_Map then
            em := emitter.Map;
        else
            return;
        end if;

        evt := new Particles_Event;
        evt.Construct( EVT_PARTICLE_BURST, NULL_ID, em, x, y, z, rate, lifetime, entityId );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_Particle_Burst;

    ----------------------------------------------------------------------------

    procedure Queue_Start_Particles( componentId : Component_Id;
                                     emitter     : Value'Class;
                                     x, y, z     : Float;
                                     rate        : Integer;
                                     entityId    : Entity_Id := NULL_ID ) is
        evt : A_Particles_Event;
        em  : Map_Value;
    begin
        if emitter.Is_String then
            em := Create( (1 => Pair( "name", emitter )) ).Map;
        elsif emitter.Is_Map then
            em := emitter.Map;
        else
            return;
        end if;

        evt := new Particles_Event;
        evt.Construct( EVT_START_PARTICLES, componentId, em, x, y, z, rate, Milliseconds( 0 ), entityId );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_Start_Particles;

    ----------------------------------------------------------------------------

    procedure Queue_Stop_Particles( componentId : Component_Id ) is
        evt : A_Particles_Event := new Particles_Event;
    begin
        evt.Construct( evtId       => EVT_STOP_PARTICLES,
                       componentId => componentId,
                       emitter     => Null_Value.Map,
                       x           => 0.0,
                       y           => 0.0,
                       z           => 0.0,
                       rate        => 0,
                       lifetime    => Milliseconds( 0 ),
                       entityId    => NULL_ID );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_Stop_Particles;

end Events.Particles;

