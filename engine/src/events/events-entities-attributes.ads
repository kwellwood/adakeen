--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Events.Entities.Attributes is

    -- A notification that an entity attribute has changed.
    EVT_ENTITY_ATTRIBUTE_CHANGED : constant Event_Id := 302;

    -- A command to set an attribute of an entity.
    EVT_SET_ENTITY_ATTRIBUTE : constant Event_Id := 303;

    -- An event involving an entity attribute.
    type Entity_Attribute_Event is new Entity_Event with private;
    type A_Entity_Attribute_Event is access all Entity_Attribute_Event'Class;

    -- Returns the attribute name.
    function Get_Attribute( this : not null access Entity_Attribute_Event'Class ) return String;

    -- Returns the attribute's value. (not a copy, do not modify it!)
    function Get_Value( this : not null access Entity_Attribute_Event'Class ) return Value;

    ----------------------------------------------------------------------------

    -- Queues an Entity_Attribute_Changed event.
    procedure Queue_Entity_Attribute_Changed( id        : Entity_Id;
                                              attribute : String;
                                              val       : Value'Class );

    -- Queues a Set_Entity_Attribute event with a value of any type.
    procedure Queue_Set_Entity_Attribute( id        : Entity_Id;
                                          attribute : String;
                                          val       : Value'Class );

private

    type Entity_Attribute_Event is new Entity_Event with
        record
            attribute : Unbounded_String;
            val       : Value;
        end record;

    procedure Adjust( this : access Entity_Attribute_Event );

    -- 'name' is event name
    -- 'attribute' is the name of the attribute
    -- 'val' is the value of the attribute
    procedure Construct( this      : access Entity_Attribute_Event;
                         evtId     : Event_Id;
                         id        : Entity_Id;
                         attribute : String;
                         val       : Value'Class );

end Events.Entities.Attributes;
