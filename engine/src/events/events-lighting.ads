--
-- Copyright (c) 2015-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Object_Ids;                        use Object_Ids;
with Values.Maps;                       use Values.Maps;
with Values.Tagged_Ids;                 use Values.Tagged_Ids;

package Events.Lighting is

    -- A command to show a light source.
    EVT_LIGHT_CREATED : constant Event_Id := 600;

    -- A command to hide a light source.
    EVT_LIGHT_DELETED : constant Event_Id := 601;

    -- A command to updated the properties of a light source.
    EVT_LIGHT_UPDATED : constant Event_Id := 602;

    type Light_Event is new Event with private;
    type A_Light_Event is access all Light_Event'Class;

    -- Returns the id of the light component that queued the event.
    function Get_Component_Id( this : not null access Light_Event'Class ) return Component_Id;

    -- Returns the id of the entity the light is attached to.
    function Get_Entity_Id( this : not null access Light_Event'Class ) return Entity_Id;

    -- Returns the light's properties. On an update event, only the given
    -- properties are changing.
    function Get_Properties( this : not null access Light_Event'Class ) return Map_Value;

    ----------------------------------------------------------------------------

    -- Queues a Light_On event to show light 'componentId' attached to entity
    -- 'entityId'. The light will move with the entity. 'x,y' is the light's
    -- location in world coordinates.
    procedure Queue_Light_Created( componentId : Component_Id;
                                   entityId    : Entity_Id;
                                   properties  : Map_Value'Class );

    -- Queues a Light_Off event to hide the light 'componentId'.
    procedure Queue_Light_Deleted( componentId : Component_Id );

    -- Queues a Light_Updated event to notify that the properties of
    -- 'componentId' have changed. Only the given properties have changed.
    procedure Queue_Light_Updated( componentId : Component_Id;
                                   properties  : Map_Value'Class );

private

    type Light_Event is new Event with
        record
            componentId : Component_Id := NULL_ID;
            entityId    : Entity_Id := NULL_ID;
            properties  : Map_Value;
        end record;

    procedure Adjust( this : access Light_Event );

    procedure Construct( this        : access Light_Event;
                         evtId       : Event_Id;
                         componentId : Component_Id;
                         entityId    : Entity_Id;
                         properties  : Map_Value'Class );

    function To_String( this : access Light_Event ) return String;

end Events.Lighting;
