--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Events.Manager;

package body Events.Entities.Camera_Targets is

    not overriding
    procedure Construct( this   : access Follow_Entity_Event;
                         id     : Entity_Id;
                         top    : Integer;
                         bottom : Integer;
                         left   : Integer;
                         right  : Integer ) is
    begin
        Entity_Event(this.all).Construct( EVT_FOLLOW_ENTITY, id );
        this.top := top;
        this.bottom := bottom;
        this.left := left;
        this.right := right;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Bounds_Bottom( this : not null access Follow_Entity_Event'Class ) return Integer is (this.bottom);

    function Get_Bounds_Left( this : not null access Follow_Entity_Event'Class ) return Integer is (this.left);

    function Get_Bounds_Right( this : not null access Follow_Entity_Event'Class ) return Integer is (this.right);

    function Get_Bounds_Top( this : not null access Follow_Entity_Event'Class ) return Integer is (this.top);

    --==========================================================================

    procedure Queue_Follow_Entity( id     : Entity_Id;
                                   top    : Integer := 0;
                                   bottom : Integer := 0;
                                   left   : Integer := 0;
                                   right  : Integer := 0 ) is
        evt : A_Follow_Entity_Event := new Follow_Entity_Event;
    begin
        evt.Construct( id, top, bottom, left, right );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_Follow_Entity;

end Events.Entities.Camera_Targets;
