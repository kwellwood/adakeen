--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Events.Entities.Sprites is

    EVT_FRAME_CHANGED : constant Event_Id := 310;

    -- A notification that an entity's display frame has changed.
    type Frame_Changed_Event is new Entity_Event with private;
    type A_Frame_Changed_Event is access all Frame_Changed_Event'Class;

    -- Returns the id of the new display frame or 0 for none.
    function Get_Frame( this : not null access Frame_Changed_Event'Class ) return Natural;

    ----------------------------------------------------------------------------

    EVT_ENTITY_Z_CHANGED : constant Event_Id := 311;

    -- A notification that an entity's Z depth has changed.
    type Entity_Z_Event is new Entity_Event with private;
    type A_Entity_Z_Event is access all Entity_Z_Event'Class;

    -- Returns the new Z depth of the entity.
    function Get_Z( this : not null access Entity_Z_Event'Class ) return Float;

    -- Returns the new Z draw order of the entity.
    function Get_ZOrder( this : not null access Entity_Z_Event'Class ) return Integer;

    ----------------------------------------------------------------------------

    -- Queues a Frame_Changed event.
    procedure Queue_Frame_Changed( id : Entity_Id; frame : Natural );

    -- Queues a Entity_Z event.
    procedure Queue_Entity_Z_Changed( id : Entity_Id; z : Float; zOrder : Integer );

private

    ----------------------------------------------------------------------------

    type Frame_Changed_Event is new Entity_Event with
        record
            frame : Natural;
        end record;

    procedure Construct( this  : access Frame_Changed_Event;
                         id    : Entity_Id;
                         frame : Natural );

    ----------------------------------------------------------------------------

    type Entity_Z_Event is new Entity_Event with
        record
            z      : Float;
            zOrder : Integer;
        end record;

    procedure Construct( this   : access Entity_Z_Event;
                         id     : Entity_Id;
                         z      : Float;
                         zOrder : Integer );

end Events.Entities.Sprites;
