--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Events.Entities.Camera_Targets is

    EVT_FOLLOW_ENTITY : constant Event_Id := 309;

    -- A command to the game view to follow an entity with the scene.
    type Follow_Entity_Event is new Entity_Event with private;
    type A_Follow_Entity_Event is access all Follow_Entity_Event'Class;

    -- Returns the bottom edge of the bounding box, relative to camera center.
    function Get_Bounds_Bottom( this : not null access Follow_Entity_Event'Class ) return Integer;

    -- Returns the left edge of the bounding box, relative to camera center.
    function Get_Bounds_Left( this : not null access Follow_Entity_Event'Class ) return Integer;

    -- Returns the right of the bounding box, relative to camera center.
    function Get_Bounds_Right( this : not null access Follow_Entity_Event'Class ) return Integer;

    -- Returns the top edge of the bounding box, relative to camera center.
    function Get_Bounds_Top( this : not null access Follow_Entity_Event'Class ) return Integer;

    ----------------------------------------------------------------------------

    -- Queues a Follow_Entity event.
    procedure Queue_Follow_Entity( id     : Entity_Id;
                                   top    : Integer := 0;
                                   bottom : Integer := 0;
                                   left   : Integer := 0;
                                   right  : Integer := 0 );

private

    type Follow_Entity_Event is new Entity_Event with
        record
            top    : Integer := 0;
            bottom : Integer := 0;
            left   : Integer := 0;
            right  : Integer := 0;
        end record;

    procedure Construct( this   : access Follow_Entity_Event;
                         id     : Entity_Id;
                         top    : Integer;
                         bottom : Integer;
                         left   : Integer;
                         right  : Integer );

end Events.Entities.Camera_Targets;
