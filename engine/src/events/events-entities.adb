--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Component_Families;                use Component_Families;
--with Debugging;                         use Debugging;
with Events.Manager;
with Support;                           use Support;
with Values.Construction;               use Values.Construction;

package body Events.Entities is

    not overriding
    procedure Construct( this  : access Entity_Event;
                         evtId : Event_Id;
                         id    : Entity_Id ) is
    begin
        Event(this.all).Construct( evtId );
        this.id := id;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Id( this : not null access Entity_Event'Class ) return Entity_Id is (this.id);

    overriding
    function To_String( this : access Entity_Event ) return String
    is ("<Entity_Event - evtId:" & this.evtId'Img & ", id: " & Image( this.id ) & ">");

    --==========================================================================

    not overriding
    procedure Construct( this      : access Entity_Directive_Event;
                         id        : Entity_Id;
                         directive : Directive_Bits;
                         mode      : Directive_Mode ) is
    begin
        Entity_Event(this.all).Construct( EVT_ENTITY_DIRECTIVE, id );
        this.directive := directive;
        this.mode := mode;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Directive( this : not null access Entity_Directive_Event'Class ) return Directive_Bits is (this.directive);

    function Get_Mode( this : not null access Entity_Directive_Event'Class ) return Directive_Mode is (this.mode);

    overriding
    function To_String( this : access Entity_Directive_Event ) return String
    is (
        "<Entity_Directive_Event - id: " & Image( this.id ) &
        ", directive: " & Directive_Bits'Image( this.directive ) &
        ", mode: " & Directive_Mode'Image( this.mode ) & ">"
    );

    --==========================================================================

    procedure Adjust( this : access Entity_Spawn_Event ) is
    begin
        Entity_Event(this.all).Adjust;
        this.state := Clone( this.state ).Map;
    end Adjust;

    ----------------------------------------------------------------------------

    procedure Construct( this     : access Entity_Spawn_Event;
                         evtId    : Event_Id;
                         template : String;
                         state    : Map_Value;
                         name     : String;
                         id       : Entity_Id ) is
    begin
        Entity_Event(this.all).Construct( evtId, (if id /= NULL_ID then id
                                                  elsif state.Valid then Cast_Tagged_Id( state.Get( "id" ), NULL_ID )
                                                  else NULL_ID) );
        this.template := To_Unbounded_String( template );

        if state.Valid then
            this.state := Clone( state ).Map;
        end if;

        if name'Length > 0 then
            this.name := To_Unbounded_String( name );
        elsif state.Valid then
            this.name := state.Get_Unbounded_String( "name", "" );
        end if;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Entity_Name( this : not null access Entity_Spawn_Event'Class ) return String is (To_String( this.name ));

    ----------------------------------------------------------------------------

    function Get_State( this : not null access Entity_Spawn_Event'Class ) return Map_Value is (this.state);

    ----------------------------------------------------------------------------

    function Get_Template( this : not null access Entity_Spawn_Event'Class ) return String is (To_String( this.template ));

    ----------------------------------------------------------------------------

    overriding
    function To_String( this : access Entity_Spawn_Event ) return String is
        loc : constant Map_Value := this.state.Get( Family_Name( LOCATION_ID ) ).Map;
    begin
        return "<Entity_Spawn_Event - template: " & this.Get_Template &
               ", x: " & (if loc.Valid then Image( loc.Get_Float( "x" ) ) else "?") &
               ", y: " & (if loc.Valid then Image( loc.Get_Float( "x" ) ) else "?") & ">";
    end To_String;

    --==========================================================================

    overriding
    procedure Adjust( this : access Message_Entity_Event ) is
    begin
        Entity_Event(this.all).Adjust;
        this.params := Clone( this.params ).Map;
    end Adjust;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this    : access Message_Entity_Event;
                         id      : Entity_Id;
                         message : Hashed_String;
                         params  : Map_Value ) is
    begin
        Entity_Event(this.all).Construct( EVT_MESSAGE_ENTITY, id );
        this.message := message;
        this.params := Clone( params ).Map;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Message( this : not null access Message_Entity_Event'Class ) return Hashed_String is (this.message);

    function Get_Parameters( this : not null access Message_Entity_Event'Class ) return Map_Value is (this.params);

    ----------------------------------------------------------------------------

    overriding
    function To_String( this : access Message_Entity_Event ) return String
    is (
        "<Message_Entity_Event - id: " & Image( this.id ) &
        ", message: " & To_String( this.message ) & ">"
    );

    --==========================================================================

    overriding
    procedure Adjust( this : access Spawn_Entity_Event ) is
    begin
        Event(this.all).Adjust;
        this.properties := Clone( this.properties ).Map;
    end Adjust;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this       : access Spawn_Entity_Event;
                         template   : String;
                         properties : Map_Value ) is
    begin
        Event(this.all).Construct( EVT_SPAWN_ENTITY );
        this.template := To_Unbounded_String( template );
        this.properties := Clone( properties ).Map;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Properties( this : not null access Spawn_Entity_Event'Class ) return Map_Value is (this.properties);

    function Get_Template( this : not null access Spawn_Entity_Event'Class ) return String is (To_String( this.template ));

    overriding
    function To_String( this : access Spawn_Entity_Event ) return String
    is ("<Spawn_Entity_Event - template: " & To_String( this.template ) & ">");

    --==========================================================================

    function Create_Entity_Spawned_Event( id           : Entity_Id;
                                          name         : String;
                                          template     : String;
                                          initialState : Map_Value ) return A_Event is
        evt : constant A_Entity_Spawn_Event := new Entity_Spawn_Event;
    begin
        evt.Construct( EVT_ENTITY_SPAWNED, template, initialState, name, id );
        return A_Event(evt);
    end Create_Entity_Spawned_Event;

    ----------------------------------------------------------------------------

    procedure Queue_Delete_Entity( id : Entity_Id ) is
        evt : A_Entity_Event := new Entity_Event;
    begin
        evt.Construct( EVT_DELETE_ENTITY, id );
        Events.Manager.Global.Queue_Event( A_Event(evt) );
    end Queue_Delete_Entity;

    ----------------------------------------------------------------------------

    procedure Queue_Entity_Deleted( id : Entity_Id ) is
        evt : A_Entity_Event := new Entity_Event;
    begin
        evt.Construct( EVT_ENTITY_DELETED, id );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_Entity_Deleted;

    ----------------------------------------------------------------------------

    procedure Queue_Entity_Directive( id        : Entity_Id;
                                      directive : Directive_Bits;
                                      mode      : Directive_Mode ) is
        evt : A_Entity_Directive_Event := new Entity_Directive_Event;
    begin
        evt.Construct( id, directive, mode );
        Events.Manager.Global.Queue_Event( A_Event(evt), VIEW_CHANNEL );
    end Queue_Entity_Directive;

    ----------------------------------------------------------------------------

    procedure Queue_Message_Entity( id      : Entity_Id;
                                    message : Hashed_String;
                                    params  : Map_Value ) is
        evt : A_Message_Entity_Event := new Message_Entity_Event;
    begin
        evt.Construct( id, message, params );
        Events.Manager.Global.Queue_Event( A_Event(evt), VIEW_CHANNEL );
    end Queue_Message_Entity;

    ----------------------------------------------------------------------------

    procedure Queue_Spawn_Entity( template   : String;
                                  properties : Map_Value ) is
        evt : A_Spawn_Entity_Event := new Spawn_Entity_Event;
    begin
        evt.Construct( template, properties );
        Events.Manager.Global.Queue_Event( A_Event(evt) );
    end Queue_Spawn_Entity;

end Events.Entities;
