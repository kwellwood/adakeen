--
-- Copyright (c) 2012-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;

package Events.Audio is

    -- A command to begin playing a looped music track.
    EVT_PLAY_MUSIC : constant Event_Id := 200;

    -- A command to play a sound effect once through.
    EVT_PLAY_SOUND : constant Event_Id := 201;

    -- An event regarding playing some audio.
    type Play_Audio_Event is new Event with private;
    type A_Play_Audio_Event is access all Play_Audio_Event'Class;

    -- Returns the filename of the audio to play.
    function Get_Audio_Name( this : not null access Play_Audio_Event'Class ) return String;

    -- Returns the minimum time span allowed between repeated instances of this
    -- audio.
    function Get_Repeat_Delay( this : not null access Play_Audio_Event'Class ) return Time_Span;

    ----------------------------------------------------------------------------

    -- A command to stop any music currently playing.
    EVT_STOP_MUSIC : constant Event_Id := 202;

    ----------------------------------------------------------------------------

    -- Queues a Play_Music_Event.
    procedure Queue_Play_Music( name : String );

    -- Queues a Play_Sound_Event. If the same named sound effect already has
    -- started playing less than 'repeatDelay' ago, it will not be played.
    procedure Queue_Play_Sound( name : String; repeatDelay : Time_Span := Time_Span_Zero );

    -- Queues a Stop_Music_Event.
    procedure Queue_Stop_Music;

private

    type Play_Audio_Event is new Event with
        record
            audioName   : Unbounded_String;
            repeatDelay : Time_Span := Time_Span_Zero;
        end record;

    procedure Construct( this        : access Play_Audio_Event;
                         evtId       : Event_Id;
                         audioName   : String;
                         repeatDelay : Time_Span );

    function To_String( this : access Play_Audio_Event ) return String;

end Events.Audio;
