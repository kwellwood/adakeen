--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--
with Ada.Exceptions;                    use Ada.Exceptions;
with Ada.Unchecked_Deallocation;
with Debugging;                         use Debugging;
with Events.Manager;                    use Events.Manager;
with Values.Errors;                     use Values.Errors;

package body Events.Corrals is

    procedure Remove_Listener_From_Dispatchees( this     : not null access Corral'Class;
                                                listener : not null access Event_Listener'Class;
                                                evtId    : Event_Id );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Corral( manager : not null access Event_Manager'Class;
                            name    : String ) return A_Corral is
        this : constant A_Corral := new Corral;
    begin
        this.Construct( manager, name );
        return this;
    end Create_Corral;

    ----------------------------------------------------------------------------

    procedure Add_Listener( this     : not null access Corral'Class;
                            listener : not null access Event_Listener'Class;
                            evtId    : Event_Id ) is
        pos       : Event_Id_Maps.Cursor;
        emptyList : Listener_Vectors.Vector;
        inserted  : Boolean;
    begin
        -- find the listeners for event id
        pos := this.listeners.Find( evtId );
        if not Has_Element( pos ) then
            -- insert an empty listener list if the event id not found
            this.listeners.Insert( evtId, emptyList, pos, inserted );
        end if;
        -- add 'listener' to the listeners of the event id
        if not this.listeners.Constant_Reference( pos ).Contains( listener ) then
            this.listeners.Reference( pos ).Append( listener );
            if Integer(this.listeners.Reference( pos ).Length) = 1 then
                this.manager.Register_Corral( this, evtId );
            end if;
        else
            Dbg( "Corral <" & this.Get_Name &
                 "> already registered for event id" & Event_Id'Image( evtId ),
                 D_EVENTS, Warning );
        end if;
    end Add_Listener;

    ----------------------------------------------------------------------------

    procedure Close( this : not null access Corral'Class ) is
        syncEvt : A_Sync_Event;
        evt     : A_Event;
    begin
        this.lock.Lock;
        while not this.syncEvents.Is_Empty loop
            this.syncEvents.Pop( syncEvt );
            syncEvt.Set_Response( No_Response );
            -- synchronous events are deleted by their initiating thread
            -- after receiving a response
        end loop;
        while not this.asyncEvents.Is_Empty loop
            this.asyncEvents.Pop( evt );
            Delete( evt );
        end loop;
        this.closed := True;
        this.lock.Unlock;
     end Close;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this    : access Corral;
                         manager : not null access Event_Manager'Class;
                         name    : String ) is
    begin
        Limited_Object(this.all).Construct;
        this.manager := manager;
        this.name := To_Unbounded_String( name );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Corral ) is
    begin
        this.listeners.Clear;
        this.Close;
        Limited_Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Dispatch( this     : not null access Corral'Class;
                        evt      : in out A_Event;
                        response : out Response_Type ) is
        pos      : Event_Id_Maps.Cursor;
        listener : A_Event_Listener;
    begin
        response := No_Response;

        -- find the listeners for event 'evt'
        pos := this.listeners.Find( evt.Get_Event_Id );
        if Has_Element( pos ) then

            -- a copy of the listener list is iterated while dispatching 'evt'
            -- to allow the listener list to be modified during iteration.
            --
            -- the iterated copy is stored as an object member instead of on the
            -- stack because a listener's Handle_Event may actually unregister
            -- another listener that is going to receive the same event. by
            -- making the iterated listener list available to the
            -- Unregister_Listener methods, they can clear an unregistered
            -- listener from the iterated listener list, avoiding dispatching
            -- events to listeners that have already been unregistered.
            --
            -- this mechanism assumes that recursive dispatching does not occur.
            pragma Assert( this.dispatchEvt = 0 );     -- no recursive dispatching
            this.dispatchEvt := evt.Get_Event_Id;
            this.dispatchees := Copy( this.listeners.Constant_Reference( pos ) );
            for i in 1..Integer(this.dispatchees.Length) loop
                listener := this.dispatchees.Element( i );
                begin
                    if listener /= null then
                        listener.Handle_Event( evt, response );
                        exit when evt = null;
                    end if;
                exception
                    when e : others =>
                        Set_Response( response, ST_FAILED,
                                      Create( Generic_Exception,
                                              "Unhandled exception in event listener " &
                                              listener.To_String & ": " &
                                              Exception_Name( e ) & " - " &
                                              Exception_Message( e ) ) );
                        Dbg( "Exception in event listener " &
                             listener.To_String & ": id" & Event_Id'Image( evt.Get_Event_Id ),
                             D_EVENTS, Error );
                        Dbg( e, D_EVENTS, Error );
                        exit;
                end;
            end loop;
            this.dispatchEvt := 0;

        end if;
        Delete( evt );
    exception
        when others =>
            this.dispatchEvt := 0;
            raise;
    end Dispatch;

    ----------------------------------------------------------------------------

    procedure End_Frame( this : not null access Corral'Class; chan : Event_Channel ) is
    begin
        this.lock.Lock;
        this.channels(chan).frameTime := Clock;
        this.channels(chan).frameId := this.nextSeqId - 1;
        this.channels(chan).frameCount := this.channels(chan).frameCount + 1;
        this.lock.Unlock;
    end End_Frame;

    ----------------------------------------------------------------------------

    function Get_Frame_Count( this : not null access Corral'Class;
                              chan : Event_Channel ) return Integer is (this.channelsFrame(chan).frameCount);

    ----------------------------------------------------------------------------

    function Get_Frame_End( this : not null access Corral'Class;
                            chan : Event_Channel ) return Time is (this.channelsFrame(chan).frameTime);

    ----------------------------------------------------------------------------

    function Get_Name( this : not null access Corral'Class ) return String is (To_String( this.name ));

    ----------------------------------------------------------------------------

    overriding
    function Get_Process_Name( this : access Corral ) return String is ("Corral(" & this.Get_Name & ")");

    ----------------------------------------------------------------------------

    procedure Dispatch_Async( this : not null access Corral'Class;
                              evt  : in out A_Event ) is
    begin
        pragma Debug( Dbg( "Corral(" & To_String( this.name ) &
                           "): Queuing " & To_String( evt ),
                           D_EVENTS, Info ) );

        this.lock.Lock;
        if not this.closed then
            evt.evtSeq := this.nextSeqId;
            this.nextSeqId := this.nextSeqId + 1;
            if not this.asyncEvents.Is_Full then
                this.asyncEvents.Push( evt );
                evt := null;
            else
                Dbg( "Corral(" & To_String( this.name ) &
                     ") is full, dropping " & To_String( evt ),
                     D_EVENTS, Error );
            end if;
        end if;
        this.lock.Unlock;

        Delete( evt );    -- delete if it wasn't already consumed
    end Dispatch_Async;

    ----------------------------------------------------------------------------

    procedure Dispatch_Sync( this     : not null access Corral'Class;
                             evt      : in out A_Event;
                             response : out Response_Type ) is
        syncEvt : A_Sync_Event := new Sync_Event;
        dup     : A_Event := Copy( evt );
    begin
        pragma Debug( Dbg( "Corral '" & To_String( this.name ) &
                           "': Queueing synchronous " & To_String( evt ),
                           D_EVENTS, Info ) );
        syncEvt.Set_Event( dup );    -- consumes 'dup'

        this.lock.Lock;
        if not this.closed then
            this.syncEvents.Push( syncEvt );
        else
            syncEvt.Set_Response( No_Response );
        end if;
        this.lock.Unlock;

        syncEvt.Wait_Response( response );
        Delete( syncEvt );
        if response /= No_Response then
            Delete( evt );           -- consume the event to indicate a response
        end if;
    end Dispatch_Sync;

    ----------------------------------------------------------------------------

    procedure Remove_Listener( this     : not null access Corral'Class;
                               listener : not null access Event_Listener'Class;
                               evtId    : Event_Id := 0 ) is
        pos  : Event_Id_Maps.Cursor;
        pos2 : Listener_Vectors.Cursor;
    begin
        if evtId = 0 then
            -- iterate over the listener lists of all events
            pos := this.listeners.First;
            while Has_Element( pos ) loop
                -- find 'listener' in the listener list for this particular event
                pos2 := this.listeners.Constant_Reference( pos ).Find( A_Event_Listener(listener) );
                if Has_Element( pos2 ) then
                    -- found; remove it and continue to other event ids
                    this.Remove_Listener_From_Dispatchees( listener, Key( pos ) );
                    this.listeners.Reference( pos ).Delete( pos2 );
                    if this.listeners.Constant_Reference( pos ).Is_Empty then
                        this.manager.Unregister_Corral( this, Key( pos ) );
                    end if;
                end if;
                Next( pos );
            end loop;
        else
            pos := this.listeners.Find( evtId );
            if Has_Element( pos ) then
                pos2 := this.listeners.Constant_Reference( pos ).Find( A_Event_Listener(listener) );
            end if;

            if Has_Element( pos2 ) then
                -- found listener registered for 'evtId'; remove the registration
                this.Remove_Listener_From_Dispatchees( listener, evtId );
                this.listeners.Reference( pos ).Delete( pos2 );
                if this.listeners.Constant_Reference( pos ).Is_Empty then
                    this.manager.Unregister_Corral( this, evtId );
                end if;
            else
                Dbg( "Unable to remove listener " & listener.To_String &
                     " for event id" & Event_Id'Image( evtId ) & " in corral <" &
                     this.Get_Name & ">: Listener not found",
                     D_EVENTS, Warning );
            end if;
        end if;
    end Remove_Listener;

    ----------------------------------------------------------------------------

    procedure Remove_Listener_From_Dispatchees( this     : not null access Corral'Class;
                                                listener : not null access Event_Listener'Class;
                                                evtId    : Event_Id ) is
    begin
        if evtId = this.dispatchEvt then
            for i in 1..Integer(this.dispatchees.Length) loop
                if this.dispatchees.Element( i ) = listener then
                    this.dispatchees.Replace_Element( i, null );
                    exit;
                end if;
            end loop;
        end if;
    end Remove_Listener_From_Dispatchees;

    ----------------------------------------------------------------------------

    overriding
    procedure Tick( this : access Corral; time : Tick_Time ) is
        pragma Unreferenced( time );
        syncEvt   : A_Sync_Event;
        evt       : A_Event;
        response  : Response_Type;
        remaining : Integer;
        index     : Integer := 1;         -- index in this.asyncEvents
    begin
        -- mark the queue length now to avoid processing any events added
        -- during the tick. we will process no more than 'remaining' async events.
        this.lock.Lock;
        remaining := this.asyncEvents.Length;
        this.channelsFrame := this.channels;      -- latch the event channels for this frame
        this.lock.Unlock;

        -- now that the queue length has been capped, signal the semaphore
        -- because event dispatching is starting.
        this.startDispatch.Set;

        loop
            -- drain the queue's synchronous channel first and check it again
            -- after each asynchronously dispatched event.
            loop
                syncEvt := null;
                this.lock.Lock;
                if not this.syncEvents.Is_Empty then
                    this.syncEvents.Pop( syncEvt );
                end if;
                this.lock.Unlock;
                exit when syncEvt = null;

                syncEvt.Take_Event( evt );
                pragma Debug( Dbg( "Corral(" & To_String( this.name ) &
                                   "): Invoking " & To_String( evt ),
                                   D_EVENTS, Info ) );
                this.Dispatch( evt, response );        -- consumes 'evt'
                syncEvt.Set_Response( response );
                -- 'syncEvt' is deleted by its original thread
            end loop;

            -- drain the queue's asynchronous channel
            if remaining > 0 then
                this.lock.Lock;
                if not this.closed then
                    -- find the next dispatchable event
                    while remaining > 0 loop
                        pragma Assert( this.asyncEvents.Length >= index );
                        evt := this.asyncEvents.Peek( index );
                        remaining := remaining - 1;

                        -- examine the event
                        if evt /= null then
                            -- dispatch the event if it's ready
                            if evt.evtChan = NO_CHANNEL or evt.evtSeq <= this.channelsFrame(evt.evtChan).frameId then
                                if index = 1 then
                                    this.asyncEvents.Pop( evt );             -- pop it off the front
                                else
                                    this.asyncEvents.Poke( index, null );    -- create a hole in the queue
                                    index := index + 1;
                                end if;
                                exit;

                            -- skip the pending event (unfinished frame)
                            else
                                index := index + 1;
                            end if;

                        -- skip over a hole left by a previously processed event
                        else
                            if index = 1 then
                                this.asyncEvents.Pop( evt );                 -- pop it off the front
                                pragma Assert( evt = null );
                            else
                                index := index + 1;
                            end if;
                        end if;

                        evt := null;
                    end loop;
                end if;
                this.lock.Unlock;
            else
                exit;
            end if;

            if evt /= null then
                pragma Debug( Dbg( "Corral(" & this.Get_Name &
                                   "): Dispatching " & To_String( evt ),
                                   D_EVENTS, Info ) );
                this.Dispatch( evt, response );            -- consumes 'evt'
            end if;
        end loop;
    end Tick;

    ----------------------------------------------------------------------------

    overriding
    function To_String( this : access Corral ) return String
    is ("<" & this.Get_Class_Name & ":" & this.Get_Name & ">");

    ----------------------------------------------------------------------------

    procedure Wait_Dispatch_Start( this    : not null access Corral'Class;
                                   timeout : Time_Span ) is
    begin
        if timeout > Time_Span_Zero then
            select
                this.startDispatch.Wait_One;     -- only one thread is signalled per frame
            or
                delay To_Duration( timeout );
            end select;
        else
            this.startDispatch.Wait_One;
        end if;
    end Wait_Dispatch_Start;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Corral ) is
    begin
        if this /= null then
            this.manager.Unregister_Corral( this );
            Delete( A_Limited_Object(this) );
        end if;
    end Delete;

    --==========================================================================

    protected body Sync_Event is

        procedure Set_Event( evt : in out A_Event ) is
        begin
            pragma Assert( event = null );
            event := evt;
            evt := null;
        end Set_Event;

        ------------------------------------------------------------------------

        entry Wait_Response( r : out Response_Type ) when complete is
        begin
            r := response;
        end Wait_Response;

        ------------------------------------------------------------------------

        procedure Take_Event( evt : in out A_Event ) is
        begin
            evt := event;
            event := null;
        end Take_Event;

        ------------------------------------------------------------------------

        procedure Set_Response( r : Response_Type ) is
        begin
            pragma Assert( not complete );
            response := r;
            complete := True;
        end Set_Response;

    end Sync_Event;

    ----------------------------------------------------------------------------

    procedure Delete( evt : in out A_Sync_Event ) is
        procedure Free is new Ada.Unchecked_Deallocation( Sync_Event, A_Sync_Event );
        e : A_Event := null;
    begin
        evt.Take_Event( e );
        Delete( e );
        Free( evt );
    end Delete;

end Events.Corrals;
