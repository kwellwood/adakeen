--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Events.Manager;                    use Events.Manager;
with Support;                           use Support;
with Values.Construction;               use Values.Construction;
with Values.Errors;                     use Values.Errors;

package body Events.World is

    not overriding
    procedure Construct( this           : access Create_World_Event;
                         width, height  : Positive;
                         tileWidth      : Positive;
                         libName,
                         playerTemplate : String ) is
    begin
        Event(this.all).Construct( EVT_CREATE_WORLD );
        this.width := width;
        this.height := height;
        this.tileWidth := tileWidth;
        this.libName := To_Unbounded_String( libName );
        this.playerTemplate := To_Unbounded_String( playerTemplate );
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Height( this : not null access Create_World_Event'Class ) return Positive is (this.height);

    function Get_Library_Name( this : not null access Create_World_Event'Class ) return String is (To_String( this.libName ));

    function Get_Player_Template( this : not null access Create_World_Event'Class ) return String is (To_String( this.playerTemplate ));

    function Get_Tile_Width( this : not null access Create_World_Event'Class ) return Positive is (this.tileWidth);

    function Get_Width( this : not null access Create_World_Event'Class ) return Positive is (this.width);

    --==========================================================================

    not overriding
    procedure Construct( this : access Load_World_Event; filename : String ) is
    begin
        Event(this.all).Construct( EVT_LOAD_WORLD );
        this.filename := To_Unbounded_String( filename );
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Filename( this : not null access Load_World_Event'Class ) return String is (To_String( this.filename ));

    overriding
    function To_String( this : access Load_World_Event ) return String
    is ("<Load_World_Event - filename: " & To_String( this.filename ) & ">");

    --==========================================================================

    overriding
    procedure Adjust( this : access World_Loaded_Event ) is
    begin
        Event(this.all).Adjust;
        this.map := Copy( this.map );
    end Adjust;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this      : access World_Loaded_Event;
                         map       : not null A_Map;
                         tileWidth : Positive;
                         libName   : String ) is
    begin
        Event(this.all).Construct( EVT_WORLD_LOADED );
        this.map := Copy( map );
        this.tileWidth := tileWidth;
        this.libName := To_Unbounded_String( libName );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out World_Loaded_Event ) is
    begin
        Delete( this.map );
        Event(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Get_Library_Name( this : not null access World_Loaded_Event'Class ) return String is (To_String( this.libName ));

    function Get_Map( this : not null access World_Loaded_Event'Class ) return A_Map is (this.map);

    function Get_Tile_Width( this : not null access World_Loaded_Event'Class ) return Positive is (this.tileWidth);

    --==========================================================================

    overriding
    procedure Adjust( this : access World_Property_Event ) is
    begin
        Event(this.all).Adjust;
        this.val := Clone( this.val );
    end Adjust;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this    : access World_Property_Event;
                         evtId   : Event_Id;
                         name    : String;
                         val     : Value'Class ) is
    begin
        Event(this.all).Construct( evtId );
        this.name := To_Unbounded_String( name );
        this.val := Clone( val );
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Property_Name( this : not null access World_Property_Event'Class ) return String is (To_String( this.name ));

    function Get_Value( this : not null access World_Property_Event'Class ) return Value is (this.val);

    overriding
    function To_String( this : access World_Property_Event ) return String
    is (
        "<World_Property_Event - name: " & To_String( this.name ) &
        ", val: " & this.val.Image & ">"
    );

    --==========================================================================

    not overriding
    procedure Construct( this   : access Resize_World_Event;
                         x, y   : Integer;
                         width,
                         height : Positive ) is
    begin
        Event(this.all).Construct( EVT_RESIZE_WORLD );
        this.x := x;
        this.y := y;
        this.width := width;
        this.height := height;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Height( this : not null access Resize_World_Event'Class ) return Positive is (this.height);

    function Get_Width( this : not null access Resize_World_Event'Class ) return Positive is (this.width);

    function Get_X( this : not null access Resize_World_Event'Class ) return Integer is (this.x);

    function Get_Y( this : not null access Resize_World_Event'Class ) return Integer is (this.y);

    --==========================================================================

    overriding
    procedure Adjust( this : access Tile_Event ) is
    begin
        Event(this.all).Adjust;
        this.items := this.items.Copy;
    end Adjust;

    ----------------------------------------------------------------------------

    function Get_Count( this : not null access Tile_Event'Class ) return Natural is (Natural(this.items.Length));

    ----------------------------------------------------------------------------

    function Get_List( this : not null access Tile_Event'Class ) return Tile_Change_Lists.Vector is (this.items.Copy);

    ----------------------------------------------------------------------------

    overriding
    function To_String( this : access Tile_Event ) return String
    is ("<Tile_Event - " & Image( Integer(this.items.Length) ) & " changes>");

    --==========================================================================

    not overriding
    procedure Construct( this  : access Layer_Event;
                         evtId : Event_Id;
                         layer : Integer ) is
    begin
        Event(this.all).Construct( evtId );
        this.layer := layer;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Layer( this : not null access Layer_Event'Class ) return Integer is (this.layer);

    --==========================================================================

    overriding
    procedure Adjust( this : access Layer_Create_Event ) is
    begin
        Layer_Event(this.all).Adjust;
        this.map := Copy( this.map );
        this.properties := Clone( this.properties ).Map;
    end Adjust;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this       : access Layer_Create_Event;
                         evtId      : Event_Id;
                         layer      : Integer;
                         layerType  : Layer_Type;
                         layerData  : A_Map;
                         properties : Map_Value ) is
    begin
        Layer_Event(this.all).Construct( evtId, layer );
        this.layerType := layerType;
        this.map := Copy( layerData );
        this.properties := Clone( properties ).Map;
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Layer_Create_Event ) is
    begin
        Delete( this.map );
        Layer_Event(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Get_Layer_Data( this : not null access Layer_Create_Event'Class ) return A_Map is (this.map);

    ----------------------------------------------------------------------------

    function Get_Properties( this : not null access Layer_Create_Event'Class ) return Map_Value is (this.properties);

    ----------------------------------------------------------------------------

    function Get_Type( this : not null access Layer_Create_Event'Class ) return Layer_Type is (this.layerType);

    ----------------------------------------------------------------------------

    overriding
    function To_String( this : access Layer_Create_Event ) return String
    is ("<Layer_Create_Event - layer: '" & Image( this.layer ) & ", type: " & this.layerType'Img & ">");

    --==========================================================================

    overriding
    procedure Adjust( this : access Layer_Created_Event ) is
    begin
        Layer_Create_Event(this.all).Adjust;
        this.propDefs := Clone( this.propDefs ).Map;
    end Adjust;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this       : access Layer_Created_Event;
                         evtId      : Event_Id;
                         layer      : Integer;
                         layerType  : Layer_Type;
                         layerData  : A_Map;
                         properties : Map_Value;
                         propDefs   : Map_Value ) is
    begin
        Layer_Create_Event(this.all).Construct( evtId, layer, layerType, layerData, properties );
        this.propDefs := Clone( propDefs ).Map;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Property_Defs( this : not null access Layer_Created_Event'Class ) return Map_Value is (this.propDefs);

    ----------------------------------------------------------------------------

    overriding
    function To_String( this : access Layer_Created_Event ) return String
    is ("<Layer_Created_Event - layer: '" & Image( this.layer ) & ", type: " & this.layerType'Img & ">");

    --==========================================================================

    overriding
    procedure Adjust( this : access Layer_Property_Event ) is
    begin
        Layer_Event(this.all).Adjust;
        this.val := Clone( this.val );
    end Adjust;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this  : access Layer_Property_Event;
                         evtId : Event_Id;
                         layer : Integer;
                         name  : String;
                         val   : Value'Class ) is
    begin
        Layer_Event(this.all).Construct( evtId, layer );
        this.name := To_Unbounded_String( name );
        this.val := Clone( val );
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Property_Name( this : not null access Layer_Property_Event'Class ) return String is (To_String( this.name ));

    ----------------------------------------------------------------------------

    function Get_Value( this : not null access Layer_Property_Event'Class ) return Value is (this.val);

    ----------------------------------------------------------------------------

    overriding
    function To_String( this : access Layer_Property_Event ) return String
    is ("<Layer_Property_Event - layer: '" & Image( this.layer ) &
          ", name: " & To_String( this.name ) & ", value: " & Image( this.val ) & ">");

    --==========================================================================

    not overriding
    procedure Construct( this       : access Layer_Swap_Event;
                         evtId      : Event_Id;
                         layer      : Integer;
                         otherLayer : Integer ) is
    begin
        Layer_Event(this.all).Construct( evtId, layer );
        this.otherLayer := otherLayer;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Other_Layer( this : not null access Layer_Swap_Event'Class ) return Integer is (this.otherLayer);

    ----------------------------------------------------------------------------

    overriding
    function To_String( this : access Layer_Swap_Event ) return String
    is ("<Layer_Swap_Event - layer: '" & Image( this.layer ) & ", other: " & Image( this.otherLayer ) & ">");

    --==========================================================================

    procedure Queue_Create_World( width, height  : Positive;
                                  tileWidth      : Positive;
                                  libName,
                                  playerTemplate : String ) is
        evt : A_Create_World_Event := new Create_World_Event;
    begin
        evt.Construct( width, height, tileWidth, libName, playerTemplate );
        Events.Manager.Global.Queue_Event( A_Event(evt), VIEW_CHANNEL );
    end Queue_Create_World;

    ----------------------------------------------------------------------------

    procedure Queue_Load_World( filename : String ) is
        evt : A_Load_World_Event := new Load_World_Event;
    begin
        evt.Construct( filename );
        Events.Manager.Global.Queue_Event( A_Event(evt), VIEW_CHANNEL );
    end Queue_Load_World;

    ----------------------------------------------------------------------------

    procedure Queue_World_Loaded( map       : not null A_Map;
                                  tileWidth : Positive;
                                  libName   : String ) is
        evt : A_World_Loaded_Event := new World_Loaded_Event;
    begin
        evt.Construct( map, tileWidth, libName );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_World_Loaded;

    ----------------------------------------------------------------------------

    procedure Queue_Resize_World( width, height : Positive ) is
        evt : A_Resize_World_Event := new Resize_World_Event;
    begin
        evt.Construct( 0, 0, width, height );
        Events.Manager.Global.Queue_Event( A_Event(evt), VIEW_CHANNEL );
    end Queue_Resize_World;

    ----------------------------------------------------------------------------

    procedure Queue_Resize_World( x, y : Integer; width, height : Positive ) is
        evt : A_Resize_World_Event := new Resize_World_Event;
    begin
        evt.Construct( x, y, width, height );
        Events.Manager.Global.Queue_Event( A_Event(evt), VIEW_CHANNEL );
    end Queue_Resize_World;

    ----------------------------------------------------------------------------

    procedure Queue_Set_World_Property( name : String; val : Value'Class ) is
        evt : A_World_Property_Event := new World_Property_Event;
    begin
        evt.Construct( EVT_SET_WORLD_PROPERTY, name, val );
        Events.Manager.Global.Queue_Event( A_Event(evt), VIEW_CHANNEL );
    end Queue_Set_World_Property;

    ----------------------------------------------------------------------------

    procedure Queue_World_Property_Changed( name : String; val : Value'Class ) is
        evt : A_World_Property_Event := new World_Property_Event;
    begin
        evt.Construct( EVT_WORLD_PROPERTY_CHANGED, name, val );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_World_Property_Changed;

    ----------------------------------------------------------------------------

    procedure Queue_Set_Tile( layer    : Integer;
                              col, row : Integer;
                              tileId   : Natural ) is
        evt : A_Tile_Event := new Tile_Event;
    begin
        evt.Construct( EVT_SET_TILE );
        evt.items.Append( Tile_Change'(layer, col, row, tileId) );
        Events.Manager.Global.Queue_Event( A_Event(evt), VIEW_CHANNEL );
    end Queue_Set_Tile;

    ----------------------------------------------------------------------------

    procedure Queue_Set_Tiles( tiles : Tile_Change_Lists.Vector ) is
        evt : A_Tile_Event := new Tile_Event;
    begin
        evt.Construct( EVT_SET_TILE );
        evt.items.Append( tiles );
        Events.Manager.Global.Queue_Event( A_Event(evt), VIEW_CHANNEL );
    end Queue_Set_Tiles;

    ----------------------------------------------------------------------------

    procedure Queue_Tile_Changed( layer    : Integer;
                                  col, row : Integer;
                                  tileId   : Natural ) is
        evt : A_Tile_Event := new Tile_Event;
    begin
        evt.Construct( EVT_TILE_CHANGED );
        evt.items.Append( Tile_Change'(layer, col, row, tileId) );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_Tile_Changed;

    ----------------------------------------------------------------------------

    procedure Queue_Tiles_Changed( tiles : Tile_Change_Lists.Vector ) is
        evt : A_Tile_Event := new Tile_Event;
    begin
        evt.Construct( EVT_TILE_CHANGED );
        evt.items.Append( tiles );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_Tiles_Changed;

    ----------------------------------------------------------------------------

    procedure Queue_Delete_Layer( layer : Integer ) is
        evt : A_Layer_Event := new Layer_Event;
    begin
        evt.Construct( EVT_DELETE_LAYER, layer );
        Events.Manager.Global.Queue_Event( A_Event(evt), VIEW_CHANNEL );
    end Queue_Delete_Layer;

    ----------------------------------------------------------------------------

    procedure Queue_Layer_Deleted( layer : Integer ) is
        evt : A_Layer_Event := new Layer_Event;
    begin
        evt.Construct( EVT_LAYER_DELETED, layer );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_Layer_Deleted;

    ----------------------------------------------------------------------------

    procedure Queue_Create_Layer( beforeLayer : Integer;
                                  layerType   : Layer_Type;
                                  layerData   : A_Map;
                                  properties  : Map_Value ) is
        evt : A_Layer_Create_Event := new Layer_Create_Event;
    begin
        evt.Construct( EVT_CREATE_LAYER, beforeLayer, layerType, layerData, properties );
        Events.Manager.Global.Queue_Event( A_Event(evt), VIEW_CHANNEL );
    end Queue_Create_Layer;

    ----------------------------------------------------------------------------

    procedure Queue_Layer_Created( layer      : Integer;
                                   layerType  : Layer_Type;
                                   layerData  : A_Map;
                                   properties : Map_Value;
                                   propDefs   : Map_Value ) is
        evt : A_Layer_Created_Event := new Layer_Created_Event;
    begin
        evt.Construct( EVT_LAYER_CREATED, layer, layerType, layerData, properties, propDefs );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_Layer_Created;

    ----------------------------------------------------------------------------

    procedure Queue_Set_Layer_Property( layer : Integer;
                                        name  : String;
                                        val   : Value'Class ) is
        evt : A_Layer_Property_Event := new Layer_Property_Event;
    begin
        evt.Construct( EVT_SET_LAYER_PROPERTY, layer, name, val );
        Events.Manager.Global.Queue_Event( A_Event(evt), VIEW_CHANNEL );
    end Queue_Set_Layer_Property;

    ----------------------------------------------------------------------------

    procedure Queue_Layer_Property_Changed( layer : Integer;
                                            name  : String;
                                            val   : Value'Class ) is
        evt : A_Layer_Property_Event := new Layer_Property_Event;
    begin
        evt.Construct( EVT_LAYER_PROPERTY_CHANGED, layer, name, val );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_Layer_Property_Changed;

    ----------------------------------------------------------------------------

    procedure Queue_Swap_Layers( layer, otherLayer : Integer ) is
        evt : A_Layer_Swap_Event := new Layer_Swap_Event;
    begin
        evt.Construct( EVT_SWAP_LAYERS, layer, otherLayer );
        Events.Manager.Global.Queue_Event( A_Event(evt), VIEW_CHANNEL );
    end Queue_Swap_Layers;

    ----------------------------------------------------------------------------

    procedure Queue_Layers_Swapped( layer, otherLayer : Integer ) is
        evt : A_Layer_Swap_Event := new Layer_Swap_Event;
    begin
        evt.Construct( EVT_LAYERS_SWAPPED, layer, otherLayer );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_Layers_Swapped;

    ----------------------------------------------------------------------------

    procedure Queue_World_Modified is
        evt : A_Event := new Event;
    begin
        evt.Construct( EVT_WORLD_MODIFIED );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_World_Modified;

    ----------------------------------------------------------------------------

    procedure Queue_World_Unmodified is
        evt : A_Event := new Event;
    begin
        evt.Construct( EVT_WORLD_UNMODIFIED );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_World_Unmodified;

end Events.World;
