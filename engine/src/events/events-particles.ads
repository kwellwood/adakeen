--
-- Copyright (c) 2015-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Object_Ids;                        use Object_Ids;
with Values.Maps;                       use Values.Maps;
with Values.Tagged_Ids;                 use Values.Tagged_Ids;

package Events.Particles is

    -- A command to emit an instant or limited length particle burst.
    EVT_PARTICLE_BURST : constant Event_Id := 700;

    -- A command to create a particle emitter, emitting particles until stopped.
    EVT_START_PARTICLES : constant Event_Id := 701;

    -- A command to stop a particle emitter.
    EVT_STOP_PARTICLES : constant Event_Id := 702;

    type Particles_Event is new Event with private;
    type A_Particles_Event is access all Particles_Event'Class;

    -- Returns the component id associated with the event (if any).
    function Get_Component_Id( this : not null access Particles_Event'Class ) return Component_Id;

    -- Returns the emitter properties.
    function Get_Emitter( this : not null access Particles_Event'Class ) return Map_Value;

    -- Returns the id of the entity to follow, or NULL_ID if none.
    function Get_Entity_Id( this : not null access Particles_Event'Class ) return Entity_Id;

    -- Returns the lifetime of the emitter, or zero if it emits instantly.
    function Get_Lifetime( this : not null access Particles_Event'Class ) return Time_Span;

    -- Returns the emission rate in particles/second. If lifetime is zero, the
    -- rate is the particle count to emit immediately.
    function Get_Rate( this : not null access Particles_Event'Class ) return Integer;

    -- Returns the initial X location of the emitter.
    function Get_X( this : not null access Particles_Event'Class ) return Float;

    -- Returns the initial Y location of the emitter.
    function Get_Y( this : not null access Particles_Event'Class ) return Float;

    -- Returns the Z depth of the particles.
    function Get_Z( this : not null access Particles_Event'Class ) return Float;

    ----------------------------------------------------------------------------

    -- Queues a particles burst event. All 'count' particles will be emitted
    -- immediately. 'emitter' can be either the name (string) of an emitter
    -- defined in particles.sv, or it can be an emitter definition (map).
    procedure Queue_Particle_Burst( emitter : Value'Class;
                                    x, y, z : Float;
                                    count   : Integer );

    -- Queues a Particle_Burst event. Particles will be emitted at 'rate'
    -- particles per second until 'lifetime' expires. If 'entityId' is set, the
    -- emitter will follow the given entity until it is deleted or the lifetime
    -- expires. 'emitter' can be either the name (string) of an emitter defined
    -- in particles.sv, or it can be an emitter definition (map).
    procedure Queue_Particle_Burst( emitter  : Value'Class;
                                    x, y, z  : Float;
                                    rate     : Integer;
                                    lifetime : Time_Span;
                                    entityId : Entity_Id := NULL_ID );

    -- Queues a Start_Particles event, creating an emitter that will persist
    -- until stopped or 'componentId' is destroyed. Only one active emitter can
    -- be associated with a component id at a time. If another emitter is
    -- associated with 'componentId', it will be stopped. 'emitter' can be
    -- either the name (string) of an emitter defined in particles.sv, or it can
    -- be an emitter definition (map).
    procedure Queue_Start_Particles( componentId : Component_Id;
                                     emitter     : Value'Class;
                                     x, y, z     : Float;
                                     rate        : Integer;
                                     entityId    : Entity_Id := NULL_ID );

    -- Queues a Stop_Particles event, stopping the particle emitter associated
    -- 'componentId'.
    procedure Queue_Stop_Particles( componentId : Component_Id );

private

    type Particles_Event is new Event with
        record
            componentId : Component_Id := NULL_ID;
            emitter     : Map_Value;
            x, y        : Float := 0.0;
            z           : Float := 0.0;
            rate        : Integer;
            lifetime    : Time_Span := Milliseconds( 0 );
            entityId    : Entity_Id := NULL_ID;
        end record;

    procedure Adjust( this : access Particles_Event );

    procedure Construct( this        : access Particles_Event;
                         evtId       : Event_Id;
                         componentId : Component_Id;
                         emitter     : Map_Value;
                         x, y, z     : Float;
                         rate        : Integer;
                         lifetime    : Time_Span;
                         entityId    : Entity_Id );

    function To_String( this : access Particles_Event ) return String;

end Events.Particles;
