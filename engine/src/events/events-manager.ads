--
-- Copyright (c) 2012-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Ordered_Maps;
with Ada.Containers.Vectors;
with Events.Corrals;                    use Events.Corrals;
with Mutexes;                           use Mutexes;
with System;                            use System;

package Events.Manager is

    -- An event manager receives new events from the application and dispatches
    -- them to the appropriate event queues. It can dispatch events across
    -- threads both synchronously and asynchronously.
    type Event_Manager is new Limited_Object with private;
    type A_Event_Manager is access all Event_Manager;

    -- Creates and returns a new Event_Manager.
    function Create_Event_Manager return A_Event_Manager;

    -- Marks the end of a frame on channel 'chan'. Pending events in the channel
    -- will be released to listeners.
    procedure End_Frame( this : not null access Event_Manager'Class; chan : Event_Channel );

    -- Adds an asynchronous event to the queue. It will be dispatched to all
    -- registered event listeners as soon as possible. Events are generally
    -- dispatched in order.
    --
    -- Events channels, however, can group events together. This allows events
    -- generated within one frame of an event source to be processed completely
    -- within one frame of an event receiver, preventing an inconsistent state
    -- across event receiver frames.
    --
    -- Events with a channel will remain pending until the event channel's frame
    -- is marked complete with End_Frame( chan ).
    procedure Queue_Event( this : not null access Event_Manager'Class;
                           evt  : in out A_Event;
                           chan : Event_Channel := NO_CHANNEL );
    pragma Precondition( evt /= null );
    pragma Postcondition( evt = null );

    -- Synchronously dispatches an event to registered listeners, returning the
    -- response of the first listener to consume the event. Synchronous events
    -- are processed out-of-band with asynchronous threads. No_Response will be
    -- returned if the event has no listeners.
    --
    -- This only works across separate threads. To trigger events handled by the
    -- calling thread, call the event handling code directly or queue an event
    -- for the next frame.
    procedure Trigger_Event( this     : not null access Event_Manager'Class;
                             evt      : in out A_Event;
                             response : out Response_Type );
    pragma Precondition( evt /= null );
    pragma Postcondition( evt = null );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- API For Corrals Only

    -- Registers a corral to receive events with id 'evtId'. This should
    -- only be called by a Corral instance.
    procedure Register_Corral( this   : not null access Event_Manager'Class;
                               corral : not null A_Corral;
                               evtId  : Event_Id );

    -- Unregisters a corral from receiving events with id 'evtId'. If the
    -- corral was not registered for these events, nothing will change. This
    -- should only be called by a Corral instance.
    procedure Unregister_Corral( this   : not null access Event_Manager'Class;
                                 corral : not null A_Corral; evtId : Event_Id );

    -- Unregisters a corral from receiving all events.
    procedure Unregister_Corral( this   : not null access Event_Manager'Class;
                                 corral : not null A_Corral );

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Event_Manager );

    Global : A_Event_Manager := null;

private

    function Lt( l, r : A_Corral ) return Boolean is (l.all'Address < r.all'Address);

    package Corral_Maps is new Ada.Containers.Ordered_Maps( A_Corral, Integer, Lt, "=" );
    use Corral_Maps;

    package Corral_Vectors is new Ada.Containers.Vectors( Positive, A_Corral, "=" );
    use Corral_Vectors;

    package Event_Id_Maps is new Ada.Containers.Ordered_Maps( Event_Id, Corral_Vectors.Vector, "<", "=" );
    use Event_Id_Maps;

    type Event_Manager is new Limited_Object with
        record
            lock       : Mutex;                  -- protects fields below
            idMap      : Event_Id_Maps.Map;      -- registered corrals by event id
            corralRefs : Corral_Maps.Map;        -- reference counts of registered corrals
        end record;

end Events.Manager;
