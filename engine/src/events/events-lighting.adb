--
-- Copyright (c) 2015-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Events.Manager;                    use Events.Manager;
with Values.Construction;               use Values.Construction;

package body Events.Lighting is

    overriding
    procedure Adjust( this : access Light_Event ) is
    begin
        Event(this.all).Adjust;
        this.properties := Clone( this.properties ).Map;
    end Adjust;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this        : access Light_Event;
                         evtId       : Event_Id;
                         componentId : Component_Id;
                         entityId    : Entity_Id;
                         properties  : Map_Value'Class ) is
    begin
        Event(this.all).Construct( evtId );
        this.componentId := componentId;
        this.entityId := entityId;
        this.properties := Clone( properties ).Map;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Component_Id( this : not null access Light_Event'Class ) return Component_Id is (this.componentId);

    function Get_Entity_Id( this : not null access Light_Event'Class ) return Entity_Id is (this.entityId);

    function Get_Properties( this : not null access Light_Event'Class ) return Map_Value is (this.properties);

    ----------------------------------------------------------------------------

    overriding
    function To_String( this : access Light_Event ) return String
    is ("<Light_Event - componentId: " & Image( this.componentId ) & ">");

    --==========================================================================

    procedure Queue_Light_Created( componentId : Component_Id;
                                   entityId    : Entity_Id;
                                   properties  : Map_Value'Class ) is
        evt : A_Light_Event := new Light_Event;
    begin
        evt.Construct( EVT_LIGHT_CREATED, componentId, entityId, properties );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_Light_Created;

    ----------------------------------------------------------------------------

    procedure Queue_Light_Deleted( componentId : Component_Id ) is
        evt : A_Light_Event := new Light_Event;
    begin
        evt.Construct( EVT_LIGHT_DELETED, componentId, NULL_ID, As_Map( Null_Value ) );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_Light_Deleted;

    ----------------------------------------------------------------------------

    procedure Queue_Light_Updated( componentId : Component_Id;
                                   properties  : Map_Value'Class ) is
        evt : A_Light_Event := new Light_Event;
    begin
        evt.Construct( EVT_LIGHT_UPDATED, componentId, NULL_ID, properties );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_Light_Updated;

end Events.Lighting;

