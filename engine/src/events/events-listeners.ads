--
-- Copyright (c) 2012-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Events.Listeners is

    -- Any class can implement the Event_Listener interface so it can register
    -- with an event corral to receive events. Handle_Event will be called on
    -- the listener when an event that the listener was registered for, fires.
    type Event_Listener is limited interface;
    type A_Event_Listener is access all Event_Listener'Class;
    pragma No_Strict_Aliasing( A_Event_Listener );

    -- Handles an event the Event_Listener registered for. If 'evt' is returned
    -- null then the event was consumed and will not be passed on to other
    -- listeners. 'resp' is the handler's response to synchronous events that
    -- will be returned to a caller that triggered a synchronous event.
    procedure Handle_Event( this : access Event_Listener;
                            evt  : in out A_Event;
                            resp : out Response_Type ) is abstract;

    -- Returns a string that identifies the Event_Listener object.
    function To_String( this : access Event_Listener ) return String is abstract;

end Events.Listeners;
