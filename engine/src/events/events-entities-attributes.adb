--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Events.Manager;
with Values.Construction;               use Values.Construction;

package body Events.Entities.Attributes is

    overriding
    procedure Adjust( this : access Entity_Attribute_Event ) is
    begin
        Entity_Event(this.all).Adjust;
        this.val := Clone( this.val );
    end Adjust;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this      : access Entity_Attribute_Event;
                         evtId     : Event_Id;
                         id        : Entity_Id;
                         attribute : String;
                         val       : Value'Class ) is
    begin
        Entity_Event(this.all).Construct( evtId, id );
        this.attribute := To_Unbounded_String( attribute );
        this.val := Clone( val );
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Attribute( this : not null access Entity_Attribute_Event'Class ) return String is (To_String( this.attribute ));

    function Get_Value( this : not null access Entity_Attribute_Event'Class ) return Value is (this.val);

    --==========================================================================

    procedure Queue_Entity_Attribute_Changed( id        : Entity_Id;
                                              attribute : String;
                                              val       : Value'Class ) is
        evt : A_Entity_Attribute_Event := new Entity_Attribute_Event;
    begin
        evt.Construct( EVT_ENTITY_ATTRIBUTE_CHANGED, id, attribute, val );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_Entity_Attribute_Changed;

    ----------------------------------------------------------------------------

    procedure Queue_Set_Entity_Attribute( id        : Entity_Id;
                                          attribute : String;
                                          val       : Value'Class ) is
        evt : A_Entity_Attribute_Event := new Entity_Attribute_Event;
    begin
        evt.Construct( EVT_SET_ENTITY_ATTRIBUTE, id, attribute, val );
        Events.Manager.Global.Queue_Event( A_Event(evt), VIEW_CHANNEL );
    end Queue_Set_Entity_Attribute;

end Events.Entities.Attributes;
