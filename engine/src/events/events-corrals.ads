--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Ordered_Maps;
with Ada.Containers.Vectors;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Events.Listeners;                  use Events.Listeners;
with Mutexes;                           use Mutexes;
with Processes;                         use Processes;
with Ring_Buffers;
with Semaphores;                        use Semaphores;

limited with Events.Manager;

package Events.Corrals is

    -- An event Corral dispatches events queued by its Event Manager to
    -- corresponding registered event listeners within the Corral's thread. A
    -- Corral runs as a Process within a Process_Manager thread, dispatching a
    -- series of events on each Tick to listeners running within the same
    -- thread.
    --
    -- Once the corral is running in the context of its Process_Manager, only
    -- that thread may call procedures of the Corral. The exception is the
    -- Queue_Sync and Queue_Async procedures, which are thread-safe. These can
    -- be called at any time by its event manager.
    type Corral is new Limited_Object and Process with private;
    type A_Corral is access all Corral'Class;

    -- Creates a new corral named 'name'. Names are for debugging purposes; they
    -- do not need to be unique.
    function Create_Corral( manager : not null access Events.Manager.Event_Manager'Class;
                            name    : String ) return A_Corral;
    pragma Postcondition( Create_Corral'Result /= null );

    -- Registers an object as a listener for event id 'evtId'. Listeners
    -- will receive all matching events sent to this corral, unless the event is
    -- consumed by an earlier registered handler that wants to prevent an event
    -- from propagating. Events are dispatched in listener registration order.
    procedure Add_Listener( this     : not null access Corral'Class;
                            listener : not null access Event_Listener'Class;
                            evtId    : Event_Id );

    -- Returns the name of the corral.
    function Get_Name( this : not null access Corral'Class ) return String with Inline;

    -- Unregisters an object as a listener for event id 'evtId'. If 'evtId' is
    -- zero, 'listener' will be removed from all previous registrations.
    procedure Remove_Listener( this     : not null access Corral'Class;
                               listener : not null access Event_Listener'Class;
                               evtId    : Event_Id := 0 );

    -- Waits for the corral to begin dispatching events. If 'timeout' is set to
    -- zero, the timeout period will be infinite.
    procedure Wait_Dispatch_Start( this    : not null access Corral'Class;
                                   timeout : Time_Span );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Interface for Event Manager (called from separate thread)

    -- Queues an event from the event manager to be dispatched asynchronously to
    -- registered listeners on the next tick. If the event is part of a channel,
    -- the event will remain pending until the channel's frame ends. 'evt' will
    -- be consumed.
    procedure Dispatch_Async( this : not null access Corral'Class;
                              evt  : in out A_Event );
    pragma Precondition( evt /= null );
    pragma Postcondition( evt = null );

    -- Queues a synchronous event, dispatched on the next tick, as soon as
    -- possible and out-of-band with the asynchronous event queue. The first
    -- listener to consume the event will return its response in 'response'.
    -- 'evt' might NOT be consumed, which indicates that no listener responded
    -- to the event.
    procedure Dispatch_Sync( this     : not null access Corral'Class;
                             evt      : in out A_Event;
                             response : out Response_Type );
    pragma Precondition( evt /= null );

    -- Marks the end of the latest frame of events in channel 'chan'. All
    -- pending events in channel 'chan' will be released for processing on the
    -- next Tick.
    procedure End_Frame( this : not null access Corral'Class;
                         chan : Event_Channel );

    -- Returns the time of the end of the latest frame of events in channel
    -- 'chan' at the start of this frame (latched by Tick).
    --
    -- This is expected to only be called by the corral's own thread because the
    -- lock is not taken.
    function Get_Frame_End( this : not null access Corral'Class;
                            chan : Event_Channel ) return Time;

    -- Returns the total number of frames completed in event channel 'chan' at
    -- the start of this frame (latched by Tick).
    --
    -- This is expected to only be called by the corral's own thread because the
    -- lock is not taken.
    function Get_Frame_Count( this : not null access Corral'Class;
                              chan : Event_Channel ) return Integer;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Deletes the Corral.
    procedure Delete( this : in out A_Corral );
    pragma Postcondition( this = null );

private

    -- A Sync_Event is a queueable wrapper for an event that is being dispatched
    -- synchronously. The event is inserted by the calling thread, retrieved by
    -- the Corral's execution thread, and then the event listener's response is
    -- returned to the blocked caller thread via this protected type.
    protected type Sync_Event is

        -- Sets the event 'evt' to dispatch, consuming it. This can only be
        -- called once, by the dispatching thread (the event manager).
        procedure Set_Event( evt : in out A_Event );
        pragma Postcondition( evt = null );

        -- Returns the handling event listener's response, blocking until a
        -- response has been set. This is called by the dispatching thread (the
        -- event manager).
        entry Wait_Response( r : out Response_Type );

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        -- Retrieves the queued event. This can only be called once, by the
        -- Corral's execution thread. 'evt' will belong to the caller.
        procedure Take_Event( evt : in out A_Event );
        pragma Precondition( evt = null );

        -- Sets the event listener's response, allowing the dispatching thread
        -- waiting on Wait_Response (the event manager) to proceed. This is
        -- called by the Corral's execution thread.
        procedure Set_Response( r : Response_Type );

    private
        complete : Boolean := False;
        event    : A_Event := null;
        response : Response_Type := No_Response;
    end Sync_Event;
    type A_Sync_Event is access all Sync_Event;

    procedure Delete( evt : in out A_Sync_Event );
    pragma Postcondition( evt = null );

    ----------------------------------------------------------------------------

    type Event_Frame is
        record
            frameId    : Sequence_Id := 0;   -- last sequence id of the last frame
            frameTime  : Time := Clock;      -- real time of the end of the last frame
            frameCount : Integer := 0;
        end record;

    type Channel_Array is array (Event_Channel) of Event_Frame;

    package Event_Buffers is new Ring_Buffers( A_Event );

    package Sync_Event_Buffers is new Ring_Buffers( A_Sync_Event );

    package Listener_Vectors is new Ada.Containers.Vectors( Positive, A_Event_Listener, "=" );
    use Listener_Vectors;

    package Event_Id_Maps is new Ada.Containers.Ordered_Maps( Event_Id, Listener_Vectors.Vector, "<", "=" );
    use Event_Id_Maps;

    ----------------------------------------------------------------------------

    type Corral is new Limited_Object and Process with
        record
            manager       : access Events.Manager.Event_Manager'Class := null;
            name          : Unbounded_String;
            listeners     : Event_Id_Maps.Map;
            startDispatch : Binary_Semaphore(True);
            dispatchEvt   : Event_Id := 0;                -- event id currently being dispatched
            dispatchees   : Listener_Vectors.Vector;      -- vector of listeners receiving an event

            lock          : Mutex;                        -- protects fields below
            closed        : Boolean := False;
            nextSeqId     : Sequence_Id := Sequence_Id'First + 1;
            asyncEvents   : Event_Buffers.Buffer(32767);
            syncEvents    : Sync_Event_Buffers.Buffer(15);
            channels      : Channel_Array;
            channelsFrame : Channel_Array;     -- copy of .channels for this frame,
                                               -- latched at the beginning of dispatch
        end record;

    -- Closes the event queue, preventing any further events from being queued.
    -- Synchronous events in the queue will receive a no-response and
    -- asynchronous events will be deleted.
    procedure Close( this : not null access Corral'Class );

    procedure Construct( this    : access Corral;
                         manager : not null access Events.Manager.Event_Manager'Class;
                         name    : String );

    procedure Delete( this : in out Corral );

    -- Dispatches 'evt' to all registered listeners.
    procedure Dispatch( this     : not null access Corral'Class;
                        evt      : in out A_Event;
                        response : out Response_Type );
    pragma Precondition( evt /= null );
    pragma Postcondition( evt = null );

    function Get_Process_Name( this : access Corral ) return String;

    -- Dispatches the events that were in the queue at the start of the tick.
    -- Events in a channel that were queued after the most recently completed
    -- frame will remain pending until the next tick.
    procedure Tick( this : access Corral; time : Tick_Time );

    function To_String( this : access Corral ) return String;

end Events.Corrals;
