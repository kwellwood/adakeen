--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Values.Lists;                      use Values.Lists;

package Events.Game is

    -- A command to begin a new game session.
    EVT_START_GAME : constant Event_Id := 400;

    -- The game session has restarted.
    EVT_GAME_STARTED : constant Event_Id := 401;

    ----------------------------------------------------------------------------

    -- A command to pause or resume gameplay.
    EVT_PAUSE_GAME : constant Event_Id := 402;

    -- A notification that gameplay has been paused or resumed.
    EVT_GAME_PAUSED : constant Event_Id := 403;

    -- An event related to the running state of gameplay.
    type Gameplay_Event is new Event with private;
    type A_Gameplay_Event is access all Gameplay_Event'Class;

    -- Returns True if the gameplay has been paused or False if it has been resumed.
    function Is_Paused( this : not null access Gameplay_Event'Class ) return Boolean;

    ----------------------------------------------------------------------------

    EVT_GAME_VAR_CHANGED : constant Event_Id := 404;

    -- A notification that a game session variable has changed.
    type Game_Var_Changed_Event is new Event with private;
    type A_Game_Var_Changed_Event is access all Game_Var_Changed_Event'Class;

    -- Returns a pointer to the variable's new value (not a copy).
    function Get_Value( this : not null access Game_Var_Changed_Event'Class ) return Value;

    -- Returns the name of the variable that changed.
    function Get_Var( this : not null access Game_Var_Changed_Event'Class ) return String;

    ----------------------------------------------------------------------------

    -- A notification that the loading of a world by the game logic began or
    -- ended. If loading has ended, an error message is included on failure.
    EVT_LOADING_BEGAN : constant Event_Id := 405;
    EVT_LOADING_ENDED : constant Event_Id := 406;

    -- A notification regarding the loading state of a resource.
    type Loading_Event is new Event with private;
    type A_Loading_Event is access all Loading_Event'Class;

    -- Returns an error message if the loading has ended due to a failure. An
    -- empty string will be returned otherwise.
    function Get_Error( this : not null access Loading_Event'Class ) return String;

    -- Returns True if loading was a success or False if it failed. If it failed,
    -- call Get_Error() to retrieve the corresponding error message.
    function Is_Success( this : not null access Loading_Event'Class ) return Boolean;

    ----------------------------------------------------------------------------

    -- A command to run a script. Sent from the view to the game to allow the
    -- user to run arbitrary scripts in the game thread. This is a feature for
    -- game developers.
    EVT_RUN_SCRIPT   : constant Event_Id := 407;
    EVT_SCRIPT_DONE  : constant Event_Id := 408;
    EVT_CONSOLE_TEXT : constant Event_Id := 409;

    type Script_Event is new Event with private;
    type A_Script_Event is access all Script_Event'Class;

    -- Returns a script's resultant value, or Null if none.
    function Get_Result( this : not null access Script_Event'Class ) return Value;

    -- Returns the source code to compile and execute, or an empty string if none.
    function Get_Source( this : not null access Script_Event'Class ) return Unbounded_String;

    -- Returns the task id that correlates the script's run request with its result.
    function Get_TaskId( this : not null access Script_Event'Class ) return Integer;

    ----------------------------------------------------------------------------

    EVT_VIEW_MESSAGE : constant Event_Id := 410;      -- message to the view
    EVT_GAME_MESSAGE : constant Event_Id := 411;      -- message to the session

    -- A general message to the Game_View (EVT_VIEW_MESSAGE) or to the Session
    -- (EVT_GAME_MESSAGE). A message is identified by its name and it may
    -- include a positional list of arguments. All messages sharing a name will
    -- share the same number and value type of arguments.
    type Message_Event is new Event with private;
    type A_Message_Event is access all Message_Event'Class;

    -- Returns the arguments of the message as a positional list. Null may be
    -- returned if the message does not have any arguments.
    function Get_Args( this : not null access Message_Event'Class ) return List_Value;

    -- Returns the name of the message.
    function Get_Name( this : not null access Message_Event'Class ) return String;

    ----------------------------------------------------------------------------

    -- Queues a Console_Text event containing a text to be displayed in the
    -- script console, if available.
    procedure Queue_Console_Text( text : String );

    -- Queues a general message to the Game_View.
    procedure Queue_Game_Message( name : String );
    procedure Queue_Game_Message( name : String; args : Value_Array );

    -- Queues a Gameplay_Event indicating gameplay has been paused or resumed.
    procedure Queue_Game_Paused( paused : Boolean );

    -- Queues a EVT_GAME_STARTED notification that the game session restarted.
    procedure Queue_Game_Started;

    -- Queues a Game_Var_Changed event.
    procedure Queue_Game_Var_Changed( var : String; val : Value'Class );

    -- Queues a notification EVT_LOADING_BEGAN indicating loading began.
    procedure Queue_Loading_Began;

    -- Queues an EVT_LOADING_ENDED event indicating the loading completed. If
    -- 'success' is False, an error message 'errorMsg' string should be given.
    procedure Queue_Loading_Ended( success : Boolean := True; errorMsg : String := "" );

    -- Queues a Pause_Game event.
    procedure Queue_Pause_Game( paused : Boolean );

    -- Queues a Run_Script event to launch a script in the game session. Pass a
    -- non-zero 'taskId' to run the script in the background, across multiple
    -- frames. Its result will be announced in a Script_Done event with the
    -- given 'taskId'.
    procedure Queue_Run_Script( source : Unbounded_String; taskId : Integer := 0 );

    -- Queues a Script_Done when a script has finished to announce the result.
    -- The task id is not expected to be 0 because of how scripts are executed
    -- by the Session object.
    procedure Queue_Script_Done( result : Value'Class; taskId : Integer );
    pragma Precondition( taskId /= 0 );

    -- Queues a Start_Game event to restart the game session.
    procedure Queue_Start_Game;

    -- Queues a general message to the Game_View.
    procedure Queue_View_Message( name : String );
    procedure Queue_View_Message( name : String; args : Value_Array );

private

    type Gameplay_Event is new Event with
        record
            paused : Boolean := True;
        end record;

    procedure Construct( this   : access Gameplay_Event;
                         evtId  : Event_Id;
                         paused : Boolean );

    function To_String( this : access Gameplay_Event ) return String;

    ----------------------------------------------------------------------------

    type Game_Var_Changed_Event is new Event with
        record
            var : Unbounded_String;
            val : Value;
        end record;

    procedure Adjust( this : access Game_Var_Changed_Event );

    procedure Construct( this : access Game_Var_Changed_Event;
                         var  : String;
                         val  : Value'Class );

    function To_String( this : access Game_Var_Changed_Event ) return String;

    ----------------------------------------------------------------------------

    type Loading_Event is new Event with
        record
            success : Boolean := False;
            error   : Unbounded_String;
        end record;

    procedure Construct( this    : access Loading_Event;
                         eId     : Event_Id;
                         success : Boolean;
                         error   : String );

    function To_String( this : access Loading_Event ) return String;

    ----------------------------------------------------------------------------

    type Script_Event is new Event with
        record
            taskId : Integer := 0;      -- task id for the running script
            source : Unbounded_String;  -- source (for run events)
            result : Value;             -- result (for completed events)
        end record;

    procedure Adjust( this : access Script_Event );

    procedure Construct( this   : access Script_Event;
                         evtId  : Event_Id;
                         taskId : Integer;
                         source : Unbounded_String;
                         result : Value'Class );

    function To_String( this : access Script_Event ) return String;

    ----------------------------------------------------------------------------

    type Message_Event is new Event with
        record
            name : Unbounded_String;
            args : List_Value;
        end record;

    procedure Adjust( this : access Message_Event );

    procedure Construct( this  : access Message_Event;
                         evtId : Event_Id;
                         name  : String;
                         args  : Value_Array );

end Events.Game;
