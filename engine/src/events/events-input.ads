--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Gamepads;                          use Gamepads;
with Keyboard;                          use Keyboard;
with Mouse;                             use Mouse;

package Events.Input is

    -- A notification that a key was pressed.
    EVT_KEY_PRESS : constant Event_Id := 500;

    -- A notification that a key was released.
    EVT_KEY_RELEASE : constant Event_Id := 501;

    -- A notification of a keyboard-based event.
    type Key_Event is new Event with private;
    type A_Key_Event is access all Key_Event'Class;

    -- Returns the Allegro key code of the key involved.
    function Get_Key( this : not null access Key_Event'Class ) return Natural with Inline;

    ----------------------------------------------------------------------------

    -- A notification that a key was typed or held down for the repeat delay.
    EVT_KEY_TYPED : constant Event_Id := 502;

    -- A character was typed, or repeated due to holding down the key.
    type Key_Typed_Event is new Key_Event with private;
    type A_Key_Typed_Event is access all Key_Typed_Event'Class;

    -- Returns the character that was typed, or nul if not a visible character.
    function Get_Char( this : not null access Key_Typed_Event'Class ) return Character with Inline;

    -- Returns a boolean array of modifier keys that were pressed/not pressed at
    -- the time of the event.
    procedure Get_Modifiers( this      : not null access Key_Typed_Event'Class;
                             modifiers : out Modifiers_Array );

    -- Returns True if no modifier keys were pressed at the time of the event.
    function No_Modifiers( this : not null access Key_Typed_Event'Class ) return Boolean with Inline;

    -- Returns True if the Alt key was the only pressed modifier at the time of
    -- the event.
    function Only_Alt( this : not null access Key_Typed_Event'Class ) return Boolean with Inline;

    -- Returns True if the Ctrl key was the only pressed modifier at the time of
    -- the event.
    function Only_Ctrl( this : not null access Key_Typed_Event'Class ) return Boolean with Inline;

    -- Returns True if the Shift key was the only pressed modifier at the time
    -- of the event.
    function Only_Shift( this : not null access Key_Typed_Event'Class ) return Boolean with Inline;

    ----------------------------------------------------------------------------

    EVT_GAMEPAD_PRESS   : constant Event_Id := 503;
    EVT_GAMEPAD_RELEASE : constant Event_Id := 504;
    EVT_GAMEPAD_MOVE    : constant Event_Id := 505;
    EVT_GAMEPAD_REMOVED : constant Event_Id := 506;

    -- A notification of a gamepad-based event.
    type Gamepad_Event is new Event with private;
    type A_Gamepad_Event is access all Gamepad_Event'Class;

    -- Returns the virtual gamepad button associated with the event.
    -- See Gamepads.BUTTON_* for valid buttons. Returns BUTTON_NONE if the event
    -- does not involve a button.
    function Get_Button( this : not null access Gamepad_Event'Class ) return Gamepad_Button;

    -- Returns the controller's unique identifier. If the same physical
    -- controller is disconnected and reconnected, it will receive a new id.
    function Get_Gamepad_Id( this : not null access Gamepad_Event'Class ) return Integer;

    -- Returns the gamepad's axis associated with the event. See Gamepads.AXIS_*
    -- for valid axes. Returns AXIS_NONE if the event does not involve an axis.
    function Get_Axis( this : not null access Gamepad_Event'Class ) return Gamepad_Axis;

    -- Returns the new position of the stick along the axis. Returns 0.0 if the
    -- event does not involve an axis.
    function Get_Position( this : not null access Gamepad_Event'Class ) return Float;

    ----------------------------------------------------------------------------

    -- A notification that the mouse moved.
    EVT_MOUSE_MOVE : constant Event_Id := 507;

    -- A notification of a mouse-based event.
    type Mouse_Event is new Event with private;
    type A_Mouse_Event is access all Mouse_Event'Class;

    -- Returns the X location of the mouse at the time of the event.
    function Get_X( this : not null access Mouse_Event'Class ) return Integer with Inline;

    -- Returns the Y location of the mouse within the window at the time of the
    -- event.
    function Get_Y( this : not null access Mouse_Event'Class ) return Integer with Inline;

    -- Sets the location of the mouse at the time of the event. This can be
    -- called by an event handler to filter the mouse even location for later
    -- mouse event handlers, to make the event location relative to some widget.
    procedure Set_XY( this : not null access Mouse_Event'Class; x, y : Integer ) with Inline;

    ----------------------------------------------------------------------------

    EVT_MOUSE_SCROLL : constant Event_Id := 508;

    -- A notification that the mouse wheel was scrolled.
    type Mouse_Scroll_Event is new Mouse_Event with private;
    type A_Mouse_Scroll_Event is access all Mouse_Scroll_Event'Class;

    -- Returns the amount that the mouse wheel was scrolled.
    function Get_Amount( this : not null access Mouse_Scroll_Event'Class ) return Integer with Inline;
    pragma Postcondition( Get_Amount'Result /= 0 );

    ----------------------------------------------------------------------------

    -- A notification that a mouse button was clicked (pressed and released rapidly).
    EVT_MOUSE_CLICK : constant Event_Id := 509;

    -- A notification that a mouse button was double-clicked (clicked twice, rapidly).
    EVT_MOUSE_DOUBLECLICK : constant Event_Id := 510;

    -- A notification that a mouse button was held down for the mouse button
    -- repeat rate delay.
    EVT_MOUSE_HELD : constant Event_Id := 511;

    -- A notification that a mouse button was pressed.
    EVT_MOUSE_PRESS : constant Event_Id := 512;

    -- A notification that a mouse button was released.
    EVT_MOUSE_RELEASE : constant Event_Id := 513;

    -- A notification of a mouse button-based event.
    type Mouse_Button_Event is new Mouse_Event with private;
    type A_Mouse_Button_Event is access all Mouse_Button_Event'Class;

    -- Returns the mouse button involved in the event.
    function Get_Button( this : not null access Mouse_Button_Event'Class ) return Mouse_Button with Inline;

    -- Returns a boolean array of keyboard modifiers that were pressed at the
    -- time of the event.
    function Get_Modifiers( this : not null access Mouse_Button_Event'Class ) return Modifiers_Array with Inline;

    ----------------------------------------------------------------------------

    -- A notification that the mouse left the window area. No Mouse_Move events
    -- are generated while the mouse is outside the window unless a button
    -- pressed inside the window is still being held. Mouse_Move events will
    -- resume when the mouse enters the window again.
    EVT_MOUSE_LEAVE : constant Event_Id := 514;

    ----------------------------------------------------------------------------

    procedure Queue_Gamepad_Move( gamepadId : Integer;
                                  axis      : Gamepad_Axis;
                                  position  : Float );

    procedure Queue_Gamepad_Press( gamepadId : Integer; button : Gamepad_Button );

    procedure Queue_Gamepad_Release( gamepadId : Integer; button : Gamepad_Button );

    procedure Queue_Gamepad_Removed( gamepadId : Integer );

    -- Queues a Key_Typed event.
    procedure Queue_Key_Typed( key       : Natural;
                               char      : Character;
                               modifiers : Modifiers_Array );

    -- Queues a Key_Press event.
    procedure Queue_Key_Press( key : Natural );

    -- Queues a Key_Release event.
    procedure Queue_Key_Release( key : Natural );

    -- Creates and returns a Key_Release event without queueing it.
    function Generate_Key_Release( key : Natural ) return A_Key_Event;

    -- Queues a Mouse_Click event.
    procedure Queue_Mouse_Click( x, y : Integer; btn : Mouse_Button );

    -- Queues a Mouse_Doubleclick event.
    procedure Queue_Mouse_Doubleclick( x, y : Integer; btn : Mouse_Button );

    -- Queues a Mouse_Held event.
    procedure Queue_Mouse_Held( x, y : Integer; btn : Mouse_Button );

    -- Queues a Mouse_Move event.
    procedure Queue_Mouse_Move( x, y : Integer );

    -- Queues a Mouse_Leave event.
    procedure Queue_Mouse_Leave;

    -- Queues a Mouse_Press event.
    procedure Queue_Mouse_Press( x, y      : Integer;
                                 btn       : Mouse_Button;
                                 modifiers : Modifiers_Array );

    -- Queues a Mouse_Release event.
    procedure Queue_Mouse_Release( x, y : Integer; btn : Mouse_Button );

    -- Creates a Mouse_Release_Event instance without queueing it. This is used
    -- only for a special case in the Window widget. The event is never queued;
    -- it's passed directly to a handler procedure to simulate an event.
    function Create_Mouse_Release( x, y : Integer;
                                   btn  : Mouse_Button ) return A_Mouse_Button_Event;

    -- Queues a Mouse_Scroll event.
    procedure Queue_Mouse_Scroll( x, y, amount : Integer );
    pragma Precondition( amount /= 0 );

private

    type Key_Event is new Event with
        record
            key : Natural := 0;
        end record;

    procedure Construct( this  : access Key_Event;
                         evtId : Event_Id;
                         key   : Natural );

    function To_String( this : access Key_Event ) return String;

    ----------------------------------------------------------------------------

    type Key_Typed_Event is new Key_Event with
        record
            char      : Character := ASCII.NUL;
            modifiers : Modifiers_Array := (others => False);
        end record;

    procedure Construct( this      : access Key_Typed_Event;
                         evtId     : Event_Id;
                         key       : Natural;
                         char      : Character;
                         modifiers : Modifiers_Array );

    function To_String( this : access Key_Typed_Event ) return String;

    ----------------------------------------------------------------------------

    type Gamepad_Event is new Event with
        record
            gamepadId : Integer := 0;
            button    : Gamepad_Button := BUTTON_NONE;
            axis      : Gamepad_Axis := AXIS_NONE;
            position  : Float := 0.0;
        end record;

    procedure Construct( this      : access Gamepad_Event;
                         evtId     : Event_Id;
                         gamepadId : Integer;
                         button    : Gamepad_Button := BUTTON_NONE;
                         axis      : Gamepad_Axis := AXIS_NONE;
                         position  : Float := 0.0 );

    ----------------------------------------------------------------------------

    type Mouse_Event is new Event with
        record
            x, y : Integer := 0;
        end record;

    procedure Construct( this  : access Mouse_Event;
                         evtId : Event_Id;
                         x, y  : Integer );

    function To_String( this : access Mouse_Event ) return String;

    ----------------------------------------------------------------------------

    type Mouse_Scroll_Event is new Mouse_Event with
        record
            amount : Integer := 0;
        end record;

    procedure Construct( this   : access Mouse_Scroll_Event;
                         evtId  : Event_Id;
                         x, y   : Integer;
                         amount : Integer );
    pragma Precondition( amount /= 0 );

    ----------------------------------------------------------------------------

    type Mouse_Button_Event is new Mouse_Event with
        record
            btn       : Mouse_Button := Mouse_Left;
            modifiers : Modifiers_Array := Modifiers_Array'(others=>False);
        end record;

    procedure Construct( this      : access Mouse_Button_Event;
                         evtId     : Event_Id;
                         x, y      : Integer;
                         btn       : Mouse_Button;
                         modifiers : Modifiers_Array );

    function To_String( this : access Mouse_Button_Event ) return String;

end Events.Input;
