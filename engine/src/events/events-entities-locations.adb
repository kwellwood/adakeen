--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Events.Manager;

package body Events.Entities.Locations is

    not overriding
    procedure Construct( this  : access Entity_Location_Event;
                         evtId : Event_Id;
                         id    : Entity_Id;
                         x, y  : Float ) is
    begin
        Entity_Event(this.all).Construct( evtId, id );
        this.x := x;
        this.y := y;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_X( this : not null access Entity_Location_Event'Class ) return Float is (this.x);

    function Get_Y( this : not null access Entity_Location_Event'Class ) return Float is (this.y);

    --==========================================================================

    not overriding
    procedure Construct( this   : access Entity_Moved_Event;
                         evtId  : Event_Id;
                         id     : Entity_Id;
                         x, y   : Float;
                         prevX,
                         prevY  : Float;
                         vx, vy : Float ) is
    begin
        Entity_Location_Event(this.all).Construct( evtId, id, x, y );
        this.prevX := prevX;
        this.prevY := prevY;
        this.vx := vx;
        this.vy := vy;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Prev_X( this : not null access Entity_Moved_Event'Class ) return Float is (this.prevX);

    function Get_Prev_Y( this : not null access Entity_Moved_Event'Class ) return Float is (this.prevY);

    function Get_VX( this : not null access Entity_Moved_Event'Class ) return Float is (this.vx);

    function Get_VY( this : not null access Entity_Moved_Event'Class ) return Float is (this.vy);

    --==========================================================================

    not overriding
    procedure Construct( this   : access Entity_Size_Event;
                         evtId  : Event_Id;
                         id     : Entity_Id;
                         width,
                         height : Natural ) is
    begin
        Entity_Event(this.all).Construct( evtId, id );
        this.width := width;
        this.height := height;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Height( this : not null access Entity_Size_Event'Class ) return Natural is (this.height);

    function Get_Width( this : not null access Entity_Size_Event'Class ) return Natural is (this.width);

    --==========================================================================

    not overriding
    procedure Construct( this       : access Entity_Resized_Event;
                         evtId      : Event_Id;
                         id         : Entity_Id;
                         width,
                         height     : Natural;
                         prevWidth,
                         prevHeight : Natural ) is
    begin
        Entity_Size_Event(this.all).Construct( evtId, id, width, height );
        this.prevWidth := prevWidth;
        this.prevHeight := prevHeight;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Prev_Height( this : not null access Entity_Resized_Event'Class ) return Natural is (this.prevHeight);

    function Get_Prev_Width( this : not null access Entity_Resized_Event'Class ) return Natural is (this.prevWidth);

    --==========================================================================

    procedure Queue_Entity_Moved( id           : Entity_Id;
                                  x, y         : Float;
                                  prevX, prevY : Float;
                                  vx, vy       : Float ) is
        evt : A_Entity_Moved_Event := new Entity_Moved_Event;
    begin
        evt.Construct( EVT_ENTITY_MOVED, id, x, y, prevX, prevY, vx, vy );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_Entity_Moved;

    ----------------------------------------------------------------------------

    procedure Queue_Entity_Resized( id         : Entity_Id;
                                    width,
                                    height     : Natural;
                                    prevWidth,
                                    prevHeight : Natural ) is
        evt : A_Entity_Resized_Event := new Entity_Resized_Event;
    begin
        evt.Construct( EVT_ENTITY_RESIZED, id, width, height, prevWidth, prevHeight );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_Entity_Resized;

    ----------------------------------------------------------------------------

    procedure Queue_Move_Entity( id : Entity_Id; x, y : Float ) is
        evt : A_Entity_Location_Event := new Entity_Location_Event;
    begin
        evt.Construct( EVT_MOVE_ENTITY, id, x, y );
        Events.Manager.Global.Queue_Event( A_Event(evt), VIEW_CHANNEL );
    end Queue_Move_Entity;

    ----------------------------------------------------------------------------

    procedure Queue_Resize_Entity( id : Entity_Id; width, height : Natural ) is
        evt : A_Entity_Size_Event := new Entity_Size_Event;
    begin
        evt.Construct( EVT_RESIZE_ENTITY, id, width, height );
        Events.Manager.Global.Queue_Event( A_Event(evt), VIEW_CHANNEL );
    end Queue_Resize_Entity;

end Events.Entities.Locations;
