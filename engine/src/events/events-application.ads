--
-- Copyright (c) 2012-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Displays;                  use Allegro.Displays;

package Events.Application is

    -- A notification that the application window has lost focus.
    EVT_APP_BLUR : constant Event_Id := 100;

    -- A notification that the application window has regained focus.
    EVT_APP_FOCUS : constant Event_Id := 101;

    -- A notification that the user is attempting to close the window of the
    -- application. This does not have to be acted on, for instance, if the
    -- player hasn't saved his game yet.
    EVT_CLOSE_WINDOW : constant Event_Id := 102;

    -- A command to close the application, no questions asked. This event is
    -- queued when it's time for the application to quit.
    EVT_EXIT_APPLICATION : constant Event_Id := 103;

    ----------------------------------------------------------------------------

    -- A notification that the user clicked an item in a pulldown or popup menu.
    EVT_MENU_CLICKED : constant Event_Id := 104;

    type Menu_Event is new Event with private;
    type A_Menu_Event is access all Menu_Event'Class;

    function Get_Display( this : not null access Menu_Event'Class ) return A_Allegro_Display;

    function Get_Menu_Id( this : not null access Menu_Event'Class ) return Integer;

    ----------------------------------------------------------------------------

    -- A notification that the user or OS has resized the window. The GUI
    -- needs to update accordingly for the new display dimensions.
    EVT_WINDOW_RESIZED : constant Event_Id := 105;

    -- An event regarding the window size or location.
    type Window_Event is new Event with private;
    type A_Window_Event is access all Window_Event'Class;

    function Get_Display( this : not null access Window_Event'Class ) return A_Allegro_Display;

    function Get_Width( this : not null access Window_Event'Class ) return Integer;

    function Get_Height( this : not null access Window_Event'Class ) return Integer;

    function Get_X( this : not null access Window_Event'Class ) return Integer;

    function Get_Y( this : not null access Window_Event'Class ) return Integer;

    ----------------------------------------------------------------------------

    -- Queues an App_Blur_Event.
    procedure Queue_App_Blur;

    -- Queues an App_Focus_Event.
    procedure Queue_App_Focus;

    -- Queues a Close_Window_Event.
    procedure Queue_Close_Window;

    -- Queues a Exit_Application_Event.
    procedure Queue_Exit_Application;

    -- Queues a Menu_Clicked event when the user clicks a pulldown or popup menu
    -- item.
    procedure Queue_Menu_Clicked( display : A_Allegro_Display; menuId : Integer );

     -- Queues a Window_Resized event after the user has resized the window.
    procedure Queue_Window_Resized( display       : A_Allegro_Display;
                                    x, y          : Integer;
                                    width, height : Integer );

private

    type Menu_Event is new Event with
        record
            display : A_Allegro_Display;
            menuId  : Integer;
        end record;

    procedure Construct( this    : access Menu_Event;
                         evtId   : Event_Id;
                         display : A_Allegro_Display;
                         menuId  : Integer );

    function To_String( this : access Menu_Event ) return String;

    ---------------------------------------------------------------------

    type Window_Event is new Event with
        record
            display       : A_Allegro_Display;
            x, y          : Integer;
            width, height : Integer;
        end record;

    procedure Construct( this          : access Window_Event;
                         evtId         : Event_Id;
                         display       : A_Allegro_Display;
                         x, y          : Integer;
                         width, height : Integer );

    function To_String( this : access Window_Event ) return String;

end Events.Application;
