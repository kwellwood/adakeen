--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Events.Entities.Locations is

    -- A command to move an entity.
    EVT_MOVE_ENTITY : constant Event_Id := 304;

    -- An event regarding the location of an entity.
    type Entity_Location_Event is new Entity_Event with private;
    type A_Entity_Location_Event is access all Entity_Location_Event'Class;

    -- Returns the entity's X coordinate in world units.
    function Get_X( this : not null access Entity_Location_Event'Class ) return Float;

    -- Returns the entity's Y coordinate in world units.
    function Get_Y( this : not null access Entity_Location_Event'Class ) return Float;

    ----------------------------------------------------------------------------

    EVT_ENTITY_MOVED : constant Event_Id := 305;

    -- A notification that an entity moved.
    type Entity_Moved_Event is new Entity_Location_Event with private;
    type A_Entity_Moved_Event is access all Entity_Moved_Event'Class;

    -- Returns the entity's previous X,Y coordinates in world units.
    function Get_Prev_X( this : not null access Entity_Moved_Event'Class ) return Float;
    function Get_Prev_Y( this : not null access Entity_Moved_Event'Class ) return Float;

    -- Returns the entity's current velocity, if any, in world units.
    function Get_VX( this : not null access Entity_Moved_Event'Class ) return Float;
    function Get_VY( this : not null access Entity_Moved_Event'Class ) return Float;

    ----------------------------------------------------------------------------

    -- A command to change the physical size of an entity.
    EVT_RESIZE_ENTITY : constant Event_Id := 306;

    -- An event regarding the physical size of an entity.
    type Entity_Size_Event is new Entity_Event with private;
    type A_Entity_Size_Event is access all Entity_Size_Event'Class;

    -- Returns the entity's new height in world units.
    function Get_Height( this : not null access Entity_Size_Event'Class ) return Natural;

    -- Returns the entity's new width in world units.
    function Get_Width( this : not null access Entity_Size_Event'Class ) return Natural;

    ----------------------------------------------------------------------------

    EVT_ENTITY_RESIZED : constant Event_Id := 307;

    -- A notification that an entity's physical size changed.
    type Entity_Resized_Event is new Entity_Size_Event with private;
    type A_Entity_Resized_Event is access all Entity_Resized_Event'Class;

    -- Returns the entity's previous height in world units.
    function Get_Prev_Height( this : not null access Entity_Resized_Event'Class ) return Natural;

    -- Returns the entity's previous width in world units.
    function Get_Prev_Width( this : not null access Entity_Resized_Event'Class ) return Natural;

    ----------------------------------------------------------------------------

    -- Queues an Entity_Moved event.
    procedure Queue_Entity_Moved( id           : Entity_Id;
                                  x, y         : Float;
                                  prevX, prevY : Float;
                                  vx, vy       : Float );

    -- Queues an Entity_Resized event.
    procedure Queue_Entity_Resized( id         : Entity_Id;
                                    width,
                                    height     : Natural;
                                    prevWidth,
                                    prevHeight : Natural );

    -- Queues a Move_Entity event.
    procedure Queue_Move_Entity( id : Entity_Id; x, y : Float );

    -- Queues a Resize_Entity event.
    procedure Queue_Resize_Entity( id : Entity_Id; width, height : Natural );

private

    type Entity_Location_Event is new Entity_Event with
        record
            x, y : Float := 0.0;
        end record;

    procedure Construct( this  : access Entity_Location_Event;
                         evtId : Event_Id;
                         id    : Entity_Id;
                         x, y  : Float );

    ----------------------------------------------------------------------------

    type Entity_Moved_Event is new Entity_Location_Event with
        record
            prevX, prevY : Float := 0.0;
            vx, vy       : Float := 0.0;
        end record;

    procedure Construct( this   : access Entity_Moved_Event;
                         evtId  : Event_Id;
                         id     : Entity_Id;
                         x, y   : Float;
                         prevX,
                         prevY  : Float;
                         vx, vy : Float );

    ----------------------------------------------------------------------------

    type Entity_Size_Event is new Entity_Event with
        record
            width, height : Natural := 0;
        end record;

    procedure Construct( this   : access Entity_Size_Event;
                         evtId  : Event_Id;
                         id     : Entity_Id;
                         width,
                         height : Natural );

    ----------------------------------------------------------------------------

    type Entity_Resized_Event is new Entity_Size_Event with
        record
            prevWidth, prevHeight : Natural := 0;
        end record;

    procedure Construct( this       : access Entity_Resized_Event;
                         evtId      : Event_Id;
                         id         : Entity_Id;
                         width,
                         height     : Natural;
                         prevWidth,
                         prevHeight : Natural );

end Events.Entities.Locations;
