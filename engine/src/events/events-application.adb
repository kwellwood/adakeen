--
-- Copyright (c) 2012-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Events.Manager;

package body Events.Application is

    not overriding
    procedure Construct( this    : access Menu_Event;
                         evtId   : Event_Id;
                         display : A_Allegro_Display;
                         menuId  : Integer ) is
    begin
        Event(this.all).Construct( evtId );
        this.display := display;
        this.menuId := menuId;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Display( this : not null access Menu_Event'Class ) return A_Allegro_Display is (this.display);

    function Get_Menu_Id( this : not null access Menu_Event'Class ) return Integer is (this.menuId);

    ----------------------------------------------------------------------------

    overriding
    function To_String( this : access Menu_Event ) return String
    is ("<Menu_Event - id:" & Integer'Image( this.menuId ) & ">");

    --==========================================================================

    not overriding
    procedure Construct( this          : access Window_Event;
                         evtId         : Event_Id;
                         display       : A_Allegro_Display;
                         x, y          : Integer;
                         width, height : Integer ) is
    begin
        Event(this.all).Construct( evtId );
        this.display := display;
        this.x := x;
        this.y := y;
        this.width := width;
        this.height := height;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Display( this : not null access Window_Event'Class ) return A_Allegro_Display is (this.display);

    function Get_Width( this : not null access Window_Event'Class ) return Integer is (this.width);

    function Get_Height( this : not null access Window_Event'Class ) return Integer is (this.height);

    function Get_X( this : not null access Window_Event'Class ) return Integer is (this.x);

    function Get_Y( this : not null access Window_Event'Class ) return Integer is (this.y);

    ----------------------------------------------------------------------------

    overriding
    function To_String( this : access Window_Event ) return String
    is ("<Window_Event - x:" & Integer'Image( this.x ) & ", y:" & Integer'Image( this.y ) &
        ", width:" & Integer'Image( this.width ) & ", height:" & Integer'Image( this.height ) & ">");

    --==========================================================================

    procedure Queue_App_Blur is
        evt : A_Event := new Event;
    begin
        evt.Construct( EVT_APP_BLUR );
        Events.Manager.Global.Queue_Event( evt );
    end Queue_App_Blur;

    ----------------------------------------------------------------------------

    procedure Queue_App_Focus is
        evt : A_Event := new Event;
    begin
        evt.Construct( EVT_APP_FOCUS );
        Events.Manager.Global.Queue_Event( evt );
    end Queue_App_Focus;

    ----------------------------------------------------------------------------

    procedure Queue_Close_Window is
        evt : A_Event := new Event;
    begin
        evt.Construct( EVT_CLOSE_WINDOW );
        Events.Manager.Global.Queue_Event( evt );
    end Queue_Close_Window;

    ----------------------------------------------------------------------------

    procedure Queue_Exit_Application is
        evt : A_Event := new Event;
    begin
        evt.Construct( EVT_EXIT_APPLICATION );
        Events.Manager.Global.Queue_Event( evt );
    end Queue_Exit_Application;

    ----------------------------------------------------------------------------

    procedure Queue_Menu_Clicked( display : A_Allegro_Display; menuId : Integer ) is
        evt : A_Menu_Event := new Menu_Event;
    begin
        evt.Construct( EVT_MENU_CLICKED, display, menuId );
        Events.Manager.Global.Queue_Event( A_Event(evt) );
    end Queue_Menu_Clicked;

    ----------------------------------------------------------------------------

    procedure Queue_Window_Resized( display       : A_Allegro_Display;
                                    x, y          : Integer;
                                    width, height : Integer ) is
        evt : A_Window_Event := new Window_Event;
    begin
        evt.Construct( EVT_WINDOW_RESIZED, display, x, y, width, height );
        Events.Manager.Global.Queue_Event( A_Event(evt) );
    end Queue_Window_Resized;

end Events.Application;
