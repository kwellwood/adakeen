--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Real_Time;                     use Ada.Real_Time;
with Objects;                           use Objects;
with Values;                            use Values;

package Events is

    pragma Elaborate_Body;

    type Event_Id is new Integer;

    ----------------------------------------------------------------------------

    type Event_Status is (ST_NONE, ST_SUCCESS, ST_FAILED);

    -- A Response_Type represents a response to an event by an event listener.
    type Response_Type is private;

    -- Returns the status returned by the event listener in its response.
    function Get_Status( response : Response_Type ) return Event_Status;

    -- Returns a reference to the response value returned by the event listener.
    -- It may be of any Value type.
    function Get_Value( response : Response_Type ) return Value;

    -- Sets the Response's status and result value (by copy). This is to be
    -- called by an event listener returning a response.
    procedure Set_Response( response : in out Response_Type;
                            stat     : Event_Status;
                            val      : Value'Class := Null_Value );

    -- The default no-response, as a status of NONE and a Null value.
    No_Response : constant Response_Type;

    ----------------------------------------------------------------------------

    -- An Event object represents a command being given, or notification of a
    -- change that occurred. It contains all of the relevant information to
    -- describe the command or notification.
    type Event is new Object with private;
    type A_Event is access all Event'Class;

    -- Returns the event identifier. All event instances of the same class share
    -- the same event identifier.
    function Get_Event_Id( this : not null access Event'Class ) return Event_Id;

    -- Returns a deep copy of Event 'src'. All events are required to be
    -- copyable.
    function Copy( src : A_Event ) return A_Event;
    pragma Postcondition( Copy'Result /= src or else src = null );

    -- Deletes the Event.
    procedure Delete( this : in out A_Event );
    pragma Postcondition( this = null );

    ----------------------------------------------------------------------------

    type Event_Channel is mod 16;

    NO_CHANNEL   : constant Event_Channel := 0;    -- out-of-band event channel
    GAME_CHANNEL : constant Event_Channel := 1;    -- game state notifications
    VIEW_CHANNEL : constant Event_Channel := 2;    -- view commands to the game

private

    type Response_Type is
        record
            stat : Event_Status := ST_NONE;
            val  : Value;
        end record;

    No_Response : constant Response_Type := Response_Type'(others => <>);

    type Sequence_Id is mod 2**32;

    ----------------------------------------------------------------------------

    type Event is new Object with
        record
            evtId   : Event_Id;
            evtChan : Event_Channel := NO_CHANNEL;
            evtSeq  : Sequence_Id := 0;
            evtTime : Time := Time_First;
        end record;

    procedure Construct( this : access Event; evtId : Event_Id );

    function To_String( this : access Event ) return String;

end Events;
