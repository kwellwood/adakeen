--
-- Copyright (c) 2012-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Values.Construction;               use Values.Construction;

package body Events is

    function Get_Status( response : Response_Type ) return Event_Status is (response.stat);

    ----------------------------------------------------------------------------

    function Get_Value( response : Response_Type ) return Value is (response.val);

    ----------------------------------------------------------------------------

    procedure Set_Response( response : in out Response_Type;
                            stat     : Event_Status;
                            val      : Value'Class := Null_Value ) is
    begin
        response.stat := stat;
        response.val := Clone( val );
    end Set_Response;

    --==========================================================================

    not overriding
    procedure Construct( this : access Event; evtId : Event_Id ) is
    begin
        Object(this.all).Construct;
        this.evtId := evtId;
        this.evtTime := Clock;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Event_Id( this : not null access Event'Class ) return Event_Id is (this.evtId);

    ----------------------------------------------------------------------------

    overriding
    function To_String( this : access Event ) return String is ("<Event evtId:" & Event_Id'Image( this.evtId ) & ">");

    ----------------------------------------------------------------------------

    function Copy( src : A_Event ) return A_Event is (A_Event(Copy( A_Object(src) )));

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Event ) is
    begin
        Delete( A_Object(this) );
    end Delete;

end Events;
