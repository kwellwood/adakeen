--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Maps;                              use Maps;
with Values.Maps;                       use Values.Maps;
with Worlds;                            use Worlds;

package Events.World is

    -- A command to create a new, empty world.
    EVT_CREATE_WORLD : constant Event_Id := 800;

    type Create_World_Event is new Event with private;
    type A_Create_World_Event is access all Create_World_Event'Class;

    -- Returns the height of the new world in tiles.
    function Get_Height( this : not null access Create_World_Event'Class ) return Positive;

    -- Returns the name of the library to be used for tiles.
    function Get_Library_Name( this : not null access Create_World_Event'Class ) return String;

    -- Returns the name of the template of the player entity to be added to the
    -- world.
    function Get_Player_Template( this : not null access Create_World_Event'Class ) return String;

    -- Returns the width of a tile in world coordinates.
    function Get_Tile_Width( this : not null access Create_World_Event'Class ) return Positive;

    -- Returns the width of the new world in tiles.
    function Get_Width( this : not null access Create_World_Event'Class ) return Positive;

    ----------------------------------------------------------------------------

    -- A command to load a world from disk.
    EVT_LOAD_WORLD : constant Event_Id := 801;

    type Load_World_Event is new Event with private;
    type A_Load_World_Event is access all Load_World_Event'Class;

    -- Returns the filename of the world to load.
    function Get_Filename( this : not null access Load_World_Event'Class ) return String;

    ----------------------------------------------------------------------------

    -- A notification that a world has been loaded/created/resized.
    EVT_WORLD_LOADED : constant Event_Id := 802;

    type World_Loaded_Event is new Event with private;
    type A_World_Loaded_Event is access all World_Loaded_Event'Class;

    -- Returns the name of the library used for tiles.
    function Get_Library_Name( this : not null access World_Loaded_Event'Class ) return String;

    -- Returns a reference to the world's map. Do not modify it.
    function Get_Map( this : not null access World_Loaded_Event'Class ) return A_Map;
    pragma Postcondition( Get_Map'Result /= null );

    -- Returns the width of a tile in world coordinates.
    function Get_Tile_Width( this : not null access World_Loaded_Event'Class ) return Positive;

    ----------------------------------------------------------------------------

    -- A command to set the value of a world property.
    EVT_SET_WORLD_PROPERTY : constant Event_Id := 803;

    -- A notification that a world property changed.
    EVT_WORLD_PROPERTY_CHANGED : constant Event_Id := 804;

    -- An event involving a world property value.
    type World_Property_Event is new Event with private;
    type A_World_Property_Event is access all World_Property_Event'Class;

    -- Returns the name of the property.
    function Get_Property_Name( this : not null access World_Property_Event'Class ) return String;

    -- Returns the property's value. (not a copy; do not modify it!)
    function Get_Value( this : not null access World_Property_Event'Class ) return Value;

    ----------------------------------------------------------------------------

    -- A command to resize the world map.
    EVT_RESIZE_WORLD : constant Event_Id := 805;

    type Resize_World_Event is new Event with private;
    type A_Resize_World_Event is access all Resize_World_Event'Class;

    -- Returns the new height of the map in tiles.
    function Get_Height( this : not null access Resize_World_Event'Class ) return Positive;

    -- Returns the new width of the map in tiles.
    function Get_Width( this : not null access Resize_World_Event'Class ) return Positive;

    -- Returns the X origin of the map in tiles.
    function Get_X( this : not null access Resize_World_Event'Class ) return Integer;

    -- Returns the Y origin of the map in tiles.
    function Get_Y( this : not null access Resize_World_Event'Class ) return Integer;

    ----------------------------------------------------------------------------

    -- A command to set the tile id at a specific location.
    EVT_SET_TILE : constant Event_Id := 806;

    -- A notification that a tile in the map changed.
    EVT_TILE_CHANGED : constant Event_Id := 807;

    -- An event involving a world tile location.
    type Tile_Event is new Event with private;
    type A_Tile_Event is access all Tile_Event'Class;

    -- Returns the number of tiles involved in this event.
    function Get_Count( this : not null access Tile_Event'Class ) return Natural;

    -- Returns a list of all changed tiles involved in this event.
    function Get_List( this : not null access Tile_Event'Class ) return Tile_Change_Lists.Vector;

    ----------------------------------------------------------------------------

    -- A command to delete a layer in the world map.
    EVT_DELETE_LAYER : constant Event_Id := 808;

    -- A notification that a layer has been deleted from the world map.
    EVT_LAYER_DELETED: constant Event_Id := 809;

    type Layer_Event is new Event with private;
    type A_Layer_Event is access all Layer_Event'Class;

    -- Returns the relevant layer index.
    function Get_Layer( this : not null access Layer_Event'Class ) return Integer;

    ----------------------------------------------------------------------------

    -- A command to create a new layer in the world map.
    EVT_CREATE_LAYER : constant Event_Id := 810;

    type Layer_Create_Event is new Layer_Event with private;
    type A_Layer_Create_Event is access all Layer_Create_Event'Class;

    -- Returns an optional map containing the layer data to pre-populate. If
    -- provided, the layer data will be at layer 0. No other layers are stored
    -- in the map and its layer properties are undefined. The returned value is
    -- not a copy; do NOT modify it.
    --
    -- Returns null if no map was provided; the layer will be created empty.
    function Get_Layer_Data( this : not null access Layer_Create_Event'Class ) return A_Map;

    -- Returns a map of the property values of the new layer, indexed by name.
    -- (not a copy; do not modify it!)
    function Get_Properties( this : not null access Layer_Create_Event'Class ) return Map_Value;
    pragma Postcondition( Get_Properties'Result.Valid );

    -- Returns the type of the new layer to create.
    function Get_Type( this : not null access Layer_Create_Event'Class ) return Layer_Type;

    ----------------------------------------------------------------------------

    -- A notification that a new layer has been created in the world map.
    EVT_LAYER_CREATED : constant Event_Id := 811;

    type Layer_Created_Event is new Layer_Create_Event with private;
    type A_Layer_Created_Event is access all Layer_Created_Event'Class;

    -- Returns the property definitions of the new layer. (not a copy; do not
    -- modify it!)
    function Get_Property_Defs( this : not null access Layer_Created_Event'Class ) return Map_Value;
    pragma Postcondition( Get_Property_Defs'Result.Valid );

    ----------------------------------------------------------------------------

    -- A command to set the value of a world map layer's property.
    EVT_SET_LAYER_PROPERTY : constant Event_Id := 812;

    -- A notification that a layer property changed.
    EVT_LAYER_PROPERTY_CHANGED : constant Event_Id := 813;

    type Layer_Property_Event is new Layer_Event with private;
    type A_Layer_Property_Event is access all Layer_Property_Event'Class;

    -- Returns the name of the property.
    function Get_Property_Name( this : not null access Layer_Property_Event'Class ) return String;

    -- Returns the property's value. (not a copy; do not modify it!)
    function Get_Value( this : not null access Layer_Property_Event'Class ) return Value;

    ----------------------------------------------------------------------------

    -- A command to swap the positions of two layers in the world map.
    EVT_SWAP_LAYERS : constant Event_Id := 814;

    -- A notification that the positions of two layers in the world map have
    -- been swapped.
    EVT_LAYERS_SWAPPED : constant Event_Id := 815;

    type Layer_Swap_Event is new Layer_Event with private;
    type A_Layer_Swap_Event is access all Layer_Swap_Event'Class;

    -- Returns the other layer involved in the layer the swap.
    function Get_Other_Layer( this : not null access Layer_Swap_Event'Class ) return Integer;

    ----------------------------------------------------------------------------

    -- A notification that the world's state has been modified or is no longer
    -- modified.
    EVT_WORLD_MODIFIED   : constant Event_Id := 816;
    EVT_WORLD_UNMODIFIED : constant Event_Id := 817;

    ----------------------------------------------------------------------------

    -- Creates a new, empty world, sending by a World_Loaded event on completion,
    -- followed by others on success.
    procedure Queue_Create_World( width, height  : Positive;
                                  tileWidth      : Positive;
                                  libName,
                                  playerTemplate : String );

    -- Loads a world asynchronously. If the world has already been loaded and
    -- set to preserve its state, then it will be loaded from the cache and
    -- maintain its previous state.
    procedure Queue_Load_World( filename : String );

    -- Notifies listeners that a world has been loaded into the Game- by loading
    -- from disk, by creating a new world, etc. 'map' will be copied.
    procedure Queue_World_Loaded( map       : not null A_Map;
                                  tileWidth : Positive;
                                  libName   : String );

    -- Asynchronously sets a world property 'name' to 'val'. The value will be
    -- copied.
    procedure Queue_Set_World_Property( name : String; val : Value'Class );

    -- Notifies listeners that the property 'name' of the world has changed. The
    -- property's new value is 'val'. The value will be copied.
    procedure Queue_World_Property_Changed( name : String; val : Value'Class );

    -- Resizes the world, sending by a World_Loaded event on completion, followed
    -- by others on success.
    procedure Queue_Resize_World( width, height : Positive );

    -- Resizes the world, sending by a World_Loaded event on completion, followed
    -- by others on success. 'x,y' is the location in the current map for the new
    -- map's origin.
    procedure Queue_Resize_World( x, y          : Integer;
                                  width, height : Positive );

    -- Set a single tile in the world by location .
    procedure Queue_Set_Tile( layer    : Integer;
                              col, row : Integer;       -- tile coordinates
                              tileId   : Natural );

    -- Set a list of tiles in the world by location (layer, col/x, row/y).
    procedure Queue_Set_Tiles( tiles : Tile_Change_Lists.Vector );

    -- Notify listeners that a tile in the world has changed.
    procedure Queue_Tile_Changed( layer    : Integer;
                                  col, row : Integer;
                                  tileId   : Natural );

    -- Notify listeners that a list of tiles 'tiles' have changed.
    procedure Queue_Tiles_Changed( tiles : Tile_Change_Lists.Vector );

    -- Deletes a world map layer by index.
    procedure Queue_Delete_Layer( layer : Integer );

    -- Notifies listeners that a layer in the world map has been deleted.
    procedure Queue_Layer_Deleted( layer : Integer );

    -- Creates a new layer directly in the foreground of layer 'beforeLayer', of
    -- type 'layerType'. If 'beforeLayer' is greater than the current background
    -- layer index, the layer will be created behind all layers. 'layerData' is
    -- an optional map containing layer data to pre-populate, stored at layer 0;
    -- it will be copied. Pass null to create an empty layer. 'properties'
    -- contains layer properties that will override the defaults; it will be
    -- copied.
    procedure Queue_Create_Layer( beforeLayer : Integer;
                                  layerType   : Layer_Type;
                                  layerData   : A_Map;
                                  properties  : Map_Value );

    -- Notifies listeners that a new map layer has been created at index 'layer'.
    -- If 'layer' is > 0 (a background layer), it pushes the existing layer at
    -- that index further into the background (it becomes 'layer' + 1). If
    -- 'layer' is < 0 (a foreground layer), it pushes the existing layer at that
    -- index further into the foreground (it becomes 'layer' - 1). The 'layer'
    -- must not be 0 unless it is the first layer to exist in the world map.
    -- 'layerData' is an optional map containing the new layer's data (stored in
    -- layer 0); it will be copid. Pass null if the new layer is empty.
    -- 'properties' contains layer property values and 'propDefs' contains
    -- property definitions for the editor. They will both be copid.
    procedure Queue_Layer_Created( layer      : Integer;
                                   layerType  : Layer_Type;
                                   layerData  : A_Map;
                                   properties : Map_Value;
                                   propDefs   : Map_Value );

    -- Asynchronously sets layer property 'name' to 'val' in layer 'layer' of
    -- the world map. The value will be copied.
    procedure Queue_Set_Layer_Property( layer : Integer;
                                        name  : String;
                                        val   : Value'Class );

    -- Notifies listeners that property 'name' has changed to 'val' in layer
    -- 'layer' of the world map. The value will be copied.
    procedure Queue_Layer_Property_Changed( layer : Integer;
                                            name  : String;
                                            val   : Value'Class );

    -- Swaps the positions of two map layers by index, 'layer' with 'otherLayer'.
    -- If either 'layer' or 'otherLayer' equals 0, the request will be ignored.
    procedure Queue_Swap_Layers( layer, otherLayer : Integer );

    -- Notifies listeners that map layers 'layer' and 'otherLayer' have swapped
    -- places. All other layers are unaffected.
    procedure Queue_Layers_Swapped( layer, otherLayer : Integer );

    -- The world's state has changed since it was loaded from disk. This is for
    -- an editor application to know when the world needs to be saved.
    procedure Queue_World_Modified;

    -- The world's state is now unmodified, i.e. it was saved to disk. An
    -- EVT_WORLD_LOADED event also implies it is unmodified.
    procedure Queue_World_Unmodified;

private

    type Create_World_Event is new Event with
        record
            width,
            height         : Positive := 1;
            tileWidth      : Positive := 1;
            libName        : Unbounded_String;
            playerTemplate : Unbounded_String;
        end record;

    procedure Construct( this           : access Create_World_Event;
                         width,
                         height         : Positive;
                         tileWidth      : Positive;
                         libName,
                         playerTemplate : String );

    ----------------------------------------------------------------------------

    type Load_World_Event is new Event with
        record
            filename : Unbounded_String;
        end record;

    procedure Construct( this : access Load_World_Event; filename : String );

    function To_String( this : access Load_World_Event ) return String;

    ----------------------------------------------------------------------------

    type World_Loaded_Event is new Event with
        record
            map       : A_Map;
            tileWidth : Positive;
            libName   : Unbounded_String;
        end record;

    procedure Adjust( this : access World_Loaded_Event );

    procedure Construct( this      : access World_Loaded_Event;
                         map       : not null A_Map;
                         tileWidth : Positive;
                         libName   : String );

    procedure Delete( this : in out World_Loaded_Event );

    ----------------------------------------------------------------------------

    type World_Property_Event is new Event with
        record
            name : Unbounded_String;
            val  : Value;
        end record;

    procedure Adjust( this : access World_Property_Event );

    procedure Construct( this  : access World_Property_Event;
                         evtId : Event_Id;
                         name  : String;
                         val   : Value'Class );

    function To_String( this : access World_Property_Event ) return String;

    ----------------------------------------------------------------------------

    type Resize_World_Event is new Event with
        record
            x, y   : Integer := 0;
            width,
            height : Positive := 1;
        end record;

    procedure Construct( this   : access Resize_World_Event;
                         x, y   : Integer;
                         width,
                         height : Positive );

    ----------------------------------------------------------------------------

    type Tile_Event is new Event with
        record
            items : Tile_Change_Lists.Vector;
        end record;

    procedure Adjust( this : access Tile_Event );

    function To_String( this : access Tile_Event ) return String;

    ----------------------------------------------------------------------------

    type Layer_Event is new Event with
        record
            layer : Integer := 0;
        end record;

    procedure Construct( this  : access Layer_Event;
                         evtId : Event_Id;
                         layer : Integer );

    ----------------------------------------------------------------------------

    type Layer_Create_Event is new Layer_Event with
        record
            layerType  : Layer_Type := Layer_Type'First;
            map        : A_Map;
            properties : Map_Value;
        end record;

    procedure Adjust( this : access Layer_Create_Event );

    procedure Construct( this       : access Layer_Create_Event;
                         evtId      : Event_Id;
                         layer      : Integer;
                         layerType  : Layer_Type;
                         layerData  : A_Map;
                         properties : Map_Value );

    procedure Delete( this : in out Layer_Create_Event );

    function To_String( this : access Layer_Create_Event ) return String;

    ----------------------------------------------------------------------------

    type Layer_Created_Event is new Layer_Create_Event with
        record
            propDefs : Map_Value;
        end record;

    procedure Adjust( this : access Layer_Created_Event );

    procedure Construct( this       : access Layer_Created_Event;
                         evtId      : Event_Id;
                         layer      : Integer;
                         layerType  : Layer_Type;
                         layerData  : A_Map;
                         properties : Map_Value;
                         propDefs   : Map_Value );

    function To_String( this : access Layer_Created_Event ) return String;

    ----------------------------------------------------------------------------

    type Layer_Property_Event is new Layer_Event with
        record
            name : Unbounded_String;
            val  : Value;
        end record;

    procedure Adjust( this : access Layer_Property_Event );

    procedure Construct( this  : access Layer_Property_Event;
                         evtId : Event_Id;
                         layer : Integer;
                         name  : String;
                         val   : Value'Class );

    function To_String( this : access Layer_Property_Event ) return String;

    ----------------------------------------------------------------------------

    type Layer_Swap_Event is new Layer_Event with
        record
            otherLayer : Integer := 0;
        end record;

    procedure Construct( this       : access Layer_Swap_Event;
                         evtId      : Event_Id;
                         layer      : Integer;
                         otherLayer : Integer );

    function To_String( this : access Layer_Swap_Event ) return String;

end Events.World;
