--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Directives;                        use Directives;
with Hashed_Strings;                    use Hashed_Strings;
with Object_Ids;                        use Object_Ids;
with Values.Maps;                       use Values.Maps;
with Values.Tagged_Ids;                 use Values.Tagged_Ids;

package Events.Entities is

    -- A command to delete an entity by Entity_Id.
    EVT_DELETE_ENTITY : constant Event_Id := 300;

    -- A notification that an entity was deleted.
    EVT_ENTITY_DELETED : constant Event_Id := 301;

    -- An event involving an entity.
    type Entity_Event is new Event with private;
    type A_Entity_Event is access all Entity_Event'Class;

    -- Returns the Entity_Id of the entity involved in the event.
    function Get_Id( this : not null access Entity_Event'Class ) return Entity_Id;

    ----------------------------------------------------------------------------

    EVT_ENTITY_DIRECTIVE : constant Event_Id := 312;

    -- A command sent to an entity, either as a one-time action or ongoing until
    -- deactivated. The entity may choose to act on the directive depending on
    -- its current state.
    type Entity_Directive_Event is new Entity_Event with private;
    type A_Entity_Directive_Event is access all Entity_Directive_Event'Class;

    -- Returns the directive(s) being sent to the entity.
    function Get_Directive( this : not null access Entity_Directive_Event'Class ) return Directive_Bits;

    -- Returns the mode of the directive Once, Ongoing, or Inactive.
    function Get_Mode( this : not null access Entity_Directive_Event'Class ) return Directive_Mode;

    ----------------------------------------------------------------------------

    EVT_ENTITY_SPAWNED : constant Event_Id := 308;

    type Entity_Spawn_Event is new Entity_Event with private;
    type A_Entity_Spawn_Event is access all Entity_Spawn_Event'Class;

    -- Returns the entity's unique name, or an empty string if undefined.
    function Get_Entity_Name( this : not null access Entity_Spawn_Event'Class ) return String with Inline;

    -- Returns the properties to give to the new entity. (not a copy, do not
    -- modify it!)
    function Get_State( this : not null access Entity_Spawn_Event'Class ) return Map_Value;

    -- Returns the name of the entity template to use.
    function Get_Template( this : not null access Entity_Spawn_Event'Class ) return String;

    ----------------------------------------------------------------------------

    EVT_MESSAGE_ENTITY : constant Event_Id := 313;

    -- A command to dispatch a parameterized message to an entity.
    type Message_Entity_Event is new Entity_Event with private;
    type A_Message_Entity_Event is access all Message_Entity_Event'Class;

    -- Returns the name of the message to send the entity.
    function Get_Message( this : not null access Message_Entity_Event'Class ) return Hashed_String;

    -- Returns the message parameters. (not a copy, do not modify it!)
    function Get_Parameters( this : not null access Message_Entity_Event'Class ) return Map_Value;

    ----------------------------------------------------------------------------

    EVT_SPAWN_ENTITY : constant Event_Id := 314;

    -- A command to create a new entity in the active world.
    type Spawn_Entity_Event is new Event with private;
    type A_Spawn_Entity_Event is access all Spawn_Entity_Event'Class;

    -- Returns the properties to give to the new entity. (not a copy, do not
    -- modify it!)
    function Get_Properties( this : not null access Spawn_Entity_Event'Class ) return Map_Value;

    -- Returns the name of the entity template to use.
    function Get_Template( this : not null access Spawn_Entity_Event'Class ) return String;

    ----------------------------------------------------------------------------

    -- Creates and returns an Entity_Spawned event. The 'initialState' map will
    -- be copied.
    function Create_Entity_Spawned_Event( id           : Entity_Id;
                                          name         : String;
                                          template     : String;
                                          initialState : Map_Value ) return A_Event;

    -- Queues a Delete_Entity event.
    procedure Queue_Delete_Entity( id : Entity_Id );

    -- Queues an Entity_Deleted event.
    procedure Queue_Entity_Deleted( id : Entity_Id );

    -- Queues an Entity_Directive event to an entity.
    procedure Queue_Entity_Directive( id        : Entity_Id;
                                      directive : Directive_Bits;
                                      mode      : Directive_Mode );

    -- Queues a Message_Entity event.
    procedure Queue_Message_Entity( id      : Entity_Id;
                                    message : Hashed_String;
                                    params  : Map_Value );

    -- Queues a Spawn_Entity event. The 'properties' map will be copied.
    procedure Queue_Spawn_Entity( template   : String;
                                  properties : Map_Value );

private

    type Entity_Event is new Event with
        record
            id : Entity_Id := NULL_ID;
        end record;

    procedure Construct( this : access Entity_Event; evtId : Event_Id; id : Entity_Id );

    function To_String( this : access Entity_Event ) return String;

    ----------------------------------------------------------------------------

    type Entity_Directive_Event is new Entity_Event with
        record
            directive : Directive_Bits := NO_DIRECTIVES;
            mode      : Directive_Mode := Inactive;
        end record;

    procedure Construct( this      : access Entity_Directive_Event;
                         id        : Entity_Id;
                         directive : Directive_Bits;
                         mode      : Directive_Mode );

    function To_String( this : access Entity_Directive_Event ) return String;

    ----------------------------------------------------------------------------

    type Entity_Spawn_Event is new Entity_Event with
        record
            template : Unbounded_String;
            state    : Map_Value;
            name     : Unbounded_String;
        end record;

    procedure Adjust( this : access Entity_Spawn_Event );

    procedure Construct( this     : access Entity_Spawn_Event;
                         evtId    : Event_Id;
                         template : String;
                         state    : Map_Value;
                         name     : String;
                         id       : Entity_Id );

    function To_String( this : access Entity_Spawn_Event ) return String;

    ----------------------------------------------------------------------------

    type Message_Entity_Event is new Entity_Event with
        record
            message : Hashed_String;
            params  : Map_Value;
        end record;

    procedure Adjust( this : access Message_Entity_Event );

    procedure Construct( this    : access Message_Entity_Event;
                         id      : Entity_Id;
                         message : Hashed_String;
                         params  : Map_Value );

    function To_String( this : access Message_Entity_Event ) return String;

    ----------------------------------------------------------------------------

    type Spawn_Entity_Event is new Event with
        record
            template   : Unbounded_String;
            properties : Map_Value;
        end record;

    procedure Adjust( this : access Spawn_Entity_Event );

    procedure Construct( this       : access Spawn_Entity_Event;
                         template   : String;
                         properties : Map_Value );

    function To_String( this : access Spawn_Entity_Event ) return String;

end Events.Entities;
