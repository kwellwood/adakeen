--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Keyboard;                  use Allegro.Keyboard;
with Events.Manager;

package body Events.Input is

    not overriding
    procedure Construct( this  : access Key_Event;
                         evtId : Event_Id;
                         key   : Natural ) is
    begin
        Event(this.all).Construct( evtId );
        this.key := key;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Key( this : not null access Key_Event'Class ) return Natural is (this.key);

    ----------------------------------------------------------------------------

    overriding
    function To_String( this : access Key_Event ) return String is
    begin
        return "<Key_Event - " & Al_Keycode_To_Name( this.key ) & ">";
    end To_String;

    --==========================================================================

    not overriding
    procedure Construct( this      : access Key_Typed_Event;
                         evtId     : Event_Id;
                         key       : Natural;
                         char      : Character;
                         modifiers : Modifiers_Array ) is
    begin
        Key_Event(this.all).Construct( evtId, key );
        this.char := char;
        this.modifiers := modifiers;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Char( this : not null access Key_Typed_Event'Class ) return Character is (this.char);

    procedure Get_Modifiers( this      : not null access Key_Typed_Event'Class;
                             modifiers : out Modifiers_Array ) is
    begin
        modifiers := this.modifiers;
    end Get_Modifiers;

    function No_Modifiers( this : not null access Key_Typed_Event'Class ) return Boolean is (None( this.modifiers ));

    function Only_Alt( this : not null access Key_Typed_Event'Class ) return Boolean is (Only_Alt( this.modifiers ));

    function Only_Ctrl( this : not null access Key_Typed_Event'Class ) return Boolean is (Only_Ctrl( this.modifiers ));

    function Only_Shift( this : not null access Key_Typed_Event'Class ) return Boolean is (Only_Shift( this.modifiers ));

    ----------------------------------------------------------------------------

    overriding
    function To_String( this : access Key_Typed_Event ) return String is
    begin
        return "<Key_Typed_Event - " &
               (if this.modifiers(CMD)   then "Cmd + "   else "") &
               (if this.modifiers(CTRL ) then "Ctrl + "  else "") &
               (if this.modifiers(ALT)   then "Alt + "   else "") &
               (if this.modifiers(SHIFT) then "Shift + " else "") &
               Al_Keycode_To_Name( this.key ) & ">";
    end To_String;

    --==========================================================================

    not overriding
    procedure Construct( this      : access Gamepad_Event;
                         evtId     : Event_Id;
                         gamepadId : Integer;
                         button    : Gamepad_Button := BUTTON_NONE;
                         axis      : Gamepad_Axis := AXIS_NONE;
                         position  : Float := 0.0 ) is
    begin
        Event(this.all).Construct( evtId );
        this.gamepadId := gamepadId;
        this.button := button;
        this.axis := axis;
        this.position := position;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Axis( this : not null access Gamepad_Event'Class ) return Gamepad_Axis is (this.axis);

    function Get_Button( this : not null access Gamepad_Event'Class ) return Gamepad_Button is (this.button);

    function Get_Gamepad_Id( this : not null access Gamepad_Event'Class ) return Integer is (this.gamepadId);

    function Get_Position( this : not null access Gamepad_Event'Class ) return Float is (this.position);

    --==========================================================================

    not overriding
    procedure Construct( this  : access Mouse_Event;
                         evtId : Event_Id;
                         x, y  : Integer ) is
    begin
        Event(this.all).Construct( evtId );
        this.x := x;
        this.y := y;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_X( this : not null access Mouse_Event'Class ) return Integer is (this.x);

    function Get_Y( this : not null access Mouse_Event'Class ) return Integer is (this.y);

    procedure Set_XY( this : not null access Mouse_Event'Class; x, y : Integer ) is
    begin
        this.x := x;
        this.y := y;
    end Set_XY;

    overriding
    function To_String( this : access Mouse_Event ) return String
    is (
        "<Mouse_Event - x:" & Integer'Image( this.x ) &
        ", y:" & Integer'Image( this.y ) & ">"
    );

    --==========================================================================

    not overriding
    procedure Construct( this      : access Mouse_Button_Event;
                         evtId     : Event_Id;
                         x, y      : Integer;
                         btn       : Mouse_Button;
                         modifiers : Modifiers_Array ) is
    begin
        Mouse_Event(this.all).Construct( evtId, x, y );
        this.btn := btn;
        this.modifiers := modifiers;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Button( this : not null access Mouse_Button_Event'Class ) return Mouse_Button is (this.btn);

    function Get_Modifiers( this : not null access Mouse_Button_Event'Class ) return Modifiers_Array is (this.modifiers);

    overriding
    function To_String( this : access Mouse_Button_Event ) return String
    is (
        "<Mouse_Button_Event - x:" & Integer'Image( this.x ) &
        ", y:" & Integer'Image( this.y ) &
        ", btn: " & To_String( this.btn ) & ">"
    );

    --==========================================================================

    not overriding
    procedure Construct( this    : access Mouse_Scroll_Event;
                         evtId   : Event_Id;
                         x, y    : Integer;
                         amount  : Integer ) is
    begin
        Mouse_Event(this.all).Construct( evtId, x, y );
        this.amount := amount;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Amount( this : not null access Mouse_Scroll_Event'Class ) return Integer is (this.amount);

    --==========================================================================

    procedure Queue_Gamepad_Move( gamepadId : Integer;
                                  axis      : Gamepad_Axis;
                                  position  : Float ) is
        evt : A_Gamepad_Event := new Gamepad_Event;
    begin
        evt.Construct( EVT_GAMEPAD_MOVE, gamepadId, axis => axis, position => position );
        Events.Manager.Global.Queue_Event( A_Event(evt) );
    end Queue_Gamepad_Move;

    ----------------------------------------------------------------------------

    procedure Queue_Gamepad_Press( gamepadId : Integer; button : Gamepad_Button ) is
        evt : A_Gamepad_Event := new Gamepad_Event;
    begin
        evt.Construct( EVT_GAMEPAD_PRESS, gamepadId, button => button );
        Events.Manager.Global.Queue_Event( A_Event(evt) );
    end Queue_Gamepad_Press;

    ----------------------------------------------------------------------------

    procedure Queue_Gamepad_Release( gamepadId : Integer; button : Gamepad_Button ) is
        evt : A_Gamepad_Event := new Gamepad_Event;
    begin
        evt.Construct( EVT_GAMEPAD_RELEASE, gamepadId, button => button );
        Events.Manager.Global.Queue_Event( A_Event(evt) );
    end Queue_Gamepad_Release;

    ----------------------------------------------------------------------------

    procedure Queue_Gamepad_Removed( gamepadId : Integer ) is
        evt : A_Gamepad_Event := new Gamepad_Event;
    begin
        evt.Construct( EVT_GAMEPAD_REMOVED, gamepadId );
        Events.Manager.Global.Queue_Event( A_Event(evt) );
    end Queue_Gamepad_Removed;

    ----------------------------------------------------------------------------

    procedure Queue_Key_Press( key : Natural ) is
        evt : A_Key_Event := new Key_Event;
    begin
        evt.Construct( EVT_KEY_PRESS, key );
        Events.Manager.Global.Queue_Event( A_Event(evt) );
    end Queue_Key_Press;

    ----------------------------------------------------------------------------

    procedure Queue_Key_Release( key : Natural ) is
        evt : A_Key_Event := new Key_Event;
    begin
        evt.Construct( EVT_KEY_RELEASE, key );
        Events.Manager.Global.Queue_Event( A_Event(evt) );
    end Queue_Key_Release;

    ----------------------------------------------------------------------------

    function Generate_Key_Release( key : Natural ) return A_Key_Event is
        evt : constant A_Key_Event := new Key_Event;
    begin
        evt.Construct( EVT_KEY_RELEASE, key );
        return evt;
    end Generate_Key_Release;

    ----------------------------------------------------------------------------

    procedure Queue_Key_Typed( key       : Natural;
                               char      : Character;
                               modifiers : Modifiers_Array ) is
        evt : A_Key_Typed_Event := new Key_Typed_Event;
    begin
        evt.Construct( EVT_KEY_TYPED, key, char, modifiers );
        Events.Manager.Global.Queue_Event( A_Event(evt) );
    end Queue_Key_Typed;

    ----------------------------------------------------------------------------

    procedure Queue_Mouse_Click( x, y : Integer; btn : Mouse_Button ) is
        evt : A_Mouse_Button_Event := new Mouse_Button_Event;
    begin
        evt.Construct( EVT_MOUSE_CLICK, x, y, btn, MODIFIERS_NONE );
        Events.Manager.Global.Queue_Event( A_Event(evt) );
    end Queue_Mouse_Click;

    ----------------------------------------------------------------------------

    procedure Queue_Mouse_Doubleclick( x, y : Integer; btn : Mouse_Button ) is
        evt : A_Mouse_Button_Event := new Mouse_Button_Event;
    begin
        evt.Construct( EVT_MOUSE_DOUBLECLICK, x, y, btn, MODIFIERS_NONE );
        Events.Manager.Global.Queue_Event( A_Event(evt) );
    end Queue_Mouse_Doubleclick;

    ----------------------------------------------------------------------------

    procedure Queue_Mouse_Held( x, y : Integer; btn : Mouse_Button ) is
        evt : A_Mouse_Button_Event := new Mouse_Button_Event;
    begin
        evt.Construct( EVT_MOUSE_HELD, x, y, btn, MODIFIERS_NONE );
        Events.Manager.Global.Queue_Event( A_Event(evt) );
    end Queue_Mouse_Held;

    ----------------------------------------------------------------------------

    procedure Queue_Mouse_Move( x, y : Integer ) is
        evt : A_Mouse_Event := new Mouse_Button_Event;
    begin
        evt.Construct( EVT_MOUSE_MOVE, x, y );
        Events.Manager.Global.Queue_Event( A_Event(evt) );
    end Queue_Mouse_Move;

    ----------------------------------------------------------------------------

    procedure Queue_Mouse_Leave is
        evt : A_Event := new Event;
    begin
        evt.Construct( EVT_MOUSE_LEAVE );
        Events.Manager.Global.Queue_Event( A_Event(evt) );
    end Queue_Mouse_Leave;

    ----------------------------------------------------------------------------

    procedure Queue_Mouse_Press( x, y      : Integer;
                                 btn       : Mouse_Button;
                                 modifiers : Modifiers_Array ) is
        evt : A_Mouse_Button_Event := new Mouse_Button_Event;
    begin
        evt.Construct( EVT_MOUSE_PRESS, x, y, btn, modifiers );
        Events.Manager.Global.Queue_Event( A_Event(evt) );
    end Queue_Mouse_Press;

    ----------------------------------------------------------------------------

    procedure Queue_Mouse_Release( x, y : Integer; btn : Mouse_Button ) is
        evt : A_Mouse_Button_Event := new Mouse_Button_Event;
    begin
        evt.Construct( EVT_MOUSE_RELEASE, x, y, btn, MODIFIERS_NONE );
        Events.Manager.Global.Queue_Event( A_Event(evt) );
    end Queue_Mouse_Release;

    ----------------------------------------------------------------------------

    function Create_Mouse_Release( x, y : Integer;
                                   btn  : Mouse_Button ) return A_Mouse_Button_Event is
        evt : constant A_Mouse_Button_Event := new Mouse_Button_Event;
    begin
        evt.Construct( EVT_MOUSE_RELEASE, x, y, btn, MODIFIERS_NONE );
        return evt;
    end Create_Mouse_Release;

    ----------------------------------------------------------------------------

    procedure Queue_Mouse_Scroll( x, y : Integer; amount : Integer ) is
        evt : A_Mouse_Scroll_Event := new Mouse_Scroll_Event;
    begin
        evt.Construct( EVT_MOUSE_SCROLL, x, y, amount );
        Events.Manager.Global.Queue_Event( A_Event(evt) );
    end Queue_Mouse_Scroll;

end Events.Input;
