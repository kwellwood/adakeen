--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Events.Manager;

package body Events.Entities.Sprites is

    not overriding
    procedure Construct( this  : access Frame_Changed_Event;
                         id    : Entity_Id;
                         frame : Natural ) is
    begin
        Entity_Event(this.all).Construct( EVT_FRAME_CHANGED, id );
        this.frame := frame;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Frame( this : not null access Frame_Changed_Event'Class ) return Natural is (this.frame);

    --==========================================================================

    not overriding
    procedure Construct( this   : access Entity_Z_Event;
                         id     : Entity_Id;
                         z      : Float;
                         zOrder : Integer ) is
    begin
        Entity_Event(this.all).Construct( EVT_ENTITY_Z_CHANGED, id );
        this.z := z;
        this.zOrder := zOrder;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Z( this : not null access Entity_Z_Event'Class ) return Float is (this.z);

    ----------------------------------------------------------------------------

    function Get_ZOrder( this : not null access Entity_Z_Event'Class ) return Integer is (this.zOrder);

    --==========================================================================

    procedure Queue_Frame_Changed( id : Entity_Id; frame : Natural ) is
        evt : A_Frame_Changed_Event := new Frame_Changed_Event;
    begin
        evt.Construct( id, frame );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_Frame_Changed;

    ----------------------------------------------------------------------------

    procedure Queue_Entity_Z_Changed( id : Entity_Id; z : Float; zOrder : Integer ) is
        evt : A_Entity_Z_Event := new Entity_Z_Event;
    begin
        evt.Construct( id, z, zOrder );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_Entity_Z_Changed;

end Events.Entities.Sprites;
