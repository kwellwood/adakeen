--
-- Copyright (c) 2017-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Values.Construction;               use Values.Construction;

package body Maps.Info_Caches is

    function Create_Map_Info_Cache return A_Map_Info_Cache is
        this : constant A_Map_Info_Cache := new Map_Info_Cache;
    begin
        this.Construct;
        return this;
    end Create_Map_Info_Cache;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Map_Info_Cache ) is
    begin
        this.Delete_All;
        Limited_Object(this).Delete;
    end Delete;

    --------------------------------------------------------------------------d--

    procedure Delete_All( this : in out Map_Info_Cache'Class ) is
    begin
        for l in this.layered.First..this.layered.Last loop
            Delete( this.layered.Reference( l ) );
        end loop;
        this.layered.Clear;

        Delete( this.blended );
    end Delete_All;

    ----------------------------------------------------------------------------

    function Get_Map( this : not null access Map_Info_Cache'Class ) return A_Map is (this.map);

    ----------------------------------------------------------------------------

    function Get_Shape( this     : not null access Map_Info_Cache'Class;
                        col, row : Integer ) return Clip_Type is
    begin
        if this.map /= null and then this.map.In_Map_Area( col, row ) then
            return this.blended(row * this.map.Get_Width + col).shape;
        end if;
        return Passive;
    end Get_Shape;

    ----------------------------------------------------------------------------

    function Get_Tile( this     : not null access Map_Info_Cache'Class;
                       layer    : Integer;
                       col, row : Integer ) return A_Tile is
    begin
        if this.map /= null and then this.map.In_Map_Area( col, row ) then
            if layer in this.layered.First..this.layered.Last then
                return this.layered.Element( layer ).all(row * this.map.Get_Width + col);
            end if;
        end if;
        return null;
    end Get_Tile;

    ----------------------------------------------------------------------------

    function Get_TypeBits( this     : not null access Map_Info_Cache'Class;
                           col, row : Integer ) return Tile_Bits is
    begin
        if this.map /= null and then this.map.In_Map_Area( col, row ) then
            return this.blended(row * this.map.Get_Width + col).bits;
        end if;
        return TILE_INERT;
    end Get_TypeBits;

    ----------------------------------------------------------------------------

    function Has_TypeBits( this     : not null access Map_Info_Cache'Class;
                           col, row : Integer;
                           typeBits : Tile_Bits ) return Boolean
        is ((this.Get_TypeBits( col, row ) and typeBits) = typeBits);

    ----------------------------------------------------------------------------

    procedure Initialize( this : not null access Map_Info_Cache'Class;
                          map  : A_Map;
                          lib  : Library_Ptr ) is
    begin
        this.Delete_All;

        this.lib := lib;
        this.map := map;
        if this.map /= null then
            this.map.Add_Listener( this );

            for l in this.map.Get_Foreground_Layer..this.map.Get_Background_Layer loop
                this.layered.Append( null );
            end loop;
            this.layered.Set_Center( -this.map.Get_Foreground_Layer );

            for l in this.layered.First..this.layered.Last loop
                this.Update_Layer( l );
            end loop;

            this.blended := new Blended_Info_Array(0..(this.map.Get_Height*this.map.Get_Width-1));
            this.Update_Blended_Info;
        end if;
    end Initialize;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Layer_Added( this : not null access Map_Info_Cache; layer : Integer ) is
    begin
        this.layered.Insert_Symmetric( layer, null );   -- populated next by Update_Layer()
        this.Update_Layer( layer );
        this.Update_Blended_Info;
    end On_Layer_Added;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Layer_Deleted( this : not null access Map_Info_Cache; layer : Integer ) is
    begin
        Delete( this.layered.Reference( layer ) );
        this.layered.Delete( layer );

        this.Update_Blended_Info;
    end On_Layer_Deleted;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Layer_Property_Changed( this  : not null access Map_Info_Cache;
                                         layer : Integer;
                                         name  : String ) is
        pragma Unreferenced( layer );
    begin
        if name = PROP_SOLID then
            -- update blended info because it comes from solid layers
            this.Update_Blended_Info;
        end if;
    end On_Layer_Property_Changed;

    ----------------------------------------------------------------------------

    procedure On_Layers_Swapped( this   : not null access Map_Info_Cache;
                                 layerA : Integer;
                                 layerB : Integer ) is
    begin
        this.layered.Swap( layerA, layerB );
        -- no need to update blended info (it's order independent)
    end On_Layers_Swapped;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Tile_Changed( this     : not null access Map_Info_Cache;
                               layer    : Integer;
                               col, row : Integer ) is
    begin
        this.Update_Layer_At( layer, col, row );
        this.Update_Blended_Info( col, row );
    end On_Tile_Changed;

    ----------------------------------------------------------------------------

    procedure Update_Blended_Info( this : not null access Map_Info_Cache'Class; col, row : Integer ) is
        tile  : A_Tile := null;
        shape : Clip_Type := Passive;
        bits  : Tile_Bits := TILE_INERT;
    begin
        pragma Assert( this.map /= null );
        pragma Assert( this.map.In_Map_Area( col, row ) );

        for l in this.map.Get_Foreground_Layer..this.map.Get_Background_Layer loop
            if this.map.Get_Layer_Type( l ) = Layer_Tiles then
                if this.map.Get_Layer_Property( l, PROP_SOLID ) = Create( True ) then
                    tile := this.layered.Element( l ).all(row * this.map.Get_Width + col);
                    if tile /= null then
                        shape := Dominant( shape, tile.Get_Shape );
                        bits := bits or tile.Get_TypeBits;
                    end if;
                end if;
            end if;
        end loop;

        this.blended(row * this.map.Get_Width + col) := (shape => shape,
                                                         bits  => bits);
    end Update_Blended_Info;

    ----------------------------------------------------------------------------

    procedure Update_Blended_Info( this : not null access Map_Info_Cache'Class ) is
    begin
        for row in 0..this.map.Get_Height-1 loop
            for col in 0..this.map.Get_Width-1 loop
                this.Update_Blended_Info( col, row );
            end loop;
        end loop;
    end Update_Blended_Info;

    ----------------------------------------------------------------------------

    procedure Update_Layer( this : not null access Map_Info_Cache'Class; layer : Integer ) is
    begin
        Delete( this.layered.Reference( layer ) );

        case this.map.Get_Layer_Type( layer ) is
            when Layer_Scenery =>
                this.layered.Replace( layer, new Tiles_Array(0..0) );
                this.Update_Layer_At( layer, 0, 0 );

            when others =>
                this.layered.Replace( layer, new Tiles_Array(0..this.map.Get_Width * this.map.Get_Height - 1) );
                for row in 0..this.map.Get_Height-1 loop
                    for col in 0..this.map.Get_Width-1 loop
                        this.Update_Layer_At( layer, col, row );
                    end loop;
                end loop;

        end case;
    end Update_Layer;

    ----------------------------------------------------------------------------

    procedure Update_Layer_At( this     : not null access Map_Info_Cache'Class;
                               layer    : Integer;
                               col, row : Integer ) is
    begin
        pragma Assert( this.map /= null );
        pragma Assert( layer in this.layered.First..this.layered.Last );
        pragma Assert( this.map.In_Map_Area( col, row ) );
        pragma Assert( this.lib.Not_Null );

        this.layered.Reference( layer ).all(row * this.map.Get_Width + col) := this.lib.Get.Get_Tile( this.map.Get_Tile_Id( layer, col, row ) );
    end Update_Layer_At;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Map_Info_Cache ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

end Maps.Info_Caches;
