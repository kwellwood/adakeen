--
-- Copyright (c) 2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Events;                            use Events;
with Events.Corrals;                    use Events.Corrals;
with Events.Listeners;                  use Events.Listeners;

package Worlds.Controllers is

    type World_Controller is new Limited_Object and
                                 World_Listener and
                                 Event_Listener with private;
    type A_World_Controller is access all World_Controller'Class;

    -- Creates a new world controller that listens for events in 'corral'.
    function Create_World_Controller return A_World_Controller;
    pragma Postcondition( Create_World_Controller'Result /= null );

    -- Returns the world controller's active world.
    function Get_World( this : not null access World_Controller'Class ) return A_World;

    -- Initializes the corral before use. This must be called before the world
    -- can be set.
    procedure Initialize( this : not null access World_Controller'Class; corral : not null A_Corral );

    -- Sets the "modified" state of the world in an editor environment.
    procedure Modified( this : not null access World_Controller'Class; worldModified : Boolean );

    -- Sets the world controller's active world, changing or removing it.
    procedure Set_World( this : not null access World_Controller'Class; world : A_World );

    procedure Delete( this : in out A_World_Controller );
    pragma Postcondition( this = null );

private

    type World_Controller is new Limited_Object and
                                 World_Listener and
                                 Event_Listener with
        record
            corral : A_Corral := null;             -- not owned
            world  : A_World := null;              -- not owned
        end record;

    procedure Delete( this : in out World_Controller );

    procedure Handle_Event( this : access World_Controller;
                            evt  : in out A_Event;
                            resp : out Response_Type );

    -- Queues a series of events, notifying the game view of the world. The
    -- events include World_Loaded, World_Property_Changed, Entity_Created, and
    -- Follow_Entity. This is called by the world when it is given a listener,
    -- and when it has been resized.
    procedure Initialize_World( this : access World_Controller );

    procedure On_Entity_Added( this : access World_Controller; e : not null A_Entity );

    procedure On_Layer_Created( this  : access World_Controller;
                                layer : Integer;
                                data  : not null A_Map );

    procedure On_Layer_Deleted( this : access World_Controller; layer : Integer );

    procedure On_Layer_Property_Changed( this  : access World_Controller;
                                         layer : Integer;
                                         name  : String;
                                         val   : Value'Class );

    procedure On_Layers_Swapped( this   : access World_Controller;
                                 layerA : Integer;
                                 layerB : Integer );

    procedure On_World_Resized( this : access World_Controller );

    procedure On_Tiles_Changed( this  : access World_Controller;
                                tiles : Tile_Change_Lists.Vector );

    procedure On_World_Property_Changed( this : access World_Controller;
                                         name : String;
                                         val  : Value'Class );

end Worlds.Controllers;

