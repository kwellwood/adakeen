--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Finalization;
with Allegro.Displays;                  use Allegro.Displays;
with Assets.Archives;                   use Assets.Archives;
with Maps;                              use Maps;
with Tiles;                             use Tiles;
with Tiles.Atlases;                     use Tiles.Atlases;
with Tiles.Catalogs;                    use Tiles.Catalogs;
with Tiles.Matrices;                    use Tiles.Matrices;
with Tiles.Terrains;                    use Tiles.Terrains;

package Assets.Libraries is

    -- Initializes the tile library assets. This must be called before any
    -- libraries can be loaded. If the application uses a display, provide it so
    -- image files can be converted to the proper color depth on load.
    procedure Initialize( display : A_Allegro_Display );

    -- Finalizes all tile library assets. Existing tile library assets still
    -- need to be unloaded.
    procedure Finalize;

    ----------------------------------------------------------------------------

    -- A Tile_Library is an organized container for images refered to as Tiles.
    -- Not all images in the library have the same dimensions or file format.
    -- A library is stored as a .zip archive containing a number of image files
    -- and a catalog file describing the order and attributes of the tiles.
    --
    -- For performance reasons, tile libraries are reference counted and cached
    -- in memory. The library can be loaded in stages, first including just the
    -- tile attributes information, and then later the tile bitmaps as well.
    -- If the application uses a GUI, the tiles' bitmaps will automatically be
    -- cached to atlas bitmaps (sprite sheets) to improve drawing performance
    -- when drawing many bitmaps from the same library consecutively.
    type Tile_Library is new Asset with private;

    type A_Tile_Library is access all Tile_Library'Class;
    type Library_Ptr is tagged private;

    -- Creates a new, empty tile library. 'name' is the base filename to use
    -- when saving the library, excluding the file extension.
    --
    -- Note that an A_Tile_Library access type is returned instead of a smart
    -- Library_Ptr. The created object will NOT be managed by the asset cache.
    -- The caller owns the Tile_Library and is responsible for calling Delete().
    function Create_Library( name : String ) return A_Tile_Library;

    -- Synchronously loads a tile library by name. 'name' is the base filename
    -- of the library, not including path information or a file extension.
    -- If 'bitmaps' is True, all of the library's bitmaps will be loaded.
    -- Otherwise, just the tile information will be loaded. The asset will not
    -- be loaded on error.
    function Load_Library( name : String; bitmaps : Boolean ) return Library_Ptr;

    -- Returns True if the tile library's bitmaps have been loaded completely.
    function Are_Bitmaps_Loaded( this : not null access Tile_Library'Class ) return Boolean;

    -- Finds the id of the first tile matching 'name' in the library. If 'name'
    -- does not include a file extension, a matching .png file will be searched.
    -- Zero will be returned if no tile matching 'name' can be found.
    function Get_Id( this : not null access Tile_Library'Class;
                     name : String ) return Natural;

    -- Returns the name of the tile library. This is the same as the base
    -- filename that the library was loaded from, without a file extension.
    function Get_Name( this : not null access Tile_Library'Class ) return String;

    -- Returns a reference to a tile matrix by index. Do not modify the matrix,
    -- it belongs to the library!
    function Get_Matrix( this  : not null access Tile_Library'Class;
                         index :  Natural ) return A_Map;

    -- Returns the number of matrices in the library.
    function Get_Matrix_Count( this : not null access Tile_Library'Class ) return Natural;

    -- Returns a reference to a terrain definition by name. Do not modify the
    -- definition.
    function Get_Terrain( this  : not null access Tile_Library'Class;
                          index : Natural ) return A_Terrain_Definition;

    -- Returns the number of terrains in the library.
    function Get_Terrain_Count( this : not null access Tile_Library'Class ) return Natural;

    -- Returns a reference to a tile by id, or 'null' if 'id' doesn't exist. Do
    -- not modify the tile, it belongs to the library.
    function Get_Tile( this : not null access Tile_Library'Class;
                       id   : Natural ) return A_Tile;

    -- Iterates across all unique tiles in the tile library, in order of
    -- ascending ids.
    procedure Iterate_By_Id( this    : not null access Tile_Library'Class;
                             examine : not null access procedure( tile : not null A_Tile ) );

    -- Iterates across all slots in the tile library, beginning at 1. Not every
    -- slot in the library contains a tile, so the 'tile' argument of 'examine'
    -- may be null.
    procedure Iterate_By_Slot( this    : not null access Tile_Library'Class;
                               examine : not null access procedure( slot : Positive;
                                                                    tile : A_Tile ) );

    -- Loads the library's bitmaps from its archive file, if the library was
    -- loaded from an archive. This procedure will block until all the bitmaps
    -- have been loaded. If the library's bitmaps have already been loaded, or
    -- the library was built in memory and not loaded from an archive, it will
    -- return immediately. If another thread is loading the
    -- bitmaps, the caller will be blocked until loading is complete.
    procedure Load_Bitmaps( this : not null access Tile_Library'Class );

    -- Deletes a Tile_Library that is not managed by the cache (created with
    -- Create_Libray). Its data will be unloaded and the objected deleted.
    procedure Delete( this : in out A_Tile_Library );
    pragma Postcondition( this = null );

    -- Tile library file extension.
    LIBRARY_EXTENSION : constant String := "tlz";

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Smart Pointer API

    -- Returns a reference to the pointer's target Tile_Library.
    function Get( this : Library_Ptr ) return access Tile_Library'Class with Inline;

    -- Returns True if the pointer's target is null.
    function Is_Null( this : Library_Ptr ) return Boolean with Inline;

    -- Returns True if the pointer does reference a Tile_Library.
    function Not_Null( this : Library_Ptr ) return Boolean with Inline;

    -- Sets the pointer's target back to null.
    procedure Unset( this : in out Library_Ptr );

    -- Returns True if the pointers reference the same Tile_Library.
    function "="( l, r : Library_Ptr ) return Boolean;

private

    type Tile_Library is new Asset with
        record
            name      : Unbounded_String;        -- name of the library
            file      : A_Archive := null;       -- archive the library was loaded from
            catalog   : A_Catalog := null;       -- catalog containing tile info

            atlas     : A_Atlas := null;         -- paged bitmap atlas (sprite sheet)
            matrices  : Map_Vectors.Vector;      -- array of tile matrices
            terrains  : Terrain_Vectors.Vector;  -- array of terrain definitions

            loadLock    : Mutex;                 -- protects the bitmap loading process
            needBitmaps : Boolean := False;      -- load bitmaps with tile info?
            bmpLoaded   : Boolean := False;      -- all bitmaps have been loaded
        end record;

    procedure Construct( this     : access Tile_Library;
                         assetId  : Value'Class;
                         filePath : String;
                         bitmaps  : Boolean );

    -- Adds a tile to the library. This is used when building the library in
    -- memory. The tile will be consumed.
    --
    -- Raises DUPLICATE_TILE if a tile with the same id already exists.
    -- Note: If an exception is raised, 'tile' will not be consumed. It is then
    -- the responsibility of the caller to delete the tile.
    procedure Add_Tile( this : not null access Tile_Library'Class;
                        tile : in out A_Tile );

    function Load_Data( this : access Tile_Library ) return Boolean;

    procedure Unload_Data( this : access Tile_Library );

    ----------------------------------------------------------------------------

    type Library_Ptr is new Ada.Finalization.Controlled with
        record
            target : A_Tile_Library := null;
        end record;

    overriding
    procedure Adjust( this : in out Library_Ptr );

    overriding
    procedure Finalize( this : in out Library_Ptr );

    procedure Set( this : in out Library_Ptr; target : A_Tile_Library );

    ----------------------------------------------------------------------------

    -- The Allegro Display to use for converting bitmaps to the proper color
    -- depth on load. This is initialized by the Tile Library Manager and will
    -- remain null if the application does not use a display.
    display : A_Allegro_Display := null;

    CATALOG_FILENAME : constant String := "catalog.dat";
    MATRIX_FILENAME  : constant String := "matrices.sco";
    TERRAIN_FILENAME : constant String := "terrains.sco";
    INFO_FILENAME    : constant String := "info.txt";

end Assets.Libraries;
