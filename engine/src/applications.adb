--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Text_IO;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.System;                    use Allegro.System;
with Assets;
with Assets.Archives;
with Assets.Archives.Zip_Archives;
with Debugging;                         use Debugging;
with GNAT.OS_Lib;                       use GNAT.OS_Lib;
with Preferences;
with Support.Paths;                     use Support.Paths;

package body Applications is

    instance : A_Application := null;

    --==========================================================================

    function Get_Application return A_Application is (instance);

    ----------------------------------------------------------------------------

    procedure Terminate_Application( error : String ) is
    begin
        if instance /= null then
            instance.Show_Error( "Application Error: " & error );
            instance.Kill;
        else
            -- the application object hasn't been created yet or isn't being
            -- used.
            Ada.Text_IO.Put_Line( "Application Error: " & error );
            GNAT.OS_Lib.OS_Exit( 1 );
        end if;
    end Terminate_Application;

    --==========================================================================

    not overriding
    procedure Construct( this    : access Application;
                         company : String;
                         name    : String;
                         userDir : String ) is
    begin
        Limited_Object(this.all).Construct;
        this.company    := To_Unbounded_String( company );
        this.name       := To_Unbounded_String( name );
        this.userDir    := To_Unbounded_String( Normalize_Pathname( (if userDir /= "" then userDir else ".") ) & Slash );
        this.configFile := To_Unbounded_String( Add_Extension( name, "cfg", force => True ) );

        -- try to make sure the config directory exists
        if not Is_Directory( To_String( this.userDir ) ) then
            if not Make_Dir( To_String( this.userDir ) ) then
                Dbg( "Cannot create config directory: " & To_String( this.userDir ), D_APP, Error );
            end if;
        end if;
    end Construct;

    ----------------------------------------------------------------------------

    not overriding
    function Initialize( this : access Application ) return Boolean is
    begin
        Enable_Debug_Channels( D_INIT );

        Assets.Archives.Register_Format( "zip", Assets.Archives.Zip_Archives.Load_Zip'Access );

        -- Initialize Allegro
        if not Al_Initialize then
            this.Show_Error( "Error initializing Allegro" );
            Dbg( "Al_Initialize failed", D_APP, Error );
            raise INIT_EXCEPTION;
        end if;

        -- Initialize Preferences
        if GNAT.OS_Lib.Is_Regular_File( To_String( this.configFile ) ) then
            -- a config file exists in the working directory; use that one
            pragma Debug( Dbg( "Using config '" & To_String( this.configFile ) & "'", D_PREFS, Info ) );
            Preferences.Initialize( To_String( this.configFile ) );
        else
            -- use the config file in the user's directory, or write one there
            -- later if preferences change.
            pragma Debug( Dbg( "Using config '" &
                               To_String( this.userDir ) &
                               To_String( this.configFile ) & "'",
                               D_PREFS, Info ) );
            Preferences.Initialize( To_String( this.userDir ) &
                                    To_String( this.configFile ) );
        end if;

        if not Al_Init_Image_Addon then
            -- failed to initialize Allegro image loaders
            Dbg( "Al_Init_Image_Addon failed", D_APP, Error );
            this.Show_Error( "Error initializing Allegro image addon." );
            raise INIT_EXCEPTION with "Error initializing Allegro image addon";
        end if;

        Disable_Debug_Channels( D_INIT );
        return True;
    exception
        when e : others =>
            Dbg( "Application.Initialize Exception: ...", D_APP, Error );
            Dbg( e, D_APP, Error );
            Disable_Debug_Channels( D_INIT );

            this.Finalize;
            return False;
    end Initialize;

    ----------------------------------------------------------------------------

    function Run( this : not null access Application'Class ) return Integer is
        returnCode : Integer := NO_ERROR;
    begin
        pragma Assert( instance = null );
        instance := A_Application(this);

        pragma Debug( Dbg( "** Initializing application **", D_APP, Info ) );
        if instance.Initialize then

            pragma Debug( Dbg( "** Running application **", D_APP, Info ) );
            begin
                returnCode := instance.Main;
                pragma Debug( Dbg( "** Application run complete **", D_APP, Info ) );
            exception
                when e : others =>
                    returnCode := ERROR_UNEXPECTED_EXCEPTION;
                    pragma Debug( Dbg( "** Application run failed **", D_APP, Error ) );
                    pragma Debug( Dbg( "Exception in Application.Main: ...", D_APP, Error ) );
                    pragma Debug( Dbg( e, D_APP, Error ) );
            end;

            pragma Debug( Dbg( "** Finalizing application **", D_APP, Info ) );
            begin
                instance.Finalize;
            exception
                when e : others =>
                    -- don't show secondary exceptions in Finalize() if Main()
                    -- failed because the application may have been left in a
                    -- state that can't shutdown cleanly.
                    if returnCode /= ERROR_UNEXPECTED_EXCEPTION then
                        pragma Debug( Dbg( "Exception in Application.Finalize: ...", D_APP, Error ) );
                        pragma Debug( Dbg( e, D_APP, Error ) );
                        if returnCode = NO_ERROR then
                            returnCode := ERROR_UNEXPECTED_EXCEPTION;
                        end if;
                    end if;
            end;

        end if;
        return returnCode;
    end Run;

    ----------------------------------------------------------------------------

    not overriding
    procedure Finalize( this : access Application ) is
        pragma Unreferenced( this );
    begin
        Assets.Finalize;           -- unload all remaining assets
        Preferences.Finalize;

        if Al_Is_System_Installed then
            pragma Debug( Dbg( "Shutting down Allegro", D_APP, Info ) );
            Al_Uninstall_System;
        end if;
        pragma Debug( Dbg( "Application finalized", D_APP, Info ) );
    end Finalize;

    ----------------------------------------------------------------------------

    function Get_Company( this : not null access Application'Class ) return String is (To_String( this.company ));

    ----------------------------------------------------------------------------

    function Get_Name( this : not null access Application'Class ) return String is (To_String( this.name ));

    ----------------------------------------------------------------------------

    function Get_User_Directory( this : not null access Application'Class ) return String is (To_String( this.userDir ));

    ----------------------------------------------------------------------------

    not overriding
    procedure Show_Error( this : access Application; text : String ) is
        pragma Unreferenced( this );
    begin
        Ada.Text_IO.Put_Line( Ada.Text_IO.Standard_Error, "Error: " & text );
    end Show_Error;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Application ) is
    begin
        if this = instance then
            instance := null;
        end if;

        pragma Debug( Dbg( "Deleting application", D_APP, Info ) );
        Delete( A_Limited_Object(this) );
    end Delete;

end Applications;
