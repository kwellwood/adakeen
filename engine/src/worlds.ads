--
-- Copyright (c) 2008-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers;                    use Ada.Containers;
with Ada.Containers.Ordered_Maps;
with Ada.Containers.Vectors;
with Ada.Real_Time;                     use Ada.Real_Time;
with Ada.Streams;                       use Ada.Streams;
with Ada.Unchecked_Deallocation;
with Assets.Libraries;                  use Assets.Libraries;
with Clipping;                          use Clipping;
with Entities;                          use Entities;
with Entities.Factory;
with Hashed_Strings;                    use Hashed_Strings;
with Maps;                              use Maps;
with Maps.Animations;                   use Maps.Animations;
with Maps.Info_Caches;                  use Maps.Info_Caches;
with Object_Ids;                        use Object_Ids;
with Objects;                           use Objects;
with Processes;                         use Processes;
with Scribble.Namespaces;               use Scribble.Namespaces;
with Tiles;                             use Tiles;
with Values;                            use Values;
with Values.Lists;
with Values.Maps;                       use Values.Maps;
with Values.Tagged_Ids;                 use Values.Tagged_Ids;
with Vector_Math;                       use Vector_Math;

package Worlds is

    -- Maps Entity_Ids to Entity objects.
    package Entity_Map is new Ada.Containers.Ordered_Maps( Entity_Id, Entities.A_Entity, "<", Entities."=" );
    type A_Entity_Map is access all Entity_Map.Map;

    procedure Delete is new Ada.Unchecked_Deallocation( Entity_Map.Map, A_Entity_Map );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Tile_Change is
        record
            layer : Integer :=  0;
            col   : Integer := -1;
            row   : Integer := -1;
            id    : Natural :=  0;
        end record;

    package Tile_Change_Lists is new Ada.Containers.Vectors( Positive, Tile_Change );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- A World_Listener receives notifications when the World object changes.
    -- One listener at a time can be registered with a World object.
    type World_Listener is limited interface;
    type A_World_Listener is access all World_Listener'Class;

    -- Called when the listener is first assigned to the world.
    procedure Initialize_World( this : access World_Listener ) is null;

    -- Called when a new entity 'e' is added to the world.
    procedure On_Entity_Added( this : access World_Listener; e : not null A_Entity ) is null;

    -- Called when a new layer (index 'layer') is added to the world's map.
    -- Layer 0 of Map 'data' contains the tile contents of the new layer.
    procedure On_Layer_Created( this  : access World_Listener;
                                layer : Integer;
                                data  : not null A_Map ) is null;

    -- Called when layer 'layer' is removed from the world's map.
    procedure On_Layer_Deleted( this : access World_Listener; layer : Integer ) is null;

    -- Called when the value of property 'name' of layer index 'layer' changes
    -- to value 'val'.
    procedure On_Layer_Property_Changed( this  : access World_Listener;
                                         layer : Integer;
                                         name  : String;
                                         val   : Value'Class ) is null;

    -- Called when the layers at indices 'layerA' and 'layerB' are swapped.
    procedure On_Layers_Swapped( this   : access World_Listener;
                                 layerA : Integer;
                                 layerB : Integer ) is null;

    -- Called when the world's map is resized.
    procedure On_World_Resized( this : access World_Listener ) is null;

    -- Called when a group of tiles 'tiles' have changed.
    procedure On_Tiles_Changed( this  : access World_Listener;
                                tiles : Tile_Change_Lists.Vector ) is null;

    -- Called when the value of world property 'name' changes to value 'val'.
    procedure On_World_Property_Changed( this : access World_Listener;
                                         name : String;
                                         val  : Value'Class ) is null;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- This message is dispatched to all entities in a world, when it is loaded
    -- for gameplay.
    --
    -- "WorldLoaded" {}
    MSG_WorldLoaded   : constant Hashed_String := To_Hashed_String( "WorldLoaded" );

    -- This message is dispatched to all entities in a world when it is loaded
    -- for gameplay, just after "WorldLoaded", except when in editing mode.
    --
    -- "PlayStarted" {}
    MSG_PlayStarted   : constant Hashed_String := To_Hashed_String( "PlayStarted" );

    -- This message is dispatched to all entities in a world just before it is
    -- unloaded, just before "WorldUnloaded", except when in editing mode.
    --
    -- "PlayStopped" {}
    MSG_PlayStopped   : constant Hashed_String := To_Hashed_String( "PlayStopped" );

    -- This message is dispatched to all entities in a world, after "PlayStopped",
    -- just before it is unloaded.
    --
    -- "WorldUnloaded" {}
    MSG_WorldUnloaded : constant Hashed_String := To_Hashed_String( "WorldUnloaded" );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- A World is an object that contains a layered tile map and entities. It
    -- can be created, loaded, saved, and handles the animation of its tiles
    -- during gameplay as a Process.
    --
    -- A World is primarily a data object, but it can have a World_Listener
    -- responsible for requesting changes and handling change notifications. The
    -- game Session has one World active at a time. The active World can receive
    -- and dispatch events via the World_Controller, it updates its animations
    -- as a Process, and its entities are controlled by the Entity_System.
    type World_Object is new Object and
                             Process and
                             Scribble_Namespace with private;
    type A_World is access all World_Object'Class;

    -- Creates a new, empty world. The tile map has no layers initially; they
    -- will need to be created and configured as necessary. 'width' and 'height'
    -- are the dimensions of the world in tiles. 'tileWidth' is the size of one
    -- tile in world units. Returns null on error (eg: Can't open 'library'.)
    function Create_World( width,
                           height    : Positive;
                           tileWidth : Positive;
                           library   : String ) return A_World;

    -- Loads a world from disk. The standard resource paths will be searched in
    -- order, if 'name' does not contain a path. If 'name' does not contain a
    -- file extension, the default file extension will be used. An exception
    -- will be raised on error.
    function Load_World( name : String ) return A_World;
    pragma Postcondition( Load_World'Result /= null );

    -- Loads a world from a stream 'stream', from file 'path'. The path is not
    -- used except to set the "filename" and "filepath" properties. Returns a
    -- new World instance on success, or raises an error on failure.
    function Load_World( stream : access Root_Stream_Type'Class; path : String ) return A_World;
    pragma Postcondition( Load_World'Result /= null );

    -- Adds entity 'e' to the world, consuming it.
    procedure Add_Entity( this : not null access World_Object'Class;
                          e    : in out A_Entity );
    pragma Precondition( e /= null );
    pragma Postcondition( e = null );

    -- Inserts a new layer into the world's map of type 'layerType'. The layer
    -- will be inserted directly in front of layer 'layer'. If 'layer' is
    -- greater than the current background layer, the new layer will become the
    -- new background. If the map doesn't contain any layers yet, the new layer
    -- will be the middleground, layer 0. 'properties' overrides the default
    -- properties for the layer type. If no "title" property is defined, one
    -- will be auto-generated. 'layerData' can be optionally used to fill in the
    -- layer. If provided, the data from layer 0 will be used to populate the
    -- new layer. Both 'properties' and 'layerData' will be copied.
    procedure Add_Layer( this       : not null access World_Object'Class;
                         layer      : Integer;
                         layerType  : Layer_Type;
                         properties : Map_Value := Create_Map.Map;
                         layerData  : A_Map := null );

    -- Deletes layer 'layer' from the map by index, if it exists. The
    -- middleground (layer 0) cannot be deleted. If the layer does not exist,
    -- nothing with happen.
    procedure Delete_Layer( this  : not null access World_Object'Class;
                            layer : Integer );

    -- Finds and returns an entity by name. Null will be returned if an entity
    -- can't be found.
    function Find_Entity_By_Name( this : not null access World_Object'Class;
                                  name : String ) return A_Entity;

    -- Returns the index of the background layer.
    function Get_Background_Layer( this : not null access World_Object'Class ) return Integer;

    -- Returns the Z depth of the background layer in the map.
    function Get_Background_Z( this : not null access World_Object'Class ) return Float;

    -- Returns the world's area in world units (pixels).
    function Get_Area( this : not null access World_Object'Class ) return Rectangle;

    -- Returns the clip type of the tile located at 'x','y' in world units. The
    -- clip type represents the type of clipping that should be applied to
    -- entities coming into contact with the tile. It is computed as the
    -- dominant clip type attribute among the tiles at 'x','y' for each solid
    -- layer. The top left of the world is at (0, 0).
    function Get_Clip_Type( this : not null access World_Object'Class;
                            x, y : Float ) return Clip_Type;

    -- Returns a reference to the world's entity container- a map of Entity_Id
    -- to A_Entity references. Do NOT delete the map.
    function Get_Entities( this : not null access World_Object'Class ) return A_Entity_Map;

    -- Returns the index of the foreground layer.
    function Get_Foreground_Layer( this : not null access World_Object'Class ) return Integer;

    -- Returns the Z depth of the foreground layer in the map.
    function Get_Foreground_Z( this : not null access World_Object'Class ) return Float;

    -- Returns the map height in world units (pixels).
    function Get_Height( this : not null access World_Object'Class ) return Positive;

    -- Returns the number of layers in the map.
    function Get_Layer_Count( this : not null access World_Object'Class ) return Natural;

    -- Returns the Z depth of layer 'layer'.
    function Get_Layer_Z( this  : not null access World_Object'Class;
                          layer : Integer ) return Float;

    -- Returns the value of property 'name' in map layer 'layer', or Null if the
    -- layer or property is undefined. No copy of the value is made.
    function Get_Layer_Property( this  : not null access World_Object'Class;
                                 layer : Integer;
                                 name  : String ) return Value;

    -- Returns the world's reference to its tile library. Owned by the World.
    function Get_Library( this : not null access World_Object'Class ) return Library_Ptr;

    -- Returns the number of columns in the map / width of the world in tiles.
    function Get_Map_Columns( this : not null access World_Object'Class ) return Positive;

    -- Returns the number of rows in the map / height of the world in tiles.
    function Get_Map_Rows( this : not null access World_Object'Class ) return Positive;

    -- Returns the value of world property 'name' (without copying).
    function Get_Namespace_Name( this : access World_Object;
                                 name : String ) return Value;

    -- Returns a reference to world property 'name', as a Scribble namespace.
    function Get_Namespace_Ref( this : access World_Object;
                                name : String ) return Value;
    pragma Postcondition( Get_Namespace_Ref'Result.Is_Ref or else
                          Get_Namespace_Ref'Result.Is_Null );

    -- Returns the 'name' property of the world or Null if the specified
    -- property has not been defined. The returned value is not a copy; the
    -- caller is responsible for copying it as necessary.
    function Get_Property( this : not null access World_Object'Class;
                           name : String ) return Value;

    -- Returns the tile located at 'col,row' of map layer 'layer'. null will be
    -- returned if the location is not on the map, or if the tile id at that
    -- location is not found in the world's tile library.
    function Get_Tile( this     : not null access World_Object'Class;
                       layer    : Integer;
                       col, row : Integer ) return A_Tile;

    -- Same as above, but location 'x','y' is in world units.
    function Get_Tile( this  : not null access World_Object'Class;
                       layer : Integer;
                       x, y  : Float ) return A_Tile;

    -- Returns the value of tile attribute 'name', on a tile located at
    -- 'col','row' (in tile units). The tile in the map layer will be checked
    -- for a 'name' attribute, in order of background to foreground. The top
    -- left of the world is at (0, 0).
    function Get_Tile_Attribute( this     : not null access World_Object'Class;
                                 col, row : Integer;
                                 name     : String ) return Value;

    -- Same as above, but location 'x','y' is in world units.
    function Get_Tile_Attribute( this : not null access World_Object'Class;
                                 x, y : Float;
                                 name : String ) return Value;

    -- Returns the clip type of the tile located at 'col','row' in tile units.
    -- The clip type represents the type of clipping that should be applied to
    -- entities coming into contact with the tile. It is computed as the
    -- dominant clip type attribute among the tiles at 'col','row' for each
    -- solid layer. The top left of the world is at (0, 0).
    function Get_Tile_Clip_Type( this     : not null access World_Object'Class;
                                 col, row : Integer ) return Clip_Type;

    -- Same as above, but location 'x','y' is in world units.
    function Get_Tile_Clip_Type( this : not null access World_Object'Class;
                                 x, y : Float ) return Clip_Type;

    -- Returns the id of the tile located at 'col','row' in layer 'layer'.
    -- 'col','row' are in tile coordinates. The top left of the world is at
    -- (0, 0).
    function Get_Tile_Id( this     : not null access World_Object'Class;
                          layer    : Integer;
                          col, row : Integer ) return Natural;

    -- Same as above, but location 'x','y' is in world units.
    function Get_Tile_Id( this  : not null access World_Object'Class;
                          layer : Integer;
                          x, y  : Float ) return Natural;

    -- Returns the width/height of a single tile in world units (pixels).
    function Get_Tile_Width( this : not null access World_Object'Class ) return Positive;

    -- Returns the map width in world units (pixels).
    function Get_Width( this : not null access World_Object'Class) return Positive;

    -- Resizes the map to 'width,height' tiles, placing 'col','row' in the
    -- current map at the origin of the new resized map. An exception is raised
    -- on error (e.g. exceeding maximum world dimensions.)
    procedure Resize( this          : not null access World_Object'Class;
                      col, row      : Integer;
                      width, height : Positive );

    -- Writes the world in its current state to the file path 'path'. Set
    -- 'overwrite' to True to overwrite the file, if it already exists. A
    -- WRITE_EXCEPTION will be raised on error. The world's "filename" and
    -- "path" properties will be updated according to the path it is saved as.
    procedure Save( this      : not null access World_Object'Class;
                    path      : String;
                    overwrite : Boolean );

    -- Sets property 'name' to 'val' of map layer 'layer' (by copy).
    procedure Set_Layer_Property( this  : not null access World_Object'Class;
                                  layer : Integer;
                                  name  : String;
                                  val   : Value'Class );

    -- Sets or clears the world's World_Listener object to receive notifications
    -- when the world changes. The listener's Initialize() method will be called
    -- immediately.
    procedure Set_Listener( this     : not null access World_Object'Class;
                            listener : A_World_Listener );

    -- Sets the value of property 'name' to 'val' (by copy).
    procedure Set_Property( this : not null access World_Object'Class;
                            name : String;
                            val  : Value'Class );

    -- Sets the tile at 'col','row' in layer 'layer', by tile id. 'col','row'
    -- are in tile coordinates. The top left of the world is at (0, 0).
    procedure Set_Tile( this     : not null access World_Object'Class;
                        layer    : Integer;
                        col, row : Integer;
                        id       : Natural );

    -- Sets the tile at 'x','y' in layer 'layer', by tile id. 'x','y' are in
    -- world coordinates. The top left of the world is at (0, 0).
    procedure Set_Tile( this  : not null access World_Object'Class;
                        layer : Integer;
                        x, y  : Float;
                        id    : Natural );

    -- Sets a series of tiles listed in 'tiles'.
    procedure Set_Tiles( this  : not null access World_Object'Class;
                         tiles : Tile_Change_Lists.Vector );

    -- Swaps map layers 'layer' and 'otherLayer'.
    procedure Swap_Layers( this       : not null access World_Object'Class;
                           layer      : Integer;
                           otherLayer : Integer );

    -- Returns the width/height a tile in world units (pixels).
    function Tile_Width( this : not null access World_Object'Class ) return Positive;

    -- Deletes the World.
    procedure Delete( this : in out A_World );
    pragma Postcondition( this = null );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Returns the file extension for World files, without a leading dot.
    function World_Extension return String is ("world");
    pragma Postcondition( World_Extension'Result'Length > 0 );

    IDENTIFIER    : constant String := "galaxy";
    READ_VERSION  : constant Natural := 61 * 100 + Values.STREAM_VERSION_READ;
    WRITE_VERSION : constant Natural := 61 * 100 + Values.STREAM_VERSION_WRITE;

    FILE_NOT_FOUND,
    READ_EXCEPTION,
    WRITE_EXCEPTION : exception;

private

    type World_Object is new Object and
                             Process and
                             Scribble_Namespace with
        record
            -- the following fields are streamed --
            properties : Map_Value := Create_Map.Map;
            map        : A_Map := null;
            anmCache   : A_Animation_Cache := null;
            entities   : A_Entity_Map := new Entity_Map.Map;
            age        : Time_Span := Time_Span_Zero;

            -- the following fields are NOT streamed --
            lib        : Library_Ptr;
            mapInfo    : A_Map_Info_Cache := null;
            tileWidth  : Positive := 1;
            listener   : A_World_Listener := null;
        end record;

    --
    -- Standard World Properties:
    --
    -- Name           Type     R/W  Persisted  Description
    -- ----           ----     ---  ---------  -----------
    -- filename       string   RW   No         The filename of the world's file (not persisted)
    -- filepath       string   RW   No         The full path of the world's file (not persisted)
    -- library        string   R    Yes        The name of the world's tile library
    -- tileWidth      number   R    Yes        Width of a tile in world units
    -- player         id       RW   Yes        The id of the entity that is controlled by the view
    -- preserve       boolean  RW   Yes        Preserve world state over the game session?
    -- ambientLight   string   RW   Yes        The ambient light color of the world
    -- animated       boolean  RW   No         Tile animations are enabled? (not persisted)
    -- age            number   R    Yes*       Elapsed gameplay time since world creation (ms)
    --                                         This is persisted as a Time_Span, not in world.properties
    --

    -- raises COPY_NOT_ALLOWED
    procedure Adjust( this : access World_Object );

    procedure Construct( this : access World_Object );

    procedure Construct( this      : access World_Object;
                         width     : Positive;
                         height    : Positive;
                         tileWidth : Positive;
                         lib       : Library_Ptr );

    procedure Delete( this : in out World_Object );

    -- Generates a unique name for entity 'e', based on its template and an
    -- incrementing number.
    procedure Generate_Entity_Name( this : not null access World_Object'Class; e : A_Entity );

    -- Returns the name of the World as a Process.
    function Get_Process_Name( this : access World_Object ) return String;
    pragma Postcondition( Get_Process_Name'Result'Length > 0 );

    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out World_Object );
    for World_Object'Read use Object_Read;

    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : World_Object );
    for World_Object'Write use Object_Write;

    -- Updates the animated tiles in the map.
    procedure Tick( this : access World_Object; time : Tick_Time );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Verifies a file format header before reading the World representation.
    -- If the header is invalid, READ_EXCEPTION will be raised. An exception
    -- will also be raised on a file format / streaming error.
    function A_World_Input( stream : access Root_Stream_Type'Class ) return A_World;
    pragma Postcondition( A_World_Input'Result /= null );
    for A_World'Input use A_World_Input;

    -- Writes a file format header and then the World representation.
    procedure A_World_Output( stream : access Root_Stream_Type'Class; world : A_World );
    for A_World'Output use A_World_Output;

end Worlds;
