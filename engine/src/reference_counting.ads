--
-- Copyright (c) 2012-2013 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Finalization;                  use Ada.Finalization;
with Interfaces;                        use Interfaces;

package Reference_Counting is

    pragma Preelaborate;

    type Reference_Counted is abstract tagged private;

    type A_Reference_Counted is access all Reference_Counted'Class;

    not overriding
    procedure Delete( this : in out Reference_Counted ) is null;

    ----------------------------------------------------------------------------

    generic
        type Encapsulated is abstract new Reference_Counted with private;
    package Smart_Pointers is

        type A_Encapsulated is access all Encapsulated'Class;

        type Ref is tagged private;

        function Get( this : Ref ) return A_Encapsulated with Inline;

        function Get_Refcount( this : Ref ) return Natural with Inline;

        procedure Set( this : in out Ref; target : Encapsulated'Class ) with Inline;
        procedure Set( this : in out Ref; target : access Encapsulated'Class ) with Inline;

        -- Returns True if l and r point to equivalent objects. This is the same
        -- as calling l.Get.all = r.Get.all, but it handles null references
        -- safely.
        overriding
        function "="( l, r : Ref ) return Boolean with Inline;

        Nul      : constant Ref;
        Null_Ref : constant Ref;

    private

        type Ref is new Ada.Finalization.Controlled with
            record
                target : A_Reference_Counted := null;
            end record;

        overriding
        procedure Adjust( this : in out Ref );

        overriding
        procedure Finalize( this : in out Ref );

        Nul      : constant Ref := (Controlled with target => null);
        Null_Ref : constant Ref := (Controlled with target => null);

    end Smart_Pointers;

    ----------------------------------------------------------------------------

    package Atomic is

        function Add( ptr : access Integer_32;
                      val : Integer_32 ) return Integer_32;

    end Atomic;

private

      type Reference_Counted is abstract tagged
          record
              refs : aliased Integer_32 := 0;
          end record;

end Reference_Counting;
