--
-- Copyright (c) 2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Numerics;                      use Ada.Numerics;
with Ada.Unchecked_Deallocation;
--with Debugging;                         use Debugging;
with Resources.Scribble;                use Resources.Scribble;
with Values.Lists;                      use Values.Lists;

package body Tiles.Terrains is

    -- Converts degrees to radians.
    --
    -- 0/360 degrees: Up
    --    90 degrees: Left
    --   180 degrees: Down
    --   270 degrees: Right
    function Rad( degrees : Float ) return Float is ((degrees - 180.0) * Pi / 180.0);

    --==========================================================================

    function Copy( src : A_Terrain_Definition ) return A_Terrain_Definition is
        result    : A_Terrain_Definition := null;
        newEdge   : A_Terrain_Edge;
        newCorner : A_Terrain_Corner;
    begin
        if src /= null then
            result := new Terrain_Definition;
            result.name := src.name;

            for edge of src.edges loop
                newEdge := new Terrain_Edge'(angle => edge.angle, others => <>);

                for slice of edge.slices loop
                    newEdge.slices.Append( new Terrain_Slice'(slice.all) );
                end loop;

                result.edges.Append( newEdge );
            end loop;

            for corner of src.corners loop
                newCorner := new Terrain_Corner'(angle1 => corner.angle1,
                                                 angle2 => corner.angle2,
                                                 others => <>);

                for slice of corner.slices loop
                    newCorner.slices.Append( new Terrain_Slice'(slice.all) );
                end loop;

                result.corners.Append( newCorner );
            end loop;
        end if;
        return result;
    end Copy;

    ----------------------------------------------------------------------------

    procedure Delete( t : in out A_Terrain_Definition ) is
        procedure Free is new Ada.Unchecked_Deallocation( Terrain_Definition, A_Terrain_Definition );
        procedure Free is new Ada.Unchecked_Deallocation( Terrain_Edge, A_Terrain_Edge );
        procedure Free is new Ada.Unchecked_Deallocation( Terrain_Corner, A_Terrain_Corner );
        procedure Free is new Ada.Unchecked_Deallocation( Terrain_Slice, A_Terrain_Slice );
    begin
        if t /= null then
            for edge of t.edges loop
                for slice of edge.slices loop
                    Free( slice );
                end loop;
                Free( edge );
            end loop;

            for corner of t.corners loop
                for slice of corner.slices loop
                    Free( slice );
                end loop;
                Free( corner );
            end loop;

            Free( t );
        end if;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Delete_All( terrains : in out Terrain_Vectors.Vector ) is
    begin
        for t of terrains loop
            Delete( t );
        end loop;
        terrains.Clear;
    end Delete_All;

    ----------------------------------------------------------------------------

    procedure Load_Terrains( res      : not null A_Resource_File;
                             terrains : in out Terrain_Vectors.Vector ) is

        ------------------------------------------------------------------------

        procedure Load_Terrain( name : String; icon : Natural; val : Value ) is
            def    : A_Terrain_Definition;
            items  : List_Value;
            i      : Integer;
            item   : Value;
            angle  : Float;
            slice  : List_Value;
            tile   : List_Value;
            edge   : A_Terrain_Edge;
            corner : A_Terrain_Corner;
            tslice : A_Terrain_Slice;
        begin
            def := new Terrain_Definition;
            def.name := To_Unbounded_String( name );
            def.icon := icon;

            items := val.Lst;
            i := 1;
            while i <= items.Length loop
                item := items.Get( i );
                i := i + 1;

                -- expect an angle for the next path edge, or the first of two
                -- angles in a corner.
                if item.Is_Number then
                    angle := item.To_Float;

                    item := (if i <= items.Length then items.Get( i ) else Null_Value);
                    i := i + 1;

                    -- expect a terrain slice, or the second of two angles in a
                    -- corner.
                    if item.Is_List then
                        edge := new Terrain_Edge;
                        edge.angle := Rad( angle );

                        -- one or more terrain slices
                        while item.Is_List loop
                            slice := item.Lst;
                            tslice := new Terrain_Slice(1..slice.Length);

                            -- add all tiles to the slice
                            for j in 1..slice.Length loop
                                tile := slice.Get( j ).Lst;

                                -- add the tile to the slice
                                tslice(j).layer   := tile.Get_Int( 1 );
                                tslice(j).offsetX := tile.Get_Int( 2 );
                                tslice(j).tileId  := tile.Get_Int( 3 );
                            end loop;

                            -- sort by layer, background to foreground
                            Sort_Terrain_Tiles( tslice.all );
                            edge.slices.Append( tslice );

                            item := (if i <= items.Length then items.Get( i ) else Null_Value);
                            i := i + 1;
                        end loop;

                        -- backup- don't skip the item following the last slice
                        i := i - 1;

                        -- insert the edge into the definition, ordered by
                        -- ascending angle.
                        if def.edges.Is_Empty or else edge.angle >= def.edges.Last_Element.angle then
                            def.edges.Append( edge );
                        else
                            for j in 1..Integer(def.edges.Length) loop
                                if edge.angle < def.edges.Element( j ).angle then
                                    def.edges.Insert( j, edge );
                                    exit;
                                end if;
                            end loop;
                        end if;

                    elsif item.Is_Number then
                        -- corner
                        corner := new Terrain_Corner;
                        corner.angle1 := Rad( angle );
                        corner.angle2 := Rad( item.To_Float );

                        item := (if i <= items.Length then items.Get( i ) else Null_Value);
                        i := i + 1;

                        -- expect a terrain corner
                        if item.Is_List then

                            -- one or more terrain corner slices
                            while item.Is_List loop
                                slice := item.Lst;
                                tslice := new Terrain_Slice(1..slice.Length);

                                -- add all tiles to the corner
                                for j in 1..slice.Length loop
                                    tile := slice.Get( j ).Lst;

                                    -- add the tile to the slice
                                   tslice(j).layer   := tile.Get_Int( 1 );
                                   tslice(j).offsetX := tile.Get_Int( 2 );
                                   tslice(j).offsetY := tile.Get_Int( 3 );
                                   tslice(j).tileId  := tile.Get_Int( 4 );
                                end loop;

                                -- sort by layer, background to foreground
                                Sort_Terrain_Tiles( tslice.all );
                                corner.slices.Append( tslice );

                                item := (if i <= items.Length then items.Get( i ) else Null_Value);
                                i := i + 1;
                            end loop;

                            -- backup- don't skip the item following the last slice
                            i := i - 1;

                            def.corners.Append( corner );
                        end if;
                    end if;
                end if;
            end loop;

            if not def.edges.Is_Empty then
                terrains.Append( def );
            end if;
        end Load_Terrain;

        ------------------------------------------------------------------------

        val      : Value;
        contents : List_Value;
    begin
        Delete_All( terrains );

        if Load_Scribble( val, res ) and then val.Is_List then
            contents := val.Lst;
            for i in 1..contents.Length loop
                Load_Terrain( contents.Get( i ).Map.Get_String( "name" ),
                              contents.Get( i ).Map.Get_Int( "icon" ),
                              contents.Get( i ).Map.Get( "definition" ) );
            end loop;
        end if;
    end Load_Terrains;

end Tiles.Terrains;
