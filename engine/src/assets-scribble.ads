--
-- Copyright (c) 2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Assets.Scribble is

    -- A Scribble_Asset contains a single compiled Scribble value, compiled from
    -- source code or loaded from a Scribble Compiled Object (.sco) file. Source
    -- code will be preferred if both formats are present.
    type Scribble_Asset is new Asset with private;
    type A_Scribble_Asset is access all Scribble_Asset'Class;

    function Load_Value( filePath : String; group : String ) return A_Scribble_Asset;
    pragma Postcondition( Load_Value'Result /= null );

    -- Returns a reference to the internal Value, not a copy. DO NOT modify it!
    -- If the asset is not loaded, an Error describing the most recent failed
    -- attempt will be returned.
    function Get_Value( this : not null access Scribble_Asset'Class ) return Value;

    procedure Delete( this : in out A_Scribble_Asset );
    pragma Postcondition( this = null );

private

    type Scribble_Asset is new Asset with
        record
            val : Value;
        end record;

    procedure Construct( this     : access Scribble_Asset;
                         assetId  : Value'Class;
                         filePath : String;
                         group    : String );

    function Load_Data( this : access Scribble_Asset ) return Boolean;

    procedure Unload_Data( this : access Scribble_Asset );

end Assets.Scribble;
