--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Version is

    -- Returns the release version number, which has three components:
    -- major.minor.bugfix
    function Release return String with Inline_Always;

    -- Returns the copyright date as a range, from the year the software
    -- development began to the year the executable was built.
    function Copyright_Year return String;

    -- Returns True if the executable was compiled with the DEBUG symbol defined
    -- as True, otherwise False.
    function Is_Debug return Boolean with Inline_Always;

end Version;
