--
-- Copyright (c) 2015-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Assertions;                    use Ada.Assertions;
with Ada.Unchecked_Deallocation;
--with Debugging;                         use Debugging;
with Vector_Math;                       use Vector_Math;

package body Quad_Trees is

    -- Compares by .x1 then .y1 then .x2 then .y2.
    function "<"( l, r : Segment ) return Boolean is
    begin
        if l.x1 < r.x1 then
            return True;
        elsif l.x1 > r.x1 then
            return False;
        end if;

        if l.y1 < r.y1 then
            return True;
        elsif l.y1 > r.y1 then
            return False;
        end if;

        if l.x2 < r.x2 then
            return True;
        elsif l.x2 > r.x2 then
            return False;
        end if;

        return l.y2 < r.y2;
    end "<";

    function "<"( l, r : Element ) return Boolean is (l.seg < r.seg);

    function "="( l, r : Element ) return Boolean is (l.seg = r.seg);

    ----------------------------------------------------------------------------

    function Intersect_Seg_Seg( a, b : Segment ) return Boolean is

        ------------------------------------------------------------------------

        -- Detects overlap between two co-linear segments. Treats the segments
        -- as the diagonals of two axis-aligned rectangles, then checks for the
        -- intersection between the rectangles.
        function Colinear_Overlap( a, b : Segment ) return Boolean is
            -- rectangle bounding segment A
            min1 : constant Vec2 := (Float'Min( a.x1, a.x2 ), Float'Min( a.y1, a.y2 ));
            max1 : constant Vec2 := (Float'Max( a.x1, a.x2 ), Float'Max( a.y1, a.y2 ));

                -- rectangle bounding segment B
            min2 : constant Vec2 := (Float'Min( b.x1, b.x2 ), Float'Min( b.y1, b.y2 ));
            max2 : constant Vec2 := (Float'Max( b.x1, b.x2 ), Float'Max( b.y1, b.y2 ));

            -- rectangle separating A and B
            minInt : constant Vec2 := (Float'Max( min1.x, min2.x ), Float'Max( min1.y, min2.y ));
            maxInt : constant Vec2 := (Float'Min( max1.x, max2.x ), Float'Min( max1.y, max2.y ));
        begin
            -- if the rectangle separating 'a' and 'b' is "inside out" then the
            -- segments overlap. otherwise, its diagonal separates 'a' and 'b'.
            -- if its area is zero, then just the endpoints of 'a' and 'b' are
            -- touching. we don't count that as overlap.
            return minInt.x <= maxInt.x and minInt.y <= maxInt.y and
                   (minInt.x /= maxInt.x or minInt.y /= maxInt.y);
        end Colinear_Overlap;

        ------------------------------------------------------------------------

        uaNum  : constant Float := (b.x2 - b.x1) * (a.y1 - b.y1) - (b.y2 - b.y1) * (a.x1 - b.x1);
        ubNum  : constant Float := (a.x2 - a.x1) * (a.y1 - b.y1) - (a.y2 - a.y1) * (a.x1 - b.x1);
        denom  : constant Float := (b.y2 - b.y1) * (a.x2 - a.x1) - (b.x2 - b.x1) * (a.y2 - a.y1);
        ua, ub : Float;
    begin
        if denom /= 0.0 then
            ua := uaNum / denom;
            ub := ubNum / denom;
            -- true intersection happens when:
            -- (0.0 <= ua and then ua <= 1.0 and then 0.0 <= ub and then ub <= 1.0)
            -- but, by not including 0.0 and 1.0, we discount intersection with
            -- a line's endpoints, so two segments sharing an endpoint don't
            -- intersect.
            return 0.0 < ua and ua < 1.0 and 0.0 < ub and ub < 1.0;
        end if;

        -- if uaNum = 0.0 and ubNum = 0.0 then A and B are co-linear
        -- else A and B are parallel
        if uaNum = 0.0 and ubNum = 0.0 then
            return Colinear_Overlap( a, b );
        end if;
        return False;
    end Intersect_Seg_Seg;

    ----------------------------------------------------------------------------

    function Intersect_Rect_Rect( a, b : Rect ) return Boolean is
    (not (a.x > b.x + b.w or else b.x > a.x + a.w or else a.y > b.y + b.h or else b.y > a.y + a.h));

    ----------------------------------------------------------------------------

    function Intersect_Rect_Seg( r : Rect; seg : Segment ) return Boolean is
    begin
        -- check either endpoint is in the rect
        return (seg.x1 > r.x and seg.x1 < r.x + r.w and seg.y1 > r.y and seg.y1 < r.y + r.h)  or else    -- endpoint 1
               (seg.x2 > r.x and seg.x2 < r.x + r.w and seg.y2 > r.y and seg.y2 < r.y + r.h)  or else    -- endpoint 2
               Intersect_Seg_Seg( seg, Segment'(r.x,       r.y,       r.x + r.w, r.y      ) ) or else    -- top
               Intersect_Seg_Seg( seg, Segment'(r.x + r.w, r.y,       r.x + r.w, r.y + r.h) ) or else    -- right
               Intersect_Seg_Seg( seg, Segment'(r.x + r.w, r.y + r.h, r.x,       r.y + r.h) ) or else    -- bottom
               Intersect_Seg_Seg( seg, Segment'(r.x,       r.y + r.h, r.x,       r.y      ) );           -- left
    end Intersect_Rect_Seg;

    ----------------------------------------------------------------------------

    procedure Delete( n : in out Node ) is
        procedure Free is new Ada.Unchecked_Deallocation( Node_Array, A_Node_Array );
    begin
        if n.nodes /= null then
            for q in n.nodes'Range loop
                Delete( n.nodes(q) );
            end loop;
            Free( n.nodes );
        end if;
        n.children.Clear;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Insert( n : in out Node; seg : Segment; item : Item_Type ) is
        seg2 : Segment;
    begin
        if n.nodes /= null then
            for q in n.nodes'Range loop
                -- insert into all intersecting quadrants
                if Intersect_Rect_Seg( n.nodes(q).bounds, seg ) then
                    Insert( n.nodes(q), seg, item );
                end if;
            end loop;

        elsif Integer(n.children.Length) < n.maxChildren or else n.depth = 0 then
            -- insert it at this level until the node is full or we're already
            -- at max tree depth
            n.children.Append( Element'(seg, item) );

        else
            -- split the node into four quadrants and sort the children
            Split( n );

            while not n.children.Is_Empty loop
                -- insert into all intersecting quadrants
                seg2 := n.children.First_Element.seg;
                for q in n.nodes'Range loop
                    if Intersect_Rect_Seg( n.nodes(q).bounds, seg2 ) then
                        n.nodes(q).children.Append( n.children.First_Element );
                    end if;
                end loop;
                n.children.Delete_First;
            end loop;

            -- insert the new item after the split
            for q in n.nodes'Range loop
                if Intersect_Rect_Seg( n.nodes(q).bounds, seg ) then
                    Insert( n.nodes(q), seg, item );
                end if;
            end loop;
        end if;
    end Insert;

    ----------------------------------------------------------------------------

    procedure Intersecting_Rect( n : Node; r : Rect; found : in out Element_Sets.Set ) is
    begin
        if n.nodes /= null then
            for q in n.nodes'Range loop
                if Intersect_Rect_Rect( n.nodes(q).bounds, r ) then
                    Intersecting_Rect( n.nodes(q), r, found );
                end if;
            end loop;
        else
            for c of n.children loop
                if Intersect_Rect_Seg( r, c.seg ) then
                    found.Include( c );
                end if;
            end loop;
        end if;
    end Intersecting_Rect;

    ----------------------------------------------------------------------------

    procedure Intersecting_Seg( n : Node; seg : Segment; found : in out Element_Sets.Set ) is
    begin
        if n.nodes /= null then
            for q in n.nodes'Range loop
                if Intersect_Rect_Seg( n.nodes(q).bounds, seg ) then
                    Intersecting_Seg( n.nodes(q), seg, found );
                end if;
            end loop;
        else
            for c of n.children loop
                if Intersect_Seg_Seg( seg, c.seg ) then
                    found.Include( c );
                end if;
            end loop;
        end if;
    end Intersecting_Seg;

    ----------------------------------------------------------------------------

    procedure Iterate( n : Node; found : in out Element_Sets.Set ) is
    begin
        if n.nodes /= null then
            for q in n.nodes'Range loop
                Iterate( n.nodes(q), found );
            end loop;
        else
            for c of n.children loop
                found.Include( c );
            end loop;
        end if;
    end Iterate;

    ----------------------------------------------------------------------------

    procedure Iterate_Leaves( n       : Node;
                              examine : access procedure( x, y, w, h : Float ) ) is
    begin
        if n.nodes /= null then
            for q in n.nodes'Range loop
                Iterate_Leaves( n.nodes(q), examine );
            end loop;
        else
            examine( n.bounds.x, n.bounds.y, n.bounds.w, n.bounds.h );
        end if;
    end Iterate_Leaves;

    ----------------------------------------------------------------------------

    procedure Remove_Seg( n : in out Node; seg : Segment ) is
        c : Integer;
    begin
        if n.nodes /= null then
            for q in n.nodes'Range loop
                if Intersect_Rect_Seg( n.nodes(q).bounds, seg ) then
                    Remove_Seg( n.nodes(q), seg );
                end if;
            end loop;
        else
            c := 1;
            while c <= Integer(n.children.Length) loop
                if n.children.Element( c ).seg = seg then
                    n.children.Delete( c );
                else
                    c := c + 1;
                end if;
            end loop;
        end if;
    end Remove_Seg;

    ----------------------------------------------------------------------------

    function Size( n : Node ) return Natural is
        count : Natural := 0;
    begin
        if n.nodes = null then
            return Natural(n.children.Length);
        end if;
        for q in n.nodes'Range loop
            count := count + Size( n.nodes(q) );
        end loop;
        return count;
    end Size;

    ----------------------------------------------------------------------------

    procedure Split( n : in out Node ) is
        halfW, halfH : Float;
    begin
        Assert( n.nodes = null );
        n.nodes := new Node_Array;
        for q in n.nodes'Range loop
            n.nodes(q).maxChildren := n.maxChildren;
            n.nodes(q).depth := n.depth - 1;
        end loop;

        halfW := n.bounds.w / 2.0;
        halfH := n.bounds.h / 2.0;
        n.nodes(Top_Left).bounds := Rect'(n.bounds.x, n.bounds.y, halfW, halfH);
        n.nodes(Bottom_Left).bounds := Rect'(n.bounds.x, n.bounds.y + halfH, halfW, halfH);
        n.nodes(Top_Right).bounds := Rect'(n.bounds.x + halfW, n.bounds.y, halfW, halfH);
        n.nodes(Bottom_Right).bounds := Rect'(n.bounds.x + halfW, n.bounds.y + halfH, halfW, halfH);
    end Split;

    --==========================================================================

    procedure Clear( this : in out Tree'Class ) is
    begin
        Delete( this.root );
    end Clear;

    ----------------------------------------------------------------------------

    procedure Finalize( this : in out Tree ) is
    begin
        this.Clear;
    end Finalize;

    ----------------------------------------------------------------------------

    procedure Init( this          : in out Tree'Class;
                    width, height : Float;
                    maxDepth      : Positive;
                    maxChildren   : Natural ) is
    begin
        this.Clear;
        this.root.maxChildren := maxChildren;
        this.root.depth := maxDepth - 1;
        this.root.bounds := Rect'(0.0, 0.0, width, height);
    end Init;

    ----------------------------------------------------------------------------

    procedure Insert( this   : in out Tree'Class;
                      x1, y1 : Float;
                      x2, y2 : Float;
                      item   : Item_Type ) is
    begin
        Insert( this.root, Segment'(x1, y1, x2, y2), item );
    end Insert;

    ----------------------------------------------------------------------------

    procedure Intersecting_Rect( this    : Tree'Class;
                                 x, y    : Float;
                                 w, h    : Float;
                                 examine : access procedure( x1, y1 : Float;
                                                             x2, y2 : Float;
                                                             item   : Item_Type ) ) is
        found : Element_Sets.Set;
        r     : constant Rect := Rect'(x, y, w, h);
    begin
        if Intersect_Rect_Rect( this.root.bounds, r ) then
            Intersecting_Rect( this.root, r, found );
        end if;
        for elem of found loop
            examine( elem.seg.x1, elem.seg.y1, elem.seg.x2, elem.seg.y2, elem.item );
        end loop;
    end Intersecting_Rect;

    ----------------------------------------------------------------------------

    procedure Intersecting_Segment( this    : Tree'Class;
                                    x1, y1  : Float;
                                    x2, y2  : Float;
                                    examine : access procedure( x1, y1 : Float;
                                                                x2, y2 : Float;
                                                                item   : Item_Type ) ) is
        found : Element_Sets.Set;
        seg   : constant Segment := Segment'(x1, y1, x2, y2);
    begin
        if Intersect_Rect_Seg( this.root.bounds, seg ) then
            Intersecting_Seg( this.root, seg, found );
        end if;
        for elem of found loop
            examine( elem.seg.x1, elem.seg.y1, elem.seg.x2, elem.seg.y2, elem.item );
        end loop;
    end Intersecting_Segment;

    ----------------------------------------------------------------------------

    procedure Iterate( this    : Tree'Class;
                       examine : access procedure( x1, y1 : Float;
                                                   x2, y2 : Float;
                                                   item   : Item_Type ) ) is
        found : Element_Sets.Set;
    begin
        Iterate( this.root, found );
        for elem of found loop
            examine( elem.seg.x1, elem.seg.y1, elem.seg.x2, elem.seg.y2, elem.item );
        end loop;
    end Iterate;

    ----------------------------------------------------------------------------

    procedure Iterate_Leaves( this    : Tree'Class;
                              examine : access procedure( x, y, w, h : Float ) ) is
    begin
        Iterate_Leaves( this.root, examine );
    end Iterate_Leaves;

    ----------------------------------------------------------------------------

    procedure Remove( this   : in out Tree'Class;
                      x1, y1 : Float;
                      x2, y2 : Float ) is
        seg : constant Segment := Segment'(x1, y1, x2, y2);
    begin
        if Intersect_Rect_Seg( this.root.bounds, seg ) then
            Remove_Seg( this.root, seg );
        end if;
    end Remove;

    ----------------------------------------------------------------------------

    function Size( this : Tree'Class ) return Natural is
    begin
        return Size( this.root );
    end Size;

end Quad_Trees;
