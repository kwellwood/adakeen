--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Allegro.File_IO;                   use Allegro.File_IO;
with Interfaces;                        use Interfaces;
with Objects;                           use Objects;
with Streams;                           use Streams;
with Streams.Buffers;                   use Streams.Buffers;
with System;                            use System;
with Values.Lists;                      use Values.Lists;

package Resources is

    -- A Resource_File contains the raw data of an engine resource file, loaded
    -- into memory for parsing. The Resource_File API provides accessors for
    -- reading the data as a stream, an Allegro_file interface, and as an array.
    --
    -- Given a relative path, the Resources package will locate a file to be
    -- read according to a well-defined search order. Files will be automatically
    -- extracted from archive files as necessary.
    type Resource_File is new Limited_Object with private;
    type A_Resource_File is access all Resource_File'Class;

    -- Create and returns an Allegro_File interface to the file data. The caller
    -- is responsible for deleting the Allegro_File before the Resource_File.
    --
    -- Multiple Allegro_File instances can be created from this Resource_File
    -- and used independently.
    function Create_Allegro_File( this : not null access Resource_File'Class ) return A_Allegro_File;

    -- Creates and returns a new read-only stream backed by the file contents.
    -- The caller is responsible for calling Close() on the Buffer_Stream before
    -- deleting the Resource_File.
    --
    -- Multiple Buffer_Stream instances can be created from this Resource_File
    -- and used independently.
    function Create_Stream( this : not null access Resource_File'Class ) return A_Buffer_Stream;

    -- Returns the address of the Resource_File's data buffer. The size of the
    -- buffer in bytes can be determined by calling Size(). Returns Null_Address
    -- if the file is empty.
    function Get_Address( this : not null access Resource_File'Class ) return Address;

    -- Returns the filename of the resource.
    function Get_Filename( this : not null access Resource_File'Class ) return String;

    -- Returns the path of the resource file on disk. The path may refer to a
    -- file inside an archive.
    function Get_Path( this : not null access Resource_File'Class ) return String;

    -- Returns the size of the file in bytes.
    function Size(this : not null access Resource_File'Class ) return Unsigned_32;

    -- Deletes the Resource_File, freeing the file data from memory. Make sure
    -- all Allegro_File and Buffer_Stream objects created by this resource have
    -- been closed first.
    procedure Delete( this : in out A_Resource_File );
    pragma Postcondition( this = null );

    ----------------------------------------------------------------------------

    -- Locates a file 'filePath' according to a the standard resource search
    -- order, and loads the file's raw binary contents into memory. If 'filePath'
    -- refers to a file inside an archive, the outer archive file will be
    -- located according to the standard resource search order.
    --
    -- The search order for resource files is as follows:
    --
    --   1. [filePath]                          (If filePath is an absolute path)
    --   2. ./[filePath]                        (check the current working directory)
    --   3. ./[group]/[filePath]
    --   4. ./[group].zip<[filePath]
    --   5. [userDir]/[filePath]                (userDir is a user-writable path defined by Application)
    --   6. [mediaDir]/[filePath]               (mediaDir is defined by the "media" preference)
    --   7. [mediaDir]/[group]/[filePath]
    --   8. [mediaDir]/[group].zip<[filePath]
    --
    -- Returns null if the file is not found or cannot be read.
    function Load_Resource( filePath : String; group : String := "" ) return A_Resource_File;

    -- Searches for a file on disk according to the standard search order. If
    -- the file is found, its path will be returned. Otherwise, an empty string
    -- will be returned.
    function Locate_Resource( filePath : String; group : String ) return String;

    -- Searches for a file on disk according to the standard search order,
    -- returning True if the file is found, either on disk or in an archive. No
    -- attempt is made to load the file.
    function Resource_Exists( filePath : String; group : String ) return Boolean;

    -- Returns list of resource filenames matching 'wildcard' in the 'group'
    -- resource group. All the standard resource locations will be searched. See
    -- Load_Resource() for details on file locations. No path information will
    -- be returned - just filenames.
    function Search_Resources( wildcard : String; group : String ) return List_Value;
    pragma Postcondition( Search_Resources'Result.Valid );

    -- Attempts to find file 'filePath', according to the standard resource
    -- search order, then returns an absolute path that can be used to either
    -- overwrite the existing file (if it's in a user-writable location), or
    -- override the resource, by choosing a location earlier in the resource
    -- search order.
    --
    -- If 'filePath' contains a relative directory component, that relative path
    -- with also be a part of the returned path, but the subdirectory is not
    -- guaranteed to exist. The caller is reponsible for creating subdirectories
    -- as necessary.
    function Writable_Path( filePath : String; group : String ) return String;

    -- Creates a new Resource_File with the contents of 'path' in buffer 'data'.
    -- The buffer will be consumed.
    --
    -- 'path' is the actual resolved path of the resource file after searching.
    -- The resource file may be from inside an archive.
    function Create_Resource( path : String; data : not null A_SEA ) return A_Resource_File;
    pragma Postcondition( Create_Resource'Result /= null );

private

    -- File extension for resolving archive files when searching for a resource.
    ARCHIVE_EXTENSION : constant String := "zip";

    ----------------------------------------------------------------------------

    type Resource_File is new Limited_Object with
        record
            path : Unbounded_String;
            data : A_SEA := null;
        end record;

    procedure Construct( this : access Resource_File; path : String; data : not null A_SEA );

    procedure Delete( this : in out Resource_File );

    function To_String( this : access Resource_File ) return String;

end Resources;
