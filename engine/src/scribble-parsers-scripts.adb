--
-- Copyright (c) 2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with GNAT.OS_Lib;                       use GNAT.OS_Lib;
with Resources;                         use Resources;
with Resources.Plaintext;               use Resources.Plaintext;
with Scribble.Ast.Expressions.Symbols;  use Scribble.Ast.Expressions.Symbols;
with Scribble.Parse_Errors;             use Scribble.Parse_Errors;
with Scribble.String_Streams;           use Scribble.String_Streams;
with Support.Paths;                     use Support.Paths;
with Values.Strings;                    use Values.Strings;

package body Scribble.Parsers.Scripts is

    use Tokens;

    --==========================================================================

    function Create_Script_Parser( runtime : not null A_Scribble_Runtime ) return A_Script_Parser is
        this : constant A_Script_Parser := new Script_Parser;
    begin
        this.Construct( runtime );
        return this;
    end Create_Script_Parser;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Script_Parser ) is
    begin
        Delete( this.child );
        Parser(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Parse_Script( this : not null access Script_Parser'Class ) return A_Ast_Script is
        result : A_Ast_Script := Create_Script( this.scanner.Location );
    begin
        this.Parse_Script( result );
        return result;
    exception
        when others =>
            Delete( A_Ast_Node(result) );
            raise;
    end Parse_Script;

    ----------------------------------------------------------------------------

    --
    -- Script ::= { Script_Element }
    --
    procedure Parse_Script( this   : not null access Script_Parser'Class;
                            script : not null A_Ast_Script ) is
    begin
        loop
            exit when not this.Scan_Script_Element( script );
        end loop;
    end Parse_Script;

    ----------------------------------------------------------------------------

    --
    -- Script_Element ::= {';'} (
    --                    Include
    --                    | Member
    --                    | Timer
    --                    ) {';'}
    --
    function Scan_Script_Element( this   : not null access Script_Parser'Class;
                                  script : not null A_Ast_Script ) return Boolean is
        elem : A_Ast_Script_Element := null;
    begin
        -- skip any empty statements preceeding the next real one
        while this.scanner.Accept_Token( TK_SEMICOLON ).Not_Null loop
            null;
        end loop;

        if this.Scan_Include( script ) then
            -- skip any empty statements following the one that was parsed
            while this.scanner.Accept_Token( TK_SEMICOLON ).Not_Null loop
                null;
            end loop;
            return True;
        end if;

        elem := this.Scan_Member;
        if elem = null then
            elem := this.Scan_Timer;
        end if;

        if elem /= null then
            -- skip any empty statements following the one that was parsed
            while this.scanner.Accept_Token( TK_SEMICOLON ).Not_Null loop
                null;
            end loop;
            script.Add_Element( elem );
            return True;
        end if;

        return False;
    exception
        when others =>
            Delete( A_Ast_Node(elem) );
            raise;
    end Scan_Script_Element;

    ----------------------------------------------------------------------------

    --
    -- Include ::= 'include' string-value-token ';'
    --
    function Scan_Include( this   : not null access Script_Parser'Class;
                           script : not null A_Ast_Script ) return Boolean is
        incToken     : Token_Ptr;
        pathTok      : Token_Ptr;
        filepath     : Value;
        resolvedPath : Unbounded_String;
        source       : Unbounded_String;
    begin
        incToken := this.scanner.Accept_Token( TK_INCLUDE );
        if incToken.Not_Null then
            -- expect a filepath string
            pathTok := this.scanner.Expect_Token( TK_VALUE );
            filepath := Value_Token(pathTok.Get.all).Get_Value;
            if not filepath.Is_String then
                Raise_Parse_Error( "Expected string", pathTok.Get.Location );
            elsif filepath.Str.Length = 0 then
                Raise_Parse_Error( "Expected path", pathTok.Get.Location );
            end if;

            -- expect the ending semicolon
            this.scanner.Expect_Token( TK_SEMICOLON );

            -- parse the contents of 'filepath' into the script
            if this.child = null then
                this.child := Create_Script_Parser( this.runtime );
            end if;

            -- relative include paths (the 99% case) are relative to the
            -- including file's directory, if it has an absolute path.
            if not Is_Absolute_Path( filepath.Str.To_String ) and then
                   Is_Absolute_Path( To_String( this.path ) )
            then
                resolvedPath := To_Unbounded_String( Path_Append( Get_Directory( To_String( this.path ) ),
                                                                      filepath.Str.To_String ) );
            else
                resolvedPath := filepath.Str.To_Unbounded_String;
            end if;

            if not Load_Plaintext( To_String( resolvedPath ), "scripts", source ) then
                Raise_Parse_Error( "File '" & filepath.Str.To_String & "' not found",
                                   pathTok.Get.Location );
            end if;

            declare
                strm : A_String_Stream := Stream( source );
            begin
                this.child.Set_Input( strm, filepath.Str.To_String );
                this.child.Enable_Debug( this.enableDebug );
                this.child.Parse_Script( script );           -- may raise Parse_Error
                this.child.Expect_EOF;                       -- may raise Parse_Error
                this.child.Set_Input( null );
                Close( strm );
            exception
                when Parse_Error =>
                    Close( strm );
                    raise;
            end;

            return True;
        else
            -- no Include found
            return False;
        end if;
    end Scan_Include;

    ----------------------------------------------------------------------------

    --
    -- Member ::= (Required_Member | Member_Function | Member_Var) ';'
    --
    function Scan_Member( this : not null access Script_Parser'Class ) return A_Ast_Script_Element is
        result : A_Ast_Script_Element := null;
        func   : A_Ast_Function_Definition;
    begin
        result := this.Scan_Required_Member;
        if result = null then
            result := this.Scan_Member_Var;
            if result = null then
                func := this.Scan_Function_Definition( nameOption => Required, memberOption => Required );
                if func /= null then
                    result := Create_Script_Member( func.Location,
                                                    Create_Identifier( func.Get_Name.Location,
                                                                       func.Get_Name.Get_Name,
                                                                       "" ),
                                                    A_Ast_Expression(func),
                                                    False );
                end if;
            end if;
        end if;

        -- expect a terminating semicolon for any member that was found
        if result /= null then
            this.scanner.Expect_Token( TK_SEMICOLON );
        end if;

        return result;
    end Scan_Member;

    ----------------------------------------------------------------------------

    --
    -- Member_Var ::= ['default'] Identifier ':=' Expression<literal=True>
    --
    function Scan_Member_Var( this : not null access Script_Parser'Class ) return A_Ast_Script_Element is
        default : Token_Ptr;
        name    : Token_Ptr;
        expr    : A_Ast_Expression;
    begin
        default := this.scanner.Accept_Token( TK_DEFAULT );
        if default.Not_Null then
            name := this.scanner.Expect_Token( TK_SYMBOL );
        else
            name := this.scanner.Accept_Token( TK_SYMBOL );
            if name.Is_Null then
                -- Member_Var is not found (no 'default' and no Identifier)
                return null;
            end if;
        end if;

        this.scanner.Expect_Token( TK_COLON_EQUALS );

        expr := this.Expect_Expression( literal => True );

        return Create_Script_Member( name.Get.Location,
                                     Create_Identifier( name.Get.Location,
                                                        Symbol_Token(name.Get.all).Get_Name,
                                                        "" ),
                                     expr,
                                     default.Not_Null );
    end Scan_Member_Var;

    ----------------------------------------------------------------------------

    --
    -- Required_Member ::= 'require' Identifier
    --
    function Scan_Required_Member( this : not null access Script_Parser'Class ) return A_Ast_Script_Element is
        result : A_Ast_Script_Element := null;
        tok    : Token_Ptr;
        name   : Token_Ptr;
    begin
        tok := this.scanner.Accept_Token( TK_REQUIRE );
        if tok.Not_Null then
            -- required member variable

            name := this.scanner.Expect_Token( TK_SYMBOL );

            result := Create_Required_Member( tok.Get.Location,
                                              Create_Identifier( name.Get.Location,
                                                                 Symbol_Token(name.Get.all).Get_Name,
                                                                 "" ) );
        else
            -- no required member found
            null;
        end if;

        return result;
    end Scan_Required_Member;

    ----------------------------------------------------------------------------

    --
    -- Timer ::= 'timer' Identifier ['(' Expression<literal=True> ')'] ('in' | 'every') number-value-token ';'
    --
    function Scan_Timer( this : not null access Script_Parser'Class ) return A_Ast_Script_Element is
        result   : A_Ast_Script_Element := null;
        name     : A_Ast_Identifier;
        args     : A_Ast_Expression;
        period   : A_Ast_Expression;
        repeat   : Boolean := False;
        timerTok : Token_Ptr;
        tok      : Token_Ptr;
    begin
        timerTok := this.scanner.Accept_Token( TK_TIMER );
        if timerTok.Not_Null then
            -- expect a timer name symbol
            tok := this.scanner.Expect_Token( TK_SYMBOL );
            name := Create_Identifier( tok.Get.Location,
                                       Symbol_Token(tok.Get.all).Get_Name,
                                       "" );

            -- optional '(' for the argument
            tok := this.scanner.Accept_Token( TK_LPAREN );
            if tok.Not_Null then
                -- timer argument expression
                args := this.Expect_Expression( literal => True );
                this.scanner.Expect_Token( TK_RPAREN );
            else
                -- no argument
                null;
            end if;

            -- in | every
            tok := this.scanner.Expect_One( (TK_IN, TK_EVERY) );
            if tok.Get.Get_Type = TK_EVERY then
                -- repeating timer
                repeat := True;
            end if;

            -- timer period (milliseconds)
            tok := this.scanner.Expect_Token( TK_VALUE );
            if not Value_Token(tok.Get.all).Get_Value.Is_Number then
                Raise_Parse_Error( "Expected number", tok.Get.Location );
            end if;
            period := Create_Literal( tok.Get.Location, Value_Token(tok.Get.all).Get_Value );

            -- ending semicolon
            this.scanner.Expect_Token( TK_SEMICOLON );

            result := Create_Timer( timerTok.Get.Location,
                                    name,
                                    args,
                                    period,
                                    repeat );
        else
            -- no Timer found
            null;
        end if;

        return result;
    exception
        when others =>
            pragma Assert( result = null );
            Delete( A_Ast_Node(name) );
            Delete( A_Ast_Node(args) );
            Delete( A_Ast_Node(period) );
            raise;
    end Scan_Timer;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Script_Parser ) is
    begin
        Delete( A_Parser(this) );
    end Delete;

end Scribble.Parsers.Scripts;
