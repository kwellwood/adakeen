--
-- Copyright (c) 2014-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Objects;                           use Objects;
with Scribble.Compilers;                use Scribble.Compilers;
with Scribble.Namespaces;               use Scribble.Namespaces;
with Scribble.Runtimes;                 use Scribble.Runtimes;
with Scribble.VMs;                      use Scribble.VMs;
with Values;                            use Values;

package Script_VMs is

    -- A Script_VM encapsulates the script compiler, VM and runtime environment
    -- for compiling and executing scripts that share a common runtime.
    type Script_VM is new Limited_Object with private;
    type A_Script_VM is access all Script_VM'Class;

    -- Creates a new Script_VM object.
    function Create_Script_VM return A_Script_VM;
    pragma Postcondition( Create_Script_VM'Result /= null );

    -- Returns a reference to the game logic system's Scribble compiler.
    function Get_Compiler( this : not null access Script_VM'Class ) return A_Scribble_Compiler;

    -- Returns a reference to the global namespace used by 'this'.
    function Get_Globals( this : not null access Script_VM'Class ) return A_Scribble_Namespace;

    -- Returns a reference to the Scribble runtime used by the compiler and VM.
    function Get_Runtime( this : not null access Script_VM'Class ) return A_Scribble_Runtime;

    -- Returns a reference to the Scribble VM, with attached runtime.
    function Get_VM( this : not null access Script_VM'Class ) return A_Scribble_VM;

    -- Loads script 'file', containing an association, into the global namespace
    -- override any conflicting names.
    procedure Load_Globals( this : not null access Script_VM'Class; filename : String );

    -- Sets the value of global variable 'name' to 'val'.
    procedure Set_Global( this    : not null access Script_VM'Class;
                          name    : String;
                          val     : Value'Class;
                          consume : Boolean := False );

    procedure Delete( this : in out A_Script_VM );
    pragma Postcondition( this = null );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Returns True if the type of every element in 'args' matches the
    -- corresponding element in 'types'. There can be fewer 'args' than types.
    -- A V_NULL argument is allowed to be passed to a V_ID type parameter.
    function Check_Types( args : Value_Array; types : Type_Array ) return Boolean with Inline_Always;

private

    type Script_VM is new Limited_Object with
        record
            globals  : A_Scribble_Namespace := null;
            runtime  : A_Scribble_Runtime := null;
            compiler : A_Scribble_Compiler := null;
            vm       : A_Scribble_VM := null;
        end record;

    procedure Construct( this : access Script_VM );

    procedure Delete( this : in out Script_VM );

end Script_VMs;
