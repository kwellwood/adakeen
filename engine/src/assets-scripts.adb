--
-- Copyright (c) 2018-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Debugging;                         use Debugging;
with Resources;                         use Resources;
with Resources.Scribble;                use Resources.Scribble;
with Support.Paths;                     use Support.Paths;
with Values.Errors;                     use Values.Errors;
with Values.Strings;                    use Values.Strings;
with Version;

package body Assets.Scripts is

    -- compile with debug symbols?
    COMPILE_DEBUG : constant Boolean := Version.Is_Debug;

    -- initialized on startup with the global script runtime (runtime not owned)
    scriptRuntime : A_Scribble_Runtime := null;

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    procedure Initialize( runtime : not null A_Scribble_Runtime ) is
    begin
        pragma Assert( scriptRuntime = null, "Already initialized" );
        scriptRuntime := runtime;
    end Initialize;

    ----------------------------------------------------------------------------

    procedure Finalize is
    begin
        scriptRuntime := null;
    end Finalize;

    --==========================================================================

    function Load_Script( filePath : String; group : String ) return A_Script_Asset is
        lpath   : constant String := Add_Extension( To_Lower( filePath ), "script" );
        assetId : Value;
        asset   : A_Asset;
        this    : A_Script_Asset;
    begin
        assetId := Create( To_Lower( group ) & ":" & lpath );

        asset := Find_Asset( assetId );
        if asset /= null then
            -- found in cache
            pragma Assert( asset.Get_Type = Script_Assets,
                           "Cached asset '" & filePath & "' is not a script" );
            this := A_Script_Asset(asset);
        else
            -- create a new asset
            this := new Script_Asset;
            this.Construct( assetId, lpath, group );
            Store_Asset( A_Asset(this) );
        end if;
        return this;
    end Load_Script;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this     : access Script_Asset;
                         assetId  : Value'Class;
                         filePath : String;
                         group    : String ) is
    begin
        Asset(this.all).Construct( Script_Assets, assetId, filePath, group );
        if Length( this.diskFile ) = 0 then
            -- source file is not found; fall back to a compiled object file
            this.diskFile := To_Unbounded_String( Locate_Resource( To_String( this.filePath ) & ".sco", To_String( this.group ) ) );
        end if;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Members( this : not null access Script_Asset'Class ) return Map_Value is
    begin
        return this.members;
    end Get_Members;

    ----------------------------------------------------------------------------

    function Get_Timers( this : not null access Script_Asset'Class ) return List_Value is
    begin
        return this.timers;
    end Get_Timers;

    ----------------------------------------------------------------------------

    overriding
    function Load_Data( this : access Script_Asset ) return Boolean is
        val : Value;
    begin
        pragma Assert( scriptRuntime /= null, "Script assets not initialized" );
        if not Load_Script( val,
                            To_String( this.filePath ),
                            To_String( this.group ),
                            scriptRuntime,
                            COMPILE_DEBUG )
        then
            Dbg( val.Err.Get_Message, D_ASSET or D_SCRIPT, Error );
            this.members := val.Map;
            this.timers := val.Lst;
            return False;
        end if;

        pragma Assert( val.Is_Map, "Invalid format in script '" & To_String( this.filePath ) & "'" );

        this.members := val.Map.Get( "members" ).Map;
        if not this.members.Valid then
            this.members := Create_Map.Map;
        end if;

        this.timers := val.Map.Get( "timers" ).Lst;
        if not this.timers.Valid then
            this.timers := Create_List.Lst;
        end if;

        return True;
    end Load_Data;

    ----------------------------------------------------------------------------

    overriding
    procedure Unload_Data( this : access Script_Asset ) is
    begin
        this.members := Null_Value.Map;
        this.timers := Null_Value.Lst;
    end Unload_Data;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Script_Asset ) is
    begin
        Delete( A_Asset(this) );
    end Delete;

end Assets.Scripts;
