--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Color;                     use Allegro.Color;
with Interfaces;                        use Interfaces;

package Palette is

    function Black       return Allegro_Color is (Al_Map_RGB_f( 0.0, 0.0, 0.0 )) with Inline;
    function White       return Allegro_Color is (Al_Map_RGB_f( 1.0, 1.0, 1.0 )) with Inline;
    function Transparent return Allegro_Color is (Al_Map_RGBA_f( 0.0, 0.0, 0.0, 0.0 )) with Inline;
    function Magenta     return Allegro_Color is (Al_Map_RGB_f( 1.0, 0.0, 1.0 )) with Inline;
    function No_Color    return Allegro_Color is (Al_Map_RGBA_f( -1.0, -1.0, -1.0, -1.0 )) with Inline;

    -- Color Manipulation Functions --

    -- Returns True if the colors compare within tolerance bounds. If any of the
    -- components of the colors differ by more than 'tolerance', the comparison
    -- will fail. The integer version of Compare() accepts 0-255. The float
    -- version of Compare() accepts 0.0-1.0.
    function Compare( x, y : Allegro_Color; tolerance : Natural := 0 ) return Boolean;
    function Compare( x, y : Allegro_Color; tolerance : Float := 0.0 ) return Boolean;

    -- Returns the foreground color adjusted for contrast against the background.
    function Contrast( fg, bg : Allegro_Color; ratio : Float := 1.75 ) return Allegro_Color;

    -- Returns the alpha channel of 'color'. 255 or 1.0 is completely opaque.
    function Get_Alpha( color : Allegro_Color ) return Unsigned_8;
    function Get_Alpha( color : Allegro_Color ) return Float;

    -- Returns white or black, depending on the brightness of the background.
    -- The alpha component of 'bg' will be preserved unless 'preserveAlpha' is
    -- set to False.
    function Hard_Contrast( bg            : Allegro_Color;
                            preserveAlpha : Boolean := True ) return Allegro_Color;

    -- Converts an HTML-style color (e.g. "#RRGGBB[AA]") to an Allegro color.
    -- Returns 'default' if 'html' is not properly formatted.
    function Html_To_Color( html : String; default : Allegro_Color ) return Allegro_Color;

    -- Converts a color to an HTML-style string in the form "#RRGGBBAA".
    function Color_To_Html( color : Allegro_Color; alpha : Boolean := True ) return String;

    -- Interpolates between colors 'first' and 'last', based on 'progress' where
    -- 0.0 => first and 1.0 => last.
    function Interpolate( first, last : Allegro_Color;
                          progress    : Float ) return Allegro_Color;

    -- Returns True if 'color' is completely opaque.
    function Is_Opaque( color : Allegro_Color ) return Boolean;

    -- Returns a color that is lighter or darker than the input. The color will
    -- be darker where 0 < factor < 1 and lighter where factor > 1. This does
    -- not necessarily lighten a color beyond full saturation because it only
    -- multiplies the color components. Use Whiten() to lighten a color toward
    -- white.
    function Lighten( color : Allegro_Color; factor : Float ) return Allegro_Color;

    -- Returns a color that is linearly interpolated to be whiter by some factor,
    -- where 0 not at all, and 1 is completely white. Alpha is not changed.
    function Whiten( color : Allegro_Color; factor : Float ) return Allegro_Color;

    -- Returns a grey color with the specified brightness. For the Integer
    -- version, allowed values are 0-255. For the float version, allowed values
    -- are 0.0 to 1.0.
    function Make_Grey( brightness : Natural; alpha : Unsigned_8 := 255 ) return Allegro_Color;
    function Make_Grey( brightness : Float;   alpha : Float      := 1.0 ) return Allegro_Color;

    -- Returns colors 'a' and 'b' multiplied together.
    function Multiply( a, b : Allegro_Color ) return Allegro_Color;

    -- Sets the alpha channel of 'color' to create a new color with a different
    -- opacity. The current alpha channel of 'color' will be ignored. An alpha
    -- value of 1.0 is completely opaque.
    function Opacity( color : Allegro_Color; alpha : Float ) return Allegro_Color;

    -- Returns a neutral tint color (white) with an opacity of 'alpha'. This can
    -- be used with drawing routines to conveniently apply an alpha-only tint.
    function Opacity( alpha : Float ) return Allegro_Color is (Opacity( White, alpha ));

end Palette;
