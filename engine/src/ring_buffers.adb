--
-- Copyright (c) 2016-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Ring_Buffers is

    function Is_Empty( buf : Buffer ) return Boolean is (Length( buf ) = 0);

    ----------------------------------------------------------------------------

    function Is_Full( buf : Buffer ) return Boolean is (Length( buf ) = buf.elements'Length - 1);

    ----------------------------------------------------------------------------

    function Length( buf : Buffer ) return Natural is ((buf.elements'Length + buf.wpos - buf.rpos) mod buf.elements'Length);

    ----------------------------------------------------------------------------

    function Peek( buf : Buffer; index : Integer ) return Element_Type
    is (buf.elements((buf.rpos + index - 1) mod buf.elements'Length));

    ----------------------------------------------------------------------------

    procedure Poke( buf : in out Buffer; index : Integer; elem : Element_Type ) is
    begin
        buf.elements((buf.rpos + index - 1) mod buf.elements'Length) := elem;
    end Poke;

    ----------------------------------------------------------------------------

    procedure Pop( buf : in out Buffer; elem : out Element_Type ) is
    begin
        pragma Assert( Length( buf ) > 0 );
        elem := buf.elements(buf.rpos);
        buf.rpos := (buf.rpos + 1) mod buf.elements'Length;
    end Pop;

    ----------------------------------------------------------------------------

    procedure Push( buf : in out Buffer; elem : Element_Type ) is
    begin
        pragma Assert( Length( buf ) < buf.elements'Length - 1 );
        buf.elements(buf.wpos) := elem;
        buf.wpos := (buf.wpos + 1) mod buf.elements'Length;
    end Push;

end Ring_Buffers;
