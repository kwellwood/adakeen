--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
--with Debugging;                         use Debugging;
with GNAT.Directory_Operations;         use GNAT.Directory_Operations;
with GNAT.OS_Lib;                       use GNAT.OS_Lib;

package body Support.Paths is

    -- Returns the location of the '.' character which begins the path's file
    -- extension. If no extension is found, path'First - 1 is returned.
    function Find_Extension( path : String ) return Integer is

        ------------------------------------------------------------------------

        -- Slashes are always directory separators, and '<' acts as a directory
        -- separator because it separates paths from their outer archive file.
        function Is_Dir_Sep( c : Character ) return Boolean is (c = '/' or c = '\' or c = '<');

        ------------------------------------------------------------------------

        pos : Integer := path'Last;
    begin
        while pos > path'First and then
              not Is_Dir_Sep( path(pos) ) and then
              path(pos) /= '.'
        loop
            pos := pos - 1;
        end loop;

        -- to find the extension, we must be somewhere after the first character,
        -- at or before the last character, at a dot character and the character
        -- before the dot can't be a directory separator or archive delimiter.
        if pos + 1 <= path'Last and then pos > path'First and then
           path(pos) = '.' and then not Is_Dir_Sep( path(pos-1) )
        then
           return pos;
        end if;

        return path'First - 1;
    end Find_Extension;

    ----------------------------------------------------------------------------

    function Find_Last_Slash( path : String ) return Integer is
        pos : Integer := path'Last;
    begin
        while pos >= path'First loop
            exit when path(pos) = '/' or path(pos) = '\';
            if path(pos) = '<' then
                -- encountered a containing archive without finding a slash
                pos := path'First - 1;
                exit;
            end if;
            pos := pos - 1;
        end loop;
        return pos;
    end Find_Last_Slash;

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Add_Extension( path,
                            ext   : String;
                            force : Boolean := False ) return String is
    begin
        -- if the path (or inner-most path in archive) is empty, just return 'path'
        if path'Length > 0 and then path(path'Last) /= '<' and then ext'Length > 0 then
            if Get_Extension( path )'Length = 0 or else
               (force and then not Ends_With( To_Lower( path ), "." & To_Lower( ext ) ))
            then
                -- add the extension if we're forcing it, or if there isn't an
                -- extension
                return path & "." & ext;
            end if;
        end if;
        return path;
    end Add_Extension;

    ----------------------------------------------------------------------------

    function Ancestor_Dir( fullpath, filename : String ) return String is
    begin
        if Ends_With( fullpath, filename ) then
            return fullpath(fullpath'First..fullpath'Last-filename'Length);
        end if;
        return fullpath;
    end Ancestor_Dir;

    ----------------------------------------------------------------------------

    function App_Data_Directory return String renames OS.App_Data_Directory;

    ----------------------------------------------------------------------------

    procedure Auto_Open( filePath : String ) is
    begin
        OS.Auto_Open( Get_Outer_Path( filePath ) );
    end Auto_Open;

    ----------------------------------------------------------------------------

    function Ensure_Trailing_Slash( path : String ) return String is
    begin
        if path'Length = 0 then
            -- path to current working directory
            return "." & Slash;
        elsif path(path'Last) = '<' then
            -- empty path inside an archive (no need for "./")
            return path;
        elsif path(path'Last) = '/' or path(path'Last) = '\' then
            -- already has trailing slash
            return path;
        end if;
        -- append slash
        return path & Slash;
    end Ensure_Trailing_Slash;

    ----------------------------------------------------------------------------

    function Executable_Extension return String renames OS.Executable_Extension;

    ----------------------------------------------------------------------------

    function Executable_Path return String renames OS.Executable_Path;

    ----------------------------------------------------------------------------

    function Execution_Directory return String renames OS.Execution_Directory;

    ----------------------------------------------------------------------------

    function Get_Archive_File( path : String ) return String is
    begin
        for pos in path'Range loop
            if path(pos) = '<' then
                return path(path'First..pos-1);
            end if;
        end loop;
        return "";
    end Get_Archive_File;

    ----------------------------------------------------------------------------

    function Get_Basename( path : String ) return String is
        filename : constant String := Get_Filename( path );
        dot      : Integer := Index( filename, "." );
    begin
        if dot < filename'First then
            -- no dot? then the whole filename is the base
            dot := filename'Last + 1;
        end if;
        return filename(filename'First..dot-1);
    end Get_Basename;

    ----------------------------------------------------------------------------

    function Get_Complete_Extension( path : String ) return String is
        dot : Integer := path'Last + 1;
    begin
        for pos in reverse path'Range loop
            if path(pos) = '.' then
                dot := pos;
            elsif path(pos) = '/' or path(pos) = '\' or path(pos) = '<' then
                exit;
            end if;
        end loop;
        if dot < path'Last then
            return path(dot+1..path'Last);
        end if;
        return "";
    end Get_Complete_Extension;

    ----------------------------------------------------------------------------

    function Get_Directory( path : String ) return String is
        inner : constant String := Get_Inner_Path( path );
        pos   : constant Integer := Find_Last_Slash( inner );
    begin
        if pos >= inner'First then
            return inner(inner'First..pos);
        end if;
        return "";
    end Get_Directory;

    ----------------------------------------------------------------------------

    function Get_Dir_Name( path : String ) return String is
        dir : constant String := Get_Directory( Get_Inner_Path( path ) );
        pos : Integer;
    begin
        if dir'Length > 0 then
            -- dir'Last is a slash, so the directory name is the characters
            -- following the next-to-last slash, up to the dir'Last.
            pos := Find_Last_Slash( dir(dir'First..dir'Last-1) );
            return dir(pos+1..dir'Last-1);
        end if;
        return "";
    end Get_Dir_Name;

    ----------------------------------------------------------------------------

    function Get_Extension( path : String ) return String is
        pos : constant Integer := Find_Extension( path );
    begin
        if pos >= path'First then
            return path(pos+1..path'Last);
        end if;
        return "";
    end Get_Extension;

    ----------------------------------------------------------------------------

    function Get_Filename( path : String ) return String is
        inner : constant String := Get_Inner_Path( path );
        pos   : constant Integer := Find_Last_Slash( inner );
    begin
        if pos >= inner'First and then pos <= inner'Last then
            return inner(pos+1..inner'Last);
        end if;
        return inner;
    end Get_Filename;

    ----------------------------------------------------------------------------

    function Get_Inner_Path( path : String ) return String is
    begin
        for pos in reverse path'Range loop
            if path(pos) = '<' then
                return path(pos+1..path'Last);
            end if;
        end loop;
        return path;
    end Get_Inner_Path;

    ----------------------------------------------------------------------------

    function Get_Outer_Path( path : String ) return String is
    begin
        for pos in path'Range loop
            if path(pos) = '<' then
                return path(path'First..pos-1);
            end if;
        end loop;
        return path;
    end Get_Outer_Path;

    ----------------------------------------------------------------------------

    function Get_Path_In_Archive( path : String ) return String is
    begin
        for pos in path'Range loop
            if path(pos) = '<' then
                return path(pos+1..path'Last);
            end if;
        end loop;
        return "";
    end Get_Path_In_Archive;

    ----------------------------------------------------------------------------

    function Home_Directory return String renames OS.Home_Directory;

    ----------------------------------------------------------------------------

    function Is_Path_In_Archive( path : String ) return Boolean is
    begin
        for pos in path'Range loop
            if path(pos) = '<' then
                return True;
            end if;
        end loop;
        return False;
    end Is_Path_In_Archive;

    ----------------------------------------------------------------------------

    function Is_Newer_File( path1, path2 : String ) return Boolean is
        stamp1 : constant OS_Time := File_Time_Stamp( Get_Outer_Path( path1 ) );
        stamp2 : constant OS_Time := File_Time_Stamp( Get_Outer_Path( path2 ) );
    begin
        return stamp1 /= Invalid_Time and then stamp1 > stamp2;
    end Is_Newer_File;

    ----------------------------------------------------------------------------

    function Make_Dir( path : String ) return Boolean is
        pos : Integer;
    begin
        if path'Length > 0 then
            if Is_Path_In_Archive( path ) then
                return Make_Dir( Get_Directory( Get_Archive_File( path ) ) );
            elsif not GNAT.OS_Lib.Is_Directory( path ) then
                -- attempt to create 'path' as given
                GNAT.Directory_Operations.Make_Dir( path );
            end if;
            return True;
        else
            return False;
        end if;
    exception
        when Directory_Error =>
            -- failed, try to create its parent directory first
            pos := path'Last;
            -- skip any trailing directory separators
            while pos > path'First and then (path(pos) = '/' or path(pos) = '\') loop
                pos := pos - 1;
            end loop;
            -- skip to the last directory separator on the path and discard
            -- everything after it
            while pos > path'First and then
                  path(pos) /= '/' and then
                  path(pos) /= '\'
            loop
                if path(pos) = ':' then
                    -- found the drive letter
                    return False;
                end if;
                pos := pos - 1;
            end loop;
            if Make_Dir( path(path'First..pos) ) then
                -- try to make the child directory again
                begin
                    GNAT.Directory_Operations.Make_Dir( path );
                    return True;
                exception
                    when Directory_Error =>
                        return False;
                end;
            else
                return False;
            end if;
    end Make_Dir;

    ----------------------------------------------------------------------------

    function Other_Slash return String is (if GNAT.OS_Lib.Directory_Separator = '/' then "\" else "/");

    ----------------------------------------------------------------------------

    function Path_Append( basePath, extra : String ) return String is

        ------------------------------------------------------------------------

        function No_Leading( str : String ) return String is
        begin
            if str'Length > 0 and then
               (str(str'First) = '/' or str(str'First) = '\')
            then
                return No_Leading( str(str'First+1..str'Last) );
            end if;
            return str;
        end No_Leading;

        ------------------------------------------------------------------------

        function No_Trailing( str : String ) return String is
        begin
            if str'Length > 0 and then
               (str(str'Last) = '/' or str(str'Last) = '\')
            then
                return No_Trailing( str(str'First..str'Last-1) );
            end if;
            return str;
        end No_Trailing;

        ------------------------------------------------------------------------

    begin
        if basePath'Length = 0 and then extra'Length = 0 then
            return "";
        elsif extra'Length = 0 then
            return basePath;
        elsif basePath'Length = 0 then
            return extra;
        elsif basePath(basePath'Last) = '<' then
            -- basePath is an empty path into an archive
            return basePath & No_Leading( extra );
        end if;

        return No_Trailing( basePath ) & Slash & No_Leading( extra );
    end Path_Append;

    ----------------------------------------------------------------------------

    function Remove_Extension( path : String ) return String is
        pos : constant Integer := Find_Extension( path );
    begin
        if pos >= path'First then
            return path(path'First..pos-1);
        end if;
        return path;
    end Remove_Extension;

    ----------------------------------------------------------------------------

    function Remove_Trailing_Slash( path : String ) return String is
        pos : Integer := path'Last;
    begin
        -- remove the trailing slash(es) from the inner-most path only
        while pos >= path'First and then path(pos) /= '<' loop
            exit when path(pos) /= '/' and then path(pos) /= '\';
            pos := pos - 1;
        end loop;
        return path(path'First..pos);
    end Remove_Trailing_Slash;

    ----------------------------------------------------------------------------

    procedure Reveal_Path( path : String ) is
    begin
        OS.Reveal_Path( Get_Outer_Path( path ) );
    end Reveal_Path;

    ----------------------------------------------------------------------------

    function Slash return String is
        result : constant String (1..1) := (1 => GNAT.OS_Lib.Directory_Separator);
    begin
        return result;
    end Slash;

    ----------------------------------------------------------------------------

    function System_Font_Directory return String renames OS.System_Font_Directory;

    ----------------------------------------------------------------------------

    function Temp_Directory return String renames OS.Temp_Directory;

end Support.Paths;
