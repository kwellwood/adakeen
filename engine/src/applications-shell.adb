--
-- Copyright (c) 2012-2014 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Text_IO;
with Debugging;                         use Debugging;
with GNAT.OS_Lib;                       use GNAT.OS_Lib;
with Version;

package body Applications.Shell is

    function Get_Announce( this : not null access Shell_Application'Class ) return String
    is (
        this.Get_Name & " v" & Version.Release & " - " &
        this.Get_Company & " " &
        "Copyright (C) " & Version.Copyright_Year
    );

    ----------------------------------------------------------------------------

    overriding
    procedure Kill( this : access Shell_Application ) is
        pragma Unreferenced( this );
    begin
        GNAT.OS_Lib.OS_Exit( 1 );
    end Kill;

begin

    -- Use Ada.Text_IO as the default debug output for shell applications
    -- This can be overridden by calling Set_Output in the elaboration section
    -- of a specific application's Project package body.
    Debugging.Set_Output( Ada.Text_IO.Put_Line'Access );

end Applications.Shell;
