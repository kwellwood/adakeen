--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Exceptions;                    use Ada.Exceptions;
with GNAT.Source_Info;

package Debugging is

    -- Description of verbosity levels:
    --
    -- Info   : The most verbose level of output. This includes informational
    --          messages that describe what is happening, plus warnings and
    --          errors.
    --
    -- Warning: Warning messages indicate a potential bug in the application
    --          or a resource file but do not hinder the application's ability
    --          to execute. This verbosity also includes errors. It is the
    --          default verbosity for DEBUG builds.
    --
    -- Error  : Error messages and exceptions that prevent the application from
    --          operating normally. This is the default verbosity level for
    --          RELEASE builds.
    --
    -- Never  : The least verbose level of output. Not even error messages will
    --          be displayed; only temporary debugging messages during
    --          development, marked with a verbosity of Never, will be output.
    --          If an application limits its verbosity to Never, debug output is
    --          essentially disabled.
    --
    type Verbosity_Level is (Never, Error,  Warning, Info);

    -- Description of decoration levels:
    --
    -- NONE        : Display the text without decoration, as-is. This is the
    --               lowest level of decoration.
    --
    -- TASK_IMAGE  : Prefix the text with the image of the current task.
    --
    -- SOURCE_UNIT : Prefix the text with the current task AND the calling
    --               source unit. This is the highest level of decoration.
    --
    type Decoration_Level is (NONE, TASK_IMAGE, SOURCE_UNIT);

    -- A debug channel contains related debugging messages from a specific
    -- system (e.g. GUI) or process (e.g. initialization). The verbosity of each
    -- debug channel can be controlled individually, to enable more messages
    -- from a system of interest.
    type Debug_Channel is mod 2**32;

    D_LOG      : constant Debug_Channel := 2**00;   -- logging
    D_AUDIO    : constant Debug_Channel := 2**01;   -- audio
    D_ENTITY   : constant Debug_Channel := 2**02;   -- entities
    D_SCRIPT   : constant Debug_Channel := 2**03;   -- script
    D_EVENTS   : constant Debug_Channel := 2**04;   -- events
    D_GAME     : constant Debug_Channel := 2**05;   -- game
    D_GUI      : constant Debug_Channel := 2**06;   -- gui
    D_INIT     : constant Debug_Channel := 2**07;   -- init
    D_INPUT    : constant Debug_Channel := 2**08;   -- input
    D_PHYSICS  : constant Debug_Channel := 2**09;   -- physics
    D_PREFS    : constant Debug_Channel := 2**10;   -- prefs
    D_PROCS    : constant Debug_Channel := 2**11;   -- procs
    D_RES      : constant Debug_Channel := 2**12;   -- res
    D_TILES    : constant Debug_Channel := 2**13;   -- tiles
    D_VIEW     : constant Debug_Channel := 2**14;   -- view
    D_APP      : constant Debug_Channel := 2**15;   -- app
    D_WORLD    : constant Debug_Channel := 2**16;   -- world
    D_FONT     : constant Debug_Channel := 2**17;   -- font
    D_PARTICLES: constant Debug_Channel := 2**18;   -- particles
    D_ASSET    : constant Debug_Channel := 2**19;   -- asset
    D_FOCUS    : constant Debug_Channel := 2**20;   -- focus (GUI input focus)
    D_DEV      : constant Debug_Channel := 2**31;   -- dev

    NO_CHANNELS  : constant Debug_Channel := 0;
    ALL_CHANNELS : constant Debug_Channel := 2#0_1111111111111111111111111111111#;

    -- A prototype for the debugging output procedure.
    type Output_Proc is access procedure( str : String );

    ----------------------------------------------------------------------------

    -- Sends a line of text to debugging output. Do not specify 'entity' or
    -- 'location'; they are used to determine the calling location in the code.
    procedure Dbg( str      : String;
                   channel  : Debug_Channel := D_DEV;
                   level    : Verbosity_Level := Never;
                   entity   : String := GNAT.Source_Info.Enclosing_Entity;
                   location : String := GNAT.Source_Info.Source_Location );

    -- Sends information from an exception to debugging output. A stack
    -- traceback will also be displayed, if available (Windows only.) Do not
    -- specify 'entity' or 'location'; they are used to determine the calling
    -- location in the code.
    procedure Dbg( e        : Exception_Occurrence;
                   channel  : Debug_Channel := D_DEV;
                   level    : Verbosity_Level := Never;
                   entity   : String := GNAT.Source_Info.Enclosing_Entity;
                   location : String := GNAT.Source_Info.Source_Location );

    -- Enables additional channels 'channels' for all subsequent output messages
    -- until disabled. This is useful for including all messages on a specific
    -- channel during a certain process. Example: Add D_INIT as a debug channel
    -- to all messages generating during application initialization.
    procedure Enable_Debug_Channels( channels : Debug_Channel );

    -- Disables channels 'channels' that were previously enabled by a call to
    -- Enable_Debug_Channels().
    procedure Disable_Debug_Channels( channels : Debug_Channel );

    -- Sets the decoration level for output to 'level'. The default decoration
    -- level is NONE.
    procedure Set_Decoration( level : Decoration_Level );

    -- Sets the path of the debug output log file. If the file already exists,
    -- output will be appended.
    procedure Set_Log_File( filePath : String );

    -- Sets the procedure to receive debugging output. The default output is
    -- null. The application should call this during elaboration, if at all
    -- possible.
    procedure Set_Output( proc : Output_Proc );

    -- Sets the verbosity to 'level' for all channels 'channels'. The default
    -- channel verbosity is WARNING for debug builds and ERROR for release builds.
    procedure Set_Verbosity( channels : Debug_Channel;
                             level    : Verbosity_Level );

end Debugging;
