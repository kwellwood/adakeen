--
-- Copyright (c) 2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Scribble.Runtimes;                 use Scribble.Runtimes;
with Values.Lists;                      use Values.Lists;

package Assets.Scripts is

    -- A Script_Asset contains a compiled script (represented with a specially
    -- formatted Scribble map value), compiled from source code or loaded from a
    -- Scribble Compiled Object (.sco) file. Source code will be preferred if
    -- both formats are present.
    type Script_Asset is new Asset with private;
    type A_Script_Asset is access all Script_Asset'Class;

    function Load_Script( filePath : String; group : String ) return A_Script_Asset;
    pragma Postcondition( Load_Script'Result /= null );

    -- Returns a reference to the script's members map, not a copy. DO NOT
    -- modify it! If the asset is not loaded, an Error will be returned
    -- describing the most recent load failure.
    function Get_Members( this : not null access Script_Asset'Class ) return Map_Value;

    -- Returns a reference to the script's timers list, not a copy. DO NOT
    -- modify it! If the asset is not loaded, an Error will be returned
    -- describing the most recent load failure.
    function Get_Timers( this : not null access Script_Asset'Class ) return List_Value;

    procedure Delete( this : in out A_Script_Asset );
    pragma Postcondition( this = null );

    ----------------------------------------------------------------------------

    procedure Initialize( runtime : not null A_Scribble_Runtime );

    procedure Finalize;

private

    type Script_Asset is new Asset with
        record
            members : Map_Value;
            timers  : List_Value;
        end record;

    procedure Construct( this     : access Script_Asset;
                         assetId  : Value'Class;
                         filePath : String;
                         group    : String );

    function Load_Data( this : access Script_Asset ) return Boolean;

    procedure Unload_Data( this : access Script_Asset );

end Assets.Scripts;
