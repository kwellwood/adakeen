--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Ada.Containers.Indefinite_Hashed_Maps;
with Ada.Strings.Hash;
with Ada.Task_Identification;           use Ada.Task_Identification;
with Allegro.Configuration;             use Allegro.Configuration;
with Debugging;                         use Debugging;
with GNAT.OS_Lib;                       use GNAT.OS_Lib;
with Support;                           use Support;
with Support.Paths;                     use Support.Paths;
with Values.Casting;                    use Values.Casting;
with Values.Construction;               use Values.Construction;
with Values.Strings;                    use Values.Strings;

package body Preferences is

    package Defaults is new
        Ada.Containers.Indefinite_Hashed_Maps( String, Unbounded_String,
                                               Ada.Strings.Hash, "=", "=" );
    use Defaults;

    protected Protected_Config is

        function Get_Pref( section, name : String;
                           secondDefault : Boolean ) return Boolean;

        function Get_Pref( section, name : String;
                           secondDefault : Float ) return Float;

        function Get_Pref( section, name : String;
                           secondDefault : Integer ) return Integer;

        function Get_Pref( section, name : String;
                           secondDefault : String ) return String;

        function Get_Pref( section, name : String ) return Value;

        procedure Set_Pref( section, name : String; value : Float );

        procedure Set_Pref( section, name : String; value : Integer );

        procedure Set_Pref( section, name : String; value : String );

        procedure Set_Pref( section, name : String; val : Value'Class );

        procedure Register( name : String; value : String );

        procedure Load( path : String );

        procedure Save;

    private
        registry   : Defaults.Map;
        configPath : Unbounded_String;
        config     : A_Allegro_Config := Al_Create_Config;
    end Protected_Config;

    ----------------------------------------------------------------------------

    task type Auto_Save_Task is
        entry Initialize;
        entry Finalize;
        entry Schedule_Save;
    end Auto_Save_Task;
    type A_Auto_Save_Task is access all Auto_Save_Task;

    autoSave : A_Auto_Save_Task := null;

    procedure Schedule_Save;

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    protected body Protected_Config is

        function Get_Default( section, name : String;
                              secondDefault : Boolean ) return Boolean is
            key : constant String := To_Lower( section & "." & name );
            pos : constant Defaults.Cursor := registry.Find( key );
            val : Boolean;
        begin
            if Has_Element( pos ) then
                val := Boolean'Value( To_String( Element( pos ) ) );
                if val'Valid then
                    return val;
                end if;
                Dbg( "Default is not valid for preference <" & key & '>', D_PREFS, Warning );
            else
                null;
                pragma Debug( Dbg( "Default not found for preference <" & key & '>', D_PREFS, Info ) );
            end if;
            return secondDefault;
        exception
            when Constraint_Error =>
                -- "true" is required for a string default value to be
                -- considered True
                return False;
        end Get_Default;

        ------------------------------------------------------------------------

        function Get_Default( section, name : String;
                              secondDefault : Float ) return Float is
            key : constant String := To_Lower( section & "." & name );
            pos : constant Defaults.Cursor := registry.Find( key );
            val : Float;
        begin
            if Has_Element( pos ) then
                val := Float'Value( To_String( Element( pos ) ) );
                if val'Valid then
                    return val;
                end if;
                Dbg( "Default is not valid for preference <" & key & '>', D_PREFS, Warning );
            else
                null;
                pragma Debug( Dbg( "Default not found for preference <" & key & '>', D_PREFS, Info ) );
            end if;
            return secondDefault;
        end Get_Default;

        ------------------------------------------------------------------------

        function Get_Default( section, name : String;
                              secondDefault : Integer ) return Integer is
            key : constant String := To_Lower( section & "." & name );
            pos : constant Defaults.Cursor := registry.Find( key );
            val : Integer;
        begin
            if Has_Element( pos ) then
                val := Integer'Value( To_String( Element( pos ) ) );
                if val'Valid then
                    return val;
                end if;
                Dbg( "Default is not valid for preference <" & key & '>', D_PREFS, Warning );
            else
                null;
                pragma Debug( Dbg( "Default not found for preference <" & key & '>', D_PREFS, Info ) );
            end if;
            return secondDefault;
        end Get_Default;

        ------------------------------------------------------------------------

        function Get_Default( section, name : String;
                              secondDefault : String ) return String is
            key : constant String := To_Lower( section & "." & name );
            pos : constant Defaults.Cursor := registry.Find( key );
        begin
            if Has_Element( pos ) then
                return To_String( Element( pos ) );
            end if;
            pragma Debug( Dbg( "Default not found for preference <" & key & '>', D_PREFS, Info ) );
            return secondDefault;
        end Get_Default;

        ------------------------------------------------------------------------

        function Get_Pref( section, name : String;
                           secondDefault : Boolean ) return Boolean is
            defint : Integer := 0;
        begin
            if Get_Default( section, name, secondDefault ) then
                defint := 1;
            end if;
            declare
                val : constant String := Al_Get_Config_Value( config, section, name );
            begin
                if val'Length > 0 then
                    return Integer'Value( val ) /= 0;
                end if;
                return defint /= 0;
            exception
                when others =>
                    return defint /= 0;
            end;
        end Get_Pref;

        ------------------------------------------------------------------------

        function Get_Pref( section, name : String;
                           secondDefault : Float ) return Float is
            val : constant String := Al_Get_Config_Value( config, section, name );
        begin
            if val'Length > 0 then
                return Float'Value( val );
            end if;
            return Get_Default( section, name, secondDefault );
        exception
            when others =>
                return Get_Default( section, name, secondDefault );
        end Get_Pref;

        ------------------------------------------------------------------------

        function Get_Pref( section, name : String;
                           secondDefault : Integer ) return Integer is
            val : constant String := Al_Get_Config_Value( config, section, name );
        begin
            if val'Length > 0 then
                return Integer'Value( val );
            end if;
            return Get_Default( section, name, secondDefault );
        exception
            when others =>
                return Get_Default( section, name, secondDefault );
        end Get_Pref;

        ------------------------------------------------------------------------

        function Get_Pref( section, name : String;
                           secondDefault : String ) return String is
            val : constant String := Al_Get_Config_Value( config, section, name );
        begin
            if val'Length > 0 then
                return val;
            end if;
            return Get_Default( section, name, secondDefault );
        end Get_Pref;

        ------------------------------------------------------------------------

        function Get_Pref( section, name : String ) return Value is

            --------------------------------------------------------------------

            function Text_To_Value( txt : String ) return Value is
                f : Float;
                b : Boolean;
            begin
                if txt'Length = 0 then
                    return Create( "" );                           -- undefined/empty string
                end if;
                f := Float'Value( txt );                           -- number
                return Create( f );
            exception
                when others =>
                    b := Case_Eq( txt, "true" );
                    if b or else Case_Eq( txt, "false" ) then
                        return Create( b );                        -- boolean
                    end if;
                    return Create( txt );                          -- string
            end Text_To_Value;

            --------------------------------------------------------------------

            val : constant String := Al_Get_Config_Value( config, section, name );
        begin
            if val'Length > 0 then
                return Text_To_Value( val );
            else
                return Text_To_Value( Get_Default( section, name, "" ) );
            end if;
        end Get_Pref;

        ------------------------------------------------------------------------

        procedure Set_Pref( section, name : String; value : Float ) is
        begin
            Al_Set_Config_Value( config, section, name, Float'Image( value ) );
        end Set_Pref;

        ------------------------------------------------------------------------

        procedure Set_Pref( section, name : String; value : Integer ) is
        begin
            Al_Set_Config_Value( config, section, name, Integer'Image( value ) );
        end Set_Pref;

        ------------------------------------------------------------------------

        procedure Set_Pref( section, name : String; value : String ) is
        begin
            Al_Set_Config_Value( config, section, name, value );
        end Set_Pref;

        ------------------------------------------------------------------------

        procedure Set_Pref( section, name : String; val : Value'Class ) is
        begin
            Al_Set_Config_Value( config, section, name, Coerce_String( val ) );
        end Set_Pref;

        ------------------------------------------------------------------------

        procedure Register( name : String; value : String ) is
            pos : Defaults.Cursor := registry.Find( name );
        begin
            if Has_Element( pos ) then
                Dbg( "Overriding default for preference <" & name & '>', D_PREFS, Warning );
                registry.Delete( pos );
            end if;
            pragma Debug( Dbg( "Default preference <" & name & "> = '" & value & "'", D_PREFS, Info ) );
            registry.Insert( name, To_Unbounded_String( value ) );
        end Register;

        ------------------------------------------------------------------------

        procedure Load( path : String ) is
        begin
            Save;
            Al_Destroy_Config( config );
            configPath := To_Unbounded_String( Normalize_Pathname( path ) );
            pragma Debug( Dbg( "Loading config file: " & To_String( configPath ), D_PREFS, Info ) );
            config := Al_Load_Config_File( path );
            if config = null then
                -- failed to load config from disk; create an empty config
                config := Al_Create_Config;
            end if;
        end Load;

        ------------------------------------------------------------------------

        procedure Save is
        begin
            if config /= null and then Length( configPath ) > 0 then
                if not Al_Save_Config_File( To_String( configPath ), config ) then
                    Dbg( "Failed to write config file: " & To_String( configPath ), D_PREFS, Error );
                end if;
            end if;
        end Save;

    end Protected_Config;

    --==========================================================================

    task body Auto_Save_Task is
        initialized : Boolean := False;
        quit        : Boolean := False;
        save        : Boolean := False;

        procedure Do_Save is
        begin
            if initialized then
                Protected_Config.Save;
                save := False;
            end if;
        end Do_Save;

    begin
        while not quit loop
            select
                accept Initialize do
                    initialized := True;
                end Initialize;
            or
                accept Finalize do
                    if save then
                        Do_Save;
                    end if;
                    quit := True;
                end Finalize;
            or
                accept Schedule_Save do
                    save := True;
                end Schedule_Save;
            or
                delay 10.0;
                if save then
                    Do_Save;
                end if;
            end select;
        end loop;

        pragma Debug( Dbg( "Preferences.Auto_Save task complete", D_PREFS, Info ) );
    exception
        when e : others =>
            Dbg( "Preferences.Auto_Save Exception: ...", D_PREFS, Error );
            Dbg( e, D_PREFS, Error );
    end Auto_Save_Task;

    ----------------------------------------------------------------------------

    procedure Schedule_Save is
    begin
        if autoSave /= null then
            autoSave.Schedule_Save;
        end if;
    end Schedule_Save;

    --==========================================================================

    procedure Initialize( filename : String ) is
    begin
        if autoSave = null then
            if not Is_Directory( Get_Directory( Normalize_Pathname( filename ) ) ) then
                if not Make_Dir( Get_Directory( Normalize_Pathname( filename ) ) ) then
                    -- Failed to create the directory for the config file.
                    Dbg( "Failed to create config directory: " &
                         Get_Directory( Normalize_Pathname( filename ) ),
                         D_PREFS, Warning );
                end if;
            end if;
            Protected_Config.Load( filename );
            autoSave := new Auto_Save_Task;
            autoSave.Initialize;
        end if;
    end Initialize;

    ----------------------------------------------------------------------------

    procedure Finalize is
    begin
        if autoSave = null or else Is_Terminated( autoSave.all'Identity ) then
            return;
        end if;

        pragma Debug( Dbg( "Finalizing preference system", D_PREFS, Info ) );
        autoSave.Finalize;

        for count in 1..10 loop
            if Is_Terminated( autoSave.all'Identity ) then
                exit;
            end if;
            delay 0.1;
        end loop;
        if not Is_Terminated( autoSave.all'Identity ) then
            Dbg( "Preferences Auto_Save task failed to stop", D_PREFS, Error );
        end if;
    end Finalize;

    --==========================================================================

    function Get_Pref( section, name : String;
                       secondDefault : Boolean := False ) return Boolean
    is (Protected_Config.Get_Pref( section, name, secondDefault ));

    ----------------------------------------------------------------------------

    function Get_Pref( section, name : String;
                       secondDefault : Float := 0.0 ) return Float
    is (Protected_Config.Get_Pref( section, name, secondDefault ));

    ----------------------------------------------------------------------------

    function Get_Pref( section, name : String;
                       secondDefault : Integer := 0 ) return Integer
    is (Protected_Config.Get_Pref( section, name, secondDefault ));

    ----------------------------------------------------------------------------

    function Get_Pref( section, name : String;
                       secondDefault : String := "" ) return String
    is (Protected_Config.Get_Pref( section, name, secondDefault ));

    ----------------------------------------------------------------------------

    function Get_Pref( section, name : String ) return Value
    is (Protected_Config.Get_Pref( section, name ));

    --==========================================================================

    procedure Set_Pref( name : String; value : Boolean ) is
    begin
        Set_Pref( "preference", name, value );
    end Set_Pref;

    ----------------------------------------------------------------------------

    procedure Set_Pref( name : String; value : Float ) is
    begin
        Set_Pref( "preference", name, value );
    end Set_Pref;

    ----------------------------------------------------------------------------

    procedure Set_Pref( name : String; value : Integer ) is
    begin
        Set_Pref( "preference", name, value );
    end Set_Pref;
    ----------------------------------------------------------------------------

    procedure Set_Pref( name : String; value : String ) is
    begin
        Set_Pref( "preference", name, value );
    end Set_Pref;

    ----------------------------------------------------------------------------

    procedure Set_Pref( section, name : String; value : Boolean ) is
        valint : Integer := 0;
    begin
        if value then
            valint := 1;
        end if;
        Protected_Config.Set_Pref( section, name, valint );
        Schedule_Save;
    end Set_Pref;

    ----------------------------------------------------------------------------

    procedure Set_Pref( section, name : String; value : Float ) is
    begin
        Protected_Config.Set_Pref( section, name, value );
        Schedule_Save;
    end Set_Pref;

    ----------------------------------------------------------------------------

    procedure Set_Pref( section, name : String; value : Integer ) is
    begin
        Protected_Config.Set_Pref( section, name, value );
        Schedule_Save;
    end Set_Pref;

    ----------------------------------------------------------------------------

    procedure Set_Pref( section, name : String; value : String ) is
    begin
        Protected_Config.Set_Pref( section, name, value );
        Schedule_Save;
    end Set_Pref;

    ----------------------------------------------------------------------------

    procedure Set_Pref( section, name : String; val : Value'Class ) is
    begin
        Protected_Config.Set_Pref( section, name, val );
        Schedule_Save;
    end Set_Pref;

    --==========================================================================

    procedure Set_Default( name : String; value : Boolean ) is
    begin
        Set_Default( "preference", name, value );
    end Set_Default;

    ----------------------------------------------------------------------------

    procedure Set_Default( name : String; value : Float ) is
    begin
        Set_Default( "preference", name, value );
    end Set_Default;

    ----------------------------------------------------------------------------

    procedure Set_Default( name : String; value : Integer ) is
    begin
        Set_Default( "preference", name, value );
    end Set_Default;

    ----------------------------------------------------------------------------

    procedure Set_Default( name : String; value : String ) is
    begin
        Set_Default( "preference", name, value );
    end Set_Default;

    ----------------------------------------------------------------------------

    procedure Set_Default( section, name : String; value : Boolean ) is
    begin
        if value then
            Protected_Config.Register( To_Lower( section & "." & name ), "true" );
        else
            Protected_Config.Register( To_Lower( section & "." & name ), "false" );
        end if;
    end Set_Default;

    ----------------------------------------------------------------------------

    procedure Set_Default( section, name : String; value : Float ) is
    begin
        Protected_Config.Register( To_Lower( section & "." & name ), Float'Image( value ) );
    end Set_Default;

    ----------------------------------------------------------------------------

    procedure Set_Default( section, name : String; value : Integer ) is
    begin
        Protected_Config.Register( To_Lower( section & "." & name ), Integer'Image( value ) );
    end Set_Default;

    ----------------------------------------------------------------------------

    procedure Set_Default( section, name : String; value : String ) is
    begin
        Protected_Config.Register( To_Lower( section & "." & name ), value );
    end Set_Default;

end Preferences;
