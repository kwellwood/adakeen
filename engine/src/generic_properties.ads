--
-- Copyright (c) 2018-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Indefinite_Holders;
with Ada.Real_Time;                     use Ada.Real_Time;
with Easing;                            use Easing;
with Objects;                           use Objects;
with Processes;                         use Processes;
with Processes.Managers;                use Processes.Managers;
with Signals;                           use Signals;

generic

    type Value_Type is private;

    with function Interpolate( first, last : Value_Type; progress : Float ) return Value_Type;

package Generic_Properties is

    type Abstract_Accessor is abstract tagged null record;

    -- Returns an access to the object that owns the property that is being accessed.
    not overriding
    function Get_Owner( this : Abstract_Accessor ) return access Base_Object'Class is abstract;

    not overriding
    function Read( this : Abstract_Accessor ) return Value_Type is abstract;

    not overriding
    procedure Write( this : Abstract_Accessor; val : Value_Type ) is abstract;

    ----------------------------------------------------------------------------

    generic

        type Owner (<>) is abstract limited new Base_Object with private;

    package Accessors is

        type A_Reader is access function( obj : not null access Owner'Class ) return Value_Type;

        type A_Writer is access procedure( obj : not null access Owner'Class; val : Value_Type );

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        type Accessor is new Abstract_Accessor with
            record
                obj    : access Owner'Class;
                reader : A_Reader := null;
                writer : A_Writer := null;
            end record;

        overriding
        function Get_Owner( this : Accessor ) return access Base_Object'Class;

        overriding
        function Read( this : Accessor ) return Value_Type;

        overriding
        procedure Write( this : Accessor; val : Value_Type );

    end Accessors;

    ----------------------------------------------------------------------------

    type Property is new Limited_Object and Process with private;

    procedure Construct( this     : access Property;
                         pman     : not null A_Process_Manager;
                         accessor : Abstract_Accessor'Class );

    -- Returns True if the Property is animating its current value over time,
    -- indicating that the value may change on the next Tick.
    function Animating( this : not null access Property'Class ) return Boolean;

    -- This signal is emitted when the Property animation finishes normally,
    -- during the Tick. It is not emitted when the animation is interrupted,
    -- changed, or prematurely stopped. The value of the Property is guaranteed
    -- to match its target.
    function Finished( this : not null access Property'Class ) return access Signal'Class;

    -- Returns the current value of the Property.
    function Get( this : not null access Property'Class ) return Value_Type;

    -- Returns a pointer to the object that owns the Property, e.g. a Widget.
    function Get_Owner( this : not null access Property'Class ) return access Base_Object'Class;

    -- Returns the target value of the Property's animation. If the Property is
    -- not animating, the current value will be returned.
    function Get_Target( this : not null access Property'Class ) return Value_Type;

    -- Returns the amount of time remaining in the animation, or zero if none.
    -- To determine if the Property is currently animating, call .Animating
    -- instead. The Property may return zero time while it is still animating,
    -- just before its last frame completes.
    function Remaining( this : not null access Property'Class ) return Time_Span;

    -- Sets the new value of this Property to 'val'. If 'duration' is greater
    -- than zero, it will not change immediately. It will change over time,
    -- according to the given 'easing' function. This Property will emit its
    -- Finished signal when the animation completes. If 'duration' is zero or
    -- less, the value will change immediately.
    procedure Set( this     : not null access Property'Class;
                   val      : Value_Type;
                   duration : Time_Span := Time_Span_Zero;
                   easing   : Easing_Type := Linear );

    -- Immediately stops animation of the Property at the current value. The
    -- Finished signal will not be emitted.
    procedure Stop( this : not null access Property'Class );

private

    package Abstract_Accessors is new Ada.Containers.Indefinite_Holders(Abstract_Accessor'Class, "=");

    type Property is new Limited_Object and Process with
        record
            pman        : A_Process_Manager := null;
            accessor    : Abstract_Accessors.Holder;
            start       : Value_Type;
            target      : Value_Type;
            elapsed     : Time_Span := Time_Span_Zero;
            duration    : Time_Span := Time_Span_Zero;
            easing      : Easing_Type := Linear;
            sigFinished : aliased Signal;
        end record;

    procedure Delete( this : in out Property );

    overriding
    function Get_Process_Name( this : access Property ) return String is ("AnimatedProperty");

    procedure Tick( this : access Property; time : Tick_Time );

end Generic_Properties;
