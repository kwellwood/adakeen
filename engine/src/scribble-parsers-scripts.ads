--
-- Copyright (c) 2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Scribble.Ast.Scripts;              use Scribble.Ast.Scripts;

package Scribble.Parsers.Scripts is

    -- SCRIPT GRAMMAR
    ----------------------------------------------------------------------------
    -- SCRIPT
    --
    -- Script ::= { Script_Element }
    --
    -- Script_Element ::= {';'} (
    --                    Include
    --                    | Member
    --                    | Timer
    --                    ) {';'}
    --
    -- Include ::= 'include' string-value-token
    --
    ----------------------------------------------------------------------------
    -- MEMBERS
    --
    -- Member            ::= (Required_Member | Member_Function | Member_Var) ';'
    --   Required_Member ::= 'require' Identifier
    --   Member_Function ::= Named_Member_Definition
    --   Member_Var      ::= ['default'] Identifier ':=' Expression<literal=True>
    --
    ----------------------------------------------------------------------------
    -- TIMERS
    --
    -- Timer ::= 'timer' Identifier ['(' Expression<literal=True> ')'] ('in' | 'every') number-value-token ';'
    --
    ----------------------------------------------------------------------------

    -- A script parser can parse the elements of a script from a text stream:
    -- member functions, member variables, and timers. The script compiler then
    -- compiles the abstract syntax trees created by the parser.
    type Script_Parser is new Parser with private;
    type A_Script_Parser is access all Script_Parser'Class;

    -- Creates a new script parser.
    function Create_Script_Parser( runtime : not null A_Scribble_Runtime ) return A_Script_Parser;
    pragma Postcondition( Create_Script_Parser'Result /= null );

    -- Parses and returns a script.
    function Parse_Script( this : not null access Script_Parser'Class ) return A_Ast_Script;

    -- Deletes the parser.
    procedure Delete( this : in out A_Script_Parser );
    pragma Postcondition( this = null );

private

    type Script_Parser is new Parser with
        record
            child : A_Script_Parser := null;        -- for parsing include files
        end record;

    procedure Delete( this : in out Script_Parser );

    procedure Parse_Script( this   : not null access Script_Parser'Class;
                            script : not null A_Ast_Script );

    -- Parses a single script element, consuming an leading or trailing
    -- semicolons. See the grammar below. If no script element is found, null
    -- will be returned. A Parse_Error will be raised if the language syntax is
    -- violated; for example, if an expected grammar term is not found, or if a
    -- malformed token is encountered by the scanner.
    --
    -- Script_Element ::= {';'} (
    --                    Include
    --                    | Member
    --                    | Timer
    --                    ) {';'}
    --
    function Scan_Script_Element( this   : not null access Script_Parser'Class;
                                  script : not null A_Ast_Script ) return Boolean;

    -- Parses an include element. If found and the include file exists, its
    -- contents will be parsed into 'script' and True will be returned. See the
    -- grammar below. If no 'include' is found, False will be returned. A
    -- Parse_Error will be raised if the language syntax is violated, if a
    -- malformed token is encountered by the scanner, if the referenced file
    -- cannot be opened, or if it contains a parse error.
    --
    -- Include ::= 'include' string-value-token ';'
    --
    -- If 'include' is found then all of the following terms are expected.
    function Scan_Include( this   : not null access Script_Parser'Class;
                           script : not null A_Ast_Script ) return Boolean;

    -- Parses a script member element, including functions, variables, defaults,
    -- and requirements. See the grammar below. If no member declaration is
    -- found, null will be returned. A Parse_Error will be raised if the
    -- language syntax is violated; for example, if an expected grammar term is
    -- not found, or if a malformed token is encountered by the scanner.
    --
    -- Member ::= (Required_Member | Member_Function | Member_Var) ';'
    --
    function Scan_Member( this : not null access Script_Parser'Class ) return A_Ast_Script_Element;

    -- Parses a member variable element. It may provide a fixed value, or an
    -- overwritable default value. See the grammar below. If no script member
    -- variable is found, null will be returned. A Parse_Error will be raised if
    -- the language syntax is violated; for example, if an expected grammar term
    -- is not found, or if a malformed token is encountered by the scanner.
    --
    -- Member_Var ::= ['default'] Identifier ':=' Expression<literal=True>
    --
    -- If 'default' or Identifier is found, then all subsequent terms are
    -- expected.
    function Scan_Member_Var( this : not null access Script_Parser'Class ) return A_Ast_Script_Element;

    -- Parses a required member script element. See the grammar below. If no
    -- requirement element is found, null will be returned. A Parse_Error will
    -- be raised if the language syntax is violated; for example, if an expected
    -- grammar term is not found, or if a malformed token is encountered by the
    -- scanner.
    --
    -- Required_Member ::= 'require' Identifier
    --
    function Scan_Required_Member( this : not null access Script_Parser'Class ) return A_Ast_Script_Element;

    -- Parses a timer element. See the grammar below. If no 'timer' is found,
    -- null will be returned. A Parse_Error will be raised if the language
    -- syntax is violated; for example, if an expected grammar term is not found,
    -- or if a malformed token is encountered by the scanner.
    --
    -- Timer ::= 'timer' Identifier ['(' Expression<literal=True> ')'] ('in' | 'every') number-value-token ';'
    --
    -- If 'timer' is found then all of the following terms are expected.
    function Scan_Timer( this : not null access Script_Parser'Class ) return A_Ast_Script_Element;

end Scribble.Parsers.Scripts;
