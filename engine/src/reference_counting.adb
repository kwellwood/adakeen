--
-- Copyright (c) 2012-2013 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Unchecked_Deallocation;

package body Reference_Counting is

    package body Atomic is

        function Add( ptr : access Integer_32;
                      val : Integer_32 ) return Integer_32 is

            function Intrinsic_Sync_Add_And_Fetch( ptr : access Integer_32;
                                                   val : Integer_32 ) return Integer_32;
            pragma Import( Intrinsic, Intrinsic_Sync_Add_And_Fetch, "__sync_add_and_fetch_4" );
        begin
            return Intrinsic_Sync_Add_And_Fetch( ptr, val );
        end Add;

    end Atomic;

    --==========================================================================

    package body Smart_Pointers is

        overriding
        procedure Adjust( this : in out Ref ) is
            tmp : Integer_32;
            pragma Unreferenced( tmp );
        begin
            if this.target /= null then
                tmp := Atomic.Add( this.target.refs'Access, 1 );
            end if;
        end Adjust;

        ------------------------------------------------------------------------

        overriding
        procedure Finalize( this : in out Ref ) is
            procedure Free is new Ada.Unchecked_Deallocation( Reference_Counted'Class,
                                                              A_Reference_Counted );

            target : A_Reference_Counted := this.target;
        begin
            this.target := null;

            if target /= null then
                if Atomic.Add( target.refs'Access, -1 ) = 0 then
                    Delete( target.all );
                    Free( target );
                end if;
            end if;
        end Finalize;

        ------------------------------------------------------------------------

        function Get( this : Ref ) return A_Encapsulated is (A_Encapsulated(this.target));

        ------------------------------------------------------------------------

        function Get_Refcount( this : Ref ) return Natural
        is (if this.target /= null then Natural(this.target.refs) else 0);

        ------------------------------------------------------------------------

        procedure Set( this : in out Ref; target : access Encapsulated'Class ) is
        begin
            if this.target = A_Reference_Counted(target) then
                -- the target isn't changing
                return;
            end if;

            if this.target /= null then
                this.Finalize;     -- decrement reference count
            end if;

            if target /= null then
                this.target := A_Reference_Counted(target);
                this.Adjust;       -- increment reference count
            end if;
        end Set;

        ------------------------------------------------------------------------

        procedure Set( this : in out Ref; target : Encapsulated'Class ) is
            tmp : constant A_Encapsulated := new Encapsulated'Class'(target);
        begin
            this.Set( tmp );
        end Set;

        ------------------------------------------------------------------------

        overriding
        function "="( l, r : Ref ) return Boolean is
        begin
            if l.target = null then
                return r.target = null;
            end if;

            return r.target /= null and then
                  (l.target = r.target or else l.target.all = r.target.all);
        end "=";

    end Smart_Pointers;

end Reference_Counting;
