--
-- Copyright (c) 2015-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Signals is

    use Connection_Lists;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Base_Connection; priority : Priority_Type ) is
    begin
        Object(this.all).Construct;
        this.priority := priority;
    end Construct;

    --==========================================================================

    procedure Init( this   : in out Base_Signal'Class;
                    sender : access Base_Object'Class := null ) is
    begin
        this.Construct;
        this.sender := sender;
    end Init;

    ----------------------------------------------------------------------------

    procedure Connect( this : in out Base_Signal'Class; slot : Base_Connection'Class ) is
        pos : Cursor;
    begin
        pragma Assert( (slot.match and HAS_OBJECT) > 0 );
        pragma Assert( (slot.match and HAS_METHOD) > 0 );

        -- connections are ordered by priority and then by time of connection
        if not this.conns.Contains( slot ) then
            pos := this.conns.Last;
            while Has_Element( pos ) loop
                -- from the tail of the list, if the next connection has an
                -- equal or greater priority, then we insert after it.
                if this.conns.Constant_Reference( pos ).priority >= slot.priority then
                    Next( pos );
                    if Has_Element( pos ) then
                        this.conns.Insert( pos, slot );
                    else
                        -- appending to the end of the connection list
                        this.conns.Append( slot );
                    end if;
                    return;
                end if;
                Previous( pos );
            end loop;
            -- connection list is empty or 'slot' is the highest priority
            this.conns.Prepend( slot );
            return;
        end if;
    end Connect;

    ----------------------------------------------------------------------------

    procedure Disconnect( this : in out Base_Signal'Class; slot : Base_Connection'Class ) is
        pos, pos2 : Cursor;
    begin
        -- match exact connection to 'slot'
        if slot.match = MATCH_EXACT then
            pos := this.conns.Find( slot );
            if Has_Element( pos ) then
                this.conns.Delete( pos );
            end if;

        -- match all connections to the same object
        elsif slot.match >= MATCH_OBJECT then
            pos := this.conns.First;
            while Has_Element( pos ) loop
                pos2 := Next( pos );
                if slot.Get_Object = this.conns.Constant_Reference( pos ).Get_Object then
                    this.conns.Delete( pos );
                end if;
                pos := pos2;
            end loop;

        -- match all connections
        else -- MATCH_ALL
            while not this.conns.Is_Empty loop
                this.conns.Delete_First;
            end loop;
        end if;
    end Disconnect;

    ----------------------------------------------------------------------------

    procedure Disconnect_All( this : in out Base_Signal'Class ) is
        package Conns is new Connections(Base_Object);
        use Conns;
    begin
        this.Disconnect( All_Slots );
    end Disconnect_All;

    ----------------------------------------------------------------------------

    procedure Disconnect_All( this : in out Base_Signal'Class; target : in out Base_Object'Class ) is
        package Conns is new Connections(Base_Object);
        use Conns;
    begin
        this.Disconnect( All_Slots_Of( target ) );
    end Disconnect_All;

    ----------------------------------------------------------------------------

    procedure Disconnect_All( this : in out Base_Signal'Class; target : not null access Base_Object'Class ) is
    begin
        this.Disconnect_All( target.all );
    end Disconnect_All;

    --==========================================================================

    procedure Connect( this : in out Signal'Class; slot : Connection'Class ) is
    begin
        this.Connect( Base_Connection(slot) );
    end Connect;

    ----------------------------------------------------------------------------

    procedure Emit( this : Signal'Class ) is
        conns : constant Connection_Lists.List := this.conns.Copy;
    begin
        for c of conns loop
            Connection'Class(c).Emit( this.sender );
            exit when c.priority = Exclusive;
        end loop;
    end Emit;

    --==========================================================================

-- vvvv resume editing here vvvv
    package body Connections is

        function All_Slots return Base_Connection'Class is
            result : aliased Generic_Connection(Proto0);
        begin
            result.Construct( Low );
            result.match := MATCH_ANY;
            return result;
        end All_Slots;

        ------------------------------------------------------------------------

        function All_Slots_Of( obj : in out Target'Class ) return Base_Connection'Class is
            result : aliased Generic_Connection(Proto0);
        begin
            result.Construct( Low );
            result.match := MATCH_OBJECT;
            result.object := obj'Unchecked_Access;        -- UNSAFE! (see below)
            return result;
        end All_Slots_Of;

        ------------------------------------------------------------------------

        function Slot( obj      : in out Target'Class;
                       method   : not null A_Method0;
                       priority : Priority_Type := Normal ) return Connection'Class is
            result : aliased Generic_Connection(Proto0);
        begin
            Connection(result).Construct( priority );

            -- there be dragons here:
            -- obj had better exist on the heap because we're going to store an
            -- unchecked (unsafe) access to it. this is necessary because we may
            -- want to disconnect signals in a dispatching method, which might call
            -- Slot like this:
            --
            -- procedure Deactivate( this : access Object ) is
            -- begin
            --     this.myButton.sig.Disconnect( Slot( this.all, method ) );
            --
            -- This could create an access check problem. If Deactivate was called
            -- from a destructor, ex: "procedure Delete( this : in out Object )"
            -- then the 'this' pointer would definitely not be allowed in the
            -- assignment below without using unchecked access.
            result.object := obj'Unchecked_Access;              -- UNSAFE!
            result.method0 := method;
            return result;
        end Slot;

        ------------------------------------------------------------------------

        overriding
        procedure Emit( this   : Generic_Connection;
                        sender : access Base_Object'Class ) is
        begin
            this.object.Push_Signaller( sender );
            case this.proto is
                when Proto0 => this.method0( this.object );
            end case;
            this.object.Pop_Signaller;
        end Emit;

        ------------------------------------------------------------------------

        overriding
        function Eq( this : Generic_Connection; other : Base_Connection'Class ) return Boolean is
        begin
            if other in Generic_Connection'Class then
                return this.object = Generic_Connection(other).object and then
                       this.proto = Generic_Connection(other).proto and then
                       (case this.proto is
                           when Proto0 => (this.method0 = Generic_Connection(other).method0));
            end if;
            return False;
        end Eq;

    end Connections;

end Signals;
