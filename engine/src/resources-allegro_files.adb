--
-- Copyright (c) 2012-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Unchecked_Conversion;
with Ada.Unchecked_Deallocation;
with Allegro;                           use Allegro;
with Interfaces.C;                      use Interfaces.C;
with Interfaces.C.Strings;              use Interfaces.C.Strings;

package body Resources.Allegro_Files is

    type A_Stream_Element_Array is access all Stream_Element_Array;

    procedure Free is new Ada.Unchecked_Deallocation( Stream_Element_Array, A_Stream_Element_Array );

    ----------------------------------------------------------------------------

    type Memory_Info is
        record
            buf      : access Stream_Element_Array;
            offset   : Stream_Element_Offset;
            consumed : Boolean := False;
        end record;
    type A_Memory_Info is access all Memory_Info;
    pragma No_Strict_Aliasing( A_Memory_Info );

    ----------------------------------------------------------------------------

    procedure Delete( memInfo : in out A_Memory_Info ) is
        procedure Free is new Ada.Unchecked_Deallocation( Memory_Info, A_Memory_Info );
    begin
        if memInfo /= null then
            if memInfo.consumed then
                Free( A_Stream_Element_Array(memInfo.buf) );
            end if;
            Free( memInfo );
        end if;
    end Delete;

    ----------------------------------------------------------------------------

    function To_Memory_Info is new Ada.Unchecked_Conversion( Address, A_Memory_Info );

    procedure Mem_Move( dst, src : Address; len : Integer )
    with Import, Convention => C, External_Name => "memmove";

    ---------------------------------------------------------------------------

    function Mem_Open( path : C.Strings.chars_ptr;
                       mode : C.Strings.chars_ptr ) return Address
    with Convention => C;

    -- Warning: This does not open a usable file! Use Al_Create_File_Handle().
    function Mem_Open( path : C.Strings.chars_ptr;
                       mode : C.Strings.chars_ptr ) return Address is
        pragma Unreferenced( path, mode );
        userdata : constant A_Memory_Info := new Memory_Info'(null, 0, False);
    begin
        return userdata.all'Address;
    end Mem_Open;

    ---------------------------------------------------------------------------

    function Mem_Close( handle : A_Allegro_File ) return Bool
    with Convention => C;

    function Mem_Close( handle : A_Allegro_File ) return Bool is
        memInfo : A_Memory_Info := To_Memory_Info( Al_Get_File_Userdata( handle ) );
    begin
        Delete( memInfo );
        return B_TRUE;
    end Mem_Close;

    ---------------------------------------------------------------------------

    function Mem_Read( f    : A_Allegro_File;
                       ptr  : Address;
                       size : size_t ) return size_t
    with Convention => C;

    function Mem_Read( f    : A_Allegro_File;
                       ptr  : Address;
                       size : size_t ) return size_t is
        memInfo : constant A_Memory_Info := To_Memory_Info( Al_Get_File_Userdata( f ) );
        actual  : Integer := 0;
    begin
        if memInfo.offset <= memInfo.buf'Last then
            actual := Integer'Min( Integer(size), Integer(memInfo.buf'Last - memInfo.offset) + 1 );
            Mem_Move( ptr, memInfo.buf(memInfo.offset)'Address, actual );
            memInfo.offset := memInfo.offset + Stream_Element_Offset(actual);
        end if;
        return Interfaces.C.size_t(actual);
    end Mem_Read;

    ---------------------------------------------------------------------------

    function Mem_Write( f    : A_Allegro_File;
                        ptr  : Address;
                        size : size_t ) return size_t is (0)
    with Convention => C;

    ---------------------------------------------------------------------------

    function Mem_Flush( f : A_Allegro_File ) return Bool is (B_TRUE)
    with Convention => C;

    ---------------------------------------------------------------------------

    function Mem_Tell( f : A_Allegro_File ) return Integer_64
    with Convention => C;

    function Mem_Tell( f : A_Allegro_File ) return Integer_64 is
        memInfo : constant A_Memory_Info := To_Memory_Info( Al_Get_File_Userdata( f ) );
    begin
        return Integer_64(memInfo.offset - memInfo.buf'First);
    end Mem_Tell;

    ---------------------------------------------------------------------------

    function Mem_Seek( f      : A_Allegro_File;
                       offset : Integer_64;
                       whence : Allegro_Seek ) return Bool
    with Convention => C;

    function Mem_Seek( f      : A_Allegro_File;
                       offset : Integer_64;
                       whence : Allegro_Seek ) return Bool is
        memInfo : constant A_Memory_Info := To_Memory_Info( Al_Get_File_Userdata( f ) );
        result  : Boolean := False;
    begin
        case whence is
            when ALLEGRO_SEEK_SET =>
                if memInfo.buf'First + Stream_Element_Offset(offset) < memInfo.buf'First then
                    memInfo.offset := memInfo.buf'First;
                    result := False;
                elsif memInfo.buf'First + Stream_Element_Offset(offset) > memInfo.buf'Last then
                    memInfo.offset := memInfo.buf'Last + 1;     -- after the last byte
                    result := False;
                else
                    memInfo.offset := memInfo.buf'First + Stream_Element_Offset(offset);
                    result := True;
                end if;

            when ALLEGRO_SEEK_CUR =>
                if memInfo.offset + Stream_Element_Offset(offset) < memInfo.buf'First then
                    memInfo.offset := memInfo.buf'First;
                    result := False;
                elsif memInfo.offset + Stream_Element_Offset(offset) > memInfo.buf'Last + 1 then
                    memInfo.offset := memInfo.buf'Last + 1;     -- after the last byte
                    result := False;
                else
                    memInfo.offset := memInfo.offset + Stream_Element_Offset(offset);
                    result := True;
                end if;

            when ALLEGRO_SEEK_END =>
                if (memInfo.buf'Last + 1) + Stream_Element_Offset(offset) < memInfo.buf'First then
                    memInfo.offset := memInfo.buf'First;
                    result := False;
                elsif (memInfo.buf'Last + 1) + Stream_Element_Offset(offset) > memInfo.buf'Last + 1 then
                    memInfo.offset := memInfo.buf'Last + 1;     -- after the last byte
                    result := False;
                else
                    memInfo.offset := (memInfo.buf'Last + 1) + Stream_Element_Offset(offset);
                    result := True;
                end if;
        end case;
        if result then
            return B_TRUE;
        else
            return B_FALSE;
        end if;
    end Mem_Seek;

    ---------------------------------------------------------------------------

    function Mem_EOF( f : A_Allegro_File ) return Bool
    with Convention => C;

    function Mem_EOF( f : A_Allegro_File ) return Bool is
        memInfo : constant A_Memory_Info := To_Memory_Info( Al_Get_File_Userdata( f ) );
    begin
        if memInfo.offset > memInfo.buf'Last then
            return B_TRUE;
        else
            return B_FALSE;
        end if;
    end Mem_EOF;

    ---------------------------------------------------------------------------

    function Mem_Error( f : A_Allegro_File ) return Integer is (0)
    with Convention => C;

    ---------------------------------------------------------------------------

    function Mem_ErrMsg( f : A_Allegro_File ) return C.Strings.chars_ptr is (Null_Ptr)
    with Convention => C;

    ---------------------------------------------------------------------------

    procedure Mem_ClearErr( f : A_Allegro_File ) is null
    with Convention => C;

    ---------------------------------------------------------------------------

    function Mem_UngetC( f : A_Allegro_File; c : Integer ) return Integer is (-1)
    with Convention => C;

    ---------------------------------------------------------------------------

    function Mem_Size( f : A_Allegro_File ) return off_t
    with Convention => C;

    function Mem_Size( f : A_Allegro_File ) return off_t is
        memInfo : constant A_Memory_Info := To_Memory_Info( Al_Get_File_Userdata( f ) );
    begin
        return off_t(memInfo.buf'Length);
    end Mem_Size;

    ---------------------------------------------------------------------------

    MEMFILE_INTERFACE : constant A_Allegro_File_Interface :=
        new Allegro_File_Interface'(
            fopen     => Mem_Open'Access,
            fclose    => Mem_Close'Access,
            fread     => Mem_Read'Access,
            fwrite    => Mem_Write'Access,
            fflush    => Mem_Flush'Access,
            ftell     => Mem_Tell'Access,
            fseek     => Mem_Seek'Access,
            feof      => Mem_EOF'Access,
            ferror    => Mem_Error'Access,
            ferrmsg   => Mem_ErrMsg'Access,
            fclearerr => Mem_ClearErr'Access,
            fungetc   => Mem_UngetC'Access,
            fsize     => Mem_Size'Access
        );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Open_Memfile( buffer  : access Stream_Element_Array;
                           consume : Boolean := False ) return A_Allegro_File is
        memInfo : constant A_Memory_Info := new Memory_Info'(buffer, buffer'First, consume);
    begin
        return Al_Create_File_Handle( MEMFILE_INTERFACE, memInfo.all'Address );
    end Open_Memfile;

end Resources.Allegro_Files;
