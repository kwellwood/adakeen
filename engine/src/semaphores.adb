--
-- Copyright (c) 2016-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Semaphores is

    protected body Binary_Semaphore is

        procedure Reset is
        begin
            signalled := False;
        end Reset;

        procedure Release is
        begin
            signalled := True;
        end Release;

        procedure Set is
        begin
            signalled := True;
        end Set;

        entry Wait when signalled is
        begin
            null;
        end Wait;

        entry Sieze when signalled is
        begin
            signalled := False;
        end Sieze;

        entry Wait_One when signalled is
        begin
            signalled := False;
        end Wait_One;

    end Binary_Semaphore;

    --==========================================================================

    protected body Counting_Semaphore is

        procedure Release is
        begin
            available := available + 1;
        end Release;

        procedure Set( counts : Integer := 1 ) is
        begin
            available := available + counts;
        end Set;

        entry Sieze when available > 0 is
        begin
            available := available - 1;
        end Sieze;

        entry Wait_One when available > 0 is
        begin
            available := available - 1;
        end Wait_One;

    end Counting_Semaphore;

end Semaphores;
