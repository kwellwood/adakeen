--
-- Copyright (c) 2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Center_Arrays is

    procedure Append( this : in out Center_Array'Class; item : Element_Type ) is
    begin
        this.Insert_Symmetric( this.Last + 1, item );
    end Append;

    ----------------------------------------------------------------------------

    procedure Clear( this : in out Center_Array'Class ) is
    begin
        this.middle := 0;
        this.items.Clear;
    end Clear;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out Center_Array'Class; index : Integer ) is
    begin
        if (index = 0 and this.Length = 1) or else (index /= 0 and index in this.First..this.Last) then
            this.items.Delete( this.middle + index );
            if index < 0 then
                this.middle := this.middle - 1;
            end if;
        end if;
    end Delete;

    ----------------------------------------------------------------------------

    function Element( this : Center_Array'Class; index : Integer ) return Element_Type is (this.items.Element( this.middle + index ));

    ----------------------------------------------------------------------------

    function First( this : Center_Array'Class ) return Integer is (-this.middle);

    ----------------------------------------------------------------------------

    function First_Element( this : Center_Array'Class ) return Element_Type is (this.items.First_Element);

    ----------------------------------------------------------------------------

    function Insert_Before( this : in out Center_Array'Class; before : Integer; item : Element_Type ) return Integer is
        first : constant Integer := this.First;
        last  : constant Integer := this.Last;
        i     : Integer := before;
    begin
        if this.Length = 0 then
            -- first item always goes at the center (index 0)
            this.items.Insert( 0, item );
            return 0;
        elsif before < first then
            -- prepend to the array
            i := first;
        elsif before > last + 1 then
            -- append to the array
            i := last + 1;
        end if;

        this.items.Insert( this.middle + i, item );
        if i <= 0 then
            -- when inserting left of center, move the middle pointer back so
            -- index 0 stays referencing the same element. incrementing .middle
            -- after the insert has the asymmetric effect of inserting the new
            -- item at 'before' - 1, further away from the center.
            this.middle := this.middle + 1;
            i := i - 1;
        end if;
        return i;
    end Insert_Before;

    ----------------------------------------------------------------------------

    procedure Insert_Before( this : in out Center_Array'Class; before : Integer; item : Element_Type ) is
        ignored : Integer;
        pragma Warnings( Off, ignored );
    begin
        ignored := this.Insert_Before( before, item );
    end Insert_Before;

    ----------------------------------------------------------------------------

    function Insert_Symmetric( this : in out Center_Array'Class; index : Integer; item : Element_Type ) return Integer is
        first : constant Integer := this.First;
        last  : constant Integer := this.Last;
        i     : Integer := index;
    begin
        if this.Length = 0 then
            -- first item always goes at the center (index 0)
            this.items.Insert( 0, item );
            return 0;
        elsif index < first - 1 then
            -- prepend to the array
            i := first - 1;
        elsif index > last + 1 then
            -- append to the array
            i := last + 1;
        end if;

        if i <= 0 then
            -- when inserting left of center, move the middle pointer back so
            -- index 0 stays referencing the same element. incrementing middle
            -- prior to inserting in front of [this.middle+index] is the same as
            -- inserting behind [this.middle+index] if middle hadn't been
            -- incremented. when inserting at the negative indices, we want to
            -- do an insert-behind instead of insert-before, to preserve symmetry
            -- around the array's center.
            this.middle := this.middle + 1;
        end if;
        this.items.Insert( this.middle + i, item );
        return i;
    end Insert_Symmetric;

    ----------------------------------------------------------------------------

    procedure Insert_Symmetric( this : in out Center_Array'Class; index : Integer; item : Element_Type ) is
        ignored : Integer;
        pragma Warnings( Off, ignored );
    begin
        ignored := this.Insert_Symmetric( index, item );
    end Insert_Symmetric;

    ----------------------------------------------------------------------------

    function Is_Empty( this : Center_Array'Class ) return Boolean is (this.items.Is_Empty);

    ----------------------------------------------------------------------------

    function Last( this : Center_Array'Class ) return Integer is ((this.Length - 1) - this.middle);

    ----------------------------------------------------------------------------

    function Last_Element( this : Center_Array'Class ) return Element_Type is (this.items.Last_Element);

    ----------------------------------------------------------------------------

    function Length( this : Center_Array'Class ) return Natural is (Integer(this.items.Length));

    ----------------------------------------------------------------------------

    procedure Prepend( this : in out Center_Array'Class; item : Element_Type ) is
    begin
        this.Insert_Symmetric( this.First - 1, item );
    end Prepend;

    ----------------------------------------------------------------------------

    function Reference( this : aliased in out Center_Array; index : Integer ) return Reference_Type is
    begin
        pragma Assert( index in this.First..this.Last, "index out of range" );
        return Reference_Type'(element => this.items.Reference( this.middle + index ).Element);
    end Reference;

    ----------------------------------------------------------------------------

    procedure Replace( this : in out Center_Array'Class; index : Integer; item : Element_Type ) is
    begin
        if index in this.First..this.Last then
            this.items.Replace_Element( this.middle + index, item );
        end if;
    end Replace;

    ----------------------------------------------------------------------------

    procedure Set_Center( this : in out Center_Array'Class; index : Integer ) is
    begin
        if index in this.First..this.Last then
            this.middle := this.middle + index;
        end if;
    end Set_Center;

    ----------------------------------------------------------------------------

    procedure Swap( this : in out Center_Array'Class; indexA, indexB : Integer ) is
        first : constant Integer := this.First;
        last  : constant Integer := this.Last;
    begin
        if indexA in first..last and indexB in first..last then
            this.items.Swap( this.middle + indexA, this.middle + indexB );
        end if;
    end Swap;

end Center_Arrays;
