--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Debugging;                         use Debugging;
with Interfaces;                        use Interfaces;
with Streams;                           use Streams;
with Streams.Buffers;                   use Streams.Buffers;
with GNAT.Regexp;                       use GNAT.Regexp;
with UnZip.Streams;                     use UnZip.Streams;
with Zip_Streams;

package body Assets.Archives.Zip_Archives is

    function Load_Zip( assetId : Value; filePath : String; group : String ) return A_Archive is
        this : constant A_Archive := new Zip_Archive;
    begin
        this.Construct( assetId, filePath, group );
        return this;
    end Load_Zip;

    ----------------------------------------------------------------------------

    overriding
    function File_Exists( this : access Zip_Archive; filePath : String ) return Boolean is (this.loaded and then Exists( this.zipInfo, filePath ));

    ----------------------------------------------------------------------------

    overriding
    function Load_Data( this : access Zip_Archive ) return Boolean is
        res     : A_Resource_File := null;
        buffer  : Zip_Streams.Array_Streams.A_SEA := null;
        stream  : A_Buffer_Stream := null;
    begin
        res := Load_Resource( To_String( this.filePath ), To_String( this.group ) );
        if res = null then
            Dbg( "Error: Failed to load resource '" & To_String( this.group ) & ":" & To_String( this.filePath ) & "'", D_ASSET, Error );
            return False;                 -- file not found
        end if;

        -- copy the file contents to a private buffer
        -- the buffer will be consumed by this.zipArray
        buffer := new Stream_Element_Array(1..Stream_Element_Offset(res.Size));
        stream := res.Create_Stream;
        Stream_Element_Array'Read( stream, buffer.all );
        Close( stream );
        Delete( res );

        this.zipArray := new Array_Stream;
        this.zipArray.Set( buffer );      -- consumes 'buffer'

        -- Cache the zip info catalog
        begin
            Load( this.zipInfo, this.zipArray.all );   -- may raise exception
        exception
            when others =>
                if Is_loaded( this.zipInfo ) then
                    Delete( this.zipInfo );
                end if;
                Delete( this.zipArray );
                Dbg( "Error: Bad format in '" & To_String( this.group ) & ":" & To_String( this.filePath ) & "'", D_ASSET, Error );
                return False;             -- file is unreadable
        end;

        return True;
    end Load_Data;

    ----------------------------------------------------------------------------

    overriding
    function Load_File( this     : access Zip_Archive;
                        filePath : String ) return A_Resource_File is
        data : Streams.A_SEA;
    begin
        if this.loaded then
            data := Streams.A_SEA(this.Load_Raw( filePath ));
            if data /= null then
                return Create_Resource( To_String( this.diskFile ) & "<" & filePath, data );
            end if;
        end if;
        return null;        -- file not found or read error
    end Load_File;

    ----------------------------------------------------------------------------

    function Load_Raw( this : not null access Zip_Archive'Class; filePath : String ) return access Stream_Element_Array is
        zippedFile : Zipped_File_Type;
        compSize,
        uncompSize : File_size_type;
        fileData   : Zip_Streams.Array_Streams.A_SEA := null;
    begin
        -- open the file within the zip
        Open( zippedFile, this.zipInfo, filePath );    -- may raise exception
        Get_sizes( this.zipInfo, filePath, compSize, uncompSize );

        -- extract the file contents from the zip file
        fileData := new Stream_Element_Array(0..Stream_Element_Offset(uncompSize - 1));
        Stream_Element_Array'Read( Stream( zippedFile ), fileData.all );
        Close( zippedFile );
        return fileData;
    exception
        when others =>
            Delete( fileData );
            if Is_Open( zippedFile ) then
                Close( zippedFile );
            end if;
            return null;
    end Load_Raw;

    ----------------------------------------------------------------------------

    overriding
    procedure Search( this     : access Zip_Archive;
                      wildcard : String;
                      process  : access procedure( entryName : String ) ) is
        pattern : Regexp;

        ------------------------------------------------------------------------

        procedure Process_Entry( name : String ) is
        begin
            if Match( name, pattern ) then
                process( name );
            end if;
        end Process_Entry;

        ------------------------------------------------------------------------

        procedure Traverse is new Zip.Traverse( Process_Entry );
    begin
        if this.loaded then
            begin
                pattern := Compile( wildcard, Glob => True, Case_Sensitive => False );
            exception
                when Error_In_Regexp =>
                    -- invalid glob syntax in 'wildcard'
                    return;
            end;
            Traverse( this.zipInfo );
        end if;
    end Search;

    ----------------------------------------------------------------------------

    overriding
    procedure Unload_Data( this : access Zip_Archive ) is
    begin
        if Is_loaded( this.zipInfo ) then
            Delete( this.zipInfo );
        end if;
        Delete( this.zipArray );
    end Unload_Data;

end Assets.Archives.Zip_Archives;
