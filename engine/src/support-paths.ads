--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Support.Paths is

    pragma Elaborate_Body;

    ----------------------------------------------------------------------------
    -- Archive Path Strings

    -- NOTE: Paths can reference files or directories inside an archive file (or
    -- multiple archive files, if they're nested). The following functions deal
    -- specifically with these types of paths.
    --
    --
    -- Here is an example path to a file named "file.txt", that is inside an
    -- archive named "gfx.zip" that is itself inside an archive named
    -- "myGame.zip". The path to the outer archive is absolute.
    --
    -- C:\dir\myGame.zip<gfx.zip<some/file.txt
    --                           ^^^^^^^^^^^^^ Inner Path
    --                   ^^^^^^^^^^^^^^^^^^^^^ Path In Archive
    -- ^^^^^^^^^^^^^^^^^ Archive File
    -- ^^^^^^^^^^^^^^^^^ Outer Path
    --
    -- The following components comprise an archive path, as seen above:
    --
    -- 1. the inner path:      some/file.txt              Get_Inner_Path(p)
    -- 2. the path in archive: gfx.zip<some/file.txt      Get_Path_In_Archive(p)
    -- 3. the archive file:    C:\dir\myGame.zip          Get_Archive_File(p)
    -- 4. the outer path:      C:\dir\myGame.zip          Get_Outer_Path(p)
    --
    -- For standard paths not contained in an archive, the outer path and the
    -- inner path are the same as the whole path, but the archive file (not
    -- applicable) and the path in archive (not applicable) are both empty
    -- strings:
    --
    -- Where path = "C:\dir\myFile.exe":
    --
    --     Get_Inner_Path( path )      = "C:\dir\myFile.exe"
    --     Get_Outer_Path( path )      = "C:\dir\myFile.exe"
    --     Get_Archive_File( path )    = ""
    --     Get_Path_In_Archive( path ) = ""
    --
    -- However, the *archive* path "C:\dir\myGame.zip<" is the path to the root
    -- folder inside an archive:
    --
    --     Get_Inner_Path( path )      = ""
    --     Get_Outer_Path( path )      = "C:\dir\myGame.zip"
    --     Get_Archive_File( path )    = "C:\dir\myGame.zip"
    --     Get_Path_In_Archive( path ) = ""
    --
    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Returns the path of the outermost archive file containing 'path'. If the
    -- path does not reference a file or directory inside an archive, then an
    -- empty string will be returned.
    --
    -- This returns everything before the first archive delimiter '<' in 'path',
    -- or an empty string if no delimiter is found. See also: Get_Outer_Path().
    function Get_Archive_File( path : String ) return String;

    -- Returns 'path' relative to its inner-most archive file. The returned path
    -- will not be inside an archive. For paths outside an archive, the inner
    -- path is equivalent to the whole path.
    --
    -- This returns everything after the last archive delimiter '<' in 'path',
    -- or everything if no delimiter is found. See also: Get_Path_In_Archive().
    function Get_Inner_Path( path : String ) return String;

    -- Returns the path of the outermost archive file containing 'path'. For
    -- paths outside an archive, the outer path is equivalent to the whole path.
    --
    -- This returns everything before the first archive delimiter '<' in 'path',
    -- or everything if no delimiter is found. See also: Get_Archive_File().
    function Get_Outer_Path( path : String ) return String;

    -- Returns 'path' relative to its outermost archive file. The returned path
    -- may itself be inside a nested archive. If 'path' is not inside an
    -- archive, an empty string will be returned.
    --
    -- This returns everything after the first archive delimiter '<' in path, or
    -- an empty string if no delimiter is found. See also: Get_Inner_Path().
    function Get_Path_In_Archive( path : String ) return String;

    -- Returns True if the path resolves to a file or folder inside an archive.
    function Is_Path_In_Archive( path : String ) return Boolean;

    ----------------------------------------------------------------------------
    -- Standard Path Strings

    -- NOTE: For paths inside an archive, the following functions operate only
    -- on the inner path. For example, Get_Extension() only returns the file
    -- extension of the file referenced inside the archive(s), not the outer
    -- archive file's extension:
    --
    -- C:\dir\myGame.zip<gfx.zip<some/file.txt
    --                           ^^^^^^^^^^^^^ Inner Path
    --
    -- So, calling Get_Inner_Path() first on any path string given to one of
    -- these functions would be redundant.
    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- If 'path' has no file extension, or if 'force' is True, then a path with
    -- the appended extension is returned. Otherwise, 'path' is returned as-is.
    -- 'ext' should NOT include a leading dot character.
    function Add_Extension( path,
                            ext   : String;
                            force : Boolean := False ) return String;

    -- If 'fullpath' ends with filename, the ancestor part of 'fullpath' that
    -- contains 'filename' will be returned. Otherwise, 'fullpath' will be
    -- returned.
    --
    -- Example:
    --   fullpath => "C:\my_dir\something\file.dat"
    --   filename => "something\file.dat"
    --   returns "C:\my_dir\"
    function Ancestor_Dir( fullpath, filename : String ) return String;
    pragma Precondition( fullpath'Length > 0 );

    -- Ensures that 'path' has a trailing slash (directory separator) on its
    -- tail. If 'path' is empty, then ("." & Slash) will be returned.
    function Ensure_Trailing_Slash( path : String ) return String;

    -- Returns the base name of the file without the path. The base name is all
    -- characters up to the first '.' in the filename.
    function Get_Basename( path : String ) return String;

    -- Returns the path's complete file extension without a leading dot
    -- character, or an empty string if the path has no file extension. The
    -- complete extension is every character in the filename after the first dot.
    function Get_Complete_Extension( path : String ) return String;

    -- Returns the directory portion of 'path', including a directory separator
    -- at the end. If 'path' does not have a directory, then an empty string
    -- will be returned.
    function Get_Directory( path : String ) return String;

    -- Returns the name of the deepest directory in the path. If the path does
    -- not end with a directory separator, the portion following the last
    -- separator is assumed to be a filename; the name of its parent directory,
    -- if any, will be returned.
    function Get_Dir_Name( path : String ) return String;

    -- Returns the path's file extension without a leading dot character, or an
    -- empty string if the path has no file extension. The file extension is
    -- every character in the filename after the last dot.
    function Get_Extension( path : String ) return String;

    -- Returns the path's filename if it has one, otherwise an empty string is
    -- returned. The path is every character after the last directory separator,
    -- or the entire string if it doesn't have one.
    function Get_Filename( path : String ) return String;

    -- Returns the opposite slash of the current platform's directory separator
    -- slash character.
    function Other_Slash return String;
    pragma Postcondition( Other_Slash'Result'Length = 1 );

    -- Appends 'extra' onto 'basePath', following path naming conventions by
    -- ensuring that a directory separator character is inserted between the two
    -- path parts.
    --
    -- If both 'basePath' and 'extra' are empty then an empty string is
    -- returned, otherwise if 'basePath' is empty then 'extra' is returned, or
    -- if 'extra' is empty, then 'basePath' is returned.
    function Path_Append( basePath, extra : String ) return String;

    -- Returns a path without its file extension. If 'path' does not have a file
    -- extension, then it will be returned as-is.
    function Remove_Extension( path : String ) return String;

    -- Removes all trailing slashes (forward and backward) from tail of 'path'.
    function Remove_Trailing_Slash( path : String ) return String;

    -- Returns the current platform's directory separator character.
    function Slash return String;
    pragma Postcondition( Slash'Result'Length = 1 );

    ----------------------------------------------------------------------------
    -- Standard Folders

    -- Returns the current platform's common application data directory. This is
    -- where the standard application resources may have been installed.
    function App_Data_Directory return String;

    -- Returns the file extension for executable files on the current OS without
    -- a leading dot character.
    function Executable_Extension return String;

    -- Returns the path of the application's executable file.
    function Executable_Path return String;

    -- Returns the directory containing the executable file, or, if the executable is
    -- running on OS X inside an .app bundle, the bundle's Resources directory.
    function Execution_Directory return String;

    -- Returns the platform's home directory. The string will have a trailing
    -- directory separator.
    function Home_Directory return String;

    -- Returns the current platform's directory for system fonts.
    function System_Font_Directory return String;

    -- Returns the current platform's temporary directory for temporary files.
    function Temp_Directory return String;

    ----------------------------------------------------------------------------
    -- Path Operations

    -- Attempts to open the file 'filePath' with the file type's default
    -- application. If 'filePath' is inside an archive, the archive itself will
    -- be opened.
    procedure Auto_Open( filePath : String );

    -- Compares the file/directory timestamp of 'path1' versus 'path2'. Returns
    -- True if 'path1' is newer than 'path2' or if 'path2' does not exist.
    -- Returns False if 'path1' does not exist. If either path is inside an
    -- archive, the timestamp of its outer archive will be compared.
    function Is_Newer_File( path1, path2 : String ) return Boolean;

    -- Recursively creates directory 'path', returning True on success or False
    -- on error. If the directory already exists, True will be returned. In the
    -- unlikely event that 'path' refers to a directory inside an archive, the
    -- outer archive's directory will be created instead.
    function Make_Dir( path : String ) return Boolean;

    -- Opens the directory of 'path' in the platform's GUI and selects the
    -- specific file in 'path', if it points to an existing file. If 'path' is
    -- inside an archive, the outer archive file will be revealed instead. If
    -- the directory of 'path' does not exist then nothing happens.
    procedure Reveal_Path( path : String );

end Support.Paths;
