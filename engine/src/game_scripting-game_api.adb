--
-- Copyright (c) 2014-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Real_Time;                     use Ada.Real_Time;
with Assets;
with Clipping;                          use Clipping;
with Component_Families;                use Component_Families;
with Debugging;                         use Debugging;
with Easing;                            use Easing;
with Entities;                          use Entities;
with Entities.Entity_Attributes;        use Entities.Entity_Attributes;
with Entities.Factory;                  use Entities.Factory;
with Entities.Locations;                use Entities.Locations;
with Entities.Scripts;                  use Entities.Scripts;
with Entities.Systems;                  use Entities.Systems;
with Events.Audio;                      use Events.Audio;
with Events.Entities;                   use Events.Entities;
with Events.Game;                       use Events.Game;
with Events.Particles;                  use Events.Particles;
with Events.World;                      use Events.World;
with Games;                             use Games;
with Hashed_Strings;                    use Hashed_Strings;
with Object_Ids;                        use Object_Ids;
with Preferences;                       use Preferences;
with Resources.Scribble;                use Resources.Scribble;
with Scribble.Namespaces;               use Scribble.Namespaces;
with Scribble.VMs;                      use Scribble.VMs;
with Sessions;                          use Sessions;
with Support;                           use Support;
with Values.Casting;                    use Values.Casting;
with Values.Construction;               use Values.Construction;
with Values.Errors;                     use Values.Errors;
with Values.Maps;                       use Values.Maps;
with Values.Operations.Generics;        use Values.Operations.Generics;
with Values.Strings;                    use Values.Strings;
with Values.Tagged_Ids;                 use Values.Tagged_Ids;
with Worlds;                            use Worlds;

package body Game_Scripting.Game_API is

    function Get_ECS return A_Entity_System is (Get_Game.Get_Entity_System);

    function Get_Session return A_Session is (Get_Game.Get_Session);

    --==========================================================================
    -- LANGUAGE FUNCTIONS --

    -- $index( l : color, r : string ) return any
    -- $index( l : color, r : number ) return any
    -- $index( l : id   , r : string ) return any
    -- $index( l : map  , r : string ) return any
    -- $index( l : list , r : number ) return any
    procedure Index( state : in out Scribble_State ) is
    begin
        if state.args(1).Is_Id and state.args(2).Is_String then
            Member( state );
        else
            state.result := Binary_Index.Operate( state.args(1), state.args(2) );
        end if;
    end Index;

    ----------------------------------------------------------------------------

    -- $indexRef( l : id  , r : string ) return ref
    -- $indexRef( l : map , r : string ) return ref
    -- $indexRef( l : list, r : number ) return ref
    procedure IndexRef( state : in out Scribble_State ) is
    begin
        if state.args(1).Is_Id and state.args(2).Is_String then
            MemberRef( state );
        else
            state.result := Binary_Index_Ref.Operate( state.args(1), state.args(2) );
        end if;
    end IndexRef;

    ----------------------------------------------------------------------------

    -- $member( object : any, symbol : string ) return any
    procedure Member( state : in out Scribble_State ) is
        e     : A_Entity;
        c     : A_Component;
        obj   : Value := state.args(1);
        objId : Tagged_Id;
    begin
        -- the symbol is from the constant program data so it should be a string
        pragma Assert( state.args(2).Is_String, "Expected string in Scribble program data" );

        -- special case: in a Member_Call, $member is called with the left side
        -- as a reference to the object instead of the actual object itself.
        -- just dereference it and proceed as normal.
        if obj.Is_Ref then
            obj := Value(obj.Deref);
        end if;

        if obj.Is_Id then
            objId := Cast_Tagged_Id( obj );
            if Is_Null( objId ) then
                state.result := Create( Null_Reference, "Deferenced member '" & state.args(2).Str.To_String & "' of null id" );
                return;
            end if;

            case Get_Tag( objId ) is
                when Entity_Tag =>
                    -- index an entity with a symbol string; the entity's exported
                    -- values are available (attributes, registered attributes, etc)
                    e := Get_ECS.Get_Entity( objId );
                    if e /= null then
                        state.result := e.Get_Attributes.Get_Namespace_Name( state.args(2).Str.To_String );
                    else
                        state.result := Create( Null_Reference, "Deferenced member '" & state.args(2).Str.To_String & "' of invalid entity" );
                    end if;

                when Component_Tag =>
                    -- index a component with a symbol string; the component's
                    -- namespace names (if it's a namespace) are available.
                    c := Get_ECS.Get_Component( objId  );
                    if c /= null then
                        if c.all in Scribble_Namespace'Class then
                            state.result := A_Scribble_Namespace(c).Get_Namespace_Name( state.args(2).Str.To_String );
                        else
                            state.result := Create( Invalid_Arguments,
                                                    "Dereferenced member '" & state.args(2).Str.To_String &
                                                    "' of inaccessible component '" & c.Get_Definition & "'" );
                        end if;
                    else
                        state.result := Create( Null_Reference, "Invalid component" );
                    end if;

                when Game_Tag =>
                    if obj = GAME_ID then
                        state.result := Get_Session.Get_Namespace_Name( state.args(2).Str.To_String );
                    end if;

                when World_Tag =>
                    if Get_ECS.Get_World /= null then
                        state.result := Get_ECS.Get_World.Get_Namespace_Name( state.args(2).Str.To_String );
                    else
                        state.result := Null_Value;
                    end if;

                when others =>
                    state.result := Create( Invalid_Arguments,
                                            "Dereferenced member '" & state.args(2).Str.To_String &
                                            "' of unrecognized id" );
            end case;

        else
            -- act like the index operator
            state.result := Binary_Index.Operate( state.args(1), state.args(2) );
        end if;
    end Member;

    ----------------------------------------------------------------------------

    -- $memberRef( object : any, symbol : string ) return ref
    procedure MemberRef( state : in out Scribble_State ) is
        e     : A_Entity;
        c     : A_Component;
        objId : Tagged_Id;
    begin
        -- the symbol is from the constant program data so it should be a string
        pragma Assert( state.args(2).Is_String, "Expected string in Scribble program data" );

        if state.args(1).Is_Id then
            objId := Cast_Tagged_Id( state.args(1) );
            if Is_Null( objId ) then
                state.result := Create( Null_Reference, "Null id" );
                return;
            end if;

            case Get_Tag( objId ) is
                when Entity_Tag =>
                    -- index an entity with a symbol string; the entity's exported
                    -- values are available (attributes, registered attributes, etc)
                    e := Get_ECS.Get_Entity( objId );
                    if e /= null then
                        state.result := e.Get_Attributes.Get_Namespace_Ref( state.args(2).Str.To_String );
                    else
                        state.result := Create( Null_Reference, "Invalid entity" );
                    end if;
                when Component_Tag =>
                    -- index a component with a symbol string; the component's
                    -- namespace members (if it has one) are available.
                    c := Get_ECS.Get_Component( objId  );
                    if c /= null and then c.Get_Family = SCRIPT_ID then
                        state.result := A_Script(c).Get_Namespace_Ref( state.args(2).Str.To_String );
                    else
                        state.result := Create( Invalid_Arguments, "Invalid component" );
                    end if;
                when Game_Tag =>
                    state.result := Get_Session.Get_Namespace_Ref( state.args(2).Str.To_String );
                when World_Tag =>
                    state.result := Get_ECS.Get_World.Get_Namespace_Ref( state.args(2).Str.To_String );
                when others =>
                    state.result := Create( Invalid_Arguments, "Unrecognized id" );
            end case;

        else
            -- act like the index operator
            state.result := Binary_Index_Ref.Operate( state.args(1), state.args(2) );
        end if;
    end MemberRef;

    --==========================================================================
    -- GENERAL FUNCTIONS --

    -- assetStats() return map
    procedure AssetStats( state : in out Scribble_State ) is
        stats : Map_Value;
    begin
        Assets.Query_Cache( stats );
        state.result := Value(stats);
    end AssetStats;

    ----------------------------------------------------------------------------

    -- eval( func : function, ... ) return any
    procedure Eval( state : in out Scribble_State ) is

        procedure Print_Trace( str : String ) is
        begin
            Queue_Console_Text( str );
            Dbg( str, D_SCRIPT, Error );
        end Print_Trace;

        thread : A_Thread;
    begin
        if not state.args(1).Is_Function then
            state.result := Create( Invalid_Arguments, "eval()" );
            return;
        end if;

        thread := Create_Thread;
        thread.Load( state.args(1).Func, state.args(2..state.args'Last ) );  -- may raise Runtime_Error
        state.vm.Run( thread, Run_Complete );
        if thread.Is_Errored then
            state.result := Create( Evaluation_Error, "eval(): " & thread.Get_Result.Image );

            Queue_Console_Text( "eval(): " & thread.Get_Result.Image );
            Dbg( "eval(): " & thread.Get_Result.Image, D_SCRIPT, Error );
            thread.Iterate_Trace( Print_Trace'Access );
        else
            state.result := thread.Get_Result;
        end if;
        Delete( thread );
    end Eval;

    ----------------------------------------------------------------------------

    -- GetPref( section : string, name : string ) return any
    procedure GetPref( state : in out Scribble_State ) is
    begin
        if not Check_Types( state.args, (V_STRING, V_STRING) ) then
            state.result := Create( Invalid_Arguments, "GetPref()" );
            return;
        end if;
        state.result := Get_Pref( state.args(1).Str.To_String, state.args(2).Str.To_String );
    end GetPref;

    ----------------------------------------------------------------------------

    -- load( path : string, group := "" ) return any
    procedure Load( state : in out Scribble_State ) is
        group : Value;
    begin
        if not state.args(1).Is_String then
            state.result := Create( Invalid_Arguments, "load()" );
            return;
        end if;

        if state.args'Length = 2 then
            if not state.args(2).Is_String then
                state.result := Errors.Create( Invalid_Arguments, "load()" );
                return;
            end if;
            group := state.args(2);
        end if;

        if not Load_Value( state.result, state.args(1).Str.To_String, Cast_String( group ) ) then
            -- return the load error, prefixed with "load()"
            state.result := Create( state.result.Err.Get_Code,
                                    "load(): " & state.result.Err.Get_Message );
        end if;
    end Load;

    ----------------------------------------------------------------------------

    -- print(val : any) return null
    procedure Print( state : in out Scribble_State ) is
    begin
        if state.args(1).Is_String then
            Queue_Console_Text( state.args(1).Str.To_String );
        else
            Queue_Console_Text( Image( state.args(1) ) );
        end if;
    end Print;

    ----------------------------------------------------------------------------

    -- random() return number
    procedure Random( state : in out Scribble_State ) is
    begin
        state.result := Create( Random_Float );
    end Random;

    ----------------------------------------------------------------------------

    -- run( func : function, ... ) return any
    procedure Run( state : in out Scribble_State ) is
    begin
        if not state.args(1).Is_Function then
            state.result := Create( Invalid_Arguments, "run()" );
            return;
        end if;

        -- use a taskId of -1 (non-zero) to run the script in the background.
        -- the actual task id doesn't matter, as long as it doesn't conflict
        -- with task ids generated by the game view.
        state.result := Get_Session.Run_Script( "",
                                                state.args(1).Func,
                                                state.args(2..state.args'Last),
                                                taskId => -1 );
    end Run;

    ----------------------------------------------------------------------------

    -- SetPref( section : string, name : string, val : any ) return null
    procedure SetPref( state : in out Scribble_State ) is
    begin
        if state.argCount /= 3 or not state.args(1).Is_String or not state.args(2).Is_String then
            state.result := Create( Invalid_Arguments, "SetPref()" );
            return;
        end if;
        Set_Pref( state.args(1).Str.To_String, state.args(2).Str.To_String, state.args(3) );
    end SetPref;

    --==========================================================================
    -- EASING FUNCTIONS --

    procedure QuadraticEaseIn( state : in out Scribble_State ) is
    begin
        state.result := Create( Cast_Float( state.args(2) ) +                    -- time
                                Cast_Float( state.args(3) ) *                    -- begin (x1)
                                QuadraticEaseIn( Cast_Float( state.args(1) ) /   -- change (x2 - x1)
                                                 Cast_Float( state.args(4) )     -- duration
                                               )
                              );
    end QuadraticEaseIn;

    ----------------------------------------------------------------------------

    procedure QuadraticEaseOut( state : in out Scribble_State ) is
    begin
        state.result := Create( Cast_Float( state.args(2) ) +
                                Cast_Float( state.args(3) ) *
                                QuadraticEaseOut( Cast_Float( state.args(1) ) /
                                                  Cast_Float( state.args(4) )
                                                )
                              );
    end QuadraticEaseOut;

    ----------------------------------------------------------------------------

    procedure QuadraticEaseInOut( state : in out Scribble_State ) is
    begin
        state.result := Create( Cast_Float( state.args(2) ) +
                                Cast_Float( state.args(3) ) *
                                QuadraticEaseInOut( Cast_Float( state.args(1) ) /
                                                    Cast_Float( state.args(4) )
                                                  )
                              );
    end QuadraticEaseInOut;

    ----------------------------------------------------------------------------

    procedure CubicEaseIn( state : in out Scribble_State ) is
    begin
        state.result := Create( Cast_Float( state.args(2) ) +
                                Cast_Float( state.args(3) ) *
                                CubicEaseIn( Cast_Float( state.args(1) ) /
                                             Cast_Float( state.args(4) )
                                           )
                              );
    end CubicEaseIn;

    ----------------------------------------------------------------------------

    procedure CubicEaseOut( state : in out Scribble_State ) is
    begin
        state.result := Create( Cast_Float( state.args(2) ) +
                                Cast_Float( state.args(3) ) *
                                CubicEaseOut( Cast_Float( state.args(1) ) /
                                              Cast_Float( state.args(4) )
                                            )
                              );
    end CubicEaseOut;

    ----------------------------------------------------------------------------

    procedure CubicEaseInOut( state : in out Scribble_State ) is
    begin
        state.result := Create( Cast_Float( state.args(2) ) +
                                Cast_Float( state.args(3) ) *
                                CubicEaseInOut( Cast_Float( state.args(1) ) /
                                                Cast_Float( state.args(4) )
                                              )
                              );
    end CubicEaseInOut;

    ----------------------------------------------------------------------------

    procedure QuarticEaseIn( state : in out Scribble_State ) is
    begin
        state.result := Create( Cast_Float( state.args(2) ) +
                                Cast_Float( state.args(3) ) *
                                QuarticEaseIn( Cast_Float( state.args(1) ) /
                                               Cast_Float( state.args(4) )
                                             )
                              );
    end QuarticEaseIn;

    ----------------------------------------------------------------------------

    procedure QuarticEaseOut( state : in out Scribble_State ) is
    begin
        state.result := Create( Cast_Float( state.args(2) ) +
                                Cast_Float( state.args(3) ) *
                                QuarticEaseOut( Cast_Float( state.args(1) ) /
                                                Cast_Float( state.args(4) )
                                              )
                              );
    end QuarticEaseOut;

    ----------------------------------------------------------------------------

    procedure QuarticEaseInOut( state : in out Scribble_State ) is
    begin
        state.result := Create( Cast_Float( state.args(2) ) +
                                Cast_Float( state.args(3) ) *
                                QuarticEaseInOut( Cast_Float( state.args(1) ) /
                                                  Cast_Float( state.args(4) )
                                                )
                              );
    end QuarticEaseInOut;

    ----------------------------------------------------------------------------

    procedure QuinticEaseIn( state : in out Scribble_State ) is
    begin
        state.result := Create( Cast_Float( state.args(2) ) +
                                Cast_Float( state.args(3) ) *
                                QuinticEaseIn( Cast_Float( state.args(1) ) /
                                               Cast_Float( state.args(4) )
                                             )
                              );
    end QuinticEaseIn;

    ----------------------------------------------------------------------------

    procedure QuinticEaseOut( state : in out Scribble_State ) is
    begin
        state.result := Create( Cast_Float( state.args(2) ) +
                                Cast_Float( state.args(3) ) *
                                QuinticEaseOut( Cast_Float( state.args(1) ) /
                                                Cast_Float( state.args(4) )
                                              )
                              );
    end QuinticEaseOut;

    ----------------------------------------------------------------------------

    procedure QuinticEaseInOut( state : in out Scribble_State ) is
    begin
        state.result := Create( Cast_Float( state.args(2) ) +
                                Cast_Float( state.args(3) ) *
                                QuinticEaseInOut( Cast_Float( state.args(1) ) /
                                                  Cast_Float( state.args(4) )
                                                )
                              );
    end QuinticEaseInOut;

    ----------------------------------------------------------------------------

    procedure SineEaseIn( state : in out Scribble_State ) is
    begin
        state.result := Create( Cast_Float( state.args(2) ) +
                                Cast_Float( state.args(3) ) *
                                SineEaseIn( Cast_Float( state.args(1) ) /
                                            Cast_Float( state.args(4) )
                                          )
                              );
    end SineEaseIn;

    ----------------------------------------------------------------------------

    procedure SineEaseOut( state : in out Scribble_State ) is
    begin
        state.result := Create( Cast_Float( state.args(2) ) +
                                Cast_Float( state.args(3) ) *
                                SineEaseOut( Cast_Float( state.args(1) ) /
                                             Cast_Float( state.args(4) )
                                           )
                              );
    end SineEaseOut;

    ----------------------------------------------------------------------------

    procedure SineEaseInOut( state : in out Scribble_State ) is
    begin
        state.result := Create( Cast_Float( state.args(2) ) +
                                Cast_Float( state.args(3) ) *
                                SineEaseInOut( Cast_Float( state.args(1) ) /
                                               Cast_Float( state.args(4) )
                                             )
                              );
    end SineEaseInOut;

    ----------------------------------------------------------------------------

    procedure CircularEaseIn( state : in out Scribble_State ) is
    begin
        state.result := Create( Cast_Float( state.args(2) ) +
                                Cast_Float( state.args(3) ) *
                                CircularEaseIn( Cast_Float( state.args(1) ) /
                                                Cast_Float( state.args(4) )
                                              )
                              );
    end CircularEaseIn;

    ----------------------------------------------------------------------------

    procedure CircularEaseOut( state : in out Scribble_State ) is
    begin
        state.result := Create( Cast_Float( state.args(2) ) +
                                Cast_Float( state.args(3) ) *
                                CircularEaseOut( Cast_Float( state.args(1) ) /
                                                 Cast_Float( state.args(4) )
                                               )
                              );
    end CircularEaseOut;

    ----------------------------------------------------------------------------

    procedure CircularEaseInOut( state : in out Scribble_State ) is
    begin
        state.result := Create( Cast_Float( state.args(2) ) +
                                Cast_Float( state.args(3) ) *
                                CircularEaseInOut( Cast_Float( state.args(1) ) /
                                                   Cast_Float( state.args(4) )
                                                 )
                              );
    end CircularEaseInOut;

    ----------------------------------------------------------------------------

    procedure ExponentialEaseIn( state : in out Scribble_State ) is
    begin
        state.result := Create( Cast_Float( state.args(2) ) +
                                Cast_Float( state.args(3) ) *
                                ExponentialEaseIn( Cast_Float( state.args(1) ) /
                                                   Cast_Float( state.args(4) )
                                                 )
                              );
    end ExponentialEaseIn;

    ----------------------------------------------------------------------------

    procedure ExponentialEaseOut( state : in out Scribble_State ) is
    begin
        state.result := Create( Cast_Float( state.args(2) ) +
                                Cast_Float( state.args(3) ) *
                                ExponentialEaseOut( Cast_Float( state.args(1) ) /
                                                    Cast_Float( state.args(4) )
                                                  )
                              );
    end ExponentialEaseOut;

    ----------------------------------------------------------------------------

    procedure ExponentialEaseInOut( state : in out Scribble_State ) is
    begin
        state.result := Create( Cast_Float( state.args(2) ) +
                                Cast_Float( state.args(3) ) *
                                ExponentialEaseInOut( Cast_Float( state.args(1) ) /
                                                      Cast_Float( state.args(4) )
                                                    )
                              );
    end ExponentialEaseInOut;

    ----------------------------------------------------------------------------

    procedure ElasticEaseIn( state : in out Scribble_State ) is
    begin
        state.result := Create( Cast_Float( state.args(2) ) +
                                Cast_Float( state.args(3) ) *
                                ElasticEaseIn( Cast_Float( state.args(1) ) /
                                               Cast_Float( state.args(4) )
                                             )
                              );
    end ElasticEaseIn;

    ----------------------------------------------------------------------------

    procedure ElasticEaseOut( state : in out Scribble_State ) is
    begin
        state.result := Create( Cast_Float( state.args(2) ) +
                                Cast_Float( state.args(3) ) *
                                ElasticEaseOut( Cast_Float( state.args(1) ) /
                                                Cast_Float( state.args(4) )
                                              )
                              );
    end ElasticEaseOut;

    ----------------------------------------------------------------------------

    procedure ElasticEaseInOut( state : in out Scribble_State ) is
    begin
        state.result := Create( Cast_Float( state.args(2) ) +
                                Cast_Float( state.args(3) ) *
                                ElasticEaseInOut( Cast_Float( state.args(1) ) /
                                                  Cast_Float( state.args(4) )
                                                )
                              );
    end ElasticEaseInOut;

    ----------------------------------------------------------------------------

    procedure BackEaseIn( state : in out Scribble_State ) is
    begin
        state.result := Create( Cast_Float( state.args(2) ) +
                                Cast_Float( state.args(3) ) *
                                BackEaseIn( Cast_Float( state.args(1) ) /
                                            Cast_Float( state.args(4) )
                                          )
                              );
    end BackEaseIn;

    ----------------------------------------------------------------------------

    procedure BackEaseOut( state : in out Scribble_State ) is
    begin
        state.result := Create( Cast_Float( state.args(2) ) +
                                Cast_Float( state.args(3) ) *
                                BackEaseOut( Cast_Float( state.args(1) ) /
                                             Cast_Float( state.args(4) )
                                           )
                              );
    end BackEaseOut;

    ----------------------------------------------------------------------------

    procedure BackEaseInOut( state : in out Scribble_State ) is
    begin
        state.result := Create( Cast_Float( state.args(2) ) +
                                Cast_Float( state.args(3) ) *
                                BackEaseInOut( Cast_Float( state.args(1) ) /
                                               Cast_Float( state.args(4) )
                                             )
                              );
    end BackEaseInOut;

    ----------------------------------------------------------------------------

    procedure BounceEaseIn( state : in out Scribble_State ) is
    begin
        state.result := Create( Cast_Float( state.args(2) ) +
                                Cast_Float( state.args(3) ) *
                                BounceEaseIn( Cast_Float( state.args(1) ) /
                                              Cast_Float( state.args(4) )
                                            )
                              );
    end BounceEaseIn;

    ----------------------------------------------------------------------------

    procedure BounceEaseOut( state : in out Scribble_State ) is
    begin
        state.result := Create( Cast_Float( state.args(2) ) +
                                Cast_Float( state.args(3) ) *
                                BounceEaseOut( Cast_Float( state.args(1) ) /
                                               Cast_Float( state.args(4) )
                                             )
                              );
    end BounceEaseOut;

    ----------------------------------------------------------------------------

    procedure BounceEaseInOut( state : in out Scribble_State ) is
    begin
        state.result := Create( Cast_Float( state.args(2) ) +
                                Cast_Float( state.args(3) ) *
                                BounceEaseInOut( Cast_Float( state.args(1) ) /
                                                 Cast_Float( state.args(4) )
                                               )
                              );
    end BounceEaseInOut;

    --==========================================================================
    -- AUDIO FUNCTIONS --

    -- PlayMusic( name : string ) return null
    procedure PlayMusic( state : in out Scribble_State ) is
    begin
        if not state.args(1).Is_String then
            state.result := Create( Invalid_Arguments, "PlayMusic()" );
            return;
        end if;
        Queue_Play_Music( name => state.args(1).Str.To_String );
    end PlayMusic;

    ----------------------------------------------------------------------------

    -- StopMusic() return null
    procedure StopMusic( state : in out Scribble_State ) is
        pragma Unreferenced( state );
    begin
        Queue_Stop_Music;
    end StopMusic;

    ----------------------------------------------------------------------------

    -- PlaySound( name : string ) return null
    procedure PlaySound( state : in out Scribble_State ) is
    begin
        if not state.args(1).Is_String then
            state.result := Create( Invalid_Arguments, "PlaySound()" );
            return;
        end if;
        Queue_Play_Sound( name => state.args(1).Str.To_String );
    end PlaySound;

    --==========================================================================
    -- WORLD FUNCTIONS --

    -- LoadWorld( name : string ) return null
    procedure LoadWorld( state : in out Scribble_State ) is
    begin
        if not state.args(1).Is_String then
            state.result := Create( Invalid_Arguments, "LoadWorld()" );
            return;
        end if;
        Queue_Load_World( state.args(1).Str.To_String );
    end LoadWorld;

    ----------------------------------------------------------------------------

    -- GetTile( layer : number, x : number, y : number ) return number
    procedure GetTile( state : in out Scribble_State ) is
    begin
        if not Check_Types( state.args, (V_NUMBER, V_NUMBER, V_NUMBER) ) then
            state.result := Create( Invalid_Arguments, "GetTile()" );
            return;
        end if;
        state.result := Create( Get_ECS.Get_World.Get_Tile_Id( layer => state.args(1).To_Int,
                                                               x     => state.args(2).To_Float,
                                                               y     => state.args(3).To_Float ) );
    end GetTile;

    ----------------------------------------------------------------------------

    -- GetTileInfo( tileId : number ) return map
    procedure GetTileInfo( state : in out Scribble_State ) is
    begin
        if not state.args(1).Is_Number then
            state.result := Create( Invalid_Arguments, "GetTileInfo()" );
            return;
        end if;
        state.result := Clone( Get_ECS.Get_World.Get_Library.Get.Get_Tile( state.args(1).To_Int ).Get_Attributes );
    end GetTileInfo;

    ----------------------------------------------------------------------------

    -- IsFloor( x : number, y : number ) return boolean
    procedure IsFloor( state : in out Scribble_State ) is
        shape : Clip_Type;
    begin
        if not Check_Types( state.args, (V_NUMBER, V_NUMBER) ) then
            state.result := Create( Invalid_Arguments, "IsFloor()" );
            return;
        end if;

        shape := Get_ECS.Get_World.Get_Clip_Type( x => state.args(1).To_Float,
                                                  y => state.args(2).To_Float );
        state.result := Create( shape = Wall or else
                                shape = OneWay or else
                                Is_Slope_A( shape ) or else
                                Is_Slope_B( shape ) );
    end IsFloor;

    ----------------------------------------------------------------------------

    -- SetLayerProperty( layer : number, name : string, val : any ) return null
    procedure SetLayerProperty( state : in out Scribble_State ) is
    begin
        if not Check_Types( state.args(1..2), (V_NUMBER, V_STRING) ) then
            state.result := Create( Invalid_Arguments, "SetLayerProperty()" );
            return;
        end if;

        Get_ECS.Get_World.Set_Layer_Property( layer => state.args(1).To_Int,
                                              name  => state.args(2).Str.To_String,
                                              val   => state.args(3) );
    end SetLayerProperty;

    ----------------------------------------------------------------------------

    -- SetTile( layer : number, x : number, y : number, tileId : number ) return null
    procedure SetTile( state : in out Scribble_State ) is
    begin
        if not Check_Types( state.args, (V_NUMBER, V_NUMBER, V_NUMBER, V_NUMBER) ) then
            state.result := Create( Invalid_Arguments, "SetTile()" );
            return;
        end if;

        Get_ECS.Get_World.Set_Tile( layer => state.args(1).To_Int,
                                    x     => state.args(2).To_Float,
                                    y     => state.args(3).To_Float,
                                    id    => Integer'Max( 0, state.args(4).To_Int ) );
    end SetTile;

    --==========================================================================
    -- ENTITY FUNCTIONS --

    -- Spawn( template : string, properties : map ) return id
    procedure Spawn( state : in out Scribble_State ) is
        eid    : Entity_Id := Values.Tagged_Ids.NULL_ID;
        merged : Map_Value;
    begin
        if not Check_Types( state.args, (V_STRING, V_MAP) ) then
            state.result := Create( Invalid_Arguments, "Spawn()" );
            return;
        end if;

        -- specify the entity id here so it can be returned to the caller
        eid := Unique_Tagged_Id( Entity_Tag );

        merged := Create( (1=>Pair( "id", To_Id_Value( eid ) )), consume => True ).Map;
        if state.args(2).Map.Size > 0 then
            merged.Recursive_Merge( from => state.args(2).Map );
        end if;

        -- queue the spawn because the entity map is probably busy right now,
        -- iterating over entities to Tick them. the entity ticking would have
        -- to be done differently, allowing entities to be added or removed
        -- during iteration.
        Queue_Spawn_Entity( template   => state.args(1).Str.To_String,
                            properties => merged );

        state.result := To_Id_Value( eid );
    end Spawn;

    ----------------------------------------------------------------------------

    -- SpawnAt( template : string, x : number, y : number, properties : map := {} ) return id
    procedure SpawnAt( state : in out Scribble_State ) is
        eid    : Entity_Id := Values.Tagged_Ids.NULL_ID;
        merged : Map_Value;
        props  : Map_Value;
    begin
        if not Check_Types( state.args, (V_STRING, V_NUMBER, V_NUMBER, V_MAP) ) then
            state.result := Create( Invalid_Arguments, "SpawnAt()" );
            return;
        end if;

        -- specify the entity id here so it can be returned to the caller
        eid := Unique_Tagged_Id( Entity_Tag );

        -- If 'x' and 'y' are specified in the entity template, they will
        -- override the given x, y.
        merged := Create( (1=>Pair( "id", To_Id_Value( eid ) ),
                           2=>Pair( "Location", Create( (Pair( "x", state.args(2) ),
                                                         Pair( "y", state.args(3) )),
                                                        consume => True ) )),
                          consume => True ).Map;

        props := (if state.argCount >= 4
                  then state.args(4).Map
                  else Create_Map.Map);
        if props.Size > 0 then
            merged.Recursive_Merge( from => props );
        end if;

        Queue_Spawn_Entity( state.args(1).Str.To_String, merged );

        state.result := To_Id_Value( eid );
    end SpawnAt;

    ----------------------------------------------------------------------------

    -- Destroy( entity : id ) return null
    procedure Destroy( state : in out Scribble_State ) is
        e   : A_Entity;
        eId : Tagged_Id;
    begin
        if not state.args(1).Is_Id then
            state.result := Create( Invalid_Arguments, "Destroy()" );
            return;
        end if;

        eId := Cast_Tagged_Id( state.args(1) );
        e := Get_ECS.Get_Entity( eId );
        if e /= null then
            Queue_Delete_Entity( e.Get_Id );
        elsif not Is_Null( eId ) and then Get_Tag( eId ) /= Entity_Tag then
            Dbg( "Destroy(): Invalid entity", D_SCRIPT, Error );
        end if;
    end Destroy;

    ----------------------------------------------------------------------------

    -- EntityExists( entity : id ) return boolean
    procedure EntityExists( state : in out Scribble_State ) is
        eId : Tagged_Id;
    begin
        if not state.args(1).Is_Id then
            Dbg( "EntityExists(): Invalid arguments", D_SCRIPT, Warning );
            state.result := Create( False );
            return;
        end if;

        eId := Cast_Tagged_Id( state.args(1) );
        if not Is_Null( eId ) and then Get_Tag( eId ) /= Entity_Tag then
            Dbg( "EntityExists(): Invalid entity", D_SCRIPT, Error );
        end if;
        state.result := Create( Get_ECS.Get_Entity( eId ) /= null );
    end EntityExists;

    ----------------------------------------------------------------------------

    -- NearPlayer( entity : id ) return boolean
    procedure NearPlayer( state : in out Scribble_State ) is
        e1   : A_Entity;
        e2   : A_Entity;
        loc1 : A_Location;
        loc2 : A_Location;
    begin
        if not state.args(1).Is_Id then
            state.result := Create( Invalid_Arguments, "NearPlayer()" );
            return;
        end if;

        state.result := Create( False );

        e1 := Get_ECS.Get_Entity( Cast_Tagged_Id( state.args(1) ) );
        if e1 /= null then
            loc1 := A_Location(e1.Get_Component( LOCATION_ID ));
            if loc1 /= null then
                e2 := Get_ECS.Get_Entity( Cast_Tagged_Id( Get_ECS.Get_World.Get_Property( "player" ) ) );
                loc2 := A_Location(e2.Get_Component( LOCATION_ID ));
                if loc2 /= null then
                    state.result := Create( abs (loc1.Get_X - loc2.Get_X) < 340.0 and
                                            abs (loc1.Get_Y - loc2.Get_Y) < 240.0 );
                end if;
            end if;
        end if;
    end NearPlayer;

    ----------------------------------------------------------------------------

    -- AddComponent( entity : id, component : string, properties : map := {} ) return id
    procedure AddComponent( state : in out Scribble_State ) is
        eId : Tagged_Id;
        e   : A_Entity;
        c   : A_Component;
        cId : Component_Id;
    begin
        if not Check_Types( state.args, (V_ID, V_STRING, V_MAP) ) then
            state.result := Create( Invalid_Arguments, "AddComponent()" );
            return;
        end if;

        eId := Cast_Tagged_Id( state.args(1) );
        e := Get_ECS.Get_Entity( eId );
        if e /= null then
            c := Entities.Factory.Global.Create_Component( state.args(2).Str.To_String,
                                                           (if state.args'Length > 2 then state.args(3).Map else Create_Map.Map) );
            if c /= null then
                cId := c.Get_Id;
                e.Get_Attributes.Set_Defaults( Entities.Factory.Global.Get_Definition( state.args(2).Str.To_String ).Get_Default_Attributes );
                if e.Add_Component( c ) then
                    -- success!
                    state.result := To_Id_Value( cId );
                else
                    -- duplicate component in entity
                    Delete( c );
                    state.result := Create( Illegal_Operation,
                                            "Cannot add duplicate component '" & c.Get_Definition &
                                            "' to entity '" & e.Get_Template & "'" );
                end if;
            else
                -- component definition is unknown
                state.result := Create( Not_Found, "Component definition '" &
                                                   state.args(2).Str.To_String & "' is unknown" );
            end if;

        else
            if not Is_Null( eId ) and then Get_Tag( eId ) /= Entity_Tag then
                Dbg( "AddComponent(): Invalid entity", D_SCRIPT, Error );
            end if;
            state.result := Create( Null_Reference, "Invalid entity" );
        end if;
    end AddComponent;

    ----------------------------------------------------------------------------

    -- RemoveComponent( entity : id, component : id ) return null
    procedure RemoveComponent( state : in out Scribble_State ) is
        eId : Tagged_Id;
        cId : Tagged_Id;
        e   : A_Entity;
        c   : A_Component;
    begin
        if state.args(1).Get_Type /= V_ID or else state.args(2).Get_Type /= V_ID then
            state.result := Create( Invalid_Arguments, "RemoveComponent()" );
            return;
        end if;

        cId := Cast_Tagged_Id( state.args(2) );
        c := Get_ECS.Get_Component( cId );
        if c /= null then
            e := c.Get_Owner;
            eId := Cast_Tagged_Id( state.args(1) );
            if e.Get_Id = eId then
                e.Delete_Component( c );   -- consumes 'c'
            elsif Is_Null( eId ) then
                state.result := Create( Null_Reference, "Invalid entity" );
            elsif Get_Tag( eId ) /= Entity_Tag then
                state.result := Create( Invalid_Arguments, "Invalid entity" );
            end if;

        else
            if not Is_Null( cId ) and then Get_Tag( cId ) /= Component_Tag then
                Dbg( "RemoveComponent(): Invalid component", D_SCRIPT, Error );
            end if;
            state.result := Create( Null_Reference, "Invalid component" );
        end if;
    end RemoveComponent;

    ----------------------------------------------------------------------------

    -- RemoveComponentFamily( entity : id, family : string ) return null
    procedure RemoveComponentFamily( state : in out Scribble_State ) is
        eId : Tagged_Id;
        e   : A_Entity;
    begin
        if not Check_Types( state.args, (V_ID, V_STRING) ) then
            state.result := Create( Invalid_Arguments, "RemoveComponentFamily()" );
            return;
        end if;

        eId := Cast_Tagged_Id( state.args(1) );
        e := Get_ECS.Get_Entity( eId );
        if e /= null then
            e.Delete_Component( family => To_Family_Id( state.args(2).Str.To_String ) );

        else
            if not Is_Null( eId ) and then Get_Tag( eId ) /= Entity_Tag then
                Dbg( "RemoveComponentFamily(): Invalid entity", D_SCRIPT, Error );
            end if;
            state.result := Create( Null_Reference, "Invalid entity" );
        end if;
    end RemoveComponentFamily;

    ----------------------------------------------------------------------------

    -- Send( entity : id, name : string, params : map := {} ) return null
    procedure Send( state : in out Scribble_State ) is
        eId : Tagged_Id;
        e   : A_Entity;
    begin
        if not Check_Types( state.args, (V_ID, V_STRING, V_MAP) ) then
            state.result := Create( Invalid_Arguments, "Send()" );
            return;
        end if;

        eId := Cast_Tagged_Id( state.args(1) );
        e := Get_ECS.Get_Entity( eId );
        if e /= null then
            e.Dispatch_Message( name   => To_Hashed_String( state.args(2).Str.To_String ),
                                params => (if state.args'Length > 2 then state.args(3).Map else Create_Map.Map) );

        else
            if not Is_Null( eId ) and then Get_Tag( eId ) /= Entity_Tag then
                Dbg( "Send(): Invalid entity", D_SCRIPT, Error );
            end if;
            state.result := Create( Null_Reference, "Invalid entity" );
        end if;
    end Send;

    --==========================================================================
    -- COMPONENT FUNCTIONS --

    -- CancelScriptTimer( script : id, name : string ) return null
    procedure CancelScriptTimer( state : in out Scribble_State ) is
        cId : Tagged_Id;
        c   : A_Component;
    begin
        if not Check_Types( state.args, (V_ID, V_STRING ) ) then
            state.result := Create( Invalid_Arguments, "CancelScriptTimer()" );
            return;
        end if;

        cId := Cast_Tagged_Id( state.args(1) );
        c := Get_ECS.Get_Component( cId );
        if c /= null and then c.Get_Family = SCRIPT_ID then
            A_Script(c).Cancel_Timer( funcName => state.args(2).Str.To_String );

        else
            if not Is_Null( cId ) and then Get_Tag( cId ) /= Component_Tag then
                Dbg( "CancelScriptTimer(): Invalid component", D_SCRIPT, Error );
            end if;
            state.result := Create( Null_Reference, "Invalid component" );
        end if;
    end CancelScriptTimer;

    ----------------------------------------------------------------------------

    -- SetScriptTimer( script : id,
    --                 name   : string,
    --                 time   : number,
    --                 repeat : boolean := false,
    --                 args   : list := [] ) return null
    procedure SetScriptTimer( state : in out Scribble_State ) is
        cId : Tagged_Id;
        c   : A_Component;
    begin
        if not Check_Types( state.args, (V_ID, V_STRING, V_NUMBER, V_BOOLEAN, V_LIST) ) then
            state.result := Create( Invalid_Arguments, "SetScriptTimer()" );
            return;
        end if;

        cId := Cast_Tagged_Id( state.args(1) );
        c := Get_ECS.Get_Component( cId );
        if c /= null and then c.Get_Family = SCRIPT_ID then
            A_Script(c).Set_Timer( funcName => state.args(2).Str.To_String,
                                   waitTime => Milliseconds( Integer'Max( 0, state.args(3).To_Int ) ),
                                   repeat   => (if state.args'Length >= 4 then state.args(4).To_Boolean else False),
                                   args     => (if state.args'Length >= 5 then state.args(5).Lst else Null_Value.Lst) );

        else
            if not Is_Null( cId ) and then Get_Tag( cId ) /= Component_Tag then
                Dbg( "SetScriptTimer(): Invalid component", D_SCRIPT, Error );
            end if;
            state.result := Create( Null_Reference, "Invalid component" );
        end if;
    end SetScriptTimer;

    --==========================================================================
    -- PARTICLE FUNCTIONS --

    -- ParticleBurst( emitter : string or map,
    --                x       : number,
    --                y       : number,
    --                z       : number,
    --                count   : number ) return null
    procedure ParticleBurst( state : in out Scribble_State ) is
    begin
        if not Check_Types( state.args, (V_STRING, V_NUMBER, V_NUMBER, V_NUMBER, V_NUMBER) ) and then
           not Check_Types( state.args, (V_MAP, V_NUMBER, V_NUMBER, V_NUMBER, V_NUMBER) )
        then
            state.result := Create( Invalid_Arguments, "ParticleBurst()" );
            return;
        end if;

        Queue_Particle_Burst( emitter => state.args(1),
                              x       => state.args(2).To_Float,
                              y       => state.args(3).To_Float,
                              z       => state.args(4).To_Float,
                              count   => Integer'Max( 0, state.args(5).To_Int ) );
    end ParticleBurst;

    ----------------------------------------------------------------------------

    -- ParticleStream( emitter  : string or map,
    --                 x        : number,
    --                 y        : number,
    --                 z        : number,
    --                 rate     : number,
    --                 lifetime : number,
    --                 entityId : id := null ) return null
    procedure ParticleStream( state : in out Scribble_State ) is
    begin
        if not Check_Types( state.args, (V_STRING, V_NUMBER, V_NUMBER, V_NUMBER, V_NUMBER, V_NUMBER, V_ID) ) and then
           not Check_Types( state.args, (V_MAP,    V_NUMBER, V_NUMBER, V_NUMBER, V_NUMBER, V_NUMBER, V_ID) )
        then
            state.result := Create( Invalid_Arguments, "ParticleStream()" );
            return;
        end if;

        Queue_Particle_Burst( emitter  => state.args(1),
                              x        => state.args(2).To_Float,
                              y        => state.args(3).To_Float,
                              z        => state.args(4).To_Float,
                              rate     => Integer'Max( 0, state.args(5).To_Int ),
                              lifetime => Milliseconds( Integer'Max( 0, state.args(6).To_Int ) ),
                              entityId => Cast_Tagged_Id( (if state.argCount >= 7
                                                           then state.args(7)
                                                           else Null_Value) ) );
    end ParticleStream;

    --==========================================================================
    -- MESSAGING FUNCTIONS --

    -- PauseGame() return null
    procedure PauseGame( state : in out Scribble_State ) is
        pragma Unreferenced( state );
    begin
        Get_Session.Pause;
    end PauseGame;

    ----------------------------------------------------------------------------

    -- ResumeGame() return null
    procedure ResumeGame( state : in out Scribble_State ) is
        pragma Unreferenced( state );
    begin
        Get_Session.Resume;
    end ResumeGame;

    ----------------------------------------------------------------------------

    -- SendView( msg : string, arg1 : any, ... ) return null
    procedure SendView( state : in out Scribble_State ) is
    begin
        Queue_View_Message( Cast_String( state.args(1) ), state.args(2..state.args'Last) );
    end SendView;

end Game_Scripting.Game_API;

