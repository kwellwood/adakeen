--
-- Copyright (c) 2015-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Indefinite_Doubly_Linked_Lists;
with Objects;                           use Objects;

package Signals is

    type Priority_Type is (Low, Normal, High, Exclusive);

    type Match_Bits is mod 4;

    HAS_OBJECT : constant Match_Bits := 2;    -- the Connection has a target object; match it
    HAS_METHOD : constant Match_Bits := 1;    -- the Connection has a target method; match it when target objects are identical

    -- how to match a Connection within Disconnect(). if the Connection object
    -- to be disconnected has no object or method specified (wildcards), then it
    -- matches all of a signal's existing Connections (MATCH_ANY). if the
    -- Connection object to be disconnected has an object but no method, then
    -- it matches all of the signal's Connections to the specified object
    -- because the method is a wildcard (MATCH_OBJECT). if the Connection object
    -- to be disconnected has both an target object and method specified, then
    -- only an exactly matching Connection will be disconnected (MATCH_EXACT).
    MATCH_ANY    : constant Match_Bits := 0;
    MATCH_OBJECT : constant Match_Bits := HAS_OBJECT;
    MATCH_EXACT  : constant Match_Bits := HAS_OBJECT or HAS_METHOD;

    ----------------------------------------------------------------------------
    -- Base_Connection Class --

    type Base_Connection is abstract new Object with
        record
            priority : Priority_Type := Normal;
            match    : Match_Bits := MATCH_EXACT;
        end record;

    -- Returns True if 'this' Connection is equivalent to 'other'.
    not overriding
    function Eq( this : Base_Connection; other : Base_Connection'Class ) return Boolean is abstract;

    -- Returns a pointer to the object that will receive the signal.
    not overriding
    function Get_Object( this : Base_Connection ) return access Base_Object'Class is abstract;

    ----------------------------------------------------------------------------
    -- Base_Signal Class --

    type Base_Signal is new Object with private;

    procedure Construct( this : access Base_Connection; priority : Priority_Type );

    -- Initializes the signal before use. Argument 'sender' is the object
    -- instance emitting the signal. Init() must be called before use.
    procedure Init( this   : in out Base_Signal'Class;
                    sender : access Base_Object'Class := null );

    procedure Disconnect( this : in out Base_Signal'Class; slot : Base_Connection'Class );

    procedure Disconnect_All( this : in out Base_Signal'Class );

    procedure Disconnect_All( this : in out Base_Signal'Class; target : in out Base_Object'Class );

    procedure Disconnect_All( this : in out Base_Signal'Class; target : not null access Base_Object'Class );

    ----------------------------------------------------------------------------
    -- Connection Class --

    type Connection is abstract new Base_Connection with null record;

    not overriding
    procedure Emit( this   : Connection;
                    sender : access Base_Object'Class ) is abstract;

    ----------------------------------------------------------------------------
    -- Signal Class --

    type Signal is new Base_Signal with private;

    procedure Connect( this : in out Signal'Class; slot : Connection'Class );

    procedure Emit( this : Signal'Class );

    ----------------------------------------------------------------------------
    -- Signals.Connections --

    generic
        type Target (<>) is abstract limited new Base_Object with private;
    package Connections is

        type Prototype is (Proto0);

        type A_Method0 is access
            procedure( object : not null access Target'Class );

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        -- Creates a special Connection that can be used to disconnect all slots
        -- from a signal.
        function All_Slots return Base_Connection'Class;

        -- Creates a special Connection that can be used to disconnect all slots
        -- in 'target' from a signal.
        function All_Slots_Of( obj : in out Target'Class ) return Base_Connection'Class;

        function Slot( obj      : in out Target'Class;
                       method   : not null A_Method0;
                       priority : Priority_Type := Normal ) return Connection'Class;

        function Slot( obj      : not null access Target'Class;
                       method   : not null A_Method0;
                       priority : Priority_Type := Normal ) return Connection'Class is (Slot( obj.all, method, priority ));

    private

        type Generic_Connection(proto : Prototype) is new Connection with
            record
                object : access Target'Class := null;
                case proto is
                    when Proto0 => method0 : A_Method0 := null;
                end case;
            end record;

        overriding
        procedure Emit( this   : Generic_Connection;
                        sender : access Base_Object'Class );

        overriding
        function Eq( this : Generic_Connection; other : Base_Connection'Class ) return Boolean;

        overriding
        function Get_Object( this : Generic_Connection ) return access Base_Object'Class is (this.object);

    end Connections;

private

    function Eq_Class( left, right : Base_Connection'Class ) return Boolean is (left.Eq( right ));

    package Connection_Lists is new Ada.Containers.Indefinite_Doubly_Linked_Lists( Base_Connection'Class, Eq_Class );

    ----------------------------------------------------------------------------

    type Base_Signal is new Object with
        record
            sender : access Base_Object'Class := null;
            conns  : Connection_Lists.List;
        end record;

    procedure Connect( this : in out Base_Signal'Class; slot : Base_Connection'Class );

    ----------------------------------------------------------------------------

    type Signal is new Base_Signal with null record;

end Signals;

