--
-- Copyright (c) 2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                        use Debugging;
with Values.Construction;              use Values.Construction;
with Values.Strings;                   use Values.Strings;

package body Scribble.Script_Generators is

    function Create_Script_Generator( runtime : not null A_Scribble_Runtime ) return A_Script_Generator is
        this : constant A_Script_Generator := new Script_Generator;
    begin
        this.Construct( runtime );
        return this;
    end Create_Script_Generator;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this    : access Script_Generator;
                         runtime : not null A_Scribble_Runtime ) is
    begin
        Limited_Object(this.all).Construct;
        this.runtime := runtime;
        this.generator := Create_Value_Generator( this.runtime );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Script_Generator ) is
    begin
        Delete( this.generator );
        Limited_Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Generate( this        : not null access Script_Generator'Class;
                       node        : not null A_Ast_Script;
                       enableDebug : Boolean := False ) return Map_Value is
        result : Map_Value;
    begin
        this.enableDebug := enableDebug;
        this.members := Create_Map.Map;
        this.timers := Create_List.Lst;

        for i in 1..node.Elements_Count loop
            node.Get_Element( i ).Process( A_Ast_Script_Processor(this) );
        end loop;

        result := Create_Map.Map;
        result.Set( "members", this.members, consume => True );
        result.Set( "timers", this.timers, consume => True );
        return result;
    end Generate;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Script_Member( this : access Script_Generator; node : A_Ast_Script_Member ) is
    begin
        this.members.Set( node.Get_Name.Get_Name,
                          this.generator.Generate( node.Get_Expression, this.enableDebug ) );
    end Process_Script_Member;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Timer( this : access Script_Generator; node : A_Ast_Timer ) is
        timer : constant Map_Value := Create_Map.Map;
    begin
        timer.Set( "name", Create( node.Get_Name.Get_Name ) );
        timer.Set( "period", this.generator.Generate( node.Get_Period, this.enableDebug ) );
        timer.Set( "repeat", Create( node.Is_Repeating ) );
        if node.Get_Arguments /= null then
            timer.Set( "args", this.generator.Generate( node.Get_Arguments, this.enableDebug ) );
        end if;

        this.timers.Append( timer );
    end Process_Timer;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Script_Generator ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

end Scribble.Script_Generators;
