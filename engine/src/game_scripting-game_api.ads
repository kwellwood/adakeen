--
-- Copyright (c) 2014-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Scribble.States;                   use Scribble.States;
with Values;                            use Values;

-- This package implements the Oracle Engine Game API that is exposed to the
-- game scripting system.
private package Game_Scripting.Game_API is

    -- LANGUAGE FUNCTIONS --

    -- $index( l : id  , r : string ) return any
    -- $index( l : map , r : string ) return any
    -- $index( l : list, r : number ) return any
    procedure Index( state : in out Scribble_State );

    -- $indexRef( l : id  , r : string ) return ref
    -- $indexRef( l : map , r : string ) return ref
    -- $indexRef( l : list, r : number ) return ref
    procedure IndexRef( state : in out Scribble_State );

    -- $member( object : any, symbol : string ) return any
    procedure Member( state : in out Scribble_State );

    -- $memberRef( object : any, symbol : string ) return ref
    procedure MemberRef( state : in out Scribble_State );

    -- GENERAL FUNCTIONS --

    -- assetStats() return map
    procedure AssetStats( state : in out Scribble_State );

    -- eval( func : function, ... ) return any
    procedure Eval( state : in out Scribble_State );

    -- GetPref( section : string, name : string ) return any
    procedure GetPref( state : in out Scribble_State );

    -- load( path : string, group := "" ) return any
    procedure Load( state : in out Scribble_State );

    ----------------------------------------------------------------------------

    -- print(val : any) return null
    procedure Print( state : in out Scribble_State );

    -- random() return number
    procedure Random( state : in out Scribble_State );

    -- run( func : function, ... ) return any
    procedure Run( state : in out Scribble_State );

    -- SetPref( section : string, name : string, val : any ) return null
    procedure SetPref( state : in out Scribble_State );

    -- EASING FUNCTIONS --

    -- Easing( time, begin, change, duration : number ) return number

    procedure QuadraticEaseIn( state : in out Scribble_State );
    procedure QuadraticEaseOut( state : in out Scribble_State );
    procedure QuadraticEaseInOut( state : in out Scribble_State );

    procedure CubicEaseIn( state : in out Scribble_State );
    procedure CubicEaseOut( state : in out Scribble_State );
    procedure CubicEaseInOut( state : in out Scribble_State );

    procedure QuarticEaseIn( state : in out Scribble_State );
    procedure QuarticEaseOut( state : in out Scribble_State );
    procedure QuarticEaseInOut( state : in out Scribble_State );

    procedure QuinticEaseIn( state : in out Scribble_State );
    procedure QuinticEaseOut( state : in out Scribble_State );
    procedure QuinticEaseInOut( state : in out Scribble_State );

    procedure SineEaseIn( state : in out Scribble_State );
    procedure SineEaseOut( state : in out Scribble_State );
    procedure SineEaseInOut( state : in out Scribble_State );

    procedure CircularEaseIn( state : in out Scribble_State );
    procedure CircularEaseOut( state : in out Scribble_State );
    procedure CircularEaseInOut( state : in out Scribble_State );

    procedure ExponentialEaseIn( state : in out Scribble_State );
    procedure ExponentialEaseOut( state : in out Scribble_State );
    procedure ExponentialEaseInOut( state : in out Scribble_State );

    procedure ElasticEaseIn( state : in out Scribble_State );
    procedure ElasticEaseOut( state : in out Scribble_State );
    procedure ElasticEaseInOut( state : in out Scribble_State );

    procedure BackEaseIn( state : in out Scribble_State );
    procedure BackEaseOut( state : in out Scribble_State );
    procedure BackEaseInOut( state : in out Scribble_State );

    procedure BounceEaseIn( state : in out Scribble_State );
    procedure BounceEaseOut( state : in out Scribble_State );
    procedure BounceEaseInOut( state : in out Scribble_State );

    -- AUDIO FUNCTIONS --

    -- PlayMusic( name : string ) return null
    procedure PlayMusic( state : in out Scribble_State );

    -- StopMusic() return null
    procedure StopMusic( state : in out Scribble_State );

    -- PlaySound( name : string ) return null
    procedure PlaySound( state : in out Scribble_State );

    -- WORLD FUNCTIONS --

    -- LoadWorld( name : string ) return null
    procedure LoadWorld( state : in out Scribble_State );

    -- GetTile( layer : number, x : number, y : number ) return number
    procedure GetTile( state : in out Scribble_State );

    -- GetTileInfo( tileId : number ) return map
    procedure GetTileInfo( state : in out Scribble_State );

    -- IsFloor( x : number, y : number ) return boolean
    procedure IsFloor( state : in out Scribble_State );

    -- SetLayerProperty( layer : number, name : string, val : any ) return null
    procedure SetLayerProperty( state : in out Scribble_State );

    -- SetTile( layer : number, x : number, y : number, tileId : number ) return null
    procedure SetTile( state : in out Scribble_State );

    -- ENTITY FUNCTIONS --

    -- Spawn( template : string, properties : map ) return id
    procedure Spawn( state : in out Scribble_State );

    -- SpawnAt( template : string, x : number, y : number, properties : map := {} ) return id
    procedure SpawnAt( state : in out Scribble_State );

    -- Destroy( entity : id ) return null
    procedure Destroy( state : in out Scribble_State );

    -- EntityExists( entity : id ) return boolean
    procedure EntityExists( state : in out Scribble_State );

    -- NearPlayer( entity : id ) return boolean
    procedure NearPlayer( state : in out Scribble_State );

    -- AddComponent( entity : id, component : string, properties : map := {} ) return id
    procedure AddComponent( state : in out Scribble_State );

    -- RemoveComponent( entity : id, component : id ) return null
    procedure RemoveComponent( state : in out Scribble_State );

    -- RemoveComponentFamily( entity : id, family : string ) return null
    procedure RemoveComponentFamily( state : in out Scribble_State );

    -- Send( entity : id, name : string, params : map := {} ) return null
    procedure Send( state : in out Scribble_State );

    -- COMPONENT FUNCTIONS --

    -- CancelScriptTimer( script : id, name : string ) return null
    procedure CancelScriptTimer( state : in out Scribble_State );

    -- SetScriptTimer( script : id,
    --                 name   : string,
    --                 time   : number,
    --                 repeat : boolean := false,
    --                 args   : list := []) return null
    procedure SetScriptTimer( state : in out Scribble_State );

    -- PARTICLE FUNCTIONS --

    -- ParticleBurst( emitter : string or map,
    --                x       : number,
    --                y       : number,
    --                z       : number,
    --                count   : number ) return null
    procedure ParticleBurst( state : in out Scribble_State );

    -- ParticleStream( emitter  : string or map,
    --                 x        : number,
    --                 y        : number,
    --                 z        : number,
    --                 rate     : number,
    --                 lifetime : number,
    --                 entityId : id := null ) return null
    procedure ParticleStream( state : in out Scribble_State );

    -- GAME FUNCTIONS --

    -- PauseGame() return null
    procedure PauseGame( state : in out Scribble_State );

    -- ResumeGame() return null
    procedure ResumeGame( state : in out Scribble_State );

    -- SendView( msg : string, arg1 : any, ... ) return null
    procedure SendView( state : in out Scribble_State );

end Game_Scripting.Game_API;

