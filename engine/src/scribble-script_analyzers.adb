--
-- Copyright (c) 2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Scribble.Parse_Errors;             use Scribble.Parse_Errors;
with Scribble.Token_Locations;          use Scribble.Token_Locations;

package body Scribble.Script_Analyzers is

    function Create_Script_Analyzer( runtime : not null A_Scribble_Runtime ) return A_Script_Analyzer is
        this : constant A_Script_Analyzer := new Script_Analyzer;
    begin
        this.Construct( runtime );
        return this;
    end Create_Script_Analyzer;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this    : access Script_Analyzer;
                         runtime : not null A_Scribble_Runtime ) is
    begin
        Limited_Object(this.all).Construct;
        this.runtime := runtime;
        this.child := Create_Semantic_Analyzer( this.runtime );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Script_Analyzer ) is
    begin
        Delete( this.child );
        Limited_Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Analyze( this   : not null access Script_Analyzer'Class;
                       script : not null A_Ast_Script ) is
    begin
        this.members.Clear;

        for i in 1..script.Elements_Count loop
            script.Get_Element( i ).Process( A_Ast_Script_Processor(this) );
        end loop;

        for member of this.members loop
            if member.definition /= null then
                -- Rule 1: Member names cannot be match any reserved name in the Script component API.
                if "entity" = member.definition.Get_Name.Get_Name then
                    Raise_Parse_Error( "Name disallowed", member.definition.Get_Name.Location );
                end if;
            end if;

            -- Rule 2: Required members must be defined later.
            if member.required /= null and then member.definition = null then
                Raise_Parse_Error( "Required member is undefined", member.required.Location );
            end if;
        end loop;
    end Analyze;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Required_Member( this : access Script_Analyzer; node : A_Ast_Required_Member ) is
        name   : constant String := node.Get_Name.Get_Name;
        member : Member_Rec;
    begin
        if this.members.Contains( name ) then
            member := this.members.Element( name );

            -- Rule 3: Defined members cannot be required later.
            if member.definition /= null then
                Raise_Parse_Error( "Member previously defined at " & Image( member.definition.Location, implicitPath => node.Location.path ),
                                   node.Location );
            end if;

            -- member already required but not yet defined
            -- duplicate requirement is ok when not yet defined
        else
            -- adding the member requirement
            this.members.Insert( name, Member_Rec'(required => node, others => <>) );
        end if;
    end Process_Required_Member;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Script_Member( this : access Script_Analyzer; node : A_Ast_Script_Member ) is
        name   : constant String := node.Get_Name.Get_Name;
        member : Member_Rec;
    begin
        node.Get_Expression.Process( this.child );

        if this.members.Contains( name ) then
            member := this.members.Element( name );

            if member.definition /= null then
                -- Rule 4: Defined members cannot be marked default later.
                if node.Is_Default then
                    Raise_Parse_Error( "Member previously defined at " & Image( member.definition.Location, implicitPath => node.Location.path ),
                                       node.Location );
                end if;

                -- Rule 5: Members can only override a member defined as default.
                if not member.definition.Is_Default then
                    Raise_Parse_Error( "Member previously defined at " & Image( member.definition.Location, implicitPath => node.Location.path ),
                                       node.Location );
                end if;
            end if;

            -- overriding the default
            member.definition := node;
            this.members.Replace_Element( this.members.Find( name ), member );
        else
            -- defining the member (default or not)
            this.members.Insert( name, Member_Rec'(definition => node, others => <>) );
        end if;
    end Process_Script_Member;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Timer( this : access Script_Analyzer; node : A_Ast_Timer ) is
    begin
        node.Get_Period.Process( this.child );
        if node.Get_Arguments /= null then
            node.Get_Arguments.Process( this.child );
        end if;

        -- Rule 6: Timer names must match a member- required or defined.
        if not this.members.Contains( node.Get_Name.Get_Name ) then
            Raise_Parse_Error( "Undefined function referenced", node.Get_Name.Location );
        end if;

        -- Rule 7: Timer periods must be positive.
        if A_Ast_Literal(node.Get_Period).Get_Value.To_Float <= 0.0 then
            Raise_Parse_Error( "Positive period required", node.Get_Period.Location );
        end if;

        -- adding a new timer is ok
    end Process_Timer;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Script_Analyzer ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

end Scribble.Script_Analyzers;
