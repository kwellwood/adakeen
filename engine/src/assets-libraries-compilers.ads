--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Indefinite_Hashed_Maps;
with Ada.Strings.Equal_Case_Insensitive;
with Ada.Strings.Hash_Case_Insensitive;

package Assets.Libraries.Compilers is

    type A_Output_Proc is
        access procedure( msg : String; error : Boolean := False );

    type Compiler is new Limited_Object with private;
    type A_Compiler is access all Compiler'Class;

    -- Creates a new compiler. During compilation, messages and errors will be
    -- written to the output procedure 'output'.
    function Create_Compiler( output : A_Output_Proc ) return A_Compiler;
    pragma Postcondition( Create_Compiler'Result /= null );

    -- Compiles the tile list 'tileListPath', matrix list 'matrixListPath'
    -- (optional), terrains file 'terrainsPath' (optional), and all referenced
    -- image files into new a tile library file at 'libPath'. If 'compress' is
    -- true, the tile library file will be compressed. Compression improves disk
    -- usage at the cost of load time. Null will be returned for 'lib' if
    -- compilation fails.
    --
    -- Compilation messages, including errors, will be written to the compiler's
    -- output procedure. True will be returned on success, or False on failure.
    function Compile( this           : not null access Compiler'Class;
                      libPath        : String;
                      tileListPath   : String;
                      matrixListPath : String;
                      terrainsPath   : String;
                      compress       : Boolean ) return Boolean;
    pragma Precondition( libPath'Length > 0 );
    pragma Precondition( tileListPath'Length > 0 );

    -- Deletes the compiler.
    procedure Delete( this : in out A_Compiler );

private

    package String_Maps is new
        Ada.Containers.Indefinite_Hashed_Maps( String, String,
                                               Ada.Strings.Hash_Case_Insensitive,
                                               Ada.Strings.Equal_Case_Insensitive,
                                               Ada.Strings.Equal_Case_Insensitive );

    type Compiler is new Limited_Object with
        record
            -- constant for the life of the instance
            output       : A_Output_Proc := null;

            -- modified during Compile()
            lib          : A_Tile_Library;         -- the library being compiled
            outputDir    : Unbounded_String;       -- dir of the output file
            tempDir      : Unbounded_String;       -- temporary directory to use
            error        : Boolean := False;       -- compilation error occurred?

            files        : String_Maps.Map;        -- path-in-zip => source-path
            imageCount   : Natural := 0;           -- number of image files in lib
            tileCount    : Natural := 0;           -- number of tiles in lib
            matrixCount  : Natural := 0;           -- number of matrices in lib
            terrainCount : Natural := 0;           -- number of terrains in lib
        end record;

    procedure Construct( this : access Compiler; output : A_Output_Proc );

    -- Marks the file at filePath to be added to the zip at the relative path
    -- 'pathInZip'. True will be returned on success, or False if a matching
    -- pathInZip has already been added for a different source file.
    function Add_File( this      : not null access Compiler'Class;
                       pathInZip : String;
                       filePath  : String ) return Boolean;

    -- Writes a message to the output procedure as an error. The 'error' field
    -- will be set true.
    procedure Put_Error( this : not null access Compiler'Class;
                         msg  : String );

    -- Writes a message to the output procedure. The 'error' field will be set
    -- true if 'error' is true.
    procedure Put_Line( this  : not null access Compiler'Class;
                        msg   : String;
                        error : Boolean := False );

    -- Reads a tile index file from 'path', adding all unique bitmaps to the
    -- list of files to put in the library.
    procedure Read_Index_File( this : not null access Compiler'Class;
                               path : String );

    -- Compiles and validates a matrix list file from 'path', writing it as a
    -- binary object to 'outPath'.
    --
    -- Matrix File Format:
    -- [
    -- {
    --     name: "<MatrixName>"
    --     icon: <TileId>
    --     type: "<MatrixType>"
    --     rows: [ <ROW>, <ROW>, ... ]
    -- }
    -- ... more matrices
    -- ]
    --
    -- Where:
    --     <MatrixType> is either "object" or "pattern".
    --     <ROW> is one of the following row definitions:
    --
    --         [Column1, Column2, ... ]                       <-- Layer 0 is implicit
    --
    --         OR
    --
    --         [<Layer1>, [<Column1, Column2, ... ],          <-- Layer number is explicit
    --          <Layer2>, [<Column1, Column2, ... ],
    --          ... ]
    --
    procedure Compile_Matrix_File( this    : not null access Compiler'Class;
                                   path    : String;
                                   outPath : String );

    -- Compiles and validates a terrain definitions file from 'path', writing it
    -- as a binary object to 'outPath'.
    --
    -- Terrain File Format:
    -- [
    -- {
    --     name: "<TerrainName>"
    --     icon: <TileId>
    --     definition: [
    --         <Angle1>, [[<Layer>, <XorYoffset>, <TileId>], ...],        <-- Primary
    --                   [[<Layer>, <XorYoffset>, <TileId>], ...],        <-- Alternate(s)
    --                   ...,                                             <--
    --         <Angle2>, [ ... ],
    --         ...
    --         <AngleA>, <AngleB>, [[<Layer>, <Xoffset>, <Yoffset>, <TileId>], ...],    <-- Corner
    --         <AngleC>, <AngleD>, [ ... ],                                             <-- Another corner
    --         ...
    --     ]
    -- }
    -- ... more terrains ...
    -- ]
    --
    --
    -- Angles: Left = 90, Down = 180, Right = 270, Up = 360
    --
    -- For straight paths, a single angle in degrees specifies the angle of the
    -- path, and a list of slices follows it. The first slice is required, and
    -- it will alternate with any additional slices. A slice is composed of a
    -- list of tiles <TileId>. Each tile has a specific layer number <Layer> and
    -- offset from the path. For angles 180 and 360, the <XorYoffset> is in the
    -- X axis; for all other path angles, the offset is in the Y axis.
    --
    -- For corner paths, two angles are specified in degrees: first is the angle
    -- of the path as it approaches the corner, and second is the nagle of the
    -- path as it leaves the corner. A single list of tiles follows the angles.
    -- Each element in the list specifies a tile <TileId> in a layer <Layer>
    -- relative to the corner <Xoffset>, <Yoffset>.
    procedure Compile_Terrain_File( this    : not null access Compiler'Class;
                                    path    : String;
                                    outPath : String );

    -- Writes a binary tile catalog file to 'path' and adds it to the list of
    -- files to put in the library. Any tiles with unresolved ids will be
    -- resolved.
    procedure Write_Catalog_File( this : not null access Compiler'Class;
                                  path : String );

    -- Reads a textual info file to 'path' and adds it to the list of files to
    -- put in the library.
    procedure Write_Info_File( this : not null access Compiler'Class;
                               path : String );

    -- Writes the tile library zip file to 'path', containing all files in the
    -- 'files' field. If 'compress' is True, the zip will be compressed.
    procedure Write_Library_File( this     : not null access Compiler'Class;
                                  path     : String;
                                  compress : Boolean );

end Assets.Libraries.Compilers;
