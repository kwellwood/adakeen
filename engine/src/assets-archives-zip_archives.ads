--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Streams;                       use Ada.Streams;
with Zip;                               use Zip;
with Zip_Streams.Array_Streams;         use Zip_Streams.Array_Streams;

package Assets.Archives.Zip_Archives is

    -- This archive loader can load a .zip formatted archive from a resource
    -- file. Register it to support the ZIP file format.
    function Load_Zip( assetId : Value; filePath : String; group : String ) return A_Archive;
    pragma Postcondition( Load_Zip'Result /= null );

private

    -- Implements an Archive class for .zip compressed files with .zip and
    -- .tlz (tile library zip) extensions.
    type Zip_Archive is new Archive with
        record
            zipArray : A_Array_Stream := null;
            zipInfo  : Zip_info;
        end record;

    -- Returns True if a file 'filePath' exists in the Archive.
    function File_Exists( this : access Zip_Archive; filePath : String ) return Boolean;

    function Load_Data( this : access Zip_Archive ) return Boolean;

    -- Reads file 'filePath' from the Archive and returns it as a Resource file.
    -- The caller takes ownership of the returned object; closing the archive
    -- does not affect it. Returns null if 'filePath' does not exist or cannot
    -- be read.
    function Load_File( this : access Zip_Archive; filePath : String ) return A_Resource_File;

    function Load_Raw( this : not null access Zip_Archive'Class; filePath : String ) return access Stream_Element_Array;

    procedure Search( this     : access Zip_Archive;
                      wildcard : String;
                      process  : access procedure( entryName : String ) );

    procedure Unload_Data( this : access Zip_Archive );

end Assets.Archives.Zip_Archives;
