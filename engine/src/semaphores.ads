--
-- Copyright (c) 2016-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Semaphores is

    protected type Binary_Semaphore(initial : Boolean) is

        -- Resets the semaphore to its unsignalled state, blocking any threads
        -- that attempt to Sieze or Wait on the semaphore.
        procedure Reset;

        -- Sets the semaphore to its signalled state, releasing threads waiting
        -- on the semaphore.
        procedure Release;
        procedure Set;             -- same as Release

        -- Waits until the semaphore is signalled.
        entry Wait;

        -- Allows one thread at a time to get the semaphore, each thread
        -- resetting it after the wait.
        entry Sieze;
        entry Wait_One;            -- same as Sieze

    private
        signalled : Boolean := initial;
    end Binary_Semaphore;

    ----------------------------------------------------------------------------

    protected type Counting_Semaphore(initial : Integer) is

        -- Gives resources to the semaphore, increasing its available resources
        -- by 'counts'.
        procedure Set( counts : Integer := 1 );
        procedure Release;         -- same as Set( 1 )

        -- Waits until the semaphore has resources, then consumes one, reducing
        -- available resources by 1.
        entry Sieze;
        entry Wait_One;            -- same as Sieze

    private
        available : Integer := initial;
    end Counting_Semaphore;

end Semaphores;
