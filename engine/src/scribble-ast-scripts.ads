--
-- Copyright (c) 2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Vectors;
--with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Scribble.Ast;                      use Scribble.Ast;
with Scribble.Ast.Expressions;          use Scribble.Ast.Expressions;
with Scribble.Ast.Expressions.Symbols;  use Scribble.Ast.Expressions.Symbols;

limited with Scribble.Ast.Processors;
limited with Scribble.Ast.Script_Processors;

package Scribble.Ast.Scripts is

    SCRIPT          : constant Ast_Type := USER_AST_START;
    SCRIPT_MEMBER   : constant Ast_Type := SCRIPT + 1;
    REQUIRED_MEMBER : constant Ast_Type := SCRIPT + 2;
    SCRIPT_TIMER    : constant Ast_Type := SCRIPT + 3;

    ----------------------------------------------------------------------------

    type Ast_Script_Element is abstract new Ast_Node with private;
    type A_Ast_Script_Element is access all Ast_Script_Element'Class;

    function Get_Name( this : not null access Ast_Script_Element'Class ) return A_Ast_Identifier;

    not overriding
    procedure Process( this      : access Ast_Script_Element;
                       processor : access Scribble.Ast.Script_Processors.Ast_Script_Processor'Class ) is abstract;

    not overriding
    procedure Prune( this : access Ast_Script_Element ) is abstract;

    ----------------------------------------------------------------------------

    type Ast_Required_Member is new Ast_Script_Element with private;
    type A_Ast_Required_Member is access all Ast_Required_Member'Class;

    function Create_Required_Member( loc  : Token_Location;
                                     name : not null A_Ast_Identifier ) return A_Ast_Script_Element;

    overriding
    function Get_Type( this : access Ast_Required_Member ) return Ast_Type is (REQUIRED_MEMBER);

    ----------------------------------------------------------------------------

    type Ast_Script_Member is new Ast_Script_Element with private;
    type A_Ast_Script_Member is access all Ast_Script_Member'Class;

    function Create_Script_Member( loc     : Token_Location;
                                   name    : not null A_Ast_Identifier;
                                   expr    : not null A_Ast_Expression;
                                   default : Boolean ) return A_Ast_Script_Element;

    function Get_Expression( this : not null access Ast_Script_Member'Class ) return A_Ast_Expression;

    overriding
    function Get_Type( this : access Ast_Script_Member ) return Ast_Type is (SCRIPT_MEMBER);

    function Is_Default( this : not null access Ast_Script_Member'Class ) return Boolean;

    ----------------------------------------------------------------------------

    type Ast_Timer is new Ast_Script_Element with private;
    type A_Ast_Timer is access all Ast_Timer'Class;

    function Create_Timer( loc    : Token_Location;
                           name   : not null A_Ast_Identifier;
                           args   : A_Ast_Expression;
                           period : not null A_Ast_Expression;
                           repeat : Boolean ) return A_Ast_Script_Element;

    function Get_Arguments( this : not null access Ast_Timer'Class ) return A_Ast_Expression;

    function Get_Period( this : not null access Ast_Timer'Class ) return A_Ast_Expression;
    pragma Postcondition( Get_Period'Result /= null );
    pragma Postcondition( Get_Period'Result.all.Get_Type = LITERAL );

    overriding
    function Get_Type( this : access Ast_Timer ) return Ast_Type is (SCRIPT_TIMER);

    function Is_Repeating( this : not null access Ast_Timer'Class ) return Boolean;

    ----------------------------------------------------------------------------

    type Ast_Script is new Ast_Node with private;
    type A_Ast_Script is access all Ast_Script'Class;

    function Create_Script( loc : Token_Location ) return A_Ast_Script;

    procedure Add_Element( this    : not null access Ast_Script'Class;
                           element : not null A_Ast_Script_Element );

    function Elements_Count( this : not null access Ast_Script'Class ) return Natural;

    function Get_Element( this : not null access Ast_Script'Class; index : Integer ) return A_Ast_Script_Element;

    overriding
    function Get_Type( this : access Ast_Script ) return Ast_Type is (SCRIPT);

    procedure Prune( this : not null access Ast_Script'Class );

private

    type Ast_Script_Element is abstract new Ast_Node with
        record
            name : A_Ast_Identifier := null;
        end record;

    procedure Construct( this : access Ast_Script_Element;
                         loc  : Token_Location;
                         name : not null A_Ast_Identifier );

    procedure Delete( this : in out Ast_Script_Element );

    overriding
    procedure Process( this      : access Ast_Script_Element;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class ) is null;

    ----------------------------------------------------------------------------

    type Ast_Required_Member is new Ast_Script_Element with null record;

    procedure Process( this      : access Ast_Required_Member;
                       processor : access Scribble.Ast.Script_Processors.Ast_Script_Processor'Class );

    overriding
    procedure Prune( this : access Ast_Required_Member ) is null;

    ----------------------------------------------------------------------------

    type Ast_Script_Member is new Ast_Script_Element with
        record
            expr    : A_Ast_Expression := null;
            default : Boolean := False;
        end record;

    procedure Delete( this : in out Ast_Script_Member );

    procedure Process( this      : access Ast_Script_Member;
                       processor : access Scribble.Ast.Script_Processors.Ast_Script_Processor'Class );

    procedure Prune( this : access Ast_Script_Member );

    ----------------------------------------------------------------------------

    type Ast_Timer is new Ast_Script_Element with
        record
            args   : A_Ast_Expression;
            period : A_Ast_Expression;
            repeat : Boolean := False;
        end record;

    procedure Delete( this : in out Ast_Timer );

    procedure Process( this      : access Ast_Timer;
                       processor : access Scribble.Ast.Script_Processors.Ast_Script_Processor'Class );

    procedure Prune( this : access Ast_Timer );

    ----------------------------------------------------------------------------

    package Element_Vectors is new Ada.Containers.Vectors(Positive, A_Ast_Script_Element, "=");

    type Ast_Script is new Ast_Node with
        record
            elements : Element_Vectors.Vector;
        end record;

    procedure Delete( this : in out Ast_Script );

    overriding
    procedure Process( this      : access Ast_Script;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class ) is null;

end Scribble.Ast.Scripts;
