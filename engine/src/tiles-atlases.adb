--
-- Copyright (c) 2012-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Ada.Unchecked_Deallocation;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Color;                     use Allegro.Color;
with Allegro.State;                     use Allegro.State;
--with Debugging;                         use Debugging;
with Drawing;                           use Drawing;
with Drawing.Blending;                  use Drawing.Blending;
with Support;                           use Support;

package body Tiles.Atlases is

    function Create_Atlas( display    : not null A_Allegro_Display;
                           pageWidth,
                           pageHeight : Positive ) return A_Atlas is
        this : constant A_Atlas := new Atlas;
    begin
        this.Construct( display, pageWidth, pageHeight );
        return this;
    end Create_Atlas;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this       : in out Atlas;
                         display    : not null A_Allegro_Display;
                         pageWidth,
                         pageHeight : Positive ) is
    begin
        Limited_Object(this).Construct;
        this.display := display;
        this.pageWidth := Integer'Min( pageWidth, Al_Get_Display_Option( display, ALLEGRO_MAX_BITMAP_SIZE ) );
        this.pageHeight := Integer'Min( pageHeight, Al_Get_Display_Option( display, ALLEGRO_MAX_BITMAP_SIZE ) );
        this.Add_Page;
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Atlas ) is
        procedure Free is new Ada.Unchecked_Deallocation( Page, A_Page );
        p : A_Page;
    begin
        while not this.pages.Is_Empty loop
            p := this.pages.First_Element;
            Al_Destroy_Bitmap( p.bmp );
            Delete( A_Bin(p.bin) );
            Free( p );

            this.pages.Delete_First;
        end loop;

        Limited_Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Add_Bitmap( this     : not null access Atlas'Class;
                         filename : String;
                         bmp      : not null A_Allegro_Bitmap ) return Boolean is
        lfilename : constant String := To_Lower( filename );

        ------------------------------------------------------------------------

        procedure Store_Bitmap( page : A_Page;
                                bmp  : A_Allegro_Bitmap;
                                area : Rect ) is
            state  : aliased Allegro_State;
            state2 : aliased Allegro_State;
        begin
            Al_Store_State( state, ALLEGRO_STATE_TARGET_BITMAP or ALLEGRO_STATE_BLENDER );

            Set_Target_Bitmap( page.bmp );
            Set_Blend_Mode( state2, Overwrite );
            Al_Draw_Bitmap( bmp, Float(area.x), Float(area.y), 0 );
            Restore_Blend_Mode( state2 );

            Al_Restore_State( state );
        end Store_Bitmap;

        ------------------------------------------------------------------------

        width  : constant Integer := Al_Get_Bitmap_Width( bmp );
        height : constant Integer := Al_Get_Bitmap_Height( bmp );
        page   : A_Page;
        area   : Rect;      -- area reserved in the page's bitmap
    begin
        if this.filenames.Contains( lfilename ) then
            return False;
        end if;

        for p in 1..Integer(this.pages.Length) loop
            page := this.pages.Element( p );
            if not page.full then
                -- allocate a bin with a one pixel border on the bottom and right
                -- to protect against drawing artifacts when drawing sub bitmaps
                -- from the atlas with fractional scaling and translation.
                area := page.bin.Insert( width + 1, height + 1 );
                if area.height > 0 then
                    this.filenames.Insert( lfilename, Bitmap_Pos'(p, area.x, area.y, width, height) );
                    Store_Bitmap( page, bmp, area );
                    return True;
                elsif Integer'Max( width, height ) <= 16 then
                    -- If a bitmap this small didn't fit, then consider it full
                    page.full := True;
                end if;
            end if;
        end loop;

        this.Add_Page;
        page := this.pages.Last_Element;
        area := page.bin.Insert( width + 1, height + 1 );
        if area.height > 0 then
            this.filenames.Insert( lfilename, Bitmap_Pos'(Integer(this.pages.Length), area.x, area.y, width, height) );
            Store_Bitmap( page, bmp, area );
            return True;
        end if;

        -- the bitmap still didn't fit on a page; this really shouldn't happen
        return False;
    end Add_Bitmap;

    ----------------------------------------------------------------------------

    procedure Add_Page( this : not null access Atlas'Class ) is
        p     : constant A_Page := new Page;
        state : aliased Allegro_State;
    begin
        Al_Store_State( state, ALLEGRO_STATE_ALL );
        p.bin := Create_Bin( this.pageWidth, this.pageHeight );
        p.bmp := Al_Create_Bitmap( this.pageWidth, this.pageHeight );
        Set_Target_Bitmap( p.bmp );
        Clear_To_Color( Al_Map_RGBA( 255, 0, 255, 0 ) );
        this.pages.Append( p );
        Al_Restore_State( state );
    end Add_Page;

    ----------------------------------------------------------------------------

    function Get_Bitmap( this          : not null access Atlas'Class;
                         filename      : String;
                         x, y          : Integer := 0;
                         width, height : Integer := 0 ) return A_Allegro_Bitmap is
        use Bitmap_Maps;
        pos    : constant Bitmap_Maps.Cursor := this.filenames.Find( To_Lower( filename ) );
        br     : Bitmap_Pos;
        x1, y1 : Integer;
        w1, h1 : Integer;
    begin
        if Has_Element( pos ) then
            br := Element( pos );
            x1 := Constrain( x, 0, br.w - 1 );
            y1 := Constrain( y, 0, br.h - 1 );
            w1 := Constrain( (if width > 0 then width else br.w), 1, br.w - x1 );
            h1 := Constrain( (if height > 0 then height else br.h), 1, br.h - y1 );
            return Al_Create_Sub_Bitmap( this.pages.Element( br.page ).bmp,
                                         br.x + x1, br.y + y1, w1, h1 );
        end if;
        return null;    -- not found
    end Get_Bitmap;

    ----------------------------------------------------------------------------

    procedure Save_Pages( this : not null access Atlas'Class; name : String ) is
    begin
        for p in 1..Integer(this.pages.Length) loop
            Al_Save_Bitmap( name & Image( p ) & ".png", this.pages.Element( p ).bmp );
        end loop;
    end Save_Pages;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Atlas ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

end Tiles.Atlases;
