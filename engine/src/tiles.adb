--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Assertions;                    use Ada.Assertions;
--with Debugging;                         use Debugging;
with Support;                           use Support;
with Values.Casting;                    use Values.Casting;
with Values.Construction;               use Values.Construction;
with Values.Streaming;                  use Values.Streaming;
with Values.Strings;                    use Values.Strings;

package body Tiles is

    function Cast_Clip_Type is new Cast_Enum( Clip_Type );

    function Cast_Fill_Mode is new Cast_Enum( Fill_Mode );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    protected body Completion is

        procedure Start( success : out Boolean ) is
        begin
            if not started and then not complete then
                success := True;
                started := True;
            else
                success := False;
            end if;
        end Start;

        ------------------------------------------------------------------------

        procedure Notify_Complete is
        begin
            pragma Assert( started, "Notify_Complete without Start" );
            complete := True;
        end Notify_Complete;

        ------------------------------------------------------------------------

        function Is_Complete return Boolean is (complete);

        ------------------------------------------------------------------------

        entry Wait when complete is
        begin
            null;
        end Wait;

    end Completion;

    --==========================================================================

    function Create_Tile return A_Tile is
        this : constant A_Tile := new Tile_Object;
    begin
        this.Construct;
        return this;
    end Create_Tile;

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access Tile_Object ) is
    begin
        Limited_Object(this.all).Construct;
        this.lock := new Mutex;
        this.attributes := Create_Map.Map;
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Tile_Object ) is
    begin
        this.lock.Lock;
        Al_Destroy_Bitmap( this.bmp );
        Delete( this.lock );
        Limited_Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Get_Attribute( this : access Tile_Object'Class;
                            name : String ) return Value is
    begin
        if this /= null then
            this.lock.Lock;
            -- because Get_Attribute can be called from a different thread than
            -- the one that created this.attributes, the returned value must be
            -- a copy of the specific attribute value; it may be crossing into
            -- another thread.
            return val : constant Value := Clone( this.attributes.Get( name ) ) do
                this.lock.Unlock;
            end return;
        else
            return Null_Value;
        end if;
    end Get_Attribute;

    ----------------------------------------------------------------------------

    function Get_Attributes( this : access Tile_Object'Class ) return Map_Value is
        result : constant Map_Value := Create_Map.Map;
    begin
        if this /= null then
            this.lock.Lock;

            -- because Get_Attributes can be called from a different thread than
            -- the one that created this.attributes, the returned value must be
            -- a copy of the specific attribute value; it may be crossing into
            -- another thread.
            result.Set( "id",       Create( this.id ) );
            result.Set( "filename", Create( this.name ) );
            result.Merge( this.attributes );

            this.lock.Unlock;
        end if;
        return result;
    end Get_Attributes;

    ----------------------------------------------------------------------------

    function Get_Bitmap( this : access Tile_Object'Class ) return A_Allegro_Bitmap
    is ((if this /= null and then this.loadState.Is_Complete then this.bmp else null));

    ----------------------------------------------------------------------------

    function Get_Height( this : access Tile_Object'Class ) return Float
    is ((if this /= null then Float(Al_Get_Bitmap_Height( this.bmp )) else 0.0));

    ----------------------------------------------------------------------------

    function Get_Id( this : access Tile_Object'Class ) return Natural is ((if this /= null then this.id else 0));

    ----------------------------------------------------------------------------

    function Get_Mask( this : access Tile_Object'Class ) return A_Allegro_Bitmap
    is ((if this /= null and then this.loadState.Is_Complete then this.mask else null));

    ----------------------------------------------------------------------------

    function Get_Name( this : not null access Tile_Object'Class ) return String is (To_String( this.name ));

    ----------------------------------------------------------------------------

    function Get_Width( this : access Tile_Object'Class ) return Float
    is ((if this /= null then Float(Al_Get_Bitmap_Width( this.bmp )) else 0.0));

    ----------------------------------------------------------------------------

    function Get_BorderBottom( this : not null access Tile_Object'Class ) return Float is (this.borderBottom);

    function Get_BorderLeft( this : not null access Tile_Object'Class ) return Float is (this.borderLeft);

    function Get_BorderRight( this : not null access Tile_Object'Class ) return Float is (this.borderRight);

    function Get_BorderTop( this : not null access Tile_Object'Class ) return Float is (this.borderTop);

    function Get_Delay( this : not null access Tile_Object'Class ) return Integer is (this.dlay);

    function Get_FillMode( this : not null access Tile_Object'Class ) return Fill_Mode is (this.fillMode);

    function Get_Light( this : not null access Tile_Object'Class ) return Float is (this.light);

    function Get_LightMap( this : not null access Tile_Object'Class ) return Integer is (this.lightMap);

    function Get_LightMapX( this : not null access Tile_Object'Class ) return Integer is (this.lightMapX);

    function Get_LightMapY( this : not null access Tile_Object'Class ) return Integer is (this.lightMapY);

    function Get_OffsetX( this  : not null access Tile_Object'Class ) return Float is (this.offsetX);

    function Get_OffsetY( this  : not null access Tile_Object'Class ) return Float is (this.offsetY);

    function Get_Shape( this : access Tile_Object'Class ) return Clip_Type
        is (if this /= null then this.shape else Passive);

    function Get_TypeBits( this : not null access Tile_Object'Class ) return Tile_Bits is (this.typeBits);

    function Has_TypeBits( this : not null access Tile_Object'Class; bits : Tile_Bits ) return Boolean is ((this.typeBits and bits) = bits);

    ----------------------------------------------------------------------------

    function Is_Loaded( this : not null access Tile_Object'Class ) return Boolean is (this.loadState.Is_Complete);

    ----------------------------------------------------------------------------

    function Is_Mask_Required( this : not null access Tile_Object'Class ) return Boolean
    is (this.light > 0.0);

    ----------------------------------------------------------------------------

    not overriding
    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Tile_Object ) is
    begin
        obj.id := Integer'Input( stream );
        obj.name := Read_String_16( stream );
        obj.attributes := Value_Input( stream ).Map;
        obj.borderBottom := Cast_Float( obj.attributes.Get( "borderBottom" ), 0.0 );
        obj.borderLeft := Cast_Float( obj.attributes.Get( "borderLeft" ), 0.0 );
        obj.borderRight := Cast_Float( obj.attributes.Get( "borderRight" ), 0.0 );
        obj.borderTop := Cast_Float( obj.attributes.Get( "borderTop" ), 0.0 );
        obj.dlay := Cast_Int( obj.attributes.Get( "delay" ), 0 );
        obj.fillMode := Cast_Fill_Mode( obj.attributes.Get( "fillMode" ), Center );
        obj.light := Cast_Float( obj.attributes.Get( "light" ), 0.0 );
        obj.lightMap := Cast_Int( obj.attributes.Get( "lightMap" ), 0 );
        obj.lightMapX := Cast_Int( obj.attributes.Get( "lightMapX" ), 0 );
        obj.lightMapY := Cast_Int( obj.attributes.Get( "lightMapY" ), 0 );
        obj.offsetX := Cast_Float( obj.attributes.Get( "offsetX" ), 0.0 );
        obj.offsetY := Cast_Float( obj.attributes.Get( "offsetY" ), 0.0 );
        obj.shape := Cast_Clip_Type( obj.attributes.Get( "shape" ), Passive );
        obj.typeBits := Tile_Bits(Cast_Int( obj.attributes.Get( "typeBits" ), 0 ));
    end Object_Read;

    ----------------------------------------------------------------------------

    not overriding
    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Tile_Object ) is
    begin
        Integer'Output( stream, obj.id );
        Write_String_16( stream, obj.name );
        obj.lock.Lock;
        Value_Output( stream, obj.attributes );
        obj.lock.Unlock;
    end Object_Write;

    ----------------------------------------------------------------------------

    procedure Set_Attribute( this : not null access Tile_Object'Class;
                             name : String;
                             val  : Value'Class ) is
    begin
        this.lock.Lock;
        this.attributes.Set( name, val, consume => False );   -- must be copied across threads
        if name = "borderBottom" then
            this.borderBottom := Cast_Float( val, 0.0 );
        elsif name = "borderLeft" then
            this.borderLeft := Cast_Float( val, 0.0 );
        elsif name = "borderRight" then
            this.borderRight := Cast_Float( val, 0.0 );
        elsif name = "borderTop" then
            this.borderTop := Cast_Float( val, 0.0 );
        elsif name = "delay" then
            this.dlay := Cast_Int( val, 0 );
        elsif name = "fillMode" then
            this.fillMode := Cast_Fill_Mode( val, Center );
        elsif name = "light" then
            this.light := Cast_Float( val, 0.0 );
        elsif name = "lightMap" then
            this.lightMap := Cast_Int( val, 0 );
        elsif name = "lightMapX" then
            this.lightMapX := Cast_Int( val, 0 );
        elsif name = "lightMapY" then
            this.lightMapY := Cast_Int( val, 0 );
        elsif name = "offsetX" then
            this.offsetX := Cast_Float( val, 0.0 );
        elsif name = "offsetY" then
            this.offsetY := Cast_Float( val, 0.0 );
        elsif name = "shape" then
            this.shape := Cast_Clip_Type( val, Passive );
        elsif name = "typeBits" then
            this.typeBits := Tile_Bits(Cast_Int( val, 0 ));
        end if;
        this.lock.Unlock;
    end Set_Attribute;

    ----------------------------------------------------------------------------

    procedure Set_Bitmap( this : not null access Tile_Object'Class;
                          bmp  : in out A_Allegro_Bitmap;
                          mask : in out A_Allegro_Bitmap ) is
        ok : Boolean;
    begin
        this.loadState.Start( ok );
        if ok then
            this.bmp := bmp;
            this.mask := mask;
            bmp := null;
            mask := null;
            this.loadState.Notify_Complete;
        else
            Al_Destroy_Bitmap( bmp );
            Al_Destroy_Bitmap( mask );
            Assert( False, "Tile <" & this.Get_Name & "> set twice" );
        end if;
    end Set_Bitmap;

    ----------------------------------------------------------------------------

    not overriding
    procedure Set_Id( this : access Tile_Object; id : Natural ) is
    begin
        if id /= this.id then
            Assert( this.id = NULL_TILE_ID or this.id = ANON_TILE_ID, "Changing tile id is not allowed" );
            this.id := id;
        end if;
    end Set_Id;

    ----------------------------------------------------------------------------

    not overriding
    procedure Set_Name( this : access Tile_Object; name : String ) is
    begin
        this.name := To_Unbounded_String( name );
    end Set_Name;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Tile ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

    ----------------------------------------------------------------------------

    function A_Tile_Input( stream : access Root_Stream_Type'Class ) return A_Tile is
        tile : A_Tile := null;
    begin
        if Boolean'Input( stream ) then
            tile := Create_Tile;
            Tile_Object'Read( stream, tile.all );
        end if;
        return tile;
    end A_Tile_Input;

    ----------------------------------------------------------------------------

    procedure A_Tile_Output( stream : access Root_Stream_Type'Class; tile : A_Tile ) is
    begin
        Boolean'Output( stream, tile /= null );
        if tile /= null then
            Tile_Object'Write( stream, tile.all );
        end if;
    end A_Tile_Output;

    --==========================================================================

    function A_Tile_Id_Array_Input( stream : access Root_Stream_Type'Class ) return A_Tile_Id_Array is
        tia : A_Tile_Id_Array := null;
        len : Natural;
    begin
        if Boolean'Input( stream ) then
            len := Natural'Input( stream );
            tia := new Tile_Id_Array(1..len);
            for i in tia'Range loop
                tia(i) := Natural'Input( stream );
            end loop;
        end if;
        return tia;
    exception
        when others =>
            Delete( tia );
            raise;
    end A_Tile_Id_Array_Input;

    ----------------------------------------------------------------------------

    procedure A_Tile_Id_Array_Output( stream : access Root_Stream_Type'Class; tia : A_Tile_Id_Array ) is
    begin
        Boolean'Output( stream, tia /= null );
        if tia /= null then
            Natural'Output( stream, tia'Length );
            for i in tia'Range loop
                Natural'Output( stream, tia(i) );
            end loop;
        end if;
    end A_Tile_Id_Array_Output;

    ----------------------------------------------------------------------------

    function Copy( src : A_Tile_Id_Array ) return A_Tile_Id_Array
    is (if src /= null then new Tile_Id_Array'(src.all) else null);

end Tiles;
