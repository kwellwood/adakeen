--
-- Copyright (c) 2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Scribble.Ast.Script_Processors;    use Scribble.Ast.Script_Processors;

package body Scribble.Ast.Scripts is

    --==========================================================================
    -- Ast_Script_Element --

    not overriding
    procedure Construct( this : access Ast_Script_Element;
                         loc  : Token_Location;
                         name : not null A_Ast_Identifier ) is
    begin
        Ast_Node(this.all).Construct( loc );
        this.name := name;
    end Construct;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out Ast_Script_Element ) is
    begin
        Delete( A_Ast_Node(this.name) );
        Ast_Node(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Get_Name( this : not null access Ast_Script_Element'Class ) return A_Ast_Identifier is (this.name);

    --==========================================================================
    -- Ast_Required_Member --

    function Create_Required_Member( loc  : Token_Location;
                                     name : not null A_Ast_Identifier ) return A_Ast_Script_Element is
        this : constant A_Ast_Required_Member := new Ast_Required_Member;
    begin
        this.Construct( loc, name );
        return A_Ast_Script_Element(this);
    end Create_Required_Member;

    ----------------------------------------------------------------------------

    overriding
    procedure Process( this      : access Ast_Required_Member;
                       processor : access Scribble.Ast.Script_Processors.Ast_Script_Processor'Class ) is
    begin
        processor.Process_Required_Member( A_Ast_Required_Member(this) );
    end Process;

    --==========================================================================
    -- Ast_Script_Member --

    function Create_Script_Member( loc     : Token_Location;
                                   name    : not null A_Ast_Identifier;
                                   expr    : not null A_Ast_Expression;
                                   default : Boolean ) return A_Ast_Script_Element is
        this : constant A_Ast_Script_Member := new Ast_Script_Member;
    begin
        this.Construct( loc, name );
        this.expr := expr;
        this.default := default;
        return A_Ast_Script_Element(this);
    end Create_Script_Member;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Ast_Script_Member ) is
    begin
        Delete( A_Ast_Node(this.expr) );
        Ast_Script_Element(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Get_Expression( this : not null access Ast_Script_Member'Class ) return A_Ast_Expression is (this.expr);

    ----------------------------------------------------------------------------

    function Is_Default( this : not null access Ast_Script_Member'Class ) return Boolean is (this.default);

    ----------------------------------------------------------------------------

    overriding
    procedure Process( this      : access Ast_Script_Member;
                       processor : access Scribble.Ast.Script_Processors.Ast_Script_Processor'Class ) is
    begin
        processor.Process_Script_Member( A_Ast_Script_Member(this) );
    end Process;

    ----------------------------------------------------------------------------

    overriding
    procedure Prune( this : access Ast_Script_Member ) is
        pruned : A_Ast_Expression;
    begin
        pruned := this.expr.Prune;
        if pruned /= null then
            Delete( A_Ast_Node(this.expr) );
            this.expr := pruned;
        end if;
    end Prune;

    --==========================================================================
    -- Ast_Timer --

    function Create_Timer( loc    : Token_Location;
                           name   : not null A_Ast_Identifier;
                           args   : A_Ast_Expression;
                           period : not null A_Ast_Expression;
                           repeat : Boolean ) return A_Ast_Script_Element is
        this : constant A_Ast_Timer := new Ast_Timer;
    begin
        this.Construct( loc, name );
        this.args := args;
        this.period := period;
        this.repeat := repeat;
        return A_Ast_Script_Element(this);
    end Create_Timer;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Ast_Timer ) is
    begin
        Delete( A_Ast_Node(this.period) );
        Delete( A_Ast_Node(this.args) );
        Ast_Script_Element(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Get_Arguments( this : not null access Ast_Timer'Class ) return A_Ast_Expression is (this.args);

    ----------------------------------------------------------------------------

    function Get_Period( this : not null access Ast_Timer'Class ) return A_Ast_Expression is (this.period);

    ----------------------------------------------------------------------------

    function Is_Repeating( this : not null access Ast_Timer'Class ) return Boolean is (this.repeat);

    ----------------------------------------------------------------------------

    overriding
    procedure Process( this      : access Ast_Timer;
                       processor : access Scribble.Ast.Script_Processors.Ast_Script_Processor'Class ) is
    begin
        processor.Process_Timer( A_Ast_Timer(this) );
    end Process;

    ----------------------------------------------------------------------------

    overriding
    procedure Prune( this : access Ast_Timer ) is
        pruned : A_Ast_Expression;
    begin
        pruned := this.period.Prune;
        if pruned /= null then
            Delete( A_Ast_Node(this.period) );
            this.period := pruned;
        end if;

        if this.args /= null then
            pruned := this.args.Prune;
            if pruned /= null then
                Delete( A_Ast_Node(this.args) );
                this.args := pruned;
            end if;
        end if;
    end Prune;

    --==========================================================================
    -- Ast_Script --

    function Create_Script( loc : Token_Location ) return A_Ast_Script is
        this : constant A_Ast_Script := new Ast_Script;
    begin
        this.Construct( loc );
        return this;
    end Create_Script;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Ast_Script ) is
    begin
        for e of this.elements loop
            Delete( A_Ast_Node(e) );
        end loop;
        this.elements.Clear;
        Ast_Node(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Add_Element( this    : not null access Ast_Script'Class;
                           element : not null A_Ast_Script_Element ) is
    begin
        this.elements.Append( element );
    end Add_Element;

    ----------------------------------------------------------------------------

    function Elements_Count( this : not null access Ast_Script'Class ) return Natural is (Natural(this.elements.Length));

    ----------------------------------------------------------------------------

    function Get_Element( this : not null access Ast_Script'Class; index : Integer ) return A_Ast_Script_Element
        is (if index in 1..this.Elements_Count then this.elements.Element( index ) else null);

    ----------------------------------------------------------------------------

    procedure Prune( this : not null access Ast_Script'Class ) is
    begin
        for elem of this.elements loop
            elem.Prune;
        end loop;
    end Prune;

end Scribble.Ast.Scripts;
