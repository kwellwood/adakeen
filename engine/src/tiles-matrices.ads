--
-- Copyright (c) 2012-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Vectors;
with Maps;                              use Maps;
with Resources;                         use Resources;

package Tiles.Matrices is

    -- A tile matrix is a small layered, two-dimensional array of tile ids.
    -- Matrices are stored as mini Maps inside a tile library and used to draw
    -- repeating tile patterns or multi-tile objects for building a world map.

    -- maximum size of a matrix in either dimension
    MAX_MATRIX_SIZE : constant := 64;

    package Map_Vectors is new Ada.Containers.Vectors( Positive, A_Map, "=" );

    -- Deletes all maps in the vector and clears it.
    procedure Delete_All( maps : in out Map_Vectors.Vector );

    ----------------------------------------------------------------------------

    -- Populates vector 'matrices' using the matrices stored in the Scribble
    -- file 'res'. The format of the structure is expected to have already been
    -- validated. If an error occurs, no matrices will be returned.
    procedure Load_Matrices( res      : not null A_Resource_File;
                             matrices : in out Map_Vectors.Vector );

end Tiles.Matrices;
