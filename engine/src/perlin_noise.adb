--
-- Adapted from Ken Perlin's Improved Noise reference implementation
--
-- Copyright 2002 Ken Perlin
--
-- Retrieved 2018-05-05 from http://flafla2.github.io/2014/08/09/perlinnoise.html
--

with Interfaces.C;                      use Interfaces.C;
with Support;                           use Support;

package body Perlin_Noise is

    PERMUTATION : constant Int_Array(0..255) :=
    (
        151,160,137,91,90,15,
        131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,
        190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
        88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
        77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
        102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
        135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
        5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
        223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
        129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
        251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
        49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
        138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180
    );

    --==========================================================================

    function Create_Noise_Generator( repeat : Natural := 0 ) return A_Noise_Generator is
        this : constant A_Noise_Generator := new Noise_Generator;
    begin
        this.Construct( repeat );
        return this;
    end Create_Noise_Generator;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Noise_Generator; repeat : Natural ) is
    begin
        Object(this.all).Construct;
        this.repeat := repeat;
        for i in this.p'Range loop
            this.p(i) := PERMUTATION(i mod 256);
        end loop;
    end Construct;

    ----------------------------------------------------------------------------

    function Noise( this : Noise_Generator'Class; x, y, z : Long_Float ) return Long_Float is

        ------------------------------------------------------------------------

        -- Fade function as defined by Ken Perlin. This eases coordinate values
        -- so that they will "ease" towards integral values. This ends up smoothing
        -- the final output.
        -- 6t^5 - 15t^4 + 10t^3
        function Fade( t : Long_Float ) return Long_Float is (t * t * t * (t * (t * 6.0 - 15.0) + 10.0));

        ------------------------------------------------------------------------

        --public static double grad(int hash, double x, double y, double z)
        --{
        --    int h = hash & 15;                                    // Take the hashed value and take the first 4 bits of it (15 == 0b1111)
        --    double u = h < 8 /* 0b1000 */ ? x : y;                // If the most significant bit (MSB) of the hash is 0 then set u = x.  Otherwise y.
        --
        --    double v;                                             // In Ken Perlin's original implementation this was another conditional operator (?:).  I
        --                                                          // expanded it for readability.
        --
        --    if (h < 4 /* 0b0100 */)                               // If the first and second significant bits are 0 set v = y
        --        v = y;
        --    else if(h == 12 /* 0b1100 */ || h == 14 /* 0b1110*/)  // If the first and second significant bits are 1 set v = x
        --        v = x;
        --    else                                                  // If the first and second significant bits are not equal (0/1, 1/0) set v = z
        --        v = z;
        --
        --    return ((h&1) == 0 ? u : -u)+((h&2) == 0 ? v : -v);   // Use the last 2 bits to decide if u and v are positive or negative.  Then return their addition.
        --}

        function Grad( hash : unsigned; x, y, z : Long_Float ) return Long_Float is
        begin
            case hash and 16#F# is
                when 2#0000# => return  x + y;
                when 2#0001# => return -x + y;
                when 2#0010# => return  x - y;
                when 2#0011# => return -x - y;
                when 2#0100# => return  x + z;
                when 2#0101# => return -x + z;
                when 2#0110# => return  x - z;
                when 2#0111# => return -x - z;
                when 2#1000# => return  y + z;
                when 2#1001# => return -y + z;
                when 2#1010# => return  y - z;
                when 2#1011# => return -y - z;
                when 2#1100# => return  y + x;
                when 2#1101# => return -y + z;
                when 2#1110# => return  y - x;
                when 2#1111# => return -y - z;
                when others  => null;
            end case;
            return 0.0;      -- never happens
        end Grad;

        ------------------------------------------------------------------------

        function Inc( num : Integer ) return Integer is
            result : Integer := num + 1;
        begin
            if this.repeat > 0 then
                result := result mod this.repeat;
            end if;
            return result;
        end Inc;

        ------------------------------------------------------------------------

        function Lerp( a, b, x : Long_Float ) return Long_Float is (a + x * (b - a));

        ------------------------------------------------------------------------

        localX         : Long_Float := x;
        localY         : Long_Float := y;
        localZ         : Long_Float := z;
        xi, yi, zi     : Integer;
        xf, yf, zf     : Long_Float;
        u, v, w        : Long_Float;
        aaa, aba, aab,
        abb, baa, bba,
        bab, bbb       : unsigned;
        x1, y1, x2, y2 : Long_Float;
    begin
        -- If we have any repeat on, change the coordinates to their "local" repetitions

        if this.repeat > 0 then
            localX := Fmod( localX, Long_Float(this.repeat) );
            localY := Fmod( localY, Long_Float(this.repeat) );
            localZ := Fmod( localZ, Long_Float(this.repeat) );
        end if;

        xi := Integer(Long_Float'Floor(localX)) mod 256;       -- Calculate the "unit cube" that the point asked will be located in
        yi := Integer(Long_Float'Floor(localY)) mod 256;       -- The left bound is ( |_x_|,|_y_|,|_z_| ) and the right bound is that
        zi := Integer(Long_Float'Floor(localZ)) mod 256;       -- plus 1. Next we calculate the location (from 0.0 to 1.0) in that cube.

        xf := localX - Long_Float'Floor(localX);               -- We also fade the location to smooth the result.
        yf := localY - Long_Float'Floor(localY);
        zf := localZ - Long_Float'Floor(localZ);
        u := Fade( xf );
        v := Fade( yf );
        w := Fade( zf );

        aaa := unsigned(this.p(this.p(this.p(     xi  ) +      yi  ) +      zi  ));
        aba := unsigned(this.p(this.p(this.p(     xi  ) + Inc( yi )) +      zi  ));
        aab := unsigned(this.p(this.p(this.p(     xi  ) +      yi  ) + Inc( zi )));
        abb := unsigned(this.p(this.p(this.p(     xi  ) + Inc( yi )) + Inc( zi )));
        baa := unsigned(this.p(this.p(this.p(Inc( xi )) +      yi  ) +      zi  ));
        bba := unsigned(this.p(this.p(this.p(Inc( xi )) + Inc( yi )) +      zi  ));
        bab := unsigned(this.p(this.p(this.p(Inc( xi )) +      yi  ) + Inc( zi )));
        bbb := unsigned(this.p(this.p(this.p(Inc( xi )) + Inc( yi )) + Inc( zi )));

        x1 := Lerp( Grad( aaa, xf      , yf      , zf ),       -- The gradient function calculates the dot product between a pseudorandom
                    Grad( baa, xf - 1.0, yf      , zf ),       -- gradient vector and the vector from the input coordinate to the 8
                    u );                                       -- surrounding points in its unit cube.
        x2 := Lerp( Grad( aba, xf      , yf - 1.0, zf ),       -- This is all then lerped together as a sort of weighted average based on the faded (u,v,w)
                    Grad( bba, xf - 1.0, yf - 1.0, zf ),       -- values we made earlier.
                    u );
        y1 := Lerp( x1, x2, v );

        x1 := Lerp( Grad( aab, xf      , yf      , zf - 1.0 ),
                    Grad( bab, xf - 1.0, yf      , zf - 1.0 ),
                    u );
        x2 := Lerp( Grad( abb, xf      , yf - 1.0, zf - 1.0 ),
                    Grad( bbb, xf - 1.0, yf - 1.0, zf - 1.0 ),
                    u );
        y2 := Lerp( x1, x2, v );

        -- For convenience we bound it to 0.0 - 1.0 (theoretical min/max before is -1.0 -> 1.0)
        return (Lerp( y1, y2, w ) + 1.0) / 2.0;
    end Noise;

    ----------------------------------------------------------------------------

    function Octave_Noise( this        : Noise_Generator'Class;
                           x, y, z     : Long_Float;
                           octaves     : Positive;
                           persistence : Long_Float ) return Long_Float is
        total     : Long_Float := 0.0;
        frequency : Long_Float := 1.0;
        amplitude : Long_Float := 1.0;
        maxValue  : Long_Float := 0.0;         -- Used for normalizing result to 0.0 - 1.0
    begin
        for i in 0..octaves-1 loop
            total := total + this.Noise( x * frequency, y * frequency, z * frequency ) * amplitude;

            maxValue := maxValue + amplitude;

            amplitude := amplitude * persistence;
            frequency := frequency * 2.0;
        end loop;

        return total / maxValue;
    end Octave_Noise;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Noise_Generator ) is
    begin
        Delete( A_Object(this) );
    end Delete;

end Perlin_Noise;
