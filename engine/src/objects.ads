--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Streams;                       use Ada.Streams;

package Objects is

    pragma Preelaborate;

    type Base_Object is limited interface;
    type A_Base_Object is access all Base_Object'Class;
    pragma No_Strict_Aliasing( A_Base_Object );

    -- Constructs the object. A subclass should call its superclass' Construct
    -- before doing any work.
    not overriding
    procedure Construct( this : access Base_Object ) is null;

    -- Deletes the object's fields as part of object destruction. A subclass
    -- should call its superclass' Delete before doing any work.
    not overriding
    procedure Delete( this : in out Base_Object ) is null;

    -- Returns the name of the instance's class in lower case characters. This
    -- is defined to be the class' external tag which is not guaranteed to be
    -- unique across the entire application. If 'full' is True, the fully
    -- qualified class name which includes a package prefix will be returned.
    function Get_Class_Name( this : not null access Base_Object'Class;
                             full : Boolean := False ) return String;

    -- Returns a string representation of the object.
    not overriding
    function To_String( this : access Base_Object ) return String is abstract;

    -- Concatenates the string representation of the object, as returned by the
    -- To_String function.
    function "&"( left : access Base_Object'Class; right : String ) return String
    is ((if left = null then "null" else left.To_String) & right);

    -- Concatenates the string representation of the object, as returned by the
    -- To_String function.
    function "&"( left : String; right : access Base_Object'Class ) return String
    is (left & (if right = null then "null" else right.To_String));

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Signal Integration API

    function Signaller( this : access Base_Object ) return access Base_Object'Class is abstract;

    procedure Pop_Signaller( this : access Base_Object ) is abstract;

    procedure Push_Signaller( this   : access Base_Object;
                              sender : access Base_Object'Class ) is abstract;

    ----------------------------------------------------------------------------

    -- This is the superclass for all objects. The class provides copying,
    -- deletion, streaming and stringification procedures. All class should
    -- extend either the Object class or the Limited_Object class.
    type Object is abstract new Base_Object with private;
    type A_Object is access all Object'Class;
    pragma No_Strict_Aliasing( A_Object );

    -- Adjusts the object's fields as part of a Copy. If this class or an
    -- ancestor class doesn't support copying then COPY_NOT_ALLOWED will be
    -- raised. A subclass should call its superclass' Adjust before doing any
    -- work. When an object is copied, Adjust will be called on the new object
    -- after the memory copy and Construct will not be called on it.
    procedure Adjust( this : access Object ) is null;

    overriding
    procedure Construct( this : access Object ) is null;

    -- Returns a copy of 'src'. Not all object classes can be copied. If 'src'
    -- is not allowed to be copied then COPY_NOT_ALLOWED will be raised.
    function Copy( src : access Object'Class ) return A_Object;
    pragma Postcondition( Copy'Result /= src or else src = null );

    overriding
    procedure Delete( this : in out Object ) is null;

    -- Returns a string representation in the format: "<Class.Name@ADDRESS>"
    function To_String( this : access Object ) return String;

    -- Reads the object's representation from a stream. This should be
    -- overridden to provide streaming support for a subclass.
    not overriding
    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Object ) is null;

    -- Writes the object's representation to a stream. This should be overridden
    -- to provide streaming support for a subclass.
    not overriding
    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Object ) is null;

    -- Deletes the object.
    procedure Delete( this : in out A_Object );
    pragma Postcondition( this = null );

    -- Raised when attempting to copy an instance of a class that doesn't
    -- allow copying. This occurs if a class doesn't support copying but needs
    -- to be streamed. If a subclass does not allow copying and it is never
    -- streamed then it should extend the Limited_Object class instead.
    COPY_NOT_ALLOWED : exception;

    ----------------------------------------------------------------------------

    -- This is the superclass for all objects that can't be copied or streamed.
    -- It behaves the same as the Object class except for the restrictions
    -- imposed by its status as a limited type.
    type Limited_Object is abstract limited new Base_Object with private;
    type A_Limited_Object is access all Limited_Object'Class;
    pragma No_Strict_Aliasing( A_Limited_Object );

    -- Constructs the object. A subclass should call its superclass' Construct
    -- before doing any work.
    overriding
    procedure Construct( this : access Limited_Object ) is null;

    -- Deletes the object's fields as part of object destruction. A subclass
    -- should call its superclass' Delete before doing any work.
    overriding
    procedure Delete( this : in out Limited_Object ) is null;

    -- Returns a string representation in the format: "<Class.Name@ADDRESS>"
    function To_String( this : access Limited_Object ) return String;

    -- Deletes the object.
    procedure Delete( this : in out A_Limited_Object );
    pragma Postcondition( this = null );

private

    type Object_List;
    type A_Object_List is access all Object_List;

    type Object_List is
        record
            elem : A_Base_Object := null;
            next : A_Object_List := null;
        end record;

    ----------------------------------------------------------------------------

    type Object is abstract new Base_Object with
        record
            signallerCount : Integer := 0;
            signallers     : Object_List;
        end record;

    overriding
    function Signaller( this : access Object ) return access Base_Object'Class is (this.signallers.elem);

    procedure Pop_Signaller( this : access Object );

    procedure Push_Signaller( this   : access Object;
                              sender : access Base_Object'Class );

    for Object'Read use Object_Read;
    for Object'Write use Object_Write;

    ----------------------------------------------------------------------------

    type Limited_Object is abstract limited new Base_Object with
        record
            signallerCount : Integer := 0;
            signallers     : Object_List;
        end record;

    overriding
    function Signaller( this : access Limited_Object ) return access Base_Object'Class is (this.signallers.elem);

    procedure Pop_Signaller( this : access Limited_Object );

    procedure Push_Signaller( this   : access Limited_Object;
                              sender : access Base_Object'Class );

end Objects;
