--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Directories;                   use Ada.Directories;
with Ada.Streams;                       use Ada.Streams;
with Ada.Streams.Stream_IO;             use Ada.Streams.Stream_IO;
with Applications;                      use Applications;
with Assets.Archives;                   use Assets.Archives;
with Debugging;                         use Debugging;
with GNAT.OS_Lib;                       use GNAT.OS_Lib;
with Preferences;                       use Preferences;
with Resources.Allegro_Files;
with Support;                           use Support;
with Support.Paths;                     use Support.Paths;
with Values;                            use Values;
with Values.Strings;                    use Values.Strings;

package body Resources is

    function Find_On_Disk( filePath : String; group : String ) return String;

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Resource( path : String; data : not null A_SEA ) return A_Resource_File is
        this : constant A_Resource_File := new Resource_File;
    begin
        this.Construct( path, data );
        return this;
    end Create_Resource;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Resource_File; path : String; data : not null A_SEA ) is
    begin
        Limited_Object(this.all).Construct;
        this.path := To_Unbounded_String( path );
        this.data := data;
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Resource_File ) is
    begin
        Delete( this.data );
        Limited_Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Create_Allegro_File( this : not null access Resource_File'Class ) return A_Allegro_File
    is (Allegro_Files.Open_Memfile( this.data, consume => False ));

    ----------------------------------------------------------------------------

    function Create_Stream( this : not null access Resource_File'Class ) return A_Buffer_Stream is (Stream( this.data ));

    ----------------------------------------------------------------------------

    function Get_Address( this : not null access Resource_File'Class ) return Address
    is (if this.data'Length > 0 then this.data(this.data'First)'Address else Null_Address);

    ----------------------------------------------------------------------------

    function Get_Filename( this : not null access Resource_File'Class ) return String is (Get_Filename( this.Get_Path ));

    ----------------------------------------------------------------------------

    function Get_Path( this : not null access Resource_File'Class ) return String is (To_String( this.path ));

    ----------------------------------------------------------------------------

    function Size( this : not null access Resource_File'Class ) return Unsigned_32 is (Unsigned_32(this.data'Length));

    ----------------------------------------------------------------------------

    overriding
    function To_String( this : access Resource_File ) return String is (this.Get_Path);

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Resource_File ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

    --==========================================================================

    function Find_On_Disk( filePath : String; group : String ) return String is

        ------------------------------------------------------------------------

        function File_Exists( path : String ) return Boolean is
            archive : A_Archive;
            exists  : Boolean := False;
        begin
            if Is_Regular_File( Get_Outer_Path( path ) ) then
                if Is_Path_In_Archive( path ) then
                    -- if the path is in an archive, open the archive and look
                    --
                    -- NOTE: Get_Path_In_Archive( path ) cannot be another
                    -- archive path because there is no mechanism for opening an
                    -- archive inside another archive. this limits the archive
                    -- depth of resources to one level.
                    --
                    -- FUTURE: create an Archive class outside the Asset class
                    -- tree and allow them to load resources files and to be
                    -- loaded from resource files, supporting up to N levels of
                    -- archive nesting. the existing Archive_Asset would wrap it.
                    -- clever use would be needed to avoid opening and closing
                    -- files to reach a nested file.
                    archive := Load_Archive( Get_Outer_Path( path ), "", keepLoaded => True );
                    exists := archive.Is_Loaded and then archive.File_Exists( Get_Path_In_Archive( path ) );
                    Delete( archive );
                else
                    -- for regular files, Is_Regular_File() was sufficient to
                    -- check if the file exists.
                    exists := True;
                end if;
            end if;
            return exists;
        end File_Exists;

        ------------------------------------------------------------------------

        mediaDir : constant String := Get_Pref( "application", "media" );
        userDir  : constant String := (if Get_Application /= null then Get_Application.Get_User_Directory else "");
        archPath : Unbounded_String;
    begin
        -- 0. Look at [filePath] (if it is in an archive)
        if Is_Path_In_Archive( filePath ) then
            -- search for the outer archive with a recursive call to Find_On_Disk()
            archPath := To_Unbounded_String( Find_On_Disk( Get_Archive_File( filePath ), group ) );
            if Length( archPath ) > 0 then
                -- the outer archive file was found; verify the file inside it exists
                if File_Exists( To_String( archPath ) & "<" & Get_Path_In_Archive( filePath ) ) then
                    return      To_String( archPath ) & "<" & Get_Path_In_Archive( filePath );
                end if;
            end if;
            return "";
        end if;

        -- 1. Look at [filePath] (if it is absolute)
        if Is_Absolute_Path( filePath ) then
            if File_Exists( filePath ) then
                return filePath;
            else
                -- don't try to resolve an absolute path that doesn't exist, to
                -- a different path.
                return "";

                -- the absolute path doesn't exist; search for just the filename instead
                --return Find_On_Disk( Get_Filename( filePath ), group );
            end if;
        end if;

        -- 2. Look at ./[filePath]
        if File_Exists( filePath ) then
            return      filePath;
        end if;

        -- 3. Look at ./[group]/[filePath]
        if group'Length > 0 then
            if File_Exists( group & Slash & Get_Filename( filePath ) ) then
                return      group & Slash & Get_Filename( filePath );
            end if;
        end if;

        -- 4. Look at ./[group].zip<[filePath]
        if group'Length > 0 then
            if File_Exists( Add_Extension( group, ARCHIVE_EXTENSION ) & "<" & filePath ) then
                return      Add_Extension( group, ARCHIVE_EXTENSION ) & "<" & filePath;
            end if;
        end if;

        -- 5. Look at [userDir]/[filePath]
        if userDir'Length > 0 then
            if File_Exists( userDir & Get_Filename( filePath ) ) then
                return      userDir & Get_Filename( filePath );
            end if;
        end if;

        -- 6. Look at [mediaDir]/[filePath]
        if mediaDir'Length > 0 then
            if File_Exists( mediaDir & Get_Filename( filePath ) ) then
                return      mediaDir & Get_Filename( filePath );
            end if;
        end if;

        -- 7. Look at [mediaDir]/[group]/[filePath]
        if mediaDir'Length > 0 and group'Length > 0 then
            if File_Exists( mediaDir & group & Slash & Get_Filename( filePath ) ) then
                return      mediaDir & group & Slash & Get_Filename( filePath );
            end if;
        end if;

        -- 8. Look at [mediaDir]/[group].zip<[filePath]
        if mediaDir'Length > 0 and group'Length > 0 then
            if File_Exists( Add_Extension( mediaDir & group, ARCHIVE_EXTENSION ) & "<" & filePath ) then
                return      Add_Extension( mediaDir & group, ARCHIVE_EXTENSION ) & "<" & filePath;
            end if;
        end if;

        -- Resource not found
        return "";
    end Find_On_Disk;

    ----------------------------------------------------------------------------

    function Load_Resource( filePath : String; group : String := "" ) return A_Resource_File is

        ------------------------------------------------------------------------

        function Read_From_Archive( path : String ) return A_SEA is
            archive : A_Archive;
            res     : A_Resource_File;
            buffer  : A_SEA := null;
        begin
            archive := Load_Archive( Get_Outer_Path( path ), "", keepLoaded => True );
            if archive.Is_Loaded then
                res := archive.Load_File( Get_Path_In_Archive( path ) );
                if res /= null then
                    declare
                        resData : Stream_Element_Array(1..Stream_Element_Offset(res.Size));
                        for resData'Address use res.Get_Address;
                    begin
                        buffer := new Stream_Element_Array(1..Stream_Element_Offset(res.Size));
                        buffer.all := resData;
                        pragma Debug( Dbg( "Resolved " & group & ':' & filePath & " -> " & path,
                                           D_RES, Info ) );
                    end;
                    Delete( res );
                end if;
            end if;
            Delete( archive );
            return buffer;
        end Read_From_Archive;

        ------------------------------------------------------------------------

        function Read_From_File( path : String ) return A_SEA is
            len    : Long_Integer := 0;
            ft     : File_Type;
            buffer : A_SEA := null;
        begin
            len := File_Length( path );
            if len >= 0 then
                Open( ft, In_File, path );
                if Is_Open( ft ) then
                    pragma Debug( Dbg( "Resolved " & group & ':' & filePath & " -> " & path, D_RES, Info ) );
                    buffer := new Stream_Element_Array(0..Stream_Element_Offset(len-1));
                    Stream_Element_Array'Read( Stream( ft ), buffer.all );
                    Close( ft );
                end if;
            end if;
            return buffer;
        exception
            when others =>
                Delete( buffer );
                if Is_Open( ft ) then
                    Close( ft );
                end if;
                return null;
        end Read_From_File;

        ------------------------------------------------------------------------

        diskPath : Unbounded_String;
        data     : A_SEA := null;
        res      : A_Resource_File := null;
    begin
        -- locate the file
        diskPath := To_Unbounded_String( Find_On_Disk( filePath, group ) );

        -- does the file exist?
        if Length( diskPath ) > 0 then
            if Is_Path_In_Archive( To_String( diskPath ) ) then
                -- file is inside archive file 'diskPath'
                data := Read_From_Archive( To_String( diskPath ) );
            else
                -- file is directly on disk, not in an archive
                data := Read_From_File( To_String( diskPath ) );
            end if;
            if data /= null then
                res := Create_Resource( To_String( diskPath ), data );
            end if;
        end if;

        return res;
    end Load_Resource;

    ----------------------------------------------------------------------------

    function Locate_Resource( filePath : String; group : String ) return String renames Find_On_Disk;

    ----------------------------------------------------------------------------

    function Search_Resources( wildcard : String; group : String ) return List_Value is
        result : constant List_Value := Create_List.Lst;

        ------------------------------------------------------------------------

        procedure Add_From_Disk( dirEntry : Directory_Entry_Type ) is
            filename : constant String := Get_Filename( Full_Name( dirEntry ) );
            val      : constant Value := Create( filename );
        begin
            if not result.Contains( val ) then
                result.Append( val );
            end if;
        end Add_From_Disk;

        ------------------------------------------------------------------------

        procedure Search_Archive( filePath : String ) is
            archive : A_Archive;

            --------------------------------------------------------------------

            procedure Add_From_Archive( entryName : String ) is
                filename : constant String := Get_Filename( entryName );
                val      : constant Value := Create( filename );
            begin
                if not result.Contains( val ) then
                    result.Append( val );
                end if;
            end Add_From_Archive;

            --------------------------------------------------------------------

        begin
            if Is_Regular_File( filePath ) then
                archive := Load_Archive( filePath, "", keepLoaded => True );
                if archive.Is_Loaded then
                    archive.Search( wildcard, Add_From_Archive'Access );
                end if;
                Delete( archive );
            end if;
        end Search_Archive;

        ------------------------------------------------------------------------

        filter   : constant Filter_Type := (Ordinary_File => True, others => False);
        mediaDir : constant String := Get_Pref( "application", "media" );
        userDir  : constant String := (if Get_Application /= null then Get_Application.Get_User_Directory else "");
    begin
        -- 1. Search for ./[wildcard]
        Search( ".", wildcard, filter, Add_From_Disk'Access );

        if group'Length > 0 then
            -- 2. Search for ./[group]/[wildcard]
            if Is_Directory( group & Slash ) then
                Search( group & Slash, wildcard, filter, Add_From_Disk'Access );
            end if;

            -- 3. Search for ./[group].zip<[wildcard]
            Search_Archive( Add_Extension( group, ARCHIVE_EXTENSION ) );
        end if;

        -- 4. Search for [userDir]/[wildcard]
        if Is_Directory( userDir ) then
            Search( userDir, wildcard, filter, Add_From_Disk'Access );
        end if;

        if mediaDir'Length > 0 then
            -- 5. Search for [mediaDir]/[wildcard]
            if Is_Directory( mediaDir ) then
                Search( mediaDir, wildcard, filter, Add_From_Disk'Access );
            end if;

            if group'Length > 0 then
                -- 6. Search for [mediaDir]/[group]/[wildcard]
                if Is_Directory( mediaDir & group & Slash ) then
                    Search( mediaDir & group & Slash, wildcard, filter, Add_From_Disk'Access );
                end if;

                -- 7. Search for [mediaDir]/[group].zip<[filePath]
                Search_Archive( Add_Extension( mediaDir & group, ARCHIVE_EXTENSION ) );
            end if;
        end if;

        return result;
    end Search_Resources;

    ----------------------------------------------------------------------------

    function Resource_Exists( filePath : String; group : String ) return Boolean
    is (Find_On_Disk( filePath, group )'Length > 0);

    ----------------------------------------------------------------------------

    function Writable_Path( filePath : String; group : String ) return String is

        ------------------------------------------------------------------------

        function Is_Dir_Writable( dir : String ) return Boolean is
            fd : File_Descriptor;
            ok : Boolean := False;
        begin
            Delete_File( dir & "temp.tmp", ok );
            fd := Create_New_File( dir & "temp.tmp", Text );
            if fd /= Invalid_FD then
                Close( fd );
                Delete_File( dir & "temp.tmp", ok );
                return True;
            end if;
            return False;
        end Is_Dir_Writable;

        ------------------------------------------------------------------------

        found : Unbounded_String;
    begin
        -- absolute file: always write it where requested
        if not Is_Path_In_Archive( filePath ) and then Is_Absolute_Path( filePath ) then
            return filePath;
        end if;

        found := To_Unbounded_String( Find_On_Disk( filePath, group ) );
        if Length( found ) > 0 then
            if not Is_Path_In_Archive( To_String( found ) ) then
                -- found the existing naked resource file
                return Normalize_Pathname( To_String( found ) );
            end if;

            -- found it but it's in an archive. check if the archive's directory
            -- is writable, then return 'filePath' relative to the archive's
            -- directory. the caller is responsible for creating the subdirectory
            -- if it needs one.
            if Is_Dir_Writable( Get_Directory( Get_Archive_File( To_String( found ) ) ) ) then
                -- the directory containing the file's archive is writable, so
                -- return the file's path relative to its container archive but
                -- rebased to the archive's directory.
                return Get_Directory( Get_Archive_File( To_String( found ) ) ) & Get_Inner_Path( filePath );
            end if;
        end if;

        -- the target couldn't be found, either on disk or inside an archive. at
        -- this point, the path could be a normal relative file path (not
        -- absolute), or a path to a file inside an archive but either the
        -- archive or the file inside it doesn't exist (because Find_On_Disk()
        -- couldn't find it.)
        --
        -- if the target file is an archive path, then the path to its archive
        -- file may be relative or absolute- we don't know.

        -- try writing the file relative to the working directory. (for installed
        -- applications, the working directory is probably read-only.) for a
        -- path into an archive, use the inner (the file's path relative to its
        -- containing archive). for normal files, this will just be the whole
        -- relative file path.
        if Get_Application = null or else Is_Dir_Writable( Get_Directory( Normalize_Pathname( Get_Inner_Path( filePath ) ) ) ) then
            return Normalize_Pathname( Get_Inner_Path( filePath ) );
        end if;

        -- fallback to the working directory if it's writable or the application
        -- doesn't have a user directory. 'filePath' is relative.
        if Get_Application = null or else Is_Dir_Writable( "./" ) then
            return Normalize_Pathname( Get_Inner_Path( filePath ) );
        end if;

        -- user directory is available and always writable
        return Normalize_Pathname( Get_Application.Get_User_Directory & Get_Inner_Path( filePath ) );
    end Writable_Path;

end Resources;

