--
-- Copyright (c) 2012-2015 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Clipping is

    type Solidity_Array is array (Clip_Type) of Integer;

    -- mapping of clip_type to solidity values for the "or" operator.
    solidity : constant Solidity_Array :=
        Solidity_Array'(
            Passive => 0,
            OneWay  => 50,

            Slope_22_Up_Floor_Thin     => 70,
            Slope_22_Up_Ceiling_Thin   => 70,
            Slope_22_Down_Floor_Thin   => 70,
            Slope_22_Down_Ceiling_Thin => 70,

            Slope_45_Up_Floor     => 75,
            Slope_45_Up_Ceiling   => 75,
            Slope_45_Down_Floor   => 75,
            Slope_45_Down_Ceiling => 75,

            Slope_22_Up_Floor_Wide    => 80,
            Slope_22_Up_Ceiling_Wide  => 80,
            Slope_22_Down_Floor_Wide  => 80,
            Slope_22_Down_Ceiling_Wide => 80,

            Wall => 100
        );

    ----------------------------------------------------------------------------

    function Constrain( val, min, max : Float ) return Float
    is (if val < min then min elsif val > max then max else val);

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Dominant( a, b : Clip_Type ) return Clip_Type is (if solidity(b) > solidity(a) then b else a);

    ----------------------------------------------------------------------------

    procedure To_Clip_Type( str   : String;
                            clip  : out Clip_Type;
                            valid : out Boolean ) is
    begin
        clip := Clip_Type'Value( str );
        if clip'Valid then
            valid := True;
        else
            clip := Clip_Type'First;
            valid := False;
        end if;
    exception
        when others =>
            clip := Clip_Type'First;
            valid := False;
    end To_Clip_Type;

    ----------------------------------------------------------------------------

    function To_String( tile : Clip_Type ) return String is
    ((case tile is
          when Passive => "Passive",
          when OneWay  => "Floor",
          when Wall    => "Solid",
          when others  => "Slope"));

    ----------------------------------------------------------------------------

    function Is_Slope_A( tile : Clip_Type ) return Boolean is
        right : constant Integer := Slope_Right_Y( tile, 16 );
    begin
        return right >= 0 and then Slope_Left_Y( tile, 16 ) > right;
    end Is_Slope_A;

    ----------------------------------------------------------------------------

    function Is_Slope_B( tile : Clip_Type ) return Boolean is
        left : constant Integer := Slope_Left_Y( tile, 16 );
    begin
        return left >= 0 and then Slope_Right_Y( tile, 16 ) > left;
    end Is_Slope_B;

    ----------------------------------------------------------------------------

    function Is_Slope_C( tile : Clip_Type ) return Boolean is
        left : constant Integer := Slope_Left_Y( tile, 16 );
    begin
        return left <= 0 and then left > Slope_Right_Y( tile, 16 );
    end Is_Slope_C;

    ----------------------------------------------------------------------------

    function Is_Slope_D( tile : Clip_Type ) return Boolean is
        right : constant Integer := Slope_Right_Y( tile, 16 );
    begin
        return right <= 0 and then right > Slope_Left_Y( tile, 16 );
    end Is_Slope_D;

    ----------------------------------------------------------------------------

    function Solid_Bottom( tile : Clip_Type ) return Boolean
    is (
        (case tile is
            when Wall                     |
                 Slope_45_Up_Floor        |
                 Slope_45_Down_Floor      |
                 Slope_22_Up_Floor_Thin   |
                 Slope_22_Up_Floor_Wide   |
                 Slope_22_Down_Floor_Wide |
                 Slope_22_Down_Floor_Thin => True,
            when others => False
        )
    );

    ----------------------------------------------------------------------------

    function Solid_Left( tile : Clip_Type ) return Boolean
    is (
        (case tile is
            when Wall                       |
                 Slope_45_Up_Ceiling        |
                 Slope_45_Down_Floor        |
                 Slope_22_Up_Floor_Wide     |
                 Slope_22_Up_Ceiling_Wide   |
                 Slope_22_Up_Ceiling_Thin   |
                 Slope_22_Down_Floor_Wide   |
                 Slope_22_Down_Floor_Thin   |
                 Slope_22_Down_Ceiling_Wide => True,
            when others => False
        )
    );

    ----------------------------------------------------------------------------

    function Solid_Right( tile : Clip_Type ) return Boolean
    is (
        (case tile is
            when Wall                       |
                 Slope_45_Up_Floor          |
                 Slope_45_Down_Ceiling      |
                 Slope_22_Up_Floor_Thin     |
                 Slope_22_Up_Floor_Wide     |
                 Slope_22_Up_Ceiling_Wide   |
                 Slope_22_Down_Floor_Wide   |
                 Slope_22_Down_Ceiling_Thin |
                 Slope_22_Down_Ceiling_Wide => True,
            when others => False
        )
    );

    ----------------------------------------------------------------------------

    function Solid_Top( tile : Clip_Type ) return Boolean
    is (
        (case tile is
            when OneWay                     |
                 Wall                       |
                 Slope_45_Up_Ceiling        |
                 Slope_45_Down_Ceiling      |
                 Slope_22_Up_Ceiling_Wide   |
                 Slope_22_Up_Ceiling_Thin   |
                 Slope_22_Down_Ceiling_Thin |
                 Slope_22_Down_Ceiling_Wide => True,
            when others => False
        )
    );

    ----------------------------------------------------------------------------

    function Slope_Left_Y( tile : Clip_Type; tileWidth : Positive ) return Integer
    is (
        (case tile is
            when Slope_45_Up_Floor          =>  tileWidth,
            when Slope_45_Up_Ceiling        => -tileWidth,
            when Slope_45_Down_Floor        =>  0,
            when Slope_45_Down_Ceiling      => -0,

            when Slope_22_Up_Floor_Thin     =>  tileWidth,
            when Slope_22_Up_Floor_Wide     =>  (tileWidth / 2 - 1),
            when Slope_22_Up_Ceiling_Wide   => -tileWidth,
            when Slope_22_Up_Ceiling_Thin   => -(tileWidth / 2 - 1),

            when Slope_22_Down_Floor_Wide   =>  0,
            when Slope_22_Down_Floor_Thin   =>  (tileWidth / 2),
            when Slope_22_Down_Ceiling_Thin => -0,
            when Slope_22_Down_Ceiling_Wide => -(tileWidth / 2),

            when others => 0
        )
    );

    ----------------------------------------------------------------------------

    function Slope_Right_Y( tile : Clip_Type; tileWidth : Positive ) return Integer
    is (
        (case tile is
            when Slope_45_Up_Floor          =>  0,
            when Slope_45_Up_Ceiling        =>  0,
            when Slope_45_Down_Floor        =>  tileWidth,
            when Slope_45_Down_Ceiling      => -tileWidth,

            when Slope_22_Up_Floor_Thin     =>  (tileWidth / 2),
            when Slope_22_Up_Floor_Wide     =>  0,
            when Slope_22_Up_Ceiling_Wide   => -(tileWidth / 2),
            when Slope_22_Up_Ceiling_Thin   => -0,

            when Slope_22_Down_Floor_Wide   =>  (tileWidth / 2 - 1),
            when Slope_22_Down_Floor_Thin   =>  tileWidth,
            when Slope_22_Down_Ceiling_Thin => -(tileWidth / 2 - 1),
            when Slope_22_Down_Ceiling_Wide => -tileWidth,

            when others => 0
        )
    );

    ----------------------------------------------------------------------------

    function Slope_X( tile      : Clip_Type;
                      y         : Float;
                      col, row  : Integer;
                      tileWidth : Positive ) return Float is
        leftY  : constant Float := Float(Integer'Min( abs Slope_Left_Y( tile, tileWidth ), tileWidth - 1 ));
        rightY : constant Float := Float(Integer'Min( abs Slope_Right_Y( tile, tileWidth ), tileWidth - 1 ));

        -- (y := mx + b)  ==>  (x := (y - b) / m)
        b : constant Float := Float(row * tileWidth) + leftY;
        m : constant Float := (rightY - leftY) / Float(tileWidth);
    begin
        -- Calculate X using y := mx + b
        --
        -- The slope's X is calculated using the formula x:=(y-b)/m, derived
        -- from the slope of a line: y:=mx+b, where m is the slope (dY / dX) and
        -- b is the vertical offset (e.g. the value of y, where x=0.)
        --
        -- Because the edge of an entity in the positive direction of each axis
        -- (right, down) is always at least one pixel less than the tile
        -- boundary of a wall, the slope needs to be adjusted so that
        -- 0 <= X <= tileWidth over 0 <= Y <= tileWidth-1.
        --
        -- This is done simply by constraining the leftY and rightY properties
        -- of the tile to be <= tileWidth-1. When calculating m and b, we use
        -- these constrained Y values, which causes the slope of our line to
        -- slightly decrease, and b to adjust if necessary (b = leftY). Thus,
        -- at Y = 0, 0 <= X <= tileWidth and at Y = (tileWidth-1),
        -- 0 <= x <= tileWidth.
        --
        -- Why are Slope_X and Slope_Y calculated differently?
        --
        -- Slope_X is calculated differently than Slope_Y value (which uses a
        -- weighted average of leftY and rightY) because the slope's delta Y
        -- values are constrained to tileWidth-1. Because the slope's two data
        -- points (leftY, rightY) are expressed in the Y axis, and our
        -- constraint (y <= tileWidth-1) is also in the Y axis, calculating X is
        -- as simple as constraining leftY and rightY.
        --
        -- However, when calculating Slope_Y, the constraint (x <= tileWidth-1)
        -- is in the X axis but deltaX is fixed, because the width of a slope is
        -- always exactly one tile, even though the height of it (delta Y) can
        -- vary between 1 pixel and tileWidth pixels:
        --   _______                _______
        --  |######/|              |####/  |       This is enforced by the
        --  |####/  | <- Valid     |###/   |       restrictions:
        --  |##/    |   Invalid -> |##/    |       0 <= leftY  <= tileWidth and
        --  |/______|              |#/_____|       0 <= rightY <= tileWidth
        --  ^       ^                ^  ^
        --  tileWidth             < tileWidth
        --
        -- Translating the X axis constraint of (0 <= X <= tileWidth-1) into
        -- the Y axis of leftY and rightY, using the y := mx + b form, requires
        -- deltaX to be multiplied by a special scalar, K, in the 'm' component.
        --
        -- The deriviation of this scalar is not exactly straight forward, and
        -- the weighted average equation currently used in the Slope_Y function
        -- is much clearer, in my opinion, as to what's actually happening.
        return Float(col * tileWidth) +
               Constrain( (y - b) / m, 0.0, Float(tileWidth) );
    end Slope_X;

    ----------------------------------------------------------------------------

    function Slope_Y( tile      : Clip_Type;
                      x         : Float;
                      col, row  : Integer;
                      tileWidth : Positive ) return Float is
        leftY  : constant Float := Float(abs Slope_Left_Y( tile, tileWidth ));
        rightY : constant Float := Float(abs Slope_Right_Y( tile, tileWidth ));
        localX : constant Float := x - Float(col * tileWidth);

        -- rx: relative location of x within the column (0.0=left, 1.0=right)
        rx : constant Float := localX / Float(tileWidth);
        adjustedRightY : Float;
    begin
        -- The slope's Y is calculated as a weighted average of leftY and
        -- rightY, which are properties of the slope tile.
        --
        -- However, a problem arises when the slope neighbors a wall in the
        -- positive direction (right or down), because an entity's positive
        -- edge is always at least one pixel shy of the tile boundary.
        -- (This is also true when calcuating Slope_X. See Slope_X for details.)
        --
        -- When a slope is traversed in the X axis up to one pixel shy of its
        -- right edge (X <= tileWidth-1), its rightY can never be full reached;
        -- only tileWidth/(tileWidth-1) percent of the rightY will be reached
        -- vertically before its right edge hits the wall at tileWidth-1, or one
        -- pixel shy of the end of the slope:
        --   _____
        --   \####|  The dot in the picture to the left illustrates the closest
        --   __\.#|  that the right edge can come to the wall on the right. The
        --   ###|\|  Y value at that point on the linear slope is shy of the
        --   ###| |  maximum rightY value, but since it can't go further right,
        --   ^    |  the entity can't be moved down any further down by the
        -- entity    slope, either.
        -- moving ->
        --
        -- The solution adopted here is to transform rightY (as 'adjustedRightY'),
        -- such that Y = originalRightY at X = tileWidth-1:
        --
        -- adjustedRightY := leftY + (rightY - leftY) * tileWidth / (tileWidth - 1);
        --
        -- We're effectively extending rightY (positively or negatively)
        -- proportionately to the change in the slope 'm' when m's deltaX is
        -- scaled from 'tileWidth' to 'tileWidth-1').
        --
        -- This changes the slope m by a slight amount without changing B,
        -- which causes Y != rightY at X > tileWidth - 1. To prevent this
        -- exursion of the slope beyond rightY in the rightmost pixel of the
        -- slope, the slope's calculated Y value is constrained between leftY
        -- and rightY where X > tileWidth-1. This enforces that Y stays between
        -- leftY and rightY (inclusive) over the full range of 0 <= X <= tileWidth.
        --
        -- Because of this constraint on Y where X > tileWidth-1, the clipping
        -- shape of the slope tile now resembles the diagrams below, where the
        -- triangular point on the right edge has been flattened in the Y axis
        -- over the range tileWidth-1 < X <= tileWidth:
        --
        --   \#######|                   In this figure, you can see that the
        --     \#####|                   slope reaches its maximum Y at
        --       \###|                   X=(tileWidth-1) and the slope flattens
        --        *'''  < y=rightY       out where the slope's Y constraint
        --        ^                      takes effect.
        --        x=tileWidth-1
        --                               ...
        --        ___
        --       /*##|  < y=rightY       This figure shows an opposite angled
        --     /#####|                   slope. In both diagrams, the * marks
        --   /#######|                   the point (x=tileWidth-1, y=rightY).
        --

        adjustedRightY := leftY + (rightY - leftY) * Float(tileWidth) / Float(tileWidth - 1);

        return Float(row * tileWidth) +
               Constrain( val => ((1.0 - rx) * leftY) + (rx * adjustedRightY),
                          min => Float'Min( leftY, rightY ),
                          max => Float'Max( leftY, rightY ) );

        -- This 'adjustedRightY' modfication to rightY in the weighted average
        -- calculation above can be translated into a form of y := mx + b. The
        -- whole weighted average calcuation simplifies down to y := mx + b form
        -- with a special scalar, K, that scales the deltaX of tileWidth-1 in
        -- the 'm' component of the linear slope equation:
        --
        -- b := leftY
        -- K := 1 / (tileWidth^2)
        -- m := deltaY / scaledDeltaX
        -- ...  m := (rightY - leftY) / scaledDeltaX
        -- ...  m := (rightY - leftY) / (deltaX * K)
        -- ...  m := (rightY - leftY) / ((tileWidth-1) * K)
        -- ...  m := (rightY - leftY) / ((tileWidth-1) * (1 / tileWidth^2))
        -- y := m * x + b
        --
        -- The special scalar of (1 / tileWidth^2) applied to the shortened
        -- deltaX (tileWidth-1) is what transforms the X axis constraint of
        -- 0 <= X <= tileWidth-1 into the Y axis, such that Y is in the full
        -- range leftY..rightY over the shortened deltaX range.
        --
        -- The code below implements the linear slope algorithm described above.
        -- It is functionally equivalent to the weighted average method.
        -- -----
        --begin
        --    leftY  : constant Float := Float(abs Slope_Left_Y( tile, tileWidth ));
        --    rightY : constant Float := Float(abs Slope_Right_Y( tile, tileWidth ));
        --    localX : constant Float := x - Float(col * tileWidth);
        --    b : constant Float := leftY;
        --    m : constant Float := Float(tileWidth * tileWidth) /
        --                          (Float(tileWidth - 1) * (rightY - leftY));
        --begin
        --    return Float(row * tileWidth) +
        --           Constrain( val => m * localX + b,
        --                      min => Float'Min( leftY, rightY ),
        --                      max => Float'Max( leftY, rightY ) );
        --end;
    end Slope_Y;

end Clipping;
