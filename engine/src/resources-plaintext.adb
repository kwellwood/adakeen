--
-- Copyright (c) 2016-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;

package body Resources.Plaintext is

    function Read_Plaintext( resource : not null A_Resource_File ) return Unbounded_String is
        text : Unbounded_String;
        data : String(1..Integer(resource.Size));     -- backed by 'resource'
        for data'Address use resource.Get_Address;
    begin
        text := To_Unbounded_String( data );
        return text;
    end Read_Plaintext;

    ----------------------------------------------------------------------------

    function Load_Plaintext( filePath : String; group : String; text : out Unbounded_String ) return Boolean is
        resource : A_Resource_File := null;
    begin
        text := Null_Unbounded_String;
        resource := Load_Resource( filePath, group );
        if resource /= null then
            text := Read_Plaintext( resource );
            Delete( resource );
            return True;
        end if;
        return False;
    end Load_Plaintext;

    ----------------------------------------------------------------------------

    function Load_Plaintext( filePath : String; group : String ) return Unbounded_String is
        text : Unbounded_String;
        ok   : Boolean := False;
        pragma Warnings( Off, ok );
    begin
        ok := Load_Plaintext( filePath, group, text );
        return text;
    end Load_Plaintext;

end Resources.Plaintext;
