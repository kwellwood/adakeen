--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Calendar;                      use Ada.Calendar;
with Ada.Calendar.Formatting;
with Ada.Directories;
with Ada.Exceptions;                    use Ada.Exceptions;
with Ada.Real_Time;                     use Ada.Real_Time;
with Ada.Streams.Stream_IO;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Ada.Text_IO;
with Applications;                      use Applications;
with GNAT.OS_Lib;
with Resources;                         use Resources;
with Resources.Images;                  use Resources.Images;
with Resources.Scribble;                use Resources.Scribble;
with Scribble;                          use Scribble;
with Scribble.Compilers;                use Scribble.Compilers;
with Scribble.IO;                       use Scribble.IO;
with Support;                           use Support;
with Support.Paths;                     use Support.Paths;
with Tiles.Construction;                use Tiles.Construction;
with Values.Casting;                    use Values.Casting;
with Values.Construction;               use Values.Construction;
with Values.Errors;                     use Values.Errors;
with Values.Lists;                      use Values.Lists;
with Values.Strings;                    use Values.Strings;
with Version;
with Zip;                               use Zip;
with Zip.Compress;
with Zip.Create;                        use Zip.Create;
with Zip_Streams;                       use Zip_Streams;

package body Assets.Libraries.Compilers is

    function Trim_Whitespace_And_Comments( str : String ) return Unbounded_String is
        last : Integer := Index( str, "//" );
    begin
        if last < str'First then
            last := str'Last;
        else
            last := last - 1;
        end if;
        if last >= str'First and then str(last) = ASCII.CR then
            -- remove Windows CR characters on *nix systems
            last := last - 1;
        end if;
        return To_Unbounded_String( Trim( str(str'First..last), Both ) );
    end Trim_Whitespace_And_Comments;

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Compiler( output : A_Output_Proc ) return A_Compiler is
        this : constant A_Compiler := new Compiler;
    begin
        this.Construct( output );
        return this;
    end Create_Compiler;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Compiler; output : A_Output_Proc ) is
    begin
        Limited_Object(this.all).Construct;
        this.output := output;
    end Construct;

    ----------------------------------------------------------------------------

    function Add_File( this      : not null access Compiler'Class;
                       pathInZip : String;
                       filePath  : String ) return Boolean is
        use String_Maps;
        pos      : String_Maps.Cursor;
        inserted : Boolean;
    begin
        this.files.Insert( pathInZip, filePath, pos, inserted );
        if not inserted then
            -- does the existing record match the source file? (not an error)
            return Ada.Strings.Equal_Case_Insensitive( Element( pos ), filePath );
        else
            -- inserted ok
            if Is_Format_Supported( Get_Extension( filePath ) ) then
                this.imageCount := this.imageCount + 1;
            end if;
            return True;
        end if;
    end Add_File;

    ----------------------------------------------------------------------------

    function Compile( this           : not null access Compiler'Class;
                      libPath        : String;
                      tileListPath   : String;
                      matrixListPath : String;
                      terrainsPath   : String;
                      compress       : Boolean ) return Boolean is
    begin
        this.error := False;
        this.imageCount := 0;
        this.tileCount := 0;
        this.matrixCount := 0;
        this.terrainCount := 0;
        this.files.Clear;

        if not Equal_Case_Insensitive( Get_Extension( libPath ), LIBRARY_EXTENSION ) then
            -- invalid library extension
            this.Put_Error( "Invalid library filename: " & Get_Filename( libPath ) );
            return False;
        end if;

        -- - - - - - - - - - - - - - - -
        -- Setup directories

        -- ensure the output directory for the archive exists
        this.outputDir := To_Unbounded_String( Get_Directory( libPath ) );
        if this.outputDir = "" then
            this.outputDir := To_Unbounded_String( "." & Slash );
        end if;
        if not GNAT.OS_Lib.Is_Directory( To_String( this.outputDir ) ) then
            if not Make_Dir( To_String( this.outputDir ) ) then
                this.Put_Error( "Failed to create directory: " &
                                To_String( this.outputDir ) );
                return False;
            end if;
        end if;

        -- create a temporary directory
        this.tempDir := To_Unbounded_String( Temp_Directory & "tmp" & Image( Random_32 ) & Slash );
        while GNAT.OS_Lib.Is_Directory( To_String( this.tempDir ) ) loop
            this.tempDir := To_Unbounded_String( Temp_Directory & "tmp" & Image( Random_32 ) & Slash );
        end loop;
        if not Make_Dir( To_String( this.tempDir ) ) then
            this.Put_Error( "Failed to create temporary directory" );
            return False;
        end if;

        -- - - - - - - - - - - - - - - -
        -- Create library from sources

        this.lib := Create_Library( Remove_Extension( Get_Filename( libPath ) ) );

        if not this.error then
            this.Read_Index_File( tileListPath );
        end if;

        if not this.error and then matrixListPath'Length > 0 then
            this.Compile_Matrix_File( matrixListPath, To_String( this.tempDir ) & MATRIX_FILENAME );
        end if;

        if not this.error and then terrainsPath'Length > 0 then
            this.Compile_Terrain_File( terrainsPath, To_String( this.tempDir ) & TERRAIN_FILENAME );
        end if;

        -- - - - - - - - - - - - - - - -
        -- Write library files

        if not this.error then
            this.Write_Catalog_File( To_String( this.tempDir ) & CATALOG_FILENAME );
        end if;
        if not this.error then
            this.Write_Info_File( To_String( this.tempDir ) & INFO_FILENAME );
        end if;

        if not this.error then
            this.Write_Library_File( libPath, compress );
        end if;

        -- - - - - - - - - - - - - - - -
        -- Cleanup

        Delete( A_Asset(this.lib) );

        begin
            Ada.Directories.Delete_Tree( To_String( this.tempDir ) );
        exception
            when others =>
                -- failed to clean up temporary directory
                this.Put_Line( "Failed to delete temporary directory", error => False );
        end;

        return not this.error;
    exception
        when e : others =>
            this.Put_Error( "Exception: " & Exception_Name( e ) & " - " &
                            Exception_Message( e ) );
            Delete( A_Asset(this.lib) );
            return False;
    end Compile;

    ----------------------------------------------------------------------------

    procedure Put_Line( this  : not null access Compiler'Class;
                        msg   : String;
                        error : Boolean := False ) is
    begin
        this.error := this.error or error;
        if this.output /= null then
            this.output.all( msg, error );
        end if;
    end Put_Line;

    ----------------------------------------------------------------------------

    procedure Put_Error( this : not null access Compiler'Class;
                         msg  : String ) is
    begin
        this.Put_Line( msg, error => True );
    end Put_Error;

    ----------------------------------------------------------------------------

    procedure Read_Index_File( this : not null access Compiler'Class;
                               path : String ) is
        line : Positive := 1;

        ------------------------------------------------------------------------

        -- 'text' contains no leading or trailing whitespace or comments
        function Scan_Tile( text : Unbounded_String ) return A_Tile is

            --------------------------------------------------------------------

            function Is_Whitespace( c : Character ) return Boolean is (c = ' ' or c = ASCII.HT);

            function Index_Of_Whitespace( str : Unbounded_String; start : Positive ) return Natural is
            begin
                for i in start..Length( str ) loop
                    if Is_Whitespace( Element( str, i ) ) then
                        return i;
                    end if;
                end loop;
                return 0;
            end Index_Of_Whitespace;

            --------------------------------------------------------------------

            tile     : A_Tile;
            first    : Integer := 1;
            last     : Integer;
            foundId  : Boolean;
            filename : Unbounded_String;
        begin
            last := Index_Of_Whitespace( text, first ) - 1;
            last := (if last < first then Length( text ) else last);

            -- empty line?
            if last < first then
                return null;
            end if;

            -- accept optional tile id
            foundId := True;
            for i in first..last loop
                if Element( text, i ) not in '0'..'9' then
                    foundId := False;
                    exit;
                end if;
            end loop;

            if foundId then
                tile := Create_Tile( Natural'Value( Slice( text, first, last ) ) );

                -- skip the following whitespace
                first := last + 1;
                while first <= Length( text ) and then Is_Whitespace( Element( text, first ) ) loop
                    first := first + 1;
                end loop;
                last := Index_Of_Whitespace( text, first ) - 1;
                last := (if last < first then Length( text ) else last);
            else
                -- anonymous tile id (assigned later)
                tile := Create_Tile( ANON_TILE_ID );
            end if;

            -- expect filename for non-empty tiles
            filename := Unbounded_Slice( text, first, last );
            if tile.Get_Id = NULL_TILE_ID and Length( filename ) > 0 then
                this.Put_Error( path & ":" & Image( line ) & ": Unexpected token following tile id" );
                Delete( tile );
                return null;
            elsif tile.Get_Id /= NULL_TILE_ID and Length( filename ) = 0 then
                this.Put_Error( path & ":" & Image( line ) & ": Expected filename" );
                Delete( tile );
                return null;
            elsif tile.Get_Id = NULL_TILE_ID then
                -- success!
                return tile;
            end if;

            -- ensure the filename is real
            if not GNAT.OS_Lib.Is_Regular_File( Get_Directory( path ) &
                                                To_String( filename ) )
            then
                this.Put_Error( path & ":" & Image( line ) & ": File not found: " &
                                Get_Directory( path ) & To_String( filename ) );
                Delete( tile );
                return null;
            end if;

            -- strip path information from the tile's name
            tile.Set_Name( Get_Filename( To_String( filename ) ) );

            -- mark the tile's image to be added to the library
            if not this.Add_File( "images/" & tile.Get_Name,
                                  Get_Directory( path ) & To_String( filename ) )
            then
                this.Put_Error( path & ":" & Image( line ) & ": Tile filename collision" );
                Delete( tile );
                return null;
            end if;

            -- accept optional attributes map
            first := last + 1;
            if first <= Length( text ) then
                declare
                    attrs : Map_Value;
                    keys  : List_Value;
                begin
                    attrs := Scribble.Compilers.Compile( Unbounded_Slice( text, first, Length( text ) ) ).Map;
                    if attrs.Valid then
                        keys := attrs.Get_Keys.Lst;
                        for i in 1..keys.Length loop
                            tile.Set_Attribute( keys.Get_String( i ),
                                                attrs.Get( keys.Get_String( i ) ) );
                        end loop;
                    else
                        this.Put_Error( path & ":" & Image( line ) & ": Expected attributes map" );
                    end if;
                exception
                    when Parse_Error =>
                        this.Put_Error( path & ":" & Image( line ) & ": Syntax error in attributes" );
                        Delete( tile );
                        return null;
                end;
            end if;

            return tile;
        end Scan_Tile;

        ------------------------------------------------------------------------

        use Ada.Text_IO;

        file : Ada.Text_IO.File_Type;
        text : Unbounded_String;
        tile : A_Tile;
    begin
        -- open the tile list file
        begin
            Open( file, In_File, path );
            if not Is_Open( file ) then
                this.Put_Error( "File not found: " & path );
                return;
            end if;
        exception
            when Name_Error =>
                this.Put_Error( "File not found: " & path );
                return;
            when others =>
                this.Put_Error( "Cannot open file: " & path );
                return;
        end;

        -- read line by line
        while not End_Of_File( file ) loop
            text := Trim_Whitespace_And_Comments( Get_Line( file ) );
            if Length( text ) > 0 then
                -- parse the line into a tile with attributes
                tile := Scan_Tile( text );
                begin
                    if tile /= null then
                        this.lib.Add_Tile( tile );
                        this.tileCount := this.tileCount + 1;
                    end if;
                exception
                    when e : DUPLICATE_TILE =>
                        Delete( tile );
                        this.Put_Error( path & ":" & Image( line ) & ": " &
                                        Exception_Message( e ) );
                    when e : others =>
                        Delete( tile );
                        this.Put_Error( path & ":" & Image( line ) & ": " &
                                        "Unknown error: " & Exception_Name( e ) &
                                        " - " & Exception_Message( e ) );
                end;
            end if;
            line := line + 1;
        end loop;

        -- display the progress
        if not this.error then
            this.Put_Line( "Parsed " & Image( this.tileCount ) & " tiles" );
        end if;

        Close( file );
    end Read_Index_File;

    ----------------------------------------------------------------------------

    procedure Compile_Matrix_File( this    : not null access Compiler'Class;
                                   path    : String;
                                   outPath : String ) is
        use Ada.Streams.Stream_IO;

        val        : Value;
        content    : List_Value;

        matrix     : Map_Value;
        name       : Value := Values.Strings.Create( "Undefined" );
        icon       : Value;
        matrixType : Value;
        rows       : List_Value;
        height     : Integer;
        width      : Integer;

        row        : List_Value;
        layers     : List_Value;
        hasLayers  : Boolean;
        index      : Integer;
        layer      : Integer;
        id         : Integer;

        map        : A_Map;
        matrices   : Map_Vectors.Vector;
        output     : File_Type;
        ok         : Boolean;
        pragma Warnings( Off, ok );
    begin
        -- 1. read the file
        if not Load_Value( val, GNAT.OS_Lib.Normalize_Pathname( path ), "" ) then
            if val.Err.Get_Code = Not_Found then
                -- No error: matrix file is optional
                this.Put_Line( "Skipping matrices, file not found: " & path );
            else
                this.Put_Error( val.Err.Get_Message );
            end if;
            return;
        end if;

        -- 2. validate the file contents
        content := val.Lst;
        if not content.Valid then
            this.Put_Error( path & ": Invalid file format" );
            return;
        end if;
        for m in 1..content.Length loop
            matrix := content.Get( m ).Map;
            if not matrix.Valid then
                this.Put_Error( path & ": Element " & Image( m ) & ": Expected matrix (map)" );
                Delete_All( matrices );
                return;
            end if;

            -- validate the "name" field
            name := matrix.Get( "name" );
            if not name.Is_String then
                this.Put_Error( path & ": Matrix " & Image( m ) & ": Expected name field (string)" );
                Delete_All( matrices );
                return;
            end if;

            -- validate the "icon" field
            icon := matrix.Get( "icon" );
            if not icon.Is_Int and then icon.To_Int > 0 then
                this.Put_Error( path & ": Matrix '" & Cast_String( name ) & "': Expected icon field (tile id)" );
                Delete_All( matrices );
                return;
            end if;

            -- validate the "type" field
            matrixType := matrix.Get( "type" );
            if not matrixType.Is_String then
                this.Put_Error( path & ": Matrix '" & Cast_String( name ) & "': Expected type field (string)" );
                Delete_All( matrices );
                return;
            end if;

            -- validate the "rows" field
            rows := matrix.Get( "rows" ).Lst;
            if not rows.Valid then
                this.Put_Error( path & ": Element '" & Cast_String( name ) & "': Expected rows field (list)" );
                Delete_All( matrices );
                return;
            else
                height := rows.Length;
                if height = 0 then
                    this.Put_Error( path & ": Matrix '" & Cast_String( name ) & "': Matrix is empty" );
                    Delete_All( matrices );
                    return;
                end if;
            end if;

            -- loop over the rows (one or more)
            for r in 1..height loop

                -- validate the row
                row := rows.Get( r ).Lst;
                if not row.Valid or else row.Length = 0 then
                    this.Put_Error( path & ": Matrix '" & Cast_String( name ) & "': Error in row " & Image( r ) );
                    Delete_All( matrices );
                    return;
                end if;

                -- check if the row format has explicit layer numbers
                hasLayers := False;
                for c in 1..row.Length loop
                    if row.Get( c ).Is_List then
                        hasLayers := True;
                        exit;
                    end if;
                end loop;

                -- create the map with the first layer
                if r = 1 then
                    if not hasLayers then
                        -- the row is just a list of columns
                        width := row.Length;
                    elsif row.Length >= 2 and then row.Get( 2 ).Is_List then
                        -- the row is layered, use the columns of the first layer
                        width := row.Get( 2 ).Lst.Length;
                    else
                        -- row has a bad format
                        this.Put_Error( path & ": Matrix '" & Cast_String( name ) & "': Error in row " & Image( r ) );
                        Delete_All( matrices );
                        return;
                    end if;
                    map := Create_Map( width, height );
                    map.Add_Layer( 0, Layer_Tiles, Create_Map( (Pair( "name", name ),
                                                                Pair( "icon", icon ),
                                                                Pair( "type", matrixType ))
                                                             ).Map );
                    matrices.Append( map );
                    this.matrixCount := this.matrixCount + 1;
                end if;

                if hasLayers then
                    -- the 'row' list already contains a list of alternating
                    -- layer numbers and rows (column lists).
                    layers := row;
                else
                    -- create an explicit layer 0 for the row
                    layers := Create_List( (1 => Values.Construction.Create( 0 ), 2 => Value(row)), consume => True ).Lst;
                end if;

                -- loop over the layers in the row
                index := 1;
                while index <= layers.Length loop

                    if index = layers.Length or else
                       not layers.Get( index ).Is_Int or else
                       not layers.Get( index + 1 ).Is_List
                    then
                        -- either the row has an odd number of items, or it
                        -- isn't composed of alternating layer numbers and
                        -- column lists.
                        this.Put_Error( path & ": Matrix '" & Cast_String( name ) & "': Error in row " & Image( r ) );
                        Delete_All( matrices );
                        return;
                    end if;

                    layer := layers.Get( index ).To_Int;
                    row := layers.Get( index + 1 ).Lst;

                    -- validate the row width matches the matrix width
                    if row.Length /= width then
                         this.Put_Error( path & ": Matrix " & Image( m ) & ": Row " & Image( r ) &
                                         ": Expected " & Image( width ) & " columns" );
                         Delete_All( matrices );
                         return;
                    end if;

                    -- create a foreground or background layer as necessary
                    if layer < map.Get_Foreground_Layer and layer = -1 then
                        map.Add_Layer( map.Get_Foreground_Layer - 1, Layer_Tiles );
                    elsif layer > map.Get_Background_Layer and layer = 1 then
                        map.Add_Layer( map.Get_Background_Layer + 1, Layer_Tiles );
                    end if;

                    if layer not in map.Get_Foreground_Layer..map.Get_Background_Layer then
                        -- the layer could not be created
                        this.Put_Error( path & ": Matrix '" & Cast_String( name ) & "': Invalid layer in row " & Image( r ) );
                        Delete_All( matrices );
                        return;
                    end if;

                    -- loop over the columns of the row
                    for c in 1..width loop
                        id := row.Get_Int( c, -1 );
                        if id = 0 or else this.lib.Get_Tile( id ) /= null then
                            map.Set_Tile( layer, c - 1, r - 1, id );
                        else
                            this.Put_Error( path & ": Matrix '" & Cast_String( name ) &
                                            "': Error at row " & Image( r ) & ", column " & Image( c ) & ", layer " & Image( layer ) );
                            Delete_All( matrices );
                            return;
                        end if;
                    end loop; -- columns

                    index := index + 2;
                end loop; -- layers

            end loop; -- rows

            if width = 0 then
                this.Put_Error( path & ": Matrix '" & Cast_String( name ) & "': Matrix is empty" );
                Delete_All( matrices );
                return;
            end if;
        end loop; -- matrices

        -- 3. write the compiled file
        Ada.Streams.Stream_IO.Create( output, Out_File, outPath );
        if not Is_Open( output ) then
            this.Put_Error( "Unable to create file: " & outPath );
            Delete_All( matrices );
            return;
        end if;
        Integer'Output( Stream( output ), this.matrixCount );
        for m of matrices loop
            A_Map'Output( Stream( output ), m );
        end loop;
        Close( output );

        -- 4. add to the file list
        if this.matrixCount > 0 and not this.error then
            this.Put_Line( "Parsed " & Image( this.matrixCount ) & " matrices" );
            ok := this.Add_File( Get_Filename( outPath ), outPath );
        end if;
    end Compile_Matrix_File;

    ----------------------------------------------------------------------------

    procedure Compile_Terrain_File( this    : not null access Compiler'Class;
                                    path    : String;
                                    outPath : String ) is
        use Ada.Streams.Stream_IO;

        ------------------------------------------------------------------------

        procedure Validate_Terrain( name : String; val : Value ) is
            items : List_Value;
            i     : Integer;
            item  : Value;
            slice : List_Value;
            tile  : List_Value;
        begin
            if not val.Is_List then
                this.Put_Error( path & ": Invalid definition for terrain '" & name &
                                "': Expected list" );
            end if;

            items := val.Lst;
            i := 1;
            while i <= items.Length and not this.error loop
                item := items.Get( i );
                i := i + 1;

                -- expect an angle for the next path edge, or the first of two
                -- angles in a corner.
                if item.Is_Number then
                    item := (if i <= items.Length then items.Get( i ) else Null_Value);
                    i := i + 1;

                    -- expect a terrain slice, or the second of two angles in a
                    -- corner.
                    if item.Is_List then

                        -- one or more terrain slices
                        while item.Is_List loop
                            slice := item.Lst;

                            -- add all tiles to the slice
                            for j in 1..slice.Length loop
                                tile := slice.Get( j ).Lst;
                                if tile.Valid and then tile.Length = 3 then
                                    if not tile.Get( 1 ).Is_Int then
                                        this.Put_Error( path & ": Expected layer (element 1) in list at index " & Image( j ) &
                                                        " of slice at index " & Image( i - 1 ) &
                                                        " of terrain '" & name & "'" );
                                    elsif not tile.Get( 2 ).Is_Int then
                                        this.Put_Error( path & ": Expected offset (element 2) in list at index " & Image( j ) &
                                                        " of slice at index " & Image( i - 1 ) &
                                                        " of terrain '" & name & "'" );
                                    elsif not tile.Get( 3 ).Is_Int or else tile.Get_Int( 3 ) < 0 then
                                        this.Put_Error( path & ": Expected tile id (element 3) in list at index " & Image( j ) &
                                                        " of slice at index " & Image( i - 1 ) &
                                                        " of terrain '" & name & "'" );
                                    end if;
                                else
                                    this.Put_Error( path & ": Expected tile list at index " & Image( j ) &
                                                    " of slice at index " & Image( i - 1 ) &
                                                    " of terrain '" & name & "'" );
                                end if;
                            end loop;

                            item := (if i <= items.Length then items.Get( i ) else Null_Value);
                            i := i + 1;
                        end loop;

                        -- backup- don't skip the item following the last slice
                        i := i - 1;

                    -- corner
                    elsif item.Is_Number then

                        item := (if i <= items.Length then items.Get( i ) else Null_Value);
                        i := i + 1;

                        -- expect a terrain corner
                        if item.Is_List then

                            -- one or more terrain corner slices
                            while item.Is_List loop
                                slice := item.Lst;

                                -- add all tiles to the corner
                                for j in 1..slice.Length loop
                                    tile := slice.Get( j ).Lst;
                                    if tile.Valid and then tile.Length = 4 then
                                        if not tile.Get( 1 ).Is_Int then
                                            this.Put_Error( path & ": Expected layer (element 1) in list at index " & Image( j ) &
                                                            " of corner at index " & Image( i - 1 ) &
                                                            " of terrain '" & name & "'" );
                                        elsif not tile.Get( 2 ).Is_Int then
                                            this.Put_Error( path & ": Expected X offset (element 2) in list at index " & Image( j ) &
                                                            " of corner at index " & Image( i - 1 ) &
                                                            " of terrain '" & name & "'" );
                                        elsif not tile.Get( 3 ).Is_Int then
                                            this.Put_Error( path & ": Expected Y offset (element 3) in list at index " & Image( j ) &
                                                            " of corner at index " & Image( i - 1 ) &
                                                            " of terrain '" & name & "'" );
                                        elsif not tile.Get( 4 ).Is_Int or else tile.Get_Int( 4 ) < 0 then
                                            this.Put_Error( path & ": Expected tile id (element 4) in list at index " & Image( j ) &
                                                            " of corner at index " & Image( i - 1 ) &
                                                            " of terrain '" & name & "'" );
                                        end if;
                                    else
                                        this.Put_Error( path & ": Expected tile list at index " & Image( j ) &
                                                        " of corner at index " & Image( i - 1 ) &
                                                        " of terrain '" & name & "'" );
                                    end if;
                                end loop;

                                item := (if i <= items.Length then items.Get( i ) else Null_Value);
                                i := i + 1;
                            end loop;

                            -- backup- don't skip the item following the last slice
                            i := i - 1;

                        else
                            this.Put_Error( path & ": Expected terrain corner list at index " &
                                            Image( i - 1 ) & " of terrain '" & name & "'" );
                        end if;

                    else
                        this.Put_Error( path & ": Expected terrain slice list at index " &
                                        Image( i - 1 ) & " of terrain '" & name & "'" );
                    end if;
                else
                    this.Put_Error( path & ": Expected path angle number at index " &
                                    Image( i - 1 ) & " of terrain '" & name & "' " &
                                    " found: " & Image( item ) );
                end if;
            end loop;

            if not this.error then
                this.terrainCount := this.terrainCount + 1;
            end if;
        end Validate_Terrain;

        ------------------------------------------------------------------------

        val     : Value;
        content : List_Value;
        def     : Map_Value;
        output  : File_Type;
        ok      : Boolean;
        pragma Warnings( Off, ok );
    begin
        -- 1. read the terrains file
        if not Load_Value( val, GNAT.OS_Lib.Normalize_Pathname( path ), "" ) then
            if val.Err.Get_Code = Not_Found then
                -- No error: matrix file is optional
                this.Put_Line( "Skipping terrains, file not found: " & path );
            else
                this.Put_Error( val.Err.Get_Message );
            end if;
            return;
        end if;

        -- 2. validate the file contents
        content := val.Lst;
        if not content.Valid then
            this.Put_Error( path & ": Expected list" );
            return;
        end if;
        for i in 1..content.Length loop
            def := content.Get( i ).Map;
            if not def.Valid then
                this.Put_Error( path & ": Expected terrain map at element " & Image( i ) );
            elsif not def.Get( "name" ).Is_String or else def.Get_String( "name" )'Length = 0 then
                this.Put_Error( path & ": Expected 'name' for terrain at element " & Image( i ) );
            elsif not def.Get( "icon" ).Is_Int or else def.Get_Int( "icon" ) <= 0 then
                this.Put_Error( path & ": Expected 'icon' for terrain '" & def.Get_String( "name" ) & "'" );
            else
                Validate_Terrain( def.Get_String( "name" ), def.Get( "definition" ) );
            end if;
        end loop;

        -- 3. write the compiled file
        Ada.Streams.Stream_IO.Create( output, Out_File, outPath );
        if not Is_Open( output ) then
            this.Put_Error( "Unable to create file: " & outPath );
            return;
        end if;
        Scribble.IO.Write( Stream( output ), content, Get_Filename( path ) );
        Close( output );

        -- 4. add to the file list
        if this.terrainCount > 0 and not this.error then
            this.Put_Line( "Parsed " & Image( this.terrainCount ) & " terrains" );
            ok := this.Add_File( Get_Filename( outPath ), outPath );
        end if;
    end Compile_Terrain_File;

    ----------------------------------------------------------------------------

    procedure Write_Catalog_File( this : not null access Compiler'Class;
                                  path : String ) is
        use Ada.Streams.Stream_IO;
        file : File_Type;
        ok   : Boolean;
        pragma Warnings( Off, ok );
    begin
        Ada.Streams.Stream_IO.Create( file, Out_File, path );
        if not Is_Open( file ) then
            this.Put_Error( "Unable to create file " & path );
            return;
        end if;

        A_Catalog'Output( Stream( file ), this.lib.catalog );
        Close( file );

        -- should always succeed
        ok := this.Add_File( Get_Filename( path ), path );
    exception
        when e : others =>
            if Is_Open( file ) then
                Close( file );
            end if;
            this.Put_Error( "Error writing " & path & ": " & Exception_Name( e ) &
                            " - " & Exception_Message( e ) );
    end Write_Catalog_File;

    ----------------------------------------------------------------------------

    procedure Write_Info_File( this : not null access Compiler'Class;
                               path : String ) is
        use Ada.Text_IO;
        file : File_Type;
        ok   : Boolean;
        pragma Warnings( Off, ok );
    begin
        Ada.Text_IO.Create( file, Out_File, path );
        if not Is_Open( file ) then
            this.Put_Error( "Unable to create file: " & path );
            return;
        end if;

        Put_Line( file, "Version    : " & Image( Catalog_Version_Write ) );
        Put_Line( file, "Created By : " & Get_Application.Get_Name & " " & Version.Release );
        Put_Line( file, "Date       : " & Ada.Calendar.Formatting.Image( Clock ) & " UTC" );
        Put_Line( file, "Images     : " & Image( this.imageCount ) );
        Put_Line( file, "Tiles      : " & Image( this.tileCount ) );
        Put_Line( file, "Matrices   : " & Image( this.matrixCount ) );
        Put_Line( file, "Terrains   : " & Image( this.terrainCount ) );
        Close( file );

        -- should always succeed
        ok := this.Add_File( Get_Filename( path ), path );
    end Write_Info_File;

    ----------------------------------------------------------------------------

    procedure Write_Library_File( this     : not null access Compiler'Class;
                                  path     : String;
                                  compress : Boolean ) is
        use String_Maps;

        archive            : Zip_Create_info;
        fileStream         : aliased File_Zipstream;
        filesRemaining     : Natural;
        pos                : String_Maps.Cursor;
        lastProgressReport : Ada.Real_Time.Time;
        success            : Boolean;
    begin
        -- Delete the old file first, if it exists
        GNAT.OS_Lib.Delete_File( path, success );

        -- Create a new zip file
        if compress then
            Zip.Create.Create( archive, fileStream'Unrestricted_Access, path, Zip.Compress.Shrink );
        else
            Zip.Create.Create( archive, fileStream'Unrestricted_Access, path, Zip.Compress.Store );
        end if;

        -- Add all of the files to the archive
        filesRemaining := Natural(this.files.Length);
        this.Put_Line( "Adding " & Image( filesRemaining ) & " files..." );
        lastProgressReport := Clock;
        pos := this.files.First;
        while Has_Element( pos ) loop
            begin
                Add_File( archive, Element( pos ), Key( pos ) );
            exception
                when e : others =>
                    this.Put_Error( "Error adding " & Element( pos ) & ": " &
                                    Exception_Name( e ) );
            end;

            Next( pos );
            filesRemaining := filesRemaining - 1;

            -- report the progress every 5 seconds and at the end
            if Clock - Seconds( 5 ) > lastProgressReport then
                if filesRemaining > 0 then
                    this.Put_Line( Image( 100 - (filesRemaining * 100 / Integer(this.files.Length)) ) & "% complete" );
                end if;
                lastProgressReport := Clock;
            end if;
        end loop;
        this.Put_Line( "100% complete" );

        -- Finalize and close the zip file
        Finish( archive );

        -- Clean up on error
        if this.error then
            GNAT.OS_Lib.Delete_File( path, success );
        else
            this.Put_Line( "Wrote " & path );
        end if;
    end Write_Library_File;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Compiler ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

end Assets.Libraries.Compilers;
