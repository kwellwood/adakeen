--
-- Copyright (c) 2012 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Real_Time;                     use Ada.Real_Time;

package Processes is

    type Tick_Time is
        record
            total   : Time_Span;      -- time elapsed since the process began
            elapsed : Time_Span;      -- time elapsed since the previous tick
        end record;

    ----------------------------------------------------------------------------

    -- A Process is attached to a Process_Manager to execute code periodically
    -- in time slices (also called a frame). Implementing the Process interface
    -- with any class and attaching an instance of it a process manager allows
    -- for any object to have its own behavior.
    type Process is limited interface;
    type A_Process is access all Process'Class;

    -- Returns a name identifying the process.
    function Get_Process_Name( this : access Process ) return String is abstract;

    -- Executes one frame (time slice) of the process logic. 'time' contains
    -- information including how much time has elapsed since the last call to
    -- Tick and how long the process has been attached to the process manager.
    --
    -- Processes are intended to execute their behavior iteratively; to update
    -- their state based on the passage of time and perform longer actions in
    -- small increments. Tick should finish its work and return as quickly as
    -- possible, to allow all the other processes attached to the manager
    -- enough time to execute before the next scheduled tick.
    procedure Tick( this : access Process; time : Tick_Time ) is abstract;

end Processes;
