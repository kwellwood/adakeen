--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Debugging.CLI is

    ----------------------------------------------------------------------------
    -- This package parses the command line arguments at elaboration time. The
    -- following are the recognized arguments:
    --
    -- -DV=[verbosity]          (Debugging Verbosity)
    -- -DV[system]=[verbosity]  (Debugging Verbosity per system)
    --
    --     Where:
    --     [verbosity] is the verbosity level.
    --         'e' : Errors only
    --         'w' : Warnings and errors only
    --         'i' : All info, warnings and errors
    --
    --     [system] is the name of a specific debugging system. The standard
    --         systems names are predefined in the Engine_Debugging package.
    --
    -- -DD=[decoration]         (Debugging Decoration)
    --
    --     Where:
    --     [decoration] is the level of decoration for every line output by the
    --         debugging package. The decoration levels are:
    --         '0' : No decoration
    --         '1' : Prefixed with current task image
    --         '2' : Prefixed with calling source unit and current task image
    --
    -- -DF=[filename]           (Debugging file)
    --
    --     Where:
    --     [filename] is the file name or path to write debugging information.
    --         Debugging output will be appended to the file. If just a filename
    --         is given, the file will be written to the current working
    --         directory. This is usually the same directory as the executable.
    ----------------------------------------------------------------------------

    -- Parses command line arguments to override decoration and verbosity
    -- default settings. This should be called as early as possible, preferably
    -- during elaboration.
    procedure Parse_Arguments;

end Debugging.CLI;
