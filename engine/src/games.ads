--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Entities.Systems;                  use Entities.Systems;
with Events;                            use Events;
with Events.Corrals;                    use Events.Corrals;
with Objects;                           use Objects;
with Processes.Managers;                use Processes.Managers;
with Processes;                         use Processes;
with Script_VMs;                        use Script_VMs;
with Sessions;                          use Sessions;

package Games is

    -- The Game class is reponsible for managing the game's simulation, including
    -- game session management, script execution, and entity interactions. The
    -- game logic runs in the Game's process manager thread. It communicates
    -- with other threads, including the game view, by sending and receiving
    -- events via the event manager.
    type Game is abstract new Limited_Object with private;
    type A_Game is access all Game'Class;

    -- Creates the global Game object, returning a reference to it. The Game's
    -- internal thread will update at a rate of 'updateHz' when it is started,
    -- simulating in fixed time steps.
    function Create_Game( updateHz : Positive;
                          scriptVM : not null A_Script_VM;
                          session  : not null A_Session ) return A_Game;

    -- Initializes the game and its session. This must be called just after
    -- construction and before use.
    procedure Initialize( this : not null access Game'Class );

    -- Attaches a Process to the game's Process_Manager.
    procedure Attach( this : not null access Game'Class;
                      proc : not null access Process'Class );

    -- Detaches a Process from the game's Process_Manager.
    procedure Detach( this : not null access Game'Class;
                      proc : not null access Process'Class );

    -- Returns the Game's event Corral.
    function Get_Corral( this : not null access Game'Class ) return A_Corral;
    pragma Postcondition( Get_Corral'Result /= null );

    -- Returns the Game's entity system.
    function Get_Entity_System( this : not null access Game'Class ) return A_Entity_System;
    pragma Postcondition( Get_Entity_System'Result /= null );

    -- Returns the Game's process manager.
    function Get_Process_Manager( this : not null access Game'Class ) return A_Process_Manager;
    pragma Postcondition( Get_Process_Manager'Result /= null );

    -- Returns the Game's script VM, used for script compilation and execution.
    function Get_Script_VM( this : not null access Game'Class ) return A_Script_VM;

    -- Returns the Game's Session.
    function Get_Session( this : not null access Game'Class ) return A_Session;

    -- Starts the Game's execution thread. This can only happen once. If the
    -- game has already been started, nothing will change. Be sure to call
    -- Stop() before deleting the object.
    procedure Start( this : not null access Game'Class );

    -- Synchronously stops the game's execution thread. This must be called
    -- before deleting the object if Start() has been called. If the game has
    -- not been started, or already stopped, nothing will change.
    procedure Stop( this : not null access Game'Class );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Deletes the Game object. If the Game has been started, it must be stopped
    -- before it can be deleted.
    procedure Delete( this : in out A_Game );
    pragma Postcondition( this = null );

    -- Returns a pointer to the global Game instance or null if it hasn't been
    -- created yet.
    function Get_Game return A_Game;

private

    type Game is new Limited_Object with
        record
            pman        : A_Process_Manager := null;
            scriptVM    : A_Script_VM := null;
            ecs         : A_Entity_System := null;
            session     : A_Session := null;
            frames      : Integer := 0;            -- total number of game frames
            frameSyncOn : Boolean := False;        -- frame synchronization active? (debugging only)
        end record;

    procedure Construct( this     : access Game;
                         updateHz : Positive;
                         scriptVM : not null A_Script_VM;
                         session  : not null A_Session );

    procedure Delete( this : in out Game );

end Games;
