--
-- Copyright (c) 2016-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Script_VMs;                        use Script_VMs;

package Game_Scripting is

    -- Creates and returns a Sript_VM with all the basic engine functions
    -- registered. An anonymous namespace is registered but initialized as
    -- empty. It can be loaded from a script as necessary.
    function Create_Game_VM return A_Script_VM;

end Game_Scripting;
