--
-- Copyright (c) 2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Values.Construction;               use Values.Construction;
with Values.Strings;                    use Values.Strings;

package body Values.Allegro_Colors is

    function Cast_Color( val : Value'Class; default : Allegro_Color := Black ) return Allegro_Color
        is (if val.Is_Color then To_Color( val ) else default);

    ----------------------------------------------------------------------------

    function Coerce_Color( val : Value'Class; default : Allegro_Color := Black ) return Allegro_Color is
    begin
        if val.Is_Color then
            return To_Color( val );
        elsif val.Is_String then
            return Html_To_Color( val.Str.To_String, default );
        end if;
        return default;
    end Coerce_Color;

    ----------------------------------------------------------------------------

    function Create( color : Allegro_Color ) return Value is
        r, g, b, a : Float;
    begin
        Al_Unmap_RGBA_f( color, r, g, b, a );
        return Create_Color( r, g, b, a );
    end Create;

    ----------------------------------------------------------------------------

    function To_Color( this : Value'Class ) return Allegro_Color is
        r, g, b, a : Float;
    begin
        this.To_RGBA( r, g, b, a );
        return Al_Map_RGBA_f( r, g, b, a );
    end To_Color;

end Values.Allegro_Colors;
