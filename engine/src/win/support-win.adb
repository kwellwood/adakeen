--
-- Copyright (c) 2012-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Unchecked_Conversion;
with Debugging;                         use Debugging;
with GNAT.OS_Lib;                       use GNAT.OS_Lib;
with Support.Paths;                     use Support.Paths;
--with Win32;                             use Win32;
--with Win32.Winbase;                     use Win32.Winbase;
--with Win32.Windef;                      use Win32.Windef;

package body Support.Win is

    function App_Data_Directory return String is
        path : aliased String(1..MAX_PATH);
    begin
        if SHGetFolderPathA( Null_Address, CSIDL_COMMON_APPDATA,
                             Null_Address, SHGFP_TYPE_CURRENT,
                             path'Unrestricted_Access ) < 0
        then
            return "";
        end if;
        for i in path'Range loop
            if path(i) = ASCII.NUL then
                return path(1..i - 1) & '\';
            end if;
        end loop;
        return path & '\';
    end App_Data_Directory;

    ----------------------------------------------------------------------------

    procedure Auto_Open( filePath : String ) is
        args : Argument_List(1..1);
        pid  : Process_Id;
        pragma Warnings( Off, pid );
    begin
        if Is_Regular_File( filePath ) then
            args(1) := new String'(filePath);
        else
            return;
        end if;
        pid := GNAT.OS_Lib.Non_Blocking_Spawn( "explorer.exe", args );
        for i in args'Range loop
            Free( args(i) );
        end loop;
    end Auto_Open;

    ----------------------------------------------------------------------------

    function Executable_Path return String is
        function To_LPSTR is new Ada.Unchecked_Conversion( System.Address, LPSTR );

        path : aliased String(1..MAX_PATH);
        len  : Interfaces.C.unsigned_long;
    begin
        len := GetModuleFileNameA( Null_Address, To_LPSTR( path(path'First)'Address ), MAX_PATH );
        if len = 0 then
            -- error getting executable path
            return Normalize_Pathname( "." );
        end if;
        return Normalize_Pathname( path(1..Integer(len)) );
    end Executable_Path;

    ----------------------------------------------------------------------------

    function Execution_Directory return String is (Get_Directory( Executable_Path ));

    ----------------------------------------------------------------------------

    function Home_Directory return String is
        str : GNAT.OS_Lib.String_Access := Getenv( "USERPROFILE" );
    begin
        if str /= null then
            declare
                result : String(1..Integer'Min(MAX_PATH, str'Length));
            begin
                result := str(str'First..str'First + Integer'Min(MAX_PATH, str'Length) - 1);
                Free( str );
                return result & '\';
            end;
        else
            Dbg( "Warning: Environment variable UserProfile not found" );
            return "";
        end if;
    end Home_Directory;

    ----------------------------------------------------------------------------

    procedure Reveal_Path( path : String ) is
        args : Argument_List(1..3);
        pid  : Process_Id;
        pragma Warnings( Off, pid );
    begin
        if Is_Directory( path ) then
            args(1) := new String'("/n,");
            args(2) := new String'("/root,");
            args(3) := new String'(path);
        elsif Is_Regular_File( path ) then
            args(1) := new String'("/n,");
            args(2) := new String'("/select,");
            args(3) := new String'(path);
        else
            return;
        end if;
        pid := GNAT.OS_Lib.Non_Blocking_Spawn( "explorer.exe", args );
        for i in args'Range loop
            Free( args(i) );
        end loop;
    end Reveal_Path;

    ----------------------------------------------------------------------------

    function System_Font_Directory return String is
        str : GNAT.OS_Lib.String_Access := Getenv( "SystemRoot" );
    begin
        if str /= null then
            declare
                result : String(1..Integer'Min(MAX_PATH, str'Length));
            begin
                result := str(str'First..str'First + Integer'Min(MAX_PATH, str'Length) - 1);
                Free( str );
                return result & "\fonts\";
            end;
        else
            Dbg( "Warning: Environment variable SystemRoot not found" );
            return "";
        end if;
    end System_Font_Directory;

    ----------------------------------------------------------------------------

    function Temp_Directory return String is
        str : GNAT.OS_Lib.String_Access := Getenv( "TEMP" );
    begin
        if str = null then
            str := Getenv( "TMP" );
        end if;
        if str /= null then
            declare
                result : String(1..Integer'Min(MAX_PATH, str'Length));
            begin
                result := str(str'First..str'First + Integer'Min(MAX_PATH, str'Length) - 1);
                Free( str );

                -- in some weird setups, the TEMP folder may actually have
                -- multiple paths. choose the last one.
                for i in reverse result'Range loop
                    if result(i) = ';' then
                        return result(i+1..result'Last) & "\";
                    end if;
                end loop;

                return result & "\";
            end;
        else
            Dbg( "Warning: Environment variable TEMP not found" );
            return "";
        end if;
    end Temp_Directory;

end Support.Win;
