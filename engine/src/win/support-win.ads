--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

private with Interfaces.C;
private with System;

private package Support.Win is

    -- Returns the path of the directory where system-wide application-specific
    -- data files can be read and written on a Windows system.
    function App_Data_Directory return String;

    -- Attempts to open the file 'filePath' with the file type's default
    -- application.
    procedure Auto_Open( filePath : String );

    -- Returns the end-of-line sequence for Windows.
    function EOL return String is (String'(1 => ASCII.CR, 2 => ASCII.LF));

    -- Returns the file extension of executables on Windows without a leading
    -- dot character.
    function Executable_Extension return String is ("exe");

    -- Returns the path of the application's executable file.
    function Executable_Path return String;
    pragma Postcondition( Executable_Path'Result'Length > 0 );

    -- Returns the absolute path of the executable's directory.
    function Execution_Directory return String;
    pragma Postcondition( Execution_Directory'Result'Length > 0 );

    -- Returns the path of the user's home directory.
    function Home_Directory return String;
    pragma Postcondition( Home_Directory'Result'Length > 0 );

    -- Opens an Explorer window at the directory specified by 'path'. If 'path'
    -- is a file, the file will be selected in the window.
    procedure Reveal_Path( path : String );

    -- Returns the path of the Windows font directory.
    function System_Font_Directory return String;

    -- Returns the current platform's temporary directory.
    function Temp_Directory return String;

private

    use Interfaces.C;
    use System;

    type CSIDL is new Unsigned_32;

    CSIDL_DESKTOP              : constant CSIDL := 16#0000#;
    CSIDL_INTERNET             : constant CSIDL := 16#0001#;
    CSIDL_PROGRAMS             : constant CSIDL := 16#0002#;
    CSIDL_CONTROLS             : constant CSIDL := 16#0003#;
    CSIDL_PRINTERS             : constant CSIDL := 16#0004#;
    CSIDL_PERSONAL             : constant CSIDL := 16#0005#;
    CSIDL_FAVORITES            : constant CSIDL := 16#0006#;
    CSIDL_STARTUP              : constant CSIDL := 16#0007#;
    CSIDL_RECENT               : constant CSIDL := 16#0008#;
    CSIDL_SENDTO               : constant CSIDL := 16#0009#;
    CSIDL_DESKTOPDIRECTORY     : constant CSIDL := 16#0010#;
    CSIDL_BITBUCKET            : constant CSIDL := 16#000a#;
    CSIDL_STARTMENU            : constant CSIDL := 16#000b#;
    CSIDL_MYDOCUMENTS          : constant CSIDL := 16#000c#;
    CSIDL_MYMUSIC              : constant CSIDL := 16#000d#;
    CSIDL_MYVIDEO              : constant CSIDL := 16#000e#;
    CSIDL_LOCAL_APPDATA        : constant CSIDL := 16#001c#;
    CSIDL_COMMON_APPDATA       : constant CSIDL := 16#0023#;
    CSIDL_PROGRAM_FILES        : constant CSIDL := 16#0026#;
    CSIDL_PROGRAM_FILES_COMMON : constant CSIDL := 16#002b#;

    MAX_PATH : constant := 260;

    type    VOID      is null record;
    subtype PVOID     is System.Address;
    subtype HANDLE    is PVOID;
    subtype HINSTANCE is HANDLE;
    subtype HWND      is HANDLE;
    subtype CHAR      is Interfaces.C.char;
    type    PCHAR     is access all CHAR;
    subtype LPSTR     is PCHAR;
        pragma No_Strict_Aliasing( LPSTR );
    subtype ULONG     is Interfaces.C.unsigned_long;
    subtype DWORD     is ULONG;
    subtype INT       is Interfaces.C.int;

    type SHGFP is (SHGFP_TYPE_CURRENT, SHGFP_TYPE_DEFAULT);
    for SHGFP'Size use 32;

    function GetModuleFileNameA( hModule    : HINSTANCE;
                                 lpFilename : LPSTR;
                                 nSize      : DWORD
                               ) return DWORD;
    pragma Import (Stdcall, GetModuleFileNameA, "GetModuleFileNameA");

    function SHGetFolderPathA( hwndOwner : HWND;
                               nFolder   : CSIDL;
                               hToken    : PVOID;
                               dwFlags   : SHGFP;
                               lpszPath  : access String ) return INT;
    pragma Import( Stdcall, SHGetFolderPathA, "SHGetFolderPathA" );

end Support.Win;
