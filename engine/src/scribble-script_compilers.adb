--
-- Copyright (c) 2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Exceptions;                    use Ada.Exceptions;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Debugging;                         use Debugging;
with Resources.Plaintext;               use Resources.Plaintext;
with Scribble.Ast;                      use Scribble.Ast;
with Scribble.Ast.Scripts;              use Scribble.Ast.Scripts;
with Scribble.Parse_Errors;             use Scribble.Parse_Errors;
with Scribble.Parsers;                  use Scribble.Parsers;
with Scribble.String_Streams;           use Scribble.String_Streams;
with Values;                            use Values;

package body Scribble.Script_Compilers is

    function Create_Script_Compiler( runtime : not null A_Scribble_Runtime ) return A_Script_Compiler is
        this : constant A_Script_Compiler := new Script_Compiler;
    begin
        this.Construct( runtime );
        return this;
    end Create_Script_Compiler;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this    : access Script_Compiler;
                         runtime : not null A_Scribble_Runtime ) is
    begin
        Limited_Object(this.all).Construct;
        this.runtime := runtime;
        this.parser := Create_Script_Parser( this.runtime );
        this.semantics := Create_Script_Analyzer( this.runtime );
        this.generator := Create_Script_Generator( this.runtime );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Script_Compiler ) is
    begin
        Delete( this.parser );
        Delete( this.semantics );
        Delete( this.generator );
        Limited_Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Compile_Script( this        : not null access Script_Compiler'Class;
                             filePath    : String;
                             enableDebug : Boolean := False ) return Map_Value is
        result : Map_Value;
        source : Unbounded_String;
        strm   : A_String_Stream;
        ast    : A_Ast_Script;
    begin
        -- Stage 1: Read Source --
        if not Load_Plaintext( filePath, "scripts", source ) then
            Raise_Parse_Error( "File not found", (0, 0, 0, To_Unbounded_String( filePath )) );
        end if;

        -- Stage 2: Parsing --
        strm := Stream( source );
        this.parser.Set_Input( strm, filePath );
        this.parser.Enable_Debug( enableDebug );
        ast := this.parser.Parse_Script;             -- may raise Parse_Error
        this.parser.Expect_EOF;                      -- may raise Parse_Error
        this.parser.Set_Input( null );
        Close( strm );

        -- Stage 3: Semantic Analysis --
        this.semantics.Analyze( ast );               -- may raise Parse_Error

        -- Stage 4: Tree Pruning --
        ast.Prune;

        -- Stage 5: Code Generation --
        begin
            result := this.generator.Generate( ast, enableDebug );
        exception
            when e : others =>
                Raise_Parse_Error( "Code generation failed: " & Exception_Message( e ),
                                   (0, 0, 0, To_Unbounded_String( filePath )) );
        end;

        Delete( A_Ast_Node(ast) );
        return result;
    exception
        when e : Parse_Error =>
            this.parser.Set_Input( null );
            Close( strm );
            Delete( A_Ast_Node(ast) );
            Dbg( Format_Parse_Error( Exception_Message( e ) ) );
            raise;
    end Compile_Script;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Script_Compiler ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

end Scribble.Script_Compilers;
