--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Displays;                  use Allegro.Displays;

package Applications.Gui is

    -- An abstract application class with a single window for graphics and
    -- mouse/keyboard facilities.
    type Gui_Application is abstract new Application with private;
    type A_Gui_Application is access all Gui_Application'Class;

    -- Returns the Allegro display backing the application's window.
    function Get_Display( this : not null access Gui_Application'Class ) return A_Allegro_Display;

    -- Returns True. Gui_Applications use a Display window, returned by
    -- Get_Display.
    overriding
    function Is_Display_Supported( this : access Gui_Application ) return Boolean is (True);

    -- Returns True if the mouse is enabled for the application, allowing it to
    -- be drawn in fullscreen mode and for mouse events to be generated.
    function Is_Mouse_Enabled( this : not null access Gui_Application'Class ) return Boolean;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Displays an error message to the user with a native message box titled
    -- 'title'. The short text 'heading' is displayed above the more descriptive
    -- 'text' message.
    procedure Show_Error( this    : not null access Gui_Application'Class;
                          title   : String;
                          heading : String;
                          text    : String );

    -- Displays an error message to the user with a native message box. The
    -- title defaults to "Error!" and a heading of "An error occurred" just
    -- above the error 'text' to be displayed.
    procedure Show_Error( this : access Gui_Application; text : String );

private

    type Gui_Application is abstract new Application with
        record
            display : A_Allegro_Display;   -- the allegro display window

            -- these fields don't change after construction --
            defaultWidth     : Natural := 640;
            defaultHeight    : Natural := 480;
            defaultWindowed  : Boolean := True;
            defaultVsync     : Natural := 0;        -- 0 = Default, 1 = ON, 2 = OFF
            defaultColorBits : Natural := 32;       -- bits per pixel (BPP)
            defaultDepthBits : Natural := 32;       -- Z depth buffer bits
            defaultSamples   : Natural := 4;        -- multi-sampling resolution
            resizableWindow  : Boolean := False;    -- is the window resizable? (if windowed)
            framelessWindow  : Boolean := False;    -- is the window frameless? (if windowed)
            useMouse         : Boolean := False;    -- application uses the mouse
        end record;

    -- Initializes the application, setting up graphics and hardware. Returns
    -- True on success.
    --
    -- Subclass implementations must call this first.
    function Initialize( this : access Gui_Application ) return Boolean;

    -- Finalizes the application and releases all resources. Do not call this if
    -- the application didn't successfully initialize.
    --
    -- Subclass implementations must call this last.
    procedure Finalize( this : access Gui_Application );

end Applications.Gui;
