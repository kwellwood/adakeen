--
-- Copyright (c) 2016-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Game_Scripting.Game_API;
with Object_Ids;                        use Object_Ids;
with Scribble.Namespaces;               use Scribble.Namespaces;
with Scribble.Runtimes;                 use Scribble.Runtimes;
with Sessions;
with Values;                            use Values;
with Values.Tagged_Ids;                 use Values.Tagged_Ids;

package body Game_Scripting is

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Game_VM return A_Script_VM is
        this : constant A_Script_VM := Create_Script_VM;
    begin
        -- allow references, when compiling, to an anonymous namespace. It will
        -- generally be the global namespace, or a script namespace that falls
        -- back to the global namespace.
        this.Get_Runtime.Reserve_Namespace( ANONYMOUS_NAMESPACE );

        -- - - - - - - - - - - - -
        -- Oracle Engine Game API

        -- language functions
        this.Get_Runtime.Define( FUNC_INDEX,      Game_API.Index'Access,     2, 2 );
        this.Get_Runtime.Define( FUNC_INDEX_REF,  Game_API.IndexRef'Access,  2, 2 );
        this.Get_Runtime.Define( FUNC_MEMBER,     Game_API.Member'Access,    2, 2 );
        this.Get_Runtime.Define( FUNC_MEMBER_REF, Game_API.MemberRef'Access, 2, 2 );

        -- general functions
        this.Get_Runtime.Define( "assetStats",    Game_API.AssetStats'Access, 0,   0, True );
        this.Get_Runtime.Define( "eval",          Game_API.Eval'Access,       1, 255, True );
        this.Get_Runtime.Define( "GetPref",       Game_API.GetPref'Access,    2,   2, True );
        this.Get_Runtime.Define( "load",          Game_API.Load'Access,       1,   2, True );
        this.Get_Runtime.Define( "print",         Game_API.Print'Access,      1,   1, True );
        this.Get_Runtime.Define( "random",        Game_API.Random'Access,     0,   0, True );
        this.Get_Runtime.Define( "run",           Game_API.Run'Access,        1, 255, True );
        this.Get_Runtime.Define( "SetPref",       Game_API.SetPref'Access,    3,   3, True );

        -- easing functions
        this.Get_Runtime.Define( "QuadraticEaseIn",      Game_API.QuadraticEaseIn'Access,      4, 4, True );
        this.Get_Runtime.Define( "QuadraticEaseOut",     Game_API.QuadraticEaseOut'Access,     4, 4, True );
        this.Get_Runtime.Define( "QuadraticEaseInOut",   Game_API.QuadraticEaseInOut'Access,   4, 4, True );
        this.Get_Runtime.Define( "CubicEaseIn",          Game_API.CubicEaseIn'Access,          4, 4, True );
        this.Get_Runtime.Define( "CubicEaseOut",         Game_API.CubicEaseOut'Access,         4, 4, True );
        this.Get_Runtime.Define( "CubicEaseInOut",       Game_API.CubicEaseInOut'Access,       4, 4, True );
        this.Get_Runtime.Define( "QuarticEaseIn",        Game_API.QuarticEaseIn'Access,        4, 4, True );
        this.Get_Runtime.Define( "QuarticEaseOut",       Game_API.QuarticEaseOut'Access,       4, 4, True );
        this.Get_Runtime.Define( "QuarticEaseInOut",     Game_API.QuarticEaseInOut'Access,     4, 4, True );
        this.Get_Runtime.Define( "QuinticEaseIn",        Game_API.QuinticEaseIn'Access,        4, 4, True );
        this.Get_Runtime.Define( "QuinticEaseOut",       Game_API.QuinticEaseOut'Access,       4, 4, True );
        this.Get_Runtime.Define( "QuinticEaseInOut",     Game_API.QuinticEaseInOut'Access,     4, 4, True );
        this.Get_Runtime.Define( "SineEaseIn",           Game_API.SineEaseIn'Access,           4, 4, True );
        this.Get_Runtime.Define( "SineEaseOut",          Game_API.SineEaseOut'Access,          4, 4, True );
        this.Get_Runtime.Define( "SineEaseInOut",        Game_API.SineEaseInOut'Access,        4, 4, True );
        this.Get_Runtime.Define( "CircularEaseIn",       Game_API.CircularEaseIn'Access,       4, 4, True );
        this.Get_Runtime.Define( "CircularEaseOut",      Game_API.CircularEaseOut'Access,      4, 4, True );
        this.Get_Runtime.Define( "CircularEaseInOut",    Game_API.CircularEaseInOut'Access,    4, 4, True );
        this.Get_Runtime.Define( "ExponentialEaseIn",    Game_API.ExponentialEaseIn'Access,    4, 4, True );
        this.Get_Runtime.Define( "ExponentialEaseOut",   Game_API.ExponentialEaseOut'Access,   4, 4, True );
        this.Get_Runtime.Define( "ExponentialEaseInOut", Game_API.ExponentialEaseInOut'Access, 4, 4, True );
        this.Get_Runtime.Define( "ElasticEaseIn",        Game_API.ElasticEaseIn'Access,        4, 4, True );
        this.Get_Runtime.Define( "ElasticEaseOut",       Game_API.ElasticEaseOut'Access,       4, 4, True );
        this.Get_Runtime.Define( "ElasticEaseInOut",     Game_API.ElasticEaseInOut'Access,     4, 4, True );
        this.Get_Runtime.Define( "BackEaseIn",           Game_API.BackEaseIn'Access,           4, 4, True );
        this.Get_Runtime.Define( "BackEaseOut",          Game_API.BackEaseOut'Access,          4, 4, True );
        this.Get_Runtime.Define( "BackEaseInOut",        Game_API.BackEaseInOut'Access,        4, 4, True );
        this.Get_Runtime.Define( "BounceEaseIn",         Game_API.BounceEaseIn'Access,         4, 4, True );
        this.Get_Runtime.Define( "BounceEaseOut",        Game_API.BounceEaseOut'Access,        4, 4, True );
        this.Get_Runtime.Define( "BounceEaseInOut",      Game_API.BounceEaseInOut'Access,      4, 4, True );

        -- audio functions
        this.Get_Runtime.Define( "PlayMusic", Game_API.PlayMusic'Access, 1, 1, True );
        this.Get_Runtime.Define( "StopMusic", Game_API.StopMusic'Access, 0, 0, True );
        this.Get_Runtime.Define( "PlaySound", Game_API.PlaySound'Access, 1, 1, True );

        -- world functions
        this.Get_Runtime.Define( "LoadWorld",        Game_API.LoadWorld'Access,        1, 1, True );
        this.Get_Runtime.Define( "GetTile",          Game_API.GetTile'Access,          3, 3, True );
        this.Get_Runtime.Define( "GetTileInfo",      Game_API.GetTileInfo'Access,      1, 1, True );
        this.Get_Runtime.Define( "IsFloor",          Game_API.IsFloor'Access,          2, 2, True );
        this.Get_Runtime.Define( "SetLayerProperty", Game_API.SetLayerProperty'Access, 3, 3, True );
        this.Get_Runtime.Define( "SetTile",          Game_API.SetTile'Access,          4, 4, True );

        -- entity functions
        this.Get_Runtime.Define( "Spawn",                 Game_API.Spawn'Access,                 2, 2, True );
        this.Get_Runtime.Define( "SpawnAt",               Game_API.SpawnAt'Access,               3, 4, True );
        this.Get_Runtime.Define( "Destroy",               Game_API.Destroy'Access,               1, 1, True );
        this.Get_Runtime.Define( "EntityExists",          Game_API.EntityExists'Access,          1, 1, True );
        this.Get_Runtime.Define( "NearPlayer",            Game_API.NearPlayer'Access,            1, 1, True );
        this.Get_Runtime.Define( "AddComponent",          Game_API.AddComponent'Access,          2, 3, True );
        this.Get_Runtime.Define( "RemoveComponent",       Game_API.RemoveComponent'Access,       2, 2, True );
        this.Get_Runtime.Define( "RemoveComponentFamily", Game_API.RemoveComponentFamily'Access, 2, 2, True );
        this.Get_Runtime.Define( "Send",                  Game_API.Send'Access,                  2, 3, True );

        -- component functions
        this.Get_Runtime.Define( "CancelScriptTimer", Game_API.CancelScriptTimer'Access, 2, 2, True );
        this.Get_Runtime.Define( "SetScriptTimer",    Game_API.SetScriptTimer'Access,    3, 5, True );

        -- particle functions
        this.Get_Runtime.Define( "ParticleBurst",  Game_API.ParticleBurst'Access,  5, 5, True );
        this.Get_Runtime.Define( "ParticleStream", Game_API.ParticleStream'Access, 6, 7, True );

        -- game functions
        this.Get_Runtime.Define( "PauseGame",  Game_API.PauseGame'Access,  0,   0, True );
        this.Get_Runtime.Define( "ResumeGame", Game_API.ResumeGame'Access, 0,   0, True );
        this.Get_Runtime.Define( "SendView",   Game_API.SendView'Access,   1, 255, True );

        -- Always define 'game' and 'world' to allow access to these objects
        -- from Scribble scripts using the global namespace.
        this.Set_Global( "game", Sessions.GAME_ID );
        this.Set_Global( "world", To_Id_Value( Create_Tagged_Id( World_Tag, 1 ) ) );

        return this;
    end Create_Game_VM;

end Game_Scripting;
