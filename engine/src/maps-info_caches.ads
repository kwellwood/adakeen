--
-- Copyright (c) 2017-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Unchecked_Deallocation;
with Assets.Libraries;                  use Assets.Libraries;
with Center_Arrays;
with Clipping;                          use Clipping;
with Tiles;                             use Tiles;

package Maps.Info_Caches is

    -- A Map_Object is a layered 2D grid of tile ids. It's composed of a stack
    -- of Layers of different types and dimensions. To get and set tile ids in
    -- the map, locations are addressed by coordinates (layer, x, y).
    -- x, y coordinates start at ZERO. The smallest layer number is in the
    -- foreground and the largest layer number is in the background.
    type Map_Info_Cache is new Limited_Object and Map_Listener with private;
    type A_Map_Info_Cache is access all Map_Info_Cache'Class;

    -- Creates a new empty map info cache. The map and library will need to be
    -- set before the cache can be queried.
    function Create_Map_Info_Cache return A_Map_Info_Cache;
    pragma Postcondition( Create_Map_Info_Cache'Result /= null );

    -- Returns a pointer to the info cache's map.
    function Get_Map( this : not null access Map_Info_Cache'Class ) return A_Map;

    -- Returns the tile shape to be used for map collisions at 'col', 'row'. The
    -- shape is calculated as the most dominant shape of all the solid layers
    -- at the given position.
    function Get_Shape( this     : not null access Map_Info_Cache'Class;
                        col, row : Integer ) return Clip_Type;

    -- Returns a pointer to the tile at 'col,row' in layer 'layer', or null if
    -- the tile is not found or the location does not exist.
    function Get_Tile( this     : not null access Map_Info_Cache'Class;
                       layer    : Integer;
                       col, row : Integer ) return A_Tile;

    -- Returns the OR'ed tile bits of all tiles in solid layers at 'col', 'row'.
    function Get_TypeBits( this     : not null access Map_Info_Cache'Class;
                           col, row : Integer ) return Tile_Bits;

    -- Returns True if the tiles at location 'col,row' have all of 'typeBits'.
    function Has_TypeBits( this     : not null access Map_Info_Cache'Class;
                           col, row : Integer;
                           typeBits : Tile_Bits ) return Boolean;

    -- Initializes the cache from a map and its corresponding tile library.
    procedure Initialize( this : not null access Map_Info_Cache'Class;
                          map  : A_Map;
                          lib  : Library_Ptr );

    procedure Delete( this : in out A_Map_Info_Cache );

private

    type Tiles_Array is array (Integer range <>) of A_Tile;
    type A_Tiles_Array is access all Tiles_Array;

    procedure Delete is new Ada.Unchecked_Deallocation( Tiles_Array, A_Tiles_Array );

    package Tile_Layers is new Center_Arrays( A_Tiles_Array, "=" );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Blended_Info is
        record
            shape : Clip_Type := Passive;
            bits  : Tile_Bits := TILE_INERT;
        end record;

    type Blended_Info_Array is array (Integer range <>) of Blended_Info;
    type A_Blended_Info_Array is access all Blended_Info_Array;

    procedure Delete is new Ada.Unchecked_Deallocation( Blended_Info_Array, A_Blended_Info_Array );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Map_Info_Cache is new Limited_Object and Map_Listener with
        record
            map     : A_Map;
            lib     : Library_Ptr;
            layered : Tile_Layers.Center_Array; -- layer-specific info
            blended : A_Blended_Info_Array;
        end record;

    procedure Delete( this : in out Map_Info_Cache );

    procedure Delete_All( this : in out Map_Info_Cache'Class );

    procedure On_Layer_Added( this : not null access Map_Info_Cache; layer : Integer );

    procedure On_Layer_Deleted( this : not null access Map_Info_Cache; layer : Integer );

    procedure On_Layer_Property_Changed( this  : not null access Map_Info_Cache;
                                         layer : Integer;
                                         name  : String );

    procedure On_Layers_Swapped( this   : not null access Map_Info_Cache;
                                 layerA : Integer;
                                 layerB : Integer );

    procedure On_Tile_Changed( this     : not null access Map_Info_Cache;
                               layer    : Integer;
                               col, row : Integer );

    -- Updates the cached blended info (not layer-specific) at 'col', 'row'.
    procedure Update_Blended_Info( this : not null access Map_Info_Cache'Class; col, row : Integer );

    -- Updates all cached blended info (not layer-specific) from the map. When to call this:
    --
    -- * When a layer is added to or removed from the map.
    -- * When the 'solid' property of any layer changes.
    procedure Update_Blended_Info( this : not null access Map_Info_Cache'Class );

    procedure Update_Layer( this : not null access Map_Info_Cache'Class; layer : Integer );

    procedure Update_Layer_At( this     : not null access Map_Info_Cache'Class;
                               layer    : Integer;
                               col, row : Integer );

end Maps.Info_Caches;
