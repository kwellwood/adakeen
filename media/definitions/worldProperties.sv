//
// Standard world property descriptions for editing in Tinker
//

[
    {property: "lib"         , displayName: "Tile Library"  , readonly: true, value: ""}
    {property: "size"        , displayName: "Map Size"      , readonly: true, value: ""}

    {property: "title"       , displayName: "Title"         , value: ""       }
    {property: "preserve"    , displayName: "Preserve State", value: false    }
    {property: "introduction", displayName: "Introduction"  , value: ""       }
    {property: "ambientLight", displayName: "Ambient Light" , value: "#FFFFFF"}
    {property: "music"       , displayName: "Music"         , value: ""       }
    {property: "animated"    , displayName: "Animated Tiles", value: false    }
]
