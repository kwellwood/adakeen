// Filename:    icons/index.txt
// Description: Icons for the Tinker application

// DO NOT change the id numbers for tiles used as entity frames
// the id numbers are stored in the entities that use them
51  entity-pole.png {fillMode: "stretch", borderTop: 4, borderBottom: 4}
200 entity-entrance.png
201 entity-exit.png
202 entity-flagspawner.png
203 entity-swimmessage.png
//204 unused
205 entity-door.png
206 entity-liftpath.png
207 entity-emitter.png
208 entity-light.png
209 entity-sunlight.png

tinker-64.png

undo.png
redo.png

ruler.png
dimensions-16.png
crop.png
play.png
pause_icon.png

scrollbar-h.png {fillMode: "stretch", borderLeft: 4, borderRight: 4}
scrollbar-v.png {fillMode: "stretch", borderTop: 4, borderBottom: 4}

up-white-16.png
down-white-16.png

information.png
properties-black-16.png
folder-open.png
document-block.png
disk-save.png
disk-save-as.png
resize.png
warning.png
import-image.png
import-sv.png
tag-pencil.png
clipping.png
outlines.png
clipboard-white-16.png

enum-open.png
enum-closed.png

accordion-open.png
accordion-closed.png

grid-dark.png
grid-light.png

handle_v.png
handle_h.png

tool-panner-32.png
tool-pointer-32.png
tool-surveyor-32.png
tool-painter-32.png
tool-pattern-32.png
tool-stamper-32.png
tool-terrain-32.png
tool-picker-32.png
tool-eraser-32.png
tool-connector-32.png
tool-spawner-32.png

tool-entrance.png
tool-exit.png
tool-flag.png
tool-pole.png
tool-swimmessage.png
tool-fence.png
tool-lakeshoreh.png
tool-lakeshorev.png
tool-door.png
tool-lockeddoor.png
tool-bridge.png
tool-lift.png
tool-liftpath.png
tool-emitter.png
tool-light.png
tool-sunlight.png
tool-painter-16.png
tool-pattern-16.png
tool-terrain-16.png

terrain-undo.png
terrain-commit.png

cursor-spawner.png        {focusX: 8, focusY:  8}
cursor-connector-a.png    {focusX: 0, focusY:  0}
cursor-connector-b.png    {focusX: 0, focusY:  0}
cursor-connector-b2.png   {focusX: 0, focusY:  0}
cursor-eraser.png         {focusX: 0, focusY:  0}
cursor-pannable.png       {focusX: 8, focusY:  8}
cursor-panning.png        {focusX: 8, focusY:  8}
cursor-painter.png        {focusX: 7, focusY: 25}
cursor-pencil-pattern.png {focusX: 1, focusY: 17}
cursor-picker.png         {focusX: 3, focusY: 15}
cursor-stamper.png        {focusX: 7, focusT: 13}

checkbox.png
checkbox-focus.png
checkbox-hover.png
checkbox-press.png
checkbox-press-focus.png
checkbox-press-hover.png

slider_track.png              {fillMode: "stretchX", borderLeft: 2, borderRight: 2}
slider_handle.png
slider_handle-hover.png
slider_handle-focus.png
slider_handle-focus-hover.png
slider_handle-press.png

border_white1.png           {fillMode: "stretch", borderLeft: 1, borderRight: 1, borderTop: 1, borderBottom: 1}
border_white2.png           {fillMode: "stretch", borderLeft: 2, borderRight: 2, borderTop: 2, borderBottom: 2}
border_top_white1.png       {fillMode: "stretch", borderTop: 1}
border_bottom_white1.png    {fillMode: "stretch", borderBottom: 1}
border_left_white1.png      {fillMode: "stretch", borderLeft: 1}

new-file-white-32.png
open-white-32.png
save-white-32.png
properties-white-32.png
undo-white-32.png
redo-white-32.png

file-16.png
light-on-white-16.png
play-white-16.png
music-white-16.png
grid-white-16.png
shadows-white-16.png
atom-white-16.png
layers-white-16.png
minimize-white-16.png
maximize-white-16.png
openapp-white-16.png
search-white-16.png
delete-white-16.png
selection-16.png
plus-white-16.png
minus-white-16.png
clear-white-16.png
crop-white-16.png
console-16.png
