//
// Retrieved from http://github.com/mattdesl/lwjgl-basics/wiki/ShaderLesson5
//

varying vec4 varyingColor;
varying vec2 varyingTexcoord;

uniform sampler2D al_tex;
uniform bool      al_use_tex;
uniform float     resolution;

// the amount to blur, i.e. how far off center to sample from 
// 1.0 -> blur by one pixel
// 2.0 -> blur by two pixels, etc.
uniform float radius;

// the direction of our blur
// (1.0, 0.0) -> x-axis blur
// (0.0, 1.0) -> y-axis blur
uniform vec2 dir;

void main()
{
    vec2 blur = (radius / resolution) * dir;

    // apply blurring, using a 5-tap filter with predefined gaussian weights
    vec4 sum = vec4(0.0);

    sum += texture2D(al_tex, varyingTexcoord - 2.0 * blur) * 1.0 / 16.0;
    sum += texture2D(al_tex, varyingTexcoord - 1.0 * blur) * 4.0 / 16.0;
    sum += texture2D(al_tex, varyingTexcoord             ) * 6.0 / 16.0;
    sum += texture2D(al_tex, varyingTexcoord + 1.0 * blur) * 4.0 / 16.0;
    sum += texture2D(al_tex, varyingTexcoord + 2.0 * blur) * 1.0 / 16.0;

    gl_FragColor = sum;
}
