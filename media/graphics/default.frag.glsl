
uniform sampler2D al_tex;
uniform bool      al_use_tex;

varying vec4 varyingColor;
varying vec2 varyingTexcoord;

void main()
{
    vec4 tex;
    if (al_use_tex)
    {
        tex = texture2D(al_tex, varyingTexcoord);
    }
    else
    {
        tex = vec4(1.0, 1.0, 1.0, 1.0);
    }

    gl_FragColor = varyingColor * tex;

    // discard all transparent fragments to avoid writing to the depth buffer.
    if (gl_FragColor.a == 0.0)
    {
        discard;
    }
}
