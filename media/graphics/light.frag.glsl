
uniform sampler2D al_tex;
uniform bool      al_use_tex;

uniform float lightRadius;                  // world coordinates
uniform vec4  lightColor;                   // (r, g, b)
uniform float lightAngle;                   // radians (0 to 2*Pi)
uniform float lightArc;                     // radians (0 to 2*Pi)

varying vec4 varyingColor;
varying vec2 varyingTexcoord;

void main()
{
    // light's texture to draw (not currently used)
    // when al_use_tex is true, diffuseColor = texture2D
    // when al_use_tex is false, diffuseColor = white
    vec4 diffuseColor = vec4(float(!al_use_tex)) + (texture2D(al_tex, varyingTexcoord) * float(al_use_tex));

    // light position is in the center of the area being drawn
    vec2 lightPos = vec2(lightRadius, lightRadius);
    
    // the polar coordinates angle of this frag in radians, relative to the light at the center.
    // angle zero is to the right. angle increases counter-clockwise.
    float fragAngle = atan(lightPos.y - varyingTexcoord.y, varyingTexcoord.x - lightPos.x);

    // this is the angular distance of the fragment from the light's center ray (lightAngle). it's
    // used to apply a falloff effect at the edge of the light cone.
    float angleDist = radians(180.0) - abs(abs(fragAngle - lightAngle) - radians(180.0)); 

    // treat all arcs >= 180 degrees as not having a cone shape. this is done by treating every frag
    // as though it is distance 0 from 'lightAngle'.
    angleDist *= float(lightArc < radians(180.0));
    
    // the angular brightness falloff is x^8 for a nice sharp effect
    float coneBrightness = (angleDist * angleDist) / ((lightArc / 2.0) * (lightArc / 2.0));
    diffuseColor.a *= clamp(1.0 - coneBrightness * coneBrightness * coneBrightness * coneBrightness, 0.0, 1.0);

    // distance of the fragment being drawn from the source of the light
    float lightDist = distance(varyingTexcoord, lightPos);

    // light brightness (1.0 = full bright)
    float brightness = clamp(1.0 - (lightDist * lightDist) / (lightRadius * lightRadius), 0.0, 1.0);
    brightness *= brightness;

    // Pre-multiply light color with intensity
    // "N dot L" term also goes here for normal mapping
    vec3 diffuse = (lightColor.rgb * lightColor.a) * 1.0;

    vec3 finalColor = diffuseColor.rgb * diffuse * brightness;
    gl_FragColor = varyingColor * vec4(finalColor, diffuseColor.a);
}
