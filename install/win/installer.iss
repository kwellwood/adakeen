; Keen Galaxy Installer
; Copyright (C) 2014-2019 Kevin Wellwood
;
; This script requires the Inno Setup Preprocessor add-on for Inno Setup

#define appName "Keen Galaxy"
#define keenExe "keen.exe"
#define tinkerExe "tinker.exe"
#define transmogExe "transmog.exe"
#define root AddBackslash(SourcePath) + "..\..\"
#define versionShort GetFileProductVersion( root + "bin\" + keenExe )
#define versionFull GetFileVersion( root + "bin\" + keenExe )

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{0CC3827E-83C2-409A-821D-6F1C2A8FE22E}
AppName={#appName}
AppVerName={#appName} {#versionShort}
AppPublisher=Kevin Wellwood
AppPublisherURL=http://bitbucket.org/kwellwood/keen
AppSupportURL=http://bitbucket.org/kwellwood/keen
AppUpdatesURL=http://bitbucket.org/kwellwood/keen
DefaultDirName={pf32}\{#appName}
DisableDirPage=yes
DefaultGroupName={#appName}
DisableProgramGroupPage=yes
PrivilegesRequired=admin
OutputDir={#root}\dist
OutputBaseFilename=keen-{#versionFull}-installer
Compression=lzma
SolidCompression=yes
UninstallDisplayIcon={app}\{#keenExe}
VersionInfoDescription={#appName} Installer
VersionInfoProductName={#appName} Installer
VersionInfoVersion={#versionFull}
SetupIconFile=thumbsup.ico
WizardSmallImageFile=installer-small.bmp
WizardImageFile=installer-left.bmp

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Types]
Name: "full";   Description: "Full Installation"
Name: "custom"; Description: "Custom Installation"; Flags: iscustom

[Components]
Name: "game";            Description: "Keen Galaxy Game (required)"; Types: full custom; Flags: fixed
Name: "game\media";      Description: "Game Files (required)";       Types: full custom; Flags: fixed
Name: "tinker";          Description: "Tinker Map Editor";           Types: full
Name: "tinker\rawmedia"; Description: "Editable Game Files";         Types: full

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked
Name: "assoc_world"; Description: "{cm:AssocFileExtension,Tinker Map Editor,.world}"; Components: tinker

[Files]
; Keen and Game Files
Source: "{#root}\bin\{#keenExe}";      DestDir: "{app}\";                            Flags: ignoreversion;      Components: game
Source: "{#root}\bin\*.dll";           DestDir: "{app}\";                            Flags: ignoreversion;      Components: game
Source: "{#root}\bin\readme.txt";      DestDir: "{app}\";                            Flags: replacesameversion; Components: game
Source: "{#root}\credits\credits.txt"; DestDir: "{app}\credits\";                    Flags: replacesameversion; Components: game
Source: "{#root}\credits\license\*.*"; DestDir: "{app}\credits\license\";            Flags: ignoreversion replacesameversion; Components: game
Source: "{#root}\media\*.zip";         DestDir: "{commonappdata}\{#appName}\media\"; Flags: replacesameversion; Components: game\media

; Tinker Map Editor
Source: "{#root}\bin\{#tinkerExe}"; DestDir: "{app}\"; Flags: ignoreversion; Components: tinker
Source: "{#root}\bin\{#transmogExe}";  DestDir: "{app}\"; Flags: ignoreversion; Components: tinker

; Editable Game Files
Source: "{#root}\media\graphics\shadowlands.tlz"; DestDir: "{userdocs}\{#appName}\"; Flags: replacesameversion; Components: tinker\rawmedia
Source: "{#root}\media\graphics\world.tlz";       DestDir: "{userdocs}\{#appName}\"; Flags: replacesameversion; Components: tinker\rawmedia
Source: "{#root}\media\worlds\border.world";      DestDir: "{userdocs}\{#appName}\"; Flags: replacesameversion; Components: tinker\rawmedia
Source: "{#root}\media\worlds\bwb.world";         DestDir: "{userdocs}\{#appName}\"; Flags: replacesameversion; Components: tinker\rawmedia
Source: "{#root}\media\worlds\chasm.world";       DestDir: "{userdocs}\{#appName}\"; Flags: replacesameversion; Components: tinker\rawmedia
Source: "{#root}\media\worlds\crystalus.world";   DestDir: "{userdocs}\{#appName}\"; Flags: replacesameversion; Components: tinker\rawmedia
Source: "{#root}\media\worlds\descendents.world"; DestDir: "{userdocs}\{#appName}\"; Flags: replacesameversion; Components: tinker\rawmedia
Source: "{#root}\media\worlds\fireisle.world";    DestDir: "{userdocs}\{#appName}\"; Flags: replacesameversion; Components: tinker\rawmedia
Source: "{#root}\media\worlds\hillville.world";   DestDir: "{userdocs}\{#appName}\"; Flags: replacesameversion; Components: tinker\rawmedia
Source: "{#root}\media\worlds\miragia.world";     DestDir: "{userdocs}\{#appName}\"; Flags: replacesameversion; Components: tinker\rawmedia
Source: "{#root}\media\worlds\oasis.world";       DestDir: "{userdocs}\{#appName}\"; Flags: replacesameversion; Components: tinker\rawmedia
Source: "{#root}\media\worlds\pit.world";         DestDir: "{userdocs}\{#appName}\"; Flags: replacesameversion; Components: tinker\rawmedia
Source: "{#root}\media\worlds\sandyego.world";    DestDir: "{userdocs}\{#appName}\"; Flags: replacesameversion; Components: tinker\rawmedia
Source: "{#root}\media\worlds\shadowlands.world"; DestDir: "{userdocs}\{#appName}\"; Flags: replacesameversion; Components: tinker\rawmedia
Source: "{#root}\media\worlds\slugville.world";   DestDir: "{userdocs}\{#appName}\"; Flags: replacesameversion; Components: tinker\rawmedia

[Icons]
Name: "{group}\{#appName}";                       Filename: "{app}\{#keenExe}";    Components: game
Name: "{group}\Tinker Map Editor";                Filename: "{app}\{#tinkerExe}";  Components: tinker
Name: "{group}\Read Me";                          Filename: "{app}\readme.txt";    Components: game
Name: "{commondesktop}\{#appName}";               Filename: "{app}\{#keenExe}";    Tasks: desktopicon
Name: "{group}\{cm:UninstallProgram,{#appName}}"; Filename: "{uninstallexe}"

[Registry]
Root: HKLM; Subkey: "Software\Classes\KeenGalaxy.AssocFile.world"; ValueType: String; ValueData: "Keen Galaxy World"; Flags: uninsdeletekey
Root: HKLM; Subkey: "Software\Classes\KeenGalaxy.AssocFile.world\DefaultIcon"; ValueType: String; ValueData: "{app}\{#tinkerExe},0"; Flags: uninsdeletekey
Root: HKLM; Subkey: "Software\Classes\KeenGalaxy.AssocFile.world\Shell\Open"; ValueName: Icon; ValueType: String; ValueData: "{app}\{#tinkerExe}"; Flags: uninsdeletekey
Root: HKLM; Subkey: "Software\Classes\KeenGalaxy.AssocFile.world\Shell\Open\Command"; ValueType: String; ValueData: """{app}\{#tinkerExe}"" ""%1"""; Flags: uninsdeletekey
Root: HKLM; Subkey: "Software\Classes\.world"; ValueType: String; ValueData: "KeenGalaxy.AssocFile.world"; Flags: uninsdeletevalue uninsdeletekeyifempty; Tasks: assoc_world

Root: HKLM; Subkey: "Software\KeenGalaxy"; Flags: uninsdeletekey
Root: HKLM; Subkey: "Software\KeenGalaxy\Capabilities"; ValueType: String; ValueName: "ApplicationName"; ValueData: "Tinker Map Editor"; Flags: uninsdeletekey
Root: HKLM; Subkey: "Software\KeenGalaxy\Capabilities"; ValueType: String; ValueName: "ApplicationDescription"; ValueData: "World editor for Keen Galaxy"; Flags: uninsdeletekey
Root: HKLM; Subkey: "Software\KeenGalaxy\Capabilities\FileAssociations"; ValueName: ".world"; ValueType: String; ValueData: "KeenGalaxy.AssocFile.world"; Flags: uninsdeletekey

Root: HKLM; Subkey: "Software\RegisteredApplications"; ValueType: String; ValueName: "KeenGalaxy"; ValueData: "Software\KeenGalaxy\Capabilities"; Flags: uninsdeletevalue uninsdeletekeyifempty

[Run]
Filename: "{app}\{#keenExe}"; Description: "{cm:LaunchProgram,{#appName}}"; Flags: nowait postinstall skipifsilent
