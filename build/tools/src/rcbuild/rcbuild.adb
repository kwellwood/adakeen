--
-- Copyright (c) 2012 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Calendar;                      use Ada.Calendar;
with Ada.Calendar.Formatting;           use Ada.Calendar.Formatting;
with Ada.Calendar.Time_Zones;           use Ada.Calendar.Time_Zones;
with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Exceptions;                    use Ada.Exceptions;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Ada.Text_IO;                       use Ada.Text_IO;
with GNAT.OS_Lib;                       use GNAT.OS_Lib;

procedure RCBUILD is

    type Version_Type is
        record
            major, minor, patch : Natural := 0;
        end record;

    ----------------------------------------------------------------------------

    nl : constant String := ASCII.CR & ASCII.LF;

    constants : constant String :=
        "#define VOS__WINDOWS32      4"                & nl &
        "#define VS_FF_DEBUG         1"                & nl &
        "#define VS_FF_PRERELEASE    2"                & nl &
        "#define VFT_APP             1"                & nl &
        nl &
        "#define STR(s)              #s"               & nl &
        "#define STRINGIFY(s)        STR(s)"           & nl &
        nl &
        "#if DEBUG == 1"                               & nl &
        "#define VI_DEBUG            VS_FF_DEBUG"      & nl &
        "#define VI_DEBUG_STR        "", Debug"""      & nl &
        "#else"                                        & nl &
        "#define VI_DEBUG            0"                & nl &
        "#define VI_DEBUG_STR        """""             & nl &
        "#endif"                                       & nl &
        nl &
        "#if PRERELEASE == 1" & nl &
        "#define VI_PRERELEASE       VS_FF_PRERELEASE" & nl &
        "#define VI_PRERELEASE_STR   "", Prerelease""" & nl &
        "#else"                                        & nl &
        "#define VI_PRERELEASE       0"                & nl &
        "#define VI_PRERELEASE_STR   """""             & nl &
        "#endif"                                       & nl &
        nl &
        "#define VI_COMMENTS_STR     ""Built "" STRINGIFY(BUILD_DATE) "" "" STRINGIFY(BUILD_TIME) VI_DEBUG_STR VI_PRERELEASE_STR ""\0""";

    versionResource : constant String :=
        "1 VERSIONINFO"                                                   & nl &
        "  PRODUCTVERSION VI_PRODUCTVERSION"                              & nl &
        "  FILEVERSION    VI_FILEVERSION"                                 & nl &
        "  FILEFLAGS      (VI_DEBUG|VI_PRERELEASE)"                       & nl &
        "  FILEOS         VOS__WINDOWS32"                                 & nl &
        "  FILETYPE       VFT_APP"                                        & nl &
        "  FILESUBTYPE    0x0L"                                           & nl &
        "BEGIN"                                                           & nl &
        "    BLOCK ""StringFileInfo"""                                    & nl &
        "    BEGIN"                                                       & nl &
        "        BLOCK ""040904b0"""                                      & nl &
        "        BEGIN"                                                   & nl &
        "            VALUE ""ProductName"",      VI_PRODUCTNAME_STR"      & nl &
        "            VALUE ""ProductVersion"",   VI_PRODUCTVERSION_STR"   & nl &
        "            VALUE ""CompanyName"",      VI_COMPANYNAME_STR"      & nl &
        "            VALUE ""FileDescription"",  VI_FILEDESCRIPTION_STR"  & nl &
        "            VALUE ""FileVersion"",      VI_FILEVERSION_STR"      & nl &
        "            VALUE ""InternalName"",     VI_INTERNALNAME_STR"     & nl &
        "            VALUE ""OriginalFilename"", VI_ORIGINALFILENAME_STR" & nl &
        "            VALUE ""LegalCopyright"",   VI_LEGALCOPYRIGHT_STR"   & nl &
        "            VALUE ""Comments"",         VI_COMMENTS_STR"         & nl &
        "        END"                                                     & nl &
        "    END"                                                         & nl &
        "    BLOCK ""VarFileInfo"""                                       & nl &
        "    BEGIN"                                                       & nl &
        "        VALUE ""Translation"", 0x409, 1200"                      & nl &
        "    END"                                                         & nl &
        "END";

    ----------------------------------------------------------------------------

    function To_Boolean( str : String ) return Boolean is
        value : constant String := To_Lower( str );
    begin
        return value = "true" or else value = "yes" or else value = "1";
    end To_Boolean;

    ----------------------------------------------------------------------------

    function To_Version( str : String ) return Version_Type is
        result : Version_Type;
        pos    : Integer;
        comma  : Integer;
    begin
        -- major
        pos := str'First;
        comma := Index( str, ".", pos );
        if comma > pos then
            result.major := Natural'Value( str(pos..comma-1) );
        elsif comma < pos then
            result.major := Natural'Value( str(pos..str'Last) );
            return result;
        else
            return result;
        end if;

        -- minor
        pos := comma + 1;
        comma := Index( str, ".", pos );
        if comma > pos then
            result.minor := Natural'Value( str(pos..comma-1) );
        elsif comma < pos then
            result.minor := Natural'Value( str(pos..str'Last) );
            return result;
        else
            return result;
        end if;

        -- patch
        pos := comma + 1;
        comma := Index( str, ".", pos );
        if comma > pos then
            result.patch := Natural'Value( str(pos..comma-1) );
        elsif comma < pos then
            result.patch := Natural'Value( str(pos..str'Last) );
            return result;
        else
            return result;
        end if;

        return result;
    end To_Version;

    ----------------------------------------------------------------------------

    function To_String( ver : Version_Type ) return String is
    begin
        return Trim( Natural'Image( ver.major ), Left ) & ',' &
               Trim( Natural'Image( ver.minor ), Left ) & ',' &
               Trim( Natural'Image( ver.patch ), Left ) & ',' &
               "BUILD_REV";
    end To_String;

    ----------------------------------------------------------------------------

    function Make_VI_PRODUCTVERSION( ver : Version_Type ) return String is
    begin
        return Trim( Natural'Image( ver.major ), Left ) & '.' &
               Trim( Natural'Image( ver.minor ), Left ) & '.' &
               Trim( Natural'Image( ver.patch ), Left );
    end Make_VI_PRODUCTVERSION;

    ----------------------------------------------------------------------------

    function Make_VI_FILEVERSION( ver : Version_Type ) return String is
    begin
        return """" &
               Trim( Natural'Image( ver.major ), Left ) & '.' &
               Trim( Natural'Image( ver.minor ), Left ) & '.' &
               Trim( Natural'Image( ver.patch ), Left ) &
               " Rev "" STRINGIFY(BUILD_REV) ""\0""";
    end Make_VI_FILEVERSION;

    ----------------------------------------------------------------------------

    companyName,
    legalCopyright,
    productName,
    originalFilename,
    internalName,
    fileDescription : Unbounded_String;
    productVersion,
    fileVersion     : Version_Type;

    ----------------------------------------------------------------------------

    -- Read Version File
    procedure Read_Version_File( filename : STRING ) is
        ft : File_Type;
    begin
        Open( ft, In_File, filename );
        if not Is_Open( ft ) then
            return;
        end if;
        while not End_Of_File( ft ) loop
            declare
                line  : constant String := Trim( Get_Line( ft ), Both );
                name  : constant String := To_Upper( line(line'First..Index( line, " " ) - 1) );
                value : constant String := Trim( line(Index( line, " " )..line'Last), Left );
            begin
                if name = "COMPANYNAME" then
                    companyName := To_Unbounded_String( value );
                elsif name = "LEGALCOPYRIGHT" then
                    legalCopyright := To_Unbounded_String( value );
                elsif name = "PRODUCTNAME" then
                    productName := To_Unbounded_String( value );
                elsif name = "PRODUCTVERSION" then
                    productVersion := To_Version( value );
                elsif name = "ORIGINALFILENAME" then
                    originalFilename := To_Unbounded_String( value );
                elsif name = "INTERNALNAME" then
                    internalName := To_Unbounded_String( value );
                elsif name = "FILEDESCRIPTION" then
                    fileDescription := To_Unbounded_String( value );
                elsif name = "FILEVERSION" then
                    fileVersion :=  To_Version( value );
                end if;
            end;
        end loop;
        Close( ft );
    exception
        when others =>
            if Is_Open( ft ) then
                Close( ft );
            end if;
            raise Constraint_Error with "Error reading version file";
    end Read_Version_File;

    ----------------------------------------------------------------------------

    procedure Read_Arguments is
        arg : INTEGER := 3;
    begin
        while arg < Argument_Count loop
            if Argument( arg ) = "-D" then
                if arg < Argument_Count then
                    if Index( Argument( arg + 1 ), "=" ) > 1 then
                        declare
                            argc  : constant String := Argument( arg + 1 );
                            name  : constant String := To_Upper( argc(argc'First..Index( argc, "=" )-1) );
                            value : constant String := argc(Index( argc, "=" )+1..argc'Last);
                        begin
                            if name = "PRODUCTVERSION" then
                                productVersion := To_Version( value );
                            elsif name = "FILEVERSION" then
                                fileVersion := To_Version( value );
                            end if;
                            arg := arg + 2;
                        end;
                    else
                        raise Constraint_Error with "Expected name=value following '-D'";
                    end if;
                else
                    raise Constraint_Error with "Expected argument following '-D'";
                end if;
            else
                raise Constraint_Error with "Unrecognized argument '" & Argument( arg ) & "'";
            end if;
        end loop;
    end Read_Arguments;

    ---------------------------------------------------------------------------

    -- Write Version Resource
    procedure Write_Resource_File( filename : STRING ) is
        ft      : File_Type;
        success : Boolean;
    begin
        Create( ft, Out_File, filename );
        if not Is_Open( ft ) then
            return;
        end if;
        Put_Line( ft, constants );
        Put_Line( ft, "" );
        Put_Line( ft, "// Auto-Generated Version Info -------------------------------------------------" );
        Put_Line( ft, "#define VI_PRODUCTVERSION       " & To_String( productVersion ) );
        Put_Line( ft, "#define VI_FILEVERSION          " & To_String( fileVersion ) );
        Put_Line( ft, "#define VI_PRODUCTNAME_STR      """ & To_String( productName ) & "\0""" );
        Put_Line( ft, "#define VI_PRODUCTVERSION_STR   """ & Make_VI_PRODUCTVERSION( productVersion) & "\0""" );
        Put_Line( ft, "#define VI_COMPANYNAME_STR      """ & To_String( companyName ) & "\0""" );
        Put_Line( ft, "#define VI_FILEDESCRIPTION_STR  """ & To_String( fileDescription ) & "\0""" );
        Put_Line( ft, "#define VI_FILEVERSION_STR      " & Make_VI_FILEVERSION( fileVersion) );
        Put_Line( ft, "#define VI_INTERNALNAME_STR     """ & To_String( internalName ) & "\0""" );
        Put_Line( ft, "#define VI_ORIGINALFILENAME_STR """ & To_String( originalFilename ) & "\0""" );
        Put_Line( ft, "#define VI_LEGALCOPYRIGHT_STR   """ & To_String( legalCopyright ) & "\0""" );
        Put_Line( ft, "// -----------------------------------------------------------------------------" );
        Put_Line( ft, "" );
        Put_Line( ft, versionResource );
        Close( ft );
    exception
        when others =>
            if Is_Open( ft ) then
                Close( ft );
            end if;
            Delete_File( filename, success );
            raise Constraint_Error with "Error writing resource file";
    end Write_Resource_File;

    ----------------------------------------------------------------------------

    inFilename,
    outFilename : Unbounded_String;
begin
    if Argument_Count >= 2 then
        inFilename := To_Unbounded_String( Argument( 1 ) );
        outFilename := To_Unbounded_String( Argument( 2 ) );
    else
        -- unexpected argument count for building
        Put_Line( "rcbuild: Not enough arguments" );
        GNAT.OS_Lib.OS_Exit( 1 );
    end if;

    Read_Version_File( To_String( inFilename ) );
    Read_Arguments;
    Write_Resource_File( To_String( outFilename ) );

exception
    when e : others =>
        Put_Line( "rcbuild: Error: " & Exception_Message( e ) );
        GNAT.OS_Lib.OS_Exit( 1 );
end RCBUILD;
