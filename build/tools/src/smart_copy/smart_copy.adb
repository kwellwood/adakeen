--
-- Copyright (c) 2012 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Exceptions;                    use Ada.Exceptions;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Ada.Text_IO;
with GNAT.Directory_Operations;         use GNAT.Directory_Operations;
with GNAT.Directory_Operations.Iteration;
 use GNAT.Directory_Operations.Iteration;
with GNAT.OS_Lib;                       use GNAT.OS_Lib;

procedure Smart_Copy is

    -- Raised by Copy_Any on a copy operation failure.
    Copy_Error : exception;

    ----------------------------------------------------------------------------

    -- Determines if two files are identical based on name and length. A result
    -- of False is definitive, but a result of True is only an indication that
    -- the files may be identical.
    function Identical_Files( path1, path2 : String ) return Boolean is

        ------------------------------------------------------------------------

        function File_Length( path : String ) return Long_Integer is
            fd  : File_Descriptor;
            len : Long_Integer := -1;
        begin
            fd := Open_Read( path, Binary );
            if fd /= Invalid_FD then
                len := File_Length( fd );
                Close( fd );
            end if;
            return len;
        end File_Length;

        ------------------------------------------------------------------------

    begin
        return File_Length( path1 ) = File_Length( path2 );
    end Identical_Files;

    ----------------------------------------------------------------------------

    -- Returns the name of a file or directory without any parent directory info.
    function Get_Pathname( path : String ) return String is
        first : Integer;
        last  : Integer := path'Last;
    begin
        -- ignore any trailing slash(es)
        while last >= path'First and then (path(last) = '/' or else path(last) = '\') loop
            last := last - 1;
        end loop;

        first := last;
        while first >= path'First and then path(first) /= '/' and then path(first) /= '\' loop
            first := first - 1;
        end loop;

        return path(first+1..last);
    end Get_Pathname;

    ----------------------------------------------------------------------------

    function Recursive_Make_Dir( path : String ) return Boolean is
        pos : Integer;
    begin
        if path'Length > 0 then
            if not GNAT.OS_Lib.Is_Directory( path ) then
                -- attempt to create 'path' as given
                GNAT.Directory_Operations.Make_Dir( path );
            end if;
            return True;
        else
            return False;
        end if;
    exception
        when Directory_Error =>
            -- failed, try to create its parent directory first
            pos := path'Last;
            -- skip any trailing directory separators
            while pos > path'First and then (path(pos) = '/' or else path(pos) = '\') loop
                pos := pos - 1;
            end loop;
            -- skip to the last directory separator on the path and discard
            -- everything after it
            while pos > path'First and then
                  path(pos) /= '/' and then
                  path(pos) /= '\'
            loop
                if path(pos) = ':' then
                    -- found the drive letter
                    return False;
                end if;
                pos := pos - 1;
            end loop;
            if Recursive_Make_Dir( path(path'First..pos) ) then
                -- try to make the child directory again
                begin
                    GNAT.Directory_Operations.Make_Dir( path );
                    return True;
                exception
                    when Directory_Error =>
                        return False;
                end;
            else
                return False;
            end if;
    end Recursive_Make_Dir;

    ----------------------------------------------------------------------------

    procedure Recursive_Make_Dir( path : String ) is
    begin
        if not Recursive_Make_Dir( path ) then
            raise Directory_Error with "Unable to make dir " & path;
        end if;
    end Recursive_Make_Dir;

    ----------------------------------------------------------------------------

    -- 'src' must be a file.
    -- 'dst' can be a file or a directory. if 'dst' is a directory, the filename
    --       of 'src' will be preserved.
    --
    -- the copy will not be performed if the destination file exists and is
    -- identical.
    function Copy_File( src, dst : String ) return Boolean is
        destpath : Unbounded_String := To_Unbounded_String( dst );
        success  : Boolean;
    begin
        if Is_Directory( dst ) then
            -- copying to a directory, preserve the source's filename
            destpath := To_Unbounded_String( Normalize_Pathname( dst & Directory_Separator & Get_Pathname( src ) ) );
        else
            -- copying to a specific file
            destpath := To_Unbounded_String( Normalize_Pathname( dst ) );
        end if;

        if Is_Regular_File( dst ) then
            -- the destination file already exists
            if Identical_Files( src, To_String( destpath ) ) then
                --Ada.Text_IO.Put_Line( "Copy_File( " & src & " -> " & To_String( destpath ) & " ) : Identical" );
                return True;
            end if;
        end if;

        --Ada.Text_IO.Put_Line( "Copy_File( " & src & " -> " & To_String( destpath ) & " )" );
        Copy_File( src, To_String( destpath ), success, Overwrite, Full );
        if not success then
            Ada.Text_IO.Put_Line( "smart_copy: Unable to copy " & src & " to " & To_String( destpath ) );
        end if;
        return success;
    end Copy_File;

    ----------------------------------------------------------------------------

    -- 'src' must be a wildcard pattern for matching filenames. (ex: mydir/*.bmp)
    -- 'dst' must be a directory.
    function Copy_Files( src, dst : String ) return Boolean is
        success : Boolean := True;

        ------------------------------------------------------------------------

        procedure Copy_Action( path : String; index : Positive; quit : in out Boolean ) is
            pragma Unreferenced( index );
        begin
           quit := not Copy_File( path, dst );
           if quit then
               success := False;
           end if;
        end Copy_Action;

        ------------------------------------------------------------------------

        procedure Iterate is new Wildcard_Iterator( Copy_Action );
    begin
        Iterate( src );
        return success;
    end Copy_Files;

    ----------------------------------------------------------------------------

    -- 'src' may be a file or a directory. if it's a directory, the contents of
    --       it will be copied into the destination directory.
    -- 'dst' must be a directory.
    --
    -- hidden files/folders in the tree (names beginning with a dot) are ignored.
    function Copy_Dir( src, dst : String ) return Boolean is
        success : Boolean := True;

        ------------------------------------------------------------------------

        procedure Copy_Action( path : String; index : Positive; quit : in out Boolean ) is
            pragma Unreferenced( index );
            name : String := Get_Pathname( path );
        begin
            if Is_Directory( path ) and then (name'Length > 0 and then name(name'First) /= '.') then
                begin
                    Recursive_Make_Dir( dst & Directory_Separator & name );
                    --Ada.Text_IO.Put_Line( "Recursive_Make_Dir( " & dst & Directory_Separator & name & " )" );
                exception
                    when Directory_Error =>
                        --Ada.Text_IO.Put_Line( "Recursive_Make_Dir( " & dst & Directory_Separator & name & " ) : Exists" );
                        null;
                end;
            end if;
            quit := not Copy_Dir( path, dst & Directory_Separator & name );
            if quit then
                success := False;
            end if;
        end Copy_Action;
        procedure Iterate is new Wildcard_Iterator( Copy_Action );

        ------------------------------------------------------------------------

        srcname : String := Get_Pathname( src );
    begin
        if srcname'Length > 0 and then srcname(srcname'First) = '.' then
            -- don't copy hidden stuff
            return True;
        end if;

        if Is_Directory( src ) then
            Iterate( Normalize_Pathname( src & Directory_Separator & "*" ) );
            return success;
        else
            return Copy_File( src, dst );
        end if;
    end Copy_Dir;

    ----------------------------------------------------------------------------

    -- raises Copy_Error if an error occurs and the application should abort.
    procedure Copy_Any( src, dst : String ) is
    begin
        if Is_Regular_File( src ) then
            -- Copy the source file to the destination
            if not Copy_File( src, dst ) then
                -- specific failed file already printed out
                raise Copy_Error with "Unable to copy file";
            end if;
        elsif Is_Directory( src ) then
            -- Recursively copy the source directory to the destination directory
            if Is_Directory( dst ) or else not Is_Regular_File( dst ) then
                begin
                    -- create the destination directory if it doesn't exist
                    Recursive_Make_Dir( dst & Directory_Separator & Get_Pathname( src ) );
                exception
                    when Directory_Error =>
                        null;
                end;
                if not Copy_Dir( src,
                                 Normalize_Pathname( dst &
                                                     Directory_Separator &
                                                     Get_Pathname( src ) ) ) then
                    -- specific failure already printed out
                    raise Copy_Error with "Unable to copy directory";
                end if;
            else
                raise Copy_Error with "Destination is not a directory";
            end if;
        elsif Index( src, "*" ) > Index( src, "" & GNAT.Os_Lib.Directory_Separator ) then
            if Is_Directory( dst ) then
                if not Copy_Files( src, Normalize_Pathname( dst ) ) then
                    -- specific failed file already printed out
                    raise Copy_Error with "Unable to complete copy of " & src & " to " & dst;
                end if;
            else
                raise Copy_Error with "Destination is not a directory";
            end if;
        else
            raise Copy_Error with "File or directory " & src & " not found";
        end if;
    end Copy_Any;

    ----------------------------------------------------------------------------

begin
    if Argument_Count = 2 then
        -- may raise Copy_Error
        Copy_Any( Argument( 1 ), Argument( 2 ) );
    elsif Argument_Count > 2 then
        if not Is_Regular_File( Argument( Argument_Count ) ) then
            if not Is_Directory( Argument( Argument_Count ) ) then
                begin
                    -- create the destination directory if it doesn't exist
                    Recursive_Make_Dir( Argument( Argument_Count ) );
                exception
                    when Directory_Error =>
                        raise Copy_Error with "Unable to create " & Argument( Argument_Count );
                end;
            end if;
            for i in 1..(Argument_Count - 1) loop
                Copy_Any( Argument( i ), Argument( Argument_Count ) );
            end loop;
        else
            raise Copy_Error with "Destination is not a directory";
        end if;
    else
        raise Copy_Error with "Invalid number of arguments";
    end if;
exception
    when e : Copy_Error =>
        Ada.Text_IO.Put_Line( "smart_copy: " & Exception_Message( e ) );
        OS_Exit( 1 );
    when e : others =>
        Ada.Text_IO.Put_Line( "smart_copy: Exception: " & Exception_Message( e ) );
        OS_Exit( 1 );
end Smart_Copy;
