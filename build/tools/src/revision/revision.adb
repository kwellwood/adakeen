--
-- Copyright (c) 2012-2013 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Exceptions;                    use Ada.Exceptions;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Ada.Text_IO;                       use Ada.Text_IO;
with GNAT.OS_Lib;                       use GNAT.OS_Lib;

procedure REVISION is

#if WINDOWS then
    SEP : constant Character := ';';
    EXE : constant String := ".exe";
#else
    SEP : constant Character := ':';
    EXE : constant String := "";
#end if;

    ---------------------------------------------------------------------------

    function Find_On_Path( name : String ) return String is
        path  : String_Access := Getenv( "PATH" );
        first,
        last  : Integer;
    begin
        if name'Length = 0 then
            return "";
        elsif Is_Absolute_Path( name ) then
            return name;
        end if;

        first := path'First;
        last := path'First;

        while last <= path'Last + 1 loop
            if last = path'Last + 1 or else path(last) = SEP then
                declare
                    pth : constant String := Normalize_Pathname( path(first..last-1) &
                                                                 Directory_Separator & name & EXE );
                begin
                    if Is_Regular_File( pth ) then
                        Free( path );
                        return pth;
                    end if;
                end;
                first := last + 1;
            end if;
            last := last + 1;
        end loop;

        Free( path );
        return name;
    end Find_On_Path;

    ---------------------------------------------------------------------------

    fd         : File_Descriptor;
    returnCode : Integer;
    rev        : Integer := 0;
    tmpName    : Temp_File_Name;
    success    : Boolean;
    len        : Integer;
    result     : String_Access;
begin
    if Argument_Count = 1 then

        declare
            exePath : constant String := Find_On_Path( "git" );
            args    : Argument_List(1..6);
        begin
            if exePath'Length < 3 then
                Put_Line( "revision: git not found on path" );
                OS_Exit( 1 );
            end if;
            args(1) := new String'("-C");
            args(2) := new String'(Argument( 1 ));
            args(3) := new String'("describe");
            args(4) := new String'("--long");
            args(5) := new String'("--tags");
            args(6) := new String'("--always");
            Create_Temp_File( fd, tmpName );
            Spawn( exePath, args, fd, returnCode );
            Close( fd );
            for i in args'Range loop
                Free( args(i) );
            end loop;
        end;

        fd := Open_Read( tmpName, Text );
        if fd /= Invalid_FD then
            len := Integer(File_Length( fd ));
            if len > 0 then
                result := new String(1..len);
                len := Read( fd, result(result'First)'Address, len );
                if returnCode = 0 then
                    declare
                        first : Integer := result.all'Last + 1;
                        last  : Integer := result.all'Last;
                    begin
                        for i in reverse result.all'Range loop
                            if result(i) = '-' and then i > result.all'First then
                                last := i - 1;
                                for j in reverse result.all'First..last loop
                                    if result.all(j) = '-' then
                                        first := j + 1;
                                        exit;
                                    elsif not Is_Digit( result(j) ) then
                                        raise Constraint_Error;
                                    end if;
                                end loop;
                                exit when first <= last;
                            end if;
                        end loop;
                        if first > last then
                            raise Constraint_Error;
                        end if;

                        rev := Integer'Value( result.all(first..last) );
                    exception
                        when others =>
                            Put_Line( "revision: Error parsing '" & result.all & "'" );
                            Free( result );
                            Close( fd );
                            Delete_File( tmpName, success );
                            OS_Exit( 1 );
                    end;
                else
                    Put_Line( "revision: Error executing git (returned" & returnCode'Img & ")" );
                    Put_Line( result.all );
                    Free( result );
                    Close( fd );
                    Delete_File( tmpName, success );
                    OS_Exit( 1 );
                end if;
                Free( result );
            else
                Put_Line( "revision: Failed to execute git" );
                Close( fd );
                Delete_File( tmpName, success );
                OS_Exit( 1 );
            end if;
            Close( fd );
        else
            Put_Line( "revision: Failed to read output from git" );
            Delete_File( tmpName, success );
            OS_Exit( 1 );
        end if;

        Delete_File( tmpName, success );
    else
        Put_Line( "revision: Incorrect number of arguments" );
        OS_Exit( 1 );
    end if;

    Put( Trim( Integer'Image( rev ), Left ) );
exception
    when e : others =>
        Put_Line( "revision: Exception: " & Exception_Name( e ) &
                  " -- " & Exception_Message( e ) );
        OS_Exit( 1 );
end REVISION;
