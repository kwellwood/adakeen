--
-- Copyright (c) 2012-2014 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Containers.Indefinite_Doubly_Linked_Lists;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Ada.Text_IO;                       use Ada.Text_IO;
with GNAT.Directory_Operations;         use GNAT.Directory_Operations;
with GNAT.Directory_Operations.Iteration;
 use GNAT.Directory_Operations.Iteration;
with GNAT.OS_Lib;                       use GNAT.OS_Lib;

procedure MKBUNDLE is

    package String_Lists is new Ada.Containers.Indefinite_Doubly_Linked_Lists( String, "=" );
    use String_Lists;

    ----------------------------------------------------------------------------

    procedure Display_Error( msg : String ) is
    begin
        Ada.Text_IO.Put_Line( "mkbundle: " & msg );
        OS_Exit( 1 );
    end Display_Error;

    ----------------------------------------------------------------------------

    procedure Create_Dir( path : String; ignoreFailure : Boolean := False ) is
    begin
        Make_Dir( path );
    exception
        when Directory_Error =>
            if not ignoreFailure then
                Display_Error( "Error creating '" & path & "'" );
            end if;
    end Create_Dir;

    ----------------------------------------------------------------------------

    -- Returns the name of a file or directory without any parent directory info.
    function Get_Pathname( path : String ) return String is
        first : Integer;
        last  : Integer := path'Last;
    begin
        -- ignore any trailing slash(es)
        while last >= path'First and then (path(last) = '/' or else path(last) = '\') loop
            last := last - 1;
        end loop;

        first := last;
        while first >= path'First and then path(first) /= '/' and then path(first) /= '\' loop
            first := first - 1;
        end loop;

        return path(first+1..last);
    end Get_Pathname;

    ----------------------------------------------------------------------------

    -- 'src' must be a file.
    -- 'dst' can be a file or a directory. if 'dst' is a directory, the filename
    --       of 'src' will be preserved.
    function Copy_File( src, dst : String ) return Boolean is
        destpath : Unbounded_String := To_Unbounded_String( dst );
        success  : Boolean;
    begin
        if Is_Directory( dst ) then
            -- copying to a directory, preserve the source's filename
            destpath := To_Unbounded_String( Normalize_Pathname( dst & Directory_Separator & Get_Pathname( src ) ) );
        else
            -- copying to a specific file
            destpath := To_Unbounded_String( Normalize_Pathname( dst ) );
        end if;

        Copy_File( src, To_String( destpath ), success, Overwrite, Full );
        return success;
    end Copy_File;

    ----------------------------------------------------------------------------

    -- 'src' may be a file or a directory. if it's a directory, the contents of
    --       it will be copied into the destination directory.
    -- 'dst' must be a directory.
    --
    -- hidden files/folders in the tree (names beginning with a dot) are ignored.
    function Copy_Dir( src, dst : String ) return Boolean is
        success : Boolean := True;

        ------------------------------------------------------------------------

        procedure Copy_Action( path : String; index : Positive; quit : in out Boolean ) is
            pragma Unreferenced( index );
            name : String := Get_Pathname( path );
        begin
            if Is_Directory( path ) and then (name'Length > 0 and then name(name'First) /= '.') then
                Create_Dir( dst & Directory_Separator & name, ignoreFailure => True );
            end if;
            quit := not Copy_Dir( path, dst & Directory_Separator & name );
            if quit then
                success := False;
            end if;
        end Copy_Action;
        procedure Iterate is new Wildcard_Iterator( Copy_Action );

        ------------------------------------------------------------------------

        srcname : String := Get_Pathname( src );
    begin
        if srcname'Length > 0 and then srcname(srcname'First) = '.' then
            -- don't copy hidden stuff
            return True;
        end if;

        if Is_Directory( src ) then
            Iterate( Normalize_Pathname( src & Directory_Separator & "*" ) );
            return success;
        else
            return Copy_File( src, dst );
        end if;
    end Copy_Dir;

    ----------------------------------------------------------------------------

    procedure Display_Instructions is
    begin
        Ada.Text_IO.New_Line;
        Ada.Text_IO.Put_Line( "Usage: mkbundle exepath [dest_dir] [options]" );
        Ada.Text_IO.New_Line;
        Ada.Text_IO.Put_Line( "Options:" );
        Ada.Text_IO.Put_Line( "  -n bundlename   Specifies a name for the bundle" );
        Ada.Text_IO.Put_Line( "  -s bundlesig    Specifies a 4 character Mac OS bundle signature" );
        Ada.Text_IO.Put_Line( "  -v version      Sets short application version string (default: 1.0)" );
        Ada.Text_IO.Put_Line( "  -V long_version Sets the whole application version string" );
        Ada.Text_IO.Put_Line( "  -p plistpath    Specifies the Info.plist file with remaining keys" );
        Ada.Text_IO.Put_Line( "  -e              Embeds the Allegro framework into the bundle" );
        Ada.Text_IO.Put_Line( "  -r respath      Copies a path to the bundle's Resources" );
        Ada.Text_IO.New_Line;
    end Display_Instructions;

    ----------------------------------------------------------------------------

    function Get_Basename( path : String ) return String is
        fslash : Integer := Index( path, "/", Backward );
        bslash : Integer := Index( path, "\", Backward );
        pos    : Integer := Integer'Max( fslash, bslash );
        first  : Integer := path'First;
        last   : Integer := path'Last;
    begin
        if pos >= path'First and then pos < path'Last then
            first := pos + 1;
        end if;
        pos := Index( path, ".", Backward );
        if pos > first then
            last := pos - 1;
        end if;
        return path(first..last);
    end Get_Basename;

    ----------------------------------------------------------------------------

    execPath,
    destPath,
    bundleName,
    bundleSig,
    ver,
    longVer,
    plistPath,
    plistKeys  : Unbounded_String;
    embed      : Boolean;
    resources  : String_Lists.List;
    arg        : Integer;
begin
    if Argument_Count < 1 then
        Display_Instructions;
        OS_Exit( 1 );
    end if;

    execPath := To_Unbounded_String( Normalize_Pathname( Argument( 1 ) ) );
    if not Is_Readable_File( To_String( execPath ) ) then
        Display_Error( "File '" & Argument( 1 ) & "' not found." );
    end if;

    bundleName := To_Unbounded_String( Get_Basename( To_String( execpath ) ) );
    destPath   := To_Unbounded_String( Normalize_Pathname( "." ) );
    ver        := To_Unbounded_String( "1.0" );
    embed      := False;
    arg        := 2;

    while arg <= Argument_Count loop

        -- destination dir
        if arg = 2 and then Head( Argument( arg ), 1 ) /= "-" then
            destPath := To_Unbounded_String( Normalize_Pathname( Argument( arg ) ) );
            if not Is_Directory( To_String( destPath ) ) then
                Display_Error( "Directory '" & Argument( arg ) & "' not found." );
            end if;
            arg := arg + 1;

        -- bundle name
        elsif Argument( arg ) = "-n" then
            if arg >= Argument_Count then
                Display_Error( "Expected bundlename" );
            end if;
            bundleName := To_Unbounded_String( Argument( arg + 1 ) );
            arg := arg + 2;

        -- bundle signature
        elsif Argument( arg ) = "-s" then
            if arg >= Argument_Count then
                Display_Error( "Expected bundlesig" );
            end if;
            bundleSig := To_Unbounded_String( Head( Argument( arg + 1 ), 4, ' ' ) );
            arg := arg + 2;

        -- app version
        elsif Argument( arg ) = "-v" then
            if arg >= Argument_Count then
                Display_Error( "Expected version" );
            end if;
            ver := To_Unbounded_String( Argument( arg + 1 ) );
            arg := arg + 2;

        -- long app version
        elsif Argument( arg ) = "-V" then
            if arg >= Argument_Count then
                Display_Error( "Expected long_version" );
            end if;
            longVer := To_Unbounded_String( Argument( arg + 1 ) );
            arg := arg + 2;

        -- Info.plist Path
        elsif Argument( arg ) = "-p" then
            if arg >= Argument_Count then
                Display_Error( "Expected Info.plist path" );
            end if;
            plistPath := To_Unbounded_String( Normalize_Pathname( Argument( arg + 1 ) ) );
            if not Is_Readable_File( To_String( plistPath ) ) then
                Display_Error( "File '" & Argument( arg + 1 ) & "' not found" );
            end if;
            arg := arg + 2;

        -- embed allegro framework
        elsif Argument( arg ) = "-e" then
            embed := True;
            if not Is_Readable_File( "/Developer/Tools/pbxcp" ) then
                Display_Error( "Apple Developer Tools required to embed Allegro." );
            elsif not Is_Readable_File( "/Library/Frameworks/Allegro.framework/Resources/Embeddable" ) then
                Display_Error( "Allegro embeddable framework not found." );
            end if;
            arg := arg + 1;

        -- add path to resources
        elsif Argument( arg ) = "-r" then
            if arg >= Argument_Count then
                Display_Error( "Expected respath" );
            end if;
            if not Is_Directory( Normalize_Pathname( Argument( arg + 1 ) ) ) and then
               not Is_Readable_File( Normalize_Pathname( Argument( arg + 1 ) ) )
            then
                Display_Error( "Path '" & Argument( arg + 1 ) & "' not found." );
            end if;
            resources.Append( Normalize_Pathname( Argument( arg + 1 ) ) );
            arg := arg + 2;

        else
            Display_Error( "Unrecognized argument." );

        end if;
    end loop;

    if Length( longVer ) = 0 then
        longVer := ver;
    end if;

    if Length( plistPath ) = 0 then
        Display_Error( "Missing -p argument" );
    end if;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Read the plist file
    declare
        ft : Ada.Text_IO.File_Type;
    begin
        if Length( plistPath ) > 0 then
            Open( ft, In_File, To_String( plistPath ) );
            while not End_Of_File( ft ) loop
                Append( plistKeys, Get_Line( ft ) & ASCII.LF );
            end loop;
            Close( ft );
        end if;
    exception
        when others =>
            null;
    end;

    declare
        bundlePath : constant String := To_String( destPath ) & Directory_Separator & To_String( bundleName ) & ".app";
        success    : Boolean;
    begin
        -- check requirements
        if Is_Directory( bundlePath ) then
            Display_Error( "Bundle '" & bundlePath & "' already exists." );
        end if;

        -- create directory structure
        Create_Dir( bundlePath );
        Create_Dir( bundlePath & "/Contents" );
        Create_Dir( bundlePath & "/Contents/MacOS" );
        Create_Dir( bundlePath & "/Contents/Resources" );
        Create_Dir( bundlePath & "/Contents/Frameworks" );

        -- write PkgInfo
        declare
            ft : File_Type;
        begin
            Create( ft, Name => bundlePath & "/Contents/PkgInfo" );
            Put( ft, "APPL" & To_String( bundleSig ) );
            Close( ft );
        exception
            when others =>
                Close( ft );
                Display_Error( "Error writing '" & bundlePath & "/Contents/PkgInfo'" );
        end;

        -- write Info.plist
        declare
            ft : File_Type;
        begin
            Create( ft, Name => bundlePath & "/Contents/Info.plist" );
            Put_Line( ft, "<?xml version=""1.0"" encoding=""UTF-8""?>" );
            Put_Line( ft, "<!DOCTYPE plist PUBLIC ""-//Apple//DTD PLIST 1.0//EN"" ""http://www.apple.com/DTDs/PropertyList-1.0.dtd"">" );
            Put_Line( ft, "<plist version=""1.0"">" );
            Put_Line( ft, "<dict>" );
            Put_Line( ft, "    <key>CFBundleInfoDictionaryVersion</key>" );
            Put_Line( ft, "    <string>6.0</string>" );
            Put_Line( ft, "    <key>CFBundleDevelopmentRegion</key>" );
            Put_Line( ft, "    <string>English</string>" );
            Put_Line( ft, "    <key>CFBundlePackageType</key>" );
            Put_Line( ft, "    <string>APPL</string>" );
            Put_Line( ft, "    <key>CFBundleSignature</key>" );
            Put_Line( ft, "    <string>" & To_String( bundleSig ) & "</string>" );
            Put_Line( ft, "    <key>NSPrincipalClass</key>" );
            Put_Line( ft, "    <string>NSApplication</string>" );
            Put_Line( ft, "    <key>NSAppleScriptEnabled</key>" );
            Put_Line( ft, "    <string>NO</string>" );
            Put_Line( ft, "    <key>LSMinimumSystemVersion</key>" );
            Put_Line( ft, "    <string>10.6.8</string>" );
            Put_Line( ft, "    <key>CFBundleName</key>" );
            Put_Line( ft, "    <string>" & To_String( bundleName ) & "</string>" );
            Put_Line( ft, "    <key>CFBundleExecutable</key>" );
            Put_Line( ft, "    <string>" & Get_Pathname( To_String( execPath ) ) & "</string>" );
            Put_Line( ft, "    <key>CFBundleShortVersionString</key>" );
            Put_Line( ft, "    <string>" & To_String( ver ) & "</string>" );
            Put_Line( ft, "    <key>CFBundleVersion</key>" );
            Put_Line( ft, "    <string>" & To_String( longVer ) & "</string>" );

            Put( ft, To_String( plistKeys ) );

            Put_Line( ft, "</dict>" );
            Put_Line( ft, "</plist>" );
            Close( ft );
        exception
            when others =>
                Close( ft );
                Display_Error( "Error writing '" & bundlePath & "/Contents/Info.plist'" );
        end;

        -- copy resources
        declare

            --------------------------------------------------------------------

            procedure Copy_Resource( pos : String_Lists.Cursor ) is
                res : constant String := Element( pos );
            begin
                If Is_Directory( res ) then
                    -- copy a resource directory
                    Create_Dir( bundlePath & "/Contents/Resources/" & Get_Pathname( res ), ignoreFailure => True );
                    if not Copy_Dir( res,
                                     Normalize_Pathname( bundlePath &
                                                         "/Contents/Resources/" &
                                                         Get_Pathname( res ) ) ) then
                        Display_Error( "Error copying '" & res & "'" );
                    end if;
                else
                    -- copy a resource file
                    if not Copy_File( res, bundlePath & "/Contents/Resources/" ) then
                        Display_Error( "Error copying '" & res & "'" );
                    end if;
                end if;
            end Copy_Resource;

            --------------------------------------------------------------------

        begin
            resources.Iterate( Copy_Resource'Access );
        end;

        -- embed Allegro framework
        if embed then
            declare
                arg1 : GNAT.OS_Lib.String_Access := new String'("-exclude");
                arg2 : GNAT.OS_Lib.String_Access := new String'(".DS_Store");
                arg3 : GNAT.OS_Lib.String_Access := new String'("-resolve-src-symlinks");
                arg4 : GNAT.OS_Lib.String_Access := new String'("/Library/Frameworks/Allegro.framework");
                arg5 : GNAT.OS_Lib.String_Access := new String'(bundlePath & "/Contents/Frameworks");
            begin
                Spawn( "/Developer/Tools/pbxcp",
                       Argument_List'(arg1, arg2, arg3, arg4, arg5),
                       success );
                Free( arg1 );
                Free( arg2 );
                Free( arg3 );
                Free( arg4 );
                Free( arg5 );
                if not success then
                    Display_Error( "Error embedding Allegro framework." );
                end if;
            end;
        end if;

        -- copy the executable
        Copy_File( To_String( execPath ), bundlePath & "/Contents/MacOS", success, Overwrite, Full );
        if not success then
            Display_Error( "Error copying '" & To_String( execPath ) & "'" );
        end if;

    end;
end MKBUNDLE;
