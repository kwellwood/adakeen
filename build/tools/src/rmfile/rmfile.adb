--
-- Copyright (c) 2012 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Ada.Text_IO;
with GNAT.Directory_Operations;         use GNAT.Directory_Operations;
with GNAT.Directory_Operations.Iteration;
 use GNAT.Directory_Operations.Iteration;
with GNAT.OS_Lib;                       use GNAT.OS_Lib;

procedure RMFILE is
    quiet : Boolean := False;

    ----------------------------------------------------------------------------

    procedure Delete_File( path : String ) is
        success : Boolean;
    begin
        Delete_File( Normalize_Pathname( path ), success );
        if not success then
            if Is_Regular_File( Normalize_Pathname( path ) ) then
                if not quiet then
                    Ada.Text_IO.Put_Line( "Unable to delete " & path );
                end if;
            end if;
        end if;
    end Delete_File;

    ----------------------------------------------------------------------------

    first : Integer := 1;
    last  : Integer := Argument_Count;
begin
    if Argument_Count > 1 then
        if Argument( 1 ) = "-q" then
            quiet := True;
            first := first + 1;
        end if;
    end if;

    for i in first..last loop
        if Is_Directory( Normalize_Pathname( Argument( i ) ) ) then
            -- Delete a directory tree
            begin
                Remove_Dir( Normalize_Pathname( Argument( i ) ), Recursive => True );
            exception
                when Directory_Error =>
                    if not quiet then
                        Ada.Text_IO.Put_Line( "Unable to delete " & Argument( i ) );
                    end if;
            end;
        else
            -- Delete files or file patterns
            if Index( Argument( i ), "*" ) >= Argument( i )'First or else
               Index( Argument( i ), "?" ) >= Argument( i )'First
            then
                -- Delete files matching a pattern
                declare
                    procedure Examine( item  : String;
                                       index : Positive;
                                       quit  : in out Boolean ) is
                        pragma Unreferenced( index, quit );
                    begin
                        Delete_File( item );
                    end Examine;

                    procedure Iterate is new Wildcard_Iterator( Examine );
                begin
                    Iterate( Argument( i ) );
                end;
            else
                -- Delete a single file
                Delete_File( Argument( i ) );
            end if;
        end if;
    end loop;
end RMFILE;
