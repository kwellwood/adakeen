--
-- Copyright (c) 2012 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Ada.Text_IO;                       use Ada.Text_IO;

procedure ANNOUNCE is
    lines : Integer := 0;
    first : Integer := 1;
begin
    if Argument_Count = 0 then
        return;
    elsif Argument_Count > 1 then
        -- Check the first argument for a line count (# of lines before/after announce)
        declare
            str : constant String := Argument(1);
        begin
            if str(str'First) = '-' then
                lines := Natural'Value( str(str'First+1..str'Last) );
                first := 2;
            end if;
        end;
    end if;

    for i in 1..lines loop
        if i = 1 then
            Put_Line( "." );
        else
            Put_Line( i * "#" );
        end if;
    end loop;

    if lines > 0 then
        Put( (lines * "#") & "# " );
    end if;
    for i in first..Argument_Count loop
        Put( Argument(i) & ' ' );
    end loop;
    New_Line;

    for i in reverse 1..lines loop
        if i = 1 then
            Put_Line( "." );
        else
            Put_Line( i * "#" );
        end if;
    end loop;
exception
    when others =>
        null;
end ANNOUNCE;
