--
-- Copyright (c) 2012 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Calendar;                      use Ada.Calendar;
with Ada.Calendar.Formatting;           use Ada.Calendar.Formatting;
with Ada.Calendar.Time_Zones;           use Ada.Calendar.Time_Zones;
with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Ada.Text_IO;                       use Ada.Text_IO;

procedure BUILD_DATE is
    dateStr : constant String := Image( Clock, Time_Zone => UTC_Time_Offset );
begin
    if Argument_Count = 1 then
        if To_Lower( Argument( 1 ) ) = "-d" then
            -- write the date to stdout
            Put( Standard_Output, dateStr(dateStr'First..Index( dateStr, " " )-1) );
            return;
        elsif To_Lower( Argument( 1 ) ) = "-t" then
            -- write the time to stdout
            Put( Standard_Output, dateStr(Index( dateStr, " " )+1..dateStr'Last) );
            return;
        end if;
    end if;

    -- write the full date and time to stdout
    Put( Standard_Output, Image( Clock ) );
end BUILD_DATE;
