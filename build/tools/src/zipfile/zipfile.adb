--
-- Copyright (c) 2012 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Directories;
with Ada.Exceptions;                    use Ada.Exceptions;
with Ada.Streams;                       use Ada.Streams;
with Ada.Streams.Stream_IO;             use Ada.Streams.Stream_IO;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Ada.Text_IO;                       use Ada.Text_IO;
with GNAT.OS_Lib;                       use GNAT.OS_Lib;
with GNAT.Directory_Operations.Iteration;
 use GNAT.Directory_Operations.Iteration;
with Zip_Streams;                       use Zip_Streams;
with Zip.Compress;                      use Zip.Compress;
with Zip.Create;

procedure Zipfile is

    ----------------------------------------------------------------------------

    function Add_Extension( s : String; ext : String ) return String is
    begin
        if s'Length < ext'Length + 1 or else
           To_Upper( s(s'Last-ext'Length..s'Last) ) = '.' & To_Upper( ext )
        then
            return s;
        else
            return s & '.' & ext;
        end if;
    end Add_Extension;

    ----------------------------------------------------------------------------

    function Get_Archive_Dirpath( path : String ) return String is
        colon : Integer := Index( path, ":" );
    begin
        if colon >= path'First then
            return path(colon+1..path'Last);
        end if;
        -- path is root directory unless explicitly specified
        return "";
    end Get_Archive_Dirpath;

    ----------------------------------------------------------------------------

    function Get_Archive_Filepath( path : String ) return String is
        colon : Integer := Index( path, ":" );
    begin
        if colon >= path'First then
            return path(colon+1..path'Last);
        end if;
        return path;
    end Get_Archive_Filepath;

    ----------------------------------------------------------------------------

    function Get_Disk_Path( path : String ) return String is
        colon : Integer := Index( path, ":" );
    begin
        if colon >= path'First then
            return path(path'First..colon-1);
        end if;
        return path;
    end Get_Disk_Path;

    ----------------------------------------------------------------------------

    -- Returns the name of a file or directory without any parent directory info.
    function Get_Pathname( path : String ) return String is
        first : Integer;
        last  : Integer := path'Last;
    begin
        -- ignore any trailing slash(es)
        while last >= path'First and then (path(last) = '/' or else path(last) = '\') loop
            last := last - 1;
        end loop;

        first := last;
        while first >= path'First and then path(first) /= '/' and then path(first) /= '\' loop
            first := first - 1;
        end loop;

        return path(first+1..last);
    end Get_Pathname;

    ----------------------------------------------------------------------------

    method : Compression_Method := Deflate_Fixed;
    curArg : Integer := 1;
begin
    if Argument_Count < 2 then
        Put_Line( "Usage: zipfile <new_zipfile.zip> <filepath1[:innerpath1]> [...]" );
        OS_Exit( 1 );
    end if;

    declare
        myStream      : aliased File_Zipstream;
        zipFileStream : constant Zipstream_Class_Access := myStream'Unchecked_Access;
        info          : Zip.Create.Zip_Create_Info;
        zipName       : STRING := Add_Extension( Argument( curArg ), "zip" );
        success       : BOOLEAN;
    begin
        -- delete the existing zip file
        if Is_Regular_File( zipName ) then
            Delete_File( zipName, success );
            if not success then
                Put_Line( "zipfile: Error deleting file: " & zipName );
                OS_Exit( 3 );
            end if;
        end if;

        Zip.Create.Create( info, zipFileStream, zipName, method );

        for i in curArg+1..Argument_Count loop
            declare

                ----------------------------------------------------------------

                procedure Add_File( srcPath, zipPath : String ) is
                    inStream   : aliased File_Zipstream;
                    streamFile : constant Zipstream_Class_Access := inStream'Unchecked_Access;
                begin
                    -- add the file to the zip
                    --Ada.Text_IO.Put_Line( "Add File( " & srcPath & ":" & zipPath & " )" );
                    Set_Name( streamFile.all, srcPath );
                    Set_Time( streamFile.all, Ada.Directories.Modification_Time( srcPath ) );
                    Zip_Streams.Open( inStream, In_File );
                    Set_Name( streamFile.all, zipPath );
                    Zip.Create.Add_Stream( info, streamFile.all );
                    Close( inStream );
                end Add_File;

                ----------------------------------------------------------------

                procedure Add_Dir( srcDir, zipDir : String ) is

                    procedure Add( path : String; index : Positive; quit : in out Boolean ) is
                        pragma Unreferenced( index, quit );
                        name : String := Get_Pathname( path );
                    begin
                        if name'Length > 0 and then name(name'First) /= '.' then
                            if Is_Directory( path ) then
                                --Ada.Text_IO.Put_Line( "Add_Dir( " & path & ":" & (if zipDir /= "" then zipDir & Directory_Separator else "") & name & " )" );
                                Add_Dir( path, (if zipDir /= "" then zipDir & Directory_Separator else "") & name );
                            elsif Is_Regular_File( path ) then
                                Add_File( path, zipDir & Directory_Separator & name );
                            end if;
                        end if;
                    end Add;

                    procedure Iterate is new Wildcard_Iterator( Add );
                begin
                    Iterate( Normalize_Pathname( srcDir & Directory_Separator & "*" ) );
                end Add_Dir;

                ----------------------------------------------------------------

                srcPath : constant String := Get_Disk_Path( Argument( i ) );
            begin
                if Is_Directory( srcPath ) then
                    Add_Dir( srcPath, Get_Archive_Dirpath( Argument( i ) ) );
                elsif Is_Regular_File( srcPath ) then
                    Add_File( srcPath, Get_Archive_Filepath( Argument( i ) ) );
                else
                    Put_Line( "zipfile: File not found: " & srcPath );
                    OS_Exit( 4 );
                end if;
            end;
        end loop;

        Zip.Create.Finish( info );
    end;

exception
    when e : others =>
        Put_Line( "zipfile: Exception: " & Exception_Name( e ) & " -- " & Exception_Message( e ) );
        OS_Exit( 2 );
end Zipfile;
