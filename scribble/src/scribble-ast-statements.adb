--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Scribble.Ast.Expressions.Operators;use Scribble.Ast.Expressions.Operators;
with Scribble.Ast.Processors;           use Scribble.Ast.Processors;
with Scribble.Namespaces;               use Scribble.Namespaces;
with Values;                            use Values;

package body Scribble.Ast.Statements is

    overriding
    procedure Delete( this : in out Ast_Statement ) is
    begin
        Delete( A_Ast_Node(this.next) );
        Ast_Node(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Get_Next( this : not null access Ast_Statement'Class ) return A_Ast_Statement is (this.next);

    ----------------------------------------------------------------------------

    procedure Set_Next( this      : not null access Ast_Statement'Class;
                        statement : A_Ast_Statement ) is
    begin
        this.next := statement;
    end Set_Next;

    --==========================================================================

    function Create_Block( loc : Token_Location; parent : A_Ast_Block ) return A_Ast_Block is
        this : constant A_Ast_Block := new Ast_Block;
    begin
        this.Construct( loc, parent );
        return this;
    end Create_Block;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this   : access Ast_Block;
                         loc    : Token_Location;
                         parent : A_Ast_Block ) is
    begin
        Ast_Statement(this.all).Construct( loc );
        this.parent := parent;
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Ast_Block ) is
    begin
        Delete( A_Ast_Node(this.inner) );
        Ast_Statement(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Append( this      : not null access Ast_Block'Class;
                      statement : in out A_Ast_Statement ) is
    begin
        if this.inner = null then
            this.inner := statement;
            this.last := statement;
        else
            this.last.Set_Next( statement );
            this.last := statement;
        end if;
        statement := null;
    end Append;

    ----------------------------------------------------------------------------

    function Define_Parameter( this  : not null access Ast_Block'Class;
                               ident : not null A_Ast_Identifier ) return Boolean is
    begin
        pragma Assert( this.parent = null, "Parameters may only be defined in the root scope" );

        for v of this.variables loop
            if v.ident.Get_Name = ident.Get_Name then
                return False;
            end if;
        end loop;
        this.variables.Append( Var_Def'(ident => ident,
                                        const => False,
                                        loc   => this.Location) );
        this.parameterCount := this.parameterCount + 1;
        return True;
    end Define_Parameter;

    ----------------------------------------------------------------------------

    function Define_Variable( this  : not null access Ast_Block'Class;
                              ident : not null A_Ast_Identifier;
                              const : Boolean;
                              loc   : Token_Location ) return Boolean is
    begin
        for v of this.variables loop
            if v.ident.Get_Name = ident.Get_Name then
                return False;
            end if;
        end loop;
        this.variables.Append( Var_Def'(ident => ident,
                                        const => const,
                                        loc   => loc) );
        this.Reserve_Variable_Space;
        return True;
    end Define_Variable;

    ----------------------------------------------------------------------------

    function Get_First( this : not null access Ast_Block'Class ) return A_Ast_Statement is (this.inner);

    ----------------------------------------------------------------------------

    function Get_Nth_Variable( this : not null access Ast_Block'Class;
                               n    : Positive ) return A_Ast_Identifier is
    begin
        if n <= Integer(this.variables.Length) - this.parameterCount then
            return this.variables.Element( this.parameterCount + n ).ident;
        end if;
        return null;
    end Get_Nth_Variable;

    ----------------------------------------------------------------------------

    function Get_Nth_Variable_Location( this : not null access Ast_Block'Class;
                                        n    : Positive ) return Token_Location is
    begin
        if n <= Integer(this.variables.Length) - this.parameterCount then
            return this.variables.Element( this.parameterCount + n ).loc;
        end if;
        return UNKNOWN_LOCATION;
    end Get_Nth_Variable_Location;

    ----------------------------------------------------------------------------

    function Get_Parent( this : not null access Ast_Block'Class ) return A_Ast_Block is (this.parent);

    ----------------------------------------------------------------------------

    function Is_Defining_Identifier( this  : not null access Ast_Block'Class;
                                     ident : not null A_Ast_Identifier ) return Boolean is
    begin
        for v of this.variables loop
            if v.ident = ident then
                return True;
            end if;
        end loop;

        if this.parent /= null then
            return this.parent.Is_Defining_Identifier( ident );
        end if;

        return False;
    end Is_Defining_Identifier;

    ----------------------------------------------------------------------------

    function Is_Identifier_Constant( this  : not null access Ast_Block'Class;
                                     ident : not null A_Ast_Identifier ) return Boolean is
    begin
        for v of this.variables loop
            if v.ident.Get_Name = ident.Get_Name and then
               v.ident.Location <= ident.Location
            then
                return v.const;
            end if;
        end loop;

        if this.parent /= null then
            return this.parent.Is_Identifier_Constant( ident );
        end if;

        return False;
    end Is_Identifier_Constant;

    ----------------------------------------------------------------------------

    procedure Prepend( this      : not null access Ast_Block'Class;
                       statement : in out A_Ast_Statement ) is
    begin
        if this.inner = null then
            this.inner := statement;
            this.last := statement;
        else
            statement.Set_Next( this.inner );
            this.inner := statement;
        end if;
        statement := null;
    end Prepend;

    ----------------------------------------------------------------------------

    overriding
    procedure Process( this      : access Ast_Block;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class ) is
    begin
        processor.Process_Block( A_Ast_Block(this) );
    end Process;

    ----------------------------------------------------------------------------

    overriding
    procedure Prune( this : access Ast_Block ) is
        statement : A_Ast_Statement := this.inner;
        tmp       : A_Ast_Statement;
    begin
        while statement /= null loop
            statement.Prune;
            if statement.Get_Type = RETURN_STATEMENT or else
               statement.Get_Type = EXIT_STATEMENT
            then
                -- found a statement that ends execution in the block;
                -- prune out any statements that follow it
                if statement.Get_Next /= null then
                    tmp := statement.Get_Next;
                    Delete( A_Ast_Node(tmp) );
                    statement.Set_Next( null );
                end if;
                this.last := statement;
                exit;
            end if;

            statement := statement.Get_Next;
        end loop;
    end Prune;

    ----------------------------------------------------------------------------

    procedure Reserve_Variable_Space( this    : not null access Ast_Block'Class;
                                      minimum : Natural := 0 ) is
    begin
        if this.reservedVars < (Integer(this.variables.Length) - this.parameterCount) + minimum then
            this.reservedVars := (Integer(this.variables.Length) - this.parameterCount) + minimum;
            if this.parent /= null then
                -- pass the reservation to the parent
                this.parent.Reserve_Variable_Space( this.reservedVars );
            end if;
        end if;
    end Reserve_Variable_Space;

    ----------------------------------------------------------------------------

    function Resolve_Identifier_Index( this  : not null access Ast_Block'Class;
                                       ident : not null A_Ast_Identifier ) return Integer is
    begin
        if ident.Get_Namespace /= ANONYMOUS_NAMESPACE then
            return 0;
        end if;

        for i in 1..Integer(this.variables.Length) loop
            if this.variables.Element( i ).ident.Get_Name = ident.Get_Name and then
               this.variables.Element( i ).loc <= ident.Location
            then
                if i <= this.parameterCount then
                    -- resolved to a parameter
                    -- first parameter is -1, second is -2, etc.
                    return -i;
                else
                    -- resolved to a local variable in this scope
                    return this.Variables_Offset + (i - this.parameterCount);
                end if;
            end if;
        end loop;

        if this.parent /= null then
            return this.parent.Resolve_Identifier_Index( ident );
        end if;

        return 0;
    end Resolve_Identifier_Index;

    ----------------------------------------------------------------------------

    function Reserved_Variables( this : not null access Ast_Block'Class ) return Natural is (this.reservedVars);

    ----------------------------------------------------------------------------

    procedure Set_Parent( this   : not null access Ast_Block'Class;
                          parent : A_Ast_Block ) is
    begin
        this.parent := parent;
    end Set_Parent;

    ----------------------------------------------------------------------------

    function Variable_Count( this : not null access Ast_Block'Class ) return Natural
    is (Integer(this.variables.Length) - this.parameterCount);

    ----------------------------------------------------------------------------

    function Variables_Offset( this : not null access Ast_Block'Class ) return Natural is
        offset   : Natural := 0;
        ancestor : A_Ast_Block := this.parent;
    begin
        while ancestor /= null loop
            offset := offset + ancestor.Variable_Count;
            ancestor := ancestor.Get_Parent;
        end loop;
        return offset;
    end Variables_Offset;

    --==========================================================================

    function Create_Exit( loc : Token_Location ) return A_Ast_Statement is
        this : constant A_Ast_Exit := new Ast_Exit;
    begin
        this.Construct( loc );
        return A_Ast_Statement(this);
    end Create_Exit;

    ----------------------------------------------------------------------------

    overriding
    procedure Process( this      : access Ast_Exit;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class ) is
    begin
        processor.Process_Exit( A_Ast_Exit(this) );
    end Process;

    --==========================================================================

    function Create_Expression_Statement( expr : not null A_Ast_Expression ) return A_Ast_Statement is
        this : constant A_Ast_Expression_Statement := new Ast_Expression_Statement;
    begin
        this.Construct( expr );
        return A_Ast_Statement(this);
    end Create_Expression_Statement;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Ast_Expression_Statement;
                         expr : not null A_Ast_Expression ) is
    begin
        Ast_Statement(this.all).Construct( expr.Location );
        this.expr := expr;
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Ast_Expression_Statement ) is
    begin
        Delete( A_Ast_Node(this.expr) );
        Ast_Statement(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Get_Expression( this : not null access Ast_Expression_Statement'Class ) return A_Ast_Expression is (this.expr);

    ----------------------------------------------------------------------------

    overriding
    procedure Process( this      : access Ast_Expression_Statement;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class ) is
    begin
        processor.Process_Expression_Statement( A_Ast_Expression_Statement(this) );
    end Process;

    ----------------------------------------------------------------------------

    overriding
    procedure Prune( this : access Ast_Expression_Statement ) is
        pruned : A_Ast_Expression;
    begin
        pruned := this.expr.Prune;
        if pruned /= null then
            Delete( A_Ast_Node(this.expr) );
            this.expr := pruned;
        end if;
    end Prune;

    --==========================================================================

    function Create_If( loc : Token_Location ) return A_Ast_If is
        this : constant A_Ast_If := new Ast_If;
    begin
        this.Construct( loc );
        return this;
    end Create_If;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Ast_If ) is
    begin
        for c of this.ifCase loop
            Delete( A_Ast_Node(c.condition) );
            Delete( A_Ast_Node(c.block) );
        end loop;
        Delete( A_Ast_Node(this.elseCase) );
        Ast_Statement(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Add_Case( this      : not null access Ast_If'Class;
                        condition : in out A_Ast_Expression;
                        block     : in out A_Ast_Block ) is
    begin
        this.ifCase.Append( If_Case'(condition, block) );
        condition := null;
        block := null;
    end Add_Case;

    ----------------------------------------------------------------------------

    function Case_Count( this : not null access Ast_If'Class ) return Natural is (Integer(this.ifCase.Length));

    ----------------------------------------------------------------------------

    function Get_Case_Block( this : not null access Ast_If'Class; n : Positive ) return A_Ast_Block
    is ((if n <= Integer(this.ifCase.Length) then this.ifCase.Element( n ).block else null));

    ----------------------------------------------------------------------------

    function Get_Case_Condition( this : not null access Ast_If'Class; n : Positive ) return A_Ast_Expression
    is ((if n <= Integer(this.ifCase.Length) then this.ifCase.Element( n ).condition else null));

    ----------------------------------------------------------------------------

    function Get_Case_Location( this : not null access Ast_If'Class; n : Positive ) return Token_Location
    is ((if n <= Integer(this.ifCase.Length) then this.ifCase.Element( n ).block.Location else UNKNOWN_LOCATION));

    ----------------------------------------------------------------------------

    function Get_Else_Block( this : not null access Ast_If'Class ) return A_Ast_Block is (this.elseCase);

    ----------------------------------------------------------------------------

    overriding
    procedure Process( this      : access Ast_If;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class ) is
    begin
        processor.Process_If( A_Ast_If(this) );
    end Process;

    ----------------------------------------------------------------------------

    overriding
    procedure Prune( this : access Ast_If ) is
        ifCase : If_Case;
        pruned : A_Ast_Expression;
        cond   : Value;
        i      : Integer;
    begin
        i := Integer(this.ifCase.First_Index);
        while i <= Integer(this.ifCase.Last_Index) loop
            -- prune the case's condition expression
            ifCase := this.ifCase.Element( i );
            pruned := ifCase.condition.Prune;
            if pruned /= null then
                Delete( A_Ast_Node(ifCase.condition) );
                ifCase.condition := pruned;
                this.ifCase.Replace_Element( i, ifCase );
            end if;

            -- check if the condition is a constant value
            if ifCase.condition.Get_Type /= LITERAL then
                -- prune the condition's statement block
                ifCase.block.Prune;
            else
                -- determine if the case will never be executed or always be executed
                cond := A_Ast_Literal(ifCase.condition).Get_Value;
                if (cond.Is_Boolean and then not cond.To_Boolean) or else
                   (cond.Is_Number and then cond.To_Int = 0)
                then
                    -- the condition always evaluates false; remove this case.
                    -- if it's not the first can't remove the first case because
                    -- an Ast_If must always have at least one case
                    if i > Integer(this.ifCase.First_Index) then
                        Delete( A_Ast_Node(ifCase.condition) );
                        Delete( A_Ast_Node(ifCase.block) );
                        this.ifCase.Delete( i );
                        i := i - 1;   -- back up the interation
                    end if;
                else
                    -- the condition always evaluates true; remove all
                    -- subsequent cases and 'else'
                    ifCase.block.Prune;
                    while Integer(this.ifCase.Last_Index) > i loop
                        ifCase := this.ifCase.Last_Element;
                        Delete( A_Ast_Node(ifCase.condition) );
                        Delete( A_Ast_Node(ifCase.block) );
                        this.ifCase.Delete_Last;
                    end loop;
                    Delete( A_Ast_Node(this.elseCase) );
                    exit;
                end if;
            end if;

            i := i + 1;
        end loop;

        if this.elseCase /= null then
            this.elseCase.Prune;
        end if;
    end Prune;

    ----------------------------------------------------------------------------

    procedure Set_Else_Block( this  : not null access Ast_If'Class;
                              block : in out A_Ast_Block ) is
    begin
        Delete( A_Ast_Node(this.elseCase) );
        this.elseCase := block;
        block := null;
    end Set_Else_Block;

    --==========================================================================

    function Create_Loop( loc : Token_Location; block : not null A_Ast_Block ) return A_Ast_Loop is
        this : constant A_Ast_Loop := new Ast_Loop;
    begin
        this.Construct( loc, block );
        return this;
    end Create_Loop;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this  : access Ast_Loop;
                         loc   : Token_Location;
                         block : not null A_Ast_Block ) is
    begin
        Ast_Statement(this.all).Construct( loc );
        this.block := block;
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Ast_Loop ) is
    begin
        Delete( A_Ast_Node(this.block) );
        Ast_Statement(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Double_Nest( this : not null access Ast_Loop'Class ) is
        outer : A_Ast_Block;
    begin
        outer := Create_Block( this.block.Location, this.block.Get_Parent );
        this.block.Set_Parent( outer );
        outer.Append( A_Ast_Statement(this.block) );
        this.block := outer;
    end Double_Nest;

    ----------------------------------------------------------------------------

    function Get_Body( this : not null access Ast_Loop'Class ) return A_Ast_Block is (this.block);

    ----------------------------------------------------------------------------

    overriding
    procedure Process( this      : access Ast_Loop;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class ) is
    begin
        processor.Process_Loop( A_Ast_Loop(this) );
    end Process;

    ----------------------------------------------------------------------------

    overriding
    procedure Prune( this : access Ast_Loop ) is
    begin
        this.block.Prune;
    end Prune;

    ----------------------------------------------------------------------------

    procedure Set_Body( this  : not null access Ast_Loop'Class;
                        block : in out A_Ast_Block ) is
    begin
        Delete( A_Ast_Node(this.block) );
        this.block := block;
        block := null;
    end Set_Body;

    --==========================================================================

    function Create_Return( expr : not null A_Ast_Expression ) return A_Ast_Statement is
        this : constant A_Ast_Return := new Ast_Return;
    begin
        this.Construct( expr );
        return A_Ast_Statement(this);
    end Create_Return;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Ast_Return; expr : not null A_Ast_Expression ) is
    begin
        Ast_Statement(this.all).Construct( expr.Location );
        this.expr := expr;
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Ast_Return ) is
    begin
        Delete( A_Ast_Node(this.expr) );
        Ast_Statement(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Get_Expression( this : not null access Ast_Return'Class ) return A_Ast_Expression is (this.expr);

    ----------------------------------------------------------------------------

    overriding
    procedure Process( this      : access Ast_Return;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class ) is
    begin
        processor.Process_Return( A_Ast_Return(this) );
    end Process;

    ----------------------------------------------------------------------------

    overriding
    procedure Prune( this : access Ast_Return ) is
        pruned : A_Ast_Expression;
    begin
        pruned := this.expr.Prune;
        if pruned /= null then
            Delete( A_Ast_Node(this.expr) );
            this.expr := pruned;
        end if;
    end Prune;

    --==========================================================================

    function Create_Var( loc   : Token_Location;
                         var   : not null A_Ast_Identifier;
                         expr  : not null access Ast_Expression'Class;
                         const : Boolean := False ) return A_Ast_Statement is
        this : constant A_Ast_Var := new Ast_Var;
    begin
        this.Construct( loc, var, A_Ast_Expression(expr), const );
        return A_Ast_Statement(this);
    end Create_Var;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this  : access Ast_Var;
                         loc   : Token_Location;
                         var   : not null A_Ast_Identifier;
                         expr  : A_Ast_Expression;
                         const : Boolean ) is
    begin
        Ast_Statement(this.all).Construct( loc );
        this.identLoc := var.Location;

        -- the Var statement's expression is generated as a direct assignment,
        -- consuming the right-hand expression 'expr' and the assignment
        -- target 'var'.
        this.expr := Create_Assign( ASSIGN_DIRECT, var.Location, var, expr );

        this.var := var;
        this.var.Set_Location( this.loc );
        this.const := const;
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Ast_Var ) is
    begin
        Delete( A_Ast_Node(this.expr) );    -- owns this.var
        Ast_Statement(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Get_Expression( this : not null access Ast_Var'Class ) return A_Ast_Expression is (this.expr);

    ----------------------------------------------------------------------------

    function Get_Variable( this : not null access Ast_Var'Class ) return A_Ast_Identifier is (this.var);

    ----------------------------------------------------------------------------

    function Get_Variable_Location( this : not null access Ast_Var'Class ) return Token_Location is (this.identLoc);

    ----------------------------------------------------------------------------

    function Is_Constant( this : not null access Ast_Var'Class ) return Boolean is (this.const);

    ----------------------------------------------------------------------------

    overriding
    procedure Process( this      : access Ast_Var;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class ) is
    begin
        processor.Process_Var( A_Ast_Var(this) );
    end Process;

    ----------------------------------------------------------------------------

    overriding
    procedure Prune( this : access Ast_Var ) is
        pruned : A_Ast_Expression := null;
    begin
        if this.expr /= null then
            pruned := this.expr.Prune;
            if pruned /= null then
                Delete( A_Ast_Node(this.expr) );
                this.expr := pruned;
            end if;
        end if;
    end Prune;

    --==========================================================================

    function Create_Yield( loc : Token_Location ) return A_Ast_Statement is
        this : constant A_Ast_Yield := new Ast_Yield;
    begin
        this.Construct( loc );
        return A_Ast_Statement(this);
    end Create_Yield;

    ----------------------------------------------------------------------------

    overriding
    procedure Process( this      : access Ast_Yield;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class ) is
    begin
        processor.Process_Yield( A_Ast_Yield(this) );
    end Process;

end Scribble.Ast.Statements;
