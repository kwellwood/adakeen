--
-- Copyright (c) 2013-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Unchecked_Deallocation;
with Values.Lists;                     use Values.Lists;
with Values.Maps;                      use Values.Maps;
with Values.Strings;                   use Values.Strings;

package body Scribble.Value_Generators is

    function Create_Value_Generator( runtime : not null A_Scribble_Runtime ) return A_Value_Generator is
        this : constant A_Value_Generator := new Value_Generator;
    begin
        this.runtime := runtime;
        return this;
    end Create_Value_Generator;

    ----------------------------------------------------------------------------

    function Generate( this        : not null access Value_Generator'Class;
                       node        : not null A_Ast_Expression;
                       enableDebug : Boolean := False ) return Value is
    begin
        this.result := Null_Value;
        this.enableDebug := enableDebug;

        node.Process( this );
        return this.result;
    end Generate;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Function_Definition( this : access Value_Generator; node : A_Ast_Function_Definition ) is
    begin
        if this.codeGen = null then
            this.codeGen := Create_Code_Generator( this.runtime );
        end if;
        this.result := Value(this.codeGen.Generate( node, this.enableDebug ));
    end Process_Function_Definition;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_List( this : access Value_Generator; node : A_Ast_List ) is
        val : constant List_Value := Create_List.Lst;
    begin
        for i in 1..node.Length loop
            pragma Assert( node.Get_Element( i ).Get_Type in Ast_Value_Type );
            node.Get_Element( i ).Process( this );
            val.Append( this.result, consume => True );
        end loop;

        this.result := Value(val);
    end Process_List;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Literal( this : access Value_Generator; node : A_Ast_Literal ) is
    begin
        this.result := node.Get_Value;
    end Process_Literal;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Map( this : access Value_Generator; node : A_Ast_Map ) is
        val : constant Map_Value := Create_Map.Map;
        key : Value;
    begin
        for i in 1..node.Length loop
            pragma Assert( node.Get_Key( i ).Get_Type = LITERAL );
            pragma Assert( node.Get_Value( i ).Get_Type in Ast_Value_Type );

            node.Get_Key( i ).Process( this );
            key := this.result;

            node.Get_Value( i ).Process( this );
            val.Set( key.Str.To_Unbounded_String, this.result, consume => True );
        end loop;

        this.result := Value(val);
    end Process_Map;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Value_Generator ) is
        procedure Free is new Ada.Unchecked_Deallocation( Value_Generator'Class, A_Value_Generator );
    begin
        if this /= null then
            Delete( this.codeGen );
            Free( this );
        end if;
    end Delete;

end Scribble.Value_Generators;
