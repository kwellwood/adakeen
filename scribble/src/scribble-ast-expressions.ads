--
-- Copyright (c) 2013-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Vectors;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Values;                            use Values;
with Scribble.Runtimes;                 use Scribble.Runtimes;

limited with Scribble.Ast.Processors;

package Scribble.Ast.Expressions is

    -- Ast_Expression is the abstract base class of every node of an expression
    -- in the abstract syntax tree. Ast_Expression trees implement the Composite
    -- design pattern. Non-leaf expression nodes represent an operation to be
    -- performed on their child expression nodes.
    --
    -- Every expression node has a reference to its parent (declared in
    -- Ast_Expression) and its children, if it has any (declared in a subclass).
    -- The parent of the root node of an expression tree is null.
    --
    -- Expression trees are capable of pruning themselves, using a limited set
    -- of functions (e.g. add, subtract, etc.) on literal child values to reduce
    -- the expression tree to only what must be resolved at runtime.
    type Ast_Expression is abstract new Ast_Node with private;
    type A_Ast_Expression is access all Ast_Expression'Class;

    -- Returns True if this expression element is allowed in a valid reference
    -- or False if it is not.
    not overriding
    function Can_Produce_Reference( this : access Ast_Expression ) return Boolean is (False);

    -- Returns a reference to the expression on the left side of the node, if it
    -- has one. The node maintains ownership.
    function Get_Left( this : access Ast_Expression ) return A_Ast_Expression is (null);

    -- Returns a reference to the parent expression node of this, or null if it
    -- has no parent.
    function Get_Parent( this : not null access Ast_Expression'Class ) return A_Ast_Expression;

    -- Returns a reference to the expression on the right side of the node, if
    -- it has one. The node maintains ownership.
    function Get_Right( this : access Ast_Expression ) return A_Ast_Expression is (null);

    -- Recursively prunes the expression node and its children, returning a node
    -- to replace this one. If the node can't be pruned, null will be returned.
    -- If the return value is not null, then this node may safely be deleted.
    not overriding
    function Prune( this : access Ast_Expression ) return A_Ast_Expression is (null);

    -- Sets the parent expression node of this, or null if it has no parent.
    procedure Set_Parent( this   : not null access Ast_Expression'Class;
                          parent : A_Ast_Expression );

    package Expression_Vectors is new Ada.Containers.Vectors( Positive, A_Ast_Expression, "=" );

    ----------------------------------------------------------------------------

    -- Ast_Builtin is a node that references a built-in function exported by the
    -- compiler's runtime. It can only be used as the left-hand side of a
    -- function call.
    type Ast_Builtin is new Ast_Expression with private;
    type A_Ast_Builtin is access all Ast_Builtin'Class;

    -- Creates a new Ast_Builtin referencing exported function 'name'.
    function Create_Builtin( loc  : Token_Location;
                             name : String ) return A_Ast_Expression;
    pragma Precondition( name'Length > 0 );
    pragma Postcondition( Create_Builtin'Result /= null );

    -- Returns the name of the function.
    function Get_Name( this : not null access Ast_Builtin'Class ) return String;

    -- Returns the Function_Id of the referenced function.
    function Get_Fid( this : not null access Ast_Builtin'Class ) return Function_Id;

    overriding
    function Get_Type( this : access Ast_Builtin ) return Ast_Type is (BUILTIN);

    procedure Process( this      : access Ast_Builtin;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class );

    ----------------------------------------------------------------------------

    -- Ast_Function_Call is an expression node that represents a parameterized
    -- function invocation at runtime. The function to be called is discovered
    -- by evaluating an expression as a Function value, just before the call.
    type Ast_Function_Call is new Ast_Expression with private;
    type A_Ast_Function_Call is access all Ast_Function_Call'Class;

    -- Creates a new Ast_Function representing a functional invocation of the
    -- result of an expression 'callExpr'. The 'callExpr' expression must
    -- evaluate to a Function value in order for the function call to occur. The
    -- Ast_Function_Call will take ownership of 'callExpr'.
    function Create_Function_Call( loc      : Token_Location;
                                   callExpr : not null A_Ast_Expression ) return A_Ast_Function_Call;
    pragma Postcondition( Create_Function_Call'Result /= null );

    -- Creates a new Ast_Function, just like above, but with the first argument's
    -- expression 'arg1' specified. Additional arguments can be added with
    -- Add_Argument. Both 'callExpr' and 'arg1' will become owned by the new
    -- Ast_Function_Call.
    function Create_Function_Call( loc      : Token_Location;
                                   callExpr : not null A_Ast_Expression;
                                   arg1     : not null A_Ast_Expression ) return A_Ast_Function_Call;
    pragma Postcondition( Create_Function_Call'Result /= null );

    -- Adds an expression argument to the function call. The arguments will be
    -- evaluated in order from left to right (first to last) at runtime. True
    -- will be returned in 'added', or False if the function call's maximum
    -- argument count (255) as been exceeded. 'arg' will be consumed on success.
    procedure Add_Argument( this  : not null access Ast_Function_Call'Class;
                            arg   : in out A_Ast_Expression;
                            added : out Boolean );
    pragma Precondition( arg /= null );
    pragma Postcondition( added xor arg /= null );

    -- Returns the number of arguments that will be evaluated and passed to the
    -- function.
    function Arguments_Count( this : not null access Ast_Function_Call'Class ) return Natural;

    -- Returns a reference to the expression of argument number 'n'. If 'n' is
    -- not a valid argument number, null will be returned. Argument numbers
    -- start at one. Ownership is maintained by the Ast_Function_Call.
    function Get_Argument( this : not null access Ast_Function_Call'Class;
                           n    : Positive ) return A_Ast_Expression;

    -- Returns the expression to evaluate in order to produce the function that
    -- will be called.
    function Get_Call_Expression( this : not null access Ast_Function_Call'Class ) return A_Ast_Expression;

    overriding
    function Get_Type( this : access Ast_Function_Call ) return Ast_Type is (FUNCTION_CALL);

    procedure Process( this      : access Ast_Function_Call;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class );

    function Prune( this : access Ast_Function_Call ) return A_Ast_Expression;

    ----------------------------------------------------------------------------

    -- A Ast_Member_Call represents an invocation of a function retrieved from a
    -- Scribble object (id or map) using the membership operator. The object
    -- (id or map) is always passed by reference to the function being invoked,
    -- as the first argument.
    type Ast_Member_Call is new Ast_Function_Call with private;
    type A_Ast_Member_Call is access all Ast_Member_Call'Class;

    -- Creates a new Ast_Member_Call representing an invocation of a function
    -- retrieved from a Scribble object. The 'callExpr' expression must
    -- evaluate to a Function value in order for the function call to occur.
    function Create_Member_Call( loc      : Token_Location;
                                 callExpr : not null A_Ast_Expression ) return A_Ast_Function_Call;
    pragma Postcondition( Create_Member_Call'Result /= null );

    -- Returns a reference to the Member_Call's call expression. The node
    -- maintains ownership.
    function Get_Left( this : access Ast_Member_Call ) return A_Ast_Expression;

    -- Returns the member name containing the function to call.
    function Get_Member_Name( this : not null access Ast_Member_Call'Class ) return String;

    overriding
    function Get_Type( this : access Ast_Member_Call ) return Ast_Type is (MEMBER_CALL);

    procedure Process( this      : access Ast_Member_Call;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class );

    -- Member_Call instances are created as part of pruning the AST. They do not
    -- themselves need to be pruned.
    overriding
    function Prune( this : access Ast_Member_Call ) return A_Ast_Expression is (null);

    ----------------------------------------------------------------------------

    -- Ast_List is an expression node that represents a non-literal list value
    -- containing sub-expressions for atleast one of its elements, requiring the
    -- list to be constructed at runtime.
    type Ast_List is new Ast_Expression with private;
    type A_Ast_List is access all Ast_List'Class;

    -- Creates a new empty Ast_List.
    function Create_List( loc : Token_Location ) return A_Ast_Expression;
    pragma Postcondition( Create_List'Result /= null );

    -- Appends an element to the list as an expression. 'element' will be
    -- consumed.
    procedure Append( this    : not null access Ast_List'Class;
                      element : in out A_Ast_Expression );
    pragma Precondition( element /= null );
    pragma Postcondition( element = null );

    -- Returns a reference to the expression for an element in the list by
    -- index, or null if 'index' is past the end of the list. Ownership is
    -- maintained by the Ast_List.
    function Get_Element( this  : not null access Ast_List'Class;
                          index : Positive ) return A_Ast_Expression;

    overriding
    function Get_Type( this : access Ast_List ) return Ast_Type is (LIST);

    -- Returns the number of elements in the list.
    function Length( this : not null access Ast_List'Class ) return Natural;

    procedure Process( this      : access Ast_List;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class );

    function Prune( this : access Ast_List ) return A_Ast_Expression;

    ----------------------------------------------------------------------------

    -- Ast_Literal is an expression node that represents a literal value of any
    -- type in the Value class hierarchy. While the value can be a literal map
    -- or list, some container values need to be constructed at runtime and are
    -- represented by Ast_Map or Ast_List nodes.
    type Ast_Literal is new Ast_Expression with private;
    type A_Ast_Literal is access all Ast_Literal'Class;

    -- Creates a new Ast_Literal, given the literal value it represents and its
    -- location in the source code. 'val' will be consumed, not copied.
    function Create_Literal( loc : Token_Location; val : Value'Class ) return A_Ast_Expression;
    pragma Postcondition( Create_Literal'Result /= null );

    -- Returns the literal value that the node represents.
    function Get_Value( this : not null access Ast_Literal'Class ) return Value;

    overriding
    function Get_Type( this : access Ast_Literal ) return Ast_Type is (LITERAL);

    procedure Process( this      : access Ast_Literal;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class );

    ----------------------------------------------------------------------------

    -- Ast_Map is an expression node that represents a non-literal map value
    -- containing sub-expressions for atleast one of its keys or values,
    -- requiring the map to be constructed at runtime.
    type Ast_Map is new Ast_Expression with private;
    type A_Ast_Map is access all Ast_Map'Class;

    -- Creates a new empty Ast_Map.
    function Create_Map( loc : Token_Location ) return A_Ast_Expression;
    pragma Postcondition( Create_Map'Result /= null );

    -- Returns a reference to the expression for a key in the pair list by
    -- index, or null if 'index' is off the end of the pair list. 'index' starts
    -- at one. Ownership of the expression is maintained by the Ast_Map.
    function Get_Key( this  : not null access Ast_Map'Class;
                      index : Positive ) return A_Ast_Expression;

    overriding
    function Get_Type( this : access Ast_Map ) return Ast_Type is (MAP);

    -- Returns a reference to the expression for a value in the pair list by
    -- index, or null if 'index' is off the end of the pair list. 'index' starts
    -- at one. Ownership of the expression is maintained by the Ast_Map.
    function Get_Value( this  : not null access Ast_Map'Class;
                        index : Positive ) return A_Ast_Expression;

    -- Inserts a key/value pair as expressions. Both 'key' and 'value' will be
    -- consumed.
    --
    -- Because both keys can values can be expressions that are evaluated at
    -- runtime, the number of pairs added to the map at compile time may not
    -- match the size of the map at runtime, due to key collision. In the event
    -- of a key collision, the key in the last pair added to the Ast_Map will
    -- win.
    procedure Insert( this : not null access Ast_Map'Class;
                      key  : in out A_Ast_Expression;
                      val  : in out A_Ast_Expression );
    pragma Precondition( key /= null );
    pragma Precondition( val /= null );
    pragma Postcondition( key = null );
    pragma Postcondition( val = null );

    -- Returns True if no pairs have been added to the map.
    function Is_Empty( this : not null access Ast_Map'Class ) return Boolean;

    procedure Process( this      : access Ast_Map;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class );

    function Prune( this : access Ast_Map ) return A_Ast_Expression;

    -- Returns the number of key/value pairs in the map at compile time. Because
    -- keys can be expressions, this value may not match the size of the map at
    -- runtime, due to key collision. See the comments for Insert.
    function Length( this : not null access Ast_Map'Class ) return Natural;

    ----------------------------------------------------------------------------

    -- Ast_Self is an expression node that represents a special reference to the
    -- currently executing function, to support recursion. Its value can only be
    -- determined at runtime.
    type Ast_Self is new Ast_Expression with private;
    type A_Ast_Self is access all Ast_Self'Class;

    -- Creates a new Ast_Symbol referencing the currently executing function.
    function Create_Self( loc : Token_Location ) return A_Ast_Expression;
    pragma Postcondition( Create_Self'Result /= null );

    overriding
    function Get_Type( this : access Ast_Self ) return Ast_Type is (SELF);

    procedure Process( this      : access Ast_Self;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class );

private

    type Ast_Expression is abstract new Ast_Node with
        record
            parent : A_Ast_Expression := null;         -- not owned
        end record;

    ----------------------------------------------------------------------------

    type Ast_Builtin is new Ast_Expression with
        record
            name : Unbounded_String;
        end record;

    procedure Construct( this : access Ast_Builtin;
                         loc  : Token_Location;
                         name : String );

    ----------------------------------------------------------------------------

    type Ast_Function_Call is new Ast_Expression with
        record
            callExpr : A_Ast_Expression := null;
            args     : Expression_Vectors.Vector;
        end record;

    procedure Construct( this     : access Ast_Function_Call;
                         loc      : Token_Location;
                         callExpr : A_Ast_Expression );

    procedure Delete( this : in out Ast_Function_Call );

    ----------------------------------------------------------------------------

    type Ast_Member_Call is new Ast_Function_Call with
        record
            name : Unbounded_String;             -- object member name
        end record;

    procedure Construct( this     : access Ast_Member_Call;
                         loc      : Token_Location;
                         callExpr : A_Ast_Expression );

    ----------------------------------------------------------------------------

    type Ast_List is new Ast_Expression with
        record
            elements : Expression_Vectors.Vector;
        end record;

    procedure Delete( this : in out Ast_List );

    ----------------------------------------------------------------------------

    type Ast_Literal is new Ast_Expression with
        record
            val : Value;
        end record;

    procedure Construct( this : access Ast_Literal;
                         loc  : Token_Location;
                         val  : Value'Class );

    ----------------------------------------------------------------------------

    type Expr_Pair is
        record
            key : A_Ast_Expression := null;
            val : A_Ast_Expression := null;
        end record;

    package Expr_Pairs is new Ada.Containers.Vectors(Positive, Expr_Pair, "=");

    type Ast_Map is new Ast_Expression with
        record
            pairs : Expr_Pairs.Vector;
        end record;

    procedure Delete( this : in out Ast_Map );

    ----------------------------------------------------------------------------

    type Ast_Self is new Ast_Expression with null record;

end Scribble.Ast.Expressions;
