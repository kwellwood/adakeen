--
-- Copyright (c) 2013-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Scribble.Ast.Processors;           use Scribble.Ast.Processors;

package body Scribble.Ast.Expressions.Functions is

    function Create_Function_Definition( loc      : Token_Location;
                                         name     : A_Ast_Identifier;
                                         isMember : Boolean := False ) return A_Ast_Function_Definition is
        this : constant A_Ast_Function_Definition := new Ast_Function_Definition;
    begin
        -- don't know why the compiler forced me to cast this
        Ast_Node(this.all).Construct( loc );
        this.name := name;
        this.isMember := isMember;
        if this.isMember then
            this.params.Append( Parameter'(Create_Identifier( loc, "this" ), null) );
        end if;
        return this;
    end Create_Function_Definition;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Ast_Function_Definition ) is
    begin
        for p of this.params loop
            Delete( A_Ast_Node(p.name) );
            Delete( A_Ast_Node(p.default) );
        end loop;
        Delete( A_Ast_Node(this.block) );
        Delete( A_Ast_Node(this.name) );
        Ast_Expression(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Add_Parameter( this    : not null access Ast_Function_Definition'Class;
                             name    : in out A_Ast_Identifier;
                             default : in out A_Ast_Expression;
                             added   : out Boolean ) is
    begin
        if this.Parameters_Count < 255 then
            this.params.Append( Parameter'(name, default) );
            name := null;
            default := null;
            added := True;
        else
            added := False;
        end if;
    end Add_Parameter;

    ----------------------------------------------------------------------------

    function Get_Body( this : not null access Ast_Function_Definition'Class ) return A_Ast_Block is (this.block);

    ----------------------------------------------------------------------------

    function Get_Internal_Name( this : not null access Ast_Function_Definition'Class ) return String is (To_String( this.internalName ));

    ----------------------------------------------------------------------------

    function Get_Name( this : not null access Ast_Function_Definition'Class ) return A_Ast_Identifier is (this.name);

    ----------------------------------------------------------------------------

    function Get_Parameter( this : not null access Ast_Function_Definition'Class;
                            n    : Positive ) return A_Ast_Identifier
    is ((if n <= this.Parameters_Count then this.params.Element( n ).name else null));

    ----------------------------------------------------------------------------

    function Get_Parameter_Default( this : not null access Ast_Function_Definition'Class;
                                    n    : Positive ) return A_Ast_Expression
    is ((if n <= this.Parameters_Count then this.params.Element( n ).default else null));

    ----------------------------------------------------------------------------

    function Get_Path( this : not null access Ast_Function_Definition'Class ) return String is (To_String( this.path ));

    ----------------------------------------------------------------------------

    function Get_Source( this : not null access Ast_Function_Definition'Class ) return Unbounded_String is (this.source);

    ----------------------------------------------------------------------------

    function Get_Source_Offset( this : not null access Ast_Function_Definition'Class ) return Token_Location is (this.sourceOffset);

    ----------------------------------------------------------------------------

    function Is_Member( this : not null access Ast_Function_Definition'Class ) return Boolean is (this.isMember);

    ----------------------------------------------------------------------------

    function Parameters_Count( this : not null access Ast_Function_Definition'Class ) return Natural is (Integer(this.params.Length));

    ----------------------------------------------------------------------------

    overriding
    procedure Process( this      : access Ast_Function_Definition;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class ) is
    begin
        processor.Process_Function_Definition( A_Ast_Function_Definition(this) );
    end Process;

    ----------------------------------------------------------------------------

    overriding
    function Prune( this : access Ast_Function_Definition ) return A_Ast_Expression is
        pruned : A_Ast_Expression;
    begin
        for p of this.params loop
            if p.default /= null then
                pruned := p.default.Prune;
                if pruned /= null then
                    Delete( A_Ast_Node(p.default) );
                    p.default := pruned;
                end if;
            end if;
        end loop;

        if this.block /= null then
            this.block.Prune;
        end if;

        return null;
    end Prune;

    ----------------------------------------------------------------------------

    procedure Set_Body( this  : not null access Ast_Function_Definition'Class;
                        block : in out A_Ast_Block ) is
    begin
        Delete( A_Ast_Node(this.block) );
        this.block := block;
        block := null;
    end Set_Body;

    ----------------------------------------------------------------------------

    procedure Set_Internal_Name( this : not null access Ast_Function_Definition'Class;
                                 name : String ) is
    begin
        this.internalName := To_Unbounded_String( name );
    end Set_Internal_Name;

    ----------------------------------------------------------------------------

    procedure Set_Path( this : not null access Ast_Function_Definition'Class;
                        path : Unbounded_String ) is
    begin
        this.path := path;
    end Set_Path;

    ----------------------------------------------------------------------------

    procedure Set_Source( this   : not null access Ast_Function_Definition'Class;
                          source : Unbounded_String;
                          offset : Token_Location := UNKNOWN_LOCATION ) is
    begin
        this.source := source;
        this.sourceOffset := offset;
    end Set_Source;

end Scribble.Ast.Expressions.Functions;
