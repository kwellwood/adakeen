--
-- Copyright (c) 2013-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings;                       use Ada.Strings;
with Ada.Unchecked_Deallocation;

package body Scribble.Generic_Tokens is

    function Atomic_Add( ptr : access Integer_32;
                         val : Integer_32 ) return Integer_32 is

        function Intrinsic_Sync_Add_And_Fetch( ptr : access Integer_32;
                                               val : Integer_32 ) return Integer_32;
        pragma Import( Intrinsic, Intrinsic_Sync_Add_And_Fetch, "__sync_add_and_fetch_4" );
    begin
        return Intrinsic_Sync_Add_And_Fetch( ptr, val );
    end Atomic_Add;

    ----------------------------------------------------------------------------

    subtype Character_Type is Unsigned_8;

    CT_NORMAL     : constant Character_Type := 0;
    CT_WHITESPACE : constant Character_Type := 1;
    CT_EOL        : constant Character_Type := 2;

    type Character_Type_Array is array (0..255) of Character_Type;

    charTypes : constant Character_Type_Array := Character_Type_Array'(
        -- Special characters
          9 => CT_WHITESPACE,            --   9 TAB
         10 => CT_EOL,                   --  10 LF
         13 => CT_WHITESPACE,            --  13 CR
         32 => CT_WHITESPACE,            --  32 Space

        -- All Others...
        others => CT_NORMAL
    );

    ----------------------------------------------------------------------------

    function Is_EOL( c : Character ) return Boolean is ((charTypes(Character'Pos( c )) and CT_EOL) = CT_EOL);

    ----------------------------------------------------------------------------

    function Is_Whitespace( c : Character ) return Boolean is ((charTypes(Character'Pos( c )) and CT_WHITESPACE) = CT_WHITESPACE);

    --==========================================================================

    procedure Adjust( this : in out Token_Ptr ) is
        tmp : Integer_32;
        pragma Unreferenced( tmp );
    begin
        if this.target /= null then
            tmp := Atomic_Add( this.target.refs'Access, 1 );
        end if;
    end Adjust;

    ----------------------------------------------------------------------------

    procedure Finalize( this : in out Token_Ptr ) is
        procedure Free is new Ada.Unchecked_Deallocation( Token'Class, A_Naked_Token );

        target : A_Naked_Token := this.target;
    begin
        this.target := null;

        if target /= null then
            if Atomic_Add( target.refs'Access, -1 ) = 0 then
                Delete( target.all );
                Free( target );
            end if;
        end if;
    end Finalize;

    ----------------------------------------------------------------------------

    function "="( l, r : Token_Ptr ) return Boolean is (l.target = r.target);

    function Get( this : Token_Ptr ) return access Token'Class is (this.target);

    function Is_Null( this : Token_Ptr'Class ) return Boolean is (this.target = null);

    function Not_Null( this : Token_Ptr'Class ) return Boolean is (this.target /= null);

    ----------------------------------------------------------------------------

    procedure Set( this : in out Token_Ptr; target : access Token'Class ) is
    begin
        if this.target = A_Naked_Token(target) then
            -- the target isn't changing
            return;
        end if;

        if this.target /= null then
            this.Finalize;     -- decrement reference count
        end if;

        if target /= null then
            this.target := A_Naked_Token(target);
            this.Adjust;       -- increment reference count
        end if;
    end Set;

    ----------------------------------------------------------------------------

    procedure Unset( this : in out Token_Ptr'Class ) is
    begin
        this.Set( null );
    end Unset;

    --==========================================================================

    function Get_Type( this : not null access Token'Class ) return Token_Enum is (this.t);

    function Location( this : not null access Token'Class ) return Token_Location is (this.loc);

    ----------------------------------------------------------------------------

    function Create_Token( t : Token_Enum; loc : Token_Location ) return Token_Ptr
    is (Token_Ptr'(Controlled with target => new Token'(1, t, loc)));

    --==========================================================================

    function Get_Name( this : not null access Symbol_Token'Class ) return String is (To_String(this.name));

    ----------------------------------------------------------------------------

    function Create_Symbol( name : String; loc : Token_Location ) return Token_Ptr
    is (Token_Ptr'(Controlled with target => new Symbol_Token'(1, TOKEN_SYMBOL, loc, To_Unbounded_String( name ))));

    --==========================================================================

    function Get_Value( this : not null access Value_Token'Class ) return Value is (this.val);

    ----------------------------------------------------------------------------

    -- No need to copy the value put into the token because the tokenizer
    -- created it itself, and only atomic, immutable values can be parsed as
    -- tokens by the tokenizer.
    function Create_Value( val : Value'Class; loc : Token_Location ) return Token_Ptr
    is (Token_Ptr'(Controlled with target => new Value_Token'(1, TOKEN_VALUE, loc, Value(val))));

end Scribble.Generic_Tokens;
