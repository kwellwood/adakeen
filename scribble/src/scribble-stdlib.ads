--
-- Copyright (c) 2014-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Scribble.States;                   use Scribble.States;
with Values;                            use Values;

package Scribble.Stdlib is

    -- Math Functions  - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- number abs(x : number)
    procedure Abs_Val( state : in out Scribble_State );

    -- number cos(x : number)
    procedure Cos( state : in out Scribble_State );

    -- number sin(x : number)
    procedure Sin( state : in out Scribble_State );

    -- number tan(x : number)
    procedure Tan( state : in out Scribble_State );

    -- number acos(x : number)
    procedure Acos( state : in out Scribble_State );

    -- number asin(x : number)
    procedure Asin( state : in out Scribble_State );

    -- number atan(x : number)
    procedure Atan( state : in out Scribble_State );

    -- number log2(x : number)
    procedure Log2( state : in out Scribble_State );

    -- number log10(x : number)
    procedure Log10( state : in out Scribble_State );

    -- number ln(x : number)
    procedure Ln( state : in out Scribble_State );

    -- any max(a : any, ...)
    procedure Max( state : in out Scribble_State );

    -- any min(a : any, ...)
    procedure Min( state : in out Scribble_State );

    -- number round(x : number)
    procedure Round( state : in out Scribble_State );

    -- number sqrt(x : number)
    procedure Sqrt( state : in out Scribble_State );

    -- String/List Functions - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- list   append(container : list,   val : any   )
    -- string append(container : string, str : string)
    procedure Append( state : in out Scribble_State );

    -- list appendUnique(container : list, val : any)
    procedure AppendUnique( state : in out Scribble_State );

    -- boolean beginsWith(str : string, pattern : string)
    procedure BeginsWith( state : in out Scribble_State );

    -- boolean endsWith(str : string, pattern : string)
    procedure EndsWith( state : in out Scribble_State );

    -- list   head(container : list,   count : number)
    -- string head(container : string, count : number)
    procedure Head( state : in out Scribble_State );

    -- number indexOf(container : string, pattern : string, from : number := 1)
    -- number indexOf(container : list,   val     : any,    from : number := 1)
    procedure IndexOf( state : in out Scribble_State );

    -- list   insert(container : list,   val : any,    before : number)
    -- string insert(container : string, str : string, before : number)
    procedure Insert( state : in out Scribble_State );

    -- number length(str       : string)
    -- number length(container : list  )
    -- number length(container : map   )
    procedure Length( state : in out Scribble_State );

    -- string ltrim(str : string)
    procedure Ltrim( state : in out Scribble_State );

    -- list   prepend(container : list,   val : any   )
    -- string prepend(container : string, str : string)
    procedure Prepend( state : in out Scribble_State );

    -- list prependUnique(container : list, val : any)
    procedure PrependUnique( state : in out Scribble_State );

    -- list   remove(container : list,   start : number, count : number := 1)
    -- string remove(container : string, start : number, count : number := 1)
    procedure Remove( state : in out Scribble_State );

    -- string reverse(str : string)
    -- list reverse(lst : list)
    procedure Reversed( state : in out Scribble_State );

    -- string rtrim(str : string)
    procedure Rtrim( state : in out Scribble_State );

    -- string slice(container : string, start : number, count : number)
    -- list   slice(container : list,   start : number, count : number)
    procedure Slice( state : in out Scribble_State );

    -- list sort(container : list)
    procedure Sort( state : in out Scribble_State );

    -- list split(str : string, delim : string, keepEmpty : boolean := true)
    procedure Split( state : in out Scribble_State );

    -- string sprintf(format : string, ...)
    procedure Sprintf( state : in out Scribble_State );

    -- list   tail(container : list,   count : number)
    -- string tail(container : string, count : number)
    procedure Tail( state : in out Scribble_State );

    -- string trim(str : string)
    procedure Trim( state : in out Scribble_State );

    -- Map Functions - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- list keys(container : map)
    procedure Keys( state : in out Scribble_State );

    -- map merge(base : map, overriding : map)
    procedure Merge( state : in out Scribble_State );

    -- map rmerge(base : map, overriding : map)
    procedure Rmerge( state : in out Scribble_State );

    -- Color Functions - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- rgbaColor(r, g, b, a : number)
    procedure RgbaColor( state : in out Scribble_State );

    -- Miscellaneous Functions - - - - - - - - - - - - - - - - - - - - - - - - -

    -- string image(what : any)
    procedure Image( state : in out Scribble_State );

    -- boolean isBoolean(val : any)
    procedure IsBoolean( state : in out Scribble_State );

    -- boolean isColor(val : any)
    procedure IsColor( state : in out Scribble_State );

    -- boolean isError(val : any)
    procedure IsError( state : in out Scribble_State );

    -- boolean isFunction(val : any)
    procedure IsFunction( state : in out Scribble_State );

    -- boolean isId(val : any)
    procedure isId( state : in out Scribble_State );

    -- boolean isList(val : any)
    procedure IsList( state : in out Scribble_State );

    -- boolean isMap(val : any)
    procedure IsMap( state : in out Scribble_State );

    -- boolean isNumber(val : any)
    procedure IsNumber( state : in out Scribble_State );

    -- boolean isString(val : any)
    procedure IsString( state : in out Scribble_State );

    -- null sleep(millis : number)
    procedure Sleep_ms( state : in out Scribble_State );

    -- string type(val : any)
    procedure Type_Val( state : in out Scribble_State );

end Scribble.Stdlib;
