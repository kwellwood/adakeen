--
-- Copyright (c) 2014-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Assertions;                    use Ada.Assertions;
with Ada.Exceptions;                    use Ada.Exceptions;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Ada.Unchecked_Deallocation;
with Scribble.Debugging;                use Scribble.Debugging;
with Scribble.Opcodes;                  use Scribble.Opcodes;
with Scribble.States;                   use Scribble.States;
with Values.Construction;               use Values.Construction;
with Values.Errors;                     use Values.Errors;
with Values.Operations.Generics;        use Values.Operations.Generics;
with Values.Strings;                    use Values.Strings;

package body Scribble.VMs is

    outputProc : A_Output_Proc := null;
    debugProc  : A_Output_Proc := null;

    ----------------------------------------------------------------------------

    procedure Debug_Line( str : String ) is
    begin
        if debugProc /= null then
            debugProc.all( str );
        end if;
    end Debug_Line;

    ----------------------------------------------------------------------------

    procedure Print_Line( str : String ) is
    begin
        if outputProc /= null then
            outputProc.all( str );
        else
            Debug_Line( "print: " & str );
        end if;
    end Print_Line;

    ----------------------------------------------------------------------------

    procedure Set_Debug_Output( proc : A_Output_Proc ) is
    begin
        debugProc := proc;
    end Set_Debug_Output;

    ----------------------------------------------------------------------------

    procedure Set_Output( proc : A_Output_Proc ) is
    begin
        outputProc := proc;
    end Set_Output;

    --==========================================================================

    function Create_Thread( stackSize : Positive := 1024 ) return A_Thread
    is (new Thread(Integer'Max( MIN_STACK_SIZE, Integer'Min( stackSize, MAX_STACK_SIZE ) )));

    ----------------------------------------------------------------------------

    procedure Enter_Function( this : in out Thread'Class;
                              func : Function_Value ) is
    begin
        this.Push( Create( this.pc ) );     -- push the caller's PC register
        this.pc := 1;                       -- set the new PC register value

        this.Push( this.fn );               -- push the calling function
        this.fn := func;                    -- update the function register

        this.Push( Create( this.fp ) );     -- push the caller's frame pointer
        this.fp := this.ts;                 -- set the new frame pointer
    end Enter_Function;

    ----------------------------------------------------------------------------

    function Get_First_Frame( this : Thread'Class ) return String is
    begin
        return To_String( this.firstFrame );
    end Get_First_Frame;

    ----------------------------------------------------------------------------

    function Get_Namespace( this : Thread'Class;
                            name : String ) return A_Scribble_Namespace is
        crsr : constant Namespace_Maps.Cursor := this.namespaces.Find( name );
    begin
        if Namespace_Maps.Has_Element( crsr ) then
            return Namespace_Maps.Element( crsr );
        end if;
        return null;
    end Get_Namespace;

    ----------------------------------------------------------------------------

    function Get_Result( this : Thread'Class ) return Value is (this.result);

    ----------------------------------------------------------------------------

    function Is_Busy( this : Thread'Class ) return Boolean is (this.busy);

    ----------------------------------------------------------------------------

    function Is_Errored( this : Thread'Class ) return Boolean is (this.err);

    ----------------------------------------------------------------------------

    function Is_Running( this : Thread'Class ) return Boolean is (this.running);

    ----------------------------------------------------------------------------

    procedure Iterate_Trace( this : Thread'Class; examine : access procedure( str : String ) ) is
        frameNum : Positive := 1;
    begin
        for frame of reverse this.trace loop
            -- format is: "#<index> <frameName> [at <path>:<line>]"
            examine.all( "#" & Trim( Integer'Image( frameNum ), Left ) & " " &
                         (if Length( frame.name ) > 0 then To_String( frame.name ) else "???") & " " &
                         (if Length( frame.path ) > 0 then "at " & To_String( frame.path ) & ":" elsif frame.loc.line > 0 then "at " else "") &
                         (if frame.loc.line > 0 then Trim( Integer'Image( frame.loc.line ), Left ) else "") );
            frameNum := frameNum + 1;
        end loop;
    end Iterate_Trace;

    ----------------------------------------------------------------------------

    procedure Load( this      : in out Thread'Class;
                    func      : Function_Value;
                    args      : Value_Array;
                    baseFrame : String := "" ) is
    begin
        if args'Length not in func.Min_Args..func.Arg_Count then
            if func.Min_Args = func.Arg_Count then
                raise Runtime_Error with "Expected" & Integer'Image( func.Arg_Count ) & " arguments";
            else
                raise Runtime_Error with "Expected" & Integer'Image( func.Min_Args ) &
                                         " to" & Integer'Image( func.Arg_Count ) &
                                         " arguments";
            end if;
        end if;

        -- reinitialize thread state
        this.running := True;
        this.err := False;
        this.result := Null_Value;
        this.trace.Clear;

        -- reinitialize registers
        this.ts := 0;
        this.fp := 0;
        this.pc := 0;
        this.fn := Null_Value.Func;

        --
        -- Activation Record
        --
        -- TOP                                                              Pushed by | Unwound by
        --
        -- | Result                                 |                       Callee      op:RET
        -- | Execution                              |                       Callee      op:RET
        -- +----------------------------------------+
        -- | Local variables                        |  1..(1+(vars-1))      op:EVAL     op:RET
        -- |----------------------------------------|
        -- | Caller's fp register (frame pointer)   |  0 <-- frame pointer  op:EVAL     op:RET
        -- +----------------------------------------+
        -- | Caller's function (Function value)     | -1                    op:EVAL     op:RET
        -- |----------------------------------------|
        -- | Caller's pc register (program counter) | -2                    op:EVAL     op:RET
        -- |----------------------------------------|
        -- | Arguments (last on top)                | -3..-(3+(args-1))     Caller      op:RET
        -- +----------------------------------------+
        -- | Local function                         | -(3+args)             Caller      op:RET
        -- +----------------------------------------+
        -- | Execution ...                          |                       Caller
        --
        -- BOTTOM - thread.stack(1)

        -- push the function to evaluate
        this.Push( func );

        -- push the arguments onto the stack
        for i in args'Range loop
            this.Push( Clone( args(i) ) );
        end loop;

        -- use the function's defaults for missing arguments
        for i in (args'Length+1)..func.Arg_Count loop
            this.Push( Clone( func.Arg_Default( i ) ) );
        end loop;

        this.firstFrame := To_Unbounded_String( (if baseFrame'Length > 0 then baseFrame else func.Get_Name) );
        this.Push_Frame( To_String( this.firstFrame ), func.Get_Path );
        this.Enter_Function( func );
    end Load;

    ----------------------------------------------------------------------------

    procedure Load( this      : in out Thread'Class;
                    func      : Function_Value;
                    baseFrame : String := "" ) is
        args : Value_Array(1..0);
    begin
        this.Load( func, args, baseFrame );     -- may raise Runtime_Error
    end Load;

    ----------------------------------------------------------------------------

    procedure Pop( this : in out Thread'Class; top : out Value ) is
    begin
        pragma Assert( this.ts >= this.stack'First, "Stack underflow" );
        top := this.stack(this.ts);
        this.ts := this.ts - 1;
    end Pop;

    ----------------------------------------------------------------------------

    procedure Pop_Frame( this : in out Thread'Class ) is
    begin
        pragma Assert( not this.trace.Is_Empty, "Trace stack underflow" );
        this.trace.Delete_Last;
    end Pop_Frame;

    ----------------------------------------------------------------------------

    procedure Push( this : in out Thread'Class;
                    val  : Value'Class ) is
    begin
        pragma Assert( this.ts < this.stack'Last, "Stack overflow" );
        this.ts := this.ts + 1;
        this.stack(this.ts) := Value(val);
    end Push;

    ----------------------------------------------------------------------------

    procedure Push_Frame( this : in out Thread'Class; name : String; path : String ) is
    begin
        this.trace.Append( Trace_Frame'(name => To_Unbounded_String( name ),
                                        path => To_Unbounded_String( path ),
                                        others => <>) );
    end Push_Frame;

    ----------------------------------------------------------------------------

    procedure Register_Namespace( this      : in out Thread'Class;
                                  name      : String;
                                  namespace : access Scribble_Namespace'Class ) is
        crsr : Namespace_Maps.Cursor := this.namespaces.Find( name );
    begin
        if Namespace_Maps.Has_Element( crsr ) then
            if namespace /= null then
                this.namespaces.Replace_Element( crsr, namespace );
            else
                this.namespaces.Delete( crsr );
            end if;
        elsif namespace /= null then
            this.namespaces.Insert( name, namespace );
        end if;
    end Register_Namespace;

    ----------------------------------------------------------------------------

    procedure Set_Error( this : in out Thread'Class; err : Value'Class ) is
    begin
        if not this.err then
            this.Set_Result( err );
            this.err := True;
        end if;
    end Set_Error;

    ----------------------------------------------------------------------------

    procedure Set_Result( this : in out Thread'Class; result : Value'Class ) is
    begin
        if not this.err then
            -- this copy is necessary because namespace reads and results of ada
            -- function calls during script execution are not copies.
            this.result := Clone( result );
            this.running := False;
        end if;
    end Set_Result;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Thread ) is
        procedure Free is new Ada.Unchecked_Deallocation( Thread'Class, A_Thread );
    begin
        Free( this );
    end Delete;

    --==========================================================================

    function Create_Scribble_VM( runtime : not null A_Scribble_Runtime ) return A_Scribble_VM is
        this : constant A_Scribble_VM := new Scribble_VM;
    begin
        this.runtime := runtime;
        return this;
    end Create_Scribble_VM;

    ----------------------------------------------------------------------------

    function Invoke( this     : not null access Scribble_VM'Class;
                     thread   : not null A_Thread;
                     fid      : Function_Id;
                     argCount : Natural ) return Value is
        state : Scribble_State(argCount);
        proto : Prototype;
    begin
        this.runtime.Get_Prototype( fid, proto );
        if proto.func /= null then
            if argCount in proto.minArgs..proto.maxArgs then
                thread.Push_Frame( To_String( proto.name ), "" );
                state.vm := this;
                state.args := thread.stack((thread.ts-argCount+1)..thread.ts);  -- shift the array down to start at 1
                proto.func.all( state );   -- may raise exception
                thread.Pop_Frame;
                return state.result;
            else
                if proto.minArgs = proto.maxArgs then
                    return Create( Invalid_Arguments,
                                   To_String( proto.name ) & "(): " &
                                   "Expected" & Natural'Image( proto.minArgs ) & " arguments" );
                else
                    return Create( Invalid_Arguments,
                                   To_String( proto.name ) & "(): " &
                                   "Expected" & Natural'Image( proto.minArgs ) & ".." &
                                   Trim( Natural'Image( proto.maxArgs ), Left ) & " arguments" );
                end if;
            end if;
        else
            return Create( Undefined_Function, "Not a function" );
        end if;
    exception
        when e : others =>
            thread.Pop_Frame;
            return Create( Generic_Exception,
                           Exception_Name( e ) & ": " &
                           Exception_Message( e ) );
    end Invoke;

    ----------------------------------------------------------------------------

    function Namespace_Read( this      : not null access Scribble_VM'Class;
                             thread    : not null A_Thread;
                             namespace : String;
                             name      : String;
                             reference : Boolean ) return Value is
        ns      : A_Scribble_Namespace;
        nsFound : Boolean := False;
        result  : Value;
    begin
        -- check for a thread-specific namespace first
        ns := thread.Get_Namespace( namespace );
        if ns /= null then
            nsFound := True;
            result := (if reference then ns.Get_Namespace_Ref( name ) else ns.Get_Namespace_Name( name ));
        end if;

        -- fall back to a runtime namespace if no thread namespace found, or
        -- 'name' was not defined in the thread namespace.
        if result.Is_Null then
            ns := this.runtime.Get_Namespace( namespace );

            if ns /= null then
                nsFound := True;
                result := (if reference then ns.Get_Namespace_Ref( name ) else ns.Get_Namespace_Name( name ));
            end if;
        end if;

        if result.Is_Null then
            if not nsFound then
                if namespace = ANONYMOUS_NAMESPACE then
                    -- no anonymous namespace was found; this is a simple
                    -- undefined variable reference.
                    result := Create( Undefined_Name, "Variable '" & name & "' is undefined.");
                else
                    -- the requested namespace was not found
                    result := Create( Invalid_Namespace, "Namespace '" & namespace & "' is undefined" );
                end if;
            else
                if namespace = ANONYMOUS_NAMESPACE then
                    -- the anonymous namespace was found but 'name' isn't
                    -- defined in it. report the error as though the anonymous
                    -- namespace wasn't the intended namespace, because its only
                    -- an implicit reference.
                    result := Create( Undefined_Name, "Variable '" & name & "' is undefined." );
                else
                    -- the requested namespace exists but doesn't define 'name'
                    result := Create( Undefined_Name,
                                      "Name '" & name & "' is undefined " &
                                      "in namespace '" &  namespace & "'" );
                end if;
            end if;
        end if;
        return result;
    end Namespace_Read;

    ----------------------------------------------------------------------------

    procedure Pop_Thread( this : not null access Scribble_VM'Class ) is
    begin
        this.trace.Delete_Last;
    end Pop_Thread;

    ----------------------------------------------------------------------------

    procedure Push_Thread( this : not null access Scribble_VM'Class; thread : not null A_Thread ) is
    begin
        this.trace.Append( thread );
    end Push_Thread;

    ----------------------------------------------------------------------------

    procedure Run( this   : not null access Scribble_VM'Class;
                   thread : not null A_Thread;
                   mode   : Run_Mode ) is
        op       : Opcode;
        opU      : Natural;
        opS      : Integer;
        left,
        right    : Value;
        assignOp : Assign_Op;
        func     : Function_Value;
        doYield  : Boolean := (mode = Run_Step);
    begin
        if not thread.Is_Running then
            return;
        end if;

        Assert( not thread.Is_Busy, "Thread is busy" );
        thread.busy := True;
        this.Push_Thread( thread );

        while thread.Is_Running loop
            pragma Assert( thread.pc <= thread.fn.Program_Length, "Unexpected end of program" );

            if thread.fn.Get_Debug_Info /= null then
                -- update the code location in the traceback
                -- translate .lines from .source lines into source file lines
                thread.trace.Reference( thread.trace.Last ).loc.line := thread.fn.Get_Debug_Info.lines(thread.pc) + (thread.fn.Get_Debug_Info.firstLine - 1);
            end if;

            op := thread.fn.Get_Opcode( thread.pc );
            case Get_Instruction( op ) is

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when NOOP =>
                    -- do nothing
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when PUSHC =>
                    opU := Get_U( op );

                    pragma Assert( opU <= thread.fn.Data_Length,
                                   "PUSHC: Invalid data address" );

                    -- always copy on read from program data
                    thread.Push( Clone( thread.fn.Get_Data( opU ) ) );
                    if thread.stack(thread.ts).Is_Error then
                        thread.Set_Error( thread.stack(thread.ts) );
                    end if;
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when PUSHI =>
                    thread.Push( Create( Get_S( op ) ) );
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when PUSHN =>
                    thread.Pop( left );   -- namespace
                    thread.Pop( right );  -- name

                    pragma Assert( left.Is_String, "PUSHN: Invalid namespace" );

                    if right.Is_String then
                        thread.Push( this.Namespace_Read( thread,
                                                          left.Str.To_String,
                                                          right.Str.To_String,
                                                          reference => True ) );
                        if thread.stack(thread.ts).Is_Error then
                            thread.Set_Error( thread.stack(thread.ts) );
                        end if;
                    else
                        thread.Set_Error( Create( Type_Mismatch, "Expected string" ) );
                    end if;
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when PUSHND =>
                    thread.Pop( left );    -- namespace
                    thread.Pop( right );   -- name

                    pragma Assert( left.Is_String, "PUSHND: Invalid namespace" );

                    if right.Is_String then
                        thread.Push( this.Namespace_Read( thread,
                                                          left.Str.To_String,
                                                          right.Str.To_String,
                                                          reference => False ) );
                        if thread.stack(thread.ts).Is_Error then
                            thread.Set_Error( thread.stack(thread.ts) );
                        end if;
                    else
                        thread.Set_Error( Create( Type_Mismatch, "Expected string" ) );
                    end if;
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when PUSHNUL =>
                    opU := Get_U( op );

                    pragma Assert( thread.ts + opU <= thread.stack'Last,
                                   "PUSHNUL: Stack overflow" );

                    thread.stack(thread.ts+1..thread.ts+opU) := (others => Null_Value);
                    thread.ts := thread.ts + opU;
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when PUSHS =>
                    opS := Get_S( op );

                    pragma Assert( thread.fp + opS >= thread.stack'First and thread.fp + opS <= thread.ts,
                                   "PUSHS: Invalid stack address" );

                    if thread.stack(thread.fp + opS).Is_Ref then
                        thread.Push( thread.stack(thread.fp + opS) );
                    else
                        thread.Push( Create_Ref( thread.stack(thread.fp + opS)'Unrestricted_Access ) );
                    end if;
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when PUSHSD =>
                    opS := Get_S( op );

                    pragma Assert( thread.fp + opS >= thread.stack'First and thread.fp + opS <= thread.ts,
                                   "PUSHSD: Invalid stack address" );

                    if thread.stack(thread.fp + opS).Is_Ref then
                        thread.Push( thread.stack(thread.fp + opS).Deref );
                    else
                        thread.Push( thread.stack(thread.fp + opS) );
                    end if;
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when PUSHT =>

                    pragma Assert( thread.ts > thread.stack'First, "PUSHT: Stack underflow" );
                    pragma Assert( thread.ts + 1 <= thread.stack'Last, "PUSHT: Stack overflow" );

                    thread.Push( thread.stack(thread.ts) );
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when STORE =>
                    opU := Get_U( op );
                    assignOp := Assign_Op'Val( opU );
                    thread.Pop( left );     -- target location
                    thread.Pop( right );    -- value

                    pragma Assert( assignOp'Valid, "STORE: Invalid instruction" );
                    pragma Assert( left.Is_Ref or left.Is_Error,
                                   "STORE: Invalid target of assignment (" & left.Image & ")" );

                    if left.Is_Ref then
                        case assignOp is
                            when DIRECT   => null;
                            when ADD      => right := Binary_Add.Operate( left.Deref, right );
                            when SUBTRACT => right := Binary_Subtract.Operate( left.Deref, right );
                            when MULTIPLY => right := Binary_Multiply.Operate( left.Deref, right );
                            when DIVIDE   => right := Binary_Divide.Operate( left.Deref, right );
                            when CONCAT   => right := Binary_Concat.Operate( left.Deref, right );
                        end case;
                        if right.Is_Error then
                            thread.Set_Error( right );
                        end if;
                        left.Replace( Clone( right ) );   -- always store a copy
                        thread.Push( right );
                    else
                        thread.Set_Error( Create( Illegal_Operation, "Invalid target of assignment" ) );
                    end if;
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when STORENUL =>
                    opS := Get_S( op );

                    pragma Assert( (thread.fp + opS) in thread.stack'First..thread.ts,
                                   "STORENUL: Invalid stack address" );

                    thread.stack(thread.fp + opS) := Null_Value;
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when POP =>
                    pragma Assert( thread.ts > thread.stack'First, "POP: Stack underflow" );

                    thread.ts := thread.ts - 1;
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when SWAP =>
                    pragma Assert( thread.ts > thread.stack'First + 1, "SWAP: Stack underflow" );

                    left := thread.stack(thread.ts);
                    thread.stack(thread.ts) := thread.stack(thread.ts-1);
                    thread.stack(thread.ts-1) := left;
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when DEREF =>
                    thread.Pop( left );     -- ref

                    pragma Assert( left.Is_Ref, "DEREF: Dereferenced invalid value" );

                    thread.Push( left.Deref );
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when INVOKE =>
                    opU := Get_U( op );
                    thread.Pop( right );    -- fid

                    pragma Assert( right.Is_Id, "INVOKE: Invalid FID" );
                    pragma Assert( opU <= thread.ts, "INVOKE failed: Stack underflow" );

                    -- never copy arguments on invoking internal function
                    left := this.Invoke( thread, To_Function_Id( right ), opU );
                    if left.Is_Error then
                        -- comment out this line to continue without failing
                        thread.Set_Error( left );
                    end if;

                    -- pop the arguments off the stack
                    thread.ts := thread.ts - opU;

                    -- push the result on the stack (not a copy)
                    thread.Push( left );
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when EVAL =>
                    opU := Get_U( op );
                    func := thread.stack(thread.ts - opU).Func;  -- Scribble function to call

                    if not func.Valid then
                        -- not a Function; clean up and return an Error
                        thread.ts := thread.ts - opU - 1;   -- pop arguments and function
                        if func.Is_Error then
                            thread.Push( func );            -- just propagate the error
                        else
                            thread.Push( Create( Type_Mismatch, "Not a function: " & Image( func ) ) );
                        end if;

                        -- comment out this line to continue without failing
                        thread.Set_Error( thread.stack(thread.ts) );
                        thread.pc := thread.pc + 1;

                    elsif opU < func.Min_Args or func.Arg_Count < opU then
                        -- incorrect number of arguments; clean up and return an Errror
                        thread.ts := thread.ts - opU - 1;   -- pop arguments and function
                        if func.Min_Args = func.Arg_Count then
                            thread.Push( Create( Invalid_Arguments,
                                                 "Expected" & Integer'Image( func.Arg_Count ) &
                                                 " arguments" ) );
                        else
                            thread.Push( Create( Invalid_Arguments,
                                                 "Expected" & Integer'Image( func.Min_Args ) &
                                                 " to" & Integer'Image( func.Arg_Count ) &
                                                 " arguments" ) );
                        end if;

                        -- comment out this line to continue without failing
                        thread.Set_Error( thread.stack(thread.ts) );
                        thread.pc := thread.pc + 1;

                    else
                        -- supplement the given arguments with defaults, as necessary
                        if opU < func.Arg_Count then
                            for i in (opU+1)..func.Arg_Count loop
                                thread.Push( Clone( func.Arg_Default( i ) ) );
                            end loop;
                        end if;
                        thread.pc := thread.pc + 1;
                        thread.Push_Frame( func.Get_Name, func.Get_Path );
                        thread.Enter_Function( func );
                    end if;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when RET =>
                    -- pop the execution frame from the trace so a traceback can
                    -- be printed from the calling location if needed.
                    thread.Pop_Frame;

                    -- capture the result
                    thread.Pop( left );

                    -- unwind the stack back to the frame pointer
                    thread.ts := thread.fp;

                    -- restore the caller's frame pointer
                    pragma Assert( thread.ts >= thread.stack'First, "RET: Stack underflow" );
                    thread.fp := thread.stack(thread.ts).To_Int;
                    thread.ts := thread.ts - 1;
                    if thread.fp > 0 then
                        -- restore the calling function
                        pragma Assert( thread.ts >= thread.stack'First, "RET: Stack underflow" );
                        func := thread.fn;
                        thread.fn := thread.stack(thread.ts).Func;
                        thread.ts := thread.ts - 1;

                        -- restore the caller's program counter
                        pragma Assert( thread.ts >= thread.stack'First, "RET: Stack underflow" );
                        thread.pc := thread.stack(thread.ts).To_Int;
                        thread.ts := thread.ts - 1;

                        -- pop the arguments passed by the caller and the called function
                        thread.ts := thread.ts - func.Arg_Count - 1;

                        -- push the result as part of evaluating an
                        -- expression in the caller. this is the same as pushing
                        -- onto the stack from any other source (e.g. push from
                        -- local var, namespace, etc.) the value will definitely
                        -- be copied when it is stored by the caller or when the
                        -- root function returns.
                        thread.Push( left );
                    else
                        -- found the bottom of the stack; the thread is complete
                        thread.Set_Result( left );
                    end if;

                    if left.Is_Error then
                        thread.Set_Error( left );
                    end if;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when JUMP =>
                    opS := Get_S( op );

                    pragma Assert( (thread.pc + opS) in 1..thread.fn.Program_Length,
                                   "JUMP: Invalid address" );

                    thread.pc := thread.pc + opS;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when JUMPZ =>
                    opS := Get_S( op );
                    thread.Pop( left );     -- test for truth

                    if (left.Is_Boolean and then not left.To_Boolean) or else
                       (left.Is_Number and then left.To_Long_Float = 0.0) or else
                       (left.Is_Error)
                    then
                        -- value is False; do the jump
                        thread.pc := thread.pc + opS;
                    else
                        -- value is assumed True; don't jump, just step
                        thread.pc := thread.pc + 1;
                    end if;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when EQ =>
                    thread.Pop( right );
                    thread.Pop( left );
                    thread.Push( Create( left = right ) );
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when NEQ =>
                    thread.Pop( right );
                    thread.Pop( left );
                    thread.Push( Create( left /= right ) );
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when LT =>
                    thread.Pop( right );
                    thread.Pop( left );
                    thread.Push( Create( left < right ) );
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when GT =>
                    thread.Pop( right );
                    thread.Pop( left );
                    thread.Push( Create( left > right ) );
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when LTE =>
                    thread.Pop( right );
                    thread.Pop( left );
                    thread.Push( Create( left <= right ) );
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when GTE =>
                    thread.Pop( right );
                    thread.Pop( left );
                    thread.Push( Create( left >= right ) );
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when BNOT =>
                    thread.Pop( right );
                    thread.Push( Unary_Not.Operate( right ) );
                    if thread.stack(thread.ts).Is_Error then
                        thread.Set_Error( thread.stack(thread.ts) );
                    end if;
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when NEG =>
                    thread.Pop( right );
                    thread.Push( Unary_Negate.Operate( right ) );
                    if thread.stack(thread.ts).Is_Error then
                        thread.Set_Error( thread.stack(thread.ts) );
                    end if;
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when BAND =>
                    thread.Pop( right );
                    thread.Pop( left );
                    thread.Push( Binary_And.Operate( left, right ) );
                    if thread.stack(thread.ts).Is_Error then
                        thread.Set_Error( thread.stack(thread.ts) );
                    end if;
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when BOR =>
                    thread.Pop( right );
                    thread.Pop( left );
                    thread.Push( Binary_Or.Operate( left, right ) );
                    if thread.stack(thread.ts).Is_Error then
                        thread.Set_Error( thread.stack(thread.ts) );
                    end if;
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when BXOR =>
                    thread.Pop( right );
                    thread.Pop( left );
                    thread.Push( Binary_Xor.Operate( left, right ) );
                    if thread.stack(thread.ts).Is_Error then
                        thread.Set_Error( thread.stack(thread.ts) );
                    end if;
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when ADD =>
                    thread.Pop( right );
                    thread.Pop( left );
                    thread.Push( Binary_Add.Operate( left, right ) );
                    if thread.stack(thread.ts).Is_Error then
                        thread.Set_Error( thread.stack(thread.ts) );
                    end if;
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when SUB =>
                    thread.Pop( right );
                    thread.Pop( left );
                    thread.Push( Binary_Subtract.Operate( left, right ) );
                    if thread.stack(thread.ts).Is_Error then
                        thread.Set_Error( thread.stack(thread.ts) );
                    end if;
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when MUL =>
                    thread.Pop( right );
                    thread.Pop( left );
                    thread.Push( Binary_Multiply.Operate( left, right ) );
                    if thread.stack(thread.ts).Is_Error then
                        thread.Set_Error( thread.stack(thread.ts) );
                    end if;
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when DIV =>
                    thread.Pop( right );
                    thread.Pop( left );
                    thread.Push( Binary_Divide.Operate( left, right ) );
                    if thread.stack(thread.ts).Is_Error then
                        thread.Set_Error( thread.stack(thread.ts) );
                    end if;
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when MODULO =>
                    thread.Pop( right );
                    thread.Pop( left );
                    thread.Push( Binary_Modulo.Operate( left, right ) );
                    if thread.stack(thread.ts).Is_Error then
                        thread.Set_Error( thread.stack(thread.ts) );
                    end if;
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when POW =>
                    thread.Pop( right );
                    thread.Pop( left );
                    thread.Push( Binary_Power.Operate( left, right ) );
                    if thread.stack(thread.ts).Is_Error then
                        thread.Set_Error( thread.stack(thread.ts) );
                    end if;
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when CONCAT =>
                    thread.Pop( right );
                    thread.Pop( left );
                    thread.Push( Binary_Concat.Operate( left, right ) );
                    if thread.stack(thread.ts).Is_Error then
                        thread.Set_Error( thread.stack(thread.ts) );
                    end if;
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when SIGN =>
                    thread.Pop( right );
                    thread.Push( Unary_Sign.Operate( right ) );
                    if thread.stack(thread.ts).Is_Error then
                        thread.Set_Error( thread.stack(thread.ts) );
                    end if;
                    thread.pc := thread.pc + 1;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when YIELD =>
                    thread.pc := thread.pc + 1;
                    doYield := True;
                    if mode = Run_Complete then
                        thread.Set_Error( Create( Illegal_Operation, "Yield not allowed" ) );
                    end if;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                when others =>
                    thread.Set_Error( Create( Illegal_Operation, "Illegal instruction" ) );

            end case;

            -- yield execution out of the VM without finishing evaluation. when
            -- not in Run_Complete mode, this occurs at a yield instruction and
            -- when single-stepping.
            exit when doYield;
        end loop;

        this.Pop_Thread;
        thread.busy := False;

        if thread.err then
            -- the thread failed; generate a stack trace for debugging purposes.
            --
            -- the thread's own stack trace only goes back to the original
            -- Scribble function executed by this VM's Run() method. but, when
            -- evaluation of one thread dives back into Ada and triggers the
            -- recursive evaluation of another thread on the same VM, the total
            -- execution trace can span a stack of threads on this VM.
            --
            -- to generate a complete execution trace, the frames of each thread
            -- on the stack must be copied and added to the earliest trace
            -- history of 'thread'.

            -- look backward through the VM's stack of threads, from the most
            -- recent one to the original. for each thread on the stack,
            for thred of reverse this.trace loop
                -- prepend each of its trace frames to the front (oldest part)
                -- of 'thread's trace.
                for frame of reverse thred.trace loop
                    --the first frame of the first thread will be at the front
                    -- of 'thread's trace history.
                    thread.trace.Prepend( frame );
                end loop;
            end loop;
        end if;
    exception
        when others =>
            this.Pop_Thread;
            thread.busy := False;
            for th of reverse this.trace loop
                for frame of reverse th.trace loop
                    thread.trace.Prepend( frame );
                end loop;
            end loop;
            raise;
    end Run;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Scribble_VM ) is
        procedure Free is new Ada.Unchecked_Deallocation( Scribble_VM'Class, A_Scribble_VM );
    begin
        Free( this );
    end Delete;

end Scribble.VMs;
