--
-- Copyright (c) 2013-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;

package body Scribble.Token_Locations is

    function Image( loc          : Token_Location;
                    implicitPath : Unbounded_String := Null_Unbounded_String ) return String is

        ------------------------------------------------------------------------

        -- Return 'path' as a String with all slash characters in it facing the
        -- same way. The first slash in the string determines the direction.
        function Normalize_Slashes( path : Unbounded_String ) return String is
            result : String := To_String( path );
            slash  : Character := ASCII.NUL;
        begin
            for i in result'Range loop
                if result(i) = '/' or result(i) = '\' then
                    if slash = ASCII.NUL then
                        slash := result(i);
                    else
                        result(i) := slash;
                    end if;
                end if;
            end loop;
            return result;
        end Normalize_Slashes;

        ------------------------------------------------------------------------

    begin
        if Length( loc.path ) > 0 and loc.path /= implicitPath then
            -- include the path, line and column
            return Normalize_Slashes( loc.path ) & ':' & Trim( loc.line'Img, Left ) & ':' & Trim( loc.column'Img, Left );
        else
            -- just the line and column (the filename is implicit)
            return Trim( loc.line'Img, Left ) & ':' & Trim( loc.column'Img, Left );
        end if;
    end Image;

end Scribble.Token_Locations;
