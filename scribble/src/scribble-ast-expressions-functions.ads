--
-- Copyright (c) 2013-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Indefinite_Vectors;
with Scribble.Ast.Expressions.Symbols;  use Scribble.Ast.Expressions.Symbols;
with Scribble.Ast.Statements;           use Scribble.Ast.Statements;

package Scribble.Ast.Expressions.Functions is

    -- Ast_Function_Definition is an expression node that represents the
    -- definition of a function. It declares the parameters accepted by the
    -- function, the default values of any optional parameters, and the
    -- implementation of the function.
    --
    -- An Ast_Function_Definition evaluates to a compiled Function value.
    -- Functions are a special class of Value which, in general, cannot be
    -- operated on and are useful only in certain contexts. For example,
    -- Scribble syntax does not permit a function definition within the general
    -- expression syntax because it would not be useful. Generally, the syntax
    -- permits a function definition to be used "instead of" an expression, as
    -- opposed to "within" an expression, like the right hand side of an
    -- assignment operator, local variable initialization, or the return
    -- statement.
    --
    -- Function implementations are specified in one of two ways: binding to an
    -- internal Ada function in the Scribble runtime registry, or as a Scribble
    -- statement block. The 'internalName' field of the class contains the name
    -- of the internal function to call if the implementation is a binding. Or,
    -- the 'block' member contains the statement tree if the implementation is
    -- written with Scribble.
    type Ast_Function_Definition is new Ast_Expression with private;
    type A_Ast_Function_Definition is access all Ast_Function_Definition'Class;

    -- Creates a new function definition, given the location of the 'function'
    -- token in the input.
    --
    -- 'name' is the optional compile-time name for the function, used to
    -- identify the function in stack traces. Function names are known in the
    -- following situations: Named functions (ex: function foo() ...), keys in
    -- map literals (ex: {foo: function() ...), and local variable definitions
    -- (ex: local foo := function() ...). 'name' will become owned by the
    -- A_Ast_Function_Definition.
    --
    -- Set 'isMember' to True to make a function that acts like it's a member of
    -- a Scribble object, implicitly requiring a first parameter "this" that is
    -- the Map value or Id value of an object to which this member function
    -- belongs. The "this" argument should be passed by reference in Scribble.
    function Create_Function_Definition( loc      : Token_Location;
                                         name     : A_Ast_Identifier;
                                         isMember : Boolean := False ) return A_Ast_Function_Definition;
    pragma Postcondition( Create_Function_Definition'Result /= null );

    -- Adds a parameter to the function definition. True will be returned in
    -- 'added' on success, or False if the function has too many parameters.
    -- 'name' and 'default' will be consumed on success.
    procedure Add_Parameter( this    : not null access Ast_Function_Definition'Class;
                             name    : in out A_Ast_Identifier;
                             default : in out A_Ast_Expression;
                             added   : out Boolean );
    pragma Precondition( name /= null );
    pragma Postcondition( added xor name /= null );
    pragma Postcondition( not added or else default = null );

    -- Returns the function's body as a statement block, or null if one has not
    -- been defined.
    function Get_Body( this : not null access Ast_Function_Definition'Class ) return A_Ast_Block;

    -- Returns the name of the internal function that the function definition
    -- represents, or an empty string if none.
    function Get_Internal_Name( this : not null access Ast_Function_Definition'Class ) return String;

    -- Returns the external name of the function definition if this is a named
    -- function definition. Null will be returned if this is an anonymous
    -- function definition.
    function Get_Name( this : not null access Ast_Function_Definition'Class ) return A_Ast_Identifier;

    -- Returns a reference to the name of parameter 'n'. If 'n' is not a valid
    -- parameter number, null will be returned. Parameter numbers start at one.
    -- Ownership of the symbol is maintained by the Ast_Function_Definition.
    function Get_Parameter( this : not null access Ast_Function_Definition'Class;
                            n    : Positive ) return A_Ast_Identifier;

    -- Returns a reference to the default value expression of parameter 'n'. If
    -- 'n' is not a valid parameter number, or no default value expression was
    -- defined, then null will be returned. Parameter numbers start at one.
    -- Ownership of the expression is maintained by the Ast_Function_Definition.
    function Get_Parameter_Default( this : not null access Ast_Function_Definition'Class;
                                    n    : Positive ) return A_Ast_Expression;

    -- Returns the path of the file containing the source, otherwise empty.
    function Get_Path( this : not null access Ast_Function_Definition'Class ) return String;

    -- Returns the source code snippet that was parsed to create this node.
    function Get_Source( this : not null access Ast_Function_Definition'Class ) return Unbounded_String;

    -- Returns the location offset of this node's source snippet from the top of
    -- the original source file that was parsed. To find the source location of
    -- a child node in this node's source string (returned by .Get_Source), this
    -- offset must be subtracted from the child node's reported location.
    function Get_Source_Offset( this : not null access Ast_Function_Definition'Class ) return Token_Location;

    overriding
    function Get_Type( this : access Ast_Function_Definition ) return Ast_Type is (FUNCTION_DEFINITION);

    -- Returns True if the function is a member function contained in an object,
    -- either an internal object represented by an ID, or a Scribble map. Member
    -- functions have an implicit 'this' parameter and must be invoked using
    -- the member operator: obj.myFunc(arg2, arg3);
    function Is_Member( this : not null access Ast_Function_Definition'Class ) return Boolean;

    -- Returns the number of parameters added to the definition.
    function Parameters_Count( this : not null access Ast_Function_Definition'Class ) return Natural;

    procedure Process( this      : access Ast_Function_Definition;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class );

    function Prune( this : access Ast_Function_Definition ) return A_Ast_Expression;

    -- Sets the function body's implementation as a statement block. 'block'
    -- be consumed.
    procedure Set_Body( this  : not null access Ast_Function_Definition'Class;
                        block : in out A_Ast_Block );
    pragma Precondition( block /= null );
    pragma Postcondition( block = null );

    -- Sets the name of the internal function that the function definition
    -- represents.
    procedure Set_Internal_Name( this : not null access Ast_Function_Definition'Class;
                                 name : String );

    -- Sets the path of the source file from which this function was compiled.
    procedure Set_Path( this : not null access Ast_Function_Definition'Class;
                        path : Unbounded_String );

    -- Sets the source code snippet that was parsed to create this node.
    -- 'offset' is the offset of the function definition's source snippet from
    -- the top of the original source file. If this node is the top-level
    -- definition in the source code, and 'source' contains the entire original
    -- source file, then 'offset' must be all zeros. Otherwise, it should be set
    -- to the offset of 'source' from the top of the original source file.
    procedure Set_Source( this   : not null access Ast_Function_Definition'Class;
                          source : Unbounded_String;
                          offset : Token_Location := UNKNOWN_LOCATION );

private

    -- Represents a parameter and its default value, if any.
    type Parameter is
        record
            name    : A_Ast_Identifier := null;
            default : A_Ast_Expression := null;
        end record;

    package Parameter_Vectors is new Ada.Containers.Indefinite_Vectors( Positive, Parameter, "=" );

    type Ast_Function_Definition is new Ast_Expression with
        record
            name         : A_Ast_Identifier;
            isMember     : Boolean := False;
            params       : Parameter_Vectors.Vector;
            block        : A_Ast_Block := null;
            internalName : Unbounded_String;
            path         : Unbounded_String;
            source       : Unbounded_String;
            sourceOffset : Token_Location := UNKNOWN_LOCATION;
        end record;

    overriding
    procedure Delete( this : in out Ast_Function_Definition );

end Scribble.Ast.Expressions.Functions;
