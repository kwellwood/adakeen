--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers;                    use Ada.Containers;
with Ada.Containers.Doubly_Linked_Lists;
with Ada.Containers.Ordered_Maps;
with Ada.Containers.Vectors;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Interfaces;                        use Interfaces;
with Scribble.Ast;                      use Scribble.Ast;
with Scribble.Ast.Expressions;          use Scribble.Ast.Expressions;
with Scribble.Ast.Expressions.Functions;use Scribble.Ast.Expressions.Functions;
with Scribble.Ast.Expressions.Operators;use Scribble.Ast.Expressions.Operators;
with Scribble.Ast.Expressions.Symbols;  use Scribble.Ast.Expressions.Symbols;
with Scribble.Ast.Processors;           use Scribble.Ast.Processors;
with Scribble.Ast.Statements;           use Scribble.Ast.Statements;
with Scribble.Debugging;                use Scribble.Debugging;
with Scribble.Opcodes;                  use Scribble.Opcodes;
with Scribble.Runtimes;                 use Scribble.Runtimes;
with Scribble.Token_Locations;          use Scribble.Token_Locations;
with Values;                            use Values;
with Values.Functions;                  use Values.Functions;

private package Scribble.Code_Generators is

    -- The Scribble code generator builds a Function value from a fully verified
    -- abstract syntax tree.
    type Code_Generator is limited new Ast_Processor with private;
    type A_Code_Generator is access all Code_Generator'Class;

    -- Creates a new instance of the Scribble code generator. The Scribble
    -- runtime provides information about the existence of directly exported
    -- Ada functions.
    function Create_Code_Generator( runtime : not null A_Scribble_Runtime ) return A_Code_Generator;
    pragma Postcondition( Create_Code_Generator'Result /= null );

    -- Generates and returns the Function defined by 'node'. If 'enableDebug' is
    -- True, debugging information will be included in the returned Function. A
    -- Parse_Error will be raised if an error is encountered during code
    -- generation (e.g. max program data size is exceeded.)
    function Generate( this        : not null access Code_Generator'Class;
                       node        : not null A_Ast_Function_Definition;
                       enableDebug : Boolean := False ) return Function_Value;
    pragma Postcondition( Generate'Result.Valid );

    procedure Delete( this : in out A_Code_Generator );
    pragma Postcondition( this = null );

private

    -- A Label represents the destination for a program counter forward jump to
    -- an instruction at a yet-to-be-determined address.
    --
    -- Labels are uniquely identified by their index in the code generator's
    -- forward jump table. Label indexes begin at zero. When a forward jump
    -- instruction is generated, its jump distance is set equal to its target
    -- label's index. The actual jump distance is determined later. Since
    -- backward jump distances are always negative and known when they are
    -- generated, there is no confusion in an Opcode between a backward jump
    -- distance and a forward jump label.
    --
    -- When the jump destination represented by a Label is code generated, its
    -- real address will be resolved. After code generation is complete, all
    -- forward jump distances will be resolved in one pass using the addresses
    -- of their target Labels.
    type Label is
        record
            name    : Natural := 0;   -- name of label (IF_CASE, LOOP_END, etc.)
            address : Natural := 0;   -- destination address (0 = unresolved)
        end record;

    package Label_Vectors is new Ada.Containers.Vectors( Positive, Label, "=" );
    package Opcode_Vectors is new Ada.Containers.Vectors( Positive, Opcode, Interfaces."=" );
    package Integer_Vectors is new Ada.Containers.Vectors( Positive, Integer, "=" );
    package Value_Vectors is new Ada.Containers.Vectors( Positive, Value, "=" );

    -- for debugging information
    type Var_Use_Rec is
        record
            ident : A_Ast_Identifier;
            loc   : Token_Location := UNKNOWN_LOCATION;
        end record;

    package Func_Name_Maps is new Ada.Containers.Ordered_Maps( Function_Id, Unbounded_String, "<", "=" );
    package Var_Lists is new Ada.Containers.Doubly_Linked_Lists( Var_Use_Rec, "=" );
    package Var_Name_Vectors is new Ada.Containers.Vectors( Positive, Var_Lists.List, Var_Lists."=" );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Code_Generator is limited new Ast_Processor with
        record
            runtime      : A_Scribble_Runtime := null;
            child        : A_Code_Generator := null;  -- for generating nested function definitions

            params       : Natural := 0;              -- the function's parameter count
            instructions : Opcode_Vectors.Vector;
            data         : Value_Vectors.Vector;
            scope        : A_Ast_Block := null;       -- current variable scope (not owned)
            fjt          : Label_Vectors.Vector;      -- forward jump table

            -- used to generate debug info
            enableDebug  : Boolean := False;          -- generate debug info?
            lines        : Integer_Vectors.Vector;    -- line number of each instruction
            stmtLine     : Natural := 0;              -- line number of the current statement
            funcNames    : Func_Name_Maps.Map;        -- maps function id to name
            varNames     : Var_Name_Vectors.Vector;   -- contains use of registers in the stack
        end record;

    -- Adds a copy of 'val' to the program data, returning its index. If an
    -- identical value already exists, the existing index will be returned and
    -- the size of the program data will not grow. Program data is constant.
    -- If the maximum program data size is exceeded, a Constraint_Error will be
    -- raised.
    function Add_Data( this : not null access Code_Generator'Class;
                       val  : Value'Class ) return Positive;

    -- Returns the offset of the current function in the stack, relative to the
    -- frame pointer.
    function Current_Function_Frame_Offset( this : not null access Code_Generator'Class ) return Integer;

    -- Defines a new unresolved label named 'name'. After calling this,
    -- instructions jumping forward to the label can be generated using Op_Jump
    -- and Op_Jumpz. Once the address of the label becomes known, calling
    -- Set_Label_Address will resolve the label to a program instruction and no
    -- more jumps to it can be generated. (At that point in the code generation
    -- process, they would be backward jumps.)
    --
    -- Example:
    --
    -- this.Define_Label( BLOCK_END );        -- where BLOCK_END is some arbitrary predefined constant
    -- ... code generation ...
    -- this.Op_Jumpz( BLOCK_END );            -- conditional jump forward to label BLOCK_END
    -- ... code generation ...
    -- Set_Label_Address( BLOCK_END, here );  -- where 'here' is the current program position
    -- ...
    --
    -- Definitions of labels with the same name can be nested. For example, if a
    -- statement block was nested inside the statement block in the above
    -- example, such that this.Define_Label( BLOCK_END ) was called a second
    -- time during code generation, before the first label's address was
    -- resolved, all nested calls to this.Op_Jump( BLOCK_END ) would then
    -- resolve to the second (or inner) BLOCK_END label until
    -- this.Set_Label_Address( BLOCK_END ) was called. At which point, al
    -- further calls to this.Op_Jump( BLOCK_END ) would again to resolve to the
    -- first (or outer) label.
    procedure Define_Label( this : not null access Code_Generator'Class;
                            name : Positive );

    -- Finds the index of a label by name. If multiple labels of the same name
    -- are nested, then the returned index will belong to the innermost label,
    -- or the most recently defined label matching 'name' that is still
    -- unresolved.
    --
    -- See the comments for Define_Label for more information about labels.
    function Find_Label_Index( this : not null access Code_Generator'Class;
                               name : Positive ) return Positive;

    -- Resolves the address of a label in the forward jump table by name, once
    -- its address has become known. If multiple labels of the same name are
    -- nested, then the label whose address is resolved will be the innermost
    -- label, or the most recently defined label matching 'name' that is still
    -- unresolved.
    --
    -- See the comments for Define_Label for more information about labels.
    procedure Set_Label_Address( this    : not null access Code_Generator'Class;
                                 name    : Positive;
                                 address : Natural );
    pragma Precondition( address /= 0 );

    -- Generates a Debug_Info structure for 'node' after code generation is
    -- complete.
    function Generate_Debug_Info( this : not null access Code_Generator'Class;
                                  node : not null A_Ast_Function_Definition ) return A_Debug_Info;

    -- Converts an identifier index to a stack location relative to the frame
    -- pointer. Negative indexes reference arguments. Positive indexes reference
    -- local variables. Zero is not a valid identifier index.
    function Ident_Index_To_Frame_Offset( this  : not null access Code_Generator'Class;
                                          index : Integer ) return Integer;
    pragma Precondition( index /= 0 );

    -- Generates a DEREF instruction, replacing the Reference on the top of the
    -- stack with its target value.
    procedure Op_DEREF( this : not null access Code_Generator'Class; line : Natural );

    -- Generates an EVAL instruction to evaluate the Function value on top of
    -- the stack with 'args' number of arguments.
    procedure Op_EVAL( this : not null access Code_Generator'Class;
                       args : Natural;
                       line : Natural );
    pragma Precondition( args <= 255 );

    -- Generates an INVOKE instruction to call an internal function id 'fid',
    -- passing 'args' number of arguments from the stack. 'name' is the string
    -- name of the function, if known, purely for generating debug info. If the
    -- name of the function is not known, pass an empty string.
    procedure Op_INVOKE( this : not null access Code_Generator'Class;
                         name : String;
                         fid  : Function_Id;
                         args : Natural;
                         line : Natural );
    pragma Precondition( args <= 255 );

    -- Generates a JUMP instruction. If 'target' is negative, the jump is
    -- interpreted as a jump backward. However, if 'target' is non-negative,
    -- then it is interpreted as a forward jump to the most recently defined
    -- label named 'target'. The actual address of the label, and thus the
    -- distance of the jump forward, will be resolved later.
    --
    -- See the comments for Define_Label for more information about labels.
    procedure Op_JUMP( this   : not null access Code_Generator'Class;
                       target : Integer;
                       line   : Natural );
    pragma Precondition( target /= 0 );

    -- Generates a JUMPZ instruction. If 'target' is negative, the jump is
    -- interpreted as a jump backward. However, if 'target' is non-negative,
    -- then it is interpreted as a forward jump to the most recently defined
    -- label named 'target'. The actual address of the label, and thus the
    -- distance of the jump forward, will be resolved later.
    --
    -- See the comments for Define_Label for more information about labels.
    procedure Op_JUMPZ( this   : not null access Code_Generator'Class;
                        target : Integer;
                        line   : Natural );
    pragma Precondition( target /= 0 );

    -- Generates a NOOP (no operation) instruction.
    procedure Op_NOOP( this : not null access Code_Generator'Class; line : Natural );

    -- Generates a POP instruction.
    procedure Op_POP( this : not null access Code_Generator'Class; line : Natural );

    -- Generates a PUSHC instruction, adding 'val' to the constant program data,
    -- if it has not already been added.
    procedure Op_PUSHC( this : not null access Code_Generator'Class;
                        val  : Value'Class;
                        line : Natural );

    -- Generates a PUSHN instruction to get a reference to 'name' in 'namespace'
    -- at runtime. If the name is determined at runtime, pass an empty string
    -- here.
    procedure Op_PUSHN( this      : not null access Code_Generator'Class;
                        namespace : String;
                        name      : String;
                        line      : Natural );

    -- Generates a PUSHND instruction to read 'name' from 'namespace' at runtime.
    -- If the name is determined at runtime, pass an empty string here.
    procedure Op_PUSHND( this      : not null access Code_Generator'Class;
                         namespace : String;
                         name      : String;
                         line      : Natural );

    -- Generates a PUSHNUL instruction to push 'count' Null values or Null
    -- References onto the stack.
    procedure Op_PUSHNUL( this  : not null access Code_Generator'Class;
                          count : Integer;
                          line  : Natural );

    -- Generates a PUSHS instruction, pushing the stack location at 'offset'
    -- relative to the frame pointer.
    procedure Op_PUSHS( this   : not null access Code_Generator'Class;
                        offset : Integer;
                        line   : Natural );

    -- Generates a PUSHSD instruction, pushing the stack location at 'offset'
    -- relative to the frame pointer, then dereferencing it.
    procedure Op_PUSHSD( this   : not null access Code_Generator'Class;
                         offset : Integer;
                         line   : Natural );

    -- Generates a PUSHT instruction, pushing the top of the stack onto the
    -- stack again, duplicating it (no copy).
    procedure Op_PUSHT( this : not null access Code_Generator'Class;
                        line : Natural );

    -- Generates a RET instruction to return from the current stack frame.
    procedure Op_RET( this : not null access Code_Generator'Class; line : Natural );

    -- Generates a STORE instruction, popping the reference on top of the stack
    -- and replacing its target value with the new top of the stack.
    procedure Op_STORE( this     : not null access Code_Generator'Class;
                        assignOp : Assign_Op;
                        line     : Natural );

    -- Generates a STORENUL instruction, writing a Null value into a Reference
    -- located on the stack at an 'offset' relative to the frame pointer.
    procedure Op_STORENUL( this   : not null access Code_Generator'Class;
                           offset : Integer;
                           line   : Natural );

    -- Generates a SWAP instruction, swapping the top of the stack with the
    -- value directly underneath it.
    procedure Op_SWAP( this : not null access Code_Generator'Class;
                       line : Natural );

    -- Generates a YIELD instruction, optionally yielding execution control to
    -- the VM at runtime.
    procedure Op_YIELD( this : not null access Code_Generator'Class; line : Natural );

    -- expressions: operands
    procedure Process_Literal( this : access Code_Generator; node : A_Ast_Literal );
    procedure Process_Map( this : access Code_Generator; node : A_Ast_Map );
    procedure Process_List( this : access Code_Generator; node : A_Ast_List );
    procedure Process_Function_Call( this : access Code_Generator; node : A_Ast_Function_Call );
    procedure Process_Member_Call( this : access Code_Generator; node : A_Ast_Member_Call );
    procedure Process_Self( this : access Code_Generator; node : A_Ast_Self );
    procedure Process_This( this : access Code_Generator; node : A_Ast_This );
    procedure Process_Builtin( this : access Code_Generator; node : A_Ast_Builtin ) is null;
    procedure Process_Identifier( this : access Code_Generator; node : A_Ast_Identifier );
    procedure Process_Membership( this : access Code_Generator; node : A_Ast_Membership );
    procedure Process_Name_Expr( this : access Code_Generator; node : A_Ast_Name_Expr );

    -- expressions: operators
    procedure Process_Unary_Op( this : access Code_Generator; node : A_Ast_Unary_Op );
    procedure Process_Reference( this : access Code_Generator; node : A_Ast_Reference );
    procedure Process_Binary_Op( this : access Code_Generator; node : A_Ast_Binary_Op );
    procedure Process_Assign( this : access Code_Generator; node : A_Ast_Assign );
    procedure Process_Index( this : access Code_Generator; node : A_Ast_Index );
    procedure Process_Conditional( this : access Code_Generator; node : A_Ast_Conditional );

    -- expressions: function definition
    procedure Process_Function_Definition( this : access Code_Generator; node : A_Ast_Function_Definition );

    -- statements
    procedure Process_Block( this : access Code_Generator; node : A_Ast_Block );
    procedure Process_Exit( this : access Code_Generator; node : A_Ast_Exit );
    procedure Process_Expression_Statement( this : access Code_Generator; node : A_Ast_Expression_Statement );
    procedure Process_If( this : access Code_Generator; node : A_Ast_If );
    procedure Process_Loop( this : access Code_Generator; node : A_Ast_Loop );
    procedure Process_Return( this : access Code_Generator; node : A_Ast_Return );
    procedure Process_Var( this : access Code_Generator; node : A_Ast_Var );
    procedure Process_Yield( this : access Code_Generator; node : A_Ast_Yield );

end Scribble.Code_Generators;

