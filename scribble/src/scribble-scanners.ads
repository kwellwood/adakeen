--
-- Copyright (c) 2013-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Streams;                       use Ada.Streams;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Scribble.Tokenizers;
with Scribble.Token_Locations;          use Scribble.Token_Locations;

generic
    with package Tokenizers is new Scribble.Tokenizers(<>);
package Scribble.Scanners is

    use Tokenizers;
    use Tokenizers.Tokens;

    type Scanner is tagged limited private;
    type A_Scanner is access all Scanner'Class;

    -- Creates a new Scanner with an empty input stream.
    function Create_Scanner return A_Scanner;
    pragma Postcondition( Create_Scanner'Result /= null );

    -- Returns the next token from the input stream, if its type matches any
    -- type in the 'acceptable' set. A Parse_Error will be raised if an error
    -- occurs while parsing the next token (e.g. the token is malformed or
    -- unrecognized).
    function Accept_One( this       : not null access Scanner'Class;
                         acceptable : Token_Enums ) return Token_Ptr;

    -- Returns the next token from the input stream, if its type matches 't'.
    -- Otherwise, null will be returned. A Parse_Error will be raised if an
    -- error occurs while parsing the next token (e.g. the token is malformed or
    -- unrecognized).
    function Accept_Token( this : not null access Scanner'Class;
                           t    : Token_Enum ) return Token_Ptr;

    -- Verifies the next token in the input stream matches one of the acceptable
    -- types in 'acceptable', returning the token on success. Otherwise, a
    -- Parse_Error will be raised. A Parse_Error will be raised if an error
    -- occurs while parsing the next token (e.g. the token is malformed or
    -- unrecognized).
    function Expect_One( this     : not null access Scanner'Class;
                         expected : Token_Enums ) return Token_Ptr;

    -- Verifies the next token in the input stream matches type 't', returning it
    -- on success. Otherwise, a Parse_Error will be raised. A Parse_Error will
    -- be raised if an error occurs while parsing the next token.
    function Expect_Token( this : not null access Scanner'Class; t : Token_Enum ) return Token_Ptr;

    -- Same as the functional version of Expect_Token() above, but the token is
    -- discarded on success.
    procedure Expect_Token( this : not null access Scanner'Class; t : Token_Enum );

    -- Returns the source code that has been scanned up to this point.
    function Get_Source( this : not null access Scanner'Class ) return Unbounded_String;

    -- Returns the current parsing position within the input stream.
    function Location( this : not null access Scanner'Class ) return Token_Location;

    -- Sets the input stream from which to scan. If 'strm' is null, the state of
    -- the scanner will be reset and only EOF tokens will be returned until a
    -- new stream is set. 'filepath' is the path of the file being scanned (when
    -- available), or an empty string if none.
    procedure Set_Input( this     : not null access Scanner'Class;
                         strm     : access Root_Stream_Type'Class;
                         filepath : String := "" );

    -- Steps backward in the token stream and returns the most recently scanned
    -- token to the stream. Only one step back is allowed; this procedure cannot
    -- be called twice in a row.
    procedure Step_Back( this : not null access Scanner'Class );

    -- Deletes the scanner.
    procedure Delete( this : in out A_Scanner );
    pragma Postcondition( this = null );

private

    type Scanner is tagged limited
        record
            tokenizer : A_Tokenizer := Create_Tokenizer;
        end record;

end Scribble.Scanners;
