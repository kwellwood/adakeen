--
-- Copyright (c) 2013-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Unchecked_Deallocation;

package body Scribble.Ast is

    not overriding
    procedure Construct( this : access Ast_Node; loc : Token_Location ) is
    begin
        this.loc := loc;
    end Construct;

    ----------------------------------------------------------------------------

    function Location( this : not null access Ast_Node'Class ) return Token_Location is (this.loc);

    ----------------------------------------------------------------------------

    procedure Set_Location( this : not null access Ast_Node'Class;
                            loc  : Token_Location ) is
    begin
        this.loc := loc;
    end Set_Location;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Ast_Node ) is
        procedure Free is new Ada.Unchecked_Deallocation( Ast_Node'Class, A_Ast_Node );
    begin
        if this /= null then
            this.Delete;
            Free( this );
        end if;
    end Delete;

end Scribble.Ast;
