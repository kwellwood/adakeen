--
-- Copyright (c) 2014-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Ada.Unchecked_Deallocation;
with Interfaces;                        use Interfaces;
with Scribble.Stdlib;                   use Scribble.Stdlib;
with Values.Construction;               use Values.Construction;
with Values.Errors;                     use Values.Errors;
with Values.Lists;                      use Values.Lists;
with Values.Maps;                       use Values.Maps;
with Values.Operations.Generics;        use Values.Operations.Generics;
with Values.Strings;                    use Values.Strings;

package body Scribble.Runtimes is

    -- the global Reserved_Namespace pointer for Runtime.Reserve_Namespace
    RESERVED_NAMESPACE_INSTANCE : constant A_Scribble_Namespace := new Reserved_Namespace;

    --==========================================================================

    function To_Function_Id( val : Value'Class ) return Function_Id
    is (if val.Is_Id then Function_Id(val.To_Id) else Invalid_FID);

    ----------------------------------------------------------------------------

    function To_Id_Value( id : Function_Id ) return Value is (Create_Id( Unsigned_64(id) ));

    --==========================================================================

    -- $buildList( target : list, index : number, value : any ) return list
    procedure Scribble_Build_List( state : in out Scribble_State ) is
    begin
        if state.args(2).Is_Number then
            -- consumes args(3) into args(1) and returns args(1) without copying
            state.args(1).Lst.Reference( state.args(2).To_Int ).Replace( state.args(3) );
        end if;
        state.result := state.args(1);
    end Scribble_Build_List;

    ----------------------------------------------------------------------------

    -- $buildMap( target : map, key : string, value : any ) return map
    procedure Scribble_Build_Map( state : in out Scribble_State ) is
    begin
        if state.args(2).Is_String then
            if not state.args(3).Is_Null then
                -- consumes args(3) into args(1) and returns args(1) without copying
                state.args(1).Map.Reference( state.args(2).Str.To_Unbounded_String ).Replace( state.args(3) );
            else
                -- removes field args(2) from args(1) and returns args(1) without copying
                state.args(1).Map.Remove( state.args(2).Str.To_Unbounded_String );
            end if;
        end if;
        state.result := state.args(1);
    end Scribble_Build_Map;

    ----------------------------------------------------------------------------

    -- $in( l : any, r : map  ) return boolean
    -- $in( l : any, r : list ) return boolean
    procedure Scribble_In( state : in out Scribble_State ) is
    begin
        state.result := Binary_In.Operate( state.args(1), state.args(2) );
    end Scribble_In;

    ----------------------------------------------------------------------------

    -- $index( l : map , r : string ) return any
    -- $index( l : list, r : number ) return any
    procedure Scribble_Index( state : in out Scribble_State ) is
    begin
        state.result := Binary_Index.Operate( state.args(1), state.args(2) );
    end Scribble_Index;

    ----------------------------------------------------------------------------

    -- $indexRef( l : map , r : string ) return ref
    -- $indexRef( l : list, r : number ) return ref
    procedure Scribble_Index_Ref( state : in out Scribble_State ) is
    begin
        state.result := Binary_Index_Ref.Operate( state.args(1), state.args(2) );
    end Scribble_Index_Ref;

    ----------------------------------------------------------------------------

    -- $member( object : any, symbol : string ) return any
    procedure Scribble_Member( state : in out Scribble_State ) is
        obj : Value := state.args(1);
    begin
        -- special case: the Member_Call operation invokes $member with a
        -- reference to the object, instead of the object value itself. just
        -- dereference it and continue as normal.
        if obj.Is_Ref then
            obj := Value(obj.Deref);
        end if;

        state.result := Binary_Index.Operate( obj, state.args(2) );
    end Scribble_Member;

    ----------------------------------------------------------------------------

    -- $memberRef( object : any, symbol : string ) return ref
    procedure Scribble_Member_Ref( state : in out Scribble_State ) is
    begin
        state.result := Binary_Index_Ref.Operate( state.args(1), state.args(2) );
    end Scribble_Member_Ref;

    --==========================================================================

    function Create_Scribble_Runtime return A_Scribble_Runtime is
        this : constant A_Scribble_Runtime := new Scribble_Runtime;
    begin
        -- register the language's built-in functions (required for operation)
        this.Define( "$buildList"   , Scribble_Build_List'Access       , 3, 3 );
        this.Define( "$buildMap"    , Scribble_Build_Map'Access        , 3, 3 );
        this.Define( FUNC_MEMBER    , Scribble_Member'Access           , 2, 2 );
        this.Define( FUNC_MEMBER_REF, Scribble_Member_Ref'Access       , 2, 2 );
        this.Define( "$in"          , Scribble_In'Access               , 2, 2 );
        this.Define( FUNC_INDEX     , Scribble_Index'Access            , 2, 2 );
        this.Define( FUNC_INDEX_REF , Scribble_Index_Ref'Access        , 2, 2 );
        this.Define( "isBoolean"    , Scribble.Stdlib.IsBoolean'Access , 1, 1, True );
        this.Define( "isColor"      , Scribble.Stdlib.IsColor'Access   , 1, 1, True );
        this.Define( "isError"      , Scribble.Stdlib.IsError'Access   , 1, 1, True );
        this.Define( "isFunction"   , Scribble.Stdlib.IsFunction'Access, 1, 1, True );
        this.Define( "isId"         , Scribble.Stdlib.isId'Access      , 1, 1, True );
        this.Define( FUNC_ISLIST    , Scribble.Stdlib.IsList'Access    , 1, 1, True );
        this.Define( FUNC_ISMAP     , Scribble.Stdlib.IsMap'Access     , 1, 1, True );
        this.Define( "isNumber"     , Scribble.Stdlib.IsNumber'Access  , 1, 1, True );
        this.Define( "isString"     , Scribble.Stdlib.IsString'Access  , 1, 1, True );
        this.Define( FUNC_KEYS      , Scribble.Stdlib.Keys'Access      , 1, 1, True );
        this.Define( FUNC_LENGTH    , Scribble.Stdlib.Length'Access    , 1, 1, True );
        this.Define( "type"         , Scribble.Stdlib.Type_Val'Access  , 1, 1, True );

        -- register the language's standard functions
        -- It's conceivable that these could be moved to one or more namespaces
        -- to keep the anonymous namespace clean. e.g. math::cos().

        -- math functions
        this.Define( "abs"  , Scribble.Stdlib.Abs_Val'Access, 1,   1, True );
        this.Define( "cos"  , Scribble.Stdlib.Cos'Access    , 1,   1, True );
        this.Define( "sin"  , Scribble.Stdlib.Sin'Access    , 1,   1, True );
        this.Define( "tan"  , Scribble.Stdlib.Tan'Access    , 1,   1, True );
        this.Define( "acos" , Scribble.Stdlib.Acos'Access   , 1,   1, True );
        this.Define( "asin" , Scribble.Stdlib.Asin'Access   , 1,   1, True );
        this.Define( "atan" , Scribble.Stdlib.Atan'Access   , 1,   1, True );
        this.Define( "log2" , Scribble.Stdlib.Log2'Access   , 1,   1, True );
        this.Define( "log10", Scribble.Stdlib.Log10'Access  , 1,   1, True );
        this.Define( "ln"   , Scribble.Stdlib.Ln'Access     , 1,   1, True );
        this.Define( "max"  , Scribble.Stdlib.Max'Access    , 1, 255, True );
        this.Define( "min"  , Scribble.Stdlib.Min'Access    , 1, 255, True );
        this.Define( "round", Scribble.Stdlib.Round'Access  , 1,   1, True );
        this.Define( "sqrt" , Scribble.Stdlib.Sqrt'Access   , 1,   1, True );

        -- string/list functions
        this.Define( "append"       , Scribble.Stdlib.Append'Access       , 2,   2, True );
        this.Define( "appendUnique" , Scribble.Stdlib.AppendUnique'Access , 2,   2, True );
        this.Define( "beginsWith"   , Scribble.Stdlib.BeginsWith'Access   , 2,   2, True );
        this.Define( "endsWith"     , Scribble.Stdlib.EndsWith'Access     , 2,   2, True );
        this.Define( "head"         , Scribble.Stdlib.Head'Access         , 2,   2, True );
        this.Define( "indexOf"      , Scribble.Stdlib.IndexOf'Access      , 2,   3, True );
        this.Define( "insert"       , Scribble.Stdlib.Insert'Access       , 3,   3, True );
        this.Define( "ltrim"        , Scribble.Stdlib.Ltrim'Access        , 1,   1, True );
        this.Define( "prepend"      , Scribble.Stdlib.Prepend'Access      , 2,   2, True );
        this.Define( "prependUnique", Scribble.Stdlib.PrependUnique'Access, 2,   2, True );
        this.Define( "remove"       , Scribble.Stdlib.Remove'Access       , 2,   3, True );
        this.Define( "reverse"      , Scribble.Stdlib.Reversed'Access     , 1,   1, True );
        this.Define( "rtrim"        , Scribble.Stdlib.Rtrim'Access        , 1,   1, True );
        this.Define( "slice"        , Scribble.Stdlib.Slice'Access        , 3,   3, True );
        this.Define( "sort"         , Scribble.Stdlib.Sort'Access         , 1,   2, True );
        this.Define( "split"        , Scribble.Stdlib.Split'Access        , 2,   3, True );
--      this.Define( "sprintf"      , Scribble.Stdlib.Sprintf'Access      , 1, 255, True );
        this.Define( "tail"         , Scribble.Stdlib.Tail'Access         , 2,   2, True );
        this.Define( "trim"         , Scribble.Stdlib.Trim'Access         , 1,   1, True );

        -- map functions
        this.Define( "merge" , Scribble.Stdlib.Merge'Access , 2, 2, True );
        this.Define( "rmerge", Scribble.Stdlib.Rmerge'Access, 2, 2, True );

        -- color functions
        this.Define( "rgbaColor", Scribble.Stdlib.RgbaColor'Access , 4, 4, True );

        -- miscellaneous
        this.Define( "image" , Scribble.Stdlib.Image'Access,    1,   1, True );
--      this.Define( "sleep" , Scribble.Stdlib.Sleep_ms'Access, 1,   1, True );

        return this;
    end Create_Scribble_Runtime;

    ----------------------------------------------------------------------------

    procedure Define( this     : not null access Scribble_Runtime'Class;
                      name     : String;
                      func     : not null A_Ada_Function;
                      minArgs  : Natural;
                      maxArgs  : Natural;
                      exported : Boolean := False ) is
    begin
        this.prototypes.Include( Fid_Hash( name ),
                                 Prototype'(func,
                                            minArgs,
                                            maxArgs,
                                            exported,
                                            To_Unbounded_String( name )) );
    end Define;

    ----------------------------------------------------------------------------

    function Get_Environment( this : not null access Scribble_Runtime'Class ) return Unbounded_String is
        result  : Unbounded_String;
        namePos : Namespace_Maps.Cursor;
    begin
        Append( result, "Namespaces:" & ASCII.LF );
        namePos := this.namespaces.First;
        while Namespace_Maps.Has_Element( namePos ) loop
            if Namespace_Maps.Key( namePos )'Length = 0 then
                Append( result, "  <anonymous>" & ASCII.LF );
            else
                Append( result, "  " & Namespace_Maps.Key( namePos ) & ASCII.LF );
            end if;
            Namespace_Maps.Next( namePos );
        end loop;
        if this.namespaces.Is_Empty then
            Append( result, "  <none>" & ASCII.LF );
        end if;

        Append( result, "Functions:" & ASCII.LF );
        for proto of this.prototypes loop
            if proto.exported then
                Append( result, "  " & To_String( proto.name ) & "(" );
                if proto.maxArgs > 0 then
                    Append( result,
                            Trim( Natural'Image( proto.minArgs ), Left ) & ".." &
                            Trim( Natural'Image( proto.maxArgs ), Left ) );
                end if;
                Append( result, ")" );
                --if proto.exported then
                --    Append( result, " [export]" );
                --end if;
                Append( result, ASCII.LF );
            end if;
        end loop;
        if this.prototypes.Is_Empty then
            Append( result, "  <none>" & ASCII.LF );
        end if;

        return result;
    end Get_Environment;

    ----------------------------------------------------------------------------

    function Get_Namespace( this : not null access Scribble_Runtime'Class;
                            name : String ) return A_Scribble_Namespace is
        crsr : constant Namespace_Maps.Cursor := this.namespaces.Find( name );
    begin
        if Namespace_Maps.Has_Element( crsr ) then
            return Namespace_Maps.Element( crsr );
        end if;
        return null;
    end Get_Namespace;

    ----------------------------------------------------------------------------

    procedure Get_Prototype( this  : not null access Scribble_Runtime'Class;
                             fid   : Function_Id;
                             proto : out Prototype ) is
        crsr : constant Prototype_Maps.Cursor := this.prototypes.Find( fid );
    begin
        if Prototype_Maps.Has_Element( crsr ) then
            proto := Prototype_Maps.Element( crsr );
        else
            proto := Prototype'(func => null, others => <>);
        end if;
    end Get_Prototype;

    ----------------------------------------------------------------------------

    function Is_Defined( this : not null access Scribble_Runtime'Class;
                         fid  : Function_Id ) return Boolean
        is (this.prototypes.Contains( fid ));

    ----------------------------------------------------------------------------

    function Is_Namespace( this      : not null access Scribble_Runtime'Class;
                           namespace : String ) return Boolean is
        crsr : constant Namespace_Maps.Cursor := this.namespaces.Find( namespace );
    begin
        return Namespace_Maps.Has_Element( crsr );
    end Is_Namespace;

    ----------------------------------------------------------------------------

    procedure Register_Namespace( this      : not null access Scribble_Runtime'Class;
                                  name      : String;
                                  namespace : access Scribble_Namespace'Class ) is
        crsr : Namespace_Maps.Cursor := this.namespaces.Find( name );
    begin
        if Namespace_Maps.Has_Element( crsr ) then
            if namespace /= null then
                this.namespaces.Replace_Element( crsr, namespace );
            else
                this.namespaces.Delete( crsr );
            end if;
        elsif namespace /= null then
            this.namespaces.Insert( name, namespace );
        end if;
    end Register_Namespace;

    ----------------------------------------------------------------------------

    procedure Reserve_Namespace( this : not null access Scribble_Runtime'Class;
                                 name : String ) is
    begin
        if not this.Is_Namespace( name ) then
            this.Register_Namespace( name, RESERVED_NAMESPACE_INSTANCE );
        end if;
    end Reserve_Namespace;

    ----------------------------------------------------------------------------

    procedure Unregister_Namespace( this : not null access Scribble_Runtime'Class;
                                    name : String ) is
    begin
        this.Register_Namespace( name, null );
    end Unregister_Namespace;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Scribble_Runtime ) is
        procedure Free is new Ada.Unchecked_Deallocation( Scribble_Runtime'Class, A_Scribble_Runtime );
    begin
        Free( this );
    end Delete;

end Scribble.Runtimes;
