--
-- Copyright (c) 2015 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Values.Streaming is

    -- Reads a value from a stream.
    function Value_Input( stream : access Root_Stream_Type'Class ) return Value;

    -- Writes a value to a stream.
    procedure Value_Output( stream : access Root_Stream_Type'Class; this : Value'Class );

end Values.Streaming;
