--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Streams;                       use Ada.Streams;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Scribble.Ast;                      use Scribble.Ast;
with Scribble.Ast.Expressions;          use Scribble.Ast.Expressions;
with Scribble.Ast.Expressions.Functions;use Scribble.Ast.Expressions.Functions;
with Scribble.Ast.Statements;           use Scribble.Ast.Statements;
with Scribble.Generic_Tokens;
with Scribble.Runtimes;                 use Scribble.Runtimes;
with Scribble.Scanners;
with Scribble.Token_Locations;          use Scribble.Token_Locations;
with Scribble.Token_Types;              use Scribble.Token_Types;
with Scribble.Tokenizers;
with Values;                            use Values;

private package Scribble.Parsers is

    -- SCRIBBLE GRAMMAR
    ----------------------------------------------------------------------------
    -- VALUES
    --
    -- Value         ::= value-token
    --                   | ('-' number-value-token)
    --                   | List_Value
    --                   | Map_Value
    --                   | Anon_Function_Definition
    --                   | Named_Function_Definition
    --   List_Value  ::= '[' [Expression_List<literal=True>] ']'
    --   Map_Value   ::= '{' [Map_Pair {(',' | end-of-line) Map_Pair}] '}'
    --     Map_Pair  ::= Named_Function_Definition
    --                   | Named_Member_Definition
    --                   | (Map_Key ':' (Anon_Function_Definition | Anon_Member_Definition | Expression<literal=True>))
    --       Map_Key ::= string-value-token | symbol-token
    --
    ----------------------------------------------------------------------------
    -- EXPRESSIONS
    --
    -- Expression_List<literal> ::= (Anon_Function_Definition | Expression<literal>) [(',' | end-of-line) Expression_List<literal>]
    --
    -- Expression<literal> ::= Assign_Term<literal>
    --
    -- Assign_Term<literal=False> ::= Ternary_Term<literal> [Assign_Op (Anon_Function_Definition | Assign_Term)]
    -- Assign_Term<literal=True>  ::= Ternary_Term<literal>
    --   Assign_Op ::= ':=' | '+=' | '-=' | '*=' | '/=' | '&='
    --
    -- Ternary_Term<literal> ::= Binary_Term<literal> ['?' Ternary_Term<literal> ':' Ternary_Term<literal>]
    --
    -- Binary_Term<literal> ::= Lterm<literal> [Binary_Op Binary_Term<literal>]
    --   Binary_Op ::= 'and' | 'or' | 'xor' | 'in' | '=' | '>' | '>=' | '<' |
    --                 '<=' | '!=' | '+' | '-' | '&' | '*' | '/' | '%' | '^'
    --
    -- Lterm<literal> ::= Unary_Op<literal> Lterm<literal>
    -- Lterm<literal> ::= Rterm<literal>
    --   Unary_Op<literal=False> ::= '!' | '-' | '@'
    --   Unary_Op<literal=True>  ::= '!' | '-'
    --
    -- Rterm<literal=False> ::= Operand<literal> {Index | Function_Call | Membership}
    -- Rterm<literal=True>  ::= Operand<literal>
    --
    -- Index ::= '[' Expression<literal=False> ']'
    --
    -- Function_Call ::= '(' [Expression_List<literal=False>] ')'
    --
    -- Membership ::= '.' Identifier
    --
    -- Operand<literal=False> ::= Literal<literal> | Identifier_Expression | ('(' Expression<literal> ')')
    -- Operand<literal=True>  ::= Literal<literal> | ('(' Expression<literal> ')')
    --
    -- Literal<literal=False>  ::= value-token | 'self' | 'this' | List_Literal<literal> | Map_Literal<literal>
    -- Literal<literal=True>   ::= value-token | List_Literal<literal> | Map_Literal<literal>
    --   List_Literal<literal> ::= '[' [Expression_List<literal>] ']'
    --   Map_Literal<literal>  ::= '{' [Map_Pair<literal> {(',' | end-of-line) Map_Pair<literal>}] '}'
    --     Map_Pair<literal>   ::= Named_Function_Definition
    --                             | Named_Member_Definition
    --                             | (Map_Key ':' (Anon_Function_Definition | Anon_Member_Definition | Expression<literal>))
    --       Map_Key           ::= string-value-token | symbol-token
    --
    -- Identifier_Expression ::= Identifier ['::' (Identifier | Name_Expression)]
    --   Name_Expression     ::= '[' Expression<literal=False> ']'
    --
    -- Identifier     ::= symbol-token
    --   symbol-token ::= letter {letter | digit | '_'}
    --
    ----------------------------------------------------------------------------
    -- FUNCTION DEFINITIONS
    --
    -- Anon_Function_Definition  ::= 'function' Function_Definition
    -- Anon_Member_Definition    ::= 'member' ['function'] Function_Definition
    -- Named_Function_Definition ::= 'function' Identifier Function_Definition
    -- Named_Member_Definition   ::= 'member' ['function'] Identifier Function_Definition
    --   Function_Definition     ::= Parameters_Prototype Function_Implementation
    --   Parameters_Prototype    ::= '(' [Parameter {',' Parameter}] ')'
    --   Parameter               ::= Identifier [':' Value_Type] [':=' Expression<literal=True>]
    --   Value_Type              ::= 'map' | 'boolean' | 'id' | 'list' | 'number' | 'string'
    --   Function_Implementation ::= (':' Identifier) | (Statement_List 'end' 'function')
    --
    ----------------------------------------------------------------------------
    -- STATEMENTS
    --
    -- Statement_List ::= {Statement}
    --
    -- Statement ::= {';'} (
    --               Block
    --               | Exit
    --               | For
    --               | If
    --               | Loop
    --               | Return
    --               | Var
    --               | While
    --               | Yield
    --               | Expression_Statement
    --               ) {';'}
    --
    -- Block ::= 'block' Statement_List 'end' 'block' ';'
    --
    -- Exit ::= 'exit' ['when' Expression<literal=False>] ';'
    --
    -- For         ::= 'for' Identifier (For_Range | For_Each) Loop
    --   For_Range ::= 'is' Expression<literal=False> 'to' Expression<literal=False> ['step' Expression<literal=False>]
    --   For_Each  ::= 'of' Expression<literal=False>
    --
    -- If ::= 'if' Expression<literal=False> 'then' Statement_List
    --        {'elsif' Expression<literal=False> 'then' Statement_List}
    --        ['else' Statement_List]
    --        'end' 'if' ';'
    --
    -- Loop ::= 'loop' Statement_List 'end' 'loop' ';'
    --
    -- Return ::= 'return' [Expression<literal=False> | Anon_Function_Definition] ';'
    --
    -- Var ::= 'local' Identifier [':=' (Expression<literal=False> | Anon_Function_Definition)] ';'
    -- Var ::= 'constant' Identifier ':=' (Expression<literal=False> | Anon_Function_Definition) ';'
    -- Var ::= Named_Function_Definition ';'
    --
    -- While ::= 'while' Expression<literal=False> Loop
    --
    -- Yield ::= 'yield' ';'
    --
    -- Expression_Statement ::= Expression<literal=False> ';'
    --
    ----------------------------------------------------------------------------

    -- A Scribble Parser parses expressions and function definitions from a
    -- text stream, returning an abstract syntax tree representing the language
    -- construct that was parsed.
    type Parser is tagged limited private;
    type A_Parser is access all Parser'Class;

    -- Creates a new Scribble parser.
    function Create_Parser( runtime : not null A_Scribble_Runtime ) return A_Parser;
    pragma Postcondition( Create_Parser'Result /= null );

    -- Enables or disables debugging info included in subsequent parsed items.
    procedure Enable_Debug( this : not null access Parser'Class; enabled : Boolean );

    -- Raises a Parse_Error exception if any source code remains in the stream,
    -- not including comments and whitespace.
    procedure Expect_EOF( this : not null access Parser'Class );

    -- Parses and returns a single Value written with Scribble syntax. Comments
    -- are ignored. The return object may be an Ast_Literal, Ast_List, Ast_Map,
    -- or Ast_Function_Definition. A Parse_Error will be raised if an error
    -- occurs during parsing, or if no literal Value is found.
    function Expect_Value( this : not null access Parser'Class ) return A_Ast_Expression;
    pragma Postcondition( Expect_Value'Result.all.Get_Type in Ast_Value_Type );

    -- Returns all of the source code that has been parsed from the stream since
    -- Set_Input() was called.
    function Get_Source( this : not null access Parser'Class ) return Unbounded_String;

    -- Sets the input stream to parse, read from file 'path'. The path is used
    -- for debug info and error messages. Ownership of 'strm' is maintained by
    -- the caller. Pass a null 'strm' to remove the stream before deleting it.
    procedure Set_Input( this : not null access Parser'Class;
                         strm : access Root_Stream_Type'Class;
                         path : String := "" );

    -- Deletes the parser.
    procedure Delete( this : in out A_Parser );
    pragma Postcondition( this = null );

private

    package Tokens is new Scribble.Generic_Tokens(Token_Type, Name, To_Token_Type, TK_SYMBOL, TK_VALUE, TK_EOL, TK_EOF, Is_Delimiter);
    package Tokenizers is new Scribble.Tokenizers(Tokens);
    package Scanners is new Scribble.Scanners(Tokenizers);
    use Scanners;

    type Optional_Item is (Disallowed, Optional, Required);

    type Parser is tagged limited
        record
            path        : Unbounded_String;
            scanner     : A_Scanner := null;
            runtime     : A_Scribble_Runtime := null;
            enableDebug : Boolean := False;
        end record;

    procedure Construct( this : access Parser; runtime : not null A_Scribble_Runtime );

    procedure Delete( this : in out Parser );

    -- Parses an expression from the token scanner. A Parse_Exception will be
    -- raised if an Expression term is not found, or if it must be literal but
    -- it contains non-literal elements, or if a malformed token is encountered
    -- by the scanner.
    function Expect_Expression( this : not null access Parser'Class; literal : Boolean ) return A_Ast_Expression;
    pragma Postcondition( Expect_Expression'Result /= null );

    -- Parses an Expression from the token scanner. See the grammar below. If
    -- 'literal' is True, then the expression must conform to the literal subset
    -- of the Expression syntax.
    --
    -- If no Expression is found, null will be returned. A Parse_Error will be
    -- raised if the language syntax is violated; for example, if an expected
    -- grammar term is not found, or if the expression must be literal but it
    -- contains non-literal elements, or if a malformed token is encountered by
    -- the scanner.
    --
    -- Expression<literal> ::= Assign_Term<literal>
    --
    function Scan_Expression( this : not null access Parser'Class; literal : Boolean ) return A_Ast_Expression;

    -- Parses an Expression_List term from the token scanner; a comma-separated
    -- list of Expressions or anonymous function definitions. See the grammar
    -- below. If no valid list elements are found, the list returned will be
    -- empty. A Parse_Error will be raised if the language syntax is violated;
    -- for example, if an expected grammar term is not found, or if a malformed
    -- token is encountered by the scanner.
    --
    -- Expression_List<literal> ::= (Anon_Function_Definition | Expression<literal>) [(',' | end-of-line) Expression_List<literal>]
    --
    -- For every comma found, a following Expression is expected.
    function Scan_Expression_List( this : not null access Parser'Class; literal : Boolean ) return Expression_Vectors.Vector;

    -- Parses an Assign_Term from the token scanner. See the gramar below. If no
    -- Assign_Term is found, null will be returned. A Parse_Error will be raised
    -- if the language syntax is violated; for example, if an expected grammar
    -- term is not found, or if a malformed token is encountered by the scanner.
    --
    -- Assign_Term<literal=False> ::= Ternary_Term<literal> [Assign_Op (Anon_Function_Definition | Assign_Term)]
    -- Assign_Term<literal=True>  ::= Ternary_Term<literal>
    --   Assign_Op ::= ':=' | '+=' | '-=' | '*=' | '/=' | '&='
    --
    -- If an Assign_Op is found then all of the following terms are expected.
    function Scan_Assign_Term( this : not null access Parser'Class; literal : Boolean ) return A_Ast_Expression;

    -- Parses a Ternary_Term from the token scanner. See the grammar below. If
    -- no Ternary_Term is found, null will be returned. A Parse_Error will be
    -- raised if the language syntax is violated; for example, if an expected
    -- grammar term is not found, or if a malformed token is encountered by the
    -- scanner.
    --
    -- Ternary_Term<literal> ::= Binary_Term<literal> ['?' Ternary_Term<literal> ':' Ternary_Term<literal>]
    --
    -- If the question mark is found then all of the following terms are expected.
    function Scan_Ternary_Term( this : not null access Parser'Class; literal : Boolean ) return A_Ast_Expression;

    -- Parses a Binary_Term from the token scanner. See the grammar below. This
    -- function will recursively scan as long as the Binary_Op encountered after
    -- Lterm has an operator precedence greater than 'precedence'. If a binary
    -- operator of the same or lesser precedence is encountered it will remain
    -- in the token stream and only Lterm will parsed and returned. If no
    -- Binary_Term is found, null will be returned. A Parse_Error will be raised
    -- if the language syntax is violated; for example, if an expected grammar
    -- term is not found, or if a malformed token is encountered by the scanner.
    --
    -- Binary_Term<literal> ::= Lterm<literal> [Binary_Op Binary_Term<literal>]
    --   Binary_Op ::= 'and' | 'or' | 'xor' | 'in' | '=' | '>' | '>=' | '<' |
    --                 '<=' | '!=' | '+' | '-' | '&' | '*' | '/' | '%' | '^'
    --
    -- Operator precedence (greatest to least):
    --    Exponentiation   ^
    --    Multiplication   * / %
    --    Addition         + - &
    --    Comparison       = > >= < <= !=
    --    Logical          and or xor in
    --
    -- If a higher precedence Binary_Op is found then the following Binary_Term
    -- is expected.
    function Scan_Binary_Term( this       : not null access Parser'Class;
                               literal    : Boolean;
                               precedence : Integer := 0 ) return A_Ast_Expression;

    -- Parses an Lterm (a term with an optional unary operator on the left) from
    -- the token scanner. See the grammar below. If no Lterm is found, null will
    -- be returned. A Parse_Error will be raised if the language syntax is
    -- violated; for example, if an expected grammar term is not found, or if
    -- a malformed token is encountered by the scanner.
    --
    -- Lterm<literal> ::= Unary_Op<literal> Lterm<literal>
    -- Lterm<literal> ::= Rterm<literal>
    --   Unary_Op<literal=False> ::= '!' | '-' | '@'
    --   Unary_Op<literal=True>  ::= '!' | '-'
    --
    -- In the first production, if the unary_op is found then the following
    -- Lterm is expected.
    function Scan_Lterm( this : not null access Parser'Class; literal : Boolean ) return A_Ast_Expression;

    -- Parses an Rterm (a term with optional operators on the right, not to be
    -- confused with binary operators) from the token scanner. See the grammar
    -- below. If no Rterm is found, null will be returned. A Parse_Error will be
    -- raised if the language syntax is violated; for example, if an expected
    -- grammar term is not found, or if a malformed token is encountered by the
    -- scanner.
    --
    -- Rterm<literal=False> ::= Operand<literal> {Index | Function_Call | Membership}
    -- Rterm<literal=True>  ::= Operand<literal>
    --
    function Scan_Rterm( this : not null access Parser'Class; literal : Boolean ) return A_Ast_Expression;

    -- Parses an Index operation from the token scanner, given the container
    -- expression (left-hand side). See the grammar below. If an Index is found,
    -- 'containerExpr' will be consumed. If no Index is found, null will be
    -- returned and 'containerExpr' will not be consumed. A Parse_Error will be
    -- raised if the language syntax is violated; for example, if an expected
    -- grammar term is not found, or if a malformed token is encountered by the
    -- scanner.
    --
    -- Index ::= '[' Expression<literal=False> ']'
    --
    -- If the opening bracket is found then all of the following terms are
    -- expected.
    function Scan_Index( this          : not null access Parser'Class;
                         containerExpr : not null A_Ast_Expression ) return A_Ast_Expression;

    -- Parses a Function_Call operation from the token scanner, given the
    -- function expression (left-hand side). See the grammar below. If a
    -- Function_Call operation is found, 'funcExpr' will be consumed. If no
    -- Function_Call is found, null will be returned and 'funcExpr' will not be
    -- consumed. A Parse_Error will be raised if the language syntax is
    -- violated; for example, if an expected grammar term is not found, or if a
    -- malformed token is encountered by the scanner. When an exception is
    -- thrown, 'funcExpr' is not consumed.
    --
    -- Function_Call ::= '(' [Expression_List<literal=False>] ')'
    --
    -- If the opening parenthesis is found then all of the following terms are
    -- expected.
    function Scan_Function_Call( this     : not null access Parser'Class;
                                 funcExpr : not null A_Ast_Expression ) return A_Ast_Expression;

    -- Parses a Membership operation from the token scanner, given the
    -- left-hand expression. See the grammar below. If a Membership operation
    -- is found, 'left' will be consumed. If no Membership is found, null will
    -- be returned and 'left' will not be consumed. A Parse_Error will be raised
    -- if the language syntax is violated; for example, if an expected grammar
    -- term is not found, or if a malformed token is encountered by the scanner.
    -- When an exception is thrown, 'left' is not consumed.
    --
    -- Membership ::= '.' Identifier
    --
    -- If the dot is found then the following identifier is expected.
    function Scan_Membership( this : not null access Parser'Class;
                              left : not null A_Ast_Expression ) return A_Ast_Expression;

    -- Parses an Operand from the token scanner. See the grammar below. If no
    -- Operand is found, null will be returned. A Parse_Error will be raised if
    -- the language syntax is violated; for example, if an expected grammar term
    -- is not found, or if a malformed token is encountered by the scanner.
    --
    -- Operand<literal=False> ::= Literal<literal> | Identifier_Expression | ('(' Expression<literal> ')')
    -- Operand<literal=True>  ::= Literal<literal> | ('(' Expression<literal> ')')
    --
    -- In the third production, if the opening parenthensis is found then an
    -- expression is expected.
    function Scan_Operand( this : not null access Parser'Class; literal : Boolean ) return A_Ast_Expression;

    -- Parses a Literal value from the token scanner. See the grammar below. If
    -- no Literal is found, null will be returned. A Parse_Error will be raised
    -- if the language syntax is violated; for example, if an expected grammar
    -- term is not found, or if a malformed token is encountered by the scanner.
    --
    -- Literal<literal=False>  ::= value-token | 'self' | 'this' | List_Literal<literal> | Map_Literal<literal>
    -- Literal<literal=True>   ::= value-token | List_Literal<literal> | Map_Literal<literal>
    --   List_Literal<literal> ::= '[' [Expression_List<literal>] ']'
    --   Map_Literal<literal>  ::= '{' [Map_Pair<literal> {(',' | end-of-line) Map_Pair<literal>}] '}'
    --     Map_Pair<literal>   ::= Named_Function_Definition
    --                             | Named_Member_Definition
    --                             | (Map_Key ':' (Anon_Function_Definition | Anon_Member_Definition | Expression<literal>))
    --       Map_Key           ::= string-value-token | symbol-token
    --
    -- In the List_Literal rule, if the opening bracket is found then the
    -- remainder is expected. In the Map_Literal rule, if the opening brace is
    -- found then the remainer is expected.
    function Scan_Literal( this : not null access Parser'Class; literal : Boolean ) return A_Ast_Expression;

    -- Parses an Identifier_Expression from the token scanner. See the grammar
    -- below. If not Identifier_Expression is found, null will be returned. A
    -- Parse_Error will be raised if the language syntax is violated; for
    -- example, if an expected grammar term is not found, or if a malformed
    -- token is encountered by the scanner.
    --
    -- Identifier_Expression ::= Identifier ['::' (Identifier | Name_Expression)]
    --   Name_Expression     ::= '[' Expression<literal=False> ']'
    --
    -- If the double colon (namespace) operator is found then the following
    -- Identifier or Name_Expression is expected.
    function Scan_Identifier_Expression( this : not null access Parser'Class ) return A_Ast_Expression;

    -- Parses a Function_Definition from the token scanner. See the grammar
    -- below. the 'nameOption' argument specifies whether a function name is
    -- optional (accepts Anon_Function_Definition, Anon_Member_Definition,
    -- Named_Function_Definition, or Named_Member_Definition), disallowed
    -- (accepts only Anon_Function_Definition and Anon_Member_Definition), or
    -- required (accepts only Named_Function_Definition and
    -- Named_Member_Definition). If no function definition is found, null will
    -- be returned. If 'memberOption' is Optional or Required, the 'function'
    -- keyword may (or must) be prefixed with the 'member' keyword, making the
    -- function definition a member function. A member function has an implicit
    -- first parameter 'this' that must be passed by reference. A Parse_Error
    -- will be raised if the language syntax is violated; for example, if an
    -- expected grammar term is not found, or if a malformed token is
    -- encountered by the scanner.
    --
    -- If 'nameOption' is 'Disallowed' and 'function' is found followed by an
    -- Identifer, a Parse_Error will be raised. If 'nameOption' is Required and
    -- 'function' is found followed by anything other than an Identifier, the
    -- Named_Function_Definition is considered to be not found and null will be
    -- returned; scanned tokens will be pushed back.
    --
    -- Anon_Function_Definition  ::= 'function' Function_Definition
    -- Anon_Member_Definition    ::= 'member' ['function'] Function_Definition
    -- Named_Function_Definition ::= 'function' Identifier Function_Definition
    -- Named_Member_Definition   ::= 'member' ['function'] Identifier Function_Definition
    --   Function_Definition     ::= Parameters_Prototype Function_Implementation
    --   Parameters_Prototype    ::= '(' [Parameter {',' Parameter}] ')'
    --   Parameter               ::= Identifier [':' Value_Type] [':=' Expression<literal=True>]
    --   Value_Type              ::= 'map' | 'boolean' | 'id' | 'list' | 'number' | 'string'
    --   Function_Implementation ::= (':' Identifier) | (Statement_List 'end' 'function')
    --
    function Scan_Function_Definition( this         : not null access Parser'Class;
                                       nameOption   : Optional_Item;
                                       nameLoc      : Token_Location := (1, 1, 1, others => <>);
                                       nameString   : String := "";
                                       memberOption : Optional_Item := Disallowed
                                     ) return A_Ast_Function_Definition;

    -- Scans and returns the next statement of any kind. See the grammar below.
    -- All extraneous semicolons are ignored, to skip empty statements.
    --
    -- Statement ::= {';'} (
    --               Block
    --               | Exit
    --               | For
    --               | If
    --               | Loop
    --               | Return
    --               | Var
    --               | While
    --               | Expression_Statement
    --               ) {';'}
    --
    function Scan_Statement( this   : not null access Parser'Class;
                             parent : A_Ast_Block ) return A_Ast_Statement;

    -- Parses a Block statement from the token scanner. See the grammar below.
    -- 'parent' is the parent statement block containing the statement. If no
    -- Block is found, null will be returned. A Parse_Error will be raised if
    -- the language syntax is violated; for example, if an expected grammar term
    -- is not found, or if a malformed token is encountered by the scanner.
    --
    -- Block ::= 'block' Statement_List 'end' 'block' ';'
    --
    -- If the 'block' is found then the following terms are expected.
    function Scan_Block( this   : not null access Parser'Class;
                         parent : A_Ast_Block ) return A_Ast_Statement;

    -- Parses an Exit statement from the token scanner. See the grammar below.
    -- 'parent' is the parent statement block containing the statement. If no
    -- Exit is found, null will be returned. A Parse_Error will be raised if the
    -- language syntax is violated; for example, if an expected grammar term is
    -- not found, or if a malformed token is encountered by the scanner.
    --
    -- If the optional 'when' clause is found, the actual Ast_Statement class
    -- returned will be an Ast_If node enclosing the Ast_Exit node.
    --
    -- Exit ::= 'exit' ['when' Expression<literal=False>] ';'
    --
    -- If the 'exit' token is found then the following terms are interpreted as
    -- an Exit statement.
    function Scan_Exit( this   : not null access Parser'Class;
                        parent : A_Ast_Block ) return A_Ast_Statement;

    -- Parses an Expression as a statement from the token scanner. See the
    -- grammar below. If no Expression is found, null will be returned. A
    -- Parse_Error will be raised if the language syntax is violated; for
    -- example, if an expected grammar term is not found, or if a malformed
    -- token is encountered by the scanner.
    --
    -- Expression_Statement ::= Expression<literal=False> ';'
    --
    -- If an Expression is found then the semicolon is expected.
    function Scan_Expression_Statement( this : not null access Parser'Class ) return A_Ast_Statement;

    -- Parses a For statement from the token scanner. See the grammar below. If
    -- no For is found, null will be returned. A Parse_Error will be raised if
    -- the language syntax is violated; for example, if an expected grammar term
    -- is not found, or if a malformed token is encountered by the scanner.
    --
    -- For         ::= 'for' Identifier (For_Range | For_Each) Loop
    --   For_Range ::= 'is' Expression<literal=False> 'to' Expression<literal=False> ['step' Expression<literal=False>]
    --   For_Each  ::= 'of' Expression<literal=False>
    --
    -- If a 'for' keyword is found then the following terms are interpreted as a
    -- For statement.
    function Scan_For( this   : not null access Parser'Class;
                       parent : A_Ast_Block ) return A_Ast_Statement;

    -- Parses an If statement from the token scanner. See the grammar below.
    -- 'parent' is the parent statement block containing the statement. If no If
    -- statement is found, null will be returned. A Parse_Error will be raised
    -- if the language syntax is violated; for example, if an expected grammar
    -- term is not found, or if a malformed token is encountered by the scanner.
    --
    -- If ::= 'if' Expression<literal=False> 'then' Statement_List
    --        {'elsif' Expression<literal=False> 'then' Statement_List}
    --        ['else' Statement_List]
    --        'end' 'if' ';'
    --
    -- If an 'if' keyword is found then the following Expression, 'then', and
    -- statement list are expected.
    --
    -- If an 'elsif' keyword is found after the 'if' statement list, or after
    -- another 'elsif' statement list, then the following Expression, 'then' and
    -- statement list are expected.
    --
    -- If an 'else' keyword is found then the following statement list is
    -- expected.
    function Scan_If( this   : not null access Parser'Class;
                      parent : A_Ast_Block ) return A_Ast_Statement;

    -- Parses a Loop statement from the token scanner. See the grammar below.
    -- 'parent' is the parent statement block containing the statement. If no
    -- Loop statement is found, null will be returned. A Parse_Error will be
    -- raised if the language syntax is violated; for example, if an expected
    -- grammar term is not found, or if a malformed token is encountered by the
    -- scanner.
    --
    -- Loop ::= 'loop' Statement_List 'end' 'loop' ';'
    --
    -- If the 'loop' keyword is found then the following terms are expected.
    function Scan_Loop( this   : not null access Parser'Class;
                        parent : A_Ast_Block ) return A_Ast_Loop;

    -- Parses a Return statement from the token scanner. See the grammar below.
    -- If no Return is found, null will be returned. A Parse_Error will be
    -- raised if the language syntax is violated; for example, if an expected
    -- grammar term is not found, or if a malformed token is encountered by the
    -- scanner.
    --
    -- Return ::= 'return' [Expression<literal=False> | Anon_Function_Definition] ';'
    --
    -- If the 'return' keyword is found then the terms following it are expected.
    function Scan_Return( this : not null access Parser'Class ) return A_Ast_Statement;

    -- Parses a variable declaration statement from the token scanner. See the
    -- grammar below. If no Var is found, null will be returned. A Parse_Error
    -- will be raised if the language syntax is violated; for example, if an
    -- expected grammar term is not found, or if a malformed token is
    -- encountered by the scanner.
    --
    -- Var ::= 'local' Identifier [':=' (Expression<literal=False> | Anon_Function_Definition)] ';'
    -- Var ::= 'constant' Identifier ':=' (Expression<literal=False> | Anon_Function_Definition) ';'
    -- Var ::= Named_Function_Definition ';'
    --
    -- If the 'local' or 'constant' keyword is found, or the 'function' keyword
    -- followed by an Identifier is found, then the terms following it
    -- are expected.
    function Scan_Var( this : not null access Parser'Class ) return A_Ast_Statement;

    -- Parses a While statement from the token scanner. See the grammar below.
    -- If no While is found, null will be returned. A Parse_Error will be raised
    -- if the language syntax is violated; for example, if an expected grammar
    -- term is not found, or if a malformed token is encountered by the scanner.
    --
    -- While ::= 'while' Expression<literal=False> Loop
    --
    -- If a 'while' keyword is found then the following terms are interpreted as
    -- a While statement.
    function Scan_While( this   : not null access Parser'Class;
                         parent : A_Ast_Block ) return A_Ast_Statement;

    -- Parses a yield statement from the token scanner. See the grammar below.
    -- If no Yield is found, null will be returned. A Parse_Error will be raised
    -- if the language syntax is violated; for example, if an expected grammar
    -- term is not found, or if a malformed token is encountered by the scanner.
    --
    -- Yield ::= 'yield' ';'
    --
    -- If a 'yield' keyword is found then the following terms are interpreted as
    -- a Yield statement.
    function Scan_Yield( this : not null access Parser'Class ) return A_Ast_Statement;

    -- Parses a series of statements from the token scanner, returning an
    -- Ast_Block. See the grammar below. A Parse_Error will be raised if the
    -- language syntax is violated; for example, if an expected grammar term is
    -- not found, or if a malformed token is encountered by the scanner.
    --
    -- Blocks are special statements in the sense that they are containers for
    -- other statements. They provide no behavior, but they encapsulate an inner
    -- scope for local variables. 'parent' is a reference to the block's parent
    -- block, or null if the block is a top-level block, like a function body.
    -- The block is ended by the first non-statement encountered by the parser.
    --
    -- Statement blocks can be empty. null will never be returned by this method.
    --
    -- Statement_List ::= {Statement}
    --
    function Scan_Statement_List( this   : not null access Parser'Class;
                                  parent : A_Ast_Block ) return A_Ast_Block;
    pragma Postcondition( Scan_Statement_List'Result /= null );

    -- Parses and returns a single literal value written with Scribble syntax,
    -- including a function definition. The top-level Value to be scanned may be
    -- a Named_Function_Definition. Comments are ignored. See the syntax below.
    -- A Parse_Error will be raised if an error occurs during parsing.
    --
    -- Value         ::= value-token
    --                   | ('-' number-value-token)
    --                   | List_Value
    --                   | Map_Value
    --                   | Anon_Function_Definition
    --                   | Named_Function_Definition
    --   List_Value  ::= '[' [Expression_List<literal=True>] ']'
    --   Map_Value   ::= '{' [Map_Pair {(',' | end-of-line) Map_Pair}] '}'
    --     Map_Pair  ::= Named_Function_Definition
    --                   | Named_Member_Definition
    --                   | (Map_Key ':' (Anon_Function_Definition | Anon_Member_Definition | Expression<literal=True>))
    --       Map_Key ::= string-value-token | symbol-token
    --
    function Scan_Value( this : not null access Parser'Class ) return A_Ast_Expression;

end Scribble.Parsers;
