--
-- Copyright (c) 2014-2015 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Values.Lists;                      use Values.Lists;

package body Values.Strings.Operations is

    function Split( source    : Unbounded_String;
                    delim     : String;
                    keepEmpty : Boolean := True ) return Value is
        result : constant List_Value := As_List( Create_List );
        pos    : Integer := 1;
        next   : Integer;
    begin
        if Length( source ) = 0 then
            return Value(result);
        elsif delim'Length = 0 then
            result.Append( source );
            return Value(result);
        end if;

        loop
            next := Index( source, delim, pos );
            exit when next < pos;

            if next > pos or else keepEmpty then
                result.Append( Slice( source, pos, next - 1 ) );
            end if;

            pos := next + delim'Length;
        end loop;

        if pos <= Length( source ) then
            result.Append( Slice( source, pos, Length( source ) ) );
        elsif keepEmpty then
            result.Append( "" );
        end if;

        return Value(result);
    end Split;

end Values.Strings.Operations;
