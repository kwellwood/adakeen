--
-- Copyright (c) 2013-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Scribble.Token_Locations;          use Scribble.Token_Locations;

limited with Scribble.Ast.Processors;

private package Scribble.Ast is

    -- Identifies the class of a node in the abstract syntax tree
    type Ast_Type is new Positive;

    LITERAL             : constant Ast_Type := 1;    -- value
    MAP                 : constant Ast_Type := 2;    -- value, container
    LIST                : constant Ast_Type := 3;    -- value, container
    FUNCTION_DEFINITION : constant Ast_Type := 4;    -- value
    FUNCTION_CALL       : constant Ast_Type := 5;
    MEMBER_CALL         : constant Ast_Type := 6;
    SELF                : constant Ast_Type := 7;
    BUILTIN             : constant Ast_Type := 8;

    IDENTIFIER          : constant Ast_Type := 9;    -- symbol
    THIS_ARG            : constant Ast_Type := 10;   -- symbol
    MEMBERSHIP          : constant Ast_Type := 11;   -- symbol
    NAME_EXPRESSION     : constant Ast_Type := 12;   -- symbol

    UNARY_OP            : constant Ast_Type := 13;
    REFERENCE_OP        : constant Ast_Type := 14;
    BINARY_OP           : constant Ast_Type := 15;
    ASSIGN_OP           : constant Ast_Type := 16;
    INDEX_OP            : constant Ast_Type := 17;
    CONDITIONAL_OP      : constant Ast_Type := 18;

    STATEMENT           : constant Ast_Type := 19;
    BLOCK_STATEMENT     : constant Ast_Type := 20;
    EXIT_STATEMENT      : constant Ast_Type := 21;
    IF_STATEMENT        : constant Ast_Type := 22;
    LOOP_STATEMENT      : constant Ast_Type := 23;
    RETURN_STATEMENT    : constant Ast_Type := 24;
    VAR_STATEMENT       : constant Ast_Type := 25;

    USER_AST_START      : constant Ast_Type := 1024;

    subtype Ast_Value_Type is Ast_Type range LITERAL..FUNCTION_DEFINITION;

    subtype Ast_Container_Type is Ast_Type range MAP..LIST;

    subtype Ast_Symbol_Type is Ast_Type range IDENTIFIER..NAME_EXPRESSION;

    ----------------------------------------------------------------------------

    -- Ast_Node is the abstract base class of every node in the abstract syntax
    -- tree.
    --
    -- The syntax tree is composed primarily of statement trees (Ast_Statement)
    -- and expressions (Ast_Expression), using the Composite pattern to model
    -- branches as trees of nodes with a common base class.
    --
    -- This class supports the Visitor pattern via the Ast_Processor interface
    -- declared this package. Each node in the tree is also capable of reporting
    -- its node type with the Get_Type function.
    type Ast_Node is abstract tagged limited private;
    type A_Ast_Node is access all Ast_Node'Class;

    procedure Construct( this : access Ast_Node; loc : Token_Location );

    procedure Delete( this : in out Ast_Node ) is null;

    -- Returns the node's actual type.
    function Get_Type( this : access Ast_Node ) return Ast_Type is abstract;

    -- Returns the node's corresponding location in the source code.
    function Location( this : not null access Ast_Node'Class ) return Token_Location;

    -- Visits the node with an Ast_Processor, calling the method of the visitor
    -- that corresponds with the class of this node.
    procedure Process( this      : access Ast_Node;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class ) is abstract;

    -- Sets the node's corresponding location in the source code.
    procedure Set_Location( this : not null access Ast_Node'Class;
                            loc  : Token_Location );

    -- Deletes the node and all its children.
    procedure Delete( this : in out A_Ast_Node );
    pragma Postcondition( this = null );

private

    type Ast_Node is abstract tagged limited
        record
            loc : Token_Location := UNKNOWN_LOCATION;
        end record;

end Scribble.Ast;
