--
-- Copyright (c) 2015-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Float_Text_IO;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Values.Errors;
with Values.Functions;
with Values.Indirects;
with Values.Lists;
with Values.Maps;
with Values.Strings;

pragma Elaborate( Values.Errors );
pragma Elaborate( Values.Functions );
pragma Elaborate( Values.Indirects );
pragma Elaborate( Values.Lists );
pragma Elaborate( Values.Maps );
pragma Elaborate( Values.Strings );

package body Values is

    package Object_Accesses is new System.Address_To_Access_Conversions( Object_Refs );

    ----------------------------------------------------------------------------

    function Atomic_Add( ptr : access Counter;
                         val : Counter ) return Counter is

        function Intrinsic_Sync_Add_And_Fetch( ptr : access Counter;
                                               val : Counter ) return Counter;
        pragma Import( Intrinsic, Intrinsic_Sync_Add_And_Fetch, "__sync_add_and_fetch_4" );
    begin
        return Intrinsic_Sync_Add_And_Fetch( ptr, val );
    end Atomic_Add;

    ----------------------------------------------------------------------------

    type Value_Type_Array is array (Integer range 2#0_000#..2#1_111#) of Value_Type;

    -- This table only maps Value type tags to Value_Type enumeration values.
    -- The SINGLETON_TYPE is ambiguous, mapping to either V_NULL or V_BOOLEAN.
    -- The singleton type must be disambiguated by its payload before looking
    -- it up in this table. The SINGLETON_TYPE entry here maps to V_BOOLEAN,
    -- since the null payload is easier to explicitly test.
    type_table : constant Value_Type_Array :=
    (
        SINGLETON_TYPE => V_BOOLEAN,
        ID_TYPE        => V_ID,
        REF_TYPE       => V_REFERENCE,
        COLOR_TYPE     => V_COLOR,
        STRING_TYPE    => V_STRING,
        LIST_TYPE      => V_LIST,
        MAP_TYPE       => V_MAP,
        FUNCTION_TYPE  => V_FUNCTION,
        ERROR_TYPE     => V_ERROR,
        INDIRECT_TYPE  => V_REFERENCE,     -- extension of REF_TYPE
        others         => V_NULL
    );

    ----------------------------------------------------------------------------

    type Comparator is access function( left, right : Value'Class ) return Integer;

    type Comparator_Array is array (Integer range 2#0_000#..2#1_111#) of Comparator;

    pragma Suppress(Elaboration_Check, Strings.Compare_Strings);
    pragma Suppress(Elaboration_Check, Lists.Compare_Lists);
    pragma Suppress(Elaboration_Check, Maps.Compare_Maps);
    pragma Suppress(Elaboration_Check, Errors.Compare_Errors);

    comparators : constant Comparator_Array :=
    (
        STRING_TYPE   => Strings.Compare_Strings'Access,
        LIST_TYPE     => Lists.Compare_Lists'Access,
        MAP_TYPE      => Maps.Compare_Maps'Access,
        ERROR_TYPE    => Errors.Compare_Errors'Access,
        others        => null
    );

    ----------------------------------------------------------------------------

    type Destructor is access procedure( obj : Address );

    type Destructor_Array is array (Integer range 2#1_000#..2#1_111#) of Destructor;

    pragma Suppress(Elaboration_Check, Strings.Delete_String);
    pragma Suppress(Elaboration_Check, Lists.Delete_List);
    pragma Suppress(Elaboration_Check, Maps.Delete_Map);
    pragma Suppress(Elaboration_Check, Functions.Delete_Function);
    pragma Suppress(Elaboration_Check, Errors.Delete_Error);
    pragma Suppress(Elaboration_Check, Indirects.Delete_Indirect);

    destructors : constant Destructor_Array :=
    (
        STRING_TYPE   => Strings.Delete_String'Access,
        LIST_TYPE     => Lists.Delete_List'Access,
        MAP_TYPE      => Maps.Delete_Map'Access,
        FUNCTION_TYPE => Functions.Delete_Function'Access,
        ERROR_TYPE    => Errors.Delete_Error'Access,
        INDIRECT_TYPE => Indirects.Delete_Indirect'Access,
        others        => null
    );

    ----------------------------------------------------------------------------

    type Imager is access function( this : Value'Class ) return String;

    type Imager_Array is array (Integer range 2#0_000#..2#1_111#) of Imager;

    function Image_Singleton( this : Value'Class ) return String;
    function Image_Id       ( this : Value'Class ) return String;
    function Image_Ref      ( this : Value'Class ) return String;
    function Image_Color    ( this : Value'Class ) return String;

    pragma Suppress(Elaboration_Check, Strings.Image_String);
    pragma Suppress(Elaboration_Check, Lists.Image_List);
    pragma Suppress(Elaboration_Check, Maps.Image_Map);
    pragma Suppress(Elaboration_Check, Functions.Image_Function);
    pragma Suppress(Elaboration_Check, Errors.Image_Error);
    pragma Suppress(Elaboration_Check, Indirects.Image_Indirect);

    imagers : constant Imager_Array :=
    (
        SINGLETON_TYPE => Image_Singleton'Access,
        ID_TYPE        => Image_Id'Access,
        REF_TYPE       => Image_Ref'Access,
        COLOR_TYPE     => Image_Color'Access,
        STRING_TYPE    => Strings.Image_String'Access,
        LIST_TYPE      => Lists.Image_List'Access,
        MAP_TYPE       => Maps.Image_Map'Access,
        FUNCTION_TYPE  => Functions.Image_Function'Access,
        ERROR_TYPE     => Errors.Image_Error'Access,
        INDIRECT_TYPE  => Indirects.Image_Indirect'Access,
        others         => null
    );

    --==========================================================================

    function Image( vt : Value_Type ) return String is
    begin
        case vt is
            when V_NULL      => return "null";
            when V_BOOLEAN   => return "boolean";
            when V_NUMBER    => return "number";
            when V_STRING    => return "string";
            when V_COLOR     => return "color";
            when V_ID        => return "id";
            when V_LIST      => return "list";
            when V_MAP       => return "map";
            when V_FUNCTION  => return "function";
            when V_REFERENCE => return "reference";
            when V_ERROR     => return "error";
        end case;
    end Image;

    --==========================================================================
    -- Values

    overriding
    procedure Adjust( this : in out Value ) is
        thisObj : Address;
        tmp     : Counter;
        pragma Unreferenced( tmp );
    begin
        if this.Is_Object then
            thisObj := this.Get_Address;
            if thisObj /= Null_Address then
                tmp := Atomic_Add( Object_Accesses.To_Pointer( thisObj ).refs'Access, 1 );
            end if;
        end if;
    end Adjust;

    ----------------------------------------------------------------------------

    function Compare( this : Value'Class; other : Value'Class ) return Integer is
        thisType  : Integer;
        otherType : Integer;
    begin
        if this.v = other.v then
            return 0;
        end if;

        -- check if either value is a number
        if this.Is_Number then
            if other.Is_Number then
                -- compare two numbers
                if U64_To_F64( this.v ) > U64_To_F64( other.v ) then
                    return 1;
                else
                    return -1;
                end if;
            end if;
            return 1;    -- numbers are greater than all other types
        elsif other.Is_Number then
            return -1;   -- numbers are greater than all other types
        end if;

        thisType := this.Get_Type;
        otherType := other.Get_Type;

        -- compare matching types
        if thisType = otherType then
            -- use a type-specific comparator function
            if comparators(thisType) /= null then
                return comparators(thisType).all(this, other);
            end if;

            -- compare the payload
            if this.v > other.v then
                return 1;
            else
                return -1;
            end if;

        -- compare by type
        elsif thisType > otherType then
            return 1;
        else
            return -1;
        end if;
    end Compare;

    ----------------------------------------------------------------------------

    overriding
    procedure Finalize( this : in out Value ) is
        thisObj : Address;
    begin
        if this.Is_Object then
            thisObj := this.Get_Address;
            if thisObj /= Null_Address and then Atomic_Add( Object_Accesses.To_Pointer( thisObj ).refs'Access, -1 ) = 0 then
                -- free the object using the type destructor table
                destructors(this.Get_Type).all( thisObj );
            end if;
            this.Set_Address( Null_Address );
        end if;
    end Finalize;

    ----------------------------------------------------------------------------

    -- Returns the payload of the value as an address.
    function Get_Address( this : Value'Class ) return Address is
        type Uint_Addr is mod 2 ** Standard'Address_Size;
        function To_Addr is new Ada.Unchecked_Conversion( Uint_Addr, Address );
    begin
        return To_Addr( Uint_Addr(PAYLOAD_MASK and this.v) );
    end Get_Address;

    ----------------------------------------------------------------------------

    -- Assigns an address up to 48-bits wide into the payload of the value.
    procedure Set_Address( this : in out Value'Class; addr : Address ) is
        type Uint_Addr is mod 2 ** Standard'Address_Size;
        function From_Addr is new Ada.Unchecked_Conversion( Address, Uint_Addr );
    begin
        this.v := (this.v and not PAYLOAD_MASK) or Unsigned_64(From_Addr( addr ));
    end Set_Address;

    ----------------------------------------------------------------------------

    function Get_Type( this : Value'Class ) return Value_Type
        is (if this.Is_Number then V_NUMBER
            elsif this.v = TAG_NULL then V_NULL
            else type_table(this.Get_Type));

    ----------------------------------------------------------------------------

    function Image( this : Value'Class ) return String is

        function Image( f : Float; precision : Natural := 3 ) return String is
            txt : String(1..18) := (others => ' ');
        begin
            Ada.Float_Text_IO.Put( txt, f, Aft => precision, Exp => 0 );
            return Ada.Strings.Fixed.Trim( txt, Both );
        end Image;

    begin
        if this.Is_Number then
            if U64_To_F64( this.v ) = Long_Float'Rounding( U64_To_F64( this.v ) ) then
                return Trim( Long_Long_Integer'Image( Long_Long_Integer(U64_To_F64( this.v )) ), Left );
            else
                return Image( Float(U64_To_F64( this.v )), precision => 3 );
            end if;
        end if;
        return imagers(this.Get_Type).all( this );
    end Image;

    ----------------------------------------------------------------------------

    function Is_Null    ( this : Value'Class ) return Boolean is (this.v = TAG_NULL);
    function Is_Boolean ( this : Value'Class ) return Boolean is ((this.v = TAG_FALSE) or (this.v = TAG_TRUE));
    function Is_Number  ( this : Value'Class ) return Boolean is ((this.v and QNAN) /= QNAN);
    function Is_String  ( this : Value'Class ) return Boolean is ((this.v and FULL_TYPE_MASK) = TAG_STRING);
    function Is_Id      ( this : Value'Class ) return Boolean is ((this.v and FULL_TYPE_MASK) = TAG_ID);
    function Is_Color   ( this : Value'Class ) return Boolean is ((this.v and FULL_TYPE_MASK) = TAG_COLOR);
    function Is_List    ( this : Value'Class ) return Boolean is ((this.v and FULL_TYPE_MASK) = TAG_LIST);
    function Is_Map     ( this : Value'Class ) return Boolean is ((this.v and FULL_TYPE_MASK) = TAG_MAP);
    function Is_Function( this : Value'Class ) return Boolean is ((this.v and FULL_TYPE_MASK) = TAG_FUNCTION);
    function Is_Ref     ( this : Value'Class ) return Boolean is ((this.v and FULL_TYPE_MASK) = TAG_REF or else
                                                                  (this.v and FULL_TYPE_MASK) = TAG_INDIRECT);
    function Is_Error   ( this : Value'Class ) return Boolean is ((this.v and FULL_TYPE_MASK) = TAG_ERROR);

    ----------------------------------------------------------------------------

    overriding
    function "="( this : Value; other : Value ) return Boolean is (Value'Class(this).Compare( Value'Class(other) ) = 0);

    function "<"( this : Value'Class; other : Value'Class ) return Boolean is (this.Compare( other ) < 0);
    function ">"( this : Value'Class; other : Value'Class ) return Boolean is (this.Compare( other ) > 0);
    function "<="( this : Value'Class; other : Value'Class ) return Boolean is (this.Compare( other ) <= 0);
    function ">="( this : Value'Class; other : Value'Class ) return Boolean is (this.Compare( other ) >= 0);

    ----------------------------------------------------------------------------

    procedure Set_Object( this : in out Value'Class; obj : Address ) is
        thisObj : constant Address := this.Get_Address;
    begin
        if thisObj /= obj then
            if thisObj /= Null_Address then
                this.Finalize;    -- decrement reference count
            end if;

            if obj /= Null_Address then
                this.Set_Address( obj );
                this.Adjust;      -- increment reference count
            end if;
        end if;
    end Set_Object;

    --==========================================================================
    -- Singletons

    function Image_Singleton( this : Value'Class ) return String is
    begin
        case (this.v and PAYLOAD_MASK) is
            when 0 => return "null";
            when 1 => return "false";
            when 2 => return "true";
            when others => return "???";
        end case;
    end Image_Singleton;

    --==========================================================================
    -- Booleans

    function To_Boolean( this : Value'Class ) return Boolean is (this.v = TAG_TRUE);

    --==========================================================================
    -- Numbers

    function Is_Int( this : Value'Class ) return Boolean is (U64_To_F64( this.v ) = Long_Float'Rounding( U64_To_F64( this.v ) ));

    ----------------------------------------------------------------------------

    function To_Float( this : Value'Class ) return Float is (Float(U64_To_F64( this.v )));

    function To_Int( this : Value'Class ) return Integer is
    begin
        -- constrain Long_Float to the Integer range
        return Integer(Long_Float'Min( Long_Float'Max( Long_Float(Integer'First), U64_To_F64( this.v ) ), Long_Float(Integer'Last) ));
    end To_Int;

    function To_Long_Float( this : Value'Class ) return Long_Float is (U64_To_F64( this.v ));

    --==========================================================================
    -- Identifiers

    function Image_Id( this : Value'Class ) return String is
        hex : constant String := "0123456789ABCDEF";
        img : String(1..13) := "$000000000000";
        val : Unsigned_64 := (this.v and PAYLOAD_MASK);
    begin
        for i in reverse img'First+1..img'Last loop
            img(i) := hex(hex'First + Integer(val and 16#00000000_0000000F#));
            val := Shift_Right( val, 4 );
        end loop;
        return img;
    end Image_Id;

    ----------------------------------------------------------------------------

    function To_Id( this : Value'Class ) return Unsigned_64 is (this.v and PAYLOAD_MASK);

    --==========================================================================
    -- References

    function Deref( this : Value'Class ) return Value'Class is
    begin
        if (this.v and TAG_INDIRECT) = TAG_INDIRECT then
            return Indirects.Read_Indirect( this );
        else -- (this.v and TAG_REF) = TAG_REF
            return Value_Accesses.To_Pointer( this.Get_Address ).all;
        end if;
    end Deref;

    ----------------------------------------------------------------------------

    function Image_Ref( this : Value'Class ) return String is
    begin
        return '@' & Trim( Unsigned_64'Image( this.v and PAYLOAD_MASK ), Left );
    end Image_Ref;

    ----------------------------------------------------------------------------

    procedure Replace( this : Value'Class; val : Value'Class ) is
    begin
        if (this.v and TAG_INDIRECT) = TAG_INDIRECT then
            Indirects.Write_Indirect( this, val );
        else -- (this.v and TAG_REF) = TAG_REF
            Value_Accesses.To_Pointer( this.Get_Address ).all := val;
        end if;
    end Replace;

    --==========================================================================
    -- Colors

    function Image_Color( this : Value'Class ) return String is
        hex : constant String := "0123456789ABCDEF";
        img : String(1..9) := "#RRGGBBAA";
        val : Unsigned_64 := (this.v and PAYLOAD_MASK);
    begin
        for i in reverse img'First+1..img'Last loop
            img(i) := hex(hex'First + Integer(val and 16#00000000_0000000F#));
            val := Shift_Right( val, 4 );
        end loop;
        return img;
    end Image_Color;

    ----------------------------------------------------------------------------

    procedure To_RGB( this : Value'Class; r, g, b : out Float ) is
        a : Float := 0.0;
    begin
        this.To_RGBA( r, g, b, a );
    end To_RGB;

    ----------------------------------------------------------------------------

    procedure To_RGBA( this : Value'Class; r, g, b, a : out Float ) is
        val : constant Unsigned_64 := (this.v and PAYLOAD_MASK);
    begin
        r := Float(Shift_Right( val, 24 ) and 16#FF#) / 255.0;
        g := Float(Shift_Right( val, 16 ) and 16#FF#) / 255.0;
        b := Float(Shift_Right( val,  8 ) and 16#FF#) / 255.0;
        a := Float(             val       and 16#FF#) / 255.0;
    end To_RGBA;

    --==========================================================================
    -- Object Interfaces

    function Err( this : Value'Class ) return Errors.Error_Value is (Errors.As_Error( this ));

    function Func( this : Value'Class ) return Functions.Function_Value is (Functions.As_Function( this ));

    function Lst( this : Value'Class ) return Lists.List_Value is (Lists.As_List( this ));

    function Map( this : Value'Class ) return Maps.Map_Value is (Maps.As_Map( this ));

    function Str( this : Value'Class ) return Strings.String_Value is (Strings.As_String( this ));

    --==========================================================================

    procedure Delete( va : in out A_Value_Array ) is
        procedure Free is new Ada.Unchecked_Deallocation( Value_Array, A_Value_Array );
    begin
        Free( va );
    end Delete;

    --==========================================================================

    function Read_String( stream : access Root_Stream_Type'Class ) return Unbounded_String is
        str : String_Access := new String(1..Natural'Input( stream ));
    begin
        if str'Length > 0 then
            String'Read( stream, str.all );
        end if;
        return result : Unbounded_String do
            result := To_Unbounded_String( str.all );
            Free( str );
        end return;
    end Read_String;

    ----------------------------------------------------------------------------

    procedure Write_String( stream : access Root_Stream_Type'Class; str : String ) is
    begin
        Integer'Output( stream, str'Length );
        if str'Length > 0 then
            String'Write( stream, str );
        end if;
    end Write_String;

    ----------------------------------------------------------------------------

    procedure Write_String( stream : access Root_Stream_Type'Class; str : Unbounded_String ) is
    begin
        Integer'Output( stream, Length( str ) );
        if Length( str ) > 0 then
            String'Write( stream, To_String( str ) );
        end if;
    end Write_String;

end Values;
