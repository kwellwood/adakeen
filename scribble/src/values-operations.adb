--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Values.Errors;                     use Values.Errors;
with Values.Lists;                      use Values.Lists;
with Values.Maps;                       use Values.Maps;
with Values.Strings;                    use Values.Strings;
with Values.Construction;               use Values.Construction;

package body Values.Operations is

    function Unary_Error( right : Value'Class ) return Value is (Value(right));

    function Unary_Null( right : Value'Class ) return Value
        is (Errors.Create( Null_Reference, "Value is null" ));

    function Unary_Undefined( right : Value'Class ) return Value
        is (Errors.Create( Type_Mismatch, "Invalid operand type" ));

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Unary_Operation ) is
    begin
        this.initialized := True;

        this.table := Unary_Func_Table'(
            V_NULL  => Unary_Null'Access,
            V_ERROR => Unary_Error'Access,
            others  => Unary_Undefined'Access     -- default undefined behavior
        );
    end Construct;

    ----------------------------------------------------------------------------

    function Dispatch( this  : not null access Unary_Operation'Class;
                       right : Value'Class ) return Value is
    begin
        if not this.initialized then
            this.Construct;
        end if;
        return this.table(right.Get_Type).all( right );
    end Dispatch;

    --==========================================================================

    package body Generic_Unary is

        function Operate( right : Value'Class ) return Value is
        begin
            return instance.Dispatch( right );
        end Operate;

    end Generic_Unary;

    --==========================================================================

    function Negate_Color( right : Value'Class ) return Value is
        r, g, b, a : Float := 0.0;
    begin
        right.To_RGBA( r, g, b, a );
        return Create_Color( 1.0 - r, 1.0 - g, 1.0 - b, a );
    end Negate_Color;

    ----------------------------------------------------------------------------

    function Negate_Number( right : Value'Class ) return Value
        is (Create( -right.To_Long_Float ));

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access Negate_Op ) is
    begin
        Unary_Operation(this.all).Construct;
        this.table(V_COLOR)  := Negate_Color'Access;
        this.table(V_NUMBER) := Negate_Number'Access;
    end Construct;

    --==========================================================================

    function Not_Boolean( right : Value'Class ) return Value
        is (Create( not right.To_Boolean ));

    ----------------------------------------------------------------------------

    function Not_Number( right : Value'Class ) return Value
        is (Create( right.To_Long_Float = 0.0 ));

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access Not_Op ) is
    begin
        Unary_Operation(this.all).Construct;
        this.table(V_BOOLEAN) := Not_Boolean'Access;
        this.table(V_NUMBER ) := Not_Number'Access;
    end Construct;

    --==========================================================================

    -- The sign of any value (except for numbers) is simply 1 (positive).
    function Sign_Any( right : Value'Class ) return Value
        is (Create( 1 ));

    ----------------------------------------------------------------------------

    function Sign_Number( right : Value'Class ) return Value
        is (Create( Long_Float'(if right.To_Long_Float > 0.0 then 1.0
                     elsif right.To_Long_Float < 0.0 then -1.0
                     else 0.0) ));

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access Sign_Op ) is
    begin
        Unary_Operation(this.all).Construct;
        this.table := Unary_Func_Table'(
            V_NUMBER => Sign_Number'Access,
            others   => Sign_Any'Access
        );
    end Construct;

    --==========================================================================

    -- This function is called when one of the operands is an error, propagating
    -- it. If both operands are errors, the left side will be propagated.
    function Binary_Error( left, right : Value'Class ) return Value
        is (if left.Get_Type = V_ERROR then left else right);

    -- This function is called when an operand is an unexpected Null type,
    -- producing an error.
    function Binary_Null( left, right : Value'Class ) return Value
        is (Errors.Create( Null_Reference, "Value is null" ));

    -- This function is called when the operation is undefined for the given
    -- operand types.
    function Binary_Undefined( left, right : Value'Class ) return Value
        is (Errors.Create( Type_Mismatch, "Invalid operand types" ));

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Binary_Operation ) is
    begin
        this.initialized := True;

        -- default undefined behavior
        for i in Value_Type'Range loop
            for j in Value_Type'Range loop
                this.table(i, j) := Binary_Undefined'Access;
            end loop;
        end loop;

        -- override to handle unexpected nulls
        for i in Value_Type'Range loop
            this.table(i, V_NULL) := Binary_Null'Access;
            this.table(V_NULL, i) := Binary_Null'Access;
        end loop;

        -- override to handle errors
        for i in Value_Type'Range loop
            this.table(i, V_ERROR) := Binary_Error'Access;
            this.table(V_ERROR, i) := Binary_Error'Access;
        end loop;
    end Construct;

    ----------------------------------------------------------------------------

    function Dispatch( this  : not null access Binary_Operation'Class;
                       left  : Value'Class;
                       right : Value'Class ) return Value is
    begin
        if not this.initialized then
            this.Construct;
        end if;
        return this.table(left.Get_Type, right.Get_Type).all( left, right );
    end Dispatch;

    --==========================================================================

    package body Generic_Binary is

        function Operate( left, right : Value'Class ) return Value is
        begin
            return instance.Dispatch( left, right );
        end Operate;

    end Generic_Binary;

    --==========================================================================

    function Add_Color_Color( left, right : Value'Class ) return Value is
        r1, g1, b1, a1 : Float;
        r2, g2, b2, a2 : Float;
    begin
        right.To_RGBA( r2, g2, b2, a2 );
        left.To_RGBA( r1, g1, b1, a1 );

        -- alpha of left is preserved
        return Create_Color( Float'Min( r1 + r2 * a2, 1.0 ),
                             Float'Min( g1 + g2 * a2, 1.0 ),
                             Float'Min( b1 + b2 * a2, 1.0 ),
                             a1 );
    end Add_Color_Color;

    ----------------------------------------------------------------------------

    function Add_Number_Number( left, right : Value'Class ) return Value
        is (Create( left.To_Long_Float + right.To_Long_Float ));

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access Add_Op ) is
    begin
        Binary_Operation(this.all).Construct;
        this.table(V_COLOR,  V_COLOR ) := Add_Color_Color'Access;
        this.table(V_NUMBER, V_NUMBER) := Add_Number_Number'Access;
    end Construct;

    --==========================================================================

    function And_Boolean_Boolean( left, right : Value'Class ) return Value
        is (Create( left.To_Boolean and right.To_Boolean ));

    function And_Boolean_Number( left, right : Value'Class ) return Value
        is (Create( left.To_Boolean and (right.To_Long_Float /= 0.0) ));

    function And_Number_Boolean( left, right : Value'Class ) return Value
        is (Create( (left.To_Long_Float /= 0.0) and right.To_Boolean ));

    function And_Number_Number( left, right : Value'Class ) return Value
        is (Create( (left.To_Long_Float /= 0.0) and (right.To_Long_Float /= 0.0) ));

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access And_Op ) is
    begin
        Binary_Operation(this.all).Construct;
        this.table(V_BOOLEAN, V_BOOLEAN) := And_Boolean_Boolean'Access;
        this.table(V_BOOLEAN, V_NUMBER ) := And_Boolean_Number'Access;
        this.table(V_NUMBER,  V_BOOLEAN) := And_Number_Boolean'Access;
        this.table(V_NUMBER,  V_NUMBER ) := And_Number_Number'Access;
    end Construct;

    --==========================================================================

    function Concat_List_List( left, right : Value'Class ) return Value
        is (left.Lst & right.Lst);

    ----------------------------------------------------------------------------

    -- Warning: potential Ada stack overflow for large strings
    function Concat_String_String( left, right : Value'Class ) return Value
        is (Create( left.Str.To_String & right.Str.To_String ));

    ----------------------------------------------------------------------------

    -- Warning: potential Ada stack overflow for large strings
    function Concat_String_Any( left, right : Value'Class ) return Value
        is (Create( left.Str.To_String & right.Image ));

    ----------------------------------------------------------------------------

    -- Warning: potential Ada stack overflow for large strings
    function Concat_Any_String( left, right : Value'Class ) return Value
        is (Create( left.Image & right.Str.To_String ));

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access Concat_Op ) is
    begin
        Binary_Operation(this.all).Construct;
        this.table(V_LIST,   V_LIST  ) := Concat_List_List'Access;
        this.table(V_STRING, V_STRING) := Concat_String_String'Access;

        this.table(V_STRING, V_BOOLEAN) := Concat_String_Any'Access;
        this.table(V_STRING, V_ERROR  ) := Concat_String_Any'Access;
        this.table(V_STRING, V_ID     ) := Concat_String_Any'Access;
        this.table(V_STRING, V_NULL   ) := Concat_String_Any'Access;
        this.table(V_STRING, V_NUMBER ) := Concat_String_Any'Access;

        this.table(V_BOOLEAN, V_STRING) := Concat_Any_String'Access;
        this.table(V_ERROR,   V_STRING) := Concat_Any_String'Access;
        this.table(V_ID,      V_STRING) := Concat_Any_String'Access;
        this.table(V_NULL,    V_STRING) := Concat_Any_String'Access;
        this.table(V_NUMBER,  V_STRING) := Concat_Any_String'Access;
    end Construct;

    --==========================================================================

    function Divide_Number_Number( left, right : Value'Class ) return Value is
    begin
        return Create( left.To_Long_Float / right.To_Long_Float );
    exception
        when others =>
            return Errors.Create( Illegal_Operation, "Division by zero" );
    end Divide_Number_Number;

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access Divide_Op ) is
    begin
        Binary_Operation(this.all).Construct;
        this.table(V_NUMBER, V_NUMBER) := Divide_Number_Number'Access;
    end Construct;

    --==========================================================================

    function In_Any_Map( left, right : Value'Class ) return Value
        is (Create( right.Map.Has_Value( left ) ));

    ----------------------------------------------------------------------------

    function In_Any_List( left, right : Value'Class ) return Value
        is (Create( right.Lst.Find( left ) > 0 ));

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access In_Op ) is
    begin
        Binary_Operation(this.all).Construct;
        this.table(V_BOOLEAN, V_MAP) := In_Any_Map'Access;
        this.table(V_ERROR,   V_MAP) := In_Any_Map'Access;
        this.table(V_ID,      V_MAP) := In_Any_Map'Access;
        this.table(V_LIST,    V_MAP) := In_Any_Map'Access;
        this.table(V_MAP,     V_MAP) := In_Any_Map'Access;
        this.table(V_NULL,    V_MAP) := In_Any_Map'Access;
        this.table(V_NUMBER,  V_MAP) := In_Any_Map'Access;
        this.table(V_STRING,  V_MAP) := In_Any_Map'Access;

        this.table(V_BOOLEAN, V_LIST) := In_Any_List'Access;
        this.table(V_ERROR,   V_LIST) := In_Any_List'Access;
        this.table(V_ID,      V_LIST) := In_Any_List'Access;
        this.table(V_LIST,    V_LIST) := In_Any_List'Access;
        this.table(V_MAP,     V_LIST) := In_Any_List'Access;
        this.table(V_NULL,    V_LIST) := In_Any_List'Access;
        this.table(V_NUMBER,  V_LIST) := In_Any_List'Access;
        this.table(V_STRING,  V_LIST) := In_Any_List'Access;
    end Construct;

    --==========================================================================

    function Index_Color_Number( left, right : Value'Class ) return Value is
        r, g, b, a : Float := 0.0;
    begin
        left.To_RGBA( r, g, b, a );
        case right.To_Int is
            when 1 => return Create( r );
            when 2 => return Create( g );
            when 3 => return Create( b );
            when 4 => return Create( a );
            when others => null;
        end case;
        return Errors.Create( Out_Of_Range, "Invalid color channel" );
    end Index_Color_Number;

    ----------------------------------------------------------------------------

    function Index_Color_String( left, right : Value'Class ) return Value is
        r, g, b, a : Float := 0.0;
    begin
        left.To_RGBA( r, g, b, a );
        if right.Str.Length = 1 then
            case right.Str.Get( 1 ) is
                when 'r' => return Create( r );
                when 'g' => return Create( g );
                when 'b' => return Create( b );
                when 'a' => return Create( a );
                when others => null;
            end case;
        end if;
        return Errors.Create( Out_Of_Range, "Invalid color channel" );
    end Index_Color_String;

    ----------------------------------------------------------------------------

    function Index_Map_String( left, right : Value'Class ) return Value
        is (left.Map.Get( right.Str.To_String ));

    ----------------------------------------------------------------------------

    function Index_Expected_Number( left, right : Value'Class ) return Value
        is (Errors.Create( Type_Mismatch, "Expected integer index" ));

    ----------------------------------------------------------------------------

    function Index_Expected_String( left, right : Value'Class ) return Value
        is (Errors.Create( Type_Mismatch, "Expected string index" ));

    ----------------------------------------------------------------------------

    function Index_List_Number( left, right : Value'Class ) return Value
        is (left.Lst.Get( right.To_Int ));

    ----------------------------------------------------------------------------

    function Index_String_Number( left, right : Value'Class ) return Value is
        index : constant Integer := right.To_Int;
        str   : constant String := left.Str.To_String;
    begin
        if index >= 1 and then index <= str'Length then
            return Create( String'(1=>str(str'First + (index - 1))) );
        end if;
        return Errors.Create( Out_Of_Range, "String index out of range" );
    end Index_String_Number;

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access Index_Op ) is
    begin
        Binary_Operation(this.all).Construct;
        this.table(V_COLOR, V_NUMBER ) := Index_Color_Number'Access;
        this.table(V_COLOR, V_STRING)  := Index_Color_String'Access;
        this.table(V_COLOR, V_BOOLEAN) := Index_Expected_String'Access;
        this.table(V_COLOR, V_COLOR  ) := Index_Expected_String'Access;
        this.table(V_COLOR, V_ID     ) := Index_Expected_String'Access;
        this.table(V_COLOR, V_LIST   ) := Index_Expected_String'Access;
        this.table(V_COLOR, V_MAP    ) := Index_Expected_String'Access;

        this.table(V_MAP, V_STRING ) := Index_Map_String'Access;
        this.table(V_MAP, V_BOOLEAN) := Index_Expected_String'Access;
        this.table(V_MAP, V_COLOR  ) := Index_Expected_String'Access;
        this.table(V_MAP, V_ID     ) := Index_Expected_String'Access;
        this.table(V_MAP, V_LIST   ) := Index_Expected_String'Access;
        this.table(V_MAP, V_MAP    ) := Index_Expected_String'Access;
        this.table(V_MAP, V_NUMBER ) := Index_Expected_String'Access;

        this.table(V_LIST, V_NUMBER ) := Index_List_Number'Access;
        this.table(V_LIST, V_BOOLEAN) := Index_Expected_Number'Access;
        this.table(V_LIST, V_COLOR  ) := Index_Expected_Number'Access;
        this.table(V_LIST, V_ID     ) := Index_Expected_Number'Access;
        this.table(V_LIST, V_LIST   ) := Index_Expected_Number'Access;
        this.table(V_LIST, V_MAP    ) := Index_Expected_Number'Access;
        this.table(V_LIST, V_STRING ) := Index_Expected_Number'Access;

        this.table(V_STRING, V_NUMBER ) := Index_String_Number'Access;
        this.table(V_STRING, V_BOOLEAN) := Index_Expected_Number'Access;
        this.table(V_STRING, V_COLOR  ) := Index_Expected_Number'Access;
        this.table(V_STRING, V_ID     ) := Index_Expected_Number'Access;
        this.table(V_STRING, V_LIST   ) := Index_Expected_Number'Access;
        this.table(V_STRING, V_MAP    ) := Index_Expected_Number'Access;
        this.table(V_STRING, V_STRING ) := Index_Expected_Number'Access;
    end Construct;

    --==========================================================================

    function Index_Ref_Map_String( left, right : Value'Class ) return Value
        is (left.Map.Reference( right.Str.To_String, createMissing => True ));

    ----------------------------------------------------------------------------

    function Index_Ref_List_Number( left, right : Value'Class ) return Value is
        index : constant Integer := right.To_Int;
    begin
        if index > 0 then
            return left.Lst.Reference( index, createMissing => True );
        end if;
        return Errors.Create( Invalid_Arguments, "Invalid list index" );
    end Index_Ref_List_Number;

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access Index_Ref_Op ) is
    begin
        Binary_Operation(this.all).Construct;
        this.table(V_LIST, V_NUMBER) := Index_Ref_List_Number'Access;
        this.table(V_MAP,  V_STRING) := Index_Ref_Map_String'Access;
    end Construct;

    --==========================================================================

    function Modulo_Number_Number( left, right : Value'Class ) return Value
        is (Create( left.To_Int mod right.To_Int ));

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access Modulo_Op ) is
    begin
        Binary_Operation(this.all).Construct;
        this.table(V_NUMBER, V_NUMBER) := Modulo_Number_Number'Access;
    end Construct;

    --==========================================================================

    function Multiply_Color_Color( left, right : Value'Class ) return Value is
        r1, g1, b1, a1 : Float;
        r2, g2, b2, a2 : Float;
    begin
        left.To_RGBA( r1, g1, b1, a1 );
        right.To_RGBA( r2, g2, b2, a2 );

        -- alpha IS multiplied
        return Create_Color( Float'Min( r1 * r2, 1.0 ),
                             Float'Min( g1 * g2, 1.0 ),
                             Float'Min( b1 * b2, 1.0 ),
                             Float'Min( a1 * a2, 1.0 ) );
    end Multiply_Color_Color;

    ----------------------------------------------------------------------------

    function Multiply_Color_Number( left, right : Value'Class ) return Value is
        r, g, b, a : Float;
    begin
        left.To_RGBA( r, g, b, a );
        r := Float'Min( Float'Max( 0.0, r * right.To_Float ), 1.0 );
        g := Float'Min( Float'Max( 0.0, g * right.To_Float ), 1.0 );
        b := Float'Min( Float'Max( 0.0, b * right.To_Float ), 1.0 );

        -- alpha is NOT multiplied
        return Create_Color( r, g, b, a );
    end Multiply_Color_Number;

    ----------------------------------------------------------------------------

    function Multiply_Number_Number( left, right : Value'Class ) return Value
        is (Create( left.To_Long_Float * right.To_Long_Float ));

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access Multiply_Op ) is
    begin
        Binary_Operation(this.all).Construct;
        this.table(V_COLOR,  V_COLOR ) := Multiply_Color_Color'Access;
        this.table(V_COLOR,  V_NUMBER) := Multiply_Color_Number'Access;
        this.table(V_NUMBER, V_NUMBER) := Multiply_Number_Number'Access;
    end Construct;

    --==========================================================================

    function Or_Boolean_Boolean( left, right : Value'Class ) return Value
        is (Create( left.To_Boolean or right.To_Boolean ));

    function Or_Boolean_Number( left, right : Value'Class ) return Value
        is (Create( left.To_Boolean or (right.To_Long_Float /= 0.0) ));

    function Or_Number_Boolean( left, right : Value'Class ) return Value
        is (Create( (left.To_Long_Float /= 0.0) or right.To_Boolean ));

    function Or_Number_Number( left, right : Value'Class ) return Value
        is (Create( (left.To_Long_Float /= 0.0) or (right.To_Long_Float /= 0.0) ));

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access Or_Op ) is
    begin
        Binary_Operation(this.all).Construct;
        this.table(V_BOOLEAN, V_BOOLEAN) := Or_Boolean_Boolean'Access;
        this.table(V_BOOLEAN, V_NUMBER)  := Or_Boolean_Number'Access;
        this.table(V_NUMBER,  V_BOOLEAN) := Or_Number_Boolean'Access;
        this.table(V_NUMBER,  V_NUMBER)  := Or_Number_Number'Access;
    end Construct;

    --==========================================================================

    function Power_Number_Number( left, right : Value'Class ) return Value is
        exp : constant Integer := right.To_Int;
    begin
        if exp >= 0 then
            return Create( left.To_Long_Float ** exp );
        end if;
        return Errors.Create( Illegal_Operation, "Negative exponent" );
    end Power_Number_Number;

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access Power_Op ) is
    begin
        Binary_Operation(this.all).Construct;
        this.table(V_NUMBER, V_NUMBER) := Power_Number_Number'Access;
    end Construct;

    --==========================================================================

    function Subtract_Color_Color( left, right : Value'Class ) return Value is
        r1, g1, b1, a1 : Float;
        r2, g2, b2, a2 : Float;
    begin
        left.To_RGBA( r1, g1, b1, a1 );
        right.To_RGBA( r2, g2, b2, a2 );

        return Create_Color( Float'Max( 0.0, r1 - r2 * a2 ),
                             Float'Max( 0.0, g1 - g2 * a2 ),
                             Float'Max( 0.0, b1 - b2 * a2 ),
                             a1 );
    end Subtract_Color_Color;

    ----------------------------------------------------------------------------

    function Subtract_Number_Number( left, right : Value'Class ) return Value
        is (Create( left.To_Long_Float - right.To_Long_Float ));

    ----------------------------------------------------------------------------

    function Subtract_List_List( left, right : Value'Class ) return Value is
        ll     : constant List_Value := left.Lst;
        rl     : constant List_Value := right.Lst;
        result : constant List_Value := Create_List.Lst;
        val    : Value;
        found  : Boolean;
    begin
        for i in 1..ll.Length loop
            val := ll.Get( i );
            found := False;
            for j in 1..rl.Length loop
                if val = rl.Get( j ) then
                    found := True;
                    exit;
                end if;
            end loop;
            if not found then
                result.Append( val );
            end if;
        end loop;
        return Value(result);
    end Subtract_List_List;

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access Subtract_Op ) is
    begin
        Binary_Operation(this.all).Construct;
        this.table(V_COLOR,  V_COLOR)  := Subtract_Color_Color'Access;
        this.table(V_NUMBER, V_NUMBER) := Subtract_Number_Number'Access;
        this.table(V_LIST,   V_LIST)   := Subtract_List_List'Access;
    end Construct;

    --==========================================================================

    function Xor_Boolean_Boolean( left, right : Value'Class ) return Value
        is (Create( left.To_Boolean xor right.To_Boolean ));

    function Xor_Boolean_Number( left, right : Value'Class ) return Value
        is (Create( left.To_Boolean xor (right.To_Long_Float /= 0.0) ));

    function Xor_Number_Boolean( left, right : Value'Class ) return Value
        is (Create( (left.To_Long_Float /= 0.0) xor right.To_Boolean ));

    function Xor_Number_Number( left, right : Value'Class ) return Value
        is (Create( (left.To_Long_Float /= 0.0) xor (right.To_Long_Float /= 0.0) ));

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access Xor_Op ) is
    begin
        Binary_Operation(this.all).Construct;
        this.table(V_BOOLEAN, V_BOOLEAN) := Xor_Boolean_Boolean'Access;
        this.table(V_BOOLEAN, V_NUMBER)  := Xor_Boolean_Number'Access;
        this.table(V_NUMBER,  V_BOOLEAN) := Xor_Number_Boolean'Access;
        this.table(V_NUMBER,  V_NUMBER)  := Xor_Number_Number'Access;
    end Construct;

end Values.Operations;
