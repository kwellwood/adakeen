--
-- Copyright (c) 2015 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Values.Errors is

    type Error_Code is
    (
        Generic_Exception,
        Illegal_Operation,
        Invalid_Arguments,
        Type_Mismatch,
        Null_Reference,
        Out_Of_Range,
        Evaluation_Error,
        Compile_Error,
        Undefined_Function,
        Invalid_Namespace,
        Undefined_Name,
        File_Error,
        Not_Found
    );

    function Create( code : Error_Code; msg : String ) return Value;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Error_Value is new Value with private;

    -- Returns a Error_Value pointing to the data of 'this'. The data is never
    -- copied.
    function As_Error( this : Value'Class ) return Error_Value;

    -- Returns the error's general code.
    function Get_Code( this : Error_Value'Class ) return Error_Code;
    pragma Precondition( this.Valid );

    -- Returns the error's specific message.
    function Get_Message( this : Error_Value'Class ) return String;
    pragma Precondition( this.Valid );

    function Valid( this : Error_Value'Class ) return Boolean is (this.Is_Error);

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    function Compare_Errors( left, right : Value'Class ) return Integer;
    pragma Precondition( left.Is_Error );
    pragma Precondition( right.Is_Error );

    procedure Delete_Error( obj : Address );

    function Image_Error( this : Value'Class ) return String;
    pragma Precondition( this.Is_Error );

    procedure Read_Error( stream : access Root_Stream_Type'Class;
                          this   : in out Value'Class );
    pragma Precondition( this.Is_Error );

    procedure Write_Error( stream : access Root_Stream_Type'Class;
                           this   : Value'Class );
    pragma Precondition( this.Is_Error );

private

    type Error_Data;
    type A_Error_Data is access all Error_Data;
    pragma No_Strict_Aliasing( A_Error_Data );

    type Error_Value is new Value with
        record
            data : A_Error_Data := null;
        end record;

end Values.Errors;
