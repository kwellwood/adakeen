--
-- Copyright (c) 2013-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Vectors;
with Scribble.Ast.Expressions;          use Scribble.Ast.Expressions;
with Scribble.Ast.Expressions.Symbols;  use Scribble.Ast.Expressions.Symbols;

package Scribble.Ast.Statements is

    -- Ast_Statement is the abstract base class of every statement node in the
    -- abstract syntax tree.
    --
    -- Like expressions, Ast_Statement trees implement the Composite design
    -- pattern. Statement blocks (represented by the Ast_Block class) are the
    -- container class for statements, maintaining them as a linked list. Thus,
    -- every statement has a reference to the statement that follows it in its
    -- statement block.
    type Ast_Statement is abstract new Ast_Node with private;
    type A_Ast_Statement is access all Ast_Statement'Class;

    -- Returns a reference to the statement that follows this one within its
    -- statement block, or null if there is none. Ownership is maintained by the
    -- Ast_Statement.
    function Get_Next( this : not null access Ast_Statement'Class ) return A_Ast_Statement;

    overriding
    function Get_Type( this : access Ast_Statement ) return Ast_Type is (STATEMENT);

    -- Attempts to prune any expressions embedded in the statement. If the
    -- statement contains other statements within it, they will also be pruned.
    not overriding
    procedure Prune( this : access Ast_Statement ) is null;

    -- Sets the statement that will follow this one within its statement block.
    -- If 'statement' is replacing an existing next statement, be sure to delete
    -- the old one first. This method will discard the previous pointer.
    procedure Set_Next( this      : not null access Ast_Statement'Class;
                        statement : A_Ast_Statement );

    ----------------------------------------------------------------------------

    -- Ast_Block represents a special container statement. It is itself a
    -- statement that contains a linked list of other statements. It has no
    -- direct behavior; only semantic the ability to contain a nested scope for
    -- variable declarations.
    --
    -- Execution proceeds from the statement prior to the block, to the first
    -- statement within the block.
    --
    -- Nested blocks contain a reference to their parent (outer) block. The
    -- outermost block is the root block, or the statement block implementing
    -- the body of a function definition. The parent block reference is
    -- necessary to resolve identifiers in an inner block that reference a local
    -- variable or parameter defined in an outer block. Local variables defined
    -- in an inner block obscure local variables or parameters of the same name
    -- that are defined in an outer block.
    --
    -- Each Ast_Block instance maintains a table of local variables and
    -- parameters that are defined by a statement directly within the block. The
    -- table is populated during the semantic analysis stage. Each block also
    -- has the ability to resolve identifiers defined within it, or a parent
    -- scope, to identifier indexes, for use during the code generation stage.
    type Ast_Block is new Ast_Statement with private;
    type A_Ast_Block is access all Ast_Block'Class;

    -- Creates a new Ast_Block (or statement block). The lifespan of 'parent'
    -- must outlast this block. If the block is the body of a function and thus
    -- has no parent, null should be given.
    function Create_Block( loc : Token_Location; parent : A_Ast_Block ) return A_Ast_Block;
    pragma Postcondition( Create_Block'Result /= null );

    -- Appends 'statement' to the list of statements inside the block.
    -- 'statement' will be consumed. 'statement' must be a single statement
    -- only, with no next statement.
    procedure Append( this      : not null access Ast_Block'Class;
                      statement : in out A_Ast_Statement );
    pragma Precondition( statement /= null );
    pragma Precondition( statement.Get_Next = null );
    pragma Postcondition( statement = null );

    -- Defines a parameter represented by identifier 'ident' within the block,
    -- returning True on success, or False if the parameter name has already
    -- been defined. Parameter names only apply to function body blocks.
    -- Parameter names in a function prototype must be defined in order (left to
    -- right), and defined before any local variables.
    function Define_Parameter( this  : not null access Ast_Block'Class;
                               ident : not null A_Ast_Identifier ) return Boolean;

    -- Defines a variable represented by identifier 'ident' within the block,
    -- returning True on success. 'const' indicates if the variable is being
    -- declared constant. 'loc' is the location of the statement that is
    -- defining the variable. The variable is not considered within scope for
    -- references until after the definition's location. False will be returned
    -- if a variable of the same name has already been defined directly within
    -- this scope (not in a parent scope).
    function Define_Variable( this  : not null access Ast_Block'Class;
                              ident : not null A_Ast_Identifier;
                              const : Boolean;
                              loc   : Token_Location ) return Boolean;

    -- Returns the first statement in the block. If the block is empty, this may
    -- be null. Ownership is maintained by the Ast_Block.
    function Get_First( this : not null access Ast_Block'Class ) return A_Ast_Statement;

    -- Returns the identifier that was given to Define_Variable when the n'th
    -- variable was defined in this block. Null will be returned if 'n' is
    -- greater than the number of variables defined directly in this block. Note
    -- that this function returns identifiers for variables only, not
    -- parameters, if any are defined.
    function Get_Nth_Variable( this : not null access Ast_Block'Class;
                               n    : Positive ) return A_Ast_Identifier;

    -- Returns the variable definition location that was given to
    -- Define_Variable when the n'th variable was defined in this block. An
    -- empty location will be returned if 'n' is greater than the number of
    -- variables defined directly in this block. Note that this function returns
    -- locations for variables only, not parameters, if any are defined.
    function Get_Nth_Variable_Location( this : not null access Ast_Block'Class;
                                        n    : Positive ) return Token_Location;

    -- Returns a reference to the block that contains this block. Returns null
    -- if this block has no parent (e.g. it's a function body).
    function Get_Parent( this : not null access Ast_Block'Class ) return A_Ast_Block;

    overriding
    function Get_Type( this : access Ast_Block ) return Ast_Type is (BLOCK_STATEMENT);

    -- Returns True if the identifier 'ident' has been defined as a local
    -- variable or constant in this scope and 'ident' is same Ast_Identifier
    -- instance that was used to define it.
    --
    -- This is useful for differentiating the initial assignment to a constant
    -- from a later reference to it. Later references will use the same name
    -- and namespace, but not the same Ast_Identifier instance from the AST.
    function Is_Defining_Identifier( this  : not null access Ast_Block'Class;
                                     ident : not null A_Ast_Identifier ) return Boolean;

    -- Returns True if the identifier 'ident' has been defined as a constant in
    -- this scope. If the identifier is defined but not as a constant, or it has
    -- not been defined in this scope, then False will be returned.
    function Is_Identifier_Constant( this  : not null access Ast_Block'Class;
                                     ident : not null A_Ast_Identifier ) return Boolean;

    -- Prepends 'statement' to the list of statements inside the block.
    -- 'statement' will be consumed. 'statement' must be a single statement
    -- only, with no next statement.
    procedure Prepend( this      : not null access Ast_Block'Class;
                       statement : in out A_Ast_Statement );
    pragma Precondition( statement /= null );
    pragma Precondition( statement.Get_Next = null );
    pragma Postcondition( statement = null );

    procedure Process( this      : access Ast_Block;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class );

    procedure Prune( this : access Ast_Block );

    -- Resolves an identifier to the index of a variable or parameter in this
    -- scope or a parent scope. The returned index indicates whether the
    -- identifier is a parameter (negative index) or local variable (positive
    -- index). Zero will be returned if 'ident' could not be resolved to a local
    -- variable or parameter.
    --
    -- For parameters, -1 maps to the first parameter defined in the root block,
    -- -2 maps to the second, etc.
    --
    -- For local variables, the index of the variable within the root block's
    -- reserved variable space will be returned, where 1 is the first location.
    --
    --          x=>-1 y=>-2 z=>-3       <-- parameter indexes
    -- function(    x,    y,    z)
    --     var a;          a => 1       <-- local variable indexes
    --     var b;          b => 2       <-- ...
    --     if true then
    --         var c;      c => 3       <-- ...
    --     else
    --         var d;      d => 3       <-- ... ('d' shares index with 'c')
    --     end;
    -- end
    --
    -- Identifiers can only resolve to variables or parameters defined within a
    -- block lexically prior to the reference. This prevents a variable's
    -- initializer from referencing the variable being defined.
    function Resolve_Identifier_Index( this  : not null access Ast_Block'Class;
                                       ident : not null A_Ast_Identifier ) return Integer;

    -- Returns the number of local variables reserved in this scope and all
    -- deeper scopes. This does not include parameters or variable space
    -- reserved in parent scopes.
    function Reserved_Variables( this : not null access Ast_Block'Class ) return Natural;

    -- Changes the block's parent. 'parent' is not consumed because it is not
    -- owned by this block. The lifespan of the parent block must outlast this
    -- block.
    procedure Set_Parent( this   : not null access Ast_Block'Class;
                          parent : A_Ast_Block );

    -- Returns the number of local variables directly defined in this block, not
    -- including parameters, or variables in parent scopes or nested scopes.
    function Variable_Count( this : not null access Ast_Block'Class ) return Natural;

    -- Returns the offset of the local variables in this scope within the root
    -- block's reserved variable space. This is effectively a recursive sum of
    -- all ancestor scopes' variable counts.
    function Variables_Offset( this : not null access Ast_Block'Class ) return Natural;

    ----------------------------------------------------------------------------

    -- Ast_Exit represents a statement that causes execution to jump to the
    -- statement following the innermost Ast_Loop statement block that contains
    -- the exit statement.
    --
    -- The semantic analysis stage ensures that the exit statement can only be
    -- used within a loop statement block.
    type Ast_Exit is new Ast_Statement with private;
    type A_Ast_Exit is access all Ast_Exit'Class;

    -- Creates a new instance of Ast_Exit.
    function Create_Exit( loc : Token_Location ) return A_Ast_Statement;
    pragma Postcondition( Create_Exit'Result /= null );

    overriding
    function Get_Type( this : access Ast_Exit ) return Ast_Type is (EXIT_STATEMENT);

    procedure Process( this      : access Ast_Exit;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class );

    ----------------------------------------------------------------------------

    -- Ast_Expression_Statement is a statement that evaluates an expression
    -- (e.g. a function call). The result of the expression is discarded, so the
    -- evaluation of the expression must produce a side effect in order to be
    -- useful.
    --
    -- Function calls are the most common form of expression statements.
    type Ast_Expression_Statement is new Ast_Statement with private;
    type A_Ast_Expression_Statement is access all Ast_Expression_Statement'Class;

   -- Creates a new expression statement that will evaluate expression 'expr'
   -- and discard the result. 'expr' will be consumed.
    function Create_Expression_Statement( expr : not null A_Ast_Expression ) return A_Ast_Statement;
    pragma Postcondition( Create_Expression_Statement'Result /= null );

    -- Returns a reference to the expression that will be evaluated. Ownership
    -- is maintained by the Ast_Expression_Statement.
    function Get_Expression( this : not null access Ast_Expression_Statement'Class ) return A_Ast_Expression;

    procedure Process( this      : access Ast_Expression_Statement;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class );

    procedure Prune( this : access Ast_Expression_Statement );

    ----------------------------------------------------------------------------

    -- Ast_If represents a single if-elsif-else statement. The initial "if"
    -- condition and the optional following "elsif" conditions are stored in an
    -- array if If_Case structs, in lexical order, with the "if" condition
    -- always first.
    --
    -- If-statement evaluation is then performed by simply iterating through a
    -- list of numbered cases, evaluating the conditional expression and
    -- proceeding to the corresponding block of the first case to evaluate True.
    -- There will always be at least one case: the initial "if/then".
    --
    -- Should the conditions of all cases evaluate False, execution proceeds to
    -- the optional "else" statement block.
    type Ast_If is new Ast_Statement with private;
    type A_Ast_If is access all Ast_If'Class;

    -- Creates a new if/elsif/else statement.
    function Create_If( loc : Token_Location ) return A_Ast_If;
    pragma Postcondition( Create_If'Result /= null );

    -- Sets the block of code to be executed when all previous cases' conditions
    -- evaluate False and 'condition' evaluates True. 'condition' and 'block'
    -- will be consumed.
    procedure Add_Case( this      : not null access Ast_If'Class;
                        condition : in out A_Ast_Expression;
                        block     : in out A_Ast_Block );
    pragma Precondition( condition /= null );
    pragma Precondition( block /= null );
    pragma Postcondition( condition = null );
    pragma Postcondition( block = null );

    -- Returns the number of conditional cases added to the if statement, not
    -- including the 'else' case. The count will always be at least 1 in a valid
    -- AST structure.
    function Case_Count( this : not null access Ast_If'Class ) return Natural;

    -- Returns a reference to the code block to be executed if the condition for
    -- case number 'n' evaluates True and all previous cases evaluated False.
    -- The initial required "if" case is always case 1. null will be returned
    -- if no nth case has been defined. The Ast_If statement maintains ownership
    -- of the block.
    function Get_Case_Block( this : not null access Ast_If'Class; n : Positive ) return A_Ast_Block;

    -- Returns a reference to the condition to be evaluated for case number 'n',
    -- if the conditions for all previous cases evaluated False. The initial
    -- required "if" case is always case 1. null will be returned if no nth case
    -- has been defined. The Ast_If statement maintains ownership of the
    -- expression.
    function Get_Case_Condition( this : not null access Ast_If'Class; n : Positive ) return A_Ast_Expression;

    -- Returns the source location of case number 'n'. This is the location of
    -- the case's IF or ELSIF token. UNKNOWN_LOCATION will be returned if no nth
    -- case has been defined.
    function Get_Case_Location( this : not null access Ast_If'Class; n : Positive ) return Token_Location;

    -- Returns a reference to the code block to be executed if all conditional
    -- cases evaluate False. null will be returned if no "else" case has been
    -- specified. The Ast_If statement maintains ownership of the block.
    function Get_Else_Block( this : not null access Ast_If'Class ) return A_Ast_Block;

    overriding
    function Get_Type( this : access Ast_If ) return Ast_Type is (IF_STATEMENT);

    procedure Process( this      : access Ast_If;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class );

    procedure Prune( this : access Ast_If );

    -- Sets the block of code to be executed when all previous conditions
    -- evaluate False. 'block' will be consumed.
    procedure Set_Else_Block( this  : not null access Ast_If'Class;
                              block : in out A_Ast_Block );

    ----------------------------------------------------------------------------

    -- Ast_Loop represents a special statement block that executes a list of
    -- statements repeatedly, jumping from the last statement in the loop to the
    -- first statement in the loop until an exit statement is encountered, to
    -- break out of the loop.
    type Ast_Loop is new Ast_Statement with private;
    type A_Ast_Loop is access all Ast_Loop'Class;

    -- Creates a new loop statement. 'block' will be consumed.
    function Create_Loop( loc : Token_Location; block : not null A_Ast_Block ) return A_Ast_Loop;
    pragma Postcondition( Create_Loop'Result /= null );

    -- Nests the loop's body block within a new block. The blocks' parent
    -- references will be updated appropriately.
    --
    -- This is necessary for constructing 'for' and 'while' statements in the
    -- parser because an if-statement must preceed all other statements in the
    -- loop, which would prevent the declaration of local variables, unless they
    -- are nested in a deeper block.
    --
    -- Ast_Loop            Ast_Loop
    --    Ast_Block   =>      Ast_Block(new)
    --                           Ast_Block
    procedure Double_Nest( this : not null access Ast_Loop'Class );

    -- Returns a reference to the loop's body as a statement block. Ownership is
    -- maintained by the Ast_Loop.
    function Get_Body( this : not null access Ast_Loop'Class ) return A_Ast_Block;

    overriding
    function Get_Type( this : access Ast_Loop ) return Ast_Type is (LOOP_STATEMENT);

    procedure Process( this      : access Ast_Loop;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class );

    procedure Prune( this : access Ast_Loop );

    -- Sets the loop's body.
    procedure Set_Body( this  : not null access Ast_Loop'Class;
                        block : in out A_Ast_Block );

    ----------------------------------------------------------------------------

    -- Ast_Return represents a statement that ends execution of the current
    -- function and returns a resultant value to the function's caller.
    type Ast_Return is new Ast_Statement with private;
    type A_Ast_Return is access all Ast_Return'Class;

    -- Creates a new return statement that will return the result of evaluating
    -- 'expr'. 'expr' will be consumed.
    function Create_Return( expr : not null A_Ast_Expression ) return A_Ast_Statement;
    pragma Postcondition( Create_Return'Result /= null );

    -- Returns a reference to the expression that will be evaluated for return.
    -- Ownership is maintained by the Ast_Return.
    function Get_Expression( this : not null access Ast_Return'Class ) return A_Ast_Expression;
    pragma Postcondition( Get_Expression'Result /= null );

    overriding
    function Get_Type( this : access Ast_Return ) return Ast_Type is (RETURN_STATEMENT);

    procedure Process( this      : access Ast_Return;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class );

    procedure Prune( this : access Ast_Return );

    ----------------------------------------------------------------------------

    -- Ast_Var represents a local variable declaration. The variable definition
    -- will be added to its parent block's identifier table during the semantic
    -- analysis stage, obscuring any parameter or variable definition of the
    -- same name in an outer statement block.
    --
    -- The semantic analysis stage will ensure that all Var statements occur
    -- before any other statement within their statement block.
    type Ast_Var is new Ast_Statement with private;
    type A_Ast_Var is access all Ast_Var'Class;

    -- Creates a new var statement that will initialize local variable 'var' to
    -- 'expr'. 'var' and 'expr' will be consumed. The variable's value will be
    -- constant if 'const' is True.
    --
    -- 'loc' is the location of the semicolon that ends the statement, thus
    -- beginning the valid scope of 'var'. The location of 'var' will be changed
    -- to match the location of its defining statement. This is done so that the
    -- statement's identifier will be in scope when assigning the initializer
    -- expression to it.
    function Create_Var( loc   : Token_Location;
                         var   : not null A_Ast_Identifier;
                         expr  : not null access Ast_Expression'Class;
                         const : Boolean := False ) return A_Ast_Statement;
    pragma Precondition( (if const then expr /= null else True) );
    pragma Postcondition( Create_Var'Result /= null );

    -- Returns a reference to the expression that will be assigned to the
    -- variable. Ownership is maintained by the Ast_Var.
    function Get_Expression( this : not null access Ast_Var'Class ) return A_Ast_Expression;

    overriding
    function Get_Type( this : access Ast_Var ) return Ast_Type is (VAR_STATEMENT);

    -- Returns a reference to the variable to be defined. Ownership is
    -- maintained by the Ast_Var. Note that the variable's .Location matches the
    -- statement, not its original lexical location. Call Get_Variable_Location
    -- to get the identifier's lexical location.
    function Get_Variable( this : not null access Ast_Var'Class ) return A_Ast_Identifier;

    -- Returns the actual lexical location of the identifier within the
    -- statement. Calling Get_Variable.Location returns the statement's location,
    -- which is marked at the terminating semicolon.
    function Get_Variable_Location( this : not null access Ast_Var'Class ) return Token_Location;

    -- Returns True if the variable was declared to have a constant value.
    function Is_Constant( this : not null access Ast_Var'Class ) return Boolean;

    procedure Process( this      : access Ast_Var;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class );

    procedure Prune( this : access Ast_Var );

    ----------------------------------------------------------------------------

    -- Ast_Yield represents a statement that allows the VM's execution loop to
    -- exit without completing evaluation. This affords the host Ada thread an
    -- opportunity to perform other actions before returning to evaluation.
    type Ast_Yield is new Ast_Statement with private;
    type A_Ast_Yield is access all Ast_Yield'Class;

    function Create_Yield( loc : Token_Location ) return A_Ast_Statement;
    pragma Postcondition( Create_Yield'Result /= null );

    procedure Process( this      : access Ast_Yield;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class );

private

    type Ast_Statement is abstract new Ast_Node with
        record
            next : A_Ast_Statement := null;
        end record;

    procedure Delete( this : in out Ast_Statement );

    ----------------------------------------------------------------------------

    type Var_Def is
        record
            ident : A_Ast_Identifier;
            const : Boolean := False;
            loc   : Token_Location := UNKNOWN_LOCATION;  -- location of the defining statement
        end record;

    function Name_Eq( l, r : Var_Def ) return Boolean is (l.ident.Get_Name = r.ident.Get_Name);

    package Var_Vectors is new Ada.Containers.Vectors( Positive, Var_Def, Name_Eq );

    type Ast_Block is new Ast_Statement with
        record
            parent         : A_Ast_Block := null;         -- not owned
            inner          : A_Ast_Statement := null;
            last           : A_Ast_Statement := null;
            variables      : Var_Vectors.Vector;
            parameterCount : Natural := 0;  -- number of parameters preceeding local variables
            reservedVars   : Natural := 0;  -- total space reserved for local variables
                                            --     (including the needs of deeper scopes)
        end record;

    procedure Construct( this   : access Ast_Block;
                         loc    : Token_Location;
                         parent : A_Ast_Block );

    procedure Delete( this : in out Ast_Block );

    -- Reserves space for at least 'minimum' local variables in the root
    -- Ast_Block. This is necessary for nested scopes to indicate their need for
    -- local variable space in the function's activation record. If the parent
    -- block is not the root, it will add its own variables to 'count' and pass
    -- a larger reservation to its parent.
    --
    -- When a new local variable is defined in a nested scope, the block should
    -- call its own Reserve_Variable_Space( 0 ) to recheck its needs.
    procedure Reserve_Variable_Space( this    : not null access Ast_Block'Class;
                                      minimum : Natural := 0 );

    ----------------------------------------------------------------------------

    type Ast_Exit is new Ast_Statement with null record;

    ----------------------------------------------------------------------------

    type Ast_Expression_Statement is new Ast_Statement with
        record
            expr : A_Ast_Expression := null;
        end record;

    procedure Construct( this : access Ast_Expression_Statement;
                         expr : not null A_Ast_Expression );

    procedure Delete( this : in out Ast_Expression_Statement );

    ----------------------------------------------------------------------------

    -- Represents a single case of the if-statement:
    -- a condition and a statement block.
    type If_Case is
        record
            condition : A_Ast_Expression := null;
            block     : A_Ast_Block := null;
        end record;

    package Case_Vectors is new Ada.Containers.Vectors( Positive, If_Case, "=" );

    type Ast_If is new Ast_Statement with
       record
            ifCase   : Case_Vectors.Vector;
            elseCase : A_Ast_Block := null;
       end record;

    procedure Delete( this : in out Ast_If );

    ----------------------------------------------------------------------------

    type Ast_Loop is new Ast_Statement with
        record
            block : A_Ast_Block := null;
        end record;

    procedure Construct( this  : access Ast_Loop;
                         loc   : Token_Location;
                         block : not null A_Ast_Block );

    procedure Delete( this : in out Ast_Loop );

    ----------------------------------------------------------------------------

    type Ast_Return is new Ast_Statement with
        record
            expr : A_Ast_Expression := null;
        end record;

    procedure Construct( this : access Ast_Return; expr : not null A_Ast_Expression );

    procedure Delete( this : in out Ast_Return );

    ----------------------------------------------------------------------------

    type Ast_Var is new Ast_Statement with
        record
            var      : A_Ast_Identifier := null;    -- owned by 'expr'
            expr     : A_Ast_Expression := null;
            const    : Boolean := False;
            identLoc : Token_Location;
        end record;

    procedure Construct( this  : access Ast_Var;
                         loc   : Token_Location;
                         var   : not null A_Ast_Identifier;
                         expr  : A_Ast_Expression;
                         const : Boolean );

    procedure Delete( this : in out Ast_Var );

    ----------------------------------------------------------------------------

    type Ast_Yield is new Ast_Statement with null record;

end Scribble.Ast.Statements;
