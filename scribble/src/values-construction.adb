--
-- Copyright (c) 2015-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Values.Lists;                      use Values.Lists;
with Values.Maps;                       use Values.Maps;

pragma Elaborate( Values.Lists );
pragma Elaborate( Values.Maps );

package body Values.Construction is

    type Cloner is access function( val : Value'Class ) return Value;

    type Cloner_Array is array (Integer range 2#0_000#..2#1_111#) of Cloner;

    pragma Suppress(Elaboration_Check, Lists.Clone_List);
    pragma Suppress(Elaboration_Check, Maps.Clone_Map);

    cloners : constant Cloner_Array :=
    (
        LIST_TYPE => Lists.Clone_List'Access,
        MAP_TYPE  => Maps.Clone_Map'Access,
        others    => null
    );

    --==========================================================================

    function Clone( this : Value'Class ) return Value is
        typ : constant Integer := this.Get_Type;
    begin
        if not this.Is_Number and then cloners(typ) /= null then
            return cloners(typ).all( this );
        end if;
        -- immutable object
        return Value(this);
    end Clone;

    ----------------------------------------------------------------------------

    function Create( num : Integer ) return Value is (Value'(Controlled with v => F64_To_U64( Long_Float(num) )));

    function Create( num : Float ) return Value is (Value'(Controlled with v => F64_To_U64( Long_Float(num) )));

    function Create( num : Long_Float ) return Value is (Value'(Controlled with v => F64_To_U64( num )));

    ----------------------------------------------------------------------------

    function Create( bool : Boolean ) return Value is (Value'(Controlled with v => (if bool then TAG_TRUE else TAG_FALSE)));

    ----------------------------------------------------------------------------

    function Create_Color( r, g, b : Float; a : Float := 1.0 ) return Value
        is (Value'(Controlled with v => TAG_COLOR or
                                        (PAYLOAD_MASK and (Shift_Left( Unsigned_64(r * 255.0) and 16#FF#, 24 ) or
                                                           Shift_Left( Unsigned_64(g * 255.0) and 16#FF#, 16 ) or
                                                           Shift_Left( Unsigned_64(b * 255.0) and 16#FF#,  8 ) or
                                                                     ( Unsigned_64(a * 255.0) and 16#FF#     )))));

    ----------------------------------------------------------------------------

    function Create_Id( id : Unsigned_64 ) return Value is (Value'(Controlled with v => TAG_ID or (id and PAYLOAD_MASK)));

    ----------------------------------------------------------------------------

    function Create_Ref( target : not null A_Value ) return Value is
        type Uint_Addr is mod 2 ** Standard'Address_Size;
        function From_Addr is new Ada.Unchecked_Conversion( Address, Uint_Addr );
        use Value_Accesses;
    begin
        return Value'(Controlled with v => TAG_REF or (PAYLOAD_MASK and Unsigned_64(From_Addr( To_Address( Object_Pointer(target) ) ))));
    end Create_Ref;

end Values.Construction;
