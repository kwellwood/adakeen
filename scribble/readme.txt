Scribble Scripting
==================
Scribble is a lightweight scripting language with a compiler and runtime written in Ada. It provides
a simple dynamically typed scripting language with first-class functions. Scribble source code is
compiled into an intermediate byte-code format and then executed very quickly by a stack-based virtual
machine. 

Below is an example program written in Scribble, and a definition of the language's grammar.
Updated March 28, 2018.

EXAMPLE
=======

    //
    // Solution to the Eight Queens Problem
    //
    // Adapted from Edsger Dijkstra's algorithm
    //
    function generate(x    := [],
                      n    := 1,
                      col  := [true, true, true, true, true, true, true, true],
                      up   := [true, true, true, true, true, true, true, true,
                               true, true, true, true, true, true, true, true],
                      down := [true, true, true, true, true, true, true, true,
                               true, true, true, true, true, true, true, true])
        for h is 1 to 8 loop
            // check if square N,H is free
            if col[h] and up[8+n-h] and down[n+h] then
                // place the queen at N,H
                x[n] := h;
                col[h] := false;
                up[8+n-h] := false;
                down[n+h] := false;
    
                if n = 8 then
                    print(image(x));            // print this solution
                else
                    self(x, n+1, col, up, down);  // add another queen
                end if;
    
                // remove the queen at N,H
                down[n+h] := true;
                up[8+n-h] := true;
                col[h] := true;
            end if;
        end loop;
    end function;
    
    generate();

GRAMMAR
=======

EXPRESSIONS
-----------

    Expression_List ::= (Expression | Anon_Function_Definition) [',' Expression_List]

    Expression ::= Assign_Term

    Assign_Term ::= Ternary_Term [Assign_Op (Anon_Function_Definition | Assign_Term)]

    Ternary_Term ::= Binary_Term ['?' Ternary_Term ':' Ternary_Term]

    Binary_Term ::= Lterm [Binary_Op Binary_Term]
      Binary_Op ::= 'and' | 'or' | 'xor' | 'in' | '=' | '>' | '>=' | '<' | '<=' | '!=' |
                    '+' | '-' | '&' | '*' | '/' | '%' | '^'

    Lterm      ::= Unary_Op Lterm
    Lterm      ::= Rterm
      Unary_Op ::= '!' | '-' | '@'

    Rterm ::= Operand {Index | Function_Call | Membership}

    Index ::= '[' Expression ']'

    Function_Call ::= '(' [Expression_List] ')'

    Membership ::= '.' Identifier

    Operand ::= Literal | Identifier_Expression | ('(' Expression ')')

    Literal        ::= value-token | 'self' | 'this' | List_Literal | Map_Literal
      List_Literal   ::= '[' [List_Element {(',' | end-of-line) List_Element}] ']'
        List_Element ::= Expression | Anon_Function_Definition
      Map_Literal    ::= '{' [Map_Pair {(',' | end-of-line) Map_Pair}] '}'
        Map_Pair     ::= Named_Function_Definition
                         | Named_Member_Definition
                         | (Map_Key ':' (Anon_Function_Definition | Anon_Member_Definition | Expression))
          Map_Key    ::= string-value-token | symbol-token

    Identifier_Expression ::= Identifier ['::' (Identifier | Name_Expression)]
      Name_Expression     ::= '[' Expression ']'

    Identifier     ::= symbol-token
      symbol-token ::= letter {letter | digit | '_'}

OPERATOR PRECEDENCE
--------------------
    Membership/Index  . []             (greatest)
    Unary             ! - @
    Exponentiation    ^
    Multiplication    * / %
    Addition          + - &
    Comparison        = > >= < <= !=
    Logical           and or xor in    (least)

FUNCTION DEFINITIONS
--------------------

    Anon_Function_Definition  ::= 'function' Function_Definition
    Anon_Member_Definition    ::= 'member' ['function'] Function_Definition
    Named_Function_Definition ::= 'function' Identifier Function_Definition
    Named_Member_Definition   ::= 'member' ['function'] Identifier Function_Definition
      Function_Definition     ::= Parameters_Prototype Function_Implementation
      Parameters_Prototype    ::= '(' [Parameter {',' Parameter}] ')'
      Parameter               ::= Identifier [':' Value_Type] [':=' Expression]
      Value_Type              ::= 'map' | 'boolean' | 'id' | 'list' | 'number' | 'string'
      Function_Implementation ::= (':' Identifier) | (Statement_List 'end' 'function')

STATEMENTS
----------

    Statement_List ::= {Statement}

    Statement ::= {';'} (
                  Block
                  | Exit
                  | For
                  | If
                  | Loop
                  | Return
                  | Var
                  | While
                  | Expression_Statement
                  ) {';'}

    Block ::= 'block' Statement_List 'end' 'block' ';'

    Exit ::= 'exit' ['when' Expression] ';'

    For         ::= 'for' Identifier (For_Range | For_Each) Loop
      For_Range ::= 'is' Expression 'to' Expression ['step' Expression]
      For_Each  ::= 'of' Expression

    If ::= 'if' Expression 'then' Statement_List
           {'elsif' Expression 'then' Statement_List}
           ['else' Statement_List]
           'end' 'if' ';'

    Loop ::= 'loop' Statement_List 'end' 'loop' ';'

    Return ::= 'return' [Expression | Anon_Function_Definition] ';'

    Var ::= 'local' Identifier [':=' (Expression | Anon_Function_Definition)] ';'
    Var ::= 'constant' Identifier ':=' (Expression | Anon_Function_Definition) ';'
    Var ::= Named_Function_Definition ';'

    While ::= 'while' Expression Loop

    Expression_Statement ::= Expression ';'
