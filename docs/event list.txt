
Event Channels:
    0: Any
    1: Game -> View
    2: View -> Game

Application

EVT_APP_BLUR : constant Event_Id := 100;
EVT_APP_FOCUS : constant Event_Id := 101;
EVT_CLOSE_WINDOW : constant Event_Id := 102;
EVT_EXIT_APPLICATION : constant Event_Id := 103;

Audio

EVT_PLAY_MUSIC : constant Event_Id := 200; (Channel 1)
EVT_PLAY_SOUND : constant Event_Id := 201; (Channel 1)
EVT_STOP_MUSIC : constant Event_Id := 202; (Channel 1)

Entities

EVT_DELETE_ENTITY : constant Event_Id := 300; (Channel 0) (from view and game)
EVT_ENTITY_DELETED : constant Event_Id := 301; (Channel 1)
EVT_ENTITY_ATTRIBUTE_CHANGED : constant Event_Id := 302; (Channel 1)
EVT_SET_ENTITY_ATTRIBUTE : constant Event_Id := 303; (Channel 2)
EVT_MOVE_ENTITY : constant Event_Id := 304; (Channel 2)
EVT_ENTITY_MOVED : constant Event_Id := 305; (Channel 1)
EVT_RESIZE_ENTITY : constant Event_Id := 306; (Channel 2)
EVT_ENTITY_RESIZED : constant Event_Id := 307; (Channel 1)
EVT_ENTITY_SPAWNED : constant Event_Id := 308; (Channel 1)
EVT_FOLLOW_ENTITY : constant Event_Id := 309; (Channel 1)
EVT_FRAME_CHANGED : constant Event_Id := 310; (Channel 1)
EVT_LAYER_CHANGED : constant Event_Id := 311; (Channel 1)
EVT_ENTITY_DIRECTIVE : constant Event_Id := 312; (Channel 2)
EVT_MESSAGE_ENTITY : constant Event_Id := 313; (Channel 2)
EVT_SPAWN_ENTITY : constant Event_Id := 314; (Channel 2)

Game

EVT_END_GAME : constant Event_Id := 400; (Channel 2)
EVT_GAME_PAUSED : constant Event_Id := 401; (Channel 1)
EVT_GAME_STATE : constant Event_Id := 402; (Channel 1)
EVT_GAME_VAR_CHANGED : constant Event_Id := 403; (Channel 1)
EVT_LOADING_WORLD : constant Event_Id := 404; (Channel 1)
EVT_NEW_GAME : constant Event_Id := 405; (Channel 2)
EVT_PAUSE_GAME : constant Event_Id := 406; (Channel 2)
EVT_PLAYER_DIED : constant Event_Id := 407; (Channel 1)
EVT_RUN_SCRIPT : constant Event_Id := 408; (Channel 2)
EVT_SCROLL_VIEW : constant Event_Id := 409; (Channel 1)
EVT_VIEW_READY : constant Event_Id := 410; (Channel 1)

Input

EVT_KEY_PRESS : constant Event_Id := 500;
EVT_KEY_RELEASE : constant Event_Id := 501;
EVT_KEY_TYPED : constant Event_Id := 502;
EVT_MOUSE_MOVE : constant Event_Id := 503;
EVT_MOUSE_SCROLL : constant Event_Id := 504;
EVT_MOUSE_CLICK : constant Event_Id := 505;
EVT_MOUSE_DOUBLECLICK : constant Event_Id := 506;
EVT_MOUSE_HELD : constant Event_Id := 507;
EVT_MOUSE_PRESS : constant Event_Id := 508;
EVT_MOUSE_RELEASE : constant Event_Id := 509;

Lighting

EVT_LIGHT_CREATED : constant Event_Id := 600; (Channel 1)
EVT_LIGHT_DELETED : constant Event_Id := 601; (Channel 1)
EVT_LIGHT_UPDATED : constant Event_Id := 602; (Channel 1)

Particles

EVT_PARTICLE_BURST : constant Event_Id := 700; (Channel 1)
EVT_START_PARTICLES : constant Event_Id := 701; (Channel 1)
EVT_STOP_PARTICLES : constant Event_Id := 702; (Channel 1)

World

EVT_CREATE_WORLD : constant Event_Id := 800; (Channel 2)
EVT_LOAD_WORLD : constant Event_Id := 801; (Channel 0) (from view and game)
EVT_WORLD_LOADED : constant Event_Id := 802; (Channel 1)
EVT_SET_WORLD_PROPERTY : constant Event_Id := 803; (Channel 2)
EVT_WORLD_PROPERTY_CHANGED : constant Event_Id := 804; (Channel 1)
EVT_RESIZE_WORLD : constant Event_Id := 805; (Channel 2)
EVT_SET_TILE : constant Event_Id := 806; (Channel 2)
EVT_TILE_CHANGED : constant Event_Id := 807; (Channel 1)
EVT_DELETE_LAYER : constant Event_Id := 808; (Channel 2)
EVT_LAYER_DELETED : constant Event_Id := 809; (Channel 1)
EVT_CREATE_LAYER : constant Event_Id := 810; (Channel 2)
EVT_LAYER_CREATED : constant Event_Id := 810; (Channel 1)
EVT_SET_LAYER_PROPERTY : constant Event_Id := 812; (Channel 2)
EVT_LAYER_PROPERTY_CHANGED : constant Event_Id := 813; (Channel 1)
EVT_SWAP_LAYERS : constant Event_Id := 814; (Channel 2)
EVT_LAYERS_SWAPPED : constant Event_Id := 815; (Channel 1)
EVT_WORLD_MODIFIED : constant Event_Id := 816; (Channel 1)

Keen

EVT_DIALOG : constant Event_Id := 10000; (Channel 1)
EVT_DIALOG_RESPONSE : constant Event_Id := 10001; (Channel 2)
EVT_MESSAGE : constant Event_Id := 10002; (Channel 1)

Tinker

EVT_IMPORT_WORLD : constant Event_Id := 10100; (Channel 2)
EVT_SAVE_WORLD : constant Event_Id := 10101; (Channel 2)
