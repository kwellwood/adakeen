< new_game
> game_started
>   game_var_changed*
>   new_world
>     world_prop_changed*
>     entity_created*
>     follow_entity
>   play_started
...
<   pause_game(true)
>   game_paused(true)
<   pause_game(false)
>   game_paused(false)
...
>   player_dying
>   player_died

>   play_stopped
