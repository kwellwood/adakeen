--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Sessions.Tinker is

    function Create return A_Session;

private

    type Tinker_Session is new Session with null record;

    procedure Handle_Event( this : access Tinker_Session;
                            evt  : in out A_Event;
                            resp : out Response_Type );
    pragma Precondition( evt /= null );

    procedure On_Initialize( this : access Tinker_Session );

    procedure Request_Restart( this : access Tinker_Session );

end Sessions.Tinker;
