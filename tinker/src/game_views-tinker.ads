--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Commands;                          use Commands;
with Commands.History;                  use Commands.History;
with Signals.Simple;
with Signals.Keys;
with Tools;                             use Tools;
with Widgets.Buttons;                   use Widgets.Buttons;
with Widgets.Buttons.Groups;            use Widgets.Buttons.Groups;
with Widgets.Labels;                    use Widgets.Labels;
with Widgets.Menu_Items;                use Widgets.Menu_Items;
with Widgets.Panels;                    use Widgets.Panels;
with Widgets.Panels.Dialogs;            use Widgets.Panels.Dialogs;
with Widgets.Panels.Dialogs.Save_Changes;
 use Widgets.Panels.Dialogs.Save_Changes;
with Widgets.Scenes.Tinker;             use Widgets.Scenes.Tinker;
with Widgets.Scroll_Panes;              use Widgets.Scroll_Panes;
with Widgets.Sliders;                   use Widgets.Sliders;
with Widgets.Split_Panes;               use Widgets.Split_Panes;
with Widgets.Stack_Layouts;             use Widgets.Stack_Layouts;
with Widgets.Tab_Views;                 use Widgets.Tab_Views;

package Game_Views.Tinker is

    type Tinker_View is new Game_View with private;
    type A_Tinker_View is access all Tinker_View'Class;

    -- Creates a new Tinker_View object.
    function Create return A_Game_View;

    -- Signals emitted by the view
    function FilenameChanged( this : not null access Tinker_View'Class ) return access Signal'Class;   -- current world's filename changed
    function ModifiedChanged( this : not null access Tinker_View'Class ) return access Signal'Class;   -- modified state of the world changed
    function ToolChanged( this : not null access Tinker_View'Class ) return access Signal'Class;       -- the selected tool changed

    -- Returns a pointer to the view's command history manager.
    function Commands( this : not null access Tinker_View'Class ) return A_Command_History;
    pragma Postcondition( Commands'Result /= null );

    -- Returns the filename of the currently open world.
    function Get_Filename( this : not null access Tinker_View'Class ) return String;

    -- Returns a pointer to the Tinker_Scene widget.
    function Get_Scene( this : not null access Tinker_View'Class ) return A_Tinker_Scene;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Performs a dialog action. If the world has been modified and 'askToSave'
    -- is True, the dialog action will be postponed and the Save Changes?
    -- confirmation dialog will be shown first.
    procedure Do_Dialog_Action( this      : not null access Tinker_View'Class;
                                action    : Dialog_Action;
                                askToSave : Boolean := True );

    -- Opens world file 'path'. If the current world hasn't been saved since it
    -- was last modified, the user will be prompted to save their changes first.
    procedure Open_World( this : not null access Tinker_View'Class; path : String );

    -- Saves the current world to disk as a temporary file and loads it with the
    -- Keen Galaxy game.
    procedure Play_World( this : not null access Tinker_View'Class );

    -- Saves the current world to disk, returning True on success. If the
    -- current world hasn't been given a name yet, or 'saveAs' is True, the user
    -- will be prompted to chose the file to save. If an error occurs in writing
    -- the file to disk, an error message will be displayed before False
    -- is returned.
    function Save_World( this   : not null access Tinker_View'Class;
                         saveAs : Boolean := False ) return Boolean;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Returns the currently selected tool.
    function Get_Selected_Tool( this : not null access Tinker_View'Class ) return A_Tool;
    pragma Postcondition( Get_Selected_Tool'Result /= null );

    -- Replaces a tool with new tool 'replacement'. The current tool of the same
    -- type will be replaced and 'replacement' will be consumed. This is used
    -- when a tool is reconfigured.
    procedure Replace_Tool( this        : not null access Tinker_View'Class;
                            replacement : not null access Tool'Class );

    -- Selects a new tool for use.
    procedure Select_Tool( this : not null access Tinker_View'Class; toolType : Tool_Type );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Hides the current info panel if it matches 'panel', replacing it with the
    -- default empty panel. This is called, for example, when an entity is
    -- deleted to ensure its attributes panel does not remain visible. This must
    -- be called before 'panel' can be deleted by the caller, to ensure it is
    -- not rooted in the widget tree.
    procedure Hide_Info_Panel( this : not null access Tinker_View'Class; panel : A_Widget );

    -- Sets the info panel that is shown in the "Info" section of the left-hand
    -- tray, replacing the current info panel. 'panel' will become owned by the
    -- widget tree until it is hidden again, so the caller must maintain a
    -- pointer to 'panel' because it will ultimately be the caller's job to
    -- delete the widget. If 'panel' is null, the default info panel will be
    -- displayed.
    procedure Show_Info_Panel( this : not null access Tinker_View'Class; panel : A_Widget );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Returns the global pointer to the Tinker_View. Null will be returned if
    -- the view hasn't been instantiated yet.
    function Get_View return A_Tinker_View with Inline;

private

    type Tool_Array is array (Tool_Type) of A_Tool;
    type Tool_Button_Array is array (Tool_Type) of A_Button;

    type Tinker_View is new Game_View with
        record
            loaded       : Boolean := False;           -- world is loaded
            modified     : Boolean := False;           -- world modified since load/create?
            history      : A_Command_History := null;  -- undo/redo support
            selectedPath : Unbounded_String;           -- path to open (from dialog)
            filename     : Unbounded_String;           -- world filename
            recent       : List_Value;
            gridsnap     : Boolean := True;            -- snap items to grid

            -- widgets
            scene           : A_Tinker_Scene;
            worldArea       : A_Label;
            scenePos        : A_Label;
            sceneEntityArea : A_Label;
            sceneTileArea   : A_Label;
            selectedCount   : A_Label;
            sceneZoom       : A_Label;
            zoomIn          : A_Button;
            zoomOut         : A_Button;
            zoomSlider      : A_Slider;
            filenameLabel   : A_Label;
            undoButton      : A_Button;
            redoButton      : A_Button;
            saveButton      : A_Button;
            btnWorldProps   : A_Button;
            playButton      : A_Button;

            -- menubar items
            undoMenu        : A_Menu_Item;
            redoMenu        : A_Menu_Item;
            cropWorld       : A_Menu_Item;
            saveMenu        : A_Menu_Item;
            saveAsMenu      : A_Menu_Item;
            exportMenu      : A_Menu_Item;
            exportImageMenu : A_Menu_Item;

            -- dialogs
            dlgAskSave    : A_Save_Changes_Dialog;
            dlgImport     : A_Dialog;
            dlgNewWorld   : A_Dialog;
            dlgResize     : A_Dialog;
            dlgAbout      : A_Dialog;

            -- left area
            leftTray      : A_Panel;        -- tray widget
            infoPanel     : A_Scroll_Pane;
            defaultInfo   : A_Widget;
            worldInfo     : A_Widget;

            -- bottom area
            bottomSplit   : A_Split_Pane;
            bottomTray    : A_Panel;        -- tray widget
            bottomButton  : A_Button;       -- tray open/close button
            spawnModule   : A_Panel;        -- tray content

            -- right area
            rightSplit    : A_Split_Pane;
            rightTray     : A_Panel;        -- tray widget
            rightButton   : A_Button;       -- tray open/close button
            palTabs       : A_Tab_View;     -- tray content

            -- tools area
            toolButtons   : Tool_Button_Array;
            toolOptions   : A_Stack_Layout;
            toolGroup     : A_Button_Group;
            tools         : Tool_Array;
            pendingTool   : A_Tool := null;
            selectedTool  : Tool_Type := Panner_Tool;

            -- application signals
            sigModifiedChanged : aliased Signal;
            sigFilenameChanged : aliased Signal;
            sigToolChanged     : aliased Signal;
        end record;

    procedure Construct( this : access Tinker_View );

    procedure Delete( this : in out Tinker_View );

    procedure Handle_Event( this : access Tinker_View;
                            evt  : in out A_Event;
                            resp : out Response_Type );
    pragma Precondition( evt /= null );

    function Is_World_Loaded( this : not null access Tinker_View'Class ) return Boolean;

    procedure On_Close_Window( this    : access Tinker_View;
                               allowed : in out Boolean );

    -- Private helper to actually replace the existing tool with 'replacement'.
    procedure Replace_Tool_Private( this        : not null access Tinker_View'Class;
                                    replacement : not null access Tool'Class );

    ----------------------------------------------------------------------------

    package Connections is new Signals.Connections(Tinker_View);
    use Connections;

    package Key_Connections is new Signals.Keys.Connections(Tinker_View);
    use Key_Connections;

    package String_Connections is new Signals.Simple.Connections(Tinker_View);
    use String_Connections;

end Game_Views.Tinker;
