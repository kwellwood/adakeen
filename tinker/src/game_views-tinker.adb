--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Ada.Exceptions;                    use Ada.Exceptions;
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Assets.Fonts;                      use Assets.Fonts;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Native_Dialogs;            use Allegro.Native_Dialogs;
with Applications;                      use Applications;
with Applications.Gui;                  use Applications.Gui;
with Debugging;                         use Debugging;
with Easing;                            use Easing;
with Events.Tinker;                     use Events.Tinker;
with Events.World;                      use Events.World;
with GNAT.OS_Lib;                       use GNAT.OS_Lib;
with Icons;                             use Icons;
with Keyboard;                          use Keyboard;
with Signals.Keys;                      use Signals.Keys;
with Preferences;                       use Preferences;
with Rendering_Options;                 use Rendering_Options;
with Support;                           use Support;
with Support.Paths;                     use Support.Paths;
with Tool_Contexts;                     use Tool_Contexts;
with Tools.Connectors;                  use Tools.Connectors;
with Tools.Erasers;                     use Tools.Erasers;
with Tools.Panners;                     use Tools.Panners;
with Tools.Pickers;                     use Tools.Pickers;
with Tools.Pointers;                    use Tools.Pointers;
with Tools.Surveyors;                   use Tools.Surveyors;
with Values.Casting;                    use Values.Casting;
with Values.Construction;               use Values.Construction;
with Values.Maps;                       use Values.Maps;
with Values.Strings;                    use Values.Strings;
with Vector_Math;                       use Vector_Math;
with Widgets.Accordions;                use Widgets.Accordions;
with Widgets.Buttons.Pushes;            use Widgets.Buttons.Pushes;
with Widgets.Buttons.Toggles;           use Widgets.Buttons.Toggles;
with Widgets.Menubars;                  use Widgets.Menubars;
with Widgets.Menu_Items.Sub_Menus;      use Widgets.Menu_Items.Sub_Menus;
with Widgets.Palettes;                  use Widgets.Palettes;
with Widgets.Palettes.Patterns;         use Widgets.Palettes.Patterns;
with Widgets.Palettes.Terrains;         use Widgets.Palettes.Terrains;
with Widgets.Palettes.Tiles;            use Widgets.Palettes.Tiles;
with Widgets.Panels.Console_Modules;    use Widgets.Panels.Console_Modules;
with Widgets.Panels.Dialogs.About;      use Widgets.Panels.Dialogs.About;
with Widgets.Panels.Dialogs.Import_World;
 use Widgets.Panels.Dialogs.Import_World;
with Widgets.Panels.Dialogs.New_World;  use Widgets.Panels.Dialogs.New_World;
with Widgets.Panels.Dialogs.Resize_World;
 use Widgets.Panels.Dialogs.Resize_World;
with Widgets.Panels.Layer_Modules;      use Widgets.Panels.Layer_Modules;
with Widgets.Panels.Properties.Worlds;  use Widgets.Panels.Properties.Worlds;
with Widgets.Panels.Spawner_Modules;    use Widgets.Panels.Spawner_Modules;
with Widgets.Row_Layouts;               use Widgets.Row_Layouts;
with Widgets.Scenes;                    use Widgets.Scenes;
with Widgets.Scenes.Tinker.Mini_Maps;   use Widgets.Scenes.Tinker.Mini_Maps;
with Worlds;

pragma Elaborate_All( Preferences );

package body Game_Views.Tinker is

    WINDOW_TITLE : constant String := "Tinker";

    MAX_UNDO_HISTORY : constant := 100;

    MAX_RECENT_FILES : constant := 10;

    SCENE_MIN_HEIGHT  : constant := 150.0;          -- scene and header height
    BOTTOM_MIN_HEIGHT : constant := 150.0;          -- entity palette height

    LEFT_MIN_WIDTH    : constant := 226.0;          -- info panel width
    SCENE_MIN_WIDTH   : constant := 315.0;          -- scene/entity palette width
    RIGHT_MIN_WIDTH   : constant := 332.0;          -- palette panel width
    SPLIT_WIDTH       : constant := 4.0;            -- width of a Split_Pane bar

    BAR_HEIGHT : constant := 28.0;

    TRAY_CLOSED_SIZE : constant := BAR_HEIGHT;
    TRAY_SLIDE_TIME  : constant Time_Span := Milliseconds( 250 );

    -- the global view
    instance : A_Tinker_View := null;

    ----------------------------------------------------------------------------

    -- Adds world file 'path' to the recent files list and updates the menu. If
    -- the path is already on the list, it will be moved to the top.
    procedure Add_Recent_File( this : not null access Tinker_View'Class;
                               path : String );

    -- Clears the recent files list.
    procedure Clear_Recent_Files( this : not null access Tinker_View'Class );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Shows the Export Image save dialog and then saves a rendering of the
    -- scene to an image file.
    procedure Dialog_Export_Image( this : not null access Tinker_View'Class );

    -- Shows the Import Scribble dialog and then loads the imported world.
    procedure Dialog_Import_Scribble( this : not null access Tinker_View'Class );

    -- Shows a file open dialog titled 'title', located in folder 'dir', for
    -- opening files of type 'extension' (no dot included). If the user does not
    -- specify a file extension, then 'extension' will be used, but it will not
    -- be forced. If the preference 'application.openDir_<extension>' points to
    -- an existing folder, it will override 'dir'. Once a file has been chosen,
    -- the folder of the selected file will be stored in the
    -- 'application.openDir_<extension>' pref for next time the dialog is used
    -- to open a file of the same type.
    --
    -- Returns the chosen path, or an empty string if the dialog is cancelled.
    function Dialog_Open_File( this      : not null access Tinker_View'Class;
                               dir       : String;
                               title     : String;
                               extension : String ) return String;

    -- Shows the Open File dialog and then loads the selected world.
    procedure Dialog_Open_World( this : not null access Tinker_View'Class );

    -- Shows a file save dialog titled 'title', located in folder 'dir', for
    -- saving files of type 'extension' (no dot included). If the user does not
    -- specify a file extension, then 'extension' will be used, but it will not
    -- be forced. If the preference 'application.saveDir_<extension>' points to
    -- an existing folder, it will override 'dir'. Once a file has been chosen,
    -- the folder of the selected file will be stored in the
    -- 'application.saveDir_<extension>' pref for next time the dialog is used
    -- to save a file of the same type.
    --
    -- Returns the chosen path, or an empty string if the dialog is cancelled.
    -- If the user chooses an existing file, a confirmation dialog will be
    -- displayed.
    function Dialog_Save_File( this      : not null access Tinker_View'Class;
                               dir       : String;
                               title     : String;
                               extension : String ) return String;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    procedure Init_Menubar( this : not null access Tinker_View'Class );

    procedure Init_Widgets( this : not null access Tinker_View'Class );

    function Is_Modified( this : not null access Tinker_View'Class ) return Boolean is (this.modified);

    -- Loads the world file selected in the Recent menu or the open file dialog.
    procedure Load_Selected_World( this : not null access Tinker_View'Class );

    procedure Set_Filename( this : not null access Tinker_View'Class; filepath : String );

    procedure Set_Modified( this : not null access Tinker_View'Class; modified : Boolean );

    -- Displays an error dialog box, pausing gameplay while it is visible.
    procedure Show_Error( this    : not null access Tinker_View'Class;
                          title   : String;
                          heading : String;
                          text    : String );

    -- Updates the filename in the various places where it is displayed. This is
    -- called when the current filename changes.
    procedure Update_Filename( this : not null access Tinker_View'Class );

    procedure Zoom_Scene( this : not null access Tinker_View'Class;
                          key  : Key_Arguments );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    procedure On_Clicked_Tool( this : not null access Tinker_View'Class );

    procedure On_Command_History_Changed( this : not null access Tinker_View'Class );

    procedure On_Entity_Area_Changed( this : not null access Tinker_View'Class );

    procedure On_Initialize( this : not null access Tinker_View'Class );

    procedure On_Finalize( this : not null access Tinker_View'Class );

    procedure On_Loading_Failed( this : not null access Tinker_View'Class );

    procedure On_Quitting( this : not null access Tinker_View'Class );

    procedure On_Recent_List_Changed( this : not null access Tinker_View'Class );

    procedure On_Selected_Count_Changed( this : not null access Tinker_View'Class );

    procedure On_Tile_Area_Changed( this : not null access Tinker_View'Class );

    procedure On_Tool_Pos_Changed( this : not null access Tinker_View'Class );

    procedure On_Tool_Selected( this : not null access Tinker_View'Class );

    procedure On_Tool_Shortcut( this : not null access Tinker_View'Class;
                                args : Key_Arguments );

    procedure On_World_Loaded( this : not null access Tinker_View'Class );

    procedure On_World_Property_Changed( this : not null access Tinker_View'Class; name : String );

    procedure On_Zoom_Changed( this : not null access Tinker_View'Class );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- file menu

    procedure File_New( this : not null access Tinker_View'Class );

    procedure File_Open( this : not null access Tinker_View'Class );

    procedure File_Recent( this : not null access Tinker_View'Class );

    procedure File_Recent_Clear( this : not null access Tinker_View'Class );

    procedure File_Save( this : not null access Tinker_View'Class );

    procedure File_Saveas( this : not null access Tinker_View'Class );

    procedure File_Export_Image( this : not null access Tinker_View'Class );

    procedure File_Export_Scribble( this : not null access Tinker_View'Class );

    procedure File_Import_Image( this : not null access Tinker_View'Class );

    procedure File_Import_Scribble( this : not null access Tinker_View'Class );

    procedure File_Quit( this : not null access Tinker_View'Class );

    -- edit menu

    procedure Edit_Undo( this : not null access Tinker_View'Class );

    procedure Edit_Redo( this : not null access Tinker_View'Class );

    -- world menu

    procedure World_Properties( this : not null access Tinker_View'Class );

    procedure World_Crop( this : not null access Tinker_View'Class );

    procedure World_Resize( this : not null access Tinker_View'Class );

    procedure World_Respawn( this : not null access Tinker_View'Class );

    -- options menu

    procedure Options_Gridsnap_Off( this : not null access Tinker_View'Class );
    procedure Options_Gridsnap_On( this : not null access Tinker_View'Class );

    procedure Options_Free_Zooming_Off( this : not null access Tinker_View'Class );
    procedure Options_Free_Zooming_On( this : not null access Tinker_View'Class );

    -- help menu

    procedure Help_About( this : not null access Tinker_View'Class );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    procedure Changed_Zoom_Slider( this : not null access Tinker_View'Class );

    procedure Clicked_Toggle_Grid( this : not null access Tinker_View'Class );

    procedure Clicked_Toggle_Lighting( this : not null access Tinker_View'Class );

    procedure Clicked_Toggle_Light_Buffer( this : not null access Tinker_View'Class );

    procedure Clicked_Toggle_Particles( this : not null access Tinker_View'Class );

    procedure Clicked_Toggle_Shadows( this : not null access Tinker_View'Class );

    procedure Clicked_Toggle_Tray_Bottom( this : not null access Tinker_View'Class );

    procedure Clicked_Toggle_Tray_Right( this : not null access Tinker_View'Class );

    procedure Clicked_Zoom_In( this : not null access Tinker_View'Class );

    procedure Clicked_Zoom_Out( this : not null access Tinker_View'Class );

    procedure Dragged_Bottom_Tray( this : not null access Tinker_View'Class );

    procedure Dragged_Left_Tray( this : not null access Tinker_View'Class );

    procedure Dragged_Right_Tray( this : not null access Tinker_View'Class );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    procedure Shortcut_Panner( this : not null access Tinker_View'Class );

    procedure Shortcut_Pointer( this : not null access Tinker_View'Class );

    procedure Shortcut_Surveyor( this : not null access Tinker_View'Class );

    procedure Shortcut_Painter( this : not null access Tinker_View'Class );

    procedure Shortcut_Pattern( this : not null access Tinker_View'Class );

    procedure Shortcut_Stamper( this : not null access Tinker_View'Class );

    procedure Shortcut_Eraser( this : not null access Tinker_View'Class );

    procedure Shortcut_Picker( this : not null access Tinker_View'Class );

    procedure Shortcut_Connector( this : not null access Tinker_View'Class );

    procedure Shortcut_Spawner( this : not null access Tinker_View'Class );

    procedure Shortcut_Toggle_Tray_Bottom( this : not null access Tinker_View'Class );

    procedure Shortcut_Toggle_Tray_Right( this : not null access Tinker_View'Class );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    procedure Shortcut_Tool_Delete( this : not null access Tinker_View'Class );

    procedure Shortcut_Tool_Copy( this : not null access Tinker_View'Class );

    procedure Shortcut_Tool_Cut( this : not null access Tinker_View'Class );

    procedure Shortcut_Tool_Paste( this : not null access Tinker_View'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    procedure Zoom_Scene( this : not null access Tinker_View'Class;
                          key   : Key_Arguments ) is
    begin
        case key.code is
            when ALLEGRO_KEY_F1     => this.scene.Set_Zoom_And_Focus( 1.0 );
            when ALLEGRO_KEY_F2     => this.scene.Set_Zoom_And_Focus( 2.0 );
            when ALLEGRO_KEY_F3     => this.scene.Set_Zoom_And_Focus( 3.0 );
            when ALLEGRO_KEY_F4     => this.scene.Set_Zoom_And_Focus( 4.0 );
            when ALLEGRO_KEY_0      => this.scene.Set_Zoom_And_Focus( 1.0 );
            when ALLEGRO_KEY_MINUS  => this.scene.Zoom_Out;
            when ALLEGRO_KEY_EQUALS => this.scene.Zoom_In;
            when others             => pragma Assert( False, "Unrecognized key in Zoom_Scene" );
        end case;
    end Zoom_Scene;

    --==========================================================================

    function Create return A_Game_View is
        this : A_Tinker_View := new Tinker_View;
    begin
        this.Construct;
        pragma Assert( instance = null, "Multiple views instantiated" );
        instance := this;
        return A_Game_View(this);
    exception
        when others =>
            Delete( A_Game_View(this) );
            raise;
    end Create;

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access Tinker_View ) is
    begin
        Game_View(this.all).Construct( Get_Pref( "application", "maxfps", 60 ) );
        this.sigModifiedChanged.Init( this );
        this.sigFilenameChanged.Init( this );
        this.sigToolChanged    .Init( this );

        this.Initializing.Connect( Slot( this, On_Initialize'Access ) );
        this.Finalizing.Connect( Slot( this, On_Finalize'Access ) );
        this.Quitting.Connect( Slot( this, On_Quitting'Access ) );

        this.LoadingFailed.Connect( Slot( this, On_Loading_Failed'Access ) );

        this.FilenameChanged.Connect( Slot( this, Update_Filename'Access ) );
        this.ModifiedChanged.Connect( Slot( this, Update_Filename'Access ) );    -- update the filename because we display a "(modified)" suffix
        this.ToolChanged.Connect( Slot( this, On_Tool_Selected'Access ) );

        this.history := Create_Command_History( MAX_UNDO_HISTORY );
        this.history.HistoryChanged.Connect( Slot( this, On_Command_History_Changed'Access ) );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Tinker_View ) is
    begin
        Delete( this.worldInfo );
        Delete( this.toolGroup );
        Delete( this.pendingTool );
        for t in this.tools'Range loop
            Delete( this.tools( t ) );
        end loop;
        Delete( this.history );
        Game_View(this).Delete;
        instance := null;
    end Delete;

    ----------------------------------------------------------------------------

    function FilenameChanged( this : not null access Tinker_View'Class ) return access Signal'Class is (this.sigFilenameChanged'Access);

    function ModifiedChanged( this : not null access Tinker_View'Class ) return access Signal'Class is (this.sigModifiedChanged'Access);

    function ToolChanged( this : not null access Tinker_View'Class ) return access Signal'Class is (this.sigToolChanged'Access);

    ----------------------------------------------------------------------------

    procedure Add_Recent_File( this : not null access Tinker_View'Class;
                               path : String ) is
        val  : constant Value := Create( path );
        ustr : Unbounded_String;
    begin
        this.recent.Remove( this.recent.Find( val ) );
        this.recent.Prepend( val );
        if this.recent.Length > MAX_RECENT_FILES then
            this.recent.Remove( MAX_RECENT_FILES + 1 );
        end if;

        -- save the recent list to Preferences
        for i in 1..this.recent.Length loop
            Append( ustr, this.recent.Get_String( i ) & ';' );
        end loop;
        Set_Pref( "application", "recent", To_String( ustr ) );

        -- update the menu
        this.On_Recent_List_Changed;
    end Add_Recent_File;

    ----------------------------------------------------------------------------

    procedure Clear_Recent_Files( this : not null access Tinker_View'Class ) is
    begin
        this.recent.Clear;
        Set_Pref( "application", "recent", "" );
        this.On_Recent_List_Changed;
    end Clear_Recent_Files;

    ----------------------------------------------------------------------------

    function Commands( this : not null access Tinker_View'Class ) return A_Command_History is (this.history);

    ----------------------------------------------------------------------------

    procedure Dialog_Export_Image( this : not null access Tinker_View'Class ) is
        path : Unbounded_String;
        bmp  : A_Allegro_Bitmap;
    begin
        -- 1. go to the directory most recently used
        -- 2. if it doesn't exist, go to the user's home directory
        path := To_Unbounded_String( this.Dialog_Save_File( Home_Directory, "Export as Image...", "png" ) );
        if Length( path ) = 0 then
            return;                 -- dialog cancelled
        end if;
        path := To_Unbounded_String( Add_Extension( To_String( path ), "png", force => True ) );

        -- render the scene
        bmp := this.scene.Draw_To_Bitmap;
        if bmp /= null then
            -- write it to disk
            if Al_Save_Bitmap( To_String( path ), bmp ) then
                Auto_Open( To_String( path ) );
            else
                this.Show_Error( "Error Exporting Image",
                                 "Failed to write file: " & Get_Filename( To_String( path ) ),
                                 "Unknown error" );
            end if;
            Al_Destroy_Bitmap( bmp );
        else
            this.Show_Error( "Error Exporting Image",
                             "Failed to write file: " & Get_Filename( To_String( path ) ),
                             "World is too large to export" );
        end if;
    exception
        when e : others =>
            this.Show_Error( "Error Exporting Image",
                             "Failed to write file: " & Get_Filename( To_String( path ) ),
                             Exception_Message( e ) );
    end Dialog_Export_Image;

    ----------------------------------------------------------------------------

    procedure Dialog_Export_Scribble( this : not null access Tinker_View'Class ) is
        saveDir : Unbounded_String;
        path    : Unbounded_String;
    begin
        -- 1. go to the directory most recently used
        -- 2. if it doesn't exist, go to the world's directory
        -- 3. if it doesn't exist, go to the user's home directory
        if Length( this.filename ) > 0 then
            saveDir := To_Unbounded_String( Get_Directory( Get_Outer_Path( To_String( this.filename ) ) ) );    -- (2)
        end if;
        if not Is_Directory( To_String( saveDir ) ) then
            saveDir := To_Unbounded_String( Home_Directory );                                                   -- (3)
        end if;

        path := To_Unbounded_String( this.Dialog_Save_File( To_String( saveDir ), "Export as Scribble...", "world.sv" ) );
        if Length( path ) = 0 then
            return;                 -- dialog cancelled
        end if;

        Queue_Export_World( To_String( path ), "sv" );
    end Dialog_Export_Scribble;

    ----------------------------------------------------------------------------

    procedure Dialog_Import_Scribble( this : not null access Tinker_View'Class ) is
        openDir : Unbounded_String;
        path    : Unbounded_String;
    begin
        -- 1. go to the directory most recently used
        -- 2. if it doesn't exist, go to the worlds directory in media
        -- 3. if it doesn't exist, go to the base media directory
        -- 4. if it doesn't exist, go to the user's home directory
        openDir := To_Unbounded_String( Get_Pref( "application", "media" ) & "worlds" & Slash );    -- (2)
        if not Is_Directory( To_String( openDir ) ) then
            openDir := To_Unbounded_String( String'(Get_Pref( "application", "media" )) );          -- (3)
        end if;
        if not Is_Directory( To_String( openDir ) ) then
            openDir := To_Unbounded_String( Home_Directory );                                       -- (4)
        end if;

        path := To_Unbounded_String( this.Dialog_Open_File( To_String( openDir ), "Import World...", "world.sv" ) );
        if Length( path ) = 0 then
            return;                 -- dialog cancelled
        end if;

        Queue_Import_World( To_String( path ) );
    end Dialog_Import_Scribble;

    ----------------------------------------------------------------------------

    function Dialog_Open_File( this      : not null access Tinker_View'Class;
                               dir       : String;
                               title     : String;
                               extension : String ) return String is
        dialog  : A_Allegro_Filechooser;
        openDir : Unbounded_String;
        path    : Unbounded_String;
    begin
        openDir := Get_Pref( "application", "openDir_" & To_Lower( extension ), dir );
        if not Is_Directory( To_String( openDir ) ) then
            openDir := To_Unbounded_String( dir );
            if not Is_Directory( To_String( openDir ) ) then
                openDir := To_Unbounded_String( Execution_Directory );
            end if;
        end if;

        dialog := Al_Create_Native_File_Dialog( To_String( openDir ),
                                                title,
                                                "*." & To_Lower( extension ) & ";*.*",
                                                ALLEGRO_FILECHOOSER_FILE_MUST_EXIST );

        this.Pause_Game;
        if Al_Show_Native_File_Dialog( this.display, dialog ) then
            path := To_Unbounded_String( Al_Get_Native_File_Dialog_Path( dialog, 0 ) );
        end if;
        Al_Destroy_Native_File_Dialog( dialog );
        this.Resume_Game;

        Set_Pref( "application", "openDir_" & To_Lower( extension ), Get_Directory( To_String( path ) ) );
        return To_String( path );
    end Dialog_Open_File;

    ----------------------------------------------------------------------------

    procedure Dialog_Open_World( this : not null access Tinker_View'Class ) is
        openDir : Unbounded_String;
        path    : Unbounded_String;
    begin
        -- 1. go to the directory most recently used
        -- 2. if it doesn't exist, go to the worlds directory in media
        -- 3. if it doesn't exist, go to the base media directory
        -- 4. if it doesn't exist, go to the exe's directory
        openDir := To_Unbounded_String( Get_Pref( "application", "media" ) & "worlds" & Slash );    -- (2)
        if not Is_Directory( To_String( openDir ) ) then
            openDir := To_Unbounded_String( String'(Get_Pref( "application", "media" )) );          -- (3)
            if not Is_Directory( To_String( openDir ) ) then
                openDir := To_Unbounded_String( Execution_Directory );                              -- (4)
            end if;
        end if;

        path := To_Unbounded_String( this.Dialog_Open_File( To_String( openDir ), "Open World...", "world" ) );
        if Length( path ) = 0 then
            return;                 -- dialog cancelled
        end if;

        this.selectedPath := path;
        this.Load_Selected_World;
    end Dialog_Open_World;

    ----------------------------------------------------------------------------

    function Dialog_Save_File( this      : not null access Tinker_View'Class;
                               dir       : String;
                               title     : String;
                               extension : String ) return String is
        dialog  : A_Allegro_Filechooser;
        path    : Unbounded_String;
        saveDir : Unbounded_String;
    begin
        saveDir := Get_Pref( "application", "saveDir_" & To_Lower( extension ), dir );
        if not Is_Directory( To_String( saveDir ) ) then
            saveDir := To_Unbounded_String( dir );
            if not Is_Directory( To_String( saveDir ) ) then
                saveDir := To_Unbounded_String( Home_Directory );
            end if;
        end if;

        dialog := Al_Create_Native_File_Dialog( To_String( saveDir ),
                                                title,
                                                "*." & To_Lower( extension ) & ";*.*",
                                                ALLEGRO_FILECHOOSER_SAVE );

        this.Pause_Game;
        if Al_Show_Native_File_Dialog( this.display, dialog ) then
            path := To_Unbounded_String( Al_Get_Native_File_Dialog_Path( dialog, 0 ) );
            Al_Destroy_Native_File_Dialog( dialog );
            this.Resume_Game;
            if Length( path ) = 0 then
                -- the dialog was cancelled (OS X)
                return "";
            end if;

            path := To_Unbounded_String( Add_Extension( To_String( path ),
                                                        extension,
                                                        force => False ) );
        else
            -- the dialog was cancelled
            Al_Destroy_Native_File_Dialog( dialog );
            this.Resume_Game;
            return "";
        end if;

        Set_Pref( "application", "saveDir_" & To_Lower( extension ), Get_Directory( To_String( path ) ) );
        return To_String( path );
    end Dialog_Save_File;

    ----------------------------------------------------------------------------

    procedure Do_Dialog_Action( this      : not null access Tinker_View'Class;
                                action    : Dialog_Action;
                                askToSave : Boolean := True ) is
    begin
        if this.Is_Modified and then askToSave then
            this.dlgAskSave.Set_Next_Action( action );
            this.dlgAskSave.Show;
        else
            case action is
                when DA_Export_Image    => this.Dialog_Export_Image;
                when DA_Export_Scribble => this.Dialog_Export_Scribble;
                when DA_Import_Image    => this.dlgImport.Show;
                when DA_Import_Scribble => this.Dialog_Import_Scribble;
                when DA_New             => this.dlgNewWorld.Show;
                when DA_Open            => this.Dialog_Open_World;
                when DA_Recent          => this.Load_Selected_World;
                when DA_Quit            => this.Quit;
                when DA_None            => null;
            end case;
        end if;
    end Do_Dialog_Action;

    ----------------------------------------------------------------------------

    function Get_Filename( this : not null access Tinker_View'Class ) return String is (To_String( this.filename ));

    ----------------------------------------------------------------------------

    function Get_Scene( this : not null access Tinker_View'Class ) return A_Tinker_Scene is (this.scene);

    ----------------------------------------------------------------------------

    function Get_Selected_Tool( this : not null access Tinker_View'Class ) return A_Tool is
        pendingType : Tool_Type;
    begin
        -- consume the pending tool into its tool slot before returning the
        -- selected too because the pending tool might go into the selected tool
        -- slot.
        if this.pendingTool /= null then
            pendingType := this.pendingTool.Get_Type;

            this.Replace_Tool_Private( this.pendingTool );
            this.pendingTool := null;

            -- was the selected tool replaced by the pending tool?
            if pendingType = this.selectedTool then
                this.tools(this.selectedTool).On_Selected;
                this.sigToolChanged.Emit;
            end if;
        end if;

        return this.tools(this.selectedTool);
    end Get_Selected_Tool;

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Event( this : access Tinker_View;
                            evt  : in out A_Event;
                            resp : out Response_Type ) is
        pragma Unreferenced( resp );
    begin
        case evt.Get_Event_Id is
            when EVT_WORLD_MODIFIED =>
                this.Set_Modified( True );

            when EVT_WORLD_UNMODIFIED =>
                this.Set_Modified( False );

            when EVT_ERROR_MESSAGE =>
                this.Show_Error( A_Error_Message_Event(evt).Get_Title,
                                 A_Error_Message_Event(evt).Get_Heading,
                                 A_Error_Message_Event(evt).Get_Text );

            when others =>
                Game_View(this.all).Handle_Event( evt, resp );

        end case;
    end Handle_Event;

    ----------------------------------------------------------------------------

    procedure Hide_Info_Panel( this : not null access Tinker_View'Class; panel : A_Widget ) is
    begin
        if this.infoPanel /= null and then panel = this.infoPanel.Get_Client then
            this.Show_Info_Panel( this.defaultInfo );
        end if;
    end Hide_Info_Panel;

    ----------------------------------------------------------------------------

    procedure Init_Menubar( this : not null access Tinker_View'Class ) is
        mb   : A_Menubar := Create_Menubar( this, "menubar" );
        menu : A_Sub_Menu;
        sm   : A_Sub_Menu;
        mi   : A_Menu_Item;
    begin
        -- File menu --
        menu := Create_Sub_Menu( this, "pdmFile", "File" );

            mi := Create_Menu_Item( this, "miFileNew", "New...", Create_Icon( "tinker:document-block" ) );
            mi.Set_Shortcut("Ctrl+N");
            mi.Clicked.Connect( Slot( this, File_New'Access ) );
            menu.Add_Menu_Item( mi );

            mi := Create_Menu_Item( this, "miFileOpen", "Open...", Create_Icon( "tinker:folder-open" ) );
            mi.Set_Shortcut("Ctrl+O");
            mi.Clicked.Connect( Slot( this, File_Open'Access ) );
            menu.Add_Menu_Item( mi );

            sm := Create_Sub_Menu( this, "miFileRecent", "Open Recent" );
            sm.Set_Enabled( False );

                mi := Create_Menu_Item( this, "miFileRecentClear", "Clear List" );
                mi.Clicked.Connect( Slot( this, File_Recent_Clear'Access ) );
                sm.Add_Menu_Item( mi );

            menu.Add_Menu_Item( A_Menu_Item(sm) );

            mi := Create_Menu_Item( this, "miFileImport", "Import...", Create_Icon( "tinker:import-sv" ) );
            mi.Set_Shortcut("Ctrl+I");
            mi.Clicked.Connect( Slot( this, File_Import_Scribble'Access ) );
            menu.Add_Menu_Item( mi );

            mi := Create_Menu_Item( this, "miFileImportImage", "Import Image...", Create_Icon( "tinker:import-image" ) );
            mi.Clicked.Connect( Slot( this, File_Import_Image'Access ) );
            menu.Add_Menu_Item( mi );

            mi := Create_Menu_Item( this, "", "Save", Create_Icon( "tinker:disk-save" ) );
            mi.Set_Shortcut("Ctrl+S");
            mi.Clicked.Connect( Slot( this, File_Save'Access ) );
            mi.Set_Enabled( False );
            this.saveMenu := mi;
            menu.Add_Menu_Item( mi );

            mi := Create_Menu_Item( this, "", "Save As...", Create_Icon( "tinker:disk-save-as" ) );
            mi.Set_Shortcut("Shift+Ctrl+S");
            mi.Clicked.Connect( Slot( this, File_Saveas'Access ) );
            mi.Set_Enabled( False );
            this.saveAsMenu := mi;
            menu.Add_Menu_Item( mi );

            mi := Create_Menu_Item( this, "", "Export..." );
            mi.Clicked.Connect( Slot( this, File_Export_Scribble'Access ) );
            mi.Set_Enabled( False );
            this.exportMenu := mi;
            menu.Add_Menu_Item( mi );

            mi := Create_Menu_Item( this, "", "Export Image..." );
            mi.Clicked.Connect( Slot( this, File_Export_Image'Access ) );
            mi.Set_Enabled( False );
            this.exportImageMenu := mi;
            menu.Add_Menu_Item( mi );

            mi := Create_Menu_Separator( this );
            menu.Add_Menu_Item( mi );

            mi := Create_Menu_Item( this, "miFileQuit", "Quit" );
            mi.Set_Shortcut("Ctrl+Q");
            mi.Clicked.Connect( Slot( this, File_Quit'Access ) );
            menu.Add_Menu_Item( mi );

        mb.Add_Menu( menu );

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        -- Edit menu --
        menu := Create_Sub_Menu( this, "pdmEdit", "Edit" );

            mi := Create_Menu_Item( this, "", "Undo", Create_Icon( "tinker:undo" ) );
            mi.Set_Shortcut("Ctrl+Z");
            mi.Set_Enabled( False );
            mi.Clicked.Connect( Slot( this, Edit_Undo'Access ) );
            this.undoMenu := mi;
            menu.Add_Menu_Item( mi );

            mi := Create_Menu_Item( this, "", "Redo", Create_Icon( "tinker:redo" ) );
            mi.Set_Shortcut("Ctrl+Y");
            mi.Set_Enabled( False );
            mi.Clicked.Connect( Slot( this, Edit_Redo'Access ) );
            this.redoMenu := mi;
            menu.Add_Menu_Item( mi );

        mb.Add_Menu( menu );

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        -- World menu --
        menu := Create_Sub_Menu( this, "pdmWorld", "World" );

            mi := Create_Menu_Item( this, "miWorldProp", "Properties", Create_Icon( "tinker:properties-black-16" ) );
            mi.Set_Enabled( this.Is_World_Loaded );
            mi.Clicked.Connect( Slot( this, World_Properties'Access ) );
            menu.Add_Menu_Item( mi );

            mi := Create_Menu_Item( this, "miWorldCrop", "Crop", Create_Icon( "tinker:crop" ) );
            mi.Set_Shortcut("Ctrl+Shift+X");
            mi.Set_Enabled( False );
            mi.Clicked.Connect( Slot( this, World_Crop'Access ) );
            this.cropWorld := mi;
            menu.Add_Menu_Item( mi );

            mi := Create_Menu_Item( this, "miWorldResize", "Resize...", Create_Icon( "tinker:resize" ) );
            mi.Set_Enabled( this.Is_World_Loaded );
            mi.Clicked.Connect( Slot( this, World_Resize'Access ) );
            menu.Add_Menu_Item( mi );

            mi := Create_Menu_Item( this, "miWorldRespawn", "Respawn Entities" );
            mi.Set_Enabled( this.Is_World_Loaded );
            mi.Clicked.Connect( Slot( this, World_Respawn'Access ) );
            menu.Add_Menu_Item( mi );

            mi := Create_Menu_Separator( this );
            menu.Add_Menu_Item( mi );

            mi := Create_Menu_Item( this, "miWorldPlay", "Play", Create_Icon( "tinker:play" ) );
            mi.Set_Shortcut("Ctrl+P");
            mi.Set_Enabled( this.Is_World_Loaded );
            mi.Clicked.Connect( Slot( this, Play_World'Access ) );
            menu.Add_Menu_Item( mi );

        mb.Add_Menu( menu );

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        -- Options menu --
        menu := Create_Sub_Menu( this, "pdmOptions", "Options" );

            mi := Create_Menu_Checkbox( this, "miOptionsGrid", "Snap Items to Grid" );
            mi.Set_Checked( Get_Pref( "gridsnap" ) );
            mi.Selected.Connect( Slot( this,   Options_Gridsnap_On'Access ) );
            mi.Unselected.Connect( Slot( this, Options_Gridsnap_Off'Access ) );
            menu.Add_Menu_Item( mi );

            mi := Create_Menu_Checkbox( this, "miOptionsFreeZoom", "Free Zooming" );
            mi.Set_Checked( Get_Pref( "freezooming" ) );
            mi.Selected.Connect( Slot( this,   Options_Free_Zooming_On'Access ) );
            mi.Unselected.Connect( Slot( this, Options_Free_Zooming_Off'Access ) );
            menu.Add_Menu_Item( mi );

        mb.Add_Menu( menu );

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        -- Help menu --
        menu := Create_Sub_Menu( this, "pdmHelp", "Help" );

            mi := Create_Menu_Item( this, "miHelpAbout", "About...", Create_Icon( "tinker:information" ) );
            mi.Clicked.Connect( Slot( this, Help_About'Access ) );
            menu.Add_Menu_Item( mi );

        mb.Add_Menu( menu );
        this.win.Set_Menubar( mb );

        this.On_Recent_List_Changed;
    end Init_Menubar;

    ----------------------------------------------------------------------------

    procedure Init_Widgets( this : not null access Tinker_View'Class ) is
        win : constant A_Window := this.Get_Window;
    begin
        win.Set_Title( WINDOW_TITLE );
        win.Set_Min_Size( 960, 700 );

        -- [ menubar ] --
        this.Init_Menubar;

        -- [ tools ] --

        this.tools(Panner_Tool)    := Create_Panner;
        this.tools(Pointer_Tool)   := Create_Pointer;
        this.tools(Surveyor_Tool)  := Create_Surveyor;
        this.tools(Painter_Tool)   := null;
        this.tools(Pattern_Tool)   := null;
        this.tools(Stamp_Tool)     := null;
        this.tools(Terrain_Tool)   := null;
        this.tools(Eraser_Tool)    := Create_Eraser;
        this.tools(Picker_Tool)    := Create_Picker;
        this.tools(Connector_Tool) := Create_Connector;
        this.tools(Spawner_Tool)   := null;

        -- [ dialogs ] --
        this.dlgNewWorld := Create_New_Dialog( this );
        win.Add_Widget( this.dlgNewWorld );

        this.dlgAskSave := Create_Save_Changes_Dialog( this );
        win.Add_Widget( this.dlgAskSave );

        this.dlgImport := Create_Import_Dialog( this );
        win.Add_Widget( this.dlgImport );

        this.dlgResize := Create_Resize_Dialog( this );
        win.Add_Widget( this.dlgResize );

        this.dlgAbout := Create_About_Dialog( this );
        win.Add_Widget( this.dlgAbout );

        -- [ workspace ] --
        declare
            toolbar,
            statusbar,
            scenePanel,
            panel       : A_Panel;
            label       : A_Label;
            rowLayout   : A_Row_Layout;
            button      : A_Button;
            scrollPane  : A_Scroll_Pane;
            palette     : A_Palette;
            leftSplit   : A_Split_Pane;
            accord      : A_Accordion;
            miniMap     : A_Mini_Map;
            layerModule : A_Layer_Module;
        begin

            -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

            this.scene := Create_Scene( this, "scene" );
            this.scene.Set_Style( "scene" );

            this.scene.Enable_Option( TINT_ACTIVE_LAYER, Get_Pref( "drawLayerTint" ) );
            this.scene.Enable_Option( SHOW_OUTLINES, Get_Pref( "drawOutlines" ) );
            this.scene.Enable_Option( SHOW_CLIPPING, Get_Pref( "drawClipping" ) );
            this.scene.Enable_Draw_Grid( Get_Pref( "drawGrid" ) );
            this.scene.ToolPositionChanged.Connect( Slot( this, On_Tool_Pos_Changed'Access ) );
            this.scene.EntityAreaChanged.Connect( Slot( this, On_Entity_Area_Changed'Access ) );
            this.scene.TileAreaChanged.Connect( Slot( this, On_Tile_Area_Changed'Access ) );
            this.scene.SelectedCountChanged.Connect( Slot( this, On_Selected_Count_Changed'Access ) );
            this.scene.MapChanged.Connect( Slot( this, On_World_Loaded'Access ) );
            this.scene.WorldPropertyChanged.Connect( Slot( this, On_World_Property_Changed'Access ) );
            this.scene.ZoomChanged.Connect( Slot( this, On_Zoom_Changed'Access ) );

            toolbar := Create_Panel( this );
            win.Add_Widget( toolbar );
            toolbar.Set_Style( "toolbar" );
            toolbar.Fill_Horizontal( win );
            toolbar.Top.Attach( win.Top );
            toolbar.Height.Set( 40.0 );

                rowLayout := Create_Row_Layout( this );
                toolbar.Add_Widget( rowLayout );
                rowLayout.Set_Style_Property( "background.padLeft",  Create( 4 ) );
                rowLayout.Set_Style_Property( "background.spacing",  Create( 1 ) );
                rowLayout.Set_Style_Property( "background.padRight", Create( 4 ) );
                rowLayout.Fill( rowLayout.Get_Parent );

                    -- new world
                    button := Create_Push_Button( this );
                    button.Set_Style( "toolbar" );
                    button.Set_Focusable( False );
                    button.Width.Set( 32.0 );
                    button.Height.Set( 32.0 );
                    button.Set_Icon( Create_Icon( "tinker:new-file-white-32" ) );
                    button.Clicked.Connect( Slot( this, File_New'Access ) );
                    rowLayout.Append( button );

                    -- open world
                    button := Create_Push_Button( this );
                    button.Set_Style( "toolbar" );
                    button.Set_Focusable( False );
                    button.Width.Set( 32.0 );
                    button.Height.Set( 32.0 );
                    button.Set_Icon( Create_Icon( "tinker:open-white-32" ) );
                    button.Clicked.Connect( Slot( this, File_Open'Access ) );
                    rowLayout.Append( button );

                    -- save world
                    button := Create_Push_Button( this );
                    button.Set_Style( "toolbar" );
                    button.Set_Focusable( False );
                    button.Width.Set( 32.0 );
                    button.Height.Set( 32.0 );
                    button.Set_Enabled( False );
                    button.Set_Icon( Create_Icon( "tinker:save-white-32" ) );
                    button.Clicked.Connect( Slot( this, File_Save'Access ) );
                    rowLayout.Append( button );
                    this.saveButton := button;

                    -- world properties
                    button := Create_Push_Button( this );
                    button.Set_Style( "toolbar" );
                    button.Set_Focusable( False );
                    button.Width.Set( 32.0 );
                    button.Height.Set( 32.0 );
                    button.Set_Enabled( False );
                    button.Set_Icon( Create_Icon( "tinker:properties-white-32" ) );
                    button.Clicked.Connect( Slot( this, World_Properties'Access ) );
                    rowLayout.Append( button );
                    this.btnWorldProps := button;

                    -- undo
                    button := Create_Push_Button( this );
                    button.Set_Style( "toolbar" );
                    button.Set_Focusable( False );
                    button.Width.Set( 32.0 );
                    button.Height.Set( 32.0 );
                    button.Set_Enabled( False );
                    button.Set_Icon( Create_Icon( "tinker:undo-white-32" ) );
                    button.Clicked.Connect( Slot( this, Edit_Undo'Access ) );
                    rowLayout.Append( button );
                    this.undoButton := button;

                    -- redo
                    button := Create_Push_Button( this );
                    button.Set_Style( "toolbar" );
                    button.Set_Focusable( False );
                    button.Width.Set( 32.0 );
                    button.Height.Set( 32.0 );
                    button.Set_Enabled( False );
                    button.Set_Icon( Create_Icon( "tinker:redo-white-32" ) );
                    button.Clicked.Connect( Slot( this, Edit_Redo'Access ) );
                    rowLayout.Append( button );
                    this.redoButton := button;

                    label := Create_Label( this );
                    label.Set_Style( "toolbarDiv" );
                    label.Set_Preferred_Width( 7.0 );
                    rowLayout.Append( label );

                    this.toolGroup := Create_Button_Group;
                    this.toolGroup.Set_Keep_Selected( True );

                    -- panner tool
                    button := Create_Push_Button( this );
                    button.Set_Style( "toolbar" );
                    button.Set_Focusable( False );
                    button.Width.Set( 32.0 );
                    button.Height.Set( 32.0 );
                    button.Set_State( True );
                    button.Set_Enabled( False );
                    button.Set_Icon( Create_Icon( "tinker:tool-panner-32" ) );
                    button.Set_Attribute( "tool", Tool_Type'Pos( Panner_Tool ) );
                    button.Clicked.Connect( Slot( this, On_Clicked_Tool'Access ) );
                    this.toolGroup.Add( button );
                    rowLayout.Append( button );
                    this.toolButtons(Panner_Tool) := button;

                    -- pointer tool
                    button := Create_Push_Button( this );
                    button.Set_Style( "toolbar" );
                    button.Set_Focusable( False );
                    button.Width.Set( 32.0 );
                    button.Height.Set( 32.0 );
                    button.Set_Enabled( False );
                    button.Set_Icon( Create_Icon( "tinker:tool-pointer-32" ) );
                    button.Set_Attribute( "tool", Tool_Type'Pos( Pointer_Tool ) );
                    button.Clicked.Connect( Slot( this, On_Clicked_Tool'Access ) );
                    this.toolGroup.Add( button );
                    rowLayout.Append( button );
                    this.toolButtons(Pointer_Tool) := button;

                    -- surveyor tool
                    button := Create_Push_Button( this );
                    button.Set_Style( "toolbar" );
                    button.Set_Focusable( False );
                    button.Width.Set( 32.0 );
                    button.Height.Set( 32.0 );
                    button.Set_Enabled( False );
                    button.Set_Icon( Create_Icon( "tinker:tool-surveyor-32" ) );
                    button.Set_Attribute( "tool", Tool_Type'Pos( Surveyor_Tool ) );
                    button.Clicked.Connect( Slot( this, On_Clicked_Tool'Access ) );
                    this.toolGroup.Add( button );
                    rowLayout.Append( button );
                    this.toolButtons(Surveyor_Tool) := button;

                    -- painter tool
                    button := Create_Push_Button( this );
                    button.Set_Style( "toolbar" );
                    button.Set_Focusable( False );
                    button.Width.Set( 32.0 );
                    button.Height.Set( 32.0 );
                    button.Set_Enabled( False );
                    button.Set_Icon( Create_Icon( "tinker:tool-painter-32" ) );
                    button.Set_Attribute( "tool", Tool_Type'Pos( Painter_Tool ) );
                    button.Clicked.Connect( Slot( this, On_Clicked_Tool'Access ) );
                    this.toolGroup.Add( button );
                    rowLayout.Append( button );
                    this.toolButtons(Painter_Tool) := button;

                    -- pattern tool
                    button := Create_Push_Button( this );
                    button.Set_Style( "toolbar" );
                    button.Set_Focusable( False );
                    button.Width.Set( 32.0 );
                    button.Height.Set( 32.0 );
                    button.Set_Enabled( False );
                    button.Set_Icon( Create_Icon( "tinker:tool-pattern-32" ) );
                    button.Set_Attribute( "tool", Tool_Type'Pos( Pattern_Tool ) );
                    button.Clicked.Connect( Slot( this, On_Clicked_Tool'Access ) );
                    this.toolGroup.Add( button );
                    rowLayout.Append( button );
                    this.toolButtons(Pattern_Tool) := button;

                    -- stamper tool
                    button := Create_Push_Button( this );
                    button.Set_Style( "toolbar" );
                    button.Set_Focusable( False );
                    button.Width.Set( 32.0 );
                    button.Height.Set( 32.0 );
                    button.Set_Enabled( False );
                    button.Set_Icon( Create_Icon( "tinker:tool-stamper-32" ) );
                    button.Set_Attribute( "tool", Tool_Type'Pos( Stamp_Tool ) );
                    button.Clicked.Connect( Slot( this, On_Clicked_Tool'Access ) );
                    this.toolGroup.Add( button );
                    rowLayout.Append( button );
                    this.toolButtons(Stamp_Tool) := button;

                    -- terrain tool
                    button := Create_Push_Button( this );
                    button.Set_Style( "toolbar" );
                    button.Set_Focusable( False );
                    button.Width.Set( 32.0 );
                    button.Height.Set( 32.0 );
                    button.Set_Enabled( False );
                    button.Set_Icon( Create_Icon( "tinker:tool-terrain-32" ) );
                    button.Set_Attribute( "tool", Tool_Type'Pos( Terrain_Tool ) );
                    button.Clicked.Connect( Slot( this, On_Clicked_Tool'Access ) );
                    this.toolGroup.Add( button );
                    rowLayout.Append( button );
                    this.toolButtons(Terrain_Tool) := button;

                    -- picker tool
                    button := Create_Push_Button( this );
                    button.Set_Style( "toolbar" );
                    button.Set_Focusable( False );
                    button.Width.Set( 32.0 );
                    button.Height.Set( 32.0 );
                    button.Set_Enabled( False );
                    button.Set_Icon( Create_Icon( "tinker:tool-picker-32" ) );
                    button.Set_Attribute( "tool", Tool_Type'Pos( Picker_Tool ) );
                    button.Clicked.Connect( Slot( this, On_Clicked_Tool'Access ) );
                    this.toolGroup.Add( button );
                    rowLayout.Append( button );
                    this.toolButtons(Picker_Tool) := button;

                    -- eraser tool
                    button := Create_Push_Button( this );
                    button.Set_Style( "toolbar" );
                    button.Set_Focusable( False );
                    button.Width.Set( 32.0 );
                    button.Height.Set( 32.0 );
                    button.Set_Enabled( False );
                    button.Set_Icon( Create_Icon( "tinker:tool-eraser-32" ) );
                    button.Set_Attribute( "tool", Tool_Type'Pos( Eraser_Tool ) );
                    button.Clicked.Connect( Slot( this, On_Clicked_Tool'Access ) );
                    this.toolGroup.Add( button );
                    rowLayout.Append( button );
                    this.toolButtons(Eraser_Tool) := button;

                    -- connector tool
                    button := Create_Push_Button( this );
                    button.Set_Style( "toolbar" );
                    button.Set_Focusable( False );
                    button.Width.Set( 32.0 );
                    button.Height.Set( 32.0 );
                    button.Set_Enabled( False );
                    button.Set_Icon( Create_Icon( "tinker:tool-connector-32" ) );
                    button.Set_Attribute( "tool", Tool_Type'Pos( Connector_Tool ) );
                    button.Clicked.Connect( Slot( this, On_Clicked_Tool'Access ) );
                    this.toolGroup.Add( button );
                    rowLayout.Append( button );
                    this.toolButtons(Connector_Tool) := button;

                    -- spawner tool
                    button := Create_Push_Button( this );
                    button.Set_Style( "toolbar" );
                    button.Set_Focusable( False );
                    button.Width.Set( 32.0 );
                    button.Height.Set( 32.0 );
                    button.Set_Enabled( False );
                    button.Set_Icon( Create_Icon( "tinker:tool-spawner-32" ) );
                    button.Set_Attribute( "tool", Tool_Type'Pos( Spawner_Tool ) );
                    button.Clicked.Connect( Slot( this, On_Clicked_Tool'Access ) );
                    this.toolGroup.Add( button );
                    rowLayout.Append( button );
                    this.toolButtons(Spawner_Tool) := button;

                    label := Create_Label( this );
                    label.Set_Style( "toolbarDiv" );
                    label.Set_Preferred_Width( 7.0 );
                    rowLayout.Append( label );

                    this.toolOptions := Create_Stack_Layout( this );
                    rowLayout.Append( this.toolOptions );
                    rowLayout.Set_Expander( A_Widget(this.toolOptions) );

                        -- Panner options
                        this.toolOptions.Append( this.tools(Panner_Tool).Create_Options( A_Game_View(this) ) );

                        -- Pointer options (none)
                        this.toolOptions.Append( this.tools(Pointer_Tool).Create_Options( A_Game_View(this) ) );

                        -- Surveyor options
                        this.toolOptions.Append( this.tools(Surveyor_Tool).Create_Options( A_Game_View(this) ) );

                        -- Painter options
                        this.toolOptions.Append( Create_Panel( this ) );      -- place holder until Painter_Tool created

                        -- Pattern options
                        this.toolOptions.Append( Create_Panel( this ) );      -- place holder until Pattern_Tool created

                        -- Stamper options
                        this.toolOptions.Append( Create_Panel( this ) );      -- place holder until Stamper_Tool created

                        -- Terrain options
                        this.toolOptions.Append( Create_Panel( this ) );      -- place holder until Terrain_Tool created

                        -- Eraser options
                        this.toolOptions.Append( this.tools(Eraser_Tool).Create_Options( A_Game_View(this) ) );

                        -- Picker options
                        this.toolOptions.Append( this.tools(Picker_Tool).Create_Options( A_Game_View(this) ) );

                        -- Connector options
                        this.toolOptions.Append( this.tools(Connector_Tool).Create_Options( A_Game_View(this) ) );

                        -- Spawner options
                        this.toolOptions.Append( Create_Panel( this ) );      -- place holder until Spawner_Tool created

                    this.toolOptions.Set_Index( 1 );

            -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

            statusbar := Create_Panel( this );
            win.Add_Widget( statusbar );
            statusbar.Set_Style( "statusbar" );
            statusbar.Fill_Horizontal( win );
            statusbar.Bottom.Attach( win.Bottom );
            statusbar.Height.Set( BAR_HEIGHT );

                rowLayout := Create_Row_Layout( this );
                statusbar.Add_Widget( rowLayout );
                rowLayout.Fill( rowLayout.Get_Parent );
                rowLayout.Set_Style_Property( "background.padLeft", Create( 5 ) );
                rowLayout.Set_Style_Property( "background.spacing", Create( 5 ) );

                    -- world size
                    this.worldArea := Create_Label( this );
                    this.worldArea.Set_Icon( Create_Icon( "tinker:dimensions-16" ) );
                    this.worldArea.Set_Style( "statusbar" );
                    this.worldArea.Width.Set( 100.0 );
                    rowLayout.Append( this.worldArea );

                    label := Create_Label( this );
                    label.Set_Style( "statusbarDiv" );
                    rowLayout.Append( label );

                    -- mouse position
                    this.scenePos := Create_Label( this );
                    this.scenePos.Set_Icon( Create_Icon( "tinker:ruler" ) );
                    this.scenePos.Set_Style( "statusbar" );
                    this.scenePos.Width.Set( 100.0 );
                    rowLayout.Append( this.scenePos );

                    label := Create_Label( this );
                    label.Set_Style( "statusbarDiv" );
                    rowLayout.Append( label );

                    -- selected tile area size
                    this.sceneTileArea := Create_Label( this );
                    this.sceneTileArea.Set_Style( "statusbar" );
                    this.sceneTileArea.Width.Set( 100.0 );
                    this.sceneTileArea.Set_Icon( Create_Icon( "tinker:selection-16" ) );
                    rowLayout.Append( this.sceneTileArea );

                    label := Create_Label( this );
                    label.Set_Style( "statusbarDiv" );
                    rowLayout.Append( label );

                    -- selected entity area size
                    this.sceneEntityArea := Create_Label( this );
                    this.sceneEntityArea.Set_Style( "statusbar" );
                    this.sceneEntityArea.Width.Set( 100.0 );
                    this.sceneEntityArea.Set_Icon( Create_Icon( "tinker:selection-16" ) );
                    rowLayout.Append( this.sceneEntityArea );

                    label := Create_Label( this );
                    label.Set_Style( "statusbarDiv" );
                    rowLayout.Append( label );

                    -- selected entity count
                    this.selectedCount := Create_Label( this );
                    this.selectedCount.Set_Style( "statusbar" );
                    this.selectedCount.Width.Set( 150.0 );
                    rowLayout.Append( this.selectedCount );

                    -- expander
                    panel := Create_Panel( this );
                    panel.Set_Style_Property( "background.color", Create( "#00000000" ) );
                    rowLayout.Append( panel );
                    rowLayout.Set_Expander( panel );

                    label := Create_Label( this );
                    label.Set_Style( "statusbar" );
                    label.Set_Text( "Zoom" );
                    rowLayout.Append( label );

                    this.sceneZoom := Create_Label( this );
                    this.sceneZoom.Set_Style( "statusbar" );
                    this.sceneZoom.Set_Text( "100%" );
                    this.sceneZoom.Width.Set( 40.0 );
                    rowLayout.Append( this.sceneZoom );

                    label := Create_Label( this );
                    label.Set_Style( "statusbarDiv" );
                    rowLayout.Append( label );

                    this.zoomOut := Create_Push_Button( this );
                    this.zoomOut.Set_Style( "statusbar" );
                    this.zoomOut.Set_Icon( Create_Icon( "tinker:minus-white-16" ) );
                    this.zoomOut.Width.Set( BAR_HEIGHT );
                    this.zoomOut.Set_Enabled( False );
                    this.zoomOut.Clicked.Connect( Slot( this, Clicked_Zoom_Out'Access ) );
                    rowLayout.Append( this.zoomOut );

                    this.zoomSlider := Create_Slider( this );
                    this.zoomSlider.Set_Style( "statusbar" );
                    this.zoomSlider.Width.Set( 100.0 );
                    this.zoomSlider.Set_Range( Log( Widgets.Scenes.Tinker.ZOOM_MIN ),
                                               Log( Widgets.Scenes.Tinker.ZOOM_MAX ) );
                    this.zoomSlider.Set_Value( Log( this.scene.Zoom.Get ) );
                    this.zoomSlider.Changed.Connect( Slot( this, Changed_Zoom_Slider'Access ) );
                    this.zoomSlider.Enable_Continuous_Change( True );
                    this.zoomSlider.Set_Enabled( False );
                    rowLayout.Append( this.zoomSlider );

                    this.zoomIn := Create_Push_Button( this );
                    this.zoomIn.Set_Style( "statusbar" );
                    this.zoomIn.Set_Icon( Create_Icon( "tinker:plus-white-16" ) );
                    this.zoomIn.Width.Set( this.zoomOut.Width.Get );
                    this.zoomIn.Set_Enabled( False );
                    this.zoomIn.Clicked.Connect( Slot( this, Clicked_Zoom_In'Access ) );
                    rowLayout.Append( this.zoomIn );

            -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

            this.rightSplit := Create_Split_Pane( this, Vertical, Left_Side );
            win.Add_Widget( this.rightSplit );
            this.rightSplit.Top.Attach( toolbar.Bottom );
            this.rightSplit.Bottom.Attach( statusbar.Top );
            this.rightSplit.Fill_Horizontal( win );
            this.rightSplit.Set_Style( "tray" );
            this.rightSplit.Enable_Resize( Get_Pref( "rightOpen" ) );
            this.rightSplit.Dragged.Connect( Slot( this, Dragged_Right_Tray'Access ) );

                leftSplit := Create_Split_Pane( this, Vertical, Right_Side );
                this.rightSplit.Set_Left( leftSplit, LEFT_MIN_WIDTH + SPLIT_WIDTH + SCENE_MIN_WIDTH );
                leftSplit.Set_Style( "tray" );
                leftSplit.Dragged.Connect( Slot( this, Dragged_Left_Tray'Access ) );

                    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                    -- Center Column (Scene Area + Bottom Tray)

                    this.bottomSplit := Create_Split_Pane( this, Horizontal, Left_Side );
                    leftSplit.Set_Right( this.bottomSplit, SCENE_MIN_WIDTH );
                    this.bottomSplit.Set_Style( "tray" );
                    this.bottomSplit.Enable_Resize( Get_Pref( "bottomOpen" ) );
                    this.bottomSplit.Dragged.Connect( Slot( this, Dragged_Bottom_Tray'Access ) );

                        -- - - - - - - - - - - - - - - - - - - - - - - - - - - -
                        -- Scene Area

                        scenePanel := Create_Panel( this );
                        this.bottomSplit.Set_Left( scenePanel, SCENE_MIN_HEIGHT );

                            panel := Create_Panel( this );
                            scenePanel.Add_Widget( panel );
                            panel.Set_Style( "sceneHeader" );
                            panel.Fill_Horizontal( panel.Get_Parent );
                            panel.Top.Attach( panel.Get_Parent.Top );
                            panel.Height.Set( BAR_HEIGHT );

                                label := Create_Label( this );
                                panel.Add_Widget( label );
                                label.Set_Style( "sceneHeader" );
                                label.Fill( label.Get_Parent, 4.0, 0.0, 4.0, 0.0 );
                                label.Visible.Set( False );
                                label.Set_Icon( Create_Icon( "tinker:file-16" ) );
                                this.filenameLabel := label;

                                rowLayout := Create_Row_Layout( this );
                                panel.Add_Widget( rowLayout );
                                rowLayout.Fill( rowLayout.Get_Parent );
                                rowLayout.Set_Style_Property( "background.spacing", Create( 1 ) );
                                rowLayout.Set_Style_Property( "background.align", Create( "right" ) );

                                    button := Create_Push_Button( this );
                                    button.Set_Style( "sceneOptions" );
                                    button.Set_Icon( Create_Icon( "tinker:grid-white-16" ) );
                                    button.Clicked.Connect( Slot( this, Clicked_Toggle_Grid'Access ) );
                                    rowLayout.Append( button );

                                    button := Create_Toggle_Button( this );
                                    button.Set_Style( "sceneOptions" );
                                    button.Set_Icon( Create_Icon( "tinker:shadows-white-16" ) );
                                    button.Set_State( this.scene.Is_Enabled( SHOW_SHADOW_GEOM ) );
                                    button.Pressed.Connect( Slot( this, Clicked_Toggle_Shadows'Access ) );
                                    button.Released.Connect( Slot( this, Clicked_Toggle_Shadows'Access ) );
                                    rowLayout.Append( button );

                                    button := Create_Toggle_Button( this );
                                    button.Set_Style( "sceneOptions" );
                                    button.Set_Icon( Create_Icon( "tinker:atom-white-16" ) );
                                    button.Set_State( this.scene.Is_Enabled( SHOW_PARTICLES ) );
                                    button.Pressed.Connect( Slot( this, Clicked_Toggle_Particles'Access ) );
                                    button.Released.Connect( Slot( this, Clicked_Toggle_Particles'Access ) );
                                    rowLayout.Append( button );

                                    button := Create_Push_Button( this );
                                    button.Set_Style( "sceneOptions" );
                                    button.Set_Icon( Create_Icon( "tinker:light-on-white-16" ) );
                                    button.Clicked.Connect( Slot( this, Clicked_Toggle_Lighting'Access ) );
                                    rowLayout.Append( button );

                                    button := Create_Push_Button( this );
                                    button.Set_Style( "sceneOptions" );
                                    button.Set_Icon( Create_Icon( "tinker:play-white-16" ) );
                                    button.Clicked.Connect( Slot( this, Play_World'Access ) );
                                    button.Set_Enabled( False );
                                    rowLayout.Append( button );
                                    this.playButton := button;

                            scenePanel.Add_Widget( this.scene );
                            this.scene.Top.Attach( panel.Bottom );
                            this.scene.Bottom.Attach( this.scene.Get_Parent.Bottom );
                            this.scene.Fill_Horizontal( this.scene.Get_Parent );

                        -- - - - - - - - - - - - - - - - - - - - - - - - - - - -
                        -- Bottom Tray

                        this.bottomTray := Create_Panel( this );
                        this.bottomSplit.Set_Right( this.bottomTray, BOTTOM_MIN_HEIGHT );
                        this.bottomTray.Set_Style( "tray" );
                        this.bottomTray.Set_Attribute( "openSize", Float'Max( BOTTOM_MIN_HEIGHT, Get_Pref( "bottomSize" ) ) );
                        if Get_Pref( "bottomOpen" ) then
                            this.bottomTray.Height.Set( Float'Min( this.bottomTray.Get_Attribute( "openSize" ).To_Float, this.bottomSplit.Get_Right_Max_Size ) );
                        else
                            this.bottomTray.Height.Set( TRAY_CLOSED_SIZE );
                        end if;

                            this.spawnModule := A_Panel(Create_Spawner_Module( this ));
                            A_Spawner_Module(this.spawnModule).Append_Tab( Create_Console_Module( this ), "Console", Create_Icon( "tinker:console-16" ) );
                            this.bottomTray.Add_Widget( this.spawnModule );
                            this.spawnModule.Fill( this.spawnModule.Get_Parent );
                            this.spawnModule.Visible.Set( Get_Pref( "bottomOpen" ) );

                            button := Create_Toggle_Button( this );
                            this.bottomTray.Add_Widget( button );
                            button.Right.Attach( button.Get_Parent.Right );
                            button.Top.Attach( button.Get_Parent.Top );
                            button.Height.Set( BAR_HEIGHT );
                            button.Set_Style( "expander" );
                            button.Set_State( Get_Pref( "bottomOpen" ) );
                            button.Pressed.Connect( Slot( this, Clicked_Toggle_Tray_Bottom'Access ) );
                            button.Released.Connect( Slot( this, Clicked_Toggle_Tray_Bottom'Access ) );
                            this.bottomButton := button;

                    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                    -- Left Tray

                    this.leftTray := Create_Panel( this );
                    leftSplit.Set_Left( this.leftTray, LEFT_MIN_WIDTH );
                    this.leftTray.Set_Style( "tray" );
                    this.leftTray.Width.Set( Float'Max( LEFT_MIN_WIDTH, Get_Pref( "leftSize" ) ) );
                    -- ^^^ the width of the left tray (left side of left split)
                    -- must be set after the right side of the left split has
                    -- been added.

                        accord := Create_Accordion( this );
                        this.leftTray.Add_Widget( accord );
                        accord.Fill( accord.Get_Parent );
                        accord.Set_Style( "infopanel" );

                            miniMap := Create_Mini_Map( this );
                            miniMap.Height.Set( 150.0 );
                            miniMap.Set_Scene( this.scene );
                            accord.Append( miniMap, "Mini Map" );

                            layerModule := Create_Layer_Module( this, this.scene );
                            layerModule.Set_Style( "infopanel" );
                            accord.Append( layerModule, "Layers" );

                            this.infoPanel := Create_Scroll_Pane( this );
                            this.infoPanel.Draw_Hbar( False );
                            this.infoPanel.Set_Style( "infopanel" );
                            accord.Append( this.infoPanel, "Info" );
                            accord.Set_Expander( accord.Count );

                                this.defaultInfo := A_Widget(Create_Panel( this ));
                                this.infoPanel.Set_Client( this.defaultInfo );

                                this.worldInfo := A_Widget(Create_World_Module( this ));

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                -- Right Tray

                this.rightTray := Create_Panel( this );
                this.rightSplit.Set_Right( this.rightTray, RIGHT_MIN_WIDTH );
                this.rightTray.Set_Style( "tray" );
                this.rightTray.Set_Attribute( "openSize", Float'Max( RIGHT_MIN_WIDTH, Get_Pref( "rightSize" ) ) );
                if Get_Pref( "rightOpen" ) then
                    this.rightTray.Width.Set( Float'Min( this.rightTray.Get_Attribute( "openSize" ).To_Float, this.rightSplit.Get_Right_Max_Size ) );
                else
                    this.rightTray.Width.Set( TRAY_CLOSED_SIZE );
                end if;

                    this.palTabs := Create_Tab_View( this );
                    this.rightTray.Add_Widget( this.palTabs );
                    this.palTabs.Fill( this.palTabs.Get_Parent );
                    this.palTabs.Set_Style( "tilepanel" );
                    this.palTabs.Visible.Set( Get_Pref( "rightOpen" ) );

                        scrollPane := Create_Scroll_Pane( this );
                        this.palTabs.Append( scrollPane, "Tiles", Create_Icon( "tinker:tool-painter-16" ) );
                        scrollPane.Draw_Hbar( False );
                        scrollPane.Set_Style( "tilepanel" );

                            palette := Create_Tile_Palette( this );
                            palette.Set_Columns( 18 );
                            palette.Enable_Animation( True );
                            palette.Set_Style( "tilepanel" );
                            scrollPane.Draw_Hbar( False );
                            scrollPane.Set_Client( palette );

                        -- - - - - - - - - - - - - - - - - - - - - - - - - - - -

                        scrollPane := Create_Scroll_Pane( this );
                        this.palTabs.Append( scrollPane, "Patterns", Create_Icon( "tinker:tool-pattern-16" ) );
                        scrollPane.Draw_Hbar( False );
                        scrollPane.Set_Style( "tilepanel" );

                            palette := Create_Pattern_Palette( this );
                            A_Pattern_Palette(palette).Show_Type( "pattern" );
                            palette.Set_Columns( 18 );
                            palette.Enable_Animation( True );
                            palette.Set_Style( "tilepanel" );
                            scrollPane.Set_Client( palette );

                        -- - - - - - - - - - - - - - - - - - - - - - - - - - - -

                        scrollPane := Create_Scroll_Pane( this );
                        this.palTabs.Append( scrollPane, "Stamps", Create_Icon( "tinker:cursor-stamper" ) );
                        scrollPane.Draw_Hbar( False );
                        scrollPane.Set_Style( "tilepanel" );

                            palette := Create_Pattern_Palette( this );
                            A_Pattern_Palette(palette).Show_Type( "stamp" );
                            palette.Set_Columns( 18 );
                            palette.Enable_Animation( True );
                            palette.Set_Style( "tilepanel" );
                            scrollPane.Set_Client( palette );

                        -- - - - - - - - - - - - - - - - - - - - - - - - - - - -

                        scrollPane := Create_Scroll_Pane( this );
                        this.palTabs.Append( scrollPane, "Terrains", Create_Icon( "tinker:tool-terrain-16" ) );
                        scrollPane.Draw_Hbar( False );
                        scrollPane.Set_Style( "tilepanel" );

                            palette := Create_Terrain_Palette( this );
                            palette.Set_Columns( 2 );
                            palette.Enable_Animation( True );
                            palette.Set_Style( "tilepanel" );
                            scrollPane.Set_Client( palette );

                    button := Create_Toggle_Button( this );
                    this.rightTray.Add_Widget( button );
                    button.Right.Attach( button.Get_Parent.Right );
                    button.Top.Attach( button.Get_Parent.Top );
                    button.Height.Set( BAR_HEIGHT );
                    button.Set_Style( "expander" );
                    button.Set_State( Get_Pref( "rightOpen" ) );
                    button.Pressed.Connect( Slot( this, Clicked_Toggle_Tray_Right'Access ) );
                    button.Released.Connect( Slot( this, Clicked_Toggle_Tray_Right'Access ) );
                    this.rightButton := button;
        end;

        -- [ initialize tools ] --

       for i in this.tools'Range loop
           if this.tools(i) /= null then
               this.tools(i).Init( this.scene );
           end if;
       end loop;

        -- [ shortcuts ] --

        -- menus
#if OSX'Defined then
        this.win.KeyTyped.Connect( Slot( this, File_New'Access,             ALLEGRO_KEY_N, (CMD=>Yes, others=>No) ) );
        this.win.KeyTyped.Connect( Slot( this, File_Open'Access,            ALLEGRO_KEY_O, (CMD=>Yes, others=>No) ) );
        this.win.KeyTyped.Connect( Slot( this, File_Import_Scribble'Access, ALLEGRO_KEY_I, (CMD=>Yes, others=>No) ) );
        this.win.KeyTyped.Connect( Slot( this, File_Save'Access,            ALLEGRO_KEY_S, (CMD=>Yes, others=>No) ) );
        this.win.KeyTyped.Connect( Slot( this, Edit_Undo'Access,            ALLEGRO_KEY_Z, (CMD=>Yes, others=>No) ) );
        this.win.KeyTyped.Connect( Slot( this, Edit_Redo'Access,            ALLEGRO_KEY_Y, (CMD=>Yes, others=>No) ) );
        this.win.KeyTyped.Connect( Slot( this, World_Crop'Access,           ALLEGRO_KEY_X, (CMD=>Yes, SHIFT=>Yes, others=>No) ) );
        this.win.KeyTyped.Connect( Slot( this, Play_World'Access,           ALLEGRO_KEY_P, (CMD=>Yes, others=>No) ) );
#else
        this.win.KeyTyped.Connect( Slot( this, File_New'Access,             ALLEGRO_KEY_N, (CTRL=>Yes, others=>No) ) );
        this.win.KeyTyped.Connect( Slot( this, File_Open'Access,            ALLEGRO_KEY_O, (CTRL=>Yes, others=>No) ) );
        this.win.KeyTyped.Connect( Slot( this, File_Import_Scribble'Access, ALLEGRO_KEY_I, (CTRL=>Yes, others=>No) ) );
        this.win.KeyTyped.Connect( Slot( this, File_Save'Access,            ALLEGRO_KEY_S, (CTRL=>Yes, others=>No) ) );
        this.win.KeyTyped.Connect( Slot( this, Edit_Undo'Access,            ALLEGRO_KEY_Z, (CTRL=>Yes, others=>No) ) );
        this.win.KeyTyped.Connect( Slot( this, Edit_Redo'Access,            ALLEGRO_KEY_Y, (CTRL=>Yes, others=>No) ) );
        this.win.KeyTyped.Connect( Slot( this, World_Crop'Access,           ALLEGRO_KEY_X, (CTRL=>Yes, SHIFT=>Yes, others=>No) ) );
        this.win.KeyTyped.Connect( Slot( this, Play_World'Access,           ALLEGRO_KEY_P, (CTRL=>Yes, others=>No) ) );
#end if;

        -- tools
        win.KeyTyped.Connect( Slot( this, Shortcut_Panner'Access,    ALLEGRO_KEY_P, (others => No) ) );
        win.KeyTyped.Connect( Slot( this, Shortcut_Pointer'Access,   ALLEGRO_KEY_S, (others => No) ) );
        win.KeyTyped.Connect( Slot( this, Shortcut_Surveyor'Access,  ALLEGRO_KEY_V, (others => No) ) );
        win.KeyTyped.Connect( Slot( this, Shortcut_Painter'Access,   ALLEGRO_KEY_T, (others => No) ) );
        win.KeyTyped.Connect( Slot( this, Shortcut_Pattern'Access,   ALLEGRO_KEY_M, (others => No) ) );
        win.KeyTyped.Connect( Slot( this, Shortcut_Stamper'Access,   ALLEGRO_KEY_A, (others => No) ) );
        win.KeyTyped.Connect( Slot( this, Shortcut_Eraser'Access,    ALLEGRO_KEY_E, (others => No) ) );
        win.KeyTyped.Connect( Slot( this, Shortcut_Picker'Access,    ALLEGRO_KEY_K, (others => No) ) );
        win.KeyTyped.Connect( Slot( this, Shortcut_Connector'Access, ALLEGRO_KEY_C, (others => No) ) );
        win.KeyTyped.Connect( Slot( this, Shortcut_Spawner'Access,   ALLEGRO_KEY_N, (others => No) ) );

        win.KeyPressed.Connect( Slot( this, Shortcut_Panner'Access,  ALLEGRO_KEY_ESCAPE, (others => No) ) );

        -- shared tool functions
        win.KeyTyped.Connect( Slot( this, Shortcut_Tool_Delete'Access, ALLEGRO_KEY_DELETE, (others=>No) ) );
#if OSX'Defined then
        win.KeyTyped.Connect( Slot( this, Shortcut_Tool_Copy'Access,   ALLEGRO_KEY_C,         (CMD=>Yes, others=>No) ) );
        win.KeyTyped.Connect( Slot( this, Shortcut_Tool_Cut'Access,    ALLEGRO_KEY_X,         (CMD=>Yes, others=>No) ) );
        win.KeyTyped.Connect( Slot( this, Shortcut_Tool_Paste'Access,  ALLEGRO_KEY_V,         (CMD=>Yes, others=>No) ) );
        win.KeyTyped.Connect( Slot( this, On_Tool_Shortcut'Access,     ALLEGRO_KEY_A,         (CMD=>Yes, others=>No) ) );
        win.KeyTyped.Connect( Slot( this, On_Tool_Shortcut'Access,     ALLEGRO_KEY_MINUS,     (CMD=>Yes, others=>No) ) );
        win.KeyTyped.Connect( Slot( this, On_Tool_Shortcut'Access,     ALLEGRO_KEY_PAD_MINUS, (CMD=>Yes, others=>No) ) );
        win.KeyTyped.Connect( Slot( this, On_Tool_Shortcut'Access,     ALLEGRO_KEY_EQUALS,    (CMD=>Yes, others=>No) ) );
        win.KeyTyped.Connect( Slot( this, On_Tool_Shortcut'Access,     ALLEGRO_KEY_PAD_PLUS,  (CMD=>Yes, others=>No) ) );
#else
        win.KeyTyped.Connect( Slot( this, Shortcut_Tool_Copy'Access,   ALLEGRO_KEY_C,         (CTRL=>Yes, others=>No) ) );
        win.KeyTyped.Connect( Slot( this, Shortcut_Tool_Cut'Access,    ALLEGRO_KEY_X,         (CTRL=>Yes, others=>No) ) );
        win.KeyTyped.Connect( Slot( this, Shortcut_Tool_Paste'Access,  ALLEGRO_KEY_V,         (CTRL=>Yes, others=>No) ) );
        win.KeyTyped.Connect( Slot( this, On_Tool_Shortcut'Access,     ALLEGRO_KEY_A,         (CTRL=>Yes, others=>No) ) );
        win.KeyTyped.Connect( Slot( this, On_Tool_Shortcut'Access,     ALLEGRO_KEY_MINUS,     (CTRL=>Yes, others=>No) ) );
        win.KeyTyped.Connect( Slot( this, On_Tool_Shortcut'Access,     ALLEGRO_KEY_PAD_MINUS, (CTRL=>Yes, others=>No) ) );
        win.KeyTyped.Connect( Slot( this, On_Tool_Shortcut'Access,     ALLEGRO_KEY_EQUALS,    (CTRL=>Yes, others=>No) ) );
        win.KeyTyped.Connect( Slot( this, On_Tool_Shortcut'Access,     ALLEGRO_KEY_PAD_PLUS,  (CTRL=>Yes, others=>No) ) );
#end if;

        -- zoom
        win.KeyTyped.Connect( Slot( this, Zoom_Scene'Access, ALLEGRO_KEY_F1,     (others => No) ) );
        win.KeyTyped.Connect( Slot( this, Zoom_Scene'Access, ALLEGRO_KEY_F2,     (others => No) ) );
        win.KeyTyped.Connect( Slot( this, Zoom_Scene'Access, ALLEGRO_KEY_F3,     (others => No) ) );
        win.KeyTyped.Connect( Slot( this, Zoom_Scene'Access, ALLEGRO_KEY_F4,     (others => No) ) );
        win.KeyTyped.Connect( Slot( this, Zoom_Scene'Access, ALLEGRO_KEY_0,      (others => No) ) );
        win.KeyTyped.Connect( Slot( this, Zoom_Scene'Access, ALLEGRO_KEY_MINUS,  (others => No) ) );
        win.KeyTyped.Connect( Slot( this, Zoom_Scene'Access, ALLEGRO_KEY_EQUALS, (others => No) ) );

        -- window layout
        win.KeyTyped.Connect( Slot( this, Shortcut_Toggle_Tray_Right'Access,  ALLEGRO_KEY_CLOSEBRACE, (others => No) ) );
        win.KeyTyped.Connect( Slot( this, Shortcut_Toggle_Tray_Bottom'Access, ALLEGRO_KEY_QUOTE,      (others => No) ) );

        -- scene drawing
        win.KeyTyped.Connect( Slot( this, Clicked_Toggle_Light_Buffer'Access, ALLEGRO_KEY_L, (CTRL => Yes, others => No) ) );
    end Init_Widgets;

    ----------------------------------------------------------------------------

    function Is_World_Loaded( this : not null access Tinker_View'Class ) return Boolean is (this.scene /= null and then this.scene.Is_Map_Loaded);

    ----------------------------------------------------------------------------

    procedure Load_Selected_World( this : not null access Tinker_View'Class ) is
        path : constant String := To_String( this.selectedPath );
    begin
        if path'Length > 0 then
            this.selectedPath := To_Unbounded_String( "" );
            Set_Pref( "application", "openDir_" & To_Lower( Get_Extension( path ) ), Get_Directory( path ) );
            this.Add_Recent_File( path );
            Queue_Load_World( path );
        end if;
    end Load_Selected_World;

    ----------------------------------------------------------------------------

    procedure Open_World( this : not null access Tinker_View'Class; path : String ) is
    begin
        this.selectedPath := To_Unbounded_String( path );
        this.Do_Dialog_Action( DA_Recent );
    end Open_World;

    ----------------------------------------------------------------------------

    procedure On_Clicked_Tool( this : not null access Tinker_View'Class ) is
    begin
        this.Select_Tool( Tool_Type'Val( A_Widget(this.Signaller).Get_Attribute( "tool" ).To_Int ) );
        this.scene.Request_Focus;
    end On_Clicked_Tool;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Close_Window( this    : access Tinker_View;
                               allowed : in out Boolean ) is
    begin
        this.Do_Dialog_Action( DA_Quit );
        allowed := False;
    end On_Close_Window;

    ----------------------------------------------------------------------------

    procedure On_Command_History_Changed( this : not null access Tinker_View'Class ) is
    begin
        this.undoMenu.Set_Enabled( this.Commands.Can_Undo );
        this.redoMenu.Set_Enabled( this.Commands.Can_Redo );
        this.undoButton.Set_Enabled( this.Commands.Can_Undo );
        this.redoButton.Set_Enabled( this.Commands.Can_Redo );
    end On_Command_History_Changed;

    ----------------------------------------------------------------------------

    procedure On_Entity_Area_Changed( this : not null access Tinker_View'Class ) is
        area : constant Rectangle := this.scene.Get_Entity_Area;
    begin
        if area.width * area.height > 0.0 then
            this.sceneEntityArea.Set_Text( Image( Integer(Float'Ceiling( area.width )) ) & " x " &
                                           Image( Integer(Float'Ceiling( area.height )) ) );
        else
            this.sceneEntityArea.Set_Text( "" );
        end if;
    end On_Entity_Area_Changed;

    ----------------------------------------------------------------------------

    procedure On_Initialize( this : not null access Tinker_View'Class ) is
    begin
        this.Get_Corral.Add_Listener( this, EVT_WORLD_MODIFIED );
        this.Get_Corral.Add_Listener( this, EVT_WORLD_UNMODIFIED );
        this.Get_Corral.Add_Listener( this, EVT_ERROR_MESSAGE );
        this.Get_Corral.Add_Listener( this, EVT_WORLD_LOADED );

        Register_Font_Name( "standard", "dejavusans.ttf" );  -- may raise exception
#if WINDOWS'Defined then
        Register_Font_Name( "monospace", System_Font_Directory & "cour.ttf" );
#elsif OSX'Defined then
        Register_Font_Name( "monospace", System_Font_Directory & "Courier New.ttf" );
#elsif LINUX'Defined then
        Register_Font_Name( "monospace", System_Font_Directory & "truetype/dejavu/DejaVuSansMono.ttf" );
#end if;

        declare
            recent : constant String := Get_Pref( "application", "recent" );
            first  : Integer := recent'First;
            last   : Integer := Index( recent, ";", first );
            path   : String_Value;
        begin
            this.recent := Create_List.Lst;
            loop
                if last < first then
                    last := recent'Last + 1;
                end if;
                exit when last <= first;
                path := Create( recent(first..last-1) ).Str;

                if this.recent.Length < MAX_RECENT_FILES and then path.Length > 0 then
                    this.recent.Append( path.To_String );
                end if;
                first := last + 1;
                last := Index( recent, ";", first + 1 );
            end loop;
        end;

        this.Init_Widgets;

        this.Start_Game;
    end On_Initialize;

    ----------------------------------------------------------------------------

    procedure On_Finalize( this : not null access Tinker_View'Class ) is
    begin
        this.Get_Corral.Remove_Listener( this );
    end On_Finalize;

    ----------------------------------------------------------------------------

    procedure On_Loading_Failed( this : not null access Tinker_View'Class ) is
    begin
        this.Show_Error( "File Error", "Error opening world", this.Get_Load_Error );
    end On_Loading_Failed;

    ----------------------------------------------------------------------------

    procedure On_Quitting( this : not null access Tinker_View'Class ) is
    begin
        this.Show_Info_Panel( null );
    end On_Quitting;

    ----------------------------------------------------------------------------

    procedure On_Recent_List_Changed( this : not null access Tinker_View'Class ) is
        menu : constant A_Sub_Menu := A_Sub_Menu(this.Get_Widget( "miFileRecent" ));
        item : A_Menu_Item;
    begin
        menu.Set_Enabled( False );
        menu.Clear;

        for i in 1..this.recent.Length loop
            item := Create_Menu_Item( this, "", Get_Filename( this.recent.Get_String( i ) ) );
            item.Set_Attribute( "path", this.recent.Get( i ) );
            item.Clicked.Connect( Slot( this, File_Recent'Access ) );
            menu.Add_Menu_Item( item );
        end loop;
        if this.recent.Length > 0 then
            menu.Set_Enabled( True );
        end if;

        item := Create_Menu_Separator( this );
        menu.Add_Menu_Item( item );

        item := Create_Menu_Item( this, "miFileRecentClear", "Clear List" );
        item.Clicked.Connect( Slot( this, File_Recent_Clear'Access ) );
        menu.Add_Menu_Item( item );
    end On_Recent_List_Changed;

    ----------------------------------------------------------------------------

    procedure On_Selected_Count_Changed( this : not null access Tinker_View'Class ) is
    begin
        if this.scene.Get_Entity_Selection_Count > 0 then
            this.selectedCount.Set_Text( Image( this.scene.Get_Entity_Selection_Count ) & " items selected" );
        else
            this.selectedCount.Set_Text( "" );
        end if;
    end On_Selected_Count_Changed;

    ----------------------------------------------------------------------------

    procedure On_Tile_Area_Changed( this : not null access Tinker_View'Class ) is
        area : constant Rectangle := this.scene.Get_Tile_Area;
    begin
        this.cropWorld.Set_Enabled( area.width * area.height > 0.0 );
        if this.cropWorld.Is_Enabled then
            this.sceneTileArea.Set_Text( Image( Integer(area.width) / this.scene.Get_Tile_Width ) & " x " &
                                         Image( Integer(area.height) / this.scene.Get_Tile_Width ) );
        else
            this.sceneTileArea.Set_Text( "" );
        end if;
    end On_Tile_Area_Changed;

    ----------------------------------------------------------------------------

    procedure On_Tool_Pos_Changed( this : not null access Tinker_View'Class ) is
        toolX : constant Float := this.scene.Get_Tool_X;
        toolY : constant Float := this.scene.Get_Tool_Y;
    begin
        if toolX >= 0.0 and toolY >= 0.0 then
            this.scenePos.Set_Text( Image( Integer(toolX) ) & ", " & Image( Integer(toolY) ) );
        else
            this.scenePos.Set_Text( "" );
        end if;
    end On_Tool_Pos_Changed;

    ----------------------------------------------------------------------------

    procedure On_Tool_Selected( this : not null access Tinker_View'Class ) is
        selectedIndex : constant Integer := 1 + Tool_Type'Pos( this.selectedTool );
    begin
        if this.toolButtons(this.selectedTool) /= null then
            -- set the tool to be selected
            this.toolButtons(this.selectedTool).Set_State( True );
            this.toolOptions.Set_Index( selectedIndex );
        else
            -- the tool doesn't have a button, like Spawner_Tool
            this.toolGroup.Unset( force => True );
            this.toolOptions.Set_Index( 1 );
        end if;
    end On_Tool_Selected;

    ----------------------------------------------------------------------------

    procedure On_Tool_Shortcut( this : not null access Tinker_View'Class;
                                args : Key_Arguments ) is
    begin
        if this.scene.Is_Map_Loaded then
            this.Get_Selected_Tool.Handle_Key_Typed( args.code, args.modifiers );
        end if;
    end On_Tool_Shortcut;

    ----------------------------------------------------------------------------

    procedure On_World_Loaded( this : not null access Tinker_View'Class ) is
    begin
        this.Set_Modified( False );

        -- update action buttons
        this.btnWorldProps.Set_Enabled( this.Is_World_Loaded );
        this.Show_Info_Panel( (if this.Is_World_Loaded then this.worldInfo else null) );

        -- update current tool
        this.Select_Tool( Panner_Tool );
        Delete( this.tools(Painter_Tool) );   -- no selected tile
        Delete( this.tools(Pattern_Tool) );   -- no selected pattern
        Delete( this.tools(Stamp_Tool) );     -- no selected stamp
        Delete( this.tools(Terrain_Tool) );   -- no selected terrain

        -- enable tool buttons
        for t in Tool_Type'Range loop
            if this.toolButtons(t) /= null then
                this.toolButtons(t).Set_Enabled( this.tools(t) /= null );
            end if;
        end loop;

        -- reset the visible palette to Tiles
        this.palTabs.Set_Index( 1 );

        -- update scene option buttons
        this.playButton.Set_Enabled( this.Is_World_Loaded );

        -- update menu items
        this.Get_Widget( "miWorldProp" ).Set_Enabled( this.Is_World_Loaded );
        this.Get_Widget( "miWorldResize" ).Set_Enabled( this.Is_World_Loaded );
        this.Get_Widget( "miWorldRespawn" ).Set_Enabled( this.Is_World_Loaded );
        this.Get_Widget( "miWorldPlay" ).Set_Enabled( this.Is_World_Loaded );

        this.exportMenu.Set_Enabled( this.Is_World_Loaded );
        this.exportImageMenu.Set_Enabled( this.Is_World_Loaded );

        -- the "Save" and "Save As" menu items are available whenever a world
        -- is loaded.
        this.saveMenu.Set_Enabled( this.Is_World_Loaded );
        this.saveAsMenu.Set_Enabled( this.Is_World_Loaded );

        -- the zoom buttons on the statusbar are enabled whenever a world is loaded
        this.zoomIn.Set_Enabled( this.Is_World_Loaded );
        this.zoomOut.Set_Enabled( this.Is_World_Loaded );
        this.zoomSlider.Set_Enabled( this.Is_World_Loaded );

        if this.Is_World_Loaded then
            this.worldArea.Set_Text( Image( this.scene.Get_Width_Tiles ) & " x " &
                                     Image( this.scene.Get_Height_Tiles ) );
        else
            this.worldArea.Set_Text( "" );
        end if;

        -- the "Save" button on the action bar is only enabled when the current
        -- world has been modified, to help show the modification state.
        this.saveButton.Set_Enabled( this.Is_World_Loaded );

        this.Commands.Clear;

        this.Resume_Game;
    end On_World_Loaded;

    ----------------------------------------------------------------------------

    procedure On_World_Property_Changed( this : not null access Tinker_View'Class; name : String ) is
    begin
        if name = "filepath" then
            this.Set_Filename( Cast_String( this.scene.Get_World_Property( name ) ) );
        end if;
    end On_World_Property_Changed;

    ----------------------------------------------------------------------------

    procedure On_Zoom_Changed( this : not null access Tinker_View'Class ) is
    begin
        -- call Update_Value() instead of Set_Value() to change the slider's
        -- internal value to reflect the scene's zoom level without emitting
        -- another signal to change it.
        this.sceneZoom.Set_Text( Image( Integer(Float'Rounding( this.scene.Zoom.Get * 100.0 )) ) & "%" );
        this.zoomSlider.Update_Value( Log( this.scene.Zoom.Get ) );
    end On_Zoom_Changed;

    ----------------------------------------------------------------------------

    procedure Play_World( this : not null access Tinker_View'Class ) is
        exe  : Unbounded_String;
        args : String_List_Access;
        pid  : Process_Id := Invalid_Pid;
    begin
        if not this.Is_World_Loaded then
            return;
        end if;

        -- save the world in a temporary file
        if not Trigger_Save_Temporary_World( Temp_Directory & "play.world" ) then
            this.Show_Error( "Error Playing World",
                             "Cannot play world",
                             "Failed to write temporary file" );
            return;
        end if;

        -- launch keen to play the temporary world file
        args := new String_List'(new String'("-level"),
                                 new String'(Temp_Directory & "play.world"));

        exe := To_Unbounded_String( Add_Extension( Execution_Directory & "keen", Executable_Extension ) );
#if OSX'Defined then
        if not Is_Regular_File( To_String( exe ) ) then
            exe := To_Unbounded_String(
                Normalize_Pathname( Path_Append( Execution_Directory,
                                                 "../../../Keen Galaxy.app/Contents/MacOS/"
                                               ) & "keen" )
            );
        end if;
#end if;
        pragma Debug( Dbg( "Opening:" & To_String( exe ) & " " & args(1).all & " " & args(2).all, D_VIEW, Info ) );
        pid := Non_Blocking_Spawn( To_String( exe ), args.all );
        Free( args );
        if pid = Invalid_Pid then
            this.Show_Error( "Error Playing World",
                             "Cannot play world",
                             "Failed to launch Keen Galaxy" );
        end if;
    end Play_World;

    ----------------------------------------------------------------------------

    procedure Replace_Tool( this        : not null access Tinker_View'Class;
                            replacement : not null access Tool'Class ) is
        replacementType : constant Tool_Type := replacement.Get_Type;
    begin
        -- don't replace the selected tool, it may be in use on the stack. set
        -- it pending so that the next call to Get_Selected_Tool() or Select_Tool()
        -- will do the replacement later.
        if replacementType = this.selectedTool then
            pragma Assert( this.pendingTool = null );
            this.pendingTool := A_Tool(replacement);
            return;
        end if;

        this.Replace_Tool_Private( replacement );
    end Replace_Tool;

    ----------------------------------------------------------------------------

    procedure Replace_Tool_Private( this        : not null access Tinker_View'Class;
                                    replacement : not null access Tool'Class ) is
        replacementType : constant Tool_Type := replacement.Get_Type;
        index           : constant Integer := 1 + Tool_Type'Pos( replacementType );
        options         : A_Widget;
    begin
        -- replace the tool in its slot
        Delete( this.tools(replacementType) );
        this.tools(replacementType) := A_Tool(replacement);

        -- associate it with the scene
        this.tools(replacementType).Init( this.scene );

        -- replace the options
        options := this.toolOptions.Remove( index );
        Delete( options );
        this.toolOptions.Insert( index, this.tools(replacementType).Create_Options( A_Game_View(this) ) );

        -- enable the tool button
        if this.toolButtons(replacementType) /= null then
            this.toolButtons(replacementType).Set_Enabled( True );
        end if;

        -- ensure the tool's options are visible
        if replacementType = this.selectedTool then
            this.toolOptions.Set_Index( index );
        end if;
    end Replace_Tool_Private;

    ----------------------------------------------------------------------------

    function Save_World( this   : not null access Tinker_View'Class;
                         saveAs : Boolean := False ) return Boolean is
        path    : Unbounded_String;
        saveDir : Unbounded_String;
    begin
        if this.Get_Filename'Length = 0 or else Is_Path_In_Archive( this.Get_Filename ) or else saveAs then
            -- 1. go to the directory most recently used
            -- 2. if it doesn't exist, go to the worlds directory in media
            -- 3. if it doesn't exist, go to the base media directory
            -- 4. if it doesn't exist, go to the exe's directory
            saveDir := Get_Pref( "application", "media" ) & "worlds" & Slash;   -- (2)
            if not Is_Directory( To_String( saveDir ) ) then
                saveDir := Get_Pref( "application", "media" );                  -- (3)
                if not Is_Directory( To_String( saveDir ) ) then
                    saveDir := To_Unbounded_String( Execution_Directory );      -- (4)
                end if;
            end if;

            path := To_Unbounded_String( this.Dialog_Save_File( To_String( saveDir ), "Save World...", Worlds.World_Extension ) );
            if Length( path ) = 0 then
                return False;             -- dialog cancelled
            end if;

            this.Add_Recent_File( To_String( path ) );
        else
            path := To_Unbounded_String( this.Get_Filename );
        end if;

        Queue_Save_World( To_String( path ) );
        this.Set_Filename( To_String( path ) );
        return True;
    end Save_World;

    ----------------------------------------------------------------------------

    procedure Select_Tool( this : not null access Tinker_View'Class; toolType : Tool_Type ) is
    begin
        -- consume the pending tool into its tool slot before changing the
        -- selected tool because the pending tool might go into the tool slot
        -- that will become selected.
        --
        -- don't consume a tool pending for the selected tool slot because it
        -- may still be in use.
        if this.pendingTool /= null and then this.pendingTool.Get_Type /= this.selectedTool then
            this.Replace_Tool_Private( this.pendingTool );
            this.pendingTool := null;
        end if;

        -- if the selected tool type is changing and there is a tool in that slot...
        if toolType /= this.selectedTool and this.tools(toolType) /= null then
            if this.tools(this.selectedTool) /= null then
                this.tools(this.selectedTool).On_Unselected;
            end if;
            this.selectedTool := toolType;
            this.tools(this.selectedTool).On_Selected;
            this.sigToolChanged.Emit;
        end if;
    end Select_Tool;

    ----------------------------------------------------------------------------

    procedure Set_Filename( this : not null access Tinker_View'Class; filepath : String ) is
    begin
        if filepath /= To_String( this.filename ) then
            this.filename := To_Unbounded_String( filepath );
            this.sigFilenameChanged.Emit;
        end if;
    end Set_Filename;

    ----------------------------------------------------------------------------

    procedure Show_Info_Panel( this : not null access Tinker_View'Class; panel : A_Widget ) is
        prev : A_Widget;
        pragma Warnings( Off, prev );
    begin
        if this.infoPanel /= null and then panel /= this.infoPanel.Get_Client then
            -- remove the current info panel but don't delete it. the caller that
            -- showed it before should be maintaining a pointer to it.
            prev := this.infoPanel.Remove_Client;

            this.infoPanel.Set_Client( (if panel /= null then panel else this.defaultInfo) );
        end if;
    end Show_Info_Panel;

    ----------------------------------------------------------------------------

    procedure Set_Modified( this : not null access Tinker_View'Class; modified : Boolean ) is
    begin
        if this.modified /= modified then
            this.modified := modified;
            this.sigModifiedChanged.Emit;
        end if;
    end Set_Modified;

    ----------------------------------------------------------------------------

    procedure Show_Error( this    : not null access Tinker_View'Class;
                          title   : String;
                          heading : String;
                          text    : String ) is
    begin
        this.Pause_Game;
        A_Gui_Application(Get_Application).Show_Error( title, heading, text );
        this.Resume_Game;
    end Show_Error;

    ----------------------------------------------------------------------------

    procedure Update_Filename( this : not null access Tinker_View'Class ) is
        filename : constant String := (if this.Get_Filename'Length > 0 then Get_Filename( this.Get_Filename ) else "Untitled");
        modified : constant String := (if this.modified then " (modified)" else "");
    begin
        this.win.Set_Title( WINDOW_TITLE & " - " & filename & modified );
        this.filenameLabel.Set_Text( filename & modified );
        this.filenameLabel.Visible.Set( True );
    end Update_Filename;

    ----------------------------------------------------------------------------

    function Get_View return A_Tinker_View is (instance);

    --==========================================================================
    --Menu Handlers

    procedure File_New( this : not null access Tinker_View'Class ) is
    begin
        this.Do_Dialog_Action( DA_New );
    end File_New;

    ----------------------------------------------------------------------------

    procedure File_Open( this : not null access Tinker_View'Class ) is
    begin
        this.Do_Dialog_Action( DA_Open );
    end File_Open;

    ----------------------------------------------------------------------------

    procedure File_Recent( this : not null access Tinker_View'Class ) is
    begin
        this.Open_World( Cast_String( A_Widget(this.Signaller).Get_Attribute( "path" ) ) );
    end File_Recent;

    ----------------------------------------------------------------------------

    procedure File_Recent_Clear( this : not null access Tinker_View'Class ) is
    begin
        this.Clear_Recent_Files;
    end File_Recent_Clear;

    ----------------------------------------------------------------------------

    procedure File_Save( this : not null access Tinker_View'Class ) is
    begin
        if this.Is_World_Loaded then
            if this.Save_World( saveAs => False ) then
                null;
            end if;
        end if;
    end File_Save;

    ----------------------------------------------------------------------------

    procedure File_Saveas( this : not null access Tinker_View'Class ) is
    begin
        if this.Is_World_Loaded then
            if this.Save_World( saveAs => True ) then
                null;
            end if;
        end if;
    end File_Saveas;

    ----------------------------------------------------------------------------

    procedure File_Import_Image( this : not null access Tinker_View'Class ) is
    begin
        this.Do_Dialog_Action( DA_Import_Image );
    end File_Import_Image;

    ----------------------------------------------------------------------------

    procedure File_Import_Scribble( this : not null access Tinker_View'Class ) is
    begin
        this.Do_Dialog_Action( DA_Import_Scribble );
    end File_Import_Scribble;

    ----------------------------------------------------------------------------

    procedure File_Export_Image( this : not null access Tinker_View'Class ) is
    begin
        this.Do_Dialog_Action( DA_Export_Image, askToSave => False );
    end File_Export_Image;

    ----------------------------------------------------------------------------

    procedure File_Export_Scribble( this : not null access Tinker_View'Class ) is
    begin
        this.Do_Dialog_Action( DA_Export_Scribble, askToSave => False );
    end File_Export_Scribble;

    ----------------------------------------------------------------------------

    procedure File_Quit( this : not null access Tinker_View'Class ) is
    begin
        this.Do_Dialog_Action( DA_Quit );
    end File_Quit;

    ----------------------------------------------------------------------------

    procedure Edit_Undo( this : not null access Tinker_View'Class ) is
    begin
        if not this.Get_Selected_Tool.Is_Busy then
            this.Commands.Undo;
        end if;
    end Edit_Undo;

    ----------------------------------------------------------------------------

    procedure Edit_Redo( this : not null access Tinker_View'Class ) is
    begin
        if not this.Get_Selected_Tool.Is_Busy then
            this.Commands.Redo;
        end if;
    end Edit_Redo;

    ----------------------------------------------------------------------------

    procedure World_Properties( this : not null access Tinker_View'Class ) is
    begin
        if this.Is_World_Loaded then
            this.Show_Info_Panel( this.worldInfo );
        end if;
    end World_Properties;

    ----------------------------------------------------------------------------

    procedure World_Crop( this : not null access Tinker_View'Class ) is
        area : constant Rectangle := this.scene.Get_Tile_Area;
    begin
        if area.width * area.height > 0.0 then
            -- immediately clear the selected area and the undo history so the
            -- command can't be repeated before the Game thread responds with
            -- the World_Loaded event.
            this.tools(Surveyor_Tool).Do_Clear;
            this.Commands.Clear;

            Queue_Resize_World( Integer(area.x) / this.scene.Get_Tile_Width,
                                Integer(area.y) / this.scene.Get_Tile_Width,
                                Integer(area.width) / this.scene.Get_Tile_Width,
                                Integer(area.height) / this.scene.Get_Tile_Width );
        end if;
    end World_Crop;

    ----------------------------------------------------------------------------

    procedure World_Resize( this : not null access Tinker_View'Class ) is
    begin
        if this.Is_World_Loaded then
            this.dlgResize.Show;
        end if;
    end World_Resize;

    ----------------------------------------------------------------------------

    procedure World_Respawn( this : not null access Tinker_View'Class ) is
    begin
        if this.Is_World_Loaded then
            -- clear the undo history because this operation can't be undone
            this.tools(Surveyor_Tool).Do_Clear;
            this.Commands.Clear;

            Queue_Repawn_Entities;
        end if;
    end World_Respawn;

    ----------------------------------------------------------------------------

    procedure Options_Free_Zooming_Off( this : not null access Tinker_View'Class ) is
        pragma Unreferenced( this );
    begin
        Set_Pref( "freezooming", False );
    end Options_Free_Zooming_Off;

    ----------------------------------------------------------------------------

    procedure Options_Free_Zooming_On( this : not null access Tinker_View'Class ) is
        pragma Unreferenced( this );
    begin
        Set_Pref( "freezooming", True );
    end Options_Free_Zooming_On;

    ----------------------------------------------------------------------------

    procedure Options_Gridsnap_Off( this : not null access Tinker_View'Class ) is
        pragma Unreferenced( this );
    begin
        Set_Pref( "gridsnap", False );
    end Options_Gridsnap_Off;

    ----------------------------------------------------------------------------

    procedure Options_Gridsnap_On( this : not null access Tinker_View'Class ) is
        pragma Unreferenced( this );
    begin
        Set_Pref( "gridsnap", True );
    end Options_Gridsnap_On;

    ----------------------------------------------------------------------------

    procedure Help_About( this : not null access Tinker_View'Class ) is
    begin
        this.dlgAbout.Show;
    end Help_About;

    ----------------------------------------------------------------------------

    procedure Changed_Zoom_Slider( this : not null access Tinker_View'Class ) is
    begin
        this.scene.Set_Zoom_And_Focus( Exp( this.zoomSlider.Get_Value ) );
    end Changed_Zoom_Slider;

    ----------------------------------------------------------------------------

    procedure Clicked_Toggle_Grid( this : not null access Tinker_View'Class ) is
    begin
        if String'(Get_Pref( "drawGrid" )) = "" then
            Set_Pref( "drawGrid", "light" );
        elsif String'(Get_Pref( "drawGrid" )) = "light" then
            Set_Pref( "drawGrid", "dark" );
        else -- Get_Pref( "drawGrid" ) = "dark"
            Set_Pref( "drawGrid", "" );
        end if;
        this.scene.Enable_Draw_Grid( Get_Pref( "drawGrid" ) );
    end Clicked_Toggle_Grid;

    ----------------------------------------------------------------------------

    procedure Clicked_Toggle_Lighting( this : not null access Tinker_View'Class ) is
    begin
        -- cycle between normal lighting, light buffer, and no lighting
        if this.scene.Is_Enabled( SHOW_LIGHT_BUFFER ) then
            -- disable lighting
            this.scene.Enable_Option( SHOW_LIGHT_BUFFER, False );
            this.scene.Enable_Option( SHOW_LIGHTING, False );
        elsif this.scene.Is_Enabled( SHOW_LIGHTING ) then
            -- enable light buffer
            this.scene.Enable_Option( SHOW_LIGHT_BUFFER, True );
        else
            -- enable normal lighting
            this.scene.Enable_Option( SHOW_LIGHTING, True );
        end if;
    end Clicked_Toggle_Lighting;

    ----------------------------------------------------------------------------

    procedure Clicked_Toggle_Light_Buffer( this : not null access Tinker_View'Class ) is
    begin
        this.scene.Enable_Option( SHOW_LIGHT_BUFFER, not this.scene.Is_Enabled( SHOW_LIGHT_BUFFER ) );
    end Clicked_Toggle_Light_Buffer;

    ----------------------------------------------------------------------------

    procedure Clicked_Toggle_Particles( this : not null access Tinker_View'Class ) is
    begin
        this.scene.Enable_Option( SHOW_PARTICLES, not this.scene.Is_Enabled( SHOW_PARTICLES ) );
    end Clicked_Toggle_Particles;

    ----------------------------------------------------------------------------

    procedure Clicked_Toggle_Shadows( this : not null access Tinker_View'Class ) is
    begin
        this.scene.Enable_Option( SHOW_SHADOW_GEOM or
                                  --SHOW_LIGHT_QTREE or
                                  SHOW_LIGHT_GEOM,
                                  not this.scene.Is_Enabled( SHOW_SHADOW_GEOM ) );
    end Clicked_Toggle_Shadows;

    ----------------------------------------------------------------------------

    procedure Clicked_Toggle_Tray_Bottom( this : not null access Tinker_View'Class ) is
        opening : constant Boolean := not Get_Pref( "bottomOpen" );
        height  : Float;
    begin
        if opening then
            -- open the entity module
            height := Constrain( this.bottomTray.Get_Attribute( "openSize" ).To_Float, BOTTOM_MIN_HEIGHT, this.bottomSplit.Get_Right_Max_Size );
        else
            -- store the open size
            if not this.bottomTray.Height.Animating then
                this.bottomTray.Set_Attribute( "openSize", Float'Max( BOTTOM_MIN_HEIGHT, this.bottomTray.Height.Get ) );
            end if;

            -- close the tile palettes
            height := TRAY_CLOSED_SIZE;
        end if;

        Set_Pref( "bottomOpen", opening );
        this.bottomSplit.Enable_Resize( opening );
        this.spawnModule.Visible.Set( opening, TRAY_SLIDE_TIME, Use_Easing( Impulse_Out, opening ) );
        this.bottomTray.Height.Set( height, TRAY_SLIDE_TIME, Use_Easing( Quad_Out, opening ) );
    end Clicked_Toggle_Tray_Bottom;

    ----------------------------------------------------------------------------

    procedure Clicked_Toggle_Tray_Right( this : not null access Tinker_View'Class ) is
        opening : constant Boolean := not Get_Pref( "rightOpen" );
        width   : Float;
    begin
        if opening then
            -- open the tile palettes
            width := Constrain( this.rightTray.Get_Attribute( "openSize" ).To_Float, RIGHT_MIN_WIDTH, this.rightSplit.Get_Right_Max_Size );
        else
            -- store the open size
            if not this.rightTray.Width.Animating then
                this.rightTray.Set_Attribute( "openSize", Float'Max( RIGHT_MIN_WIDTH, this.rightTray.Width.Get ) );
            end if;

            -- close the tile palettes
            width := TRAY_CLOSED_SIZE;
        end if;

        Set_Pref( "rightOpen", opening );
        this.rightSplit.Enable_Resize( opening );
        this.palTabs.Visible.Set( opening, TRAY_SLIDE_TIME, Use_Easing( Impulse_Out, opening ) );
        this.rightTray.Width.Set( width, TRAY_SLIDE_TIME, Use_Easing( Quad_Out, opening ) );
    end Clicked_Toggle_Tray_Right;

    ----------------------------------------------------------------------------

    procedure Clicked_Zoom_In( this : not null access Tinker_View'Class ) is
    begin
        this.scene.Zoom_In;
    end Clicked_Zoom_In;

    ----------------------------------------------------------------------------

    procedure Clicked_Zoom_Out( this : not null access Tinker_View'Class ) is
    begin
        this.scene.Zoom_Out;
    end Clicked_Zoom_Out;

    ----------------------------------------------------------------------------

    procedure Dragged_Bottom_Tray( this : not null access Tinker_View'Class ) is
    begin
        Set_Pref( "bottomSize", Float'Max( BOTTOM_MIN_HEIGHT, this.bottomTray.Height.Get ) );
    end Dragged_Bottom_Tray;

    ----------------------------------------------------------------------------

    procedure Dragged_Left_Tray( this : not null access Tinker_View'Class ) is
    begin
        Set_Pref( "leftSize", Float'Max( LEFT_MIN_WIDTH, this.leftTray.Width.Get ) );
    end Dragged_Left_Tray;

    ----------------------------------------------------------------------------

    procedure Dragged_Right_Tray( this : not null access Tinker_View'Class ) is
    begin
        Set_Pref( "rightSize", Float'Max( RIGHT_MIN_WIDTH, this.rightTray.Width.Get ) );
    end Dragged_Right_Tray;

    ----------------------------------------------------------------------------

    procedure Shortcut_Panner( this : not null access Tinker_View'Class ) is
    begin
        if this.Get_Selected_Tool.Get_Type /= Panner_Tool then
            this.Select_Tool( Panner_Tool );
        elsif this.tools(Surveyor_Tool).Can_Clear then
            -- panner already selected, clear the tile selection
            this.tools(Surveyor_Tool).Do_Clear;
        end if;
    end Shortcut_Panner;

    ----------------------------------------------------------------------------

    procedure Shortcut_Pointer  ( this : not null access Tinker_View'Class ) is begin this.Select_Tool( Pointer_Tool   ); end Shortcut_Pointer;
    procedure Shortcut_Surveyor ( this : not null access Tinker_View'Class ) is begin this.Select_Tool( Surveyor_Tool  ); end Shortcut_Surveyor;
    procedure Shortcut_Painter  ( this : not null access Tinker_View'Class ) is begin this.Select_Tool( Painter_Tool   ); end Shortcut_Painter;
    procedure Shortcut_Pattern  ( this : not null access Tinker_View'Class ) is begin this.Select_Tool( Pattern_Tool   ); end Shortcut_Pattern;
    procedure Shortcut_Stamper  ( this : not null access Tinker_View'Class ) is begin this.Select_Tool( Stamp_Tool     ); end Shortcut_Stamper;
    procedure Shortcut_Eraser   ( this : not null access Tinker_View'Class ) is begin this.Select_Tool( Eraser_Tool    ); end Shortcut_Eraser;
    procedure Shortcut_Picker   ( this : not null access Tinker_View'Class ) is begin this.Select_Tool( Picker_Tool    ); end Shortcut_Picker;
    procedure Shortcut_Connector( this : not null access Tinker_View'Class ) is begin this.Select_Tool( Connector_Tool ); end Shortcut_Connector;
    procedure Shortcut_Spawner  ( this : not null access Tinker_View'Class ) is begin this.Select_Tool( Spawner_Tool   ); end Shortcut_Spawner;

    ----------------------------------------------------------------------------

    procedure Shortcut_Toggle_Tray_Bottom( this : not null access Tinker_View'Class ) is
    begin
        this.bottomButton.Toggle_State;
    end Shortcut_Toggle_Tray_Bottom;

    ----------------------------------------------------------------------------

    procedure Shortcut_Toggle_Tray_Right( this : not null access Tinker_View'Class ) is
    begin
        this.rightButton.Toggle_State;
    end Shortcut_Toggle_Tray_Right;

    ----------------------------------------------------------------------------

    procedure Shortcut_Tool_Delete( this : not null access Tinker_View'Class ) is
    begin
        -- if the tool acts on entities, delete selected entities.
        -- if the tool acts on the map, delete selected tiles.
        if this.Is_World_Loaded and then not this.Get_Selected_Tool.Is_Busy then
            if this.Get_Selected_Tool.Acts_On( Entity_Subject ) and then this.tools(Pointer_Tool).Can_Delete then
                this.tools(Pointer_Tool).Do_Delete;

            elsif this.Get_Selected_Tool.Acts_On( Map_Subject ) and then this.tools(Surveyor_Tool).Can_Delete then
                this.tools(Surveyor_Tool).Do_Delete;

            end if;
        end if;
    end Shortcut_Tool_Delete;

    ----------------------------------------------------------------------------

    procedure Shortcut_Tool_Copy( this : not null access Tinker_View'Class ) is
    begin
        -- if the tool acts on entities, copy selected entities.
        -- if the tool acts on the map, copy selected tiles.
        if this.Is_World_Loaded and then not this.Get_Selected_Tool.Is_Busy then
            if this.Get_Selected_Tool.Acts_On( Entity_Subject ) and then this.tools(Pointer_Tool).Can_Copy then
                this.tools(Pointer_Tool).Do_Copy;

            elsif this.Get_Selected_Tool.Acts_On( Map_Subject ) and then this.tools(Surveyor_Tool).Can_Copy then
                this.tools(Surveyor_Tool).Do_Copy;

            end if;
        end if;
    end Shortcut_Tool_Copy;

    ----------------------------------------------------------------------------

    procedure Shortcut_Tool_Cut( this : not null access Tinker_View'Class ) is
    begin
        -- if the tool acts on entities, cut selected entities.
        -- if the tool acts on the map, cut selected tiles.
        if this.Is_World_Loaded and then not this.Get_Selected_Tool.Is_Busy then
            if this.Get_Selected_Tool.Acts_On( Entity_Subject ) and then this.tools(Pointer_Tool).Can_Copy then
                this.tools(Pointer_Tool).Do_Copy;
                this.tools(Pointer_Tool).Do_Delete;

            elsif this.Get_Selected_Tool.Acts_On( Map_Subject ) and then this.tools(Surveyor_Tool).Can_Copy then
                this.tools(Surveyor_Tool).Do_Copy;
                this.tools(Surveyor_Tool).Do_Delete;

            end if;
        end if;
    end Shortcut_Tool_Cut;

    ----------------------------------------------------------------------------

    procedure Shortcut_Tool_Paste( this : not null access Tinker_View'Class ) is
    begin
        -- if the clipboard contains entities, paste entities.
        -- if the clipboard contains tiles, past tiles.
        if this.Is_World_Loaded and then not this.Get_Selected_Tool.Is_Busy then
            if this.tools(Pointer_Tool).Can_Paste then
                this.tools(Pointer_Tool).Do_Paste;

            elsif this.tools(Surveyor_Tool).Can_Paste then
                this.tools(Surveyor_Tool).Do_Paste;

            end if;
        end if;
    end Shortcut_Tool_Paste;

    ----------------------------------------------------------------------------

begin

    Preferences.Set_Default( "gridsize",       16 );
    Preferences.Set_Default( "gridsnap",       True );
    Preferences.Set_Default( "drawOutlines",   False );
    Preferences.Set_Default( "drawGrid",       "" );
    Preferences.Set_Default( "drawClipping",   False );

    Preferences.Set_Default( "bottomOpen", True );
    Preferences.Set_Default( "bottomSize", BOTTOM_MIN_HEIGHT );

    Preferences.Set_Default( "rightOpen", True );
    Preferences.Set_Default( "rightSize", RIGHT_MIN_WIDTH );

    Preferences.Set_Default( "leftSize",  LEFT_MIN_WIDTH );

    -- unused by the scene widget in tinker, but defined to avoid warnings
    Preferences.Set_Default( "development", "camera.speed", 0.0 );
    Preferences.Set_Default( "development", "camera.slowdownDistance", 1.0 );
    Preferences.Set_Default( "development", "camera.slowdownPercent", 1.0 );

end Game_Views.Tinker;
