--
-- Copyright (c) 2017-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Maps;                              use Maps;

package Tools.Stampers is

    type Stamper is new Tool with private;
    type A_Stamper is access all Stamper'Class;

    function Create_Stamper( stamp : not null A_Map ) return A_Tool;
    pragma Postcondition( Create_Stamper'Result /= null );

    overriding
    function Acts_On( this : access Stamper; target : Subject_Type ) return Boolean is (target = Map_Subject);

    procedure Apply_To_Map( this      : access Stamper;
                            func      : Function_Type;
                            modifiers : Modifiers_Array;
                            first     : Boolean;
                            context   : Map_Tool_Context );

    function Create_Options( this : access Stamper;
                             view : A_Game_View ) return A_Widget;

    procedure Draw_Preview_Foreground( this      : access Stamper;
                                       modifiers : Modifiers_Array;
                                       context   : Tool_Preview_Context );

    procedure Draw_Preview_Layer( this      : access Stamper;
                                  modifiers : Modifiers_Array;
                                  context   : Tool_Preview_Context );

    overriding
    function Get_Cursor( this : access Stamper ) return String is ("tinker:cursor-stamper");

private

    MAX_SIZE : constant := 63;

    type Stamper is new Tool with
        record
            stamp : A_Map := null;
        end record;

    procedure Construct( this : access Stamper; stamp : A_Map );

    procedure Delete( this : in out Stamper );

end Tools.Stampers;
