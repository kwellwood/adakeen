--
-- Copyright (c) 2015-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Game_Views.Tinker;                 use Game_Views.Tinker;

package body Tools.Panners is

    function Create_Panner return A_Tool is
        this : constant A_Tool := new Panner;
    begin
        this.Construct( Panner_Tool );
        return this;
    end Create_Panner;

    ----------------------------------------------------------------------------

    overriding
    procedure Apply_To_Map( this      : access Panner;
                            func      : Function_Type;
                            modifiers : Modifiers_Array;
                            first     : Boolean;
                            context   : Map_Tool_Context ) is
    begin
        if not first then
            return;
        end if;

        if func = Secondary then
            -- rotate to the Pointer tool (Panner -> Pointer -> Surveyor -> repeat)
            this.view.Select_Tool( Pointer_Tool );
        end if;
    end Apply_To_Map;

end Tools.Panners;
