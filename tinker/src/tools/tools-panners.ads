--
-- Copyright (c) 2015-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Tools.Panners is

    -- The Panner tool allows the user to pan the map by grabbing it and dragging
    -- it around without interacting with it in any way.
    --
    -- Operates on the map only. Does not interact with entities.
    --
    -- Primary function:
    --   Grabs and drags the map. This same functionality is also performend by
    --   the right mouse button, regardless of selected tool. The drag function
    --   is specially implemented by the Tinker_Scene object instead of this
    --   tool.
    --
    -- Secondary function:
    --   Switches to the Pointer tool as part of the cycle:
    --   Panner -> Pointer -> Surveyor -> (repeat)
    --
    type Panner is new Tool with private;

    function Create_Panner return A_Tool;
    pragma Postcondition( Create_Panner'Result /= null );

    overriding
    function Acts_On( this : access Panner; target : Subject_Type ) return Boolean is (target = Map_Subject);

    procedure Apply_To_Map( this      : access Panner;
                            func      : Function_Type;
                            modifiers : Modifiers_Array;
                            first     : Boolean;
                            context   : Map_Tool_Context );

    overriding
    function Get_Cursor( this : access Panner ) return String is ("tinker:cursor-pannable");

private

    type Panner is new Tool with null record;

end Tools.Panners;
