--
-- Copyright (c) 2017-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Commands;                          use Commands;
--with Debugging;                         use Debugging;
with Drawing.Primitives;                use Drawing.Primitives;
with Game_Views.Tinker;                 use Game_Views.Tinker;
with Preferences;                       use Preferences;
with Signals;                           use Signals;
with Support;                           use Support;
with Values;                            use Values;
with Values.Construction;               use Values.Construction;
with Values.Lists;                      use Values.Lists;
with Values.Maps;                       use Values.Maps;
with Values.Strings;                    use Values.Strings;
with Widgets.Buttons.Checkboxes;        use Widgets.Buttons.Checkboxes;
with Widgets.Row_Layouts;               use Widgets.Row_Layouts;

package body Tools.Surveyors is

    package Connections is new Signals.Connections(Surveyor);
    use Connections;

    ----------------------------------------------------------------------------

    procedure Clicked_All_Layers_Disable( this : not null access Surveyor'Class );

    procedure Clicked_All_Layers_Enable( this : not null access Surveyor'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Surveyor return A_Tool is
        this : constant A_Tool := new Surveyor;
    begin
        this.Construct( Surveyor_Tool );
        this.layerSpecific := not Get_Pref( "tools", "surveyor.allLayers", not this.layerSpecific );
        return this;
    end Create_Surveyor;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Surveyor ) is
    begin
        Delete( this.content );
        Delete( this.previous );
        Tool(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    overriding
    procedure Apply_To_Map( this      : access Surveyor;
                            func      : Function_Type;
                            modifiers : Modifiers_Array;
                            first     : Boolean;
                            context   : Map_Tool_Context ) is
        tileWidth : constant Float := Float(this.scene.Get_Tile_Width);

        function In_Selected_Area( x, y : Float ) return Boolean is (Contains( this.scene.Get_Tile_Area, x, y ));

        function Round_To_Tile( val : Float ) return Float is (Float'Rounding( val / tileWidth ) * tileWidth);

        dragSlack : constant Float := From_Target_Pixels( 2.0 );     -- slack in world units (+/- 2 screen pixels)
        mapWidth  : constant Float := Float(this.scene.Get_Map.Get_Width) * tileWidth;
        mapHeight : constant Float := Float(this.scene.Get_Map.Get_Height) * tileWidth;
        tx        : constant Float := Round_To_Tile( context.x );
        ty        : constant Float := Round_To_Tile( context.y );
        oldArea   : Rectangle;
        newArea   : Rectangle;
        bgLayer   : Integer;
        fgLayer   : Integer;
        cmd       : A_Set_Tiles_Command;
    begin
        if first then
            -- choose what to do with the tool
            if func = Primary then
                if In_Selected_Area( context.x, context.y ) then
                    -- start dragging the selection
                    this.dragX := context.x;
                    this.dragY := context.y;
                    this.dragging := True;
                    this.dragContent := not modifiers(CTRL);  -- CTRL moves selection, not content

                    -- always update the .previous and .content caches from the
                    -- current state of the map before dragging. events like switching
                    -- tools may have deleted the cache and events like changing the
                    -- active layer or visible layers may have invalidated the cache.
                    this.Update_Cache( this.scene.Get_Map, this.scene.Get_Tile_Area, tileWidth );

                else
                    -- start a new selection (within the map)
                    if 0.0 <= tx and 0.0 <= ty and tx <= mapWidth and ty <= mapHeight then
                        this.oldArea := this.scene.Get_Tile_Area;
                        this.scene.Tile_Area_Begin( tx, ty );
                        this.creating := True;
                        Delete( this.content );
                        Delete( this.previous );
                    end if;
                end if;

            elsif func = Secondary then
                if this.scene.Is_Tile_Area_Active then
                    -- clear the selection
                    this.view.Commands.Execute( Create_Command_Select_Tiles( this.scene.Get_Tile_Area, (others => 0.0) ) );
                    this.view.Commands.Prevent_Combine;
                    Delete( this.content );
                    Delete( this.previous );
                else
                    -- rotate to the Panner tool (Panner -> Pointer -> Surveyor -> repeat)
                    this.view.Select_Tool( Panner_Tool );
                end if;
            end if;

        elsif this.dragging then
            -- continue dragging the selection
            oldArea := this.scene.Get_Tile_Area;
            newArea := oldArea;
            if context.x /= this.dragX then
                -- the 'dragSlack' term adds mouse movement slack to dragging,
                -- so that the mouse must drag a couple pixels further across
                -- the half tile rounding boundary before the drag actually
                -- occurs. this prevents the selection from jittering sometimes
                -- when dragging.
                newArea.x := Constrain( newArea.x + Round_To_Tile( (context.x - this.dragX) - Float'Copy_Sign( dragSlack, (context.x - this.dragX) ) ), 0.0, mapWidth - newArea.width );
            end if;
            if context.y /= this.dragY then
                newArea.y := Constrain( newArea.y + Round_To_Tile( (context.y - this.dragY) - Float'Copy_Sign( dragSlack, (context.y - this.dragY) ) ), 0.0, mapHeight - newArea.height );
            end if;

            if newArea /= oldArea then
                this.view.Commands.Execute( Create_Command_Select_Tiles( oldArea, newArea ) );
                this.dragX := this.dragX + (newArea.x - oldArea.x);
                this.dragY := this.dragY + (newArea.y - oldArea.y);

                if this.dragContent then
                    -- scale from world units to tiles
                    oldArea.x      := oldArea.x / tileWidth;
                    oldArea.y      := oldArea.y / tileWidth;
                    oldArea.width  := oldArea.width / tileWidth;
                    oldArea.height := oldArea.height / tileWidth;

                    -- scale from world units to tiles
                    newArea.x      := newArea.x / tileWidth;
                    newArea.y      := newArea.y / tileWidth;
                    newArea.width  := newArea.width / tileWidth;
                    newArea.height := newArea.height / tileWidth;

                    cmd := Create_Command_Set_Tiles;

                    fgLayer := (if this.layerSpecific then this.scene.Get_Active_Layer else this.previous.Get_Foreground_Layer);
                    bgLayer := (if this.layerSpecific then this.scene.Get_Active_Layer else this.previous.Get_Background_Layer);

                    -- replace the content where the selection was previously
                    for layer in fgLayer..bgLayer loop
                        if this.scene.Is_Layer_Visible( layer ) then
                            for y in 0..(this.content.Get_Height-1) loop
                                for x in 0..(this.content.Get_Width-1) loop
                                    if this.content.Get_Tile_Id( layer, x, y ) /= 0 then
                                        cmd.Add( layer,
                                                 Integer(oldArea.x) + x,
                                                 Integer(oldArea.y) + y,
                                                 this.content.Get_Tile_Id( layer, x, y ),
                                                 this.previous.Get_Tile_Id( layer,
                                                                            Integer(oldArea.x) + x,
                                                                            Integer(oldArea.y) + y ) );
                                    end if;
                                end loop;
                            end loop;
                        end if;
                    end loop;

                    -- merge the content into the new location
                    -- zeros in the content do not overwrite existing tiles
                    for layer in fgLayer..bgLayer loop
                        if this.scene.Is_Layer_Visible( layer ) then
                            for y in 0..(this.content.Get_Height-1) loop
                                for x in 0..(this.content.Get_Width-1) loop
                                    if this.content.Get_Tile_Id( layer, x, y ) /= 0 then
                                        cmd.Add( layer,
                                                 Integer(newArea.x) + x,
                                                 Integer(newArea.y) + y,
                                                 this.previous.Get_Tile_Id( layer,
                                                                            Integer(newArea.x) + x,
                                                                            Integer(newArea.y) + y ),
                                                 this.content.Get_Tile_Id( layer, x, y ) );
                                    end if;
                                end loop;
                            end loop;
                        end if;
                    end loop;

                    this.view.Commands.Execute( cmd );
                end if;
            end if;

        elsif this.creating then
            -- continue creating the selection
            this.scene.Tile_Area_Update( Constrain( tx, 0.0, mapWidth ), Constrain( ty, 0.0, mapHeight ) );

        end if;
    end Apply_To_Map;

    ----------------------------------------------------------------------------

    overriding
    function Can_Clear( this : access Surveyor ) return Boolean is
        area : constant Rectangle := this.scene.Get_Tile_Area;
    begin
        return area.width * area.height > 0.0;
    end Can_Clear;

    ----------------------------------------------------------------------------

    overriding
    function Can_Copy( this : access Surveyor ) return Boolean is (this.Can_Delete);

    ----------------------------------------------------------------------------

    overriding
    function Can_Delete( this : access Surveyor ) return Boolean is
        tileWidth : constant Float := Float(this.scene.Get_Tile_Width);
        area      : constant Rectangle := this.scene.Get_Tile_Area;
        fgLayer   : constant Integer := (if this.layerSpecific then this.scene.Get_Active_Layer else this.scene.Get_Foreground_Layer);
        bgLayer   : constant Integer := (if this.layerSpecific then this.scene.Get_Active_Layer else this.scene.Get_Background_Layer);
    begin
        for layer in fgLayer..bgLayer loop
            if this.scene.Is_Layer_Visible( layer ) then
                for y in 0..(Integer(area.height / tileWidth)-1) loop
                    for x in 0..(Integer(area.width / tileWidth)-1) loop
                        if this.scene.Get_Map.Get_Tile_Id( layer,
                                                           Integer(area.x / tileWidth) + x,
                                                           Integer(area.y / tileWidth) + y ) /= 0
                        then
                            -- found a non-zero tile to delete
                            return True;
                        end if;
                    end loop;
                end loop;
            end if;
        end loop;

        return False;
    end Can_Delete;

    ----------------------------------------------------------------------------

    overriding
    function Can_Paste( this : access Surveyor ) return Boolean is
        clipboard : constant List_Value := this.view.Get_Clipboard( "tiles" ).Lst;
    begin
        return clipboard.Valid and then
               clipboard.Length > 1 and then
               Case_Eq( clipboard.Get( 1 ).Map.Get_String( "lib" ), this.scene.Get_Library );
    end Can_Paste;

    ----------------------------------------------------------------------------

    procedure Clicked_All_Layers_Disable( this : not null access Surveyor'Class ) is
        view : constant A_Tinker_View := A_Tinker_View(A_Widget(this.Signaller).Get_View);
    begin
        this.layerSpecific := True;
        Set_Pref( "tools", "surveyor.allLayers", not this.layerSpecific );
        view.Get_Scene.Set_Active_Layer( view.Get_Scene.Get_Active_Layer );
        view.Get_Scene.Request_Focus;
    end Clicked_All_Layers_Disable;

    ----------------------------------------------------------------------------

    procedure Clicked_All_Layers_Enable( this : not null access Surveyor'Class ) is
        view : constant A_Tinker_View := A_Tinker_View(A_Widget(this.Signaller).Get_View);
    begin
        this.layerSpecific := False;
        Set_Pref( "tools", "surveyor.allLayers", not this.layerSpecific );
        view.Get_Scene.Set_Active_Layer( view.Get_Scene.Get_Active_Layer );
        view.Get_Scene.Request_Focus;
    end Clicked_All_Layers_Enable;

    ----------------------------------------------------------------------------

    overriding
    function Create_Options( this : access Surveyor;
                             view : A_Game_View ) return A_Widget is
        row : A_Row_Layout;
    begin
        row := Create_Row_Layout( view );
        row.Set_Style_Property( "background.align", Create( "center" ) );

            this.btnAllLayers := A_Button(Create_Checkbox( view ));
            this.btnAllLayers.Set_Style( "surveyorOptions" );
            this.btnAllLayers.Set_Text( " All Layers" );
            this.btnAllLayers.Set_State( not this.layerSpecific );
            this.btnAllLayers.Pressed.Connect( Slot( this, Clicked_All_Layers_Enable'Access ) );
            this.btnAllLayers.Released.Connect( Slot( this, Clicked_All_Layers_Disable'Access ) );
            row.Append( this.btnAllLayers );

        row.Set_Preferred_Width( A_Widget(row).Get_Min_Width );
        return A_Widget(row);
    end Create_Options;

    ----------------------------------------------------------------------------

    procedure Do_Clear( this : access Surveyor ) is
        area : constant Rectangle := this.scene.Get_Tile_Area;
    begin
        if area.width * area.height > 0.0 then
            this.view.Commands.Execute( Create_Command_Select_Tiles( area, (others => 0.0) ) );
        end if;
    end Do_Clear;

    ----------------------------------------------------------------------------

    overriding
    procedure Do_Copy( this : access Surveyor ) is
        tileWidth : constant Float := Float(this.scene.Get_Tile_Width);
        area      : constant Rectangle := this.scene.Get_Tile_Area;
        fgLayer   : constant Integer := (if this.layerSpecific then this.scene.Get_Active_Layer else this.scene.Get_Foreground_Layer);
        bgLayer   : constant Integer := (if this.layerSpecific then this.scene.Get_Active_Layer else this.scene.Get_Background_Layer);
        tiles     : List_Value;
        tileId    : Natural;
    begin
        if area.width * area.height <= 0.0 then
            return;
        end if;

        tiles := Create_List.Lst;
        tiles.Append( Create_Map( (Pair( "x", Create( area.x / tileWidth ) ),
                                   Pair( "y", Create( area.y / tileWidth ) ),
                                   Pair( "w", Create( area.width / tileWidth ) ),
                                   Pair( "h", Create( area.height / tileWidth ) ),
                                   Pair( "layerSpecific", Create( this.layerSpecific ) ),
                                   Pair( "lib", Create( this.scene.Get_Library ) )
                                  ) ) );

        for layer in fgLayer..bgLayer loop
            if this.scene.Is_Layer_Visible( layer ) then
                for y in 0..(Integer(area.height / tileWidth)-1) loop
                    for x in 0..(Integer(area.width / tileWidth)-1) loop
                        tileId := this.scene.Get_Map.Get_Tile_Id( layer,
                                                                  Integer(area.x / tileWidth) + x,
                                                                  Integer(area.y / tileWidth) + y );
                        if tileId /= 0 then
                            tiles.Append( Create( layer ) );
                            tiles.Append( Create( x ) );
                            tiles.Append( Create( y ) );
                            tiles.Append( Create( tileId ) );
                        end if;
                    end loop;
                end loop;
            end if;
        end loop;

        if tiles.Length > 1 then
            this.view.Set_Clipboard( "tiles", tiles );
        end if;
    end Do_Copy;

    ----------------------------------------------------------------------------

    overriding
    procedure Do_Delete( this : access Surveyor ) is
        tileWidth : constant Float := Float(this.scene.Get_Tile_Width);
        area      : constant Rectangle := this.scene.Get_Tile_Area;
        fgLayer   : constant Integer := (if this.layerSpecific then this.scene.Get_Active_Layer else this.scene.Get_Foreground_Layer);
        bgLayer   : constant Integer := (if this.layerSpecific then this.scene.Get_Active_Layer else this.scene.Get_Background_Layer);
        cmd       : A_Set_Tiles_Command := Create_Command_Set_Tiles;
    begin
        for layer in fgLayer..bgLayer loop
            if this.scene.Is_Layer_Visible( layer ) then
                for y in 0..(Integer(area.height / tileWidth)-1) loop
                    for x in 0..(Integer(area.width / tileWidth)-1) loop
                        cmd.Add( layer,
                                 Integer(area.x / tileWidth) + x,
                                 Integer(area.y / tileWidth) + y,
                                 this.scene.Get_Map.Get_Tile_Id( layer,
                                                                 Integer(area.x / tileWidth) + x,
                                                                 Integer(area.y / tileWidth) + y ),
                                 0 );
                    end loop;
                end loop;
            end if;
        end loop;

        if cmd.Tile_Count = 0 then
            Delete( A_Command(cmd) );
            return;
        end if;

        -- simultaneously clear the selection
        cmd.Add( Create_Command_Select_Tiles( this.scene.Get_Tile_Area, Rectangle'(0.0, 0.0, 0.0, 0.0) ) );

        this.view.Commands.Execute( cmd );
        this.view.Commands.Prevent_Combine;
    end Do_Delete;

    ----------------------------------------------------------------------------

    overriding
    procedure Do_Paste( this : access Surveyor ) is
        tileWidth : constant Float := Float(this.scene.Get_Tile_Width);
        clipboard : constant List_Value := this.view.Get_Clipboard( "tiles" ).Lst;
        area      : Rectangle;
        content   : A_Map;
        cmd       : A_Set_Tiles_Command;
        index     : Integer;
        layer     : Integer;
        x         : Integer;
        y         : Integer;
        tileId    : Integer;
    begin
        if not clipboard.Valid or else
           clipboard.Length < 2 or else
           not Case_Eq( clipboard.Get( 1 ).Map.Get_String( "lib" ), this.scene.Get_Library )
        then
            return;
        end if;

        this.layerSpecific := clipboard.Get( 1 ).Map.Get_Boolean( "layerSpecific" );
        layer := this.scene.Get_Active_Layer;

        -- in tile coordinates
        area.x      := clipboard.Get( 1 ).Map.Get_Float( "x" );
        area.y      := clipboard.Get( 1 ).Map.Get_Float( "y" );
        area.width  := clipboard.Get( 1 ).Map.Get_Float( "w" );
        area.height := clipboard.Get( 1 ).Map.Get_Float( "h" );

        if area.x * tileWidth < this.scene.Get_Viewport.x then
            -- shift the content to avoid going off the left of the viewport
            area.x := Float'Ceiling( this.scene.Get_Viewport.x / tileWidth );
            if Integer(area.x + area.width) > this.scene.Get_Width_Tiles then
                -- constrain to the right edge of the map
                area.x := Float(this.scene.Get_Width_Tiles) - area.width;
            end if;
        elsif (this.scene.Get_Viewport.x + this.scene.Get_Viewport.width) < (area.x + area.width) * tileWidth then
            -- shift the content to avoid going off the right of the viewport
            area.x := Float'Floor( (this.scene.Get_Viewport.x + this.scene.Get_Viewport.width) / tileWidth ) - area.width;
            if area.x < 0.0 then
                -- constrain to the left edge of the map
                area.x := 0.0;
            end if;
        else
            -- the area to paste is completely within the width of the viewport
            null;
        end if;
        if area.x < 0.0 or area.x + area.width > Float(this.scene.Get_Width_Tiles) then
            -- the paste width is wider than the map width
            area.x := 0.0;
            area.width := Float(this.scene.Get_Width_Tiles);
        end if;

        if area.y * tileWidth < this.scene.Get_Viewport.y then
            -- shift the content to avoid going off the top of the viewport
            area.y := Float'Ceiling( this.scene.Get_Viewport.y / tileWidth );
            if Integer(area.y + area.height) > this.scene.Get_Width_Tiles then
                -- constrain to the bottom edge of the map
                area.y := Float(this.scene.Get_Width_Tiles) - area.height;
            end if;
        elsif (this.scene.Get_Viewport.y + this.scene.Get_Viewport.height) < (area.y + area.height) * tileWidth then
            -- shift the content to avoid going off the bottom of the viewport
            area.y := Float'Floor( (this.scene.Get_Viewport.y + this.scene.Get_Viewport.height) / tileWidth ) - area.height;
            if area.y < 0.0 then
                -- constrain to the top edge of the map
                area.y := 0.0;
            end if;
        else
            -- the area to paste is completely within the height of the viewport
            null;
        end if;
        if area.y < 0.0 or area.y + area.height > Float(this.scene.Get_Height_Tiles) then
            -- the paste height is wider than the map height
            area.y := 0.0;
            area.height := Float(this.scene.Get_Height_Tiles);
        end if;

        -- create a cache to hold the pasted content
        content := this.scene.Get_Map.Copy_Area( Integer(area.x),
                                                 Integer(area.y),
                                                 Integer(area.width),
                                                 Integer(area.height) );
        for layer in content.Get_Foreground_Layer..content.Get_Background_Layer loop
            for y in 0..Integer(area.height)-1 loop
                for x in 0..Integer(area.width)-1 loop
                    content.Set_Tile( layer, x, y, 0 );
                end loop;
            end loop;
        end loop;

        cmd := Create_Command_Set_Tiles;

        index := 2;
        while index < clipboard.Length loop
            if not this.layerSpecific then
                -- if all layers are being set, use the layer from the clipboard.
                -- otherwise, use the active layer.
                layer := clipboard.Get_Int( index + 0, layer );
            end if;
            x := clipboard.Get_Int( index + 1, -1 );
            y := clipboard.Get_Int( index + 2, -1 );
            tileId := clipboard.Get_Int( index + 3,  0 );
            index := index + 4;

            if this.scene.Is_Layer_Visible( layer ) then
                cmd.Add( layer, Integer(area.x) + x, Integer(area.y) + y,
                         this.scene.Get_Map.Get_Tile_Id( layer, Integer(area.x) + x, Integer(area.y) + y ),
                         tileId );
            end if;
        end loop;

        -- switch to this tool if it's not already in use
        this.view.Select_Tool( Surveyor_Tool );
        this.btnAllLayers.Set_State( not this.layerSpecific );   -- and the same mode as the paste

        -- the map content within the selection is now stale. this is probably
        -- not necessary because the cache will be updated before it is used
        -- again anyway.
        Delete( this.content );
        Delete( this.previous );

        -- simultaneously select the newly pasted area
        cmd.Add( Create_Command_Select_Tiles( this.scene.Get_Tile_Area,
                                              Rectangle'(area.x * tileWidth,
                                                         area.y * tileWidth,
                                                         area.width * tileWidth,
                                                         area.height * tileWidth) ) );

        this.view.Commands.Execute( cmd );
        this.view.Commands.Prevent_Combine;
    end Do_Paste;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Selected( this : access Surveyor ) is
    begin
        -- clear the content so it will be captured again
        -- both .content and .previous are out of date, due to potential changes by other tools
        Delete( this.content );
        Delete( this.previous );
    end On_Selected;

    ----------------------------------------------------------------------------

    overriding
    procedure Post_Apply_Map( this      : access Surveyor;
                              func      : Function_Type;
                              modifiers : Modifiers_Array;
                              context   : Map_Tool_Context ) is
        pragma Unreferenced( modifiers, context );
    begin
        if func /= Primary then
            return;
        end if;

        if this.dragging then
            -- finish dragging
            this.dragging := False;
            this.view.Commands.Prevent_Combine;

        elsif this.creating then
            -- finish creating selection
            this.view.Commands.Execute( Create_Command_Select_Tiles( this.oldArea, this.scene.Get_Tile_Area ) );
            this.view.Commands.Prevent_Combine;
            this.creating := False;

        end if;
    end Post_Apply_Map;

    ----------------------------------------------------------------------------

    procedure Update_Cache( this      : not null access Surveyor'Class;
                            map       : not null A_Map;
                            area      : Rectangle;
                            tileWidth : Float ) is
        tileArea : Rectangle;
    begin
        if area.width * area.height <= 0.0 then
            return;
        end if;

        Delete( this.content );
        Delete( this.previous );

        tileArea.x      := area.x / tileWidth;
        tileArea.y      := area.y / tileWidth;
        tileArea.width  := area.width / tileWidth;
        tileArea.height := area.height / tileWidth;

        -- copy the selected tiles
        -- oldArea.x, .y, .width, .height are all multiples of tileWidth
        this.content := map.Copy_Area( Integer(tileArea.x),
                                       Integer(tileArea.y),
                                       Integer(tileArea.width),
                                       Integer(tileArea.height) );

        -- copy '.previous' and clear the area being moved
        this.previous := Copy( map );
        for layer in this.previous.Get_Foreground_Layer..this.previous.Get_Background_Layer loop
            -- only clear tile data; leave scenery alone
            if this.previous.Get_Layer_Type( layer ) = Layer_Tiles then
                for y in Integer(tileArea.y)..Integer(tileArea.y+tileArea.height-1.0) loop
                    for x in Integer(tileArea.x)..Integer(tileArea.x+tileArea.width-1.0) loop
                        this.previous.Set_Tile( layer, x, y, 0 );
                    end loop;
                end loop;
            end if;
        end loop;
    end Update_Cache;

end Tools.Surveyors;

