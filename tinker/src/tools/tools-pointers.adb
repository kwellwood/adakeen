--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Commands;                          use Commands;
--with Debugging;                         use Debugging;
with Entities.Factory;
with Entities.Templates;                use Entities.Templates;
with Game_Views.Tinker;                 use Game_Views.Tinker;
with Preferences;                       use Preferences;
with Scene_Entities;                    use Scene_Entities;
with Support;                           use Support;
with Values.Lists;                      use Values.Lists;
with Values.Maps;                       use Values.Maps;
with Vector_Math;                       use Vector_Math;
with Widgets.Scenes;                    use Widgets.Scenes;
with Widgets.Scenes.Tinker;             use Widgets.Scenes.Tinker;

package body Tools.Pointers is

    -- Contines an on-going move action for all selected entities, moving them
    -- by 'dx', 'dy' in world coordinates.
    procedure Continue_Move( this : not null access Pointer'Class; dx, dy : Float );

    -- Continues an on-going resize action for entity 'entity', where the tool
    -- is being applied at 'mouseX', 'mouseY' in widget coordinates of the entity.
    procedure Continue_Resize( this           : not null access Pointer'Class;
                               entity         : not null A_Entity;
                               mouseX, mouseY : Float );

    -- Finishes moving 'entity' and all other selected entities after a drag to
    -- move action has completed.
    procedure Finish_Move( this   : not null access Pointer'Class;
                           entity : not null A_Entity );

    -- Finishes resizing 'entity' after a drag to resize action has completed.
    procedure Finish_Resize( this : not null access Pointer'Class;
                             entity : not null A_Entity );

    -- If the widget for entity 'entity' supports resizing, then the resize
    -- direction of the handle at 'wx,wy' on the widget representing the entity
    -- will be returned. If 'wx,wy' in widget coordinates is not on a handle, or
    -- the entity does not support resizing in that direction, then Dir_None
    -- will be returned.
    function Get_Resize_Direction( this   : not null access Pointer'Class;
                                   entity : not null A_Entity;
                                   wx, wy : Float ) return Direction_Type;

    -- Returns the location 'newX', 'newY', of entity 'entity' after it is
    -- dragged 'dx', 'dy' distance in world units. Grid snapping will be applied
    -- to the final location, as appropriate, based on the entity's template's
    -- tool parameters.
    procedure Location_After_Drag( this       : not null access Pointer'Class;
                                   entity     : not null A_Entity;
                                   dx, dy     : Float;
                                   newX, newY : out Float );

    -- Freezes/unfreezes updating of entities in the selection. Be sure to
    -- unfreeze once for each freeze.
    procedure Selection_Freeze_Updates( this     : not null access Pointer'Class;
                                        entities : Entity_Sets.Set;
                                        freeze   : Boolean );

    -- Begins an on-going move action for entity 'entity' and all other selected
    -- entities. The action will be continued by calling Continue_Move() during
    -- subsequent tool use, and completed by calling Finish_Move() after the
    -- tool is done being applied (press, drag..drag..drag, release).
    procedure Start_Move( this   : not null access Pointer'Class;
                          entity : not null A_Entity );

    -- Begins an on-going resize action for entity 'entity'. The edge of the
    -- entity that is being dragged will be determined by .applyX, .applyY.
    procedure Start_Resize( this   : not null access Pointer'Class;
                            entity : not null A_Entity );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Pointer return A_Tool is
        this : constant A_Tool := new Pointer;
    begin
        this.Construct( Pointer_Tool );
        return this;
    end Create_Pointer;

    ----------------------------------------------------------------------------

    overriding
    procedure Apply_To_Entity( this      : access Pointer;
                               func      : Function_Type;
                               modifiers : Modifiers_Array;
                               first     : Boolean;
                               context   : Entity_Tool_Context ) is
        template   : A_Template;
        toolParams : Map_Value;
    begin
        if context.entity = null then
            return;
        end if;

        if func = Secondary then
            if first then
                if this.scene.Get_Entity_Selection_Count > 0 then
                    -- clear the currently selected entity set
                    this.scene.Entity_Selection_Clear;
                else
                    -- rotate to the Surveyor tool (Panner -> Pointer -> Surveyor -> repeat)
                    this.view.Select_Tool( Surveyor_Tool );
                end if;
            end if;
            return;
        end if;
        pragma Assert( func = Primary );

        -- begin dragging or resizing
        if first then
            if modifiers(CTRL) then
                -- toggle selection of the entity
                this.scene.Entity_Selection_Toggle( context.entity );
            elsif modifiers(SHIFT) then
                -- include entity in selection
                this.scene.Entity_Selection_Add( context.entity );
            else
                -- reset selection to be just this entity if it wasn't already
                -- included in the selection
                if not context.entity.Is_Selected then
                    this.scene.Entity_Selection_Set( context.entity );
                end if;
            end if;

            if context.entity.Is_Selected then
                -- cache the position inside the entity's widget where the tool
                -- was when the user started to apply it.
                this.applyX := context.x;
                this.applyY := context.y;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

                -- based on the selected entity's template, cache its tool
                -- parameters that determine how tools should interact with this
                -- entity.
                template := Entities.Factory.Global.Get_Template( context.entity.Get_Template );
                if template /= null then
                    toolParams := template.Get_Tool_Parameters;
                else
                    toolParams := Create_Map.Map;
                end if;

                this.minEWidth := toolParams.Get_Float( "minWidth", default => 0.0 );
                this.minEHeight := toolParams.Get_Float( "minHeight", default => 0.0 );

                this.resizableWidth  := toolParams.Get_Boolean( "resizableWidth", toolParams.Get_Boolean( "resizable", False ) );
                this.resizableHeight := toolParams.Get_Boolean( "resizableHeight", toolParams.Get_Boolean( "resizable", False ) );

                -- if neither "snapCenter" nor "snapCenterX" is defined, default it to
                -- "not resizableWidth". this means that entities will default to snapping
                -- to the center of grid cells, except for entities with a resizable width.
                -- if it's resizable, it's most likely that it should be snapped to grid
                -- edges (if snapped at all) because resizing snaps in increments of the
                -- grid size.
                this.snapCenterX := toolParams.Get_Boolean( "snapCenterX", toolParams.Get_Boolean( "snapCenter", not this.resizableWidth ) );
                this.snapCenterY := toolParams.Get_Boolean( "snapCenterY", toolParams.Get_Boolean( "snapCenter", not this.resizableHeight ) );
                this.snapEnabled := toolParams.Get_Boolean( "snap", False );

                -- the placement offset allows the entity's position to be offset
                -- after grid-snapping (effectively an offset from the grid).
                this.placeOffsetX := toolParams.Get_Float( "placementOffsetX", 0.0 );
                this.placeOffsetY := toolParams.Get_Float( "placementOffsetY", 0.0 );

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

                if this.Get_Resize_Direction( context.entity, this.applyX, this.applyY ) /= Dir_None then
                    -- the tool is being applied to a resizing handle area.
                    -- begin resizing the entity by dragging the selected edge.
                    this.dragAction := Drag_Resize;
                    this.Start_Resize( context.entity );
                else
                    -- the tool is being applied inside the entity. begin moving
                    -- the entity by dragging it around with the tool.
                    this.dragAction := Drag_Move;
                    this.Start_Move( context.entity );
                end if;
            end if;

        -- continue dragging or resizing
        elsif context.entity.Is_Selected then
            case this.dragAction is
                when Drag_Move   => this.Continue_Move( context.x - this.applyX, context.y - this.applyY );
                when Drag_Resize => this.Continue_Resize( context.entity, context.x, context.y );
            end case;
        end if;
    end Apply_To_Entity;

    ----------------------------------------------------------------------------

    overriding
    procedure Post_Apply_Entity( this      : access Pointer;
                                 func      : Function_Type;
                                 modifiers : Modifiers_Array;
                                 context   : Entity_Tool_Context ) is
        pragma Unreferenced( modifiers );
    begin
        if context.entity = null then
            return;
        end if;
        if func /= Primary then
            return;
        end if;

        if context.entity.Is_Selected then
            case this.dragAction is
                when Drag_Move   => this.Finish_Move( context.entity );
                when Drag_Resize => this.Finish_Resize( context.entity );
            end case;

            this.dragAction := Drag_Move;
            this.resizeDir := Dir_None;
        end if;
    end Post_Apply_Entity;

    ----------------------------------------------------------------------------

    overriding
    procedure Apply_To_Map( this      : access Pointer;
                            func      : Function_Type;
                            modifiers : Modifiers_Array;
                            first     : Boolean;
                            context   : Map_Tool_Context ) is
    begin
        -- - - - area selection - - - --
        if func = Primary then

            if first then
                if not modifiers(SHIFT) and not modifiers(CTRL) then
                    -- clear it first unless CTRL or SHIFT is used to modify the
                    -- current selection
                    this.scene.Entity_Selection_Clear;
                end if;

                -- start new selection area
                this.scene.Entity_Area_Begin( context.x, context.y );
            else
                -- update selection area
                this.scene.Entity_Area_Update( context.x, context.y,
                                               (if modifiers(CTRL) then Select_Toggle
                                                elsif modifiers(SHIFT) then Select_Include
                                                else Select_Normal) );
            end if;

        -- - - - clear selection - - - --
        elsif func = Secondary then

            if first then
                if this.scene.Get_Entity_Selection_Count > 0 then
                    -- clear the currently selected entity set
                    this.scene.Entity_Selection_Clear;
                else
                    -- rotate to the Surveyor tool (Panner -> Pointer -> Surveyor -> repeat)
                    this.view.Select_Tool( Surveyor_Tool );
                end if;
            end if;

        end if;
    end Apply_To_Map;

    ----------------------------------------------------------------------------

    overriding
    procedure Post_Apply_Map( this      : access Pointer;
                              func      : Function_Type;
                              modifiers : Modifiers_Array;
                              context   : Map_Tool_Context ) is
        pragma Unreferenced( modifiers, context );
    begin
        if func /= Primary then
            return;
        end if;

        -- no longer selecting an area
        this.scene.Entity_Area_Confirm;
    end Post_Apply_Map;

    ----------------------------------------------------------------------------

    overriding
    function Can_Copy( this : access Pointer ) return Boolean is (this.Can_Delete);

    ----------------------------------------------------------------------------

    overriding
    function Can_Delete( this : access Pointer ) return Boolean is
    begin
        if this.scene.Get_Entity_Selection_Count = 1 then
            -- if only one entity is selected, make sure it isn't the player
            -- because that one can't be deleted.
            return this.scene.Get_Selected_Entities.First_Element.Is_Deletable;
        end if;

        return this.scene.Get_Entity_Selection_Count > 0;
    end Can_Delete;

    ----------------------------------------------------------------------------

    overriding
    function Can_Paste( this : access Pointer ) return Boolean is
        clipboard : constant List_Value := this.view.Get_Clipboard( "entities" ).Lst;
    begin
        return clipboard.Valid and then clipboard.Length > 1;
    end Can_Paste;

    ----------------------------------------------------------------------------

    procedure Continue_Move( this : not null access Pointer'Class; dx, dy : Float ) is
    begin
        for entity of this.scene.Get_Selected_Entities loop
            entity.Get_Widget.Move( dx, dy );
        end loop;
        this.movedByDrag := True;
    end Continue_Move;

    ----------------------------------------------------------------------------

    overriding
    procedure Do_Copy( this : access Pointer ) is
    begin
        -- Copy to clipboard is located in the Tinker_Scene because Tinker_Sprites
        -- need access to it from their pop-up menu.
        this.scene.Copy_Selected_Entities;
    end Do_Copy;

    ----------------------------------------------------------------------------

    overriding
    procedure Do_Delete( this : access Pointer ) is
        cmd : A_Compound_Command := Create_Compound_Command;
    begin
        for entity of this.scene.Get_Selected_Entities loop
            if entity.Is_Deletable then
                cmd.Add( Create_Command_Delete_Entity( entity ) );
            end if;
        end loop;

        if cmd.Is_Empty then
            Delete( A_Command( cmd ) );
            return;
        end if;

        this.view.Commands.Execute( cmd );
    end Do_Delete;

    ----------------------------------------------------------------------------

    overriding
    procedure Do_Paste( this : access Pointer ) is
        clipboard      : constant List_Value := this.view.Get_Clipboard( "entities" ).Lst;
        pasteX, pasteY : Float := 0.0;        -- paste offset from copy location
        ex, ey         : Float;
        ew, eh         : Float;
        boundsX1,
        boundsY1,
        boundsX2,
        boundsY2       : Float;
        eid            : Entity_Id;
        cmd            : A_Compound_Command;
    begin
        if not clipboard.Valid or else clipboard.Length < 2 then
            return;
        end if;

        -- calculate a bounding box around the group of items to be pasted
        boundsX1 := clipboard.Get( 2 ).Map.Get_Float( "x" ) - clipboard.Get( 2 ).Map.Get_Float( "w" ) / 2.0;
        boundsY1 := clipboard.Get( 2 ).Map.Get_Float( "y" ) - clipboard.Get( 2 ).Map.Get_Float( "h" ) / 2.0;
        boundsX2 := clipboard.Get( 2 ).Map.Get_Float( "x" ) + clipboard.Get( 2 ).Map.Get_Float( "w" ) / 2.0;
        boundsY2 := clipboard.Get( 2 ).Map.Get_Float( "y" ) + clipboard.Get( 2 ).Map.Get_Float( "h" ) / 2.0;
        for i in 3..clipboard.Length loop
            ex := clipboard.Get( i ).Map.Get_Float( "x" );
            ey := clipboard.Get( i ).Map.Get_Float( "y" );
            ew := clipboard.Get( i ).Map.Get_Float( "w" );
            eh := clipboard.Get( i ).Map.Get_Float( "h" );
            boundsX1 := Float'Min( ex - ew / 2.0, boundsX1 );
            boundsY1 := Float'Min( ey - eh / 2.0, boundsY1 );
            boundsX2 := Float'Max( ex + ew / 2.0, boundsX2 );
            boundsY2 := Float'Max( ey + eh / 2.0, boundsY2 );
        end loop;

        if boundsX1 < this.scene.Get_Viewport.x then
            -- if the left edge of the left-most copied item is off the left side
            -- of the viewport, shift all the pasted items right so that the
            -- left-most item is on the left side of the viewport.
            pasteX := this.scene.Get_Viewport.x - boundsX1;
        elsif (this.scene.Get_Viewport.x + this.scene.Get_Viewport.width) < boundsX2 then
            -- if the right edge of the right-most copied item is off the right
            -- side of the viewport, shift all the pasted items left so that the
            -- right-most item is is on the right side of the viewport.
            pasteX := (this.scene.Get_Viewport.x + this.scene.Get_Viewport.width) - boundsX2;
        else
            -- the bounding box of the entire copy selection is within the width
            -- of the screen.
            pasteX := 0.0;
        end if;
        if boundsY1 < this.scene.Get_Viewport.y then
            pasteY := this.scene.Get_Viewport.y - boundsY1;
        elsif (this.scene.Get_Viewport.y + this.scene.Get_Viewport.height) < boundsY2 then
            pasteY := (this.scene.Get_Viewport.y + this.scene.Get_Viewport.height) - boundsY2;
        else
            pasteY := 0.0;
        end if;

        -- clamp the paste offset to a multiple of tileWidth so entities that
        -- were grid snapped will remain so. here, we round the paste offset up
        -- to the next tileWidth to that items are placed fully within the
        -- viewport. (simply rounding can leaves part of the edge objects outside
        -- the viewport)
        pasteX := Float'Copy_Sign( Float'Ceiling( abs pasteX / Float(this.scene.Get_Tile_Width) ) * this.scene.Get_Tile_Width, pasteX );
        pasteY := Float'Copy_Sign( Float'Ceiling( abs pasteY / Float(this.scene.Get_Tile_Width) ) * this.scene.Get_Tile_Width, pasteY );

        this.scene.Entity_Selection_Clear;
        cmd := Create_Compound_Command;
        for i in 2..clipboard.Length loop
            eid := Unique_Tagged_Id( Entity_Tag );
            ex := clipboard.Get( i ).Map.Get_Float( "x" ) + pasteX;
            ey := clipboard.Get( i ).Map.Get_Float( "y" ) + pasteY;

            cmd.Add( Create_Command_Spawn_Entity( template => clipboard.Get( i ).Map.Get_String( "template" ),
                                                  name => clipboard.Get( i ).Map.Get_String( "name" ),
                                                  x => Float'Rounding( Constrain( ex, 0.0, Float(this.scene.Get_Width_Tiles * this.scene.Get_Tile_Width) ) ),
                                                  y => Float'Rounding( Constrain( ey, 0.0, Float(this.scene.Get_Height_Tiles * this.scene.Get_Tile_Width) ) ),
                                                  width => clipboard.Get( i ).Map.Get_Float( "w" ),
                                                  height => clipboard.Get( i ).Map.Get_Float( "h" ),
                                                  attributes => clipboard.Get( i ).Map.Get( "attributes" ).Map,
                                                  entityId => eid ) );
            this.scene.Entity_Selection_Add_Pending( eid );
        end loop;
        this.view.Commands.Execute( cmd );

        -- switch to this tool if it's not already in use
        this.view.Select_Tool( Pointer_Tool );
    end Do_Paste;

    ----------------------------------------------------------------------------

    procedure Finish_Move( this   : not null access Pointer'Class;
                           entity : not null A_Entity ) is
        cmd        : A_Compound_Command;
        dx, dy     : Float;
        newX, newY : Float;
    begin
        -- calculate the total distance that the entity was dragged
        dx := entity.Get_Widget.Get_Geometry.x - this.moveFromX;
        dy := entity.Get_Widget.Get_Geometry.y - this.moveFromY;

        -- Allow the selected entities to receive movement events again. they
        -- were frozen earlier in Apply_To_Entity().
        this.Selection_Freeze_Updates( this.scene.Get_Selected_Entities, False );

        -- move entities to match their widget's new location
        if this.movedByDrag then
            cmd := Create_Compound_Command;
            for entity of this.scene.Get_Selected_Entities loop
                this.Location_After_Drag( entity, dx, dy, newX, newY );

                -- update the scene entity's location immediately, to avoid a
                -- visible delay between when the drag is released and when the
                -- entity moved event is generated by the game processing the
                -- command.
                if newX /= entity.Get_Entity_X or newY /= entity.Get_Entity_Y then
                    cmd.Add( Create_Command_Move_Entity( entity.Get_Entity_Id,
                                                         entity.Get_Entity_X, entity.Get_Entity_Y,
                                                         newX, newY ) );
                end if;

                entity.Set_Entity_Location( newX, newY );
            end loop;
            this.view.Commands.Execute( cmd );
        end if;
    end Finish_Move;

    ----------------------------------------------------------------------------

    procedure Finish_Resize( this   : not null access Pointer'Class;
                             entity : not null A_Entity ) is
        cmd             : A_Command;
    begin
        -- resize the entity to match its widget's new size
        cmd := Create_Command_Resize_Entity( entity.Get_Entity_Id,
                                             this.resizeFromW, this.resizeFromH,
                                             (if this.resizableWidth then entity.Get_Widget.Width.Get else entity.Get_Entity_Width),
                                             (if this.resizableHeight then entity.Get_Widget.Height.Get else entity.Get_Entity_Height),
                                             entity.Get_Entity_X, entity.Get_Entity_Y,
                                             entity.Get_Render_X, entity.Get_Render_Y );

        -- update the entity's location so there isn't a visual glitch between
        -- now and when the next Entity_Moved event is received.
        entity.Set_Entity_Location( entity.Get_Render_X, entity.Get_Render_Y );

        this.view.Commands.Execute( cmd );
    end Finish_Resize;

    ----------------------------------------------------------------------------

    -- Returns the resizing direction associated with the resize handle that
    -- 'x,y' is within, or Dir_None if the point is not in a resize handle area.
    -- Not all entities are resizable in every direction.
    function Get_Resize_Direction( this   : not null access Pointer'Class;
                                   entity : not null A_Entity;
                                   wx, wy : Float ) return Direction_Type is

        function Effective_Zoom( w : A_Widget ) return Float is (if w /= null then w.Zoom.Get * Effective_Zoom( w.Get_Parent ) else 1.0);

        HANDLE_WIDTH : constant Float := GRAB_BORDER_SIZE / Effective_Zoom( entity.Get_Widget );

        top    : Rectangle := (0.0, 0.0, entity.Get_Widget.Get_Viewport.width, HANDLE_WIDTH);
        left   : Rectangle := (0.0, 0.0, HANDLE_WIDTH, entity.Get_Widget.Get_Viewport.height );
        bottom : Rectangle := (0.0, entity.Get_Widget.Get_Viewport.height - HANDLE_WIDTH, entity.Get_Widget.Get_Viewport.width, HANDLE_WIDTH);
        right  : Rectangle := (entity.Get_Widget.Get_Viewport.width - HANDLE_WIDTH, 0.0, HANDLE_WIDTH, entity.Get_Widget.Get_Viewport.height);
    begin
        if this.resizableWidth then
            top.x := top.x + HANDLE_WIDTH;
            top.width := top.width - HANDLE_WIDTH * 2.0;
            bottom.x := top.x;
            bottom.width := top.width;
        end if;
        if this.resizableHeight then
            left.y := left.y + HANDLE_WIDTH;
            left.height := left.height - HANDLE_WIDTH * 2.0;
            right.y := left.y;
            right.height := left.height;
        end if;

        return (
            if (this.resizableWidth and this.resizableHeight) and then
               Contains( Rectangle'(left.x, top.y, left.width, top.height), wx, wy ) then Dir_Up_Left

            elsif (this.resizableWidth and this.resizableHeight) and then
                  Contains( Rectangle'(right.x, top.y, right.width, top.height), wx, wy ) then Dir_Up_Right

            elsif (this.resizableWidth and this.resizableHeight) and then
                  Contains( Rectangle'(left.x, bottom.y, left.width, bottom.height), wx, wy ) then Dir_Down_Left

            elsif (this.resizableWidth and this.resizableHeight) and then
                  Contains( Rectangle'(right.x, bottom.y, right.width, bottom.height), wx, wy ) then Dir_Down_Right

            elsif this.resizableHeight and then
                  Contains( top, wx, wy ) then Dir_Up

            elsif this.resizableHeight and then
                  Contains( bottom, wx, wy ) then Dir_Down

            elsif this.resizableWidth and then
                  Contains( left, wx, wy ) then Dir_Left

            elsif this.resizableWidth and then
                  Contains( right, wx, wy ) then Dir_Right

            else Dir_None
        );
    end Get_Resize_Direction;

    ----------------------------------------------------------------------------

    procedure Location_After_Drag( this       : not null access Pointer'Class;
                                   entity     : not null A_Entity;
                                   dx, dy     : Float;
                                   newX, newY : out Float ) is
        gridSnap : constant Boolean := Get_Pref( "gridsnap" );
        gridSize : constant Float := Get_Pref( "gridsize" );

        function Snap( n : Float; size : Float; gridCenter : Boolean ) return Float is
        begin
            if gridSnap and this.snapEnabled then
                if gridCenter then
                    return Support.Snap( n, gridSize, True );
                else
                    return Support.Snap( n - size / 2.0, gridSize, False ) + size / 2.0;
                end if;
            end if;
            return n;
        end Snap;

    begin
        newX := Snap( entity.Get_Render_X - this.placeOffsetX + dx, entity.Get_Entity_Width, this.snapCenterX ) + this.placeOffsetX;
        newY := Snap( entity.Get_Render_Y - this.placeOffsetY + dy, entity.Get_Entity_Height, this.snapCenterY ) + this.placeOffsetY;
    end Location_After_Drag;

    ----------------------------------------------------------------------------

    procedure Continue_Resize( this           : not null access Pointer'Class;
                               entity         : not null A_Entity;
                               mouseX, mouseY : Float ) is
        gridSnap : constant Boolean := Get_Pref( "gridsnap" );

        ------------------------------------------------------------------------

        function Snap_Size( size : Float; gridSize : Float ) return Float is
            amount : Float := 0.0;
        begin
            if this.snapEnabled and then gridSnap then
                -- snap the resize delta to whole steps of 'gridsize'.
                -- this requires that the entity be snapped to grid edges.
                amount := Fmod( size, gridSize );
                if amount < 8.0 then
                    amount := -amount;
                else
                    amount := gridSize - amount;
                end if;
            end if;
            return size + amount;
        end Snap_Size;

        ------------------------------------------------------------------------

        gridSize   : constant Float := Get_Pref( "gridsize" );
        minEWidth  : constant Float := Float'Max( gridSize, this.minEWidth );
        minEHeight : constant Float := Float'Max( gridSize, this.minEHeight );

        dx         : constant Float := mouseX - this.applyX;
        dy         : constant Float := mouseY - this.applyY;
        width      : constant Float := entity.Get_Widget.Width.Get;
        height     : constant Float := entity.Get_Widget.Height.Get;
        moveX,
        moveY      : Float := 0.0;
        newSize    : Float;
    begin
        if this.resizeDir.x /= 0 and this.resizableWidth then
            newSize := Float'Max( Snap_Size( width + dx * Float(this.resizeDir.x), gridSize ), minEWidth );
            if newSize /= width then
                -- distance to move. the center moves half this distance, and
                -- the edge moves the full distance.
                moveX := this.resizeDir.x * (newSize - width);

                if this.resizeDir.x > 0 then
                    -- if the right or bottom edges move, the drag point has to
                    -- be updated to maintain the same relative distance to the
                    -- edge that is being dragged.
                    this.applyX := this.applyX + moveX;
                end if;

                -- resize the entity to the new size
                entity.Resize( newSize, entity.Get_Entity_Height );
            end if;
        end if;

        if this.resizeDir.y /= 0 and this.resizableHeight then
            newSize := Float'Max( minEHeight, Snap_Size( height + dy * Float(this.resizeDir.y), gridSize ) );
            if newSize /= height then
                moveY := this.resizeDir.y * (newSize - height);

                if this.resizeDir.y > 0 then
                    -- dragging bottom edge
                    this.applyY := this.applyY + moveY;
                end if;

                entity.Resize( entity.Get_Entity_Width, newSize );
            end if;
        end if;

        -- move the center as the entity size changes, to make it appear that
        -- only one edge is being dragged while the rest of the entity is still.
        entity.Move_Render_Location( moveX / 2.0, moveY / 2.0 );
    end Continue_Resize;

    ----------------------------------------------------------------------------

    procedure Selection_Freeze_Updates( this     : not null access Pointer'Class;
                                        entities : Entity_Sets.Set;
                                        freeze   : Boolean ) is
        pragma Unreferenced( this );
    begin
        for entity of entities loop
            entity.Freeze_Updates( freeze );
        end loop;
    end Selection_Freeze_Updates;

    ----------------------------------------------------------------------------

    procedure Start_Move( this   : not null access Pointer'Class;
                          entity : not null A_Entity ) is
    begin
        this.movedByDrag := False;
        this.moveFromX := entity.Get_Widget.Get_Geometry.x;
        this.moveFromY := entity.Get_Widget.Get_Geometry.y;
        this.Selection_Freeze_Updates( this.scene.Get_Selected_Entities, True );
    end Start_Move;

    ----------------------------------------------------------------------------

    procedure Start_Resize( this   : not null access Pointer'Class;
                            entity : not null A_Entity ) is
    begin
        this.resizeDir := this.Get_Resize_Direction( entity, this.applyX, this.applyY );
        this.resizeFromW := entity.Get_Entity_Width;
        this.resizeFromH := entity.Get_Entity_Height;
    end Start_Resize;

end Tools.Pointers;

