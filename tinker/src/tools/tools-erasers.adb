--
-- Copyright (c) 2014-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Keyboard;                  use Allegro.Keyboard;
with Commands;                          use Commands;
with Drawing;                           use Drawing;
with Drawing.Primitives;                use Drawing.Primitives;
with Game_Views.Tinker;                 use Game_Views.Tinker;
with Icons;                             use Icons;
with Palette;                           use Palette;
with Preferences;                       use Preferences;
with Signals;                           use Signals;
with Support;                           use Support;
with Tools.Painters;                    use Tools.Painters;
with Widgets.Buttons.Checkboxes;        use Widgets.Buttons.Checkboxes;
with Widgets.Buttons.Pushes;            use Widgets.Buttons.Pushes;
with Widgets.Input_Boxes.Constraints;   use Widgets.Input_Boxes.Constraints;
with Widgets.Labels;                    use Widgets.Labels;
with Widgets.Row_Layouts;               use Widgets.Row_Layouts;

package body Tools.Erasers is

    package Connections is new Signals.Connections(Eraser);
    use Connections;

    ----------------------------------------------------------------------------

    procedure Clicked_All_Layers_Disable( this : not null access Eraser'Class );

    procedure Clicked_All_Layers_Enable( this : not null access Eraser'Class );

    procedure Clicked_Size_Decrease( this : not null access Eraser'Class );

    procedure Clicked_Size_Increase( this : not null access Eraser'Class );

    procedure Entered_Size( this : not null access Eraser'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Eraser return A_Tool is
        this : constant A_Eraser := new Eraser;
    begin
        this.Construct( Eraser_Tool );
        this.size := ((Constrain( Get_Pref( "tools", "brushSize", this.size ), 1, MAX_SIZE ) / 2) * 2) + 1;
        this.layerSpecific := not Get_Pref( "tools", "erasor.allLayers", False );     -- default to True
        return A_Tool(this);
    end Create_Eraser;

    ----------------------------------------------------------------------------

    procedure Clicked_All_Layers_Disable( this : not null access Eraser'Class ) is
        view : constant A_Tinker_View := A_Tinker_View(A_Widget(this.Signaller).Get_View);
    begin
        this.layerSpecific := True;
        Set_Pref( "tools", "erasor.allLayers", not this.layerSpecific );
        view.Get_Scene.Set_Active_Layer( view.Get_Scene.Get_Active_Layer );
        view.Get_Scene.Request_Focus;
    end Clicked_All_Layers_Disable;

    ----------------------------------------------------------------------------

    procedure Clicked_All_Layers_Enable( this : not null access Eraser'Class ) is
        view : constant A_Tinker_View := A_Tinker_View(A_Widget(this.Signaller).Get_View);
    begin
        this.layerSpecific := False;
        Set_Pref( "tools", "erasor.allLayers", not this.layerSpecific );
        view.Get_Scene.Set_Active_Layer( view.Get_Scene.Get_Active_Layer );
        view.Get_Scene.Request_Focus;
    end Clicked_All_Layers_Enable;

    ----------------------------------------------------------------------------

    procedure Clicked_Size_Decrease( this : not null access Eraser'Class ) is
    begin
        if this.size > 2 then
            this.size := this.size - 2;
            this.sizeInput.Set_Text( Image( this.size ) );
            Set_Pref( "tools", "brushSize", this.size );
        end if;
    end Clicked_Size_Decrease;

    ----------------------------------------------------------------------------

    procedure Clicked_Size_Increase( this : not null access Eraser'Class ) is
    begin
        if this.size < MAX_SIZE then
            this.size := this.size + 2;
            this.sizeInput.Set_Text( Image( this.size ) );
            Set_Pref( "tools", "brushSize", this.size );
        end if;
    end Clicked_Size_Increase;

    ----------------------------------------------------------------------------

    overriding
    procedure Apply_To_Map( this      : access Eraser;
                            func      : Function_Type;
                            modifiers : Modifiers_Array;
                            first     : Boolean;
                            context   : Map_Tool_Context ) is
        pragma Unreferenced( modifiers );
        tw       : constant Integer := this.scene.Get_Tile_Width;
        halfSize : constant Integer := this.size / 2;
        midX     : constant Integer := Integer(context.x) / tw;
        midY     : constant Integer := Integer(context.y) / tw;
        tileX1   : constant Integer := Integer'Max( 0, midX - halfSize );
        tileY1   : constant Integer := Integer'Max( 0, midY - halfSize );
        tileX2   : constant Integer := Integer'Min( midX + halfSize, this.scene.Get_Map.Get_Width - 1 );
        tileY2   : constant Integer := Integer'Min( midY + halfSize, this.scene.Get_Map.Get_Height - 1 );
        cmd      : A_Set_Tiles_Command;
        tileId   : Natural;
    begin
        -- erase a tile --
        if func = Primary then
            if first then
                this.lastX := -1;
                this.lastY := -1;
            end if;

            -- don't execute the erase command multiple times for the same tile when
            -- dragging the eraser around.
            if midX /= this.lastX or midY /= this.lastY then
                this.lastX := midX;
                this.lastY := midY;

                cmd := Create_Command_Set_Tiles;

                for tileY in tileY1..tileY2 loop
                    for tileX in tileX1..tileX2 loop

                        -- only erase tiles within the active map area and on
                        -- visible layers
                        if this.scene.In_Active_Area( tileX, tileY ) then
                            for layer in this.scene.Get_Map.Get_Foreground_Layer..this.scene.Get_Map.Get_Background_Layer loop
                                if this.scene.Is_Layer_Visible( layer ) then
                                    if layer = context.layer or not this.layerSpecific then
                                        cmd.Add( layer, tileX, tileY,
                                                 this.scene.Get_Map.Get_Tile_Id( layer, tileX, tileY ), 0 );
                                    end if;
                                end if;
                            end loop;
                        end if;

                    end loop;
                end loop;

                if cmd.Tile_Count > 0 then
                    this.view.Commands.Execute( cmd );
                else
                    Delete( A_Command(cmd) );
                end if;
            end if;

        -- pick a tile and paint --
        elsif func = Secondary then
            for layer in this.scene.Get_Map.Get_Foreground_Layer..this.scene.Get_Map.Get_Background_Layer loop
                -- pick from the active layer or the tile in the most foreground visible layer
                if this.scene.Is_Layer_Visible( layer ) then
                    if not this.layerSpecific or layer = context.layer then
                        tileId := this.scene.Get_Map.Get_Tile_Id( context.layer,
                                                                  Integer(context.x) / this.scene.Get_Tile_Width,
                                                                  Integer(context.y) / this.scene.Get_Tile_Width );
                        exit when tileId /= 0 or this.layerSpecific;
                    end if;
                end if;
            end loop;
            if tileId /= 0 then
                this.view.Replace_Tool( Create_Painter( tileId ) );
                this.view.Select_Tool( Painter_Tool );
            end if;
        end if;
    end Apply_To_Map;

    ----------------------------------------------------------------------------

    overriding
    function Create_Options( this : access Eraser;
                             view : A_Game_View ) return A_Widget is
        row    : A_Row_Layout;
        label  : A_Label;
        button : A_Button;
    begin
        row := Create_Row_Layout( view );

            label := Create_Label( view );
            label.Set_Style( "eraserOptions" );
            label.Set_Text( "Brush width:" );
            label.Set_Preferred_Width( label.Get_Min_Width );
            row.Append( label );

            button := Create_Push_Button( view );
            button.Set_Style( "eraserOptions" );
            button.Set_Icon( Create_Icon( "tinker:minus-white-16" ) );
            button.Width.Set( 20.0 );
            button.Height.Set( 20.0 );
            button.Clicked.Connect( Slot( this, Clicked_Size_Decrease'Access ) );
            row.Append( button );

            this.sizeInput := Create_Input_Box( view );
            this.sizeInput.Set_Style( "eraserOptions" );
            this.sizeInput.Set_Text( Image( this.size ) );
            this.sizeInput.Width.Set( 25.0 );
            this.sizeInput.Height.Set( 20.0 );
            this.sizeInput.Accepted.Connect( Slot( this, Entered_Size'Access ) );
            this.sizeInput.Blurred.Connect( Slot( this, Entered_Size'Access ) );
            this.sizeInput.Set_Constraint( Int_Only'Access );
            this.sizeInput.Set_Max_Length( 2 );
            row.Append( this.sizeInput );

            button := Create_Push_Button( view );
            button.Set_Style( "eraserOptions" );
            button.Set_Icon( Create_Icon( "tinker:plus-white-16" ) );
            button.Width.Set( 20.0 );
            button.Height.Set( 20.0 );
            button.Clicked.Connect( Slot( this, Clicked_Size_Increase'Access ) );
            row.Append( button );

            label := Create_Label( view );
            label.Set_Style( "eraserOptions" );
            label.Set_Text( " | " );
            row.Append( label );

            this.btnAllLayers := A_Button(Create_Checkbox( view ));
            this.btnAllLayers.Set_Style( "erasorOptions" );
            this.btnAllLayers.Set_Text( " All Layers" );
            this.btnAllLayers.Set_State( not this.layerSpecific );
            this.btnAllLayers.Pressed.Connect( Slot( this, Clicked_All_Layers_Enable'Access ) );
            this.btnAllLayers.Released.Connect( Slot( this, Clicked_All_Layers_Disable'Access ) );
            row.Append( this.btnAllLayers );

        row.Set_Preferred_Width( A_Widget(row).Get_Min_Width );
        return A_Widget(row);
    end Create_Options;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Preview_Foreground( this      : access Eraser;
                                       modifiers : Modifiers_Array;
                                       context   : Tool_Preview_Context ) is
        pragma Unreferenced( modifiers );
        tw         : constant Integer := this.scene.Get_Tile_Width;
        halfSize   : constant Integer := this.size / 2;
        midX       : constant Integer := Integer(context.x) / tw;
        midY       : constant Integer := Integer(context.y) / tw;
        tileX1     : constant Integer := Integer'Max( 0, midX - halfSize );
        tileY1     : constant Integer := Integer'Max( 0, midY - halfSize );
        tileX2     : constant Integer := Integer'Min( midX + halfSize, this.scene.Get_Map.Get_Width - 1 );
        tileY2     : constant Integer := Integer'Min( midY + halfSize, this.scene.Get_Map.Get_Height - 1 );
    begin
        if not this.scene.Get_Map.In_Map_Area( midX, midY ) or not this.scene.Is_Mouse_In_Viewport then
            return;
        end if;

        -- highlight the preview area
        -- this should be drawn in front of all layers
        if tileX1 <= tileX2 and tileY1 <= tileY2 then
            -- the highlighted eraser area is not filled when operating on a
            -- single layer that is not active.
            if this.scene.Is_Layer_Visible( context.layer ) or not this.Is_Layer_Specific then
                Rectfill_XYWH( Float(tileX1 * tw), Float(tileY1 * tw),
                               Float((tileX2 - tileX1 + 1) * tw), Float((tileY2 - tileY1 + 1) * tw),
                               Tools.Painters.ERASE_TINT );
            end if;
            Rect_XYWH( Float(tileX1 * tw), Float(tileY1 * tw),
                       Float((tileX2 - tileX1 + 1) * tw), Float((tileY2 - tileY1 + 1) * tw),
                       Whiten( Opacity( Tools.Painters.ERASE_TINT, 1.0 ), 0.25 ) );
        end if;
    end Draw_Preview_Foreground;

    ----------------------------------------------------------------------------

    procedure Entered_Size( this : not null access Eraser'Class ) is
    begin
        this.size := Integer'Value( this.sizeInput.Get_Text );
        this.size := ((this.size / 2) * 2) + 1;
        this.sizeInput.Set_Text( Image( this.size) );
    end Entered_Size;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Selected( this : access Eraser ) is
    begin
        this.size := ((Constrain( Get_Pref( "tools", "brushSize", this.size ), 1, MAX_SIZE ) / 2) * 2) + 1;
        if this.sizeInput /= null then
            this.sizeInput.Set_Text( Image( this.size ) );
        end if;
    end On_Selected;

    ----------------------------------------------------------------------------

    overriding
    procedure Post_Apply_Map( this      : access Eraser;
                              func      : Function_Type;
                              modifiers : Modifiers_Array;
                              context   : Map_Tool_Context ) is
        pragma Unreferenced( func, modifiers, context );
    begin
        this.view.Commands.Prevent_Combine;    -- no more combining tile commands
    end Post_Apply_Map;

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Key_Typed( this      : access Eraser;
                                key       : Integer;
                                modifiers : Modifiers_Array ) is
    begin
        if key = ALLEGRO_KEY_MINUS or key = ALLEGRO_KEY_PAD_MINUS then
            this.Clicked_Size_Decrease;
        elsif key = ALLEGRO_KEY_EQUALS or key = ALLEGRO_KEY_PAD_PLUS then
            this.Clicked_Size_Increase;
        else
            Tool(this.all).Handle_Key_Typed( key, modifiers );
        end if;
    end Handle_Key_Typed;

end Tools.Erasers;
