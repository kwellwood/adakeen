--
-- Copyright (c) 2014-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Clipping;                          use Clipping;
with Icons;                             use Icons;
with Widgets.Buttons;                   use Widgets.Buttons;
with Widgets.Labels;                    use Widgets.Labels;

package Tools.Pickers is

    type Picker is new Tool with private;
    type A_Picker is access all Picker'Class;

    function Create_Picker return A_Tool;
    pragma Postcondition( Create_Picker'Result /= null );

    overriding
    function Acts_On( this : access Picker; target : Subject_Type ) return Boolean is (target = Map_Subject);

    procedure Apply_To_Map( this      : access Picker;
                            func      : Function_Type;
                            modifiers : Modifiers_Array;
                            first     : Boolean;
                            context   : Map_Tool_Context );

    function Create_Options( this : access Picker;
                             view : A_Game_View ) return A_Widget;

    procedure Draw_Preview_Foreground( this      : access Picker;
                                       modifiers : Modifiers_Array;
                                       context   : Tool_Preview_Context );

    overriding
    function Get_Cursor( this : access Picker ) return String is ("tinker:cursor-picker");

    procedure On_Selected( this : access Picker );

private

    type Picker is new Tool with
        record
            lastX        : Integer := -1;
            lastY        : Integer := -1;
            tileLabel    : A_Label := null;
            btnAllLayers : A_Button := null;
        end record;

    procedure Update_Label( this  : not null access Picker'Class;
                            icon  : Icon_Type;
                            shape : Clip_Type );

end Tools.Pickers;
