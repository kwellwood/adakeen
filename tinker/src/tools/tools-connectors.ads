--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Tools.Connectors is

    type Connector is new Tool with private;
    type A_Connector is access all Connector'Class;

    -- Creates a new entity connector tool.
    function Create_Connector return A_Tool;
    pragma Postcondition( Create_Connector'Result /= null );

    overriding
    function Acts_On( this : access Connector; target : Subject_Type ) return Boolean is (target = Entity_Subject);

    procedure Apply_To_Entity( this      : access Connector;
                               func      : Function_Type;
                               modifiers : Modifiers_Array;
                               first     : Boolean;
                               context   : Entity_Tool_Context );

    procedure Draw_Preview_Foreground( this      : access Connector;
                                       modifiers : Modifiers_Array;
                                       context   : Tool_Preview_Context );

    function Get_Attached_Entity( this : access Connector ) return Entity_Id;

    function Get_Cursor( this : access Connector ) return String;

    function Is_Busy( this : access Connector ) return Boolean;

    procedure On_Selected( this : access Connector );

private

    type Connector is new Tool with
        record
            fromId : Entity_Id := NULL_ID;
        end record;

    overriding
    function Get_Attached_Entity( this : access Connector ) return Entity_Id is (this.fromId);

    function Get_Cursor( this : access Connector ) return String is ("tinker:cursor-connector-" & (if Is_Null( this.fromId ) then "a" else "b"));

end Tools.Connectors;
