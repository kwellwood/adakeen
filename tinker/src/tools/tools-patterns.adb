--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Assets.Libraries;                  use Assets.Libraries;
with Commands;                          use Commands;
--with Debugging;                         use Debugging;
with Drawing;                           use Drawing;
with Drawing.Bitmaps;                   use Drawing.Bitmaps;
with Drawing.Primitives;                use Drawing.Primitives;
with Game_Views.Tinker;                 use Game_Views.Tinker;
with Icons;                             use Icons;
with Palette;                           use Palette;
with Preferences;                       use Preferences;
with Signals;                           use Signals;
with Support;                           use Support;
with Tiles;                             use Tiles;
with Tools.Painters;                    use Tools.Painters;
with Values.Casting;                    use Values.Casting;
with Values.Strings;                    use Values.Strings;
with Widgets.Buttons;                   use Widgets.Buttons;
with Widgets.Buttons.Checkboxes;        use Widgets.Buttons.Checkboxes;
with Widgets.Buttons.Pushes;            use Widgets.Buttons.Pushes;
with Widgets.Input_Boxes.Constraints;   use Widgets.Input_Boxes.Constraints;
with Widgets.Labels;                    use Widgets.Labels;
with Widgets.Row_Layouts;               use Widgets.Row_Layouts;

package body Tools.Patterns is

    package Connections is new Signals.Connections(Patternbrush);
    use Connections;

    ----------------------------------------------------------------------------

    procedure Clicked_Offset_Disable( this : not null access Patternbrush'Class );

    procedure Clicked_Offset_Enable( this : not null access Patternbrush'Class );

    procedure Clicked_Size_Decrease( this : not null access Patternbrush'Class );

    procedure Clicked_Size_Increase( this : not null access Patternbrush'Class );

    procedure Entered_Size( this : not null access Patternbrush'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Patternbrush( pattern : not null A_Map;
                                  offsetX,
                                  offsetY : Integer ) return A_Tool is
        this : constant A_Patternbrush := new Patternbrush;
    begin
        this.Construct( pattern, offsetX, offsetY );
        return A_Tool(this);
    end Create_Patternbrush;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this    : access Patternbrush;
                         pattern : A_Map;
                         offsetX,
                         offsetY : Integer ) is
    begin
        Tool(this.all).Construct( Pattern_Tool );
        this.layerSpecific := True;
        this.pattern := Copy( pattern );
        this.offsetX := offsetX;
        this.offsetY := offsetY;
        this.useOffset := Get_Pref( "tools", "pattern.useOffset", this.useOffset );
        this.size := ((Constrain( Get_Pref( "tools", "brushSize", this.size ), 1, MAX_SIZE ) / 2) * 2) + 1;
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Patternbrush ) is
    begin
        Delete( this.pattern );
        Tool(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    overriding
    procedure Apply_To_Map( this      : access Patternbrush;
                            func      : Function_Type;
                            modifiers : Modifiers_Array;
                            first     : Boolean;
                            context   : Map_Tool_Context ) is
        tw       : constant Natural := this.scene.Get_Tile_Width;
        halfSize : constant Integer := this.size / 2;
        midX     : constant Integer := Integer(context.x) / tw;
        midY     : constant Integer := Integer(context.y) / tw;
        tileX1   : constant Integer := Integer'Max( 0, Integer(context.x) / tw - halfSize );
        tileY1   : constant Integer := Integer'Max( 0, Integer(context.y) / tw - halfSize );
        tileX2   : constant Integer := Integer'Min( Integer(context.x) / tw + halfSize, this.scene.Get_Map.Get_Width - 1 );
        tileY2   : constant Integer := Integer'Min( Integer(context.y) / tw + halfSize, this.scene.Get_Map.Get_Height - 1 );
        width    : constant Integer := this.pattern.Get_Width;
        height   : constant Integer := this.pattern.Get_Height;
        cmd      : A_Set_Tiles_Command;
        offX     : Integer := 0;
        offY     : Integer := 0;
        tileId   : Natural := 0;
    begin
        -- paint a pattern --
        if func = Primary then
            if this.scene.Get_Map.Get_Layer_Type( context.layer ) /= Layer_Tiles then
                -- patterns allowed only on tile layers
                return;
            end if;

            if first then
                this.startTileX := midX;
                this.startTileY := midY;
                this.lastX := -1;
                this.lastY := -1;
            end if;

            -- don't execute the patternbrush multiple times for the same exact tiles
            -- when the patternbrush is being dragged around.
            if midX /= this.lastX or midY /= this.lastY then
                this.lastX := midX;
                this.lastY := midY;

                if this.useOffset then
                    offX := this.offsetX;
                    offY := this.offsetY;
                end if;

                if modifiers(CTRL) then
                    -- Auto-Offset constraint: draw the pattern as though the first
                    -- draw location is at offset of the pattern.
                    offX := width  - ((this.startTileX - offX) mod width  );
                    offY := height - ((this.startTileY - offY) mod height );
                end if;

                if this.scene.Is_Layer_Visible( context.layer ) then
                    cmd := Create_Command_Set_Tiles;

                    for tileY in tileY1..tileY2 loop
                        for tileX in tileX1..tileX2 loop
                            if this.scene.In_Active_Area( tileX, tileY ) then
                                tileId := this.pattern.Get_Tile_Id( 0, (tileX + offX) mod width, (tileY + offY) mod height );
                                if tileId > 0 then
                                    cmd.Add( context.layer, tileX, tileY,
                                             this.scene.Get_Map.Get_Tile_Id( context.layer, tileX, tileY ),
                                             tileId );
                                end if;
                            end if;
                        end loop;
                    end loop;

                    if cmd.Tile_Count > 0 then
                        this.view.Commands.Execute( cmd );
                    else
                        Delete( A_Command(cmd) );
                    end if;
                end if;
            end if;

        -- pick a tile and paint --
        elsif func = Secondary then
            if this.scene.Is_Layer_Visible( context.layer ) then
                tileId := this.scene.Get_Map.Get_Tile_Id( context.layer,
                                                          Integer(context.x) / this.scene.Get_Tile_Width,
                                                          Integer(context.y) / this.scene.Get_Tile_Width );
            end if;
            if tileId = 0 then
                return;
            end if;

            this.view.Replace_Tool( Create_Painter( tileId ) );
            this.view.Select_Tool( Painter_Tool );
        end if;
    end Apply_To_Map;

    ----------------------------------------------------------------------------

    procedure Clicked_Offset_Disable( this : not null access Patternbrush'Class ) is
    begin
        this.useOffset := False;
        Set_Pref( "tools", "pattern.useOffset", this.useOffset );
    end Clicked_Offset_Disable;

    ----------------------------------------------------------------------------

    procedure Clicked_Offset_Enable( this : not null access Patternbrush'Class ) is
    begin
        this.useOffset := True;
        Set_Pref( "tools", "pattern.useOffset", this.useOffset );
    end Clicked_Offset_Enable;

    ----------------------------------------------------------------------------

    procedure Clicked_Size_Decrease( this : not null access Patternbrush'Class ) is
    begin
        if this.size > 2 then
            this.size := this.size - 2;
            this.sizeInput.Set_Text( Image( this.size ) );
            Set_Pref( "tools", "brushSize", this.size );
        end if;
    end Clicked_Size_Decrease;

    ----------------------------------------------------------------------------

    procedure Clicked_Size_Increase( this : not null access Patternbrush'Class ) is
    begin
        if this.size < MAX_SIZE then
            this.size := this.size + 2;
            this.sizeInput.Set_Text( Image( this.size ) );
            Set_Pref( "tools", "brushSize", this.size );
        end if;
    end Clicked_Size_Increase;

    ----------------------------------------------------------------------------

    overriding
    function Create_Options( this : access Patternbrush;
                             view : A_Game_View ) return A_Widget is
        row      : A_Row_Layout;
        label    : A_Label;
        button   : A_Button;
        checkbox : A_Checkbox;
    begin
        row := Create_Row_Layout( view );
        row.Set_Style_Property( "background.align", Create( "center" ) );

            label := Create_Label( view );
            label.Set_Style( "patternOptions" );
            label.Set_Icon( Create_Icon( A_Tinker_View(view).Get_Scene.Get_Library,
                                         this.pattern.Get_Layer_Property( 0, "icon" ).To_Int ) );
            label.Set_Text( "Brush width:" );
            label.Set_Preferred_Width( label.Get_Min_Width );
            row.Append( label );

            button := Create_Push_Button( view );
            button.Set_Style( "patternOptions" );
            button.Set_Icon( Create_Icon( "tinker:minus-white-16" ) );
            button.Width.Set( 20.0 );
            button.Height.Set( 20.0 );
            button.Clicked.Connect( Slot( this, Clicked_Size_Decrease'Access ) );
            row.Append( button );

            this.sizeInput := Create_Input_Box( view );
            this.sizeInput.Set_Style( "patternOptions" );
            this.sizeInput.Set_Text( Image( this.size ) );
            this.sizeInput.Width.Set( 25.0 );
            this.sizeInput.Height.Set( 20.0 );
            this.sizeInput.Accepted.Connect( Slot( this, Entered_Size'Access ) );
            this.sizeInput.Blurred.Connect( Slot( this, Entered_Size'Access ) );
            this.sizeInput.Set_Constraint( Int_Only'Access );
            this.sizeInput.Set_Max_Length( 2 );
            row.Append( this.sizeInput );

            button := Create_Push_Button( view );
            button.Set_Style( "patternOptions" );
            button.Set_Icon( Create_Icon( "tinker:plus-white-16" ) );
            button.Width.Set( 20.0 );
            button.Height.Set( 20.0 );
            button.Clicked.Connect( Slot( this, Clicked_Size_Increase'Access ) );
            row.Append( button );

            label := Create_Label( view );
            label.Set_Style( "patternOptions" );
            label.Set_Text( " | " );
            row.Append( label );

            checkbox := Create_Checkbox( view );
            checkbox.Set_Style( "patternOptions" );
            checkbox.Set_Text( "Offset: " & Image( this.offsetX ) & ", " & Image( this.offsetY ) );
            checkbox.Set_State( this.useOffset );
            checkbox.Pressed.Connect( Slot( this, Clicked_Offset_Enable'Access ) );
            checkbox.Released.Connect( Slot( this, Clicked_Offset_Disable'Access ) );
            row.Append( checkbox );

            label := Create_Label( view );
            label.Set_Style( "patternOptions" );
            label.Set_Text( " | " & Cast_String( this.pattern.Get_Layer_Property( 0, "name" ) ) );
            row.Append( label );

        row.Set_Preferred_Width( A_Widget(row).Get_Min_Width );
        return A_Widget(row);
    end Create_Options;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Preview_Foreground( this      : access Patternbrush;
                                       modifiers : Modifiers_Array;
                                       context   : Map_Tool_Context ) is
        tw       : constant Natural := this.scene.Get_Tile_Width;
        halfSize : constant Integer := this.size / 2;
        midX     : constant Integer := Integer(context.x) / tw;
        midY     : constant Integer := Integer(context.y) / tw;
        tileX1   : constant Integer := Integer'Max( 0, midX - halfSize );
        tileY1   : constant Integer := Integer'Max( 0, midY - halfSize );
        tileX2   : constant Integer := Integer'Min( midX + halfSize, this.scene.Get_Map.Get_Width - 1 );
        tileY2   : constant Integer := Integer'Min( midY + halfSize, this.scene.Get_Map.Get_Height - 1 );
        pragma Unreferenced( modifiers );
    begin
        if not this.scene.Get_Map.In_Map_Area( midX, midY ) or
           not this.scene.Is_Mouse_In_Viewport or
           this.scene.Get_Map.Get_Layer_Type( context.layer ) /= Layer_Tiles    -- patterns allowed only on tile layers
        then
            return;
        end if;

        -- highlight the affected area to paint
        if tileY1 <= tileY2 and tileX1 <= tileX2 then

            -- the highlighted paint area is not filled when the active layer is
            -- not visible
            if this.scene.Is_Layer_Visible( context.layer ) then
                Rectfill_XYWH( Float(tileX1 * tw), Float(tileY1 * tw),
                               Float((tileX2 - tileX1 + 1) * tw), Float((tileY2 - tileY1 + 1) * tw),
                               Tools.Painters.DRAW_TINT );
            end if;

            -- highlighted area border
            Rect_XYWH( Float(tileX1 * tw), Float(tileY1 * tw),
                       Float((tileX2 - tileX1 + 1) * tw), Float((tileY2 - tileY1 + 1) * tw),
                       Whiten( Opacity( Tools.Painters.DRAW_TINT, 1.0 ), 0.25 ) );
        end if;
    end Draw_Preview_Foreground;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Preview_Layer( this      : access Patternbrush;
                                  modifiers : Modifiers_Array;
                                  context   : Map_Tool_Context ) is
        tw       : constant Natural := this.scene.Get_Tile_Width;
        halfSize : constant Integer := this.size / 2;
        midX     : constant Integer := Integer(context.x) / tw;
        midY     : constant Integer := Integer(context.y) / tw;
        tileX1   : constant Integer := Integer'Max( 0, midX - halfSize );
        tileY1   : constant Integer := Integer'Max( 0, midY - halfSize );
        tileX2   : constant Integer := Integer'Min( midX + halfSize, this.scene.Get_Map.Get_Width - 1 );
        tileY2   : constant Integer := Integer'Min( midY + halfSize, this.scene.Get_Map.Get_Height - 1 );
        width    : constant Integer := this.pattern.Get_Width;
        height   : constant Integer := this.pattern.Get_Height;
        offX     : Integer := 0;
        offY     : Integer := 0;
        tileId   : Natural;
        bmp      : A_Allegro_Bitmap;
        x, y     : Float;
    begin
        if not this.scene.Get_Map.In_Map_Area( midX, midY ) or
           not this.scene.Is_Mouse_In_Viewport or
           this.scene.Get_Map.Get_Layer_Type( context.layer ) /= Layer_Tiles    -- patterns allowed only on tile layers
        then
            return;
        end if;

        if this.useOffset then
            offX := this.offsetX;
            offY := this.offsetY;
        end if;

        if modifiers(CTRL) then
            -- Auto-Offset constraint: draw the pattern as though the first draw
            -- location is at offset of the pattern.
            if this.startTileX >= 0 and this.startTileY >= 0 then
                -- mouse is held down drawing
                offX := width  - ((this.startTileX - offX) mod width  );
                offY := height - ((this.startTileY - offY) mod height );
            else
                -- not currently drawing
                offX := width  - ((midX - offX) mod width );
                offY := height - ((midY - offY) mod height);
            end if;
        end if;

        -- draw the tile preview
        if this.scene.Is_Layer_Visible( context.layer ) then
            for tileY in tileY1..tileY2 loop
                for tileX in tileX1..tileX2 loop
                    if this.scene.In_Active_Area( tileX, tileY ) then
                        tileId := this.pattern.Get_Tile_Id( 0, (tileX + offX) mod width, (tileY + offY) mod height );
                        if tileId > 0 then
                            x := Float(tileX * tw);
                            y := Float(tileY * tw);
                            To_Target_Pixel_Rounded( x, y );

                            bmp := context.mapLib.Get.Get_Tile( tileId ).Get_Bitmap;
                            Draw_Bitmap_Region( bmp,
                                                0.0, 0.0,
                                                Float(Integer'Min( Al_Get_Bitmap_Width( bmp ), tw )),
                                                Float(Integer'Min( Al_Get_Bitmap_Height( bmp ), tw )),
                                                x, y, 0.0,
                                                Opacity( Tools.Painters.TILE_OPACITY ) );
                        end if;
                    end if;
                end loop;
            end loop;
        end if;
    end Draw_Preview_Layer;

    ----------------------------------------------------------------------------

    procedure Entered_Size( this : not null access Patternbrush'Class ) is
    begin
        this.size := Integer'Value( this.sizeInput.Get_Text );
        this.size := ((this.size / 2) * 2) + 1;
        this.sizeInput.Set_Text( Image( this.size) );
    end Entered_Size;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Selected( this : access Patternbrush ) is
    begin
        this.size := ((Constrain( Get_Pref( "tools", "brushSize", this.size ), 1, MAX_SIZE ) / 2) * 2) + 1;
        if this.sizeInput /= null then
            this.sizeInput.Set_Text( Image( this.size ) );
        end if;
    end On_Selected;

    ----------------------------------------------------------------------------

    overriding
    procedure Post_Apply_Map( this      : access Patternbrush;
                              func      : Function_Type;
                              modifiers : Modifiers_Array;
                              context   : Map_Tool_Context ) is
        pragma Unreferenced( func, modifiers, context );
    begin
        this.view.Commands.Prevent_Combine;    -- no more combining tile commands
        this.startTileX := -1;
        this.startTileY := -1;
    end Post_Apply_Map;

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Key_Typed( this      : access Patternbrush;
                                key       : Integer;
                                modifiers : Modifiers_Array ) is
    begin
        if key = ALLEGRO_KEY_MINUS or key = ALLEGRO_KEY_PAD_MINUS then
            this.Clicked_Size_Decrease;
        elsif key = ALLEGRO_KEY_EQUALS or key = ALLEGRO_KEY_PAD_PLUS then
            this.Clicked_Size_Increase;
        else
            Tool(this.all).Handle_Key_Typed( key, modifiers );
        end if;
    end Handle_Key_Typed;

end Tools.Patterns;
