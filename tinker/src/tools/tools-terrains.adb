--
-- Copyright (c) 2017-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Numerics;                      use Ada.Numerics;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Allegro.Color;                     use Allegro.Color;
with Commands;                          use Commands;
--with Debugging;                         use Debugging;
with Drawing;                           use Drawing;
with Drawing.Primitives;                use Drawing.Primitives;
with Drawing.Tiles;                     use Drawing.Tiles;
with Game_Views.Tinker;                 use Game_Views.Tinker;
with Icons;                             use Icons;
with Signals;                           use Signals;
with Tools.Painters;
with Values;                            use Values;
with Values.Lists;                      use Values.Lists;
with Values.Maps;                       use Values.Maps;
with Widgets.Buttons.Pushes;            use Widgets.Buttons.Pushes;
with Widgets.Labels;                    use Widgets.Labels;
with Widgets.Row_Layouts;               use Widgets.Row_Layouts;
with Widgets.Scenes;                    use Widgets.Scenes;

package body Tools.Terrains is

    package Connections is new Signals.Connections(Terrain_Brush);
    use Connections;

    ----------------------------------------------------------------------------

    type Float_Array is array (Integer range <>) of Float;

    -- Converts degrees to radians.
    --
    -- 0/360 degrees: Up
    --    90 degrees: Left
    --   180 degrees: Down
    --   270 degrees: Right
    function Rad( degrees : Float ) return Float is ((degrees - 180.0) * Pi / 180.0);

    ----------------------------------------------------------------------------

    -- Fuzzy tolerance for comparing floating point angles.
    FUZZ : constant Float := 0.09375;

    -- Compares two angles, using fuzzy floating point comparison.
    function Eqish( a, b : Float ) return Boolean is (b - FUZZ <= a and a <= b + FUZZ);

    ----------------------------------------------------------------------------

    -- These are the angles at which the path is allowed to turn; pairs of angles
    -- in radians. The angle is of each segment, assuming they intersect at the
    -- origin.
    VALID_ANGLES : constant Float_Array :=
    (
        -- co-linear
        -- note: these aren't actually corners once the segment is added; they
        -- are the angles of co-linear segments between the last path segment
        -- and the new path segment.
        Rad( 360.0 ),  Rad( 180.0 ),
        Rad(  90.0 ),  Rad( 270.0 ),
        Rad(  45.0 ),  Rad( 225.0 ),
        Rad( 135.0 ),  Rad( 315.0 ),
        Rad(  67.5 ),  Rad( 247.5 ),
        Rad( 112.5 ),  Rad( 292.5 ),

        -- horizontal and vertical corners
        Rad( 360.0 ),  Rad(  90.0 ),             --          __|
        Rad(  90.0 ),  Rad( 180.0 ),             --  ``|
        Rad( 180.0 ),  Rad( 270.0 ),             --            |``
        Rad( 270.0 ),  Rad( 360.0 ),             --    |__

        -- horizontal and 45 degrees
        Rad(  90.0 ),  Rad( 315.0 ),
        Rad(  90.0 ),  Rad( 225.0 ),
        Rad(  45.0 ),  Rad( 270.0 ),
        Rad( 135.0 ),  Rad( 270.0 ),

        -- horizontal and 22.5 degrees
        Rad(  90.0 ),  Rad( 292.5 ),
        Rad(  90.0 ),  Rad( 247.5 ),
        Rad(  67.5 ),  Rad( 270.0 ),
        Rad( 112.0 ),  Rad( 270.0 ),

        -- vertical and 45 degrees
        Rad( 135.0 ),  Rad( 360.0 ),
        Rad( 360.0 ),  Rad( 225.0 )
    );

    ----------------------------------------------------------------------------

    -- Returns a valid point, given requested point 'p2', with respect to the
    -- already existing segments in 'points'.
    procedure Constrain_Point( this    : not null access Terrain_Brush'Class;
                               p2      : in out Vec2;
                               allowed : out Boolean );

    -- Returns True if 'point' is within the 'map' area, inclusive of the right
    -- and bottom edges.
    function In_Map_Area_Inclusive( this  : not null access Terrain_Brush'Class;
                                    point : Vec2 ) return Boolean;

    -- Iterates through all tiles in all layers along the terrain's path.
    procedure Iterate_Tiles( this    : not null access Terrain_Brush'Class;
                             examine : not null access procedure( layer    : Integer;
                                                                  col, row : Integer;
                                                                  tileId   : Natural ) );

    -- Called when the Commit button is clicked.
    procedure On_Commit( this : not null access Terrain_Brush'Class );

    -- Called when the Undo button is clicked or the secondary action is applied.
    procedure On_Undo( this : not null access Terrain_Brush'Class );

    -- Updates the undo and commit button visibility and locations.
    procedure Update_Buttons( this : not null access Terrain_Brush'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Terrain_Brush( lib   : Library_Ptr;
                                   index : Natural ) return A_Tool is
        this : constant A_Terrain_Brush := new Terrain_Brush;
    begin
        this.Construct( lib, index );
        return A_Tool(this);
    end Create_Terrain_Brush;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this  : access Terrain_Brush;
                         lib   : Library_Ptr;
                         index : Natural ) is
    begin
        Tool(this.all).Construct( Terrain_Tool );
        this.lib := lib;
        this.index := index;
        this.definition := this.lib.Get.Get_Terrain( index );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Terrain_Brush ) is
    begin
        this.path.Clear;
        if this.btnUndo /= null then
            A_Scene(this.btnUndo.Get_Parent).Remove_Widget( this.btnUndo );
            A_Scene(this.btnCommit.Get_Parent).Remove_Widget( this.btnCommit );
            Delete( A_Widget(this.btnUndo) );
            Delete( A_Widget(this.btnCommit) );
        end if;
        Tool(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    overriding
    procedure Apply_To_Map( this      : access Terrain_Brush;
                            func      : Function_Type;
                            modifiers : Modifiers_Array;
                            first     : Boolean;
                            context   : Map_Tool_Context ) is
        pragma Unreferenced( modifiers );
        tileWidth : constant Float := Float(this.scene.Get_Tile_Width);
        point     : Vec2;
        angle     : Float;
        allowed   : Boolean;
    begin
        if not first then
            return;
        end if;

        case func is
            when Primary =>
                point := Vec2'(context.x, context.y);

                if this.path.Is_Empty then
                    -- constrain the first point to the grid and that's all
                    point := Vec2'(Float'Rounding( point.x / tileWidth ) * tileWidth,
                                   Float'Rounding( point.y / tileWidth ) * tileWidth);

                    if not this.In_Map_Area_Inclusive( point ) then
                        -- don't add the point
                        return;
                    end if;
                else
                    -- if at least one point as been added, a subsequent point
                    -- may only be added at certain angles relative to the
                    -- previous point.
                    this.Constrain_Point( point, allowed );
                    allowed := allowed and then this.In_Map_Area_Inclusive( point );

                    if not allowed or point = this.path.Last_Element then
                        -- don't add the point
                        return;
                    end if;
                end if;

                -- check if the new point continues the line segment between the
                -- last two points to avoid adding a new co-linear point.
                if Integer(this.path.Length) > 1 then
                    -- calculate the angle between the last two points
                    angle := Angle_Of( this.path.Element( Integer(this.path.Length) - 1 ),
                                       this.path.Element( Integer(this.path.Length) ) );
                    if angle = Angle_Of( this.path.Element( Integer(this.path.Length) ), point ) then
                        -- same angle as the previous segment; just update the last point
                        this.path.Replace_Element( Integer(this.path.Length), point );
                    else
                        -- add the N'th point
                        this.path.Append( point );
                    end if;
                else
                    -- add the first or second point
                    this.path.Append( point );
                end if;

                if this.btnUndo = null then
                    this.btnUndo := Create_Push_Button( this.view );
                    this.btnUndo.Set_Style( "terrainTool" );
                    this.btnUndo.Set_Icon( Create_Icon( "tinker:terrain-undo" ) );
                    this.btnUndo.Visible.Set( False );
                    this.btnUndo.Clicked.Connect( Slot( this, On_Undo'Access ) );
                    this.scene.Add_Widget( this.btnUndo );

                    this.btnCommit := Create_Push_Button( this.view );
                    this.btnCommit.Set_Style( "terrainTool" );
                    this.btnCommit.Set_Icon( Create_Icon( "tinker:terrain-commit" ) );
                    this.btnCommit.Visible.Set( False );
                    this.btnCommit.Clicked.Connect( Slot( this, On_Commit'Access ) );
                    this.scene.Add_Widget( this.btnCommit );
                end if;

                this.Update_Buttons;

            when Secondary =>
                if not this.path.Is_Empty then
                    -- delete the most recent path point
                    this.On_Undo;
                else
                    -- stop terrain building and fallback to the panner tool
                    this.view.Select_Tool( Panner_Tool );
                end if;

        end case;
    end Apply_To_Map;

    ----------------------------------------------------------------------------

    procedure Constrain_Point( this    : not null access Terrain_Brush'Class;
                               p2      : in out Vec2;
                               allowed : out Boolean ) is
        p1 : constant Vec2 := this.path.Last_Element;

        ------------------------------------------------------------------------

        -- Returns the point nearest to 'destination' that lies on a ray
        -- originating at 'start' and incremented by 'step'.
        function Nearest_Point( start, destination, step : Vec2 ) return Vec2 is
            nearest  : Vec2 := start;
            ray      : Vec2 := start;
            minDist  : Float := 1.0e9;
            prevDist : Float := 1.0e9;
            dist     : Float;
        begin
            loop
                dist := Length( destination - ray );
                if dist < minDist then
                    minDist := dist;
                    nearest := ray;
                end if;
                exit when dist > prevDist;
                prevDist := dist;
                ray := ray + step;
            end loop;
            return nearest;
        end Nearest_Point;

        ------------------------------------------------------------------------

        tileWidth : constant Float := Float(this.scene.Get_Tile_Width);
        xSign     : constant Float := (if p2.x >= p1.x then 1.0 else -1.0);
        ySign     : constant Float := (if p2.y >= p1.y then 1.0 else -1.0);
        nearest   : Vec2;
        result    : Vec2;
    begin
        result := p1;
        allowed := False;

        -- using ray casting, walk each of the valid angles from 'p1' to find
        -- the nearest valid point to 'p2'. valid angles are 0 degrees
        -- (horizontal), 90 degrees (vertical), 45 degrees (sleep slope) and
        -- 22.5 degrees (gentle slope).

        -- 0 degrees (horizontal)
        nearest := Nearest_Point( p1, p2, (tileWidth * xSign, 0.0) );
        if Length( nearest - p2 ) < Length( result - p2 ) then
            result := nearest;
            allowed := True;
        end if;

        -- 90 degrees (vertical)
        nearest := Nearest_Point( p1, p2, (0.0, tileWidth * ySign) );
        if Length( nearest - p2 ) < Length( result - p2 ) then
            result := nearest;
            allowed := True;
        end if;

        -- 45 degrees (steep slope)
        nearest := Nearest_Point( p1, p2, (tileWidth * xSign, tileWidth * ySign) );
        if Length( nearest - p2 ) < Length( result - p2 ) then
            result := nearest;
            allowed := True;
        end if;

        -- 22.5 degrees (gentle slope)
        nearest := Nearest_Point( p1, p2, (tileWidth * xSign * 2.0, tileWidth * ySign) );
        if Length( nearest - p2 ) < Length( result - p2 ) then
            result := nearest;
            allowed := True;
        end if;

        -- check if the line is at a valid angle with respect to the previous
        -- segment
        if allowed and then Integer(this.path.Length) >= 2 then
            declare
                a1 : constant Float := Angle_Of( this.path.Last_Element, this.path.Element( Integer(this.path.Length) - 1 ) );
                a2 : constant Float := Angle_Of( this.path.Last_Element, result );
                i  : Integer := VALID_ANGLES'First;
            begin
                allowed := False;
                while i < VALID_ANGLES'Last loop
                    if (Eqish( a1, VALID_ANGLES(i) ) and Eqish( a2, VALID_ANGLES(i + 1) )) or else
                       (Eqish( a2, VALID_ANGLES(i) ) and Eqish( a1, VALID_ANGLES(i + 1) ))
                    then
                        allowed := True;
                        exit;
                    end if;
                    i := i + 2;
                end loop;
            end;
        end if;

        -- check if the line intersects an existing segment
        if allowed and then Integer(this.path.Length) > 1 and then result /= this.path.Last_Element then
            declare
                newSeg : constant Segment := (this.path.Last_Element, result);
                ix     : Vec2;
                found  : Boolean;
            begin
                for i in 2..Integer(this.path.Length)-1 loop
                    Intersect_Full( newSeg, Segment'(this.path.Element( i - 1 ), this.path.Element( i )), ix, found );
                    if found then
                        -- if the new segment ends at the first point, don't count that as an intersection
                        if i > 2 or else newSeg.p2 /= this.path.Element( i - 1 ) then
                            allowed := False;
                            exit;
                        end if;
                    end if;
                end loop;
            end;
        end if;

        p2 := result;
    end Constrain_Point;

    ----------------------------------------------------------------------------

    overriding
    function Create_Options( this : access Terrain_Brush;
                             view : A_Game_View ) return A_Widget is
        row   : A_Row_Layout;
        label : A_Label;
    begin
        row := Create_Row_Layout( view );

            label := Create_Label( view );
            label.Set_Style( "terrainOptions" );
            label.Set_Icon( Create_Icon(  A_Tinker_View(view).Get_Scene.Get_Library,
                                          this.definition.icon ) );
            label.Set_Text( To_String( this.definition.name ) );
            row.Append( label );

        row.Set_Preferred_Width( A_Widget(row).Get_Min_Width );
        return A_Widget(row);
    end Create_Options;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Preview_Foreground( this      : access Terrain_Brush;
                                       modifiers : Modifiers_Array;
                                       context   : Tool_Preview_Context ) is
        tileWidth : constant Float := Float(this.scene.Get_Tile_Width);

        ------------------------------------------------------------------------

        procedure Draw_Tile( layer    : Integer;
                             col, row : Integer;
                             tileId   : Natural ) is
            pragma Unreferenced( layer );
            pragma Unreferenced( tileId );
            x : Float := Float(col) * tileWidth;
            y : Float := Float(row) * tileWidth;
        begin
            To_Target_Pixel_Rounded( x, y );
            Rectfill_XYWH( x, y, tileWidth, tileWidth,
                           Tools.Painters.DRAW_TINT );
        end Draw_Tile;

        ------------------------------------------------------------------------

        pragma Unreferenced( modifiers );
        thickness : constant Float := From_Target_Pixels( 2.0 );
        p1, p2    : Vec2;
        allowed   : Boolean;
    begin
        this.Iterate_Tiles( Draw_Tile'Access );

        -- draw the path segment lines over the previewed tiles
        for i in 2..Integer(this.path.Length) loop
            p1 := this.path.Element( i - 1 );
            p2 := this.path.Element( i );
            Line( p1.x, p1.y, p2.x, p2.y, Al_Map_RGB( 255, 255, 255 ), thickness );
        end loop;

        -- draw a preview of the next line segment
        if not this.path.Is_Empty and this.scene.Is_Mouse_In_Viewport and this.scene.Is_Hovered then
            p1 := this.path.Last_Element;
            p2 := Vec2'(context.x, context.y);

            this.Constrain_Point( p2, allowed );
            allowed := allowed and then this.In_Map_Area_Inclusive( p2 );

            Line( p1.x, p1.y, p2.x, p2.y,
                  (if allowed then Al_Map_RGB( 212, 128, 212 ) else Al_Map_RGB( 255, 0, 0 )),
                  thickness );
        end if;
    end Draw_Preview_Foreground;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Preview_Layer( this      : access Terrain_Brush;
                                  modifiers : Modifiers_Array;
                                  context   : Tool_Preview_Context ) is
        tileWidth : constant Float := Float(this.scene.Get_Tile_Width);

        ------------------------------------------------------------------------

        procedure Draw_Tile( layer    : Integer;
                             col, row : Integer;
                             tileId   : Natural ) is
            pragma Unreferenced( layer );
            x : Float := Float(col) * tileWidth;
            y : Float := Float(row) * tileWidth;
        begin
            To_Target_Pixel_Rounded( x, y );
            Draw_Tile_Filled( context.mapLib.Get.Get_Tile( tileId ),
                              x, y, 0.0,
                              tileWidth, tileWidth );
        end Draw_Tile;

        ------------------------------------------------------------------------

        pragma Unreferenced( modifiers );
    begin
        this.Iterate_Tiles( Draw_Tile'Access );
    end Draw_Preview_Layer;

    ----------------------------------------------------------------------------

    function Get_Terrain_Index( this : not null access Terrain_Brush'Class ) return Natural is (this.index);

    ----------------------------------------------------------------------------

    function In_Map_Area_Inclusive( this  : not null access Terrain_Brush'Class;
                                    point : Vec2 ) return Boolean is
        tileWidth : constant Float := Float(this.scene.Get_Tile_Width);
        map       : constant A_Map := this.scene.Get_Map;
    begin
        return point.x >= 0.0 and then point.y >= 0.0 and then
               Float'Floor( point.x ) <= Float(map.Get_Width) * tileWidth and then
               Float'Floor( point.y ) <= Float(map.Get_Height) * tileWidth;
    end In_Map_Area_Inclusive;

    ----------------------------------------------------------------------------

    overriding
    function Is_Busy( this : access Terrain_Brush ) return Boolean is (not this.path.Is_Empty);

    ----------------------------------------------------------------------------

    procedure Iterate_Tiles( this    : not null access Terrain_Brush'Class;
                             examine : not null access procedure( layer    : Integer;
                                                                  col, row : Integer;
                                                                  tileId   : Natural ) ) is
        tileWidth : constant Float := Float(this.scene.Get_Tile_Width);

        ------------------------------------------------------------------------

        procedure Examine_Segment( a, b : Vec2; skipFirst, skipLast : Boolean ) is
            n      : constant Vec2 := Normal( b - a );
            angle  : constant Float := Angle_Of( a, b );
            t2     : constant Vec2 := b;
            t1     : Vec2 := a;
            pos    : Natural := 0;      -- position, in tiles
            slope  : Float := 0.0;
            center : Vec2;
            row,
            col    : Natural := 0;
            slice  : A_Terrain_Slice;
        begin
            -- calculate the slope for non-vertical segments
            if b.x /= a.x then
                slope := (b.y - a.y) / (b.x - a.x);
            end if;

            -- pre-increment the position along the segment. we skip the
            -- first tile width if 'skipFirst' is enabled because the corner
            -- with the preceding segment already determined this slice
            if skipFirst then
                pos := 1;
            end if;

            loop
                if a.x /= b.x then
                    -- stepping horizontally or at an angle
                    t1.x := a.x + Float(pos) * tileWidth * Float'Copy_Sign( 1.0, b.x - a.x );
                    t1.y := a.y + Float(pos) * tileWidth * slope * Float'Copy_Sign( 1.0, b.x - a.x );
                else
                    -- stepping vertically
                    t1.y := a.y + Float(pos) * tileWidth * Float'Copy_Sign( 1.0, b.y - a.y );
                end if;

                -- exit the loop one tile width before the end
                if skipLast then
                    exit when a.x /= b.x and abs (t2.x - t1.x) <= tileWidth;  -- horizontal floors and slopes
                    exit when a.x = b.x and abs (t2.y - t1.y) <= tileWidth;   -- vertical walls
                else
                    exit when a.x /= b.x and t2.x = t1.x;                     -- horizontal floors and slopes
                    exit when a.x = b.x and t2.y = t1.y;                      -- vertical walls
                end if;

                --if Eqish( slope, 0.0 ) then
                --    shape := Wall;
                --elsif Eqish( slope, 1.0 ) then
                --    shape := (if n.y < 0.0 then Slope_45_Down_Ceiling else Slope_45_Down_Floor);
                --elsif Eqish( slope, -1.0 ) then
                --    shape := (if n.y < 0.0 then Slope_45_Up_Ceiling else Slope_45_Up_Floor);
                --elsif Eqish( slope, 0.5 ) then
                --    if n.y < 0.0 then
                --        shape := (if pos mod 2 = 0 then Slope_22_Down_Ceiling_Wide else Slope_22_Down_Ceiling_Thin);
                --    else
                --        shape := (if pos mod 2 = 0 then Slope_22_Down_Floor_Wide else Slope_22_Down_Floor_Thin);
                --    end if;
                --elsif Eqish( slope, -0.5 ) then
                --    if n.y < 0.0 then
                --        shape := (if pos mod 2 = 0 then Slope_22_Up_Ceiling_Thin else Slope_22_Up_Ceiling_Wide);
                --    else
                --        shape := (if pos mod 2 = 0 then Slope_22_Up_Floor_Thin else Slope_22_Up_Floor_Wide);
                --    end if;
                --else
                --    shape := Wall;
                --end if;

                -- calculate the 'center' of the tile, used to determine which
                -- tile col,row will be affected by this segment. it's found by
                -- walking the t1 -> t2 segment for 1/2 of a tile width, then
                -- walking off it perpendicularly in the direction of the normal
                -- for a very short distance. this will choose the tile that
                -- lies along the line segment in the direction of the normal.
                center := t1 + (Normalize( t2 - t1 ) * (tileWidth / 2.0)) + n;
                center := center / tileWidth;

                col := Integer(Float'Floor( center.x ));
                row := Integer(Float'Floor( center.y ));

                -- examine the tiles from the terrain definition
                for edge of this.definition.edges loop
                    if Eqish( angle, edge.angle ) then
                       slice := edge.slices.Element( pos mod Integer(edge.slices.Length) );
                        for j in slice'Range loop
                            if a.x /= b.x then
                                examine.all( slice(j).layer,
                                             col, row + slice(j).offsetX,
                                             slice(j).tileId );
                            elsif a.y /= b.y then
                                examine.all( slice(j).layer,
                                             col + slice(j).offsetX, row,
                                             slice(j).tileId );
                            end if;
                        end loop;
                        exit;
                    end if;
                end loop;

                pos := pos + 1;
            end loop;
        end Examine_Segment;

        ------------------------------------------------------------------------

        procedure Examine_Corner( a, b, c : Vec2 ) is
            angle1 : constant Float := Angle_Of( a, b );
            angle2 : constant Float := Angle_Of( b, c );
        begin
            for corner of this.definition.corners loop
                if Eqish( angle1, corner.angle1 ) and Eqish( angle2, corner.angle2 ) then
                    for slice of corner.slices loop
                        for j in slice'Range loop
                            examine.all( slice(j).layer,
                                         Integer(b.x / tileWidth) + slice(j).offsetX + (if slice(j).offsetX > 0 then -1 else 0),
                                         Integer(b.y / tileWidth) + slice(j).offsetY + (if slice(j).offsetY > 0 then -1 else 0),
                                         slice(j).tileId );
                        end loop;
                    end loop;
                    exit;
                end if;
            end loop;
        end Examine_Corner;

        ------------------------------------------------------------------------

        p1, p2, p3 : Vec2;
    begin
        -- step along the path, handling one slice at a time until reaching
        -- a corner, then handling the corner as a whole (two slices of path).
        for i in 2..Integer(this.path.Length) loop
            p1 := this.path.Element( i - 1 );
            p2 := this.path.Element( i );
            Examine_Segment( p1, p2,
                             skipFirst => (2 < i),
                             skipLast  => (i < Integer(this.path.Length)) );
        end loop;
        for i in 3..Integer(this.path.Length) loop
            p1 := this.path.Element( i - 2 );
            p2 := this.path.Element( i - 1 );
            p3 := this.path.Element( i );
            Examine_Corner( p1, p2, p3 );
        end loop;
    end Iterate_Tiles;

    ----------------------------------------------------------------------------

    procedure On_Commit( this : not null access Terrain_Brush'Class ) is
        view : constant A_Tinker_View := A_Tinker_View(this.btnCommit.Get_View);

        procedure Set_Tile( layer    : Integer;
                            col, row : Integer;
                            tileId   : Natural ) is
        begin
            -- the Set_Tile commands automatically combine together
            view.Commands.Execute( Create_Command_Set_Tile( layer, col, row,
                                                            this.scene.Get_Map.Get_Tile_Id( layer, col, row ),
                                                            tileId ) );
        end Set_Tile;

    begin
        this.Iterate_Tiles( Set_Tile'Access );
        view.Commands.Prevent_Combine;

        this.path.Clear;
        this.btnUndo.Visible.Set( False );
        this.btnCommit.Visible.Set( False );
    end On_Commit;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Unselected( this : access Terrain_Brush ) is
    begin
        this.path.Clear;

        if this.btnUndo /= null then
            this.btnUndo.Visible.Set( False );
            this.btnCommit.Visible.Set( False );
        end if;
    end On_Unselected;

    ----------------------------------------------------------------------------

    procedure On_Undo( this : not null access Terrain_Brush'Class ) is
    begin
        if not this.path.Is_Empty then
            this.path.Delete_Last;
        end if;

        if this.btnUndo /= null then
            this.Update_Buttons;
        end if;
    end On_Undo;

    ----------------------------------------------------------------------------

    procedure Update_Buttons( this : not null access Terrain_Brush'Class ) is
        halfWidth : constant Float := this.btnUndo.Get_Preferred_Width / 2.0;
        a, b, c   : Vec2;
    begin
        if Integer(this.path.Length) = 1 then
            -- center the undo button under the point
            this.btnUndo.X.Set( this.path.Last_Element.x - halfWidth );
            this.btnUndo.Y.Set( this.path.Last_Element.y + 8.0 );

        elsif Integer(this.path.Length) > 1 then
            -- locate the buttons a short distance back along the preceding
            -- segment so they won't interfere with the next segment

            -- The last segment runs from A -> B
            a := this.path.Element( this.path.Last_Index - 1 );
            b := this.path.Element( this.path.Last_Index     );

            -- B is now a point several pixels back along the segment from A <- B
            b := b + Normalize(a - b) * (8.0 + halfWidth);

            -- C is now near the A -> B segment, but (8 + halfWidth) pixels away
            -- from B in the Normal's perpendicular direction. this will be the
            -- center of the commit button.
            c := b + Normal(a - b) * (8.0 + halfWidth);
            this.btnCommit.X.Set( c.x - halfWidth );
            this.btnCommit.Y.Set( c.y - halfWidth );

            -- C is now near the A -> B segment, but (8 + halfWidth) pixels away
            -- from B in the opposite of the Normal's perpendicular direction.
            -- this will be the center of the undo button.
            c := b - Normal(a - b) * (8.0 + halfWidth);
            this.btnUndo.X.Set( c.x - halfWidth );
            this.btnUndo.Y.Set( c.y - halfWidth );
        end if;

        this.btnUndo.Visible.Set( Integer(this.path.Length) > 0 );
        this.btnCommit.Visible.Set( Integer(this.path.Length) > 1 );
    end Update_Buttons;

end Tools.Terrains;
