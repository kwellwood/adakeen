--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Maps;                              use Maps;
with Widgets.Input_Boxes;               use Widgets.Input_Boxes;

package Tools.Patterns is

    type Patternbrush is new Tool with private;
    type A_Patternbrush is access all Patternbrush'Class;

    function Create_Patternbrush( pattern  : not null A_Map;
                                  offsetX,
                                  offsetY : Integer ) return A_Tool;
    pragma Postcondition( Create_Patternbrush'Result /= null );

    overriding
    function Acts_On( this : access Patternbrush; target : Subject_Type ) return Boolean is (target = Map_Subject);

    procedure Apply_To_Map( this      : access Patternbrush;
                            func      : Function_Type;
                            modifiers : Modifiers_Array;
                            first     : Boolean;
                            context   : Map_Tool_Context );

    function Create_Options( this : access Patternbrush;
                             view : A_Game_View ) return A_Widget;

    procedure Draw_Preview_Foreground( this      : access Patternbrush;
                                       modifiers : Modifiers_Array;
                                       context   : Tool_Preview_Context );

    procedure Draw_Preview_Layer( this      : access Patternbrush;
                                  modifiers : Modifiers_Array;
                                  context   : Tool_Preview_Context );

    overriding
    function Get_Cursor( this : access Patternbrush ) return String is ("tinker:cursor-pencil-pattern");

    procedure On_Selected( this : access Patternbrush );

    procedure Post_Apply_Map( this      : access Patternbrush;
                              func      : Function_Type;
                              modifiers : Modifiers_Array;
                              context   : Map_Tool_Context );

    procedure Handle_Key_Typed( this      : access Patternbrush;
                                key       : Integer;
                                modifiers : Modifiers_Array );

private

    MAX_SIZE : constant := 63;

    type Patternbrush is new Tool with
        record
            pattern    : A_Map := null;
            startTileX,
            startTileY : Integer := -1;
            lastX,
            lastY      : Integer := -1;
            size       : Natural := 1;
            sizeInput  : A_Input_Box := null;
            offsetX,
            offsetY    : Integer := 0;
            useOffset  : Boolean := False;
        end record;

    procedure Construct( this    : access Patternbrush;
                         pattern : A_Map;
                         offsetX,
                         offsetY : Integer );

    procedure Delete( this : in out Patternbrush );

end Tools.Patterns;
