--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Color;                     use Allegro.Color;
with Widgets.Input_Boxes;               use Widgets.Input_Boxes;

package Tools.Painters is

    type Painter is new Tool with private;
    type A_Painter is access all Painter'Class;

    function Create_Painter( tileId : Natural ) return A_Tool;
    pragma Postcondition( Create_Painter'Result /= null );

    overriding
    function Acts_On( this : access Painter; target : Subject_Type ) return Boolean is (target = Map_Subject);

    procedure Apply_To_Map( this      : access Painter;
                            func      : Function_Type;
                            modifiers : Modifiers_Array;
                            first     : Boolean;
                            context   : Map_Tool_Context );

    function Create_Options( this : access Painter;
                             view : A_Game_View ) return A_Widget;

    procedure Draw_Preview_Foreground( this      : access Painter;
                                       modifiers : Modifiers_Array;
                                       context   : Tool_Preview_Context );

    procedure Draw_Preview_Layer( this      : access Painter;
                                  modifiers : Modifiers_Array;
                                  context   : Tool_Preview_Context );

    overriding
    function Get_Cursor( this : access Painter ) return String is ("tinker:cursor-painter");

    function Get_Tile_Id( this : not null access Painter'Class ) return Natural;

    procedure Handle_Key_Typed( this      : access Painter;
                                key       : Integer;
                                modifiers : Modifiers_Array );

    procedure On_Selected( this : access Painter );

    procedure Post_Apply_Map( this      : access Painter;
                              func      : Function_Type;
                              modifiers : Modifiers_Array;
                              context   : Map_Tool_Context );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    TILE_OPACITY : constant := 0.8;

    function DRAW_TINT  return Allegro_Color is (Al_Map_RGBA( 10, 47, 157, 51 ));
    function ERASE_TINT return Allegro_Color is (Al_Map_RGBA( 157, 37, 10, 127 ));

private

    MAX_SIZE : constant := 31;

    type Painter is new Tool with
        record
            tileId       : Natural := 0;
            lastX, lastY : Integer := -1;
            size         : Natural := 1;
            sizeInput    : A_Input_Box := null;
        end record;

    procedure Construct( this : access Painter; tileId : Natural );

end Tools.Painters;
