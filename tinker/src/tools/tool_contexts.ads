--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Assets.Libraries;                  use Assets.Libraries;
with Scene_Entities;                    use Scene_Entities;

package Tool_Contexts is

    type Subject_Type is (Map_Subject, Entity_Subject);

    type Function_Type is (Primary, Secondary);

    -- Provided to the tool when it is applied to an entity, to encapsulate all
    -- the information necessary for the tool to operate.
    type Entity_Tool_Context is
        record
            entity : A_Entity;      -- entity being operated on
            x, y   : Float;         -- x,y of mouse in world units
        end record;

    -- Provided to the tool when it is applied to the map, to encapsulate all
    -- the information necessary for the tool to operate.
    type Map_Tool_Context is
        record
            mapLib : Library_Ptr;   -- world map's tile library
            x, y   : Float;         -- x,y of mouse in world units
            layer  : Integer;       -- actve map layer
        end record;

    subtype Tool_Preview_Context is Map_Tool_Context;

end Tool_Contexts;
