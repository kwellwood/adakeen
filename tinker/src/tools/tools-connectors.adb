--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Commands;                          use Commands;
--with Debugging;                         use Debugging;
with Drawing.Primitives;                use Drawing.Primitives;
with Game_Views.Tinker;                 use Game_Views.Tinker;
with Palette.Tango;                     use Palette.Tango;
with Scene_Entities;                    use Scene_Entities;
with Values;                            use Values;
with Values.Lists;                      use Values.Lists;
with Widgets.Scenes;                    use Widgets.Scenes;

package body Tools.Connectors is

    function Create_Connector return A_Tool is
        this : constant A_Tool := new Connector;
    begin
        this.Construct( Connector_Tool );
        return this;
    end Create_Connector;

    ----------------------------------------------------------------------------

    overriding
    procedure Apply_To_Entity( this      : access Connector;
                               func      : Function_Type;
                               modifiers : Modifiers_Array;
                               first     : Boolean;
                               context   : Entity_Tool_Context ) is
        cmd         : A_Compound_Command;
        connections : List_Value;
    begin
        if not first then
            return;
        end if;

        if func = Primary then

            if context.entity = null then
                return;        -- didn't click on an entity
            end if;

            -- - - - start connection - - - --
            if not this.Is_Busy then
                if modifiers(CTRL) then
                    cmd := Create_Compound_Command;

                    -- delete all outgoing connections from this entity
                    connections := context.entity.Get_Attribute( "connections" ).Lst;
                    if connections.Is_List then
                        for i in 1..connections.Length loop
                            cmd.Add( Create_Command_Connect( context.entity.Get_Entity_Id,
                                                             Cast_Tagged_Id( connections.Get( i ) ),
                                                             connect => False ) );
                        end loop;
                    end if;

                    -- delete all incoming connections to this entity
                    declare
                        procedure Examine( entity : A_Entity ) is
                        begin
                            connections := entity.Get_Attribute( "connections" ).Lst;
                            if connections.Is_List then
                                for i in 1..connections.Length loop
                                    if Cast_Tagged_Id( connections.Get( i ) ) = context.entity.Get_Entity_Id then
                                        cmd.Add( Create_Command_Connect( entity.Get_Entity_Id,
                                                                         context.entity.Get_Entity_Id,
                                                                         connect => False ) );
                                    end if;
                                end loop;
                            end if;
                        end Examine;
                    begin
                        this.scene.Iterate_Entities( Examine'Access );
                    end;

                    if not cmd.Is_Empty then
                        this.view.Commands.Execute( cmd );
                    else
                        Delete( A_Command(cmd) );
                    end if;
                else
                    -- first entity clicked
                    this.fromId := context.entity.Get_Entity_Id;
                    context.entity.Get_Widget.Set_Mouse_Cursor( this.Get_Cursor );
                end if;

            -- - - - complete connection - - - --
            elsif context.entity.Get_Entity_Id /= this.fromId then
                if modifiers(CTRL) then
                    -- create a two-way connection if CTRL is pressed on the
                    -- second entity
                    cmd := Create_Compound_Command;
                    cmd.Add( Create_Command_Connect( this.fromId, context.entity.Get_Entity_Id, connect => True ) );
                    cmd.Add( Create_Command_Connect( context.entity.Get_Entity_Id, this.fromId, connect => True ) );
                    this.view.Commands.Execute( cmd );
                else
                    this.view.Commands.Execute( Create_Command_Connect( this.fromId, context.entity.Get_Entity_Id, connect => True ) );
                end if;
                this.fromId := NULL_ID;
                this.scene.Find_Entity( context.entity.Get_Entity_Id ).Get_Widget.Set_Mouse_Cursor( this.Get_Cursor );
            end if;

        elsif func = Secondary then

            if this.Is_Busy then
                -- unselect first entity
                this.fromId := NULL_ID;
                if context.entity /= null then
                    context.entity.Get_Widget.Set_Mouse_Cursor( this.Get_Cursor );
                end if;
            else
                -- stop connecting and fallback to the pointer tool
                this.view.Select_Tool( Pointer_Tool );
            end if;

        end if;
    end Apply_To_Entity;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Preview_Foreground( this      : access Connector;
                                       modifiers : Modifiers_Array;
                                       context   : Tool_Preview_Context ) is
        pragma Unreferenced( modifiers );
        entity : A_Entity;
    begin
        -- Draw a line between the mouse cursor and the tool's attached entity
        -- (used by the connector tool)
        entity := this.scene.Find_Entity( this.fromId );
        if entity /= null then
            Pixel_Line( entity.Get_Widget.Get_Center_X, entity.Get_Widget.Get_Center_Y,
                        context.x, context.y,
                        Scarlet1,
                        2.0 );
        end if;
    end Draw_Preview_Foreground;

    ----------------------------------------------------------------------------

    overriding
    function Is_Busy( this : access Connector ) return Boolean is (this.fromId /= NULL_ID);

    ----------------------------------------------------------------------------

    overriding
    procedure On_Selected( this : access Connector ) is
    begin
        this.fromId := NULL_ID;
    end On_Selected;

end Tools.Connectors;
