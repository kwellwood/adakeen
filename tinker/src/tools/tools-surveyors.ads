--
-- Copyright (c) 2017-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Maps;                              use Maps;
with Vector_Math;                       use Vector_Math;
with Widgets.Buttons;                   use Widgets.Buttons;

package Tools.Surveyors is

    -- The Surveyor tool is a tile selection manipulator. It can select/unselect
    -- an area of tiles, and copy, paste, move or delete tiles in the selected
    -- area. Only one rectangular area can be selected at a tile. Clicking and
    -- dragging on the map within the selected area moves the tiles within the
    -- selection. Clicking outside the selected area begins creating a new
    -- selection.
    --
    -- When a tile selection exists, all tile-based tools will be constrained to
    -- the selected area. Clearing the tile selection removes the constraint.
    --
    -- Operates on the map only. Does not interact with entities.
    --
    -- Options:
    --   All Layers: (Boolean)
    --     When True (default), move, copy and delete operations apply to tiles
    --     within the selected area on all layers simultaneously. When False,
    --     move, copy and delete operations only apply to tiles on the active
    --     layer.
    --
    -- Primary function:
    --   Clicking on the map (outside of an existing selection) begins selecting
    --   a rectangular area of tiles until the primary function button is
    --   released.
    --
    --   If a selection already exists and the click occurs within the selected
    --   area, then the selection will begin to move with the mouse. The
    --   following modifiers, when held during the click, determine the drag
    --   behavior:
    --
    --     NONE: The selected area and the tiles within it will move. The
    --           "All Layers" option determines whether tiles in the active
    --           layer or all layers will be affected by the move.
    --     CTRL: Only the selected area will move, without affecting any tiles.
    --
    --     Area movement will end when the primary function button is released.
    --
    -- Secondary function:
    --   If a tile selection exists, the selection will be cleared.
    --
    --   Otherwise, if no selection exists, it switches to the Panner tool as
    --   part of the cycle: Panner -> Pointer -> Surveyor -> (repeat)
    --
    -- Copy function:
    --   All currently visible non-zero tiles within the selected area will be
    --   copied to the clipboard. If "All Layers" is True, the tiles in all
    --   currently visible layers will be copied. Otherwise, only tiles in the
    --   active layer (if visible) will be copied. When tiles are copied from
    --   the active layer only, they can be pasted into any other layer. However,
    --   when tiles are copied from all layers simultaneously, the paste will
    --   affect all correspondingly numbered layers in the destination map.
    --
    -- Paste function:
    --   Tiles can be copied within a map or between maps that share the same
    --   tile library. Tiles that were copied from a single layer will be pasted
    --   into the active layer. Tiles that were copied from all layers will be
    --   pasted into matching layer numbers. Layers that are not visible at the
    --   time of the paste operation will not be affected.
    --
    --   Like the Pointer tool pastes entities, the Surveyor tool will paste
    --   tiles in their original (copied) location, or as close to it as the
    --   current map size and scene viewport will allow, keeping the pasted area
    --   both within the map and within the scene's current viewport.
    --
    --   Empty tiles (tile id 0) are not pasted. Tiles will only be overwritten
    --   by existing (non-zero) tiles in the pasted selection.
    --
    -- Delete function:
    --   Tiles within the selected area will be erased (cleared to tile id 0).
    --   If "All Layers" is True, the tiles in all layers will be erased.
    --   Otherwise, only tiles in the active layer will be erased.
    --
    type Surveyor is new Tool with private;

    function Create_Surveyor return A_Tool;
    pragma Postcondition( Create_Surveyor'Result /= null );

    overriding
    function Acts_On( this : access Surveyor; target : Subject_Type ) return Boolean is (target = Map_Subject);

    -- Clears a selection, starts a selection, or drags a selection.
    procedure Apply_To_Map( this      : access Surveyor;
                            func      : Function_Type;
                            modifiers : Modifiers_Array;
                            first     : Boolean;
                            context   : Map_Tool_Context );

    -- Completes the current operation.
    procedure Post_Apply_Map( this      : access Surveyor;
                              func      : Function_Type;
                              modifiers : Modifiers_Array;
                              context   : Map_Tool_Context );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    function Can_Clear( this : access Surveyor ) return Boolean;

    function Can_Copy( this : access Surveyor ) return Boolean;

    function Can_Delete( this : access Surveyor ) return Boolean;

    function Can_Paste( this : access Surveyor ) return Boolean;

    procedure Do_Clear( this : access Surveyor );

    procedure Do_Copy( this : access Surveyor );

    procedure Do_Delete( this : access Surveyor );

    procedure Do_Paste( this : access Surveyor );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Creates and returns an options widget to display on the toolbar.
    function Create_Options( this : access Surveyor;
                             view : A_Game_View ) return A_Widget;

    overriding
    function Get_Cursor( this : access Surveyor ) return String is ("ARROW");

    procedure On_Selected( this : access Surveyor );

private

    type Surveyor is new Tool with
        record
            oldArea     : Rectangle;
            creating    : Boolean := False;
            dragging    : Boolean := False;
            dragContent : Boolean := False;
            dragX,
            dragY       : Float := -1.0;
            content     : A_Map := null;       -- selected content
            previous    : A_Map := null;       -- previous content where the selection is

            btnAllLayers : A_Button;
        end record;

    procedure Delete( this : in out Surveyor );

    -- Updates '.content' and '.previous' caches based on the current tile
    -- selection. tileArea' is the currently selected area (in world units) and
    -- 'tileWidth' is the width of a tile in world units.
    procedure Update_Cache( this      : not null access Surveyor'Class;
                            map       : not null A_Map;
                            area      : Rectangle;
                            tileWidth : Float );

end Tools.Surveyors;
