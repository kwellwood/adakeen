--
-- Copyright (c) 2014-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Widgets.Buttons;                   use Widgets.Buttons;
with Widgets.Input_Boxes;               use Widgets.Input_Boxes;

package Tools.Erasers is

    type Eraser is new Tool with private;
    type A_Eraser is access all Eraser'Class;

    function Create_Eraser return A_Tool;
    pragma Postcondition( Create_Eraser'Result /= null );

    overriding
    function Acts_On( this : access Eraser; target : Subject_Type ) return Boolean is (target = Map_Subject);

    procedure Apply_To_Map( this      : access Eraser;
                            func      : Function_Type;
                            modifiers : Modifiers_Array;
                            first     : Boolean;
                            context   : Map_Tool_Context );

    function Create_Options( this : access Eraser;
                             view : A_Game_View ) return A_Widget;

    procedure Draw_Preview_Foreground( this      : access Eraser;
                                       modifiers : Modifiers_Array;
                                       context   : Tool_Preview_Context );

    overriding
    function Get_Cursor( this : access Eraser ) return String is ("tinker:cursor-eraser");

    procedure On_Selected( this : access Eraser );

    procedure Post_Apply_Map( this      : access Eraser;
                              func      : Function_Type;
                              modifiers : Modifiers_Array;
                              context   : Map_Tool_Context );

    procedure Handle_Key_Typed( this      : access Eraser;
                                key       : Integer;
                                modifiers : Modifiers_Array );

private

    MAX_SIZE : constant := 31;

    type Eraser is new Tool with
        record
            lastX, lastY : Integer := -1;    -- last erased in tile coordinates
            size         : Natural := 1;
            sizeInput    : A_Input_Box := null;
            btnAllLayers : A_Button := null;
        end record;

end Tools.Erasers;
