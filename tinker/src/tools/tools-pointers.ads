--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Directions;                        use Directions;

package Tools.Pointers is

    -- The Pointer tool is an entity and entity selection manipulator. It can
    -- select/unselect entities, copy, paste, move or delete the entity selection,
    -- sand it can resize individual entities. Clicking and dragging on the map
    -- begins drawing an area to modify the selected set.
    --
    -- Operates on both entities and the map.
    --
    -- Primary function:
    --   When clicking on the map, begins marking an entity selection area. The
    --   entity selection area will be active as long as the primary function
    --   button is held. It will interact with entities touching it based on the
    --   following modifiers when the click occurs:
    --
    --     NONE : the selection will be cleared and then will select entities (RESET).
    --     SHIFT: the selection area will select entities (OR).
    --     CTRL : the selection area will toggle the selection state of entities (XOR).
    --
    --   When clicking on an entity, first the selected set of entities will be
    --   affected based on the following modifiers when the click occurs:
    --
    --     NONE : if the entity is unselected, the selection will be cleared and
    --            only the clicked entity will be selected (RESET).
    --     SHIFT: the clicked entity will be selected (OR).
    --     CTRL : the selection state of the clicked entity will be toggled (XOR).
    --
    --   After the entity selection has been updated, either movement or resizing
    --   also occurs. If the entity is resizable and the click occured within a
    --   resizing handle, then the clicked entity will begin resizing. Otherwise,
    --   all selected entities will begin dragging. Both resizing and movement
    --   end when the primary function button is released.
    --
    -- Secondary function:
    --   If the selected set is not empty, the selection will be cleared.
    --
    --   Otherwise, if the selected set is empty, it switches to the Surveyor
    --   tool as part of the cycle:
    --   Panner -> Pointer -> Surveyor -> (repeat)
    --
    -- Copy function:
    --   All selected entities will be copied to the clipboard.
    --
    -- Paste function:
    --   If the clipboard contains one or more entities, they will be pasted to
    --   the map in their original (copied) location, or as close to it as the
    --   current map size and scene viewport will allow, keeping entities both
    --   within the map and within the scene's current viewport.
    --
    --   When pasting multiple entities, they will maintain their original
    --   relative spacing to the extent that the map size allows, and the group
    --   will be pasted within the scene's viewport as much as possible.
    --
    -- Delete function:
    --   All deletable entities in the selection will be deleted. Any undeleted
    --   entities will remain selected.
    --
    type Pointer is new Tool with private;
    type A_Pointer is access all Pointer'Class;

    function Create_Pointer return A_Tool;
    pragma Postcondition( Create_Pointer'Result /= null );

    overriding
    function Acts_On( this : access Pointer; target : Subject_Type ) return Boolean is (True);

    -- Selects, moves or resizes an entity.
    procedure Apply_To_Entity( this      : access Pointer;
                               func      : Function_Type;
                               modifiers : Modifiers_Array;
                               first     : Boolean;
                               context   : Entity_Tool_Context );

    -- Selects entities within an area.
    procedure Apply_To_Map( this      : access Pointer;
                            func      : Function_Type;
                            modifiers : Modifiers_Array;
                            first     : Boolean;
                            context   : Map_Tool_Context );

    procedure Post_Apply_Entity( this      : access Pointer;
                                 func      : Function_Type;
                                 modifiers : Modifiers_Array;
                                 context   : Entity_Tool_Context );

    procedure Post_Apply_Map( this      : access Pointer;
                              func      : Function_Type;
                              modifiers : Modifiers_Array;
                              context   : Map_Tool_Context );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    function Can_Copy( this : access Pointer ) return Boolean;

    function Can_Delete( this : access Pointer ) return Boolean;

    function Can_Paste( this : access Pointer ) return Boolean;

    -- Copies all selected entities to the clipboard.
    procedure Do_Copy( this : access Pointer );

    -- Deletes all selected entities.
    procedure Do_Delete( this : access Pointer );

    -- Pastes entities from the clipboard.
    procedure Do_Paste( this : access Pointer );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    overriding
    function Get_Cursor( this : access Pointer ) return String is ("ARROW");

    ----------------------------------------------------------------------------

    GRAB_BORDER_SIZE : constant := 4.0;

private

    type Drag_Action is (Drag_Move, Drag_Resize);

    type Pointer is new Tool with
        record
            applyX,                            -- location in entity widget where tool was first applied
            applyY      : Float := -1.0;       --
            dragAction  : Drag_Action := Drag_Move;

            -- cached tool parameters
            minEWidth       : Float := 0.0;
            minEHeight      : Float := 0.0;
            resizableWidth  : Boolean := False;
            resizableHeight : Boolean := False;
            snapCenterX     : Boolean := False;
            snapCenterY     : Boolean := False;
            snapEnabled     : Boolean := False;
            placeOffsetX    : Float := 0.0;
            placeOffsetY    : Float := 0.0;

            moveFromX,                         -- location of entity widget when move started
            moveFromY   : Float := -1.0;       --
            movedByDrag : Boolean := False;    -- entity actually moved by drag?

            resizeDir   : Direction_Type;      -- edge or corner of entity being dragged to resize
            resizeFromW : Float := 0.0;
            resizeFromH : Float := 0.0;
        end record;

end Tools.Pointers;
