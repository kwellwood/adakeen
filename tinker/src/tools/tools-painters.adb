--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Keyboard;                  use Allegro.Keyboard;
with Assets.Libraries;                  use Assets.Libraries;
with Clipping;                          use Clipping;
with Commands;                          use Commands;
--with Debugging;                         use Debugging;
with Drawing;                           use Drawing;
with Drawing.Bitmaps;                   use Drawing.Bitmaps;
with Drawing.Primitives;                use Drawing.Primitives;
with Game_Views.Tinker;                 use Game_Views.Tinker;
with Icons;                             use Icons;
with Palette;                           use Palette;
with Preferences;                       use Preferences;
with Signals;                           use Signals;
with Support;                           use Support;
with Tiles;                             use Tiles;
with Widgets.Buttons;                   use Widgets.Buttons;
with Widgets.Buttons.Pushes;            use Widgets.Buttons.Pushes;
with Widgets.Input_Boxes.Constraints;   use Widgets.Input_Boxes.Constraints;
with Widgets.Labels;                    use Widgets.Labels;
with Widgets.Row_Layouts;               use Widgets.Row_Layouts;

package body Tools.Painters is

    package Connections is new Signals.Connections(Painter);
    use Connections;

    ----------------------------------------------------------------------------

    procedure Clicked_Size_Decrease( this : not null access Painter'Class );

    procedure Clicked_Size_Increase( this : not null access Painter'Class );

    procedure Entered_Size( this : not null access Painter'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Painter( tileId : Natural ) return A_Tool is
        this : constant A_Painter := new Painter;
    begin
        this.Construct( tileId );
        return A_Tool(this);
    end Create_Painter;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Painter; tileId : Natural ) is
    begin
        Tool(this.all).Construct( Painter_Tool );
        this.layerSpecific := True;
        this.tileId := tileId;
        this.size := ((Constrain( Get_Pref( "tools", "brushSize", this.size ), 1, MAX_SIZE ) / 2) * 2) + 1;
    end Construct;

    ----------------------------------------------------------------------------

    procedure Clicked_Size_Decrease( this : not null access Painter'Class ) is
    begin
        if this.size > 2 then
            this.size := this.size - 2;
            this.sizeInput.Set_Text( Image( this.size ) );
            Set_Pref( "tools", "brushSize", this.size );
        end if;
    end Clicked_Size_Decrease;

    ----------------------------------------------------------------------------

    procedure Clicked_Size_Increase( this : not null access Painter'Class ) is
    begin
        if this.size < MAX_SIZE then
            this.size := this.size + 2;
            this.sizeInput.Set_Text( Image( this.size ) );
            Set_Pref( "tools", "brushSize", this.size );
        end if;
    end Clicked_Size_Increase;

    ----------------------------------------------------------------------------

    overriding
    function Create_Options( this : access Painter;
                             view : A_Game_View ) return A_Widget is
        lib    : constant Library_Ptr := Load_Library( A_Tinker_View(view).Get_Scene.Get_Library, False );
        row    : A_Row_Layout;
        label  : A_Label;
        button : A_Button;
    begin
        row := Create_Row_Layout( view );

            label := Create_Label( view );
            label.Set_Style( "painterOptions" );
            label.Set_Icon( Create_Icon( lib, this.tileId ) );
            label.Set_Text( "Brush width:" );
            label.Set_Preferred_Width( label.Get_Min_Width );
            row.Append( label );

            button := Create_Push_Button( view );
            button.Set_Style( "painterOptions" );
            button.Set_Icon( Create_Icon( "tinker:minus-white-16" ) );
            button.Width.Set( 20.0 );
            button.Height.Set( 20.0 );
            button.Clicked.Connect( Slot( this, Clicked_Size_Decrease'Access ) );
            row.Append( button );

            this.sizeInput := Create_Input_Box( view );
            this.sizeInput.Set_Style( "painterOptions" );
            this.sizeInput.Set_Text( Image( this.size ) );
            this.sizeInput.Width.Set( 25.0 );
            this.sizeInput.Height.Set( 20.0 );
            this.sizeInput.Accepted.Connect( Slot( this, Entered_Size'Access ) );
            this.sizeInput.Blurred.Connect( Slot( this, Entered_Size'Access ) );
            this.sizeInput.Set_Constraint( Int_Only'Access );
            this.sizeInput.Set_Max_Length( 2 );
            row.Append( this.sizeInput );

            button := Create_Push_Button( view );
            button.Set_Style( "painterOptions" );
            button.Set_Icon( Create_Icon( "tinker:plus-white-16" ) );
            button.Width.Set( 20.0 );
            button.Height.Set( 20.0 );
            button.Clicked.Connect( Slot( this, Clicked_Size_Increase'Access ) );
            row.Append( button );

            label := Create_Label( view );
            label.Set_Style( "painterOptions" );
            label.Set_Text( " | Tile Id: " & Image( this.tileId ) &
                            " [" & To_String( lib.Get.Get_Tile( this.tileId ).Get_Shape ) & "]" );
            row.Append( label );

        row.Set_Preferred_Width( A_Widget(row).Get_Min_Width );
        return A_Widget(row);
    end Create_Options;

    ----------------------------------------------------------------------------

    overriding
    procedure Apply_To_Map( this      : access Painter;
                            func      : Function_Type;
                            modifiers : Modifiers_Array;
                            first     : Boolean;
                            context   : Map_Tool_Context ) is
        tw       : constant Integer := this.scene.Get_Tile_Width;
        halfSize : constant Integer := this.size / 2;
        midX     : constant Integer := Integer(context.x) / tw;
        midY     : constant Integer := Integer(context.y) / tw;
        tileX1   : constant Integer := Integer'Max( 0, midX - halfSize );
        tileY1   : constant Integer := Integer'Max( 0, midY - halfSize );
        tileX2   : constant Integer := Integer'Min( midX + halfSize, this.scene.Get_Map.Get_Width - 1 );
        tileY2   : constant Integer := Integer'Min( midY + halfSize, this.scene.Get_Map.Get_Height - 1 );
        cmd      : A_Set_Tiles_Command;
        oldId    : Natural;
    begin
        if first then
            this.lastX := -1;
            this.lastY := -1;
        end if;

        case func is
            when Primary =>
                -- don't execute the paint command multiple times for the same
                -- tile when dragging the tool around.
                if midX /= this.lastX or midY /= this.lastY then
                    this.lastX := midX;
                    this.lastY := midY;

                    if this.scene.Is_Layer_Visible( context.layer ) then
                        cmd := Create_Command_Set_Tiles;

                        for tileY in tileY1..tileY2 loop
                            for tileX in tileX1..tileX2 loop
                                if this.scene.In_Active_Area( tileX, tileY ) then
                                    oldId := this.scene.Get_Map.Get_Tile_Id( context.layer, tileX, tileY );

                                    -- modified primary function: erase a tile
                                    if modifiers(CTRL) then
                                        cmd.Add( context.layer, tileX, tileY, oldId, 0 );

                                    -- primary function: paint a tile
                                    elsif this.tileId > 0 then
                                        cmd.Add( context.layer, tileX, tileY, oldId, this.tileId );
                                    end if;
                                end if;
                            end loop;
                        end loop;

                        if cmd.Tile_Count > 0 then
                            this.view.Commands.Execute( cmd );
                        else
                            Delete( A_Command(cmd) );
                        end if;
                    end if;
                end if;

            when Secondary =>
                -- secondary function: pick a tile
                if this.scene.Is_Layer_Visible( context.layer ) then
                    oldId := this.scene.Get_Map.Get_Tile_Id( context.layer, midX, midY );
                    if first and oldId > 0 then
                        this.view.Replace_Tool( Create_Painter( oldId ) );
                    end if;
                end if;

        end case;
    end Apply_To_Map;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Preview_Foreground( this      : access Painter;
                                       modifiers : Modifiers_Array;
                                       context   : Tool_Preview_Context ) is
        tw       : constant Integer := this.scene.Get_Tile_Width;
        halfSize : constant Integer := this.size / 2;
        midX     : constant Integer := Integer(context.x) / tw;
        midY     : constant Integer := Integer(context.y) / tw;
        tileX1   : constant Integer := Integer'Max( 0, midX - halfSize );
        tileY1   : constant Integer := Integer'Max( 0, midY - halfSize );
        tileX2   : constant Integer := Integer'Min( midX + halfSize, this.scene.Get_Map.Get_Width - 1 );
        tileY2   : constant Integer := Integer'Min( midY + halfSize, this.scene.Get_Map.Get_Height - 1 );
        tint     : Allegro_Color;
    begin
        if not this.scene.Get_Map.In_Map_Area( midX, midY ) or not this.scene.Is_Mouse_In_Viewport then
            return;
        end if;

        -- highlight the affected area to paint
        if tileY1 <= tileY2 and tileX1 <= tileX2 then
            tint := (if modifiers(CTRL) then ERASE_TINT else DRAW_TINT);

            -- the highlighted paint area is not filled when the active layer is
            -- not visible
            if this.scene.Is_Layer_Visible( context.layer ) then
                Rectfill_XYWH( Float(tileX1 * tw), Float(tileY1 * tw),
                               Float((tileX2 - tileX1 + 1) * tw), Float((tileY2 - tileY1 + 1) * tw),
                               tint );
            end if;

            -- highlighted area border
            Rect_XYWH( Float(tileX1 * tw), Float(tileY1 * tw),
                       Float((tileX2 - tileX1 + 1) * tw), Float((tileY2 - tileY1 + 1) * tw),
                       Whiten( Opacity( tint, 1.0 ), 0.25 ) );
        end if;
    end Draw_Preview_Foreground;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Preview_Layer( this      : access Painter;
                                  modifiers : Modifiers_Array;
                                  context   : Tool_Preview_Context ) is
        tw       : constant Integer := this.scene.Get_Tile_Width;
        halfSize : constant Integer := this.size / 2;
        midX     : constant Integer := Integer(context.x) / tw;
        midY     : constant Integer := Integer(context.y) / tw;
        tileX1   : constant Integer := Integer'Max( 0, midX - halfSize );
        tileY1   : constant Integer := Integer'Max( 0, midY - halfSize );
        tileX2   : constant Integer := Integer'Min( midX + halfSize, this.scene.Get_Map.Get_Width - 1 );
        tileY2   : constant Integer := Integer'Min( midY + halfSize, this.scene.Get_Map.Get_Height - 1 );
        tile     : A_Tile;
        x, y     : Float;
    begin
        if not this.scene.Get_Map.In_Map_Area( midX, midY ) or not this.scene.Is_Mouse_In_Viewport then
            return;
        end if;

        -- if not holding the modifier to erase instead of paint
        if not modifiers(CTRL) then
            -- draw the tile preview
            -- if the active layer is visible
            if this.scene.Is_Layer_Visible( context.layer ) then
                tile := context.mapLib.Get.Get_Tile( this.tileId );
                for tileY in tileY1..tileY2 loop
                    for tileX in tileX1..tileX2 loop
                        if this.scene.In_Active_Area( tileX, tileY ) then
                            x := Float(tileX * tw);
                            y := Float(tileY * tw);
                            To_Target_Pixel_Rounded( x, y );

                            Draw_Bitmap_Region( tile.Get_Bitmap,
                                                0.0, 0.0,
                                                Float'Min( tile.Get_Width, Float(tw) ), Float'Min( tile.Get_Height, Float(tw) ),
                                                x, y, 0.0,
                                                Opacity( TILE_OPACITY ) );
                        end if;
                    end loop;
                end loop;
            end if;
        end if;
    end Draw_Preview_Layer;

    ----------------------------------------------------------------------------

    procedure Entered_Size( this : not null access Painter'Class ) is
    begin
        this.size := Integer'Value( this.sizeInput.Get_Text );
        this.size := ((this.size / 2) * 2) + 1;
        this.sizeInput.Set_Text( Image( this.size) );
    end Entered_Size;

    ----------------------------------------------------------------------------

    function Get_Tile_Id( this : not null access Painter'Class ) return Natural is (this.tileId);

    ----------------------------------------------------------------------------

    overriding
    procedure On_Selected( this : access Painter ) is
    begin
        this.size := ((Constrain( Get_Pref( "tools", "brushSize", this.size ), 1, MAX_SIZE ) / 2) * 2) + 1;
        if this.sizeInput /= null then
            this.sizeInput.Set_Text( Image( this.size ) );
        end if;
    end On_Selected;

    ----------------------------------------------------------------------------

    overriding
    procedure Post_Apply_Map( this      : access Painter;
                              func      : Function_Type;
                              modifiers : Modifiers_Array;
                              context   : Map_Tool_Context ) is
        pragma Unreferenced( func, modifiers, context );
    begin
        this.view.Commands.Prevent_Combine;    -- no more combining tile commands
    end Post_Apply_Map;

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Key_Typed( this      : access Painter;
                                key       : Integer;
                                modifiers : Modifiers_Array ) is
    begin
        if key = ALLEGRO_KEY_MINUS or key = ALLEGRO_KEY_PAD_MINUS then
            this.Clicked_Size_Decrease;
        elsif key = ALLEGRO_KEY_EQUALS or key = ALLEGRO_KEY_PAD_PLUS then
            this.Clicked_Size_Increase;
        else
            Tool(this.all).Handle_Key_Typed( key, modifiers );
        end if;
    end Handle_Key_Typed;

end Tools.Painters;
