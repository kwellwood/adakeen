--
-- Copyright (c) 2014-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Assets.Libraries;                  use Assets.Libraries;
with Drawing;                           use Drawing;
with Drawing.Primitives;                use Drawing.Primitives;
with Game_Views.Tinker;                 use Game_Views.Tinker;
with Palette;                           use Palette;
with Preferences;                       use Preferences;
with Signals;                           use Signals;
with Support;                           use Support;
with Tiles;                             use Tiles;
with Tools.Painters;                    use Tools.Painters;
with Widgets.Buttons.Checkboxes;        use Widgets.Buttons.Checkboxes;
with Widgets.Row_Layouts;               use Widgets.Row_Layouts;

package body Tools.Pickers is

    package Connections is new Signals.Connections(Picker);
    use Connections;

    ----------------------------------------------------------------------------

    procedure Clicked_All_Layers_Disable( this : not null access Picker'Class );

    procedure Clicked_All_Layers_Enable( this : not null access Picker'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Picker return A_Tool is
        this : constant A_Picker := new Picker;
    begin
        this.Construct( Picker_Tool );
        this.layerSpecific := not Get_Pref( "tools", "picker.allLayers", True );    -- default to False
        return A_Tool(this);
    end Create_Picker;

    ----------------------------------------------------------------------------

    overriding
    procedure Apply_To_Map( this      : access Picker;
                            func      : Function_Type;
                            modifiers : Modifiers_Array;
                            first     : Boolean;
                            context   : Map_Tool_Context ) is
        pragma Unreferenced( modifiers );
        tileX  : constant Integer := Integer(Float'Floor( context.x / Float(this.scene.Get_Tile_Width) ));
        tileY  : constant Integer := Integer(Float'Floor( context.y / Float(this.scene.Get_Tile_Width) ));
        tileId : Natural := 0;
        lib    : Library_Ptr;
    begin
        if first then
            this.lastX := -1;
            this.lastY := -1;
        end if;

        if tileX /= this.lastX or tileY /= this.lastY then
            this.lastX := tileX;
            this.lastY := tileY;

            for layer in this.scene.Get_Map.Get_Foreground_Layer..this.scene.Get_Map.Get_Background_Layer loop
                if this.scene.Is_Layer_Visible( layer ) then
                    if not this.layerSpecific or layer = context.layer then
                        tileId := this.scene.Get_Map.Get_Tile_Id( layer, tileX, tileY );
                        exit when tileId /= 0 or this.layerSpecific;
                    end if;
                end if;
            end loop;

            if tileId = 0 then
                return;
            end if;

            this.view.Replace_Tool( Create_Painter( tileId ) );

            -- immediately use the new paintbrush, which causes the tile palette to
            -- update its selected tile highlight.
            this.view.Select_Tool( Painter_Tool );

            -- the primary function only picks the tile, so we switch back to the
            -- picker tool here. the secondary function keeps the painter tool.
            if func = Primary then
                this.view.Select_Tool( Picker_Tool );

                lib := Load_Library( this.scene.Get_Library, False );
                this.Update_Label( Create_Icon( lib, tileId ),
                                   lib.Get.Get_Tile( tileId ).Get_Shape );

            end if;
        end if;
    end Apply_To_Map;

    ----------------------------------------------------------------------------

    procedure Clicked_All_Layers_Disable( this : not null access Picker'Class ) is
        view : constant A_Tinker_View := A_Tinker_View(A_Widget(this.Signaller).Get_View);
    begin
        this.layerSpecific := True;
        Set_Pref( "tools", "picker.allLayers", not this.layerSpecific );
        view.Get_Scene.Set_Active_Layer( view.Get_Scene.Get_Active_Layer );
        view.Get_Scene.Request_Focus;
    end Clicked_All_Layers_Disable;

    ----------------------------------------------------------------------------

    procedure Clicked_All_Layers_Enable( this : not null access Picker'Class ) is
        view : constant A_Tinker_View := A_Tinker_View(A_Widget(this.Signaller).Get_View);
    begin
        this.layerSpecific := False;
        Set_Pref( "tools", "picker.allLayers", not this.layerSpecific );
        view.Get_Scene.Set_Active_Layer( view.Get_Scene.Get_Active_Layer );
        view.Get_Scene.Request_Focus;
    end Clicked_All_Layers_Enable;

    ----------------------------------------------------------------------------

    overriding
    function Create_Options( this : access Picker;
                             view : A_Game_View ) return A_Widget is
        row   : A_Row_Layout;
        label : A_Label;
    begin
        row := Create_Row_Layout( view );

            this.btnAllLayers := A_Button(Create_Checkbox( view ));
            this.btnAllLayers.Set_Style( "painterOptions" );
            this.btnAllLayers.Set_Text( " All Layers" );
            this.btnAllLayers.Set_State( not this.layerSpecific );
            this.btnAllLayers.Pressed.Connect( Slot( this, Clicked_All_Layers_Enable'Access ) );
            this.btnAllLayers.Released.Connect( Slot( this, Clicked_All_Layers_Disable'Access ) );
            row.Append( this.btnAllLayers );

            label := Create_Label( view );
            label.Set_Style( "painterOptions" );
            label.Set_Text( " | " );
            row.Append( label );

            this.tileLabel := Create_Label( view );
            this.tileLabel.Set_Style( "painterOptions" );
            row.Append( this.tileLabel );

        row.Set_Preferred_Width( A_Widget(row).Get_Min_Width );
        return A_Widget(row);
    end Create_Options;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Preview_Foreground( this      : access Picker;
                                       modifiers : Modifiers_Array;
                                       context   : Tool_Preview_Context ) is
        pragma Unreferenced( modifiers );
        tw    : constant Natural := this.scene.Get_Tile_Width;
        tileX : constant Integer := Integer(Float'Floor( context.x / Float(tw) ));
        tileY : constant Integer := Integer(Float'Floor( context.y / Float(tw) ));
    begin
        if not this.scene.Get_Map.In_Map_Area( tileX, tileY ) or not this.scene.Is_Mouse_In_Viewport then
            return;
        end if;

        -- highlight the picking area when a visible, non-zero tile is available
        -- to be picked. all visible layers will be checked if tool is not in
        -- layer specific mode, otherwise just the active layer (context.layer)
        -- will be checked.
        for layer in this.scene.Get_Map.Get_Foreground_Layer..this.scene.Get_Map.Get_Background_Layer loop
            if this.scene.Is_Layer_Visible( layer ) then
                if not this.layerSpecific or layer = context.layer then
                    if this.scene.Get_Map.Get_Tile_Id( layer, tileX, tileY ) /= 0 then
                        -- highlighted picking area
                        Rect_XYWH( Float(tileX * tw), Float(tileY * tw),
                                   Float(tw), Float(tw),
                                   White );
                        exit;
                    end if;
                end if;
            end if;
        end loop;
    end Draw_Preview_Foreground;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Selected( this : access Picker ) is
    begin
        this.Update_Label( NO_ICON, Passive );
    end On_Selected;

    ----------------------------------------------------------------------------

    procedure Update_Label( this  : not null access Picker'Class;
                            icon  : Icon_Type;
                            shape : Clip_Type ) is
    begin
        this.tileLabel.Set_Icon( icon );
        if icon /= NO_ICON then
            this.tileLabel.Set_Text( "Tile Id: " & Image( icon.Get_Tile.Get_Id ) & " [" & To_String( shape ) & "]" );
        else
            this.tileLabel.Set_Text( "" );
        end if;
    end Update_Label;

end Tools.Pickers;
