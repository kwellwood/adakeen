--
-- Copyright (c) 2017-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Assets.Libraries;                  use Assets.Libraries;
with Commands;                          use Commands;
--with Debugging;                         use Debugging;
with Drawing;                           use Drawing;
with Drawing.Bitmaps;                   use Drawing.Bitmaps;
with Drawing.Primitives;                use Drawing.Primitives;
with Game_Views.Tinker;                 use Game_Views.Tinker;
with Icons;                             use Icons;
with Palette;                           use Palette;
with Tiles;                             use Tiles;
with Tools.Painters;                    use Tools.Painters;
with Values.Casting;                    use Values.Casting;
with Widgets.Labels;                    use Widgets.Labels;
with Widgets.Row_Layouts;               use Widgets.Row_Layouts;
with Widgets.Scenes;                    use Widgets.Scenes;

package body Tools.Stampers is

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Stamper( stamp : not null A_Map ) return A_Tool is
        this : constant A_Stamper := new Stamper;
    begin
        this.Construct( stamp );
        return A_Tool(this);
    end Create_Stamper;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Stamper; stamp : A_Map ) is
    begin
        Tool(this.all).Construct( Stamp_Tool );
        this.layerSpecific := stamp.Get_Layer_Count = 1;
        this.stamp := Copy( stamp );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Stamper ) is
    begin
        Delete( this.stamp );
        Tool(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    overriding
    procedure Apply_To_Map( this      : access Stamper;
                            func      : Function_Type;
                            modifiers : Modifiers_Array;
                            first     : Boolean;
                            context   : Map_Tool_Context ) is
        pragma Unreferenced( modifiers );
        tw       : constant Natural := this.scene.Get_Tile_Width;
        startX   : constant Integer := Integer(context.x) / tw - this.stamp.Get_Width / 2;
        startY   : constant Integer := Integer(context.y) / tw - this.stamp.Get_Height / 2;
        cmd      : A_Set_Tiles_Command;
        tileId   : Natural := 0;
        oldId    : Natural;
        mapLayer : Integer;
    begin
        -- apply a stamp --
        if func = Primary then
            if not first then
                return;
            end if;

            cmd := Create_Command_Set_Tiles;

            for stampLayer in this.stamp.Get_Foreground_Layer..this.stamp.Get_Background_Layer loop
                -- target map layer for drawing
                -- if the stamp is only one layer, the active layer will be the only target
                -- otherwise layers from the stamp will go directly to the map, 1:1
                mapLayer := (if this.layerSpecific then context.layer else stampLayer);

                if stampLayer in this.scene.Get_Map.Get_Foreground_Layer..this.scene.Get_Map.Get_Background_Layer and then
                   this.scene.Get_Map.Get_Layer_Type( mapLayer ) = Layer_Tiles and then
                   this.scene.Is_Layer_Visible( mapLayer )
                then
                    for tileY in 0..(this.stamp.Get_Height-1) loop
                        for tileX in 0..(this.stamp.Get_Width-1) loop
                            if startX in 0..(this.scene.Get_Map.Get_Width-1) and startY in 0..(this.scene.Get_Map.Get_Height-1) then
                                if this.scene.In_Active_Area( startX + tileX, startY + tileY ) then
                                    oldId := this.scene.Get_Map.Get_Tile_Id( mapLayer, startX + tileX, startY + tileY );
                                    tileId := this.stamp.Get_Tile_Id( stampLayer, tileX, tileY );
                                    if tileId > 0 then
                                        cmd.Add( mapLayer, startX + tileX, startY + tileY, oldId, tileId );
                                    end if;
                                end if;
                            end if;
                        end loop; -- tileX
                    end loop; -- tileY
                end if;
            end loop; -- stampLayer

            if cmd.Tile_Count > 0 then
                this.view.Commands.Execute( cmd );
                this.view.Commands.Prevent_Combine;    -- no more combining tile commands
            else
                Delete( A_Command(cmd) );
            end if;

        -- pick a tile and paint --
        elsif func = Secondary then
            for layer in this.scene.Get_Map.Get_Foreground_Layer..this.scene.Get_Map.Get_Background_Layer loop
                -- pick from the active layer or the tile in the most foreground visible layer
                if this.scene.Is_Layer_Visible( layer ) then
                    if not this.Is_Layer_Specific or layer = context.layer then
                        tileId := this.scene.Get_Map.Get_Tile_Id( context.layer,
                                                                  Integer(context.x) / this.scene.Get_Tile_Width,
                                                                  Integer(context.y) / this.scene.Get_Tile_Width );
                        exit when tileId /= 0 or this.layerSpecific;
                    end if;
                end if;
            end loop;
            if tileId /= 0 then
                this.view.Replace_Tool( Create_Painter( tileId ) );
                this.view.Select_Tool( Painter_Tool );
            end if;

        end if;
    end Apply_To_Map;

    ----------------------------------------------------------------------------

    overriding
    function Create_Options( this : access Stamper;
                             view : A_Game_View ) return A_Widget is
        row   : A_Row_Layout;
        label : A_Label;
    begin
        row := Create_Row_Layout( view );

            label := Create_Label( view );
            label.Set_Style( "stamperOptions" );
            label.Set_Icon( Create_Icon(  A_Tinker_View(view).Get_Scene.Get_Library,
                                          this.stamp.Get_Layer_Property( 0, "icon" ).To_Int ) );
            label.Set_Text( Cast_String( this.stamp.Get_Layer_Property( 0, "name" ) ) );
            row.Append( label );

        row.Set_Preferred_Width( A_Widget(row).Get_Min_Width );
        return A_Widget(row);
    end Create_Options;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Preview_Foreground( this      : access Stamper;
                                       modifiers : Modifiers_Array;
                                       context   : Tool_Preview_Context ) is
        pragma Unreferenced( modifiers );
        tw     : constant Natural := this.scene.Get_Tile_Width;
        midX   : constant Integer := Integer(context.x) / tw;
        midY   : constant Integer := Integer(context.y) / tw;
        startX : constant Integer := midX - this.stamp.Get_Width / 2;
        startY : constant Integer := midY - this.stamp.Get_Height / 2;
        tileX1 : constant Integer := Integer'Max( 0, startX );
        tileY1 : constant Integer := Integer'Max( 0, startY );
        tileX2 : constant Integer := Integer'Min( startX + this.stamp.Get_Width, this.scene.Get_Map.Get_Width ) - 1;
        tileY2 : constant Integer := Integer'Min( startY + this.stamp.Get_Height, this.scene.Get_Map.Get_Height ) - 1;
    begin
        if not this.scene.Get_Map.In_Map_Area( midX, midY ) or not this.scene.Is_Mouse_In_Viewport then
            return;
        end if;

        -- highlight the affected area to paint
        if tileY1 <= tileY2 and tileX1 <= tileX2 then

            -- the stamp's highlighted area is not filled when operating on
            -- a single layer that is not active.
            if this.scene.Is_Layer_Visible( context.layer ) or not this.Is_Layer_Specific then
                Rectfill_XYWH( Float(tileX1 * tw), Float(tileY1 * tw),
                               Float((tileX2 - tileX1 + 1) * tw), Float((tileY2 - tileY1 + 1) * tw),
                               Tools.Painters.DRAW_TINT );
            end if;

            -- highlighted area border
            Rect_XYWH( Float(tileX1 * tw), Float(tileY1 * tw),
                       Float((tileX2 - tileX1 + 1) * tw), Float((tileY2 - tileY1 + 1) * tw),
                       Whiten( Opacity( Tools.Painters.DRAW_TINT, 1.0 ), 0.25 ) );
        end if;
    end Draw_Preview_Foreground;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Preview_Layer( this      : access Stamper;
                                  modifiers : Modifiers_Array;
                                  context   : Tool_Preview_Context ) is
        pragma Unreferenced( modifiers );
        tw     : constant Natural := this.scene.Get_Tile_Width;
        midX   : constant Integer := Integer(context.x) / tw;
        midY   : constant Integer := Integer(context.y) / tw;
        startX : constant Integer := midX - this.stamp.Get_Width / 2;
        startY : constant Integer := midY - this.stamp.Get_Height / 2;
        tileId : Natural;
        bmp    : A_Allegro_Bitmap;
        x, y   : Float;
    begin
        if not this.scene.Get_Map.In_Map_Area( midX, midY ) or not this.scene.Is_Mouse_In_Viewport then
            return;
        end if;

        for layer in reverse this.stamp.Get_Foreground_Layer..this.stamp.Get_Background_Layer loop
            if layer in this.scene.Get_Map.Get_Foreground_Layer..this.scene.Get_Map.Get_Background_Layer and then
               this.scene.Get_Map.Get_Layer_Type( layer ) = Layer_Tiles and then
               this.scene.Is_Layer_Visible( layer )
            then
                for tileY in 0..this.stamp.Get_Height-1 loop
                    for tileX in 0..this.stamp.Get_Width-1 loop
                        if startX in 0..this.scene.Get_Map.Get_Width-1 and startY in 0..this.scene.Get_Map.Get_Height-1 then
                            if this.scene.In_Active_Area( startX + tileX, startY + tileY ) then
                                tileId := this.stamp.Get_Tile_Id( layer, tileX, tileY );
                                if tileId > 0 then
                                    bmp := context.mapLib.Get.Get_Tile( tileId ).Get_Bitmap;

                                    x := Float((startX + tileX) * tw);
                                    y := Float((startY + tileY) * tw);
                                    To_Target_Pixel_Rounded( x, y );

                                    Draw_Bitmap_Region( bmp,
                                                        0.0, 0.0,
                                                        Float(Integer'Min( Al_Get_Bitmap_Width( bmp ), tw )),
                                                        Float(Integer'Min( Al_Get_Bitmap_Height( bmp ), tw )),
                                                        x, y, 0.0,
                                                        Opacity( Tools.Painters.TILE_OPACITY ) );
                                end if;
                            end if;
                        end if;
                    end loop; -- tileX
                end loop; -- tileY
            end if;
        end loop; -- layer
    end Draw_Preview_Layer;

end Tools.Stampers;
