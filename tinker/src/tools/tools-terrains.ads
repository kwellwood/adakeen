--
-- Copyright (c) 2017-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Vectors;
with Assets.Libraries;                  use Assets.Libraries;
with Maps;                              use Maps;
with Tiles.Terrains;                    use Tiles.Terrains;
with Vector_Math;                       use Vector_Math;
with Widgets.Buttons;                   use Widgets.Buttons;

package Tools.Terrains is

    type Terrain_Brush is new Tool with private;
    type A_Terrain_Brush is access all Terrain_Brush'Class;

    function Create_Terrain_Brush( lib   : Library_Ptr;
                                   index : Natural ) return A_Tool;
    pragma Postcondition( Create_Terrain_Brush'Result /= null );

    overriding
    function Acts_On( this : access Terrain_Brush; target : Subject_Type ) return Boolean is (target = Map_Subject);

    procedure Apply_To_Map( this      : access Terrain_Brush;
                            func      : Function_Type;
                            modifiers : Modifiers_Array;
                            first     : Boolean;
                            context   : Map_Tool_Context );

    function Create_Options( this : access Terrain_Brush;
                             view : A_Game_View ) return A_Widget;

    procedure Draw_Preview_Layer( this      : access Terrain_Brush;
                                  modifiers : Modifiers_Array;
                                  context   : Tool_Preview_Context );

    procedure Draw_Preview_Foreground( this      : access Terrain_Brush;
                                       modifiers : Modifiers_Array;
                                       context   : Tool_Preview_Context );

    overriding
    function Get_Cursor( this : access Terrain_Brush ) return String is ("tinker:cursor-painter");

    function Get_Terrain_Index( this : not null access Terrain_Brush'Class ) return Natural;

    function Is_Busy( this : access Terrain_Brush ) return Boolean;

    procedure On_Unselected( this : access Terrain_Brush );

private

    package Point_Vectors is new Ada.Containers.Vectors( Positive, Vec2, "=" );
    use Point_Vectors;

    ----------------------------------------------------------------------------

    type Terrain_Brush is new Tool with
        record
            btnUndo    : A_Button;
            btnCommit  : A_Button;
            lib        : Library_Ptr;
            index      : Natural := 0;
            definition : A_Terrain_Definition;
            map        : A_Map;
            path       : Point_Vectors.Vector;
        end record;

    procedure Construct( this  : access Terrain_Brush;
                         lib   : Library_Ptr;
                         index : Natural );

    procedure Delete( this : in out Terrain_Brush );

end Tools.Terrains;
