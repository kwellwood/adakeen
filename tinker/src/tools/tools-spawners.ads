--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Icons;                             use Icons;
with Values.Maps;                       use Values.Maps;

package Tools.Spawners is

    type Spawner is new Tool with private;
    type A_Spawner is access all Spawner'Class;

    -- Creates a new entity using template 'template'. 'icon' represents the
    -- the entity visually. It will be displayed as 32x32.
    function Create_Spawner( template : String; icon : Icon_Type ) return A_Tool;

    overriding
    function Acts_On( this : access Spawner; target : Subject_Type ) return Boolean is (target = Map_Subject);

    procedure Apply_To_Map( this      : access Spawner;
                            func      : Function_Type;
                            modifiers : Modifiers_Array;
                            first     : Boolean;
                            context   : Map_Tool_Context );

    function Create_Options( this : access Spawner;
                             view : A_Game_View ) return A_Widget;

    procedure Draw_Preview_Foreground( this      : access Spawner;
                                       modifiers : Modifiers_Array;
                                       context   : Map_Tool_Context );

    overriding
    function Get_Cursor( this : access Spawner ) return String is ("tinker:cursor-spawner");

    function Get_Template( this : not null access Spawner'Class ) return String;

    procedure Post_Apply_Map( this      : access Spawner;
                              func      : Function_Type;
                              modifiers : Modifiers_Array;
                              context   : Map_Tool_Context );

private

    type Spawner is new Tool with
        record
            template      : Unbounded_String;
            icon          : Icon_Type;           -- shown in the tool options panel
            preview       : Icon_Type;           -- shown when placing on the map
            snapEnabled   : Boolean := False;    -- allow grid snapping?
            snapCenterX   : Boolean := False;    -- snap horizontal center to grid cell center in X axis, or left edge to grid lines?
            snapCenterY   : Boolean := False;    -- snap vertical center to grid cell center in Y axis, or top edge to grid lines?
            placeOffsetX  : Float := 0.0;        -- offset added to initial location
            placeOffsetY  : Float := 0.0;        --   after gridsnapping is applied
            initialWidth  : Float := 0.0;        -- initial size for calculating
            initialHeight : Float := 0.0;        --   entity location
            properties    : Map_Value;
            ready         : Boolean := True;     -- ready to spawn (used for preview)
        end record;

    procedure Construct( this     : access Spawner;
                         template : String;
                         icon     : Icon_Type );

    -- Returns the center location of an entity in x or y that is being placed
    -- at 'n' on the axis, with a width of height of 'size'. The center location
    -- will be appropriately gridsnapped if the entity template is snappable
    -- (this.snapEnabled) and gridsnapping is turned on ("gridsnap" preference).
    -- If the Ctrl key is held, then the "gridsnap" preference will be inverted
    -- by passing 'invertGridsnap' as True.
    --
    -- If 'gridCenter' is True, then the entity placement will be adjusted by
    -- snapping the center of the entity to the center of the grid cells.
    -- Otherwise, the top/left of the entity will be snapped to the grid lines.
    function Snapped_Center( this           : not null access Spawner'Class;
                             n              : Float;
                             size           : Float;
                             gridCenter,
                             invertGridsnap : Boolean ) return Float;

end Tools.Spawners;
