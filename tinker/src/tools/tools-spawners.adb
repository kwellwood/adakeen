--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Commands;                          use Commands;
with Drawing.Primitives;                use Drawing.Primitives;
with Drawing.Tiles;                     use Drawing.Tiles;
with Entities.Factory;                  use Entities.Factory;
with Game_Views.Tinker;                 use Game_Views.Tinker;
with Palette;                           use Palette;
with Preferences;                       use Preferences;
with Support;                           use Support;
with Tools.Painters;                    use Tools.Painters;
with Values.Construction;               use Values.Construction;
with Values;                            use Values;
with Values.Lists;                      use Values.Lists;
with Vector_Math;                       use Vector_Math;
with Widgets.Labels;                    use Widgets.Labels;
with Widgets.Row_Layouts;               use Widgets.Row_Layouts;

package body Tools.Spawners is

    function Create_Spawner( template : String; icon : Icon_Type ) return A_Tool is
        this : constant A_Spawner := new Spawner;
    begin
        this.Construct( template, icon );
        return A_Tool(this);
    end Create_Spawner;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this     : access Spawner;
                         template : String;
                         icon     : Icon_Type ) is
        toolParams : Map_Value;
    begin
        Tool(this.all).Construct( Spawner_Tool );
        this.template := To_Unbounded_String( template );
        this.icon := icon;

        -- determine entity template's spawn properties and grid snapping options
        -- from the Tools section of the entity template definition.
        toolParams := Entities.Factory.Global.Get_Template( template ).Get_Tool_Parameters;

        this.preview := Create_Icon( toolParams.Get_String( "preview" ) );
        if this.preview = NO_ICON then
            this.preview := this.icon;
        end if;

        -- If 'snap' is True, the new entity will be snapped to the grid if the
        -- gridsnap preference is enabled. Holding CTRL while using the spawner
        -- tool will reverse the gridsnap behavior. If 'snap' is False, no grid
        -- snapping will ever be performed. If 'snapCenterX' is True, snapping
        -- will snap the X center (+ placement offset X) of the entity's initial
        -- size to the center of a grid square. Otherwise, snapping will snap
        -- the center (+ placement offset X) to the grid lines. 'snapCenterY'
        -- works similarly. 'icon' will be shown as a preview of entity
        -- placement, snapped appropriately.

        this.snapEnabled := toolParams.Get_Boolean( "snap", this.snapEnabled );
        this.snapCenterX := toolParams.Get_Boolean( "snapCenterX", toolParams.Get_Boolean( "snapCenter", this.snapCenterX ) );
        this.snapCenterY := toolParams.Get_Boolean( "snapCenterY", toolParams.Get_Boolean( "snapCenter", this.snapCenterY ) );
        this.placeOffsetX := toolParams.Get_Float( "placementOffsetX", 0.0 );
        this.placeOffsetY := toolParams.Get_Float( "placementOffsetY", 0.0 );

        this.initialWidth := toolParams.Get_Float( "initialWidth", Float(this.preview.Get_Width) );
        this.initialHeight := toolParams.Get_Float( "initialHeight", Float(this.preview.Get_Height) );

        if toolParams.Get( "properties" ).Is_Map then
            this.properties := Clone( toolParams.Get( "properties" ) ).Map;
        else
            this.properties := Create_Map.Map;
        end if;
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Apply_To_Map( this      : access Spawner;
                            func      : Function_Type;
                            modifiers : Modifiers_Array;
                            first     : Boolean;
                            context   : Map_Tool_Context ) is
    begin
        if not first then
            return;
        end if;
        if func = Primary then
            if Contains( Rectangle'(0.0, 0.0,
                                    Float(this.scene.Get_Map.Get_Width * this.scene.Get_Tile_Width),
                                    Float(this.scene.Get_Map.Get_Height * this.scene.Get_Tile_Width)),
                         context.x, context.y )
            then
                this.view.Commands.Execute( Create_Command_Spawn_Entity( To_String( this.template ),
                                                                         this.Snapped_Center( context.x, this.initialWidth, this.snapCenterX, modifiers(CTRL) ) + this.placeOffsetX,
                                                                         this.Snapped_Center( context.y, this.initialHeight, this.snapCenterY, modifiers(CTRL) ) + this.placeOffsetY,
                                                                         this.properties ) );
            end if;
            this.ready := False;

        elsif func = Secondary then
            -- stop spawning things and fallback to the pointer tool
            this.view.Select_Tool( Pointer_Tool );

        end if;
    end Apply_To_Map;

    ----------------------------------------------------------------------------

    overriding
    function Create_Options( this : access Spawner;
                             view : A_Game_View ) return A_Widget is
        row   : A_Row_Layout;
        label : A_Label;
    begin
        row := Create_Row_Layout( view );

            label := Create_Label( view );
            label.Set_Style( "spawnerOptions" );
            label.Set_Icon( this.icon );
            label.Set_Text( this.Get_Template );
            row.Append( label );

        row.Set_Preferred_Width( A_Widget(row).Get_Min_Width );
        return A_Widget(row);
    end Create_Options;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Preview_Foreground( this      : access Spawner;
                                       modifiers : Modifiers_Array;
                                       context   : Tool_Preview_Context ) is
        x, y           : Float;    -- entity X, Y
        frameW, frameH : Float;    -- preview frame size
        frameX, frameY : Float;    -- preview frame x, y
    begin
        if Contains( Rectangle'(0.0, 0.0,
                                Float(this.scene.Get_Map.Get_Width * this.scene.Get_Tile_Width),
                                Float(this.scene.Get_Map.Get_Height * this.scene.Get_Tile_Width)),
                     context.x, context.y )
            and this.scene.Is_Mouse_In_Viewport
            and this.ready
        then
            -- calculate snapped entity location
            x := this.Snapped_Center( context.x, this.initialWidth, this.snapCenterX, modifiers(CTRL) ) + this.placeOffsetX;
            y := this.Snapped_Center( context.y, this.initialHeight, this.snapCenterY, modifiers(CTRL) ) + this.placeOffsetY;

            -- calculate position of preview frame
            -- the size of the frame is determined by the initial size of the entity
            frameW := Float(this.preview.Get_Width);
            frameH := Float(this.preview.Get_Height);
            frameX := x - frameW / 2.0 + this.preview.Get_Tile.Get_OffsetX;
            frameY := y - frameH / 2.0 + this.preview.Get_Tile.Get_OffsetY;

            -- tint the area behind the entity
            Rectfill_XYWH( frameX, frameY, frameW, frameH, Tools.Painters.DRAW_TINT );
            Rect_XYWH( frameX, frameY, frameW, frameH, Whiten( Opacity( Tools.Painters.DRAW_TINT, 1.0 ), 0.25 ) );

            Draw_Tile_Filled( this.preview.Get_Tile, frameX, frameY, 0.0, frameW, frameH );

            -- debugging: show the entity's bounds
            --Line( x - this.initialWidth / 2.0, y - this.initialHeight / 2.0,
            --      x + this.initialWidth / 2.0, y + this.initialHeight / 2.0,
            --      White, 1.0 );
            --Line( x + this.initialWidth / 2.0, y - this.initialHeight / 2.0,
            --      x - this.initialWidth / 2.0, y + this.initialHeight / 2.0,
            --      White, 1.0 );
        end if;
    end Draw_Preview_Foreground;

    ----------------------------------------------------------------------------

    function Get_Template( this : not null access Spawner'Class ) return String is (To_String( this.template ));

    ----------------------------------------------------------------------------

    overriding
    procedure Post_Apply_Map( this      : access Spawner;
                              func      : Function_Type;
                              modifiers : Modifiers_Array;
                              context   : Map_Tool_Context ) is
        pragma Unreferenced( modifiers, context );
    begin
        if func = Primary then
            this.ready := True;
        end if;
    end Post_Apply_Map;

    ----------------------------------------------------------------------------

    function Snapped_Center( this           : not null access Spawner'Class;
                             n              : Float;
                             size           : Float;
                             gridCenter,
                             invertGridsnap : Boolean ) return Float is
    begin
        if this.snapEnabled and then (Get_Pref( "gridsnap" ) xor invertGridsnap) then
            if gridCenter then
                return Support.Snap( n, Get_Pref( "gridsize" ), True );
            else
                return Support.Snap( n - size / 2.0, Get_Pref( "gridsize" ), False ) + size / 2.0;
            end if;
        end if;
        return n;
    end Snapped_Center;

end Tools.Spawners;
