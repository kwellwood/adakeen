--
-- Copyright (c) 2012-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Game_Views;                        use Game_Views;
with Keyboard;                          use Keyboard;
with Object_Ids;                        use Object_Ids;
with Objects;                           use Objects;
with Tool_Contexts;                     use Tool_Contexts;
with Values.Tagged_Ids;                 use Values.Tagged_Ids;
with Widgets;                           use Widgets;

limited with Game_Views.Tinker;
limited with Widgets.Scenes.Tinker;

package Tools is

    type Tool_Type is
    (
        Panner_Tool,
        Pointer_Tool,
        Surveyor_Tool,
        Painter_Tool,
        Pattern_Tool,
        Stamp_Tool,
        Terrain_Tool,
        Eraser_Tool,
        Picker_Tool,
        Connector_Tool,
        Spawner_Tool
    );

    -- Tools act on either map or entity subjects, performing a Primary and an
    -- optional Secondary function, each with CTRL/ALT/CMD modifiers to alter
    -- the operation.
    --
    -- Each tool subclass determines whether it can be applied to maps, entities,
    -- or both, and implements the action(s) to be taken.
    --
    -- When a tool operates on the map, Apply_To_Map() is called. The left mouse
    -- button applies the Primary action and the middle mouse button applies the
    -- Secondary action. When the mouse button is pressed down, 'first' is set
    -- True (the first time Apply_To_Map will be called in sequence). Until the
    -- mouse button is released, Apply_To_Map() is called again for each
    -- subsequent movement of the mouse with 'first' set to False. When the
    -- mouse button is released, Post_Apply_To_Map() is called to end the
    -- sequence.
    type Tool is abstract new Limited_Object with private;
    type A_Tool is access all Tool'Class;

    -- Initializes the Tool with the Scene that it will operate on. This must be
    -- called once after construction, before the tool can be used.
    procedure Init( this  : not null access Tool'Class;
                    scene : not null access Widgets.Scenes.Tinker.Tinker_Scene'Class );

    -- Returns True if the tool acts on subjects of type 'target'.
    not overriding
    function Acts_On( this : access Tool; target : Subject_Type ) return Boolean is (False);

    -- Returns the tool's type.
    function Get_Type( this : not null access Tool'Class ) return Tool_Type;

    -- Returns True if the tool's operation is restricted to a specific map
    -- layer. The scene will highlight the active layer.
    function Is_Layer_Specific( this : not null access Tool'Class ) return Boolean;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Applies the tool to an entity, if it acts on map subjects.
    not overriding
    procedure Apply_To_Entity( this      : access Tool;
                               func      : Function_Type;
                               modifiers : Modifiers_Array;
                               first     : Boolean;
                               context   : Entity_Tool_Context ) is null;

    -- Applies the tool to the map, if it acts on map subjects.
    not overriding
    procedure Apply_To_Map( this      : access Tool;
                            func      : Function_Type;
                            modifiers : Modifiers_Array;
                            first     : Boolean;
                            context   : Map_Tool_Context ) is null;

    -- Finishes applying the tool to an entity, if it acts on entity subjects.
    -- This is called when the mouse button applying the tool is released.
    not overriding
    procedure Post_Apply_Entity( this      : access Tool;
                                 func      : Function_Type;
                                 modifiers : Modifiers_Array;
                                 context   : Entity_Tool_Context ) is null;

    -- Finishes applying the tool to the map, if it acts on map subjects. This
    -- is called when the mouse button applying the tool is released.
    not overriding
    procedure Post_Apply_Map( this      : access Tool;
                              func      : Function_Type;
                              modifiers : Modifiers_Array;
                              context   : Map_Tool_Context ) is null;

    -- Handles a keypress forwarded from the Scene widget. Implement this to
    -- implement tool-specific shortcuts.
    procedure Handle_Key_Typed( this      : access Tool;
                                key       : Integer;
                                modifiers : Modifiers_Array );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Returns True if the tool could perform an action if Do_Clear() is called
    -- next. Certain tools support Clear operations.
    not overriding
    function Can_Clear( this : access Tool ) return Boolean is (False);

    -- Returns True if the tool could perform an action if Do_Copy() is called
    -- next. Certain tools support Copy, Cut, Paste and Delete operations.
    not overriding
    function Can_Copy( this : access Tool ) return Boolean is (False);

    -- Returns True if the tool could perform an action if Do_Delete() is called
    -- next. Certain tools support Copy, Cut, Paste and Delete operations.
    not overriding
    function Can_Delete( this : access Tool ) return Boolean is (False);

    -- Returns True if the tool could perform an action if Do_Paste() is called
    -- next. Certain tools support Copy, Cut, Paste and Delete operations.
    not overriding
    function Can_Paste( this : access Tool ) return Boolean is (False);

    -- Clears a selection or stateful construction of the tool. Certain tools
    -- support the Clear operation. This is called if Can_Clear() returns True.
    not overriding
    procedure Do_Clear( this : access Tool ) is null;

    -- Copies a selection to the clipboard. Certain tools support Copy, Cut,
    -- Paste and Delete operations. This is called when the Ctrl+C key shortcut
    -- is pressed if Can_Copy() returns True.
    not overriding
    procedure Do_Copy( this : access Tool ) is null;

    -- Deletes a selection. Certain tools support Copy, Cut, Paste and Delete
    -- operations. This is called when the Delete key shortcut is pressed if
    -- Can_Delete() returns True.
    not overriding
    procedure Do_Delete( this : access Tool ) is null;

    -- Pastes from the clipboard. Certain tools support Copy, Cut, Paste and
    -- Delete operations. This is called when the Cltr+V key shortcut is pressed
    -- if Can_Paste() returns True.
    not overriding
    procedure Do_Paste( this : access Tool ) is null;

    -- Returns True if the tool operates in a modal fashion and is currently
    -- busy in a mode (ex: Connector_Tool has already chosen entity A but not B).
    not overriding
    function Is_Busy( this : access Tool ) return Boolean is (False);

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Creates and returns an options widget to display on the toolbar.
    not overriding
    function Create_Options( this : access Tool; view : A_Game_View ) return A_Widget;

    -- Draws a preview of the tool's effect on the map layer indicated by
    -- 'context', obscured by foreground layers and affected by lighting. This
    -- will only be called for tools that operate on the map. During scene
    -- drawing, this will be called once for each layer. If the tool's operation
    -- is layer specific, this will be called only for the active layer.
    not overriding
    procedure Draw_Preview_Layer( this      : access Tool;
                                  modifiers : Modifiers_Array;
                                  context   : Tool_Preview_Context ) is null;

    -- Draws a preview of the tool's effect in the scene's foreground, unaffected
    -- by lighting. Unlike Draw_Preview_Layer(), this will also be called for
    -- tools that can operate on entities.
    not overriding
    procedure Draw_Preview_Foreground( this      : access Tool;
                                       modifiers : Modifiers_Array;
                                       context   : Tool_Preview_Context ) is null;

    -- Returns the entity currently attached to the tool, or a null id if no
    -- entity. This is used by the Connector tool to attach an entity to the
    -- mouse cursor.
    not overriding
    function Get_Attached_Entity( this : access Tool ) return Entity_Id is (NULL_ID);

    -- Returns the name of the tool's mouse cursor. For stateful tools, such as
    -- the Connector tool, the cursor may change.
    not overriding
    function Get_Cursor( this : access Tool ) return String is ("NONE");

    -- Called when the tool has been selected as the current active tool.
    not overriding
    procedure On_Selected( this : access Tool ) is null;

    -- Called when the tool is no longer selected as the current active tool.
    -- This will not be called if the tool is replaced or deleted while still
    -- active. The tool's destructor should handle all cleanup.
    not overriding
    procedure On_Unselected( this : access Tool ) is null;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Deletes the Tool.
    procedure Delete( this : in out A_Tool );

private

    type Tool is abstract new Limited_Object with
        record
            view          : access Game_Views.Tinker.Tinker_View'Class := null;
            scene         : access Widgets.Scenes.Tinker.Tinker_Scene'Class := null;
            toolType      : Tool_Type;
            layerSpecific : Boolean := False;
        end record;

    procedure Construct( this : access Tool; toolType : Tool_Type );

end Tools;
