--
-- Copyright (c) 2012-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Keyboard;                  use Allegro.Keyboard;
--with Debugging;                         use Debugging;
with Game_Views.Tinker;                 use Game_Views.Tinker;
with Widgets.Panels;                    use Widgets.Panels;

package body Tools is

    not overriding
    procedure Construct( this : access Tool; toolType : Tool_Type ) is
    begin
        Limited_Object(this.all).Construct;
        this.toolType := toolType;
    end Construct;

    ----------------------------------------------------------------------------

    function Create_Options( this : access Tool;
                             view : A_Game_View ) return A_Widget is
        pragma Unreferenced( this );
    begin
        return A_Widget(Create_Panel( view ));
    end Create_Options;

    ----------------------------------------------------------------------------

    function Get_Type( this : not null access Tool'Class ) return Tool_Type is (this.toolType);

    ----------------------------------------------------------------------------

    not overriding
    procedure Handle_Key_Typed( this      : access Tool;
                                key       : Integer;
                                modifiers : Modifiers_Array ) is
        pragma Unreferenced( modifiers );
    begin
        if key = ALLEGRO_KEY_A then
            if this.view.Get_Selected_Tool.Acts_On( Entity_Subject ) then
                this.scene.Entity_Select_All;
            end if;
        end if;
    end Handle_Key_Typed;

    ----------------------------------------------------------------------------

    procedure Init( this  : not null access Tool'Class;
                    scene : not null access Widgets.Scenes.Tinker.Tinker_Scene'Class ) is
    begin
        this.scene := scene;
        this.view := A_Tinker_View(scene.Get_View);
    end Init;

    ----------------------------------------------------------------------------

    function Is_Layer_Specific( this : not null access Tool'Class ) return Boolean is (this.layerSpecific);

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Tool ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

end Tools;
