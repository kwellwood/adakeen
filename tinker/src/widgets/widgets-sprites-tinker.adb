--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Keyboard;                  use Allegro.Keyboard;
with Commands;                          use Commands;
--with Debugging;                         use Debugging;
with Directions;                        use Directions;
with Directives.Galaxy;
with Drawing.Primitives;                use Drawing.Primitives;
with Entities.Factory;                  use Entities.Factory;
with Entities.Templates;                use Entities.Templates;
with Game_Views;                        use Game_Views;
with Game_Views.Tinker;                 use Game_Views.Tinker;
with Icons;                             use Icons;
with Lighting_Systems;                  use Lighting_Systems;
with Tool_Contexts;                     use Tool_Contexts;
with Tools;                             use Tools;
with Values.Lists;                      use Values.Lists;
with Widgets.Panels.Properties.Entities;use Widgets.Panels.Properties.Entities;
with Widgets.Menu_Items;                use Widgets.Menu_Items;
with Widgets.Scenes;                    use Widgets.Scenes;
with Widgets.Scenes.Tinker;             use Widgets.Scenes.Tinker;

package body Widgets.Sprites.Tinker is

    GRAB_BORDER_SIZE : constant := 4.0;

    package Connections is new Signals.Connections(Tinker_Sprite);
    use Connections;

    package Key_Connections is new Signals.Keys.Connections(Tinker_Sprite);
    use Key_Connections;

    package Mouse_Connections is new Signals.Mouse.Connections(Tinker_Sprite);
    use Mouse_Connections;

    package Widget_Connections is new Signals.Connections(Widget);
    use Widget_Connections;

    ----------------------------------------------------------------------------

    -- Creates the right-click menu (this.menu) if it has not already been built.
    procedure Build_Menu( this : not null access Tinker_Sprite'Class );

    -- Returns the resizing direction associated with the resize handle that
    -- 'x,y' is within, or Dir_None if the point is not in a resize handle area.
    -- Not all entities are resizable in every direction.
    function Get_Resize_Direction( this : not null access Tinker_Sprite'Class;
                                   x, y : Float ) return Direction_Type;

    function Get_Tinker_View( this : not null access Tinker_Sprite'Class ) return A_Tinker_View is (A_Tinker_View(this.Get_View));

    -- Hides the entity's info panel if it was visible in Tinker's info panel area.
    procedure Hide_Info_Panel( this : not null access Tinker_Sprite'Class );

    -- Called when the down key is typed on the sprite to make it face down.
    procedure On_Key_Down( this : not null access Tinker_Sprite'Class );

    -- Called when the left key is typed on the sprite to make it face left.
    procedure On_Key_Left( this : not null access Tinker_Sprite'Class );

    -- Called when the right key is typed on the sprite to make it face right.
    procedure On_Key_Right( this : not null access Tinker_Sprite'Class );

    -- Called when the up key is typed on the sprite to make it face up.
    procedure On_Key_Up( this : not null access Tinker_Sprite'Class );

    -- Called when the mouse exits the sprite's area on the screen.
    procedure On_Mouse_Exited( this : not null access Tinker_Sprite'Class );

    procedure On_Mouse_Moved( this  : not null access Tinker_Sprite'Class;
                              mouse : Mouse_Arguments );

    procedure On_Mouse_Pressed( this  : not null access Tinker_Sprite'Class;
                                mouse : Button_Arguments );

    procedure On_Mouse_Released_Left( this  : not null access Tinker_Sprite'Class;
                                      mouse : Button_Arguments );

    procedure On_Mouse_Released_Middle( this  : not null access Tinker_Sprite'Class;
                                        mouse : Button_Arguments );

    procedure On_Mouse_Released_Right( this  : not null access Tinker_Sprite'Class;
                                       mouse : Button_Arguments );

    -- Copies the selected entities in the scene.
    procedure On_Popup_Copy( this : not null access Tinker_Sprite'Class );

    -- Deletes the selected entities in the scene.
    procedure On_Popup_Delete( this : not null access Tinker_Sprite'Class );

    procedure On_Selected( this : not null access Tinker_Sprite'Class );

    -- Shows the entity's info panel in the info panel area.
    procedure Show_Info_Panel( this : not null access Tinker_Sprite'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Tinker_Sprite( view    : not null access Game_Views.Game_View'Class;
                                   entity  : not null A_Entity;
                                   libName : String;
                                   frame   : Natural ) return A_Sprite is
        this : A_Tinker_Sprite := new Tinker_Sprite;
    begin
        this.Construct( view, entity, libName, frame );
        return A_Sprite(this);
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Tinker_Sprite;

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this    : access Tinker_Sprite;
                         view    : not null access Game_Views.Game_View'Class;
                         entity  : not null A_Entity;
                         libName : String;
                         frame   : Natural ) is
        toolParams : Map_Value;
        template   : A_Template;
    begin
        Sprite(this.all).Construct( view, entity, libName, frame );

        this.MouseExited.Connect( Slot( this, On_Mouse_Exited'Access ) );
        this.Focused.Connect( Slot( this, Bring_To_Front'Access ) );

        this.KeyTyped.Connect( Slot( this, On_Key_Down'Access, ALLEGRO_KEY_DOWN, (others=>No) ) );
        this.KeyTyped.Connect( Slot( this, On_Key_Left'Access, ALLEGRO_KEY_LEFT, (others=>No) ) );
        this.KeyTyped.Connect( Slot( this, On_Key_Right'Access, ALLEGRO_KEY_RIGHT, (others=>No) ) );
        this.KeyTyped.Connect( Slot( this, On_Key_Up'Access, ALLEGRO_KEY_UP, (others=>No) ) );

        this.MouseMoved.Connect( Slot( this, On_Mouse_Moved'Access ) );
        this.MousePressed.Connect( Slot( this, On_Mouse_Pressed'Access ) );
        this.MouseReleased.Connect( Slot( this, On_Mouse_Released_Left'Access, Mouse_Left ) );
        this.MouseReleased.Connect( Slot( this, On_Mouse_Released_Middle'Access, Mouse_Middle ) );
        this.MouseReleased.Connect( Slot( this, On_Mouse_Released_Right'Access, Mouse_Right ) );
        this.activeModifiers := (others => False);

        this.entity.Selected.Connect( Slot( this, On_Selected'Access ) );
        this.entity.Unselected.Connect( Slot( this, Hide_Info_Panel'Access ) );

        template := Standard.Entities.Factory.Global.Get_Template( entity.Get_Template );
        if template /= null then
            toolParams := template.Get_Tool_Parameters;
        else
            toolParams := Create_Map.Map;
        end if;
        this.resizableW := toolParams.Get_Boolean( "resizableWidth", toolParams.Get_Boolean( "resizable", False ) );
        this.resizableH := toolParams.Get_Boolean( "resizableHeight", toolParams.Get_Boolean( "resizable", False ) );

        -- if neither "snapCenter" nor "snapCenterX" is defined, default it to
        -- "not resizableWidth". this means that entities will default to snapping
        -- to the center of grid cells, excep for entities with a resizable width.
        -- if it's resizable, it's most likely that it should be snapped to grid
        -- edges (if snapped at all) because resizing snaps in increments of the
        -- grid size.
        this.snapCenterX := toolParams.Get_Boolean( "snapCenterX", toolParams.Get_Boolean( "snapCenter", not this.resizableW ) );
        this.snapCenterY := toolParams.Get_Boolean( "snapCenterY", toolParams.Get_Boolean( "snapCenter", not this.resizableH ) );
        this.snapEnabled := toolParams.Get_Boolean( "snap", this.snapEnabled );

        this.minEWidth := Float'Max( 0.0, toolParams.Get_Float( "minWidth", default => 0.0 ) );
        this.minEHeight := Float'Max( 0.0, toolParams.Get_Float( "minHeight", default => 0.0 ) );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Tinker_Sprite ) is
    begin
        Delete( A_Widget(this.menu) );

        if this.infoPanel /= null then
            this.Get_Tinker_View.Hide_Info_Panel( this.infoPanel );
            Delete( this.infoPanel );
        end if;

        Sprite(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Build_Menu( this : not null access Tinker_Sprite'Class ) is
        menu     : A_Sub_Menu;
        menuItem : A_Menu_Item;
    begin
        if this.menu = null then
            menu := Create_Popup_Menu( this.Get_View );

            menuItem := Create_Menu_Item( this.Get_View, "", this.entity.Get_Template );
            menuItem.Set_Enabled( False );
            menu.Add_Menu_Item( menuItem );

            menuItem := Create_Menu_Separator( this.Get_View );
            menu.Add_Menu_Item( menuItem );

            menuItem := Create_Menu_Item( this.Get_View, "Entity Attributes", Create_Icon( "tinker:tag-pencil" ) );
            menuItem.Clicked.Connect( Slot( this, Show_Info_Panel'Access ) );
            menu.Add_Menu_Item( menuItem );

            if this.entity.Is_Deletable then
                menuItem := Create_Menu_Item( this.Get_View, "Copy" );
                menuItem.Clicked.Connect( Slot( this, On_Popup_Copy'Access ) );
                menu.Add_Menu_Item( menuItem );

                menuItem := Create_Menu_Item( this.Get_View, "Delete" );
                menuItem.Clicked.Connect( Slot( this, On_Popup_Delete'Access ) );
                menu.Add_Menu_Item( menuItem );
            end if;

            this.menu := menu;
        end if;
    end Build_Menu;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Content( this : access Tinker_Sprite ) is
        state : Widget_State := this.Get_Visual_State;
    begin
        case A_Scene(this.parent).Get_Render_Pass is

            when Render_Color   |
                 Render_Shading =>

                 -- the left mouse button being held should also count as hovering
                if this.mouseButtons(Mouse_Left) then
                    state := state or HOVER_STATE;
                end if;

                -- draw background
                Rectfill_XYWH( 0.0, 0.0,
                               this.viewport.width, this.viewport.height,
                               Lighten( this.style.Get_Color( FRAME_ELEM, state ), this.style.Get_Shade( FRAME_ELEM, state ) ) );

                Sprite(this.all).Draw_Content;

                -- draw border
                Rect_XYWH( 0.0, 0.0,
                           this.viewport.width, this.viewport.height,
                           Lighten( this.style.Get_Color( BORDER_ELEM, state ), this.style.Get_Shade( BORDER_ELEM, state ) ) );

            when Render_Light =>

                Sprite(this.all).Draw_Content;

        end case;
    end Draw_Content;

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Height( this : access Tinker_Sprite ) return Float
    is (Float'Max( Float(this.Get_Frame_Height), Float'Min( this.minEHeight, this.entity.Get_Entity_Height ) ));

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Width( this : access Tinker_Sprite ) return Float
    is (Float'Max( Float(this.Get_Frame_Width), Float'Min( this.minEWidth, this.entity.Get_Entity_Width ) ));

    ----------------------------------------------------------------------------

    function Get_Resize_Direction( this : not null access Tinker_Sprite'Class;
                                   x, y : Float ) return Direction_Type is

        function Effective_Zoom( w : A_Widget ) return Float is (if w /= null then w.Zoom.Get * Effective_Zoom( w.Get_Parent ) else 1.0);

        HANDLE_WIDTH : constant Float := GRAB_BORDER_SIZE / Effective_Zoom( A_Widget(this) );

        top    : Rectangle := (0.0, 0.0, this.viewport.width, HANDLE_WIDTH);
        left   : Rectangle := (0.0, 0.0, HANDLE_WIDTH, this.viewport.height );
        bottom : Rectangle := (0.0, this.viewport.height - HANDLE_WIDTH, this.viewport.width, HANDLE_WIDTH);
        right  : Rectangle := (this.viewport.width - HANDLE_WIDTH, 0.0, HANDLE_WIDTH, this.viewport.height);
    begin
        if this.resizableW then
            top.x := top.x + HANDLE_WIDTH;
            top.width := top.width - HANDLE_WIDTH * 2.0;
            bottom.x := top.x;
            bottom.width := top.width;
        end if;
        if this.resizableH then
            left.y := left.y + HANDLE_WIDTH;
            left.height := left.height - HANDLE_WIDTH * 2.0;
            right.y := left.y;
            right.height := left.height;
        end if;

        return (
            if (this.resizableW and this.resizableH) and then
               Contains( Rectangle'(left.x, top.y, left.width, top.height), x, y ) then Dir_Up_Left

            elsif (this.resizableW and this.resizableH) and then
                  Contains( Rectangle'(right.x, top.y, right.width, top.height), x, y ) then Dir_Up_Right

            elsif (this.resizableW and this.resizableH) and then
                  Contains( Rectangle'(left.x, bottom.y, left.width, bottom.height), x, y ) then Dir_Down_Left

            elsif (this.resizableW and this.resizableH) and then
                  Contains( Rectangle'(right.x, bottom.y, right.width, bottom.height), x, y ) then Dir_Down_Right

            elsif this.resizableH and then
                  Contains( top, x, y ) then Dir_Up

            elsif this.resizableH and then
                  Contains( bottom, x, y ) then Dir_Down

            elsif this.resizableW and then
                  Contains( left, x, y ) then Dir_Left

            elsif this.resizableW and then
                  Contains( right, x, y ) then Dir_Right

            else Dir_None
        );
    end Get_Resize_Direction;

    ----------------------------------------------------------------------------

    overriding
    function Get_Visual_State( this : access Tinker_Sprite ) return Widget_State is
    (
        Sprite(this.all).Get_Visual_State or
        (if this.resizableW or this.resizableH then RESIZABLE_STATE else 0)
    );

    ----------------------------------------------------------------------------

    procedure Hide_Info_Panel( this : not null access Tinker_Sprite'Class ) is
    begin
        this.Get_Tinker_View.Hide_Info_Panel( this.infoPanel );
    end Hide_Info_Panel;

    ----------------------------------------------------------------------------

    procedure On_Key_Down( this : not null access Tinker_Sprite'Class ) is
    begin
        this.Get_Tinker_View.Commands.Execute( Create_Command_Entity_Directive( this.entity.Get_Entity_Id, Directives.Galaxy.FACE_DOWN ) );
    end On_Key_Down;

    ----------------------------------------------------------------------------

    procedure On_Key_Left( this : not null access Tinker_Sprite'Class ) is
    begin
        this.Get_Tinker_View.Commands.Execute( Create_Command_Entity_Directive( this.entity.Get_Entity_Id, Directives.Galaxy.FACE_LEFT ) );
    end On_Key_Left;

    ----------------------------------------------------------------------------

    procedure On_Key_Right( this : not null access Tinker_Sprite'Class ) is
    begin
        this.Get_Tinker_View.Commands.Execute( Create_Command_Entity_Directive( this.entity.Get_Entity_Id, Directives.Galaxy.FACE_RIGHT ) );
    end On_Key_Right;

    ----------------------------------------------------------------------------

    procedure On_Key_Up( this : not null access Tinker_Sprite'Class ) is
    begin
        this.Get_Tinker_View.Commands.Execute( Create_Command_Entity_Directive( this.entity.Get_Entity_Id, Directives.Galaxy.FACE_UP ) );
    end On_Key_Up;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Exited( this : not null access Tinker_Sprite'Class ) is
    begin
        A_Tinker_Scene(this.parent).ToolPositionChanged.Emit;
    end On_Mouse_Exited;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Moved( this  : not null access Tinker_Sprite'Class;
                              mouse : Mouse_Arguments ) is
        tool : constant A_Tool := this.Get_Tinker_View.Get_Selected_Tool;
        dir  : Direction_Type;
    begin
        this.mouseX := mouse.x;
        this.mouseY := mouse.y;

        -- convert sprite widget coordinates to world coodinates
        this.Get_Tinker_View.Get_Scene.Notify_Tool_Position( this.geom.x + mouse.x * this.zoomFactor,
                                                             this.geom.y + mouse.y * this.zoomFactor );

        if this.mouseButtons(Mouse_Left) or this.mouseButtons(Mouse_Middle) then
            if tool.Acts_On( Entity_Subject ) then
                tool.Apply_To_Entity( (if this.mouseButtons(Mouse_Left) then Primary else Secondary),
                                      this.activeModifiers,
                                      False,
                                      Entity_Tool_Context'(this.entity,
                                                           mouse.x, mouse.y) );
            end if;

        -- update the cursor when the mouse passes over the resize handles
        else
            dir := this.Get_Resize_Direction( mouse.x, mouse.y );
            if dir = Dir_Up then
                this.Set_Mouse_Cursor( "RESIZE_N" );
            elsif dir = Dir_Down then
                this.Set_Mouse_Cursor( "RESIZE_N" );
            elsif dir = Dir_Left then
                this.Set_Mouse_Cursor( "RESIZE_W" );
            elsif dir = Dir_Right then
                this.Set_Mouse_Cursor( "RESIZE_E" );
            elsif dir = Dir_Up_Left then
                this.Set_Mouse_Cursor( "RESIZE_NW" );
            elsif dir = Dir_Up_Right then
                this.Set_Mouse_Cursor( "RESIZE_NE" );
            elsif dir = Dir_Down_Left then
                this.Set_Mouse_Cursor( "RESIZE_SW" );
            elsif dir = Dir_Down_Right then
                this.Set_Mouse_Cursor( "RESIZE_SE" );
            else
                this.Set_Mouse_Cursor( tool.Get_Cursor );
            end if;

        end if;
    end On_Mouse_Moved;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Pressed( this  : not null access Tinker_Sprite'Class;
                                mouse : Button_Arguments ) is
        tool : A_Tool;
    begin

        case mouse.button is
            -- left and middle mouse activate the primary and secondary tool functions
            when Mouse_Left | Mouse_Middle =>
                this.activeModifiers := mouse.modifiers;
                tool := this.Get_Tinker_View.Get_Selected_Tool;
                if tool.Acts_On( Entity_Subject ) then
                    tool.Apply_To_Entity( (if mouse.button = Mouse_Left then Primary else Secondary),
                                          mouse.modifiers,
                                          True,
                                          Entity_Tool_Context'(this.entity, mouse.x, mouse.y) );
                    if mouse.button = Mouse_Left and mouse.modifiers = MODIFIERS_NONE then
                        -- switch the info panel to show entity properties when
                        -- the left mouse is pressed without modifiers.
                        this.Show_Info_Panel;
                    end if;
                end if;

            -- right mouse shows the context menu
            when Mouse_Right =>
                this.menuX := mouse.x;
                this.menuY := mouse.y;

            when others => null;
        end case;

        -- freeze this entity while atleast one mouse button is held on it
        this.entity.Freeze_Updates( True );
    end On_Mouse_Pressed;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Released_Left( this  : not null access Tinker_Sprite'Class;
                                      mouse : Button_Arguments ) is
        tool : A_Tool;
    begin
        this.entity.Freeze_Updates( False );     -- corresponds with On_Mouse_Pressed()

        tool := this.Get_Tinker_View.Get_Selected_Tool;
        if tool.Acts_On( Entity_Subject ) then
            tool.Post_Apply_Entity( Primary,
                                    mouse.modifiers,
                                    Entity_Tool_Context'(this.entity, mouse.x, mouse.y) );
        end if;
    end On_Mouse_Released_Left;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Released_Middle( this  : not null access Tinker_Sprite'Class;
                                        mouse : Button_Arguments ) is
        tool : A_Tool;
    begin
        this.entity.Freeze_Updates( False );     -- corresponds with On_Mouse_Pressed()

        tool := this.Get_Tinker_View.Get_Selected_Tool;
        if tool.Acts_On( Entity_Subject ) then
            tool.Post_Apply_Entity( Secondary,
                                    mouse.modifiers,
                                    Entity_Tool_Context'(this.entity, mouse.x, mouse.y) );
        end if;
    end On_Mouse_Released_Middle;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Released_Right( this  : not null access Tinker_Sprite'Class;
                                       mouse : Button_Arguments ) is
        sx, sy : Float;
    begin
        this.entity.Freeze_Updates( False );     -- corresponds with On_Mouse_Pressed()

        this.Build_Menu;
        if this.menu /= null then
            -- when the popup menu is shown, the other mouse release events are
            -- consumed into the context menu (atleast on Windows 7), so emulate
            -- emulate it here.
            if this.mouseButtons(Mouse_Left) then
                this.On_Mouse_Released_Left( Button_Arguments'(x         => mouse.x,
                                                               y         => mouse.y,
                                                               button    => Mouse_Left,
                                                               modifiers => mouse.modifiers) );
            end if;
            if this.mouseButtons(Mouse_Middle) then
                this.On_Mouse_Released_Middle( Button_Arguments'(x         => mouse.x,
                                                                 y         => mouse.y,
                                                                 button    => Mouse_Middle,
                                                                 modifiers => mouse.modifiers) );
            end if;

            this.Translate_To_Window( this.menuX, this.menuY, sx, sy );
            this.menu.Show( sx, sy );
        end if;
    end On_Mouse_Released_Right;

    ----------------------------------------------------------------------------

    procedure On_Popup_Copy( this : not null access Tinker_Sprite'Class ) is
    begin
        this.Get_Tinker_View.Get_Scene.Copy_Selected_Entities;
    end On_Popup_Copy;

    ----------------------------------------------------------------------------

    procedure On_Popup_Delete( this : not null access Tinker_Sprite'Class ) is
    begin
        this.Get_Tinker_View.Commands.Execute( Create_Command_Delete_Entity( this.entity ) );
    end On_Popup_Delete;

    ----------------------------------------------------------------------------

    procedure On_Selected( this : not null access Tinker_Sprite'Class ) is
    begin
        if this.Get_Tinker_View.Get_Scene.Get_Entity_Selection_Count = 1 then
            this.Show_Info_Panel;
        end if;
    end On_Selected;

    ----------------------------------------------------------------------------

    procedure Show_Info_Panel( this : not null access Tinker_Sprite'Class ) is
    begin
        if this.infoPanel = null then
            this.infoPanel := A_Widget(Create_Entity_Module( this.Get_View,
                                                             this.entity.Get_Entity_Id,
                                                             this.entity.Get_Template,
                                                             this.entity.Get_Attributes ));
        end if;
        this.Get_Tinker_View.Show_Info_Panel( this.infoPanel );
    end Show_Info_Panel;

end Widgets.Sprites.Tinker;
