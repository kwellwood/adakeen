--
-- Copyright (c) 2017-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Widgets.Input_Boxes;               use Widgets.Input_Boxes;
with Widgets.Text_Boxes;                use Widgets.Text_Boxes;

limited with Game_Views.Tinker;

package Widgets.Panels.Console_Modules is

    type Console_Module is new Panel with private;
    type A_Console_Module is access all Console_Module'Class;

    function Create_Console_Module( view : not null access Game_Views.Tinker.Tinker_View'Class ) return A_Console_Module;
    pragma Postcondition( Create_Console_Module'Result /= null );

private

    type Console_Module is new Panel with
        record
            nextTaskId : Integer := 1;
            input      : A_Input_Box;
            output     : A_Textbox;
        end record;

    procedure Construct( this : access Console_Module;
                         view : not null access Game_Views.Game_View'Class );

    procedure Handle_Event( this : access Console_Module;
                            evt  : in out A_Event;
                            resp : out Response_Type );

end Widgets.Panels.Console_Modules;
