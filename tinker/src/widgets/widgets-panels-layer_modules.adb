--
-- Copyright (c) 2017-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Keyboard;                  use Allegro.Keyboard;
with Commands;                          use Commands;
--with Debugging;                         use Debugging;
with Events.World;                      use Events.World;
with Game_Views.Tinker;                 use Game_Views.Tinker;
with Icons;                             use Icons;
with Keyboard;                          use Keyboard;
with Maps;                              use Maps;
with Preferences;                       use Preferences;
with Values.Casting;                    use Values.Casting;
with Values.Construction;               use Values.Construction;
with Widgets.Buttons.Checkboxes;        use Widgets.Buttons.Checkboxes;
with Widgets.Buttons.Pushes;            use Widgets.Buttons.Pushes;
with Widgets.Buttons.Toggles;           use Widgets.Buttons.Toggles;
with Widgets.Labels;                    use Widgets.Labels;
with Widgets.Panels;                    use Widgets.Panels;
with Widgets.Panels.Properties.Layers;  use Widgets.Panels.Properties.Layers;

package body Widgets.Panels.Layer_Modules is

    ROW_HEIGHT : constant := 28.0;

    package Connections is new Signals.Connections(Layer_Module);
    use Connections;

    package Key_Connections is new Signals.Keys.Connections(Layer_Module);
    use Key_Connections;

    ----------------------------------------------------------------------------

    procedure Set_Layer( controls : in out Layer_Controls; layer : Integer ) is
    begin
        controls.layer := layer;
        controls.selected.Set_Attribute( "layer", Create( controls.layer ) );
        controls.visible.Set_Attribute( "layer", Create( controls.layer ) );
        A_Layer_Property_Module(controls.infoPanel).Set_Layer( controls.layer );
    end Set_Layer;

    ----------------------------------------------------------------------------

    -- Creates and inserts the control widgets for a new layer 'layer' at index
    -- 'index' in this.layers array.
    procedure Add_Layer_Controls( this     : not null access Layer_Module'Class;
                                  index    : Positive;
                                  layer    : Integer;
                                  props    : Map_Value;
                                  propDefs : Map_Value );

    -- Adds a new background layer.
    procedure Clicked_Add_Layer( this : not null access Layer_Module'Class );

    -- Shows/hides the clipping shapes layer.
    procedure Clicked_Clipping_Layer( this : not null access Layer_Module'Class );

    -- Deletes the active layer.
    procedure Clicked_Delete_Layer( this : not null access Layer_Module'Class );

    -- Swaps the active layer with the one behind it.
    procedure Clicked_Layer_Back( this : not null access Layer_Module'Class );

    -- Shows/hides outlines between populated (non-zero) tiles and empty (zero) tiles.
    procedure Clicked_Layer_Outlines( this : not null access Layer_Module'Class );

    -- Enables/disables tinting that indicates the active layer.
    procedure Clicked_Layer_Tint( this : not null access Layer_Module'Class );

    -- Swaps the active layer with the one in front of it.
    procedure Clicked_Layer_Front( this : not null access Layer_Module'Class );

    -- Selects the active layer.
    procedure Clicked_Select_Layer( this : not null access Layer_Module'Class );

    -- Shows/hides a layer.
    procedure Clicked_Show_Layer( this : not null access Layer_Module'Class );

    function Get_Tinker_View( this : not null access Layer_Module'Class ) return A_Tinker_View is (A_Tinker_View(this.Get_View));

    procedure On_Layer_Created( this : not null access Layer_Module'Class;
                                evt  : A_Layer_Created_Event );

    procedure On_Layer_Deleted( this : not null access Layer_Module'Class;
                                evt  : A_Layer_Event );

    procedure On_Layer_Property_Changed( this : not null access Layer_Module'Class;
                                         evt  : A_Layer_Property_Event );

    procedure On_Layers_Swapped( this : not null access Layer_Module'Class;
                                 evt  : A_Layer_Swap_Event );

    procedure On_World_Loaded( this : not null access Layer_Module'Class;
                               evt  : A_World_Loaded_Event );

    procedure Shortcut_Select_Layer( this : not null access Layer_Module'Class;
                                     key  : Key_Arguments );

    procedure Shortcut_Show_Layer( this : not null access Layer_Module'Class;
                                   key  : Key_Arguments );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Layer_Module( view  : not null access Game_Views.Tinker.Tinker_View'Class;
                                  scene : not null A_Tinker_Scene ) return A_Layer_Module is
        this : constant A_Layer_Module := new Layer_Module;
    begin
        this.Construct( A_Tinker_View(view), scene );
        return this;
    end Create_Layer_Module;

    ----------------------------------------------------------------------------

    procedure Add_Layer_Controls( this     : not null access Layer_Module'Class;
                                  index    : Positive;
                                  layer    : Integer;
                                  props    : Map_Value;
                                  propDefs : Map_Value ) is
        controls : Layer_Controls;
    begin
        controls.layer := layer;

        -- selected layer button
        controls.selected := Create_Toggle_Button( this.Get_View );
        controls.selected.Set_Text( props.Get_String( PROP_TITLE ) );
        controls.selected.Set_Style( "layers" );
        controls.selected.Height.Set( ROW_HEIGHT );
        controls.selected.Set_Focusable( False );
        controls.selected.Set_Attribute( "layer", controls.layer );
        controls.selected.Pressed.Connect( Slot( this, Clicked_Select_Layer'Access ) );
        this.selectedGroup.Add( controls.selected );

        -- layer visibility checkbox
        controls.visible := A_Button(Create_Checkbox( this.Get_View, "", "" ));
        controls.selected.Add_Child( controls.visible );
        controls.visible.Set_Style( "layers" );
        controls.visible.Set_Focusable( False );
        controls.visible.Fill_Vertical( controls.visible.Get_Parent );
        controls.visible.Right.Attach( controls.visible.Get_Parent.Right );
        controls.visible.Width.Set( ROW_HEIGHT );
        controls.visible.Set_Attribute( "layer", controls.layer );
        controls.visible.Set_State( True );
        controls.visible.Pressed.Connect( Slot( this, Clicked_Show_Layer'Access ) );
        controls.visible.Released.Connect( Slot( this, Clicked_Show_Layer'Access ) );

        -- layer properties info panel widget
        controls.infoPanel := A_Widget(Create_Layer_Property_Module( this.Get_Tinker_View,
                                                                     layer,
                                                                     propDefs,
                                                                     props ));

        this.layers.Insert( index, controls.selected );
        this.controls.Insert( index, controls );

        if this.layers.Count > 1 then
            if layer <= 0 then
                -- inserting a foreground layer
                this.foregroundLayer := this.foregroundLayer - 1;
                for i in 1..index-1 loop
                    controls := this.controls.Element( i );
                    Set_Layer( controls, controls.layer - 1 );
                    this.controls.Replace_Element( i, controls );
                end loop;
            else
                -- inserting a background layer
                for i in index+1..Integer(this.controls.Length) loop
                    controls := this.controls.Element( i );
                    Set_Layer( controls, controls.layer + 1 );
                    this.controls.Replace_Element( i, controls );
                end loop;
            end if;
        end if;

        this.Height.Set( this.Get_Preferred_Height );
    end Add_Layer_Controls;

    ----------------------------------------------------------------------------

    procedure Clicked_Add_Layer( this : not null access Layer_Module'Class ) is
    begin
        A_Tinker_View(this.view).Commands.Execute(
            Create_Command_Create_Layer( this.scene.Get_Background_Layer + 1,
                                         Layer_Scenery,
                                         null,
                                         Create_Map.Map )
        );
    end Clicked_Add_Layer;

    ----------------------------------------------------------------------------

    procedure Clicked_Delete_Layer( this : not null access Layer_Module'Class ) is
        layerData : constant A_Map := this.scene.Get_Layer_Data( this.scene.Get_Active_Layer );
    begin
        A_Tinker_View(this.view).Commands.Execute(
            Create_Command_Delete_Layer( this.scene.Get_Active_Layer,
                                         this.scene.Get_Layer_Type( this.scene.Get_Active_Layer ),
                                         layerData,    -- 'layerData' is consumed
                                         this.scene.Get_Layer_Properties( this.scene.Get_Active_Layer ) )
        );
    end Clicked_Delete_Layer;

    ----------------------------------------------------------------------------

    procedure Clicked_Clipping_Layer( this : not null access Layer_Module'Class ) is
        enabled : constant Boolean := A_Button(this.Signaller).Get_State;
    begin
        this.scene.Enable_Option( SHOW_CLIPPING, enabled );
        Set_Pref( "drawClipping", enabled );
    end Clicked_Clipping_Layer;

    ----------------------------------------------------------------------------

    procedure Clicked_Layer_Outlines( this : not null access Layer_Module'Class ) is
        enabled : constant Boolean := A_Button(this.Signaller).Get_State;
    begin
        this.scene.Enable_Option( SHOW_OUTLINES, enabled );
        Set_Pref( "drawOutlines", enabled );
    end Clicked_Layer_Outlines;

    ----------------------------------------------------------------------------

    procedure Clicked_Layer_Tint( this : not null access Layer_Module'Class ) is
        enabled : constant Boolean := A_Button(this.Signaller).Get_State;
    begin
        this.scene.Enable_Option( TINT_ACTIVE_LAYER, enabled );
        Set_Pref( "drawLayerTint", enabled );
        this.scene.Set_Active_Layer( this.scene.Get_Active_Layer );
    end Clicked_Layer_Tint;

    ----------------------------------------------------------------------------

    procedure Clicked_Select_Layer( this : not null access Layer_Module'Class ) is
        layer : constant Integer := A_Widget(this.Signaller).Get_Attribute( "layer" ).To_Int;
        index : constant Integer := 1 + layer - this.foregroundLayer;
    begin
        this.Select_Layer( layer );

        -- this check is necessary because this.controls may not be fully populated yet.
        if 1 <= index and index <= Integer(this.controls.Length) then
            this.Get_Tinker_View.Show_Info_Panel( this.controls.Element( index ).infoPanel );
        end if;
    end Clicked_Select_Layer;

    ----------------------------------------------------------------------------

    procedure Clicked_Show_Layer( this : not null access Layer_Module'Class ) is
        source : constant A_Button := A_Button(this.Signaller);
    begin
        this.scene.Set_Layer_Visible( source.Get_Attribute( "layer" ).To_Int,
                                      source.Get_State );
    end Clicked_Show_Layer;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this  : access Layer_Module;
                         view  : not null access Game_Views.Game_View'Class;
                         scene : A_Tinker_Scene ) is
        button : A_Button;
        filler : A_Panel;
        label  : A_Label;
    begin
        Panel(this.all).Construct( view, "", "Panel" );
        this.Listen_For_Event( EVT_WORLD_LOADED );
        this.Listen_For_Event( EVT_LAYER_CREATED );
        this.Listen_For_Event( EVT_LAYER_DELETED );
        this.Listen_For_Event( EVT_LAYER_PROPERTY_CHANGED );
        this.Listen_For_Event( EVT_LAYERS_SWAPPED );

        this.scene := scene;
        this.selectedGroup := Create_Button_Group;

        -- column of the layer controls
        this.layers := Create_Column_Layout( view );
        this.Add_Widget( this.layers );
        this.layers.Set_Style( "layers" );
        this.layers.Fill_Horizontal( this );
        this.layers.Top.Attach( this.Top );

        -- row of related option buttons
        this.options := Create_Row_Layout( view );
        this.Add_Widget( this.options );
        this.options.Set_Style_Property( "background.padTop", Create( 2 ) );
        this.options.Set_Style_Property( "background.padLeft", Create( 8 ) );
        this.options.Set_Style_Property( "background.padRight", Create( 8 ) );
        this.options.Set_Style_Property( "background.spacing", Create( 1 ) );
        this.options.Fill_Horizontal( this );
        this.options.Top.Attach( this.layers.Bottom );
        this.options.Height.Set( ROW_HEIGHT );

        this.options.Set_Style( "layerOptions" );

            this.addButton := Create_Push_Button( view );
            this.addButton.Set_Style( "layerOptions" );
            this.addButton.Set_Icon( Create_Icon( "tinker:plus-white-16" ) );
            this.addButton.Set_Enabled( False );
            this.addButton.Clicked.Connect( Slot( this, Clicked_Add_Layer'Access ) );
            this.options.Append( this.addButton );

            this.deleteButton := Create_Push_Button( view );
            this.deleteButton.Set_Style( "layerOptions" );
            this.deleteButton.Set_Icon( Create_Icon( "tinker:delete-white-16" ) );
            this.deleteButton.Set_Enabled( False );
            this.deleteButton.Clicked.Connect( Slot( this, Clicked_Delete_Layer'Access ) );
            this.options.Append( this.deleteButton );

            this.upButton := Create_Push_Button( view );
            this.upButton.Set_Style( "layerOptions" );
            this.upButton.Set_Icon( Create_Icon( "tinker:up-white-16" ) );
            this.upButton.Set_Enabled( False );
            this.upButton.Clicked.Connect( Slot( this, Clicked_Layer_Front'Access ) );
            this.options.Append( this.upButton );

            this.downButton := Create_Push_Button( view );
            this.downButton.Set_Style( "layerOptions" );
            this.downButton.Set_Icon( Create_Icon( "tinker:down-white-16" ) );
            this.downButton.Set_Enabled( False );
            this.downButton.Clicked.Connect( Slot( this, Clicked_Layer_Back'Access ) );
            this.options.Append( this.downButton );

            filler := Create_Panel( view );
            this.options.Append( filler );
            this.options.Set_Expander( A_Widget(filler) );

            label := Create_Label( view );
            label.Set_Style( "toolbarDiv" );
            label.Set_Preferred_Width( 7.0 );
            label.Height.Set( 16.0 );
            this.options.Append( label );

            button := Create_Toggle_Button( view );
            button.Set_Style( "layerOptions" );
            button.Set_Icon( Create_Icon( "tinker:layers-white-16" ) );
            button.Set_State( Get_Pref( "drawLayerTint" ) );
            button.Set_Enabled( False );
            button.Pressed.Connect( Slot( this, Clicked_Layer_Tint'Access ) );
            button.Released.Connect( Slot( this, Clicked_Layer_Tint'Access ) );
            this.options.Append( button );

            button := Create_Toggle_Button( view );
            button.Set_Style( "layerOptions" );
            button.Set_Icon( Create_Icon( "tinker:clipping" ) );
            button.Set_State( Get_Pref( "drawClipping" ) );
            button.Set_Enabled( False );
            button.Pressed.Connect( Slot( this, Clicked_Clipping_Layer'Access ) );
            button.Released.Connect( Slot( this, Clicked_Clipping_Layer'Access ) );
            this.options.Append( button );

            button := Create_Toggle_Button( view );
            button.Set_Style( "layerOptions" );
            button.Set_Icon( Create_Icon( "tinker:outlines" ) );
            button.Set_State( Get_Pref( "drawOutlines" ) );
            button.Set_Enabled( False );
            button.Pressed.Connect( Slot( this, Clicked_Layer_Outlines'Access ) );
            button.Released.Connect( Slot( this, Clicked_Layer_Outlines'Access ) );
            this.options.Append( button );

        this.Get_View.Get_Window.KeyTyped.Connect( Slot( this, Shortcut_Select_Layer'Access, ALLEGRO_KEY_PGUP, (others=>No) ) );
        this.Get_View.Get_Window.KeyTyped.Connect( Slot( this, Shortcut_Select_Layer'Access, ALLEGRO_KEY_PGDN, (others=>No) ) );

        this.Get_View.Get_Window.KeyTyped.Connect( Slot( this, Shortcut_Select_Layer'Access, ALLEGRO_KEY_1, (others=>No) ) );
        this.Get_View.Get_Window.KeyTyped.Connect( Slot( this, Shortcut_Select_Layer'Access, ALLEGRO_KEY_2, (others=>No) ) );
        this.Get_View.Get_Window.KeyTyped.Connect( Slot( this, Shortcut_Select_Layer'Access, ALLEGRO_KEY_3, (others=>No) ) );
        this.Get_View.Get_Window.KeyTyped.Connect( Slot( this, Shortcut_Select_Layer'Access, ALLEGRO_KEY_4, (others=>No) ) );
        this.Get_View.Get_Window.KeyTyped.Connect( Slot( this, Shortcut_Select_Layer'Access, ALLEGRO_KEY_5, (others=>No) ) );
        this.Get_View.Get_Window.KeyTyped.Connect( Slot( this, Shortcut_Select_Layer'Access, ALLEGRO_KEY_6, (others=>No) ) );
        this.Get_View.Get_Window.KeyTyped.Connect( Slot( this, Shortcut_Select_Layer'Access, ALLEGRO_KEY_7, (others=>No) ) );
        this.Get_View.Get_Window.KeyTyped.Connect( Slot( this, Shortcut_Select_Layer'Access, ALLEGRO_KEY_8, (others=>No) ) );
        this.Get_View.Get_Window.KeyTyped.Connect( Slot( this, Shortcut_Select_Layer'Access, ALLEGRO_KEY_9, (others=>No) ) );

        this.Get_View.Get_Window.KeyTyped.Connect( Slot( this, Shortcut_Show_Layer'Access, ALLEGRO_KEY_1, (CTRL=>Yes, others=>No) ) );
        this.Get_View.Get_Window.KeyTyped.Connect( Slot( this, Shortcut_Show_Layer'Access, ALLEGRO_KEY_2, (CTRL=>Yes, others=>No) ) );
        this.Get_View.Get_Window.KeyTyped.Connect( Slot( this, Shortcut_Show_Layer'Access, ALLEGRO_KEY_3, (CTRL=>Yes, others=>No) ) );
        this.Get_View.Get_Window.KeyTyped.Connect( Slot( this, Shortcut_Show_Layer'Access, ALLEGRO_KEY_4, (CTRL=>Yes, others=>No) ) );
        this.Get_View.Get_Window.KeyTyped.Connect( Slot( this, Shortcut_Show_Layer'Access, ALLEGRO_KEY_5, (CTRL=>Yes, others=>No) ) );
        this.Get_View.Get_Window.KeyTyped.Connect( Slot( this, Shortcut_Show_Layer'Access, ALLEGRO_KEY_6, (CTRL=>Yes, others=>No) ) );
        this.Get_View.Get_Window.KeyTyped.Connect( Slot( this, Shortcut_Show_Layer'Access, ALLEGRO_KEY_7, (CTRL=>Yes, others=>No) ) );
        this.Get_View.Get_Window.KeyTyped.Connect( Slot( this, Shortcut_Show_Layer'Access, ALLEGRO_KEY_8, (CTRL=>Yes, others=>No) ) );
        this.Get_View.Get_Window.KeyTyped.Connect( Slot( this, Shortcut_Show_Layer'Access, ALLEGRO_KEY_9, (CTRL=>Yes, others=>No) ) );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Layer_Module ) is
    begin
        Delete( this.selectedGroup );

        for controls of this.controls loop
            this.Get_Tinker_View.Hide_Info_Panel( controls.infoPanel );
            Delete( controls.infoPanel );
        end loop;

        Panel(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Delete_Layer_Controls( this  : not null access Layer_Module'Class;
                                     layer : Integer ) is
        index    : constant Integer := 1 + layer - this.foregroundLayer;
        btn      : A_Widget;
        controls : Layer_Controls;
    begin
        -- remove the layer selection button from the Column_Layout. The visible
        -- checkbox is a child of the button.
        btn := this.layers.Remove( index );
        Delete( btn );

        -- updated layer attributes of affected controls
        if layer < 0 then
            -- deleted a foreground layer
            for controls of this.controls loop
                if controls.layer < layer then
                    Set_Layer( controls, controls.layer + 1 );
                end if;
            end loop;
            this.foregroundLayer := this.foregroundLayer + 1;
        elsif layer > 0 then
            -- deleted a background layer
            for controls of this.controls loop
                if controls.layer > layer then
                    Set_Layer( controls, controls.layer - 1 );
                end if;
            end loop;
        end if;

        -- delete the controls record and its widgets
        controls := this.controls.Element( index );
        this.Get_Tinker_View.Hide_Info_Panel( controls.infoPanel );
        Delete( controls.infoPanel );
        -- controls.selected has already been deleted
        this.controls.Delete( index );

        this.Height.Set( this.Get_Preferred_Height );
    end Delete_Layer_Controls;

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Height( this : access Layer_Module ) return Float
    is (A_Widget(this.layers).Get_Min_Height + ROW_HEIGHT);

    ----------------------------------------------------------------------------

    overriding
    function Get_Preferred_Height( this : access Layer_Module ) return Float
    is (A_Widget(this.layers).Get_Preferred_Height + ROW_HEIGHT);

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Event( this : access Layer_Module;
                            evt  : in out A_Event;
                            resp : out Response_Type ) is
    begin
        if evt.Get_Event_Id = EVT_WORLD_LOADED then
            this.On_World_Loaded( A_World_Loaded_Event(evt) );
        elsif evt.Get_Event_Id = EVT_LAYER_CREATED then
            this.On_Layer_Created( A_Layer_Created_Event(evt) );
        elsif evt.Get_Event_Id = EVT_LAYER_DELETED then
            this.On_Layer_Deleted( A_Layer_Event(evt) );
        elsif evt.Get_Event_Id = EVT_LAYER_PROPERTY_CHANGED then
            this.On_Layer_Property_Changed( A_Layer_Property_Event(evt) );
        elsif evt.Get_Event_Id = EVT_LAYERS_SWAPPED then
            this.On_Layers_Swapped( A_Layer_Swap_Event(evt) );
        else
            Widget(this.all).Handle_Event( evt, resp );
        end if;
    end Handle_Event;

    ----------------------------------------------------------------------------

    procedure Clicked_Layer_Back( this : not null access Layer_Module'Class ) is
    begin
        if this.scene.Get_Active_Layer < this.scene.Get_Background_Layer then
            A_Tinker_View(this.view).Commands.Execute(
                Create_Command_Swap_Layers( this.scene.Get_Active_Layer,
                                            this.scene.Get_Active_Layer + 1 )
            );
        end if;
    end Clicked_Layer_Back;

    ----------------------------------------------------------------------------

    procedure On_Layer_Created( this : not null access Layer_Module'Class;
                                evt  : A_Layer_Created_Event ) is
    begin
        this.Add_Layer_Controls( 1 + evt.Get_Layer - this.foregroundLayer,
                                 evt.Get_Layer,
                                 evt.Get_Properties,
                                 evt.Get_Property_Defs );
        this.addButton.Set_Enabled( this.scene.Get_Layer_Count < Maps.MAX_LAYERS );
    end On_Layer_Created;

    ----------------------------------------------------------------------------

    procedure On_Layer_Deleted( this : not null access Layer_Module'Class;
                                evt  : A_Layer_Event ) is
    begin
        if evt.Get_Layer = this.scene.Get_Active_Layer then
            -- if the selected layer was deleted, change the selection to the
            -- next layer closer to 0.
            this.Select_Layer( evt.Get_Layer + (if evt.Get_Layer < 0 then 1 else -1) );
        end if;
        this.Delete_Layer_Controls( evt.Get_Layer );
        this.addButton.Set_Enabled( True );
    end On_Layer_Deleted;

    ----------------------------------------------------------------------------

    procedure On_Layers_Swapped( this : not null access Layer_Module'Class;
                                 evt  : A_Layer_Swap_Event ) is
        layerA    : constant Integer := evt.Get_Layer;
        layerB    : constant Integer := evt.Get_Other_Layer;
        indexA    : constant Integer := 1 + layerA - this.foregroundLayer;
        indexB    : constant Integer := 1 + layerB - this.foregroundLayer;
        controlsA : Layer_Controls := this.controls.Element( indexA );
        controlsB : Layer_Controls := this.controls.Element( indexB );
        temp      : A_Widget;
        pragma Warnings( Off, temp );
    begin
        Set_Layer( controlsA, layerB );
        Set_Layer( controlsB, layerA );

        this.controls.Swap( indexA, indexB );
        this.layers.Swap( indexA, indexB );

        if this.scene.Get_Active_Layer = evt.Get_Layer then
            this.Select_Layer( evt.Get_Other_Layer );
        elsif this.scene.Get_Active_Layer = evt.Get_Other_Layer then
            this.Select_Layer( evt.Get_Layer );
        end if;
    end On_Layers_Swapped;

    ----------------------------------------------------------------------------

    procedure Clicked_Layer_Front( this : not null access Layer_Module'Class ) is
    begin
        if this.scene.Get_Active_Layer > this.scene.Get_Foreground_Layer then
            A_Tinker_View(this.view).Commands.Execute(
                Create_Command_Swap_Layers( this.scene.Get_Active_Layer,
                                            this.scene.Get_Active_Layer - 1 )
            );
        end if;
    end Clicked_Layer_Front;

    ----------------------------------------------------------------------------

    procedure On_Layer_Property_Changed( this : not null access Layer_Module'Class;
                                         evt  : A_Layer_Property_Event ) is
        index : constant Integer := 1 + evt.Get_Layer - this.foregroundLayer;
    begin
        if evt.Get_Property_Name = PROP_TITLE then
            this.controls.Element( index ).selected.Set_Text( Cast_String( evt.Get_Value ) );
        end if;
    end On_Layer_Property_Changed;

    ----------------------------------------------------------------------------

    procedure On_World_Loaded( this : not null access Layer_Module'Class;
                               evt  : A_World_Loaded_Event ) is
    begin
        this.selectedGroup.Clear;
        this.layers.Clear;       -- deletes .selected and .visible controls

        for controls of this.controls loop
            this.Get_Tinker_View.Hide_Info_Panel( controls.infoPanel );
            Delete( controls.infoPanel );
        end loop;
        this.controls.Clear;

        this.foregroundLayer := 0;
        this.Add_Layer_Controls( 1, 0, evt.Get_Map.Get_Layer_Properties( 0 ),
                                 evt.Get_Map.Get_Layer_Property_Definitions( 0 ) );

        for layer in evt.Get_Map.Get_Foreground_Layer..evt.Get_Map.Get_Background_Layer loop
            if layer /= 0 then
                this.Add_Layer_Controls( 1 + (layer - evt.Get_Map.Get_Foreground_Layer),
                                         layer,
                                         evt.Get_Map.Get_Layer_Properties( layer ),
                                         evt.Get_Map.Get_Layer_Property_Definitions( layer ) );
            end if;
        end loop;

        -- select the middleground layer
        this.controls.Element( 1 - this.foregroundLayer ).selected.Set_State( True );

        -- enable the layer options buttons when first world loaded
        for i in 1..this.options.Count loop
            if this.options.Get_At( i ).all in Button'Class then
                A_Button(this.options.Get_At( i )).Set_Enabled( True );
            end if;
        end loop;

        -- selectively enable the add button
        this.addButton.Set_Enabled( evt.Get_Map.Get_Layer_Count < Maps.MAX_LAYERS );

        -- the delete button is not enabled because the scene sets layer 0 as
        -- active when a world is loaded.
        this.deleteButton.Set_Enabled( False );

        this.upButton.Set_Enabled( evt.Get_Map.Get_Foreground_Layer < 0 );
        this.downButton.Set_Enabled( 0 < evt.Get_Map.Get_Background_Layer );
    end On_World_Loaded;

    ----------------------------------------------------------------------------

    procedure Select_Layer( this  : not null access Layer_Module'Class;
                            layer : Integer ) is
        index : constant Integer := 1 + (layer - this.foregroundLayer);
    begin
        if 1 <= index and index <= this.layers.Count then
            this.scene.Set_Active_Layer( layer );
            this.controls.Element( index ).selected.Set_State( True );

            this.deleteButton.Set_Enabled( layer /= 0 and Cast_Boolean( this.scene.Get_Layer_Property( layer, PROP_DELETABLE ) ) );
            this.upButton.Set_Enabled( this.scene.Get_Foreground_Layer < layer );
            this.downButton.Set_Enabled( layer < this.scene.Get_Background_Layer );
        end if;
    end Select_Layer;

    ----------------------------------------------------------------------------

    procedure Shortcut_Select_Layer( this : not null access Layer_Module'Class;
                                     key  : Key_Arguments ) is
    begin
        case key.code is
            when ALLEGRO_KEY_PGUP => this.Select_Layer( Integer'Max( this.scene.Get_Active_Layer - 1, this.scene.Get_Foreground_Layer ) );
            when ALLEGRO_KEY_PGDN => this.Select_Layer( Integer'Min( this.scene.Get_Active_Layer + 1, this.scene.Get_Background_Layer ) );
            when ALLEGRO_KEY_1 |
                 ALLEGRO_KEY_2 |
                 ALLEGRO_KEY_3 |
                 ALLEGRO_KEY_4 |
                 ALLEGRO_KEY_5 |
                 ALLEGRO_KEY_6 |
                 ALLEGRO_KEY_7 |
                 ALLEGRO_KEY_8 |
                 ALLEGRO_KEY_9 =>
                this.Select_Layer( this.foregroundLayer + (key.code - ALLEGRO_KEY_1) );
            when others => pragma Assert( False );
        end case;
    end Shortcut_Select_Layer;

    ----------------------------------------------------------------------------

    procedure Shortcut_Show_Layer( this : not null access Layer_Module'Class;
                                   key  : Key_Arguments ) is
        index : constant Integer := 1 + (key.code - ALLEGRO_KEY_1);
        layer : constant Integer := this.foregroundLayer + (index - 1);
    begin
        if 1 <= index and index <= this.layers.Count then
            this.Show_Layer( layer, not this.scene.Is_Layer_Visible( layer ) );
        end if;
    end Shortcut_Show_Layer;

    ----------------------------------------------------------------------------

    procedure Show_Layer( this  : not null access Layer_Module'Class;
                          layer : Integer;
                          show  : Boolean ) is
        index : constant Integer := 1 + (layer - this.foregroundLayer);
    begin
        if 1 <= index and index <= this.layers.Count then
            this.scene.Set_Layer_Visible( layer, show );
            this.controls.Element( index ).visible.Set_State( show );
        end if;
    end Show_Layer;

end Widgets.Panels.Layer_Modules;
