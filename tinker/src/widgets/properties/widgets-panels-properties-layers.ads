--
-- Copyright (c) 2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Widgets.Buttons;                   use Widgets.Buttons;

limited with Game_Views.Tinker;

package Widgets.Panels.Properties.Layers is

    type Layer_Property_Module is new Property_Module with private;
    type A_Layer_Property_Module is access all Layer_Property_Module'Class;

    function Create_Layer_Property_Module( view     : not null access Game_Views.Tinker.Tinker_View'Class;
                                           layer    : Integer;
                                           propDefs : Map_Value;
                                           propVals : Map_Value ) return A_Layer_Property_Module;
    pragma Postcondition( Create_Layer_Property_Module'Result /= null );

    procedure Set_Layer( this  : not null access Layer_Property_Module'Class;
                         layer : Integer );

private

    type Layer_Property_Module is new Property_Module with
        record
            layer      : Integer := 0;
            btnMargins : A_Button;
        end record;

    procedure Construct( this     : access Layer_Property_Module;
                         view     : not null access Game_Views.Game_View'Class;
                         layer    : Integer;
                         propDefs : Map_Value;
                         propVals : Map_Value );

    procedure Delete( this : in out Layer_Property_Module );

    procedure Handle_Event( this : access Layer_Property_Module;
                            evt  : in out A_Event;
                            resp : out Response_Type );

    procedure On_Property_Edited( this     : access Layer_Property_Module;
                                  name     : String;
                                  oldValue : Value'Class;
                                  newValue : Value'Class );

end Widgets.Panels.Properties.Layers;
