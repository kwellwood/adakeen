--
-- Copyright (c) 2017-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Commands;                          use Commands;
--with Debugging;                         use Debugging;
with Entities.Factory;
with Entities.Templates;                use Entities.Templates;
with Events.Entities;                   use Events.Entities;
with Events.Entities.Attributes;        use Events.Entities.Attributes;
with Game_Views.Tinker;                 use Game_Views.Tinker;
with Icons;                             use Icons;
with Scene_Entities;                    use Scene_Entities;
with Values.Construction;               use Values.Construction;
with Values.Lists;                      use Values.Lists;
with Values.Strings;                    use Values.Strings;
with Values.Tagged_Ids;                 use Values.Tagged_Ids;
with Widgets.Buttons;                   use Widgets.Buttons;
with Widgets.Buttons.Pushes;            use Widgets.Buttons.Pushes;
with Widgets.Labels;                    use Widgets.Labels;

package body Widgets.Panels.Properties.Entities is

    package Connections is new Signals.Connections(Entity_Module);
    use Connections;

    ----------------------------------------------------------------------------

    function Get_Tinker_View( this : not null access Entity_Module'Class ) return A_Tinker_View is (A_Tinker_View(this.Get_View));

    procedure On_Clicked_Delete( this : not null access Entity_Module'Class );

    procedure On_Clicked_Find( this : not null access Entity_Module'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Entity_Module( view     : not null access Game_Views.Game_View'Class;
                                   eid      : Entity_Id;
                                   template : String;
                                   attrs    : Map_Value ) return A_Entity_Module is
        this : constant A_Entity_Module := new Entity_Module;
    begin
        this.Construct( view, eid, template, attrs );
        return this;
    end Create_Entity_Module;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this       : access Entity_Module;
                         view       : not null access Game_Views.Game_View'Class;
                         eid        : Entity_Id;
                         template   : String;
                         attributes : Map_Value ) is
        eTemplate   : constant A_Template := Standard.Entities.Factory.Global.Get_Template( template );
        propDefs    : constant Map_Value := Create_Map.Map;
        attrDefs    : Map_Value;
        propDef     : Map_Value;
        label       : A_Label;
        button      : A_Button;
        row         : A_Row_Layout;
        editorAttrs : List_Value;
        entity      : A_Entity;
    begin
        if eTemplate /= null then
            attrDefs := eTemplate.Get_Attribute_Definitions;
        else
            attrDefs := Create_Map.Map;
        end if;

        -- add just the attributes listed in the "editorAttributes" attribute
        editorAttrs := attributes.Get( "editorAttributes" ).Lst;
        if editorAttrs.Valid then
            for i in 1..editorAttrs.Length loop
                propDef := Create_Map( (Pair( "value", attributes.Get( editorAttrs.Get_String( i ) ) ),
                                        Pair( "displayOrder", Create( i ) )) ).Map;
                propDef.Merge( attrDefs.Get( editorAttrs.Get_String( i ) ) );
                propDefs.Set( editorAttrs.Get_String( i ), propDef );
            end loop;
        end if;

        Property_Module(this.all).Construct( view, template & " Entity", propDefs );
        this.Listen_For_Event( EVT_ENTITY_ATTRIBUTE_CHANGED );

        this.eId := eid;

        label := Create_Label( view );
        label.Set_Style( "propertyModule" );
        label.Height.Set( ROW_HEIGHT );
        label.Set_Text( "Actions:" );
        this.leftCol.Insert( 1, label );

        row := Create_Row_Layout( view );
        row.Height.Set( ROW_HEIGHT );
        row.Set_Style_Property( "background.align", Create( "left" ) );

            button := Create_Push_Button( view );
            button.Set_Style( "entityActions" );
            button.Set_Icon( Create_Icon( "tinker:search-white-16" ) );
            button.Clicked.Connect( Slot( this, On_Clicked_Find'Access ) );
            row.Append( button );

            entity := this.Get_Tinker_View.Get_Scene.Find_Entity( eid );
            if entity /= null and then entity.Is_Deletable then
                button := Create_Push_Button( view );
                button.Set_Style( "entityActions" );
                button.Set_Icon( Create_Icon( "tinker:delete-white-16" ) );
                button.Clicked.Connect( Slot( this, On_Clicked_Delete'Access ) );
                row.Append( button );
            end if;

        this.rightCol.Insert( 1, row );

        -- update the fixed width of the name column based on its contents
        this.leftCol.Width.Set( A_Widget(this.leftCol).Get_Preferred_Width );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Event( this : access Entity_Module;
                            evt  : in out A_Event;
                            resp : out Response_Type ) is
        pragma Unreferenced( resp );
    begin
        if evt.Get_Event_Id = EVT_ENTITY_ATTRIBUTE_CHANGED then
            if A_Entity_Attribute_Event(evt).Get_Id = this.eId then
                this.Handle_Property_Changed( A_Entity_Attribute_Event(evt).Get_Attribute,
                                              A_Entity_Attribute_Event(evt).Get_Value );
            end if;
        end if;
    end Handle_Event;

    ----------------------------------------------------------------------------

    procedure On_Clicked_Delete( this : not null access Entity_Module'Class ) is
        entity : constant A_Entity := this.Get_Tinker_View.Get_Scene.Find_Entity( this.eId );
    begin
        if entity /= null then
            this.Get_Tinker_View.Commands.Execute( Create_Command_Delete_Entity( entity ) );
        end if;
    end On_Clicked_Delete;

    ----------------------------------------------------------------------------

    procedure On_Clicked_Find( this : not null access Entity_Module'Class ) is
        entity : constant A_Entity := this.Get_Tinker_View.Get_Scene.Find_Entity( this.eId );
    begin
        if entity /= null then
            this.Get_Tinker_View.Get_Scene.Set_Focus( entity.Get_Entity_X, entity.Get_Entity_Y );
        end if;
    end On_Clicked_Find;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Property_Edited( this     : access Entity_Module;
                                  name     : String;
                                  oldValue : Value'Class;
                                  newValue : Value'Class ) is
    begin
        this.Get_Tinker_View.Commands.Execute(
            Create_Command_Set_Entity_Attribute( this.eId,
                                                 name,
                                                 oldValue,
                                                 newValue ) );
    end On_Property_Edited;

end Widgets.Panels.Properties.Entities;
