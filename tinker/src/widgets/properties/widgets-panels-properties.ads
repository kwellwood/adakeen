--
-- Copyright (c) 2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Indefinite_Ordered_Maps;
with Widgets.Row_Layouts;               use Widgets.Row_Layouts;

limited with Game_Views;

package Widgets.Panels.Properties is

    type Property_Module is new Panel with private;
    type A_Property_Module is access all Property_Module'Class;

private

    ROW_HEIGHT : constant := 24.0;

    package Control_Maps is new Ada.Containers.Indefinite_Ordered_Maps( String, A_Widget, "<", "=" );
    use Control_Maps;

    type Property_Module is new Panel with
        record
            props    : Map_Value;
            leftCol  : A_Column_Layout;
            rightCol : A_Column_Layout;
            controls : Control_Maps.Map;    -- maps property names to control widgets
        end record;

    -- Constructs the properties editor module for editing properties 'props'
    -- in a module under the header 'title'. The format of 'propDefs' is as
    -- follows:
    --
    -- {
    --     "<prop1>": {
    --         "value": <prop1_val>,
    --         "<attr1>": <attr1_val>,
    --         "<attr2>": ...
    --     },
    --     "<prop2>": ...
    -- }
    --
    -- Supported attributes are:
    --   displayName (string) - A nicer, human readable name for the property.
    --   displayOrder (number) - Number for ordering display of properties (ascending).
    --   readonly (boolean) - The value cannot be edited when True.
    --
    procedure Construct( this     : access Property_Module;
                         view     : not null access Game_Views.Game_View'Class;
                         title    : String;
                         propDefs : Map_Value );

    function Get_Min_Height( this : access Property_Module ) return Float;

    function Get_Preferred_Height( this : access Property_Module ) return Float;

    -- Returns the widget that controls property 'name', or null if property is
    -- not found.
    function Get_Property_Control( this : not null access Property_Module'Class;
                                   name : String ) return A_Widget;

    -- Call this when the value of a property is changed outside the property
    -- module. This will update the value of appropriate control widget.
    procedure Handle_Property_Changed( this : not null access Property_Module'Class;
                                       name : String;
                                       val  : Value'Class );

    -- Implement this procedure to receive a notification when the user has
    -- changed the value of property 'name' from 'oldValue' to 'newValue'.
    not overriding
    procedure On_Property_Edited( this     : access Property_Module;
                                  name     : String;
                                  oldValue : Value'Class;
                                  newValue : Value'Class ) is null;

end Widgets.Panels.Properties;
