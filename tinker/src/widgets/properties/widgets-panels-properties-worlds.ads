--
-- Copyright (c) 2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Widgets.Buttons;                   use Widgets.Buttons;

limited with Game_Views.Tinker;

package Widgets.Panels.Properties.Worlds is

    type World_Module is new Property_Module with private;
    type A_World_Module is access all World_Module'Class;

    function Create_World_Module( view : not null access Game_Views.Tinker.Tinker_View'Class ) return A_World_Module;
    pragma Postcondition( Create_World_Module'Result /= null );

private

    type World_Module is new Property_Module with
        record
            btnMusic : A_Button;
        end record;

    procedure Construct( this : access World_Module;
                         view : not null access Game_Views.Game_View'Class );

    procedure Handle_Event( this : access World_Module;
                            evt  : in out A_Event;
                            resp : out Response_Type );

    procedure On_Property_Edited( this     : access World_Module;
                                  name     : String;
                                  oldValue : Value'Class;
                                  newValue : Value'Class );

end Widgets.Panels.Properties.Worlds;
