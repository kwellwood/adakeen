--
-- Copyright (c) 2017-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Commands;                          use Commands;
--with Debugging;                         use Debugging;
with Events.World;                      use Events.World;
with Game_Views.Tinker;                 use Game_Views.Tinker;
with Icons;                             use Icons;
with Maps;                              use Maps;
with Support;                           use Support;
with Values.Lists;                      use Values.Lists;
with Values.Strings;                    use Values.Strings;
with Widgets.Buttons.Pushes;            use Widgets.Buttons.Pushes;
with Widgets.Input_Boxes;               use Widgets.Input_Boxes;
with Widgets.Labels;                    use Widgets.Labels;
with Widgets.Row_Layouts;               use Widgets.Row_Layouts;
with Widgets.Scenes.Tinker;             use Widgets.Scenes.Tinker;

package body Widgets.Panels.Properties.Layers is

    package Connections is new Signals.Connections(Layer_Property_Module);
    use Connections;

    ----------------------------------------------------------------------------

    function Get_Tinker_View( this : not null access Layer_Property_Module'Class ) return A_Tinker_View is (A_Tinker_View(this.Get_View));

    ----------------------------------------------------------------------------

    procedure On_Clicked_Set_Margins( this : not null access Layer_Property_Module'Class );

    procedure On_Tile_Area_Changed( this : not null access Layer_Property_Module'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Layer_Property_Module( view     : not null access Game_Views.Tinker.Tinker_View'Class;
                                           layer    : Integer;
                                           propDefs : Map_Value;
                                           propVals : Map_Value ) return A_Layer_Property_Module is
        this : constant A_Layer_Property_Module := new Layer_Property_Module;
    begin
        this.Construct( A_Tinker_View(view), layer, propDefs, propVals );
        return this;
    end Create_Layer_Property_Module;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this     : access Layer_Property_Module;
                         view     : not null access Game_Views.Game_View'Class;
                         layer    : Integer;
                         propDefs : Map_Value;
                         propVals : Map_Value ) is
        props : constant Map_Value := Create_Map.Map;
        prop  : Map_Value;
        names : List_Value;
        row   : A_Row_Layout;
        label : A_Label;
    begin
        names := propVals.Get_Keys.Lst;
        for i in 1..names.Length loop
            prop := Create_Map.Map;
            prop.Merge( propDefs.Get( names.Get_String( i ) ) );
            prop.Set( "value", propVals.Get( names.Get_String( i ) ) );
            props.Set( names.Get_String( i ), prop, consume => True );
        end loop;

        Property_Module(this.all).Construct( view, "Layer Properties", props );
        this.Listen_For_Event( EVT_LAYER_PROPERTY_CHANGED );

        this.layer := layer;

        label := Create_Label( view );
        label.Set_Style( "propertyModule" );
        label.Height.Set( ROW_HEIGHT );
        label.Set_Text( "Actions:" );
        this.leftCol.Insert( 1, label );

        row := Create_Row_Layout( view );
        row.Height.Set( ROW_HEIGHT );
        row.Set_Style_Property( "background.align", Create( "left" ) );

            if this.Get_Property_Control( Maps.PROP_MARGINT ) /= null then
                this.btnMargins := Create_Push_Button( view );
                this.btnMargins.Set_Style( "layerActions" );
                this.btnMargins.Set_Icon( Create_Icon( "tinker:crop-white-16" ) );
                this.btnMargins.Set_Enabled( this.Get_Tinker_View.Get_Scene.Is_Tile_Area_Active );
                this.btnMargins.Clicked.Connect( Slot( this, On_Clicked_Set_Margins'Access ) );
                row.Append( this.btnMargins );
            end if;

        this.rightCol.Insert( 1, row );

        this.Get_Tinker_View.Get_Scene.TileAreaChanged.Connect( Slot( this, On_Tile_Area_Changed'Access ) );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Layer_Property_Module ) is
    begin
        if this.Get_Tinker_View.Get_Scene /= null then
            this.Get_Tinker_View.Get_Scene.TileAreaChanged.Disconnect( Slot( this, On_Tile_Area_Changed'Access ) );
        end if;
        Property_Module(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Event( this : access Layer_Property_Module;
                            evt  : in out A_Event;
                            resp : out Response_Type ) is
        pragma Unreferenced( resp );
    begin
        if evt.Get_Event_Id = EVT_LAYER_PROPERTY_CHANGED then
            if A_Layer_Property_Event(evt).Get_Layer = this.layer then
                this.Handle_Property_Changed( A_Layer_Property_Event(evt).Get_Property_Name,
                                              A_Layer_Property_Event(evt).Get_Value );
            end if;
        end if;
    end Handle_Event;

    ----------------------------------------------------------------------------

    procedure On_Clicked_Set_Margins( this : not null access Layer_Property_Module'Class ) is

        ------------------------------------------------------------------------

        procedure Set_Property( name : String; value : Integer ) is
            input : constant A_Widget := this.Get_Property_Control( name );
        begin
            if input /= null and then input.all in Input_Box'Class then
                A_Input_Box(input).Set_Text( Image( value ), acceptIt => True );
            end if;
        end Set_Property;

        ------------------------------------------------------------------------

        scene : constant A_Tinker_Scene := this.Get_Tinker_View.Get_Scene;
        area  : constant Rectangle := scene.Get_Tile_Area;
    begin
        Set_Property( Maps.PROP_MARGINL, Integer(area.x) );
        Set_Property( Maps.PROP_MARGINT, Integer(area.y) );
        Set_Property( Maps.PROP_MARGINR, (scene.Get_Width_Tiles  * scene.Get_Tile_Width) - Integer(area.x + area.width ) );
        Set_Property( Maps.PROP_MARGINB, (scene.Get_Height_Tiles * scene.Get_Tile_Width) - Integer(area.y + area.height) );
    end On_Clicked_Set_Margins;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Property_Edited( this     : access Layer_Property_Module;
                                  name     : String;
                                  oldValue : Value'Class;
                                  newValue : Value'Class ) is
    begin
        this.Get_Tinker_View.Commands.Execute( Create_Command_Set_Layer_Property( this.layer, name, oldValue, newValue ) );
    end On_Property_Edited;

    ----------------------------------------------------------------------------

    procedure On_Tile_Area_Changed( this : not null access Layer_Property_Module'Class ) is
    begin
        if this.btnMargins /= null then
            this.btnMargins.Set_Enabled( this.Get_Tinker_View.Get_Scene.Is_Tile_Area_Active );
        end if;
    end On_Tile_Area_Changed;

    ----------------------------------------------------------------------------

    procedure Set_Layer( this  : not null access Layer_Property_Module'Class;
                         layer : Integer ) is
    begin
        this.layer := layer;
    end Set_Layer;

end Widgets.Panels.Properties.Layers;
