--
-- Copyright (c) 2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Icons;                             use Icons;

limited with Game_Views;

package Widgets.Panels.Properties.Tiles is

    type Tile_Module is new Property_Module with private;
    type A_Tile_Module is access all Tile_Module'Class;

    function Create_Tile_Module( view : not null access Game_Views.Game_View'Class;
                                 tile : Icon_Type ) return A_Tile_Module;
    pragma Postcondition( Create_Tile_Module'Result /= null );

private

    type Tile_Module is new Property_Module with
        record
            tile : Icon_Type;
        end record;

    procedure Construct( this : access Tile_Module;
                         view : not null access Game_Views.Game_View'Class;
                         tile : Icon_Type );

end Widgets.Panels.Properties.Tiles;
