--
-- Copyright (c) 2017-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Ada.Containers.Generic_Array_Sort;
with Ada.Strings.Equal_Case_Insensitive;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Debugging;                         use Debugging;
with Game_Views.Tinker;                 use Game_Views.Tinker;
with Keyboard;                          use Keyboard;
with Support;                           use Support;
with Values.Casting;                    use Values.Casting;
with Values.Construction;               use Values.Construction;
with Values.Lists;                      use Values.Lists;
with Values.Strings;                    use Values.Strings;
with Widgets.Buttons;                   use Widgets.Buttons;
with Widgets.Buttons.Checkboxes;        use Widgets.Buttons.Checkboxes;
with Widgets.Enumerations;              use Widgets.Enumerations;
with Widgets.Input_Boxes;               use Widgets.Input_Boxes;
with Widgets.Input_Boxes.Constraints;   use Widgets.Input_Boxes.Constraints;
with Widgets.Labels;                    use Widgets.Labels;
with Widgets.Sliders;                   use Widgets.Sliders;
with Widgets.Spinners;                  use Widgets.Spinners;

package body Widgets.Panels.Properties is

    package Connections is new Signals.Connections(Property_Module);
    use Connections;

    package Key_Connections is new Signals.Keys.Connections(Property_Module);
    use Key_Connections;

    ----------------------------------------------------------------------------

    function To_Input_String( val : Value'Class ) return String is (Cast_String( val, Image( val ) ));

    ----------------------------------------------------------------------------

    function Get_Tinker_View( this : not null access Property_Module'Class ) return A_Tinker_View is (A_Tinker_View(this.Get_View));

    procedure On_Check_Changed( this : not null access Property_Module'Class );

    procedure On_Enum_Changed( this : not null access Property_Module'Class );

    procedure On_Input_Accepted( this : not null access Property_Module'Class );

    procedure On_Input_Blurred( this : not null access Property_Module'Class );

    procedure On_Input_Escaped( this : not null access Property_Module'Class );

    procedure On_Slider_Changed( this : not null access Property_Module'Class );

    procedure On_Spinner_Changed( this : not null access Property_Module'Class );

    procedure Update_From_Input( this    : not null access Property_Module'Class;
                                 control : not null A_Input_Box );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    not overriding
    procedure Construct( this     : access Property_Module;
                         view     : not null access Game_Views.Game_View'Class;
                         title    : String;
                         propDefs : Map_Value ) is
        firstControl : A_Widget;
        nextControl  : A_Widget;
        prevControl  : A_Widget;

        ------------------------------------------------------------------------

        function Make_Displayable( name : String ) return String is
            result : Unbounded_String;
        begin
            if name'Length = 0 then
                return "";
            end if;
            for i in name'Range loop
                if i = name'First then
                    -- capitalize the first letter
                    Append( result, To_Upper( name(i) ) );
                else
                    -- preserve other capitalization
                    Append( result, name(i) );
                end if;
                if i < name'Last and then name(i+1) = To_Upper( name(i+1) ) then
                    -- spaces before capital letters
                    Append( result, " " );
                end if;
            end loop;
            return To_String( result );
        end Make_Displayable;

        ------------------------------------------------------------------------

        procedure Add_Property( name : String; propDef : Value ) is
            val     : Value;
            label   : A_Label;
            slider  : A_Slider;
            enum    : A_Enumeration;
            spinner : A_Spinner;
            check   : A_Checkbox;
            input   : A_Input_Box;
        begin
            if not propDef.Is_Map then
                return;
            end if;

            val := propDef.Map.Get( "value" );
            if val.Is_Null then
                return;
            end if;

            label := Create_Label( view );
            label.Set_Style( "propertyModule" );
            label.Height.Set( ROW_HEIGHT );
            label.Set_Text( propDef.Map.Get_String( "displayName", Make_Displayable( name ) ) & ":" );
            this.leftCol.Append( label );

            -- number range
            if propDef.Map.Get_String( "control" ) = "slider" then
                slider := Create_Slider( view );
                slider.Set_Style( "propertyModule" );
                slider.Height.Set( ROW_HEIGHT );
                slider.Set_Range( propDef.Map.Get_Float( "min", Cast_Float( val ) - 1.0 ),
                                  propDef.Map.Get_Float( "max", Cast_Float( val ) + 1.0 ) );
                slider.Set_Value( Constrain( Cast_Float( val, slider.Get_Left ), slider.Get_Left, slider.Get_Right ) );
                if propDef.Map.Get_Int( "steps" ) > 1 then
                    slider.Set_Steps( propDef.Map.Get_Int( "steps" ) );
                elsif propDef.Map.Get_Int( "stepsHint" ) > 1 then
                    slider.Set_Steps_Hint( propDef.Map.Get_Int( "stepsHint" ) );
                end if;
                slider.Set_Enabled( not propDef.Map.Get_Boolean( "readonly" ) );
                slider.Enable_Continuous_Change( slider.Get_Steps > 1 );    -- only when stepped, otherwise too many commands fill the history
                slider.Changed.Connect( Slot( this, On_Slider_Changed'Access ) );
                nextControl := A_Widget(slider);

            -- constrained number
            elsif propDef.Map.Get_String( "control" ) = "spinner" then
                spinner := Create_Spinner( view );
                spinner.Set_Style( "propertyModule" );
                spinner.Height.Set( ROW_HEIGHT );
                spinner.Set_Range( propDef.Map.Get_Float( "min", Cast_Float( val ) - 1.0 ),
                                   propDef.Map.Get_Float( "max", Cast_Float( val ) + 1.0 ) );
                spinner.Set_Increment( propDef.Map.Get_Float( "increment", 1.0 ) );
                spinner.Enable_Discrete( propDef.Map.Get_Boolean( "discrete", False ) );
                spinner.Set_Value( Cast_Float( val ) );
                spinner.Set_Enabled( not propDef.Map.Get_Boolean( "readonly" ) );
                spinner.Changed.Connect( Slot( this, On_Spinner_Changed'Access ) );
                nextControl := A_Widget(spinner);

            -- enumeration
            elsif propDef.Map.Get_String( "control" ) = "enum" then
                enum := Create_Enum( view );
                enum.Set_Style( "propertyModule" );
                enum.Height.Set( ROW_HEIGHT );
                declare
                    procedure Add_Item( text : String; val : Value ) is
                    begin
                        enum.Add_Item( text, val );
                    end Add_Item;

                    items : constant Value := propDef.Map.Get( "items" );
                    list  : List_Value;
                begin
                    if items.Is_Map then
                        items.Map.Iterate( Add_Item'Access );
                    elsif items.Is_String and then items.Str = "<<emitters>>" then
                        Add_Item( "", Create( "" ) );        -- no chosen emitter
                        list := this.Get_Tinker_View.Get_Scene.Get_Particle_System.Get_Emitter_Names;
                        for i in 1..list.Length loop
                            Add_Item( list.Get_String( i ), list.Get( i ) );
                        end loop;
                    end if;
                end;
                enum.Set_Index( 1 );
                enum.Select_Value( propDef.Map.Get( "value" ) );
                enum.SelectionChanged.Connect( Slot( this, On_Enum_Changed'Access ) );
                nextControl := A_Widget(enum);

            -- boolean
            elsif val.Is_Boolean then
                check := Create_Checkbox( view );
                check.Set_Style( "propertyModule" );
                check.Height.Set( ROW_HEIGHT );
                check.Set_State( val.To_Boolean );
                check.Set_Enabled( not propDef.Map.Get_Boolean( "readonly" ) );
                check.Pressed.Connect( Slot( this, On_Check_Changed'Access ) );
                check.Released.Connect( Slot( this, On_Check_Changed'Access ) );
                nextControl := A_Widget(check);

            -- readonly (label)
            elsif propDef.Map.Get_Boolean( "readonly" ) then
                label := Create_Label( view );
                label.Set_Style( "propertyModule" );
                label.Height.Set( ROW_HEIGHT );
                label.Set_Text( To_Input_String( val ) );
                nextControl := A_Widget(label);

            -- string, number, etc.
            else
                input := Create_Input_Box( view );
                input.Set_Style( "propertyModule" );
                input.Height.Set( ROW_HEIGHT );
                if val.Is_Number then
                    input.Set_Constraint( Number_Only'Access );
                    input.Set_Attribute( "min", propDef.Map.Get( "min" ) );
                    input.Set_Attribute( "max", propDef.Map.Get( "max" ) );
                end if;
                input.Enable_Select_All_On_Focus( True );
                input.Set_Text( To_Input_String( val ) );
                input.Accepted.Connect( Slot( this, On_Input_Accepted'Access ) );
                input.KeyPressed.Connect( Slot( this, On_Input_Escaped'Access, ALLEGRO_KEY_ESCAPE, (others => No) ) );
                input.Blurred.Connect( Slot( this, On_Input_Blurred'Access ) );
                nextControl := A_Widget(input);

            end if;

            this.controls.Insert( name, nextControl );
            nextControl.Set_Attribute( "name", name );
            this.props.Set( name, val );
            this.rightCol.Append( nextControl );

            if nextControl.Accepts_Focus then
                if prevControl = null then
                    firstControl := nextControl;
                else
                    prevControl.Set_Next( nextControl.Get_Id );
                    nextControl.Set_Prev( prevControl.Get_Id );
                end if;
                prevControl := nextControl;
            end if;
        end Add_Property;

        ------------------------------------------------------------------------

        spacing : Float;
        label   : A_Label;
    begin
        Panel(this.all).Construct( view, "", "Panel" );

        -- contains only property names and values. properties will be written
        -- one at a time by Add_Property().
        this.props := Create_Map.Map;

        this.Set_Style( "propertyModule" );
        this.Set_Style_Property( "background.spacing", Create( 4 ) );
        this.Set_Style_Property( "background.padLeft", Create( 4 ) );
        this.Set_Style_Property( "background.padRight", Create( 4 ) );
        this.Set_Style_Property( "background.padTop", Create( 4 ) );
        this.Set_Style_Property( "background.padBottom", Create( 4 ) );

        spacing := this.style.Get_Spacing( BACKGROUND_ELEM );

        label := Create_Label( view );
        this.Add_Widget( label );
        label.Set_Style( "propertyModuleTitle" );
        label.Top.Attach( this.Top );
        label.Fill_Horizontal( this );
        label.Height.Set( ROW_HEIGHT );
        label.Set_Text( title );

        this.leftCol := Create_Column_Layout( view );
        this.Add_Widget( this.leftCol );
        this.leftCol.Left.Attach( this.Left );
        this.leftCol.Top.Attach( label.Bottom, spacing );
        this.leftCol.Set_Style_Property( "background.spacing", Create( spacing ) );
        --this.leftCol.Set_Style_Property( "background.align", Create( "right" ) );

        this.rightCol := Create_Column_Layout( view );
        this.Add_Widget( this.rightCol );
        this.rightCol.Left.Attach( this.leftCol.Right, spacing );
        this.rightCol.Top.Attach( this.leftCol.Top );
        this.rightCol.Right.Attach( this.Right );
        this.rightCol.Set_Style_Property( "background.spacing", Create( spacing ) );
        this.rightCol.Set_Style_Property( "background.align", Create( "center" ) );

        -- sort and add the properties in 'propDefs'
        declare

            type Prop_Name is
                record
                    order : Integer := Integer'Last;  -- primary sort criteria
                    name  : Unbounded_String;         -- secondary sort criteria
                end record;
            type Prop_Name_Array is array (Integer range <>) of Prop_Name;

            function "<"( l, r : Prop_Name ) return Boolean is (l.order < r.order or else (l.order = r.order and then l.name < r.name));

            procedure Sort is new Ada.Containers.Generic_Array_Sort( Integer, Prop_Name, Prop_Name_Array, "<" );

            -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

            sortedNames : Prop_Name_Array(1..propDefs.Size);
            nameIndex   : Integer := sortedNames'First;

            procedure Examine( name : String; propDef : Value ) is
            begin
                if not propDef.Is_Map or else not propDef.Map.Get_Boolean( "visible", True ) then
                    return;
                end if;
                sortedNames(nameIndex).name := To_Unbounded_String( name );
                sortedNames(nameIndex).order := propDef.Map.Get_Int( "displayOrder", sortedNames(nameIndex).order );
                nameIndex := nameIndex + 1;
            end Examine;

        begin
            -- populate the property name array for sorting
            propDefs.Iterate( Examine'Access );

            -- sort the property names
            Sort( sortedNames );

            -- add all the properties in order
            for i in sortedNames'Range loop
                Add_Property( To_String( sortedNames(i).name ), propDefs.Get( sortedNames(i).name ) );
            end loop;
        end;

        -- close the focus loop
        if firstControl /= null and nextControl /= firstControl then
            firstControl.Set_Prev( nextControl.Get_Id );
            nextControl.Set_Next( firstControl.Get_Id );
        end if;

        -- update the fixed width of the name column based on its contents
        this.leftCol.Width.Set( A_Widget(this.leftCol).Get_Preferred_Width );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Height( this : access Property_Module ) return Float is
    (
        this.style.Get_Pad_Top( BACKGROUND_ELEM ) +
        ROW_HEIGHT +
        this.style.Get_Spacing( BACKGROUND_ELEM ) +
        A_Widget(this.leftCol).Get_Min_Height +
        this.style.Get_Pad_Bottom( BACKGROUND_ELEM )
    );

    ----------------------------------------------------------------------------

    overriding
    function Get_Preferred_Height( this : access Property_Module ) return Float is
    (
        this.style.Get_Pad_Top( BACKGROUND_ELEM ) +
        ROW_HEIGHT +
        this.style.Get_Spacing( BACKGROUND_ELEM ) +
        A_Widget(this.leftCol).Get_Preferred_Height +
        this.style.Get_Pad_Bottom( BACKGROUND_ELEM )
    );

    ----------------------------------------------------------------------------

    function Get_Property_Control( this : not null access Property_Module'Class;
                                   name : String ) return A_Widget is
        pos : constant Control_Maps.Cursor := this.controls.Find( name );
    begin
        if Control_Maps.Has_Element( pos ) then
            return Control_Maps.Element( pos );
        end if;
        return null;
    end Get_Property_Control;

    ----------------------------------------------------------------------------

    procedure Handle_Property_Changed( this : not null access Property_Module'Class;
                                       name : String;
                                       val  : Value'Class ) is
        control : constant A_Widget := this.Get_Property_Control( name );
    begin
        if control = null then
            return;
        end if;

        this.props.Set( name, val );

        if control.all in Input_Box'Class then
            A_Input_Box(control).Set_Text( To_Input_String( val ) );
        elsif control.all in Button'Class then
            A_Button(control).Set_State( val.To_Boolean );
        elsif control.all in Label'Class then
            A_Label(control).Set_Text( To_Input_String( val ) );
        elsif control.all in Slider'Class then
            A_Slider(control).Set_Value( val.To_Float );
        elsif control.all in Spinner'Class then
            A_Spinner(control).Set_Value( val.To_Float );
        elsif control.all in Enumeration'Class then
            A_Enumeration(control).Select_Value( val );
        else
            Dbg( "Can't update control widget " & control.To_String, D_GUI, Error );
        end if;
    end Handle_Property_Changed;

    ----------------------------------------------------------------------------

    procedure On_Check_Changed( this : not null access Property_Module'Class ) is
        source   : constant A_Button := A_Button(this.Signaller);
        name     : constant String := Cast_String( source.Get_Attribute( "name" ) );
        oldValue : constant Value := this.props.Get( name );
        newValue : constant Value := Create( source.Get_State );
    begin
        this.On_Property_Edited( name, oldValue, newValue );
        this.props.Set( name, newValue );
    end On_Check_Changed;

    ----------------------------------------------------------------------------

    procedure On_Enum_Changed( this : not null access Property_Module'Class ) is
        source   : constant A_Enumeration := A_Enumeration(this.Signaller);
        name     : constant String := Cast_String( source.Get_Attribute( "name" ) );
        oldValue : constant Value := this.props.Get( name );
        newValue : constant Value := Clone( source.Get_Value );
    begin
        this.On_Property_Edited( name, oldValue, newValue );
        this.props.Set( name, newValue );
    end On_Enum_Changed;

    ----------------------------------------------------------------------------

    procedure On_Input_Accepted( this : not null access Property_Module'Class ) is
    begin
        this.Update_From_Input( A_Input_Box(this.Signaller) );
        this.Get_Tinker_View.Get_Scene.Request_Focus;
    end On_Input_Accepted;

    ----------------------------------------------------------------------------

    procedure On_Input_Blurred( this : not null access Property_Module'Class ) is
        source : constant A_Input_Box := A_Input_Box(this.Signaller);
    begin
        source.Clear_Selection;
        this.Update_From_Input( source );
    end On_Input_Blurred;

    ----------------------------------------------------------------------------

    procedure On_Input_Escaped( this : not null access Property_Module'Class ) is
        source : constant A_Input_Box := A_Input_Box(this.Signaller);
        name   : constant String := Cast_String( source.Get_Attribute( "name" ) );
        val    : constant Value := this.props.Get( name );
    begin
        source.Set_Text( To_Input_String( val ) );
    end On_Input_Escaped;

    ----------------------------------------------------------------------------

    procedure On_Slider_Changed( this : not null access Property_Module'Class ) is
        source   : constant A_Slider := A_Slider(this.Signaller);
        name     : constant String := Cast_String( source.Get_Attribute( "name" ) );
        oldValue : constant Value := this.props.Get( name );
        newValue : constant Value := Create( source.Get_Value );
    begin
        this.On_Property_Edited( name, oldValue, newValue );
        this.props.Set( name, newValue );
    end On_Slider_Changed;

    ----------------------------------------------------------------------------

    procedure On_Spinner_Changed( this : not null access Property_Module'Class ) is
        source   : constant A_Spinner := A_Spinner(this.Signaller);
        name     : constant String := Cast_String( source.Get_Attribute( "name" ) );
        oldValue : constant Value := this.props.Get( name );
        newValue : constant Value := Create( source.Get_Value );
    begin
        this.On_Property_Edited( name, oldValue, newValue );
        this.props.Set( name, newValue );
    end On_Spinner_Changed;

    ----------------------------------------------------------------------------

    procedure Update_From_Input( this    : not null access Property_Module'Class;
                                 control : not null A_Input_Box ) is
        name     : constant String := Cast_String( control.Get_Attribute( "name" ) );
        oldValue : constant Value := this.props.Get( name );
        text     : constant String := control.Get_Text;
        newValue : Value;
    begin
        case oldValue.Get_Type is
            when V_STRING  => newValue := Create( text );
            when V_NUMBER  => newValue := Create( Long_Float'Value( text ) );
            when V_BOOLEAN => newValue := Create( Ada.Strings.Equal_Case_Insensitive( text, "true" ) );
            when others    => null;
        end case;

        this.On_Property_Edited( name, oldValue, newValue );

        -- update the property value now so the text doesn't momentarily flash
        -- back to the old value between the time when the focus is lost and
        -- when the Changed event arrives.
        this.props.Set( name, newValue );
    end Update_From_Input;

end Widgets.Panels.Properties;
