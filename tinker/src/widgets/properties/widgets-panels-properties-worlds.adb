--
-- Copyright (c) 2017-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Commands;                          use Commands;
with Debugging;                         use Debugging;
with Events.Audio;                      use Events.Audio;
with Events.World;                      use Events.World;
with Game_Views.Tinker;                 use Game_Views.Tinker;
with Icons;                             use Icons;
with Resources.Scribble;                use Resources.Scribble;
with Support;                           use Support;
with Values.Construction;               use Values.Construction;
with Values.Lists;                      use Values.Lists;
with Values.Strings;                    use Values.Strings;
with Widgets.Buttons.Toggles;           use Widgets.Buttons.Toggles;
with Widgets.Labels;                    use Widgets.Labels;
with Widgets.Row_Layouts;               use Widgets.Row_Layouts;

package body Widgets.Panels.Properties.Worlds is

    package Connections is new Signals.Connections(World_Module);
    use Connections;

    ----------------------------------------------------------------------------

    function Get_Tinker_View( this : not null access World_Module'Class ) return A_Tinker_View is (A_Tinker_View(this.Get_View));

    procedure Handle_Loaded( this : not null access World_Module'Class;
                             evt  : A_World_Loaded_Event );

    procedure On_Play_Music( this : not null access World_Module'Class );

    procedure On_Stop_Music( this : not null access World_Module'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_World_Module( view : not null access Game_Views.Tinker.Tinker_View'Class ) return A_World_Module is
        this : constant A_World_Module := new World_Module;
    begin
        this.Construct( A_Tinker_View(view) );
        return this;
    end Create_World_Module;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access World_Module;
                         view : not null access Game_Views.Game_View'Class ) is
        props        : constant Map_Value := Create_Map.Map;
        descriptions : Value;
        propDesc     : Map_Value;
        label        : A_Label;
        row          : A_Row_Layout;
    begin
        if Load_Value( descriptions, "worldProperties.sv", "definitions" ) then
            if descriptions.Is_List then
                for i in 1..descriptions.Lst.Length loop
                    propDesc := descriptions.Lst.Get( i ).Map;
                    if propDesc.Valid then
                        propDesc.Set( "displayOrder", Create( i ) );
                        props.Set( propDesc.Get_String( "property" ), propDesc );
                    else
                        Dbg( "Error: worldProperties.sv: Bad property #" & Image( i ), D_GUI or D_RES, Error );
                    end if;
                end loop;
            else
                Dbg( "Error: Invalid file format: worldProperties.sv", D_GUI or D_RES, Error );
            end if;
        else
            Dbg( "Error: File not found: worldProperties.sv", D_GUI or D_RES, Error );
        end if;

        Property_Module(this.all).Construct( view, "World Properties", props );
        this.Listen_For_Event( EVT_WORLD_LOADED );
        this.Listen_For_Event( EVT_WORLD_PROPERTY_CHANGED );

        label := Create_Label( view );
        label.Set_Style( "propertyModule" );
        label.Height.Set( ROW_HEIGHT );
        label.Set_Text( "Actions:" );
        this.leftCol.Insert( 1, label );

        row := Create_Row_Layout( view );
        row.Height.Set( ROW_HEIGHT );
        row.Set_Style_Property( "background.align", Create( "left" ) );

            this.btnMusic := Create_Toggle_Button( view );
            this.btnMusic.Set_Style( "worldActions" );
            this.btnMusic.Set_Icon( Create_Icon( "tinker:music-white-16" ) );
            this.btnMusic.Pressed.Connect( Slot( this, On_Play_Music'Access ) );
            this.btnMusic.Released.Connect( Slot( this, On_Stop_Music'Access ) );
            this.btnMusic.Blurred.Connect( Slot( this, On_Stop_Music'Access ) );
            row.Append( this.btnMusic );

        this.rightCol.Insert( 1, row );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Event( this : access World_Module;
                            evt  : in out A_Event;
                            resp : out Response_Type ) is
        pragma Unreferenced( resp );
    begin
        if evt.Get_Event_Id = EVT_WORLD_LOADED then
            this.Handle_Loaded( A_World_Loaded_Event(evt) );
        elsif evt.Get_Event_Id = EVT_WORLD_PROPERTY_CHANGED then
            this.Handle_Property_Changed( A_World_Property_Event(evt).Get_Property_Name,
                                          A_World_Property_Event(evt).Get_Value );
        end if;
    end Handle_Event;

    ----------------------------------------------------------------------------

    procedure Handle_Loaded( this : not null access World_Module'Class;
                             evt  : A_World_Loaded_Event ) is
    begin
        this.Handle_Property_Changed( "lib",          Create( evt.Get_Library_Name ) );
        this.Handle_Property_Changed( "size",         Create( Image( evt.Get_Map.Get_Width ) & " x " & Image( evt.Get_Map.Get_Height ) ) );
        this.Handle_Property_Changed( "title",        Create( "" ) );
        this.Handle_Property_Changed( "introduction", Create( "" ) );
        this.Handle_Property_Changed( "ambientLight", Create( "#FFFFFF" ) );
        this.Handle_Property_Changed( "music",        Create( "" ) );
    end Handle_Loaded;

    ----------------------------------------------------------------------------

    procedure On_Play_Music( this : not null access World_Module'Class ) is
        music : constant String := this.props.Get_String( "music" );
    begin
        if music'Length > 0 then
            this.btnMusic.Set_Icon( Create_Icon( "tinker:pause_icon" ) );
            Queue_Play_Music( music );
        else
            this.btnMusic.Set_State( False );
        end if;
    end On_Play_Music;

    ----------------------------------------------------------------------------

    procedure On_Stop_Music( this : not null access World_Module'Class ) is
    begin
        this.btnMusic.Set_Icon( Create_Icon( "tinker:music-white-16" ) );
        this.btnMusic.Set_State( False );
        Queue_Stop_Music;
    end On_Stop_Music;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Property_Edited( this     : access World_Module;
                                  name     : String;
                                  oldValue : Value'Class;
                                  newValue : Value'Class ) is
    begin
        this.Get_Tinker_View.Commands.Execute( Create_Command_Set_World_Property( name, oldValue, newValue ) );
    end On_Property_Edited;

end Widgets.Panels.Properties.Worlds;
