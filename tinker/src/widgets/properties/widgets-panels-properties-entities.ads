--
-- Copyright (c) 2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Object_Ids;                        use Object_Ids;

limited with Game_Views;

package Widgets.Panels.Properties.Entities is

    type Entity_Module is new Property_Module with private;
    type A_Entity_Module is access all Entity_Module'Class;

    function Create_Entity_Module( view     : not null access Game_Views.Game_View'Class;
                                   eid      : Entity_Id;
                                   template : String;
                                   attrs    : Map_Value ) return A_Entity_Module;
    pragma Postcondition( Create_Entity_Module'Result /= null );

private

    type Entity_Module is new Property_Module with
        record
            eId : Entity_Id;
        end record;

    procedure Construct( this       : access Entity_Module;
                         view       : not null access Game_Views.Game_View'Class;
                         eid        : Entity_Id;
                         template   : String;
                         attributes : Map_Value );

    procedure Handle_Event( this : access Entity_Module;
                            evt  : in out A_Event;
                            resp : out Response_Type );

    procedure On_Property_Edited( this     : access Entity_Module;
                                  name     : String;
                                  oldValue : Value'Class;
                                  newValue : Value'Class );

end Widgets.Panels.Properties.Entities;
