--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Numerics;                      use Ada.Numerics;
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Allegro.Color.Spaces;              use Allegro.Color.Spaces;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.State;                     use Allegro.State;
with Allegro.Transformations;           use Allegro.Transformations;
with Clipping.Drawing;                  use Clipping.Drawing;
with Component_Families;                use Component_Families;
--with Debugging;                         use Debugging;
with Drawing;                           use Drawing;
with Drawing.Bitmaps;                   use Drawing.Bitmaps;
with Drawing.Primitives;                use Drawing.Primitives;
with Game_Views;                        use Game_Views;
with Game_Views.Tinker;                 use Game_Views.Tinker;
with Palette.Tango;                     use Palette.Tango;
with Preferences;                       use Preferences;
with Support;                           use Support;
with Tools;                             use Tools;
with Tool_Contexts;                     use Tool_Contexts;
with Values.Construction;               use Values.Construction;
with Values.Lists;                      use Values.Lists;
with Widgets.Scenes.Cameras;            use Widgets.Scenes.Cameras;
with Widgets.Sprites;                   use Widgets.Sprites;
with Widgets.Sprites.Tinker;            use Widgets.Sprites.Tinker;

pragma Elaborate_All( Preferences );

package body Widgets.Scenes.Tinker is

    package Connections is new Signals.Connections(Tinker_Scene);
    use Connections;

    package Connections2 is new Signals.Connections(Scene);
    use Connections2;

    package Mouse_Connections is new Signals.Mouse.Connections(Tinker_Scene);
    use Mouse_Connections;

    ZOOM_INCREMENT : constant := 0.02;                     -- single step for zooming
    ZOOM_RATE      : constant := 1.0;                      -- zoom in/out speed (larger is faster)
    ZOOM_MIN_LOG   : constant Float := Log( ZOOM_MIN );
    ZOOM_MAX_LOG   : constant Float := Log( ZOOM_MAX );
    ZOOM_FUZZ      : constant Float := 0.1;

    ----------------------------------------------------------------------------

    function Get_Tinker_View( this : not null access Tinker_Scene'Class ) return A_Tinker_View is (A_Tinker_View(this.Get_View));

    procedure On_Map_Changed( this : not null access Tinker_Scene'Class );

    procedure On_Mouse_Entered( this : not null access Tinker_Scene'Class );

    procedure On_Mouse_Exited( this : not null access Tinker_Scene'Class );

    procedure On_Mouse_Moved( this  : not null access Tinker_Scene'Class;
                              mouse : Mouse_Arguments );

    procedure On_Mouse_Pressed( this  : not null access Tinker_Scene'Class;
                                mouse : Button_Arguments );

    procedure On_Mouse_Released_Left( this  : not null access Tinker_Scene'Class;
                                      mouse : Button_Arguments );

    procedure On_Mouse_Released_Middle( this  : not null access Tinker_Scene'Class;
                                        mouse : Button_Arguments );

    procedure On_Mouse_Released_Right( this : not null access Tinker_Scene'Class );

    procedure On_Mouse_Scrolled( this  : not null access Tinker_Scene'Class;
                                 mouse : Scroll_Arguments );

    procedure On_Resized( this : not null access Tinker_Scene'Class );

    procedure On_Tool_Selected( this : not null access Tinker_Scene'Class );

    -- Resets the widget's cursor based on the current tool. Some cursors change
    -- while a tool is in use. This is called to reset the cursor after applying
    -- the tool, and when a new tool is selected.
    procedure Update_Cursor( this : not null access Tinker_Scene'Class );

    -- Updates the layer tinting, based on the active layer and tool.
    procedure Update_Layer_Tint( this : not null access Tinker_Scene'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Scene( view : not null access Game_Views.Game_View'Class;
                           id   : String ) return A_Tinker_Scene is
        this : A_Tinker_Scene := new Tinker_Scene;
    begin
        this.Construct( view, id );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Scene;

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access Tinker_Scene;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String ) is
    begin
        Scene(this.all).Construct( view, id );

        this.entityArea.sigAreaChanged.Init( this );
        this.sigSelectedCountChanged  .Init( this );
        this.sigToolPositionChanged   .Init( this );
        this.sigZoomChanged           .Init( this );

        this.MouseEntered.Connect( Slot( this, On_Mouse_Entered'Access ) );
        this.MouseExited.Connect( Slot( this, On_Mouse_Exited'Access ) );
        this.Resized.Connect( Slot( this, On_Resized'Access ) );

        this.MouseMoved.Connect( Slot( this, On_Mouse_Moved'Access ) );
        this.MousePressed.Connect( Slot( this, On_Mouse_Pressed'Access ) );
        this.MouseReleased.Connect( Slot( this, On_Mouse_Released_Left'Access, Mouse_Left ) );
        this.MouseReleased.Connect( Slot( this, On_Mouse_Released_Middle'Access, Mouse_Middle ) );
        this.MouseReleased.Connect( Slot( this, On_Mouse_Released_Right'Access, Mouse_Right ) );
        this.MouseScrolled.Connect( Slot( this, On_Mouse_Scrolled'Access ) );

        this.MapChanged.Connect( Slot( this, On_Map_Changed'Access ) );

        this.Get_Tinker_View.ToolChanged.Connect( Slot( this, On_Tool_Selected'Access ) );
        this.Get_Tinker_View.Quitting.Connect( Slot( this, Unload_World'Access ) );

        this.Enable_Option( TINKER_SCENE_DEFAULTS );

        this.activeTint   := Al_Color_HTML( Get_Pref( "scene", "active_tint", "#FFFFFF" ) );
        this.inactiveTint := Al_Color_HTML( Get_Pref( "scene", "inactive_tint", "#404040" ) );

        this.zoomTarget := this.zoomFactor;

        this.Update_Cursor;
    end Construct;

    ----------------------------------------------------------------------------

    function EntityAreaChanged( this : not null access Tinker_Scene'Class ) return access Signal'Class is (this.entityArea.sigAreaChanged'Access);
    function SelectedCountChanged( this : not null access Tinker_Scene'Class ) return access Signal'Class is (this.sigSelectedCountChanged'Access);
    function TileAreaChanged( this : not null access Tinker_Scene'Class ) return access Signal'Class is (this.tileArea.sigAreaChanged'Access);
    function ToolPositionChanged( this : not null access Tinker_Scene'Class ) return access Signal'Class is (this.sigToolPositionChanged'Access);
    function ZoomChanged( this : not null access Tinker_Scene'Class ) return access Signal'Class is (this.sigZoomChanged'Access);

    ----------------------------------------------------------------------------

    procedure Copy_Selected_Entities( this : not null access Tinker_Scene'Class ) is
        entities : constant List_Value := Create_List.Lst;
    begin
        -- copy context object goes first
        entities.Append( Create_Map( (Pair( "x", Create( this.Get_Viewport.x + this.Get_Viewport.width / 2.0 ) ),
                                      Pair( "y", Create( this.Get_Viewport.y + this.Get_Viewport.height / 2.0 ) )) ) );

        -- all entities copied next
        for entity of this.selectedSet loop
            if entity.Is_Deletable then
                entities.Append( entity.Create_Copy, consume => True );
            end if;
        end loop;

        if entities.Length > 1 then
            this.Get_View.Set_Clipboard( "entities", entities );
        end if;
    end Copy_Selected_Entities;

    ----------------------------------------------------------------------------

    overriding
    function Create_Entity_Widget( this         : access Tinker_Scene;
                                   entity       : not null A_Entity;
                                   initialState : Map_Value ) return A_Widget is
        vis : constant Map_Value := initialState.Get( Family_Name( VISIBLE_ID ) ).Map;
    begin
        if vis.Valid then
            if vis.Get_String( "render" ) = "sprite" then
                return A_Widget(Create_Tinker_Sprite( this.Get_View, entity, vis.Get_String( "lib" ), vis.Get_Int( "frame" ) ));
            end if;
        end if;
        return null;
    end Create_Entity_Widget;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Content_Foreground( this : access Tinker_Scene ) is

        -- Draw the shapes of solid tiles in the clipping layers
        procedure Draw_Clipping is
            drawX, drawY : Float;
        begin
            for y in this.tileY1..this.tileY2 loop
                drawY := Float(y * this.tileWidth);
                for x in this.tileX1..this.tileX2 loop
                    drawX := Float(x * this.tileWidth);
                    Draw( this.mapInfo.Get_Shape( x, y ), drawX, drawY, 0.0, this.tileWidth, Butter1, Scarlet1 );
                end loop;
            end loop;
        end Draw_Clipping;

        ------------------------------------------------------------------------

        procedure Draw_Layer_Outlines is
            lineWidth      : constant Float := From_Target_Pixels( 1.0 );
            edgeColor      : Allegro_Color;
            drawX1, drawY1 : Float;
            drawX2, drawY2 : Float;
        begin
            edgeColor := Opacity( this.Get_Layer_Tint( this.activeLayer ), 1.0 );

            for y in this.tileY1..this.tileY2 loop
                drawY1 := Float(y * this.tileWidth);
                drawY2 := drawY1 + Float(this.tileWidth);

                for x in this.tileX1..this.tileX2 loop
                    drawX1 := Float(x * this.tileWidth);
                    drawX2 := drawX1 + Float(this.tileWidth);

                    if this.map.Get_Tile_Id( this.activeLayer, x, y ) = 0 then
                        if x < this.tileX2 then
                            -- check tile to the right
                            if this.map.Get_Tile_Id( this.activeLayer, x + 1, y ) > 0 then
                                Line( drawX2, drawY1, drawX2, drawY2, edgeColor, lineWidth );
                            end if;
                        end if;
                        if x > this.tileX1 then
                            -- check tile to the left
                            if this.map.Get_Tile_Id( this.activeLayer, x - 1, y ) > 0 then
                                Line( drawX1, drawY1, drawX1, drawY2, edgeColor, lineWidth );
                            end if;
                        end if;
                        if y < this.tileY2 then
                            -- check tile to the bottom
                            if this.map.Get_Tile_Id( this.activeLayer, x, y + 1 ) > 0 then
                                Line( drawX1, drawY2, drawX2, drawY2, edgeColor, lineWidth );
                            end if;
                        end if;
                        if y > this.tileY1 then
                            -- check tile to the top
                            if this.map.Get_Tile_Id( this.activeLayer, x, y - 1 ) > 0 then
                                Line( drawX1, drawY1, drawX2, drawY1, edgeColor, lineWidth );
                            end if;
                        end if;
                    end if;

                end loop;
            end loop;
        end Draw_Layer_Outlines;

        ------------------------------------------------------------------------

        -- Draw a grid on top of all the tiles.
        procedure Draw_Grid is
            lineLength : constant Float := From_Target_Pixels( Float(this.gridIcon.Get_Height) );
            thickness  : constant Float := From_Target_Pixels( Float(this.gridIcon.Get_Width) );
            draw       : Float;
            row        : Natural := 0;
            trans,
            backup     : Allegro_Transform;
        begin
            if this.gridIcon.Get_Bitmap = null then
                return;
            end if;

            Al_Hold_Bitmap_Drawing( True );

            -- draw the vertical grid lines; 'gridIcon' is a single vertical line
            -- that will be rotated to draw the lines horizontally
            row := 0;
            loop
                -- calculated instead of incremented, to avoid accumulating error
                draw := Float(this.tileY1 * this.tileWidth) + row * lineLength;
                exit when draw >= Float((this.tileY2 + 1) * this.tileWidth);

                for x in this.tileX1..(this.tileX2 + 1) loop
                    Draw_Bitmap_Stretched( this.gridIcon.Get_Bitmap,
                                           Float(x * this.tileWidth), draw, 0.0,
                                           thickness,
                                           lineLength );
                end loop;
                row := row + 1;
            end loop;

            -- draw the horizontal grid lines by rotating 90 degrees clockwise
            -- around the origin (0,0 in world coordinates) and then doing a
            -- horizontal mirror (Y axis in rotated coordinates).
            Al_Copy_Transform( backup, Al_Get_Current_Transform.all );
            Al_Identity_Transform( trans );
            Al_Scale_Transform( trans, 1.0, -1.0 );
            Al_Rotate_Transform( trans, Pi / 2.0 );
            Al_Compose_Transform( trans, backup );
            Al_Use_Transform( trans );
            row := 0;
            loop
                draw := Float(this.tileX1 * this.tileWidth) + row * lineLength;
                exit when draw >= Float((this.tileX2 + 1) * this.tileWidth);

                for x in this.tileY1..(this.tileY2 + 1) loop
                    Draw_Bitmap_Stretched( this.gridIcon.Get_Bitmap,
                                           Float(x * this.tileWidth), draw, 0.0,
                                           thickness,
                                           lineLength );
                end loop;
                row := row + 1;
            end loop;
            Al_Use_Transform( backup );

            Al_Hold_Bitmap_Drawing( False );
        end Draw_Grid;

        ------------------------------------------------------------------------

        -- Draw connection lines between the entities
        procedure Draw_Entity_Connections is
            ARROW_ANGLE  : constant := Pi / 4.0;
            ARROW_LENGTH : constant := 8.0;
            COLOR        : constant Allegro_Color := Butter1;
            THICKNESS    : constant Float := 2.0 / this.zoomFactor;

            entB   : A_Entity;
            connsA,
            connsB : List_Value;
            angle  : Float;
            ax, ay : Float;
            bx, by : Float;
        begin
            for entA of this.entityMap loop
                ax := entA.Get_Widget.Get_Center_X;
                ay := entA.Get_Widget.Get_Center_Y;
                connsA := entA.Get_Attribute( "connections" ).Lst;
                if connsA.Valid then
                    for i in 1..connsA.Length loop
                        entB := this.Find_Entity( Cast_Tagged_Id( connsA.Get( i ) ) );
                        if entB /= null then
                            bx := entB.Get_Widget.Get_Center_X;
                            by := entB.Get_Widget.Get_Center_Y;
                            connsB := entB.Get_Attribute( "connections" ).Lst;
                            if connsB.Valid and then
                               connsB.Find( To_Id_Value( entA.Get_Entity_Id ) ) > 0
                            then
                                -- entity A is in entity B's connections;
                                -- it's a two way connection
                                if entA.Get_Entity_Id < entB.Get_Entity_Id and (ax /= bx or ay /= by) then
                                    angle := Arctan( by - ay, bx - ax );

                                    -- draw a two-way connection
                                    Line( ax, ay, bx, by, COLOR, THICKNESS );

                                    -- arrow on the B entity
                                    Line( bx, by,
                                          bx - ARROW_LENGTH * Cos( angle + ARROW_ANGLE ),
                                          by - ARROW_LENGTH * Sin( angle + ARROW_ANGLE ),
                                          COLOR, THICKNESS );
                                    Line( bx, by,
                                          bx - ARROW_LENGTH * Cos( angle - ARROW_ANGLE ),
                                          by - ARROW_LENGTH * Sin( angle - ARROW_ANGLE ),
                                          COLOR, THICKNESS );

                                    -- arrow on the A entity
                                    Line( ax,ay,
                                          ax - ARROW_LENGTH * Cos( angle + ARROW_ANGLE + Pi ),
                                          ay - ARROW_LENGTH * Sin( angle + ARROW_ANGLE + Pi ),
                                          COLOR, THICKNESS );
                                    Line( ax, ay,
                                          ax - ARROW_LENGTH * Cos( angle - ARROW_ANGLE + Pi ),
                                          ay - ARROW_LENGTH * Sin( angle - ARROW_ANGLE + Pi ),
                                          COLOR, THICKNESS );
                                end if;

                            -- entity A is not in entity B's connections;
                            -- it's a one-way connection
                            elsif ax /= bx or ay /= by then
                                angle := Arctan( by - ay, bx - ax );

                                -- draw a one-way connection (A to B)
                                Line( ax, ay, bx, by, COLOR, THICKNESS );

                                -- arrow on the B entity
                                Line( bx, by,
                                      bx - ARROW_LENGTH * Cos( angle + ARROW_ANGLE ),
                                      by - ARROW_LENGTH * Sin( angle + ARROW_ANGLE ),
                                      COLOR, THICKNESS );
                                Line( bx, by,
                                      bx - ARROW_LENGTH * Cos( angle - ARROW_ANGLE ),
                                      by - ARROW_LENGTH * Sin( angle - ARROW_ANGLE ),
                                      COLOR, THICKNESS );
                            end if;
                        end if;
                    end loop;
                end if;
            end loop;
        end Draw_Entity_Connections;

        ------------------------------------------------------------------------

        procedure Draw_Selection( s : Selection_Type ) is
            r : constant Rectangle := Get_Rect( s );
        begin
            if s.active then
                if r.width /= 0.0 or r.height /= 0.0 then
                    Rectfill_XYWH( r.x, r.y, r.width, r.height, Opacity( Skyblue2, 0.35 ) );
                    Rect_XYWH( r.x, r.y, r.width, r.height, Skyblue1 );
                end if;
            end if;
        end Draw_Selection;

        ------------------------------------------------------------------------

        procedure Draw_Tool_Preview is
            tool    : A_Tool;
            mouseX,
            mouseY  : Float;
        begin
            if this.Get_Window.Is_Modal_Active then
                -- don't draw tool preview when a dialog is up
                return;
            end if;

            this.Get_Mouse_Location( mouseX, mouseY );
            tool := this.Get_Tinker_View.Get_Selected_Tool;
            tool.Draw_Preview_Foreground(
                (CTRL => not this.mouseButtons(Mouse_Right) and (
                     this.Get_Tinker_View.Get_Input_Handler.Is_Key_Down( ALLEGRO_KEY_LCTRL ) or
                     this.Get_Tinker_View.Get_Input_Handler.Is_Key_Down( ALLEGRO_KEY_RCTRL )),
                 others => False),
                Tool_Preview_Context'(this.lib,
                                      mouseX, mouseY,
                                      this.activeLayer)
            );
        end Draw_Tool_Preview;

        ------------------------------------------------------------------------

    begin
        if not this.Is_Map_Loaded then
            return;
        end if;

        -- Draw crosshairs to show the world focus point
        --Line_H( this.focusX - 12.0, this.focusX + 12.0, this.focusY, White );
        --Line_V( this.focusX, this.focusY - 12.0, this.focusY + 12.0, White );

        if this.Is_Enabled( SHOW_CLIPPING ) then
            Draw_Clipping;
        end if;

        -- draw layer outlines over the active layer. outlines are created by
        -- borders with tile id 0.
        if this.Is_Enabled( SHOW_OUTLINES ) then
            Draw_Layer_Outlines;
        end if;

        -- draw a dashed line tile grid
        if this.Is_Enabled( SHOW_GRID ) then
            Draw_Grid;
        end if;

        -- draw lines indicating existing entity connections
        if this.Is_Enabled( SHOW_CONNECTIONS ) then
            Draw_Entity_Connections;
        end if;

        -- draw selection boxes: entities (when selecting) and tiles
        if this.Is_Enabled( SHOW_SELECTION ) then
            Draw_Selection( this.entityArea );
            Draw_Selection( this.tileArea );
        end if;

        -- draw the layer-independent component of the current tool's preview
        if this.Is_Enabled( SHOW_MAP_TOOL ) then
            Draw_Tool_Preview;
        end if;
    end Draw_Content_Foreground;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Layer_Overlay( this : access Tinker_Scene; layer : Integer ) is

        procedure Draw_Map_Tool_Preview is
            tool    : A_Tool;
            mouseX,
            mouseY  : Float;
        begin
            if this.Get_Window.Is_Modal_Active then
                -- don't draw tool highlight when a dialog is up
                return;
            end if;

            -- draw the preview of layer-specific tools over the active layer
            -- and non-layer-specific tools over every layer
            tool := this.Get_Tinker_View.Get_Selected_Tool;
            if layer = this.activeLayer or not tool.Is_Layer_Specific then
                if tool.Acts_On( Map_Subject ) then
                    this.Get_Mouse_Location( mouseX, mouseY );
                    tool.Draw_Preview_Layer(
                        (CTRL => not this.mouseButtons(Mouse_Right) and (
                             this.Get_Tinker_View.Get_Input_Handler.Is_Key_Down( ALLEGRO_KEY_LCTRL ) or
                             this.Get_Tinker_View.Get_Input_Handler.Is_Key_Down( ALLEGRO_KEY_RCTRL )),
                         others => False),
                        Tool_Preview_Context'(this.lib,
                                              mouseX, mouseY,
                                              layer)
                    );
                end if;
            end if;
        end Draw_Map_Tool_Preview;

        ------------------------------------------------------------------------

        backup : Allegro_Transform;
        trans  : Allegro_Transform;
    begin
        -- draw the overlay relative to the layer's Z depth
        Al_Copy_Transform( backup, Al_Get_Current_Transform.all );
        Al_Identity_Transform( trans );
        Al_Translate_Transform_3d( trans, 0.0, 0.0, this.Get_Layer_Z( layer ) );
        Al_Compose_Transform( trans, backup );
        Al_Use_Transform( trans );

        if this.Is_Enabled( SHOW_MAP_TOOL ) then
            Draw_Map_Tool_Preview;
        end if;

        Al_Use_Transform( backup );
    end Draw_Layer_Overlay;

    ----------------------------------------------------------------------------

    procedure Draw_Customized( this     : not null access Tinker_Scene'Class;
                               viewport : Rectangle;
                               options  : Render_Options ) is
        backupVis      : constant Boolean := this.vis;
        backupZoom     : constant Float := this.zoomFactor;
        backupViewport : constant Rectangle := this.viewport;
        backupOptions  : constant Render_Options := this.options;
    begin
        if not this.Is_Map_Loaded then
            return;
        end if;

        this.vis        := True;        -- always draw it
        this.zoomFactor := 1.0;         -- caller can scale as desired
        this.viewport   := viewport;

        this.Enable_Option( backupOptions, False );
        this.Enable_Option( options, True );

        this.Draw;

        this.vis        := backupVis;
        this.zoomFactor := backupZoom;
        this.viewport   := backupViewport;

        this.Enable_Option( options, False );
        this.Enable_Option( backupOptions, True );
    end Draw_Customized;

    ----------------------------------------------------------------------------

    function Draw_To_Bitmap( this : not null access Tinker_Scene'Class ) return A_Allegro_Bitmap is
        state : Allegro_State;
        bmp   : A_Allegro_Bitmap;
    begin
        if not this.Is_Map_Loaded then
            return null;
        end if;

        bmp := Al_Create_Bitmap( this.map.Get_Width * this.tileWidth,
                                 this.map.Get_Height * this.tileWidth );
        if bmp = null then
            -- not enough memory to allocate the bitmap
            return null;
        end if;

        Al_Store_State( state, ALLEGRO_STATE_TARGET_BITMAP );
        Set_Target_Bitmap( bmp );
        Clear_To_Color( Transparent );
        this.Draw_Customized( viewport => (x      => 0.0,
                                           y      => 0.0,
                                           width  => Float(this.map.Get_Width * this.tileWidth),
                                           height => Float(this.map.Get_Height * this.tileWidth)),
                              options => (this.options
                                          or SHOW_ALL_LAYERS
                                          or SHOW_LIGHTING
                                          or SHOW_PARTICLES
                                          or SHOW_SPRITES)
                                          and not SHOW_SELECTION
                                          and not SHOW_MAP_TOOL
                                          and not SHOW_LAYER_TINT
                                          and not SHOW_SPRITES_FRONT );
        Al_Restore_State( state );

        return bmp;
    end Draw_To_Bitmap;

    ----------------------------------------------------------------------------

    procedure Enable_Draw_Grid( this : not null access Tinker_Scene'class; grid : String ) is
    begin
        this.Enable_Option( SHOW_GRID, grid'Length > 0 );
        this.drawGrid := To_Unbounded_String( grid );
        if grid'Length > 0 then
            this.gridIcon := Create_Icon( "tinker:grid-" & grid );
        else
            this.gridIcon := NO_ICON;
        end if;
        Set_Pref( "drawGrid", grid );
    end Enable_Draw_Grid;

    ----------------------------------------------------------------------------

    procedure Entity_Area_Begin( this : not null access Tinker_Scene'Class; x, y : Float ) is
    begin
        this.entityArea.active := True;
        this.entityArea.x1 := x;
        this.entityArea.y1 := y;
        this.entityArea.x2 := x;
        this.entityArea.y2 := y;
        this.entityArea.sigAreaChanged.Emit;
        this.wasSelected := this.selectedSet;
    end Entity_Area_Begin;

    ----------------------------------------------------------------------------

    procedure Entity_Area_Confirm( this : not null access Tinker_Scene'Class ) is
    begin
        if this.entityArea.active then
            this.entityArea.active := False;
            this.entityArea.sigAreaChanged.Emit;
        end if;
    end Entity_Area_Confirm;

    ----------------------------------------------------------------------------

    procedure Entity_Area_Update( this : not null access Tinker_Scene'Class;
                                  x, y : Float;
                                  mode : Select_Mode ) is
    begin
        if this.entityArea.active then
            this.entityArea.x2 := x;
            this.entityArea.y2 := y;
            this.entityArea.sigAreaChanged.Emit;

            for entity of this.entityMap loop
                -- entity inside selection area --
                if Intersect( entity.Get_Widget.Get_Geometry, Get_Rect( this.entityArea ) ) then
                    if mode = Select_Toggle and then this.wasSelected.Contains( entity ) then
                        -- invert the selected state of entities within the
                        -- selection area. if it was already selected then
                        -- unselect it.
                        this.Entity_Selection_Remove( entity );
                    else
                        this.Entity_Selection_Add( entity );
                    end if;

                -- entity outside selection area --
                else
                    if mode = Select_Include then
                        -- add all entities within the area to the selection.
                        -- those outside the selection area are not removed from
                        -- the selection if they were already selected.
                        if not this.wasSelected.Contains( entity ) then
                            this.Entity_Selection_Remove( entity );
                        end if;
                    elsif mode = Select_Toggle and then this.wasSelected.Contains( entity ) then
                        -- invert the selected state of entities, so if it was
                        -- already selected then make sure it stays selected.
                        this.Entity_Selection_Add( entity );
                    else
                        this.Entity_Selection_Remove( entity );
                    end if;

                end if;
            end loop;

        end if;
    end Entity_Area_Update;

    ----------------------------------------------------------------------------

    procedure Entity_Select_All( this : not null access Tinker_Scene'Class ) is
    begin
        for entity of this.entities loop
            this.Entity_Selection_Add( entity );
        end loop;
    end Entity_Select_All;

    ----------------------------------------------------------------------------

    procedure Entity_Selection_Add( this   : not null access Tinker_Scene'Class;
                                    entity : not null A_Entity ) is
    begin
        if not this.selectedSet.Contains( entity ) then
            this.selectedSet.Insert( entity );
            this.pendingSelection.Exclude( entity.Get_Entity_Id );
            entity.Set_Selected( True );
            this.sigSelectedCountChanged.Emit;
        end if;
    end Entity_Selection_Add;

    ----------------------------------------------------------------------------

    procedure Entity_Selection_Add_Pending( this : not null access Tinker_Scene'Class;
                                            eid  : Entity_Id ) is
    begin
        this.pendingSelection.Include( eid );
    end Entity_Selection_Add_Pending;

    ----------------------------------------------------------------------------

    procedure Entity_Selection_Clear( this : not null access Tinker_Scene'Class ) is
        pos : Entity_Sets.Cursor := this.selectedSet.First;
    begin
        while Entity_Sets.Has_Element( pos ) loop
            Entity_Sets.Element( pos ).Set_Selected( False );
            pos := Entity_Sets.Next( pos );
        end loop;
        this.selectedSet.Clear;
        this.pendingSelection.Clear;
        this.sigSelectedCountChanged.Emit;
    end Entity_Selection_Clear;

    ----------------------------------------------------------------------------

    procedure Entity_Selection_Remove( this   : not null access Tinker_Scene'Class;
                                       entity : not null A_Entity ) is
        pos : Entity_Sets.Cursor := this.selectedSet.Find( entity );
    begin
        if Entity_Sets.Has_Element( pos ) then
            this.selectedSet.Delete( pos );
            entity.Set_Selected( False );
            this.sigSelectedCountChanged.Emit;
        end if;
    end Entity_Selection_Remove;

    ----------------------------------------------------------------------------

    procedure Entity_Selection_Set( this   : not null access Tinker_Scene'Class;
                                    entity : A_Entity ) is
    begin
        this.Entity_Selection_Clear;
        if entity /= null then
            this.selectedSet.Insert( entity );
            entity.Set_Selected( True );
            this.sigSelectedCountChanged.Emit;
        end if;
    end Entity_Selection_Set;

    ----------------------------------------------------------------------------

    procedure Entity_Selection_Toggle( this   : not null access Tinker_Scene'Class;
                                       entity : not null A_Entity ) is
        pos : Entity_Sets.Cursor := this.selectedSet.Find( entity );
    begin
        if Entity_Sets.Has_Element( pos ) then
            this.selectedSet.Delete( pos );
            entity.Set_Selected( False );
        else
            this.selectedSet.Insert( entity );
            entity.Set_Selected( True );
        end if;
        this.sigSelectedCountChanged.Emit;
    end Entity_Selection_Toggle;

    ----------------------------------------------------------------------------

    overriding
    procedure Find_Widget_At( this     : access Tinker_Scene;
                              x, y     : Float;
                              children : Boolean;
                              wx, wy   : out Float;
                              found    : out A_Widget ) is
        -- translate viewport coordinates into content coordinates
        cx            : constant Float := this.viewport.x + x / this.zoomFactor;
        cy            : constant Float := this.viewport.y + y / this.zoomFactor;
        checkEntities : Boolean;
    begin
        wx := 0.0;
        wy := 0.0;
        found := null;

        if not this.vis or not this.enable then
            return;
        end if;

        if children then
            -- disable interaction with entities unless the currently selected
            -- tool can act on them. other child widgets are unaffected.
            checkEntities := this.Is_Map_Loaded and then
                             this.Get_Tinker_View.Get_Selected_Tool.Acts_On( Entity_Subject );

            -- iterate in reverse to check children in Z-order, front to back
            for child of reverse this.children loop
                if Contains( child.geom, cx, cy ) then
                    -- child widgets representing entities have attribute "eid"
                    -- set to the represented entity's id. this test is inside
                    -- the Contains() intersection test so we don't lookup the
                    -- attribute for every single widget in the scene.
                    if checkEntities or else not child.Get_Attribute( "eid" ).Is_Id then
                        child.Find_Widget_At( cx - child.geom.x, cy - child.geom.y, True, wx, wy, found );
                        if found /= null then
                            return;
                        end if;
                    end if;
                end if;
            end loop;
        end if;

        -- verify x, y is within the viewport
        if Contains( Rectangle'(0.0, 0.0, this.geom.width, this.geom.height), x, y ) then
            -- translate the viewport coordinates into content coordinates
            wx := cx;
            wy := cy;
            found := A_Widget(this);
        end if;
    end Find_Widget_At;

    ----------------------------------------------------------------------------

    function Get_Active_Layer( this : not null access Tinker_Scene'Class ) return Integer is (this.activeLayer);

    ----------------------------------------------------------------------------

    function Get_Entity_Area( this : not null access Tinker_Scene'Class ) return Rectangle is (Get_Rect( this.entityArea ));

    ----------------------------------------------------------------------------

    function Get_Entity_Selection_Count( this : not null access Tinker_Scene'Class ) return Natural is (Integer(this.selectedSet.Length));

    ----------------------------------------------------------------------------

    function Get_Selected_Entities( this : not null access Tinker_Scene'Class ) return Entity_Sets.Set is (this.selectedSet.Copy);

    ----------------------------------------------------------------------------

    function Get_Tile_Area( this : not null access Tinker_Scene'Class ) return Rectangle is (Get_Rect( this.tileArea ));

    ----------------------------------------------------------------------------

    function Get_Tool_X( this : not null access Tinker_Scene'Class ) return Float is (this.toolX);

    ----------------------------------------------------------------------------

    function Get_Tool_Y( this : not null access Tinker_Scene'Class ) return Float is (this.toolY);

    ----------------------------------------------------------------------------

    function In_Active_Area( this     : not null access Tinker_Scene'Class;
                             col, row : Integer ) return Boolean is
    begin
        return (this.tileArea.x1 = this.tileArea.x2 or this.tileArea.y1 = this.tileArea.y2) or else
               Contains( Get_Rect( this.tileArea ),
                         Float(col * this.tileWidth + this.tileWidth / 2),
                         Float(row * this.tileWidth + this.tileWidth / 2) );
    end In_Active_Area;

    ----------------------------------------------------------------------------

    function Is_Mouse_In_Viewport( this : not null access Tinker_Scene'Class ) return Boolean is
        mouseX, mouseY : Float;
    begin
        this.Get_Mouse_Location( mouseX, mouseY );
        return Contains( this.viewport, mouseX, mouseY );
    end Is_Mouse_In_Viewport;

    ----------------------------------------------------------------------------

    function Is_Panning( this : not null access Tinker_Scene'Class ) return Boolean
    is (this.panStartX >= 0.0 or else this.panStartY >= 0.0);

    ----------------------------------------------------------------------------

    function Is_Tile_Area_Active( this : not null access Tinker_Scene'Class ) return Boolean is (this.tileArea.active);

    ----------------------------------------------------------------------------

    procedure Notify_Tool_Position( this : not null access Tinker_Scene'Class; wx, wy : Float ) is
    begin
        this.toolX := wx;
        this.toolY := wy;
        this.sigToolPositionChanged.Emit;
    end Notify_Tool_Position;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Moved( this  : not null access Tinker_Scene'Class;
                              mouse : Mouse_Arguments ) is
        onMap : Boolean;
        tool  : A_Tool;
    begin
        if this.Is_Map_Loaded then
            onMap := mouse.x >= 0.0 and then mouse.y >= 0.0 and then
                     mouse.x < Float(this.map.Get_Width * this.tileWidth) and then
                     mouse.y < Float(this.map.Get_Height * this.tileWidth);

            if onMap then
                this.Notify_Tool_Position( mouse.x, mouse.y );
            else
                this.Notify_Tool_Position( -1.0, -1.0 );
            end if;

            -- special case: the panner tool pans the map on all buttons, and
            -- all other tools pan on right-mouse only.
            if this.Is_Panning then
                if mouse.x /= this.panStartX or else mouse.y /= this.panStartY then
                    this.Shift_Focus( this.panStartX - mouse.x,
                                      this.panStartY - mouse.y );
                end if;
                return;
            end if;

            if this.mouseButtons(Mouse_Left) or this.mouseButtons(Mouse_Middle) then
                tool := this.Get_Tinker_View.Get_Selected_Tool;
                if tool.Acts_On( Map_Subject ) then
                    tool.Apply_To_Map( (if this.mouseButtons(Mouse_Left) then Primary else Secondary),
                                       this.activeModifiers,
                                       False,
                                       Map_Tool_Context'(this.lib,
                                                         mouse.x, mouse.y,
                                                         this.activeLayer) );
                end if;
                if tool.Acts_On( Entity_Subject ) then
                    tool.Apply_To_Entity( (if this.mouseButtons(Mouse_Left) then Primary else Secondary),
                                          this.activeModifiers,
                                          False,
                                          Entity_Tool_Context'(null,
                                                               mouse.x,
                                                               mouse.y) );
                end if;
            end if;
        end if;
    end On_Mouse_Moved;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Pressed( this  : not null access Tinker_Scene'Class;
                                mouse : Button_Arguments ) is
        tool : A_Tool;
    begin
        if this.Is_Map_Loaded then
            this.activeModifiers := mouse.modifiers;

            if (mouse.button = Mouse_Left and this.Get_Tinker_View.Get_Selected_Tool.Get_Type = Panner_Tool) or
               mouse.button = Mouse_Right
            then
                -- pan the map with panner tool, or any tool's right mouse button
                this.panStartX := mouse.x;
                this.panStartY := mouse.y;

            elsif mouse.button = Mouse_Left or mouse.button = Mouse_Middle then
                tool := this.Get_Tinker_View.Get_Selected_Tool;
                if tool.Acts_On( Map_Subject ) then
                    tool.Apply_To_Map( (if mouse.button = Mouse_Left then Primary else Secondary),
                                       this.activeModifiers,
                                       True,
                                       Map_Tool_Context'(this.lib,
                                                         mouse.x, mouse.y,
                                                         this.activeLayer) );
                end if;
                if tool.Acts_On( Entity_Subject ) then
                    tool.Apply_To_Entity( (if this.mouseButtons(Mouse_Left) then Primary else Secondary),
                                          this.activeModifiers,
                                          True,
                                          Entity_Tool_Context'(null,
                                                               mouse.x,
                                                               mouse.y) );
                end if;
            end if;
        end if;
        this.Update_Cursor;
    end On_Mouse_Pressed;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Released_Left( this  : not null access Tinker_Scene'Class;
                                      mouse : Button_Arguments ) is
        tool : A_Tool;
    begin
        -- complete the tool's primary action
        tool := this.Get_Tinker_View.Get_Selected_Tool;
        if tool.Get_Type = Panner_Tool then
            -- stop panning unless the right mouse button of the Panner_Tool
            -- (which also pans) is being held down.
            if not this.mouseButtons(Mouse_Right) then
                this.panStartX := -1.0;
                this.panStartY := -1.0;
            end if;
        else
            if tool.Acts_On( Map_Subject ) then
                tool.Post_Apply_Map( Primary,
                                     this.activeModifiers,
                                     Map_Tool_Context'(this.lib,
                                                       mouse.x, mouse.y,
                                                       this.activeLayer) );
            end if;
            if tool.Acts_On( Entity_Subject ) then
                tool.Post_Apply_Entity( Primary,
                                        this.activeModifiers,
                                        Entity_Tool_Context'(null,
                                                             mouse.x, mouse.y) );
            end if;
        end if;
        this.Update_Cursor;
    end On_Mouse_Released_Left;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Released_Middle( this  : not null access Tinker_Scene'Class;
                                        mouse : Button_Arguments ) is
        tool : A_Tool;
    begin
        -- complete the tool's sceondary action
        tool := this.Get_Tinker_View.Get_Selected_Tool;
        if this.Is_Layer_Visible( this.activeLayer ) or not tool.Is_Layer_Specific then
            if tool.Acts_On( Map_Subject ) then
                tool.Post_Apply_Map( Secondary,
                                     this.activeModifiers,
                                     Map_Tool_Context'(this.lib,
                                                       mouse.x, mouse.y,
                                                       this.activeLayer) );
            end if;
            if tool.Acts_On( Entity_Subject ) then
                tool.Post_Apply_Entity( Secondary,
                                        this.activeModifiers,
                                        Entity_Tool_Context'(null,
                                                             mouse.x, mouse.y) );
            end if;
        end if;
        this.Update_Cursor;
    end On_Mouse_Released_Middle;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Released_Right( this : not null access Tinker_Scene'Class ) is
    begin
        -- stop panning unless the left mouse button of the Panner_Tool (which
        -- also pans) is being held down.
        if not this.mouseButtons(Mouse_Left) or else this.Get_Tinker_View.Get_Selected_Tool.Get_Type /= Panner_Tool then
            this.panStartX := -1.0;
            this.panStartY := -1.0;
        end if;
        this.Update_Cursor;
    end On_Mouse_Released_Right;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Scrolled( this  : not null access Tinker_Scene'Class;
                                 mouse : Scroll_Arguments ) is
        dfx, dfy : Float;
        dz       : Float;
    begin
         dfx := mouse.x - (this.viewport.x + this.viewport.width / 2.0);          -- mouse distance from center
         dfy := mouse.y - (this.viewport.y + this.viewport.height / 2.0);         --
         dz := this.zoomFactor;
         if mouse.scrollAmt > 0.0 then
            for i in 1..Integer(mouse.scrollAmt*WHEEL_SENSITIVITY) loop
                this.Zoom_Out;
            end loop;
            dz := this.zoomFactor / dz;                   -- change in zoom
            this.Shift_Focus( dfx - dfx / dz, dfy - dfy / dz );
        else
            for i in 1..Integer(-mouse.scrollAmt*WHEEL_SENSITIVITY) loop
                this.Zoom_In;
            end loop;
            dz := this.zoomFactor / dz;                   -- change in zoom
            this.Shift_Focus( dfx - dfx / dz, dfy - dfy / dz );
        end if;
    end On_Mouse_Scrolled;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Entity_Added( this : access Tinker_Scene; entity : not null A_Entity ) is
    begin
        if this.pendingSelection.Contains( entity.Get_Entity_Id ) then
            this.Entity_Selection_Add( entity );
        end if;
    end On_Entity_Added;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Entity_Removed( this : access Tinker_Scene; entity : not null A_Entity ) is
    begin
        this.Entity_Selection_Remove( entity );
    end On_Entity_Removed;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Entered( this : not null access Tinker_Scene'Class ) is
    begin
        this.Update_Cursor;
    end On_Mouse_Entered;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Exited( this : not null access Tinker_Scene'Class ) is
    begin
        this.Notify_Tool_Position( -1.0, -1.0 );
    end On_Mouse_Exited;

    ----------------------------------------------------------------------------

    procedure On_Resized( this : not null access Tinker_Scene'Class ) is
    begin
        -- during a scene resize in tinker, ground the viewport to the world in
        -- the upper left corner, instead of in the center. this looks much
        -- better when resizing the application window and when opening/closing
        -- the palette on the right side. this call to Shift_Focus() won't
        -- scroll the contents of the scene, it will only move the scene focus
        -- point back to the center of the new viewport.
        this.Shift_Focus( 0.0, 0.0 );
    end On_Resized;

    ----------------------------------------------------------------------------

    procedure On_Tool_Selected( this : not null access Tinker_Scene'Class ) is
    begin
        this.Update_Cursor;
        this.Update_Layer_Tint;
    end On_Tool_Selected;

    ----------------------------------------------------------------------------

    procedure On_Map_Changed( this : not null access Tinker_Scene'Class ) is
    begin
        this.zoomTarget := this.zoomFactor;
        this.activeLayer := 0;
        this.Update_Cursor;
        this.Update_Layer_Tint;

        this.Tile_Area_Clear;

        -- don't call .Entity_Selection_Clear() because the entities that were
        -- selected before the world transition don't exist anymore.
        this.selectedSet.Clear;
        this.pendingSelection.Clear;
        this.sigSelectedCountChanged.Emit;
    end On_Map_Changed;

    ----------------------------------------------------------------------------

    procedure Set_Active_Layer( this  : not null access Tinker_Scene'Class;
                                layer : Integer ) is
    begin
        if this.Is_Map_Loaded then
            if layer in this.map.Get_Foreground_Layer..this.map.Get_Background_Layer then
                this.activeLayer := layer;
                this.Update_Layer_Tint;
            end if;
        end if;
    end Set_Active_Layer;

    ----------------------------------------------------------------------------

    procedure Set_Zoom_And_Focus( this : not null access Tinker_Scene'Class;
                                  zoom : Float ) is
        zoom2 : constant Float := Constrain( zoom, ZOOM_MIN, ZOOM_MAX );
    begin
        this.zoomTarget := zoom2;
        this.Zoom.Set( zoom2 );
        this.sigZoomChanged.Emit;

        -- changing the zoom doesn't cause .focusXY to be updated, so we can
        -- refocus on it.
        this.Shift_Focus( 0.0, 0.0 );
    end Set_Zoom_And_Focus;

    ----------------------------------------------------------------------------

    procedure Shift_Focus( this : not null access Tinker_Scene'Class; x, y : Float ) is
        halfWidth  : constant Float := this.viewport.width / 2.0;
        halfHeight : constant Float := this.viewport.height / 2.0;
        mapWidth   : Float;
        mapHeight  : Float;
    begin
        if not this.Is_Map_Loaded then
            return;
        end if;

        mapWidth := Float(this.map.Get_Width * this.tileWidth);
        mapHeight := Float(this.map.Get_Height * this.tileWidth);

        this.cam.Set_Focus( Constrain( this.cam.Get_FocusX + x, halfWidth, mapWidth - halfWidth ),
                            Constrain( this.cam.Get_FocusY + y, halfHeight, mapHeight - halfHeight ) );
    end Shift_Focus;

    ----------------------------------------------------------------------------

    overriding
    procedure Tick( this : access Tinker_Scene; time : Tick_Time ) is
        logZoom : Float;
    begin
        if this.zoomTarget < this.zoomFactor then
            -- zooming out
            logZoom := Log( this.zoomFactor ) - (ZOOM_MAX_LOG - ZOOM_MIN_LOG) * ZOOM_RATE * Float(To_Duration( time.elapsed ));
            this.Zoom.Set( Float'Max( this.zoomTarget, Exp( logZoom ) ) );
            this.sigZoomChanged.Emit;
            this.cam.Set_Focus( this.cam.Get_FocusX, this.cam.Get_FocusY, center => True );

        elsif this.zoomTarget > this.zoomFactor then
            -- zooming in
            logZoom := Log( this.zoomFactor ) + (ZOOM_MAX_LOG - ZOOM_MIN_LOG) * ZOOM_RATE * Float(To_Duration( time.elapsed ));
            this.Zoom.Set( Float'Min( Exp( logZoom ), this.zoomTarget ) );
            this.sigZoomChanged.Emit;
            this.cam.Set_Focus( this.cam.Get_FocusX, this.cam.Get_FocusY, center => True );
        end if;

        -- update camera position to keep the focus point near the center
        Scene(this.all).Tick( time );
    end Tick;

    ----------------------------------------------------------------------------

    procedure Tile_Area_Begin( this : not null access Tinker_Scene'Class; x, y : Float ) is
    begin
        this.tileArea.active := True;
        this.tileArea.x1 := x;
        this.tileArea.y1 := y;
        this.tileArea.x2 := this.tileArea.x1;
        this.tileArea.y2 := this.tileArea.y1;
        this.tileArea.sigAreaChanged.Emit;
    end Tile_Area_Begin;

    ----------------------------------------------------------------------------

    procedure Tile_Area_Clear( this : not null access Tinker_Scene'Class ) is
    begin
        this.tileArea.active := False;
        this.tileArea.x2 := this.tileArea.x1;
        this.tileArea.y2 := this.tileArea.y1;
        this.tileArea.sigAreaChanged.Emit;
    end Tile_Area_Clear;

    ----------------------------------------------------------------------------

    procedure Tile_Area_Confirm( this : not null access Tinker_Scene'Class ) is
    begin
        if this.tileArea.active then
            this.Tile_Area_Set( Get_Rect( this.tileArea ) );
        end if;
    end Tile_Area_Confirm;

    ----------------------------------------------------------------------------

    procedure Tile_Area_Set( this : not null access Tinker_Scene'Class; area : Rectangle ) is
    begin
        this.tileArea.active := (area.width * area.height) > 0.0;
        if this.tileArea.active then
            -- set the selected area
            this.tileArea.x1 := area.x;
            this.tileArea.y1 := area.y;
            this.tileArea.x2 := area.x + area.width;
            this.tileArea.y2 := area.y + area.height;
        else
            -- cleared the selected area
            this.tileArea.x2 := this.tileArea.x1;
            this.tileArea.y2 := this.tileArea.y1;
        end if;
        this.tileArea.sigAreaChanged.Emit;
    end Tile_Area_Set;

    ----------------------------------------------------------------------------

    procedure Tile_Area_Update( this : not null access Tinker_Scene'Class; x, y : Float ) is
    begin
        if this.tileArea.active then
            if x /= this.tileArea.x2 or y /= this.tileArea.y2 then
                this.tileArea.x2 := x;
                this.tileArea.y2 := y;
                this.tileArea.sigAreaChanged.Emit;
            end if;
        end if;
    end Tile_Area_Update;

    ----------------------------------------------------------------------------

    procedure Update_Cursor( this : not null access Tinker_Scene'Class ) is
    begin
        if this.Is_Map_Loaded then
            if this.Is_Panning then
                this.Set_Mouse_Cursor( "tinker:cursor-panning" );
            else
                this.Set_Mouse_Cursor( this.Get_Tinker_View.Get_Selected_Tool.Get_Cursor );
            end if;
        else
            this.Set_Mouse_Cursor( "NONE" );
        end if;
    end Update_Cursor;

    ----------------------------------------------------------------------------

    procedure Update_Layer_Tint( this : not null access Tinker_Scene'Class ) is
        tintTheActive  : Boolean;
        hideForeground : Boolean;
        tool           : A_Tool;
    begin
        if this.Is_Map_Loaded then
            tool := this.Get_Tinker_View.Get_Selected_Tool;

            -- apply both active and inactive tint colors when using a map tool
            -- that affects only the active layer, or when the active layer
            -- tinting is forced on.
            tintTheActive := (tool.Acts_On( Map_Subject ) and tool.Is_Layer_Specific)
                          or this.Is_Enabled( TINT_ACTIVE_LAYER );

            -- temporarily hide layers in front of the active layer when using
            -- a layer specific tool
            hideForeground := (tool.Acts_On( Map_Subject ) and tool.Is_Layer_Specific);

            for i in this.map.Get_Foreground_Layer..this.map.Get_Background_Layer loop
                this.Set_Layer_Secondary_Tint( i, (if i = this.activeLayer or not tintTheActive
                                                   then this.activeTint
                                                   else this.inactiveTint) );
                this.Set_Layer_Secondary_Visible( i, not hideForeground or i >= this.activeLayer );
            end loop;
        end if;
    end Update_Layer_Tint;

    ----------------------------------------------------------------------------

    procedure Zoom_In( this : not null access Tinker_Scene'Class ) is
        logZoom : Float;
    begin
        if not Get_Pref( "freezooming" ) then
            -- zoom in again if we're at or closely approaching the current zoom target
            if Log( this.zoomFactor ) + (ZOOM_MAX_LOG - ZOOM_MIN_LOG) * ZOOM_FUZZ >= Log( this.zoomTarget ) then
                if this.zoomTarget < 1.0 then
                    this.zoomTarget := 1.0;
                elsif this.zoomTarget < ZOOM_MAX then
                    this.zoomTarget := this.zoomTarget + 1.0;
                end if;
            end if;
        else
            logZoom := Log( this.zoomFactor ) + (ZOOM_MAX_LOG - ZOOM_MIN_LOG) * ZOOM_INCREMENT;
            this.Set_Zoom_And_Focus( Exp( logZoom ) );
        end if;
    end Zoom_In;

    ----------------------------------------------------------------------------

    procedure Zoom_Out( this : not null access Tinker_Scene'Class ) is
        logZoom : Float;
    begin
        if not Get_Pref( "freezooming" ) then
            -- zoom out again if we're at or closely approaching the current zoom target
            if Log( this.zoomFactor ) - (ZOOM_MAX_LOG - ZOOM_MIN_LOG) * ZOOM_FUZZ <= Log( this.zoomTarget ) then
                if this.zoomTarget = 1.0 then
                    this.zoomTarget := ZOOM_MIN;
                elsif this.zoomTarget > 1.0 then
                    this.zoomTarget := this.zoomTarget - 1.0;
                end if;
            end if;
        else
            logZoom := Log( this.zoomFactor ) - (ZOOM_MAX_LOG - ZOOM_MIN_LOG) * ZOOM_INCREMENT;
            this.Set_Zoom_And_Focus( Exp( logZoom ) );
        end if;
    end Zoom_Out;

begin

    Preferences.Set_Default( "scene", "active_tint", "#FFFFFF" );
    Preferences.Set_Default( "scene", "inactive_tint", "#404040" );

end Widgets.Scenes.Tinker;
