--
-- Copyright (c) 2017-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Events.Game;                       use Events.Game;
with Game_Views.Tinker;                 use Game_Views.Tinker;
with Icons;                             use Icons;
with Values.Casting;                    use Values.Casting;
with Values.Errors;                     use Values.Errors;
with Values.Strings;                    use Values.Strings;
with Widgets.Buttons;                   use Widgets.Buttons;
with Widgets.Buttons.Pushes;            use Widgets.Buttons.Pushes;
with Widgets.Labels;                    use Widgets.Labels;
with Widgets.Row_Layouts;               use Widgets.Row_Layouts;
with Widgets.Scroll_Panes;              use Widgets.Scroll_Panes;

package body Widgets.Panels.Console_Modules is

    package Connections is new Signals.Connections(Console_Module);
    use Connections;

    package Mouse_Connections is new Signals.Mouse.Connections(Console_Module);
    use Mouse_Connections;

    ----------------------------------------------------------------------------

    function Get_Tinker_View( this : not null access Console_Module'Class ) return A_Tinker_View is (A_Tinker_View(this.Get_View));

    procedure On_Accepted( this : not null access Console_Module'Class );

    procedure On_Clicked_Clear( this : not null access Console_Module'Class );

    procedure On_Clicked_Copy( this : not null access Console_Module'Class );

    procedure On_Clicked_Textbox( this : not null access Console_Module'Class );

    procedure On_Shown( this : not null access Console_Module'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Console_Module( view : not null access Game_Views.Tinker.Tinker_View'Class ) return A_Console_Module is
        this : constant A_Console_Module := new Console_Module;
    begin
        this.Construct( A_Tinker_View(view) );
        return this;
    end Create_Console_Module;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Console_Module;
                         view : not null access Game_Views.Game_View'Class ) is
        row        : A_Row_Layout;
        label      : A_Label;
        btn        : A_Button;
        scrollPane : A_Scroll_Pane;
    begin
        Panel(this.all).Construct( view, "", "Panel" );
        this.Listen_For_Event( EVT_SCRIPT_DONE );
        this.Listen_For_Event( EVT_CONSOLE_TEXT );

        this.Shown.Connect( Slot( this, On_Shown'Access ) );

        this.Set_Style( "console" );

        row := Create_Row_Layout( view );
        this.Add_Widget( row );
        row.Set_Style( "console" );
        row.Fill_Horizontal( this );
        row.Bottom.Attach( this.Bottom );

            label := Create_Label( view );
            label.Set_Preferred_Height( 24.0 );
            label.Set_Preferred_Width( 24.0 );
            label.Set_Style( "console" );
            label.Set_Text( ">" );
            row.Append( label );

            this.input := Create_Input_Box( view, this.Get_Id & ":input" );
            this.input.Set_Style( "console" );
            this.input.Enable_History( True );
            this.input.Accepted.Connect( Slot( this, On_Accepted'Access ) );
            this.input.Set_Prev( this.Get_Id & ":copy" );
            this.input.Set_Next( this.Get_Id & ":clear" );
            row.Append( this.input );
            row.Set_Expander( this.input );

            btn := Create_Push_Button( view, this.Get_Id & ":clear" );
            btn.Set_Style( "console" );
            btn.Set_Icon( Create_Icon( "tinker:clear-white-16" ) );
            btn.Set_Preferred_Width( 24.0 );
            btn.Clicked.Connect( Slot( this, On_Clicked_Clear'Access ) );
            btn.Set_Prev( this.Get_Id & ":input" );
            btn.Set_Next( this.Get_Id & ":copy" );
            row.Append( btn );

            btn := Create_Push_Button( view, this.Get_Id & ":copy" );
            btn.Set_Style( "console" );
            btn.Set_Icon( Create_Icon( "tinker:clipboard-white-16" ) );
            btn.Set_Preferred_Width( 24.0 );
            btn.Clicked.Connect( Slot( this, On_Clicked_Copy'Access ) );
            btn.Set_Prev( this.Get_Id & ":clear" );
            btn.Set_Next( this.Get_Id & ":input" );
            row.Append( btn );

        scrollPane := Create_Scroll_Pane( view, "console" );
        this.Add_Widget( scrollPane );
        scrollPane.Set_Style( "console" );
        scrollPane.Fill_Horizontal( this );
        scrollPane.Top.Attach( this.Top );
        scrollPane.Bottom.Attach( row.Top );
        scrollPane.Draw_Hbar( False );

            this.output := Create_Textbox( view, "consoleout" );
            this.output.Set_Style( "console" );
            this.output.MouseClicked.Connect( Slot( this, On_Clicked_Textbox'Access, Mouse_Any ) );
            scrollPane.Set_Client( this.output );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Event( this : access Console_Module;
                            evt  : in out A_Event;
                            resp : out Response_Type ) is
        pragma Unreferenced( resp );
        val : Value;
    begin
        if evt.Get_Event_Id = EVT_SCRIPT_DONE then
            val := A_Script_Event(evt).Get_Result;
            if val.Is_Error then
                this.output.Append_Line( val.Err.Get_Code'Img & ": " & val.Err.Get_Message );
            elsif not val.Is_Null then
                this.output.Append_Line( val.Image );
            end if;

        elsif evt.Get_Event_Id = EVT_CONSOLE_TEXT then
            if A_Script_Event(evt).Get_Result.Is_String then
                this.output.Append_Line( Cast_String( A_Script_Event(evt).Get_Result ) );
            elsif A_Script_Event(evt).Get_Result.Is_List then
                for i in 1..A_Script_Event(evt).Get_Result.Lst.Length loop
                    this.output.Append_Line( Cast_String( A_Script_Event(evt).Get_Result.Lst.Get( i ) ) );
                end loop;
            end if;

        end if;
    end Handle_Event;

    ----------------------------------------------------------------------------

    procedure On_Accepted( this : not null access Console_Module'Class ) is
    begin
        if this.input.Get_Text'Length = 0 then
            return;
        end if;

        -- Clear the undo/redo history because the script can damage game state
        -- assumed by Commands in the history.
        this.Get_Tinker_View.Commands.Clear;

        Queue_Run_Script( To_Unbounded_String( this.input.Get_Text ), this.nextTaskId );
        this.nextTaskId := this.nextTaskId + 1;
        this.input.Set_Text( "" );
    end On_Accepted;

    ----------------------------------------------------------------------------

    procedure On_Clicked_Clear( this : not null access Console_Module'Class ) is
    begin
        this.output.Clear;
    end On_Clicked_Clear;

    ----------------------------------------------------------------------------

    procedure On_Clicked_Copy( this : not null access Console_Module'Class ) is
    begin
        this.Get_View.Set_Clipboard( "text", Create( this.output.Get_Text ) );
    end On_Clicked_Copy;

    ----------------------------------------------------------------------------

    procedure On_Clicked_Textbox( this : not null access Console_Module'Class ) is
    begin
        this.input.Request_Focus;
    end On_Clicked_Textbox;

    ----------------------------------------------------------------------------

    procedure On_Shown( this : not null access Console_Module'Class ) is
    begin
        this.input.Request_Focus;
    end On_Shown;

end Widgets.Panels.Console_Modules;
