--
-- Copyright (c) 2017-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Indefinite_Ordered_Maps;
--with Debugging;                         use Debugging;
with Entities.Factory;                  use Entities.Factory;
with Entities.Templates;                use Entities.Templates;
with Events.World;                      use Events.World;
with Game_Views.Tinker;                 use Game_Views.Tinker;
with Preferences;                       use Preferences;
with Values.Errors;                     use Values.Errors;
with Values.Lists;                      use Values.Lists;
with Widgets.Palettes;                  use Widgets.Palettes;
with Widgets.Palettes.Entities;         use Widgets.Palettes.Entities;
with Widgets.Scroll_Panes;              use Widgets.Scroll_Panes;

package body Widgets.Panels.Spawner_Modules is

    procedure Load_Entities( this : not null access Spawner_Module'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Spawner_Module( view : not null access Game_Views.Tinker.Tinker_View'Class ) return A_Spawner_Module is
        this : constant A_Spawner_Module := new Spawner_Module;
    begin
        this.Construct( A_Tinker_View(view) );
        return this;
    end Create_Spawner_Module;

    ----------------------------------------------------------------------------

    procedure Append_Tab( this    : not null access Spawner_Module;
                          content : not null access Widget'Class;
                          title   : String;
                          icon    : Icon_Type := NO_ICON ) is
    begin
        this.tabs.Append( content, title, icon );
    end Append_Tab;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Spawner_Module;
                         view : not null access Game_Views.Game_View'Class ) is
    begin
        Panel(this.all).Construct( view, "", "Panel" );
        this.Listen_For_Event( EVT_WORLD_LOADED );

        this.Set_Style( "entityPalette" );

        this.tabs := Create_Tab_View( view );
        this.Add_Widget( this.tabs );
        this.tabs.Fill( this );
        this.tabs.Set_Style( "entityPalette" );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Event( this : access Spawner_Module;
                            evt  : in out A_Event;
                            resp : out Response_Type ) is
        pragma Unreferenced( resp );
    begin
        if evt.Get_Event_Id = EVT_WORLD_LOADED then
            this.Load_Entities;
            this.tabs.Set_Index( 1 );
        end if;
    end Handle_Event;

    ----------------------------------------------------------------------------

    procedure Load_Entities( this : not null access Spawner_Module'Class ) is

        package Palette_Maps is new Ada.Containers.Indefinite_Ordered_Maps(String, A_Entity_Palette, "<", "=");
        use Palette_Maps;

        pals : Palette_Maps.Map;

        ------------------------------------------------------------------------

        function Add_Tab( title : String; iconSize : Float ) return A_Entity_Palette is
            scrollPane : A_Scroll_Pane;
            palette    : A_Entity_Palette;
        begin
            scrollPane := Create_Scroll_Pane( this.Get_View );
            scrollPane.Draw_Hbar( False );
            scrollPane.Set_Style( "entityPalette" );

            palette := Create_Entity_Palette( this.Get_View );
            scrollPane.Set_Client( palette );
            palette.Set_Style( "entityPalette" );
            palette.Set_Icon_Size( iconSize );
            this.tabs.Insert( 1, scrollPane, title );
            return palette;
        end Add_Tab;

        ------------------------------------------------------------------------

        procedure Examine( template : A_Template ) is
            palName : constant String := template.Get_Tool_Parameters.Get_String( "palette" );
        begin
            if palName'Length > 0 then
                if not pals.Contains( palName ) then
                    pals.Insert( palName, Add_Tab( palName, Get_Pref( "application", "entityPaletteSize", 48.0 ) ) );
                end if;
                pals.Element( palName ).Add_Entity( template.Get_Name,
                                                    Create_Icon( template.Get_Tool_Parameters.Get_String( "icon" ) ) );
            end if;
        end Examine;

        ------------------------------------------------------------------------

    begin
        if this.loaded then
            return;
        end if;
        this.loaded := True;

        Entities.Factory.Global.Iterate_Templates( Examine'Access );
    end Load_Entities;

end Widgets.Panels.Spawner_Modules;
