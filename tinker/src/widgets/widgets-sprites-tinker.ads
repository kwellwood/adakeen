--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Keyboard;                          use Keyboard;
with Widgets.Menu_Items.Sub_Menus;      use Widgets.Menu_Items.Sub_Menus;

limited with Game_Views;

package Widgets.Sprites.Tinker is

    type Tinker_Sprite is new Sprite with private;
    type A_Tinker_Sprite is access all Tinker_Sprite'Class;

    -- Creates a new Tinker_Sprite for use in the Tinker editor, visually
    -- representity entity 'entity'.
    function Create_Tinker_Sprite( view    : not null access Game_Views.Game_View'Class;
                                   entity  : not null A_Entity;
                                   libName : String;
                                   frame   : Natural ) return A_Sprite;

private

    type Tinker_Sprite is new Sprite with
        record
            minEWidth       : Float := 0.0;        -- min entity width when resized by mouse
            minEHeight      : Float := 0.0;        -- min entity height when resized by mouse
            snapCenterX     : Boolean := False;    -- snaps the center of the object to the grid in the X axis
            snapCenterY     : Boolean := False;    -- snaps the center of the object to the grid in the Y axis
            snapEnabled     : Boolean := False;    -- gridsnap the entity?

            resizableW      : Boolean := False;   -- user can resize the entity's width?
            resizableH      : Boolean := False;   -- user can resize the entity's height?

            mouseX,                                -- last known coordinates of the
            mouseY          : Float := -1.0;       --   mouse in the sprite
            activeModifiers : Modifiers_Array;

            menuX           : Float := 0.0;        -- coordinates where the popup
            menuY           : Float := 0.0;        --   menu will be shown
            menu            : A_Sub_Menu := null;  -- right-click popup menu (belongs to this)

            infoPanel       : A_Widget := null;    -- attributes info panel
        end record;

    procedure Construct( this    : access Tinker_Sprite;
                         view    : not null access Game_Views.Game_View'Class;
                         entity  : not null A_Entity;
                         libName : String;
                         frame   : Natural );

    procedure Delete( this : in out Tinker_Sprite );

    procedure Draw_Content( this : access Tinker_Sprite );

    function Get_Min_Height( this : access Tinker_Sprite ) return Float;

    function Get_Min_Width( this : access Tinker_Sprite ) return Float;

    function Get_Visual_State( this : access Tinker_Sprite ) return Widget_State;

end Widgets.Sprites.Tinker;
