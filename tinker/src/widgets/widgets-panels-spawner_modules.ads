--
-- Copyright (c) 2017-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Icons;                             use Icons;
with Widgets.Tab_Views;                 use Widgets.Tab_Views;

limited with Game_Views.Tinker;

package Widgets.Panels.Spawner_Modules is

    type Spawner_Module is new Panel with private;
    type A_Spawner_Module is access all Spawner_Module'Class;

    function Create_Spawner_Module( view : not null access Game_Views.Tinker.Tinker_View'Class ) return A_Spawner_Module;
    pragma Postcondition( Create_Spawner_Module'Result /= null );

    procedure Append_Tab( this    : not null access Spawner_Module;
                          content : not null access Widget'Class;
                          title   : String;
                          icon    : Icon_Type := NO_ICON );

private

    type Spawner_Module is new Panel with
        record
            loaded : Boolean := False;
            tabs   : A_Tab_View;
        end record;

    procedure Construct( this : access Spawner_Module;
                         view : not null access Game_Views.Game_View'Class );

    procedure Handle_Event( this : access Spawner_Module;
                            evt  : in out A_Event;
                            resp : out Response_Type );

end Widgets.Panels.Spawner_Modules;
