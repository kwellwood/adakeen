--
-- Copyright (c) 2017-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.State;                     use Allegro.State;
with Allegro.Transformations;           use Allegro.Transformations;
--with Debugging;                         use Debugging;
with Drawing;                           use Drawing;
with Drawing.Bitmaps;                   use Drawing.Bitmaps;
with Drawing.Primitives;                use Drawing.Primitives;
with Game_Views;                        use Game_Views;
with Interfaces;                        use Interfaces;
with Renderers;
with Events.World;                      use Events.World;

package body Widgets.Scenes.Tinker.Mini_Maps is

    package Connections is new Signals.Connections(Mini_Map);
    use Connections;

    package Mouse_Connections is new Signals.Mouse.Connections(Mini_Map);
    use Mouse_Connections;

    ----------------------------------------------------------------------------

    procedure Focus_Scene_At( this : not null access Mini_Map'Class; x, y : Float );

    procedure On_Mouse_Pressed( this  : not null access Mini_Map'Class;
                                mouse : Button_Arguments );

    procedure On_Mouse_Moved( this  : not null access Mini_Map'Class;
                              mouse : Mouse_Arguments );

    procedure On_Mouse_Released( this  : not null access Mini_Map'Class;
                                 mouse : Button_Arguments );

    procedure On_Mouse_Scrolled( this  : not null access Mini_Map'Class;
                                 mouse : Scroll_Arguments );

    procedure On_Resized( this : not null access Mini_Map'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Mini_Map( view : not null access Game_Views.Game_View'Class ) return A_Mini_Map is
        this : constant A_Mini_Map := new Mini_Map;
    begin
        this.Construct( view );
        return this;
    end Create_Mini_Map;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Mini_Map;
                         view : not null access Game_Views.Game_View'Class ) is
    begin
        Widget(this.all).Construct( view, "", "MiniMap" );
        this.Listen_For_Event( EVT_WORLD_LOADED );
        this.Listen_For_Event( EVT_TILE_CHANGED );
        this.MousePressed.Connect( Slot( this, On_Mouse_Pressed'Access ) );
        this.MouseMoved.Connect( Slot( this, On_Mouse_Moved'Access ) );
        this.MouseReleased.Connect( Slot( this, On_Mouse_Released'Access ) );
        this.MouseScrolled.Connect( Slot( this, On_Mouse_Scrolled'Access ) );
        this.Resized.Connect( Slot( this, On_Resized'Access ) );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Mini_Map ) is
    begin
        Al_Destroy_Bitmap( this.buffer );
        Widget(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Content( this : access Mini_Map ) is
        shadeColor  : constant Allegro_Color := Al_Map_RGBA_f( 0.0, 0.0, 0.0, 0.5 );
        worldWidth  : Float := 0.0;
        worldHeight : Float := 0.0;
        sceneView   : Rectangle;
        scaleToView : Float := 0.0;
        scaleToBuf  : Float := 0.0;
        state       : Allegro_State;
        trans       : Allegro_Transform;
        drawX,
        drawY       : Float;
        vx, vy,
        vw, vh      : Float;
    begin
        if this.buffer = null or else this.scene = null or else not this.scene.Is_Map_Loaded then
            return;
        end if;

        worldWidth  := Float(this.scene.Get_Tile_Width * this.scene.Get_Width_Tiles);
        worldHeight := Float(this.scene.Get_Tile_Width * this.scene.Get_Height_Tiles);
        sceneView   := this.scene.Get_Viewport;

        -- scale from world coordinates to mini map coordinates so that it fits
        -- completely within the mini map viewport and preserves the world map's
        -- perspective.
        scaleToView := Float'Min( this.viewport.width / worldWidth, this.viewport.height / worldHeight );

        -- drawing offsets for centering the map in the viewport (in viewport coordinates)
        drawX := (this.viewport.width - worldWidth * scaleToView) / 2.0;
        drawY := (this.viewport.height - worldHeight * scaleToView) / 2.0;

        -- scale from world coordinates to the mini map's buffer coordinates,
        -- which is a multiple of the actual viewport size. the buffer is larger
        -- than the viewport so that it can be scaled down using linear
        -- filtering for better image quality.
        scaleToBuf := scaleToView * BUFFER_SCALE;

        if this.redraw then
            Al_Store_State( state, ALLEGRO_STATE_TARGET_BITMAP );

            Set_Target_Bitmap( this.buffer );
            Clear_To_Color( Transparent );
            Al_Clear_Depth_Buffer( 1.0 );                 -- infinite depth

            if this.scene.Is_Map_Loaded then
                Al_Identity_Transform( trans );
                Al_Scale_Transform_3d( trans, scaleToBuf, scaleToBuf, Renderers.Z_SCALE );
                Al_Use_Transform( trans );

                this.scene.Draw_Customized( viewport => (0.0, 0.0, worldWidth, worldHeight),
                                            options  => SHOW_ALL_LAYERS );
            end if;

            Al_Restore_State( state );
            this.redraw := False;
        end if;

        -- draw the background
        Rectfill( 0.0, 0.0, this.viewport.width, this.viewport.height, 0.0, Black );

        -- draw the minimap
        Draw_Bitmap_Region_Stretched( this.buffer,
                                      0.0, 0.0,
                                      this.viewport.width * BUFFER_SCALE, this.viewport.height * BUFFER_SCALE,
                                      drawX, drawY, 0.0,
                                      this.viewport.width, this.viewport.height );

        vx := drawX + sceneView.x * scaleToView;
        vy := drawY + sceneView.y * scaleToView;
        vw := Float'Min( sceneView.width, worldWidth ) * scaleToView;
        vh := Float'Min( sceneView.height, worldHeight ) * scaleToView;

        -- draw a shaded area around the selection:
        -- top left, top right, bottom left, bottom right
        --
        --   +-----+--------------+
        --   |     |       2      |
        --   |     +--------+-----+
        --   |  1  |        |     |
        --   |     |  View  |     |
        --   |     |        |  4  |
        --   +-----+--------+     |
        --   |      3       |     |
        --   +--------------+-----+
        Rectfill_XYWH( 0.0,     0.0,     vx,                                           vy + vh,                                       shadeColor );
        Rectfill_XYWH( vx,      0.0,     drawX - vx + worldWidth * scaleToView,        vy,                                            shadeColor );
        Rectfill_XYWH( 0.0,     vy + vh, vx + vw,                                      drawY - (vy + vh) + worldHeight * scaleToView, shadeColor );
        Rectfill_XYWH( vx + vw, vy,      drawX - (vx + vw) + worldWidth * scaleToView, drawY - vy + worldHeight * scaleToView,        shadeColor );

        -- draw a rectangle around the viewport
        Rect_XYWH( vx, vy, vw, vh, White );
    end Draw_Content;

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Event( this : access Mini_Map;
                            evt  : in out A_Event;
                            resp : out Response_Type ) is
    begin
        if evt.Get_Event_Id = EVT_WORLD_LOADED then
            this.redraw := True;
        elsif evt.Get_Event_Id = EVT_TILE_CHANGED then
            this.redraw := True;
        else
            Widget(this.all).Handle_Event( evt, resp );
        end if;
    end Handle_Event;

    ----------------------------------------------------------------------------

    procedure Focus_Scene_At( this : not null access Mini_Map'Class; x, y : Float ) is
        sceneWidth  : Float;
        sceneHeight : Float;
        scaleToView : Float;
        drawX       : Float;
        drawY       : Float;
    begin
        if this.scene = null or else not this.scene.Is_Map_Loaded then
            return;
        end if;

        sceneWidth  := Float(this.scene.Get_Tile_Width * this.scene.Get_Width_Tiles);
        sceneHeight := Float(this.scene.Get_Tile_Width * this.scene.Get_Height_Tiles);

        -- scale from world coordinates to mini map coordinates so that it fits
        -- completely within the mini map viewport and preserves the world map's
        -- perspective.
        scaleToView := Float'Min( this.viewport.width / sceneWidth, this.viewport.height / sceneHeight );

        -- drawing offsets for centering the map in the viewport (in viewport coordinates)
        drawX := (this.viewport.width - sceneWidth * scaleToView) / 2.0;
        drawY := (this.viewport.height - sceneHeight * scaleToView) / 2.0;

        this.scene.Set_Focus( (x - drawX) / scaleToView, (y - drawY) / scaleToView, center => True );
    end Focus_Scene_At;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Pressed( this  : not null access Mini_Map'Class;
                                mouse : Button_Arguments ) is
    begin
        if mouse.button = Mouse_Left then
            this.dragging := True;
            this.Focus_Scene_At( mouse.x, mouse.y );
        end if;
    end On_Mouse_Pressed;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Moved( this  : not null access Mini_Map'Class;
                              mouse : Mouse_Arguments ) is
    begin
        if this.dragging then
            this.Focus_Scene_At( mouse.x, mouse.y );
        end if;
    end On_Mouse_Moved;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Released( this  : not null access Mini_Map'Class;
                                 mouse : Button_Arguments ) is
    begin
        if mouse.button = Mouse_Left then
            this.dragging := False;
        end if;
    end On_Mouse_Released;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Scrolled( this  : not null access Mini_Map'Class;
                                 mouse : Scroll_Arguments ) is
    begin
         if mouse.scrollAmt > 0.0 then
            for i in 1..Integer(mouse.scrollAmt * WHEEL_SENSITIVITY) loop
                this.scene.Zoom_Out;
            end loop;
        else
            for i in 1..Integer(-mouse.scrollAmt * WHEEL_SENSITIVITY) loop
                this.scene.Zoom_In;
            end loop;
        end if;
    end On_Mouse_Scrolled;

    ----------------------------------------------------------------------------

    procedure On_Resized( this : not null access Mini_Map'Class ) is
        minWidth  : constant Integer := Integer(Float'Ceiling( this.viewport.width * BUFFER_SCALE ));
        minHeight : constant Integer := Integer(Float'Ceiling( this.viewport.height * BUFFER_SCALE ));
        state     : Allegro_State;
    begin
        if this.buffer /= null and then
           (minWidth > Al_Get_Bitmap_Width( this.buffer ) or else
            minHeight > Al_Get_Bitmap_Height( this.buffer ))
        then
            Al_Destroy_Bitmap( this.buffer );
        end if;
        if this.buffer = null and minWidth >= 16 and minHeight >= 16 then
            Al_Store_State( state, ALLEGRO_STATE_BITMAP );

            Al_Set_New_Bitmap_Flags( ALLEGRO_VIDEO_BITMAP or
                                     ALLEGRO_MIN_LINEAR or
                                     ALLEGRO_NO_PRESERVE_TEXTURE );
            Al_Set_New_Bitmap_Depth( Al_Get_Display_Option( this.view.Get_Display, ALLEGRO_DEPTH_SIZE ) );
            this.buffer := Al_Create_Bitmap( minWidth, minHeight );
            Al_Set_New_Bitmap_Depth( 0 );
            Set_Target_Bitmap( this.buffer );
            this.view.Use_Shader( "default" );      -- initialize the shader to use

            Al_Restore_State( state );
        end if;
        this.redraw := True;
    end On_Resized;

    ----------------------------------------------------------------------------

    procedure Set_Scene( this  : not null access Mini_Map'Class; scene : A_Tinker_Scene ) is
    begin
        if scene /= this.scene then
            this.scene := scene;
            this.redraw := True;
        end if;
    end Set_Scene;

end Widgets.Scenes.Tinker.Mini_Maps;
