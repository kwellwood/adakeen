--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Icons;                             use Icons;
with Keyboard;                          use Keyboard;

package Widgets.Scenes.Tinker is

    type Select_Mode is (Select_Normal, Select_Toggle, Select_Include);

    ----------------------------------------------------------------------------

    type Tinker_Scene is new Scene with private;
    type A_Tinker_Scene is access all Tinker_Scene'Class;

    function Create_Scene( view : not null access Game_Views.Game_View'Class;
                           id   : String ) return A_Tinker_Scene;
    pragma Postcondition( Create_Scene'Result /= null );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Signals

    function EntityAreaChanged( this : not null access Tinker_Scene'Class ) return access Signal'Class;
    function SelectedCountChanged( this : not null access Tinker_Scene'Class ) return access Signal'Class;
    function TileAreaChanged( this : not null access Tinker_Scene'Class ) return access Signal'Class;
    function ToolPositionChanged( this : not null access Tinker_Scene'Class ) return access Signal'Class;
    function ZoomChanged(this : not null access Tinker_Scene'Class ) return access Signal'Class;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Drawing Options

    -- Draws area 'viewport' of the scene to the target bitmap, using the given
    -- drawing options 'options' and no zooming. The scene will be translated
    -- such that 0,0 in world coordinates will be drawn at 0,0. Any desired
    -- transformations or clipping should be applied to the target bitmap by the
    -- caller first.
    --
    -- Note that 'options' will completely override the current options
    procedure Draw_Customized( this     : not null access Tinker_Scene'Class;
                               viewport : Rectangle;
                               options  : Render_Options );

    -- Creates a new bitmap and renders the entire scene into it, regardless of
    -- the viewport. This is used for exporting a world to an image file.
    function Draw_To_Bitmap( this : not null access Tinker_Scene'Class ) return A_Allegro_Bitmap;

    -- Enable/disable drawing certain information.
    procedure Enable_Draw_Grid( this : not null access Tinker_Scene'class; grid : String );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Tools

    -- Returns the layer currently active for tools.
    function Get_Active_Layer( this : not null access Tinker_Scene'Class ) return Integer;

    -- Returns the tool's X position in world coordinates, or < 0.0 if the mouse
    -- is not currently over the map area.
    function Get_Tool_X( this : not null access Tinker_Scene'Class ) return Float;

    -- Returns the tool's Y position in world coordinates, or < 0.0 if the mouse
    -- is not currently over the map area.
    function Get_Tool_Y( this : not null access Tinker_Scene'Class ) return Float;

    -- Notifies the scene of the tool position in world coordinates, if the
    -- mouse is hovering over an entity widget instead of the scene itself.
    procedure Notify_Tool_Position( this : not null access Tinker_Scene'Class; wx, wy : Float );

    -- Sets the active layer for editing tiles.
    procedure Set_Active_Layer( this  : not null access Tinker_Scene'Class;
                                layer : Integer );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Entity Selection

    -- Copies the selected entities to the clipboard.
    procedure Copy_Selected_Entities( this : not null access Tinker_Scene'Class );

    -- Selects all entities in the world.
    procedure Entity_Select_All( this : not null access Tinker_Scene'Class );

    -- Selects 'entity', adding it to the selection.
    procedure Entity_Selection_Add( this   : not null access Tinker_Scene'Class;
                                    entity : not null A_Entity );

    -- Adds entity 'eid' to the entity selection when it is created later.
    procedure Entity_Selection_Add_Pending( this : not null access Tinker_Scene'Class;
                                            eid  : Entity_Id );

    -- Clears the current entity selection, unselecting all entities.
    procedure Entity_Selection_Clear( this : not null access Tinker_Scene'Class );

    -- Unselects 'entity', removing it from the selection.
    procedure Entity_Selection_Remove( this   : not null access Tinker_Scene'Class;
                                       entity : not null A_Entity );

    -- Selects only 'entity', clearing the rest of the selection. Passing null
    -- will simply clear the selection.
    procedure Entity_Selection_Set( this   : not null access Tinker_Scene'Class;
                                    entity : A_Entity );

    -- Selects 'entity', if it's not part of the selection, or unselects it if
    -- it is part of the selection.
    procedure Entity_Selection_Toggle( this   : not null access Tinker_Scene'Class;
                                       entity : not null A_Entity );

    -- Begins an entity selection area driven by the mouse, with the starting
    -- point at 'x,y' in world coordinates. The opposite corner of the selection
    -- area is updated by calling Entity_Area_Update() as the mouse moves. When
    -- the mouse button is released, call Entity_Area_Confirm() to end the
    -- selection operation and select the entities or tiles it encompasses.
    procedure Entity_Area_Begin( this : not null access Tinker_Scene'Class; x, y : Float );

    -- Completes the active entity selection operation, confirming that all
    -- objects within the selection area should be selected.
    procedure Entity_Area_Confirm( this : not null access Tinker_Scene'Class );

    -- Updates one corner of the entity selection area, if it's active. Call
    -- this when the mouse moves during a selection operation.
    procedure Entity_Area_Update( this : not null access Tinker_Scene'Class;
                                  x, y : Float;
                                  mode : Select_Mode );

    -- Returns the current entity selection area in world units. Its size will
    -- be 0.0 if no selection is active.
    function Get_Entity_Area( this : not null access Tinker_Scene'Class ) return Rectangle;

    -- Returns the number of selected entities.
    function Get_Entity_Selection_Count( this : not null access Tinker_Scene'Class ) return Natural;

    -- Returns a deep copy of the selected entities. The entities' widgets are
    -- not copied.
    function Get_Selected_Entities( this : not null access Tinker_Scene'Class ) return Entity_Sets.Set;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Tile Selection

    -- Returns True if 'col,row' in tiles is within the active area for tile
    -- editing. The active area includes the whole map, or it will be limited to
    -- the selected area if a tile selection is active. This is useful fo
    -- tile-based Tools to determine if a tile is editable.
    function In_Active_Area( this     : not null access Tinker_Scene'Class;
                             col, row : Integer ) return Boolean;

    -- Returns True if a tile area is currently selected.
    function Is_Tile_Area_Active( this : not null access Tinker_Scene'Class ) return Boolean;

    -- Returns the current tile selection area in world units. Its size will
    -- be 0.0 if no selection is active.
    function Get_Tile_Area( this : not null access Tinker_Scene'Class ) return Rectangle;

    -- Begins a tile selection area driven by the mouse, with the starting point
    -- at 'x,y' in world coordinates. The opposite corner of the selection
    -- area is updated by calling Tile_Selection_Update() as the mouse moves.
    -- When the mouse button is released, call Tile_Selection_Confirm() to end
    -- the comfirm the selection area.
    procedure Tile_Area_Begin( this : not null access Tinker_Scene'Class; x, y : Float );

    -- Clears the current tile selection area, if any.
    procedure Tile_Area_Clear( this : not null access Tinker_Scene'Class );

    -- Completes the active tile selection operation, making the selection
    -- available as a constraint for all tile tools.
    procedure Tile_Area_Confirm( this : not null access Tinker_Scene'Class );

    -- Sets the tile selection area. If the size of 'area' is 0, the tile
    -- selection will be cleared.
    procedure Tile_Area_Set( this : not null access Tinker_Scene'Class; area : Rectangle );

    -- Updates one corner of the tile selection area, if it's active. Call this
    -- when the mouse moves during a selection operation.
    procedure Tile_Area_Update( this : not null access Tinker_Scene'Class; x, y : Float );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Viewport Focus

    -- Returns True when the mouse is within the scene's viewport on the screen.
    function Is_Mouse_In_Viewport( this : not null access Tinker_Scene'Class ) return Boolean;

    -- Sets the absolute zoom factor 'zoom' for the scene, keeping the current
    -- focus point centered.
    procedure Set_Zoom_And_Focus( this : not null access Tinker_Scene'Class;
                                  zoom : Float );

    -- Moves the focus point of the scene relative to it's current location,
    -- in world units. The center of the scene is bounded such that the viewport
    -- never goes off the top or left of the world, and only up to 25% of the
    -- viewport size off the bottom or right edge.
    procedure Shift_Focus( this : not null access Tinker_Scene'Class; x, y : Float );

    -- Zooms in, keeping the scene focus centered.
    procedure Zoom_In( this : not null access Tinker_Scene'Class );

    -- Zooms out, keeping the scene focus centered.
    procedure Zoom_Out( this : not null access Tinker_Scene'Class );

    ----------------------------------------------------------------------------

    SHOW_MAP_TOOL      : constant Render_Options := 2**16;   -- draw map tool preview
    SHOW_CONNECTIONS   : constant Render_Options := 2**17;   -- draw connections between entities
    SHOW_SELECTION     : constant Render_Options := 2**18;   -- draw the selection in progress
    SHOW_OUTLINES      : constant Render_Options := 2**19;   -- draw outlines around tiles
    SHOW_CLIPPING      : constant Render_Options := 2**20;   -- draw solid clipping shapes on top
    SHOW_GRID          : constant Render_Options := 2**21;   -- draw the configured grid bitmap
    TINT_ACTIVE_LAYER  : constant Render_Options := 2**22;   -- always tint the active layer

    TINKER_SCENE_DEFAULTS : constant Render_Options := SCENE_DEFAULTS
                                                    or SHOW_SPRITES_FRONT
                                                    or SHOW_MAP_TOOL
                                                    or SHOW_CONNECTIONS
                                                    or SHOW_SELECTION
                                                    or SHOW_GRID;

    ZOOM_MIN : constant := 0.5;         -- zoom minimum (out)
    ZOOM_MAX : constant := 4.0;         -- zoom maximum (in)

    -- mouse wheel sensitivity for zooming
    WHEEL_SENSITIVITY : constant := 2.0;

private

    pragma Inline( Get_Active_Layer );
    pragma Inline( Get_Entity_Area );
    pragma Inline( Get_Entity_Selection_Count );
    pragma Inline( Get_Selected_Entities );
    pragma Inline( Get_Tile_Area );
    pragma Inline( Get_Tool_X );
    pragma Inline( Get_Tool_Y );
    pragma Inline( Is_Tile_Area_Active );

    type Selection_Type is
        record
            active         : Boolean := False;      -- visible / in-use
            resizing       : Boolean := False;      -- currently being resized (implies .active = true)
            x1, y1, x2, y2 : Float := 0.0;
            sigAreaChanged : aliased Signal;
        end record;

    function Get_Rect( s : Selection_Type ) return Rectangle is (if s.active then Rectangle'(Float'Min( s.x1, s.x2 ), Float'Min( s.y1, s.y2 ), abs (s.x2 - s.x1), abs (s.y2 - s.y1)) else Rectangle'(others => 0.0));

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Provides an ordered Set of entity ids.
    package Entity_Id_Sets is new Ada.Containers.Ordered_Sets( Entity_Id, "<", "=" );
    use Entity_Id_Sets;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Tinker_Scene is new Scene with
        record
            sigSelectedCountChanged : aliased Signal;
            sigToolPositionChanged  : aliased Signal;
            sigZoomChanged          : aliased Signal;

            activeModifiers : Modifiers_Array := Modifiers_Array'(others=>False);
            activeLayer     : Integer := 0;
            scrollSpeed     : Float := 0.0;
            zoomTarget      : Float := 1.0;

            gridIcon        : Icon_Type;
            drawGrid        : Unbounded_String;
            activeTint      : Allegro_Color := White;
            inactiveTint    : Allegro_Color := White;

            toolX : Float := -1.0;      -- the tool's current world position when
            toolY : Float := -1.0;      -- the mouse is hovering over the map

            -- used by the panner tool
            panStartX : Float := -1.0;
            panStartY : Float := -1.0;

            -- used by the pointer tool
            selectedSet      : Entity_Sets.Set;      -- set of selected entities
            wasSelected      : Entity_Sets.Set;
            pendingSelection : Entity_Id_Sets.Set;   -- to be selected when created
            entityArea       : Selection_Type;
            tileArea         : Selection_Type;
        end record;

    procedure Construct( this : access Tinker_Scene;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );

    procedure Draw_Content_Foreground( this : access Tinker_Scene );

    procedure Draw_Layer_Overlay( this : access Tinker_Scene; layer : Integer );

    procedure Find_Widget_At( this     : access Tinker_Scene;
                              x, y     : Float;
                              children : Boolean;
                              wx, wy   : out Float;
                              found    : out A_Widget );

    function Create_Entity_Widget( this         : access Tinker_Scene;
                                   entity       : not null A_Entity;
                                   initialState : Map_Value ) return A_Widget;

    -- Returns True if the mouse is panning the map around. This can be done
    -- with the right mouse button, or the left mouse button of PANNER_TOOL. The
    -- .panStartX and .panStartY fields indicate the panning state.
    function Is_Panning( this : not null access Tinker_Scene'Class ) return Boolean with Inline;

    procedure On_Entity_Added( this : access Tinker_Scene; entity : not null A_Entity );

    procedure On_Entity_Removed( this : access Tinker_Scene; entity : not null A_Entity );

    procedure Tick( this : access Tinker_Scene; time : Tick_Time );

end Widgets.Scenes.Tinker;
