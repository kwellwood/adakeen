--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Assets.Libraries;                  use Assets.Libraries;
with Clipping.Drawing;
--with Debugging;                         use Debugging;
with Events.World;                      use Events.World;
with Game_Views;                        use Game_Views;
with Game_Views.Tinker;                 use Game_Views.Tinker;
with Palette.Tango;                     use Palette.Tango;
with Tiles;                             use Tiles;
with Tools;                             use Tools;
with Tools.Painters;                    use Tools.Painters;
with Widgets.Panels.Properties.Tiles;   use Widgets.Panels.Properties.Tiles;

package body Widgets.Palettes.Tiles is

    package Connections is new Signals.Connections(Tile_Palette);
    use Connections;

    ----------------------------------------------------------------------------

    function Get_Tinker_View( this : not null access Tile_Palette'Class ) return A_Tinker_View is (A_Tinker_View(this.Get_View));

    procedure On_Tool_Selected( this : not null access Tile_Palette'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Tile_Palette( view : not null access Game_Views.Game_View'Class;
                                  id   : String := "" ) return A_Palette is
        this : A_Tile_Palette := new Tile_Palette;
    begin
        this.Construct( view, id );
        return A_Palette(this);
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Tile_Palette;

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access Tile_Palette;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String ) is
    begin
        Palette(this.all).Construct( view, id );
        this.Get_Tinker_View.ToolChanged.Connect( Slot( this, On_Tool_Selected'Access ) );
        this.Listen_For_Event( EVT_WORLD_LOADED );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Tile_Palette ) is
    begin
        this.Hide_Info_Panel;
        Palette(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Decoration( this : access Tile_Palette; index : Positive ) is
    begin
        Clipping.Drawing.Draw( this.items.Element( index ).Get_Tile.Get_Shape, 0.0, 0.0, 0.0, Integer(this.Get_Icon_Size), Butter1, Scarlet1 );
    end Draw_Decoration;

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Event( this : access Tile_Palette;
                            evt  : in out A_Event;
                            resp : out Response_Type ) is
        pragma Unreferenced( resp );
    begin
        if evt.Get_Event_Id = EVT_WORLD_LOADED then
            this.Set_Library_Name( A_World_Loaded_Event(evt).Get_Library_Name );
        end if;
    end Handle_Event;

    ----------------------------------------------------------------------------

    procedure Hide_Info_Panel( this : not null access Tile_Palette'Class ) is
    begin
        if this.infoPanel /= null then
            this.Get_Tinker_View.Hide_Info_Panel( this.infoPanel );
            Delete( this.infoPanel );
        end if;
    end Hide_Info_Panel;

    ----------------------------------------------------------------------------

    overriding
    procedure Select_Item( this : access Tile_Palette; index : Natural ) is
    begin
        this.Get_Tinker_View.Replace_Tool( Create_Painter( this.tileIds.Element( index ) ) );
        this.Get_Tinker_View.Select_Tool( Painter_Tool );

        -- Note: The selected item needs to be set after Select_Tool because the
        -- On_Tool_Selected() handler doesn't know exactly which index was
        -- selected, it only knows to select the first slot with a tile id that
        -- matches the tool. If multiple slots have the same tile id, it will
        -- only choose the first slot.
        this.selectedItem := index;
    end Select_Item;

    ----------------------------------------------------------------------------

    procedure On_Tool_Selected( this : not null access Tile_Palette'Class ) is
        tool   : constant A_Tool := this.Get_Tinker_View.Get_Selected_Tool;
        tileId : Natural := 0;
    begin
        if tool.Get_Type = Painter_Tool then
            this.selectedItem := 0;
            this.Hide_Info_Panel;
            tileId := A_Painter(tool).Get_Tile_Id;
            if tileId > 0 then
                for i in 1..this.Item_Count loop
                    if tileId = this.tileIds.Element( i ) then
                        this.selectedItem := i;
                        this.Show_Info_Panel;
                        exit;
                    end if;
                end loop;
            end if;
        end if;
    end On_Tool_Selected;

    ----------------------------------------------------------------------------

    procedure Set_Library_Name( this : not null access Tile_Palette'Class;
                                name : String ) is
        lib : Library_Ptr;

        ------------------------------------------------------------------------

        procedure Examine( slot : Positive; tile : A_Tile ) is
        begin
            this.items.Append( Create_Icon( lib, tile.Get_Id, required => False ) );
            this.tileIds.Append( tile.Get_Id );
            pragma Assert( this.Item_Count = slot, "Item count mismatch" );
        end Examine;

        ------------------------------------------------------------------------

    begin
        this.Clear;
        this.tileIds.Clear;
        this.Hide_Info_Panel;

        lib := Load_Library( name, bitmaps => True );
        if lib.Get.Is_Loaded then
            -- load the tile ids for the tile slots
            lib.Get.Iterate_By_Slot( Examine'Unrestricted_Access );

            this.Scroll_To( 0.0, 0.0 );
        end if;
    end Set_Library_Name;

    ----------------------------------------------------------------------------

    procedure Show_Info_Panel( this : not null access Tile_Palette'Class ) is
    begin
        pragma Assert( this.infoPanel = null );

        if this.selectedItem > 0 then
            this.infoPanel := A_Widget(Create_Tile_Module( this.Get_Tinker_View,
                                                           Create_Icon( this.items.Element( this.selectedItem ).Get_Library,
                                                                        this.tileIds.Element( this.selectedItem ) ) ));
            this.Get_Tinker_View.Show_Info_Panel( this.infoPanel );
        end if;
    end Show_Info_Panel;

end Widgets.Palettes.Tiles;
