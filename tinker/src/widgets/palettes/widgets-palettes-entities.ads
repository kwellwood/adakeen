--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

private with Ada.Containers.Indefinite_Vectors;

package Widgets.Palettes.Entities is

    type Entity_Palette is new Palette with private;
    type A_Entity_Palette is access all Entity_Palette'Class;

    -- Creates a new Entity_Palette. 'cursor' is the name of the mouse cursor
    -- used by all the Spawner tools in this palette.
    function Create_Entity_Palette( view : not null access Game_Views.Game_View'Class;
                                    id   : String := "" ) return A_Entity_Palette;
    pragma Postcondition( Create_Entity_Palette'Result /= null );

    -- Adds an entity to the palette.
    procedure Add_Entity( this     : not null access Entity_Palette'Class;
                          template : String;
                          icon     : Icon_Type );

private

    type Tool_Slot is
        record
            template : Unbounded_String;
            icon     : Icon_Type;
        end record;

    package Slot_Vectors is new Ada.Containers.Indefinite_Vectors( Positive, Tool_Slot, "=" );

    type Entity_Palette is new Palette with
        record
            slots : Slot_Vectors.Vector;
        end record;

    procedure Select_Item( this : access Entity_Palette; index : Natural );

end Widgets.Palettes.Entities;
