--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers;                    use Ada.Containers;
with Bin_Packing;                       use Bin_Packing;
with Clipping.Drawing;                  use Clipping.Drawing;
--with Debugging;                         use Debugging;
with Events.World;                      use Events.World;
with Game_Views;                        use Game_Views;
with Game_Views.Tinker;                 use Game_Views.Tinker;
with Palette.Tango;                     use Palette.Tango;
with Tiles;                             use Tiles;
with Tools;                             use Tools;
with Tools.Patterns;                    use Tools.Patterns;
with Tools.Stampers;                    use Tools.Stampers;
with Values.Casting;                    use Values.Casting;

package body Widgets.Palettes.Patterns is

    package Connections is new Signals.Connections(Pattern_Palette);
    use Connections;

    ----------------------------------------------------------------------------

    procedure To_Row_And_Col( this  : not null access Pattern_Palette'Class;
                              index : Natural;
                              row,
                              col   : out Natural );

    function Get_Tinker_View( this : not null access Pattern_Palette'Class ) return A_Tinker_View is (A_Tinker_View(this.Get_View));

    procedure On_Resized( this : not null access Pattern_Palette'Class );

    procedure Organize_Patterns( this : not null access Pattern_Palette'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Pattern_Palette( view : not null access Game_Views.Game_View'Class;
                                     id   : String := "" ) return A_Palette is
        this : A_Palette := new Pattern_Palette;
    begin
        this.Construct( view, id );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Pattern_Palette;

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access Pattern_Palette;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String ) is
    begin
        Palette(this.all).Construct( view, id );
        this.Resized.Connect( Slot( this, On_Resized'Access ) );
        this.Listen_For_Event( EVT_WORLD_LOADED );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Pattern_Palette ) is
    begin
        this.patterns.Clear;
        Palette(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Decoration( this : access Pattern_Palette; index : Positive ) is
    begin
        Clipping.Drawing.Draw( this.items.Element( index ).Get_Tile.Get_Shape, 0.0, 0.0, 0.0, Integer(this.Get_Icon_Size), Butter1, Scarlet1 );
    end Draw_Decoration;

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Event( this : access Pattern_Palette;
                            evt  : in out A_Event;
                            resp : out Response_Type ) is
        pragma Unreferenced( resp );
    begin
        if evt.Get_Event_Id = EVT_WORLD_LOADED then
            this.Set_Library_Name( A_World_Loaded_Event(evt).Get_Library_Name );
        end if;
    end Handle_Event;

    ----------------------------------------------------------------------------

    procedure On_Resized( this : not null access Pattern_Palette'Class ) is
        newColumns : constant Integer := this.Get_Columns;
    begin
        if newColumns /= this.actualCols then
            -- the number of columns changed when resized; re-layout the patterns
            this.Organize_Patterns;
        end if;
        this.actualCols := newColumns;
    end On_Resized;

    ----------------------------------------------------------------------------

    procedure Organize_Patterns( this : not null access Pattern_Palette'Class ) is

        ------------------------------------------------------------------------

        -- Returns the most foreground tile of pattern 'pattern' at column and
        -- row 'col,row'. This is the tile that will be displayed in the palette.
        function Get_Foreground_Tile( pattern : not null A_Map; col, row : Integer ) return Natural is
        begin
            for layer in pattern.Get_Foreground_Layer..pattern.Get_Background_Layer loop
                if pattern.Get_Tile_Id( layer, col, row ) /= 0 then
                    return pattern.Get_Tile_Id( layer, col, row );
                end if;
            end loop;
            return 0;
        end Get_Foreground_Tile;

        ------------------------------------------------------------------------

        columns   : constant Natural := this.Get_Columns;
        bin       : A_Bin;
        itemCount : Natural := 0;
        mrec      : Pattern_Rec;
        area      : Rect;
    begin
        if this.lib.Is_Null or else not this.lib.Get.Is_Loaded then
            return;
        end if;

        this.patterns.Clear;
        this.items.Clear;

        bin := Create_Bin( this.Get_Columns, 1000 );
        for i in 1..this.lib.Get.Get_Matrix_Count loop
            mrec.pattern := this.lib.Get.Get_Matrix( i );

            -- filter out matrices according to .matrixType
            if Length( this.matrixType ) = 0 or else Cast_String( mrec.pattern.Get_Layer_Property( 0, "type" ) ) = this.matrixType then
                area := bin.Insert( mrec.pattern.Get_Width, mrec.pattern.Get_Height );
                mrec.col := area.x + 1;
                mrec.row := area.y + 1;

                -- if the item index of the bottom right of this pattern is greater
                -- than the item count, then increase the count to include it.
                if ((mrec.row + mrec.pattern.Get_Height - 1) - 1) * columns + (mrec.col + mrec.pattern.Get_Width - 1) > itemCount then
                    itemCount := ((mrec.row + mrec.pattern.Get_Height - 1) - 1) * columns + (mrec.col + mrec.pattern.Get_Width - 1);
                end if;

                this.patterns.Append( mrec );
            end if;
        end loop;
        Delete( bin );

        -- pre-populate the items vector so we can replace them one at a time
        this.items.Append( NO_ICON, Count_Type(itemCount) );

        -- add all icons from each pattern to the prepopulated list
        for mrec of this.patterns loop
            for row in 0..(mrec.pattern.Get_Height - 1) loop
                for col in 0..(mrec.pattern.Get_Width - 1) loop
                    this.items.Replace_Element( (mrec.row - 1 + row) * columns + (mrec.col + col),
                                                Create_Icon( this.lib,
                                                             Get_Foreground_Tile( mrec.pattern, col, row ),
                                                             required => False ) );
                end loop;
            end loop;
        end loop;
    end Organize_Patterns;

    ----------------------------------------------------------------------------

    overriding
    procedure Select_Item( this : access Pattern_Palette; index : Natural ) is
        row,
        col  : Natural;
        mrec : Pattern_Rec;
    begin
        if index = 0 then
            return;
        end if;

        this.To_Row_And_Col( index, row, col );

        for m in 1..this.patterns.Length loop
            mrec := this.patterns.Element( Integer(m) );
            if row >= mrec.row and then row < mrec.row + mrec.pattern.Get_Height and then
                col >= mrec.col and then col < mrec.col + mrec.pattern.Get_Width
            then
                this.selectedItem := index;
                if Cast_String( mrec.pattern.Get_Layer_Property( 0, "type" ) ) = "pattern" then
                    this.Get_Tinker_View.Replace_Tool( Create_Patternbrush( mrec.pattern, col - mrec.col, row - mrec.row ) );
                    this.Get_Tinker_View.Select_Tool( Pattern_Tool );
                elsif Cast_String( mrec.pattern.Get_Layer_Property( 0, "type" ) ) = "stamp" then
                    this.Get_Tinker_View.Replace_Tool( Create_Stamper( mrec.pattern ) );
                    this.Get_Tinker_View.Select_Tool( Stamp_Tool );
                end if;
                exit;
            end if;
        end loop;
    end Select_Item;

    ----------------------------------------------------------------------------

    procedure Set_Library_Name( this : not null access Pattern_Palette'Class;
                                name : String ) is
    begin
        this.Clear;
        this.patterns.Clear;

        this.lib := Load_Library( name, bitmaps => True );
        if this.lib.Get.Is_Loaded then
            this.Organize_Patterns;
            this.Scroll_To( 0.0, 0.0 );
        end if;
    end Set_Library_Name;

    ----------------------------------------------------------------------------

    procedure Show_Type( this       : not null access Pattern_Palette'Class;
                         matrixType : String ) is
    begin
        this.matrixType := To_Unbounded_String( matrixType );
        this.Organize_Patterns;
    end Show_Type;

    ----------------------------------------------------------------------------

    procedure To_Row_And_Col( this  : not null access Pattern_Palette'Class;
                              index : Natural;
                              row,
                              col   : out Natural ) is
        columns : constant Natural := this.Get_Columns;
    begin
        row := 1 + Integer'Max( 0, index - 1 ) / columns;
        col := index - (row - 1) * columns;
    end To_Row_And_Col;

end Widgets.Palettes.Patterns;
