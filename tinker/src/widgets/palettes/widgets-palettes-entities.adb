--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Game_Views;                        use Game_Views;
with Game_Views.Tinker;                 use Game_Views.Tinker;
with Tools;                             use Tools;
with Tools.Spawners;                    use Tools.Spawners;

package body Widgets.Palettes.Entities is

    function Get_Tinker_View( this : not null access Entity_Palette'Class ) return A_Tinker_View is (A_Tinker_View(this.Get_View));

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Entity_Palette( view : not null access Game_Views.Game_View'Class;
                                    id   : String := "" ) return A_Entity_Palette is
        this : A_Entity_Palette := new Entity_Palette;
    begin
        this.Construct( view, id );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Entity_Palette;

    ----------------------------------------------------------------------------

    procedure Add_Entity( this     : not null access Entity_Palette'Class;
                          template : String;
                          icon     : Icon_Type ) is
        slot : Tool_Slot;
    begin
        slot.template := To_Unbounded_String( template );
        slot.icon := icon;
        this.slots.Append( slot );
        this.items.Append( slot.icon );
    end Add_Entity;

    ----------------------------------------------------------------------------

    overriding
    procedure Select_Item( this : access Entity_Palette; index : Natural ) is
    begin
        this.selectedItem := index;
        this.Get_Tinker_View.Replace_Tool( Create_Spawner( To_String( this.slots.Element( this.selectedItem ).template ),
                                                           this.slots.Element( this.selectedItem ).icon ) );
        this.Get_Tinker_View.Select_Tool( Spawner_Tool );
    end Select_Item;

end Widgets.Palettes.Entities;
