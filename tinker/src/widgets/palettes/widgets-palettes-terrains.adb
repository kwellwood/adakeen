--
-- Copyright (c) 2017-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Assets.Libraries;                  use Assets.Libraries;
--with Debugging;                         use Debugging;
with Events.World;                      use Events.World;
with Game_Views;                        use Game_Views;
with Game_Views.Tinker;                 use Game_Views.Tinker;
with Tools;                             use Tools;
with Tools.Terrains;                    use Tools.Terrains;

package body Widgets.Palettes.Terrains is

    package Connections is new Signals.Connections(Terrain_Palette);
    use Connections;

    ----------------------------------------------------------------------------

    function Get_Tinker_View( this : not null access Terrain_Palette'Class ) return A_Tinker_View is (A_Tinker_View(this.Get_View));

    procedure On_Tool_Selected( this : not null access Terrain_Palette'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Terrain_Palette( view : not null access Game_Views.Game_View'Class;
                                     id   : String := "" ) return A_Palette is
        this : A_Terrain_Palette := new Terrain_Palette;
    begin
        this.Construct( view, id );
        return A_Palette(this);
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Terrain_Palette;

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access Terrain_Palette;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String ) is
    begin
        Palette(this.all).Construct( view, id );
        this.Get_Tinker_View.ToolChanged.Connect( Slot( this, On_Tool_Selected'Access ) );
        this.Listen_For_Event( EVT_WORLD_LOADED );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Event( this : access Terrain_Palette;
                            evt  : in out A_Event;
                            resp : out Response_Type ) is
        pragma Unreferenced( resp );
    begin
        if evt.Get_Event_Id = EVT_WORLD_LOADED then
            this.Set_Library_Name( A_World_Loaded_Event(evt).Get_Library_Name );
        end if;
    end Handle_Event;

    ----------------------------------------------------------------------------

    overriding
    procedure Select_Item( this : access Terrain_Palette; index : Natural ) is
    begin
        this.selectedItem := index;
        this.Get_Tinker_View.Replace_Tool( Create_Terrain_Brush( this.items.Element( this.selectedItem ).Get_Library, index ) );
        this.Get_Tinker_View.Select_Tool( Terrain_Tool );
    end Select_Item;

    ----------------------------------------------------------------------------

    procedure On_Tool_Selected( this : not null access Terrain_Palette'Class ) is
        tool : constant A_Tool := this.Get_Tinker_View.Get_Selected_Tool;
    begin
        if tool.Get_Type = Terrain_Tool then
            this.selectedItem := A_Terrain_Brush(tool).Get_Terrain_Index;
        end if;
    end On_Tool_Selected;

    ----------------------------------------------------------------------------

    procedure Set_Library_Name( this : not null access Terrain_Palette'Class;
                                name : String ) is
        lib : Library_Ptr;
    begin
        this.Clear;

        lib := Load_Library( name, bitmaps => True );
        if lib.Get.Is_Loaded then
            for i in 1..lib.Get.Get_Terrain_Count loop
                this.items.Append( Create_Icon( lib, lib.Get.Get_Terrain( i ).icon, required => False ) );
            end loop;

            this.Scroll_To( 0.0, 0.0 );
        end if;
    end Set_Library_Name;

end Widgets.Palettes.Terrains;
