--
-- Copyright (c) 2012-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Vectors;
with Icons;                             use Icons;
with Scrollables;                       use Scrollables;

package Widgets.Palettes is

    type Palette is abstract new Widget and Animated and Scrollable with private;
    type A_Palette is access all Palette'Class;

    -- Enables and disables animated items in the palette. Default is disabled.
    procedure Enable_Animation( this : not null access Palette'Class; enable : Boolean );

    -- Sets the number of columns of items to display, or zero to fill the
    -- available width.
    procedure Set_Columns( this    : not null access Palette'Class;
                           columns : Natural );

    -- Sets the maximum width/height of icons in the palette. Items will be
    -- drawn to fit in this square area while maintaining their original
    -- perspective. If 'size' <= 0, the icons will expand to fill the palette's
    -- width when a fixed number of columns is used.
    procedure Set_Icon_Size( this : not null access Palette'Class; size : Float );

private

    BACKGROUND_ELEM : constant := 1;
    ITEM_ELEM       : constant := 2;
    BORDER_ELEM     : constant := 3;

    DISABLED_STATE  : constant Widget_State := 2**0;
    SELECTED_STATE  : constant Widget_State := 2**1;
    HOVER_STATE     : constant Widget_State := 2**2;

    package Icon_Vectors is new Ada.Containers.Vectors( Positive, Icon_Type, "=" );
    use Icon_Vectors;

    ----------------------------------------------------------------------------

    type Palette is abstract new Widget and Animated and Scrollable with
        record
            items        : Icon_Vectors.Vector;
            columns      : Natural := 0;
            iconSize     : Float := 0.0;
            animated     : Boolean := False;
            hoverItem    : Integer := 0;
            selectedItem : Integer := 0;
        end record;

    procedure Clear( this : not null access Palette'Class );

    procedure Construct( this : access Palette;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );

    procedure Draw_Content( this : access Palette );

    not overriding
    procedure Draw_Decoration( this : access Palette; index : Positive ) is null;

    function Get_Columns( this : not null access Palette'Class ) return Positive;

    function Get_Icon_Size( this : not null access Palette'Class ) return Float;

    function Get_Min_Height( this : access Palette ) return Float;

    function Get_Min_Width( this : access Palette ) return Float;

    function Get_Scroll_Inc_X( this : access Palette ) return Float;

    function Get_Scroll_Inc_Y( this : access Palette ) return Float;

    function Is_Selectable( this : not null access Palette'Class; item : Natural ) return Boolean;

    function Item_Count( this : not null access Palette'Class ) return Natural is (Integer(this.items.Length));

    -- Handles when the mouse is clicked on the item at index 'index' (base 1).
    -- This is not called unless Is_Selectable() returns True.
    procedure Select_Item( this : access Palette; item : Natural ) is null;

    procedure Tick( this : access Palette; time : Tick_Time );

end Widgets.Palettes;
