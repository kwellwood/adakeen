--
-- Copyright (c) 2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Widgets.Palettes.Terrains is

    type Terrain_Palette is abstract new Palette with private;

    -- Creates a new palette for choosing terrains. Use Set_Library_Name to set
    -- the tile library to use.
    function Create_Terrain_Palette( view : not null access Game_Views.Game_View'Class;
                                     id   : String := "" ) return A_Palette;

private

    type Terrain_Palette is new Palette with null record;
    type A_Terrain_Palette is access all Terrain_Palette'Class;

    procedure Construct( this : access Terrain_Palette;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );

    procedure Handle_Event( this : access Terrain_Palette;
                            evt  : in out A_Event;
                            resp : out Response_Type );

    procedure Select_Item( this : access Terrain_Palette; index : Natural );

    -- Clears the palette's items and refreshes the list from the terrain
    -- definitions in tile library 'name'.
    procedure Set_Library_Name( this : not null access Terrain_Palette'Class;
                                name : String );

end Widgets.Palettes.Terrains;
