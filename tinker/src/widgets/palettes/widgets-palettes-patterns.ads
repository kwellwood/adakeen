--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Vectors;
with Assets.Libraries;                  use Assets.Libraries;
with Events;
with Maps;                              use Maps;

package Widgets.Palettes.Patterns is

    type Pattern_Palette is abstract new Palette with private;
    type A_Pattern_Palette is access all Pattern_Palette'Class;

    function Create_Pattern_Palette( view : not null access Game_Views.Game_View'Class;
                                     id   : String := "" ) return A_Palette;

    -- Filters the matrices in the world's tile library to only show matrices of
    -- type 'matrixType'. If an empty string is given, all matrices will be
    -- shown. This takes effect immediately.
    procedure Show_Type( this       : not null access Pattern_Palette'Class;
                         matrixType : String );

private

    type Pattern_Rec is
        record
            pattern : A_Map := null;
            row,
            col     : Natural := 0;
        end record;

    overriding
    function "="( l, r : Pattern_Rec ) return Boolean is (l.pattern = r.pattern);

    package Pattern_Vectors is new Ada.Containers.Vectors( Positive, Pattern_Rec, "=" );

    ----------------------------------------------------------------------------

    type Pattern_Palette is new Palette with
        record
            lib        : Library_Ptr;
            matrixType : Unbounded_String;
            patterns   : Pattern_Vectors.Vector;
            actualCols : Integer := 0;
        end record;

    procedure Construct( this : access Pattern_Palette;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );

    procedure Delete( this : in out Pattern_Palette );

    procedure Draw_Decoration( this : access Pattern_Palette; index : Positive );

    procedure Handle_Event( this : access Pattern_Palette;
                            evt  : in out A_Event;
                            resp : out Response_Type );

    procedure Select_Item( this : access Pattern_Palette; index : Natural );

    -- Clears the palette's items and refreshes the list from the patterns in
    -- tile library 'name'.
    procedure Set_Library_Name( this : not null access Pattern_Palette'Class;
                                name : String );

end Widgets.Palettes.Patterns;
