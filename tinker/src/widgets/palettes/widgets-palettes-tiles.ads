--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Widgets.Palettes.Tiles is

    type Tile_Palette is abstract new Palette with private;

    -- Creates a new palette for displaying tiles. Use Set_Library_Name to set
    -- the tile library to display.
    function Create_Tile_Palette( view : not null access Game_Views.Game_View'Class;
                                  id   : String := "" ) return A_Palette;

private

    package Int_Vectors is new Ada.Containers.Vectors( Positive, Natural, "=" );
    use Int_Vectors;

    type Tile_Palette is new Palette and Animated with
        record
            tileIds   : Int_Vectors.Vector;
            infoPanel : A_Widget := null;
        end record;
    type A_Tile_Palette is access all Tile_Palette'Class;

    procedure Construct( this : access Tile_Palette;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );

    procedure Delete( this : in out Tile_Palette );

    procedure Draw_Decoration( this : access Tile_Palette; index : Positive );

    procedure Handle_Event( this : access Tile_Palette;
                            evt  : in out A_Event;
                            resp : out Response_Type );

    -- Hides and destroys the currently displayed tile info panel, if any.
    procedure Hide_Info_Panel( this : not null access Tile_Palette'Class );

    procedure Select_Item( this : access Tile_Palette; index : Natural );

    -- Clears the palette's items and refreshes the list from the slot list in
    -- tile library 'name'.
    procedure Set_Library_Name( this : not null access Tile_Palette'Class;
                                name : String );

    -- Shows tile properties for the currently selected tile in the info panel.
    procedure Show_Info_Panel( this : not null access Tile_Palette'Class );

end Widgets.Palettes.Tiles;
