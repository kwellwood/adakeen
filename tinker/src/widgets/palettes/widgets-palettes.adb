--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.State;                     use Allegro.State;
with Allegro.Transformations;           use Allegro.Transformations;
--with Debugging;                         use Debugging;
with Drawing;                           use Drawing;
with Drawing.Bitmaps;                   use Drawing.Bitmaps;
with Drawing.Primitives;                use Drawing.Primitives;
with Drawing.Tiles;                     use Drawing.Tiles;
with Styles;                            use Styles;
with Widget_Styles.Registry;            use Widget_Styles.Registry;

package body Widgets.Palettes is

    package Connections is new Signals.Connections(Palette);
    use Connections;

    package Mouse_Connections is new Signals.Mouse.Connections(Palette);
    use Mouse_Connections;

    ----------------------------------------------------------------------------

    -- Divides 'num' by 'denom', rounding up. This works for non-negative
    -- integers only.
    function Div_Ceil( num, denom : Integer ) return Integer is ((num - 1) / denom + 1);

    procedure On_Mouse_Exited( this : not null access Palette'Class );

    procedure On_Mouse_Moved( this  : not null access Palette'Class;
                              mouse : Mouse_Arguments );

    procedure On_Mouse_Pressed( this  : not null access Palette'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    procedure Clear( this : not null access Palette'Class ) is
    begin
        this.items.Clear;
        this.selectedItem := 0;
        this.hoverItem := 0;
    end Clear;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Palette;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String ) is
    begin
        Widget(this.all).Construct( view, id, "Palette" );
        this.MouseExited.Connect( Slot( this, On_Mouse_Exited'Access ) );
        this.MouseMoved.Connect( Slot( this, On_Mouse_Moved'Access ) );
        this.MousePressed.Connect( Slot( this, On_Mouse_Pressed'Access, Mouse_Left ) );
        this.Set_Focusable( False );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Content( this : access Palette ) is
        padLeft      : constant Float := this.style.Get_Pad_Left( BACKGROUND_ELEM );
        padRight     : constant Float := this.style.Get_Pad_Right( BACKGROUND_ELEM );
        padTop       : constant Float := this.style.Get_Pad_Top( BACKGROUND_ELEM );
        spacing      : constant Float := this.style.Get_Spacing( BACKGROUND_ELEM );
        columns      : constant Integer := this.Get_Columns;
        iconSize     : constant Float := this.Get_Icon_Size;
        firstRow     : constant Integer := Integer'Max( Integer(Float'Floor( (this.viewport.y - padTop) / (iconSize + spacing) )) + 1, 1 );
        lastRow      : constant Integer := Integer'Max( Integer(Float'Floor( ((this.viewport.y - padTop) + this.viewport.height) / (iconSize + spacing) )) + 1, 1 );
        contentWidth : Float;
        state        : Widget_State;
        leftX        : Float;
        drawX,
        drawY        : Float;
        index        : Integer := 0;
        bmp          : A_Allegro_Bitmap;
        alState      : Allegro_State;
        contentTrans : Allegro_Transform;
        itemTrans    : Allegro_Transform;
    begin
        contentWidth := Float(columns) * (iconSize + spacing) - spacing;

        case this.style.Get_Align( BACKGROUND_ELEM ) is
            when Top_Left | Left | Bottom_Left =>
                leftX := padLeft;

            when Top_Center | Center | Bottom_Center =>
                leftX := padLeft + ((this.viewport.width - (padLeft + padRight)) - contentWidth) / 2.0;

            when Top_Right | Right | Bottom_Right =>
                leftX := this.viewport.width - padRight - contentWidth;

        end case;

        -- draw all the tiles first so bitmap drawing can be deferred
        index := (firstRow - 1) * columns;
        Al_Hold_Bitmap_Drawing( True );
        for row in firstRow..lastRow loop
            drawY := Float'Rounding( padTop + Float(row - 1) * (iconSize + spacing) );
            for col in 1..columns loop
                index := index + 1;
                exit when index > this.Item_Count;
                drawX := Float'Rounding( leftX + Float(col - 1) * (iconSize + spacing) );

                bmp := this.items.Element( index ).Get_Tile.Get_Bitmap;
                if bmp /= null then
                    Draw_Bitmap_Stretched( bmp,
                                           drawX, drawY, 0.0,
                                           iconSize, iconSize,
                                           Fit );
                end if;
            end loop;
            exit when index > this.Item_Count;
        end loop;
        Al_Hold_Bitmap_Drawing( False );

        -- draw decorations by subclass
        Al_Store_State( alState, ALLEGRO_STATE_TRANSFORM );
        Al_Copy_Transform( contentTrans, Al_Get_Current_Transform.all );
        index := (firstRow - 1) * columns;
        for row in firstRow..lastRow loop
            drawY := Float'Rounding( padTop + Float(row - 1) * (iconSize + spacing) );
            for col in 1..columns loop
                index := index + 1;
                exit when index > this.Item_Count;
                drawX := Float'Rounding( leftX + Float(col - 1) * (iconSize + spacing) );

                Al_Identity_Transform( itemTrans );
                Al_Translate_Transform( itemTrans, drawX, drawY );
                Al_Compose_Transform( itemTrans, contentTrans );
                Al_Use_Transform( itemTrans );

                if this.items.Element( index ).Get_Bitmap /= null then
                    A_Palette(this).Draw_Decoration( index );
                end if;
            end loop;
            exit when index > this.Item_Count;
        end loop;
        Al_Restore_State( alState );

        -- draw styles decorations on the tiles
        index := (firstRow - 1) * columns;
        for row in firstRow..lastRow loop
            drawY := Float'Rounding( padTop + Float(row - 1) * (iconSize + spacing) );
            for col in 1..columns loop
                index := index + 1;
                exit when index > this.Item_Count;
                drawX := Float'Rounding( leftX + Float(col - 1) * (iconSize + spacing) );

                state := DEFAULT_STATE
                         or (if index = this.hoverItem    then HOVER_STATE    else 0)
                         or (if index = this.selectedItem then SELECTED_STATE else 0);

                -- item's color overlay
                Pixel_Rectfill( drawX, drawY,
                                drawX + iconSize - 1.0,
                                drawY + iconSize - 1.0,
                                Lighten( this.style.Get_Color( ITEM_ELEM, state ),
                                         this.style.Get_Shade( ITEM_ELEM, state ) ) );
                -- item's border
                Draw_Tile_Filled( this.style.Get_Image( BORDER_ELEM, state ).Get_Tile,
                                  drawX, drawY, 0.0,
                                  iconSize, iconSize,
                                  Lighten( this.style.Get_Tint( BORDER_ELEM, state ),
                                           this.style.Get_Shade( BORDER_ELEM, state ) ) );
            end loop;
            exit when index > this.Item_Count;
        end loop;
    end Draw_Content;

    ----------------------------------------------------------------------------

    procedure Enable_Animation( this : not null access Palette'Class; enable : Boolean ) is
    begin
        this.animated := enable;
    end Enable_Animation;

    ----------------------------------------------------------------------------

    function Get_Columns( this : not null access Palette'Class ) return Positive is
        padLeft        : Float;
        padRight       : Float;
        availableWidth : Float;
        columnWidth    : Float;
    begin
        if this.columns > 0 then
            return this.columns;
        end if;

        padLeft := this.style.Get_Pad_Left( BACKGROUND_ELEM );
        padRight := this.style.Get_Pad_Right( BACKGROUND_ELEM );
        availableWidth := this.viewport.width - (padLeft + padRight);
        columnWidth := this.Get_Icon_Size + this.style.Get_Spacing( BACKGROUND_ELEM );

        return Integer'Max( 1, Integer(Float'Floor( availableWidth / columnWidth )) );
    end Get_Columns;

    ----------------------------------------------------------------------------

    function Get_Icon_Size( this : not null access Palette'Class ) return Float is
        padLeft        : Float;
        padRight       : Float;
        availableWidth : Float;
        spacing        : Float;
    begin
        if this.iconSize > 0.0 or this.columns = 0 then
            return this.iconSize;
        end if;

        padLeft := this.style.Get_Pad_Left( BACKGROUND_ELEM );
        padRight := this.style.Get_Pad_Right( BACKGROUND_ELEM );
        availableWidth := this.viewport.width - (padLeft + padRight);

        if this.columns = 1 then
            return Float'Max( 1.0, availableWidth );
        end if;

        spacing := (Float(this.columns - 1) * this.style.Get_Spacing( BACKGROUND_ELEM ));

        return Float'Max( 1.0, Float'Floor( (availableWidth - spacing) / Float(this.columns) ) );
    end Get_Icon_Size;

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Height( this : access Palette ) return Float is
        spacing : constant Float := this.style.Get_Spacing( BACKGROUND_ELEM );
    begin
        return this.style.Get_Pad_Top( BACKGROUND_ELEM ) +
               (Float(Div_Ceil( this.Item_Count, this.Get_Columns )) * (this.Get_Icon_Size + spacing) - spacing) +
               this.style.Get_Pad_Bottom( BACKGROUND_ELEM );
    end Get_Min_Height;

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Width( this : access Palette ) return Float is
        spacing : constant Float := this.style.Get_Spacing( BACKGROUND_ELEM );
    begin
        return this.style.Get_Pad_Left( BACKGROUND_ELEM ) +
               (Float(Integer'Max( 1, this.columns )) * (this.Get_Icon_Size + spacing) - spacing) +
               this.style.Get_Pad_Right( BACKGROUND_ELEM );
    end Get_Min_Width;

    ----------------------------------------------------------------------------

    overriding
    function Get_Scroll_Inc_X( this : access Palette ) return Float is (8.0);

    ----------------------------------------------------------------------------

    overriding
    function Get_Scroll_Inc_Y( this : access Palette ) return Float
    is (this.Get_Icon_Size + this.style.Get_Spacing( BACKGROUND_ELEM ));

    ----------------------------------------------------------------------------

    function Is_Selectable( this : not null access Palette'Class; item : Natural ) return Boolean
    is ((1 <= item and item <= this.Item_Count) and then this.items.Element( item ).Get_Bitmap /= null);

    ----------------------------------------------------------------------------

    procedure On_Mouse_Exited( this : not null access Palette'Class ) is
    begin
        this.hoverItem := 0;
    end On_Mouse_Exited;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Moved( this  : not null access Palette'Class;
                              mouse : Mouse_Arguments ) is
        spacing      : constant Float := this.style.Get_Spacing( BACKGROUND_ELEM );
        iconSize     : constant Float := this.Get_Icon_Size;
        padLeft      : constant Float := this.style.Get_Pad_Left( BACKGROUND_ELEM );
        padRight     : constant Float := this.style.Get_Pad_Right( BACKGROUND_ELEM );
        padTop       : constant Float := this.style.Get_Pad_Top( BACKGROUND_ELEM );
        padBottom    : constant Float := this.style.Get_Pad_Bottom( BACKGROUND_ELEM );
        columns      : constant Integer := this.Get_Columns;
        contentWidth : Float;
        leftX        : Float;
        row, col     : Integer;
        index        : Integer;
    begin
        this.hoverItem := 0;

        contentWidth := Float(columns) * (iconSize + spacing) - spacing;

        case this.style.Get_Align( BACKGROUND_ELEM ) is
            when Top_Left | Left | Bottom_Left =>
                leftX := padLeft;

            when Top_Center | Center | Bottom_Center =>
                leftX := padLeft + ((this.viewport.width - (padLeft + padRight)) - contentWidth) / 2.0;

            when Top_Right | Right | Bottom_Right =>
                leftX := this.viewport.width - padRight - contentWidth;

        end case;

        if leftX <= mouse.x and then mouse.x < leftX + contentWidth and then
           padTop <= mouse.y and then mouse.y < this.Get_Preferred_Height - padBottom
        then
            row := Integer(Float'Floor( (mouse.y - padTop) / (iconSize + spacing) ));
            col := Integer(Float'Floor( (mouse.x - leftX) / (iconSize + spacing) ));
            index := 1 + row * this.Get_Columns + col;

            if A_Palette(this).Is_Selectable( index ) then
                this.hoverItem := index;
            end if;
        end if;
    end On_Mouse_Moved;

    ----------------------------------------------------------------------------

    procedure On_Mouse_Pressed( this  : not null access Palette'Class ) is
    begin
        if this.hoverItem > 0 and then this.Is_Selectable( this.hoverItem ) then
            this.Select_Item( this.hoverItem );
        end if;
    end On_Mouse_Pressed;

    ----------------------------------------------------------------------------

    procedure Set_Columns( this    : not null access Palette'Class;
                           columns : Natural ) is
    begin
        if columns /= this.columns then
            this.columns := columns;
            this.Update_Geometry;                -- update because size hint is affected
        end if;
    end Set_Columns;

    ----------------------------------------------------------------------------

    procedure Set_Icon_Size( this : not null access Palette'Class; size : Float ) is
    begin
        if size /= this.iconSize then
            this.iconSize := Float'Max( 0.0, size );
            this.Update_Geometry;                -- update because size hint is affected
        end if;
    end Set_Icon_Size;

    ----------------------------------------------------------------------------

    overriding
    procedure Tick( this : access Palette; time : Tick_Time ) is
    begin
        if this.animated then
            for item of this.items loop
                item.Update_Animation( time.total );
            end loop;
        end if;
    end Tick;

begin

    Register_Style( "Palette",
                    (BACKGROUND_ELEM => To_Unbounded_String( "background" ),
                     ITEM_ELEM       => To_Unbounded_String( "item" ),
                     BORDER_ELEM     => To_Unbounded_String( "border" )),
                    (BACKGROUND_ELEM => Area_Element,
                     ITEM_ELEM       => Area_Element,
                     BORDER_ELEM     => Area_Element),
                    (0 => To_Unbounded_String( "disabled" ),
                     1 => To_Unbounded_String( "selected" ),
                     2 => To_Unbounded_String( "hover" )) );

end Widgets.Palettes;

