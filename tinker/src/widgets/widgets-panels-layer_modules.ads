--
-- Copyright (c) 2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Vectors;
with Widgets.Buttons;                   use Widgets.Buttons;
with Widgets.Buttons.Groups;            use Widgets.Buttons.Groups;
with Widgets.Row_Layouts;               use Widgets.Row_Layouts;
with Widgets.Scenes.Tinker;             use Widgets.Scenes.Tinker;

limited with Game_Views.Tinker;

package Widgets.Panels.Layer_Modules is

    type Layer_Module is new Panel with private;
    type A_Layer_Module is access all Layer_Module'Class;

    function Create_Layer_Module( view  : not null access Game_Views.Tinker.Tinker_View'Class;
                                  scene : not null A_Tinker_Scene ) return A_Layer_Module;
    pragma Postcondition( Create_Layer_Module'Result /= null );

    procedure Select_Layer( this  : not null access Layer_Module'Class;
                            layer : Integer );

    procedure Show_Layer( this  : not null access Layer_Module'Class;
                          layer : Integer;
                          show  : Boolean );

private

    type Layer_Controls is
        record
            layer     : Integer := 0;
            selected  : A_Button;
            visible   : A_Button;
            infoPanel : A_Widget;       -- layer properties info panel
        end record;

    package Control_Vectors is new Ada.Containers.Vectors(Positive, Layer_Controls, "=");
    use Control_Vectors;

    ----------------------------------------------------------------------------

    type Layer_Module is new Panel with
        record
            scene           : A_Tinker_Scene;
            selectedGroup   : A_Button_Group;
            layers          : A_Column_Layout;
            controls        : Control_Vectors.Vector;
            options         : A_Row_Layout;
            foregroundLayer : Integer := 0;
            addButton       : A_Button;
            deleteButton    : A_Button;
            upButton        : A_Button;
            downButton      : A_Button;
        end record;

    procedure Construct( this  : access Layer_Module;
                         view  : not null access Game_Views.Game_View'Class;
                         scene : A_Tinker_Scene );

    procedure Delete( this : in out Layer_Module );

    function Get_Min_Height( this : access Layer_Module ) return Float;

    function Get_Preferred_Height( this : access Layer_Module ) return Float;

    procedure Handle_Event( this : access Layer_Module;
                            evt  : in out A_Event;
                            resp : out Response_Type );

end Widgets.Panels.Layer_Modules;
