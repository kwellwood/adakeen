--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Native_Dialogs;            use Allegro.Native_Dialogs;
with Events.World;                      use Events.World;
with Game_Views;                        use Game_Views;
with Game_Views.Tinker;                 use Game_Views.Tinker;
with GNAT.OS_Lib;                       use GNAT.OS_Lib;
with Preferences;                       use Preferences;
with Support;                           use Support;
with Support.Paths;                     use Support.Paths;
with Values.Construction;               use Values.Construction;
with Values.Casting;                    use Values.Casting;
with Values.Strings;                    use Values.Strings;
with Widgets.Buttons.Pushes;            use Widgets.Buttons.Pushes;
with Widgets.Row_Layouts;               use Widgets.Row_Layouts;
with Widgets.Input_Boxes.Constraints;   use Widgets.Input_Boxes.Constraints;
with Widgets.Labels;                    use Widgets.Labels;
with Widgets.Panels;                    use Widgets.Panels;

package body Widgets.Panels.Dialogs.New_World is

    package Key_Connections is new Signals.Keys.Connections(New_Dialog);
    use Key_Connections;

    package Connections is new Signals.Connections(New_Dialog);
    use Connections;

    ----------------------------------------------------------------------------

    procedure Clicked_Browse( this : not null access New_Dialog'Class );

    procedure Clicked_Cancel( this : not null access New_Dialog'Class );

    procedure Clicked_Ok( this : not null access New_Dialog'Class );

    procedure On_Hidden( this : not null access New_Dialog'Class );

    procedure On_Shown( this : not null access New_Dialog'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_New_Dialog( view : not null access Game_Views.Game_View'Class;
                                id   : String := "" ) return A_Dialog is
        this : A_New_Dialog := new New_Dialog;
    begin
        this.Construct( view, id );
        return A_Dialog(this);
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_New_Dialog;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access New_Dialog;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String ) is
        ROW_HEIGHT : constant := 24.0;
        leftCol    : A_Column_Layout;
        rightCol   : A_Column_Layout;
        row        : A_Row_Layout;
        label      : A_Label;
        cancel     : A_Button;
        button     : A_Button;
        panel      : A_Panel;
    begin
        Dialog(this.all).Construct( view, id, "New World...", Create_Icon( "tinker:document-block" ) );
        this.Hidden.Connect( Slot( this, On_Hidden'Access ) );
        this.Shown.Connect( Slot( this, On_Shown'Access ) );
        this.KeyPressed.Connect( Slot( this, Clicked_Cancel'Access, ALLEGRO_KEY_ESCAPE ) );

        this.Set_Style( "dialog" );
        this.Set_Preferred_Width( 258.0 );
        this.Set_Preferred_Height( 191.0 );

        leftCol := Create_Column_Layout( view );
        this.Add_Widget( leftCol );
        leftCol.Top.Attach( this.Top, 8.0 );
        leftCol.Left.Attach( this.Left, 8.0 );
        leftCol.Set_Style( "dialog" );
        leftCol.Width.Set( 60.0 );

            label := Create_Label( view );
            label.Height.Set( ROW_HEIGHT );
            label.Set_Style( "dialog" );
            label.Set_Text( "Width:" );
            leftCol.Append( label );

            label := Create_Label( view );
            label.Height.Set( ROW_HEIGHT );
            label.Set_Style( "dialog" );
            label.Set_Text( "Height:" );
            leftCol.Append( label );

            label := Create_Label( view );
            label.Height.Set( ROW_HEIGHT );
            label.Set_Style( "dialog" );
            label.Set_Text( "Library:" );
            leftCol.Append( label );

            label := Create_Label( view );
            label.Height.Set( ROW_HEIGHT );
            label.Set_Style( "dialog" );
            label.Set_Text( "Player:" );
            leftCol.Append( label );

        rightCol := Create_Column_Layout( view );
        this.Add_Widget( rightCol );
        rightCol.Top.Attach( leftCol.Top );
        rightCol.Left.Attach( leftCol.Right );
        rightCol.Set_Style( "dialog" );
        rightCol.Width.Set( 180.0 );

            row := Create_Row_Layout( view );
            row.Height.Set( ROW_HEIGHT );
            row.Set_Style( "dialog" );
            rightCol.Append( row );

                this.ibWidth := Create_Input_Box( view, this.id & ":ibWidth" );
                this.ibWidth.Height.Set( ROW_HEIGHT );
                this.ibWidth.Width.Set( 53.0 );
                this.ibWidth.Set_Style( "dialog" );
                this.ibWidth.Set_Constraint( Constraints.Int_Only'Access );
                this.ibWidth.Accepted.Connect( Slot( this, Clicked_Ok'Access ) );
                this.ibWidth.Set_Max_Length( 4 );
                this.ibWidth.Set_Prev( this.id & ":btnCancel" );
                this.ibWidth.Set_Next( this.id & ":ibHeight" );
                row.Append( this.ibWidth );

                label := Create_Label( view );
                label.Height.Set( ROW_HEIGHT );
                label.Set_Style( "dialog" );
                label.Set_Text( "Tiles" );
                row.Append( label );

            row := Create_Row_Layout( view );
            row.Height.Set( ROW_HEIGHT );
            row.Set_Style( "dialog" );
            rightCol.Append( row );

                this.ibHeight := Create_Input_Box( view, this.id & ":ibHeight" );
                this.ibHeight.Height.Set( ROW_HEIGHT );
                this.ibHeight.Width.Set( 53.0 );
                this.ibHeight.Set_Style( "dialog" );
                this.ibHeight.Set_Constraint( Constraints.Int_Only'Access );
                this.ibHeight.Accepted.Connect( Slot( this, Clicked_Ok'Access ) );
                this.ibHeight.Set_Max_Length( 4 );
                this.ibHeight.Set_Prev( this.id & ":ibWidth" );
                this.ibHeight.Set_Next( this.id & ":ibLib" );
                row.Append( this.ibHeight );

                label := Create_Label( view );
                label.Height.Set( ROW_HEIGHT );
                label.Set_Style( "dialog" );
                label.Set_Text( "Tiles" );
                row.Append( label );

            row := Create_Row_Layout( view );
            row.Height.Set( ROW_HEIGHT );
            row.Set_Style_Property( "background.spacing", Create( 4 ) );
            rightCol.Append( row );

                this.ibLib := Create_Input_Box( view, this.id & ":ibLib" );
                this.ibLib.Width.Set( 106.0 );
                this.ibLib.Set_Style( "dialog" );
                this.ibLib.Set_Constraint( Constraints.Basename_Only'Access );
                this.ibLib.Accepted.Connect( Slot( this, Clicked_Ok'Access ) );
                this.ibLib.Set_Prev( this.id & ":ibHeight" );
                this.ibLib.Set_Next( this.id & ":btnLib" );
                row.Append( this.ibLib );

                button := Create_Push_Button( view, this.id & ":btnLib" );
                button.Width.Set( 70.0 );
                button.Set_Style( "dialog" );
                button.Set_Text( "Browse..." );
                button.Clicked.Connect( Slot( this, Clicked_Browse'Access ) );
                button.Set_Prev( this.id & ":ibLib" );
                button.Set_Next( this.id & ":enuPlayer" );
                row.Append( button );

                this.enuPlayer := Create_Enum( view, this.id & ":enuPlayer" );
                this.enuPlayer.Height.Set( ROW_HEIGHT );
                this.enuPlayer.Width.Set( 180.0 );
                this.enuPlayer.Set_Style( "dialog" );
                this.enuPlayer.Add_Item( "Platform Keen", Create( "Keen" ) );
                this.enuPlayer.Add_Item( "Map Keen", Create( "LittleKeen" ) );
                this.enuPlayer.Set_Prev( this.id & ":btnLib" );
                this.enuPlayer.Set_Next( this.id & ":btnOk" );
                rightCol.Append( this.enuPlayer );

        panel := Create_Panel( view );
        this.Add_Widget( panel );
        panel.Set_Style( "dialogResponse" );
        panel.Height.Set( 1.0 + 4.0 + 28.0 + 4.0 );
        panel.Fill_Horizontal( this );
        panel.Bottom.Attach( this.Bottom );

            cancel := Create_Push_Button( view, this.id & ":btnCancel" );
            panel.Add_Widget( cancel );
            cancel.Set_Style( "dialogResponse" );
            cancel.Set_Text( "Cancel" );
            cancel.Bottom.Attach( panel.Bottom, 4.0 );
            cancel.Right.Attach( panel.Right, 4.0 );
            cancel.Width.Set( 80.0 );
            cancel.Height.Set( 28.0 );
            cancel.Set_Prev( this.id & ":btnOk" );
            cancel.Set_Next( this.id & ":ibWidth" );
            cancel.Clicked.Connect( Slot( this, Clicked_Cancel'Access ) );

            button := Create_Push_Button( view, this.id & ":btnOk" );
            panel.Add_Widget( button );
            button.Set_Style( "dialogResponse" );
            button.Set_Text( "OK" );
            button.Bottom.Attach( cancel.Bottom );
            button.Right.Attach( cancel.Left, 4.0 );
            button.Width.Set( 80.0 );
            button.Height.Set( 28.0 );
            button.Set_Prev( this.id & ":enuPlayer" );
            button.Set_Next( this.id & ":btnCancel" );
            button.Clicked.Connect( Slot( this, Clicked_Ok'Access ) );
    end Construct;

    ----------------------------------------------------------------------------

    procedure Clicked_Browse( this : not null access New_Dialog'Class ) is
        media  : constant String := Ensure_Trailing_Slash( Get_Pref( "application", "media" ) );
        dialog : A_Allegro_Filechooser;
        path   : Unbounded_String;
        dir    : Unbounded_String;
    begin
        dir := To_Unbounded_String( Path_Append( media, "graphics/" ) );
        if not Is_Directory( To_String( dir ) ) then
            -- if media/graphics doesn't exist, fallback to media
            dir := To_Unbounded_String( media );
        end if;

        dialog := Al_Create_Native_File_Dialog( To_String( dir ),
                                                "Choose Tile Library...",
                                                "*.tlz",
                                                ALLEGRO_FILECHOOSER_FILE_MUST_EXIST );
        if Al_Show_Native_File_Dialog( Get_View.Get_Display, dialog ) then
            path := To_Unbounded_String( Al_Get_Native_File_Dialog_Path( dialog, 0 ) );
        end if;
        Al_Destroy_Native_File_Dialog( dialog );

        -- the chosen file must be within the media directory, otherwise we
        -- won't be able to find it.
        if Length( path ) > 0 then
            dir := To_Unbounded_String( Normalize_Pathname( Get_Directory( To_String( path ) ) ) );
            if dir = Normalize_Pathname( Execution_Directory ) or else
               dir = Normalize_Pathname( media ) or else
               dir = Normalize_Pathname( Path_Append( media, "graphics/" ) )
            then
                this.ibLib.Set_Text( Remove_Extension( Get_Filename( To_String( path ) ) ) );
            elsif Al_Show_Native_Message_Box( Get_View.Get_Display, "Error",
                                              "Cannot use selected file",
                                              "The selected tile library must exist in the media directory",
                                              "", ALLEGRO_MESSAGEBOX_ERROR ) = 0
            then
                null;
            end if;
        end if;
    end Clicked_Browse;

    ----------------------------------------------------------------------------

    procedure Clicked_Cancel( this : not null access New_Dialog'Class ) is
    begin
        this.Hide;
    end Clicked_Cancel;

    ----------------------------------------------------------------------------

    procedure Clicked_Ok( this : not null access New_Dialog'Class ) is
        wstr    : constant String := this.ibWidth.Get_Text;
        hstr    : constant String := this.ibHeight.Get_Text;
        libName : constant String := this.ibLib.Get_Text;
        player  : constant String := Cast_String( this.enuPlayer.Get_Value );
        width   : Integer;
        height  : Integer;
    begin
        if wstr'Length    > 0 and then
           hstr'Length    > 0 and then
           libName'Length > 0
        then
            width  := Integer'Value( wstr );
            height := Integer'Value( hstr );
            if width > 0 and then height > 0 then
                Queue_Create_World( width, height, 16, libName, player );
                this.Hide;
            end if;
        end if;
    end Clicked_Ok;

    ----------------------------------------------------------------------------

    procedure On_Hidden( this : not null access New_Dialog'Class ) is
    begin
        A_Tinker_View(this.Get_View).Get_Scene.Request_Focus;
    end On_Hidden;

    ----------------------------------------------------------------------------

    procedure On_Shown( this : not null access New_Dialog'Class ) is
    begin
        this.ibWidth.Set_Text( "" );
        this.ibHeight.Set_Text( "" );
        this.ibLib.Set_Text( "" );
        this.enuPlayer.Set_Index( 1 );

        this.ibWidth.Request_Focus;
    end On_Shown;

end Widgets.Panels.Dialogs.New_World;
