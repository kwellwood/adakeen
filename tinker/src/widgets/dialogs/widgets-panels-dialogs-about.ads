--
-- Copyright (c) 2012-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Widgets.Buttons;                   use Widgets.Buttons;

package Widgets.Panels.Dialogs.About is

    function Create_About_Dialog( view : not null access Game_Views.Game_View'Class;
                                  id   : String := "" ) return A_Dialog;
    pragma Postcondition( Create_About_Dialog'Result /= null );

private

    type About_Dialog is new Dialog with
        record
            btnOk : A_Button;
        end record;
    type A_About_Dialog is access all About_Dialog'Class;

    procedure Construct( this : access About_Dialog;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );

end Widgets.Panels.Dialogs.About;
