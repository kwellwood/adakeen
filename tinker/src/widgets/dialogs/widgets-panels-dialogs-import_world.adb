--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Native_Dialogs;            use Allegro.Native_Dialogs;
--with Debugging;                         use Debugging;
with Events.Tinker;                     use Events.Tinker;
with Game_Views;                        use Game_Views;
with Game_Views.Tinker;                 use Game_Views.Tinker;
with GNAT.OS_Lib;                       use GNAT.OS_Lib;
with Preferences;                       use Preferences;
with Support;                           use Support;
with Support.Paths;                     use Support.Paths;
with Values.Casting;                    use Values.Casting;
with Values.Strings;                    use Values.Strings;
with Widgets.Buttons;                   use Widgets.Buttons;
with Widgets.Buttons.Pushes;            use Widgets.Buttons.Pushes;
with Widgets.Input_Boxes.Constraints;   use Widgets.Input_Boxes.Constraints;
with Widgets.Labels;                    use Widgets.Labels;
with Widgets.Panels;                    use Widgets.Panels;
with Widgets.Row_Layouts;               use Widgets.Row_Layouts;

package body Widgets.Panels.Dialogs.Import_World is

    package Key_Connections is new Signals.Keys.Connections(Import_Dialog);
    use Key_Connections;

    package Connections is new Signals.Connections(Import_Dialog);
    use Connections;

    ----------------------------------------------------------------------------

    procedure Clicked_Browse_Images( this : not null access Import_Dialog'Class );

    procedure Clicked_Browse_Libs( this : not null access Import_Dialog'Class );

    procedure Clicked_Cancel( this : not null access Import_Dialog'Class );

    procedure Clicked_Ok( this : not null access Import_Dialog'Class );

    procedure On_Hidden( this : not null access Import_Dialog'Class );

    procedure On_Shown( this : not null access Import_Dialog'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Import_Dialog( view : not null access Game_Views.Game_View'Class;
                                   id   : String := "" ) return A_Dialog is
        this : A_Import_Dialog := new Import_Dialog;
    begin
        this.Construct( view, id );
        return A_Dialog(this);
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Import_Dialog;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Import_Dialog;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String ) is
        ROW_HEIGHT : constant Float := 24.0;
        leftCol    : A_Column_Layout;
        midCol     : A_Column_Layout;
        rightCol   : A_Column_Layout;
        panel      : A_Panel;
        label      : A_Label;
        button     : A_Button;
        cancel     : A_Button;
    begin
        Dialog(this.all).Construct( view, id, "Import World From Image...", Create_Icon( "tinker:import-image" ) );
        this.Hidden.Connect( Slot( this, On_Hidden'Access ) );
        this.Shown.Connect( Slot( this, On_Shown'Access ) );
        this.KeyPressed.Connect( Slot( this, Clicked_Cancel'Access, ALLEGRO_KEY_ESCAPE ) );

        this.Set_Style( "dialog" );
        this.Set_Preferred_Width( 316.0 );
        this.Set_Preferred_Height( 191.0 );

        leftCol := Create_Column_Layout( view );
        this.Add_Widget( leftCol );
        leftCol.Top.Attach( this.Top, 8.0 );
        leftCol.Left.Attach( this.Left, 8.0 );
        leftCol.Set_Style( "dialog" );
        leftCol.Width.Set( 65.0 );

            label := Create_Label( view );
            label.Height.Set( ROW_HEIGHT );
            label.Set_Style( "dialog" );
            label.Set_Text( "Image:" );
            leftCol.Append( label );

            label := Create_Label( view );
            label.Height.Set( ROW_HEIGHT );
            label.Set_Style( "dialog" );
            label.Set_Text( "Library:" );
            leftCol.Append( label );

            label := Create_Label( view );
            label.Height.Set( ROW_HEIGHT );
            label.Set_Style( "dialog" );
            label.Set_Text( "Player:" );
            leftCol.Append( label );

            label := Create_Label( view );
            label.Height.Set( ROW_HEIGHT );
            label.Set_Style( "dialog" );
            label.Set_Text( "Tolerance:" );
            leftCol.Append( label );

        midCol := Create_Column_Layout( view );
        this.Add_Widget( midCol );
        midCol.Top.Attach( leftCol.Top );
        midCol.Left.Attach( leftCol.Right, 4.0 );
        midCol.Set_Style( "dialog" );
        midCol.Width.Set( 160.0 );

            this.ibImage := Create_Input_Box( view, this.id & ":ibImage" );
            this.ibImage.Height.Set( ROW_HEIGHT );
            this.ibImage.Width.Set( midCol.Width.Get );
            this.ibImage.Set_Style( "dialog" );
            this.ibImage.Set_Max_Length( 255 );
            this.ibImage.Set_Prev( this.id & ":btnCancel" );
            this.ibImage.Set_Next( this.id & ":btnImage" );
            midCol.Append( this.ibImage );

            this.ibLib := Create_Input_Box( view, this.id & ":ibLib" );
            this.ibLib.Height.Set( ROW_HEIGHT );
            this.ibLib.Width.Set( midCol.Width.Get );
            this.ibLib.Set_Style( "dialog" );
            this.ibLib.Set_Constraint( Constraints.Basename_Only'Access );
            this.ibLib.Set_Prev( this.id & ":btnImage" );
            this.ibLib.Set_Next( this.id & ":btnLib" );
            midCol.Append( this.ibLib );

            this.enuPlayer := Create_Enum( view, this.id & ":enuPlayer" );
            this.enuPlayer.Height.Set( ROW_HEIGHT );
            this.enuPlayer.Width.Set( midCol.Width.Get );
            this.enuPlayer.Set_Style( "dialog" );
            this.enuPlayer.Add_Item( "Platform Keen", Create( "Keen" ) );
            this.enuPlayer.Add_Item( "Map Keen", Create( "LittleKeen" ) );
            this.enuPlayer.Set_Prev( this.id & ":btnLib" );
            this.enuPlayer.Set_Next( this.id & ":ibTolerance" );
            midCol.Append( this.enuPlayer );

            this.ibTolerance := Create_Input_Box( view, this.id & ":ibTolerance" );
            this.ibTolerance.Height.Set( ROW_HEIGHT );
            this.ibTolerance.Width.Set( midCol.Width.Get / 2.0 );
            this.ibTolerance.Set_Style( "dialog" );
            this.ibTolerance.Set_Constraint( Constraints.Int_Only'Access );
            this.ibTolerance.Set_Max_Length( 3 );
            this.ibTolerance.Set_Text( "0" );
            this.ibTolerance.Set_Prev( this.id & ":enuPlayer" );
            this.ibTolerance.Set_Next( this.id & ":btnOk" );
            midCol.Append( this.ibTolerance );

        rightCol := Create_Column_Layout( view );
        this.Add_Widget( rightCol );
        rightCol.Top.Attach( midCol.Top );
        rightCol.Left.Attach( midCol.Right, 4.0 );
        rightCol.Set_Style( "dialog" );
        rightCol.Width.Set( 65.0 );

            button := Create_Push_Button( view, this.id & ":btnImage", "Browse..." );
            button.Height.Set( ROW_HEIGHT );
            button.Width.Set( 65.0 );
            button.Set_Style( "dialog" );
            button.Set_Prev( this.id & ":ibImage" );
            button.Set_Next( this.id & ":ibLib" );
            button.Clicked.Connect( Slot( this, Clicked_Browse_Images'Access ) );
            rightCol.Append( button );

            button := Create_Push_Button( view, this.id & ":btnLib", "Browse..." );
            button.Height.Set( ROW_HEIGHT );
            button.Width.Set( 65.0 );
            button.Set_Style( "dialog" );
            button.Set_Prev( this.id & ":ibLib" );
            button.Set_Next( this.id & ":enuPlayer" );
            button.Clicked.Connect( Slot( this, Clicked_Browse_Libs'Access ) );
            rightCol.Append( button );

        panel := Create_Panel( view );
        this.Add_Widget( panel );
        panel.Set_Style( "dialogResponse" );
        panel.Height.Set( 1.0 + 4.0 + 28.0 + 4.0 );
        panel.Fill_Horizontal( this );
        panel.Bottom.Attach( this.Bottom );

            cancel := Create_Push_Button( view, this.id & ":btnCancel" );
            panel.Add_Widget( cancel );
            cancel.Set_Style( "dialogResponse" );
            cancel.Set_Text( "Cancel" );
            cancel.Right.Attach( panel.Right, 4.0 );
            cancel.Bottom.Attach( panel.Bottom, 4.0 );
            cancel.Width.Set( 80.0 );
            cancel.Height.Set( 28.0 );
            cancel.Set_Prev( this.id & ":btnOk" );
            cancel.Set_Next( this.id & ":ibImage" );
            cancel.Clicked.Connect( Slot( this, Clicked_Cancel'Access ) );

            button := Create_Push_Button( view, this.id & ":btnOk" );
            panel.Add_Widget( button );
            button.Set_Style( "dialogResponse" );
            button.Set_Text( "Import" );
            button.Right.Attach( cancel.Left, 4.0 );
            button.Bottom.Attach( cancel.Bottom );
            button.Width.Set( 80.0 );
            button.Height.Set( 28.0 );
            button.Clicked.Connect( Slot( this, Clicked_Ok'Access ) );
            button.Set_Prev( this.id & ":ibTolerance" );
            button.Set_Next( this.id & ":btnCancel" );
    end Construct;

    ----------------------------------------------------------------------------

    procedure Clicked_Browse_Images( this : not null access Import_Dialog'Class ) is
        dialog : A_Allegro_Filechooser;
    begin
        dialog := Al_Create_Native_File_Dialog( Support.Paths.Home_Directory,
                                                "Choose Map Image...",
                                                "*.bmp",
                                                ALLEGRO_FILECHOOSER_FILE_MUST_EXIST );
        if Al_Show_Native_File_Dialog( Get_View.Get_Display, dialog ) then
            if Al_Get_Native_File_Dialog_Path( dialog, 0 )'Length > 0 then
                this.ibImage.Set_Text( Al_Get_Native_File_Dialog_Path( dialog, 0 ) );
            end if;
        end if;
        Al_Destroy_Native_File_Dialog( dialog );
    end Clicked_Browse_Images;

    ----------------------------------------------------------------------------

    procedure Clicked_Browse_Libs( this : not null access Import_Dialog'Class ) is
        dialog : A_Allegro_Filechooser;
        path   : Unbounded_String;
        dir    : Unbounded_String;
    begin
        dir := Get_Pref( "application", "media" ) & "graphics/";
        if not Is_Directory( To_String( dir ) ) then
            dir := Get_Pref( "application", "media" );
            if not Is_Directory( To_String( dir ) ) then
                dir := To_Unbounded_String( Execution_Directory );
            end if;
        end if;

        dialog := Al_Create_Native_File_Dialog( To_String( dir ),
                                                "Choose Tile Library...",
                                                "*.tlz",
                                                ALLEGRO_FILECHOOSER_FILE_MUST_EXIST );
        if Al_Show_Native_File_Dialog( Get_View.Get_Display, dialog ) then
            path := To_Unbounded_String( Al_Get_Native_File_Dialog_Path( dialog, 0 ) );
        end if;
        Al_Destroy_Native_File_Dialog( dialog );

        -- the chosen file must be within the media directory, otherwise we
        -- won't be able to find it.
        if Length( path ) > 0 then
            dir := To_Unbounded_String( Normalize_Pathname( Get_Directory( To_String( path ) ) ) );
            if dir = Normalize_Pathname( Execution_Directory ) or else
               dir = Normalize_Pathname( Get_Pref( "application", "media" ) ) or else
               dir = Normalize_Pathname( String'(Get_Pref( "application", "media" )) & "graphics/" )
            then
                this.ibLib.Set_Text( Remove_Extension( Get_Filename( To_String( path ) ) ) );
            elsif Al_Show_Native_Message_Box( Get_View.Get_Display, "Error",
                                              "Cannot use selected file",
                                              "The selected tile library must exist in the media directory",
                                              "", ALLEGRO_MESSAGEBOX_ERROR ) = 0
            then
                null;
            end if;
        end if;
    end Clicked_Browse_Libs;

    ----------------------------------------------------------------------------

    procedure Clicked_Cancel( this : not null access Import_Dialog'Class ) is
    begin
        this.Hide;
    end Clicked_Cancel;

    ----------------------------------------------------------------------------

    procedure Clicked_Ok( this : not null access Import_Dialog'Class ) is
        image     : constant String := this.ibImage.Get_Text;
        libName   : constant String := this.ibLib.Get_Text;
        player    : constant String := Cast_String( this.enuPlayer.Get_Value );
        tolerance : constant String := this.ibTolerance.Get_Text;
    begin
        if image'Length     > 0 and then
           libName'Length   > 0 and then
           tolerance'Length > 0
        then
            Queue_Import_World( image, libName, player, Integer'Value( tolerance ) );
            this.Hide;
        end if;
    end Clicked_Ok;

    ----------------------------------------------------------------------------

    procedure On_Hidden( this : not null access Import_Dialog'Class ) is
    begin
        A_Tinker_View(this.Get_View).Get_Scene.Request_Focus;
    end On_Hidden;

    ----------------------------------------------------------------------------

    procedure On_Shown( this : not null access Import_Dialog'Class ) is
    begin
        this.ibImage.Set_Text( "" );
        this.ibLib.Set_Text( "" );
        this.enuPlayer.Set_Index( 1 );
        this.ibTolerance.Set_Text( "0" );

        this.ibImage.Request_Focus;
    end On_Shown;

end Widgets.Panels.Dialogs.Import_World;
