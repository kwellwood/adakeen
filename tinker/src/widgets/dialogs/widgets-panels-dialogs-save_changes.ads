--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Widgets.Buttons;                   use Widgets.Buttons;

package Widgets.Panels.Dialogs.Save_Changes is

    type Dialog_Action is (
        DA_None,
        DA_New,
        DA_Open,
        DA_Recent,
        DA_Import_Image,
        DA_Import_Scribble,
        DA_Export_Image,
        DA_Export_Scribble,
        DA_Quit
    );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Save_Changes_Dialog is new Dialog with private;
    type A_Save_Changes_Dialog is access all Save_Changes_Dialog'Class;

    function Create_Save_Changes_Dialog( view : not null access Game_Views.Game_View'Class;
                                         id   : String := "" ) return A_Save_Changes_Dialog;
    pragma Postcondition( Create_Save_Changes_Dialog'Result /= null );

    -- Sets the next dialog action to be performed after the Save Changes dialog
    -- has been answered.
    procedure Set_Next_Action( this : not null access Save_Changes_Dialog;
                               next : Dialog_Action );

private

    type Save_Changes_Dialog is new Dialog with
        record
            next   : Dialog_Action := DA_None;
            btnYes : A_Button;
        end record;

    procedure Construct( this : access Save_Changes_Dialog;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );

end Widgets.Panels.Dialogs.Save_Changes;
