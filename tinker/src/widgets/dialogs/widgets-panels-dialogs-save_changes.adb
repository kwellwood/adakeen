--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Keyboard;                  use Allegro.Keyboard;
--with Debugging;                         use Debugging;
with Game_Views;                        use Game_Views;
with Game_Views.Tinker;                 use Game_Views.Tinker;
with Support;                           use Support;
with Widgets.Buttons.Pushes;            use Widgets.Buttons.Pushes;
with Widgets.Labels;                    use Widgets.Labels;
with Widgets.Panels;                    use Widgets.Panels;

package body Widgets.Panels.Dialogs.Save_Changes is

    package Connections is new Signals.Connections(Save_Changes_Dialog);
    use Connections;

    package Key_Connections is new Signals.Keys.Connections(Save_Changes_Dialog);
    use Key_Connections;

    ----------------------------------------------------------------------------

    procedure Clicked_Cancel( this : not null access Save_Changes_Dialog'Class );

    procedure Clicked_No( this : not null access Save_Changes_Dialog'Class );

    procedure Clicked_Yes( this : not null access Save_Changes_Dialog'Class );

    function Get_Tinker_View( this : not null access Save_Changes_Dialog'Class ) return A_Tinker_View is (A_Tinker_View(this.Get_View));

    procedure On_Hidden( this : not null access Save_Changes_Dialog'Class );

    procedure On_Shown( this : not null access Save_Changes_Dialog'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Save_Changes_Dialog( view : not null access Game_Views.Game_View'Class;
                                         id   : String := "" ) return A_Save_Changes_Dialog is
        this : A_Save_Changes_Dialog := new Save_Changes_Dialog;
    begin
        this.Construct( view, id );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Save_Changes_Dialog;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Save_Changes_Dialog;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String ) is
        label1 : A_Label;
        label2 : A_Label;
        panel  : A_Panel;
        no     : A_Button;
        cancel : A_Button;
    begin
        Dialog(this.all).Construct( view, id, "Unsaved Changes", Create_Icon( "tinker:warning" ) );
        this.Hidden.Connect( Slot( this, On_Hidden'Access ) );
        this.Shown.Connect( Slot( this, On_Shown'Access ) );
        this.KeyPressed.Connect( Slot( this, Clicked_Cancel'Access, ALLEGRO_KEY_ESCAPE ) );
        this.KeyPressed.Connect( Slot( this, Clicked_Yes'Access, ALLEGRO_KEY_Y ) );
        this.KeyPressed.Connect( Slot( this, Clicked_No'Access, ALLEGRO_KEY_N ) );

        this.Set_Style( "dialog" );
        this.Set_Preferred_Width( 340.0 );
        this.Set_Preferred_Height( 116.0 );

        label1 := Create_Label( view );
        this.Add_Widget( label1 );
        label1.Set_Style( "dialog" );
        label1.Set_Text( "The world has been modified." );
        label1.Horizontal_Center.Attach( label1.Get_Parent.Horizontal_Center );
        label1.Top.Attach( label1.Get_Parent.Top, 4.0 );
        label1.Height.Set( 18.0 );

        label2 := Create_Label( view );
        this.Add_Widget( label2 );
        label2.Set_Style( "dialog" );
        label2.Set_Text( "Do you want to save your changes?" );
        label2.Horizontal_Center.Attach( label2.Get_Parent.Horizontal_Center );
        label2.Top.Attach( label1.Bottom, 6.0 );
        label2.Height.Set( 18.0 );

        panel := Create_Panel( view );
        this.Add_Widget( panel );
        panel.Set_Style( "dialogResponse" );
        panel.Height.Set( 1.0 + 4.0 + 28.0 + 4.0 );
        panel.Fill_Horizontal( this );
        panel.Bottom.Attach( this.Bottom );

            cancel := Create_Push_Button( view, this.id & ":btnCancel", "Cancel" );
            panel.Add_Widget( cancel );
            cancel.Set_Style( "dialogResponse" );
            cancel.Bottom.Attach( panel.Bottom, 4.0 );
            cancel.Right.Attach( panel.Right, 4.0 );
            cancel.Width.Set( 80.0 );
            cancel.Height.Set( 28.0 );
            cancel.Clicked.Connect( Slot( this, Clicked_Cancel'Access ) );
            cancel.Set_Prev( this.id & ":btnNo" );
            cancel.Set_Next( this.id & ":btnYes" );

            no := Create_Push_Button( view, this.id & ":btnNo", "No" );
            panel.Add_Widget( no );
            no.Set_Style( "dialogResponse" );
            no.Bottom.Attach( panel.Bottom, 4.0 );
            no.Right.Attach( cancel.Left, 4.0 );
            no.Width.Set( 80.0 );
            no.Height.Set( 28.0 );
            no.Clicked.Connect( Slot( this, Clicked_No'Access ) );
            no.Set_Prev( this.id & ":btnYes" );
            no.Set_Next( this.id & ":btnCancel" );

            this.btnYes := Create_Push_Button( view, this.id & ":btnYes", "Yes" );
            panel.Add_Widget( this.btnYes );
            this.btnYes.Set_Style( "dialogResponse" );
            this.btnYes.Bottom.Attach( no.Bottom );
            this.btnYes.Right.Attach( no.Left, 4.0 );
            this.btnYes.Width.Set( 80.0 );
            this.btnYes.Height.Set( 28.0 );
            this.btnYes.Clicked.Connect( Slot( this, Clicked_Yes'Access ) );
            this.btnYes.Set_Prev( this.id & ":btnCancel" );
            this.btnYes.Set_Next( this.id & ":btnNo" );
    end Construct;

    ----------------------------------------------------------------------------

    procedure Clicked_Cancel( this : not null access Save_Changes_Dialog'Class ) is
    begin
        this.Hide;
    end Clicked_Cancel;

    ----------------------------------------------------------------------------

    procedure Clicked_No( this : not null access Save_Changes_Dialog'Class ) is
    begin
        this.Hide;
        if this.next /= DA_None then
            this.Get_Tinker_View.Do_Dialog_Action( this.next, askToSave => False );
            this.next := DA_None;
        end if;
    end Clicked_No;

    ----------------------------------------------------------------------------

    procedure Clicked_Yes( this : not null access Save_Changes_Dialog'Class ) is
    begin
        this.Hide;
        if this.next /= DA_None then
            if this.Get_Tinker_View.Save_World then
                this.Get_Tinker_View.Do_Dialog_Action( this.next, askToSave => False );
            end if;
            this.next := DA_None;
        end if;
    end Clicked_Yes;

    ----------------------------------------------------------------------------

    procedure On_Hidden( this : not null access Save_Changes_Dialog'Class ) is
    begin
        this.Get_Tinker_View.Resume_Game;
        this.Get_Tinker_View.Get_Scene.Request_Focus;
    end On_Hidden;

    ----------------------------------------------------------------------------

    procedure On_Shown( this : not null access Save_Changes_Dialog'Class ) is
    begin
        this.Get_Tinker_View.Pause_Game;
        this.btnYes.Request_Focus;
    end On_Shown;

    ----------------------------------------------------------------------------

    procedure Set_Next_Action( this : not null access Save_Changes_Dialog;
                               next : Dialog_Action ) is
    begin
        this.next := next;
    end Set_Next_Action;

end Widgets.Panels.Dialogs.Save_Changes;
