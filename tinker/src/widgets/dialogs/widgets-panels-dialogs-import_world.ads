--
-- Copyright (c) 2012-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Widgets.Enumerations;              use Widgets.Enumerations;
with Widgets.Input_Boxes;               use Widgets.Input_Boxes;

package Widgets.Panels.Dialogs.Import_World is

    function Create_Import_Dialog( view : not null access Game_Views.Game_View'Class;
                                   id   : String := "" ) return A_Dialog;
    pragma Postcondition( Create_Import_Dialog'Result /= null );

private

    type Import_Dialog is new Dialog with
        record
            ibLib       : A_Input_Box;
            ibImage     : A_Input_Box;
            ibTolerance : A_Input_Box;
            enuPlayer   : A_Enumeration;
        end record;
    type A_Import_Dialog is access all Import_Dialog'Class;

    procedure Construct( this : access Import_Dialog;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );

end Widgets.Panels.Dialogs.Import_World;
