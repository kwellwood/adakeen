--
-- Copyright (c) 2012-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

private with Widgets.Input_Boxes;

package Widgets.Panels.Dialogs.Resize_World is

    function Create_Resize_Dialog( view : not null access Game_Views.Game_View'Class;
                                   id   : String := "" ) return A_Dialog;
    pragma Postcondition( Create_Resize_Dialog'Result /= null );

private

    use Widgets.Input_Boxes;

    type Resize_Dialog is new Dialog with
        record
            mapHeight,
            mapWidth  : Integer;
            ibWidth   : A_Input_Box;                -- not owned
            ibHeight  : A_Input_Box;                -- not owned
        end record;
    type A_Resize_Dialog is access all Resize_Dialog'Class;

    procedure Construct( this : access Resize_Dialog;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );

    procedure Handle_Event( this : access Resize_Dialog;
                            evt  : in out A_Event;
                            resp : out Response_Type );
    pragma Precondition( evt /= null );

end Widgets.Panels.Dialogs.Resize_World;
