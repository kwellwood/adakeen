--
-- Copyright (c) 2012-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Widgets.Buttons;                   use Widgets.Buttons;
with Widgets.Enumerations;              use Widgets.Enumerations;
with Widgets.Input_Boxes;               use Widgets.Input_Boxes;

package Widgets.Panels.Dialogs.New_World is

    function Create_New_Dialog( view : not null access Game_Views.Game_View'Class;
                                id   : String := "" ) return A_Dialog;
    pragma Postcondition( Create_New_Dialog'Result /= null );

private

    type New_Dialog is new Dialog with
        record
            ibWidth   : A_Input_Box;
            ibHeight  : A_Input_Box;
            ibLib     : A_Input_Box;
            enuPlayer : A_Enumeration;
            btnLib    : A_Button;
            btnOk     : A_Button;
            btnCancel : A_Button;
        end record;
    type A_New_Dialog is access all New_Dialog'Class;

    procedure Construct( this : access New_Dialog;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );

end Widgets.Panels.Dialogs.New_World;
