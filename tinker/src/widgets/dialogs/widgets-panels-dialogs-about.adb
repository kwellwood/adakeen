--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Keyboard;                  use Allegro.Keyboard;
--with Debugging;                         use Debugging;
with GNAT.OS_Lib;                       use GNAT.OS_Lib;
with Game_Views;                        use Game_Views;
with Game_Views.Tinker;                 use Game_Views.Tinker;
with Preferences;                       use Preferences;
with Support;                           use Support;
with Support.Paths;                     use Support.Paths;
with Values.Strings;                    use Values.Strings;
with Version;
with Widgets.Buttons.Pushes;            use Widgets.Buttons.Pushes;
with Widgets.Labels;                    use Widgets.Labels;
with Widgets.Text_Boxes;                use Widgets.Text_Boxes;

package body Widgets.Panels.Dialogs.About is

    package Key_Connections is new Signals.Keys.Connections(About_Dialog);
    use Key_Connections;

    package Connections is new Signals.Connections(About_Dialog);
    use Connections;

    ----------------------------------------------------------------------------

    procedure Clicked_License( this : not null access About_Dialog'Class );

    procedure Clicked_Ok( this : not null access About_Dialog'Class );

    procedure On_Hidden( this : not null access About_Dialog'Class );

    procedure On_Shown( this : not null access About_Dialog'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_About_Dialog( view : not null access Game_Views.Game_View'Class;
                                  id   : String := "" ) return A_Dialog is
        this : A_About_Dialog := new About_Dialog;
    begin
        this.Construct( view, id );
        return A_Dialog(this);
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_About_Dialog;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access About_Dialog;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String ) is
        label   : A_Label;
        textbox : A_Textbox;
        panel   : A_Panel;
        button  : A_Button;
    begin
        Dialog(this.all).Construct( view, id, "About Tinker", Create_Icon( "tinker:information" ) );
        this.Hidden.Connect( Slot( this, On_Hidden'Access ) );
        this.Shown.Connect( Slot( this, On_Shown'Access ) );
        this.KeyPressed.Connect( Slot( this, Clicked_Ok'Access, ALLEGRO_KEY_ESCAPE ) );

        this.Set_Style( "dialog" );
        this.Set_Preferred_Width( 385.0 );
        this.Set_Preferred_Height( 175.0 );

        label := Create_Label( view );
        this.Add_Widget( label );
        label.Set_Style( "dialog" );
        label.Set_Icon( Create_Icon( "tinker:tinker-64" ) );
        label.Left.Attach( label.Get_Parent.Left, 8.0 );
        label.Top.Attach( label.Get_Parent.Top, 8.0 );

        textbox := Create_Textbox( view );
        this.Add_Widget( textbox );
        textbox.Top.Attach( label.Top, 5.0 );
        textbox.Left.Attach( label.Right, 6.0 );
        textbox.Set_Style( "dialog" );
        textbox.Append_Line( "Tinker Map Editor" );
        textbox.Append_Line( "v" & Version.Release );
        textbox.Append_Line( "Kevin Wellwood (C) " & Version.Copyright_Year );

        label := Create_Label( view );
        this.Add_Widget( label );
        label.Set_Style( "dialog" );
        label.Set_Text( "With icons by http://icons8.com" );
        label.Set_Style_Property( "background.align", Create( "center" ) );
        label.Fill_Horizontal( label.Get_Parent, 5.0, 5.0 );
        label.Top.Attach( label.Get_Parent.Top, 69.0 );
        label.Height.Set( 15.0 );

        label := Create_Label( view );
        this.Add_Widget( label );
        label.Set_Style( "dialog" );
        label.Set_Text( "Portions Copyright (C) by their respective authors." );
        label.Set_Style_Property( "background.align", Create( "center" ) );
        label.Fill_Horizontal( label.Get_Parent, 5.0, 5.0 );
        label.Top.Attach( label.Get_Parent.Top, 89.0 );   -- 20px under the above label
        label.Height.Set( 15.0 );

        panel := Create_Panel( view );
        this.Add_Widget( panel );
        panel.Set_Style( "dialogResponse" );
        panel.Height.Set( 1.0 + 4.0 + 28.0 + 4.0 );
        panel.Fill_Horizontal( this );
        panel.Bottom.Attach( this.Bottom );

            button := Create_Push_Button( view, this.id & ":btnLic" );
            panel.Add_Widget( button );
            button.Set_Style( "dialogResponse" );
            button.Set_Text( "Third-Party Licenses" );
            button.Set_Icon( Create_Icon( "tinker:openapp-white-16" ) );
            button.Left.Attach( panel.Left, 4.0 );
            button.Bottom.Attach( panel.Bottom, 4.0 );
            button.Width.Set( 180.0 );
            button.Height.Set( 28.0 );
            button.Set_Prev( this.id & ":btnOk" );
            button.Set_Next( this.id & ":btnOk" );
            button.Clicked.Connect( Slot( this, Clicked_License'Access ) );

            button := Create_Push_Button( view, this.id & ":btnOk" );
            panel.Add_Widget( button );
            button.Set_Style( "dialogResponse" );
            button.Set_Text( "OK" );
            button.Right.Attach( panel.Right, 4.0 );
            button.Bottom.Attach( panel.Bottom, 4.0 );
            button.Width.Set( 80.0 );
            button.Height.Set( 28.0 );
            button.Set_Prev( this.id & ":btnLic" );
            button.Set_Next( this.id & ":btnLic" );
            button.Clicked.Connect( Slot( this, Clicked_Ok'Access ) );
            this.btnOk := button;
    end Construct;

    ----------------------------------------------------------------------------

    procedure Clicked_License( this : not null access About_Dialog'Class ) is
        pragma Unreferenced( this );
    begin
        Reveal_Path( Normalize_Pathname( Get_Pref( "application", "license" ) ) );
    end Clicked_License;

    ----------------------------------------------------------------------------

    procedure Clicked_Ok( this : not null access About_Dialog'Class ) is
    begin
        this.Hide;
    end Clicked_Ok;

    ----------------------------------------------------------------------------

    procedure On_Hidden( this : not null access About_Dialog'Class ) is
    begin
        A_Tinker_View(this.Get_View).Get_Scene.Request_Focus;
    end On_Hidden;

    ----------------------------------------------------------------------------

    procedure On_Shown( this : not null access About_Dialog'Class ) is
    begin
        this.btnOk.Request_Focus;
    end On_Shown;

end Widgets.Panels.Dialogs.About;
