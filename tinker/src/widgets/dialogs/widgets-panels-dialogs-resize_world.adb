--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Keyboard;                  use Allegro.Keyboard;
--with Debugging;                         use Debugging;
with Events.World;                      use Events.World;
with Game_Views;                        use Game_Views;
with Game_Views.Tinker;                 use Game_Views.Tinker;
with Support;                           use Support;
with Widgets.Buttons;                   use Widgets.Buttons;
with Widgets.Buttons.Pushes;            use Widgets.Buttons.Pushes;
with Widgets.Input_Boxes.Constraints;   use Widgets.Input_Boxes.Constraints;
with Widgets.Labels;                    use Widgets.Labels;
with Widgets.Row_Layouts;               use Widgets.Row_Layouts;

package body Widgets.Panels.Dialogs.Resize_World is

    package Connections is new Signals.Connections(Resize_Dialog);
    use Connections;

    package Key_Connections is new Signals.Keys.Connections(Resize_Dialog);
    use Key_Connections;

    ----------------------------------------------------------------------------

    procedure Clicked_Cancel( this : not null access Resize_Dialog'Class );

    procedure Clicked_Ok( this : not null access Resize_Dialog'Class );

    function Get_Tinker_View( this : not null access Resize_Dialog'Class ) return A_Tinker_View is (A_Tinker_View(this.Get_View));

    procedure On_Hidden( this : not null access Resize_Dialog'Class );

    procedure On_Shown( this : not null access Resize_Dialog'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Resize_Dialog( view : not null access Game_Views.Game_View'Class;
                                   id   : String := "" ) return A_Dialog is
        this : A_Resize_Dialog := new Resize_Dialog;
    begin
        this.Construct( view, id );
        return A_Dialog(this);
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Resize_Dialog;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Resize_Dialog;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String ) is
        ROW_HEIGHT : constant := 24.0;
        button     : A_Button;
        cancel     : A_Button;
        label      : A_Label;
        panel      : A_Panel;
        leftCol    : A_Column_Layout;
        rightCol   : A_Column_Layout;
        row        : A_Row_Layout;
    begin
        Dialog(this.all).Construct( view, id, "Resize...", Create_Icon( "tinker:resize" ) );

        this.Hidden.Connect( Slot( this, On_Hidden'Access ) );
        this.Shown.Connect( Slot( this, On_Shown'Access ) );
        this.KeyPressed.Connect( Slot( this, Clicked_Cancel'Access, ALLEGRO_KEY_ESCAPE ) );

        this.Listen_For_Event( EVT_WORLD_LOADED );

        this.Set_Style( "dialog" );
        this.Set_Preferred_Width( 220.0 );
        this.Set_Preferred_Height( 155.0 );

        label := Create_Label( view );
        this.Add_Widget( label );
        label.Fill_Horizontal( label.Get_Parent, 8.0, 8.0 );
        label.Top.Attach( label.Get_Parent.Top, 4.0 );
        label.Height.Set( 20.0 );
        label.Set_Style( "dialog" );
        label.Set_Text( "New Dimensions:" );

        leftCol := Create_Column_Layout( view );
        this.Add_Widget( leftCol );
        leftCol.Top.Attach( label.Bottom, 4.0 );
        leftCol.Left.Attach( this.Left, 8.0 );
        leftCol.Set_Style( "dialog" );
        leftCol.Width.Set( 70.0 );

            label := Create_Label( view );
            label.Height.Set( ROW_HEIGHT );
            label.Set_Style( "dialog" );
            label.Set_Text( "Width:" );
            leftCol.Append( label );

            label := Create_Label( view );
            label.Height.Set( ROW_HEIGHT );
            label.Set_Style( "dialog" );
            label.Set_Text( "Height:" );
            leftCol.Append( label );

        rightCol := Create_Column_Layout( view );
        this.Add_Widget( rightCol );
        rightCol.Top.Attach( leftCol.Top );
        rightCol.Left.Attach( leftCol.Right );
        rightCol.Set_Style( "dialog" );
        rightCol.Width.Set( 106.0 );

            row := Create_Row_Layout( view );
            row.Height.Set( ROW_HEIGHT );
            row.Set_Style( "dialog" );
            rightCol.Append( row );

                this.ibWidth := Create_Input_Box( view, this.id & ":ibWidth" );
                this.ibWidth.Height.Set( ROW_HEIGHT );
                this.ibWidth.Width.Set( 53.0 );
                this.ibWidth.Set_Style( "dialog" );
                this.ibWidth.Set_Constraint( Constraints.Int_Only'Access );
                this.ibWidth.Accepted.Connect( Slot( this, Clicked_Ok'Access ) );
                this.ibWidth.Set_Max_Length( 4 );
                this.ibWidth.Set_Prev( this.id & ":btnCancel" );
                this.ibWidth.Set_Next( this.id & ":ibHeight" );
                row.Append( this.ibWidth );

                label := Create_Label( view );
                label.Height.Set( ROW_HEIGHT );
                label.Set_Style( "dialog" );
                label.Set_Text( "Tiles" );
                row.Append( label );

            row := Create_Row_Layout( view );
            row.Height.Set( ROW_HEIGHT );
            row.Set_Style( "dialog" );
            rightCol.Append( row );

                this.ibHeight := Create_Input_Box( view, this.id & ":ibHeight" );
                this.ibHeight.Height.Set( ROW_HEIGHT );
                this.ibHeight.Width.Set( 53.0 );
                this.ibHeight.Set_Style( "dialog" );
                this.ibHeight.Set_Constraint( Constraints.Int_Only'Access );
                this.ibHeight.Accepted.Connect( Slot( this, Clicked_Ok'Access ) );
                this.ibHeight.Set_Max_Length( 4 );
                this.ibHeight.Set_Prev( this.id & ":ibWidth" );
                this.ibHeight.Set_Next( this.id & ":btnOk" );
                row.Append( this.ibHeight );

                label := Create_Label( view );
                label.Height.Set( ROW_HEIGHT );
                label.Set_Style( "dialog" );
                label.Set_Text( "Tiles" );
                row.Append( label );

        panel := Create_Panel( view );
        this.Add_Widget( panel );
        panel.Set_Style( "dialogResponse" );
        panel.Height.Set( 1.0 + 4.0 + 28.0 + 4.0 );
        panel.Fill_Horizontal( this );
        panel.Bottom.Attach( this.Bottom );

            cancel := Create_Push_Button( view, this.id & ":btnCancel" );
            panel.Add_Widget( cancel );
            cancel.Set_Style( "dialogResponse" );
            cancel.Set_Text( "Cancel" );
            cancel.Bottom.Attach( panel.Bottom, 4.0 );
            cancel.Right.Attach( panel.Right, 4.0 );
            cancel.Width.Set( 80.0 );
            cancel.Height.Set( 28.0 );
            cancel.Set_Prev( this.id & ":btnOk" );
            cancel.Set_Next( this.id & ":ibWidth" );
            cancel.Clicked.Connect( Slot( this, Clicked_Cancel'Access ) );

            button := Create_Push_Button( view, this.id & ":btnOk" );
            panel.Add_Widget( button );
            button.Set_Style( "dialogResponse" );
            button.Set_Text( "OK" );
            button.Bottom.Attach( cancel.Bottom );
            button.Right.Attach( cancel.Left, 4.0 );
            button.Width.Set( 80.0 );
            button.Height.Set( 28.0 );
            button.Set_Prev( this.id & ":ibHeight" );
            button.Set_Next( this.id & ":btnCancel" );
            button.Clicked.Connect( Slot( this, Clicked_Ok'Access ) );
    end Construct;

    ----------------------------------------------------------------------------

    procedure Clicked_Cancel( this : not null access Resize_Dialog'Class ) is
    begin
        this.Hide;
    end Clicked_Cancel;

    ----------------------------------------------------------------------------

    procedure Clicked_Ok( this : not null access Resize_Dialog'Class ) is
        wstr   : constant String := this.ibWidth.Get_Text;
        hstr   : constant String := this.ibHeight.Get_Text;
        width  : Integer;
        height : Integer;
    begin
        if wstr'Length > 0 and then hstr'Length > 0 then
            width := Integer'Value( wstr );
            height := Integer'Value( hstr );
            if width > 0 and then height > 0 then
                Queue_Resize_World( width, height );
                this.Hide;
            end if;
        end if;
    end Clicked_Ok;

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Event( this : access Resize_Dialog;
                            evt  : in out A_Event;
                            resp : out Response_Type ) is
        pragma Unreferenced( resp );
    begin
        if evt.Get_Event_Id = EVT_WORLD_LOADED then
            this.mapWidth := A_World_Loaded_Event(evt).Get_Map.Get_Width;
            this.mapHeight := A_World_Loaded_Event(evt).Get_Map.Get_Height;
        end if;
    end Handle_Event;

    ----------------------------------------------------------------------------

    procedure On_Hidden( this : not null access Resize_Dialog'Class ) is
    begin
        this.Get_Tinker_View.Resume_Game;
        this.Get_Tinker_View.Get_Scene.Request_Focus;
    end On_Hidden;

    ----------------------------------------------------------------------------

    procedure On_Shown( this : not null access Resize_Dialog'Class ) is
    begin
        this.Get_Tinker_View.Pause_Game;
        this.ibWidth.Set_Text( Image( this.mapWidth ) );
        this.ibHeight.Set_Text( Image( this.mapHeight ) );
        this.ibWidth.Request_Focus;
    end On_Shown;

end Widgets.Panels.Dialogs.Resize_World;
