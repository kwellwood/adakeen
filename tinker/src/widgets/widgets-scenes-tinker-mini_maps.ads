--
-- Copyright (c) 2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Widgets.Scenes.Tinker.Mini_Maps is

    type Mini_Map is new Widget with private;
    type A_Mini_Map is access all Mini_Map'Class;

    function Create_Mini_Map( view : not null access Game_Views.Game_View'Class ) return A_Mini_Map;
    pragma Postcondition( Create_Mini_Map'Result /= null );

    -- Sets the scene to be displayed.
    procedure Set_Scene( this : not null access Mini_Map'Class; scene : A_Tinker_Scene );

private

    BUFFER_SCALE : constant := 2.0;

    -- mouse wheel sensitivity for zooming
    WHEEL_SENSITIVITY : constant := 2.0;

    type Mini_Map is new Widget with
        record
            scene    : A_Tinker_Scene;      -- the scene to draw
            buffer   : A_Allegro_Bitmap;    -- bitmap of the scene (enlarged by atleast BUFFER_SCALE)
            redraw   : Boolean := True;     -- need to redraw map?
            dragging : Boolean := False;    -- dragging with left mouse button?
        end record;

    procedure Construct( this : access Mini_Map;
                         view : not null access Game_Views.Game_View'Class );

    procedure Delete( this : in out Mini_Map );

    procedure Draw_Content( this : access Mini_Map );

    procedure Handle_Event( this : access Mini_Map;
                            evt  : in out A_Event;
                            resp : out Response_Type );

end Widgets.Scenes.Tinker.Mini_Maps;
