--
-- Copyright (c) 2012-2014 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Worlds.Importing is

    -- Raises an exception on error.
    function Import_World( imagePath,
                           libName,
                           playerTemplate : String;
                           tolerance      : Natural ) return A_World;
    pragma Precondition( imagePath'Length > 0 );
    pragma Precondition( libName'Length > 0 );
    pragma Postcondition( Import_World'Result /= null );

end Worlds.Importing;
