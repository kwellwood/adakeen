--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Events.Manager;                    use Events.Manager;
with Values.Errors;                     use Values.Errors;

package body Events.Tinker is

    not overriding
    procedure Construct( this    : access Error_Message_Event;
                         title   : String;
                         heading : String;
                         text    : String ) is
    begin
        Event(this.all).Construct( EVT_ERROR_MESSAGE );
        this.heading := To_Unbounded_String( heading );
        this.text := To_Unbounded_String( text );
        this.title := To_Unbounded_String( title );
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Heading( this : not null access Error_Message_Event'Class ) return String is (To_String( this.heading ));

    function Get_Text( this : not null access Error_Message_Event'Class ) return String is (To_String( this.text ));

    function Get_Title( this : not null access Error_Message_Event'Class ) return String is (To_String( this.title ));

    --==========================================================================

    not overriding
    procedure Construct( this           : access Import_World_Event;
                         filename       : String;
                         libName        : String;
                         playerTemplate : String;
                         tolerance      : Natural ) is
    begin
        Event(this.all).Construct( EVT_IMPORT_WORLD );
        this.filename := To_Unbounded_String( filename );
        this.libName := To_Unbounded_String( libName );
        this.playerTemplate := To_Unbounded_String( playerTemplate );
        this.tolerance := tolerance;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Filename( this : not null access Import_World_Event'Class ) return String is (To_String( this.filename ));

    function Get_Library_Name( this : not null access Import_World_Event'Class ) return String is (To_String( this.libName ));

    function Get_Player_Template( this : not null access Import_World_Event'Class ) return String is (To_String( this.playerTemplate ));

    function Get_Tolerance( this : not null access Import_World_Event'Class ) return Natural is (this.tolerance);

    --==========================================================================

    not overriding
    procedure Construct( this      : access Save_World_Event;
                         filename  : String;
                         format    : String;
                         temporary : Boolean ) is
    begin
        Event(this.all).Construct( EVT_SAVE_WORLD );
        this.filename := To_Unbounded_String( filename );
        this.format := To_Unbounded_String( format );
        this.temporary := temporary;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Filename( this : not null access Save_World_Event'Class ) return String is (To_String( this.filename ));

    ----------------------------------------------------------------------------

    function Get_Format( this : not null access Save_World_Event'Class ) return String is (To_String( this.format ));

    ----------------------------------------------------------------------------

    function Is_Temporary( this : not null access Save_World_Event'Class ) return Boolean is (this.temporary);

    --==========================================================================

    procedure Queue_Error_Message( title   : String;
                                   heading : String;
                                   text    : String ) is
        evt : A_Error_Message_Event := new Error_Message_Event;
    begin
        evt.Construct( title, heading, text );
        Events.Manager.Global.Queue_Event( A_Event(evt), GAME_CHANNEL );
    end Queue_Error_Message;

    ----------------------------------------------------------------------------

    procedure Queue_Export_World( filePath : String; format : String ) is
        evt : A_Save_World_Event := new Save_World_Event;
    begin
        evt.Construct( filePath, format => format, temporary => True );
        Events.Manager.Global.Queue_Event( A_Event(evt), VIEW_CHANNEL );
    end Queue_Export_World;

    ----------------------------------------------------------------------------

    procedure Queue_Import_World( filename       : String;
                                  libName        : String := "";
                                  playerTemplate : String := "";
                                  tolerance      : Natural := 0 ) is
        evt : A_Import_World_Event := new Import_World_Event;
    begin
        evt.Construct( filename, libName, playerTemplate, tolerance );
        Events.Manager.Global.Queue_Event( A_Event(evt), VIEW_CHANNEL );
    end Queue_Import_World;

    ----------------------------------------------------------------------------

    procedure Queue_Save_World( filename : String ) is
        evt : A_Save_World_Event := new Save_World_Event;
    begin
        evt.Construct( filename, format => "", temporary => False );
        Events.Manager.Global.Queue_Event( A_Event(evt), VIEW_CHANNEL );
    end Queue_Save_World;

    ----------------------------------------------------------------------------

    function Trigger_Save_Temporary_World( filename : String ) return Boolean is
        evt  : A_Save_World_Event := new Save_World_Event;
        resp : Response_Type;
    begin
        evt.Construct( filename, format => "", temporary => True );
        Events.Manager.Global.Trigger_Event( A_Event(evt), resp );
        return Get_Status( resp ) = ST_SUCCESS;
    end Trigger_Save_Temporary_World;

    ----------------------------------------------------------------------------

    procedure Queue_Repawn_Entities is
        evt : A_Event := new Event;
    begin
        evt.Construct( EVT_RESPAWN_ENTITIES );
        Events.Manager.Global.Queue_Event( evt );
    end Queue_Repawn_Entities;

end Events.Tinker;
