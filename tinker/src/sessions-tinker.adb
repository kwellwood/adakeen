--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Containers.Doubly_Linked_Lists;
with Ada.Exceptions;                    use Ada.Exceptions;
with Debugging;                         use Debugging;
with Entities;                          use Entities;
with Entities.Factory;                  use Entities.Factory;
with Events.Tinker;                     use Events.Tinker;
with Events.World;                      use Events.World;
with Games;                             use Games;
with Maps;                              use Maps;
with Resources;                         use Resources;
with Support.Paths;                     use Support.Paths;
with Values.Casting;                    use Values.Casting;
with Values.Construction;               use Values.Construction;
with Values.Errors;                     use Values.Errors;
with Values.Strings;                    use Values.Strings;
with Worlds.Importing;                  use Worlds.Importing;
with Worlds.Scribble_Format;

package body Sessions.Tinker is

    procedure Handle_Create_World( this : not null access Tinker_Session'Class;
                                   evt  : not null A_Create_World_Event;
                                   resp : out Response_Type );

    procedure Handle_Import_World( this : not null access Tinker_Session'Class;
                                   evt  : not null A_Import_World_Event;
                                   resp : out Response_Type );

    procedure Handle_Resize_World( this : not null access Tinker_Session'Class;
                                   evt  : not null A_Resize_World_Event;
                                   resp : out Response_Type );

    procedure Handle_Save_World( this : not null access Tinker_Session'Class;
                                 evt  : not null A_Save_World_Event;
                                 resp : out Response_Type );

    procedure Handle_Respawn_Entities( this : not null access Tinker_Session'Class;
                                       resp : out Response_Type );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create return A_Session is
        this : constant A_Session := new Tinker_Session;
    begin
        this.Construct;
        this.cacheEnabled := False;
        return this;
    end Create;

    ----------------------------------------------------------------------------

    procedure Handle_Create_World( this : not null access Tinker_Session'Class;
                                   evt  : not null A_Create_World_Event;
                                   resp : out Response_Type ) is
        newWorld : A_World;
        player   : A_Entity;
    begin
        newWorld := Create_World( evt.Get_Width,
                                  evt.Get_Height,
                                  evt.Get_Tile_Width,
                                  evt.Get_Library_Name );
        if newWorld = null then
            -- the tile library was not found
            Set_Response( resp, ST_FAILED, Create( File_Error, "Library <" & evt.Get_Library_Name & "> not found" ) );
            return;
        end if;

        -- Layer 0: Middleground
        newWorld.Add_Layer( 0, Layer_Tiles, Create_Map( (Pair( PROP_TITLE    , Create( "Middleground" ) ),
                                                         Pair( PROP_DELETABLE, Create( False )          ),
                                                         Pair( PROP_SOLID    , Create( True  )          )) ).Map );

        -- Layer +1: Background
        newWorld.Add_Layer( 1, Layer_Tiles, Create_Map( (Pair( PROP_TITLE    , Create( "Background" ) ),
                                                         Pair( PROP_DELETABLE, Create( False )        ),
                                                         Pair( PROP_SOLID    , Create( True  )        )) ).Map );

        -- Layer -1: Foreground
        newWorld.Add_Layer( -1, Layer_Tiles, Create_Map( (Pair( PROP_TITLE    , Create( "Foreground" ) ),
                                                          Pair( PROP_DELETABLE, Create( False )        ),
                                                          Pair( PROP_SOLID    , Create( False )        )) ).Map );

        if evt.Get_Player_Template'Length > 0 then
            -- raises Constraint_Error if template not found
            player := Entities.Factory.Global.Create_Entity( evt.Get_Player_Template,
                                                             Create_Map( (1=>Pair("Location",
                                                                                  Create_Map( (Pair("x", Create( Long_Float(newWorld.Get_Width / 2) )),
                                                                                               Pair("y", Create( Long_Float(newWorld.Get_Height / 2) ))) ))) ).Map
                                                           );
            if player = null then
                Set_Response( resp, ST_FAILED, Create( Undefined_Name,
                                                       "Cannot create entity template <" &
                                                       evt.Get_Player_Template & ">" ) );
                Delete( newWorld );
                return;
            end if;
            newWorld.Set_Property( "player", To_Id_Value( player.Get_Id ) );
            newWorld.Add_Entity( player );
        end if;

        this.Load_World( "", newWorld );     -- consumes 'newWorld'
        this.worldController.Modified( True );
        Set_Response( resp, ST_SUCCESS );
    end Handle_Create_World;

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Event( this : access Tinker_Session;
                            evt  : in out A_Event;
                            resp : out Response_Type ) is
    begin
        case evt.Get_Event_Id is
            when EVT_SAVE_WORLD       => this.Handle_Save_World( A_Save_World_Event(evt), resp );
            when EVT_CREATE_WORLD     => this.Handle_Create_World( A_Create_World_Event(evt), resp );
            when EVT_IMPORT_WORLD     => this.Handle_Import_World( A_Import_World_Event(evt), resp );
            when EVT_RESIZE_WORLD     => this.Handle_Resize_World( A_Resize_World_Event(evt), resp );
            when EVT_RESPAWN_ENTITIES => this.Handle_Respawn_Entities( resp );
            when others               => Session(this.all).Handle_Event( evt, resp );
        end case;
    end Handle_Event;

    ----------------------------------------------------------------------------

    procedure Handle_Import_World( this : not null access Tinker_Session'Class;
                                   evt  : not null A_Import_World_Event;
                                   resp : out Response_Type ) is
        ext      : constant String := To_Lower( Get_Complete_Extension( evt.Get_Filename ) );
        imported : A_World;
    begin
        if ext = "png" or ext = "bmp" then
            -- may raise an exception
            imported := Import_World( evt.Get_Filename,
                                      evt.Get_Library_Name,
                                      evt.Get_Player_Template,
                                      evt.Get_Tolerance );
        elsif ext = "world.sv" then
            imported := Worlds.Scribble_Format.Import( evt.Get_Filename );
            if imported = null then
                raise Constraint_Error with "Import failed";
            end if;
        else
            raise Constraint_Error with "Unsupported file extension";
        end if;

        pragma Assert( imported /= null );
        this.Load_World( "", imported );  -- consumes 'imported'
        this.worldController.Modified( True );

        Set_Response( resp, ST_SUCCESS );
    exception
        when e : others =>
            Dbg( "Exception in Session.Handle_Import_World...", D_GAME or D_RES or D_WORLD, Error );
            Dbg( e, D_GAME or D_RES or D_WORLD, Error );
            Delete( imported );
            Set_Response( resp, ST_FAILED, Create( Generic_Exception, Exception_Message( e ) ) );
    end Handle_Import_World;

    ----------------------------------------------------------------------------

    procedure Handle_Resize_World( this : not null access Tinker_Session'Class;
                                   evt  : not null A_Resize_World_Event;
                                   resp : out Response_Type ) is
    begin
        if this.Get_World = null then
            Set_Response( resp, ST_FAILED, Create( Illegal_Operation, "No world to resize" ) );
        end if;

        -- may raise an exception
        this.Get_World.Resize( evt.Get_X, evt.Get_Y, evt.Get_Width, evt.Get_Height );

        Set_Response( resp, ST_SUCCESS );
    exception
        when e : others =>
            Dbg( "Exception in Session.Handle_Resize_World...", D_GAME or D_ENTITY, Error );
            Dbg( e, D_GAME or D_ENTITY, Error );
            Set_Response( resp, ST_FAILED, Create( Generic_Exception, Exception_Message( e ) ) );
    end Handle_Resize_World;

    ----------------------------------------------------------------------------

    procedure Handle_Save_World( this : not null access Tinker_Session'Class;
                                 evt  : not null A_Save_World_Event;
                                 resp : out Response_Type ) is
        origFilepath : Value;
    begin
        if this.Get_World = null then
            Set_Response( resp, ST_FAILED, Create( Illegal_Operation, "No world to save" ) );
            return;
        end if;

        origFilepath := this.Get_World.Get_Property( "filepath" );

        if evt.Get_Format = "sv" then
            if not Worlds.Scribble_Format.Export( this.Get_World, evt.Get_Filename ) then
                raise WRITE_EXCEPTION with "File export failed";
            end if;
        else
            -- may raise exception
            this.Get_World.Save( evt.Get_Filename, overwrite => True );
        end if;

        if not evt.Is_Temporary then
            -- mark the world as unmodified after a successful save.
            this.worldController.Modified( False );
        else
            -- restore the original filename and don't mark the world as
            -- unmodified because it was saved to a temporary location.
            this.Get_World.Set_Property( "filepath", origFilepath );
            this.Get_World.Set_Property( "filename", Create( Get_Filename( Cast_String( origFilepath ) ) ) );
        end if;

        Set_Response( resp, ST_SUCCESS );
    exception
        when e : others =>
            -- exception writing the world to disk
            Set_Response( resp, ST_FAILED, Create( Generic_Exception, Exception_Message( e ) ) );
            if not evt.Is_Temporary then
                Queue_Error_Message( "Error Saving World",
                                     "Failed to write " & evt.Get_Filename,
                                     Exception_Message( e ) );
            end if;
    end Handle_Save_World;

    ----------------------------------------------------------------------------

    procedure Handle_Respawn_Entities( this : not null access Tinker_Session'Class;
                                       resp : out Response_Type ) is
        package Entity_Lists is new Ada.Containers.Doubly_Linked_Lists( A_Entity, "=" );

        ents : Entity_Lists.List;
        e2   : A_Entity;
    begin
        if this.Get_World = null then
            Set_Response( resp, ST_FAILED, Create( Illegal_Operation, "No world loaded" ) );
            return;
        end if;

        -- Respawn all entities in the world, preserving only their locations,
        -- ids, and attributes. This is useful during development for updating
        -- existing entities with the latest entity templates or Scribble code.

        for e of this.Get_World.Get_Entities.all loop
            ents.Append( Entities.Factory.Global.Copy_Entity( e ) );
        end loop;

        for e of ents loop
            e2 := e;
            this.game.Get_Entity_System.Delete_Entity( e.Get_Id );
            e.Set_System( this.game.Get_Entity_System );
            this.Get_World.Add_Entity( e );
            e2.Dispatch_Message( MSG_Spawned );
        end loop;

        Set_Response( resp, ST_SUCCESS );
    exception
        when e : others =>
            Dbg( "Exception in Session.Handle_Respawn_Entities...", D_GAME or D_ENTITY, Error );
            Dbg( e, D_GAME or D_ENTITY, Error );
            Set_Response( resp, ST_FAILED, Create( Generic_Exception, Exception_Message( e ) ) );
    end Handle_Respawn_Entities;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Initialize( this : access Tinker_Session ) is
    begin
        this.isEditor := True;

        -- register to listen for events
        this.game.Get_Corral.Add_Listener( this, EVT_CREATE_WORLD );
        this.game.Get_Corral.Add_Listener( this, EVT_IMPORT_WORLD );
        this.game.Get_Corral.Add_Listener( this, EVT_RESIZE_WORLD );
        this.game.Get_Corral.Add_Listener( this, EVT_SAVE_WORLD );
        this.game.Get_Corral.Add_Listener( this, EVT_RESPAWN_ENTITIES );
    end On_Initialize;

    ----------------------------------------------------------------------------

    overriding
    procedure Request_Restart( this : access Tinker_Session ) is
    begin
        this.Restart;

        -- load a filename passed on the command line or the default world
        for i in 1..Argument_Count loop
             declare
                 mapName : constant String := Argument( i );
             begin
                 if mapName(mapName'First) /= '-' then
                     this.Load_World( mapName );          -- may raise exception
                     return;
                 end if;
             exception
                 when e : others =>
                     Dbg( "Failed to load world <" & mapName & ">: " &
                          Exception_Message( e ),
                          D_GAME or D_RES or D_WORLD, Error );
             end;
        end loop;

        -- only load "default.world" when it exists
        if Resource_Exists( "default", "worlds" ) then
            this.Load_World( "default" );                 -- may raise exception
        end if;
    exception
        when others =>
            -- failed to load the default world; carry on
            null;
    end Request_Restart;

end Sessions.Tinker;

