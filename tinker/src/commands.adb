--
-- Copyright (c) 2015-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
--with Ada.Tags;                          use Ada.Tags;
--with Debugging;                         use Debugging;
with Events.Entities;                   use Events.Entities;
with Events.Entities.Attributes;        use Events.Entities.Attributes;
with Events.Entities.Locations;         use Events.Entities.Locations;
with Events.World;                      use Events.World;
with Game_Views.Tinker;                 use Game_Views.Tinker;
with Hashed_Strings;                    use Hashed_Strings;
with Values.Construction;               use Values.Construction;
with Values.Strings;

package body Commands is

    procedure Delete( this : in out A_Command ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

    --==========================================================================

    procedure Add( this : in out Compound_Command'Class; cmd : access Command'Class ) is
    begin
        if cmd /= null then
            this.group.Prepend( A_Command(cmd) );
        end if;
    end Add;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Compound_Command ) is
        cmd : A_Command;
    begin
        while not this.group.Is_Empty loop
            cmd := this.group.First_Element;
            Delete( cmd );
            this.group.Delete_First;
        end loop;
        Command(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    overriding
    procedure Execute( this : in out Compound_Command ) is
    begin
        -- this command executed first
        for c of reverse this.group loop
            c.Execute;
        end loop;
    end Execute;

    ----------------------------------------------------------------------------

    function Is_Empty( this : Compound_Command'Class ) return Boolean is (this.group.Is_Empty);

    ----------------------------------------------------------------------------

    overriding
    procedure Undo( this : Compound_Command ) is
    begin
        for c of this.group loop
            c.Undo;
        end loop;
        -- this command undone last
    end Undo;

    ----------------------------------------------------------------------------

    function Create_Compound_Command return A_Compound_Command is (new Compound_Command);

    --==========================================================================

    type Connect_Command is new Command with
        record
            eidA, eidB : Entity_Id;
            connect    : Boolean := True;
        end record;

    procedure Execute( this : in out Connect_Command );
    procedure Undo( this : Connect_Command );

    ----------------------------------------------------------------------------

    overriding
    procedure Execute( this : in out Connect_Command ) is
    begin
        Queue_Message_Entity( this.eidA,
                              (if this.connect then To_Hashed_String( "Connect" ) else To_Hashed_String( "Disconnect" )),
                              Create( (1=>Pair("eid", To_Id_Value( this.eidB ))) ).Map );
    end Execute;

    ----------------------------------------------------------------------------

    overriding
    procedure Undo( this : Connect_Command ) is
    begin
        Queue_Message_Entity( this.eidA,
                              (if this.connect then To_Hashed_String( "Disconnect" ) else To_Hashed_String( "Connect" )),
                              Create( (1=>Pair("eid", To_Id_Value( this.eidB ))) ).Map );
    end Undo;

    ----------------------------------------------------------------------------

    function Create_Command_Connect( eidA, eidB : Entity_Id;
                                     connect    : Boolean ) return A_Command is
    begin
        return new Connect_Command'(Command with
                                    eidA => eidA,
                                    eidB => eidB,
                                    connect => connect);
    end Create_Command_Connect;

    --==========================================================================

    -- This is extended by Create_Layer_Command and Delete_Layer_Command
    type Layer_Command is abstract new Command with
        record
            layer     : Integer;
            layerType : Layer_Type;
            layerData : A_Map;
            props     : Map_Value;
        end record;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out Layer_Command ) is
    begin
        Delete( this.layerData );
        Command(this).Delete;
    end Delete;

    --==========================================================================

    type Create_Layer_Command is new Layer_Command with null record;

    procedure Execute( this : in out Create_Layer_Command );
    procedure Undo( this : Create_Layer_Command );

    ----------------------------------------------------------------------------

    overriding
    procedure Execute( this : in out Create_Layer_Command ) is
    begin
        Queue_Create_Layer( this.layer, this.layerType, this.layerData, this.props );
    end Execute;

    ----------------------------------------------------------------------------

    overriding
    procedure Undo( this : Create_Layer_Command ) is
    begin
        -- When deleting the layer created before background (positive) layer N, we delete N.
        -- When deleting the layer created before foreground (negative) layer M, we delete M-1.
        Queue_Delete_Layer( (if this.layer <= 0 then this.layer - 1 else this.layer) );
    end Undo;

    ----------------------------------------------------------------------------

    function Create_Command_Create_Layer( beforeLayer : Integer;
                                          layerType   : Layer_Type;
                                          layerData   : A_Map;
                                          props       : Map_Value ) return A_Command is
    begin
        return new Create_Layer_Command'(Command with
                                         layer => beforeLayer,
                                         layerType => layerType,
                                         layerData => Copy( layerData ),
                                         props => Clone( props ).Map);
    end Create_Command_Create_Layer;

    --==========================================================================

    -- Deletes an existing entity
    type Delete_Entity_Command is new Command with
        record
            eid        : Entity_Id := NULL_ID;
            name       : Unbounded_String;
            template   : Unbounded_String;
            x, y       : Float := 0.0;
            w, h       : Integer := 0;
            attributes : Map_Value;
        end record;

    procedure Execute( this : in out Delete_Entity_Command );
    procedure Undo( this : Delete_Entity_Command );

    ----------------------------------------------------------------------------

    overriding
    procedure Execute( this : in out Delete_Entity_Command ) is
    begin
        Queue_Delete_Entity( this.eid );
    end Execute;

    ----------------------------------------------------------------------------

    overriding
    procedure Undo( this : Delete_Entity_Command ) is
        procedure Examine( name : String; val : Value ) is
        begin
            Queue_Set_Entity_Attribute( this.eid, name, val );
        end Examine;
    begin
        Queue_Spawn_Entity( To_String( this.template ),
                            Create( (Pair( "id", To_Id_Value( this.eid ) ),
                                     Pair( "name", Strings.Create( this.name ) ),
                                     Pair( "Location",
                                           Create( (Pair( "x", Create( this.x ) ),
                                                    Pair( "y", Create( this.y ) ),
                                                    Pair( "width", Create( this.w ) ),
                                                    Pair( "height", Create( this.h ) )) )
                                         ))
                                  ).Map
                          );
        this.attributes.Iterate( Examine'Access );
    end Undo;

    ----------------------------------------------------------------------------

    function Create_Command_Delete_Entity( entity : not null A_Entity ) return A_Command is
    begin
        return new Delete_Entity_Command'(Command with
                                          eid        => entity.Get_Entity_Id,
                                          name       => To_Unbounded_String( entity.Get_Entity_Name ),
                                          template   => To_Unbounded_String( entity.Get_Template ),
                                          x          => entity.Get_Entity_X,
                                          y          => entity.Get_Entity_Y,
                                          w          => Integer(entity.Get_Entity_Width),
                                          h          => Integer(entity.Get_Entity_Height),
                                          attributes => Clone( entity.Get_Attributes ).Map);
    end Create_Command_Delete_Entity;

    --==========================================================================

    type Delete_Layer_Command is new Layer_Command with null record;

    procedure Execute( this : in out Delete_Layer_Command );
    procedure Undo( this : Delete_Layer_Command );

    ----------------------------------------------------------------------------

    overriding
    procedure Execute( this : in out Delete_Layer_Command ) is
    begin
        Queue_Delete_Layer( this.layer );
    end Execute;

    ----------------------------------------------------------------------------

    overriding
    procedure Undo( this : Delete_Layer_Command ) is
    begin
        -- When re-creating background (positive) layer N, we insert before N.
        -- When re-creating foreground (negative) layer M, we insert before M+1.
        Queue_Create_Layer( (if this.layer < 0 then this.layer + 1 else this.layer), this.layerType, this.layerData, this.props );
    end Undo;

    ----------------------------------------------------------------------------

    function Create_Command_Delete_Layer( layer     : Integer;
                                          layerType : Layer_Type;
                                          layerData : not null A_Map;
                                          props     : Map_Value ) return A_Command is
    begin
        return new Delete_Layer_Command'(Command with
                                         layer => layer,
                                         layerType => layerType,
                                         layerData => layerData,   -- consumed!
                                         props => Clone( props ).Map);
    end Create_Command_Delete_Layer;

    --==========================================================================

    -- Sends a directive to an entity
    type Entity_Directive_Command is new Command with
        record
            eid       : Entity_Id := NULL_ID;
            directive : Directive_Bits := NO_DIRECTIVES;
        end record;

    procedure Execute( this : in out Entity_Directive_Command );
    procedure Undo( this : Entity_Directive_Command );

    ----------------------------------------------------------------------------

    overriding
    procedure Execute( this : in out Entity_Directive_Command ) is
    begin
        Queue_Entity_Directive( this.eid, this.directive, Once );
    end Execute;

    ----------------------------------------------------------------------------

    overriding
    procedure Undo( this : Entity_Directive_Command ) is null;

    ----------------------------------------------------------------------------

    function Create_Command_Entity_Directive( eid       : Entity_Id;
                                              directive : Directive_Bits ) return A_Command is
    begin
        return new Entity_Directive_Command'(Command with
                                             eid => eid,
                                             directive => directive);
    end Create_Command_Entity_Directive;

    --==========================================================================

    -- Sets the value of an entity's attribute
    type Select_Tiles_Command is new Compound_Command with
        record
            oldArea : Rectangle;
            newArea : Rectangle;
        end record;

    procedure Combine( this : in out Select_Tiles_Command; cmd : in out A_Command );
    procedure Execute( this : in out Select_Tiles_Command );
    procedure Undo( this : Select_Tiles_Command );

    ----------------------------------------------------------------------------

    overriding
    procedure Combine( this : in out Select_Tiles_Command; cmd : in out A_Command ) is
    begin
        this.Add( cmd );
        cmd := null;
    end Combine;

    ----------------------------------------------------------------------------

    overriding
    procedure Execute( this : in out Select_Tiles_Command ) is
    begin
        Get_View.Get_Scene.Tile_Area_Set( this.newArea );
        Compound_Command(this).Execute;
    end Execute;

    ----------------------------------------------------------------------------

    overriding
    procedure Undo( this : Select_Tiles_Command ) is
    begin
        Compound_Command(this).Undo;
        Get_View.Get_Scene.Tile_Area_Set( this.oldArea );
    end Undo;

    ----------------------------------------------------------------------------

    function Create_Command_Select_Tiles( oldArea, newArea : Rectangle ) return A_Command is
    begin
        return new Select_Tiles_Command'(Compound_Command with
                                         newArea => (if newArea.width * newArea.height > 0.0 then newArea else (others => 0.0)),
                                         oldArea => (if oldArea.width * oldArea.height > 0.0 then oldArea else (others => 0.0)));
    end Create_Command_Select_Tiles;

    --==========================================================================

    -- Sets the value of an entity's attribute
    type Set_Entity_Attribute_Command is new Command with
        record
            eid    : Entity_Id := NULL_ID;
            name   : Unbounded_String;
            oldVal : Value;
            newVal : Value;
        end record;

    procedure Execute( this : in out Set_Entity_Attribute_Command );
    procedure Undo( this : Set_Entity_Attribute_Command );

    ----------------------------------------------------------------------------

    overriding
    procedure Execute( this : in out Set_Entity_Attribute_Command ) is
    begin
        Queue_Set_Entity_Attribute( this.eid, To_String( this.name ), this.newVal );
    end Execute;

    ----------------------------------------------------------------------------

    overriding
    procedure Undo( this : Set_Entity_Attribute_Command ) is
    begin
        Queue_Set_Entity_Attribute( this.eid, To_String( this.name ), this.oldVal );
    end Undo;

    ----------------------------------------------------------------------------

    function Create_Command_Set_Entity_Attribute( eid    : Entity_Id;
                                                  name   : String;
                                                  oldVal : Value'Class;
                                                  newVal : Value'Class ) return A_Command is
    begin
        if oldVal = newVal then
            return null;
        end if;
        return new Set_Entity_Attribute_Command'(Command with
                                                 eid => eid,
                                                 name => To_Unbounded_String( name ),
                                                 oldVal => Clone( oldVal ),
                                                 newVal => Clone( newVal ));
    end Create_Command_Set_Entity_Attribute;

    --==========================================================================

    -- Moves and/or resizes an entity
    type Move_Entity_Command is new Command with
        record
            eid        : Entity_Id := NULL_ID;
            oldX, oldY : Float := -1.0;
            newX, newY : Float := -1.0;
            oldW, oldH : Integer := -1;
            newW, newH : Integer := -1;
        end record;

    procedure Execute( this : in out Move_Entity_Command );
    procedure Undo( this : Move_Entity_Command );

    ----------------------------------------------------------------------------

    overriding
    procedure Execute( this : in out Move_Entity_Command ) is
    begin
        if this.newW /= this.oldW or else this.newH /= this.oldH then
            Queue_Resize_Entity( this.eid, this.newW, this.newH );
        end if;
        if this.newX /= this.oldX or else this.newY /= this.oldY then
            Queue_Move_Entity( this.eid, this.newX, this.newY );
        end if;
    end Execute;

    ----------------------------------------------------------------------------

    overriding
    procedure Undo( this : Move_Entity_Command ) is
    begin
        if this.newX /= this.oldX or else this.newY /= this.oldY then
            Queue_Move_Entity( this.eid, this.oldX, this.oldY );
        end if;
        if this.newW /= this.oldW or else this.newH /= this.oldH then
            Queue_Resize_Entity( this.eid, this.oldW, this.oldH );
        end if;
    end Undo;

    ----------------------------------------------------------------------------

    function Create_Command_Move_Entity( eid        : Entity_Id;
                                         oldX, oldY : Float;
                                         newX, newY : Float ) return A_Command is
    begin
        if newX = oldX and then newY = oldY then
            return null;
        end if;
        return new Move_Entity_Command'(Command with
                                        eid => eid,
                                        oldX => oldX, oldY => oldY,
                                        newX => newX, newY => newY,
                                        others => <>);
    end Create_Command_Move_Entity;

    ----------------------------------------------------------------------------

    function Create_Command_Resize_Entity( eid        : Entity_Id;
                                           oldW, oldH : Float;
                                           newW, newH : Float;
                                           oldX, oldY : Float := -1.0;
                                           newX, newY : Float := -1.0 ) return A_Command is
    begin
        if newW = oldW and then newH = oldH then
            return null;
        end if;
        return new Move_Entity_Command'(Command with
                                        eid => eid,
                                        oldX => oldX, oldY => oldY,
                                        oldW => Integer(oldW), oldH => Integer(oldH),
                                        newX => newX, newY => newY,
                                        newW => Integer(newW), newH => Integer(newH));
    end Create_Command_Resize_Entity;

    --==========================================================================

    -- Sets a property of a map layer in the active world
    type Set_Layer_Property_Command is new Command with
        record
            layer  : Integer;
            name   : Unbounded_String;
            oldVal : Value;
            newVal : Value;
        end record;

    procedure Execute( this : in out Set_Layer_Property_Command );
    procedure Undo( this : Set_Layer_Property_Command );

    ----------------------------------------------------------------------------

    overriding
    procedure Execute( this : in out Set_Layer_Property_Command ) is
    begin
        Queue_Set_Layer_Property( this.layer, To_String( this.name ), this.newVal );
    end Execute;

    ----------------------------------------------------------------------------

    overriding
    procedure Undo( this : Set_Layer_Property_Command ) is
    begin
        Queue_Set_Layer_Property( this.layer, To_String( this.name ), this.oldVal );
    end Undo;

    ----------------------------------------------------------------------------

    function Create_Command_Set_Layer_Property( layer  : Integer;
                                                name   : String;
                                                oldVal : Value'Class;
                                                newVal : Value'Class ) return A_Command is
    begin
        if oldVal = newVal then
            return null;
        end if;
        return new Set_Layer_Property_Command'(Command with
                                               layer => layer,
                                               name => To_Unbounded_String( name ),
                                               oldVal => Clone( oldVal ),
                                               newVal => Clone( newVal ));
    end Create_Command_Set_Layer_Property;

    --==========================================================================

    function Create_Command_Set_Tiles return A_Set_Tiles_Command is (new Set_Tiles_Command'(Compound_Command with others => <>));

    ----------------------------------------------------------------------------

    function Create_Command_Set_Tile( layer, col, row : Integer;
                                      oldTileId       : Natural;
                                      newTileId       : Natural ) return A_Set_Tiles_Command is
        cmd : A_Set_Tiles_Command := null;
    begin
        if newTileId /= oldTileId then
            cmd := new Set_Tiles_Command'(Compound_Command with others => <>);
            cmd.Add( layer, col, row, oldTileId, newTileId );
        end if;
        return cmd;
    end Create_Command_Set_Tile;

    ----------------------------------------------------------------------------

    procedure Add( this            : in out Set_Tiles_Command'Class;
                   layer, col, row : Integer;
                   oldTileId       : Natural;
                   newTileId       : Natural ) is
    begin
        if newTileId /= oldTileId then
            this.tiles.Append( Tile_Change'(layer, col, row, newTileId) );
            this.oldIds.Prepend( oldTileId );     -- stored in reverse order for Undo
        end if;
    end Add;

    ----------------------------------------------------------------------------

    overriding
    procedure Combine( this : in out Set_Tiles_Command; cmd : in out A_Command ) is
    begin
        this.Add( cmd );
        cmd := null;
    end Combine;

    ----------------------------------------------------------------------------

    overriding
    procedure Execute( this : in out Set_Tiles_Command ) is
    begin
        Queue_Set_Tiles( this.tiles );
        Compound_Command(this).Execute;
    end Execute;

    ----------------------------------------------------------------------------

    function Tile_Count( this : Set_Tiles_Command ) return Natural is (Integer(this.tiles.Length));

    ----------------------------------------------------------------------------

    overriding
    procedure Undo( this : Set_Tiles_Command ) is
        tiles : Tile_Change_Lists.Vector;
    begin
        Compound_Command(this).Undo;

        pragma Assert( Integer(this.tiles.Length) = Integer(this.oldIds.Length) );

        -- 'tiles' has been reversed because Undo must do everything in the
        -- reverse order. oldIds is stored in reverse order already because it
        -- is only used by Undo().
        tiles := this.tiles.Copy;
        tiles.Reverse_Elements;

        for i in 1..Integer(tiles.Length) loop
            tiles.Reference( i ).id := this.oldIds(i);
        end loop;

        Queue_Set_Tiles( tiles );
    end Undo;

    --==========================================================================

    -- Sets a property of the active world
    type Set_World_Property_Command is new Command with
        record
            name   : Unbounded_String;
            oldVal : Value;
            newVal : Value;
        end record;

    procedure Execute( this : in out Set_World_Property_Command );
    procedure Undo( this : Set_World_Property_Command );

    ----------------------------------------------------------------------------

    overriding
    procedure Execute( this : in out Set_World_Property_Command ) is
    begin
        Queue_Set_World_Property( To_String( this.name ), this.newVal );
    end Execute;

    ----------------------------------------------------------------------------

    overriding
    procedure Undo( this : Set_World_Property_Command ) is
    begin
        Queue_Set_World_Property( To_String( this.name ), this.oldVal );
    end Undo;

    ----------------------------------------------------------------------------

    function Create_Command_Set_World_Property( name   : String;
                                                oldVal : Value'Class;
                                                newVal : Value'Class ) return A_Command is
    begin
        if oldVal = newVal then
            return null;
        end if;
        return new Set_World_Property_Command'(Command with
                                                name => To_Unbounded_String( name ),
                                                oldVal => Clone( oldVal ),
                                                newVal => Clone( newVal ));
    end Create_Command_Set_World_Property;

    --==========================================================================

    -- Spawns a new entity
    type Spawn_Entity_Command is new Command with
        record
            template   : Unbounded_String;
            properties : Map_Value;
            attributes : Map_Value;
        end record;

    procedure Execute( this : in out Spawn_Entity_Command );
    procedure Undo( this : Spawn_Entity_Command );

    ----------------------------------------------------------------------------

    overriding
    procedure Execute( this : in out Spawn_Entity_Command ) is
        eid : constant Entity_Id := To_Tagged_Id( this.properties.Get( "id" ) );

        procedure Examine( name : String; val : Value ) is
        begin
            Queue_Set_Entity_Attribute( eid, name, val );
        end Examine;
    begin
        Queue_Spawn_Entity( To_String( this.template ), this.properties );
        if this.attributes.Valid then
            this.attributes.Iterate( Examine'Access );
        end if;
    end Execute;

    ----------------------------------------------------------------------------

    overriding
    procedure Undo( this : Spawn_Entity_Command ) is
    begin
        Queue_Delete_Entity( To_Tagged_Id( this.properties.Get( "id" ) ) );
    end Undo;

    ----------------------------------------------------------------------------

    function Create_Command_Spawn_Entity( template   : String;
                                          x, y       : Float;
                                          properties : Map_Value ) return A_Command is
        mergedProps : Map_Value;
    begin
        mergedProps := Create( (Pair( "id", To_Id_Value( Unique_Tagged_Id( Entity_Tag ) ) ),
                                Pair( "name", Strings.Create( template & "1" ) ),
                                Pair( "Location", Create( (Pair( "x", Create( x ) ),
                                                           Pair( "y", Create( y ) )),
                                                          consume => True ) )),
                               consume => True ).Map;
        mergedProps.Recursive_Merge( properties );
        return new Spawn_Entity_Command'(Command with
                                         template   => To_Unbounded_String( template ),
                                         properties => mergedProps,
                                         attributes => Null_Value.Map);
    end Create_Command_Spawn_Entity;

    ----------------------------------------------------------------------------

    function Create_Command_Spawn_Entity( template      : String;
                                          name          : String;
                                          x, y          : Float;
                                          width, height : Float;
                                          attributes    : Map_Value := Null_Value.Map;
                                          entityId      : Entity_Id := NULL_ID ) return A_Command is
        properties : Map_Value;
    begin
        properties := Create( (Pair( "id", To_Id_Value( (if entityId /= NULL_ID then entityId else Unique_Tagged_Id( Entity_Tag )) ) ),
                               Pair( "name", Strings.Create( name ) ),
                               Pair( "Location",
                                     Create( (Pair( "x", Create( x ) ),
                                              Pair( "y", Create( y ) ),
                                              Pair( "width", Create( width ) ),
                                              Pair( "height", Create( height ) )) )
                                   ))
                            ).Map;
        return new Spawn_Entity_Command'(Command with
                                         template   => To_Unbounded_String( template ),
                                         properties => properties,
                                         attributes => Clone( attributes ).Map);
    end Create_Command_Spawn_Entity;

    --==========================================================================

    -- Swaps two map layers
    type Swap_Layers_Command is new Command with
        record
            layer      : Integer := 0;
            otherLayer : Integer := 0;
        end record;

    procedure Execute( this : in out Swap_Layers_Command );
    procedure Undo( this : Swap_Layers_Command );

    ----------------------------------------------------------------------------

    overriding
    procedure Execute( this : in out Swap_Layers_Command ) is
    begin
        Queue_Swap_Layers( this.layer, this.otherLayer );
    end Execute;

    ----------------------------------------------------------------------------

    overriding
    procedure Undo( this : Swap_Layers_Command ) is
    begin
        Queue_Swap_Layers( this.otherLayer, this.layer );
    end Undo;

    ----------------------------------------------------------------------------

    function Create_Command_Swap_Layers( layer, otherLayer : Integer ) return A_Command is
    begin
        return new Swap_Layers_Command'(Command with
                                        layer => layer,
                                        otherLayer => otherLayer);
    end Create_Command_Swap_Layers;

end Commands;
