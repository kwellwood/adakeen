--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Applications.Gui.Games.Tinker is

    function Create return A_Application;

private

    type Tinker_Application is new Game_Application with null record;

    procedure On_Initialize( this : access Tinker_Application );

end Applications.Gui.Games.Tinker;
