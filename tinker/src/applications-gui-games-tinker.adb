--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Debugging;                         use Debugging;
with Entities.Factory;
with Entities.Galaxy;
with Game_Views.Tinker;
with Galaxy_API;
with Preferences;                       use Preferences;
with Scribble.VMs;                      use Scribble.VMs;
with Sessions.Tinker;
with Support.Paths;                     use Support.Paths;
with Values.Lists;                      use Values.Lists;

package body Applications.Gui.Games.Tinker is

    USER_DIR    : constant String := Home_Directory & "Documents" & Slash & "Keen Galaxy" & Slash;
    MEDIA_DIR   : constant String := App_Data_Directory & "Keen Galaxy" & Slash & "media" & Slash;
    CREDITS_DIR : constant String := "credits" & Slash;

    ----------------------------------------------------------------------------

    procedure Scribble_Debug( str : String ) is
    begin
        Dbg( "Scribble: " & str, D_SCRIPT, Error );
    end Scribble_Debug;

    ----------------------------------------------------------------------------

    procedure Scribble_Output( str : String ) is
    begin
        Dbg( str );
    end Scribble_Output;

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create return A_Application is
        this : constant A_Game_Application := new Tinker_Application;
    begin
        this.Construct( company => "Kevin Wellwood",
                        name    => "tinker",
                        userDir => USER_DIR );

        this.Set_View( Game_Views.Tinker.Create );
        this.Set_Session( Sessions.Tinker.Create );

        this.defaultWidth    := 1024;
        this.defaultHeight   := 640;
        this.resizableWindow := True;
        this.useMouse        := True;

        return A_Application(this);
    end Create;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Initialize( this : access Tinker_Application ) is
    begin
        -- Register additional Scribble functions supported by Keen Galaxy
        Galaxy_API.Initialize( this.game.Get_Script_VM );

        -- Define all entity components supported by Keen Galaxy
        Entities.Galaxy.Define_Components( Entities.Factory.Global );

        pragma Debug( Dbg( "Compiling component definitions...", D_ENTITY, Info ) );
        Entities.Factory.Global.Load_Component_Definitions;

        pragma Debug( Dbg( "Compiling entity templates...", D_ENTITY, Info ) );
        Entities.Factory.Global.Load_Entity_Templates;
    end On_Initialize;

begin

    Scribble.VMs.Set_Output( Scribble_Output'Access );
    Scribble.VMs.Set_Debug_Output( Scribble_Debug'Access );

    Preferences.Set_Default( "application", "media"  , MEDIA_DIR );
    Preferences.Set_Default( "application", "license", CREDITS_DIR );
    Preferences.Set_Default( "application", "openDir", USER_DIR );
    Preferences.Set_Default( "application", "saveDir", USER_DIR );

end Applications.Gui.Games.Tinker;
