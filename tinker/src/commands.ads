--
-- Copyright (c) 2015-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Doubly_Linked_Lists;
with Directives;                        use Directives;
with Object_Ids;                        use Object_Ids;
with Objects;                           use Objects;
with Maps;                              use Maps;
with Scene_Entities;                    use Scene_Entities;
with Tiles;                             use Tiles;
with Values;                            use Values;
with Values.Maps;                       use Values.Maps;
with Values.Tagged_Ids;                 use Values.Tagged_Ids;
with Vector_Math;                       use Vector_Math;
with Worlds;                            use Worlds;

package Commands is

    type Command is abstract new Limited_Object with null record;
    type A_Command is access all Command'Class;

    -- Executes the command, causing a change in the game state.
    not overriding
    procedure Execute( this : in out Command ) is abstract;

    -- Combines 'cmd' into 'this', if possible. 'cmd' was executed after 'this'.
    not overriding
    procedure Combine( this : in out Command; cmd : in out A_Command ) is null;

    -- Performs the opposite command, undoing whatever change Execute() made to
    -- the game state.
    not overriding
    procedure Undo( this : Command ) is abstract;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Deletes the Command.
    procedure Delete( this : in out A_Command );

    ----------------------------------------------------------------------------

    type Compound_Command is abstract new Command with private;
    type A_Compound_Command is access all Compound_Command'Class;

    -- Adds 'cmd' to the command, executed in order of addition. 'cmd' will be
    -- consumed even though the pointer is not modified.
    procedure Add( this : in out Compound_Command'Class; cmd : access Command'Class );

    -- Returns False if any commands have been added to the compound command.
    function Is_Empty( this : Compound_Command'Class ) return Boolean;

    -- Creates a compound Command object, encapsulating multiple commands as a
    -- single undoable action.
    function Create_Compound_Command return A_Compound_Command;

    ----------------------------------------------------------------------------

    type Set_Tiles_Command is new Compound_Command with private;
    type A_Set_Tiles_Command is access all Set_Tiles_Command'Class;

    -- Adds a tile to be set from 'oldTile' to 'newTileId' at 'col','row' in map
    -- layer 'layer. If 'oldTileId' = 'newTileId' then the change is ignored.
    procedure Add( this            : in out Set_Tiles_Command'Class;
                   layer, col, row : Integer;
                   oldTileId       : Natural;
                   newTileId       : Natural );

    -- Creates a Command object that sets tiles in the map. Build the tile list
    -- incrementally by adding tiles to set.
    function Create_Command_Set_Tiles return A_Set_Tiles_Command;

    -- Creates a Command object that sets tiles in the map. This version of the
    -- constructor accepts the first tile to set so that .Add() isn't needed for
    -- single tile commands. The tile at 'col','row' in layer 'layer' will be
    -- changed from 'oldTileId' to 'newTileId'.
    --
    -- NOTE: For backwards compatibility, if 'oldTileId' = 'newTileId' then null
    -- will be returned instead of an empty command created.
    function Create_Command_Set_Tile( layer, col, row : Integer;
                                      oldTileId       : Natural;
                                      newTileId       : Natural ) return A_Set_Tiles_Command;

    -- Returns the number of tiles affected by the command.
    function Tile_Count( this : Set_Tiles_Command ) return Natural;

    ----------------------------------------------------------------------------

    -- Creates a Command object that connects or disconnects entity 'eid1' to
    -- 'eid2'.
    function Create_Command_Connect( eidA, eidB : Entity_Id;
                                     connect    : Boolean ) return A_Command;

    -- Creates a Command object that creates a new layer in the foreground of
    -- layer index 'beforeLayer'. The layer is of type 'layerType'. If
    -- 'layerData' is not null, then the new layer will contain the layer data
    -- of layer 0 in the map (it will be copied). 'props' contains the layer
    -- properties for the new layer (it will be copied).
    function Create_Command_Create_Layer( beforeLayer : Integer;
                                          layerType   : Layer_Type;
                                          layerData   : A_Map;
                                          props       : Map_Value ) return A_Command;

    -- Creates a Command object that deletes the game entity corresponding with
    -- scene entity 'entity'. The entity, post-undo, may not be in exactly the
    -- same state as before the delete, but enough information is preserved to
    -- be useful within the world editor.
    function Create_Command_Delete_Entity( entity : not null A_Entity ) return A_Command;

    -- Creates a Command object that deletes layer 'layer'. The extra arguments
    -- describe the current layer so the operation can be undone. 'props' will
    -- be copied. 'layerData' will be CONSUMED. The caller releases ownership.
    function Create_Command_Delete_Layer( layer     : Integer;
                                          layerType : Layer_Type;
                                          layerData : not null A_Map;
                                          props     : Map_Value ) return A_Command;
    pragma Precondition( props.Valid );

    -- Creates a Command object that sends 'directive' to entity 'eid'. The
    -- command can't be undone, due to the nature of the command, but it can be
    -- useful to put the command in the undo list for the purposes of faithfully
    -- re-executing a series of commands.
    function Create_Command_Entity_Directive( eid       : Entity_Id;
                                              directive : Directive_Bits ) return A_Command;

    -- Creates a Command object that moves entity 'eid'.
    function Create_Command_Move_Entity( eid        : Entity_Id;
                                         oldX, oldY : Float;
                                         newX, newY : Float ) return A_Command;

    -- Creates a Command object that resizes and optionally moves entity 'eid'.
    function Create_Command_Resize_Entity( eid        : Entity_Id;
                                           oldW, oldH : Float;
                                           newW, newH : Float;
                                           oldX, oldY : Float := -1.0;
                                           newX, newY : Float := -1.0 ) return A_Command;

    -- Creates a Command object that selects or unselects an area of tiles. Pass
    -- a Rectangle with 0 size in 'newArea' to unselect an area. 'oldArea' is
    -- the previous tile selection area (size of 0 if no previous selection).
    function Create_Command_Select_Tiles( oldArea, newArea : Rectangle ) return A_Command;

    -- Creates a Command object that sets attribute 'name' of entity 'eid'.
    function Create_Command_Set_Entity_Attribute( eid    : Entity_Id;
                                                  name   : String;
                                                  oldVal : Value'Class;
                                                  newVal : Value'Class ) return A_Command;

    -- Creates a Command object that sets the 'name' property of world map layer
    -- 'layer'.
    function Create_Command_Set_Layer_Property( layer  : Integer;
                                                name   : String;
                                                oldVal : Value'Class;
                                                newVal : Value'Class ) return A_Command;

    -- Creates a Command object that sets the 'name' property of the world.
    function Create_Command_Set_World_Property( name   : String;
                                                oldVal : Value'Class;
                                                newVal : Value'Class ) return A_Command;

    -- Creates a Command object that spawns an instance of 'template' at 'x, y'
    -- using spawn properties 'properties' to parameterize the template's
    -- components.
    function Create_Command_Spawn_Entity( template   : String;
                                          x, y       : Float;
                                          properties : Map_Value ) return A_Command;

    -- Creates a Command object that spawns an instance of 'template' at 'x,y',
    -- named 'name' and sized as 'width,height' with entity attributes
    -- 'attributes'. If the attributes are null, they will be ignored. If
    -- 'entityId' is set, it will be used, otherwise a unique entity id will be
    -- automatically generated.
    function Create_Command_Spawn_Entity( template      : String;
                                          name          : String;
                                          x, y          : Float;
                                          width, height : Float;
                                          attributes    : Map_Value := Null_Value.Map;
                                          entityId      : Entity_Id := NULL_ID ) return A_Command;

    -- Creates a Command object that swaps map layer 'layer' with 'otherLayer'.
    function Create_Command_Swap_Layers( layer, otherLayer : Integer ) return A_Command;

private

    package Command_Lists is new Ada.Containers.Doubly_Linked_Lists( A_Command, "=" );

    ----------------------------------------------------------------------------

    type Compound_Command is new Command with
        record
            group : Command_Lists.List;
        end record;

    procedure Delete( this : in out Compound_Command );

    procedure Execute( this : in out Compound_Command );

    procedure Undo( this : Compound_Command );

    ----------------------------------------------------------------------------

    type Set_Tiles_Command is new Compound_Command with
        record
            tiles  : Tile_Change_Lists.Vector;
            oldIds : Tile_Id_Vectors.Vector;
        end record;

    procedure Combine( this : in out Set_Tiles_Command; cmd : in out A_Command );

    procedure Execute( this : in out Set_Tiles_Command );

    procedure Undo( this : Set_Tiles_Command );

end Commands;
