--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Events;

package Events.Tinker is

    EVT_ERROR_MESSAGE : constant Event_Id := 10100;

    type Error_Message_Event is new Event with private;
    type A_Error_Message_Event is access all Error_Message_Event'Class;

    function Get_Heading( this : not null access Error_Message_Event'Class ) return String;

    function Get_Text( this : not null access Error_Message_Event'Class ) return String;

    function Get_Title( this : not null access Error_Message_Event'Class ) return String;

    ----------------------------------------------------------------------------

    EVT_IMPORT_WORLD : constant Event_Id := 10101;

    type Import_World_Event is new Event with private;
    type A_Import_World_Event is access all Import_World_Event'Class;

    function Get_Filename( this : not null access Import_World_Event'Class ) return String;

    function Get_Library_Name( this : not null access Import_World_Event'Class ) return String;

    function Get_Player_Template( this : not null access Import_World_Event'Class ) return String;

    function Get_Tolerance( this : not null access Import_World_Event'Class ) return Natural;

    ----------------------------------------------------------------------------

    EVT_SAVE_WORLD : constant Event_Id := 10102;

    type Save_World_Event is new Event with private;
    type A_Save_World_Event is access all Save_World_Event'Class;

    function Get_Filename( this : not null access Save_World_Event'Class ) return String;

    -- Returns the file format in which to save the world. An empty string
    -- indicates the native file format.
    function Get_Format( this : not null access Save_World_Event'Class ) return String;

    -- Returns True the world is being saved to a temporary file. If so, then
    -- the "filename" and "filepath" properties will not be modified and no
    -- WORLD_UNMODIFIED event will be queued upon success.
    function Is_Temporary( this : not null access Save_World_Event'Class ) return Boolean;

    ----------------------------------------------------------------------------

    EVT_RESPAWN_ENTITIES : constant Event_Id := 10103;

    ----------------------------------------------------------------------------

    -- Queues an error message to be displayed to the user. 'title' is the title
    -- of the error message box. 'heading' is a short description of the type of
    -- error. 'text' is the full error text displayed under the heading. The GUI
    -- may display the error however it chooses and no other action is required.
    procedure Queue_Error_Message( title   : String;
                                   heading : String;
                                   text    : String );

    -- Asynchronously exports the world to 'filePath' as file format 'format'.
    procedure Queue_Export_World( filePath : String; format : String );

    procedure Queue_Import_World( filename       : String;
                                  libName        : String := "";
                                  playerTemplate : String := "";
                                  tolerance      : Natural := 0 );

    -- Asynchronously saves the world. An EVT_WORLD_UNMODIFIED event will be
    -- queued if response when the world is successfully saved.
    procedure Queue_Save_World( filename : String );

    -- Synchronously saves the world, returning True on success. This assumes
    -- the world is being saved to a temporary location for immediate use, so no
    -- EVT_WORLD_UNMODIFIED event will be queued.
    function Trigger_Save_Temporary_World( filename : String ) return Boolean;

    -- Respawns all entities in the world, preserving their ids and public state
    -- only. This is useful to refresh existing entities when their scripts
    -- change. Private script state will be lost.
    procedure Queue_Repawn_Entities;

private

    type Error_Message_Event is new Event with
        record
            title   : Unbounded_String;       -- title of the error message
            heading : Unbounded_String;       -- short description of the type of error
            text    : Unbounded_String;       -- full message text
        end record;

    procedure Construct( this    : access Error_Message_Event;
                         title   : String;
                         heading : String;
                         text    : String );

    ----------------------------------------------------------------------------

    type Import_World_Event is new Event with
        record
            filename       : Unbounded_String;
            libName        : Unbounded_String;
            playerTemplate : Unbounded_String;
            tolerance      : Natural := 0;
        end record;

    procedure Construct( this           : access Import_World_Event;
                         filename       : String;
                         libName        : String;
                         playerTemplate : String;
                         tolerance      : Natural );

    ----------------------------------------------------------------------------

    type Save_World_Event is new Event with
        record
            filename  : Unbounded_String;
            format    : Unbounded_String;
            temporary : Boolean := False;
        end record;

    procedure Construct( this      : access Save_World_Event;
                         filename  : String;
                         format    : String;
                         temporary : Boolean );

end Events.Tinker;
