--
-- Copyright (c) 2015-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;

package body Commands.History is

    function Create_Command_History( depth : Natural ) return A_Command_History is
        this : constant A_Command_History := new Command_History'(Limited_Object with depth => depth, others => <>);
    begin
        this.sigHistoryChanged.Init( this );
        return this;
    end Create_Command_History;

    ----------------------------------------------------------------------------

    function HistoryChanged( this : not null access Command_History'Class ) return access Signal'Class is (this.sigHistoryChanged'Access);

    ----------------------------------------------------------------------------

    function Can_Redo( this : Command_History'Class ) return Boolean is (not this.redoList.Is_Empty);

    ----------------------------------------------------------------------------

    function Can_Undo( this : Command_History'Class ) return Boolean is (not this.undoList.Is_Empty);

    ----------------------------------------------------------------------------

    procedure Clear( this : in out Command_History'Class ) is
    begin
        this.allowCombine := True;
        if not this.redoList.Is_Empty or not this.undoList.Is_Empty then
            this.Clear_List( this.redoList );
            this.Clear_List( this.undoList );
            this.sigHistoryChanged.Emit;
        end if;
    end Clear;

    ----------------------------------------------------------------------------

    procedure Clear_List( this : in out Command_History'Class; list : in out Command_Lists.List ) is
        pragma Unreferenced( this );
        cmd : A_Command;
    begin
        while not list.Is_Empty loop
            cmd := list.First_Element;
            Delete( cmd );
            list.Delete_First;
        end loop;
    end Clear_List;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out Command_History ) is
    begin
        this.Clear;
        Limited_Object(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Execute( this     : in out Command_History'Class;
                       cmd      : access Command'Class;
                       undoable : Boolean := True ) is
        tmp : A_Command;
    begin
        if cmd = null then
            return;
        end if;

        cmd.Execute;
        if not undoable then
            -- if the command can't be undone, then discard it immediately and
            -- don't put it on the undo list.
            tmp := A_Command(cmd);
            Delete( tmp );
            return;
        end if;

        this.Clear_List( this.redoList );

        -- try to combine 'cmd' with the most recent command. this allows a
        -- series of commands that override .Combine() (like Set_Tile_Command)
        -- to be grouped together and undone/redone as a single action.
        if this.allowCombine and then not this.undoList.Is_Empty then
            tmp := A_Command(cmd);
            this.undoList.First_Element.Combine( tmp );
            if tmp = null then
                -- command was combined with the latest command on the undo list;
                -- no need to add it separately. note that the HistoryChanged
                -- signal is not emitted because the undo list hasn't actually
                -- changed.
                return;
            end if;
        end if;

        this.undoList.Prepend( A_Command(cmd) );
        if Integer(this.undoList.Length) > this.depth then
            Delete( this.undoList.Reference( this.undoList.Last ) );
            this.undoList.Delete_Last;
        end if;
        this.allowCombine := True;

        this.sigHistoryChanged.Emit;
    end Execute;

    ----------------------------------------------------------------------------

    procedure Prevent_Combine( this : in out Command_History'Class ) is
    begin
        this.allowCombine := False;
    end Prevent_Combine;

    ----------------------------------------------------------------------------

    procedure Redo( this : in out Command_History'Class ) is
        cmd : A_Command;
    begin
        if not this.redoList.Is_Empty then
            cmd := this.redoList.First_Element;
            this.redoList.Delete_First;
            this.undoList.Prepend( cmd );
            this.allowCombine := False;
            cmd.Execute;
            this.sigHistoryChanged.Emit;
        end if;
    end Redo;

    ----------------------------------------------------------------------------

    procedure Undo( this : in out Command_History'Class ) is
       cmd : A_Command;
    begin
       if not this.undoList.Is_Empty then
            cmd := this.undoList.First_Element;
            this.undoList.Delete_First;
            this.redoList.Prepend( cmd );
            this.allowCombine := False;
            cmd.Undo;
            this.sigHistoryChanged.Emit;
        end if;
    end Undo;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Command_History ) is
    begin
        Delete( A_Limited_Object(this) );
    end Delete;

end Commands.History;
