--
-- Copyright (c) 2015-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Signals;                           use Signals;

package Commands.History is

    -- A command history manager encapsulates undo and redo command lists,
    -- allowing commands to be executed, backed out, and re-executed.
    type Command_History is new Limited_Object with private;
    type A_Command_History is access all Command_History'Class;

    -- Returns a new command history manager.
    function Create_Command_History( depth : Natural ) return A_Command_History;

    -- Signal indicating the command history changed, so the results of Can_Redo
    -- and Can_Undo may have changed.
    function HistoryChanged( this : not null access Command_History'Class ) return access Signal'Class;

    -- Returns True if the next call to Redo will succeed.
    function Can_Redo( this : Command_History'Class ) return Boolean;

    -- Returns True if the next call to Undo will succeed.
    function Can_Undo( this : Command_History'Class ) return Boolean;

    -- Clears the undo/redo history.
    procedure Clear( this : in out Command_History'Class );

    -- Prevents combining the next command with the most recent command.
    procedure Prevent_Combine( this : in out Command_History'Class );

    -- Executes 'cmd' and adds it to the command history, on the undo list, if
    -- 'undoable' is True. If the command is undoable, any commands in the redo
    -- list will also be discarded. 'cmd' will be consumed. If 'cmd' is null,
    -- it will be ignored.
    procedure Execute( this     : in out Command_History'Class;
                       cmd      : access Command'Class;
                       undoable : Boolean := True );

    -- Re-executes the most recently undone command.
    procedure Redo( this : in out Command_History'Class );

    -- Performs an undo of the most recent command, adding it to the undo list
    -- to be re-executed later with Redo().
    procedure Undo( this : in out Command_History'Class );

    -- Deletes the Command_History manager.
    procedure Delete( this : in out A_Command_History );

private

    type Command_History is new Limited_Object with
        record
            depth        : Integer := 0;          -- maximum undo-depth
            redoList     : Command_Lists.List;    -- most recently undone first
            undoList     : Command_Lists.List;    -- most recently executed first
            allowCombine : Boolean := True;       -- allow combining commands

            sigHistoryChanged : aliased Signal;
        end record;

    -- Clears command list 'list'.
    procedure Clear_List( this : in out Command_History'Class;
                          list : in out Command_Lists.List );

    procedure Delete( this : in out Command_History );

end Commands.History;
