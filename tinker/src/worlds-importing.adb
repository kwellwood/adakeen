--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Vectors;
with Ada.Exceptions;                    use Ada.Exceptions;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Bitmaps.Locking;           use Allegro.Bitmaps.Locking;
with Allegro.Color;                     use Allegro.Color;
with Allegro.State;                     use Allegro.State;
with Debugging;                         use Debugging;
with Drawing;                           use Drawing;
with Interfaces;                        use Interfaces;
with Palette;                           use Palette;
with Support;                           use Support;
with Values.Construction;               use Values.Construction;
with Values.Strings;                    use Values.Strings;
with Worlds;                            use Worlds;

package body Worlds.Importing is

    TILE_WIDTH_WORLD_UNITS : constant := 16;

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Import_World( imagePath,
                           libName,
                           playerTemplate : String;
                           tolerance      : Natural ) return A_World is

        UPDATE_DELAY : constant Time_Span := Seconds( 3 );

        package Int_Vectors is new Ada.Containers.Vectors(Positive, Natural, "=");

        ------------------------------------------------------------------------

        function Is_Transparent( bmp : A_Allegro_Bitmap ) return Boolean is
            r, g, b, a : Unsigned_8;
        begin
            if bmp = null then
                return False;
            end if;
            Al_Lock_Bitmap( bmp,
                            ALLEGRO_PIXEL_FORMAT_ANY_32_WITH_ALPHA,
                            ALLEGRO_LOCK_READONLY );
            for y in 0..(Al_Get_Bitmap_Height( bmp ) - 1) loop
                for x in 0..(Al_Get_Bitmap_Width( bmp ) - 1) loop
                    Al_Unmap_RGBA( Al_Get_Pixel( bmp, x, y ), r, g, b, a );
                    if a = 0 then
                        Al_Unlock_Bitmap( bmp );
                        return True;
                    end if;
                end loop;
            end loop;
            Al_Unlock_Bitmap( bmp );
            return False;
        end Is_Transparent;

        ------------------------------------------------------------------------

        function Match( a, b : A_Allegro_Bitmap ) return Boolean is
        begin
            if a = null and then b = null then
                return True;
            end if;

            if a = null or else b = null or else
               Al_Get_Bitmap_Width( a ) /= Al_Get_Bitmap_Width( b ) or else
               Al_Get_Bitmap_Height( a ) /= Al_Get_Bitmap_Height( b )
            then
                return False;
            end if;

            Al_Lock_Bitmap( a,
                            ALLEGRO_PIXEL_FORMAT_ANY_32_WITH_ALPHA,
                            ALLEGRO_LOCK_READONLY );
            Al_Lock_Bitmap( b,
                            ALLEGRO_PIXEL_FORMAT_ANY_32_WITH_ALPHA,
                            ALLEGRO_LOCK_READONLY );
            for y in 0..(Al_Get_Bitmap_Height( a ) - 1) loop
                for x in 0..(Al_Get_Bitmap_Width( a ) - 1) loop
                    if not Compare( Al_Get_Pixel( a, x, y ),
                                    Al_Get_Pixel( b, x, y ),
                                    tolerance )
                    then
                        Al_Unlock_Bitmap( b );
                        Al_Unlock_Bitmap( a );
                        return False;
                    end if;
                end loop;
            end loop;
            Al_Unlock_Bitmap( b );
            Al_Unlock_Bitmap( a );
            return True;
        end Match;
        ------------------------------------------------------------------------

        function Transparent_Match( bg, fg : A_Allegro_Bitmap ) return Boolean is
            fgp : Allegro_Color;
        begin
            if bg = null or else fg = null or else
               Al_Get_Bitmap_Width( bg ) /= Al_Get_Bitmap_Width( fg ) or else
               Al_Get_Bitmap_Height( bg ) /= Al_Get_Bitmap_Height( fg )
            then
                return False;
            end if;

            Al_Lock_Bitmap( bg,
                            ALLEGRO_PIXEL_FORMAT_ANY_32_WITH_ALPHA,
                            ALLEGRO_LOCK_READONLY );
            Al_Lock_Bitmap( fg,
                            ALLEGRO_PIXEL_FORMAT_ANY_32_WITH_ALPHA,
                            ALLEGRO_LOCK_READONLY );
            for y in 0..(Al_Get_Bitmap_Height( bg ) - 1) loop
                for x in 0..(Al_Get_Bitmap_Width( bg ) - 1) loop
                    fgp := Al_Get_Pixel( fg, x, y );
                    if fgp /= Transparent and then
                       not Compare( fgp, Al_Get_Pixel( bg, x, y ), tolerance )
                    then
                        Al_Unlock_Bitmap( fg );
                        Al_Unlock_Bitmap( bg );
                        return False;
                    end if;
                end loop;
            end loop;
            Al_Unlock_Bitmap( fg );
            Al_Unlock_Bitmap( bg );
            return True;
        end Transparent_Match;

        ------------------------------------------------------------------------

        procedure Update_Progress( percent, secsRemaining : Integer ) is
        begin
            pragma Debug( Dbg( "  " & Image( percent ) & "% : " &
                               Image( secsRemaining ) & "s remaining",
                               D_WORLD, Info ) );
        end Update_Progress;

        ------------------------------------------------------------------------

        bmp          : A_Allegro_Bitmap := Al_Load_Bitmap( imagePath );
        lib          : constant Library_Ptr := Load_Library( libName, bitmaps => True );
        world        : A_World := null;
        player       : A_Entity := null;
        tw           : constant Natural := TILE_WIDTH_WORLD_UNITS;
        start        : constant Time := Clock;
        lastUpdate   : Time := start;      -- time of last progress update
        progress     : Natural;
        lastProgress : Natural := 0;       -- progress at last update
        matched      : Natural := 0;       -- tiles successfully matched
        tileIds      : Int_Vectors.Vector;
        transIds     : Int_Vectors.Vector;
        tmpBmp       : A_Allegro_Bitmap;
        clk          : Time;
        secsLeft     : Integer;
        state        : Allegro_State;
    begin
        if bmp = null then
            raise Constraint_Error with "Unable to read file <" & imagePath & ">";
        elsif not lib.Get.Is_Loaded then
            raise Constraint_Error with "Unable to load library <" & libName & ">";
        end if;

        if Al_Get_Bitmap_Width( bmp ) mod tw /= 0 or else
           Al_Get_Bitmap_Height( bmp ) mod tw /= 0
        then
            raise Constraint_Error with "Bitmap dimensions must be a multiple of " &
                                        Image( tw );
        end if;

        declare
            procedure Examine( tile : not null A_Tile ) is
            begin
                tileIds.Append( tile.Get_Id );
            end Examine;
        begin
            lib.Get.Iterate_By_Id( Examine'Unrestricted_Access );
        end;

        Al_Store_State( state, ALLEGRO_STATE_NEW_BITMAP_PARAMETERS );
        Al_Set_New_Bitmap_Flags( ALLEGRO_MEMORY_BITMAP );
        tmpBmp := Al_Create_Bitmap( tw, tw );
        Al_Restore_State( state );
        for t in 1..tileIds.Length loop
            if Is_Transparent( lib.Get.Get_Tile( tileIds.Element( Positive(t) ) ).Get_Bitmap ) then
                transIds.Append( tileIds.Element( Positive(t) ) );
            end if;
        end loop;

        if Al_Get_Bitmap_Width( bmp ) / tw > Maps.MAX_WIDTH or
           Al_Get_Bitmap_Height( bmp ) / tw > Maps.MAX_HEIGHT
        then
            raise Constraint_Error with "Bitmap is too large";
        end if;

        world := Create_World( Al_Get_Bitmap_Width( bmp ) / tw,
                               Al_Get_Bitmap_Height( bmp ) / tw,
                               tw,
                               libName );
        if world = null then
            raise FILE_NOT_FOUND with "Library <" & libName & "> not found";
        end if;

        -- Layer 0: Middleground
        world.Add_Layer( 0, Layer_Tiles, Create_Map( (Pair( PROP_TITLE    , Create( "Middleground" ) ),
                                                      Pair( PROP_DELETABLE, Create( False )          ),
                                                      Pair( PROP_SOLID    , Create( True  )          )) ).Map );

        -- Layer +1: Background
        world.Add_Layer( 1, Layer_Tiles, Create_Map( (Pair( PROP_TITLE    , Create( "Background" ) ),
                                                      Pair( PROP_DELETABLE, Create( False )        ),
                                                      Pair( PROP_SOLID    , Create( True  )        )) ).Map );

        -- Layer -1: Foreground
        world.Add_Layer( 0, Layer_Tiles, Create_Map( (Pair( PROP_TITLE    , Create( "Foreground" ) ),
                                                      Pair( PROP_DELETABLE, Create( False )        ),
                                                      Pair( PROP_SOLID    , Create( False )        )) ).Map );

        if playerTemplate'Length > 0 then
            -- raises Constraint_Error if 'playerTemplate' not found
            player := Entities.Factory.Global.Create_Entity( playerTemplate,
                                                             Create_Map( (1=>Pair("Location",
                                                                                  Create_Map( (Pair("x", Create( Long_Float(Al_Get_Bitmap_Width( bmp ) / 2) )),
                                                                                               Pair("y", Create( Long_Float(Al_Get_Bitmap_Height( bmp ) / 2) ))) ))) ).Map
                                                           );
            world.Set_Property( "player", To_Id_Value( player.Get_Id ) );
            world.Add_Entity( player );
        end if;

        pragma Debug( Dbg( "Matching tiles...", D_WORLD, Info ) );
        for y in 0..(world.Get_Map_Rows - 1) loop
            for x in 0..(world.Get_Map_Columns - 1) loop

                declare
                    subBmp    : A_Allegro_Bitmap := Al_Create_Sub_Bitmap( bmp, x * tw, y * tw, tw, tw );
                    bgTile,
                    fgTile    : A_Tile;
                    matchedFg,
                    matchedBg : Boolean := False;
                    foundFg,
                    foundBg   : Boolean := False;
                begin
                    -- try to match just a background
                    for bg in 1..tileIds.Length loop
                        bgTile := lib.Get.Get_Tile( tileIds.Element( Positive(bg) ) );
                        if bgTile.Get_Bitmap /= null then
                            if Match( subBmp, bgTile.Get_Bitmap ) then
                                world.Set_Tile( 1, x, y, bgTile.Get_Id );
                                matchedBg := True;
                                foundBg := True;
                                exit;
                            end if;
                        end if;
                    end loop;

                    -- try to find a foreground tile that works
                    if not matchedBg then
                        for fg in 1..transIds.Length loop
                            fgTile := lib.Get.Get_Tile( transIds.Element( Positive(fg) ) );
                            if Transparent_Match( subBmp, fgTile.Get_Bitmap ) then
                                -- try to combine a background with the foreground for a match
                                for bg in 1..tileIds.Length loop
                                    bgTile := lib.Get.Get_Tile( tileIds.Element( Positive(bg) ) );
                                    if bgTile.Get_Bitmap /= null then
                                        Al_Store_State( state, ALLEGRO_STATE_TARGET_BITMAP );
                                        Set_Target_Bitmap( tmpBmp );
                                        Clear_To_Color( Al_Map_RGB( 255, 0, 255 ) );
                                        Al_Draw_Bitmap( bgTile.Get_Bitmap, 0.0, 0.0, 0 );
                                        Al_Draw_Bitmap( fgTile.Get_Bitmap, 0.0, 0.0, 0 );
                                        Al_Restore_State( state );
                                        if Match( subBmp, tmpBmp ) then
                                            foundFg := not matchedFg;
                                            if not matchedFg then
                                                world.Set_Tile( 2, x, y, fgTile.Get_Id );
                                            else
                                                -- found multiple foreground matches
                                                world.Set_Tile( 2, x, y, 0 );
                                            end if;
                                            foundBg := not matchedBg;
                                            if not matchedBg then
                                                world.Set_Tile( 1, x, y, bgTile.Get_Id );
                                                matchedBg := True;
                                            else
                                                -- found multiple background matches
                                                world.Set_Tile( 1, x, y, 0 );
                                            end if;
                                        end if;
                                    end if;
                                end loop;
                                matchedFg := True;
                            end if;
                        end loop;
                    end if;

                    if foundFg or else foundBg then
                        matched := matched + 1;
                    end if;
                    Al_Destroy_Bitmap( subBmp );
                end;

                if x mod 20 = 0 then
                    clk := Clock;
                    if clk - lastUpdate >= UPDATE_DELAY then
                        progress := 100 * (y * world.Get_Map_Columns + x + 1) /
                                          (world.Get_Map_Columns * world.Get_Map_Rows);
                        if progress /= lastProgress then
                            secsLeft := Integer(Long_Float(100.0 / Float(progress)) *
                                                Long_Float(To_Duration(clk - start)) -
                                                Long_Float(To_Duration(clk - start)));
                            Update_Progress( progress, secsLeft );
                            lastProgress := progress;
                            lastUpdate := clk;
                        end if;
                    end if;
                end if;

            end loop;
        end loop;
        Update_Progress( 100, 0 );
        pragma Debug( Dbg( "Matched " & Image( matched ) & " of " &
                           Image( world.Get_Map_Columns * world.Get_Map_Rows ) &
                           " tiles: " & Image( 100 * matched / (world.Get_Map_Columns * world.Get_Map_Rows) ) & '%',
                           D_WORLD, Info ) );

        pragma Debug( matched = 0, Dbg( "Failed to match any tiles.", D_WORLD, Info ) );

        Al_Destroy_Bitmap( tmpBmp );
        Al_Destroy_Bitmap( bmp );
        return world;
    exception
        when e : others =>
            Dbg( "Error importing world: " &
                 Exception_Name( e ) & " - " & Exception_Message( e ),
                 D_WORLD, Error );
            Al_Destroy_Bitmap( tmpBmp );
            Al_Destroy_Bitmap( bmp );
            Delete( world );
            raise;
    end Import_World;

end Worlds.Importing;
