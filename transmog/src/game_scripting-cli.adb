--
-- Copyright (c) 2018-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Exceptions;                    use Ada.Exceptions;
with Ada.Streams;                       use Ada.Streams;
with Ada.Streams.Stream_IO;             use Ada.Streams.Stream_IO;
with Ada.Text_IO;                       use Ada.Text_IO;
with GNAT.OS_Lib;                       use GNAT.OS_Lib;
with Scribble;                          use Scribble;
with Scribble.CLI;                      use Scribble.CLI;
with Scribble.IO;                       use Scribble.IO;
with Scribble.Parse_Errors;             use Scribble.Parse_Errors;
with Scribble.Script_Compilers;         use Scribble.Script_Compilers;
with Support;                           use Support;
with Support.Paths;                     use Support.Paths;
with Values.Maps;                       use Values.Maps;

package body Game_Scripting.CLI is

    procedure Error_Line( str : String ) is
    begin
        Ada.Text_IO.Put_Line( Ada.Text_IO.Standard_Error, str );
    end Error_Line;

    ----------------------------------------------------------------------------

    function Compile_Script( runtime      : not null A_Scribble_Runtime;
                             sourcePath   : String;
                             compiledPath : String;
                             enableDebug  : Boolean ) return Exit_Status is
        compiler : A_Script_Compiler;
        obj      : Map_Value;
        outFile  : Stream_IO.File_Type;
    begin
        if not Case_Eq( Get_Extension( sourcePath ), "script" ) then
            Error_Line( "Unrecognized file extension: " & sourcePath );
            return Read_Fail;
        end if;

        -- 1. Read and compile the source code
        begin
            compiler := Create_Script_Compiler( runtime );

            -- may raise Parse_Error
            obj := compiler.Compile_Script( Normalize_Pathname( sourcePath ),
                                            enableDebug => enableDebug );

            Delete( compiler );
        exception
            when e : Parse_Error =>
                Error_Line( Format_Parse_Error( Exception_Message( e ) ) & ASCII.LF &
                            "Compilation failed: " & sourcePath );
                Delete( compiler );
                return Parse_Fail;
        end;

        if not obj.Valid or else not obj.Contains( "members" ) then
            Error_Line( "Unexpected format: " & sourcePath );
            return Parse_Fail;
        end if;

        -- 2. Write the compiled file
        Create( outFile, Out_File, Normalize_Pathname( compiledPath ) );
        if not Is_Open( outFile ) then
            Error_Line( "Cannot create file: " & compiledPath );
            return Write_Fail;
        end if;
        Scribble.IO.Write( Stream( outFile ), obj, Get_Filename( sourcePath ) );
        Close( outFile );

        return Success;
    end Compile_Script;

end Game_Scripting.CLI;
