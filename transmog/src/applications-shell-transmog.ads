--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Command_Line;                  use Ada.Command_Line;
with GNAT.Command_Line;                 use GNAT.Command_Line;
with Script_VMs;                        use Script_VMs;

package Applications.Shell.Transmog is

    function Create return A_Application;

private

    type Transmog_App is new Shell_Application with
        record
            returnCode : Exit_Status := Success;
            optParser  : Opt_Parser;
            optConfig  : Command_Line_Configuration;
            quiet      : aliased Boolean := False;    -- suppress non-error output?
            gameVM     : A_Script_VM := null;
        end record;
    type A_Transmog_App is access all Transmog_App'Class;

    function Main( this : access Transmog_App ) return Integer;

end Applications.Shell.Transmog;
