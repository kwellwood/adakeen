--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Command_Line;                  use Ada.Command_Line;
with Allegro;
with Applications;                      use Applications;
with Applications.Shell.Transmog;
with Debugging;                         use Debugging;

package body Main is

    procedure Main is
        app  : A_Application;
        stat : Integer := 0;
    begin
        app := Applications.Shell.Transmog.Create;
        stat := app.Run;
        Delete( app );

        Set_Exit_Status( Exit_Status(stat) );
    exception
        when e : others =>
            Dbg( "Exception in Main: ...", D_DEV, Error );
            Dbg( e, D_DEV, Error );
            Set_Exit_Status( Failure );
    end Main;

begin
    Allegro.Set_Ada_Main( Main'Access );
end Main;
