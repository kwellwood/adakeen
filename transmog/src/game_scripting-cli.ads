--
-- Copyright (c) 2018-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Command_Line;                  use Ada.Command_Line;
with Scribble.Runtimes;                 use Scribble.Runtimes;

package Game_Scripting.CLI is

    -- Compiles Scribble script 'sourcePath', given Scribble runtime 'runtime',
    -- and writes the compiled Scribble object to 'compiledPath'. If
    -- 'enableDebug' is True, debugging info will be included.
    --
    -- The source file must have a ".script" file extension. It will be located
    -- relative to the current working directory, and any included files will be
    -- located relative to the source file's directory.
    function Compile_Script( runtime      : not null A_Scribble_Runtime;
                             sourcePath   : String;
                             compiledPath : String;
                             enableDebug  : Boolean ) return Exit_Status;

end Game_Scripting.CLI;
