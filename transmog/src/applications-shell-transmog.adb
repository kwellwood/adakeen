--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Ordered_Sets;
with Ada.Exceptions;                    use Ada.Exceptions;
with Ada.Text_IO;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Assets;                            use Assets;
with Assets.Scripts;
with Entities.Factory;                  use Entities.Factory;
with Entities.Galaxy;
with Game_Scripting;                    use Game_Scripting;
with Game_Scripting.CLI;                use Game_Scripting.CLI;
with GNAT.Directory_Operations;         use GNAT.Directory_Operations;
with GNAT.OS_Lib;
with GNAT.Strings;                      use GNAT.Strings;
with Galaxy_API;
with Preferences;
with Scribble.CLI;                      use Scribble.CLI;
with Support;                           use Support;
with Support.Paths;                     use Support.Paths;
with Assets.Libraries;                  use Assets.Libraries;
with Assets.Libraries.Compilers;        use Assets.Libraries.Compilers;
with Values.Lists;                      use Values.Lists;
with Worlds;                            use Worlds;
with Worlds.Scribble_Format;

package body Applications.Shell.Transmog is

    -- Look for media in the current working directory by default
    MEDIA_DIR : constant String := "media" & Support.Paths.Slash;

    procedure Put_Error( line : String ) is
    begin
        Ada.Text_IO.Put_Line( Ada.Text_IO.Standard_Error, line );
    end Put_Error;

    ----------------------------------------------------------------------------

    -- Displays 'msg' in error output with the executable name prefixed, followed
    -- by a second line suggesting use of the "--help" switch. Call this when
    -- execution is aborted because of an invalid or missing command line switch
    -- or argument.
    procedure Args_Error( this : not null access Transmog_App'Class; msg : String );

    procedure Display_Usage( this : not null access Transmog_App'Class );

    function Init_Entity_Factory( this : not null access Transmog_App'Class ) return Boolean;

    procedure Init_Script_VM( this : not null access Transmog_App'Class );

    procedure Finalize_Entity_Factory( this : not null access Transmog_App'Class );

    procedure Finalize_Script_VM( this : not null access Transmog_App'Class );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    procedure Compile( this : not null access Transmog_App'Class );

    procedure Decompile( this : not null access Transmog_App'Class );

    procedure Pack( this : not null access Transmog_App'Class );

    procedure Print( this : not null access Transmog_App'Class );

    procedure Recolor( this : not null access Transmog_App'Class );

    procedure Shell( this : not null access Transmog_App'Class );

    procedure Sv2World( this : not null access Transmog_App'Class );

    procedure Upgrade( this : not null access Transmog_App'Class );

    procedure World2Sv( this : not null access Transmog_App'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create return A_Application is
        this : constant A_Transmog_App := new Transmog_App;
    begin
        this.Construct( company => "Kevin Wellwood",
                        name    => "Transmogrifier",
                        userDir => "" );            -- use working directory
        this.configFile := To_Unbounded_String( "tinker.cfg" );

        Set_Usage( this.optConfig,
                   Usage =>
                       "<command> ...",

                   Help =>
                       EOL &
                       this.Get_Announce &
                       EOL,

                   Help_Msg =>
                       EOL &
                       "  Where <command> is:" & EOL &
                       EOL &
                       "  * compile [switches] <source-file> <compiled-file>" & EOL &
                       EOL &
                       "      Compiles Scribble <source-file> into <compiled-file>." & EOL &
                       "      Switches are:" & EOL &
                       EOL &
                       "        -n<xyz>    : Declare <xyz> as a valid namespace" & EOL &
                       "        -d --debug : Include debugging info" & EOL &
                       EOL &
                       "  * decompile [switches] <compiled-file>" & EOL &
                       EOL &
                       "      Displays instruction disassembly for <compiled-file>." & EOL &
                       "      Switches are:" & EOL &
                       EOL &
                       "        -s --sub <value> : Index into sub-value of the compiled object (repeatable)" & EOL &
                       EOL &
                       "  * pack [switches] <library-file>" & EOL &
                       EOL &
                       "      Packages images into a tile library <library-file>." & EOL &
                       "      Switches are:" & EOL &
                       EOL &
                       "        -c --compress        : Compress the resulting file" & EOL &
                       "        -i --index <file>    : The tile index file to compile (required)" & EOL &
                       "        -m --matrices <file> : The tile matrices file to compile" & EOL &
                       "        -q --quiet           : Suppress all messages except errors" & EOL &
                       "        -t --terrains <file> : The terrains file to compile" & EOL &
                       EOL &
                       "  * print [switches] <compiled-file>" & EOL &
                       EOL &
                       "      Prints the source code of <compiled-file> (if available)." & EOL &
                       "      Switches are:" & EOL &
                       EOL &
                       "        -s --sub <value> : Index into sub-value of the compiled object (repeatable)" & EOL &
                       EOL &
                       "  * recolor [switches] <image-files>" & EOL &
                       EOL &
                       "      Modifies <image-files>, clamping all colors to the EGA palette." & EOL &
                       "      Wildcards are permitted, to recolor multiple images." & EOL &
                       "      Switches are:" & EOL &
                       EOL &
                       "        -q --quiet   : Suppress all messages except errors" & EOL &
                       EOL &
                       "  * shell [switches]" & EOL &
                       EOL &
                       "      Interactive Scribble shell mode." & EOL &
                       "      Switches are:" & EOL &
                       EOL &
                       "        -n<xyz> : Declare <xyz> as a valid namespace" & EOL &
                       EOL &
                       "  * sv2world [switches] <sv-files>" & EOL &
                       EOL &
                       "      Converts all files <sv-files> from a text-based Scribble file format to the native" & EOL &
                       "      world file format. Wildcards are permitted, to convert multiple files. File names" & EOL &
                       "      must use the standard "".world.sv"" extension. Each converted file will be written" & EOL &
                       "      in the same directory as the source, with a "".world"" extension instead. If a" & EOL &
                       "      destination file already exists, the conversion will be skipped." & EOL &
                       "      Switches are:" & EOL &
                       EOL &
                       "        -q --quiet : Suppress all messages except errors" & EOL &
                       "        -f --force : Overwrite any existing files" & EOL &
                       "        -r --rm    : Delete converted source files" & EOL &
                       EOL &
                       "  * upgrade [switches] <world-files>" & EOL &
                       EOL &
                       "      Read and rewrite all worlds <world-files> to upgrade the file format." & EOL &
                       "      Wildcards are permitted, to upgrade multiple files." & EOL &
                       "      Switches are:" & EOL &
                       EOL &
                       "        -q --quiet   : Suppress all messages except errors" & EOL &
                       EOL &
                       "  * world2sv [switches] <world-files>" & EOL &
                       EOL &
                       "      Converts all worlds <world-files> to a text-based Scribble file format. Wildcards" & EOL &
                       "      are permitted, to convert multiple files. File names must use the standard "".world""" & EOL &
                       "      extension. Each converted file will be written in the same directory as the source," & EOL &
                       "      with a "".world.sv"" extension instead. If a destination file already exists, the" & EOL &
                       "      conversion will be skipped." & EOL &
                       "      Switches are:" & EOL &
                       EOL &
                       "        -q --quiet : Suppress all messages except errors" & EOL &
                       "        -f --force : Overwrite any existing files" & EOL
                 );

        return A_Application(this);
    end Create;

    ----------------------------------------------------------------------------

    procedure Init_Script_VM( this : not null access Transmog_App'Class ) is
    begin
        pragma Assert( this.gameVM = null );        -- it should not already be initialized
        this.gameVM := Create_Game_VM;
        Assets.Scripts.Initialize( this.gameVM.Get_Runtime );
        Galaxy_API.Initialize( this.gameVM );
    end Init_Script_VM;

    ----------------------------------------------------------------------------

    function Init_Entity_Factory( this : not null access Transmog_App'Class ) return Boolean is
    begin
        Assets.Libraries.Initialize( null );

        this.Init_Script_VM;

        Entities.Factory.Global := Create_Entity_Factory;
        Entities.Galaxy.Define_Components( Entities.Factory.Global );

        Entities.Factory.Global.Load_Component_Definitions;
        Entities.Factory.Global.Load_Entity_Templates;

        return True;
    exception
        when e : others =>
            Put_Error( Exception_Name( e ) & " - " & Exception_Message( e ) );
            Put_Error( "Failed to load entity templates" );
            return False;
    end Init_Entity_Factory;

    ----------------------------------------------------------------------------

    procedure Finalize_Entity_Factory( this : not null access Transmog_App'Class ) is
    begin
        Delete( Entities.Factory.Global );

        this.Finalize_Script_VM;

        Assets.Unload_All( Library_Assets );
        Assets.Libraries.Finalize;
    end Finalize_Entity_Factory;

    ----------------------------------------------------------------------------

    procedure Finalize_Script_VM( this : not null access Transmog_App'Class ) is
    begin
        Assets.Unload_All( Script_Assets );
        Assets.Scripts.Finalize;
        Delete( this.gameVM );
    end Finalize_Script_VM;

    ----------------------------------------------------------------------------

    overriding
    function Main( this : access Transmog_App ) return Integer is
        args : GNAT.OS_Lib.Argument_List_Access;
   begin
        if Argument_Count = 0 then
            this.Display_Usage;
            return Integer(this.returnCode);
        end if;

        -- operate on all the arguments following the first argument
        args := new String_List(1..Argument_Count-1);
        for i in args'Range loop
            args(i) := new String'(Argument( 1 + i ));
        end loop;

        Initialize_Option_Scan( this.optParser, args );

        if Case_Eq( Argument( 1 ), "-h" ) or Case_Eq( Argument( 1 ), "--help" ) then
            this.Display_Usage;

        elsif Case_Eq( Argument( 1 ), "compile" ) then
            this.Compile;

        elsif Case_Eq( Argument( 1 ), "decompile" ) then
            this.Decompile;

        elsif Case_Eq( Argument( 1 ), "pack" ) then
            this.Pack;

        elsif Case_Eq( Argument( 1 ), "print" ) then
            this.Print;

        elsif Case_Eq( Argument( 1 ), "recolor" ) then
            this.Recolor;

        elsif Case_Eq( Argument( 1 ), "shell" ) then
            this.Shell;

        elsif Case_Eq( Argument( 1 ), "sv2world" ) then
            this.Sv2World;

        elsif Case_Eq( Argument( 1 ), "upgrade" ) then
            this.Upgrade;

        elsif Case_Eq( Argument( 1 ), "world2sv" ) then
            this.World2Sv;

        else
            this.Args_Error( "unrecognized command '" & Argument( 1 ) & "'" );
            this.returnCode := Bad_Arguments;

        end if;

        for i in args'Range loop
            Free( args(i) );
        end loop;

        return Integer(this.returnCode);
    exception
        when e : others =>
            Put_Error( "Exception " & Exception_Name( e ) & " - " & Exception_Message( e ) );
            this.returnCode := Exception_Fail;
            return Integer(this.returnCode);
    end Main;

    ----------------------------------------------------------------------------

    procedure Args_Error( this : not null access Transmog_App'Class; msg : String ) is
        pragma Unreferenced( this );
    begin
        Put_Error( Base_Name( Command_Name ) & ": " & msg );
        Try_Help;
    end Args_Error;

    ----------------------------------------------------------------------------

    procedure Compile( this : not null access Transmog_App'Class ) is
        namespaces : String_Vectors.Vector;

        ------------------------------------------------------------------------

        procedure Switch_Handler( switch : String; parameter : String; section : String ) is
            pragma Unreferenced( section );
        begin
            if switch = "-n" then
                namespaces.Append( parameter );
            end if;
        end Switch_Handler;

        ------------------------------------------------------------------------

        enableDebug  : aliased Boolean := False;
        sourcePath   : Unbounded_String;
        compiledPath : Unbounded_String;
        extraArg     : Unbounded_String;
    begin
        Define_Switch( this.optConfig, enableDebug'Access, "-d",  "--debug", Help => "Include debug info", Value => True );
        Define_Switch( this.optConfig,                     "-n?",            Help => "Declare namespace",  Argument => "<xyz>" );

        this.returnCode := Success;

        begin
            -- parse command line switches
            Getopt( this.optConfig, Switch_Handler'Unrestricted_Access, this.optParser );

            -- parse the source input file path
            sourcePath := To_Unbounded_String( Get_Argument( Parser => this.optParser ) );
            if sourcePath /= "" then
                -- parse the compiled output file path
                compiledPath := To_Unbounded_String( Get_Argument( Parser => this.optParser ) );
                if compiledPath /= "" then
                    extraArg := To_Unbounded_String( Get_Argument( Parser => this.optParser ) );
                    if extraArg /= "" then
                        -- found extra arguments
                        this.Args_Error( "unexpected argument: " & To_String( extraArg ) );
                        this.returnCode := Bad_Arguments;
                    end if;
                else
                    -- <compiled-file> is optional
                    -- generate an output path from the source path
                    compiledPath := To_Unbounded_String( Replace_Extension( To_String( sourcePath ), "sco" ) );
                end if;
            else
                -- <source-file> is required
                this.Args_Error( "missing argument: <source-file>" );
                this.returnCode := Bad_Arguments;
            end if;

            if this.returnCode = Success then
                if Case_Eq( Get_Extension( To_String( sourcePath ) ), "script" ) then
                    -- compile a galaxy engine Script file
                    this.Init_Script_VM;
                    this.returnCode := Compile_Script( this.gameVM.Get_Runtime,
                                                       To_String( sourcePath ),
                                                       To_String( compiledPath ),
                                                       enableDebug );
                    this.Finalize_Script_VM;
                else
                    -- compile a Scribble object file
                    this.returnCode := Compile_Object( To_String( sourcePath ),
                                                       To_String( compiledPath ),
                                                       namespaces,
                                                       enableDebug );
                end if;
            end if;
        exception
            when Invalid_Switch =>
                -- Getopt() already displayed the error and help
                this.returnCode := Bad_Arguments;

            when Invalid_Parameter =>
                -- this won't happen
                this.Args_Error( "missing parameter for '-" & Full_Switch( this.optParser ) & "'" );
                this.returnCode := Bad_Arguments;

            when Exit_From_Command_Line =>
                null;
        end;
    end Compile;

    ----------------------------------------------------------------------------

    procedure Decompile( this : not null access Transmog_App'Class ) is
        indices : constant List_Value := Create_List.Lst;

        ------------------------------------------------------------------------

        procedure Switch_Handler( switch : String; parameter : String; section : String ) is
            pragma Unreferenced( section );
        begin
            if switch = "-s" then
                indices.Append( parameter );
            end if;
        end Switch_Handler;

        ------------------------------------------------------------------------

        compiledPath : Unbounded_String;
        extraArg     : Unbounded_String;
    begin
        Define_Switch( this.optConfig, "-s:", "--sub:", Help => "Sub-index", Argument => "<value>" );

        this.returnCode := Success;

        begin
            -- parse command line switches
            Getopt( this.optConfig, Switch_Handler'Unrestricted_Access, this.optParser );

            -- parse the compiled file path
            compiledPath := To_Unbounded_String( Get_Argument( Parser => this.optParser ) );
            if compiledPath /= "" then
                extraArg := To_Unbounded_String( Get_Argument( Parser => this.optParser ) );
                if extraArg /= "" then
                    -- found extra arguments
                    this.Args_Error( "unexpected argument: '" & To_String( extraArg ) & "'" );
                    this.returnCode := Bad_Arguments;
                end if;
            else
                -- <compiled-file> is required
                this.Args_Error( "missing argument: <compiled-file>" );
                this.returnCode := Bad_Arguments;
            end if;

            if this.returnCode = Success then
                this.returnCode := Disassemble( To_String( compiledPath ), indices );
            end if;
        exception
            when Invalid_Switch =>
                this.returnCode := Bad_Arguments;
                -- Getopt() already displayed the error and help

            when Invalid_Parameter =>
                this.Args_Error( "missing parameter for '-" & Full_Switch( this.optParser ) & "'" );
                this.returnCode := Bad_Arguments;

            when Exit_From_Command_Line =>
                null;
        end;
    end Decompile;

    ----------------------------------------------------------------------------

    procedure Display_Usage( this : not null access Transmog_App'Class ) is
    begin
        Display_Help( this.optConfig );
    end Display_Usage;

    ----------------------------------------------------------------------------

    procedure Pack( this : not null access Transmog_App'Class ) is

        ------------------------------------------------------------------------

        procedure Output_Line( msg : String; error : Boolean := False ) is
        begin
            if error then
                Put_Error( msg );
            elsif not this.quiet then
                Ada.Text_IO.Put_Line( msg );
            end if;
        end Output_Line;

        ------------------------------------------------------------------------

        compress    : aliased Boolean := False;
        indexPath   : aliased GNAT.OS_Lib.String_Access;
        matrixPath  : aliased GNAT.OS_Lib.String_Access;
        terrainPath : aliased GNAT.OS_Lib.String_Access;
        libPath     : aliased GNAT.OS_Lib.String_Access;
        compiler    : A_Compiler := Create_Compiler( Pack.Output_Line'Unrestricted_Access );
    begin
        Define_Switch( this.optConfig, this.quiet'Access,  "-q",  "--quiet",      Help => "Quiet mode",        Value => True );
        Define_Switch( this.optConfig, compress'Access,    "-c",  "--compress",   Help => "Compress archive",  Value => True );
        Define_Switch( this.optConfig, indexPath'Access,   "-i=", "--index=",     Help => "Index filepath",    Argument => "<file>" );
        Define_Switch( this.optConfig, matrixPath'Access,  "-m=", "--matrices=",  Help => "Matrices filepath", Argument => "<file>" );
        Define_Switch( this.optConfig, terrainPath'Access, "-t=", "--terrains=",  Help => "Terrains filepath", Argument => "<file>" );

        this.returnCode := Success;

        begin
            -- parse command line switches
            Getopt( this.optConfig, null, this.optParser );

            -- parse the library output path
            libPath := new String'(Get_Argument( Parser => this.optParser ));
            if libPath.all = "" then
                -- it's required
                this.Args_Error( "missing argument: <library-file>" );
                this.returnCode := Bad_Arguments;
            end if;

            -- create the library from individual resources
            if this.returnCode = Success then

                -- "-i" or "--index" is required
                if indexPath.all /= "" then
                    if not compiler.Compile( libPath.all, indexPath.all, matrixPath.all, terrainPath.all, compress ) then
                        this.returnCode := Failure;
                    end if;
                else
                    this.Args_Error( "missing switch: --index <file>" );
                    this.returnCode := Bad_Arguments;
                end if;
            end if;
        exception
            when Invalid_Switch =>
                this.returnCode := Bad_Arguments;
                -- Getopt() already displayed the error and help

            when Invalid_Parameter =>
                Ada.Text_IO.Put_Line( "missing parameter for '-" & Full_Switch( this.optParser ) & "'" );
                this.returnCode := Bad_Arguments;

            when Exit_From_Command_Line =>
                null;
        end;

        Delete( compiler );
        Free( indexPath );
        Free( matrixPath );
        Free( terrainPath );
        Free( libPath );
    end Pack;

    ----------------------------------------------------------------------------

    procedure Print( this : not null access Transmog_App'Class ) is
        indices : constant List_Value := Create_List.Lst;

        ------------------------------------------------------------------------

        procedure Switch_Handler( switch : String; parameter : String; section : String ) is
            pragma Unreferenced( section );
        begin
            if switch = "-s" then
                indices.Append( parameter );
            end if;
        end Switch_Handler;

        ------------------------------------------------------------------------

        compiledPath : Unbounded_String;
        extraArg     : Unbounded_String;
    begin
        Define_Switch( this.optConfig, "-s:", "--sub:", Help => "Sub-index", Argument => "<value>" );

        this.returnCode := Success;

        begin
            -- parse command line switches
            Getopt( this.optConfig, Switch_Handler'Unrestricted_Access, this.optParser );

            -- parse the compiled file path
            compiledPath := To_Unbounded_String( Get_Argument( Parser => this.optParser ) );
            if compiledPath /= "" then
                extraArg := To_Unbounded_String( Get_Argument( Parser => this.optParser ) );
                if extraArg /= "" then
                    -- found extra arguments
                    this.Args_Error( "unexpected argument: '" & To_String( extraArg ) & "'" );
                    this.returnCode := Bad_Arguments;
                end if;
            else
                -- <compiled-file> is required
                this.Args_Error( "missing argument: <compiled-file>" );
                this.returnCode := Bad_Arguments;
            end if;

            if this.returnCode = Success then
                this.returnCode := Display_Source( To_String( compiledPath ), indices );
            end if;
        exception
            when Invalid_Switch =>
                this.returnCode := Bad_Arguments;
                -- Getopt() already displayed the error and help

            when Invalid_Parameter =>
                this.Args_Error( "missing parameter for '-" & Full_Switch( this.optParser ) & "'" );
                this.returnCode := Bad_Arguments;

            when Exit_From_Command_Line =>
                null;
        end;
    end Print;

    ----------------------------------------------------------------------------

    procedure Recolor( this : not null access Transmog_App'Class ) is

        ------------------------------------------------------------------------

        procedure Recolor( path : String ) is

            --------------------------------------------------------------------

            function Lt( l, r : Allegro_Color ) return Boolean is
                r1, g1, b1, a1 : Float;
                r2, g2, b2, a2 : Float;
            begin
                Al_Unmap_RGBA_f( l, r1, g1, b1, a1 );
                Al_Unmap_RGBA_f( r, r2, g2, b2, a2 );
                if r1 < r2 then
                    return True;
                elsif r1 = r2 then
                    if g1 < g2 then
                        return True;
                    elsif g1 = g2 then
                        if b1 < b2 then
                            return True;
                        elsif b1 = b2 then
                            return a1 < a2;
                        end if;
                    end if;
                end if;
                return False;
            end Lt;

            --------------------------------------------------------------------

            package Color_Sets is new Ada.Containers.Ordered_Sets(Allegro_Color, Lt, "=");
            use Color_Sets;

            type Ega_Palette is array (Integer range 0..15+1) of Allegro_Color;

            ega : constant Ega_Palette := (
                                           Al_Map_RGB( 16#00#, 16#00#, 16#00# ), -- black
                                           Al_Map_RGB( 16#00#, 16#00#, 16#AA# ), -- blue
                                           Al_Map_RGB( 16#00#, 16#AA#, 16#00# ), -- green
                                           Al_Map_RGB( 16#00#, 16#AA#, 16#AA# ), -- cyan
                                           Al_Map_RGB( 16#AA#, 16#00#, 16#00# ), -- red
                                           Al_Map_RGB( 16#AA#, 16#00#, 16#AA# ), -- magenta
                                           Al_Map_RGB( 16#AA#, 16#55#, 16#00# ), -- brown
                                           Al_Map_RGB( 16#AA#, 16#AA#, 16#AA# ), -- light gray
                                           Al_Map_RGB( 16#55#, 16#55#, 16#55# ), -- dark gray
                                           Al_Map_RGB( 16#55#, 16#55#, 16#FF# ), -- bright blue
                                           Al_Map_RGB( 16#55#, 16#FF#, 16#55# ), -- bright green
                                           Al_Map_RGB( 16#55#, 16#FF#, 16#FF# ), -- bright cyan
                                           Al_Map_RGB( 16#FF#, 16#55#, 16#55# ), -- bright red
                                           Al_Map_RGB( 16#FF#, 16#55#, 16#FF# ), -- bright magenta
                                           Al_Map_RGB( 16#FF#, 16#FF#, 16#55# ), -- bright yellow
                                           Al_Map_RGB( 16#FF#, 16#FF#, 16#FF# ), -- white
                                           Al_Map_RGB( 16#FF#, 16#00#, 16#94# )  -- (non ega) pink transparent
                                          );
            bmp      : A_Allegro_Bitmap;
            replaced : Color_Sets.Set;
            match    : Allegro_Color;
            best     : Float;
            diff     : Float;
            r1, g1, b1, a1 : Float;
            r2, g2, b2, a2 : Float;
        begin
            bmp := Al_Load_Bitmap( path );
            if bmp /= null then
                Al_Set_Target_Bitmap( bmp );
                replaced.Clear;

                for y in 0..Al_Get_Bitmap_Height( bmp )-1 loop
                    for x in 0..Al_Get_Bitmap_Width( bmp )-1 loop
                        Al_Unmap_RGBA_f( Al_Get_Pixel( bmp, x, y ), r1, g1, b1, a1 );
                        best := 3.0;

                        for i in ega'Range loop
                            Al_Unmap_RGBA_f( ega(i), r2, g2, b2, a2 );
                            diff := abs (r2 - r1) + abs (g2 - g1) + abs (b2 - b1);
                            if diff <= best then
                                best := diff;
                                match := Al_Map_RGBA_f( r2, g2, b2, a1 );
                            end if;
                        end loop;

                        if best > 0.0 then
                            if not replaced.Contains( match ) then
                                Al_Unmap_RGBA_f( match, r2, g2, b2, a2 );
                                if not this.quiet then
                                    Ada.Text_IO.Put_Line( "  Replaced (" &
                                                                Image( Integer(r1 * 255.0) ) &
                                                          "," & Image( Integer(g1 * 255.0) ) &
                                                          "," & Image( Integer(b1 * 255.0) ) &
                                                          ") with (" &
                                                                Image( Integer(r2 * 255.0) ) &
                                                          "," & Image( Integer(g2 * 255.0) ) &
                                                          "," & Image( Integer(b2 * 255.0) ) &
                                                          ")" );
                                end if;
                                replaced.Include( match );
                            end if;
                            Al_Put_Pixel( x, y, match );
                        end if;
                    end loop;
                end loop;

                if not replaced.Is_Empty then
                    if Al_Save_Bitmap( path, bmp ) then
                        if not this.quiet then
                            Ada.Text_IO.Put_Line( "Saved " & path );
                        end if;
                    else
                        Put_Error( "Failed to save " & path );
                    end if;
                end if;
                Al_Destroy_Bitmap( bmp );
            else
                Put_Error( "Failed to load " & path );
            end if;
        end Recolor;

        ------------------------------------------------------------------------

        paths : String_Vectors.Vector;
        path  : Unbounded_String;
    begin
        Define_Switch( this.optConfig, this.quiet'Access, "-q", "--quiet", Help => "Quiet mode",       Value => True );

        this.returnCode := Success;

        begin
            -- parse command line switches
            Getopt( this.optConfig, null, this.optParser );

            loop
                path := To_Unbounded_String( Get_Argument( Do_Expansion => True, Parser => this.optParser ) );
                exit when Length( path ) = 0;
                paths.Append( Add_Extension( GNAT.OS_Lib.Normalize_Pathname( To_String( path ) ), "png" ) );
            end loop;

            if paths.Is_Empty then
                Put_Error( "No files found." );
                this.returnCode := Bad_Arguments;
            end if;

            if this.returnCode = Success then
                if Al_Init_Image_Addon then
                    for p of paths loop
                        Recolor( p );
                    end loop;
                    Al_Shutdown_Image_Addon;
                else
                    this.returnCode := Failure;
                end if;
            end if;
        exception
            when others =>
                null;
        end;
    end Recolor;

    ----------------------------------------------------------------------------

    procedure Shell( this : not null access Transmog_App'Class ) is
        namespaces : String_Vectors.Vector;

        ------------------------------------------------------------------------

        procedure Switch_Handler( switch : String; parameter : String; section : String ) is
            pragma Unreferenced( section );
        begin
            if switch = "-n" then
                namespaces.Append( parameter );
            end if;
        end Switch_Handler;

        ------------------------------------------------------------------------

        extraArg : Unbounded_String;
    begin
        Define_Switch( this.optConfig, "-n?", Help => "Declare namespace",  Argument => "<xyz>" );

        this.returnCode := Success;

        begin
            -- parse command line switches
            Getopt( this.optConfig, Switch_Handler'Unrestricted_Access, this.optParser );

            extraArg := To_Unbounded_String( Get_Argument( Parser => this.optParser ) );
            if extraArg /= "" then
                -- found extra arguments
                this.Args_Error( "unexpected argument: " & To_String( extraArg ) );
                this.returnCode := Bad_Arguments;
            end if;

            if this.returnCode = Success then
                if namespaces.Is_Empty then
                    -- declare the anonymous namespace if no others were declared
                    namespaces.Append( "" );
                end if;

                Ada.Text_IO.Put_Line( "" );
                Ada.Text_IO.Put_Line( this.Get_Announce );
                Ada.Text_IO.Put_Line( "Scribble Language Version" & Integer'Image( Scribble.LANGUAGE_VERSION ) );
                Ada.Text_IO.Put_Line( "" );

                Interpretor_Prompt( namespaces );
            end if;
        exception
            when Invalid_Switch =>
                -- Getopt() already displayed the error and help
                this.returnCode := Bad_Arguments;

            when Invalid_Parameter =>
                -- this won't happen
                this.Args_Error( "missing parameter for '-" & Full_Switch( this.optParser ) & "'" );
                this.returnCode := Bad_Arguments;

            when Exit_From_Command_Line =>
                null;
        end;
    end Shell;

    ----------------------------------------------------------------------------

    procedure Sv2World( this : not null access Transmog_App'Class ) is

        ------------------------------------------------------------------------

        function Convert( filePath : String; savePath : String; overwrite : Boolean ) return Boolean is
            world : A_World;
        begin
            if GNAT.OS_Lib.Is_Regular_File( filePath ) then
                if not GNAT.OS_Lib.Is_Regular_File( savePath ) or overwrite then
                    world := Worlds.Scribble_Format.Import( filePath );
                    if world = null then
                        return False;
                    end if;
                    world.Save( savePath, overwrite );        -- may raise exception
                    Delete( world );
                    if not this.quiet then
                        Ada.Text_IO.Put_Line( "Converted " & filePath );
                    end if;
                    return True;
                else
                    Put_Error( "Skipping " & filePath & " - " & Get_Filename( savePath ) & " already exists" );
                end if;
            else
                Put_Error( "Skipping " & filePath & " - file not found" );
            end if;
            return False;
        exception
            when others =>
                Delete( world );
                Put_Error( "Skipping " & filePath & " - conversion error" );
                return False;
        end Convert;

        ------------------------------------------------------------------------

        paths     : String_Vectors.Vector;
        path      : Unbounded_String;
        savePath  : Unbounded_String;
        overwrite : aliased Boolean := False;
        remove    : aliased Boolean := False;
        ok        : Boolean;
    begin
        Define_Switch( this.optConfig, this.quiet'Access, "-q", "--quiet", Help => "Quiet mode",                  Value => True );
        Define_Switch( this.optConfig, overwrite'Access,  "-f", "--force", Help => "Overwrite destination files", Value => True );
        Define_Switch( this.optConfig, remove'Access,     "-r", "--rm",    Help => "Remove source files",         Value => True );

        this.returnCode := Success;

        begin
            -- parse command line switches
            Getopt( this.optConfig, null, this.optParser );

            loop
                path := To_Unbounded_String( Get_Argument( Do_Expansion => True, Parser => this.optParser ) );
                exit when Length( path ) = 0;
                paths.Append( Add_Extension( GNAT.OS_Lib.Normalize_Pathname( To_String( path ) ), "world.sv" ) );
            end loop;

            if paths.Is_Empty then
                this.Args_Error( "missing argument: <sv-files>" );
                this.returnCode := Bad_Arguments;
            end if;

            if this.returnCode = Success then
                if this.Init_Entity_Factory then
                    for p of paths loop
                        savePath := To_Unbounded_String( Get_Directory( p ) & Get_Basename( p ) & "." & World_Extension );
                        if Convert( p, To_String( savePath ), overwrite ) then
                            if remove then
                                GNAT.OS_Lib.Delete_File( p, ok );
                                if not ok then
                                    Put_Error( "Unable to delete " & p );
                                end if;
                            end if;
                        end if;
                    end loop;
                    this.Finalize_Entity_Factory;
                else
                    this.returnCode := Failure;
                end if;
            end if;
        exception
            when Invalid_Switch =>
                -- Getopt() already displayed the error and help
                this.returnCode := Bad_Arguments;

            when Invalid_Parameter =>
                -- this won't happen
                this.Args_Error( "missing parameter for '-" & Full_Switch( this.optParser ) & "'" );
                this.returnCode := Bad_Arguments;

            when Exit_From_Command_Line =>
                null;
        end;
    end Sv2World;

    ----------------------------------------------------------------------------

    procedure Upgrade( this : not null access Transmog_App'Class ) is

        ------------------------------------------------------------------------

        procedure Upgrade_Map( path : String ) is
            world : A_World := null;
        begin
            world := Load_World( path );
            world.Save( path, overwrite => True );
            Delete( world );
            if not this.quiet then
                Ada.Text_IO.Put_Line( "Upgraded " & path );
            end if;
        exception
            when FILE_NOT_FOUND =>
                Delete( world );
                Put_Error( "Skipping " & path & " - File not found" );
            when READ_EXCEPTION =>
                -- this exception occurs if the world file header is missing or
                -- damaged. a general exception occurs if the world contents are
                -- not readable as expected.
                Delete( world );
                Put_Error( "Skipping " & path & " - Not a world file" );
            when e : others =>
                Delete( world );
                Put_Error( "Skipping " & path & " - Error reading file: " &
                           Exception_Name( e ) & " - " & Exception_Message( e ) );
        end Upgrade_Map;

        ------------------------------------------------------------------------

        paths : String_Vectors.Vector;
        path  : Unbounded_String;
    begin
        Define_Switch( this.optConfig, this.quiet'Access, "-q", "--quiet", Help => "Quiet mode",       Value => True );

        this.returnCode := Success;

        begin
            -- parse command line switches
            Getopt( this.optConfig, null, this.optParser );

            loop
                path := To_Unbounded_String( Get_Argument( Do_Expansion => True, Parser => this.optParser ) );
                exit when Length( path ) = 0;
                paths.Append( Add_Extension( GNAT.OS_Lib.Normalize_Pathname( To_String( path ) ), World_Extension ) );
            end loop;

            if paths.Is_Empty then
                Put_Error( "No files found." );
                this.returnCode := Bad_Arguments;
            end if;

            if this.returnCode = Success then
                if this.Init_Entity_Factory then
                    for p of paths loop
                        Upgrade_Map( p );
                    end loop;
                    this.Finalize_Entity_Factory;
                else
                    this.returnCode := Failure;
                end if;
            end if;
        exception
            when Invalid_Switch =>
                -- Getopt() already displayed the error and help
                this.returnCode := Bad_Arguments;

            when Invalid_Parameter =>
                -- this won't happen
                this.Args_Error( "missing parameter for '-" & Full_Switch( this.optParser ) & "'" );
                this.returnCode := Bad_Arguments;

            when Exit_From_Command_Line =>
                null;
        end;
    end Upgrade;

    ----------------------------------------------------------------------------

    procedure World2Sv( this : not null access Transmog_App'Class ) is

        ------------------------------------------------------------------------

        procedure Convert( path : String; savePath : String; overwrite : Boolean ) is
            world : A_World := null;
            ok    : Boolean;
            pragma Warnings( Off, ok );
        begin
            if not GNAT.OS_Lib.Is_Regular_File( savePath ) or overwrite then
                world := Load_World( path );
                GNAT.OS_Lib.Delete_File( savePath, ok );
                if Worlds.Scribble_Format.Export( world, savePath ) then
                    if not this.quiet then
                        Ada.Text_IO.Put_Line( "Converted " & path );
                    end if;
                else
                    Put_Error( "Skipping " & path & " - Error writing " & Get_Filename( savePath ) );
                end if;
                Delete( world );
            else
                Put_Error( "Skipping " & path & " - " & Get_Filename( savePath ) & " already exists" );
            end if;
        exception
            when FILE_NOT_FOUND =>
                Delete( world );
                Put_Error( "Skipping " & path & " - File not found" );
            when READ_EXCEPTION =>
                -- this exception occurs if the world file header is missing or
                -- damaged. a general exception occurs if the world contents are
                -- not readable as expected.
                Delete( world );
                Put_Error( "Skipping " & path & " - Not a world file" );
            when e : others =>
                Delete( world );
                Put_Error( "Skipping " & path & " - Error reading file: " &
                           Exception_Name( e ) & " - " & Exception_Message( e ) );
        end Convert;

        ------------------------------------------------------------------------

        paths     : String_Vectors.Vector;
        path      : Unbounded_String;
        overwrite : aliased Boolean := False;
    begin
        Define_Switch( this.optConfig, this.quiet'Access, "-q", "--quiet", Help => "Quiet mode",                  Value => True );
        Define_Switch( this.optConfig, overwrite'Access,  "-f", "--force", Help => "Overwrite destination files", Value => True );

        this.returnCode := Success;

        begin
            -- parse command line switches
            Getopt( this.optConfig, null, this.optParser );

            loop
                path := To_Unbounded_String( Get_Argument( Do_Expansion => True, Parser => this.optParser ) );
                exit when Length( path ) = 0;
                paths.Append( Add_Extension( GNAT.OS_Lib.Normalize_Pathname( To_String( path ) ), World_Extension ) );
            end loop;

            if paths.Is_Empty then
                this.Args_Error( "missing argument: <world-files>" );
                this.returnCode := Bad_Arguments;
            end if;

            if this.returnCode = Success then
                if this.Init_Entity_Factory then
                    for p of paths loop
                        Convert( p, Get_Directory( p ) & Add_Extension( Get_Basename( p ), "world.sv" ), overwrite );
                    end loop;
                    this.Finalize_Entity_Factory;
                else
                    this.returnCode := Failure;
                end if;
            end if;
        exception
            when Invalid_Switch =>
                -- Getopt() already displayed the error and help
                this.returnCode := Bad_Arguments;

            when Invalid_Parameter =>
                -- this won't happen
                this.Args_Error( "missing parameter for '-" & Full_Switch( this.optParser ) & "'" );
                this.returnCode := Bad_Arguments;

            when Exit_From_Command_Line =>
                null;
        end;
    end World2Sv;

begin

    Preferences.Set_Default( "application", "media", MEDIA_DIR );

end Applications.Shell.Transmog;
