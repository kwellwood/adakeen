--
-- Copyright (c) 2014 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Exceptions;                    use Ada.Exceptions;
with Ada.Streams;                       use Ada.Streams;
with Ada.Streams.Stream_IO;             use Ada.Streams.Stream_IO;
with Ada.Text_IO;
with Allegro;                           use Allegro;
with Content_Signing;
with GNAT.OS_Lib;                       use GNAT.OS_Lib;
with Interfaces;                        use Interfaces;
with Resources;                         use Resources;
with Streams;                           use Streams;
with Unlzexe;                           use Unlzexe;

package body Test_Unlzexe is

    procedure Decompress( path : String; exe : in out Exe_File ) is
        res : A_Resource_File;
    begin
        Delete( exe );
        res := Load_Resource( path, "" );
        if res = null then
            Ada.Text_IO.Put_Line( "File not found: " & path );
            return;
        end if;

        declare
            data : Stream_Element_Array(0..Stream_Element_Offset(res.Size-1));
            for data'Address use res.Get_Address;
        begin
            Unlzexe.Decompress( data, exe );
        end;

        if exe.data /= null then
            Ada.Text_IO.Put_Line( "Original file size:" & exe.data'Length'Img &
                                  " bytes;" & exe.dataOffset'Img & " bytes in header" );
        else
            Ada.Text_IO.Put_Line( "Decompression failed." );
        end if;

        Release( res );
    end Decompress;

    ----------------------------------------------------------------------------

    procedure Ada_Main is
        exe     : Exe_File;
        gold    : A_Resource_File;
        outFile : File_Type;
    begin
        if Argument_Count < 1 or else Argument_Count > 2 then
            Ada.Text_IO.Put_Line( "Usage: test_unlzexe <compressed-exe> [output-exe]" );
            return;
        end if;

        -- attempt to decompress the file
        Decompress( Normalize_Pathname( Argument( 1 ) ), exe );

        -- verify the result against a good decompressed copy
        if exe.data /= null then
            gold := Load_Resource( Normalize_Pathname( Argument( 1 ) ) & ".gold", "" );
            if gold /= null then
                declare
                    data : Stream_Element_Array(0..Stream_Element_Offset(gold.Size-1));
                    for data'Address use gold.Get_Address;
                    matched : Boolean := True;
                begin
                    if exe.data'Length = data'Length then
                        for i in exe.data'Range loop
                            if exe.data(i) /= data(i) then
                                Ada.Text_IO.Put_Line( "Difference at byte" & i'Img & ": found" & exe.data(i)'Img & ", expected" & data(i)'Img );
                                matched := False;
                                exit;
                            end if;
                        end loop;
                    else
                        Ada.Text_IO.Put_Line( "Size doesn't match: found" & exe.data'Length'Img & ", expected" & data'Length'Img );
                        matched := False;
                    end if;
                    if matched then
                        Ada.Text_IO.Put_Line( "Decompressed data is ok." );
                    end if;
                end;
                Release( gold );
            else
                Ada.Text_IO.Put_Line( "File not found: " & Normalize_Pathname( Argument( 1 ) ) & ".gold" );
            end if;
        end if;

        -- optionally save a copy of the uncompressed exe
        if exe.data /= null and then Argument_Count > 1 then
            Create( outFile, Out_File, Normalize_Pathname( Argument( 2 ) ) );
            Stream_Element_Array'Write( Stream( outFile ), exe.data.all );
            Close( outFile );
            Ada.Text_IO.Put_Line( "Decompressed file written to: " & Argument( 2 ) );
        end if;

        Delete( exe );
    exception
        when e : others =>
            -- unexpected failure
            Ada.Text_IO.Put_Line( Exception_Name( e ) & " -- " & Exception_Message( e ) );
            Delete( exe );
            Release( gold );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
    Content_Signing.Allow_Unsigned_Content( True );
end Test_Unlzexe;
