--
-- Copyright (c) 2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Interfaces;                        use Interfaces;

package body Carmack is

    function Expand( input : Stream_Element_Array ) return A_SEA is
        result : A_SEA;
        rpos,
        wpos   : SEO;
        tag    : Stream_Element;
        count  : SEO;
        offset : SEO;
    begin
        if input'Length < 2 then
            return null;
        end if;

        rpos := input'First;
        result := new Stream_Element_Array(1..(SEO(input(rpos)) + SEO(input(rpos+1))*256));
        rpos := rpos + 2;
        wpos := result'First;

        -- loop while there are at least 2 bytes remaining in 'input' and two
        -- bytes of empty space in 'result'
        while rpos < input'Last and then wpos < result'Last loop
            -- number of words to repeat
            count := SEO(input(rpos)) * 2;  -- convert to bytes
            rpos := rpos + 1;

            -- carmack tag (near or far pointer)
            tag := input(rpos);
            rpos := rpos + 1;

            if tag = 16#A7# or else tag = 16#A8# then
                if count > 0 then
                    if tag = 16#A7# then
                        -- near pointer is relative to current write position
                        if rpos <= input'Last then
                            offset := wpos - SEO(input(rpos)) * 2;
                            rpos := rpos + 1;
                        else
                            -- unexpected end of input
                            Delete( result );
                            return null;
                        end if;
                    else
                        -- far pointer is absolute address in written data
                        if rpos < input'Last then
                            offset := result'First + SEO(Unsigned_16(input(rpos)) or Shift_Left( Unsigned_16(input(rpos+1)), 8 )) * 2;
                            rpos := rpos + 2;
                        else
                            -- unexpected end of input
                            Delete( result );
                            return null;
                        end if;
                    end if;

                    if offset + count > wpos or else count > result'Last - wpos + 1 then
                        -- can't repeat 'count' bytes: either not enough bytes
                        -- available, or not enough room in result buffer
                        Delete( result );
                        return null;
                    end if;

                    for i in 0..count-1 loop
                        result(wpos) := result(offset + i);
                        wpos := wpos + 1;
                    end loop;
                else
                    -- the tag byte is being escaped with a count of zero
                    -- write it and the byte that follows it
                    result(wpos) := input(rpos-1); wpos := wpos + 1;
                    result(wpos) := input(rpos);   wpos := wpos + 1;
                    rpos := rpos + 1;
                end if;
            else
                -- no tag; just a regular word
                result(wpos) := input(rpos-2); wpos := wpos + 1;
                result(wpos) := input(rpos-1); wpos := wpos + 1;
            end if;
        end loop;

        if rpos <= input'Last or else wpos <= result'Last then
            -- didn't read or write all the expected bytes
            Delete( result );
        end if;

        return result;
    end Expand;

end Carmack;
