--
-- Copyright (c) 2014 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Streams;                       use Ada.Streams;
with Interfaces;                        use Interfaces;
with Streams;                           use Streams;

package Unlzexe is

    -- The MZ EXE File Format
    -- http://www.fileformat.info/format/exe/corion-mz.htm

     -- base size of the .exe header, not including relocation data
    EXE_HEADER_SIZE : constant := 28;

    type Exe_Header is
        record
            mzId             : String(1..2);   -- "MZ" or "ZM"
            imageSizeLo      : Unsigned_16;    -- Number of bytes in last 512-byte page
            imageSizeHi      : Unsigned_16;    -- Total number of 512-byte pages in executable (including the last)
            numRelocEntries  : Unsigned_16;    -- Number of relocation entries
            headerSize       : Unsigned_16;    -- Header size in 16-byte paragraphs
            minParagraphs    : Unsigned_16;    -- Minimum paragraphs of memory allocated in addition to the code size
            maxParagraphs    : Unsigned_16;    -- Maximum paragraphs of memory allocated in addition to the code size
            ss               : Unsigned_16;    -- Initial SS relative to start of executable
            sp               : Unsigned_16;    -- Initial SP
            checksum         : Unsigned_16;    -- Checksum of executable (or 0)
            ip               : Unsigned_16;    -- CS:IP relative to start of executable (entry point)
            cs               : Unsigned_16;    --
            relocTableOffset : Unsigned_16;    -- Offset of relocation table
            overlayNum       : Unsigned_16;    -- Overlay number (0 = main program)
        end record;
    for Exe_Header'Size use EXE_HEADER_SIZE * 8;
    for Exe_Header'Alignment use 1;

    -- Returns True if 'header' most likely contains a valid EXE header. It
    -- isn't checked for complete correctness.
    function Is_Valid( header : Exe_Header ) return Boolean;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Uncompressed .EXE file contents, produced by Decompress(). Be sure to
    -- delete it after use with Delete().
    type Exe_File is
        record
            data         : A_SEA := null;
            headerOffset : Natural := 0;
            dataOffset   : Natural := 0;
        end record;

    -- Deletes the uncompressed exe file data, freeing up its allocated memory.
    procedure Delete( exe : in out Exe_File );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Decompresses an LZEXE compressed executable file from 'input' into
    -- 'output'. If decompression fails, 'output' will be returned empty. This
    -- may indicate the file is not an LZEXE file, or the file is corrupt.
    procedure Decompress( input  : Stream_Element_Array;
                          output : in out Exe_File );
    pragma Precondition( input'First = 0 );

end Unlzexe;
