--
-- Copyright (c) 2014-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Streams;                       use Ada.Streams;
with Ada.Unchecked_Deallocation;
with Debugging;                         use Debugging;
with GNAT.OS_Lib;                       use GNAT.OS_Lib;
with Resources;                         use Resources;
with Support.Paths;                     use Support.Paths;
with Unlzexe;                           use Unlzexe;

package body Game_Support is

    type Game_Info_Array is array (Game_Type) of Game_Info;

    gameInfo : constant Game_Info_Array := (
        Unsupported => (game   => Unsupported,
                        name   => new String'("Unsupported game"),
                        others => <>),

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        Keen4_v14 => (game            => Keen4_v14,
                      name            => new String'("Keen 4 v1.4"),
                      headerSize      => 11776,
                      imageSize       => 251712,
                      egaDictFile     => new String'("EGADICT.CK4"),
                      egaDictOffset   => 16#000386F6#,  -- dictionaries at 0x382F6 (#1), 0x386F6 (#2)
                      egaHeadFile     => new String'("EGAHEAD.CK4"),
                      egaHeadOffset   => 16#00023E80#,
                      egaHeadFormat   => 3,
                      egaGraphFile    => new String'("EGAGRAPH.CK4"),
                      chunkCount      => 4751,
                      chunks          => (
                          TablePics       => (index =>    0, count =>    1, single => False, size => -1),
                          TableMaskedPics => (index =>    1, count =>    1, single => False, size => -1),
                          TableSprites    => (index =>    2, count =>    1, single => False, size => -1),
                          Fonts           => (index =>    3, count =>    3, single => False, size => -1),
                          Pics            => (index =>    6, count =>  115, single => False, size => -1),
                          MaskedPics      => (index =>  121, count =>    3, single => False, size => -1),
                          Sprites         => (index =>  124, count =>  397, single => False, size => -1),
                          Tiles8          => (index =>  521, count =>  104, single => True,  size => 104 * 8 * 4),
                          MaskedTiles8    => (index =>  522, count =>   20, single => True,  size =>  20 * 8 * 5),
                          Tiles16         => (index =>  523, count => 1296, single => False, size => 2 * 16 * 4),
                          MaskedTiles16   => (index => 1819, count => 2916, single => False, size => 2 * 16 * 5)
                      ),
                      tilePropsOffset => 16#000277C2#,  -- 16#2E00# + 16#000249C2#),
                      mapHeadFile     => new String'("MAPHEAD.CK4"),
                      mapHeadOffset   => 16#00027630#,  -- 16#2E00# + 16#00024830#),
                      gameMapsFile    => new String'("GAMEMAPS.CK4")),

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        Keen5_v10 => (game            => Keen5_v10,
                      name            => new String'("Keen 5 v1.0"),
                      headerSize      => 11776,         -- unverified
                      imageSize       => 254832,        -- unverified
                      egaDictFile     => new String'("EGADICT.CK5"),
                      egaDictOffset   => 16#000390C4#,  --16#000362C4#,  -- unverified (relative to start of image)
                      egaHeadFile     => new String'("EGAHEAD.CK5"),
                      egaHeadOffset   => 16#00024DC0#,  --16#00021FC0#,  -- unverified (relative to start of image)
                      egaHeadFormat   => 3,
                      egaGraphFile    => new String'("EGAGRAPH.CK5"),
                      chunkCount      => 4931,
                      chunks          => (
                          TablePics       => (index =>    0, count =>    1, single => False, size => -1),
                          TableMaskedPics => (index =>    1, count =>    1, single => False, size => -1),
                          TableSprites    => (index =>    2, count =>    1, single => False, size => -1),
                          Fonts           => (index =>    3, count =>    3, single => False, size => -1),
                          Pics            => (index =>    6, count =>   93, single => False, size => -1),
                          MaskedPics      => (index =>   99, count =>    3, single => False, size => -1),
                          Sprites         => (index =>  102, count =>  346, single => False, size => -1),
                          Tiles8          => (index =>  448, count =>  104, single => True,  size => 104 * 8 * 4),
                          MaskedTiles8    => (index =>  449, count =>   20, single => True,  size =>  20 * 8 * 5),
                          Tiles16         => (index =>  450, count => 1512, single => False, size => 2 * 16 * 4),
                          MaskedTiles16   => (index => 1962, count => 2952, single => False, size => 2 * 16 * 5)
                      ),
                      tilePropsOffset => 16#00028922#,  -- 16#2E00# + 16#00025B22#)
                      mapHeadFile     => new String'("MAPHEAD.CK5"),
                      mapHeadOffset   => 16#00025990#,  -- Do we need to add 16#2E00# to this??
                      gameMapsFile    => new String'("GAMEMAPS.CK5"))
    );

    ----------------------------------------------------------------------------

    procedure Delete( exe : in out A_Game_Exe ) is
        procedure Free is new Ada.Unchecked_Deallocation( Game_Exe, A_Game_Exe );
    begin
        if exe /= null then
            Delete( exe.data );
            Free( exe );
        end if;
    end Delete;

    ----------------------------------------------------------------------------

    -- Detects supported games and versions from an executable file. 'exe' is
    -- the raw file contents of the executable. 'exe' will be consumed into the
    -- returned record.
    function Detect_Game( dir : String; exe : A_SEA ) return A_Game_Exe is
        imageSize  : Integer;
        headerSize : Integer;
    begin
        if exe'Length < EXE_HEADER_SIZE then
            return new Game_Exe'(exe, To_Unbounded_String( dir ), gameInfo(Unsupported));
        end if;

        declare
            header : Exe_Header;
            for header'Address use exe(exe'First)'Address;
        begin
            -- verify we loaded an .exe file
            if not Is_Valid( header ) then
                return new Game_Exe'(exe, To_Unbounded_String( dir ), gameInfo(Unsupported));
            end if;

            -- calculate the .exe's header length and image size
            headerSize := Integer(header.headerSize) * 16;
            imageSize := (Integer(header.imageSizeHi) - 1) * 512 + Integer(header.imageSizeLo) - headerSize;
        end;

        -- match the .exe to a supported game
        for i in gameInfo'Range loop
            if headerSize = gameInfo(i).headerSize and then
               imageSize  = gameInfo(i).imageSize
            then
                return new Game_Exe'(exe, To_Unbounded_String( dir ), gameInfo(i));
            end if;
        end loop;

        return new Game_Exe'(exe, To_Unbounded_String( dir ), gameInfo(Unsupported));
    end Detect_Game;

    ----------------------------------------------------------------------------

    function Load_Game( path : String ) return A_Game_Exe is
        res  : A_Resource_File;
        data : A_SEA := null;
    begin
        res := Load_Resource( path, "" );
        if res = null then
            return null;
        end if;

        declare
            res_data : Stream_Element_Array(0..SEO(res.Size)-1);
            for res_data'Address use res.Get_Address;
            exe      : Exe_File;
        begin
            -- decompress if necessary
            Unlzexe.Decompress( res_data, exe );
            if exe.data /= null then
                Dbg( "Decompressed " & path );
                data := exe.data;
                exe.data := null;
            else
                Dbg( "Loaded " & path );
                data := new Stream_Element_Array(res_data'Range);
                data.all := res_data;
            end if;
        end;
        Delete( res );

        return Detect_Game( Get_Directory( Normalize_Pathname( path ) ), data );
    end Load_Game;

end Game_Support;

