--
-- Copyright (c) 2014-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with EgaGraph;                          use EgaGraph;
with Streams;                           use Streams;

package Game_Support is

    -- Supported games and versions using the EGAGRAPH format
    type Game_Type is (Unsupported, Keen4_v14, Keen5_v10);

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Chunk_Location is
        record
            index  : Integer := 0;      -- index of first chunk of the type
            count  : Integer := 0;      -- number of items of this type starting at index
            single : Boolean := False;  -- all items are in a single chunk?
            size   : Integer := 0;      -- decompressed chunk size in bytes
        end record;

    type Chunk_Type_Array is array (Chunk_Type) of Chunk_Location;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type A_String is access all String;

    type Game_Info is
        record
            game            : Game_Type;
            name            : A_String;
            headerSize      : Integer := 0;      -- exe header size (bytes)
            imageSize       : Integer := 0;      -- exe image size (bytes)
            egaDictFile     : A_String;
            egaDictOffset   : SEO := 0;          -- relative to start of exe
            egaHeadFile     : A_String;
            egaHeadOffset   : SEO := 0;          -- relative to start of exe
            egaHeadFormat   : Integer := 0;      -- number of bytes per EgaHead entry
            egaGraphFile    : A_String;
            chunkCount      : Integer := 0;
            chunks          : Chunk_Type_Array;
            tilePropsOffset : SEO := 0;          -- relative to start of exe
            mapHeadFile     : A_String;
            mapHeadOffset   : SEO := 0;
            gameMapsFile    : A_String;
        end record;

    type Game_Exe is
        record
            data : A_SEA;             -- exe raw data bytes (owned)
            path : Unbounded_String;  -- path of the exe file
            info : Game_Info;
        end record;
    type A_Game_Exe is access all Game_Exe;

    procedure Delete( exe : in out A_Game_Exe );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Loads the executable file at 'path' and returns a Game_Exe if it is
    -- found. null will be returned if the file is not found.
    function Load_Game( path : String ) return A_Game_Exe;

end Game_Support;
