--
-- Copyright (c) 2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body GameMaps is

    procedure Delete( obj : in out A_Game_Map ) is
        procedure Free is new Ada.Unchecked_Deallocation( Game_Map, A_Game_Map );
    begin
        if obj /= null then
            Delete( obj.layer0 );
            Delete( obj.layer1 );
            Delete( obj.layer2 );
            Free( obj );
        end if;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Delete( obj : in out A_Game_Maps_Array ) is
        procedure Free is new Ada.Unchecked_Deallocation( Game_Maps_Array, A_Game_Maps_Array );
    begin
        if obj /= null then
            for i in obj'Range loop
                Delete( obj(i) );
            end loop;
            Free( obj );
        end if;
    end Delete;

end GameMaps;
