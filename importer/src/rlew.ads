--
-- Copyright (c) 2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Streams;                       use Ada.Streams;
with Interfaces;                        use Interfaces;
with Streams;                           use Streams;

-- Support for Run Length Encoding (RLE) based on 16-bit words
package RLEW is

   -- Expands the RLEW-compressed data in 'input' into a dynamically allocated
   -- array. Due to limits in the compression scheme, this will never exceed
   -- 131071 bytes. 'key' is the special word indicating a repeating word is
   -- next. null will be returned if the decompression fails.
   function Expand( input : Stream_Element_Array; key : Unsigned_16 := 16#FEFE# ) return A_SEA;

end RLEW;
