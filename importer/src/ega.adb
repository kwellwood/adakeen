--
-- Copyright (c) 2014-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Interfaces;                        use Interfaces;

package body Ega is

    subtype SEO is Stream_Element_Offset;

    -- Contains the 16 EGA colors and a transparent color
    type Ega_Palette is array (0..16) of Allegro_Color;

    ----------------------------------------------------------------------------

    function Get_Bit( byte : Unsigned_8; bit : Natural ) return Unsigned_8
    is (Shift_Right( byte and Shift_Left( 1, bit ), bit ));

    ----------------------------------------------------------------------------

    function Read_Ega_Graphic_Planar( data    : Stream_Element_Array;
                                      width   : Integer;
                                      height  : Integer;
                                      format  : Pixel_Format;
                                      count   : Positive := 1;
                                      columns : Positive := 1 ) return A_Allegro_Bitmap is
    begin
        if width <= 0 or else height <= 0 or else data'Length = 0 then
            return null;
        end if;

        declare
            procedure Initialize_Palette( palette : in out Ega_Palette ) is
            begin
                palette := Ega_Palette'(
                     0 => Al_Map_RGB( 16#00#, 16#00#, 16#00# ),  -- black
                     1 => Al_Map_RGB( 16#00#, 16#00#, 16#AA# ),  -- blue
                     2 => Al_Map_RGB( 16#00#, 16#AA#, 16#00# ),  -- green
                     3 => Al_Map_RGB( 16#00#, 16#AA#, 16#AA# ),  -- cyan
                     4 => Al_Map_RGB( 16#AA#, 16#00#, 16#00# ),  -- red
                     5 => Al_Map_RGB( 16#AA#, 16#00#, 16#AA# ),  -- magenta
                     6 => Al_Map_RGB( 16#AA#, 16#55#, 16#00# ),  -- brown
                     7 => Al_Map_RGB( 16#AA#, 16#AA#, 16#AA# ),  -- light gray
                     8 => Al_Map_RGB( 16#55#, 16#55#, 16#55# ),  -- dark gray
                     9 => Al_Map_RGB( 16#55#, 16#55#, 16#FF# ),  -- bright blue
                    10 => Al_Map_RGB( 16#55#, 16#FF#, 16#55# ),  -- bright green
                    11 => Al_Map_RGB( 16#55#, 16#FF#, 16#FF# ),  -- bright cyan
                    12 => Al_Map_RGB( 16#FF#, 16#55#, 16#55# ),  -- bright red
                    13 => Al_Map_RGB( 16#FF#, 16#55#, 16#FF# ),  -- bright magenta
                    14 => Al_Map_RGB( 16#FF#, 16#FF#, 16#55# ),  -- bright yellow
                    15 => Al_Map_RGB( 16#FF#, 16#FF#, 16#FF# ),  -- white
                    16 => Al_Map_RGBA( 16#FF#, 16#00#, 16#94#, 16#00# )  -- (non ega) pink transparent
                );
            end Initialize_Palette;

            type Byte_Array is array (Natural range <>) of Unsigned_8;

            palette    : Ega_Palette;
            dataOffset : SEO := data'First;   -- offset within the data buffer
            tileOffset : SEO;                 -- offset within the tile
            bpp4       : Byte_Array(0..width*height-1) := (others => 0);
            pixel      : Integer;
            bit        : Unsigned_8;
            bmp        : A_Allegro_Bitmap;
            column     : Integer;  -- tile's column of output bitmap (0 based)
            row        : Integer;  -- tile's row of output bitmap (0 based)
        begin
            Initialize_Palette( palette );
            bmp := Al_Create_Bitmap( width  * columns,
                                     height * (count / columns + (if columns mod count > 0 then 1 else 0) ) );
            Al_Set_Target_Bitmap( bmp );
            Al_Clear_To_Color( Al_Map_RGBA( 0, 0, 0, 0 ) );

            for t in 0..count-1 loop
                column := t mod columns;
                row := t / columns;
                dataOffset := data'First + SEO(t * (if format = EGA_BGRIA then 5 else 4) * (width / 8) * height);

                -- EGA Graphic-Planar data:
                -- http://www.shikadi.net/moddingwiki/Raw_EGA_data#Graphic-planar_EGA_data
                --
                -- alpha plane (1 bit opacity, optional)
                -- blue plane (1 bit per pixel)
                -- green plane (1 bit per pixel)
                -- red plane (1 bit per pixel)
                -- intensity plane (1 bit per pixel)

                -- convert the separate BGRI planes into 4bpp
                tileOffset := dataOffset + SEO((if format = EGA_BGRIA then 1 else 0) * (width / 8) * height);
                for p in 0..3 loop
                    pixel := bpp4'First;
                    for y in 0..height-1 loop
                        for x in 0..width/8-1 loop
                            for b in 0..7 loop
                                bit := Get_Bit( Unsigned_8(data(tileOffset)), 7 - b );
                                bpp4(pixel) := bpp4(pixel) or Shift_Left( bit, p );
                                pixel := pixel + 1;
                            end loop;
                            tileOffset := tileOffset + 1;
                        end loop;
                    end loop;
                end loop;

                -- write the 4bpp pixels into the bitmap
                pixel := bpp4'First;
                for y in 0..height-1 loop
                    for x in 0..width-1 loop
                        Al_Put_Pixel( column * width + x,
                                      row * height + y,
                                      palette(Integer(bpp4(pixel))) );
                        pixel := pixel + 1;
                    end loop;
                end loop;

                -- use the alpha plane to replace solid pixels with transparency
                if format = EGA_BGRIA then
                    tileOffset := dataOffset;
                    pixel := bpp4'First;
                    for y in 0..(height - 1) loop
                        for x in 0..(width / 8 - 1) loop
                            for b in 0..7 loop
                                bit := Get_Bit( Unsigned_8(data(tileOffset)), 7 - b );
                                if bit > 0 then
                                    Al_Put_Pixel( column * width + x * 8 + b,
                                                  row * height + y,
                                                  palette(palette'Last) );
                                end if;
                                pixel := pixel + 1;
                            end loop;
                            tileOffset := tileOffset + 1;
                        end loop;
                    end loop;
                end if;

                bpp4 := (others => 0);
            end loop;

            Al_Set_Target_Bitmap( null );
            return bmp;
        end;
    end Read_Ega_Graphic_Planar;

    ----------------------------------------------------------------------------

end Ega;
