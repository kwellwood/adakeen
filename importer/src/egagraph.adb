--
-- Copyright (c) 2014 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body EgaGraph is

    procedure Delete( obj : in out A_Chunk_Array ) is
        procedure Free is new Ada.Unchecked_Deallocation( Chunk_Array, A_Chunk_Array );
    begin
        if obj /= null then
            for i in obj'Range loop
                Al_Destroy_Bitmap( obj(i).bmp );
            end loop;
            Free( obj );
        end if;
    end Delete;

end EgaGraph;
