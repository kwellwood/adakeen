--
-- Copyright (c) 2014 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Unchecked_Deallocation;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Interfaces;                        use Interfaces;

package EgaGraph is

    -- Recognized chunk types found in an EGAGRAPH archive
    type Chunk_Type is (TablePics,
                        TableMaskedPics,
                        TableSprites,
                        Fonts,
                        Pics,
                        MaskedPics,
                        Sprites,
                        Tiles8,             -- always one chunk
                        MaskedTiles8,       -- always one chunk
                        Tiles16,
                        MaskedTiles16);

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- http://www.shikadi.net/moddingwiki/Keen_4-6_Tileinfo_Format
    type Tile_Info is
        record
            animationTime : Unsigned_8 := 0;
            nextTile      : Integer := 0;
            edgeTop       : Unsigned_8 := 0;
            edgeBottom    : Unsigned_8 := 0;
            edgeLeft      : Unsigned_8 := 0;
            edgeRight     : Unsigned_8 := 0;
            behavior      : Unsigned_8 := 0;
        end record;

    type Tile_Info_Array is array (Integer range <>) of Tile_Info;
    type A_Tile_Info_Array is access Tile_Info_Array;

    procedure Delete is new Ada.Unchecked_Deallocation( Tile_Info_Array,
                                                        A_Tile_Info_Array );

    type Chunk is
        record
            chunkType : Chunk_Type := Chunk_Type'First;
            bmp       : A_Allegro_Bitmap := null;
            tileInfo  : Tile_Info;
        end record;

    type Chunk_Array is array (Integer range <>) of Chunk;
    type A_Chunk_Array is access all Chunk_Array;

    procedure Delete( obj : in out A_Chunk_Array );

end EgaGraph;
