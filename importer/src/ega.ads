--
-- Copyright (c) 2014 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Streams;                       use Ada.Streams;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;

package Ega is

    type Pixel_Format is (
        EGA_BGRI,         -- 4 planes: blue, green, red, intensity
        EGA_BGRIA         -- 5 planes: blue, green, red, intensity, alpha
    );

    -- Reads one or more EGA tiles from array 'data'. 'format' specifies the
    -- planes that compose each pixel in a tile. The planes are read in
    -- graphic-planar order. 'width' and 'height' specify the tile's dimensions,
    -- or, if 'data' contains more than one tile, the dimensions of every tile
    -- in the resultant bitmap. 'count' specifies the number of tiles to read
    -- frmo 'data', and 'columns' specifies the number of columns to draw when
    -- arranging multiple EGA tiles into the resultant bitmap.
    --
    -- EGA Graphic-Planar format:
    -- http://www.shikadi.net/moddingwiki/Raw_EGA_data#Graphic-planar_EGA_data
    --
    -- null will be returned if the width or height is 0, or if 'data' is empty.
    function Read_Ega_Graphic_Planar( data    : Stream_Element_Array;
                                      width   : Integer;
                                      height  : Integer;
                                      format  : Pixel_Format;
                                      count   : Positive := 1;
                                      columns : Positive := 1 ) return A_Allegro_Bitmap;
    pragma Precondition( width mod 8 = 0 );

end Ega;
