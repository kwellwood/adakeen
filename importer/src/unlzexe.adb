--
-- Copyright (c) 2014 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers;                    use Ada.Containers;
with Ada.Containers.Vectors;
with Ada.Unchecked_Conversion;

package body Unlzexe is

    function Is_Valid( header : Exe_Header ) return Boolean is
    begin
        return (header.mzId = "MZ" or else header.mzId = "ZM") and then
                header.overlayNum = 0 and then
                header.relocTableOffset = 16#1C#;
    end Is_Valid;

    --==========================================================================

    procedure Delete( exe : in out Exe_File ) is
    begin
        Delete( exe.data );
        exe.headerOffset := 0;
        exe.dataOffset := 0;
    end Delete;

    --==========================================================================

    type UInt16_Array is array (Natural range <>) of Unsigned_16;

    type Byte_Array is array (Natural range <>) of Unsigned_8;

    package Byte_Vectors is new Ada.Containers.Vectors( Natural, Unsigned_8, "=" );

    type Bitstream is
        record
            buf   : Unsigned_16;
            count : Unsigned_8;
        end record;

    sig90 : constant Stream_Element_Array := Stream_Element_Array'(
        16#06#, 16#0E#, 16#1F#, 16#8B#, 16#0E#, 16#0C#, 16#00#, 16#8B#,
        16#F1#, 16#4E#, 16#89#, 16#F7#, 16#8C#, 16#DB#, 16#03#, 16#1E#,
        16#0A#, 16#00#, 16#8E#, 16#C3#, 16#B4#, 16#00#, 16#31#, 16#ED#,
        16#FD#, 16#AC#, 16#01#, 16#C5#, 16#AA#, 16#E2#, 16#FA#, 16#8B#,
        16#16#, 16#0E#, 16#00#, 16#8A#, 16#C2#, 16#29#, 16#C5#, 16#8A#,
        16#C6#, 16#29#, 16#C5#, 16#39#, 16#D5#, 16#74#, 16#0C#, 16#BA#,
        16#91#, 16#01#, 16#B4#, 16#09#, 16#CD#, 16#21#, 16#B8#, 16#FF#,
        16#4C#, 16#CD#, 16#21#, 16#53#, 16#B8#, 16#53#, 16#00#, 16#50#,
        16#CB#, 16#2E#, 16#8B#, 16#2E#, 16#08#, 16#00#, 16#8C#, 16#DA#,
        16#89#, 16#E8#, 16#3D#, 16#00#, 16#10#, 16#76#, 16#03#, 16#B8#,
        16#00#, 16#10#, 16#29#, 16#C5#, 16#29#, 16#C2#, 16#29#, 16#C3#,
        16#8E#, 16#DA#, 16#8E#, 16#C3#, 16#B1#, 16#03#, 16#D3#, 16#E0#,
        16#89#, 16#C1#, 16#D1#, 16#E0#, 16#48#, 16#48#, 16#8B#, 16#F0#,
        16#8B#, 16#F8#, 16#F3#, 16#A5#, 16#09#, 16#ED#, 16#75#, 16#D8#,
        16#FC#, 16#8E#, 16#C2#, 16#8E#, 16#DB#, 16#31#, 16#F6#, 16#31#,
        16#FF#, 16#BA#, 16#10#, 16#00#, 16#AD#, 16#89#, 16#C5#, 16#D1#,
        16#ED#, 16#4A#, 16#75#, 16#05#, 16#AD#, 16#89#, 16#C5#, 16#B2#,
        16#10#, 16#73#, 16#03#, 16#A4#, 16#EB#, 16#F1#, 16#31#, 16#C9#,
        16#D1#, 16#ED#, 16#4A#, 16#75#, 16#05#, 16#AD#, 16#89#, 16#C5#,
        16#B2#, 16#10#, 16#72#, 16#22#, 16#D1#, 16#ED#, 16#4A#, 16#75#,
        16#05#, 16#AD#, 16#89#, 16#C5#, 16#B2#, 16#10#, 16#D1#, 16#D1#,
        16#D1#, 16#ED#, 16#4A#, 16#75#, 16#05#, 16#AD#, 16#89#, 16#C5#,
        16#B2#, 16#10#, 16#D1#, 16#D1#, 16#41#, 16#41#, 16#AC#, 16#B7#,
        16#FF#, 16#8A#, 16#D8#, 16#E9#, 16#13#, 16#00#, 16#AD#, 16#8B#,
        16#D8#, 16#B1#, 16#03#, 16#D2#, 16#EF#, 16#80#, 16#CF#, 16#E0#,
        16#80#, 16#E4#, 16#07#, 16#74#, 16#0C#, 16#88#, 16#E1#, 16#41#,
        16#41#, 16#26#, 16#8A#, 16#01#, 16#AA#, 16#E2#, 16#FA#, 16#EB#,
        16#A6#, 16#AC#, 16#08#, 16#C0#, 16#74#, 16#40#, 16#3C#, 16#01#,
        16#74#, 16#05#, 16#88#, 16#C1#, 16#41#, 16#EB#, 16#EA#, 16#89#
    );

    sig91 : constant Stream_Element_Array := Stream_Element_Array'(
        16#06#, 16#0E#, 16#1F#, 16#8B#, 16#0E#, 16#0C#, 16#00#, 16#8B#,
        16#F1#, 16#4E#, 16#89#, 16#F7#, 16#8C#, 16#DB#, 16#03#, 16#1E#,
        16#0A#, 16#00#, 16#8E#, 16#C3#, 16#FD#, 16#F3#, 16#A4#, 16#53#,
        16#B8#, 16#2B#, 16#00#, 16#50#, 16#CB#, 16#2E#, 16#8B#, 16#2E#,
        16#08#, 16#00#, 16#8C#, 16#DA#, 16#89#, 16#E8#, 16#3D#, 16#00#,
        16#10#, 16#76#, 16#03#, 16#B8#, 16#00#, 16#10#, 16#29#, 16#C5#,
        16#29#, 16#C2#, 16#29#, 16#C3#, 16#8E#, 16#DA#, 16#8E#, 16#C3#,
        16#B1#, 16#03#, 16#D3#, 16#E0#, 16#89#, 16#C1#, 16#D1#, 16#E0#,
        16#48#, 16#48#, 16#8B#, 16#F0#, 16#8B#, 16#F8#, 16#F3#, 16#A5#,
        16#09#, 16#ED#, 16#75#, 16#D8#, 16#FC#, 16#8E#, 16#C2#, 16#8E#,
        16#DB#, 16#31#, 16#F6#, 16#31#, 16#FF#, 16#BA#, 16#10#, 16#00#,
        16#AD#, 16#89#, 16#C5#, 16#D1#, 16#ED#, 16#4A#, 16#75#, 16#05#,
        16#AD#, 16#89#, 16#C5#, 16#B2#, 16#10#, 16#73#, 16#03#, 16#A4#,
        16#EB#, 16#F1#, 16#31#, 16#C9#, 16#D1#, 16#ED#, 16#4A#, 16#75#,
        16#05#, 16#AD#, 16#89#, 16#C5#, 16#B2#, 16#10#, 16#72#, 16#22#,
        16#D1#, 16#ED#, 16#4A#, 16#75#, 16#05#, 16#AD#, 16#89#, 16#C5#,
        16#B2#, 16#10#, 16#D1#, 16#D1#, 16#D1#, 16#ED#, 16#4A#, 16#75#,
        16#05#, 16#AD#, 16#89#, 16#C5#, 16#B2#, 16#10#, 16#D1#, 16#D1#,
        16#41#, 16#41#, 16#AC#, 16#B7#, 16#FF#, 16#8A#, 16#D8#, 16#E9#,
        16#13#, 16#00#, 16#AD#, 16#8B#, 16#D8#, 16#B1#, 16#03#, 16#D2#,
        16#EF#, 16#80#, 16#CF#, 16#E0#, 16#80#, 16#E4#, 16#07#, 16#74#,
        16#0C#, 16#88#, 16#E1#, 16#41#, 16#41#, 16#26#, 16#8A#, 16#01#,
        16#AA#, 16#E2#, 16#FA#, 16#EB#, 16#A6#, 16#AC#, 16#08#, 16#C0#,
        16#74#, 16#34#, 16#3C#, 16#01#, 16#74#, 16#05#, 16#88#, 16#C1#,
        16#41#, 16#EB#, 16#EA#, 16#89#, 16#FB#, 16#83#, 16#E7#, 16#0F#,
        16#81#, 16#C7#, 16#00#, 16#20#, 16#B1#, 16#04#, 16#D3#, 16#EB#,
        16#8C#, 16#C0#, 16#01#, 16#D8#, 16#2D#, 16#00#, 16#02#, 16#8E#,
        16#C0#, 16#89#, 16#F3#, 16#83#, 16#E6#, 16#0F#, 16#D3#, 16#EB#,
        16#8C#, 16#D8#, 16#01#, 16#D8#, 16#8E#, 16#D8#, 16#E9#, 16#72#
    );

    function To_Int16 is new Ada.Unchecked_Conversion( Unsigned_16, Integer_16 );

    ----------------------------------------------------------------------------

    procedure Decompress( input : Stream_Element_Array; output : in out Exe_File ) is
        inHead    : Exe_Header;
        outHead   : Exe_Header;
        inf       : UInt16_Array(0..7);
        outData   : Byte_Vectors.Vector;
        imageSize : Integer := 0;

        ------------------------------------------------------------------------

        -- Checks if this is an LZEXE compressed file. Returns the LZEXE version
        -- used to compress the image, or 0 if the version was not recognized.
        function Read_Header return Integer is
            inHeadBytes : Stream_Element_Array(0..EXE_HEADER_SIZE-1);
            for inHeadBytes'Address use inHead'Address;
            outHeadBytes : Stream_Element_Array(inHeadBytes'Range);
            for outHeadBytes'Address use outHead'Address;
            sigPos : Stream_Element_Offset;
        begin
            if input'Length < inHeadBytes'Length then
                return 0;
            end if;

            inHeadBytes := input(input'First..(input'First + inHeadBytes'Length - 1));
            outHeadBytes := inHeadBytes;
            if not Is_Valid( inHead ) then
                -- EXE header not recognized
                return 0;
            end if;

            sigPos := Stream_Element_Offset(Shift_Left( Unsigned_32(inHead.headerSize + inHead.cs), 4 ) + Unsigned_32(inHead.ip));
            if sigPos < input'First or input'Last < sigPos + sig90'Length - 1 then
                -- Bad file signature location
                return 0;
            end if;

            for i in sig90'Range loop
                exit when sig90(i) /= input(input'First + sigPos + (i - sig90'First));
                if i = sig90'Last then
                    return 90;
                end if;
            end loop;

            for i in sig91'Range loop
                exit when sig91(i) /= input(input'First + sigPos + (i - sig91'First));
                if i = sig91'Last then
                    return 91;
                end if;
            end loop;

            return 0;
        end Read_Header;

        ------------------------------------------------------------------------

        function GetWord( inPos : Stream_Element_Offset ) return Unsigned_16 is
            value : Unsigned_16;
        begin
            value := Unsigned_16(input(input'First + inPos));
            value := value + Shift_Left( Unsigned_16(input(input'First + inPos + 1)), 8 );
            return value;
        end GetWord;

        ------------------------------------------------------------------------

        procedure PutWord( value : Unsigned_16 ) is
        begin
            outData.Append( Unsigned_8(value and 16#00FF#) );
            outData.Append( Unsigned_8(Shift_Right( value, 8 )) );
        end PutWord;

        ------------------------------------------------------------------------

        -- Makes the new relocation table.
        procedure Make_Relocation_Table( version : Integer; success : out Boolean ) is

            -- After the header, there follow the relocation items, which are
            -- used to span multiple segments. The relocation items have the
            -- following format:
            --
            -- OFFSET   Count  TYPE    Description
            -- 0000h        1  uint16  Offset within segment
            -- 0002h        1  uint16  Segment of relocation
            --
            -- To get the position of the relocation within the file, you have
            -- to compute the physical address from the segment:offset pair,
            -- which is done by multiplying the segment by 16 and adding the
            -- offset and then adding the offset of the binary start. Note that
            -- the raw binary code starts on a paragraph boundary within the
            -- executable file. All segments are relative to the start of the
            -- executable in memory, and this value must be added to every
            -- segment if relocation is done manually.

            --------------------------------------------------------------------

            -- Create relocation table from LZEXE ver 0.90.
            function Reloc90( inPos : Stream_Element_Offset ) return Boolean is
                numEntries : Unsigned_16 := 0;
                segment    : Unsigned_16 := 0;
                offset     : Unsigned_16;
                c          : Unsigned_16;
                inPos2     : Stream_Element_Offset := inPos + 16#19D#;
            begin
                loop
                    c := GetWord( inPos2 );
                    inPos2 := inPos2 + 2;

                    while c > 0 loop
                        offset := GetWord( inPos2 );
                        inPos2 := inPos2 + 2;

                        PutWord( offset );
                        PutWord( segment );
                        numEntries := numEntries + 1;

                        c := c - 1;
                    end loop;
                    segment := segment + 16#1000#;

                    exit when segment = 16#F000# + 16#1000#;
                end loop;

                outHead.numRelocEntries := numEntries;
                return True;
            end Reloc90;

            --------------------------------------------------------------------

            -- Create relocation table from LZEXE ver 0.91.
            function Reloc91( inPos : Stream_Element_Offset ) return Boolean is
                numEntries : Unsigned_16 := 0;
                segment    : Unsigned_16 := 0;
                offset    : Unsigned_16 := 0;
                span       : Unsigned_16;
                temp       : Unsigned_16;
                inPos2     : Stream_Element_Offset;
            begin
                inPos2 := inPos + 16#158#;  -- compressed relocation table address = 16#158#

                loop
                    <<continue>>
                    temp := Unsigned_16(input(input'First + inPos2));
                    inPos2 := inPos2 + 1;
                    span := temp;
                    if span = 0 then
                        span := Unsigned_16(input(input'First + inPos2));
                        inPos2 := inPos2 + 1;
                        span := span + Shift_Left( Unsigned_16(input(input'First + inPos2)), 8 );
                        inPos2 := inPos2 + 1;
                        if span = 0 then
                            segment := segment + 16#0FFF#;
                            goto continue;
                        elsif span = 1 then
                            exit;
                        end if;
                    end if;

                    offset := offset + span;
                    segment := segment + Shift_Right( offset and (not Unsigned_16(16#0F#)), 4 );
                    offset := offset and 16#0F#;
                    PutWord( offset );
                    PutWord( segment );

                    numEntries := numEntries + 1;
                end loop;

                outHead.numRelocEntries := numEntries;
                return True;
            end Reloc91;

            --------------------------------------------------------------------

            infBytes : Stream_Element_Array(0..inf'Length*2-1);
            for infBytes'Address use inf'Address;
            inPos   : Stream_Element_Offset;
            pad     : Stream_Element_Offset;
        begin
            inPos := Stream_Element_Offset(Shift_Left( Unsigned_32(inHead.headerSize + inHead.cs), 4 ));   -- goto CS:0000
            infBytes := input((input'First + inPos) .. (input'First + inPos + infBytes'Length - 1));
            outHead.ip := inf(0);
            outHead.cs := inf(1);
            outHead.sp := inf(2);
            outHead.ss := inf(3);
                       -- inf(4) : size of compressed load module (paragraphs)
                       -- inf(5) : increase of load module size (paragraphs)
                       -- inf(6) : size of decompressor with compressed relocation table (bytes)
                       -- inf(7) : checksum of decompressor with compressed relocation table (v 0.90)
            outHead.relocTableOffset := 16#1C#;    -- offset of relocation table

            -- reserve header and extra space up to the relocation table
            outData.Append( 0, 16#1C# );

            case version is
                when 90 => success := Reloc90( inPos );
                when 91 => success := Reloc91( inPos );
                when others => success := False;
            end case;

            if not success then
                -- error in relocation table
                return;
            end if;

            -- header size in paragraphs (512 bytes each)
            inPos := Stream_Element_Offset(outData.Length);
            pad := 16#200# - (inPos mod 16#200#);
            outHead.headerSize := Unsigned_16(Shift_Right( Unsigned_32(inPos + pad), 4 ));

            while pad > 0 loop
                outData.Append( 0 );
                pad := pad - 1;
            end loop;

            success := True;
        end Make_Relocation_Table;

        ------------------------------------------------------------------------

        procedure Unpack is

            --------------------------------------------------------------------

            -- Initializes the first 16 bits in the stream.
            procedure Init_Bits( p     : in out Bitstream;
                                 inPos : in out Stream_Element_Offset ) is
            begin
                p.count := p.buf'Size;
                p.buf := GetWord( inPos );
                inPos := inPos + 2;
            end Init_Bits;

            --------------------------------------------------------------------

            -- Gets the next bit from the compressed data.
            function Get_Bit( p     : access Bitstream;
                              inPos : access Stream_Element_Offset ) return Unsigned_16 is
                bit : constant Unsigned_16 := p.buf and 16#0001#;
            begin
                p.count := p.count - 1;
                if p.count = 0 then
                    p.buf := GetWord( inPos.all );
                    inPos.all := inPos.all + 2;
                    p.count := 16#10#;
                else
                    p.buf := Shift_Right( p.buf, 1 );
                end if;
                return bit;
            end Get_Bit;

            --------------------------------------------------------------------

            BUF_SIZE      : constant := 16#4500#;
            BUF_CHUNK     : constant := 16#2000#;

            len           : Unsigned_16;
            span          : Unsigned_16;
            bits          : aliased Bitstream;
            buffer        : Byte_Array(0..BUF_SIZE-1) := (others => 0);
            bufPos        : Integer := buffer'First;
            inPos         : aliased Stream_Element_Offset;
            outPos        : Integer;
            initialOutPos : Integer;
        begin
            inPos := Stream_Element_Offset(Shift_Left( Unsigned_32(inHead.headerSize) + Unsigned_32(inHead.cs) - Unsigned_32(inf(4)), 4 ));
            outPos := Integer(Shift_Left( Unsigned_32(outHead.headerSize), 4 ));
            initialOutPos := outPos;
            Init_Bits( bits, inPos );

            loop
                <<continue>>
                if bufPos > BUF_CHUNK * 2 then
                    if Integer(outData.Length) < outPos + BUF_CHUNK then
                        outData.Reserve_Capacity( Count_Type(outPos + BUF_CHUNK) );
                    end if;
                    for i in 0..(BUF_CHUNK - 1) loop
                        outData.Append( buffer(i) );
                    end loop;
                    outPos := outPos + BUF_CHUNK;
                    bufPos := bufPos - BUF_CHUNK;
                    buffer(0..(bufPos - 1)) := buffer(BUF_CHUNK..(BUF_CHUNK + bufPos - 1));
                    -- todo: update progress here
                end if;

                if Get_Bit( bits'Access, inPos'Access ) > 0 then
                    buffer(bufPos) := Unsigned_8(input(input'First + inPos));
                    bufPos := bufPos + 1;
                    inPos := inPos + 1;
                    goto continue;
                end if;

                if Get_Bit( bits'Access, inPos'Access ) = 0 then
                    len := Shift_Left( Get_Bit( bits'Access, inPos'Access ), 1 );
                    len := len or Get_Bit( bits'Access, inPos'Access );
                    len := len + 2;
                    span := Unsigned_16(input(input'First + inPos)) or 16#FF00#;  -- 'span' becomes negative
                    inPos := inPos + 1;
                else
                    span := Unsigned_16(input(input'First + inPos));
                    inPos := inPos + 1;
                    len := Unsigned_16(input(input'First + inPos));
                    inPos := inPos + 1;
                    span := span or Shift_Left( len and not 16#07#, 5 ) or 16#E000#;
                    len := (len and 16#07#) + 2;
                    if len = 2 then
                        len := Unsigned_16(input(input'First + inPos));
                        inPos := inPos + 1;

                        if len = 0 then
                            exit;           -- end mark of compressed load module
                        elsif len = 1 then
                            goto continue;  -- segment change
                        else
                            len := len + 1;
                        end if;
                    end if;
                end if;

                while len > 0 loop
                    buffer(bufPos) := buffer(bufPos + Integer(To_Int16( span )));
                    len := len - 1;
                    bufPos := bufPos + 1;
                end loop;
            end loop;

            if bufPos /= 0 then
                if Integer(outData.Capacity) < outPos + bufPos then
                    outData.Reserve_Capacity( Count_Type(outPos + bufPos) );
                end if;
                for i in 0..(bufPos - 1) loop
                    outData.Append( buffer(i) );
                end loop;
                outPos := outPos + bufPos;
            end if;

            imageSize := outPos - initialOutPos;
        end Unpack;

        ------------------------------------------------------------------------

        -- Writes the uncompressed .EXE header.
        procedure Write_Header is
            outHeadBytes : Stream_Element_Array(0..EXE_HEADER_SIZE-1);
            for outHeadBytes'Address use outHead'Address;
        begin
            if inHead.maxParagraphs > 0 then
                outHead.minParagraphs := outHead.minParagraphs - (inf(5) + Shift_Right( inf(6) + 16 - 1, 4 ) + 9);
                if inHead.maxParagraphs < 16#FFFF# then
                    outHead.maxParagraphs := outHead.maxParagraphs - (inHead.minParagraphs - outHead.minParagraphs);
                end if;
            end if;

            -- number of bytes in last 512-byte paragraph
            outHead.imageSizeLo := Unsigned_16((Unsigned_32(imageSize) + Shift_Left( Unsigned_32(outHead.headerSize), 4 )) and 16#1FF#);

            -- total number of 512-byte paragraphs in executable (including last)
            outHead.imageSizeHi := Unsigned_16(Shift_Right( Unsigned_32(imageSize) + Shift_Left( Unsigned_32(outHead.headerSize), 4 ) + 16#1FF#, 9 ));

            -- write the header at the front of the output buffer
            for i in 0..outHeadBytes'Length-1 loop
                outData.Replace_Element( i, Unsigned_8(outHeadBytes(Stream_Element_Offset(i))) );
            end loop;
        end Write_Header;

        ------------------------------------------------------------------------

        version : Integer := 0;
        success : Boolean;
    begin
        Delete( output );

        version := Read_Header;
        if version = 0 then
             -- LZEXE format not detected
            return;
        end if;

        Make_Relocation_Table( version, success );
        if not success then
            return;
        end if;

        output.headerOffset := 0;
        output.dataOffset := Integer(outData.Length);

        Unpack;

        Write_Header;

        output.data := new Stream_Element_Array(0..Stream_Element_Offset(outData.Length - 1));
        for i in output.data'Range loop
            output.data(i) := Stream_Element(outData.Element( Integer(i) ));
        end loop;
    end Decompress;

end Unlzexe;
