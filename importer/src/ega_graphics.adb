
package body Ega_Graphics is

    procedure Delete( obj : in out A_Chunk_Array ) is
        procedure Free is new Ada.Unchecked_Deallocation( Chunk_Array, A_Chunk_Array );
    begin
        if obj /= null then
            for i in obj'Range loop
                Delete( obj(i).raw );
                Al_Destroy_Bitmap( obj(i).bmp );
            end loop;
            Free( obj );
        end if;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Delete( obj : in out A_EgaGraphics ) is
        procedure Free is new Ada.Unchecked_Deallocation( EgaGraphics, A_EgaGraphics );
    begin
        Delete( obj.header );
        Delete( obj.chunks );
        Delete( obj.tileProps );
        Delete( obj.tileMaskProps );
        Free( obj );
    end Delete;

end Ega_Graphics;
