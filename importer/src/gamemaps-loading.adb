--
-- Copyright (c) 2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Streams;                       use Ada.Streams;
with Applications.Shell.Importer;       use Applications.Shell.Importer;
with Carmack;                           use Carmack;
with Debugging;                         use Debugging;
with Interfaces;                        use Interfaces;
with Resources;                         use Resources;
with RLEW;                              use RLEW;
with Streams;                           use Streams;
with Support.Paths;                     use Support.Paths;
with System.Storage_Elements;           use System.Storage_Elements;

package body GameMaps.Loading is

    type U32_Array is array (Integer range <>) of Unsigned_32;

    -- Contains the list of GAMEMAPS file offsets of each chunk
    type MapHead is
        record
            offsets : U32_Array(0..99);
            rleWord : Unsigned_16 := 16#FEFE#;
        end record;
    type A_MapHead is access all MapHead;

    procedure Delete is new Ada.Unchecked_Deallocation( MapHead, A_MapHead );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Level_Header is
        record
            layer0Data : Unsigned_32;        -- offset in the file (bytes)
            layer1Data : Unsigned_32;        --
            layer2Data : Unsigned_32;        --
            layer0Size : Unsigned_16;        -- compressed size (bytes)
            layer1Size : Unsigned_16;        --
            layer2Size : Unsigned_16;        --
            width      : Unsigned_16;        -- width in tiles
            height     : Unsigned_16;        -- height in tiles
            name       : String(1..16);
          --signature  : String(1..4);       -- expected to be "!ID!" (optional)
        end record;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Loads the MAPHEAD header information from the 'game' exe or from disk.
    -- null will be returned if a valid MAPHEAD could not be found.
    function Load_MapHead( game : not null A_Game_Exe ) return A_MapHead;

    -- 'res' is the file resource containing the data. 'offset' is the address
    -- of the map's Level_Header structure within the file. 'rleWord' is the
    -- RLEW tag word for a repeat sequence. null will be returned if the map
    -- could not be loaded.
    function Load_Game_Map( res     : not null A_Resource_File;
                            offset  : Unsigned_32;
                            rleWord : Unsigned_16 ) return A_Game_Map;

    function Read_U16( data : Stream_Element_Array; offset : SEO ) return Unsigned_16;

    function Read_U32( data : Stream_Element_Array; offset : SEO ) return Unsigned_32;

    --==========================================================================

    function Load_GameMaps( game : not null A_Game_Exe ) return A_Game_Maps_Array is
        header : A_MapHead;
        res    : A_Resource_File;
        levels : A_Game_Maps_Array;
    begin
        -- Step 1: Load MAPHEAD from disk or the .exe
        header := Load_MapHead( game );
        if header = null then
            Output_Error( game.info.mapHeadFile.all & " not found" );
            return null;
        end if;

        -- Step 2: Load GAMEMAPS from disk
        res := Load_Resource( Get_Directory( To_String( game.path ) ) & game.info.gameMapsFile.all, "" );
        if res = null then
            Output_Error( game.info.gameMapsFile.all & " not found" );
            Delete( header );
            return null;
        end if;

        -- Step 3: Load each level from GAMEMAPS
        levels := new Game_Maps_Array;
        for i in header.offsets'Range loop
            if header.offsets(i) /= 16#FFFFFFFF# then
                if header.offsets(i) + Level_Header'Size / 8 <= res.Size then
                    levels(i) := Load_Game_Map( res, header.offsets(i), header.rleWord );
                    if levels(i) /= null then
                        Dbg( "Read level" & i'Img & ": " & To_String( levels(i).name ) );
                    else
                        Output_Error( "Failed to read data for level" & i'Img );
                    end if;
                else
                    Output_Error( "Missing data for level" & i'Img & " at offset" & header.offsets(i)'Img );
                end if;
            end if;
        end loop;

        Delete( header );
        Delete( res );
        return levels;
    end Load_GameMaps;

    ----------------------------------------------------------------------------

    function Load_MapHead( game : not null A_Game_Exe ) return A_MapHead is
        path : constant String := Get_Directory( To_String( game.path ) ) & game.info.mapHeadFile.all;

        ------------------------------------------------------------------------

        -- Reads the MAPHEAD data from 'data'. If 'data' is less than the
        -- expected 402 bytes, or it doesn't contain the expected magic word,
        -- then null will be returned.
        --
        -- Format:
        --
        -- UINT16LE          RLEW magic word (always 16#ABCD#)
        -- UINT32LE[0..99]   Offset of level headers in GAMEMAPS file
        function Read_Data( data : Stream_Element_Array ) return A_MapHead is
            header : A_MapHead := null;
            offset : Unsigned_32;
            pos    : SEO;
        begin
            if data'Length >= MapHead'Size / 8 then
                pos := data'First;

                header := new MapHead;
                header.rleWord := Read_U16( data, pos );
                pos := pos + 2;

                for i in header.offsets'Range loop
                    offset := Read_U32( data, pos );
                    if offset = 0 then
                        header.offsets(i) := 16#FFFFFFFF#;  -- level is undefined
                    else
                        header.offsets(i) := offset;
                    end if;
                    pos := pos + 4;
                end loop;
            else
                Dbg( "Bad file size: Ignoring " & path );
            end if;
            return header;
        end Read_Data;

        ------------------------------------------------------------------------

        header : A_MapHead;
        res    : A_Resource_File;
    begin
        -- look for a file on disk first
        res := Load_Resource( path, "" );
        if res /= null then
            declare
                data : Stream_Element_Array(0..SEO(res.Size-1));
                for data'Address use res.Get_Address;
            begin
                header := Read_Data( data );
                if header /= null then
                    Dbg( "Loaded " & path );
                end if;
            end;
            Delete( res );
        end if;

        -- check the suspected location in the .exe
        if header = null then
            header := Read_Data( game.data(game.info.mapHeadOffset..game.data'Last) );
            if header /= null then
                Dbg( "Loaded " & game.info.mapHeadFile.all & " from EXE" );
            end if;
        end if;

        return header;
    end Load_MapHead;

    ----------------------------------------------------------------------------

    function Load_Game_Map( res     : not null A_Resource_File;
                            offset  : Unsigned_32;
                            rleWord : Unsigned_16 ) return A_Game_Map is

        header : Level_Header;
        for header'Address use res.Get_Address + Storage_Offset(offset);

        ------------------------------------------------------------------------

        function Ada_String( str : String ) return String is
        begin
            for i in str'Range loop
                if str(i) = ASCII.NUL then
                    return str(str'First..i-1);
                end if;
            end loop;
            return str;
        end Ada_String;

        ------------------------------------------------------------------------

        function Decompress_Layer( offset : Unsigned_32; length : Unsigned_16 ) return A_Tile_Array is
            data  : Stream_Element_Array(0..SEO(length)-1);
            for data'Address use res.Get_Address + Storage_Offset(offset);
            rle   : A_SEA;
            raw   : A_SEA;
            tiles : A_Tile_Array := null;
        begin
            rle := Carmack.Expand( data );
            if rle /= null then
                raw := RLEW.Expand( rle.all, rleWord );
                if raw /= null then
                    tiles := new Tile_Array(0..raw'Length/2-1);
                    for i in tiles'Range loop
                        tiles(i) := Natural(Unsigned_16(raw(raw'First + SEO(i - tiles'First) * 2    )) or
                                Shift_Left( Unsigned_16(raw(raw'First + SEO(i - tiles'First) * 2 + 1)), 8 ));
                    end loop;
                --else
                --    Output_Error( "Error in map data (RLEW decompression failed)" );
                end if;
            --else
            --    Output_Error( "Error in map data (Carmack decompression failed)" );
            end if;

            Delete( rle );
            Delete( raw );

            return tiles;
        end Decompress_Layer;

        ------------------------------------------------------------------------

        level : A_Game_Map := new Game_Map;
    begin
        level.name := To_Unbounded_String( Ada_String( header.name ) );
        level.width := Integer(header.width);
        level.height := Integer(header.height);

        level.layer0 := Decompress_Layer( header.layer0Data, header.layer0Size );
        level.layer1 := Decompress_Layer( header.layer1Data, header.layer1Size );
        level.layer2 := Decompress_Layer( header.layer2Data, header.layer2Size );

        if level.layer0 = null or else level.layer0'Length /= level.width * level.height or else
           level.layer1 = null or else level.layer1'Length /= level.width * level.height or else
           level.layer2 = null or else level.layer2'Length /= level.width * level.height
        then
            Delete( level );
        end if;

        return level;
    end Load_Game_Map;

    ----------------------------------------------------------------------------

    function Read_U16( data : Stream_Element_Array; offset : SEO ) return Unsigned_16
    is (Unsigned_16(data(offset)) or Shift_Left( Unsigned_16(data(offset + 1)), 8 ));

    ----------------------------------------------------------------------------

    function Read_U32( data : Stream_Element_Array; offset : SEO ) return Unsigned_32
    is (Unsigned_32(data(offset)) or
        Shift_Left( Unsigned_32(data(offset + 1)),  8 ) or
        Shift_Left( Unsigned_32(data(offset + 2)), 16 ) or
        Shift_Left( Unsigned_32(data(offset + 3)), 24 ));

end GameMaps.Loading;
