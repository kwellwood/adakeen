
package body Huffman is

    procedure Expand( input  : Stream_Element_Array;
                      output : in out Stream_Element_Array;
                      last   : out Stream_Element_Offset;
                      dict   : Dictionary ) is
        inPos    : Stream_Element_Offset := input'First;
        curNode  : Unsigned_16;
        nextNode : Unsigned_16;
        c, mask  : Unsigned_8;
    begin
        last := output'First;
        curNode := 254;   -- head node
        loop
            mask := 1;
            c := Unsigned_8(input(inPos));
            inPos := inPos + 1;

            loop
                if (c and mask) /= 0 then
                    nextNode := dict(Integer(curNode)).bit1;
                else
                    nextNode := dict(Integer(curNode)).bit0;
                end if;

                if nextNode < 256 then
                    -- write a byte and go back to the head node
                    output(last) := Stream_Element(nextNode);
                    last := last + 1;
                    curNode := 254;
                else
                    -- move to the next node
                    curNode := nextNode and 16#FF#;
                end if;

                -- move to the next bit
                mask := Shift_Left( mask, 1 );

                exit when last > output'Last or else mask = 0;
            end loop;

            exit when inPos > input'Last or else last > output'Last;
        end loop;

        last := last - 1;      -- return the last byte actually written
    end Expand;

end Huffman;
