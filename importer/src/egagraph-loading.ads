--
-- Copyright (c) 2014-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Game_Support;                      use Game_Support;

package EgaGraph.Loading is

    -- Loads the bitmaps from an Apogee game's EGAGRAPH graphics file format
    -- using the game's executable 'game'. The game's accompanying EGAGRAPH file
    -- and optional EGADICT and EGAHEAD files are expected to be in the same
    -- directory as the executable. If an error occurs, null will be returned
    -- and error messages will be printed to standard error output.
    function Load_EgaGraph( game : not null A_Game_Exe ) return A_Chunk_Array;

end EgaGraph.Loading;
