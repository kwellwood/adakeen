--
-- Copyright (c) 2014-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Script_VMs;                        use Script_VMs;

package Applications.Shell.Importer is

    function Create return A_Application;

    -- Writes a line of text to standard output unless it's suppressed by the
    -- command line switches.
    procedure Output( str : String );

    -- Writes a line of text to the standard error output.
    procedure Output_Error( str : String );

private

    type Importer_Application is new Shell_Application with
        record
            gameVM : A_Script_VM;
        end record;

    function Initialize( this : access Importer_Application ) return Boolean;

    procedure Finalize( this : access Importer_Application );

    function Main( this : access Importer_Application ) return Integer;

end Applications.Shell.Importer;
