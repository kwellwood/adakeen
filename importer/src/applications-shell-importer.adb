--
-- Copyright (c) 2014-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Directories;                   use Ada.Directories;
with Ada.Exceptions;                    use Ada.Exceptions;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Ada.Text_IO;                       use Ada.Text_IO;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Assets;                            use Assets;
with Assets.Libraries;                  use Assets.Libraries;
with Assets.Scripts;
with Debugging;                         use Debugging;
with EgaGraph;                          use EgaGraph;
with EgaGraph.Loading;                  use EgaGraph.Loading;
with Entities.Factory;                  use Entities.Factory;
with Entities.Galaxy;
with Game_Scripting;                    use Game_Scripting;
with Game_Support;                      use Game_Support;
with GameMaps;                          use GameMaps;
with GameMaps.Loading;                  use GameMaps.Loading;
with GNAT.OS_Lib;                       use GNAT.OS_Lib;
with Interfaces;                        use Interfaces;
with Galaxy_API;
with Preferences;                       use Preferences;
with Support.Paths;                     use Support.Paths;
with Values;                            use Values;
with Values.Construction;               use Values.Construction;
with Values.Lists;                      use Values.Lists;
with Values.Maps;                       use Values.Maps;
with Values.Strings;                    use Values.Strings;
with Worlds;                            use Worlds;

package body Applications.Shell.Importer is

    -- Improvements:
    -- [ ] Use the foreground layer- detect which tiles are drawn on top of Keen
    --     in the middleground and duplicate them into the foreground.
    -- [ ] Support spawning entities
    -- [ ] Allow the mapping from tile behavior numbers to entity template names
    --     to be governed by a Scribble map value.

    -- Wishlist:
    -- [ ] Support external tile properties in .TLI files for mods
    -- [ ] Add and test support for Keen 5 & 6
    -- [ ] Wishlist- Bio Menace, Monster Bash, Duke Nukem 1, Cosmo, Keen 1,2,3

    -- Look for media in the current working directory
    MEDIA_DIR : constant String := Execution_Directory & ".." & Slash & "media" & Slash;

    quiet : Boolean := False;

    ----------------------------------------------------------------------------

    procedure Output( str : String ) is
    begin
        if not quiet then
            Ada.Text_IO.Put_Line( Standard_Output, str );
        end if;
    end Output;

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create return A_Application is
        this : constant A_Shell_Application := new Importer_Application;
    begin
        this.Construct( company => "Kevin Wellwood",
                        name    => "importer",
                        userDir => "" );            -- use working directory
        this.configFile := To_Unbounded_String( "" );
        return A_Application(this);
    end Create;

    ----------------------------------------------------------------------------

    overriding
    function Initialize( this : access Importer_Application ) return Boolean is
    begin
        if not Shell_Application(this.all).Initialize then
            return False;
        end if;

        Dbg("");
        Dbg( this.Get_Announce );
        Dbg("");

        -- - - - Initialize Tile Libraries - - - --

        Assets.Libraries.Initialize( null );

        -- - - - Initialize Entity Templates - - - --

        -- Scribble Scripting
        this.gameVM := Create_Game_VM;
        Assets.Scripts.Initialize( this.gameVM.Get_Runtime );
        Galaxy_API.Initialize( this.gameVM );

        -- Components and Entities
        Entities.Factory.Global := Create_Entity_Factory;
        Entities.Galaxy.Define_Components( Entities.Factory.Global );
        Entities.Factory.Global.Load_Component_Definitions;
        Entities.Factory.Global.Load_Entity_Templates;

        return True;
    end Initialize;

    ----------------------------------------------------------------------------

    overriding
    procedure Finalize( this : access Importer_Application ) is
    begin
        Delete( Entities.Factory.Global );
        Assets.Unload_All( Script_Assets );
        Assets.Scripts.Finalize;
        Delete( this.gameVM );

        Assets.Unload_All( Library_Assets );
        Assets.Libraries.Finalize;
        Shell_Application(this.all).Finalize;
    end Finalize;

    ----------------------------------------------------------------------------

    overriding
    function Main( this : access Importer_Application ) return Integer is

        overwrite : Boolean := False;

        ------------------------------------------------------------------------

        procedure Display_Usage is
        begin
            Put_Line( "Usage:" );
            Put_Line( "  " & this.Get_Name & " <exe-path> <lib-name> [-tiles] [-levels <prefix>] [-level <num> <filename>] [Switches]" );
            New_Line;
            Put_Line( "  Where:" );
            Put_Line( "    <exe-path> is path of the game executable" );
            Put_Line( "    <lib-name> is the name of the tile library to create/use");
            New_Line;
            Put_Line( "  Operations:" );
            Put_Line( "    -tiles  : Extract the graphics to a tile library (requires tlzpack)");
            Put_Line( "    -levels : Extract all levels to <prefix>XX.world" );
            Put_Line( "    -level  : Extract level #<num> to <filename>");
            New_Line;
            Put_Line( "  Switches:" );
            Put_Line( "    -ow     : Overwrite existing files and directories" );
            Put_Line( "    -q      : Quiet Mode, suppress all informational output" );
            New_Line;
            Put_Line( "  All extracted files will be written to the appropriate location in the media folder" );
            New_Line;
        end Display_Usage;

        ------------------------------------------------------------------------

        function To_Integer( str : String ) return Integer is
        begin
            for i in str'Range loop
                if not Is_Digit( str(i) ) then
                    return -1;
                end if;
            end loop;
            return Integer'Value( str );
        end To_Integer;

        ------------------------------------------------------------------------

        function To_Tile_Attributes( chunkIndex : Integer; tileInfo : Tile_Info ) return String is
            attrs : constant Map_Value := Create_Map.Map;
        begin
            -- animation attributes
            if tileInfo.animationTime > 0 then
                attrs.Set( "delay", Create( Integer(tileInfo.animationTime) ) );
            end if;
            if tileInfo.nextTile /= 0 then
                attrs.Set( "next", Create( Integer(tileInfo.nextTile + chunkIndex) ) );
            end if;

            -- clipping shape attributes
            if tileInfo.edgeTop = 1 and tileInfo.edgeBottom = 0 then
                attrs.Set( "shape", Create( "oneway" ) );
            elsif tileInfo.edgeTop = 1 and tileInfo.edgeBottom = 1 then
                attrs.Set( "shape", Create( "wall" ) );
            elsif tileInfo.edgeTop = 2 then
                attrs.Set( "shape", Create( "slope_22_down_floor_wide" ) );
            elsif tileInfo.edgeTop = 3 then
                attrs.Set( "shape", Create( "slope_22_down_floor_thin" ) );
            elsif tileInfo.edgeTop = 4 then
                attrs.Set( "shape", Create( "slope_45_down_floor" ) );
            elsif tileInfo.edgeTop = 5 then
                attrs.Set( "shape", Create( "slope_22_up_floor_wide" ) );
            elsif tileInfo.edgeTop = 6 then
                attrs.Set( "shape", Create( "slope_22_up_floor_thin" ) );
            elsif tileInfo.edgeTop = 7 then
                attrs.Set( "shape", Create( "slope_45_up_floor" ) );
            elsif tileInfo.edgeTop = 9 then
                attrs.Set( "action", Create( "hazard" ) );
            elsif tileInfo.edgeBottom = 2 then
                attrs.Set( "shape", Create( "slope_22_up_ceiling_wide" ) );
            elsif tileInfo.edgeBottom = 3 then
                attrs.Set( "shape", Create( "slope_22_up_ceiling_thin" ) );
            elsif tileInfo.edgeBottom = 4 then
                attrs.Set( "shape", Create( "slope_45_up_ceiling" ) );
            elsif tileInfo.edgeBottom = 5 then
                attrs.Set( "shape", Create( "slope_22_down_ceiling_wide" ) );
            elsif tileInfo.edgeBottom = 6 then
                attrs.Set( "shape", Create( "slope_22_down_ceiling_thin" ) );
            elsif tileInfo.edgeBottom = 7 then
                attrs.Set( "shape", Create( "slope_45_down_ceiling" ) );
            end if;

            if tileInfo.behavior = 3 then
                attrs.Set( "action", Create( "hazard" ) );
            elsif tileInfo.behavior /= 0 then
                attrs.Set( "behavior", Create( Integer(tileInfo.behavior) ) );
            end if;

            if not attrs.Is_Empty then
                return Image( attrs );
            end if;
            return "";
        end To_Tile_Attributes;

        ------------------------------------------------------------------------

        function Extract_Graphics( game    : not null A_Game_Exe;
                                   libName : String ) return Boolean is
            dir : constant String := Get_Pref( "application", "media" ) &
                                     "graphics" & Slash & libName & Slash;
            chunks : A_Chunk_Array;
        begin
            -- Load bitmaps and tile info from the exe and support files
            chunks := Load_EgaGraph( game );
            if chunks = null then
                return False;
            end if;
            Dbg( "Successfully loaded graphics from " & To_String( game.path ) );

            -- Create tile library directory
            if Is_Directory( dir ) then
                if overwrite then
                    begin
                        Ada.Directories.Delete_Tree( dir );
                    exception
                        when others => null;
                    end;
                else
                    Output_Error( "Tile library already exists: " & dir );
                    Delete( chunks );
                    return False;
                end if;
            end if;

            if not Is_Directory( dir ) then
                -- Make the directory
                if not Make_Dir( dir ) then
                    Output_Error( "Failed to create " & dir );
                    Delete( chunks );
                    return False;
                end if;

                -- Write bitmaps
                for i in chunks'Range loop
                    if chunks(i).bmp /= null then
                        if not Al_Save_Bitmap( Normalize_Pathname( dir & "chunk" & Tail( Trim( i'Img, Left ), 4, '0' ) & ".png" ), chunks(i).bmp ) then
                            Output_Error( "Failed to write file: chunk" & Tail( Trim( i'Img, Left ), 4, '0' ) & ".png" );
                            Delete( chunks );
                            return False;
                        end if;
                    end if;
                end loop;

                -- Write tile library index
                declare
                    outFile : File_Type;

                    procedure List_Chunks( chunkType : Chunk_Type; header : String ) is
                        column : Integer := 0;
                    begin
                        Put_Line( outFile, "" );
                        Put_Line( outFile, "// " & header );
                        for i in chunks'Range loop
                            if chunks(i).chunkType = chunkType then
                                if chunks(i).bmp /= null then
                                    Put_Line( outFile,
                                              Tail( Trim( i'Img, Left ), 4, '0' ) & " " &
                                                  "chunk" & Tail( Trim( i'Img, Left ), 4, '0' ) & ".png " &
                                                  To_Tile_Attributes( i, chunks(i).tileInfo ) );
                                else
                                    Put_Line( outFile, "0000" );
                                end if;
                                column := column + 1;
                            end if;
                        end loop;
                        while column mod 18 /= 0 loop
                            Put_Line( outFile, "0000" );
                            column := column + 1;
                        end loop;
                    end List_Chunks;

                begin
                    Create( outFile, Out_File, Normalize_Pathname( dir & "index.txt" ) );
                    Put_Line( outFile, "//" );
                    Put_Line( outFile, "// Extracted from " & To_String( game.path ) );
                    Put_Line( outFile, "//" );
                    List_Chunks( Pics, "Pictures" );
                    List_Chunks( MaskedPics, "Masked pictures" );
                    List_Chunks( Tiles8, "8x8 tiles" );
                    List_Chunks( Tiles16, "16x16 tiles" );
                    List_Chunks( MaskedTiles16, "16x16 masked tiles" );
                    List_Chunks( Sprites, "Sprites" );
                    Close( outFile );
                exception
                    when e : others =>
                        Output_Error( "Failed to write " &
                                      Normalize_Pathname( dir & "index.txt" ) &
                                      " -- " & Exception_Name( e ) & " - " & Exception_Message( e ) );
                        Delete( chunks );
                        return False;
                end;
            end if;

            -- Compile the tile library
            declare
                args : Argument_List := (new String'("-q"),
                                         new String'(dir & "index.txt"),
                                         new String'(Get_Pref( "application", "media" ) & "graphics" & Slash & libName & ".tlz"));
                success : Boolean;
            begin
                Spawn( Execution_Directory & "tlzpack", args, success );
                for i in args'Range loop
                    Free( args(i) );
                end loop;
                if success then
                    Dbg( "Successfully compiled " & libName & ".tlz" );
                else
                    Output_Error( "Failed to compile " & libName & ".tlz" );
                    Delete( chunks );
                    return False;
                end if;
            end;

            Delete( chunks );
            return True;
        end Extract_Graphics;

        ------------------------------------------------------------------------

        function Extract_Levels( game      : not null A_Game_Exe;
                                 libName   : String;
                                 levelNum  : Integer;
                                 prefix    : String;
                                 overwrite : Boolean ) return Boolean is

            function Import_Level( level : not null A_Game_Map; outPath : String ) return Boolean is
                world : A_World;

                procedure Fill_Layer( layer : Integer; tiles : not null A_Tile_Array; offset : Integer ) is
                begin
                    for y in 0..level.height-1 loop
                        for x in 0..level.width-1 loop
                            if tiles(y * level.width + x) > 0 then
                                world.Set_Tile( layer, x, y, offset + tiles(y * level.width + x) );
                            end if;
                        end loop;
                    end loop;
                end Fill_Layer;

            begin
                world := Create_World( level.width, level.height, 3, libName );
                if world = null then
                    raise FILE_NOT_FOUND with "Library <" & libName & "> not found";
                end if;

                Fill_Layer( 1, level.layer0, game.info.chunks(Tiles16).index );
                Fill_Layer( 2, level.layer1, game.info.chunks(MaskedTiles16).index );
                world.Set_Property( "title", Create( level.name ) );
                world.Set_Property( "introduction", Create( "Keen enters " & level.name ) );
                world.Save( outPath, overwrite );
                Delete( world );
                return True;
            exception
                when e : WRITE_EXCEPTION =>
                    Output_Error( "Failed to write " & outPath &": " &
                                  Exception_Message( e ) );
                    Delete( world );
                    return False;
                when e : others =>
                    Output_Error( "Failed to write " & outPath &": " &
                                  Exception_Name( e ) & " -- " & Exception_Message( e ) );
                    Delete( world );
                    return False;
            end Import_Level;

            levels : A_Game_Maps_Array;
            skip   : Boolean := False;
            path   : Unbounded_String;
        begin
            -- Load maps from the exe and support files
            levels := Load_GameMaps( game );
            if levels = null then
                return False;
             end if;
            Dbg( "Successfully loaded levels from " & To_String( game.path ) );

            if levelNum >= 0 then
                if levelNum in levels'Range and then levels(levelNum) /= null then
                    path := To_Unbounded_String( Add_Extension( Get_Pref( "application", "media" ) & "worlds" & Slash & prefix, "world", force => True ) );
                    if Is_Regular_File( To_String( path ) ) then
                        if not overwrite then
                            Output_Error( "File " & To_String( path ) & " already exists" );
                            Delete( levels );
                            return False;       -- this is not an error
                        end if;
                    end if;
                    if not Import_Level( levels(levelNum), To_String( path ) ) then
                        Delete( levels );
                        return False;
                    end if;
                else
                    Output_Error( "Level" & Integer'Image( levelNum ) & " not found" );
                    Delete( levels );
                    return False;
                end if;
            else
                for i in levels'Range loop
                    skip := (levels(i) = null);
                    path := To_Unbounded_String( Get_Pref( "application", "media" ) & "worlds" & Slash & prefix & Trim( Integer'Image( i ), Left ) & ".world" );
                    if Is_Regular_File( To_String( path ) ) then
                        if not overwrite then
                            Dbg( "Skipping level" & i'Img & ": " & Get_Filename( To_String( path ) ) & " already exists" );
                            skip := True;
                        end if;
                    end if;
                    if not skip and then not Import_Level( levels(i), To_String( path ) ) then
                        Delete( levels );
                        return False;
                    end if;
                end loop;
            end if;

            Dbg( "Finished converting levels" );
            Delete( levels );
            return True;
        end Extract_Levels;

        ------------------------------------------------------------------------

        exePath     : Unbounded_String;
        libName     : Unbounded_String;
        makeTlz     : Boolean := False;
        levelPrefix : Unbounded_String;
        allLevels   : Boolean := False;
        levelNum    : Integer := -1;
        game        : A_Game_Exe;
        index       : Integer;
    begin
        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        -- Process Command Line Arguments

        if Argument_Count < 3 then
            Display_Usage;
            return 1;
        end if;

        exePath := To_Unbounded_String( Argument( 1 ) );
        libName := To_Unbounded_String( Argument( 2 ) );

        index := 3;
        while index <= Argument_Count loop
            if Argument( index ) = "-tiles" then
                makeTlz := True;
            elsif Argument( index ) = "-levels" then
                allLevels := True;
                levelNum := -1;
                if index + 1 <= Argument_Count then
                    levelPrefix := To_Unbounded_String( Argument( index + 1 ) );
                    index := index + 1;
                else
                    Display_Usage;
                    return 1;
                end if;
            elsif Argument( index ) = "-level" then
                allLevels := False;
                if index + 2 <= Argument_Count then
                    levelNum := To_Integer( Argument( index + 1 ) );
                    levelPrefix := To_Unbounded_String( Argument( index + 2 ) );
                    index := index + 2;
                else
                    Display_Usage;
                    return 1;
                end if;
            elsif Argument( index ) = "-q" then
                quiet := True;
            elsif Argument( index ) = "-ow" then
                overwrite := True;
            else
                -- unknown argument
                Display_Usage;
                return 1;
            end if;
            index := index + 1;
        end loop;

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        -- Load and detect the game executable

        game := Load_Game( Normalize_Pathname( To_String( exePath ) ) );
        if game = null then
            Output_Error( "File not found: " & To_String( exePath ) );
            return 1;
        elsif game.info.game = Unsupported then
            Output_Error( "Game or version is not supported." );
            Delete( game );
            return 1;
        end if;

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        -- Extract Graphics to a Tile Library

        if makeTlz then
            if not Extract_Graphics( game, To_String( libName ) ) then
                Delete( game );
                return 1;
            end if;
        end if;

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        -- Extract Levels to Oracle Engine Worlds

        if allLevels or else levelNum >= 0 then
            if not Extract_Levels( game, To_String( libName ),
                                   levelNum, To_String( levelPrefix ),
                                   overwrite )
            then
                Delete( game );
                return 1;
            end if;
        end if;

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        -- Clean up the game info
        Delete( game );

        return 0;
    exception
        when e : others =>
            -- unexpected failure
            Output_Error( Exception_Name( e ) & " -- " & Exception_Message( e ) );
            Delete( game );
            return 1;
    end Main;

    --==========================================================================

    procedure Output_Error( str : String ) is
    begin
        Ada.Text_IO.Put_Line( Standard_Error, str );
    end Output_Error;

begin
    for i in 1..Argument_Count loop
        if Argument( i ) = "-q" then
            -- suppress non-error output
            quiet := True;
        end if;
    end loop;
    Debugging.Set_Output( Output'Access );
    Debugging.Set_Verbosity( ALL_CHANNELS, Never );

    Preferences.Set_Default( "application", "media", MEDIA_DIR );

end Applications.Shell.Importer;

