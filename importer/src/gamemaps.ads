--
-- Copyright (c) 2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Ada.Unchecked_Deallocation;

package GameMaps is

    -- The indexes for Tile_Array start at 0
    type Tile_Array is array (Natural range <>) of Natural;
    type A_Tile_Array is access all Tile_Array;

    procedure Delete is new Ada.Unchecked_Deallocation( Tile_Array, A_Tile_Array );

    type Game_Map is
        record
            name   : Unbounded_String;
            width  : Natural := 0;
            height : Natural := 0;
            layer0 : A_Tile_Array := null;     -- background
            layer1 : A_Tile_Array := null;     -- foreground
            layer2 : A_Tile_Array := null;     -- info
        end record;
    type A_Game_Map is access all Game_Map;

    procedure Delete( obj : in out A_Game_Map );

    type Game_Maps_Array is array (0..99) of A_Game_Map;
    type A_Game_Maps_Array is access all Game_Maps_Array;

    procedure Delete( obj : in out A_Game_Maps_Array );

end GameMaps;
