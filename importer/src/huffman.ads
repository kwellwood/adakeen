
with Ada.Streams;                       use Ada.Streams;
with Ada.Unchecked_Deallocation;
with Interfaces;                        use Interfaces;

package Huffman is

    type Node is
        record
            bit0 : Unsigned_16 := 0;
            bit1 : Unsigned_16 := 0;
        end record;
    for Node'Size use 32;

    DICT_SIZE : constant := 1024;

    type Dictionary is array (0..255) of Node;
    for Dictionary'Size use 256 * 32;
    type A_Dictionary is access all Dictionary;

    procedure Delete is new Ada.Unchecked_Deallocation( Dictionary, A_Dictionary );

    -- Expand 'input' into 'output' using dictionary 'dict'. The output buffer
    -- must be large enough to contain input. 'last' will point to the last used
    -- element in 'output'. If the output buffer is not large enough, then
    -- 'last' will be returned as output'First - 1.
    procedure Expand( input  : Stream_Element_Array;
                      output : in out Stream_Element_Array;
                      last   : out Stream_Element_Offset;
                      dict   : Dictionary );

end Huffman;
