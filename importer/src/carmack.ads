--
-- Copyright (c) 2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Streams;                       use Ada.Streams;
with Streams;                           use Streams;

-- Note: This compression algorithm was designed by John Carmack of id Software.
-- The expansion code was implemented based on the descriptions found on the
-- following web pages:
--
-- http://www.shikadi.net/moddingwiki/Carmack_compression
-- http://gaarabis.free.fr/_sites/specs/files/wlspec_APB.html
--
package Carmack is

   -- Expands the Carmack-compressed data in 'input' into a dynamically
   -- allocated array. Due to limits in the compression scheme, this will never
   -- exceed 65535 bytes. null will be returned if the decompression fails.
   function Expand( input : Stream_Element_Array ) return A_SEA;

end Carmack;
