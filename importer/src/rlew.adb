--
-- Copyright (c) 2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body RLEW is

    function Expand( input : Stream_Element_Array; key : Unsigned_16 := 16#FEFE# ) return A_SEA is
        rpos : SEO;

        function Read_Word return Unsigned_16 is
        begin
            rpos := rpos + 2;
            return Unsigned_16(input(rpos-2)) or Shift_Left( Unsigned_16(input(rpos-1)), 8 );
        end Read_Word;

        result : A_SEA;
        word   : Unsigned_16;
        count  : Unsigned_16;
        wpos   : SEO;
    begin
        if input'Length = 0 or else input'Length mod 2 /= 0 then
            return null;
        end if;

        rpos := input'First;
        result := new Stream_Element_Array(1..SEO(Read_Word));    -- read uncompressed length (bytes)
        wpos := result'First;

        -- loop while there are at least 2 bytes remaining in 'input' and two
        -- bytes of empty space in 'result'
        while rpos < input'Last and then wpos < result'Last loop
            word := Read_Word;

            if word = key then
                if input'Last - rpos < 3 then
                    -- unexpected end of input
                    Delete( result );
                    return null;
                end if;

                count := Read_Word;
                word := Read_Word;

                if SEO(count) * 2 > result'Last - wpos + 1 then
                    -- not enough room in the result buffer
                    Delete( result );
                    return null;
                end if;

                for i in 1..count loop
                    result(wpos)   := Stream_Element(word and 16#00FF#);
                    result(wpos+1) := Stream_Element(Shift_Right( word, 8 ) and 16#00FF#);
                    wpos := wpos + 2;
                end loop;
            else
                result(wpos)   := Stream_Element(word and 16#00FF#);
                result(wpos+1) := Stream_Element(Shift_Right( word, 8 ) and 16#00FF#);
                wpos := wpos + 2;
            end if;

        end loop;

        if rpos <= input'Last or else wpos <= result'Last then
            -- didn't read or write all the expected bytes
            Delete( result );
        end if;

        return result;
    end Expand;

end RLEW;
