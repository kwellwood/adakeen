--
-- Copyright (c) 2014-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Exceptions;                    use Ada.Exceptions;
with Ada.Streams;                       use Ada.Streams;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Ada.Unchecked_Conversion;
with Ada.Unchecked_Deallocation;
with Applications.Shell.Importer;       use Applications.Shell.Importer;
with Debugging;                         use Debugging;
with Ega;                               use Ega;
with GNAT.Directory_Operations;         use GNAT.Directory_Operations;
with Huffman;                           use Huffman;
with Resources;                         use Resources;
with Streams;                           use Streams;
with Support.Paths;                     use Support.Paths;

package body EgaGraph.Loading is

    -- Contains the list of EGAGRAPH file offsets of each chunk
    type EgaHead is array (Integer range <>) of Integer;
    type A_EgaHead is access all EgaHead;

    procedure Delete is new Ada.Unchecked_Deallocation( EgaHead, A_EgaHead );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Pic_Entry is
        record
            width  : Unsigned_16;
            height : Unsigned_16;
        end record;

    type Pic_Table is array (Integer range <>) of Pic_Entry;
    type A_Pic_Table is access Pic_Table;

    procedure Delete is new Ada.Unchecked_Deallocation( Pic_Table, A_Pic_Table );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Sprite_Entry is
        record
            width      : Unsigned_16;
            height     : Unsigned_16;
            offsetX    : Integer_16;
            offsetY    : Integer_16;
            clipLeft   : Integer_16;
            clipTop    : Integer_16;
            clipRight  : Integer_16;
            clipBottom : Integer_16;
            shifts     : Integer_16;
        end record;
    for Sprite_Entry'Size use 18 * 8;

    type Sprite_Table is array (Integer range <>) of Sprite_Entry;
    type A_Sprite_Table is access Sprite_Table;

    procedure Delete is new Ada.Unchecked_Deallocation( Sprite_Table, A_Sprite_Table );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    function Load_EgaDict( game : not null A_Game_Exe ) return A_Dictionary;

    function Load_EgaHead( game        : not null A_Game_Exe;
                           graphLength : Integer ) return A_EgaHead;

    function Read_Chunks( res      : not null A_Resource_File;
                          dict     : Dictionary;
                          header   : EgaHead;
                          gameInfo : Game_Info ) return A_Chunk_Array;

    function Read_Chunk_Pic_Table( chunk : not null A_SEA ) return A_Pic_Table;

    function Read_Chunk_Sprite_Table( chunk : not null A_SEA ) return A_Sprite_Table;

    procedure Read_Tile_Properties( chunks   : in out Chunk_Array;
                                    exe      : Stream_Element_Array;
                                    gameInfo : Game_Info );

    --==========================================================================

    function Load_EgaGraph( game : not null A_Game_Exe ) return A_Chunk_Array is
        dict   : A_Dictionary;
        header : A_EgaHead;
        res    : A_Resource_File;
        chunks : A_Chunk_Array;
    begin
        -- Step 1: Load EGADICT
        dict := Load_EgaDict( game );
        if dict = null then
            Output_Error( game.info.egaDictFile.all & " not found" );
            return null;
        end if;

        -- Step 2: Load EGAGRAPH
        res := Load_Resource( Get_Directory( To_String( game.path ) ) & game.info.egaGraphFile.all, "" );
        if res = null then
            Output_Error( game.info.egaGraphFile.all & " not found" );
            Delete( dict );
            return null;
        end if;

        -- Step 3: Load EGAHEAD from disk or the .exe
        header := Load_EgaHead( game, Integer(res.Size) );
        if header = null then
            Output_Error( game.info.egaHeadFile.all & " not found" );
            Delete( dict );
            Delete( res );
            return null;
        end if;

        -- Step 4: Decompress chunks in EGAGRAPH
        chunks := Read_Chunks( res, dict.all, header.all, game.info );
        Dbg( "Read" & chunks'Length'Img & " chunks from " & game.info.egaGraphFile.all );

        -- Step 5: Read tile properties
        Read_Tile_Properties( chunks.all, game.data.all, game.info );
        Dbg( "Read tile properties from EXE" );

        Delete( dict );
        Delete( res );
        Delete( header );

        return chunks;
    end Load_EgaGraph;

    ----------------------------------------------------------------------------

    function Load_EgaDict( game : not null A_Game_Exe ) return A_Dictionary is
        path   : constant String := Get_Directory( To_String( game.path ) ) & game.info.egaDictFile.all;
        offset : constant SEO := game.info.egaDictOffset;

        -- the signature is the last 6 bytes of the dictionary
        EGADICT_SIG : constant Stream_Element_Array := (16#FD#, 16#01#, 16#00#, 16#00#, 16#00#, 16#00#);

        res         : A_Resource_File;
        dict        : A_Dictionary := new Dictionary;
        dictBytes   : Stream_Element_Array(0..Dictionary'Size/8-1);
        for dictBytes'Address use dict.all'Address;
    begin
        -- look for a file on disk first
        res := Load_Resource( path, "" );
        if res /= null then
            declare
                data : Stream_Element_Array(0..SEO(res.Size-1));
                for data'Address use res.Get_Address;
            begin
                if data'Length = dictBytes'Length then
                    Dbg( "Loaded " & path );
                    dictBytes := data;
                    Delete( res );
                    return dict;
                elsif data'Length = dictBytes'Length - 4 then
                    -- Some external dictionary files may not include the last
                    -- four bytes because they are always 0.
                    Dbg( "Loaded " & path );
                    dictBytes(dictBytes'First..dictBytes'Last-4) := data;
                    dictBytes(dictBytes'Last-3..dictBytes'Last) := (others => 0);
                    Delete( res );
                    return dict;
                else
                    Dbg( "Bad file size: Ignoring " & path );
                end if;
            end;
            Delete( res );
        end if;

        -- check the specified location in the .exe
        if offset >= 0 then
            if EGADICT_SIG = game.data((offset + dictBytes'Length - EGADICT_SIG'Length)..(offset + dictBytes'Length - 1)) then
                dictBytes := game.data(offset..(offset + dictBytes'Length - 1));
                Dbg( "Loaded " & File_Name( path ) & " from EXE" );
                return dict;
            end if;
        end if;

        -- seach for the dictionary in the .exe
        declare
            offset : SEO := -1;
        begin
            for i in reverse game.data'First..(game.data'Last - EGADICT_SIG'Length) loop
                if game.data(i..(i + EGADICT_SIG'Length - 1)) = EGADICT_SIG then
                    offset := i + EGADICT_SIG'Length - dictBytes'Length;
                    dictBytes := game.data(offset..(offset + dictBytes'Length - 1));
                    Dbg( "Found and loaded " & File_Name( path ) & " from EXE at" & offset'Img );
                    return dict;
                end if;
            end loop;
        end;

        Delete( dict );
        return null;
    end Load_EgaDict;

    ----------------------------------------------------------------------------

    function Load_EgaHead( game        : not null A_Game_Exe;
                           graphLength : Integer ) return A_EgaHead is
        path : constant String := Get_Directory( To_String( game.path ) ) & game.info.egaHeadFile.all;

        function Read_Index( data   : Stream_Element_Array;
                             offset : SEO ) return Integer is
            index : Unsigned_64;
        begin
            if game.info.egaHeadFormat = 3 then
                index := Unsigned_64(data(offset)) or
                         Shift_Left( Unsigned_64(data(offset + 1)), 8 ) or
                         Shift_Left( Unsigned_64(data(offset + 2)), 16);
            else -- format = 4
                index := Unsigned_64(data(offset)) or
                         Shift_Left( Unsigned_64(data(offset + 1)), 8 ) or
                         Shift_Left( Unsigned_64(data(offset + 2)), 16) or
                         Shift_Left( Unsigned_64(data(offset + 3)), 24);
            end if;
            return (if index /= 16#FFFFFF# and then index /= 16#FFFFFFFF# then Integer(index) else -1);
        end Read_Index;

        res    : A_Resource_File;
        header : A_EgaHead;

        pos    : SEO;
        start  : SEO;
        index  : Integer;
        prev   : Integer;
        chunks : Integer;
    begin
        -- look for a file on disk first
        res := Load_Resource( path, "" );
        if res /= null and then res.Size > 4 then
            declare
                data : Stream_Element_Array(0..SEO(res.Size-1));
                for data'Address use res.Get_Address;
                lastEntry : constant Integer := Read_Index( data, data'Last - SEO(game.info.egaHeadFormat) + 1 );
            begin
                if data'Length mod game.info.egaHeadFormat = 0 and then
                    lastEntry <= graphLength
                then
                    if lastEntry < graphLength then
                        Dbg( "Warning: EGAHEAD last entry at" & lastEntry'Img &
                             "; expected" & graphLength'Img );
                    end if;
                    header := new EgaHead(0..(data'Length / game.info.egaHeadFormat - 1) - 1);  -- ignore the last entry
                    for i in header'Range loop
                        header(i) := Read_Index( data, SEO(i * game.info.egaHeadFormat));
                    end loop;
                    Dbg( "Loaded " & path );
                    Delete( res );
                    return header;
                else
                    Dbg( "Detected invalid data: Ignoring " & path );
                    if data'Length mod game.info.egaHeadFormat /= 0 then
                        Dbg( "Length of EGAHEAD is not divisible by" & game.info.egaHeadFormat'Img );
                    else
                        Dbg( "Last entry of EGAHEAD =" &
                              Read_Index( data, data'Last - SEO(game.info.egaHeadFormat) + 1 )'Img &
                              "; expected <=" & graphLength'Img );
                    end if;
                end if;
            end;
            Delete( res );
        end if;

        -- check the suspected location in the .exe
        if Read_Index( game.data.all, game.info.egaHeadOffset ) = 0 and then
           Read_Index( game.data.all, game.info.egaHeadOffset + SEO(game.info.chunkCount * game.info.egaHeadFormat) ) = graphLength
        then
            -- confirmed it's at the .egaHeadOffset location
            header := new EgaHead(0..game.info.chunkCount-1);
            for i in header'Range loop
                header(i) := Read_Index( game.data.all, game.info.egaHeadOffset + SEO(i * game.info.egaHeadFormat) );
            end loop;
            Dbg( "Loaded " & File_Name( path ) & " from EXE" );
            return header;
        end if;

        -- search for the file in the .exe
        pos := game.data'Last - SEO(game.info.egaHeadFormat) + 1;
        while pos >= game.data'First + SEO(game.info.egaHeadFormat) - 1 loop
            index := Read_Index( game.data.all, pos );
            if index = graphLength then
                -- found the last entry in EGAHEAD (skip it)
                prev := index;
                start := pos - SEO(game.info.egaHeadFormat);
                chunks := 1;

                -- verify the format and count the length
                loop
                    index := Read_Index( game.data.all, start );
                    if index >= prev then
                        -- the data doesn't follow an ascending format. it
                        -- can't be the header; keep searching
                        exit;
                    elsif index = 0 then
                        -- found the first chunk
                        header := new EgaHead(0..chunks-1);
                        for i in header'Range loop
                            header(i) := Read_Index( game.data.all, start + SEO(i * game.info.egaHeadFormat) );
                        end loop;
                        Dbg( "Found and loaded " & File_Name( path ) &
                              " from EXE at" & start'Img & " (indexes" &
                              header'Length'Img & " chunks)" );
                        return header;
                    end if;

                    if index /= -1 then
                        prev := index;
                    end if;
                    chunks := chunks + 1;
                    start := start - SEO(game.info.egaHeadFormat);

                    exit when start < game.data'First;    -- keep searching
                end loop;

                -- keep searching
            end if;
            pos := pos - 1;
        end loop;

        return null;
    end Load_EgaHead;

    ----------------------------------------------------------------------------

    function Read_Chunks( res      : not null A_Resource_File;
                          dict     : Dictionary;
                          header   : EgaHead;
                          gameInfo : Game_Info ) return A_Chunk_Array is

        data : Stream_Element_Array(0..SEO(res.Size - 1));
        for data'Address use res.Get_Address;

        picTable        : A_Pic_Table;
        maskedPicTable  : A_Pic_Table;
        spriteTable     : A_Sprite_Table;
        chunks          : A_Chunk_Array;
        chunkIndex      : Integer;
        firstChunkIndex : Integer;
        offset          : SEO;
        inLength        : SEO;
        outLength       : SEO;
        last            : SEO;
        rawChunk        : A_SEA;
    begin
        chunks := new Chunk_Array(0..header'Length-1);

        chunkIndex := header'First;
        while chunkIndex <= header'Last loop

            -- determine the chunk type by its index
            for chunkType in gameInfo.chunks'Range loop
                if chunkIndex = gameInfo.chunks(chunkType).index then
                    firstChunkIndex := chunkIndex;

                    -- loop over all chunks of this type
                    while chunkIndex < gameInfo.chunks(chunkType).index +
                                       (if gameInfo.chunks(chunkType).single then 1
                                           else gameInfo.chunks(chunkType).count)
                    loop
                        offset := SEO(header(chunkIndex));
                        chunks(chunkIndex).chunkType := chunkType;
                        if offset >= 0 and then offset + 4 - 1 <= data'Last then

                            -- determine the decompressed (output) length of the chunk
                            if gameInfo.chunks(chunkType).size > 0 then
                                outLength := SEO(gameInfo.chunks(chunkType).size);
                            else
                                -- first 4 bytes is the decompressed size
                                outLength := SEO(Unsigned_32(data(offset)) or
                                                 Shift_Left( Unsigned_32(data(offset + 1)),  8 ) or
                                                 Shift_Left( Unsigned_32(data(offset + 2)), 16 ) or
                                                 Shift_Left( Unsigned_32(data(offset + 3)), 24 ));
                                offset := offset + 4;
                            end if;

                            -- allocate memory for the decompressed chunk
                            rawChunk := new Stream_Element_Array(0..outLength-1);
                            rawChunk.all := (others => 0);

                            -- determine the compressed (input) length of the chunk
                            inLength := -1;
                            for nextIndex in (chunkIndex + 1)..header'Last loop
                                if header(nextIndex) /= -1 then
                                    inLength := SEO(header(nextIndex)) - offset;
                                    exit;
                                end if;
                            end loop;
                            if inLength < 0 then
                                -- last chunk goes to the end of the file
                                inLength := data'Last - offset + 1;
                            end if;

                            -- decompress the chunk
                            Huffman.Expand( data(offset..offset + inLength - 1),
                                            rawChunk.all(0..outLength - 1),
                                            last,
                                            dict );

                            -- convert the chunk to a usable format
                            case chunkType is
                                when TablePics =>
                                    Delete( picTable );
                                    picTable := Read_Chunk_Pic_Table( rawChunk );

                                when TableMaskedPics =>
                                    Delete( maskedPicTable );
                                    maskedPicTable := Read_Chunk_Pic_Table( rawChunk );

                                when TableSprites =>
                                    Delete( spriteTable );
                                    spriteTable := Read_Chunk_Sprite_Table( rawChunk );

                                when Pics =>
                                    if picTable = null then
                                        raise Constraint_Error with "Missing or invalid picture table";
                                    end if;
                                    chunks(chunkIndex).bmp :=
                                         Read_Ega_Graphic_Planar( rawChunk.all,
                                                                  Integer(picTable(chunkIndex - firstChunkIndex).width),
                                                                  Integer(picTable(chunkIndex - firstChunkIndex).height),
                                                                  EGA_BGRI );

                                when MaskedPics =>
                                    if maskedPicTable = null then
                                        raise Constraint_Error with "Missing or invalid masked picture table";
                                    end if;
                                    chunks(chunkIndex).bmp :=
                                        Read_Ega_Graphic_Planar( rawChunk.all,
                                                                 Integer(maskedPicTable(chunkIndex - firstChunkIndex).width),
                                                                 Integer(maskedPicTable(chunkIndex - firstChunkIndex).height),
                                                                 EGA_BGRIA );

                                when Sprites =>
                                    if spriteTable = null then
                                        raise Constraint_Error with "Missing or invalid sprite table";
                                    end if;
                                    chunks(chunkIndex).bmp :=
                                        Read_Ega_Graphic_Planar( rawChunk.all,
                                                                 Integer(spriteTable(chunkIndex - firstChunkIndex).width),
                                                                 Integer(spriteTable(chunkIndex - firstChunkIndex).height),
                                                                 EGA_BGRIA );

                                when Tiles8 =>
                                    chunks(chunkIndex).bmp :=
                                        Read_Ega_Graphic_Planar( rawChunk.all,
                                                                 8, 8, EGA_BGRI,
                                                                 gameInfo.chunks(Tiles8).count, 10 );

                                when MaskedTiles8 =>
                                    chunks(chunkIndex).bmp :=
                                        Read_Ega_Graphic_Planar( rawChunk.all,
                                                                 8, 8, EGA_BGRIA,
                                                                 gameInfo.chunks(MaskedTiles8).count, 1 );

                                when Tiles16 =>
                                    chunks(chunkIndex).bmp :=
                                        Read_Ega_Graphic_Planar( rawChunk.all, 16, 16, EGA_BGRI );

                                when MaskedTiles16 =>
                                    chunks(chunkIndex).bmp :=
                                        Read_Ega_Graphic_Planar( rawChunk.all, 16, 16, EGA_BGRIA );

                                when others => null;
                            end case;

                            Delete( rawChunk );
                        end if;
                        chunkIndex := chunkIndex + 1;  -- next chunk within the type
                    end loop;
                    chunkIndex := chunkIndex - 1;   -- back up to last chunk of chunkType (we increment later)

                end if;
            end loop;

            chunkIndex := chunkIndex + 1;   -- next chunk
        end loop;

        Delete( picTable );
        Delete( maskedPicTable );
        Delete( spriteTable );

        return chunks;
    exception
        when e : others =>
            Output_Error( Exception_Message( e ) );
            Delete( rawChunk );
            Delete( spriteTable );
            Delete( maskedPicTable );
            Delete( picTable );
            Delete( chunks );
            return null;
    end Read_Chunks;

    ----------------------------------------------------------------------------

    function Read_Chunk_Pic_Table( chunk : not null A_SEA ) return A_Pic_Table is
        offset : SEO := chunk'First;
        table  : A_Pic_Table;
    begin
        if chunk'Length mod (Pic_Entry'Size / 8) /= 0 then
            return null;
        end if;

        table := new Pic_Table(0..(chunk'Length / (Pic_Entry'Size / 8) - 1));
        for i in table'Range loop
            declare
                picEntry : Pic_Entry;
                for picEntry'Address use chunk(offset)'Address;
            begin
                table(i) := picEntry;
                table(i).width := table(i).width * 8;
                if table(i).width > 320 or else table(i).height > 240 then
                    Dbg( "Warning: Pic is too large: " &
                         table(i).width'Img & " x" & table(i).height'Img );
                    table(i).width := 0;
                    table(i).height := 0;
                end if;
            end;
            offset := offset + Pic_Entry'Size / 8;
        end loop;
        return table;
    end Read_Chunk_Pic_Table;

    ----------------------------------------------------------------------------

    function Read_Chunk_Sprite_Table( chunk : not null A_SEA ) return A_Sprite_Table is
        offset : SEO := chunk'First;
        table  : A_Sprite_Table;
    begin
        if chunk'Length mod (Sprite_Entry'Size / 8) /= 0 then
            return null;
        end if;

        table := new Sprite_Table(0..(chunk'Length / (Sprite_Entry'Size / 8) - 1));
        for i in table'Range loop
            declare
                spriteEntry : Sprite_Entry;
                for spriteEntry'Address use chunk(offset)'Address;
            begin
                table(i) := spriteEntry;
                table(i).width := table(i).width * 8;
                table(i).offsetX := table(i).offsetX * 8;
                table(i).offsetY := table(i).offsetY * 8;
                table(i).clipLeft := table(i).clipLeft * 8;
                table(i).clipTop := table(i).clipTop * 8;
                table(i).clipRight := table(i).clipRight * 8;
                table(i).clipBottom := table(i).clipBottom * 8;
            end;
            offset := offset + Sprite_Entry'Size / 8;
        end loop;
        return table;
    end Read_Chunk_Sprite_Table;

    ----------------------------------------------------------------------------

    procedure Read_Tile_Properties( chunks   : in out Chunk_Array;
                                    exe      : Stream_Element_Array;
                                    gameInfo : Game_Info ) is
        function To_Int8 is new Ada.Unchecked_Conversion( Stream_Element, Integer_8 );

        offset : SEO := gameInfo.tilePropsOffset;
    begin
        -- - - - - Standard Tiles - - - - --

        -- read the tiles .animationTime array
        for i in 0..(gameInfo.chunks(Tiles16).count - 1) loop
            chunks(gameInfo.chunks(Tiles16).index + i).tileInfo.animationTime := Unsigned_8(exe(offset));
            offset := offset + 1;
        end loop;

        -- read the tiles .nextTile array
        for i in 0..(gameInfo.chunks(Tiles16).count - 1) loop
            chunks(gameInfo.chunks(Tiles16).index + i).tileInfo.nextTile := Integer(To_Int8( exe(offset) ));
            offset := offset + 1;
        end loop;

        -- - - - - Masked Tiles - - - - --

        -- read the masked tiles .edgeTop array
        for i in 0..(gameInfo.chunks(MaskedTiles16).count - 1) loop
            chunks(gameInfo.chunks(MaskedTiles16).index + i).tileInfo.edgeTop := Unsigned_8(exe(offset));
            offset := offset + 1;
        end loop;

        -- read the masked tiles .edgeRight array
        for i in 0..(gameInfo.chunks(MaskedTiles16).count - 1) loop
            chunks(gameInfo.chunks(MaskedTiles16).index + i).tileInfo.edgeRight := Unsigned_8(exe(offset));
            offset := offset + 1;
        end loop;

        -- read the masked tiles .edgeBottom array
        for i in 0..(gameInfo.chunks(MaskedTiles16).count - 1) loop
            chunks(gameInfo.chunks(MaskedTiles16).index + i).tileInfo.edgeBottom := Unsigned_8(exe(offset));
            offset := offset + 1;
        end loop;

        -- read the masked tiles .edgeLeft array
        for i in 0..(gameInfo.chunks(MaskedTiles16).count - 1) loop
            chunks(gameInfo.chunks(MaskedTiles16).index + i).tileInfo.edgeLeft := Unsigned_8(exe(offset));
            offset := offset + 1;
        end loop;

        -- read the masked tiles .nextTile array
        for i in 0..(gameInfo.chunks(MaskedTiles16).count - 1) loop
            chunks(gameInfo.chunks(MaskedTiles16).index + i).tileInfo.nextTile := Integer(To_Int8( exe(offset) ));
            offset := offset + 1;
        end loop;

        -- read the masked tiles .behavior array
        for i in 0..(gameInfo.chunks(MaskedTiles16).count - 1) loop
            chunks(gameInfo.chunks(MaskedTiles16).index + i).tileInfo.behavior := Unsigned_8(exe(offset));
            offset := offset + 1;
        end loop;

        -- read the masked tiles .animationTime array
        for i in 0..(gameInfo.chunks(MaskedTiles16).count - 1) loop
            chunks(gameInfo.chunks(MaskedTiles16).index + i).tileInfo.animationTime := Unsigned_8(exe(offset));
            offset := offset + 1;
        end loop;
    end Read_Tile_Properties;

end EgaGraph.Loading;
