
with Ada.Unchecked_Deallocation;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Color;                     use Allegro.Color;
with Interfaces;                        use Interfaces;
with Streams;                           use Streams;

package Ega_Graphics is

    type EgaGraph_Header is array (Integer range <>) of Integer;
    type A_EgaGraph_Header is access all EgaGraph_Header;

    procedure Delete is new Ada.Unchecked_Deallocation( EgaGraph_Header, A_EgaGraph_Header );

    type Color_Palette is array (0..16) of Allegro_Color;

    type Chunk_Data is
        record
            raw : A_SEA;
            bmp : A_Allegro_Bitmap;
        end record;

    type Chunk_Array is array (Integer range <>) of Chunk_Data;
    type A_Chunk_Array is access all Chunk_Array;

    procedure Delete( obj : in out A_Chunk_Array );

    -- http://www.shikadi.net/moddingwiki/Keen_4-6_Tileinfo_Format
    type Tile_Property is
        record
            animationTime : Unsigned_8 := 0;
            nextTile      : Integer := 0;
            edgeTop       : Unsigned_8 := 0;
            edgeBottom    : Unsigned_8 := 0;
            edgeLeft      : Unsigned_8 := 0;
            edgeRight     : Unsigned_8 := 0;
            behavior      : Integer_8 := 0;
        end record;

    type Tile_Property_Array is array (Integer range <>) of Tile_Property;
    type A_Tile_Property_Array is access Tile_Property_Array;

    procedure Delete is new Ada.Unchecked_Deallocation( Tile_Property_Array, A_Tile_Property_Array );

    type EgaGraphics is
        record
            header        : A_EgaGraph_Header;
            palette       : Color_Palette;
            chunks        : A_Chunk_Array;
            tileProps     : A_Tile_Property_Array;
            tileMaskProps : A_Tile_Property_Array;
        end record;
    type A_EgaGraphics is access all EgaGraphics;

    procedure Delete( obj : in out A_EgaGraphics );

    type Pic_Table_Entry is
        record
            width  : Integer;
            height : Integer;
        end record;

     type Pic_Table is array (Integer range <>) of Pic_Table_Entry;
     type A_Pic_Table is access Pic_Table;

     procedure Delete is new Ada.Unchecked_Deallocation( Pic_Table, A_Pic_Table );

    type Sprite_Table_Entry is
        record
            width      : Integer;
            height     : Integer;
            offsetX    : Integer;
            offsetY    : Integer;
            clipLeft   : Integer;
            clipTop    : Integer;
            clipRight  : Integer;
            clipBottom : Integer;
            shifts     : Integer;
        end record;

     type Sprite_Table is array (Integer range <>) of Sprite_Table_Entry;
     type A_Sprite_Table is access Sprite_Table;

     procedure Delete is new Ada.Unchecked_Deallocation( Sprite_Table, A_Sprite_Table );

end Ega_Graphics;
