--
-- Copyright (c) 2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Game_Support;                      use Game_Support;

package GameMaps.Loading is

    -- Loads the game maps from an Apogee game's GAMEMAPS map file format using
    -- the game's executable 'game'. The game's accompanying GAMEMAPS file and
    -- optional MAPHEAD file are expected to be in the same directory as the
    -- executable. If an error occurs, null will be returned and error messages
    -- will be printed to standard error output.
    function Load_GameMaps( game : not null A_Game_Exe ) return A_Game_Maps_Array;

end GameMaps.Loading;
