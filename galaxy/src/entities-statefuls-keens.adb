--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Clipping;                          use Clipping;
--with Debugging;                         use Debugging;
with Directives.Galaxy;
with Entities.Camera_Targets;           use Entities.Camera_Targets;
with Entities.Entity_Attributes;        use Entities.Entity_Attributes;
with Entities.Collidables;              use Entities.Collidables;
with Entities.Galaxy;                   use Entities.Galaxy;
with Entities.Map_Clipping;             use Entities.Map_Clipping;
with Entities.Solids;                   use Entities.Solids;
with Entities.Systems;                  use Entities.Systems;
with Entities.Tile_Sensors;             use Entities.Tile_Sensors;
with Events.Audio;                      use Events.Audio;
with Events.Entities;                   use Events.Entities;
with Events.Game;                       use Events.Game;
with Games;                             use Games;
with Preferences;
with Tiles;                             use Tiles;
with Values.Casting;                    use Values.Casting;
with Values.Construction;               use Values.Construction;
with Values.Lists;                      use Values.Lists;
with Values.Strings;                    use Values.Strings;
with Vector_Math;                       use Vector_Math;
with Worlds;                            use Worlds;

package body Entities.Statefuls.Keens is

    -- Distance that Keen should move (in either axis) while dying, before death
    DEATH_DISTANCE : constant Float := 200.0;

    -- Height above his top that Keen can grab ledges
    GRAB_HEIGHT : constant Float := 4.0;
    GRAB_RANGE  : constant Float := 6.0;   -- size of grab detection area

    -- Speed of blaster shots in world units per second
    SHOT_SPEED : constant := 340.0;

    -- Time between shots
    SHOOT_DURATION : constant Time_Span := Milliseconds( 215 );

    -- Minimum delay between repeated pogo sounds (to prevent over-playing when
    -- Keen is in a tight space.)
    POGO_REPEAT_DELAY : constant Time_Span := Milliseconds( 100 );

    -- Time to keep showing the bounce frame after the pogo leaves the ground.
    POGO_BOUNCE_DURATION : constant Time_Span := Milliseconds( 50 );

    -- Minimum delay between footstep sounds
    FOOTSTEP_REPEAT_DELAY : constant Time_Span := Milliseconds( 350 );

    -- Minimum delay between head bonking sounds
    BONK_REPEAT_DELAY : constant Time_Span := Milliseconds( 100 );

    -- The distance from the bottom of a pole sprite where Keen is considered to
    -- be at the bottom. (see the .poleBottom field) Useful for determining when
    -- to stop Keen's movement to prevent him from falling off the bottom.
    POLE_BOTTOM_BUFFER : constant := 2.0;

    -- The amount of time Keen spends walking into a doorway when teleporting
    TELEPORT_DURATION : constant Time_Span := Milliseconds( 600 );

    -- Time spent flipping a switch
    SWITCHING_DURATION : constant Time_Span := Milliseconds( 215 );

    -- Minimum time span to hang before climbing up
    MIN_HANG_DELAY : constant Time_Span := Milliseconds( 200 );

    -- Time spent climbing a ledge
    CLAMBERING_DURATION : constant Time_Span := Milliseconds( 600 );

    GROUNDED_BOUNDS : constant Bounds_Type := (top =>  28, bottom => 28, left => -4, right => 45);
    AIRBORNE_BOUNDS : constant Bounds_Type := (top => -81, bottom => 52, left => -4, right => 45);
    LOOKING_BOUNDS  : constant Bounds_Type := AIRBORNE_BOUNDS;
    CLIMBING_BOUNDS : constant Bounds_Type := GROUNDED_BOUNDS;

    ----------------------------------------------------------------------------

    type Frame_Info is
        record
            filename : String_Access := null;
            id       : Integer := -1;
        end record;

    type Directional_Frame is array (Direction_8 range <>) of Frame_Info;
    type A_Directional_Frame is access all Directional_Frame;

    type Animation( directional : Boolean ) is
        record
            case directional is
                when True =>
                    dir : A_Directional_Frame := null;
                when False =>
                    frame : Frame_Info;
            end case;
        end record;
    type A_Animation is access all Animation;

    ----------------------------------------------------------------------------

    type Visible_Action is
    (
        Standing,
        Standing_And_Shooting,
        Looking_Frames,
        Looking_Back_Frames,
        Walking,
        Jumping,                -- jumping upward
        Jumping_Midair,         -- hanging in midair during a jump
        Falling,                -- falling downward
        Jumping_And_Shooting,   -- shooting in the air (jumping or falling)
        Pogoing_Midair,
        Pogoing_On_Ground,
        Hanging_From_Pole,
        Hanging_From_Pole_And_Shooting,
        Climbing_Pole,
        Hanging_From_Ledge,
        Climbing_Ledge,
        Looking_Bored,
        Checking_Watch_Bored,
        Mooning_Bored,
        Shrugging_Bored,
        Sitting_Bored,
        Reading_Bored,
        Rising_Bored,
        Entering_Doorway,
        Flipping_Switch,
        Dying_Frames
    );

    ----------------------------------------------------------------------------

    type Idle_Animation is
        record
            visibleAction : Visible_Action;
            idleDelay     : Time_Span;
            anmDuration   : Time_Span;
        end record;

    type Idle_Animation_Array is array (Positive range <>) of Idle_Animation;

    INDEX_LOOKING_BORED   : constant := 1;      -- blinking at the camera
    INDEX_MOONING_BORED   : constant := 2;      -- pulling down his pants (easter egg)
    INDEX_CHECKING_WATCH  : constant := 3;      -- checking his watch
    INDEX_SHRUGGING_BORED : constant := 4;      -- shrugging his shoulders
    INDEX_SITTING_BORED   : constant := 5;      -- sitting down to read
    INDEX_READING_BORED   : constant := 6;      -- reading a book while sitting

    idle_animations : constant Idle_Animation_Array := Idle_Animation_Array'(
        INDEX_LOOKING_BORED   => (Looking_Bored,        idleDelay => FIRST_IDLE_DELAY,     anmDuration => Milliseconds( 700 )),
        INDEX_MOONING_BORED   => (Mooning_Bored,        idleDelay => Milliseconds( 3000 ), anmDuration => Milliseconds( 2158 )),
        INDEX_CHECKING_WATCH  => (Checking_Watch_Bored, idleDelay => Milliseconds( 3000 ), anmDuration => Milliseconds( 1000 )),
        INDEX_SHRUGGING_BORED => (Shrugging_Bored,      idleDelay => Milliseconds( 3050 ), anmDuration => Milliseconds( 125 ) * 35),
        INDEX_SITTING_BORED   => (Sitting_Bored,        idleDelay => Milliseconds( 7250 ), anmDuration => Milliseconds( 115 ) * 4),
        INDEX_READING_BORED   => (Reading_Bored,        idleDelay => Milliseconds(    0 ), anmDuration => Seconds( 365 * 84600 ))
    );

    ----------------------------------------------------------------------------

    type Action_Frames is array (Visible_Action) of A_Animation;

    frames : constant Action_Frames := Action_Frames'(
        Standing => new Animation'(
            directional => True,
            dir         => new Directional_Frame'(
                D8_Left  => (filename => new String'("stand_left"),  others => <>),
                D8_Right => (filename => new String'("stand_right"), others => <>)
            )),
        Standing_And_Shooting => new Animation'(
            directional => True,
            dir         => new Directional_Frame'(
                D8_Left     => (filename => new String'("shoot_left"),  others => <>),
                D8_Right    => (filename => new String'("shoot_right"), others => <>),
                D8_Up       => (filename => new String'("shoot_up"),    others => <>),
                D8_Down     => <>,
                D8_Up_Left  => (filename => new String'("shoot_up"),    others => <>),
                D8_Up_Right => (filename => new String'("shoot_up"),    others => <>)
            )),
        Looking_Frames => new Animation'(
            directional => True,
            dir         => new Directional_Frame'(
                D8_Up   => (filename => new String'("look_up"),    others => <>),
                D8_Down => (filename => new String'("look_down1"), others => <>)
            )),
        Looking_Back_Frames => new Animation'(
            directional => True,
            dir         => new Directional_Frame'(
                D8_Up   => (filename => new String'("look_up"),    others => <>),
                D8_Down => (filename => new String'("look_down2"), others => <>)
            )),
        Walking => new Animation'(
            directional => True,
            dir         => new Directional_Frame'(
                D8_Left  => (filename => new String'("walk_left1"),  others => <>),
                D8_Right => (filename => new String'("walk_right1"), others => <>)
            )),
        Jumping => new Animation'(
            directional => True,
            dir         => new Directional_Frame'(
                D8_Left  => (filename => new String'("jump_left1"),  others => <>),
                D8_Right => (filename => new String'("jump_right1"), others => <>)
            )),
        Jumping_Midair => new Animation'(
            directional => True,
            dir         => new Directional_Frame'(
                D8_Left  => (filename => new String'("jump_left2"),  others => <>),
                D8_Right => (filename => new String'("jump_right2"), others => <>)
            )),
        Falling => new Animation'(
            directional => True,
            dir         => new Directional_Frame'(
                D8_Left  => (filename => new String'("jump_left3"),  others => <>),
                D8_Right => (filename => new String'("jump_right3"), others => <>)
            )),
        Jumping_And_Shooting => new Animation'(
            directional => True,
            dir         => new Directional_Frame'(
                D8_Left       => (filename => new String'("shoot_jump_left"),  others => <>),
                D8_Right      => (filename => new String'("shoot_jump_right"), others => <>),
                D8_Up         => (filename => new String'("shoot_jump_up"),    others => <>),
                D8_Up_Left    => (filename => new String'("shoot_jump_up"),    others => <>),
                D8_Up_Right   => (filename => new String'("shoot_jump_up"),    others => <>),
                D8_Down       => (filename => new String'("shoot_jump_down"),  others => <>),
                D8_Down_Left  => (filename => new String'("shoot_jump_down"),  others => <>),
                D8_Down_Right => (filename => new String'("shoot_jump_down"),  others => <>)
            )),
        Pogoing_Midair => new Animation'(
            directional => True,
            dir         => new Directional_Frame'(
                D8_Left  => (filename => new String'("pogo_left_air"), others => <>),
                D8_Right => (filename => new String'("pogo_right_air"), others => <>))),
        Pogoing_On_Ground => new Animation'(
            directional => True,
            dir         => new Directional_Frame'(
                D8_Left  => (filename => new String'("pogo_left_ground"), others => <>),
                D8_Right => (filename => new String'("pogo_right_ground"), others => <>))),
        Hanging_From_Pole => new Animation'(
            directional => True,
            dir         => new Directional_Frame'(
                D8_Left  => (filename => new String'("pole_left"), others => <>),
                D8_Right => (filename => new String'("pole_right"), others => <>))),
        Hanging_From_Pole_And_Shooting => new Animation'(
            directional => True,
            dir         => new Directional_Frame'(
                D8_Left       => (filename => new String'("shoot_pole_left"), others => <>),
                D8_Right      => (filename => new String'("shoot_pole_right"), others => <>),
                D8_Up         => (others => <>),
                D8_Down       => (others => <>),
                D8_Down_Left  => (filename => new String'("shoot_pole_down_left"), others => <>),
                D8_Down_Right => (filename => new String'("shoot_pole_down_right"), others => <>),
                D8_Up_Left    => (filename => new String'("shoot_pole_up_left"), others => <>),
                D8_Up_Right   => (filename => new String'("shoot_pole_up_right"), others => <>))),
        Climbing_Pole => new Animation'(
            directional => True,
            dir         => new Directional_Frame'(
                D8_Left       => (filename => new String'("pole_up_left1"), others => <>),
                D8_Right      => (filename => new String'("pole_up_right1"), others => <>),
                D8_Up         => (others => <>),
                D8_Down       => (filename => new String'("pole_down1"), others => <>),
                D8_Up_Left    => (others => <>),
                D8_Up_Right   => (others => <>),
                D8_Down_Left  => (filename => new String'("pole_down1"), others => <>),
                D8_Down_Right => (filename => new String'("pole_down1"), others => <>))),
        Hanging_From_Ledge => new Animation'(
            directional => True,
            dir         => new Directional_Frame'(
                D8_Left  => (filename => new String'("ledge_left1"), others => <>),
                D8_Right => (filename => new String'("ledge_right1"), others => <>))),
        Climbing_Ledge => new Animation'(
            directional => True,
            dir         => new Directional_Frame'(
                D8_Left  => (filename => new String'("ledge_left2"), others => <>),
                D8_Right => (filename => new String'("ledge_right2"), others => <>))),
        Looking_Bored => new Animation'(
            directional => False,
            frame       => (filename => new String'("look_up"), others => <>)),
        Mooning_Bored => new Animation'(
            directional => False,
            frame       => (filename => new String'("moon1"), others => <>)),
        Checking_Watch_Bored => new Animation'(
            directional => False,
            frame       => (filename => new String'("bored_watch"), others => <>)),
        Shrugging_Bored => new Animation'(
            directional => False,
            frame       => (filename => new String'("bored_shrug1"), others => <>)),
        Sitting_Bored => new Animation'(
            directional => False,
            frame       => (filename => new String'("bored_sit1"), others => <>)),
        Reading_Bored => new Animation'(
            directional => False,
            frame       => (filename => new String'("bored_read1"), others => <>)),
        Rising_Bored => new Animation'(
            directional => False,
            frame       => (filename => new String'("bored_rise1"), others => <>)),
        Entering_Doorway => new Animation'(
            directional => False,
            frame       => (filename => new String'("doorway1"), others => <>)),
        Flipping_Switch => new Animation'(
            directional => False,
            frame       => (filename => new String'("switch"), others => <>)),
        Dying_Frames => new Animation'(
            directional => False,
            frame       => (filename => new String'("die1"), others => <>))
    );

    LOOK_UP_DURATION   : constant Time_Span := Milliseconds( 180 ) * 1;
    LOOK_DOWN_DURATION : constant Time_Span := Milliseconds( 180 ) * 2;
    RISING_DURATION    : constant Time_Span := Milliseconds( 125 ) * 2;

    ----------------------------------------------------------------------------

    function To_D8( xv : Float; dir : Direction_Type ) return Direction_8
    is (if xv < 0.0 then To_D8( dir + Left ) elsif xv > 0.0 then To_D8( dir + Right ) else To_D8( dir ));
    pragma Inline_Always( To_D8 );

    ----------------------------------------------------------------------------

    function To_X( xv : Float; dir : Direction_Type ) return Direction_8
    is (if xv < 0.0 then D8_Left elsif xv > 0.0 then D8_Right else To_X( dir ));
    pragma Inline_Always( To_X );

    ----------------------------------------------------------------------------

    type Root_State is new State with null record;

    function obj( this : Root_State'Class ) return access Keen'Class is (A_Keen(this.owner)) with Inline_Always;

    function Directive( this : Root_State'Class; d : Directive_Bits ) return Boolean is (this.obj.Directive( d )) with Inline_Always;

    ----------------------------------------------------------------------------

    type Global_State is new Root_State with null record;

    function Handle_Message( this   : access Global_State;
                             name   : Hashed_String;
                             params : Map_Value ) return Boolean;

    ----------------------------------------------------------------------------

    type Moving_State is new Root_State with null record;

    procedure On_Top( this : access Moving_State );

    procedure On_Exit( this : access Moving_State );

    function Handle_Message( this   : access Moving_State;
                             name   : Hashed_String;
                             params : Map_Value ) return Boolean;

    function Tick( this : access Moving_State; time : Tick_Time ) return Boolean;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Starts Keen aiming up or down (on ground or in-air).
    procedure Start_Aiming( this : not null access Moving_State'Class;
                            vDir : Cardinal_Direction );

    -- Stops Keen aiming up or down, if he was aiming (on ground or in-air).
    procedure Stop_Aiming( this : not null access Moving_State'Class;
                           vDir : Cardinal_Direction );

    procedure Start_Jumping( this        : not null access Moving_State'Class;
                             minHeight   : Float;
                             maxHeight   : Float;
                             sound       : String;
                             soundRepeat : Time_Span := Time_Span_Zero );

    -- Stops the upward thrust of a jump if keen is jumping and has surpassed
    -- the minimum jump height. Unless 'force' is set to True, this does not
    -- allow keen to stop a jump early (before the minimum jump height), doing
    -- only a little hop.
    procedure Stop_Jumping( this  : not null access Moving_State'Class;
                            force : Boolean := False );

    -- Starts Keen thrusting left or right if he isn't already thrusting in a
    -- direction. 'accel' is the acceleration up to moving speed.
    procedure Start_Thrusting( this  : not null access Moving_State'Class;
                               hDir  : Cardinal_Direction;
                               accel : Float );

    -- Stops thrusting Keen left or right in the moving state, if he was
    -- thrusting in 'hDir' direction.
    procedure Stop_Thrusting( this : not null access Moving_State'Class;
                              hDir : Cardinal_Direction );

    procedure Update_Frame( this : not null access Moving_State'Class );

    ----------------------------------------------------------------------------

    type Pogoing_State is new Root_State with null record;

    procedure On_Enter( this : access Pogoing_State );

    procedure On_Exit( this : access Pogoing_State );

    function Tick( this : access Pogoing_State; time : Tick_Time ) return Boolean;

    procedure Update_Frame( this : not null access Pogoing_State'Class );

    ----------------------------------------------------------------------------

    type Moving_Shooting_State is new Root_State with null record;

    procedure On_Enter( this : access Moving_Shooting_State );

    function Tick( this : access Moving_Shooting_State; time : Tick_Time ) return Boolean;

    procedure Update_Frame( this : not null access Moving_Shooting_State'Class );

    ----------------------------------------------------------------------------

    type Looking_State is new Root_State with null record;

    procedure On_Enter( this : access Looking_State );

    procedure On_Top( this : access Looking_State );

    function Tick( this : access Looking_State; time : Tick_Time ) return Boolean;

    ----------------------------------------------------------------------------

    type Looking_Shooting_State is new Root_State with null record;

    procedure On_Enter( this : access Looking_Shooting_State );

    function Tick( this : access Looking_Shooting_State; time : Tick_Time ) return Boolean;

    ----------------------------------------------------------------------------

    type Looking_Back_State is new Root_State with null record;

    procedure On_Enter( this : access Looking_Back_State );

    procedure On_Exit( this : access Looking_Back_State );

    function Tick( this : access Looking_Back_State; time : Tick_Time ) return Boolean;

    ----------------------------------------------------------------------------

    type Teleporting_State is new Root_State with null record;

    procedure On_Enter( this : access Teleporting_State );

    function Handle_Message( this   : access Teleporting_State;
                             name   : Hashed_String;
                             params : Map_Value ) return Boolean;

    function Tick( this : access Teleporting_State; time : Tick_Time ) return Boolean;

    ----------------------------------------------------------------------------

    type Idle_State is new Root_State with null record;

    procedure On_Enter( this : access Idle_State );

    procedure On_Exit( this : access Idle_State );

    function Tick( this : access Idle_State; time : Tick_Time ) return Boolean;

    procedure Update_Frame( this : not null access Idle_State'Class );

    ----------------------------------------------------------------------------

    type Rising_State is new Root_State with null record;

    procedure On_Enter( this : access Rising_State );

    function Tick( this : access Rising_State; time : Tick_Time ) return Boolean;

    ----------------------------------------------------------------------------

    type Dying_State is new Root_State with
        record
            dead   : Boolean := False;
            deathX : Float := 0.0;
            deathY : Float := 0.0;
        end record;

    procedure Die( this : not null access Global_State'Class );

    procedure On_Enter( this : access Dying_State );

    function Tick( this : access Dying_State; time : Tick_Time ) return Boolean;

    ----------------------------------------------------------------------------

    type Climbing_State is new Root_State with null record;

    procedure On_Enter( this : access Climbing_State );

    procedure On_Exit( this : access Climbing_State );

    function Tick( this : access Climbing_State; time : Tick_Time ) return Boolean;

    procedure Update_Frame( this : not null access Climbing_State'Class );

    ----------------------------------------------------------------------------

    type Climbing_Shooting_State is new Root_State with null record;

    procedure On_Enter( this : access Climbing_Shooting_State );

    function Tick( this : access Climbing_Shooting_State; time : Tick_Time ) return Boolean;

    ----------------------------------------------------------------------------

    type Switching_State is new Root_State with null record;

    procedure On_Enter( this : access Switching_State );

    function Tick( this : access Switching_State; time : Tick_Time ) return Boolean;

    ----------------------------------------------------------------------------

    type Hanging_State is new Root_State with null record;

    procedure On_Enter( this : access Hanging_State );

    function Tick( this : access Hanging_State; time : Tick_Time ) return Boolean;

    ----------------------------------------------------------------------------

    type Clambering_State is new Root_State with null record;

    procedure On_Enter( this : access Clambering_State );

    procedure On_Exit( this : access Clambering_State );

    function Tick( this : access Clambering_State; time : Tick_Time ) return Boolean;

    --==========================================================================

    -- Possible values for the 'activateable' entity attribute that determines
    -- what Keen does when he activates another entity.
    ACTIVATE_EXIT     : constant Value := Create( "exit" );
    ACTIVATE_SWITCH   : constant Value := Create( "switch" );
    ACTIVATE_TELEPORT : constant Value := Create( "teleport" );

    -- Sets Keen's frame to match action 'action' and facing in the appropriate
    -- direction, when applicable.
    procedure Set_Frame( this : not null access Keen'Class; action : Visible_Action );

    -- Spawns a shoot travelling 'dir' away from Keen. Frame 'frameId' is the
    -- corresponding shooting frame which holds the shot spawn location relative
    -- to Keen's frame.
    procedure Spawn_Shot( this : not null access Keen'Class;
                          dir  : Cardinal_Direction );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Keen( properties : Map_Value ) return A_Component is
        pragma Unreferenced( properties );
        this : constant A_Keen := new Keen;
    begin
        this.Construct;
        return A_Component(this);
    end Create_Keen;

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access Keen ) is

        procedure Read_Pref( name : String; field : in out Float ) is
        begin
            field := Preferences.Get_Pref( "development", "components.KeenBehavior." & name, field );
        end Read_Pref;

        global : A_State;
        state  : A_State;
    begin
        -- construct the state tree
        global := new Global_State;

            state := new Moving_State;
                state.Add_State( MOVING_SHOOTING, new Moving_Shooting_State );
                state.Add_State( POGOING,         new Pogoing_State );
                state.Add_State( IDLE,            new Idle_State );
                state.Add_State( RISING,          new Rising_State );
            global.Add_State( MOVING, state );

            state := new Looking_State;
                state.Add_State( LOOKING_SHOOTING, new Looking_Shooting_State );
            global.Add_State( LOOKING, state );

            state := new Climbing_State;
                state.Add_State( CLIMBING_SHOOTING, new Climbing_Shooting_State );
            global.Add_State( CLIMBING, state );

            global.Add_State( LOOKING_BACK, new Looking_Back_State );
            global.Add_State( TELEPORTING,  new Teleporting_State );
            global.Add_State( DYING,        new Dying_State );
            global.Add_State( SWITCHING,    new Switching_State );
            global.Add_State( HANGING,      new Hanging_State );
            global.Add_State( CLAMBERING,   new Clambering_State );

        Stateful(this.all).Construct( global, MOVING );

        Read_Pref( "walkSpeed",              this.walkSpeed              );
        Read_Pref( "walkAcceleration",       this.walkAcceleration       );
        Read_Pref( "walkFriction",           this.walkFriction           );
        Read_Pref( "jumpSpeed",              this.jumpSpeed              );
        Read_Pref( "jumpAcceleration",       this.jumpAcceleration       );
        Read_Pref( "fallSpeed",              this.fallSpeed              );
        Read_Pref( "midairAcceleration",     this.midairAcceleration     );
        Read_Pref( "midairPogoAcceleration", this.midairPogoAcceleration );
        Read_Pref( "minJumpHeight",          this.minJumpHeight          );
        Read_Pref( "maxJumpHeight",          this.maxJumpHeight          );
        Read_Pref( "minPogoHeight",          this.minPogoHeight          );
        Read_Pref( "maxPogoHeight",          this.maxPogoHeight          );
        Read_Pref( "maxBounceHeight",        this.maxBounceHeight        );
        Read_Pref( "pogoFriction",           this.pogoFriction           );
        Read_Pref( "lookSpeed",              this.lookSpeed              );
        Read_Pref( "climbSpeed",             this.climbSpeed             );
        Read_Pref( "climbAcceleration",      this.climbAcceleration      );
        Read_Pref( "slideSpeed",             this.slideSpeed             );
        Read_Pref( "slideAcceleration",      this.slideAcceleration      );
    end Construct;

    ----------------------------------------------------------------------------

    function Is_Blocked( this : not null access Keen'Class; dir : Cardinal_Direction ) return Boolean is
        mapClipped : constant A_Map_Clipped := A_Map_Clipped(this.Get_Component( MAP_CLIPPED_ID ));
        solid      : A_Solid;
    begin
        if mapClipped /= null and then mapClipped.Is_Blocked( dir ) then
            return True;
        end if;
        solid := A_Solid(this.Get_Component( SOLID_ID ));
        return solid /= null and then solid.In_Contact( (case dir is
                                                         when Up    => TopEdge,
                                                         when Down  => BottomEdge,
                                                         when Left  => LeftEdge,
                                                         when Right => RightEdge) );
    end Is_Blocked;

    ----------------------------------------------------------------------------

    overriding
    function Object_Input( stream : access Root_Stream_Type'Class ) return Keen is
        this : aliased Keen;
    begin
        this.Construct;
        Keen'Read( stream, this );
        return this;
    end Object_Input;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Keen ) is
    begin
        Stateful'Read( stream, Stateful(obj) );

        obj.dir := Direction_Type'Input( stream );
        obj.jumping := Boolean'Input( stream );
        obj.jumpApexMin := Float'Input( stream );
        obj.jumpApexMax := Float'Input( stream );
        obj.stateTimer.endTime := Time_Span'Input( stream );
        obj.idleTimer.endTime := Time_Span'Input( stream );
        obj.idleNumber := Positive'Input( stream );
        obj.portalId := Entity_Id'Input( stream );
        obj.poleId := Entity_Id'Input( stream );
        obj.poleTop := Boolean'Input( stream );
        obj.poleBottom := Boolean'Input( stream );
    end Object_Read;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Keen ) is
    begin
        Stateful'Write( stream, Stateful(obj) );

        Direction_Type'Output( stream, obj.dir );
        Boolean'Output( stream, obj.jumping );
        Float'Output( stream, obj.jumpApexMin );
        Float'Output( stream, obj.jumpApexMax );
        Time_Span'Output( stream, obj.stateTimer.endTime );
        Time_Span'Output( stream, obj.idleTimer.endTime );
        Positive'Output( stream, obj.idleNumber );
        Entity_Id'Output( stream, obj.portalId );
        Entity_Id'Output( stream, obj.poleId );
        Boolean'Output( stream, obj.poleTop );
        Boolean'Output( stream, obj.poleBottom );
    end Object_Write;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Added( this : access Keen ) is
    begin
        -- grab quick references to the other components
        this.loc := A_Location(this.Get_Component( LOCATION_ID ));
        this.mov := A_Movement(this.Get_Component( MOVEMENT_ID ));
        this.vis := A_Visible(this.Get_Component( VISIBLE_ID ));

        this.stateTimer.Init( this );
        this.idleTimer.Init( this );

        -- reset the idle delay
        this.idleTimer.Start( idle_animations(this.idleNumber).idleDelay );

        this.Register_Listener( MSG_EnterTile );
        this.Register_Listener( MSG_HitEntity );
        this.Register_Listener( MSG_HitWall );
        this.Register_Listener( MSG_KillPlayer );
        this.Register_Listener( MSG_Switching );
    end On_Added;

    ----------------------------------------------------------------------------

    procedure Set_Frame( this : not null access Keen'Class; action : Visible_Action ) is

        ------------------------------------------------------------------------

        procedure Resolve_Id( id : out Natural; frame : in out Frame_Info ) is
        begin
            if frame.id < 0 then
                frame.id := this.Get_Library.Get.Get_Id( frame.filename.all );
            end if;
            id := frame.id;
        end Resolve_Id;

        ------------------------------------------------------------------------

        id  : Natural := 0;
        dir : Direction_8;
    begin
        if frames(action).directional then
            -- determine the D8 direction based on Keen's facing direction and
            -- his horizontal velocity. His facing direction is determined by
            -- which way his is being controlled to move, and his velocity
            -- follows it. the horizontal direction of his velocity overrides
            -- his control direction for the displayed frame.
            dir := To_D8( this.mov.Get_V.x, this.dir );

            if dir in frames(action).dir'Range and then frames(action).dir(dir).filename /= null then
                -- found a frame for the exact direction
                Resolve_Id( id, frames(action).dir(dir) );
            else
                -- if there's no frame for Keen's exact direction, then check if he
                -- has a partially matching left or right frame (ex: Up+Left => Left,
                -- Down+Right => Right).
                if (this.dir and Left) or (this.dir and Right) then
                    dir := To_D8( this.mov.Get_V.x, (this.dir - Up) - Down );
                    if dir in frames(action).dir'Range and then frames(action).dir(dir).filename /= null then
                        Resolve_Id( id, frames(action).dir(dir) );
                    end if;
                end if;

                -- if there's no frame for Keen's exact direction and no partial
                -- match with a left or right frame, then check if he has a partial
                -- match with an up/down frame (ex: Up+Left => Up, Down+Right => Down).
                if id = 0 then
                    if (this.dir and Up) or (this.dir and Down) then
                        dir := To_D8( (this.dir - Left) - Right );
                        if dir in frames(action).dir'Range and then frames(action).dir(dir).filename /= null then
                            Resolve_Id( id, frames(action).dir(dir) );
                        end if;
                    end if;
                end if;
            end if;
        else
            -- not-directional frame
            Resolve_Id( id, frames(action).frame );
        end if;

        if this.stateFrame /= id then
            this.stateFrame := id;
            this.vis.Set_Frame( id );
        end if;
    end Set_Frame;

    ----------------------------------------------------------------------------

    procedure Spawn_Shot( this : not null access Keen'Class;
                          dir  : Cardinal_Direction ) is
        vel  : constant Vec2 := To_Vector( dir ) * SHOT_SPEED;
        tile : constant A_Tile := this.Get_Library.Get.Get_Tile( this.vis.Get_Frame );
        x, y : Float;
    begin
        -- x,y is relative to Keen's current sprite image (ex: 0,0 = upper left
        -- of the frame).
        x := this.loc.Get_X - (Float(Al_Get_Bitmap_Width( tile.Get_Bitmap )) / 2.0) + tile.Get_OffsetX + Cast_Float( tile.Get_Attribute( "gunX" ), 0.0 );
        y := this.loc.Get_Y - (Float(Al_Get_Bitmap_Height( tile.Get_Bitmap )) / 2.0) + tile.Get_OffsetY + Cast_Float( tile.Get_Attribute( "gunY" ), 0.0 );
        Queue_Spawn_Entity( "StunnerShot",
                            Create_Map( (Pair("Location",
                                              Create_Map( (Pair("x", Create( x )),
                                                           Pair("y", Create( y ))) )),
                                         Pair("Movement",
                                              Create_Map( (Pair("vx", Create( vel.x )),
                                                           Pair("vy", Create( vel.y ))) )))
                                      ).Map );
    end Spawn_Shot;

    --========================================================================--
    -- Global_State                                                           --
    --========================================================================--

    -- This is in the global state because Keen can die in any state.
    procedure Die( this : not null access Global_State'Class ) is
    begin
        if Coerce_Boolean( this.obj.owner.Get_Attributes.Get_Namespace_Name( "god" ) ) then
            return;
        end if;

        -- bounce away in pain
        this.obj.jumping := True;
        this.obj.jumpApexMin := Float'Max( this.obj.loc.Get_Y - this.obj.maxBounceHeight, 0.0 );
        this.obj.jumpApexMax := this.obj.jumpApexMin;
        this.obj.mov.Set_V_Target( (this.obj.walkSpeed / 2.0, this.obj.jumpSpeed),
                                  (IMPULSE, IMPULSE) );
        Queue_Play_Sound( "keen_die.wav", Milliseconds( 800 ) );

        -- switch to dying state, if not already in it
        this.Set_Next( DYING );
    end Die;

    ----------------------------------------------------------------------------

    overriding
    function Handle_Message( this   : access Global_State;
                             name   : Hashed_String;
                             params : Map_Value ) return Boolean is
    begin
        if name = MSG_EnterTile then
            if Cast_String( this.obj.Get_World.Get_Tile_Attribute( params.Get_Int( "x" ),
                                                                   params.Get_Int( "y" ),
                                                                   "action" ) ) = "hazard"
            then
                this.Die;
            end if;

        elsif name = MSG_KillPlayer then
            this.Die;
        end if;

        return True;
    end Handle_Message;

    --========================================================================--
    -- Moving_State                                                           --
    --========================================================================--

    overriding
    procedure On_Top( this : access Moving_State ) is
    begin
        -- capture Keen's supported state when popping back to the Moving_State
        -- for use in the next logic frame
        this.obj.wasSupported := this.obj.Is_Supported;
        A_Camera_Target(this.obj.Get_Owner.Get_Component( CAMERA_TARGET_ID )).Set_Bounds( (if this.obj.Is_Supported then GROUNDED_BOUNDS else AIRBORNE_BOUNDS) );

        -- reset the idle timer when popping back to Moving_State
        this.obj.idleTimer.Restart;

        this.Update_Frame;
    end On_Top;

    ----------------------------------------------------------------------------

    overriding
    function Handle_Message( this   : access Moving_State;
                             name   : Hashed_String;
                             params : Map_Value ) return Boolean is
    begin
        if name = MSG_HitWall then
            if params.Get_String( "dir" ) = "up" and this.obj.mov.Get_V.y < 0.0 then
                Queue_Play_Sound( "keen_ceiling", BONK_REPEAT_DELAY );
                if this.obj.jumpApexMax >= 0.0 then
                    this.Stop_Jumping( force => True );
                end if;
                return True;
            end if;

        elsif name = MSG_HitEntity then
            if params.Get_String( "edge" ) = "top" then
                Queue_Play_Sound( "keen_ceiling", BONK_REPEAT_DELAY );
                if this.obj.jumpApexMax >= 0.0 then
                    this.Stop_Jumping( force => True );
                end if;
                return True;
            end if;

        elsif name = MSG_Switching then
            this.Set_Next( SWITCHING );
            return True;

        end if;

        -- handle hazzard tile and kill player messages
        return False;
    end Handle_Message; -- Moving_State

    ----------------------------------------------------------------------------

    overriding
    function Tick( this : access Moving_State; time : Tick_Time ) return Boolean is
        pragma Unreferenced( time );

        ------------------------------------------------------------------------

        function Try_To_Activate_Touching_Entities return Boolean is
            activated : Boolean := False;

            --------------------------------------------------------------------

            procedure Activate( e : A_Entity ) is
                activateable : constant Value := e.Get_Attributes.Get_Namespace_Name( "activateable" );
            begin
                if activateable.Is_String then
                    e.Dispatch_Message( MSG_Activate, Create_Map( (1=>Pair("eid", To_Id_Value( this.obj.Get_Owner.Get_Id ))) ).Map );
                    if activateable = ACTIVATE_SWITCH then
                        this.Set_Next( SWITCHING );
                    elsif activateable = ACTIVATE_TELEPORT then
                        this.obj.portalId := e.Get_Id;
                        this.Set_Next( TELEPORTING );
                    end if;
                    activated := True;
                end if;
            end Activate;

            --------------------------------------------------------------------

        begin
            -- only activate entities if Keen is standing on the ground
            if not this.obj.jumping and then this.obj.Is_Supported then
                A_Collidable(this.obj.Get_Component( COLLIDABLE_ID )).Iterate_Touching( Activate'Access );
                if activated then
                    -- the Up directive can be set inactive on entering the
                    -- switching state, because that's the state that occurs
                    -- when Keen activates something. if Keen is touching multiple
                    -- entities, they should all be activated.
                    this.obj.Get_Owner.Set_Directive( Directives.Galaxy.UP, Inactive );
                end if;
            end if;

            return activated;
        end Try_To_Activate_Touching_Entities;

        ------------------------------------------------------------------------

        function Try_To_Grab_Pole( vDir : Cardinal_Direction ) return Boolean is

            grabbed : Boolean := False;

            --------------------------------------------------------------------

            -- try to grab entity 'e' as though it's a climbable pole, if Keen
            -- is properly positioned relative to it. send it a "TryToClimb"
            -- message if Keen should grab it. The 'e' entity will respond with
            -- a "Climb" message if it can be climbed.
            procedure Try_To_Grab( e : A_Entity ) is
                eLoc : constant A_Location := A_Location(e.Get_Component( LOCATION_ID ));
            begin
                if grabbed then
                    return;
                end if;

                -- if keen is standing on the ground and trying to climb down but
                -- the pole doesn't extend into the ground, don't start climbing
                if vDir = Up or else
                   this.obj.loc.Get_Y + Float(this.obj.loc.Get_Height + this.obj.Get_World.Tile_Width) / 2.0 <= eLoc.Get_Bottom or else   -- room to climb down?
                   (this.obj.Get_Component( MAP_CLIPPED_ID ) /= null and then not this.obj.Is_Supported)
                then
                    -- keen must be no higher than the top of the pole and no
                    -- lower than the middle of his body at the bottom of the
                    -- pole.
                    --
                    -- we check one pixel higher than the pole top because keen
                    -- standing on top of the ground and is exactly 2 tiles
                    -- (32 px) tall. this causes his head to be one pixel higher
                    -- than the tile boundary where the pole ends when it sticks
                    -- 2 tiles up from the ground.
                    if this.obj.loc.Get_Top >= eLoc.Get_Top - 1.0 and then
                       this.obj.loc.Get_Y < eLoc.Get_Bottom - POLE_BOTTOM_BUFFER
                    then
                        -- the above check ensures Keen is not at the top of the
                        -- pole. we'll clear .poleTop if he starts climing.

                        -- keen must have the center of his body on the pole
                        if this.obj.loc.Get_X >= eLoc.Get_Left and then
                           this.obj.loc.Get_X <= eLoc.Get_Right
                        then
                            -- check if the entity is a climbable pole
                            if e.Get_Attributes.Get_Namespace_Name( "climbable" ).To_Boolean then
                                grabbed := True;
                                this.obj.poleId := e.Get_Id;
                                this.Set_Next( CLIMBING );
                            end if;
                        end if;
                    end if;
                end if;
            end Try_To_Grab;

            --------------------------------------------------------------------

        begin
            -- Keen can grab poles if he's either standing on the ground or falling downward
            if this.obj.Is_Supported or else this.obj.mov.Get_V.y > 0.0 then
                A_Collidable(this.obj.Get_Component( COLLIDABLE_ID )).Iterate_Touching( Try_To_Grab'Access );
            end if;
            return grabbed;
        end Try_To_Grab_Pole;

        ------------------------------------------------------------------------

    begin
        -- set camera bounds for walking vs. jumping
        A_Camera_Target(this.obj.Get_Owner.Get_Component( CAMERA_TARGET_ID )).Set_Bounds( (if this.obj.Is_Supported then GROUNDED_BOUNDS else AIRBORNE_BOUNDS) );

        -- Keen's bottom can be temporarily disabled to drop down from an entity
        if this.obj.bottomlessCounter > 0 then
            this.obj.bottomlessCounter := this.obj.bottomlessCounter - 1;
            if this.obj.bottomlessCounter = 0 then
                A_Solid(this.obj.Get_Owner.Get_Component( SOLID_ID )).Set_Bottom_Solid( True );
            end if;
        end if;

        -- accerate left/right
        if this.Directive( Directives.Galaxy.LEFT ) then
           if not this.obj.Is_Supported then
                this.Start_Thrusting( Left, this.obj.midairAcceleration );
            else
                this.Start_Thrusting( Left, this.obj.walkAcceleration );
            end if;
        else
            this.Stop_Thrusting( Left );
        end if;
        if this.Directive( Directives.Galaxy.RIGHT ) then
            if not this.obj.Is_Supported then
                this.Start_Thrusting( Right, this.obj.midairAcceleration );
            else
                this.Start_Thrusting( Right, this.obj.walkAcceleration );
            end if;
        else
            this.Stop_Thrusting( Right );
        end if;

        -- the UP directive...
        --   activates entities
        --   or begins climbing a pole
        --   or aims upward
        if this.Directive( Directives.Galaxy.UP ) then
            if not Try_To_Activate_Touching_Entities then
                if Try_To_Grab_Pole( Up ) then
                    return True;
                else
                    this.Start_Aiming( Up );
                end if;
            end if;
        else
            this.Stop_Aiming( Up );
        end if;
        if this.Directive( Directives.Galaxy.DOWN ) then
            if this.obj.Is_Supported and then Try_To_Grab_Pole( Down ) then
                return True;
            else
                this.Start_Aiming( Down );
            end if;
        else
            this.Stop_Aiming( Down );
        end if;

        if this.Directive( Directives.Galaxy.POGO ) then
            -- don't allow Keen to hop on his pogo if he's in a tight space that
            -- is too small to bounce
            if not (this.obj.Is_Blocked( Up ) and then this.obj.Is_Blocked( Down )) then
                this.Set_Next( POGOING );
                return True;
            end if;
        end if;

        if this.Directive( Directives.Galaxy.JUMP ) then
            -- keen can only start jumping if he hasn't started a jump before now.
            -- Start_Jumping() will also check if he's properly supported and
            -- unblocked.
            if not this.obj.jumping then
                this.Start_Jumping( this.obj.minJumpHeight,
                                    this.obj.maxJumpHeight,
                                    "keen_jump.wav" );
            end if;
        else
            -- if the directive is not active anymore, reset the .jumping flag
            -- when keen is safely back on the ground. don't allow it to be
            -- reset in mid-air or he'll be able to do a double jump as soon as
            -- he touches the ground again. the check for .jumpApexMax < 0
            -- handles the very brief case where keen is accelerating upward but
            -- has not left the ground yet (checking only Is_Supported() may be
            -- insufficient).
            if this.obj.Is_Supported and this.obj.jumpApexMax < 0.0 then
                this.obj.jumping := False;
            end if;
            -- try to stop the jump; this will succeed if he's jumping but only
            -- after he reaches the minimum apex height.
            this.Stop_Jumping;
        end if;

        -- if keen is jumping up, stop the jump when he reaches maximum height
        if this.obj.loc.Get_Y <= this.obj.jumpApexMax then
            this.Stop_Jumping;
        end if;

        -- change to the Looking state if standing on the ground and looking.
        -- this check must happen after checking the jump directive, because the
        -- looking state switches to this state when Jump is pressed. the jump
        -- key must take priority to avoid immediately looping back to looking.
        if ((this.obj.dir and Up) or (this.obj.dir and Down))
           and then this.obj.Is_Supported
           and then not this.obj.jumping
           and then not this.obj.mov.Is_Moving
        then
            -- change to the looking up/down state
            this.Set_Next( LOOKING );
            return True;
        end if;

        -- these are given by Tinker to change Keen's orientation
        if this.Directive( Directives.Galaxy.FACE_LEFT ) then
            this.obj.dir := Dir_Left;
        elsif this.Directive( Directives.Galaxy.FACE_RIGHT ) then
            this.obj.dir := Dir_Right;
        end if;

        -- check if there's a ledge to grab. the player must be falling and
        -- moving into a ledge
        if this.obj.mov.Get_V.y > 0.0 then
            declare
                world     : constant A_World := this.obj.Get_World;
                senseY1   : constant Float := this.obj.loc.Get_Top - GRAB_HEIGHT;
                senseY2   : constant Float := senseY1 + GRAB_RANGE;
                senseX    : Float;
                checkEdge : Boolean := False;
            begin
                if this.obj.mov.Is_Moving( Left ) then
                    senseX := this.obj.loc.Get_Left - 1.0;
                    checkEdge := True;
                elsif this.obj.mov.Is_Moving( Right ) then
                    senseX := this.obj.loc.Get_Right + 1.0;
                    checkEdge := True;
                end if;

                if checkEdge and then
                   world.Get_Clip_Type( senseX, senseY1 ) = Passive and then
                   world.Get_Clip_Type( senseX, senseY2 ) = Wall
                then
                    this.Set_Next( HANGING );
                    return True;
                end if;
            end;
        end if;

        -- check if Keen fell off a platform
        if this.obj.wasSupported and then
           not this.obj.Is_Supported and then
           this.obj.mov.Get_V.y > 0.0
        then
            Queue_Play_Sound( "keen_fall.wav" );
        end if;

        -- check for the shoot directive, then stop jumping and start shooting
        if this.Directive( Directives.Galaxy.SHOOT ) then
            this.Stop_Jumping;
            this.Set_Next( MOVING_SHOOTING );
            -- no need to set the moving frame
            this.obj.wasSupported := this.obj.Is_Supported;
            return True;
        end if;

        -- - - - - - -

        if this.obj.Is_Supported
           and then not this.obj.jumping
           and then not this.obj.mov.Is_Moving
           and then not this.obj.In_Editor
        then
            -- keen is standing still on the ground
            -- check if its time to start an idle animation
            if this.obj.idleTimer.Expired then
                this.Set_Next( IDLE );
            end if;
        else
            this.obj.idleTimer.Restart;
        end if;

        this.obj.wasSupported := this.obj.Is_Supported;
        this.Update_Frame;

        -- Global_State does not need to be ticked
        return True;
    end Tick; -- Moving_State

    ----------------------------------------------------------------------------

    overriding
    procedure On_Exit( this : access Moving_State ) is
    begin
        -- if Keen's bottom was temporarily disabled to drop down from an entity,
        -- put it back again.
        if this.obj.bottomlessCounter > 0 then
            this.obj.bottomlessCounter := 0;
            A_Solid(this.obj.Get_Owner.Get_Component( SOLID_ID )).Set_Bottom_Solid( True );
        end if;
    end On_Exit;

    ----------------------------------------------------------------------------

    -- starts Keen aiming up or down (on ground or in-air). Returns True if
    -- Keen's state changes to aiming.
    procedure Start_Aiming( this : not null access Moving_State'Class; vDir : Cardinal_Direction ) is
    begin
        -- aims in the given direction
        this.obj.dir := this.obj.dir + vDir;
    end Start_Aiming;

    ----------------------------------------------------------------------------

    -- stops Keen aiming up or down, if he was aiming (on ground or in-air)
    procedure Stop_Aiming( this : not null access Moving_State'Class; vDir : Cardinal_Direction ) is
    begin
        this.obj.dir := this.obj.dir - vDir;
    end Stop_Aiming;

    ----------------------------------------------------------------------------

    -- JUMPING VARIABLES
    -- (.jumping, .jumpApexMin, .jumpApexMax)
    --
    -- Starts jump (thrusting upward), if possible. Upward acceleration
    -- begins at the start of the jump and is maintained until at least the
    -- minimum jump height is reached, or the maximum jump height is
    -- reached. When the jump begins, .jumping is set True for as long as
    -- the jump directive is given and .jumpApexMax >= 0 until the apex of
    -- the jump is reached. The apex of the jump is reached when Keen either
    -- (1) hits is head on the ceiling, or (2) his jump height reaches
    -- .jumpApexMax, or (3) his jump height reaches .jumpApexMin and the
    -- jump directive is ended. After the apex, Keen will be off the ground,
    -- which prevents Start_Jumping() from starting another jump in mid-air.
    -- Once he lands on the ground again, .jumping is still True if the
    -- directive is still being held, which prevents a double-jump unless
    -- he's on his pogo.
    procedure Start_Jumping( this        : not null access Moving_State'Class;
                             minHeight   : Float;
                             maxHeight   : Float;
                             sound       : String;
                             soundRepeat : Time_Span := Time_Span_Zero ) is
    begin
        -- keen can only start jumping if he's standing on the ground and not
        -- blocked from above.
        if this.obj.Is_Supported and then not this.obj.Is_Blocked( Up ) then
            this.obj.jumpApexMin := Float'Max( this.obj.loc.Get_Y - minHeight, 0.0 );
            this.obj.jumpApexMax := Float'Max( this.obj.loc.Get_Y - maxHeight, 0.0 );
            this.obj.mov.Set_VY_Target( this.obj.jumpSpeed, this.obj.jumpAcceleration );
            Queue_Play_Sound( sound, soundRepeat );
            this.obj.jumping := True;
        end if;
    end Start_Jumping;

    ----------------------------------------------------------------------------

    procedure Stop_Jumping( this : not null access Moving_State'Class; force : Boolean := False ) is
    begin
        -- check if he's allowed to stop the jump yet (he must jump higher
        -- than the minimum jump height unless 'force' is True because he hit
        -- his head or something).
        if this.obj.loc.Get_Y <= this.obj.jumpApexMin or force then
            this.obj.jumpApexMin := -1.0;
            this.obj.jumpApexMax := -1.0;
            this.obj.mov.Set_VY_Target( 0.0, 0.0 );
        else
            -- even if the jump is not aborted because the minimum height hasn't
            -- been reached, we won't allow it to go beyond the minimum jump height.
            this.obj.jumpApexMax := this.obj.jumpApexMin;
        end if;
    end Stop_Jumping;

    ----------------------------------------------------------------------------

    procedure Start_Thrusting( this  : not null access Moving_State'Class;
                               hDir  : Cardinal_Direction;
                               accel : Float ) is
    begin
        -- keen can begin thrusting if he isn't already thrusting in a direction
        if not this.obj.mov.Is_Moving( Left ) and then not this.obj.mov.Is_Moving( Right ) then
            this.obj.dir := this.obj.dir + hDir;
            pragma Assert( hDir = Left or else hDir = Right, "Horizontal direction not given" );
            this.obj.mov.Set_VX_Target( To_Vector( hDir ).x * this.obj.walkSpeed, accel );
        end if;
    end Start_Thrusting;

    ----------------------------------------------------------------------------

    procedure Stop_Thrusting( this : not null access Moving_State'Class;
                              hDir : Cardinal_Direction ) is
    begin
        if this.obj.mov.Is_Moving( hDir ) then
            this.obj.mov.Set_VX_Target( 0.0, 0.0 );  -- coast to a stop
        end if;
    end Stop_Thrusting;

    ----------------------------------------------------------------------------

    procedure Update_Frame( this : not null access Moving_State'Class ) is
    begin
        if this.obj.Is_Supported then
            if this.obj.mov.Get_V.x = 0.0 then
                this.obj.Set_Frame( Standing );
            else
                Queue_Play_Sound( "keen_run", FOOTSTEP_REPEAT_DELAY );
                this.obj.Set_Frame( Walking );
            end if;
        else
            if this.obj.mov.Get_V.y < 0.0 then
                this.obj.Set_Frame( Jumping );
            elsif this.obj.mov.Get_V.y < this.obj.fallSpeed then
                this.obj.Set_Frame( Jumping_Midair );
            else
                this.obj.Set_Frame( Falling );
            end if;
        end if;
    end Update_Frame;

    --========================================================================--
    -- Pogoing_State                                                          --
    --========================================================================--

    overriding
    procedure On_Enter( this : access Pogoing_State ) is
    begin
        this.obj.stateTimer.Clear;
        A_Camera_Target(this.obj.Get_Owner.Get_Component( CAMERA_TARGET_ID )).Set_Bounds( AIRBORNE_BOUNDS );
        this.Update_Frame;
        this.obj.mov.Set_FX( this.obj.pogoFriction );
    end On_Enter;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Exit( this : access Pogoing_State ) is
    begin
        this.obj.mov.Set_FX( this.obj.walkFriction );
        if this.obj.jumpApexMax < this.obj.jumpApexMin then
            -- prevent keen from doing a quick jump to pogo height by getting on
            -- the pogo and then immediately off the pogo. he must stay on the
            -- pogo to reach great heights.
            this.obj.jumpApexMax := this.obj.jumpApexMin - (this.obj.maxJumpHeight - this.obj.minJumpHeight);
        end if;
    end On_Exit;

    ----------------------------------------------------------------------------

    overriding
    function Tick( this : access Pogoing_State; time : Tick_Time ) return Boolean is
        function Parent_State return access Moving_State is (Moving_State(this.parent.all)'Access);
        pragma Unreferenced( time );
    begin
        -- if Keen is bouncing off the ground right now, reset the bounce timer
        -- to keep showing the bouncing frame.
        if this.obj.Is_Supported then
            if this.obj.Is_Blocked( Up ) then
                -- Keen wedged himself into a space that is too tight to bounce
                -- so get off the stick.
                Parent_State.Stop_Jumping( force => True );
                this.Set_Next( MOVING );
                return True;
            end if;

            this.obj.stateTimer.Start( POGO_BOUNCE_DURATION );
        end if;

        if this.Directive( Directives.Galaxy.LEFT ) then
            Parent_State.Start_Thrusting( Left, this.obj.midairPogoAcceleration );
        else
            Parent_State.Stop_Thrusting( Left );
        end if;
        if this.Directive( Directives.Galaxy.RIGHT ) then
            Parent_State.Start_Thrusting( Right, this.obj.midairPogoAcceleration );
        else
            Parent_State.Stop_Thrusting( Right );
        end if;

        if this.Directive( Directives.Galaxy.UP ) then
            Parent_State.Start_Aiming( Up );
        else
            Parent_State.Stop_Aiming( Up );
        end if;
        if this.Directive( Directives.Galaxy.DOWN ) then
            Parent_State.Start_Aiming( Down );
        else
            Parent_State.Stop_Aiming( Down );
        end if;

        if this.Directive( Directives.Galaxy.POGO ) then
            -- turn the pogo off
            this.Set_Next( MOVING );
            return True;
        end if;

        if this.Directive( Directives.Galaxy.JUMP ) then
            -- keen can only start jumping if he hasn't started a jump before
            -- now. The .jumpApexMax field is checked because this should only
            -- happen once per jump.
            if not this.obj.jumping or this.obj.jumpApexMax < 0.0 then
                Parent_State.Start_Jumping( this.obj.minPogoHeight,
                                            this.obj.maxPogoHeight,
                                            "keen_pogo.wav",
                                            POGO_REPEAT_DELAY );
            end if;
        else
            -- if the directive is not active anymore, reset the .jumping flag
            -- when keen is safely back on the ground. don't allow it to be
            -- reset in mid-air or he'll be able to do a double jump as soon as
            -- he touches the ground again. the check for .jumpApexMax < 0
            -- handles the very brief case where keen is accelerating upward but
            -- has not left the ground yet (just Is_Supported may be insufficient).
            if this.obj.Is_Supported and this.obj.jumpApexMax < 0.0 then
                this.obj.jumping := False;
            end if;
            -- try to stop the jump; this will succeed if he's jumping but only
            -- after he reaches the minimum apex height.
            Parent_State.Stop_Jumping;
        end if;

        if this.Directive( Directives.Galaxy.SHOOT ) then
            -- turn the pogo off and start shooting
            Parent_State.Stop_Jumping;
            this.Set_Next( MOVING_SHOOTING );
            return True;
        end if;

        -- attempt to auto-bounce off the ground
        -- this only succeeds if keen is on the ground
        Parent_State.Start_Jumping( this.obj.minPogoHeight,
                                    this.obj.maxPogoHeight,
                                    "keen_pogo.wav",
                                    POGO_REPEAT_DELAY );

        -- if keen is jumping up, stop the when when he reaches maximum height
        if this.obj.loc.Get_Y <= this.obj.jumpApexMax then
            Parent_State.Stop_Jumping;
        end if;

        this.Update_Frame;

        -- Moving_State should not be ticked
        return True;
    end Tick; -- Pogoing_State

    ----------------------------------------------------------------------------

    procedure Update_Frame( this : not null access Pogoing_State'Class ) is
    begin
        if not this.obj.stateTimer.Expired and this.obj.mov.Get_V.y <= 0.0 then
            this.obj.Set_Frame( Pogoing_On_Ground );
        else
            this.obj.Set_Frame( Pogoing_Midair );
        end if;
    end Update_Frame;

    --========================================================================--
    -- Moving_Shooting_State                                                  --
    --========================================================================--

    overriding
    procedure On_Enter( this : access Moving_Shooting_State ) is
        ammo : Integer;
        dirX : Direction_8;
    begin
        this.Update_Frame;
        this.obj.stateTimer.Start( SHOOT_DURATION );

        -- when he's on the ground, stop running to take a shot
        if this.obj.Is_Supported then
            if this.obj.mov.Is_Moving( Left ) or else this.obj.mov.Is_Moving( Right ) then
                this.obj.mov.Set_VX_Target( 0.0, 0.0 );    -- coast to a stop
            end if;
        end if;

        ammo := this.obj.Get_Session_Var( "ammo" ).To_Int;
        if ammo > 0 then
            this.obj.Set_Session_Var( "ammo", Create( ammo - 1 ) );
            Queue_Play_Sound( "keen_shoot.wav" );
            dirX := To_X( this.obj.mov.Get_V.x, this.obj.dir );
            if this.obj.dir and Up then
                this.obj.Spawn_Shot( Up );
            elsif this.obj.dir and Down then
                this.obj.Spawn_Shot( Down );
            elsif dirX = D8_Left then
                this.obj.Spawn_Shot( Left );
            elsif dirX = D8_Right then
                this.obj.Spawn_Shot( Right );
            end if;
        else
            Queue_Play_Sound( "click.wav" );
        end if;
    end On_Enter;

    ----------------------------------------------------------------------------

    overriding
    function Tick( this : access Moving_Shooting_State; time : Tick_Time ) return Boolean is
        function Parent_State return access Moving_State is (Moving_State(this.parent.all)'Access);
        pragma Unreferenced( time );
    begin
        if this.obj.stateTimer.Expired then
            this.Set_Next( MOVING );
            return True;
        end if;

        -- if keen is jumping up, stop the when when he reaches maximum height
        -- he cannot being jumping while shooting, but he may have been jumping
        -- before the shot.
        if this.obj.loc.Get_Y <= this.obj.jumpApexMax then
            Parent_State.Stop_Jumping;
        end if;

        this.Update_Frame;

        -- Moving_State should not be ticked
        return True;
    end Tick;

    ----------------------------------------------------------------------------

    procedure Update_Frame( this : not null access Moving_Shooting_State'Class ) is
    begin
        this.obj.Set_Frame( (if this.obj.Is_Supported then Standing_And_Shooting else Jumping_And_Shooting) );
    end Update_Frame;

    --========================================================================--
    -- Looking_State                                                          --
    --========================================================================--

    overriding
    procedure On_Enter( this : access Looking_State ) is
    begin
        this.obj.stateTimer.Start( (if this.obj.dir and Up then LOOK_UP_DURATION else LOOK_DOWN_DURATION) );
        A_Camera_Target(this.obj.Get_Owner.Get_Component( CAMERA_TARGET_ID )).Set_Bounds( LOOKING_BOUNDS );
    end On_Enter;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Top( this : access Looking_State ) is
    begin
        -- this is set when the looking state is entered and every time the
        -- looking_shooting state is popped off the stack.
        this.obj.Set_Frame( Looking_Frames );
    end On_Top;

    ----------------------------------------------------------------------------

    overriding
    function Tick( this : access Looking_State; time : Tick_Time ) return Boolean is
        solid : A_Solid;
    begin
        -- stop looking immediately if the floor disappears beneath keen
        if not this.obj.Is_Supported then
            Queue_Play_Sound( "keen_fall.wav" );
            this.Set_Next( MOVING );
            return True;
        end if;

        -- check if it's time to stop looking and go back to the moving state:
        -- 1) keen wants to start walking left or right OR
        -- 2) the looking directive is no longer active
        if this.Directive( Directives.Galaxy.LEFT  ) or
           this.Directive( Directives.Galaxy.RIGHT ) or
           ((this.obj.dir and Up  ) and not this.Directive( Directives.Galaxy.UP   )) or
           ((this.obj.dir and Down) and not this.Directive( Directives.Galaxy.DOWN ))
        then
            -- time to stop looking in the direction he was looking
            this.Set_Next( LOOKING_BACK );
            return True;
        end if;

        -- Keen can shoot up while he is standing looking up
        if this.Directive( Directives.Galaxy.SHOOT ) and (this.obj.dir and Up) then
            this.Set_Next( LOOKING_SHOOTING );
            return True;
        end if;

        if this.Directive( Directives.Galaxy.JUMP ) then
            if this.obj.dir and Up then
                -- if keen tries to jump while looking up then switch to the
                -- moving action so he can jump on the next Update.
                this.Set_Next( MOVING );
                return True;
            elsif this.obj.dir and Down then
                -- if keen tries to jump while looking down, he is allowed to
                -- drop down off of a lift (if he's standing on one.) if any
                -- entity being stood upon also has a solid bottom (like a bridge)
                -- then Keen should NOT be allowed to try to drop down.
                solid := A_Solid(this.obj.Get_Owner.Get_Component( SOLID_ID ));
                if solid /= null and then solid.In_Contact( BottomEdge ) then
                    declare
                        dropdown : Boolean := True;
                        procedure Examine( contact : A_Solid ) is
                        begin
                            dropdown := dropdown and not contact.Is_Bottom_Solid;
                        end Examine;
                    begin
                        solid.Iterate_Contacts( BottomEdge, Examine'Access );
                        if dropdown then
                            solid.Set_Bottom_Solid( False );
                            this.obj.bottomlessCounter := 20;
                            Queue_Play_Sound( "keen_fall.wav" );
                            this.Set_Next( MOVING );
                            return True;
                        end if;
                    end;
                end if;
            end if;
        end if;

        -- - - - - - -

        -- if keen is steadily looking up or down, scroll the view. the
        -- timer_expired() check makes sure the looking up/down animation
        -- finishes before the view starts to scroll.
        if this.obj.stateTimer.Expired then
            Queue_View_Message( "ScrollScene", Value_Array'(Create( 0 ), Create( this.obj.lookSpeed * Float(this.obj.dir.y) * Float(To_Duration( time.elapsed )) )) );
        end if;

        -- Global_State does not need to be ticked
        return True;
    end Tick; -- Looking_State

    --========================================================================--
    -- Looking_Shooting_State                                                 --
    --========================================================================--

    overriding
    procedure On_Enter( this : access Looking_Shooting_State ) is
        ammo : Integer;
    begin
        this.obj.Set_Frame( Standing_And_Shooting );

        ammo := this.obj.Get_Session_Var( "ammo" ).To_Int;
        if ammo > 0 then
            this.obj.Set_Session_Var( "ammo", Create( ammo - 1 ) );
            this.obj.Spawn_Shot( Up );
        end if;
        Queue_Play_Sound( (if ammo > 0 then "keen_shoot.wav" else "click.wav") );

        this.obj.stateTimer.Start( SHOOT_DURATION );
    end On_Enter; -- Looking_Shooting_State

    ----------------------------------------------------------------------------

    overriding
    function Tick( this : access Looking_Shooting_State; time : Tick_Time ) return Boolean is
        pragma Unreferenced( time );
    begin
        if this.obj.stateTimer.Expired then
            this.Set_Next( LOOKING );
        end if;

        -- stop shooting immediately if the floor disappears beneath keen
        if not this.obj.Is_Supported then
            Queue_Play_Sound( "keen_fall.wav" );
            this.Set_Next( MOVING );
        end if;

        -- Looking_State should not be ticked
        return True;
    end Tick; -- Looking_Shooting_State

    --========================================================================--
    -- Looking_Back_State                                                     --
    --========================================================================--

    overriding
    procedure On_Enter( this : access Looking_Back_State ) is
    begin
        this.obj.Set_Frame( Looking_Back_Frames );
        this.obj.stateTimer.Start( (if this.obj.dir and Up then LOOK_UP_DURATION else LOOK_DOWN_DURATION) );
    end On_Enter;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Exit( this : access Looking_Back_State ) is
    begin
        this.obj.dir.y := 0;      -- clear the vertical direction component
    end On_Exit;

    ----------------------------------------------------------------------------

    overriding
    function Tick( this : access Looking_Back_State; time : Tick_Time ) return Boolean is
        pragma Unreferenced( time );
    begin
        -- if keen is done looking back then go back to moving
        if this.obj.stateTimer.Expired then
            this.Set_Next( MOVING );
        end if;

        -- stop looking immediately if the floor disappears beneath keen
        if not this.obj.Is_Supported then
            Queue_Play_Sound( "keen_fall.wav" );
            this.Set_Next( MOVING );
        end if;

        -- Global_State does not need to be ticked
        return True;
    end Tick; -- Looking_Back_State

    --========================================================================--
    -- Teleporting_State                                                      --
    --========================================================================--

    overriding
    procedure On_Enter( this : access Teleporting_State ) is
        portal  : constant A_Entity := this.obj.Get_Entity( this.obj.portalId );
        portalX : constant Float := A_Location(portal.Get_Component( LOCATION_ID )).Get_X;
        conns   : List_Value;
    begin
        this.obj.Set_Frame( Entering_Doorway );
        this.obj.stateTimer.Start( TELEPORT_DURATION );

        -- had to look up to teleport; stop looking
        this.obj.dir := this.obj.dir - Up;

        -- Keen starts walking up/away from the camera, into a doorway. If Keen
        -- is not centered on the doorway/portal, he will approach it horizontally
        -- while moving upward and complete the teleport when he reaches it.
        --
        -- Keen moves 67% of the way to the center of the door (moving dead-on looks weird)
        this.obj.mov.Set_VX_Target( ((portalX - this.obj.loc.Get_X) * 0.67) /               -- travel distance
                                    Float(To_Duration( this.obj.stateTimer.Remaining )),    -- travel time
                                    IMPULSE );

        -- find the portal's destination in its connections entity attribute
        conns := portal.Get_Attributes.Get_Namespace_Name( "connections" ).Lst;
        this.obj.portalId := (if conns.Valid then To_Tagged_Id( conns.Get( 1 ) ) else NULL_ID);
    end On_Enter;

    ----------------------------------------------------------------------------

    -- Keen is temporarily invincible while teleporting. consume the deadly
    -- messages handled by the global state to avoid dying.
    overriding
    function Handle_Message( this   : access Teleporting_State;
                             name   : Hashed_String;
                             params : Map_Value ) return Boolean is (True);

    ----------------------------------------------------------------------------

    overriding
    function Tick( this : access Teleporting_State; time : Tick_Time ) return Boolean is
        pragma Unreferenced( time );
        dest    : A_Entity;
        destLoc : A_Location;
    begin
        -- check if he's done the teleporting animation yet
        if this.obj.stateTimer.Expired then
            -- stop moving left/right toward the entrance portal
            this.obj.mov.Set_VX_Target( 0.0, IMPULSE );

            -- teleport to the destination
            dest := this.obj.Get_Entity( this.obj.portalId );
            if dest /= null then
                dest.Dispatch_Message( MSG_Activate, Create_Map( (1=>Pair("eid", To_Id_Value( this.obj.Get_Owner.Get_Id ))) ).Map );
                if dest.Get_Attributes.Get_Namespace_Name( "activateable" ) = ACTIVATE_EXIT then
                    -- if an exit entity was just activated, stay put and don't
                    -- teleport because a level change has been scheduled.
                    this.obj.stateTimer.Restart;
                    return True;
                end if;

                destLoc := A_Location(dest.Get_Component( LOCATION_ID ));
                this.obj.loc.Set_XY( destLoc.Get_X, destLoc.Get_Y );
                A_Camera_Target(this.obj.Get_Component( CAMERA_TARGET_ID )).Center;   -- recenters the camera on keen
            end if;

            this.Set_Next( MOVING );
        end if;

        -- Global_State does not need to be ticked
        return True;
    end Tick; -- Teleporting_State

    --========================================================================--
    -- Idle_State                                                             --
    --========================================================================--

    overriding
    procedure On_Enter( this : access Idle_State ) is
    begin
        this.Update_Frame;

        this.obj.stateTimer.Start( idle_animations(this.obj.idleNumber).anmDuration );
        if this.obj.idleNumber = INDEX_MOONING_BORED then
            this.obj.mooned := True;
        end if;
    end On_Enter;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Exit( this : access Idle_State ) is
    begin
        -- set the idle timer to expire when the next idle animation should play
        this.obj.idleTimer.Start( idle_animations(this.obj.idleNumber).idleDelay );
    end On_Exit;

    ----------------------------------------------------------------------------

    overriding
    function Tick( this : access Idle_State; time : Tick_Time ) return Boolean is
        pragma Unreferenced( time );

        ------------------------------------------------------------------------

        function Can_Moon return Boolean is
            result : Boolean := False;

            -- check the "moon" attribute of each touching entity
            procedure Check_For_Moon( e : A_Entity ) is
            begin
                result := result or e.Get_Attributes.Get_Namespace_Name( "moon" ).To_Boolean;
            end Check_For_Moon;

        begin
            if not this.obj.mooned then
                A_Collidable(this.obj.Get_Component( COLLIDABLE_ID )).Iterate_Touching( Check_For_Moon'Access );
            end if;
            return result;
        end Can_Moon;

        ------------------------------------------------------------------------

    begin
        -- basically any input will interrupt an idle animation
        -- but shooting can't interrupt once keen starts sitting down to read (original game behavior)
        if this.Directive( Directives.Galaxy.LEFT  ) or
           this.Directive( Directives.Galaxy.RIGHT ) or
           this.Directive( Directives.Galaxy.UP    ) or
           this.Directive( Directives.Galaxy.DOWN  ) or
           this.Directive( Directives.Galaxy.JUMP  ) or
           this.Directive( Directives.Galaxy.POGO  ) or
          (this.Directive( Directives.Galaxy.SHOOT ) and this.obj.idleNumber /= INDEX_READING_BORED)
        then
            -- keen can't be interrupted while sitting down to read
            if this.obj.idleNumber /= INDEX_SITTING_BORED then
                -- if he's sitting and reading, he has to stand back up again
                -- first. otherwise, he can abort the idle animation immediately.
                this.Set_Next( (if this.obj.idleNumber = INDEX_READING_BORED then RISING else MOVING) );
                this.obj.idleNumber := idle_animations'First;
            end if;
        end if;

        -- - - - - - -

        -- check if the idle animation is done
        if this.obj.stateTimer.Expired then

            -- set the number for the next idle animation
            this.obj.idleNumber := this.obj.idleNumber + 1;

            -- mooning easter egg logic:
            -- play the mooning idle animation only when keen is touching an
            -- entity with a "moon" entity attribute of True, and play it
            -- only once per life.
            if this.obj.idleNumber = INDEX_MOONING_BORED and then not Can_Moon then
                this.obj.idleNumber := this.obj.idleNumber + 1;
            end if;

            if this.obj.idleNumber > idle_animations'Last then
                this.obj.idleNumber := idle_animations'Last;
            end if;

            -- check if another idle animation should begin immediately
            if idle_animations(this.obj.idleNumber).idleDelay <= Time_Span_Zero then
                -- remain in the idle state, setting the state timer to the
                -- animation duration of the current idle animation.
                this.obj.stateTimer.Start( idle_animations(this.obj.idleNumber).anmDuration );
            else
                this.Set_Next( MOVING );
            end if;
        end if;

        -- stop being idle immediately if the floor disappears beneath keen
        if not this.obj.Is_Supported then
            this.obj.idleNumber := idle_animations'First;
            Queue_Play_Sound( "keen_fall.wav" );
            this.Set_Next( MOVING );
        end if;

        this.Update_Frame;

        -- Moving_State does not need to be ticked
        return True;
    end Tick; -- Idle_State

    ----------------------------------------------------------------------------

    procedure Update_Frame( this : not null access Idle_State'Class ) is
    begin
        this.obj.Set_Frame( idle_animations(this.obj.idleNumber).visibleAction );
    end Update_Frame;

    --========================================================================--
    -- Rising_State                                                           --
    --========================================================================--

    overriding
    procedure On_Enter( this : access Rising_State ) is
    begin
        this.obj.Set_Frame( Rising_Bored );
        this.obj.stateTimer.Start( RISING_DURATION );
    end On_Enter;

    ----------------------------------------------------------------------------

    overriding
    function Tick( this : access Rising_State; time : Tick_Time ) return Boolean is
        pragma Unreferenced( time );
    begin
        -- if keen is done standing up then go back to moving
        if this.obj.stateTimer.Expired then
            this.Set_Next( MOVING );
        end if;

        -- Global_State does not need to be ticked
        return True;
    end Tick; -- Rising_State

    --========================================================================--
    -- Dying_State                                                            --
    --========================================================================--

    overriding
    procedure On_Enter( this : access Dying_State ) is
    begin
        -- record the location of dying
        this.deathX := this.obj.loc.Get_X;
        this.deathY := this.obj.loc.Get_Y;
        this.dead := False;

        -- no more clipping
        this.obj.Get_Owner.Delete_Component( MAP_CLIPPED_ID );
        this.obj.Get_Owner.Delete_Component( SOLID_ID );

        -- move to the foreground
        this.obj.vis.Set_Z( this.obj.Get_World.Get_Foreground_Z );

        -- stop camera tracking
        A_Camera_Target(this.obj.Get_Component( CAMERA_TARGET_ID )).Enable( False );

        this.obj.Set_Frame( Dying_Frames );
    end On_Enter;

    ----------------------------------------------------------------------------

    overriding
    function Tick( this : access Dying_State; time : Tick_Time ) return Boolean is
        pragma Unreferenced( time );
    begin
        -- cap the height of the death bounce
        if this.obj.loc.Get_Y <= this.obj.jumpApexMin then
            this.obj.jumping := False;
            this.obj.jumpApexMin := -1.0;
            this.obj.jumpApexMax := -1.0;
            this.obj.mov.Set_VY_Target( 0.0, 0.0 );
        end if;

        -- check for death complete
        if not this.dead and
          (Manhattan_Length( (this.obj.loc.Get_X - this.deathX, this.obj.loc.Get_Y - this.deathY) ) > DEATH_DISTANCE or
           not Contains( this.obj.Get_World.Get_Area, this.obj.loc.Get_X, this.obj.loc.Get_Y ))
        then
            this.dead := True;
            Queue_Play_Sound( "keen_dead.wav" );
            this.obj.Get_Owner.Get_System.Get_Game.Get_Session.Handle_Message( "PlayerDied" );
        end if;

        -- Global_State does not need to be ticked
        return True;
    end Tick; -- Dying_State

    --========================================================================--
    -- Climbing_State                                                         --
    --========================================================================--

    overriding
    procedure On_Enter( this : access Climbing_State ) is
        pole    : constant A_Entity := this.obj.Get_Entity( this.obj.poleId );
        poleLoc : constant A_Location := A_Location(pole.Get_Component( LOCATION_ID ));
    begin
        -- stop jumping if keen jumped onto the pole
        if this.obj.jumpApexMax >= 0.0 then
            this.obj.jumpApexMin := -1.0;
            this.obj.jumpApexMax := -1.0;
            this.obj.mov.Set_VY_Target( 0.0, IMPULSE );
        end if;

        -- stop moving horizontally and ignore gravity
        this.obj.mov.Set_VX_Target( 0.0, IMPULSE );
        this.obj.loc.Set_X( poleLoc.Get_X );
        this.obj.mov.Set_Gravity( 0.0 );
        this.obj.mov.Set_F( (0.0, 0.0) );
        this.obj.Get_Owner.Delete_Component( MAP_CLIPPED_ID );

        -- setup the camera bounds
        A_Camera_Target(this.obj.Get_Owner.Get_Component( CAMERA_TARGET_ID )).Set_Bounds( CLIMBING_BOUNDS );

        -- the .poleBottom flag starts at a short buffer space above the bottom
        -- of the pole because if its too small, the player will move right past
        -- it and fall off if he's climbing fast enough.
        this.obj.poleBottom := this.obj.loc.Get_Y >= (poleLoc.Get_Bottom - POLE_BOTTOM_BUFFER);
        this.obj.poleTop := False;

        this.Update_Frame;
    end On_Enter; -- Climbing_State

    ----------------------------------------------------------------------------

    overriding
    procedure On_Exit( this : access Climbing_State ) is
        ignored : Boolean;
    begin
        this.obj.poleId := NULL_ID;
        this.obj.mov.Set_Gravity( this.obj.mov.Get_Gravity_Initial );
        this.obj.mov.Set_F( this.obj.mov.Get_F_Initial );
        ignored := this.obj.Get_Owner.Add_Component( Entities.Factory.Global.Create_Component( "PlatformClipped" ) );
    end On_Exit;

    ----------------------------------------------------------------------------

    overriding
    function Tick( this : access Climbing_State; time : Tick_Time ) return Boolean is
        pragma Unreferenced( time );

        ------------------------------------------------------------------------

        -- starts shimmying up or down the pole, if possible.
        procedure Start_Shimmying( vDir : Cardinal_Direction ) is
        begin
            -- aim up or down
            this.obj.dir := this.obj.dir + vDir;

            -- begin shimmying up or down. he can begin moving if he isn't
            -- already moving in a direction and he's not climbing past the end
            -- of the pole.
            if not this.obj.mov.Is_Moving( Up ) and then not this.obj.mov.Is_Moving( Down ) and then
               (vDir = Down or else not this.obj.poleTop)   -- can't climb off top
            then
                -- start moving up or down
                this.obj.mov.Set_VY_Target( (if vDir = Up then this.obj.climbSpeed        else this.obj.slideSpeed       ),
                                            (if vDir = Up then this.obj.climbAcceleration else this.obj.slideAcceleration) );
            end if;
        end Start_Shimmying;

        ------------------------------------------------------------------------

        -- stops shimmying up or down the pole. or, if shimmying progress was
        -- already impeded (reached the top), this just stops aiming up or down.
        procedure Stop_Shimmying( vDir : Cardinal_Direction ) is
        begin
            if this.obj.dir and vDir then
                -- stop looking up or down
                this.obj.dir := this.obj.dir - vDir;
                if this.obj.mov.Is_Moving( vDir ) then
                    -- stop moving up or down
                    this.obj.mov.Set_VY_Target( 0.0, IMPULSE );
                end if;
            end if;
        end Stop_Shimmying;

        ------------------------------------------------------------------------

        procedure Shimmy( vDir : Cardinal_Direction; go : Boolean ) is
        begin
            if go then
                Start_Shimmying( vDir );
            else
                Stop_Shimmying( vDir );
            end if;
        end Shimmy;

        ------------------------------------------------------------------------

        pole    : A_Entity;
        poleLoc : A_Location;
    begin
        Shimmy( Up,   go => this.Directive( Directives.Galaxy.UP ) );
        Shimmy( Down, go => this.Directive( Directives.Galaxy.DOWN ) );

        if this.Directive( Directives.Galaxy.LEFT ) then
            this.obj.dir := this.obj.dir + Left;              -- face left
        elsif this.Directive( Directives.Galaxy.RIGHT ) then
            this.obj.dir := this.obj.dir + Right;             -- face right
        end if;

        if this.Directive( Directives.Galaxy.JUMP ) then
            if not this.obj.jumping then                      -- no jumping until the jump key released
                if not (this.obj.dir and Down) then           -- no jumping while climbing down
                    -- start a jump from the pole
                    this.obj.jumping := True;
                    this.obj.jumpApexMin := Float'Max( this.obj.loc.Get_Y - this.obj.minJumpHeight, 0.0 );
                    this.obj.jumpApexMax := Float'Max( this.obj.loc.Get_Y - this.obj.maxJumpHeight, 0.0 );
                    this.obj.mov.Set_VY_Target( this.obj.jumpSpeed, this.obj.jumpAcceleration );
                    Queue_Play_Sound( "keen_jump.wav" );
                    this.Set_Next( MOVING );
                    return True;
                end if;
            end if;
        else
            this.obj.jumping := False;
        end if;

        if this.Directive( Directives.Galaxy.SHOOT ) then
            this.Set_Next( CLIMBING_SHOOTING );
            return True;
        end if;

        -- - - - - - -

        if this.obj.mov.Is_Moving( Up ) then
            -- stop moving up if Keen moves past the end of the pole
            this.obj.poleBottom := False;
            pole := this.obj.Get_Entity( this.obj.poleId );
            poleLoc := A_Location(pole.Get_Component( LOCATION_ID ));

            -- see the comment in Try_To_Climb for information about
            -- the area of contact that is checked here.
            if this.obj.loc.Get_Top < poleLoc.Get_Top - 1.0 then
                this.obj.poleTop := True;
                Stop_Shimmying( Up );
            end if;

        elsif this.obj.mov.Is_Moving( Down ) then
            -- detect if Keen reaches the bottom end of the pole
            this.obj.poleTop := False;
            pole := this.obj.Get_Entity( this.obj.poleId );
            poleLoc := A_Location(pole.Get_Component( LOCATION_ID ));

            -- this test must match the test in the Try_To_Grab_Pole() procedure
            -- in the MOVING state, when climbing starts.
            -- Note: setting poleBottom doesn't really matter unless we want to
            -- prevent the player from climbing off the bottom of the pole. if
            -- that is the case, call Stop_Shimmying() when the player hits the
            -- bottom of the pole and comment out the falling off the pole logic
            -- that follows.
            this.obj.poleBottom := this.obj.loc.Get_Y >= (poleLoc.Get_Bottom - POLE_BOTTOM_BUFFER);

            if this.obj.loc.Get_Y > (poleLoc.Get_Bottom - POLE_BOTTOM_BUFFER) then
                -- fall off the bottom of the pole
                Stop_Shimmying( Down );

                this.Set_Next( MOVING );
                return True;
            end if;
        end if;

        this.Update_Frame;

        -- Global_State does not need to be ticked
        return True;
    end Tick; -- Climbing_State

    ----------------------------------------------------------------------------

    procedure Update_Frame( this : not null access Climbing_State'Class ) is
    begin
        if this.obj.mov.Get_V.y < 0.0 then
            this.obj.Set_Frame( Climbing_Pole );             -- climbing up
        elsif this.obj.mov.Get_V.y > 0.0 then
            this.obj.Set_Frame( Climbing_Pole );             -- sliding down
        else
            this.obj.Set_Frame( Hanging_From_Pole );         -- hanging still
        end if;
    end Update_Frame;

    --========================================================================--
    -- Climbing_Shooting_State                                                --
    --========================================================================--

    overriding
    procedure On_Enter( this : access Climbing_Shooting_State ) is
        ammo : Integer;
        dirX : Direction_8;
    begin
        -- stop the climbing movement but don't clear the facing direction, so
        -- that the correct directional frame can be drawn.
        this.obj.mov.Set_VY_Target( 0.0, IMPULSE );

        this.obj.Set_Frame( Hanging_From_Pole_And_Shooting );
        this.obj.stateTimer.Start( SHOOT_DURATION );

        ammo := this.obj.Get_Session_Var( "ammo" ).To_Int;
        Queue_Play_Sound( (if ammo > 0 then "keen_shoot.wav" else "click.wav") );

        if ammo > 0 then
            this.obj.Set_Session_Var( "ammo", Create( ammo - 1 ) );
            dirX := To_X( this.obj.mov.Get_V.x, this.obj.dir );
            if this.obj.dir and Up then
                this.obj.Spawn_Shot( Up );
            elsif this.obj.dir and Down then
                this.obj.Spawn_Shot( Down );
            elsif dirX = D8_Left then
                this.obj.Spawn_Shot( Left );
            elsif dirX = D8_Right then
                this.obj.Spawn_Shot( Right );
            end if;
        end if;
    end On_Enter;

    ----------------------------------------------------------------------------

    overriding
    function Tick( this : access Climbing_Shooting_State; time : Tick_Time ) return Boolean is
        pragma Unreferenced( time );
    begin
        if this.obj.stateTimer.Expired then
            this.Set_Next( CLIMBING );
        end if;

         -- Climbing_State should not be ticked
        return True;
    end Tick;

    --========================================================================--
    -- Switching_State                                                        --
    --========================================================================--

    overriding
    procedure On_Enter( this : access Switching_State ) is
    begin
        -- had to look up to switch; stop looking
        this.obj.dir := this.obj.dir - Up;
        this.obj.mov.Set_VX_Target( 0.0, IMPULSE );
        this.obj.stateTimer.Start( SWITCHING_DURATION );

        this.obj.Set_Frame( Flipping_Switch );
    end On_Enter;

    ----------------------------------------------------------------------------

    overriding
    function Tick( this : access Switching_State; time : Tick_Time ) return Boolean is
        pragma Unreferenced( time );
    begin
        -- if keen is done switching then go back to moving
        if this.obj.stateTimer.Expired then
            this.Set_Next( MOVING );
        end if;

        -- Global_State does not need to be ticked
        return True;
    end Tick;

    --========================================================================--
    -- Hanging_State                                                          --
    --========================================================================--

    overriding
    procedure On_Enter( this : access Hanging_State ) is
    begin
        -- stop moving and ignore gravity
        this.obj.mov.Set_VX_Target( 0.0, IMPULSE );
        this.obj.mov.Set_VY_Target( 0.0, IMPULSE );
        this.obj.mov.Set_Gravity( 0.0 );
        this.obj.mov.Set_F( (0.0, 0.0) );

        -- adjust keen so that his top edge is 4 pixels above the ledge
        this.obj.loc.Set_Y( Float'Ceiling( (this.obj.loc.Get_Top - GRAB_HEIGHT) / Float(this.obj.Get_World.Tile_Width) )
                             * Float(this.obj.Get_World.Tile_Width)
                             + Float(this.obj.loc.Get_Height) / 2.0 );

        -- don't immediately transition to clambering or dropping yet
        this.obj.stateTimer.Start( MIN_HANG_DELAY );

        this.obj.Set_Frame( Hanging_From_Ledge );
    end On_Enter;

    ----------------------------------------------------------------------------

    overriding
    function Tick( this : access Hanging_State; time : Tick_Time ) return Boolean is
        pragma Unreferenced( time );
        world     : constant A_World := this.obj.Get_World;
        tileWidth : constant Float := Float(world.Tile_Width);

        ------------------------------------------------------------------------

        function Can_Clamber_Up return Boolean is
            newX : constant Float := this.obj.loc.Get_X + tileWidth * Float(this.obj.dir.x);
        begin
            -- check if the tile above the ledge, and the one above that, are open
            return Passive = world.Get_Clip_Type( newX, this.obj.loc.Get_Top - GRAB_HEIGHT ) and
                   Passive = world.Get_Clip_Type( newX, this.obj.loc.Get_Top - GRAB_HEIGHT - tileWidth );
        end Can_Clamber_Up;

        ------------------------------------------------------------------------

        function Is_Ledge_Still_There return Boolean is
            newX : constant Float := this.obj.loc.Get_X + tileWidth * Float(this.obj.dir.x);
        begin
            -- check if the ledge tile is still solid
            return Wall = world.Get_Clip_Type( newX, this.obj.loc.Get_Top );
        end Is_Ledge_Still_There;

        ------------------------------------------------------------------------

        ignored : Boolean;
    begin
        if not this.obj.stateTimer.Expired then
            return True;
        end if;

        -- to drop down, press Down, or press a direction away from the ledge,
        -- or wait for the ledge to disappear
        if this.Directive( Directives.Galaxy.DOWN ) or else
           (this.Directive( Directives.Galaxy.LEFT ) and this.obj.dir /= Dir_Left) or else
           (this.Directive( Directives.Galaxy.RIGHT ) and this.obj.dir /= Dir_Right) or else
           not Is_Ledge_Still_There
        then
            this.obj.mov.Set_Gravity( this.obj.mov.Get_Gravity_Initial );
            this.obj.mov.Set_F( this.obj.mov.Get_F_Initial );

            -- move down a bit so the ledge won't be detected on the next frame
            this.obj.loc.Add_Y( GRAB_HEIGHT );

            this.Set_Next( MOVING );

        -- to clamber up, press Up or the direction toward the ledge
        elsif this.Directive( Directives.Galaxy.UP ) or else
              (this.Directive( Directives.Galaxy.LEFT ) and this.obj.dir = Dir_Left) or else
              (this.Directive( Directives.Galaxy.RIGHT ) and this.obj.dir = Dir_Right)
        then
            if Can_Clamber_Up then
                this.Set_Next( CLAMBERING );
            end if;
        end if;

        return True;    -- Global_State does not need to be ticked
    end Tick;

    --========================================================================--
    -- Clambering_State                                                       --
    --========================================================================--

    overriding
    procedure On_Enter( this : access Clambering_State ) is
    begin
        this.obj.stateTimer.Start( CLAMBERING_DURATION );

        -- move to the top of the ledge. when he's hanging, the top of keen is
        -- level with the ledge. check Hanging_State to verify.
        this.obj.loc.Set_Y( this.obj.loc.Get_Y - Float(this.obj.loc.Get_Height) );
        this.obj.loc.Add_X( Float(this.obj.Get_World.Tile_Width * this.obj.dir.x) );

        this.obj.Set_Frame( Climbing_Ledge );
    end On_Enter;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Exit( this : access Clambering_State ) is
        ignored : Boolean;
    begin
        this.obj.mov.Set_Gravity( this.obj.mov.Get_Gravity_Initial );
        this.obj.mov.Set_F( this.obj.mov.Get_F_Initial );
    end On_Exit;

    ----------------------------------------------------------------------------

    overriding
    function Tick( this : access Clambering_State; time : Tick_Time ) return Boolean is
        pragma Unreferenced( time );
    begin
        -- if keen is done climbing then stand on the ledge
        if this.obj.stateTimer.Expired then
            this.Set_Next( MOVING );
        end if;

        return True;    -- Global_State does not need to be ticked
    end Tick;

    --==========================================================================

    procedure Define_Component( factory : not null A_Entity_Factory ) is
    begin
        factory.Define_Component( "KeenController",
                                  Create_Keen'Access,
                                  required =>
                                      (Create( "Location" ),
                                       Create( "Movement" ),
                                       Create( "Visible" )) );
    end Define_Component;

end Entities.Statefuls.Keens;

