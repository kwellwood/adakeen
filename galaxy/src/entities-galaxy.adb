--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Entities.Animations;
with Entities.Camera_Targets;
with Entities.Collidables;
with Entities.Emitters;
with Entities.Entity_Attributes;
with Entities.Lights;
with Entities.Locations;
with Entities.Map_Clipping.Platform;
with Entities.Map_Clipping.Precise;
with Entities.Movements;
with Entities.Scripts;
with Entities.Solids;
with Entities.Statefuls.Keens;
with Entities.Statefuls.Map_Keens;
with Entities.Tile_Sensors;
with Entities.Visibles;

package body Entities.Galaxy is

    procedure Define_Components( factory : not null A_Entity_Factory ) is
    begin
        -- - - - - - - - - - - - Standard components - - - - - - - - - - - --

        Animations           .Define_Component( factory );
        Camera_Targets       .Define_Component( factory );
        Collidables          .Define_Component( factory );
        Emitters             .Define_Component( factory );
        Entity_Attributes    .Define_Component( factory );
        Lights               .Define_Component( factory );
        Locations            .Define_Component( factory );
        Scripts              .Define_Component( factory );
        Movements            .Define_Component( factory );
        Map_Clipping.Platform.Define_Component( factory );
        Map_Clipping.Precise .Define_Component( factory );
        Solids               .Define_Component( factory );
        Tile_Sensors         .Define_Component( factory );
        Visibles             .Define_Component( factory );

        -- - - - - - - - - - - - - Keen 4 components - - - - - - - - - - - - --

        Statefuls.Keens      .Define_Component( factory );
        Statefuls.Map_Keens  .Define_Component( factory );

    end Define_Components;

end Entities.Galaxy;
