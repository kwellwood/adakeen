--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Assets.Libraries;                  use Assets.Libraries;
with Directions;                        use Directions;
with Entities.Components;               use Entities.Components;
with Entities.Factory;                  use Entities.Factory;
with Entities.Locations;                use Entities.Locations;
with Entities.Movements;                use Entities.Movements;
with Entities.Visibles;                 use Entities.Visibles;

package Entities.Statefuls.Map_Keens is

    PLAYER_ID : Family_Id := To_Family_Id( "Player" );

    -- Property    Type    Description
    -- --------    ----    -----------
    -- none

    procedure Define_Component( factory : not null A_Entity_Factory );

    ----------------------------------------------------------------------------

    type Map_Keen is new Stateful with private;
    type A_Map_Keen is access all Map_Keen'Class;

    overriding
    function Get_Dependency_Order( this : access Map_Keen ) return Natural is (DEPS_MOVEMENT + 10);

    overriding
    function Get_Family( this : access Map_Keen ) return Family_Id is (PLAYER_ID);

    overriding
    function Get_Tick_Order( this : access Map_Keen ) return Natural is (TCK_ANIMATION - 10);

private

    -- State Hierarachy:
    --
    --   Global
    --     Walking
    --     Swimming
    --     Bored

    WALKING  : constant State_Id := 10;    -- walking or standing
    SWIMMING : constant State_Id := 20;    -- swimming in water
    BORED    : constant State_Id := 30;    -- performing a bored animation

    ----------------------------------------------------------------------------

    type Map_Keen is new Stateful with
        record
            dir        : Direction_Type := Dir_Left;      -- facing direction

            -- keen's current state
            stateTimer : Timer_Type;                      -- generic state timer

            -- ** these fields do not need to be streamed **

            idleTimer  : Timer_Type;              -- timer for becoming bored
            stepTimer  : Timer_Type;              -- timer for walking/swimming sounds

            actionFrame : Natural := 0;           -- frame of current action (not necessarily the visible frame)

            walkSpeed        : Float := 92.0;     -- measured at 80 px/s with KEEN4E
            walkAcceleration : Float := 700.0;

            loc : A_Location := null;   -- ref to the LOCATION_ID component
            mov : A_Movement := null;   -- ref to the MOVEMENT_ID component
            vis : A_Visible  := null;   -- ref to the VISIBLE_ID component
        end record;
    for Map_Keen'External_Tag use "Component.Map_Keen";

    procedure Construct( this : access Map_Keen );

    -- Sends MSG_Activate message to all entities Keen is touching.
    procedure Activate_Touching( this : not null access Map_Keen'Class );

    -- Returns the library the entity is using to display itself.
    function Get_Library( this : not null access Map_Keen'Class ) return Library_Ptr
        is (A_Visible(this.Get_Owner.Get_Component( VISIBLE_ID )).Get_Library);

    function Object_Input( stream : access Root_Stream_Type'Class ) return Map_Keen;
    for Map_Keen'Input use Object_Input;

    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Map_Keen );
    for Map_Keen'Read use Object_Read;

    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Map_Keen );
    for Map_Keen'Write use Object_Write;

    -- Resets the idle counter and updates the current frame.
    procedure On_Added( this : access Map_Keen );

    -- Starts or stops moving, based on 'moving'. When 'moving is True, Keen
    -- will start or continue moving in the direction of 'dir'. If he is already
    -- moving in the opposite direction then he won't change course.
    procedure Set_Moving( this   : not null access Map_Keen'Class;
                          dir    : Cardinal_Direction;
                          moving : Boolean );

    -- Begins Keen moving in the direction 'dir'. If he is already moving in
    -- the exact opposite direction, nothing will change.
    procedure Start_Moving( this : not null access Map_Keen'Class;
                            dir  : Cardinal_Direction );

    -- If Keen is moving in direction 'dir', he will stop moving in that
    -- direction.
    procedure Stop_Moving( this : not null access Map_Keen'Class;
                           dir  : Cardinal_Direction );

end Entities.Statefuls.Map_Keens;
