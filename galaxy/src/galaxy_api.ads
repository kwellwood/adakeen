--
-- Copyright (c) 2014-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Script_VMs;                        use Script_VMs;

-- This package contains the built-in Scribble functions for Keen Galaxy.
package Galaxy_API is

    -- Defines the Keen Galaxy scripting functions with script VM 'vm'.
    --
    -- This method is currently a placeholder. Keen Galaxy does not define any
    -- specific Scribble functions.
    procedure Initialize( vm : not null A_Script_VM ) is null;

end Galaxy_API;
