--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Directives.Galaxy;
with Entities.Collidables;              use Entities.Collidables;
with Entities.Galaxy;                   use Entities.Galaxy;
with Entities.Systems;                  use Entities.Systems;
with Events.Audio;                      use Events.Audio;
with Preferences;
with Tiles;                             use Tiles;
with Values.Lists;                      use Values.Lists;
with Values.Strings;                    use Values.Strings;
with Vector_Math;                       use Vector_Math;
with Worlds;                            use Worlds;

package body Entities.Statefuls.Map_Keens is

    IDLE_DELAY    : constant Time_Span := Milliseconds( 4500 );
    IDLE_DURATION : constant Time_Span := Milliseconds( 1800 );

    STEP_SOUND_DELAY : constant Time_Span := Milliseconds( 226 );
    SWIM_SOUND_DELAY : constant Time_Span := Milliseconds( 342 );

    ----------------------------------------------------------------------------

    type Frame_Info is
        record
            filename : String_Access := null;  -- todo: store a preference key name instead that defaults to the filename
            id       : Integer := -1;
        end record;

    type Directional_Frame is array (Direction_8) of Frame_Info;
    type A_Directional_Frame is access all Directional_Frame;

    type Animation( directional : Boolean ) is
        record
            case directional is
                when True =>
                    dir   : A_Directional_Frame := null;
                when False =>
                    frame : Frame_Info;
            end case;
        end record;
    type A_Animation is access all Animation;

    ----------------------------------------------------------------------------

    type Visible_Action is
    (
        Standing_Frames,
        Walking_Frames,
        Swimming_Frames,
        Waving_Frames
    );

    type Action_Frames is array (Visible_Action) of A_Animation;

    frames : constant Action_Frames := Action_Frames'(
        Standing_Frames => new Animation'(
            directional => True,
            dir         => new Directional_Frame'(
                D8_Left       => (filename => new String'("map_stand_left" ), others => <>),
                D8_Right      => (filename => new String'("map_stand_right" ), others => <>),
                D8_Up         => (filename => new String'("map_stand_up" ), others => <>),
                D8_Down       => (filename => new String'("map_stand_down" ), others => <>),
                D8_Up_Left    => (filename => new String'("map_stand_up_left" ), others => <>),
                D8_Up_Right   => (filename => new String'("map_stand_up_right" ), others => <>),
                D8_Down_Left  => (filename => new String'("map_stand_down_left" ), others => <>),
                D8_Down_Right => (filename => new String'("map_stand_down_right" ), others => <>))),
        Walking_Frames  => new Animation'(
            directional => True,
            dir         => new Directional_Frame'(
                D8_Left       => (filename => new String'("map_walk_left1" ), others => <>),
                D8_Right      => (filename => new String'("map_walk_right1" ), others => <>),
                D8_Up         => (filename => new String'("map_walk_up1" ), others => <>),
                D8_Down       => (filename => new String'("map_walk_down1" ), others => <>),
                D8_Up_Left    => (filename => new String'("map_walk_up_left1" ), others => <>),
                D8_Up_Right   => (filename => new String'("map_walk_up_right1" ), others => <>),
                D8_Down_Left  => (filename => new String'("map_walk_down_left1" ), others => <>),
                D8_Down_Right => (filename => new String'("map_walk_down_right1" ), others => <>))),
        Swimming_Frames => new Animation'(
            directional => True,
            dir         => new Directional_Frame'(
                D8_Left       => (filename => new String'("map_swim_left1" ), others => <>),
                D8_Right      => (filename => new String'("map_swim_right1" ), others => <>),
                D8_Up         => (filename => new String'("map_swim_up1" ), others => <>),
                D8_Down       => (filename => new String'("map_swim_down1" ), others => <>),
                D8_Up_Left    => (filename => new String'("map_swim_up_left1" ), others => <>),
                D8_Up_Right   => (filename => new String'("map_swim_up_right1" ), others => <>),
                D8_Down_Left  => (filename => new String'("map_swim_down_left1" ), others => <>),
                D8_Down_Right => (filename => new String'("map_swim_down_right1" ), others => <>))),
        Waving_Frames   => new Animation'(
            directional => False,
            frame       => (filename => new String'("map_wave1" ), others => <>))
    );

    ----------------------------------------------------------------------------

    type Root_State is new State with null record;

    function obj( this : Root_State'Class ) return access Map_Keen'Class is (A_Map_Keen(this.owner)) with Inline_Always;

    function Directive( this : Root_State'Class; d : Directive_Bits ) return Boolean is (this.obj.Directive( d )) with Inline_Always;

    function Tick( this : access Root_State; time : Tick_Time ) return Boolean;

    ----------------------------------------------------------------------------

    type Walking_State is new Root_State with null record;

    procedure On_Enter( this : access Walking_State );

    function Tick( this : access Walking_State; time : Tick_Time ) return Boolean;

    ----------------------------------------------------------------------------

    type Swimming_State is new Root_State with null record;

    procedure On_Enter( this : access Swimming_State );

    procedure On_Exit( this : access Swimming_State );

    function Tick( this : access Swimming_State; time : Tick_Time ) return Boolean;

    ----------------------------------------------------------------------------

    type Bored_State is new Root_State with null record;

    procedure On_Enter( this : access Bored_State );

    function Tick( this : access Bored_State; time : Tick_Time ) return Boolean;

    ----------------------------------------------------------------------------

    -- Returns True if the center of the entity is within a water tile.
    function In_Water( this : not null access Map_Keen'Class ) return Boolean;

    -- Sets Keen's frame to match action 'action' and facing in the appropriate
    -- direction, when applicable.
    procedure Set_Frame( this : not null access Map_Keen'Class; action : Visible_Action );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Map_Keen( properties : Map_Value ) return A_Component is
        pragma Unreferenced( properties );
        this : constant A_Map_Keen := new Map_Keen;
    begin
        this.Construct;
        return A_Component(this);
    end Create_Map_Keen;

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access Map_Keen ) is

        procedure Read_Pref( name : String; field : in out Float ) is
        begin
            field := Preferences.Get_Pref( "development", "components.KeenBehavior." & name, field );
        end Read_Pref;

        globalState : A_State;
    begin
        -- construct the state tree
        globalState := new Root_State;
        globalState.Add_State( WALKING,  new Walking_State );
        globalState.Add_State( SWIMMING, new Swimming_State );
        globalState.Add_State( BORED,    new Bored_State );

        Stateful(this.all).Construct( globalState, WALKING );

        Read_Pref( "walkSpeed",        this.walkSpeed        );
        Read_Pref( "walkAcceleration", this.walkAcceleration );
    end Construct;

    ----------------------------------------------------------------------------

    procedure Activate_Touching( this : not null access Map_Keen'Class ) is

        procedure Activate( e : A_Entity ) is
        begin
            e.Dispatch_Message( MSG_Activate,
                                Create_Map( (1=>Pair("eid", To_Id_Value( this.Get_Owner.Get_Id ))) ).Map );
        end Activate;

    begin
        A_Collidable(this.Get_Component( COLLIDABLE_ID )).Iterate_Touching( Activate'Access );
    end Activate_Touching;

    ----------------------------------------------------------------------------

    function In_Water( this : not null access Map_Keen'Class ) return Boolean is
        tile : A_Tile;
    begin
        -- check the "water" boolean attribute of the tile in layer 1
        tile := this.Get_World.Get_Tile( 1, this.loc.Get_X, this.loc.Get_Y );
        return tile /= null and then tile.Get_Attribute( "water" ).To_Boolean;
    end In_Water;

    ----------------------------------------------------------------------------

    procedure Set_Moving( this   : not null access Map_Keen'Class;
                          dir    : Cardinal_Direction;
                          moving : Boolean ) is
    begin
        if moving then
            this.Start_Moving( dir );
        else
            this.Stop_Moving( dir );
        end if;
    end Set_Moving;

    ----------------------------------------------------------------------------

    overriding
    function Object_Input( stream : access Root_Stream_Type'Class ) return Map_Keen is
        this : aliased Map_Keen;
    begin
        this.Construct;
        Map_Keen'Read( stream, this );
        return this;
    end Object_Input;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Map_Keen ) is
    begin
        Stateful'Read( stream, Stateful(obj) );

        obj.dir := Direction_Type'Input( stream );
        obj.stateTimer.endTime := Time_Span'Input( stream );
    end Object_Read;

    ----------------------------------------------------------------------------

    overriding
    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Map_Keen ) is
    begin
        Stateful'Write( stream, Stateful(obj) );

        Direction_Type'Output( stream, obj.dir );
        Time_Span'Output( stream, obj.stateTimer.endTime );
    end Object_Write;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Added( this : access Map_Keen ) is
    begin
        this.stateTimer.Init( this );
        this.idleTimer.Init( this );
        this.stepTimer.Init( this );
        this.idleTimer.Start( IDLE_DELAY );

        this.loc := A_Location(this.Get_Component( LOCATION_ID ));
        this.mov := A_Movement(this.Get_Component( MOVEMENT_ID ));
        this.vis := A_Visible(this.Get_Component( VISIBLE_ID ));
    end On_Added;

    ----------------------------------------------------------------------------

    procedure Set_Frame( this : not null access Map_Keen'Class; action : Visible_Action ) is

        ------------------------------------------------------------------------

        procedure Resolve_Id( id : out Natural; frame : in out Frame_Info ) is
        begin
            if frame.id < 0 then
                frame.id := this.Get_Library.Get.Get_Id( frame.filename.all );
            end if;
            id := frame.id;
        end Resolve_Id;

        ------------------------------------------------------------------------

        id : Natural := 0;
    begin
        if frames(action).directional then
            Resolve_Id( id, frames(action).dir(To_D8( this.dir )) );
        else
            Resolve_Id( id, frames(action).frame );
        end if;

        if this.actionFrame /= id then
            this.actionFrame := id;
            this.vis.Set_Frame( id );
        end if;
    end Set_Frame;

    ----------------------------------------------------------------------------

    procedure Start_Moving( this : not null access Map_Keen'Class;
                            dir  : Cardinal_Direction ) is
    begin
        this.idleTimer.Start( IDLE_DELAY );

        -- if Keen isn't already moving in this direction and he isn't
        -- moving in the opposite direction, then start moving in this
        -- direction.
        if not this.mov.Is_Moving( dir ) and not this.mov.Is_Moving( Opposite( dir ) ) then
            -- update the facing and moving directions
            if not this.mov.Is_Moving then
                this.dir := To_Direction( dir );
            else
                this.dir := this.dir + dir;
            end if;

            -- accelerate to walking speed in this direction
            case dir is
                when Up   | Down  => this.mov.Set_VY_Target( To_Vector( dir ).y * this.walkSpeed, this.walkAcceleration );
                when Left | Right => this.mov.Set_VX_Target( To_Vector( dir ).x * this.walkSpeed, this.walkAcceleration );
            end case;
        end if;
    end Start_Moving;

    ----------------------------------------------------------------------------

    procedure Stop_Moving( this : not null access Map_Keen'Class;
                           dir  : Cardinal_Direction ) is
    begin
        if this.mov.Is_Moving( dir ) then
            this.dir := this.dir - dir;
            case dir is
                when Up   | Down  => this.mov.Set_VY_Target( 0.0, 0.0 );
                when Left | Right => this.mov.Set_VX_Target( 0.0, 0.0 );
            end case;
        end if;
    end Stop_Moving;

    --========================================================================--
    -- Root_State                                                             --
    --========================================================================--

    overriding
    function Tick( this : access Root_State; time : Tick_Time ) return Boolean is
        pragma Unreferenced( time );
    begin
        -- these are given by Tinker to change Keen's orientation
        -- handle these directives in all states
        if this.Directive( Directives.Galaxy.FACE_LEFT ) then
            this.obj.dir := Dir_Left;
        elsif this.Directive( Directives.Galaxy.FACE_RIGHT ) then
            this.obj.dir := Dir_Right;
        elsif this.Directive( Directives.Galaxy.FACE_UP ) then
            this.obj.dir := Dir_Up;
        elsif this.Directive( Directives.Galaxy.FACE_DOWN ) then
            this.obj.dir := Dir_Down;
        end if;

        return True;
    end Tick;

    --========================================================================--
    -- Walking State                                                          --
    --========================================================================--

    overriding
    procedure On_Enter( this : access Walking_State ) is
    begin
        this.obj.idleTimer.Start( IDLE_DELAY );
        this.obj.Set_Frame( (if this.obj.mov.Is_Moving then Walking_Frames else Standing_Frames) );
    end On_Enter;

    ----------------------------------------------------------------------------

    overriding
    function Tick( this : access Walking_State; time : Tick_Time ) return Boolean is
        pragma Unreferenced( time );
    begin
        -- handle movement
        this.obj.Set_Moving( Left,  this.Directive( Directives.Galaxy.LEFT  ) );
        this.obj.Set_Moving( Right, this.Directive( Directives.Galaxy.RIGHT ) );
        this.obj.Set_Moving( Up,    this.Directive( Directives.Galaxy.UP    ) );
        this.obj.Set_Moving( Down,  this.Directive( Directives.Galaxy.DOWN  ) );

        -- check if keen is activating things
        if this.Directive( Directives.Galaxy.JUMP ) or
           this.Directive( Directives.Galaxy.POGO )
        then
            this.obj.Activate_Touching;
        end if;

        -- check if Keen is bored yet
        if this.obj.idleTimer.Expired then
            this.obj.Set_Next( BORED );
        end if;

        -- check if keep entered the water
        if this.obj.In_Water then
            this.Set_Next( SWIMMING );
        end if;

        -- update frame and footsteps sounds
        if this.obj.mov.Is_Moving then
            this.obj.Set_Frame( Walking_Frames );
            if this.obj.stepTimer.Expired then
                Queue_Play_Sound( "map_footstep.wav", STEP_SOUND_DELAY );
                this.obj.stepTimer.Start( STEP_SOUND_DELAY );
            end if;
        else
            this.obj.Set_Frame( Standing_Frames );
        end if;

        return False;
    end Tick;

    --========================================================================--
    -- Swimming State                                                         --
    --========================================================================--

    overriding
    procedure On_Enter( this : access Swimming_State ) is
    begin
        -- switch the swimming frame on entry for more responsiveness
        this.obj.Set_Frame( Swimming_Frames );
    end On_Enter;

    ----------------------------------------------------------------------------

    overriding
    function Tick( this : access Swimming_State; time : Tick_Time ) return Boolean is
        pragma Unreferenced( time );
    begin
        -- handle movement
        this.obj.Set_Moving( Left,  this.Directive( Directives.Galaxy.LEFT  ) );
        this.obj.Set_Moving( Right, this.Directive( Directives.Galaxy.RIGHT ) );
        this.obj.Set_Moving( Up,    this.Directive( Directives.Galaxy.UP    ) );
        this.obj.Set_Moving( Down,  this.Directive( Directives.Galaxy.DOWN  ) );

        -- exited the water?
        if not this.obj.In_Water then
            this.Set_Next( WALKING );
        end if;

        -- update frame and swimming sounds
        Queue_Play_Sound( "map_swim.wav", SWIM_SOUND_DELAY );
        this.obj.Set_Frame( Swimming_Frames );

        return False;
    end Tick;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Exit( this : access Swimming_State ) is
        pragma Unreferenced( this );
    begin
        Queue_Play_Sound( "map_beach.wav" );
    end On_Exit;

    --========================================================================--
    -- Bored State                                                            --
    --========================================================================--

    overriding
    procedure On_Enter( this : access Bored_State ) is
    begin
        this.obj.stateTimer.Start( IDLE_DURATION );
        this.obj.Set_Frame( Waving_Frames );
    end On_Enter;

    ----------------------------------------------------------------------------

    overriding
    function Tick( this : access Bored_State; time : Tick_Time ) return Boolean is
        pragma Unreferenced( time );
    begin
        -- try to enter a level
        if this.Directive( Directives.Galaxy.JUMP ) or
           this.Directive( Directives.Galaxy.POGO )
        then
            this.obj.Activate_Touching;
            this.obj.Set_Next( WALKING );
        end if;

        -- if he gets a directive then go back to the moving state
        if this.Directive( Directives.Galaxy.LEFT )  or
           this.Directive( Directives.Galaxy.RIGHT ) or
           this.Directive( Directives.Galaxy.UP )    or
           this.Directive( Directives.Galaxy.DOWN )
        then
            this.obj.Set_Next( WALKING );
        end if;

        -- if keen is done waving
        if this.obj.stateTimer.Expired then
            this.obj.Set_Next( WALKING );
        end if;

        return False;
    end Tick;

    --==========================================================================

    procedure Define_Component( factory : not null A_Entity_Factory ) is
    begin
        factory.Define_Component( "MapKeenController",
                                  Create_Map_Keen'Access,
                                  required =>
                                      (Create( "Movement" ),
                                       Create( "Visible" ),
                                       Create( "Collidable" )) );
    end Define_Component;

end Entities.Statefuls.Map_Keens;
