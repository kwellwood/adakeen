--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Directives.Galaxy is

    UP         : constant Directive_Bits := To_Directive( 0 );
    DOWN       : constant Directive_Bits := To_Directive( 1 );
    LEFT       : constant Directive_Bits := To_Directive( 2 );
    RIGHT      : constant Directive_Bits := To_Directive( 3 );

    FACE_UP    : constant Directive_Bits := To_Directive( 4 );
    FACE_DOWN  : constant Directive_Bits := To_Directive( 5 );
    FACE_LEFT  : constant Directive_Bits := To_Directive( 6 );
    FACE_RIGHT : constant Directive_Bits := To_Directive( 7 );

    JUMP       : constant Directive_Bits := To_Directive( 8 );
    SHOOT      : constant Directive_Bits := To_Directive( 9 );
    POGO       : constant Directive_Bits := To_Directive( 10 );

end Directives.Galaxy;
