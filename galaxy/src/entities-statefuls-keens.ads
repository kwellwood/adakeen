--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Assets.Libraries;                  use Assets.Libraries;
with Directions;                        use Directions;
with Entities.Components;               use Entities.Components;
with Entities.Factory;                  use Entities.Factory;
with Entities.Locations;                use Entities.Locations;
with Entities.Movements;                use Entities.Movements;
with Entities.Visibles;                 use Entities.Visibles;

package Entities.Statefuls.Keens is

    -- When this message is received, Keen will die
    MSG_KillPlayer : constant Hashed_String := To_Hashed_String( "KillPlayer" );

    -- When this message is received, Keen will show his switching animation
    MSG_Switching  : constant Hashed_String := To_Hashed_String( "Switching" );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    PLAYER_ID : Family_Id := To_Family_Id( "Player" );

    -- Property    Type    Description
    -- --------    ----    -----------
    -- none

    procedure Define_Component( factory : not null A_Entity_Factory );

    ----------------------------------------------------------------------------

    type Keen is new Stateful with private;
    type A_Keen is access all Keen'Class;

    overriding
    function Get_Dependency_Order( this : access Keen ) return Natural is (DEPS_MAPCLIP + 10);

    overriding
    function Get_Family( this : access Keen ) return Family_Id is (PLAYER_ID);

    overriding
    function Get_Tick_Order( this : access Keen ) return Natural is (TCK_ANIMATION - 10);

private

    -- State Hierarachy:      Class:                    Extends:
    --
    --  Global                Global_State              Root_State
    --     Moving             Moving_State              Root_State
    --        Shooting        Moving_Shooting_State     Root_State
    --        Pogoing         Pogoing_State             Root_State
    --        Idle            Idle_State                Root_State
    --        Rising          Rising_State              Root_State
    --     Looking            Looking_State             Root_State
    --        Shooting        Looking_Shooting_State    Root_State
    --     Looking_Back       Looking_Back_State        Root_State
    --     Teleporting        Teleporting_State         Root_State
    --     Dying              Dying_State               Root_State
    --     Climbing           Climbing_State            Root_State
    --        Shooting        Climbing_Shooting_State   Root_State
    --     Switching          Switching_State           Root_State
    --     Hanging            Hanging_State             Root_State
    --     Clambering         Clambering_State          Root_State

    MOVING            : constant State_Id :=  10;    -- standing or moving (on ground and in the air)
    MOVING_SHOOTING   : constant State_Id :=  11;    -- shooting while standing or jumping
    POGOING           : constant State_Id :=  12;    -- moving while on a pogo stick
    IDLE              : constant State_Id :=  13;    -- performing an idle animation
    RISING            : constant State_Id :=  14;    -- rising from sitting and reading
    LOOKING           : constant State_Id :=  20;    -- looking up or down while standing
    LOOKING_SHOOTING  : constant State_Id :=  21;    -- shooting while looking up
    LOOKING_BACK      : constant State_Id :=  30;    -- transition from looking back to moving
    TELEPORTING       : constant State_Id :=  40;    -- walking into a teleport door
    DYING             : constant State_Id :=  70;    -- bouncing off the screen in death
    CLIMBING          : constant State_Id :=  80;    -- climbing on a pole
    CLIMBING_SHOOTING : constant State_Id :=  81;    -- shooting from a pole
    SWITCHING         : constant State_Id :=  90;    -- flipping a switch
    HANGING           : constant State_Id := 100;    -- hanging from a ledge
    CLAMBERING        : constant State_Id := 110;    -- clambering over a ledge

    ----------------------------------------------------------------------------

    -- Length of time standing idle before the first idle animation
    FIRST_IDLE_DELAY : constant Time_Span := Milliseconds( 4000 );

    ----------------------------------------------------------------------------

    type Keen is new Stateful with
        record
            loc : A_Location;       -- ref to the LOCATION_ID component
            mov : A_Movement;       -- ref to the MOVEMENT_ID component
            vis : A_Visible;        -- ref to the VISIBLE_ID component

            -- *** these fields are streamed ***

            -- the direction keen is facing or acting
            dir : Direction_Type := Dir_Left;

            -- When a jump begins with the jump button, .jumping is set to True
            -- and stays True until either the button is released or the player
            -- reaches the jump apex, whichever occurs LAST. jumpApexMin is the
            -- Y world coordinate that must be reached before the jump can stop
            -- (even if the jump button is released), and jumpApexMax is the Y
            -- world coordinate where jumping will stop even if the jump button
            -- is still held. The apex can also be reached by bonking into a
            -- ceiling. When the jump apex is reached, jumpApexMin and
            -- jumpApexMax will be reset to -1.
            jumping     : Boolean := False;
            jumpApexMin : Float := -1.0;
            jumpApexMax : Float := -1.0;

            -- keen's current state
            stateTimer : Timer_Type;                       -- generic state timer

            -- for the idle state
            idleTimer  : Timer_Type;                       -- time to next idle animation
            idleNumber : Positive := 1;                    -- number of the next idle animation

            -- for the teleporting state
            portalId : Entity_Id := NULL_ID;               -- teleporter entrance/exit entity

            -- for the climbing state
            poleId     : Entity_Id := NULL_ID;      -- id of pole being climbed (action = Act_Climbing)
            poleTop    : Boolean := False;          -- reached the top of the pole
            poleBottom : Boolean := False;          -- reached the bottom of the pole (works but not used)

            -- ** these fields do not need to be streamed **

            -- keen's current state
            stateFrame : Natural := 0;                     -- frame of current state (not necessarily the visible frame)

            -- for the moving state
            wasSupported : Boolean := False;               -- Is_Supported from previous frame

            -- for the idle state
            mooned : Boolean := False;                     -- did the mooning easter egg?

            -- for the looking and moving states
            bottomlessCounter : Integer := 0;              -- ticks remaining while Keen's bottom is not solid

            -- read from components.KeenBehavior.* in [development] preferences
            walkSpeed              : Float :=   92.0;
            walkAcceleration       : Float :=  700.0;
            walkFriction           : Float :=  800.0;
            jumpSpeed              : Float := -192.0;
            jumpAcceleration       : Float := 2400.0;
            fallSpeed              : Float :=   54.0;
            midairAcceleration     : Float :=  500.0;
            midairPogoAcceleration : Float :=  350.0;
            minJumpHeight          : Float :=   10.0;
            maxJumpHeight          : Float :=   40.0;
            minPogoHeight          : Float :=   16.0;
            maxPogoHeight          : Float :=   80.0;
            maxBounceHeight        : Float :=   26.0;
            pogoFriction           : Float :=    0.0;
            lookSpeed              : Float :=   90.0;
            climbSpeed             : Float :=  -48.0;
            climbAcceleration      : Float := 2400.0;
            slideSpeed             : Float :=   64.0;
            slideAcceleration      : Float := 2400.0;
        end record;
    for Keen'External_Tag use "Component.Keen_Controller";

    procedure Construct( this : access Keen );

    -- Returns the library the entity is using to display itself.
    function Get_Library( this : not null access Keen'Class ) return Library_Ptr
        is (A_Visible(this.Get_Owner.Get_Component( VISIBLE_ID )).Get_Library);

    -- Returns True if Keen is blocked from moving in the 'dir' direction,
    -- either by a solid tile or a solid entity.
    function Is_Blocked( this : not null access Keen'Class; dir : Cardinal_Direction ) return Boolean;

    -- Returns True if Keen is supported from below- either by a solid tile or
    -- a solid entity.
    function Is_Supported( this : not null access Keen'Class ) return Boolean is (this.Is_Blocked( Down ));

    function Object_Input( stream : access Root_Stream_Type'Class ) return Keen;
    for Keen'Input use Object_Input;

    procedure Object_Read( stream : access Root_Stream_Type'Class; obj : out Keen );
    for Keen'Read use Object_Read;

    procedure Object_Write( stream : access Root_Stream_Type'Class; obj : Keen );
    for Keen'Write use Object_Write;

    procedure On_Added( this : access Keen );

end Entities.Statefuls.Keens;
