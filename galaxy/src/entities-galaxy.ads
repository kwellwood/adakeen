--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Entities.Factory;                  use Entities.Factory;

package Entities.Galaxy is

    MSG_Activate   : constant Hashed_String := To_Hashed_String( "Activate" );

    -- Relative Tick Order of Components
    --
    -- 0   Activator, Climbable, Item, ItemCollector, Lethal, ShotBehavior,
    --     ShotBlocker, SpawnOnDestruct, Trigger
    --
    -- 110 ArachnutAI, BounderAI, CloudAI, FlagBehavior, InchwormAI,
    --     KeenBehavior, LickAI, LittleKeenBehavior, SlugAI, SnakeAI, MimrockAI,
    --     MushroomAI

    -- Relative Dependency Order of Components
    --
    -- 0  Activators
    -- 0  Climbable
    -- 0  Item
    -- 0  ItemCollector
    -- 0  Lethal
    -- 0  ShotBehavior
    -- 0  ShotBlocker
    -- 0  SpawnOnDestruct
    -- 10 Trigger (Location)
    -- 20 FlagBehavior (Movement)
    -- 30 ArachnutAI (Visible, Location, Movement)
    -- 30 BounderAI (Visible, Location, Movement)
    -- 30 CloudAI (Visible, Location, Movement)
    -- 30 InchwormAI (Visible, Location, Movement)
    -- 30 LickAI (Visible, Location, Movement)
    -- 30 LittleKeenBehavior (Visible, Location, Movement)
    -- 30 MimrockAI (Visible, Location, Movement)
    -- 30 MushroomAI (Visible, Location, Movement)
    -- 30 SlugAI (Visible, Location, Movement)
    -- 30 SnakeAI (Visible, Location, Movement)
    -- 40 KeenBehavior (Visible, Location, Collidable, Movement, PlatformClipped)

    -- Defines the root Keen 4 components, including the required standard
    -- components.
    procedure Define_Components( factory : not null A_Entity_Factory );

end Entities.Galaxy;
