### DETECT OS (Windows or OS X) ###
# OS-Specific variables:
#   TOOLCHAIN : Name of the toolchain to use: 'x86_64-windows', 'x86_64-darwin', etc.
#   SHORT_OS  : Short, lower case name of the OS: 'osx', 'linux', 'win'
#   IS_WIN    : 'True', ''
#   IS_OSX    : 'True', ''
#   IS_LINUX  : 'True', ''
#   EXT       : Extention of executable files
IS_LINUX :=
IS_OSX   :=
IS_WIN   :=
ifeq ($(OS), Windows_NT)
    SHORT_OS   := win

    # PROCESSOR_ARCHITECTURE can be "AMD64", "IA64", "x86", or "ARM"
    # If PROCESSOR_ARCHITECTURE = "x86" and PROCESSOR_ARCHITEW6432 exists, then make.exe is a
    # 32-bit process running on a 64-bit system under WOW64.
    TOOLCHAINS := x86_64-windows x86-windows
    TOOLCHAIN  := $(subst AMD64,x86_64-windows,$(subst x86,x86-windows,$(PROCESSOR_ARCHITECTURE)))
    TOOLCHAIN  := $(if $(PROCESSOR_ARCHITEW6432),x86_64-windows,$(TOOLCHAIN))

    IS_WIN     := True
    EXT        := .exe
else ifeq ($(shell uname),Linux)
    SHORT_OS   := linux
    TOOLCHAINS := x86_64-linux
    TOOLCHAIN  := x86_64-linux
    IS_LINUX   := True
    EXT        :=
else
    SHORT_OS   := osx
    TOOLCHAINS := x86_64-darwin
    TOOLCHAIN  := x86_64-darwin
    IS_OSX     := True
    EXT        :=
endif

### COMMON FUNCTIONS ###
# cat     : Produces a command that can be executed to display the contents of a file.
# rm      : Produces a command that can be executed to delete a file.
# slashes : Translates all slashes to backslashes on Windows.
cat     = $(if $(IS_WIN),cat $(call slashes,$1),cat $1)
rm      = $(if $(IS_WIN),del $(call slashes,$1) 2> nul,rm -rf $1)
slashes = $(subst $(if $(IS_WIN),/,\),\,$1)

################################################################################

### DEFAULT CONFIGURATION ###
# BUILD                : build configuration
# NUMBER_OF_PROCESSORS : number of processors to use for compilation
# TRANSMOG_VERBOSE     : execute transmog in verbose mode? (empty = quiet mode)
BUILD                ?= debug
NUMBER_OF_PROCESSORS ?= 4
TRANSMOG_VERBOSE     ?=

### DIRECTORY CONFIGURATION ###
BUILD_DIR   = build
OBJ_DIR     = obj/$(TOOLCHAIN)/$(BUILD)
RES_DIR     = res/$(TOOLCHAIN)
LIB_DIR     = lib/$(TOOLCHAIN)
BIN_DIR     = bin
MEDIA_DIR   = media
DIST_DIR    = dist

### EXTERNAL TOOLS ###
ECHO      = echo
NEWLINE  := $(call cat, $(BUILD_DIR)/newline)
GNATDOC   = gnatdoc$(EXT)  #     # gnatdoc gpl 2017+ http://libre.adacore.com
GNATMAKE  = gnatmake$(EXT) #     # gnatmake gpl 2017+ http://libre.adacore.com
GPRBUILD  = gprbuild$(EXT) #     # gprbuild gpl 2017+ http://libre.adacore.com
GPRCLEAN  = gprclean$(EXT) #     # gprclean gpl 2017+ http://libre.adacore.com
MAKE      = make$(EXT) #         # GNU Make 3.80+
PIPE     := |
SPACE    := 
SPACE    += 
STRIP     = strip$(EXT) #        # GNU strip 2.18+
TOUCH     = touch$(EXT)

### INTERNAL TOOLS ###
TOOL_DIR    = $(BUILD_DIR)/tools
ANNOUNCE   := $(TOOL_DIR)/announce$(EXT)
BUILD_DATE := $(TOOL_DIR)/build_date$(EXT)
MKBUNDLE   := $(TOOL_DIR)/mkbundle$(EXT)
RCBUILD    := $(TOOL_DIR)/rcbuild$(EXT)
REVISION   := $(TOOL_DIR)/revision$(EXT)
RM         := $(TOOL_DIR)/rmfile$(EXT)
SMART_COPY := $(TOOL_DIR)/smart_copy$(EXT)
ZIP        := $(TOOL_DIR)/zipfile$(EXT)
BUILD_TOOL_NAMES    := announce build_date mkbundle rcbuild revision rmfile smart_copy zipfile
BUILD_TOOLS         := $(foreach i,$(BUILD_TOOL_NAMES),$(TOOL_DIR)/$(i)$(EXT))
BUILD_TOOL_SRC_DIRS := -aIzipada/src
### TARGET EXECUTABLES ###
KEEN         := $(BIN_DIR)/keen$(EXT)
TINKER       := $(BIN_DIR)/tinker$(EXT)
TRANSMOG     := $(BIN_DIR)/transmog$(EXT)
IMPORTER     := $(BIN_DIR)/importer$(EXT)
TARGET_NAMES := tinker keen transmog importer media

### MEDIA ###
EXT_ARCHIVE          = .zip
MEDIA_ARCHIVE_NAMES  = audio definitions graphics resources scripts worlds
MEDIA_ARCHIVES      := $(foreach i,$(MEDIA_ARCHIVE_NAMES),$(MEDIA_DIR)/$(i)$(EXT_ARCHIVE))
CREDITS_DIR          = credits
README_FILE          = $(BIN_DIR)/readme.txt

### SCRIPTS MEDIA ###
EXT_COMPONENT   = .component
EXT_ENTITY      = .entity
EXT_PARTICLE    = .particle
EXT_SCRIPT      = .script
EXT_SV          = .sv
EXT_SCO         = .sco
DEFINITIONS_DIR = $(MEDIA_DIR)/definitions
SCRIPTS_DIR     = $(MEDIA_DIR)/scripts
SCO_DIRS        = $(SCRIPTS_DIR) $(DEFINITIONS_DIR)
SCRIPT_OBJECTS := $(foreach d,$(SCO_DIRS),$(foreach i,$(shell $(call cat, $(d)/zip.txt)),$(d)/$(i)))

### GRAPHICS MEDIA ###
EXT_TLZ         = .tlz
GRAPHICS_DIR    = $(MEDIA_DIR)/graphics
TILE_LIBRARIES := $(foreach i,$(shell $(call cat, $(GRAPHICS_DIR)/zip.txt)),$(filter %$(EXT_TLZ),$(GRAPHICS_DIR)/$(i)))

### WORLD MAPS MEDIA ###
EXT_WORLD    = .world
WORLD_DIR    = $(MEDIA_DIR)/worlds
WORLD_FILES := $(foreach i,$(shell $(call cat, $(WORLD_DIR)/zip.txt)),$(WORLD_DIR)/$(firstword $(subst :, ,$(i))))

### TARGET CONFIGURATION ###
BUILD_NUM     = $(shell $(REVISION) .)
VERSION       = $(shell $(call cat, $(BUILD_DIR)/version.txt))
FULL_VERSION  = $(VERSION).$(BUILD_NUM)
SCENARIO_VARS = -XVERSION=$(VERSION) -XBUILD_REV=$(BUILD_NUM) -XBUILD_DATE="$(shell $(BUILD_DATE) -d)" -XBUILD_TIME="$(shell $(BUILD_DATE) -t)" -XBUILD=$(BUILD)

### BUILD-SPECIFIC CONFIGURATION ###
# RC_DEBUG       : Switches passed to RCBUILD to indicate a debug executable
# RC_PRERELEASE  : Switches passed to RCBUILD to indicate a prerelease executable
# COMPRESS_MEDIA : Indicates that generated media should be compressed (non-empty = True)
# COMPRESS       : Commands executed after compilation to compress an executable
ifeq ($(BUILD),release)
    SCRIBBLE_DEBUG :=
    COMPRESS_MEDIA  = 1
    COMPRESSION     = compressed
else
    SCRIBBLE_DEBUG := -d
    COMPRESS_MEDIA  =
    COMPRESSION     = uncompressed
endif

################################################################################
# MAIN TARGETS #

# Default target - display build instructions
instructions: instructions-$(SHORT_OS)

tinker:      tinker-clear tinker-prebuild $(TINKER)
keen:        keen-clear keen-prebuild $(KEEN)
transmog:    transmog-clear transmog-prebuild $(TRANSMOG)
importer:    importer-clear importer-prebuild $(IMPORTER)
media:       media-clear media-prebuild $(SCRIPT_OBJECTS) $(TILE_LIBRARIES) $(MEDIA_ARCHIVES)

all: $(TARGET_NAMES)

install:
	@$(MAKE) keen BUILD=release
	@$(MAKE) tinker BUILD=release
	@$(MAKE) transmog BUILD=release
	@$(MAKE) media BUILD=RELEASE
	@$(MAKE) installer

portable:
	@$(MAKE) keen BUILD=release
	@$(MAKE) tinker BUILD=release
	@$(MAKE) transmog BUILD=release
	@$(MAKE) media BUILD=release
	@$(MAKE) portabledist

docs:
	@$(GNATDOC) -Pkeen/keen.gpr $(SCENARIO_VARS)
	@$(GNATDOC) -Ptinker/tinker.gpr $(SCENARIO_VARS)
	@$(SMART_COPY) keen/html $(DIST_DIR)/release-$(VERSION)/keen
	@$(SMART_COPY) tinker/html $(DIST_DIR)/release-$(VERSION)/tinker
	@echo Writing keen-$(VERSION)-docs.zip ...
	@$(ZIP) $(DIST_DIR)/keen-$(VERSION)-docs.zip $(DIST_DIR)/release-$(VERSION)/

clean: $(foreach i,$(TARGET_NAMES),$(i)-clean)

spotless: $(foreach i,$(TARGET_NAMES),$(i)-spotless) clean-tools

.PHONY: instructions tinker keen transmog importer media all install clean spotless docs

include $(BUILD_DIR)/Makefile.$(SHORT_OS)

################################################################################
# BUILD TOOLS #

$(TOOL_DIR)/%$(EXT):
	$(GNATMAKE) $(BUILD_TOOL_SRC_DIRS) -gnat12 -O2 -gnateDWINDOWS=$(if $(IS_WIN),True,False) -gnateDOSX=$(if $(IS_OSX),True,False) -gnateDLINUX=$(if $(IS_LINUX),True,False) -D $(TOOL_DIR)/src/$* $(TOOL_DIR)/src/$*/$*.adb -o $@
	@$(STRIP) $@

%-tool-clean:
	@$(RM) $(TOOL_DIR)/src/$*/*.o
	@$(RM) $(TOOL_DIR)/src/$*/*.ali
	@$(RM) -q $(TOOL_DIR)/$*$(EXT)

rmfile-tool-clean:
	@$(RM) $(TOOL_DIR)/src/$*/*.o
	@$(RM) $(TOOL_DIR)/src/$*/*.ali

tool-clean-message: ; @$(ECHO) Cleaning: build tools

clean-tools: tool-clean-message $(foreach i,$(BUILD_TOOL_NAMES),$(i)-tool-clean)

.PHONY: rmfile-tool-clean tool-clean-message clean-tools
.PRECIOUS: $(BUILD_TOOLS)

#------------------------------------------------------------------------------#
# MEDIA #

%$(EXT_SV) %$(EXT_SCRIPT) %$(EXT_ENTITY) %$(EXT_PARTICLE) %$(EXT_COMPONENT): ;

##
# Compile a Scribble value file
##

%$(EXT_SV)$(EXT_SCO): %$(EXT_SV)
	$(TRANSMOG) compile $(SCRIBBLE_DEBUG) $< $@

##
# Compile a Scribble script file
##

%$(EXT_SCRIPT)$(EXT_SCO): %$(EXT_SCRIPT)
	$(TRANSMOG) compile $(SCRIBBLE_DEBUG) $< $@

%$(EXT_ENTITY)$(EXT_SCO): %$(EXT_ENTITY)
	$(TRANSMOG) compile $(SCRIBBLE_DEBUG) $< $@

%$(EXT_PARTICLE)$(EXT_SCO): %$(EXT_PARTICLE)
	$(TRANSMOG) compile $(SCRIBBLE_DEBUG) $< $@

%$(EXT_COMPONENT)$(EXT_SCO): %$(EXT_COMPONENT)
	$(TRANSMOG) compile $(SCRIBBLE_DEBUG) $< $@

##
# Build a graphics library (requires 3)
##
%$(EXT_TLZ):
	$(TRANSMOG) pack $(if $(COMPRESS_MEDIA),-c,) $(if $(TRANSMOG_VERBOSE),,-q) -m $(GRAPHICS_DIR)/$(*F)/matrices.sv -t $(GRAPHICS_DIR)/$(*F)/terrains.sv -i $(GRAPHICS_DIR)/$(*F)/index.txt $(GRAPHICS_DIR)/$(*F)$(EXT_TLZ)

##
# Build a media archive
##
%$(EXT_ARCHIVE):
	$(ZIP) $@ $(foreach i,$(shell $(call cat, $(MEDIA_DIR)/$(*F)/zip.txt)),$(MEDIA_DIR)/$(*F)/$(firstword $(subst :, ,$(i))):$(lastword $(subst :, ,$(i))))

media-clean media-spotless: media-clean-message media-clear;

media-clear: $(RM)
	@$(RM) $(SCRIPT_OBJECTS)
	@$(RM) $(TILE_LIBRARIES)
	@$(RM) $(MEDIA_ARCHIVES)

media-clean-message: ; @$(ECHO) Cleaning: media

##
# Steps to prepare for building media:
#
# 1. Make sure transmog is available
# 2. Announce we're building media
##
media-prebuild: transmog-prebuild $(TRANSMOG) media-message

media-message: ; @$(ANNOUNCE) -3 Building media

.PHONY: media-clean media-spotless media-clean-message media-clear media-prebuild media-message

#------------------------------------------------------------------------------#
# APPLICATIONS #

%-clear: $(RM)
	@$(RM) $(BIN_DIR)/$*$(EXT)
	@$(RM) $*/$(OBJ_DIR)/version.o

###
# Steps for pre-building a target:
#
# 1. Build all helper tools
# 2. Display the target's building message
###
%-prebuild: %-install_runtime $(BUILD_TOOLS);

###
# Steps for compiling a target executable:
#
# 1. Display the compilation message (ex: Building keen - debug)
# 2. Compile the resources (ie: keen-resources)
# 3. Compile the source code (ie: keen-compile)
# 4. Optionally compress the executable (ie: keen-[compressed|uncompressed]
###
$(BIN_DIR)/%$(EXT): %-message %-resources %-compile %-$(COMPRESSION);

%-message: ; @$(ANNOUNCE) -3 Building $* - $(TOOLCHAIN) - $(BUILD)

###
# Steps for cleaning a target:
#
# 1. Build RMFILE tool
# 2. Display cleaning message
# 3. Clean the target's resources
# 4. Clean the target's project with gprclean
# 5. Delete the target's executable
###
%-clean: $(RM) %-clean-message %-clean_resources %-clear
	@$(GPRCLEAN) -q $(SCENARIO_VARS) -P$*/$*.gpr

%-clean-message: ; @$(ANNOUNCE) Cleaning: $*

###
# Steps for spotlessly cleaning a target:
#
# 1. Build the RMFILE tool
# 2. Display spotless message
# 3. Uninstall the target's runtime libraries
# 4. Clean the target's resources
# 5. Delete the target's executable
# 6. Clean the target's project with gprclean for all build types
###
%-spotless: $(RM) %-spotless-message %-uninstall_runtime %-clean_resources %-clear
	@$(GPRCLEAN) -q -r --target=$(TOOLCHAIN) -XBUILD=debug -P$*/$*.gpr
	@$(GPRCLEAN) -q -r --target=$(TOOLCHAIN) -XBUILD=profile -P$*/$*.gpr
	@$(GPRCLEAN) -q -r --target=$(TOOLCHAIN) -XBUILD=release -P$*/$*.gpr

%-spotless-message: ; @$(ECHO) Cleaning spotless: $*

#------------------------------------------------------------------------------#
# INSTALLER #

installer: installer-message installer-$(SHORT_OS);

installer-message: ; @$(ANNOUNCE) -3 Building installer

.PHONY: installer installer-message

#------------------------------------------------------------------------------#
# PORTABLE DISTRIBUTION #

portabledist: portable-message portable-$(SHORT_OS);

portable-message: ; @$(ANNOUNCE) -3 Building portable distribution

.PHONY: portable portable-message
