---------------------------------

Keen Galaxy v0.15.0
Copyright (c) 2008-2019 Kevin Wellwood

---------------------------------

I. COMMAND LINE ARGUMENTS

The following command line arguments can be used with Keen Galaxy:

Argument: "-?"
    Displays command line switches help for Keen Galaxy.

Argument: "-DV=y" (Debug Verbosity)
    Sets the global debugging output verbosity 'y' for all systems.

    Global verbosity levels (y):
        s = Silent
        e = Errors only
        w = Warnings and errors
        i = Info, warnings, and errors

Argument: "-DVxxx=y"
    Sets debugging output for a specific system 'xxx' to verbosity level 'y'.

    Debug systems (xxx):
        audio       Audio system; music and sound effects
        app         General application
        asset       Asset management
        entity      Entities
        eval        Script evaluation
        events      Events / event managers
        focus       GUI input focus
        font        Font assets
        game        Game logic
        gui         GUI and widgets
        init        All application initialization
        input       Keyboard, mouse and game controllers
        log         Logging
        mem         Memory allocation
        particles   Particle engine
        physics     Physics system
        prefs       Preferences and configuration
        procs       Process managers
        res         Data resources
        script      Script execution
        tiles       Tile libraries
        view        Game view
        world       Game world assets

    System verbosity levels (y):
        e = Errors only
        w = Warnings and errors
        i = Info, warnings, and errors

Argument: "-DD=y" (Debug decorators)
    Sets the debugging decorators that prefix each line of debugging output.

    Decoration levels (y):
        0 = No decoration
        1 = Thread identifier
        2 = Thread identifier and originating source unit

Argument: "-DF=y" (Debug filename)
    Logs all debugging output to a file 'y'.

Argument: "-log"
    Enables the graphical log window to display debugging output instead of stdout.

Argument: "-level" "filename" (Play level)
    Loads and plays the world at path 'filename' immediately, bypassing the menu.
    The filename argument can be an absolute file path, or file path relative to
    the executable directory.

II. PREFERENCES

II.A. Location of the Keen Configuration File

The following is a complete list of preferences that may be set in the
application configuration file, keen.cfg. The configuration file may be found
in the following locations:

Windows Vista and newer:
C:\Users\<username>\Documents\Keen Galaxy\

Mac OS X:
/Users/<username>/Documents/Keen Galaxy/

Linux:
/home/<username>/Documents/Keen Galaxy/

II.B. List of Preferences By Section

The configuration file is composed of sections, each beginning with a line
containing the section name in square brackets (e.g. "[application]"). Each line
in a section follows the format "KEY = VALUE". Blank lines are ignored.

II.B.1. [preference]

The preference section contains general (misc) preferences.

-None-

II.B.2. [application]

The application section contains preferences specific to application, including
game update rates, the graphics window, and audio preferences.

media          = C:\ProgramData\Keen Galaxy\media\   (path to media)
credits        = ./credits
xres           = 640      (pixels)
yres           = 400      (pixels)
windowed       = 1        (0 or 1)
maximized      = 0        (0 or 1)
fullscreen     = 0        (0 or 1)
music          = 1        (0 or 1)
soundfx        = 1        (0 or 1)

II.B.3. [keyboard]

The keyboard section sets key bindings for the various actions in the game.
The numeric values used are Allegro 4 key codes.

binding.move_up     = 84      (KEY_UP)
binding.move_down   = 85      (KEY_DOWN)
binding.move_left   = 82      (KEY_LEFT)
binding.move_right  = 83      (KEY_RIGHT)
binding.jump        = 117     (KEY_LCONTROL)    or 26 (KEY_Z) for OS X
binding.pogo        = 119     (KEY_ALT)         or 24 (KEY_X) for OS X
binding.shoot       = 75      (KEY_SPACE)
binding.progress    = 67      (KEY_ENTER)
binding.pause       = 16      (KEY_P)
binding.menu        = 59      (KEY_ESC)
binding.menu_next   = 84      (KEY_UP)
binding.menu_prev   = 85      (KEY_DOWN)
binding.menu_select = 67      (KEY_ENTER)

II.B.4. [development]

The development section exposes various internal values, allowing quick
tweaks during development. These are subject to change at any time (more so
than the other sections) and will be unavailable in the final release.

scene.Keen.Platform.slack_x                 = 44.0      (pixels)
scene.Keen.Platform.slack_y                 = 50.0      (pixels)
scene.Keen.Map.slack_x                      = 24.0      (pixels)
scene.Keen.Map.slack_y                      = 24.0      (pixels)
entity.player.keen.library                  = keen      (tile library name)
entity.player.keen.walk_speed               = 92.0      (pixels/sec)
entity.player.keen.walk_acceleration        = 700.0     (pixels/sec^2)
entity.player.keen.midair_acceleration      = 500.0     (pixels/sec^2)
entity.player.keen.midair_pogo_acceleration = 350.0     (pixels/sec^2)
entity.player.keen.jump_speed               = 192.0     (pixels/sec)
entity.player.keen.jump_acceleration        = 2400.0    (pixels/sec^2)
entity.player.keen.falling_speed            = 54.0      (pixels/sec)
entity.player.keen.climb_speed              = 48.0      (pixels/sec)
entity.player.keen.climb_acceleration       = 2400.0    (pixels/sec^2)
entity.player.keen.slide_speed              = 64.0      (pixels/sec)
entity.player.keen.slide_acceleration       = 2400.0    (pixels/sec^2)
entity.player.keen.min_jump_height          = 10.0      (pixels)
entity.player.keen.max_jump_height          = 40.0      (pixels)
entity.player.keen.min_pogo_height          = 16.0      (pixels)
entity.player.keen.max_pogo_height          = 80.0      (pixels)
entity.player.keen.max_bounce_height        = 26.0      (pixels)
entity.player.keen.look_speed               = 80.0      (pixels/sec)
entity.player.little_keen.library           = keen      (tile library name)
entity.player.little_keen.height            = 16        (pixels)
entity.player.little_keen.width             = 16        (pixels)
entity.player.little_keen.walk_speed        = 92.0      (pixels/sec)
entity.player.little_keen.walk_acceleration = 700.0     (pixels/sec^2)

II.B.5. [physics]

The physics section contains more development-related values that apply to
all entities handled by the game's physics system. Preferences in this section
will also be unavailable in the final release.

Map.friction_x       = 700.0      (pixels/sec^2)
Map.friction_y       = 700.0      (pixels/sec^2)
Map.gravity          = 0.0        (pixels/sec^2)
Map.max_speed_x      = 500.0      (pixels/sec)
Map.max_speed_y      = 500.0      (pixels/sec)
Platform.friction_x  = 800.0      (pixels/sec^2)
Platform.friction_y  = 0.0        (pixels/sec^2)
Platform.gravity     = 768.0      (pixels/sec^2)
Platform.max_speed_x = 140.0      (pixels/sec)
Platform.max_speed_y = 380.0      (pixels/sec)
