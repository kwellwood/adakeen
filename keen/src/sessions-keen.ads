--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Sessions.Keen is

    -- Keen's Game Session Variables
    --
    -- Name        Type      Initial   Description
    -- ----        ----      -------   -----------
    -- ammo        number    5         Stunner ammo
    -- drops       number    0         Lifewater/vita drops
    -- points      number    0         Score
    -- nextLife    number    20,000    Next score for a 1-up
    -- lives       number    3         Lives remaining
    -- ancients    number    0         Collected ancients
    -- scuba       boolean   False     Have scuba suit?
    -- blueGem     number    0         Blue gem key (0 or 1)
    -- greenGem    number    0         Green gem key (0 or 1)
    -- redGem      number    0         Red gem key (0 or 1)
    -- yellowGem   number    0         Yellow gem key (0 or 1)
    --
    -- completed   list      []        List of completed level filenames
    -- lastWin     string    ""        Last completed level filename (if won)

    function Create return A_Session;

private

    type Keen_Session is new Session with null record;

end Sessions.Keen;
