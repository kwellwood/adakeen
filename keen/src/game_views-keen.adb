--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Command_Line;                  use Ada.Command_Line;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Assets.Fonts;                      use Assets.Fonts;
with Debugging;                         use Debugging;
with Events.Game;                       use Events.Game;
with Events.World;                      use Events.World;
with Gamepads;                          use Gamepads;
with Icons;                             use Icons;
with Keyboard;                          use Keyboard;
with Palette;                           use Palette;
with Preferences;                       use Preferences;
with Signals.Keys;                      use Signals.Keys;
with Styles;                            use Styles;
with Support.Paths;                     use Support.Paths;
with Values.Casting;                    use Values.Casting;
with Values.Maps;                       use Values.Maps;
with Widgets.Game_Screens.Game_Play;    use Widgets.Game_Screens.Game_Play;
with Widgets.Game_Screens.Menus.Main;   use Widgets.Game_Screens.Menus.Main;
with Widgets.Game_Screens.Message;      use Widgets.Game_Screens.Message;
with Widgets.Game_Screens.Message.Loading;
 use Widgets.Game_Screens.Message.Loading;
with Widgets.Game_Screens.Retry;        use Widgets.Game_Screens.Retry;
with Widgets.Game_Screens.Story;        use Widgets.Game_Screens.Story;
with Widgets.Game_Screens.Title;        use Widgets.Game_Screens.Title;

package body Game_Views.Keen is

    WINDOW_TITLE : constant String := "Keen Galaxy";

    ----------------------------------------------------------------------------

    package Connections is new Signals.Connections(Keen_View);
    use Connections;

    package Key_Connections is new Signals.Keys.Connections(Game_View);
    use Key_Connections;

    ----------------------------------------------------------------------------

    -- This is invoked by a "GameOver" message.
    procedure On_Game_Over( this : not null access Keen_View'Class );

    -- This is attached to the Game_View.GameStarted signal.
    procedure On_Game_Started( this : not null access Keen_View'Class );

    -- This is attached to the Game_View.Initialize signal.
    procedure On_Initialize( this : not null access Keen_View'Class );

    -- This is attached to the Game_View.LoadingBegan signal.
    procedure On_Loading_Began( this : not null access Keen_View'Class );

    -- This is attached to the Loading_Screen.Exited signal, called when the
    -- loading screen has exited.
    procedure On_Loading_Complete( this : not null access Keen_View'Class );

    -- This is attached to the Game_View.LoadingEnded signal.
    procedure On_Loading_Ended( this : not null access Keen_View'Class );

    -- This is attached to the Game_View.LoadingFailed signal.
    procedure On_Loading_Failed( this : not null access Keen_View'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create return A_Game_View is
        this : A_Keen_View := new Keen_View;
    begin
        this.Construct( Get_Pref( "application", "maxfps", 120 ) );

        this.Initializing.Connect( Slot( this, On_Initialize'Access ) );

        this.GameStarted.Connect( Slot( this, On_Game_Started'Access ) );
        this.LoadingBegan.Connect( Slot( this, On_Loading_Began'Access ) );
        this.LoadingEnded.Connect( Slot( this, On_Loading_Ended'Access ) );
        this.LoadingFailed.Connect( Slot( this, On_Loading_Failed'Access ) );

        this.Define_Binding( INPUT_UP,    "UP",    (ALLEGRO_KEY_UP,    AXIS_LYN, BUTTON_NONE) );
        this.Define_Binding( INPUT_DOWN,  "DOWN",  (ALLEGRO_KEY_DOWN,  AXIS_LYP, BUTTON_NONE) );
        this.Define_Binding( INPUT_LEFT,  "LEFT",  (ALLEGRO_KEY_LEFT,  AXIS_LXN, BUTTON_NONE) );
        this.Define_Binding( INPUT_RIGHT, "RIGHT", (ALLEGRO_KEY_RIGHT, AXIS_LXP, BUTTON_NONE) );

#if OSX'Defined then
        -- Control and Alt can't be used on OS X because they're linked to changing
        -- the active desktop when combined with the arrow keys, by default.
        this.Define_Binding( INPUT_JUMP,    "JUMP",    (ALLEGRO_KEY_Z,     AXIS_NONE, BUTTON_SOUTH) );
        this.Define_Binding( INPUT_POGO,    "POGO",    (ALLEGRO_KEY_X,     AXIS_NONE, BUTTON_WEST) );
        this.Define_Binding( INPUT_CONSOLE, "CONSOLE", (ALLEGRO_KEY_TILDE, AXIS_NONE, BUTTON_L1) );
#else
        -- Standard controls for jump and pogo
        this.Define_Binding( INPUT_JUMP,    "JUMP",    (ALLEGRO_KEY_LCTRL, AXIS_NONE, BUTTON_SOUTH) );
        this.Define_Binding( INPUT_POGO,    "POGO",    (ALLEGRO_KEY_ALT,   AXIS_NONE, BUTTON_WEST) );
        this.Define_Binding( INPUT_CONSOLE, "CONSOLE", (ALLEGRO_KEY_TILDE, AXIS_NONE, BUTTON_L1) );
#end if;

        this.Define_Binding( INPUT_SHOOT,    "SHOOT",    (ALLEGRO_KEY_SPACE,  AXIS_NONE, BUTTON_EAST) );
        this.Define_Binding( INPUT_PROGRESS, "PROGRESS", (ALLEGRO_KEY_ENTER,  AXIS_NONE, BUTTON_BACK) );
        this.Define_Binding( INPUT_PAUSE,    "PAUSE",    (ALLEGRO_KEY_P,      AXIS_NONE, BUTTON_R1) );
        this.Define_Binding( INPUT_BACK,     "MENU",     (ALLEGRO_KEY_ESCAPE, AXIS_NONE, BUTTON_START) );

        return A_Game_View(this);
    exception
        when others =>
            Delete( A_Game_View(this) );
            raise;
    end Create;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Keen_View ) is
    begin
        if this.screenMgr /= null and then not this.screenMgr.Contains( this.scrGameplay ) then
            Delete( A_Widget(this.scrGameplay) );
        end if;
        Game_View(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Enable_Widescreen( this    : not null access Keen_View'Class;
                                 enabled : Boolean ) is
    begin
        if enabled then
            this.Get_Window.Set_Min_Size( WIDE_WINDOW_WIDTH, WIDE_WINDOW_HEIGHT );
            this.aspect.Set_Base_Size( Float(WIDE_WINDOW_WIDTH), Float(WIDE_WINDOW_HEIGHT) );
        else
            this.Get_Window.Set_Min_Size( MIN_WINDOW_WIDTH, MIN_WINDOW_HEIGHT );
            this.aspect.Set_Base_Size( Float(MIN_WINDOW_WIDTH), Float(MIN_WINDOW_HEIGHT) );
        end if;
    end Enable_Widescreen;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Close_Window( this : access Keen_View; allowed : in out Boolean ) is
        pragma Unreferenced( this );
    begin
        -- there's no "Save Game?" dialog; just quit
        allowed := True;
    end On_Close_Window;

    ----------------------------------------------------------------------------

    overriding
    procedure On_Message( this : access Keen_View;
                          name : String;
                          args : List_Value'Class ) is
        msg     : A_Message_Screen;
        content : Map_Value;
    begin
        -- MessageBox( contents : map, postMessage : string := "" )
        -- show a message box
        if name = "MessageBox" then
            -- the 'contents' map (first arg) is structured as:
            --
            -- { text : string, icon : string, textAlign : number }
            --
            -- the player can close the box by pressing any key. after doing so,
            -- the 'postMessage' (second arg), if given, will be sent to the
            -- game.
            content := args.Get( 1 ).Map;
            msg := Create_Message_Screen( this, isOpaque => True );
            msg.Set_Text( content.Get_Unbounded_String( "text" ) );
            msg.Set_Icon( Create_Icon( content.Get_String( "icon" ) ) );
            case content.Get_Int( "textAlign" ) is
                when     -1 => msg.Set_Text_Align( Left );
                when      1 => msg.Set_Text_Align( Right );
                when others => msg.Set_Text_Align( Center );
            end case;
            msg.Set_Close_Message( args.Get_String( 2 ) );
            msg.Set_Closable( True );
            msg.Set_Auto_Pause( False );
            this.screenMgr.Add_Front( msg );

        -- AskRetry( levelName : string, yesText : string, noText : string )
        -- show level retry dialog
        elsif name = "AskRetry" then
            this.screenMgr.Add_Front( Create_Retry_Screen( this,
                                                           levelName => args.Get_String( 1 ),
                                                           yesText   => Cast_String( args.Get( 2 ), "Try Again" ),
                                                           noText    => Cast_String( args.Get( 3 ), "Give Up" ) ) );

        elsif name = "GameOver" then
            this.On_Game_Over;

        elsif name = "Finale" then
            this.screenMgr.Add_Front( Create_Story_Screen( this, "finale", Create_Icon( "finale:finale01" ) ) );

        end if;
    end On_Message;

    ----------------------------------------------------------------------------

    procedure Load_Saved_Game( this : not null access Keen_View'Class ) is
    begin
        this.Load_Game( "galaxy1.sav" );
    end Load_Saved_Game;

    ----------------------------------------------------------------------------

    procedure On_Initialize( this : not null access Keen_View'Class ) is
    begin
        Register_Font_Name( "standard", "dejavusans.ttf" );
        Register_Font_Name( "dos",      "dosfont.ttf"    );
        Register_Font_Name( "menu",     "keenmenu.ttf"   );
#if WINDOWS'Defined then
        Register_Font_Name( "monospace", System_Font_Directory & "cour.ttf" );
#elsif OSX'Defined then
        Register_Font_Name( "monospace", System_Font_Directory & "Courier New.ttf" );
#elsif LINUX'Defined then
        Register_Font_Name( "monospace", System_Font_Directory & "truetype/dejavu/DejaVuSansMono.ttf" );
#end if;

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

#if WINDOWS'Defined then
        this.Get_Window.KeyTyped.Connect( Slot( this, Quit'Access, ALLEGRO_KEY_F4, (ALT=>Yes,others=>No) ) );
#elsif OSX'Defined then
        this.Get_Window.KeyTyped.Connect( Slot( this, Quit'Access, ALLEGRO_KEY_Q, (CMD=>Yes,others=>No) ) );
#elsif LINUX'Defined then
        this.Get_Window.KeyTyped.Connect( Slot( this, Quit'Access, ALLEGRO_KEY_Q, (CTRL=>Yes,others=>No) ) );
#end if;

        this.Get_Window.Set_Title( WINDOW_TITLE );
        this.Get_Window.Set_Min_Size( MIN_WINDOW_WIDTH, MIN_WINDOW_HEIGHT );

            this.aspect := Create_Aspect_Layout( this );
            this.Get_Window.Add_Widget( this.aspect );
            this.aspect.Enable_Integer_Scaling( True );
            this.aspect.Enable_Auto_Zoom( True );
            this.Enable_Widescreen( Get_Pref( "application", "widescreen" ) );

                -- Screen Manager and initial game screens:
                -- Title (in front) -> Main Menu -> Gameplay
                this.screenMgr := Create_Screen_Manager( this );
                this.aspect.Add_Widget( this.screenMgr );
                this.screenMgr.Fill( this.aspect );

                    this.screenMgr.Add_Front( Create_Main_Menu_Screen( this, gameInProgress => False ) );
                    this.screenMgr.Add_Front( Create_Title_Screen( this ) );
                    this.screenMgr.Request_Focus;

                    this.scrGameplay := A_Game_Screen(Create_Game_Play_Screen( this ));

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        -- kick off a new game immediately, starting in the level that was
        -- specified. Keen_Session knows which level to play.
        -- check command line arguments
        for i in 1..Argument_Count loop
            if Argument( i ) = "-level" and then i < Argument_Count then
                this.loadingHideDelay := Seconds( 0 );
                this.Start_Game;
                Queue_Load_World( Argument( i + 1 ) );
                exit;
            end if;
        end loop;
    end On_Initialize;

    ----------------------------------------------------------------------------

    procedure On_Game_Over( this : not null access Keen_View'Class ) is
    begin
        -- put the main menu back up
        this.screenMgr.Add_Front( Create_Main_Menu_Screen( this, gameInProgress => False ) );

        -- pop the gameplay screen
        if this.screenMgr.Pop_Back /= this.scrGameplay then
            pragma Assert( False );  -- it should have been there!
        end if;
    end On_Game_Over;

    ----------------------------------------------------------------------------

    procedure On_Game_Started( this : not null access Keen_View'Class ) is
    begin
        -- pop all screens on the stack
        while this.screenMgr.Get_Front /= null and this.screenMgr.Get_Front /= this.scrGameplay loop
            this.screenMgr.Delete_Front;
        end loop;
        if this.screenMgr.Get_Front = this.scrGameplay then
            this.scrGameplay := this.screenMgr.Pop_Front;
        end if;

        -- clear paused state of a previous game session
        this.pausedByPlayer := False;
        this.pausedByMenu := 0;

        -- push the gameplay screen back on
        this.screenMgr.Add_Back( this.scrGameplay );
    end On_Game_Started;

    ----------------------------------------------------------------------------

    procedure On_Loading_Began( this : not null access Keen_View'Class ) is
        loadingScreen : A_Loading_Screen;
    begin
        loadingScreen := Create_Loading_Screen( this, this.loadingHideDelay );
        loadingScreen.Exited.Connect( Slot( this, On_Loading_Complete'Access ) );
        this.screenMgr.Add_Front( loadingScreen );

        -- reset the loading delay to normal. when loading a user map from the
        -- command line, the loading delay is initially zero.
        this.loadingHideDelay := LOADING_DELAY;
    end On_Loading_Began;

    ----------------------------------------------------------------------------

    procedure On_Loading_Complete( this : not null access Keen_View'Class ) is
    begin
        -- loading screen exited; time to switch to the next level
        -- if level loading failed, this will do nothing
        A_Game_Play_Screen(this.scrGameplay).Swap_Scenes;
    end On_Loading_Complete;

    ----------------------------------------------------------------------------

    procedure On_Loading_Ended( this : not null access Keen_View'Class ) is
    begin
        this.userMapOk := True;       -- loading the user map didn't fail
    end On_Loading_Ended;

    ----------------------------------------------------------------------------

    procedure On_Loading_Failed( this : not null access Keen_View'Class ) is
    begin
        if not this.userMapOk then
            -- failed to load the user map from the command line
            this.userMapOk := True;      -- its "ok" because is can't fail again
            this.On_Game_Over;
        end if;
    end On_Loading_Failed;

    ----------------------------------------------------------------------------

    procedure Pause_By_Menu( this    : not null access Keen_View'Class;
                             enabled : Boolean ) is
    begin
        if enabled then
            -- a menu paused game play
            this.pausedByMenu := this.pausedByMenu + 1;
            if this.pausedByMenu = 1 and then not this.pausedByPlayer then
                this.Pause_Game;
            end if;
        else
            -- a menu resumed game play
            this.pausedByMenu := this.pausedByMenu - 1;
            if this.pausedByMenu < 0 then
                this.pausedByMenu := 0;
                Dbg( "Resume without pause", D_VIEW, Error );
            else
                if this.pausedByMenu = 0 and then not this.pausedByPlayer then
                    this.Resume_Game;
                end if;
            end if;
        end if;
    end Pause_By_Menu;

    ----------------------------------------------------------------------------

    procedure Pause_By_Player( this    : not null access Keen_View'Class;
                               enabled : Boolean ) is
    begin
        -- player can only pause/resume if the menu hasn't paused game play
        if this.pausedByMenu = 0 then
            if this.pausedByPlayer /= enabled then
                this.pausedByPlayer := enabled;
                if enabled then
                    this.Pause_Game;
                else
                    this.Resume_Game;
                end if;
            end if;
        end if;
    end Pause_By_Player;

    ----------------------------------------------------------------------------

    procedure Quit_App( this : not null access Keen_View'Class ) is
    begin
        this.Quit;
    end Quit_App;

    ----------------------------------------------------------------------------

    procedure Save_Game_To_Disk( this : not null access Keen_View'Class ) is
    begin
        this.Save_Game( "galaxy1.sav" );
    end Save_Game_To_Disk;

    ----------------------------------------------------------------------------

    procedure Start_New_Game( this : not null access Keen_View'Class ) is
    begin
        this.Start_Game;
        Queue_Game_Message( "GameStarted" );
    end Start_New_Game;

begin

    -- used by the renderer
    Preferences.Set_Default( "application", "background", "#00AAAA" );

    -- used by scene widget
    Preferences.Set_Default( "development", "camera.speed", 192.0 );
    Preferences.Set_Default( "development", "camera.slowdownDistance", 32.0 );
    Preferences.Set_Default( "development", "camera.slowdownPercent", 0.8 );

end Game_Views.Keen;
