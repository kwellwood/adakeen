--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Displays;                  use Allegro.Displays;
--with Debugging;                         use Debugging;
with Drawing;                           use Drawing;
with Drawing.Bitmaps;                   use Drawing.Bitmaps;
with Events.Game;                       use Events.Game;
with Game_Views;                        use Game_Views;
with Icons;                             use Icons;
with Interfaces;                        use Interfaces;
with Widget_Styles.Registry;            use Widget_Styles.Registry;

package body Widgets.Scoreboards is

    DIGIT_WIDTH : constant := 8.0;

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Scoreboard( view : not null access Game_Views.Game_View'Class ) return A_Scoreboard is
        this : A_Scoreboard := new Scoreboard;
    begin
        this.Construct( view );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Scoreboard;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Scoreboard;
                         view : not null access Game_Views.Game_View'Class ) is
    begin
        Widget(this.all).Construct( view, "", "Scoreboard" );
        this.Listen_For_Event( EVT_GAME_VAR_CHANGED );
        this.Set_Focusable( False );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Content( this : access Scoreboard ) is

        -- Draws a number on the scoreboard, given the location of the upper
        -- left corner of the most significant digit and the maximum number of
        -- digits to draw. If the number needs more than the maximum number of
        -- digits, the largest number possible will be drawn using the maximum
        -- number of digits. (ex: digits = 2, n = 100, draws "99")
        procedure Draw_Number( number : Natural;
                               x, y   : Float;
                               dgts   : Positive ) is
            number2     : Unsigned_64 := Unsigned_64(number);
            n           : Unsigned_64;
            exp         : Unsigned_64;
            digit       : Natural;
        begin
            if number2 >= 10 ** dgts then
                number2 := 10 ** dgts - 1;
            end if;
            n := number2;

            for i in reverse 0..dgts-1 loop
                exp := 10 ** i;
                if exp <= number2 or else i = 0 then
                    digit := Natural((n - (n mod exp)) / exp);
                    Draw_Bitmap( this.style.Get_Image( DIGIT0_ELEM + digit ).Get_Bitmap,
                                 x + Float((dgts - 1) - i) * DIGIT_WIDTH, y, 0.0,
                                 Lighten( this.style.Get_Tint( DIGIT0_ELEM + digit ),
                                          this.style.Get_Shade( DIGIT0_ELEM + digit ) ) );
                    n := n - Unsigned_64(digit) * exp;
                end if;
            end loop;
        end Draw_Number;

    begin
        Al_Hold_Bitmap_Drawing( True );

        Draw_Bitmap( this.style.Get_Image( BACKGROUND_ELEM ).Get_Bitmap,
                     0.0, 0.0, 0.0,
                     Lighten( this.style.Get_Tint( BACKGROUND_ELEM ),
                              this.style.Get_Shade( BACKGROUND_ELEM ) ) );

        Draw_Number( this.points, 6.0, 4.0, dgts => 9 );
        Draw_Number( this.lives, 22.0, 20.0, dgts => 2 );
        Draw_Number( this.ammo, 62.0, 20.0, dgts => 2 );

        Al_Hold_Bitmap_Drawing( False );
    end Draw_Content;

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Height( this : access Scoreboard ) return Float
    is (Float(this.style.Get_Image( BACKGROUND_ELEM ).Get_Height));

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Width( this : access Scoreboard ) return Float
    is (Float(this.style.Get_Image( BACKGROUND_ELEM ).Get_Width));

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Event( this : access Scoreboard;
                            evt  : in out A_Event;
                            resp : out Response_Type ) is
        pragma Unreferenced( resp );
    begin
        if evt.Get_Event_Id = EVT_GAME_VAR_CHANGED then
            declare
                var : constant String := A_Game_Var_Changed_Event(evt).Get_Var;
                val : constant Value := A_Game_Var_Changed_Event(evt).Get_Value;
            begin
                if var = "points" then
                    this.points := val.To_Int;
                elsif var = "ammo" then
                    this.ammo := val.To_Int;
                elsif var = "lives" then
                    this.lives := val.To_Int;
                end if;
            end;
        end if;
    end Handle_Event;

begin

    Register_Style( "Scoreboard",
                    (BACKGROUND_ELEM => To_Unbounded_String( "background" ),
                     DIGIT0_ELEM     => To_Unbounded_String( "digit0" ),
                     DIGIT1_ELEM     => To_Unbounded_String( "digit1" ),
                     DIGIT2_ELEM     => To_Unbounded_String( "digit2" ),
                     DIGIT3_ELEM     => To_Unbounded_String( "digit3" ),
                     DIGIT4_ELEM     => To_Unbounded_String( "digit4" ),
                     DIGIT5_ELEM     => To_Unbounded_String( "digit5" ),
                     DIGIT6_ELEM     => To_Unbounded_String( "digit6" ),
                     DIGIT7_ELEM     => To_Unbounded_String( "digit7" ),
                     DIGIT8_ELEM     => To_Unbounded_String( "digit8" ),
                     DIGIT9_ELEM     => To_Unbounded_String( "digit9" )),
                    (BACKGROUND_ELEM => Area_Element,
                     DIGIT0_ELEM     => Icon_Element,
                     DIGIT1_ELEM     => Icon_Element,
                     DIGIT2_ELEM     => Icon_Element,
                     DIGIT3_ELEM     => Icon_Element,
                     DIGIT4_ELEM     => Icon_Element,
                     DIGIT5_ELEM     => Icon_Element,
                     DIGIT6_ELEM     => Icon_Element,
                     DIGIT7_ELEM     => Icon_Element,
                     DIGIT8_ELEM     => Icon_Element,
                     DIGIT9_ELEM     => Icon_Element),
                    (0 => To_Unbounded_String( "disabled" )) );

end Widgets.Scoreboards;
