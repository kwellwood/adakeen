--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Widgets.Scenes.Keen is

    function Create_Scene( view : not null access Game_Views.Game_View'Class;
                           id   : String := "" ) return A_Scene;
    pragma Postcondition( Create_Scene'Result /= null );

private

    type Keen_Scene is new Scene with null record;

    procedure Construct( this : access Keen_Scene;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );

    procedure Delete( this : in out Keen_Scene );

    procedure Handle_Event( this : access Keen_Scene;
                            evt  : in out A_Event;
                            resp : out Response_Type );
    pragma Precondition( evt /= null );

end Widgets.Scenes.Keen;
