--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Icons;                             use Icons;

private with Ada.Containers.Indefinite_Vectors;

package Widgets.Menu_Enumerations is

    -- A Menu_Enumeration for Keen is a simple select-enumeration widget
    -- designed to match the look and feel of the options in the menus, with an
    -- animated glowing dot next to the focus menu item.
    type Menu_Enumeration is new Widget and Animated with private;
    type A_Menu_Enumeration is access all Menu_Enumeration'Class;

    -- Creates a new menu enumeration.
    function Create_Menu_Enum( view : not null access Game_Views.Game_View'Class;
                               id   : String := "" ) return A_Menu_Enumeration;
    pragma Postcondition( Create_Menu_Enum'Result /= null );

    -- Menu Enumeration Signals
    function Accepted( this : not null access Menu_Enumeration'Class ) return access Signal'Class;

    -- Appends option 'text' to the enumeration.
    procedure Add_Option( this : not null access Menu_Enumeration'Class;
                          text : String );

    -- Enable/disable wrap-around at the first and last options. Disabled by default.
    procedure Enable_Wrap( this : not null access Menu_Enumeration'Class; enabled : Boolean );

    -- Returns the number of options in the enumeration.
    function Get_Count( this : not null access Menu_Enumeration'Class ) return Natural;

    -- Returns the index of the currently selected index (1 based) or zero if
    -- the enumeration is empty.
    function Get_Index( this : not null access Menu_Enumeration'Class ) return Natural;

    -- Returns the text of the currently selected option or an empty string if
    -- the enumeration is empty.
    function Get_Text( this : not null access Menu_Enumeration'Class ) return String;

    -- Sets the enum's icon. Leave 'width' and 'height' as 0 to use the icon's
    -- native size, otherwise the icon will be proportionately scaled to fit the
    -- specified size.
    procedure Set_Icon( this : not null access Menu_Enumeration'Class;
                        icon : Icon_Type );

    -- If the index does not exist, nothing will happen. Set 'notify' to True to
    -- emit an Accepted signal if the index changes.
    procedure Set_Index( this   : not null access Menu_Enumeration'Class;
                         index  : Positive;
                         notify : Boolean := True );

private

    BACKGROUND_ELEM : constant := 1;
    ICON_ELEM       : constant := 2;
    TEXT_ELEM       : constant := 3;

    DISABLED_STATE : constant Widget_State := 2**0;
    FOCUS_STATE    : constant Widget_State := 2**1;

    ----------------------------------------------------------------------------

    package String_Vectors is new Ada.Containers.Indefinite_Vectors( Positive, String, "=" );

    type Menu_Enumeration is new Widget and Animated with
        record
            icon          : Icon_Type;
            options       : String_Vectors.Vector;
            wrapEnabled   : Boolean := False;
            index         : Natural := 0;
            sigAccepted   : aliased Signal;
            stickCentered : Boolean := True;
        end record;

    procedure Construct( this : access Menu_Enumeration;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );

    procedure Draw_Content( this : access Menu_Enumeration );

    function Get_Min_Height( this : access Menu_Enumeration ) return Float;

    function Get_Min_Width( this : access Menu_Enumeration ) return Float;

    function Get_Visual_State( this : access Menu_Enumeration ) return Widget_State;

    procedure Tick( this : access Menu_Enumeration; time : Tick_Time );

end Widgets.Menu_Enumerations;
