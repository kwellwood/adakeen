--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Keyboard;                  use Allegro.Keyboard;
--with Debugging;                         use Debugging;
with Game_Views;                        use Game_Views;
with Gamepads;                          use Gamepads;
with Keyboard;                          use Keyboard;
with Widgets.Buttons.Pushes;            use Widgets.Buttons.Pushes;

package body Widgets.Game_Screens.Menus is

    package Connections is new Signals.Connections(Menu_Screen);
    use Connections;

    package Gamepad_Connections is new Signals.Gamepads.Connections(Menu_Screen);
    use Gamepad_Connections;

    package Key_Connections is new Signals.Keys.Connections(Menu_Screen);
    use Key_Connections;

    OPTIONS_WIDTH    : constant :=  90.0;
    OPTIONS_HEIGHT   : constant :=  64.0;
    OPTIONS_X_OFFSET : constant := -6.0;
    OPTIONS_Y_OFFSET : constant := -4.0;

    ----------------------------------------------------------------------------

    -- Ignores a key signal.
    procedure Ignore_Key( this : not null access Menu_Screen'Class ) is null;

    -- Called when the down arrow is pressed on the selected menu option.
    procedure Next_Option( this : not null access Menu_Screen'Class );

    -- Called when the focused widget changes.
    procedure On_Option_Focused( this : not null access Menu_Screen'Class );

    procedure On_Shown( this : not null access Menu_Screen'Class );

    -- Called when the up arrow is pressed on the selected menu option.
    procedure Prev_Option( this : not null access Menu_Screen'Class );

    -- Called when the up/down axis of the gamepad changes.
    procedure On_Stick_Y( this      : not null access Menu_Screen'Class;
                          gamepadId : Integer;
                          position  : Float );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    overriding
    procedure Construct( this     : access Menu_Screen;
                         view     : not null access Game_Views.Game_View'Class;
                         id       : String;
                         isOpaque : Boolean ) is
    begin
        Game_Screen(this.all).Construct( view, id, isOpaque );
        this.Shown.Connect( Slot( this, On_Shown'Access ) );
        this.Set_Style( "menu" );

        this.scrollPane := Create_Scroll_Pane( this.view );
        this.Add_Child( this.scrollPane );
        this.scrollPane.Width.Set( OPTIONS_WIDTH );
        this.scrollPane.Height.Set( OPTIONS_HEIGHT );
        this.scrollPane.Horizontal_Center.Attach( this.Horizontal_Center, OPTIONS_X_OFFSET );
        this.scrollPane.Vertical_Center.Attach( this.Vertical_Center, OPTIONS_Y_OFFSET );

        this.options := Create_Column_Layout( this.view );
        this.options.Set_Style( "menu" );
        this.scrollPane.Set_Client( this.options );
    end Construct;

    ----------------------------------------------------------------------------

    procedure Add_Item( this    : not null access Menu_Screen'Class;
                        item    : not null access Widget'Class;
                        control : access Widget'Class := null ) is
        c : A_Widget;
    begin
        c := A_Widget((if control /= null then control else item));

        if not this.controls.Is_Empty then
            -- connect the next/prev focus behavior
            c.Set_Prev( this.controls.Last_Element.Get_Id );
            this.controls.Last_Element.Set_Next( c.Get_Id );
        end if;

        -- remember which item was selected last
        c.Focused.Connect( Slot( this, On_Option_Focused'Access ) );

        -- move to next/prev option
        c.GamepadMoved.Connect( Slot( this, On_Stick_Y'Access, AXIS_LYN ) );
        c.GamepadMoved.Connect( Slot( this, On_Stick_Y'Access, AXIS_LYP ) );
        c.KeyPressed.Connect( Slot( this, Next_Option'Access, ALLEGRO_KEY_DOWN, (others=>No) ) );
        c.KeyPressed.Connect( Slot( this, Prev_Option'Access, ALLEGRO_KEY_UP, (others=>No) ) );

        -- prevent the default tab-focus-switching behavior
        c.KeyTyped.Connect( Slot( this, Ignore_Key'Access, ALLEGRO_KEY_TAB, (SHIFT=>Either, others=>No) ) );

        this.options.Append( item );
        this.controls.Append( c );
    end Add_Item;

    ----------------------------------------------------------------------------

    function Create_Menu_Button( this : not null access Menu_Screen'Class;
                                 text : String ) return A_Button is
        button : constant A_Button := Create_Push_Button( this.view );
    begin
        button.Set_Text( text );
        button.Set_Style( "menu" );
        return button;
    end Create_Menu_Button;

    ----------------------------------------------------------------------------

    procedure Next_Option( this : not null access Menu_Screen'Class ) is
    begin
        if A_Widget(this.Signaller).Focus_Next then
            null;
        end if;
    end Next_Option;

    ----------------------------------------------------------------------------

    procedure On_Option_Focused( this : not null access Menu_Screen'Class ) is
        view    : constant Rectangle := this.scrollPane.Get_Client.Get_Viewport;
        focused : constant A_Widget := A_Widget(this.Signaller);
        optionY : Float;
        optionH : Float;
    begin
        for i in 1..Integer(this.controls.Length) loop
            if focused = this.controls.Element( i ) then
                -- found the index of the focused option
                this.index := i;

                -- the focused control widget may be embedded in a container.
                -- use the ancestor widget in the .options Column_Layout for
                -- visibility calculations.
                optionY := this.options.Get_At( this.index ).Y.Get;
                optionH := this.options.Get_At( this.index ).Height.Get;

                -- scroll the options up or down if one was focused outside the
                -- currently visible range.
                if optionY < view.y then
                    this.scrollPane.Get_Client.Scroll_To( 0.0, optionY );
                elsif optionY + optionH - view.height > view.y then
                    this.scrollPane.Get_Client.Scroll_To( 0.0, optionY + optionH - view.height );
                end if;

                exit;
            end if;
        end loop;
    end On_Option_Focused;

    ----------------------------------------------------------------------------

    procedure On_Shown( this : not null access Menu_Screen'Class ) is
    begin
        if this.index <= Integer(this.controls.Length) then
            this.controls.Element( this.index ).Request_Focus;
        end if;
    end On_Shown;

    ----------------------------------------------------------------------------

    procedure On_Stick_Y( this      : not null access Menu_Screen'Class;
                          gamepadId : Integer;
                          position  : Float ) is
        pragma Unreferenced( gamepadId );
    begin
        if this.stickCentered then
            if position < -AXIS_SELECTED_THRESHOLD then
                this.Prev_Option;
            elsif position > AXIS_SELECTED_THRESHOLD then
                this.Next_Option;
            end if;
        end if;

        if (abs position) > AXIS_SELECTED_THRESHOLD then
            this.stickCentered := False;
        elsif (abs position) < AXIS_CENTERED_THRESHOLD then
            this.stickCentered := True;
        end if;
    end On_Stick_Y;

    ----------------------------------------------------------------------------

    procedure Prev_Option( this : not null access Menu_Screen'Class ) is
    begin
        if A_Widget(this.Signaller).Focus_Prev then
            null;
        end if;
    end Prev_Option;

end Widgets.Game_Screens.Menus;
