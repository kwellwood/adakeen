--
-- Copyright (c) 2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Widgets.Game_Screens.Menus.Gamepad is

    -- The gamepad controls screen represents Keen's controller bindings menu,
    -- accessible from the config menu.
    type Gamepad_Screen is new Menu_Screen with private;

    -- Creates a new gamepad configuration menu screen.
    function Create_Gamepad_Screen( view : not null access Game_Views.Game_View'Class ) return A_Game_Screen;

private

    type Gamepad_Screen is new Menu_Screen with null record;
    type A_Gamepad_Screen is access all Gamepad_Screen'Class;

    procedure Construct( this : access Gamepad_Screen;
                         view : not null access Game_Views.Game_View'Class );

    procedure Delete( this : in out Gamepad_Screen );

end Widgets.Game_Screens.Menus.Gamepad;
