--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Keyboard;                  use Allegro.Keyboard;
--with Debugging;                         use Debugging;
with Game_Views;                        use Game_Views;
with Gamepads;                          use Gamepads;

package body Widgets.Game_Screens.Paddle_War is

    package Connections is new Signals.Connections(Paddle_War_Screen);
    use Connections;

    package Gamepad_Connections is new Signals.Gamepads.Connections(Game_Screen);
    use Gamepad_Connections;

    package Key_Connections is new Signals.Keys.Connections(Game_Screen);
    use Key_Connections;

    ----------------------------------------------------------------------------

    procedure On_Shown( this : not null access Paddle_War_Screen'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Paddle_War_Screen( view : not null access Game_Views.Game_View'Class ) return A_Game_Screen is
        this : A_Paddle_War_Screen := new Paddle_War_Screen;
    begin
        this.Construct( view );
        return A_Game_Screen(this);
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Paddle_War_Screen;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Paddle_War_Screen;
                         view : not null access Game_Views.Game_View'Class ) is
    begin
        Game_Screen(this.all).Construct( view, "", isOpaque => True );
        this.Shown.Connect( Slot( this, On_Shown'Access ) );
        this.Set_Style( "menu" );
        this.Set_Background( Create_Icon( "interface:paddlewar_menu" ) );

        this.GamepadPressed.Connect( Slot( this, Exit_Screen'Access, BUTTON_EAST ) );
        this.KeyPressed.Connect( Slot( this, Exit_Screen'Access, ALLEGRO_KEY_ESCAPE ) );

        -- paddle war widget
        this.paddleWar := Create_Paddle_War( view );
        this.Add_Child( this.paddleWar );
        this.paddleWar.Set_Style( "menu" );
        this.paddleWar.Horizontal_Center.Attach( this.Horizontal_Center, -5.0 );
        this.paddleWar.Vertical_Center.Attach( this.Vertical_Center, 0.0 );
    end Construct;

    ----------------------------------------------------------------------------

    procedure On_Shown( this : not null access Paddle_War_Screen'Class ) is
    begin
        this.paddleWar.Reset;
        this.paddleWar.Request_Focus;
    end On_Shown;

end Widgets.Game_Screens.Paddle_War;
