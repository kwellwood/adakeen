--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Styles;                            use Styles;
with Widgets.Labels;                    use Widgets.Labels;
with Widgets.Text_Boxes;                use Widgets.Text_Boxes;

package Widgets.Game_Screens.Message is

    -- The message screen is a blue bordered box in the center of the screen
    -- with a character's picture on the left and up to three lines of text on
    -- the right. It does not accept any input.
    type Message_Screen is new Game_Screen with private;
    type A_Message_Screen is access all Message_Screen'Class;

    -- Creates a new message screen with a default icon of Keen and no text.
    -- Set 'transparent' to False to hide the screen behind this one with a
    -- black background.
    function Create_Message_Screen( view     : not null access Game_Views.Game_View'Class;
                                    isOpaque : Boolean := False ) return A_Message_Screen;

    -- If enabled, the message screen will auto-pause gameplay while the screen
    -- is active. Don't put other opaque screens on top of the message popup
    -- screen, because this will deactivate it.
    procedure Set_Auto_Pause( this    : not null access Message_Screen'Class;
                              enabled : Boolean );

    -- Sets whether or not the message screen can be closed by the user. The
    -- default is False.
    procedure Set_Closable( this     : not null access Message_Screen'Class;
                            closable : Boolean );

    -- Sets a message 'msg' to send to the game session upon closing.
    procedure Set_Close_Message( this : not null access Message_Screen'Class;
                                 msg  : String );

    -- Sets the icon to the left of the text in the message box.
    procedure Set_Icon( this : not null access Message_Screen'Class;
                        icon : Icon_Type );

    -- Sets the text of the message box.
    procedure Set_Text( this : not null access Message_Screen'Class; text : Unbounded_String );

    -- Sets the text alignment in the message box. If it is aligned to the left,
    -- the icon will be on the right. If it is aligned to the right, the icon
    -- will be on the left. The default is Right. Vertical and centered
    -- alignments are ignored.
    procedure Set_Text_Align( this  : not null access Message_Screen'Class;
                              align : Align_Type );

private

    type Message_Screen is new Game_Screen with
        record
            closable   : Boolean := False;
            autoPause  : Boolean := False;
            closeMsg   : Unbounded_String;
            leftImage  : A_Label;
            rightImage : A_Label;
            textBox    : A_Textbox;
        end record;

    procedure Construct( this     : access Message_Screen;
                         view     : not null access Game_Views.Game_View'Class;
                         isOpaque : Boolean );

    function To_String( this : access Message_Screen ) return String;

end Widgets.Game_Screens.Message;
