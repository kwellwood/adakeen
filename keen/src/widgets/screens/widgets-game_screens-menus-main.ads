--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Widgets.Game_Screens.Menus.Main is

    -- The main menu screen represents Keen's main menu. New game, save game,
    -- configure, quit, etc.
    type Main_Menu_Screen is new Menu_Screen with private;

    -- Creates a new main menu screen.
    function Create_Main_Menu_Screen( view           : not null access Game_Views.Game_View'Class;
                                      gameInProgress : Boolean ) return A_Game_Screen;

private

    type Main_Menu_Screen is new Menu_Screen with
        record
            gameInProgress : Boolean := False;
            btnNew         : A_Button;
            btnResume      : A_Button;
        end record;

    procedure Construct( this           : access Main_Menu_Screen;
                         view           : not null access Game_Views.Game_View'Class;
                         gameInProgress : Boolean );

end Widgets.Game_Screens.Menus.Main;
