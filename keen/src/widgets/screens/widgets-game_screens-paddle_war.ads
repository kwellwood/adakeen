--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Widgets.Paddle_Wars;               use Widgets.Paddle_Wars;

package Widgets.Game_Screens.Paddle_War is

    -- The screen for playing paddle war in the Keen menu. The game is reset
    -- each time the screen is created.
    type Paddle_War_Screen is new Game_Screen with private;

    -- Creates a new paddle war screen with a paddle war widget at 0-0.
    function Create_Paddle_War_Screen( view : not null access Game_Views.Game_View'Class ) return A_Game_Screen;

private

    type Paddle_War_Screen is new Game_Screen with
        record
            paddleWar : A_Paddle_War;
        end record;
    type A_Paddle_War_Screen is access all Paddle_War_Screen'Class;

    procedure Construct( this : access Paddle_War_Screen;
                         view : not null access Game_Views.Game_View'Class );

end Widgets.Game_Screens.Paddle_War;
