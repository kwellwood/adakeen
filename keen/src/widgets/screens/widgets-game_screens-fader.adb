--
-- Copyright (c) 2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Game_Views;                        use Game_Views;
with Widget_Styles.Property_Accessors;  use Widget_Styles.Property_Accessors;

package body Widgets.Game_Screens.Fader is

    package Connections is new Signals.Connections(Game_Screen);
    use Connections;

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Fader_Screen( view      : not null access Game_Views.Game_View'Class;
                                  fromColor : Allegro_Color;
                                  toColor   : Allegro_Color;
                                  duration  : Time_Span;
                                  easing    : Easing_Type ) return A_Game_Screen is
        this : A_Fader_Screen := new Fader_Screen;
    begin
        this.Construct( view, fromColor, toColor, duration, easing );
        return A_Game_Screen(this);
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Fader_Screen;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this      : access Fader_Screen;
                         view      : not null access Game_Views.Game_View'Class;
                         fromColor : Allegro_Color;
                         toColor   : Allegro_Color;
                         duration  : Time_Span;
                         easing    : Easing_Type ) is
    begin
        Game_Screen(this.all).Construct( view, "", isOpaque => Get_Alpha( fromColor ) = 1.0 and
                                                               Get_Alpha( toColor   ) = 1.0 );

        this.propColor.Construct( view.Get_Process_Manager, Style_Color_Accessor'(this.style, BACKGROUND_ELEM, STYLE_COLOR, DEFAULT_STATE) );
        this.propColor.Set( fromColor );
        this.propColor.Set( toColor, duration, easing );
        this.propColor.Finished.Connect( Slot( this, Exit_Screen'Access ) );
    end Construct;

end Widgets.Game_Screens.Fader;
