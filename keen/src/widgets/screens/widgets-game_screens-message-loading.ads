--
-- Copyright (c) 2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Widgets.Game_Screens.Message.Loading is

    -- The loading screen is special message screen that pops up when loading
    -- begins, shows the world intro message, and disappears automatically after
    -- loading ends or a minimum amount of time elapses.
    type Loading_Screen is new Message_Screen and
                               Animated and
                               Event_Listener with private;
    type A_Loading_Screen is access all Loading_Screen'Class;

    -- Creates a new loading message screen with the text "One Moment" and
    -- Keen's face. It will automatically disappear when loading is complete and
    -- after a minimum of 'minDisplay' time elapses.
    function Create_Loading_Screen( view       : not null access Game_Views.Game_View'Class;
                                    minDisplay : Time_Span ) return A_Loading_Screen;

private

    type Loading_Screen is new Message_Screen and Animated and Event_Listener with
        record
            loadEnded  : Boolean := False;
            success    : Boolean := False;
            fading     : Boolean := False;
            timeToHide : Time := Clock;
            worldMusic : Unbounded_String;
        end record;

    procedure Construct( this       : access Loading_Screen;
                         view       : not null access Game_Views.Game_View'Class;
                         minDisplay : Time_Span );

    procedure Handle_Event( this : access Loading_Screen;
                            evt  : in out A_Event;
                            resp : out Response_Type );

    procedure Tick( this : access Loading_Screen; time : Tick_Time );

end Widgets.Game_Screens.Message.Loading;
