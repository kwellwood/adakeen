--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Keyboard;                  use Allegro.Keyboard;
--with Debugging;                         use Debugging;
with Events.Game;                       use Events.Game;
with Game_Views;                        use Game_Views;
with Gamepads;                          use Gamepads;
with Keyboard;                          use Keyboard;
with Support;                           use Support;
with Widgets.Buttons.Pushes;            use Widgets.Buttons.Pushes;
with Widgets.Labels;                    use Widgets.Labels;
with Widgets.Panels;                    use Widgets.Panels;

package body Widgets.Game_Screens.Retry is

    package Connections is new Signals.Connections(Retry_Screen);
    use Connections;

    package Gamepad_Connections is new Signals.Gamepads.Connections(Retry_Screen);
    use Gamepad_Connections;

    package Key_Connections is new Signals.Keys.Connections(Retry_Screen);
    use Key_Connections;

    DIALOG_WIDTH  : constant := 210.0;
    DIALOG_HEIGHT : constant := 92.0;

    ----------------------------------------------------------------------------

    -- Called when the user responds with No to the retry level dialog.
    procedure Clicked_No( this : not null access Retry_Screen'Class );

    -- Called when the user responds with Yes to the retry level dialog.
    procedure Clicked_Yes( this : not null access Retry_Screen'Class );

    -- Called when the up/down stick on the gamepad moves.
    procedure Gamepad_Moved( this    : not null access Retry_Screen'Class;
                             gamepad : Gamepad_Axis_Arguments );

    -- Ignores a key signal.
    procedure Ignore_Key( this : not null access Retry_Screen'Class ) is null;

    -- Called when the down arrow is pressed on the selected menu option.
    procedure Next_Option( this : not null access Retry_Screen'Class );

    procedure On_Shown( this : not null access Retry_Screen'Class );

    -- Called when the up arrow is pressed on the selected menu option.
    procedure Prev_Option( this : not null access Retry_Screen'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Retry_Screen( view      : not null access Game_Views.Game_View'Class;
                                  levelName : String;
                                  yesText   : String;
                                  noText    : String ) return A_Game_Screen is
        this : A_Game_Screen := new Retry_Screen;
    begin
        Retry_Screen(this.all).Construct( view, levelName, yesText, noText );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Retry_Screen;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this      : access Retry_Screen;
                         view      : not null access Game_Views.Game_View'Class;
                         levelName : String;
                         yesText   : String;
                         noText    : String ) is
        panel  : A_Panel;
        label1 : A_Label;
        label2 : A_Label;
        button : A_Button;
    begin
        Game_Screen(this.all).Construct( view, "", isOpaque => False );
        this.Shown.Connect( Slot( this, On_Shown'Access ) );

        this.Set_Style( "popup" );

        -- retry level dialog
        panel := Create_Panel( view );
        this.Add_Child( panel );
        panel.Center( panel.Get_Parent, DIALOG_WIDTH, DIALOG_HEIGHT );
        panel.Set_Style( "dialog" );

            -- retry level text
            label1 := Create_Label( view );
            panel.Add_Widget( label1 );
            label1.Fill_Horizontal( label1.Get_Parent );
            label1.Y.Set(  8.0 );
            label1.Set_Text( "You didn't make it past" );
            label1.Set_Style( "dialog" );

            -- level name
            label2 := Create_Label( view );
            panel.Add_Widget( label2 );
            label2.Fill_Horizontal( label2.Get_Parent );
            label2.Top.Attach( label1.Bottom );
            label2.Set_Text( levelName );
            label2.Set_Style( "dialog" );

            -- option 1 (yes, retry)
            this.btnYes := Create_Push_Button( view, this.id & ":btnYes" );
            panel.Add_Widget( this.btnYes );
            this.btnYes.Fill_Horizontal( this.btnYes.Get_Parent, 2.0, 2.0 );
            this.btnYes.Bottom.Attach( this.btnYes.Get_Parent.Bottom, 22.0 );
            this.btnYes.Height.Set( 20.0 );
            this.btnYes.Set_Text( yesText );
            this.btnYes.Set_Style( "retry" );
            this.btnYes.Set_Next( this.id & ":btnNo" );
            this.btnYes.Clicked.Connect( Slot( this, Clicked_Yes'Access ) );
            this.btnYes.KeyPressed.Connect( Slot( this, Next_Option'Access, ALLEGRO_KEY_DOWN, (others=>No) ) );
            this.btnYes.KeyPressed.Connect( Slot( this, Prev_Option'Access, ALLEGRO_KEY_UP, (others=>No) ) );
            this.btnYes.KeyTyped.Connect( Slot( this, Ignore_Key'Access, ALLEGRO_KEY_TAB, (SHIFT=>Either, others=>No) ) );
            this.btnYes.GamepadMoved.Connect( Slot( this, Gamepad_Moved'Access, AXIS_LYN ) );
            this.btnYes.GamepadMoved.Connect( Slot( this, Gamepad_Moved'Access, AXIS_LYP ) );

            -- option 2 (no, quit)
            button := Create_Push_Button( view, this.id & ":btnNo" );
            panel.Add_Widget( button );
            button.Fill_Horizontal( button.Get_Parent, 2.0, 2.0 );
            button.Bottom.Attach( button.Get_Parent.Bottom, 2.0 );
            button.Height.Set( 20.0 );
            button.Set_Text( noText );
            button.Set_Style( "retry" );
            button.Set_Prev( this.id & ":btnYes" );
            button.Clicked.Connect( Slot( this, Clicked_No'Access ) );
            button.KeyPressed.Connect( Slot( this, Next_Option'Access, ALLEGRO_KEY_DOWN, (others=>No) ) );
            button.KeyPressed.Connect( Slot( this, Prev_Option'Access, ALLEGRO_KEY_UP, (others=>No) ) );
            button.KeyTyped.Connect( Slot( this, Ignore_Key'Access, ALLEGRO_KEY_TAB, (SHIFT=>Either, others=>No) ) );
            button.GamepadMoved.Connect( Slot( this, Gamepad_Moved'Access, AXIS_LYN ) );
            button.GamepadMoved.Connect( Slot( this, Gamepad_Moved'Access, AXIS_LYP ) );
    end Construct;

    ----------------------------------------------------------------------------

    procedure Clicked_No( this : not null access Retry_Screen'Class ) is
    begin
        this.Exit_Screen;
        Queue_Game_Message( "AbandonLevel" );
    end Clicked_No;

    ----------------------------------------------------------------------------

    procedure Clicked_Yes( this : not null access Retry_Screen'Class ) is
    begin
        this.Exit_Screen;
        Queue_Game_Message( "RetryLevel" );
    end Clicked_Yes;

    ----------------------------------------------------------------------------

    procedure Gamepad_Moved( this    : not null access Retry_Screen'Class;
                             gamepad : Gamepad_Axis_Arguments ) is
    begin
        if gamepad.position < -0.5 then
            this.Prev_Option;
        elsif gamepad.position > 0.5 then
            this.Next_Option;
        end if;
    end Gamepad_Moved;

    ----------------------------------------------------------------------------

    procedure Next_Option( this : not null access Retry_Screen'Class ) is
    begin
        if A_Widget(this.Signaller).Focus_Next then
            null;
        end if;
    end Next_Option;

    ----------------------------------------------------------------------------

    procedure On_Shown( this : not null access Retry_Screen'Class ) is
    begin
        this.btnYes.Request_Focus;
    end On_Shown;

    ----------------------------------------------------------------------------

    procedure Prev_Option( this : not null access Retry_Screen'Class ) is
    begin
        if A_Widget(this.Signaller).Focus_Prev then
            null;
        end if;
    end Prev_Option;

end Widgets.Game_Screens.Retry;
