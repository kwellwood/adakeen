--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Widgets.Buttons;                   use Widgets.Buttons;

package Widgets.Game_Screens.Retry is

    -- The retry screen is shown when Keen dies, to ask the player if he wants
    -- to try the level again or return to the map.
    type Retry_Screen is new Game_Screen with private;

    -- Creates a new retry level screen.
    function Create_Retry_Screen( view      : not null access Game_Views.Game_View'Class;
                                  levelName : String;
                                  yesText   : String;
                                  noText    : String ) return A_Game_Screen;

private

    type Retry_Screen is new Game_Screen with
        record
            btnYes : A_Button;
        end record;

    procedure Construct( this      : access Retry_Screen;
                         view      : not null access Game_Views.Game_View'Class;
                         levelName : String;
                         yesText   : String;
                         noText    : String ) ;

end Widgets.Game_Screens.Retry;
