--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Vectors;
with Widgets.Buttons;                   use Widgets.Buttons;
with Widgets.Row_Layouts;               use Widgets.Row_Layouts;
with Widgets.Scroll_Panes;              use Widgets.Scroll_Panes;

package Widgets.Game_Screens.Menus is

    -- Menu_Screen is an abstract base screen for all Keen menu screens with a
    -- list of options. Menu buttons are added and automatically layed out in
    -- the menu screen. It's mostly a mechanism for controlling what is in a
    -- menu screen and for uniformly organizing the contents.
    type Menu_Screen is abstract new Game_Screen with private;

private

    package Widget_Vectors is new Ada.Containers.Vectors(Positive, A_Widget, "=");
    use Widget_Vectors;

    type Menu_Screen is abstract new Game_Screen with
        record
            scrollPane    : A_Scroll_Pane;
            options       : A_Column_Layout;
            controls      : Widget_Vectors.Vector;
            index         : Integer := 1;
            stickCentered : Boolean := True;
        end record;

    procedure Construct( this     : access Menu_Screen;
                         view     : not null access Game_Views.Game_View'Class;
                         id       : String;
                         isOpaque : Boolean );

    -- Adds an item to the menu, placing it appropriately in the column. The
    -- 'item' widget will be connected to other menu items to transfer focus
    -- behavior. If 'item' is not a focusable element (e.g. a container) then
    -- pass the focusable control widget inside it as 'control'. Otherwise, pass
    -- null for 'control'.
    procedure Add_Item( this    : not null access Menu_Screen'Class;
                        item    : not null access Widget'Class;
                        control : access Widget'Class := null );

    function Create_Menu_Button( this : not null access Menu_Screen'Class;
                                 text : String ) return A_Button;

end Widgets.Game_Screens.Menus;
