--
-- Copyright (c) 2018-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Keyboard;                  use Allegro.Keyboard;
--with Debugging;                         use Debugging;
with Game_Views;                        use Game_Views;
with Gamepads;                          use Gamepads;
with GNAT.OS_Lib;                       use GNAT.OS_Lib;
with Preferences;                       use Preferences;
with Support;                           use Support;
with Support.Paths;                     use Support.Paths;
with Widgets.Buttons;                   use Widgets.Buttons;
with Widgets.Game_Screens.Story;        use Widgets.Game_Screens.Story;
with Widgets.Screen_Managers;           use Widgets.Screen_Managers;

package body Widgets.Game_Screens.Menus.Help is

    package Connections is new Signals.Connections(Help_Menu);
    use Connections;

    package Gamepad_Connections is new Signals.Gamepads.Connections(Game_Screen);
    use Gamepad_Connections;

    package Key_Connections is new Signals.Keys.Connections(Game_Screen);
    use Key_Connections;

    ----------------------------------------------------------------------------

    procedure Clicked_Credits( this : not null access Help_Menu'Class );

    procedure Clicked_Help( this : not null access Help_Menu'Class );

    procedure Clicked_Story( this : not null access Help_Menu'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Help_Menu( view : not null access Game_Views.Game_View'Class ) return A_Game_Screen is
        this : A_Game_Screen := new Help_Menu;
    begin
        Help_Menu(this.all).Construct( view );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Help_Menu;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Help_Menu;
                         view : not null access Game_Views.Game_View'Class ) is
        button : A_Button;
    begin
        Menu_Screen(this.all).Construct( view, "", isOpaque => True );

        this.GamepadPressed.Connect( Slot( this, Exit_Screen'Access, BUTTON_EAST ) );
        this.KeyPressed.Connect( Slot( this, Exit_Screen'Access, ALLEGRO_KEY_ESCAPE ) );

        this.creditsOpenable := Clock - Seconds( 10 );

        this.Set_Background( Create_Icon( "interface:help_menu" ) );

        -- Gameplay help button
        button := this.Create_Menu_Button( "GAMEPLAY" );
        button.Pressed.Connect( Slot( this, Clicked_Help'Access ) );
        this.Add_Item( button );

        -- Story button
        button := this.Create_Menu_Button( "STORY" );
        button.Pressed.Connect( Slot( this, Clicked_Story'Access ) );
        this.Add_Item( button );

        -- Credits button
        button := this.Create_Menu_Button( "CREDITS" );
        button.Pressed.Connect( Slot( this, Clicked_Credits'Access ) );
        this.Add_Item( button );
    end Construct;

    ----------------------------------------------------------------------------

    procedure Clicked_Credits( this : not null access Help_Menu'Class ) is
    begin
        -- don't open the credits folder more than once per second, in case
        -- someone hits the option a few times quickly by accident.
        if Clock >= this.creditsOpenable then
            Reveal_Path( Normalize_Pathname( Get_Pref( "application", "license" ) ) );
            this.creditsOpenable := Clock + Seconds( 1 );
        end if;
    end Clicked_Credits;

    ----------------------------------------------------------------------------

    procedure Clicked_Help( this : not null access Help_Menu'Class ) is
    begin
        A_Screen_Manager(this.parent).Add_Front( Create_Story_Screen( this.Get_View, "help", Create_Icon( "story:help01" ) ) );
    end Clicked_Help;

    ----------------------------------------------------------------------------

    procedure Clicked_Story( this : not null access Help_Menu'Class ) is
    begin
        A_Screen_Manager(this.parent).Add_Front( Create_Story_Screen( this.Get_View, "story", Create_Icon( "story:story01" ) ) );
    end Clicked_Story;

end Widgets.Game_Screens.Menus.Help;
