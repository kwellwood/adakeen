--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Game_Views.Keen;                   use Game_Views.Keen;
with Input_Bindings;                    use Input_Bindings;
with Object_Ids;                        use Object_Ids;
with Values.Tagged_Ids;                 use Values.Tagged_Ids;
with Widgets.Input_Boxes;               use Widgets.Input_Boxes;
with Widgets.Panels;                    use Widgets.Panels;
with Widgets.Progress_Boards;           use Widgets.Progress_Boards;
with Widgets.Scenes;                    use Widgets.Scenes;
with Widgets.Scoreboards;               use Widgets.Scoreboards;
with Widgets.Text_Boxes;                use Widgets.Text_Boxes;

package Widgets.Game_Screens.Game_Play is

    -- The game play screen contains the main Scene widget where all game play
    -- occurs. It is responsible for input to the scene during game play,
    -- and handling events relevant to the scene, the progress board, and score
    -- board.
    type Game_Play_Screen is new Game_Screen with private;
    type A_Game_Play_Screen is access all Game_Play_Screen'Class;

    -- Creates a new game play screen.
    function Create_Game_Play_Screen( view : not null access Game_Views.Game_View'Class ) return A_Game_Play_Screen;

    -- Returns the Scene widget in this screen.
    function Get_Scene( this : not null access Game_Play_Screen'Class ) return A_Scene;

    -- Swaps the visible with the invisible scene after a world has been loaded.
    -- Just before a world is loaded, the visible scene is disabled so it continues
    -- showing the current world while the new one is loaded. After the screen
    -- fades out, the scenes are swapped in darkness and the new world's scene
    -- fades in.
    procedure Swap_Scenes( this : not null access Game_Play_Screen'Class );

    ----------------------------------------------------------------------------

private

    type Binding_Rec is
        record
            action    : Input_Action;
            binding   : Input_Binding;
            ignoreKey : Boolean := False;   -- events from this key should be ignored until the next key press
        end record;

    type Control_Bindings is array (Positive range <>) of Binding_Rec;

    SCENE_INPUTS : constant Input_Actions :=
    (
        INPUT_UP,
        INPUT_DOWN,
        INPUT_LEFT,
        INPUT_RIGHT,
        INPUT_SHOOT,
        INPUT_JUMP,
        INPUT_POGO,
        INPUT_PROGRESS,
        INPUT_PAUSE,
        INPUT_CONSOLE
    );

    ----------------------------------------------------------------------------

    type Game_Play_Screen is new Game_Screen with
        record
            controls : Control_Bindings(SCENE_INPUTS'Range);

            -- the id of the player entity, for sending impulses.
            player : Entity_Id := NULL_ID;

            progressBoard : A_Progress_Board := null;
            scoreboard    : A_Scoreboard := null;
            scene         : A_Scene := null;
            scene2        : A_Scene := null;
            pnlPaused     : A_Panel := null;

            consoleLog    : A_Textbox := null;
            console       : A_Panel := null;
            consoleInput  : A_Input_Box := null;
            nextTaskId    : Integer := 1;

            stickCenteredX : Boolean := True;
            stickCenteredY : Boolean := True;
        end record;

    procedure Construct( this : access Game_Play_Screen;
                         view : not null access Game_Views.Game_View'Class );

    procedure Delete( this : in out Game_Play_Screen );

    -- Inherited from the Event_Listener interface. Handles events that the view
    -- is registered to receive.
    procedure Handle_Event( this : access Game_Play_Screen;
                            evt  : in out A_Event;
                            resp : out Response_Type );
    pragma Precondition( evt /= null );

    function Get_Scene( this : not null access Game_Play_Screen'Class ) return A_Scene is (this.scene);

end Widgets.Game_Screens.Game_Play;
