--
-- Copyright (c) 2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Events.Audio;                      use Events.Audio;
with Events.Game;                       use Events.Game;
with Events.World;                      use Events.World;
with Game_Views;                        use Game_Views;
with Game_Views.Keen;                   use Game_Views.Keen;
with Values.Casting;                    use Values.Casting;
with Widgets.Game_Screens.Fader;        use Widgets.Game_Screens.Fader;
with Widgets.Screen_Managers;           use Widgets.Screen_Managers;

package body Widgets.Game_Screens.Message.Loading is

    package Connections is new Signals.Connections(Loading_Screen);
    use Connections;

    procedure On_Fade_Complete( this : not null access Loading_Screen'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Loading_Screen( view       : not null access Game_Views.Game_View'Class;
                                    minDisplay : Time_Span ) return A_Loading_Screen is
        this : A_Loading_Screen := new Loading_Screen;
    begin
        this.Construct( view, minDisplay );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Loading_Screen;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this       : access Loading_Screen;
                         view       : not null access Game_Views.Game_View'Class;
                         minDisplay : Time_Span ) is
    begin
        Message_Screen(this.all).Construct( view, isOpaque => False );
        this.timeToHide := Clock + minDisplay;

        this.Listen_For_Event( EVT_LOADING_ENDED );
        this.Listen_For_Event( EVT_WORLD_PROPERTY_CHANGED );

        this.Set_Style( "popup" );
        this.Set_Closable( False );
        this.Set_Auto_Pause( True );
        this.Set_Text_Align( Right );
        this.Set_Text( To_Unbounded_String( "One moment" ) );
        this.Set_Icon( Create_Icon( "interface:loading_keen1" ) );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Event( this : access Loading_Screen;
                            evt  : in out A_Event;
                            resp : out Response_Type ) is
        pragma Unreferenced( resp );
    begin
        case evt.Get_Event_Id is
            when EVT_LOADING_ENDED =>
                this.loadEnded := True;
                this.success := A_Loading_Event(evt).Is_Success;
                if not this.success then
                    -- loading failed and the world has not changed
                    this.Set_Icon( Create_Icon( "interface:talking_keen" ) );
                    this.Set_Text( To_Unbounded_String( "Keen stubbornly refuses to enter" ) );
                    Queue_Play_Sound( "no" );
                end if;

            when EVT_WORLD_PROPERTY_CHANGED =>
                -- as the world is loaded, change the text to show the world's
                -- introduction message
                if A_World_Property_Event(evt).Get_Property_Name = "introduction" then
                    this.Set_Text( Cast_Unbounded_String( A_World_Property_Event(evt).Get_Value ) );
                end if;

            when others => null;

        end case;
    end Handle_Event;

    ----------------------------------------------------------------------------

    procedure On_Fade_Complete( this : not null access Loading_Screen'Class ) is
        fader : A_Game_Screen;
    begin
        -- start a fade-in and hide the loading screen while the curtain is down
        fader := Create_Fader_Screen( this.view, Black, Transparent, Game_Views.Keen.FADE_IN_TIME, Linear );
        A_Screen_Manager(this.parent).Add_Front( fader );
        this.Exit_Screen;
    end On_Fade_Complete;

    ----------------------------------------------------------------------------

    overriding
    procedure Tick( this : access Loading_Screen; time : Tick_Time ) is
        pragma Unreferenced( time );
        fader : A_Game_Screen;
    begin
        -- don't exit the screen unless it has been displayed for the minimum
        -- amount of time already.
        if this.loadEnded and Clock > this.timeToHide then
            if not this.success then
                -- on failure, exit without fading
                this.Exit_Screen;
            elsif not this.fading then
                -- begin fading out by pushing a fader screen on top
                -- exit the loading screen when the fader screen exits
                this.fading := True;
                fader := Create_Fader_Screen( this.view, Transparent, Black, Game_Views.Keen.FADE_OUT_TIME, Linear );
                fader.Exited.Connect( Slot( this, On_Fade_Complete'Access ) );
                A_Screen_Manager(this.parent).Add_Front( fader );
            end if;
        end if;
    end Tick;

end Widgets.Game_Screens.Message.Loading;
