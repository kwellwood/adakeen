--
-- Copyright (c) 2018-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Keyboard;                  use Allegro.Keyboard;
--with Debugging;                         use Debugging;
with Game_Views;                        use Game_Views;
with Gamepads;                          use Gamepads;
with Tiles;                             use Tiles;
with Values.Casting;                    use Values.Casting;
with Values.Strings;                    use Values.Strings;
with Widgets.Buttons.Pushes;            use Widgets.Buttons.Pushes;

package body Widgets.Game_Screens.Story is

    package Gamepad_Connections is new Signals.Gamepads.Connections(Story_Screen);
    use Gamepad_Connections;

    package Key_Connections is new Signals.Keys.Connections(Story_Screen);
    use Key_Connections;

    ----------------------------------------------------------------------------

    function Get_Page( this : not null access Story_Screen'Class; page : String ) return Icon_Type;

    procedure Set_Page( this : not null access Story_Screen'Class; page : Icon_Type );

    procedure On_Gamepad_Moved( this    : not null access Story_Screen'Class;
                                gamepad : Gamepad_Axis_Arguments );

    procedure On_Escape( this : not null access Story_Screen'Class );

    procedure Show_Next( this : not null access Story_Screen'Class );

    procedure Show_Prev( this : not null access Story_Screen'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Story_Screen( view      : not null access Game_Views.Game_View'Class;
                                  style     : String;
                                  firstPage : Icon_Type ) return A_Game_Screen is
        this : A_Game_Screen := new Story_Screen;
    begin
        Story_Screen(this.all).Construct( view, style, firstPage );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Story_Screen;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this      : access Story_Screen;
                         view      : not null access Game_Views.Game_View'Class;
                         style     : String;
                         firstPage : Icon_Type ) is
    begin
        Game_Screen(this.all).Construct( view, "", isOpaque => True );
        this.Set_Style( style );

        this.content := Create_Label( view );
        this.Add_Child( this.content );
        this.content.Set_Icon( firstPage );
        this.content.Horizontal_Center.Attach( this.Horizontal_Center, firstPage.Get_Tile.Get_OffsetX );
        this.content.Vertical_Center.Attach( this.Vertical_Center, firstPage.Get_Tile.Get_OffsetY );

        this.btnNext := Create_Push_Button( view );
        this.Add_Child( this.btnNext );
        this.btnNext.Set_Style( style );
        this.btnNext.Right.Attach( this.Right, 8.0 );
        this.btnNext.Bottom.Attach( this.Bottom, 7.0 );

        this.KeyPressed.Connect( Slot( this, On_Escape'Access, ALLEGRO_KEY_ESCAPE ) );
        this.GamepadPressed.Connect( Slot( this, On_Escape'Access, BUTTON_EAST ) );

        this.KeyPressed.Connect( Slot( this, Show_Next'Access, ALLEGRO_KEY_RIGHT ) );
        this.KeyPressed.Connect( Slot( this, Show_Next'Access, ALLEGRO_KEY_SPACE ) );
        this.KeyPressed.Connect( Slot( this, Show_Next'Access, ALLEGRO_KEY_ENTER ) );
        this.KeyPressed.Connect( Slot( this, Show_Next'Access, ALLEGRO_KEY_LCTRL ) );
        this.KeyPressed.Connect( Slot( this, Show_Next'Access, ALLEGRO_KEY_RCTRL ) );
        this.KeyPressed.Connect( Slot( this, Show_Next'Access, ALLEGRO_KEY_ALT   ) );
        this.KeyPressed.Connect( Slot( this, Show_Next'Access, ALLEGRO_KEY_ALTGR ) );
        this.GamepadPressed.Connect( Slot( this, Show_Next'Access, BUTTON_SOUTH ) );

        this.KeyPressed.Connect( Slot( this, Show_Prev'Access, ALLEGRO_KEY_LEFT ) );
        this.GamepadMoved.Connect( Slot( this, On_Gamepad_Moved'Access, AXIS_LXN ) );
        this.GamepadMoved.Connect( Slot( this, On_Gamepad_Moved'Access, AXIS_LXP ) );

        this.Set_Page( firstPage );
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Page( this : not null access Story_Screen'Class; page : String ) return Icon_Type is
        content : constant Icon_Type := this.content.Get_Icon;
        attr    : constant Value := content.Get_Tile.Get_Attribute( page );
    begin
        if attr.Is_Number then
            return Create_Icon( content.Get_Library, attr.To_Int );
        elsif attr.Is_String then
            return Create_Icon( content.Get_Library, attr.Str.To_String );
        end if;
        return NO_ICON;
    end Get_Page;

    ----------------------------------------------------------------------------

    procedure On_Escape( this : not null access Story_Screen'Class ) is
    begin
        -- exit the screen if:
        -- 1. "exit" is true on this page (default = true)
        -- 2. and the minimum delay has expired
        if Cast_Boolean( this.content.Get_Icon.Get_Tile.Get_Attribute( "exit" ), True ) then
            if this.enableDelay <= Time_Span_Zero then
                this.Exit_Screen;
            end if;
        end if;
    end On_Escape;

    ----------------------------------------------------------------------------

    procedure On_Gamepad_Moved( this    : not null access Story_Screen'Class;
                                gamepad : Gamepad_Axis_Arguments ) is
    begin
        if gamepad.position < -0.5 then
            this.Show_Prev;
        elsif gamepad.position > 0.5 then
            this.Show_Next;
        end if;
    end On_Gamepad_Moved;

    ----------------------------------------------------------------------------

    procedure Set_Page( this : not null access Story_Screen'Class; page : Icon_Type ) is
        tile     : A_Tile;
        minDelay : Value;
        maxDelay : Value;
    begin
        if page /= NO_ICON then
            this.content.Set_Icon( page );

            tile := page.Get_Tile;
            this.content.Horizontal_Center.Set_Margin( tile.Get_OffsetX );
            this.content.Vertical_Center.Set_Margin( tile.Get_OffsetY );

            minDelay := tile.Get_Attribute( "minDelay" );
            this.enableDelay := (if minDelay.Is_Number then Milliseconds( minDelay.To_Int ) else Milliseconds( tile.Get_Delay ));

            maxDelay := tile.Get_Attribute( "maxDelay" );
            this.skipDelay := (if maxDelay.Is_Number then Milliseconds( maxDelay.To_Int ) else Milliseconds( tile.Get_Delay ));

            this.btnNext.Visible.Set( this.Get_Page( "next" ) /= NO_ICON and this.enableDelay = Time_Span_Zero );
        end if;
    end Set_Page;

    ----------------------------------------------------------------------------

    procedure Show_Next( this : not null access Story_Screen'Class ) is
    begin
        if this.enableDelay <= Time_Span_Zero then
            this.Set_Page( this.Get_Page( "next" ) );
        end if;
    end Show_Next;

    ----------------------------------------------------------------------------

    procedure Show_Prev( this : not null access Story_Screen'Class ) is
    begin
        if this.enableDelay <= Time_Span_Zero then
            this.Set_Page( this.Get_Page( "prev" ) );
        end if;
    end Show_Prev;

    ----------------------------------------------------------------------------

    overriding
    procedure Tick( this : access Story_Screen; time : Tick_Time ) is
    begin
        if this.enableDelay > Time_Span_Zero then
            this.enableDelay := this.enableDelay - time.elapsed;
            if this.enableDelay <= Time_Span_Zero then
                this.enableDelay := Time_Span_Zero;
                this.btnNext.Visible.Set( this.Get_Page( "next" ) /= NO_ICON );
            end if;
        end if;

        if this.skipDelay > Time_Span_Zero then
            this.skipDelay := this.skipDelay - time.elapsed;
            if this.skipDelay <= Time_Span_Zero then
                this.skipDelay := Time_Span_Zero;
                this.Show_Next;
            end if;
        end if;
    end Tick;

end Widgets.Game_Screens.Story;

