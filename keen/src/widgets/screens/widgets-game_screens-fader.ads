--
-- Copyright (c) 2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Widgets.Game_Screens.Fader is

    -- A fader screen fades from one color to another, over a period of time and
    -- with a customizable easing function, before exiting.
    type Fader_Screen is new Game_Screen with private;

    -- Creates a new fader screen.
    function Create_Fader_Screen( view      : not null access Game_Views.Game_View'Class;
                                  fromColor : Allegro_Color;
                                  toColor   : Allegro_Color;
                                  duration  : Time_Span;
                                  easing    : Easing_Type ) return A_Game_Screen;

private

    type Fader_Screen is new Game_Screen with
        record
            propColor : aliased Animated_Color.Property;
        end record;
    type A_Fader_Screen is access all Fader_Screen'Class;

    procedure Construct( this      : access Fader_Screen;
                         view      : not null access Game_Views.Game_View'Class;
                         fromColor : Allegro_Color;
                         toColor   : Allegro_Color;
                         duration  : Time_Span;
                         easing    : Easing_Type );

end Widgets.Game_Screens.Fader;
