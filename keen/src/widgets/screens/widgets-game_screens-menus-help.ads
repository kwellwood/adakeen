--
-- Copyright (c) 2018-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Widgets.Game_Screens.Menus.Help is

    -- The help menu screen represents Keen's help menu, accessible from the
    -- main menu. It offers game play information, story and credits.
    type Help_Menu is new Menu_Screen with private;

    -- Creates a new help menu screen.
    function Create_Help_Menu( view : not null access Game_Views.Game_View'Class ) return A_Game_Screen;

private

    type Help_Menu is new Menu_Screen with
        record
            creditsOpenable : Time;       -- limits how frequently credits are opened
        end record;

    procedure Construct( this : access Help_Menu;
                         view : not null access Game_Views.Game_View'Class );

end Widgets.Game_Screens.Menus.Help;
