--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Keyboard;                  use Allegro.Keyboard;
--with Debugging;                         use Debugging;
with Directives;                        use Directives;
with Directives.Galaxy;
with Events.Audio;                      use Events.Audio;
with Events.Entities;                   use Events.Entities;
with Events.Entities.Camera_Targets;    use Events.Entities.Camera_Targets;
with Events.Game;                       use Events.Game;
with Events.World;                      use Events.World;
with Gamepads;                          use Gamepads;
with Game_Views;                        use Game_Views;
with Values.Errors;                     use Values.Errors;
with Widgets.Game_Screens.Menus.Main;   use Widgets.Game_Screens.Menus.Main;
with Widgets.Labels;                    use Widgets.Labels;
with Widgets.Scenes.Keen;               use Widgets.Scenes.Keen;
with Widgets.Screen_Managers;           use Widgets.Screen_Managers;

package body Widgets.Game_Screens.Game_Play is

    CONSOLE_HEIGHT     : constant := 120.0;     -- pixels
    CONSOLE_SLIDE_TIME : constant Time_Span := Milliseconds( 200 );

    package Connections is new Signals.Connections(Game_Play_Screen);
    use Connections;

    package Gamepad_Connections is new Signals.Gamepads.Connections(Game_Play_Screen);
    use Gamepad_Connections;

    package Key_Connections is new Signals.Keys.Connections(Game_Play_Screen);
    use Key_Connections;

    ----------------------------------------------------------------------------

    -- Called when the Escape key is pressed to return to the main menu.
    procedure Action_Back_To_Menu( this : not null access Game_Play_Screen'Class );

    -- Called when the CONSOLE or MENU key is pressed in the console to close it.
    procedure Action_Console_Close( this : not null access Game_Play_Screen'Class );

    -- Called when the CONSOLE key is pressed to open the console.
    procedure Action_Console_Open( this : not null access Game_Play_Screen'Class );

    -- Called when the PROGRESS key is pressed to toggle the progress board's open state.
    procedure Action_Progress_Toggle( this : not null access Game_Play_Screen'Class );

    -- Creates, initializes and returns a new Keen_Scene instance. It will not
    -- be added to the screen yet.
    function Create_Scene( this : not null access Game_Play_Screen'Class ) return A_Scene;

    -- Called when the Enter key is pressed in the console to execute a command.
    procedure Entered_Command( this : not null access Game_Play_Screen'Class );

    -- Gives focus to the active control: either console or scene.
    procedure Give_Controls_Focus( this : not null access Game_Play_Screen'Class );

    -- Called when the Follow_Entity event is received for the scene to track
    -- the player entity.
    procedure Handle_Follow_Entity( this : not null access Game_Play_Screen'Class;
                                    evt  : not null A_Entity_Event );

    -- Called when a script started from the console is complete, reporting the
    -- result.
    procedure Handle_Script_Done( this : not null access Game_Play_Screen'Class;
                                  evt  : not null A_Script_Event );

    -- Handles World_Loaded events to begin ignoring any keys being held down.
    -- This prevents in advertent actions on play resuming when a player is
    -- holds down a key during the load screen.
    procedure Handle_World_Loaded( this : not null access Game_Play_Screen'Class;
                                   evt  : not null A_World_Loaded_Event );

    -- Reads the controller bindings from the Game_View and updates all key and
    -- button signal connections.
    procedure Update_Bindings( this : not null access Game_Play_Screen'Class );

    -- The console finished sliding open or closed.
    procedure On_Console_Moved( this : not null access Game_Play_Screen'Class );

    -- This is attached to the Game_View.GameStarted signal.
    procedure On_Game_Started( this : not null access Game_Play_Screen'Class );

    -- Called when a gamepad button is pressed.
    procedure On_Gamepad_Pressed( this    : not null access Game_Play_Screen'Class;
                                  gamepad : Gamepad_Button_Arguments );

    -- Called when a gamepad axis position has changed.
    procedure On_Gamepad_Moved( this    : not null access Game_Play_Screen'Class;
                                gamepad : Gamepad_Axis_Arguments );

    -- Called when a gamepad button is released.
    procedure On_Gamepad_Released( this    : not null access Game_Play_Screen'Class;
                                   gamepad : Gamepad_Button_Arguments );

    -- This is attached to the Game_View.LoadingBegan signal.
    procedure On_Loading_Began( this : not null access Game_Play_Screen'Class );

    -- This is attached to the Game_View.LoadingFailed signal.
    procedure On_Loading_Failed( this : not null access Game_Play_Screen'Class );

    -- This is attached to the Game_View.PausedChanged signal.
    procedure On_Paused_Changed( this : not null access Game_Play_Screen'Class );

    -- This is called when the progress board closes.
    procedure On_Progress_Closed( this : not null access Game_Play_Screen'Class );

    -- This is called when the progress board opens.
    procedure On_Progress_Opened( this : not null access Game_Play_Screen'Class );

    -- Called when a scene control key is pressed.
    procedure On_Scene_Key_Pressed( this : not null access Game_Play_Screen'Class;
                                    key  : Key_Arguments );

    -- Called when a registered scene key is released.
    procedure On_Scene_Key_Released( this : not null access Game_Play_Screen'Class;
                                     key  : Key_Arguments );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Game_Play_Screen( view : not null access Game_Views.Game_View'Class ) return A_Game_Play_Screen is
        this : A_Game_Play_Screen := new Game_Play_Screen;
    begin
        this.Construct( view );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Game_Play_Screen;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Game_Play_Screen;
                         view : not null access Game_Views.Game_View'Class ) is
        label : A_Label;
    begin
        Game_Screen(this.all).Construct( view, "", isOpaque => True );
        this.Focused.Connect( Slot( this, Give_Controls_Focus'Access ) );

        this.Get_View.PausedChanged.Connect( Slot( this, On_Paused_Changed'Access ) );
        this.Get_View.GameStarted.Connect( Slot( this, On_Game_Started'Access ) );
        this.Get_View.LoadingBegan.Connect( Slot( this, On_Loading_Began'Access ) );
        this.Get_View.LoadingFailed.Connect( Slot( this, On_Loading_Failed'Access ) );
        this.Get_View.BindingsChanged.Connect( Slot( this, Update_Bindings'Access ) );

        this.Listen_For_Event( EVT_FOLLOW_ENTITY );
        this.Listen_For_Event( EVT_WORLD_LOADED );
        this.Listen_For_Event( EVT_SCRIPT_DONE );

        -- todo: this really should be 'fullscreen' style because there is
        -- nothing behind the scene, but the Z-depth of the scene layers cause
        -- it to be drawn behind the background, so you can't see anything.
        this.Set_Style( "popup" );

        -- Create child widgets --

        -- scoreboard
        this.scoreboard := Create_Scoreboard( view );
        this.Add_Child( this.scoreboard );
        this.scoreboard.Set_Style( "scene" );
        --this.scoreboard.Set_Next( this.scene.Get_Id );
        this.scoreboard.X.Set( 7.0 );
        this.scoreboard.Y.Set( 4.0 );
        -- widget size is determined by the scoreboard image
        this.scoreboard.Visible.Set( False );

        -- paused dialog
        this.pnlPaused := Create_Panel( view );
        this.Add_Child( this.pnlPaused );
        this.pnlPaused.Set_Style( "dialog" );
        --this.pnlPaused.Set_Next( this.scene.Get_Id );
        this.pnlPaused.Center( this, 64.0, 40.0 );
        this.pnlPaused.Visible.Set( False );

            -- paused text label
            label := Create_Label( view );
            this.pnlPaused.Add_Widget( label );
            label.Set_Text( "Paused" );
            label.Set_Style( "dialog" );
            label.Center( label.Get_Parent );

        -- drop down progress board
        -- progress board (popup) (with transition)
        this.progressBoard := Create_Progress_Board( view );
        this.Add_Child( this.progressBoard );
        this.progressBoard.Set_Style( "scene" );
        --this.progressBoard.Set_Next( this.scene.Get_Id );
        this.progressBoard.Closed.Connect( Slot( this, On_Progress_Closed'Access ) );
        this.progressBoard.Opened.Connect( Slot( this, On_Progress_Opened'Access ) );
        this.progressBoard.Center_Horizontal( this.progressBoard.Get_Parent );
        this.progressBoard.Height.Set( this.progressBoard.Get_Min_Height );
        this.progressBoard.Y.Set( -this.progressBoard.Height.Get );

        -- Scribble console window
        this.console := Create_Panel( view );
        this.Add_Child( this.console );
        this.console.Set_Style( "console" );
        this.console.Fill_Horizontal( this );
        this.console.Height.Set( CONSOLE_HEIGHT );
        this.console.Y.Set( -CONSOLE_HEIGHT );
        this.console.Y.Finished.Connect( Slot( this, On_Console_Moved'Access ) );
        this.console.Visible.Set( False );

            this.consoleLog := Create_Textbox( view );
            this.console.Add_Widget( this.consoleLog );
            this.consoleLog.Set_Style( "console" );
            this.consoleLog.Fill( this.console, 1.0, 1.0, 1.0, 17.0 );

            label := Create_Label( view );
            this.console.Add_Widget( label );
            label.Set_Style( "console" );
            label.Set_Text( ">" );
            label.Left.Attach( label.Get_Parent.Left, 1.0 );
            label.Bottom.Attach( label.Get_Parent.Bottom, 1.0 );
            label.Width.Set( 14.0 );
            label.Height.Set( 14.0 );

            this.consoleInput := Create_Input_Box( view );
            this.console.Add_Widget( this.consoleInput );
            this.consoleInput.Set_Style( "console" );
            --this.consoleInput.Set_Next( this.scene.Get_Id );
            this.consoleInput.Left.Attach( label.Right );
            this.consoleInput.Right.Attach( this.consoleInput.Get_Parent.Right, 1.0 );
            this.consoleInput.Top.Attach( label.Top );
            this.consoleInput.Bottom.Attach( label.Bottom );
            this.consoleInput.Enable_History( True );
            this.consoleInput.Accepted.Connect( Slot( this, Entered_Command'Access ) );

        -- gameplay scene
        this.scene := this.Create_Scene;
        this.Add_Child( this.scene );
        this.scene.Fill( this );
        this.scene.Send_To_Back;

        -- background loading scene
        this.scene2 := this.Create_Scene;

        for i in SCENE_INPUTS'Range loop
            this.controls(i).action := SCENE_INPUTS(i);
        end loop;

        this.Update_Bindings;
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Game_Play_Screen ) is
    begin
        -- .scene2 is not attached to the widget tree so it must be deleted here
        Delete( A_Widget(this.scene2) );

        Game_Screen(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Action_Back_To_Menu( this : not null access Game_Play_Screen'Class ) is
    begin
        -- close the progress board before returning to the menu
        if this.progressBoard.Is_Open then
            this.progressBoard.Toggle;
            return;
        end if;

        A_Keen_View(this.Get_View).Pause_By_Menu( enabled => True );
        A_Screen_Manager(this.parent).Add_Front( Create_Main_Menu_Screen( this.Get_View, gameInProgress => True ) );
    end Action_Back_To_Menu;

    ----------------------------------------------------------------------------

    procedure Action_Console_Close( this : not null access Game_Play_Screen'Class ) is
    begin
        if not this.console.Visible.Animating then
            -- begin closing console
            this.consoleInput.Set_Text( "" );
            this.console.Y.Set( -CONSOLE_HEIGHT, CONSOLE_SLIDE_TIME, Quad_In );
            this.console.Visible.Set( False, CONSOLE_SLIDE_TIME, Impulse_In );
            Queue_Play_Sound( "menu_close" );
        end if;
    end Action_Console_Close;

    ----------------------------------------------------------------------------

    procedure Action_Console_Open( this : not null access Game_Play_Screen'Class ) is
    begin
        if not this.console.Visible.Animating then
            -- begin opening console
            this.console.Visible.Set( True, CONSOLE_SLIDE_TIME, Impulse_Out );
            this.console.Y.Set( 0.0, CONSOLE_SLIDE_TIME, Quad_Out );
            Queue_Play_Sound( "menu_open" );
        end if;
    end Action_Console_Open;

    ----------------------------------------------------------------------------

    procedure Action_Progress_Toggle( this : not null access Game_Play_Screen'Class ) is
    begin
        this.progressBoard.Toggle;
    end Action_Progress_Toggle;

    ----------------------------------------------------------------------------

    function Create_Scene( this : not null access Game_Play_Screen'Class ) return A_Scene is
        result : A_Scene;
    begin
        result := Create_Scene( this.view );
        result.Set_Style( "scene" );
        result.Set_Enabled( False );   -- don't receive load events until ready
        return result;
    end Create_Scene;

    ----------------------------------------------------------------------------

    procedure Entered_Command( this : not null access Game_Play_Screen'Class ) is
    begin
        if this.consoleInput.Get_Text'Length = 0 then
            return;
        end if;

        Queue_Run_Script( To_Unbounded_String( this.consoleInput.Get_Text ), this.nextTaskId );
        this.nextTaskId := this.nextTaskId + 1;
        this.consoleInput.Set_Text( "" );
    end Entered_Command;

    ----------------------------------------------------------------------------

    procedure Give_Controls_Focus( this : not null access Game_Play_Screen'Class ) is
    begin
        if this.consoleInput.Is_Visible then
            this.consoleInput.Request_Focus;
        else
            this.scene.Request_Focus;
        end if;
    end Give_Controls_Focus;

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Event( this : access Game_Play_Screen;
                            evt  : in out A_Event;
                            resp : out Response_Type ) is
        pragma Unreferenced( resp );
    begin
        case evt.Get_Event_Id is
            when EVT_FOLLOW_ENTITY => this.Handle_Follow_Entity( A_Entity_Event(evt) );
            when EVT_WORLD_LOADED  => this.Handle_World_Loaded( A_World_Loaded_Event(evt) );
            when EVT_SCRIPT_DONE   => this.Handle_Script_Done( A_Script_Event(evt) );
            when others            => null;
        end case;
    end Handle_Event;

    ----------------------------------------------------------------------------

    procedure Handle_Follow_Entity( this : not null access Game_Play_Screen'Class;
                                    evt  : not null A_Entity_Event ) is
    begin
        this.player := evt.Get_Id;
    end Handle_Follow_Entity;

    ----------------------------------------------------------------------------

    procedure Handle_Script_Done( this : not null access Game_Play_Screen'Class;
                                  evt  : not null A_Script_Event ) is
        val : constant Value := evt.Get_Result;
    begin
        if val.Is_Error then
            this.consoleLog.Append_Line( val.Err.Get_Code'Img & ": " & val.Err.Get_Message );
        else
            this.consoleLog.Append_Line( val.Image );
        end if;
    end Handle_Script_Done;

    ----------------------------------------------------------------------------

    procedure Handle_World_Loaded( this : not null access Game_Play_Screen'Class;
                                   evt  : not null A_World_Loaded_Event ) is
        pragma Unreferenced( evt );
    begin
        -- begin ignoring all action keys until the next press event
        for c of this.controls loop
            c.ignoreKey := True;
        end loop;
    end Handle_World_Loaded;

    ----------------------------------------------------------------------------

    procedure On_Console_Moved( this : not null access Game_Play_Screen'Class ) is
    begin
        if this.console.Y.Get >= 0.0 then
            -- opened
            this.consoleInput.Set_Text( "" );
            this.consoleInput.Request_Focus;
        else
            -- closed
            this.scene.Request_Focus;
        end if;
    end On_Console_Moved;

    ----------------------------------------------------------------------------

    procedure On_Game_Started( this : not null access Game_Play_Screen'Class ) is
    begin
        this.pnlPaused.Visible.Set( False );
        this.scene.Unload_World;
        this.scene2.Unload_World;
        this.scene.Set_Enabled( False );
        this.scene2.Set_Enabled( False );
        this.scoreboard.Visible.Set( False );
    end On_Game_Started;

    ----------------------------------------------------------------------------

    procedure On_Gamepad_Pressed( this    : not null access Game_Play_Screen'Class;
                                  gamepad : Gamepad_Button_Arguments ) is
    begin
        for c of this.controls loop
            if c.binding.button = gamepad.button then
                case c.action is
                    when INPUT_JUMP     => Queue_Entity_Directive( this.player, Directives.Galaxy.JUMP, Ongoing );
                    when INPUT_SHOOT    => Queue_Entity_Directive( this.player, Directives.Galaxy.SHOOT, Once );
                    when INPUT_POGO     => Queue_Entity_Directive( this.player, Directives.Galaxy.POGO, Once );
                    when INPUT_PAUSE    => A_Keen_View(this.Get_View).Pause_By_Player( not this.Get_View.Is_Paused );
                    when INPUT_PROGRESS => this.Action_Progress_Toggle;
                    when INPUT_CONSOLE  => this.Action_Console_Open;
                    when others => null;
                end case;
                exit;
            end if;
        end loop;
    end On_Gamepad_Pressed;

    ----------------------------------------------------------------------------

    procedure On_Gamepad_Moved( this    : not null access Game_Play_Screen'Class;
                                gamepad : Gamepad_Axis_Arguments ) is
    begin
        for c of this.controls loop
            if c.binding.axis = gamepad.axis then
                case c.action is
                    when INPUT_LEFT | INPUT_RIGHT =>
                        if this.stickCenteredX then
                            if gamepad.position < -AXIS_SELECTED_THRESHOLD then
                                Queue_Entity_Directive( this.player, Directives.Galaxy.LEFT, Ongoing );
                            elsif gamepad.position > AXIS_SELECTED_THRESHOLD then
                                Queue_Entity_Directive( this.player, Directives.Galaxy.RIGHT, Ongoing );
                            end if;
                        end if;
                        if (abs gamepad.position) > AXIS_SELECTED_THRESHOLD then
                            this.stickCenteredX := False;
                        elsif (abs gamepad.position) < AXIS_CENTERED_THRESHOLD then
                            if not this.stickCenteredX then
                                Queue_Entity_Directive( this.player, Directives.Galaxy.LEFT, Inactive );
                                Queue_Entity_Directive( this.player, Directives.Galaxy.RIGHT, Inactive );
                                this.stickCenteredX := True;
                            end if;
                        end if;

                    when INPUT_UP | INPUT_DOWN =>
                        if this.stickCenteredY then
                            if gamepad.position < -AXIS_SELECTED_THRESHOLD then
                                Queue_Entity_Directive( this.player, Directives.Galaxy.UP, Ongoing );
                            elsif gamepad.position > AXIS_SELECTED_THRESHOLD then
                                Queue_Entity_Directive( this.player, Directives.Galaxy.DOWN, Ongoing );
                            end if;
                        end if;
                        if (abs gamepad.position) > AXIS_SELECTED_THRESHOLD then
                            this.stickCenteredY := False;
                        elsif (abs gamepad.position) < AXIS_CENTERED_THRESHOLD then
                            if not this.stickCenteredY then
                                Queue_Entity_Directive( this.player, Directives.Galaxy.UP, Inactive );
                                Queue_Entity_Directive( this.player, Directives.Galaxy.DOWN, Inactive );
                                this.stickCenteredY := True;
                            end if;
                        end if;

                    when others => null;
                end case;
                exit;
            end if;
        end loop;
    end On_Gamepad_Moved;

    ----------------------------------------------------------------------------

    procedure On_Gamepad_Released( this    : not null access Game_Play_Screen'Class;
                                   gamepad : Gamepad_Button_Arguments ) is
    begin
        for c of this.controls loop
            if c.binding.button = gamepad.button then
                case c.action is
                    when INPUT_JUMP => Queue_Entity_Directive( this.player, Directives.Galaxy.JUMP, Inactive );
                    when others => null;
                end case;
                exit;
            end if;
        end loop;
    end On_Gamepad_Released;

    ----------------------------------------------------------------------------

    procedure On_Loading_Began( this : not null access Game_Play_Screen'Class ) is
    begin
        this.scene.Set_Enabled( False );       -- freeze existing world
        this.scene2.Set_Enabled( True );       -- receive new world
        this.scene2.Unload_World;
    end On_Loading_Began;

    ----------------------------------------------------------------------------

    procedure On_Loading_Failed( this : not null access Game_Play_Screen'Class ) is
    begin
        this.scene2.Unload_World;
        this.scene2.Set_Enabled( False );
        this.scene.Set_Enabled( True );
    end On_Loading_Failed;

    ----------------------------------------------------------------------------

    procedure On_Paused_Changed( this : not null access Game_Play_Screen'Class ) is
    begin
        this.pnlPaused.Visible.Set( this.Get_View.Is_Paused );
    end On_Paused_Changed;

    ----------------------------------------------------------------------------

    procedure On_Progress_Closed( this : not null access Game_Play_Screen'Class ) is
    begin
        A_Keen_View(this.Get_View).Pause_By_Menu( enabled => False );
    end On_Progress_Closed;

    ----------------------------------------------------------------------------

    procedure On_Progress_Opened( this : not null access Game_Play_Screen'Class ) is
    begin
        A_Keen_View(this.Get_View).Pause_By_Menu( enabled => True );
    end On_Progress_Opened;

    ----------------------------------------------------------------------------

    procedure On_Scene_Key_Pressed( this : not null access Game_Play_Screen'Class;
                                    key  : Key_Arguments ) is
    begin
        for c of this.controls loop
            if c.binding.keycode = key.code then
                -- signals from a key are only ignored until it's pressed again
                c.ignoreKey := False;
                case c.action is
                    when INPUT_LEFT     => Queue_Entity_Directive( this.player, Directives.Galaxy.LEFT, Ongoing );
                    when INPUT_RIGHT    => Queue_Entity_Directive( this.player, Directives.Galaxy.RIGHT, Ongoing );
                    when INPUT_UP       => Queue_Entity_Directive( this.player, Directives.Galaxy.UP, Ongoing );
                    when INPUT_DOWN     => Queue_Entity_Directive( this.player, Directives.Galaxy.DOWN, Ongoing );
                    when INPUT_JUMP     => Queue_Entity_Directive( this.player, Directives.Galaxy.JUMP, Ongoing );
                    when INPUT_POGO     => Queue_Entity_Directive( this.player, Directives.Galaxy.POGO, Once );
                    when INPUT_SHOOT    => Queue_Entity_Directive( this.player, Directives.Galaxy.SHOOT, Once );
                    when INPUT_PAUSE    => A_Keen_View(this.Get_View).Pause_By_Player( not this.Get_View.Is_Paused );
                    when INPUT_PROGRESS => this.Action_Progress_Toggle;
                    when INPUT_CONSOLE  => this.Action_Console_Open;
                    when others => null;
                end case;
                exit;
            end if;
        end loop;
    end On_Scene_Key_Pressed;

    ----------------------------------------------------------------------------

    procedure On_Scene_Key_Released( this : not null access Game_Play_Screen'Class;
                                     key  : Key_Arguments ) is
    begin
        -- if the key activates an on-going directive, cancel the directive on
        -- key release. if the game is paused then these directives will be
        -- ignored.
        for c of this.controls loop
            if c.binding.keycode = key.code then
                if not c.ignoreKey then
                    case c.action is
                        when INPUT_LEFT  => Queue_Entity_Directive( this.player, Directives.Galaxy.LEFT, Inactive );
                        when INPUT_RIGHT => Queue_Entity_Directive( this.player, Directives.Galaxy.RIGHT, Inactive );
                        when INPUT_UP    => Queue_Entity_Directive( this.player, Directives.Galaxy.UP, Inactive );
                        when INPUT_DOWN  => Queue_Entity_Directive( this.player, Directives.Galaxy.DOWN, Inactive );
                        when INPUT_JUMP  => Queue_Entity_Directive( this.player, Directives.Galaxy.JUMP, Inactive );
                        when others => null;
                    end case;
                end if;
                exit;
            end if;
        end loop;
    end On_Scene_Key_Released;

    ----------------------------------------------------------------------------

    procedure Swap_Scenes( this : not null access Game_Play_Screen'Class ) is
        s : A_Scene;
    begin
        if this.scene.Is_Map_Loaded and not this.scene2.Is_Map_Loaded then
            -- there's no new loaded map to swap, so don't do anything. loading
            -- likely failed.
            this.On_Loading_Failed;
            return;
        end if;

        -- remove scene from the widget tree so it can be swapped out. initially
        -- it is not a child of anything.
        if this.scene.Get_Parent /= null then
            this.scene.Visible.Set( False );
            this.Remove_Child( A_Widget(this.scene) );
        end if;

        -- swap .scene with .scene2
        s := this.scene;
        this.scene := this.scene2;
        this.scene2 := s;

        -- unload the scene's previous world and disable it while it waits in
        -- the background until the next load.
        this.scene2.Unload_World;
        this.scene2.Set_Enabled( False );

        -- hook up the scene that was previously the background scene and enable
        -- its loaded world for play.
        this.Add_Child( this.scene );
        this.scene.Fill( this );
        this.scene.Send_To_Back;
        this.scene.Visible.Set( True );
        this.scene.Set_Enabled( True );

        -- make sure the scoreboard is visible if a world has been loaded
        this.scoreboard.Visible.Set( this.scene.Is_Map_Loaded );

        this.consoleInput.Set_Next( this.scene.Get_Id );
        this.progressBoard.Set_Next( this.scene.Get_Id );
        this.pnlPaused.Set_Next( this.scene.Get_Id );
        this.scoreboard.Set_Next( this.scene.Get_Id );
    end Swap_Scenes;

    ----------------------------------------------------------------------------

    procedure Update_Bindings( this : not null access Game_Play_Screen'Class ) is

        ------------------------------------------------------------------------

        procedure Remove_Bindings( scene : A_Scene ) is
        begin
            scene.GamepadPressed.Disconnect_All( this );
            scene.GamepadReleased.Disconnect_All( this );
            scene.GamepadMoved.Disconnect_All( this );
            scene.KeyPressed.Disconnect_All( this );
            scene.KeyReleased.Disconnect_All( this );
        end Remove_Bindings;

        ------------------------------------------------------------------------

        procedure Add_Bindings( scene : A_Scene ) is
        begin
            for c of this.controls loop
                if this.view.Get_Binding( c.action ).button /= BUTTON_NONE and
                   this.view.Get_Binding( c.action ).button /= BUTTON_ANY
                then
                    scene.GamepadPressed.Connect( Slot( this, On_Gamepad_Pressed'Access, this.view.Get_Binding( c.action ).button ) );
                    scene.GamepadReleased.Connect( Slot( this, On_Gamepad_Released'Access, this.view.Get_Binding( c.action ).button ) );
                end if;
                if this.view.Get_Binding( c.action ).axis /= AXIS_NONE and
                   this.view.Get_Binding( c.action ).axis /= AXIS_ANY
                then
                    scene.GamepadMoved.Connect( Slot( this, On_Gamepad_Moved'Access, this.view.Get_Binding( c.action ).axis ) );
                end if;
                if this.view.Get_Binding( c.action ).keycode > 0 then
                    scene.KeyPressed.Connect( Slot( this, On_Scene_Key_Pressed'Access, this.view.Get_Binding( c.action ).keycode ) );
                    scene.KeyReleased.Connect( Slot( this, On_Scene_Key_Released'Access, this.view.Get_Binding( c.action ).keycode ) );
                end if;
            end loop;
        end Add_Bindings;

        ------------------------------------------------------------------------

    begin
        this.KeyPressed.Disconnect_All( this );
        this.GamepadPressed.Disconnect_All( this );

        this.consoleInput.KeyPressed.Disconnect_All( this );
        this.consoleInput.GamepadPressed.Disconnect_All( this );

        Remove_Bindings( this.scene );
        Remove_Bindings( this.scene2 );

        -- - - - - - - - -

        for c of this.controls loop
            c.binding := this.view.Get_Binding( c.action );
            c.ignoreKey := False;
        end loop;

        -- - - - - - - - -

        this.KeyPressed.Connect( Slot( this, Action_Back_To_Menu'Access, this.view.Get_Binding( INPUT_BACK ).keycode ) );
        this.GamepadPressed.Connect( Slot( this, Action_Back_To_Menu'Access, this.view.Get_Binding( INPUT_BACK ).button ) );

        this.consoleInput.KeyPressed.Connect( Slot( this, Action_Console_Close'Access, ALLEGRO_KEY_TAB ) );
        this.consoleInput.KeyPressed.Connect( Slot( this, Action_Console_Close'Access, this.view.Get_Binding( INPUT_BACK ).keycode ) );
        this.consoleInput.KeyPressed.Connect( Slot( this, Action_Console_Close'Access, this.view.Get_Binding( INPUT_CONSOLE ).keycode ) );
        this.consoleInput.GamepadPressed.Connect( Slot( this, Action_Console_Close'Access, this.view.Get_Binding( INPUT_BACK ).button ) );
        this.consoleInput.GamepadPressed.Connect( Slot( this, Action_Console_Close'Access, this.view.Get_Binding( INPUT_CONSOLE ).button ) );

        Add_Bindings( this.scene );
        Add_Bindings( this.scene2 );
    end Update_Bindings;

end Widgets.Game_Screens.Game_Play;
