--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Game_Views.Keen;                   use Game_Views.Keen;
with Widgets.Game_Screens.Menus.Config; use Widgets.Game_Screens.Menus.Config;
with Widgets.Game_Screens.Menus.Help;   use Widgets.Game_Screens.Menus.Help;
with Widgets.Game_Screens.Paddle_War;   use Widgets.Game_Screens.Paddle_War;
with Widgets.Screen_Managers;           use Widgets.Screen_Managers;

package body Widgets.Game_Screens.Menus.Main is

    package Connections is new Signals.Connections(Main_Menu_Screen);
    use Connections;

    ----------------------------------------------------------------------------

    -- Called when the user attempts to open the configure menu.
    procedure Clicked_Configure( this : not null access Main_Menu_Screen'Class );

    -- Called when the user selects the 'Help' menu option.
    procedure Clicked_Help( this : not null access Main_Menu_Screen'Class );

    -- Called when the user attempts to load a saved game.
    procedure Clicked_Load_Game( this : not null access Main_Menu_Screen'Class );

    -- Called when the user attempts to start a new game.
    procedure Clicked_New_Game( this : not null access Main_Menu_Screen'Class );

    -- Called when the user attempts to open the paddle war game.
    procedure Clicked_Paddle_War( this : not null access Main_Menu_Screen'Class );

   -- Called when the user attempts to quit the game.
    procedure Clicked_Quit( this : not null access Main_Menu_Screen'Class );

    -- Called when the user attempts to resume a game in progress.
    procedure Clicked_Resume_Game( this : not null access Main_Menu_Screen'Class );

    -- Called when the user attempts to save the game in progress.
    procedure Clicked_Save_Game( this : not null access Main_Menu_Screen'Class );

    function Get_Keen_View( this : not null access Main_Menu_Screen'Class ) return A_Keen_View is (A_Keen_View(this.Get_View));

    procedure On_Shown( this : not null access Main_Menu_Screen'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Main_Menu_Screen( view           : not null access Game_Views.Game_View'Class;
                                      gameInProgress : Boolean ) return A_Game_Screen is
        this : A_Game_Screen := new Main_Menu_Screen;
    begin
        Main_Menu_Screen(this.all).Construct( view, gameInProgress );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Main_Menu_Screen;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this           : access Main_Menu_Screen;
                         view           : not null access Game_Views.Game_View'Class;
                         gameInProgress : Boolean ) is
        button : A_Button;
    begin
        Menu_Screen(this.all).Construct( view, "", isOpaque => True );
        this.Shown.Connect( Slot( this, On_Shown'Access ) );
        this.gameInProgress := gameInProgress;

        this.Set_Background( Create_Icon( "interface:main_menu" ) );

        -- New Game button
        this.btnNew := this.Create_Menu_Button( "NEW GAME" );
        this.btnNew.Pressed.Connect( Slot( this, Clicked_New_Game'Access ) );
        this.Add_Item( this.btnNew );

        -- Load Game button
        button := this.Create_Menu_Button( "LOAD GAME" );
        button.Pressed.Connect( Slot( this, Clicked_Load_Game'Access ) );
        this.Add_Item( button );

        -- Save Game button
        button := this.Create_Menu_Button( "SAVE GAME" );
        button.Set_Enabled( this.gameInProgress );
        button.Pressed.Connect( Slot( this, Clicked_Save_Game'Access ) );
        this.Add_Item( button );

        -- Resume Game button
        this.btnResume := this.Create_Menu_Button( "BACK TO GAME" );
        this.btnResume.Set_Enabled( this.gameInProgress );
        this.btnResume.Pressed.Connect( Slot( this, Clicked_Resume_Game'Access ) );
        this.Add_Item( this.btnResume );

        -- Configure button
        button := this.Create_Menu_Button( "CONFIGURE" );
        button.Pressed.Connect( Slot( this, Clicked_Configure'Access ) );
        this.Add_Item( button );

        -- Paddle War button
        button := this.Create_Menu_Button( "PADDLE WAR" );
        button.Pressed.Connect( Slot( this, Clicked_Paddle_War'Access ) );
        this.Add_Item( button );

        -- Help button
        button := this.Create_Menu_Button( "HELP" );
        button.Pressed.Connect( Slot( this, Clicked_Help'Access ) );
        this.Add_Item( button );

        -- Quit button
        button := this.Create_Menu_Button( "QUIT" );
        button.Pressed.Connect( Slot( this, Clicked_Quit'Access ) );
        this.Add_Item( button );
    end Construct;

    ----------------------------------------------------------------------------

    procedure Clicked_Configure( this : not null access Main_Menu_Screen'Class ) is
    begin
        A_Screen_Manager(this.parent).Add_Front( Create_Config_Screen( this.Get_View ) );
    end Clicked_Configure;

    ----------------------------------------------------------------------------

    procedure Clicked_Help( this : not null access Main_Menu_Screen'Class ) is
    begin
        A_Screen_Manager(this.parent).Add_Front( Create_Help_Menu( this.Get_View ) );
    end Clicked_Help;

    ----------------------------------------------------------------------------

    procedure Clicked_Load_Game( this : not null access Main_Menu_Screen'Class ) is
    begin
        this.Get_Keen_View.Load_Saved_Game;
        this.Exit_Screen;
    end Clicked_Load_Game;

    ----------------------------------------------------------------------------

    procedure Clicked_New_Game( this : not null access Main_Menu_Screen'Class ) is
    begin
        this.Get_Keen_View.Start_New_Game;
        this.Exit_Screen;
    end Clicked_New_Game;

    ----------------------------------------------------------------------------

    procedure Clicked_Paddle_War( this : not null access Main_Menu_Screen'Class ) is
    begin
        A_Screen_Manager(this.parent).Add_Front( Create_Paddle_War_Screen( this.Get_View ) );
    end Clicked_Paddle_War;

    ----------------------------------------------------------------------------

    procedure Clicked_Quit( this : not null access Main_Menu_Screen'Class ) is
    begin
        this.Get_Keen_View.Quit_App;
    end Clicked_Quit;

    ----------------------------------------------------------------------------

    procedure Clicked_Resume_Game( this : not null access Main_Menu_Screen'Class ) is
    begin
        this.Exit_Screen;
        this.Get_Keen_View.Pause_By_Menu( enabled => False );
    end Clicked_Resume_Game;

    ----------------------------------------------------------------------------

    procedure Clicked_Save_Game( this : not null access Main_Menu_Screen'Class ) is
    begin
        this.Get_Keen_View.Save_Game_To_Disk;
    end Clicked_Save_Game;

    ----------------------------------------------------------------------------

    procedure On_Shown( this : not null access Main_Menu_Screen'Class ) is
    begin
        if this.gameInProgress then
            this.btnResume.Request_Focus;
        end if;
    end On_Shown;

end Widgets.Game_Screens.Menus.Main;
