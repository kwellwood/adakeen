--
-- Copyright (c) 2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Keyboard;                  use Allegro.Keyboard;
--with Debugging;                         use Debugging;
with Game_Views;                        use Game_Views;
with Game_Views.Keen;                   use Game_Views.Keen;
with Gamepads;                          use Gamepads;
with Input_Bindings;                    use Input_Bindings;
with Keyboard;                          use Keyboard;
with Values.Construction;               use Values.Construction;
with Widgets.Buttons.Pushes;            use Widgets.Buttons.Pushes;
with Widgets.Labels;                    use Widgets.Labels;
with Widgets.Panels;                    use Widgets.Panels;

package body Widgets.Game_Screens.Menus.Gamepad is

    package Connections is new Signals.Connections(Gamepad_Screen);
    use Connections;

    package Gamepad_Connections is new Signals.Gamepads.Connections(Game_Screen);
    use Gamepad_Connections;

    package Gamepad_Connections2 is new Signals.Gamepads.Connections(Gamepad_Screen);
    use Gamepad_Connections2;

    package Key_Connections is new Signals.Keys.Connections(Game_Screen);
    use Key_Connections;

    package Key_Connections2 is new Signals.Keys.Connections(Gamepad_Screen);
    use Key_Connections2;

    ----------------------------------------------------------------------------

    function Is_Axis_Action( action : Input_Action ) return Boolean is
    begin
        for a of AXIS_INPUTS loop
            if a = action then
                return True;
            end if;
        end loop;
        return False;
    end Is_Axis_Action;

    ----------------------------------------------------------------------------

    procedure Capture_Gamepad_Axis( this    : not null access Gamepad_Screen'Class;
                                    gamepad : Gamepad_Axis_Arguments );

    procedure Capture_Gamepad_Button( this    : not null access Gamepad_Screen'Class;
                                      gamepad : Gamepad_Button_Arguments );

    procedure On_Bindings_Changed( this : not null access Gamepad_Screen'Class );

    procedure On_Blurred( this : not null access Gamepad_Screen'Class );

    procedure On_Focused( this : not null access Gamepad_Screen'Class );

    procedure Pressed_Key( this     : not null access Gamepad_Screen'Class;
                           keyboard : Key_Arguments );

    procedure Start_Capture( this : not null access Gamepad_Screen'Class );

    procedure Stop_Capture( this   : not null access Gamepad_Screen'Class;
                            button : A_Button );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Gamepad_Screen( view : not null access Game_Views.Game_View'Class ) return A_Game_Screen is
        this : A_Gamepad_Screen := new Gamepad_Screen;
    begin
        this.Construct( view );
        return A_Game_Screen(this);
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Gamepad_Screen;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Gamepad_Screen;
                         view : not null access Game_Views.Game_View'Class ) is
        row    : A_Row_Layout;
        button : A_Button;
        label  : A_Label;
        panel  : A_Panel;
    begin
        Menu_Screen(this.all).Construct( view, "", isOpaque => True );
        this.view.BindingsChanged.Connect( Slot( this, On_Bindings_Changed'Access ) );

        this.GamepadPressed.Connect( Slot( this, Exit_Screen'Access, BUTTON_EAST ) );
        this.KeyPressed.Connect( Slot( this, Exit_Screen'Access, ALLEGRO_KEY_ESCAPE ) );

        this.Set_Background( Create_Icon( "interface:gamepad_menu" ) );

        this.scrollPane.Horizontal_Center.Set_Margin( -5.0 );
        this.scrollPane.Vertical_Center.Set_Margin( 0.0 );
        this.scrollPane.Width.Set( 155.0 );
        this.scrollPane.Height.Set( 56.0 );
        this.scrollPane.Set_Style( "controlsMenu" );

        for index in PLAYER_INPUTS'Range loop
            row := Create_Row_Layout( this.view );
            row.Set_Style( "controlsMenu" );
            row.Width.Set( this.scrollPane.Width.Get - 6.0 );

                button := Create_Push_Button( this.view );
                button.Set_Style( "controlsMenu" );
                button.Set_Text( this.view.Get_Binding_Name( PLAYER_INPUTS(index) ) );
                button.Blurred.Connect( Slot( this, On_Blurred'Access ) );
                button.Focused.Connect( Slot( this, On_Focused'Access ) );
                button.Pressed.Connect( Slot( this, Start_Capture'Access ) );
                button.Set_Attribute( "index", Create( index ) );
                row.Append( button );

                panel := Create_Panel( this.view );
                panel.Set_Style( "controlsMenu" );
                row.Append( panel );
                row.Set_Expander( panel );

                label := Create_Label( this.view );
                label.Set_Style( "controlsMenu" );
                row.Append( label );

            this.Add_Item( row, button );
        end loop;

        this.On_Bindings_Changed;
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Gamepad_Screen ) is
    begin
        this.view.BindingsChanged.Disconnect_All( this );
        Menu_Screen(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Capture_Gamepad_Axis( this    : not null access Gamepad_Screen'Class;
                                    gamepad : Gamepad_Axis_Arguments ) is
        button  : constant A_Button := A_Button(this.Signaller);
        index   : constant Integer := button.Get_Attribute( "index" ).To_Int;
        binding : Input_Binding := this.view.Get_Binding( PLAYER_INPUTS(index) );
    begin
        if (abs gamepad.position) > AXIS_SELECTED_THRESHOLD then
            if index in PLAYER_INPUTS'Range then
                if Is_Axis_Action( PLAYER_INPUTS(index) ) then
                    -- the action requires an axis-based input
                    binding.axis := gamepad.axis;
                    this.view.Set_Binding( PLAYER_INPUTS(index), binding );
                    this.Stop_Capture( button );
                end if;
            end if;
        end if;
    end Capture_Gamepad_Axis;

    ----------------------------------------------------------------------------

    procedure Capture_Gamepad_Button( this    : not null access Gamepad_Screen'Class;
                                      gamepad : Gamepad_Button_Arguments ) is
        button  : constant A_Button := A_Button(this.Signaller);
        index   : constant Integer := button.Get_Attribute( "index" ).To_Int;
        binding : Input_Binding := this.view.Get_Binding( PLAYER_INPUTS(index) );
    begin
        if index in PLAYER_INPUTS'Range then
            if Is_Axis_Action( PLAYER_INPUTS(index) ) then
                -- the action requires an axis-based input; ignore the button
                return;
            end if;
            binding.button := gamepad.button;
            this.view.Set_Binding( PLAYER_INPUTS(index), binding );
        end if;
        this.Stop_Capture( button );
    end Capture_Gamepad_Button;

    ----------------------------------------------------------------------------

    procedure On_Bindings_Changed( this : not null access Gamepad_Screen'Class ) is
    begin
        for i in 1..this.options.Count loop
            if Is_Axis_Action( PLAYER_INPUTS(i) ) then
                A_Label(A_Row_Layout(this.options.Get_At( i )).Get_Last).Set_Text(
                    Get_Gamepad_Axis_Description(
                        this.view.Get_Binding( PLAYER_INPUTS(i) ).axis
                    )
                );
            else
                A_Label(A_Row_Layout(this.options.Get_At( i )).Get_Last).Set_Text(
                    Get_Gamepad_Button_Description(
                        this.view.Get_Binding( PLAYER_INPUTS(i) ).button
                    )
                );
            end if;
        end loop;
    end On_Bindings_Changed;

    ----------------------------------------------------------------------------

    procedure On_Blurred( this : not null access Gamepad_Screen'Class ) is
        button : constant A_Button := A_Button(this.Signaller);
        index  : constant Integer := button.Get_Attribute( "index" ).To_Int;
        row    : constant A_Row_Layout := A_Row_Layout(this.options.Get_At( index ));
    begin
        for i in 1..row.Count loop
            row.Get_At( i ).Set_Style( "controlsMenu" );
        end loop;
    end On_Blurred;

    ----------------------------------------------------------------------------

    procedure On_Focused( this : not null access Gamepad_Screen'Class ) is
        button : constant A_Button := A_Button(this.Signaller);
        index  : constant Integer := button.Get_Attribute( "index" ).To_Int;
        row    : constant A_Row_Layout := A_Row_Layout(this.options.Get_At( index ));
    begin
        for i in 1..row.Count loop
            row.Get_At( i ).Set_Style( "controlsMenuFocused" );
        end loop;
    end On_Focused;

    ----------------------------------------------------------------------------

    procedure Pressed_Key( this     : not null access Gamepad_Screen'Class;
                           keyboard : Key_Arguments ) is
        button : constant A_Button := A_Button(this.Signaller);
    begin
        if keyboard.code = ALLEGRO_KEY_ESCAPE then
            this.Stop_Capture( button );
        end if;
    end Pressed_Key;

    ----------------------------------------------------------------------------

    procedure Start_Capture( this : not null access Gamepad_Screen'Class ) is
        button : constant A_Button := A_Button(this.Signaller);
        index  : constant Integer := button.Get_Attribute( "index" ).To_Int;
        row    : constant A_Row_Layout := A_Row_Layout(this.options.Get_At( index ));
    begin
        A_Label(row.Get_Last).Set_Text( "..." );

        button.Pressed.Disconnect_All( this );
        button.GamepadMoved.Connect( Slot( this, Capture_Gamepad_Axis'Access, AXIS_ANY, priority => Exclusive ) );
        button.GamepadPressed.Connect( Slot( this, Capture_Gamepad_Button'Access, BUTTON_ANY, priority => Exclusive ) );
        button.KeyPressed.Connect( Slot( this, Pressed_Key'Access, KEY_ANY, priority => Exclusive ) );
    end Start_Capture;

    ----------------------------------------------------------------------------

    procedure Stop_Capture( this   : not null access Gamepad_Screen'Class;
                            button : A_Button ) is
        index : constant Integer := button.Get_Attribute( "index" ).To_Int;
        row   : constant A_Row_Layout := A_Row_Layout(this.options.Get_At( index ));
    begin
        if A_Label(row.Get_Last).Get_Text = "..." then
            -- restore the label
            this.On_Bindings_Changed;
        end if;

        button.GamepadMoved.Disconnect( Slot( this, Capture_Gamepad_Axis'Access, AXIS_ANY ) );
        button.GamepadPressed.Disconnect( Slot( this, Capture_Gamepad_Button'Access, BUTTON_ANY ) );
        button.KeyPressed.Disconnect( Slot( this, Pressed_Key'Access, KEY_ANY ) );
        button.Pressed.Connect( Slot( this, Start_Capture'Access ) );
end Stop_Capture;

end Widgets.Game_Screens.Menus.Gamepad;
