--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Keyboard;                  use Allegro.Keyboard;
--with Debugging;                         use Debugging;
with Events.Game;                       use Events.Game;
with Game_Views;                        use Game_Views;
with Game_Views.Keen;                   use Game_Views.Keen;
with Gamepads;                          use Gamepads;
with Keyboard;                          use Keyboard;
with Widgets.Panels;                    use Widgets.Panels;
with Widgets.Row_Layouts;               use Widgets.Row_Layouts;

package body Widgets.Game_Screens.Message is

    package Connections is new Signals.Connections(Message_Screen);
    use Connections;

    package Gamepad_Connections is new Signals.Gamepads.Connections(Message_Screen);
    use Gamepad_Connections;

    package Key_Connections is new Signals.Keys.Connections(Message_Screen);
    use Key_Connections;

    ----------------------------------------------------------------------------

    procedure Ignore_Input( this : not null access Message_Screen'Class ) is null;

    -- Unpauses the game if auto-pause is enabled.
    procedure On_Hidden( this : not null access Message_Screen'Class );

    -- Pauses the game if auto-pause is enabled.
    procedure On_Shown( this : not null access Message_Screen'Class );

    procedure Pressed_Ok( this : not null access Message_Screen'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Message_Screen( view     : not null access Game_Views.Game_View'Class;
                                    isOpaque : Boolean := False ) return A_Message_Screen is
        this : A_Message_Screen := new Message_Screen;
    begin
        this.Construct( view, isOpaque );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Message_Screen;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this     : access Message_Screen;
                         view     : not null access Game_Views.Game_View'Class;
                         isOpaque : Boolean ) is
        panel : A_Panel;
        row   : A_Row_Layout;
    begin
        Game_Screen(this.all).Construct( view, "", isOpaque );

        this.Shown.Connect( Slot( this, On_Shown'Access ) );
        this.Hidden.Connect( Slot( this, On_Hidden'Access ) );

        this.KeyPressed.Connect( Slot( this, Ignore_Input'Access, KEY_ANY, priority => Low ) );         -- trap all key events
        this.KeyReleased.Connect( Slot( this, Ignore_Input'Access, KEY_ANY, priority => Low ) );        --
        this.KeyTyped.Connect( Slot( this, Ignore_Input'Access, KEY_ANY, priority => Low ) );           --
        this.KeyPressed.Connect( Slot( this, Pressed_Ok'Access, ALLEGRO_KEY_ENTER ) );
        this.KeyPressed.Connect( Slot( this, Pressed_Ok'Access, ALLEGRO_KEY_SPACE ) );
        this.KeyPressed.Connect( Slot( this, Pressed_Ok'Access, ALLEGRO_KEY_ESCAPE ) );

        this.GamepadPressed.Connect( Slot( this, Ignore_Input'Access, BUTTON_ANY, priority => Low ) );  -- trap all buttons
        this.GamepadPressed.Connect( Slot( this, Pressed_Ok'Access, BUTTON_SOUTH ) );

        this.Set_Style( "message" );

        panel := Create_Panel( view );
        this.Add_Child( panel );
        panel.Set_Style( "dialog" );
        panel.Center( panel.Get_Parent, 222.0, 78.0 );

            row := Create_Row_Layout( view );
            panel.Add_Widget( row );
            row.Set_Style( "dialog" );
            row.Fill( panel );

            -- left icon
            this.leftImage := Create_Label( view );
            this.leftImage.Set_Style( "dialog" );
            this.leftImage.Visible.Set( False );
            row.Append( this.leftImage );

            -- message
            this.textBox := Create_Textbox( view );
            this.textBox.Set_Style( "dialog" );
            row.Append( this.textBox );
            row.Set_Expander( this.textBox );

            -- right icon
            this.rightImage := Create_Label( view );
            this.rightImage.Set_Style( "dialog" );
            this.rightImage.Visible.Set( False );
            row.Append( this.rightImage );
    end Construct;

    ----------------------------------------------------------------------------

    procedure On_Hidden( this : not null access Message_Screen'Class ) is
    begin
        if this.autoPause then
            A_Keen_View(this.Get_View).Pause_By_Menu( enabled => False );
        end if;
    end On_Hidden;

    ----------------------------------------------------------------------------

    procedure On_Shown( this : not null access Message_Screen'Class ) is
    begin
        if this.autoPause then
            A_Keen_View(this.Get_View).Pause_By_Menu( enabled => True );
        end if;
    end On_Shown;

    ----------------------------------------------------------------------------

    procedure Pressed_Ok( this : not null access Message_Screen'Class ) is
    begin
        if this.closable then
            if Length( this.closeMsg ) > 0 then
                Queue_Game_Message( To_String( this.closeMsg ) );
            end if;
            this.Exit_Screen;
        end if;
    end Pressed_Ok;

    ----------------------------------------------------------------------------

    procedure Set_Auto_Pause( this    : not null access Message_Screen'Class;
                              enabled : Boolean ) is
    begin
        this.autoPause := enabled;
    end Set_Auto_Pause;

    ----------------------------------------------------------------------------

    procedure Set_Closable( this     : not null access Message_Screen'Class;
                            closable : Boolean ) is
    begin
        this.closable := closable;
    end Set_Closable;

    ----------------------------------------------------------------------------

    procedure Set_Close_Message( this : not null access Message_Screen'Class;
                                 msg  : String ) is
    begin
        this.closeMsg := To_Unbounded_String( msg );
    end Set_Close_Message;

    ----------------------------------------------------------------------------

    procedure Set_Icon( this : not null access Message_Screen'Class; icon : Icon_Type ) is
    begin
        this.leftImage.Set_Icon( icon );
        this.rightImage.Set_Icon( icon );
    end Set_Icon;

    ----------------------------------------------------------------------------

    procedure Set_Text( this : not null access Message_Screen'Class; text : Unbounded_String ) is
    begin
        this.textBox.Set_Text( text );
    end Set_Text;

    ----------------------------------------------------------------------------

    procedure Set_Text_Align( this  : not null access Message_Screen'Class;
                              align : Align_Type ) is
    begin
        this.leftImage.Visible.Set( (align = Right or align = Top_Right or align = Bottom_Right) );
        this.rightImage.Visible.Set( not this.leftImage.Visible.Get );
    end Set_Text_Align;

    ----------------------------------------------------------------------------

    overriding
    function To_String( this : access Message_Screen ) return String
    is ("<" & this.Get_Class_Name & " : text='" & To_String( this.textBox.Get_Text ) & "'>");

end Widgets.Game_Screens.Message;
