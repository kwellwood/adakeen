--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Game_Views;                        use Game_Views;
with Gamepads;                          use Gamepads;
with Keyboard;                          use Keyboard;

package body Widgets.Game_Screens.Title is

    package Key_Connections is new Signals.Keys.Connections(Game_Screen);
    use Key_Connections;

    package Gamepad_Connections is new Signals.Gamepads.Connections(Title_Screen);

    ----------------------------------------------------------------------------

    procedure On_Gamepad_Press( this    : not null access Title_Screen'Class;
                                gamepad : Gamepad_Button_Arguments );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Title_Screen( view : not null access Game_Views.Game_View'Class ) return A_Game_Screen is
        this : A_Title_Screen := new Title_Screen;
    begin
        this.Construct( view );
        return A_Game_Screen(this);
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Title_Screen;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Title_Screen;
                         view : not null access Game_Views.Game_View'Class ) is
    begin
        Game_Screen(this.all).Construct( view, "", isOpaque => True );
        this.Set_Style( "menu" );
        this.Set_Background( Create_Icon( "interface:title" ) );

        this.GamepadPressed.Connect( Gamepad_Connections.Slot( this, On_Gamepad_Press'Access, BUTTON_SOUTH ) );
        this.KeyPressed.Connect( Slot( this, Exit_Screen'Access, KEY_ANY ) );
    end Construct;

    ----------------------------------------------------------------------------

    procedure On_Gamepad_Press( this    : not null access Title_Screen'Class;
                                gamepad : Gamepad_Button_Arguments ) is
        pragma Unreferenced( gamepad );    -- any button will exit
    begin
        this.Exit_Screen;
    end On_Gamepad_Press;

end Widgets.Game_Screens.Title;
