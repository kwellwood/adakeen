--
-- Copyright (c) 2018-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Widgets.Buttons;                   use Widgets.Buttons;
with Widgets.Labels;                    use Widgets.Labels;

package Widgets.Game_Screens.Story is

    -- A title screen simply displays the game's splash screen and waits for
    -- any key to be pressed before exiting.
    type Story_Screen is new Game_Screen and Animated with private;

    -- Creates a new story screen for showing a sequence of images, or pages,
    -- starting with tile 'firstPage'. The page transitions are controlled with
    -- tile attributes "nextPage" and "prevPage" as tile ids. A page can pause,
    -- temporarily disabling the buttons, using the "minDelay" tile attribute.
    -- If a page has neither "nextPage" nor "prevPage" tile attributes, moving
    -- to the next page will exit the screen.
    --
    -- The screen's widget style will be 'style'.
    function Create_Story_Screen( view      : not null access Game_Views.Game_View'Class;
                                  style     : String;
                                  firstPage : Icon_Type ) return A_Game_Screen;

private

    type Story_Screen is new Game_Screen and Animated with
        record
            content     : A_Label;             -- displays the current page image
            btnNext     : A_Button;            -- button for next page
            enableDelay : Time_Span := Time_Span_Zero;
            skipDelay   : Time_Span := Time_Span_Last;
        end record;

    procedure Construct( this      : access Story_Screen;
                         view      : not null access Game_Views.Game_View'Class;
                         style     : String;
                         firstPage : Icon_Type );

    procedure Tick( this : access Story_Screen; time : Tick_Time );

end Widgets.Game_Screens.Story;
