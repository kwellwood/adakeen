--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Widgets.Menu_Enumerations;         use Widgets.Menu_Enumerations;

package Widgets.Game_Screens.Menus.Config is

    -- The config screen represents Keen's configuration menu, accessible from
    -- the main menu.
    type Config_Screen is new Menu_Screen with private;

    -- Creates a new configuration menu screen.
    function Create_Config_Screen( view : not null access Game_Views.Game_View'Class ) return A_Game_Screen;

private

    type Config_Screen is new Menu_Screen with
        record
            fullscreen : A_Menu_Enumeration;
            widescreen : A_Menu_Enumeration;
            sound      : A_Menu_Enumeration;
            music      : A_Menu_Enumeration;
        end record;
    type A_Config_Screen is access all Config_Screen'Class;

    procedure Construct( this : access Config_Screen;
                         view : not null access Game_Views.Game_View'Class );

end Widgets.Game_Screens.Menus.Config;
