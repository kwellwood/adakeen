--
-- Copyright (c) 2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Keyboard;                  use Allegro.Keyboard;
--with Debugging;                         use Debugging;
with Game_Views;                        use Game_Views;
with Game_Views.Keen;                   use Game_Views.Keen;
with Gamepads;                          use Gamepads;
with Input_Bindings;                    use Input_Bindings;
with Keyboard;                          use Keyboard;
with Values.Construction;               use Values.Construction;
with Widgets.Buttons.Pushes;            use Widgets.Buttons.Pushes;
with Widgets.Labels;                    use Widgets.Labels;
with Widgets.Panels;                    use Widgets.Panels;

package body Widgets.Game_Screens.Menus.Keyboard is

    package Connections is new Signals.Connections(Keyboard_Screen);
    use Connections;

    package Gamepad_Connections is new Signals.Gamepads.Connections(Game_Screen);
    use Gamepad_Connections;

    package Gamepad_Connections2 is new Signals.Gamepads.Connections(Keyboard_Screen);
    use Gamepad_Connections2;

    package Key_Connections is new Signals.Keys.Connections(Game_Screen);
    use Key_Connections;

    package Key_Connections2 is new Signals.Keys.Connections(Keyboard_Screen);
    use Key_Connections2;

    ----------------------------------------------------------------------------

    procedure Capture_Gamepad_Axis( this    : not null access Keyboard_Screen'Class;
                                    gamepad : Gamepad_Axis_Arguments ) is null;

    procedure Capture_Gamepad_Button( this    : not null access Keyboard_Screen'Class;
                                      gamepad : Gamepad_Button_Arguments ) is null;

    procedure Capture_Key( this     : not null access Keyboard_Screen'Class;
                           keyboard : Key_Arguments );

    procedure Ignore_Gamepad( this : not null access Keyboard_Screen'Class ) is null;

    procedure On_Bindings_Changed( this : not null access Keyboard_Screen'Class );

    procedure On_Blurred( this : not null access Keyboard_Screen'Class );

    procedure On_Focused( this : not null access Keyboard_Screen'Class );

    procedure Start_Capture( this : not null access Keyboard_Screen'Class );

    procedure Stop_Capture( this   : not null access Keyboard_Screen'Class;
                            button : A_Button );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Keyboard_Screen( view : not null access Game_Views.Game_View'Class ) return A_Game_Screen is
        this : A_Keyboard_Screen := new Keyboard_Screen;
    begin
        this.Construct( view );
        return A_Game_Screen(this);
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Keyboard_Screen;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Keyboard_Screen;
                         view : not null access Game_Views.Game_View'Class ) is
        row    : A_Row_Layout;
        button : A_Button;
        label  : A_Label;
        panel  : A_Panel;
    begin
        Menu_Screen(this.all).Construct( view, "", isOpaque => True );

        this.GamepadPressed.Connect( Slot( this, Exit_Screen'Access, BUTTON_EAST ) );
        this.KeyPressed.Connect( Slot( this, Exit_Screen'Access, ALLEGRO_KEY_ESCAPE ) );
        this.view.BindingsChanged.Connect( Slot( this, On_Bindings_Changed'Access ) );

        this.Set_Background( Create_Icon( "interface:keyboard_menu" ) );

        this.scrollPane.Horizontal_Center.Set_Margin( -5.0 );
        this.scrollPane.Vertical_Center.Set_Margin( 0.0 );
        this.scrollPane.Width.Set( 155.0 );
        this.scrollPane.Height.Set( 56.0 );
        this.scrollPane.Set_Style( "controlsMenu" );

        for index in PLAYER_INPUTS'Range loop
            row := Create_Row_Layout( this.view );
            row.Set_Style( "controlsMenu" );
            row.Width.Set( this.scrollPane.Width.Get - 6.0 );

                button := Create_Push_Button( this.view );
                button.Set_Style( "controlsMenu" );
                button.Set_Text( this.view.Get_Binding_Name( PLAYER_INPUTS(index) ) );
                button.Blurred.Connect( Slot( this, On_Blurred'Access ) );
                button.Focused.Connect( Slot( this, On_Focused'Access ) );
                button.Pressed.Connect( Slot( this, Start_Capture'Access ) );
                button.GamepadPressed.Connect( Slot( this, Ignore_Gamepad'Access, BUTTON_SOUTH ) );
                button.Set_Attribute( "index", Create( index ) );
                row.Append( button );

                panel := Create_Panel( this.view );
                panel.Set_Style( "controlsMenu" );
                row.Append( panel );
                row.Set_Expander( panel );

                label := Create_Label( this.view );
                label.Set_Style( "controlsMenu" );
                label.Set_Text( Al_Keycode_To_Name( this.view.Get_Binding( PLAYER_INPUTS(index) ).keycode ) );
                row.Append( label );

            this.Add_Item( row, button );
        end loop;
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Keyboard_Screen ) is
    begin
        this.view.BindingsChanged.Disconnect_All( this );
        Menu_Screen(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Capture_Key( this     : not null access Keyboard_Screen'Class;
                           keyboard : Key_Arguments ) is
        button  : constant A_Button := A_Button(this.Signaller);
        index   : constant Integer := button.Get_Attribute( "index" ).To_Int;
        binding : Input_Binding := this.view.Get_Binding( PLAYER_INPUTS(index) );
    begin
        if keyboard.code /= ALLEGRO_KEY_ESCAPE then
            if index in PLAYER_INPUTS'Range then
                binding.keycode := keyboard.code;
                this.view.Set_Binding( PLAYER_INPUTS(index), binding );
            end if;
        end if;
        this.Stop_Capture( button );
    end Capture_Key;

    ----------------------------------------------------------------------------

    procedure On_Bindings_Changed( this : not null access Keyboard_Screen'Class ) is
    begin
        for i in 1..this.options.Count loop
            A_Label(A_Row_Layout(this.options.Get_At( i )).Get_Last).Set_Text( Al_Keycode_To_Name( this.view.Get_Binding( PLAYER_INPUTS(i) ).keycode ) );
        end loop;
    end On_Bindings_Changed;

    ----------------------------------------------------------------------------

    procedure On_Blurred( this : not null access Keyboard_Screen'Class ) is
        button : constant A_Button := A_Button(this.Signaller);
        index  : constant Integer := button.Get_Attribute( "index" ).To_Int;
        row    : constant A_Row_Layout := A_Row_Layout(this.options.Get_At( index ));
    begin
        for i in 1..row.Count loop
            row.Get_At( i ).Set_Style( "controlsMenu" );
        end loop;
    end On_Blurred;

    ----------------------------------------------------------------------------

    procedure On_Focused( this : not null access Keyboard_Screen'Class ) is
        button : constant A_Button := A_Button(this.Signaller);
        index  : constant Integer := button.Get_Attribute( "index" ).To_Int;
        row    : constant A_Row_Layout := A_Row_Layout(this.options.Get_At( index ));
    begin
        for i in 1..row.Count loop
            row.Get_At( i ).Set_Style( "controlsMenuFocused" );
        end loop;
    end On_Focused;

    ----------------------------------------------------------------------------

    procedure Start_Capture( this : not null access Keyboard_Screen'Class ) is
        button : constant A_Button := A_Button(this.Signaller);
        index  : constant Integer := button.Get_Attribute( "index" ).To_Int;
        row    : constant A_Row_Layout := A_Row_Layout(this.options.Get_At( index ));
    begin
        A_Label(row.Get_Last).Set_Text( "..." );

        button.Pressed.Disconnect_All( this );
        button.KeyPressed.Connect( Slot( this, Capture_Key'Access, KEY_ANY, priority => High ) );
        button.GamepadMoved.Connect( Slot( this, Capture_Gamepad_Axis'Access, priority => High ) );
        button.GamepadPressed.Connect( Slot( this, Capture_Gamepad_Button'Access, priority => High ) );
    end Start_Capture;

    ----------------------------------------------------------------------------

    procedure Stop_Capture( this   : not null access Keyboard_Screen'Class;
                            button : A_Button ) is
        index : constant Integer := button.Get_Attribute( "index" ).To_Int;
        row   : constant A_Row_Layout := A_Row_Layout(this.options.Get_At( index ));
    begin
        A_Label(row.Get_Last).Set_Text( Al_Keycode_To_Name( this.view.Get_Binding( PLAYER_INPUTS(index) ).keycode ) );

        button.KeyPressed.Disconnect( Slot( this, Capture_Key'Access, KEY_ANY, priority => High ) );
        button.GamepadMoved.Disconnect( Slot( this, Capture_Gamepad_Axis'Access, priority => High ) );
        button.GamepadPressed.Disconnect( Slot( this, Capture_Gamepad_Button'Access, priority => High ) );
        button.Pressed.Connect( Slot( this, Start_Capture'Access ) );
    end Stop_Capture;

end Widgets.Game_Screens.Menus.Keyboard;
