--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Keyboard;                  use Allegro.Keyboard;
--with Debugging;                         use Debugging;
with Game_Views;                        use Game_Views;
with Game_Views.Keen;                   use Game_Views.Keen;
with Gamepads;                          use Gamepads;
with Preferences;                       use Preferences;
with Widgets.Game_Screens.Menus.Gamepad;
 use Widgets.Game_Screens.Menus.Gamepad;
with Widgets.Game_Screens.Menus.Keyboard;
 use Widgets.Game_Screens.Menus.Keyboard;
with Widgets.Screen_Managers;           use Widgets.Screen_Managers;

package body Widgets.Game_Screens.Menus.Config is

    package Connections is new Signals.Connections(Config_Screen);
    use Connections;

    package Gamepad_Connections is new Signals.Gamepads.Connections(Game_Screen);
    use Gamepad_Connections;

    package Key_Connections is new Signals.Keys.Connections(Game_Screen);
    use Key_Connections;

    ----------------------------------------------------------------------------

    procedure Clicked_Gamepad( this : not null access Config_Screen'Class );

    procedure Clicked_Keyboard( this : not null access Config_Screen'Class );

    procedure Changed_Fullscreen( this : not null access Config_Screen'Class );

    procedure Changed_Music( this : not null access Config_Screen'Class );

    procedure Changed_Sound( this : not null access Config_Screen'Class );

    procedure Changed_Widescreen( this : not null access Config_Screen'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Config_Screen( view : not null access Game_Views.Game_View'Class ) return A_Game_Screen is
        this : A_Config_Screen := new Config_Screen;
    begin
        this.Construct( view );
        return A_Game_Screen(this);
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Config_Screen;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Config_Screen;
                         view : not null access Game_Views.Game_View'Class ) is
        button : A_Button;
    begin
        Menu_Screen(this.all).Construct( view, "", isOpaque => True );

        this.GamepadPressed.Connect( Slot( this, Exit_Screen'Access, BUTTON_EAST ) );
        this.KeyPressed.Connect( Slot( this, Exit_Screen'Access, ALLEGRO_KEY_ESCAPE ) );

        this.Set_Background( Create_Icon( "interface:config_menu" ) );

        -- sound effects
        this.sound := Create_Menu_Enum( view );
        this.sound.Set_Style( "menu" );
        this.sound.Add_Option( "SOUNDS: OFF" );
        this.sound.Add_Option( "SOUNDS: ON" );
        this.sound.Enable_Wrap( True );
        this.sound.Set_Index( (if Get_Pref( "application", "soundfx" ) then 2 else 1) );
        this.sound.Accepted.Connect( Slot( this, Changed_Sound'Access ) );
        this.Add_Item( this.sound );

        -- music
        this.music := Create_Menu_Enum( view );
        this.music.Set_Style( "menu" );
        this.music.Add_Option( "MUSIC: OFF" );
        this.music.Add_Option( "MUSIC: ON" );
        this.music.Enable_Wrap( True );
        this.music.Set_Index( (if Get_Pref( "application", "music" ) then 2 else 1) );
        this.music.Accepted.Connect( Slot( this, Changed_Music'Access ) );
        this.Add_Item( this.music );

        -- fullscreen windowed
        if Get_Pref( "application", "windowed" ) then
            -- "windowed=1" means the application is not in true fullscreen mode
            -- it can convert between windowed and fullscreen windowed
            -- "fullscreen=1" means the display is in fullscreen windowed mode
            this.fullscreen := Create_Menu_Enum( view );
            this.fullscreen.Set_Style( "menu" );
            this.fullscreen.Add_Option( "FULLSCREEN: OFF" );
            this.fullscreen.Add_Option( "FULLSCREEN: ON" );
            this.fullscreen.Enable_Wrap( True );
            this.fullscreen.Set_Index( (if Get_Pref( "application", "fullscreen" ) then 2 else 1) );
            this.fullscreen.Accepted.Connect( Slot( this, Changed_Fullscreen'Access ) );
            this.Add_Item( this.fullscreen );
        end if;

        -- widescreen
        this.widescreen := Create_Menu_Enum( view );
        this.widescreen.Set_Style( "menu" );
        this.widescreen.Add_Option( "WIDESCREEN: OFF" );
        this.widescreen.Add_Option( "WIDESCREEN: ON" );
        this.widescreen.Enable_Wrap( True );
        this.widescreen.Set_Index( (if Get_Pref( "application", "widescreen" ) then 2 else 1) );
        this.widescreen.Accepted.Connect( Slot( this, Changed_Widescreen'Access ) );
        this.Add_Item( this.widescreen );

        -- keyboard controls button
        button := this.Create_Menu_Button( "KEYBOARD" );
        button.Pressed.Connect( Slot( this, Clicked_Keyboard'Access ) );
        this.Add_Item( button );

        -- gamepad controls button
        button := this.Create_Menu_Button( "CONTROLLER" );
        button.Pressed.Connect( Slot( this, Clicked_Gamepad'Access ) );
        this.Add_Item( button );

    end Construct;

    ----------------------------------------------------------------------------

    procedure Changed_Fullscreen( this : not null access Config_Screen'Class ) is
        enabled : constant Boolean := this.fullscreen.Get_Index > 1;
        display : constant A_Allegro_Display := this.Get_View.Get_Display;
        xres    : Integer;
        yres    : Integer;
    begin
        if Al_Set_Display_Flag( display, ALLEGRO_FULLSCREEN_WINDOW, enabled ) then
            Set_Pref( "application", "fullscreen", enabled );

            -- technically this shouldn't be necessary because the resize event
            -- will trigger Game_View to update the Window's size, but there's
            -- a race condition on Windows that sometimes misses the event.
            xres := Al_Get_Display_Width( display );
            yres := Al_Get_Display_Height( display );
            this.Get_View.Get_Window.Width.Set( Float(xres) );
            this.Get_View.Get_Window.Height.Set( Float(yres) );
        else
            -- put the previous enum value back
            this.fullscreen.Set_Index( (if enabled then 1 else 2), notify => False );
        end if;
    end Changed_Fullscreen;

    ----------------------------------------------------------------------------

    procedure Changed_Music( this : not null access Config_Screen'Class ) is
    begin
        Set_Pref( "application", "music", this.music.Get_Index > 1 );
    end Changed_Music;

    ----------------------------------------------------------------------------

    procedure Changed_Sound( this : not null access Config_Screen'Class ) is
    begin
        Set_Pref( "application", "soundfx", this.sound.Get_Index > 1 );
    end Changed_Sound;

    ----------------------------------------------------------------------------

    procedure Changed_Widescreen( this : not null access Config_Screen'Class ) is
        enabled : constant Boolean := this.widescreen.Get_Index > 1;
    begin
        Set_Pref( "application", "widescreen", enabled );
        A_Keen_View(this.Get_View).Enable_Widescreen( enabled );
    end Changed_Widescreen;

    ----------------------------------------------------------------------------

    procedure Clicked_Gamepad( this : not null access Config_Screen'Class ) is
    begin
        A_Screen_Manager(this.parent).Add_Front( Create_Gamepad_Screen( this.Get_View ) );
    end Clicked_Gamepad;

    ----------------------------------------------------------------------------

    procedure Clicked_Keyboard( this : not null access Config_Screen'Class ) is
    begin
        A_Screen_Manager(this.parent).Add_Front( Create_Keyboard_Screen( this.Get_View ) );
    end Clicked_Keyboard;

end Widgets.Game_Screens.Menus.Config;
