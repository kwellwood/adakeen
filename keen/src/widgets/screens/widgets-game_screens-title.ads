--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Widgets.Game_Screens.Title is

    -- A title screen simply displays the game's splash screen and waits for
    -- any key to be pressed before exiting.
    type Title_Screen is new Game_Screen with private;

    -- Creates a new title screen.
    function Create_Title_Screen( view : not null access Game_Views.Game_View'Class ) return A_Game_Screen;

private

    type Title_Screen is new Game_Screen with null record;
    type A_Title_Screen is access all Title_Screen'Class;

    procedure Construct( this : access Title_Screen;
                         view : not null access Game_Views.Game_View'Class );

end Widgets.Game_Screens.Title;
