--
-- Copyright (c) 2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Widgets.Game_Screens.Menus.Keyboard is

    -- The keyboard controls screen represents Keen's keyboard bindings menu,
    -- accessible from the config menu.
    type Keyboard_Screen is new Menu_Screen with private;

    -- Creates a new keyboard configuration menu screen.
    function Create_Keyboard_Screen( view : not null access Game_Views.Game_View'Class ) return A_Game_Screen;

private

    type Keyboard_Screen is new Menu_Screen with null record;
    type A_Keyboard_Screen is access all Keyboard_Screen'Class;

    procedure Construct( this : access Keyboard_Screen;
                         view : not null access Game_Views.Game_View'Class );

    procedure Delete( this : in out Keyboard_Screen );

end Widgets.Game_Screens.Menus.Keyboard;
