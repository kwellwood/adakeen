--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Widgets.Scoreboards is

    type Scoreboard is new Widget with private;
    type A_Scoreboard is access all Scoreboard'Class;

    function Create_Scoreboard( view : not null access Game_Views.Game_View'Class ) return A_Scoreboard;
    pragma Postcondition( Create_Scoreboard'Result /= null );

private

    BACKGROUND_ELEM : constant :=  1;
    DIGIT0_ELEM     : constant :=  2;
    DIGIT1_ELEM     : constant :=  3;
    DIGIT2_ELEM     : constant :=  4;
    DIGIT3_ELEM     : constant :=  5;
    DIGIT4_ELEM     : constant :=  6;
    DIGIT5_ELEM     : constant :=  7;
    DIGIT6_ELEM     : constant :=  8;
    DIGIT7_ELEM     : constant :=  9;
    DIGIT8_ELEM     : constant := 10;
    DIGIT9_ELEM     : constant := 11;

    DISABLED_STATE : constant Widget_State := 2**0;

    ----------------------------------------------------------------------------

    type Scoreboard is new Widget with
        record
            ammo   : Integer := 0;
            lives  : Integer := 0;
            points : Integer := 0;
        end record;

    procedure Construct( this : access Scoreboard;
                         view : not null access Game_Views.Game_View'Class );

    procedure Draw_Content( this : access Scoreboard );

    function Get_Min_Height( this : access Scoreboard ) return Float;

    function Get_Min_Width( this : access Scoreboard ) return Float;

    procedure Handle_Event( this : access Scoreboard;
                            evt  : in out A_Event;
                            resp : out Response_Type );

end Widgets.Scoreboards;
