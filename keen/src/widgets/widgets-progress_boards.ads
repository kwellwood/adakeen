--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Widgets.Progress_Boards is

    type Progress_Board is new Widget with private;
    type A_Progress_Board is access all Progress_Board'Class;

    function Create_Progress_Board( view : not null access Game_Views.Game_View'Class ) return A_Progress_Board;
    pragma Postcondition( Create_Progress_Board'Result /= null );

    -- Progress Board Signals
    function Closed( this : not null access Progress_Board'Class ) return access Signal'Class;
    function Opened( this : not null access Progress_Board'Class ) return access Signal'Class;

    -- Returns true if the board is not currently closed. It may be fully open
    -- or sliding in either direction.
    function Is_Open( this : not null access Progress_Board'Class ) return Boolean;

    procedure Show( this : not null access Progress_Board'Class; enabled : Boolean );

    -- Toggles the open/closed state of the progress board. Calling this is
    -- equivalent to this.Show( not this.Is_Open ).
    procedure Toggle( this : not null access Progress_Board'Class );

private

    BACKGROUND_ELEM : constant :=  1;
    TEXT_ELEM       : constant :=  2;
    DIGIT0_ELEM     : constant :=  3;
    DIGIT1_ELEM     : constant :=  4;
    DIGIT2_ELEM     : constant :=  5;
    DIGIT3_ELEM     : constant :=  6;
    DIGIT4_ELEM     : constant :=  7;
    DIGIT5_ELEM     : constant :=  8;
    DIGIT6_ELEM     : constant :=  9;
    DIGIT7_ELEM     : constant := 10;
    DIGIT8_ELEM     : constant := 11;
    DIGIT9_ELEM     : constant := 12;
    ANCIENT_ELEM    : constant := 13;
    REDKEY_ELEM     : constant := 14;
    BLUEKEY_ELEM    : constant := 15;
    YELLOWKEY_ELEM  : constant := 16;
    GREENKEY_ELEM   : constant := 17;

    DISABLED_STATE : constant Widget_State := 2**0;

    ----------------------------------------------------------------------------

    SLIDE_DURATION : constant Time_Span := Milliseconds( 300 );

    type Progress_Board is new Widget with
        record
            slideDir      : Integer := 0;   -- <0 = up, >0 = down

            ammo          : Natural := 0;
            ancients      : Natural := 0;
            blueKey       : Boolean := False;
            drops         : Natural := 0;
            greenKey      : Boolean := False;
            lives         : Natural := 0;
            location      : Unbounded_String;
            points        : Natural := 0;
            nextLife      : Natural := 0;
            redKey        : Boolean := False;
            scuba         : Boolean := False;
            yellowKey     : Boolean := False;

            sigClosed     : aliased Signal;
            sigOpened     : aliased Signal;
        end record;

    procedure Construct( this : access Progress_Board;
                         view : not null access Game_Views.Game_View'Class );

    procedure Draw_Content( this : access Progress_Board );

    function Get_Min_Height( this : access Progress_Board ) return Float;

    function Get_Min_Width( this : access Progress_Board ) return Float;

    procedure Handle_Event( this : access Progress_Board;
                            evt  : in out A_Event;
                            resp : out Response_Type );

end Widgets.Progress_Boards;
