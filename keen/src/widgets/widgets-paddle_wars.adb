--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Assets.Fonts;                      use Assets.Fonts;
--with Debugging;                         use Debugging;
with Drawing;                           use Drawing;
with Drawing.Bitmaps;                   use Drawing.Bitmaps;
with Drawing.Primitives;                use Drawing.Primitives;
with Drawing.Tiles;                     use Drawing.Tiles;
with Events.Audio;                      use Events.Audio;
with Game_Views;                        use Game_Views;
with Gamepads;                          use Gamepads;
with Widget_Styles.Registry;            use Widget_Styles.Registry;

package body Widgets.Paddle_Wars is

    package Connections is new Signals.Connections(Paddle_War);
    use Connections;

    package Gamepad_Connections is new Signals.Gamepads.Connections(Paddle_War);
    use Gamepad_Connections;

    package Key_Connections is new Signals.Keys.Connections(Paddle_War);
    use Key_Connections;

    ----------------------------------------------------------------------------

    BALL_SPEED        : constant := 65.0;
    BALL_RESET_DELAY  : constant Time_Span := Seconds( 1 );

    KEEN_PADDLE_SPEED : constant := 80.0;
    COMP_PADDLE_SPEED : constant := 40.0;

    KEEN_SCORE_X : constant := 31.0;
    KEEN_SCORE_Y : constant := 2.0;

    COMP_SCORE_X : constant := 137.0;
    COMP_SCORE_Y : constant := 2.0;

    ----------------------------------------------------------------------------

    -- Left X-Axis changed.
    procedure On_Gamepad_Axis( this    : not null access Paddle_War'Class;
                               gamepad : Gamepad_Axis_Arguments );

    -- Starts moving the player's paddle when he presses the left key.
    procedure On_Key_Left( this : not null access Paddle_War'Class );

    -- Stops moving the player's paddle when he releases the arrow keys.
    procedure On_Key_Released( this : not null access Paddle_War'Class );

    -- Starts moving the player's paddle when he presses the right key.
    procedure On_Key_Right( this : not null access Paddle_War'Class );

    -- Resets the ball to the middle of the field of play and zeros its
    -- horizontal motion, but maintains the direction of its vertical motion.
    -- This occurs each time a player scores.
    procedure Reset_Ball( this : not null access Paddle_War'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Paddle_War( view : not null access Game_Views.Game_View'Class;
                                id   : String := "" ) return A_Paddle_War is
        this : A_Paddle_War := new Paddle_War;
    begin
        this.Construct( view, id );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Paddle_War;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Paddle_War;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String ) is
    begin
        Widget(this.all).Construct( view, id, "PaddleWar" );

        -- Automatically reset the game every time it is displayed after being
        -- hidden.
        this.Shown.Connect( Slot( this, Reset'Access ) );

        this.GamepadMoved.Connect( Slot( this, On_Gamepad_Axis'Access, AXIS_LXN ) );
        this.GamepadMoved.Connect( Slot( this, On_Gamepad_Axis'Access, AXIS_LXP ) );

        this.KeyPressed.Connect( Slot( this, On_Key_Left'Access, ALLEGRO_KEY_LEFT ) );
        this.KeyPressed.Connect( Slot( this, On_Key_Right'Access, ALLEGRO_KEY_RIGHT ) );
        this.KeyReleased.Connect( Slot( this, On_Key_Released'Access, ALLEGRO_KEY_LEFT ) );
        this.KeyReleased.Connect( Slot( this, On_Key_Released'Access, ALLEGRO_KEY_RIGHT ) );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Content( this : access Paddle_War ) is
        font  : constant A_Font := this.style.Get_Font( TEXT_ELEM );
        drawX,
        drawY : Float;
    begin
        -- draw the background
        Draw_Tile_Filled( this.style.Get_Image( BACKGROUND_ELEM ).Get_Tile,
                          0.0, 0.0, 0.0,
                          this.viewport.width, this.viewport.height,
                          Lighten( this.style.Get_Tint( BACKGROUND_ELEM ),
                                   this.style.Get_Shade( BACKGROUND_ELEM ) ) );

        -- draw the scores
        font.Draw_String( Trim( Integer'Image( this.keenScore ), Left ),
                          KEEN_SCORE_X, KEEN_SCORE_Y,
                          Lighten( this.style.Get_Color( TEXT_ELEM ),
                                   this.style.Get_Shade( TEXT_ELEM ) ) );
        font.Draw_String( Trim( Integer'Image( this.cpuScore ), Left ),
                          COMP_SCORE_X, COMP_SCORE_Y,
                          Lighten( this.style.Get_Color( TEXT_ELEM ),
                                   this.style.Get_Shade( TEXT_ELEM ) ) );

        -- draw the computer paddle
        drawX := this.compPlayer.x - this.compPlayer.w / 2.0;
        drawY := this.compPlayer.y - this.compPlayer.h / 2.0;
        To_Target_Pixel_Rounded( drawX, drawY );
        Draw_Bitmap( this.style.Get_Image( PADDLE_ELEM ).Get_Bitmap,
                     drawX, drawY, 0.0,
                     Lighten( this.style.Get_Color( PADDLE_ELEM ),
                              this.style.Get_Shade( PADDLE_ELEM ) ) );

        -- draw the keen paddle
        drawX := this.keenPlayer.x - this.keenPlayer.w / 2.0;
        drawY := this.keenPlayer.y - this.keenPlayer.h / 2.0;
        To_Target_Pixel_Rounded( drawX, drawY );
        Draw_Bitmap( this.style.Get_Image( PADDLE_ELEM ).Get_Bitmap,
                     drawX, drawY, 0.0,
                     Lighten( this.style.Get_Color( PADDLE_ELEM ),
                              this.style.Get_Shade( PADDLE_ELEM ) ) );

        -- draw the ball (if it's in or nearly inbounds)
        if this.ball.y + this.ball.h / 2.0 >= this.boundsY1 and then
           this.ball.y - this.ball.h / 2.0 <= this.boundsY2
        then
            drawX := this.ball.x - this.ball.w / 2.0;
            drawY := this.ball.y - this.ball.h / 2.0;
            To_Target_Pixel_Rounded( drawX, drawY );
            Draw_Bitmap( this.style.Get_Image( BALL_ELEM ).Get_Bitmap,
                         drawX, drawY, 0.0,
                         Lighten( this.style.Get_Color( BALL_ELEM ),
                                  this.style.Get_Shade( BALL_ELEM ) ) );
        end if;
    end Draw_Content;

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Height( this : access Paddle_War ) return Float
    is (Float(this.style.Get_Image( BACKGROUND_ELEM ).Get_Height));

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Width( this : access Paddle_War ) return Float
    is (Float(this.style.Get_Image( BACKGROUND_ELEM ).Get_Width));

    ----------------------------------------------------------------------------

    procedure On_Gamepad_Axis( this    : not null access Paddle_War'Class;
                               gamepad : Gamepad_Axis_Arguments ) is
    begin
        this.keenPlayer.xv := gamepad.position * KEEN_PADDLE_SPEED;
    end On_Gamepad_Axis;

    ----------------------------------------------------------------------------

    procedure On_Key_Left( this : not null access Paddle_War'Class ) is
    begin
        this.keenPlayer.xv := -KEEN_PADDLE_SPEED;
    end On_Key_Left;

    ----------------------------------------------------------------------------

    procedure On_Key_Released( this : not null access Paddle_War'Class ) is
    begin
        this.keenPlayer.xv := 0.0;
    end On_Key_Released;

    ----------------------------------------------------------------------------

    procedure On_Key_Right( this : not null access Paddle_War'Class ) is
    begin
        this.keenPlayer.xv := KEEN_PADDLE_SPEED;
    end On_Key_Right;

    ----------------------------------------------------------------------------

    procedure Reset( this : not null access Paddle_War'Class ) is
    begin
        -- get the size of the paddles
        this.compPlayer.w := Float(this.style.Get_Image( PADDLE_ELEM ).Get_Width);
        this.compPlayer.h := Float(this.style.Get_Image( PADDLE_ELEM ).Get_Height);

        this.keenPlayer.w := this.compPlayer.w;
        this.keenPlayer.h := this.compPlayer.h;

        -- get the size of the ball
        this.ball.w := Float(this.style.Get_Image( BALL_ELEM ).Get_Width);
        this.ball.h := Float(this.style.Get_Image( BALL_ELEM ).Get_Height);

        -- determine the boundaries for the game
        this.boundsX1 := 3.0;
        this.boundsY1 := 16.0;
        this.boundsX2 := this.viewport.width - 3.0;
        this.boundsY2 := this.viewport.height - 13.0;

        -- reset the players
        this.compPlayer.x := this.boundsX1 + (this.boundsX2 - this.boundsX1) / 2.0;
        this.compPlayer.y := this.boundsY1;
        this.compPlayer.xv := 0.0;

        this.keenPlayer.x := this.compPlayer.x;
        this.keenPlayer.y := this.boundsY2;
        this.keenPlayer.xv := 0.0;

        -- reset the ball
        this.Reset_Ball;
        this.ball.yv := BALL_SPEED;   -- set the initial ball speed
    end Reset;

    ----------------------------------------------------------------------------

    procedure Reset_Ball( this : not null access Paddle_War'Class ) is
    begin
        this.ball.x := this.boundsX1 + (this.boundsX2 - this.boundsX1) / 2.0;
        this.ball.y := this.boundsY1 + (this.boundsY2 - this.boundsY1) / 2.0;
        this.ball.xv := 0.0;
        this.outOfBounds := False;
        this.nextBall := Time_Span_Last;
    end Reset_Ball;

    ----------------------------------------------------------------------------

    overriding
    procedure Tick( this : access Paddle_War; time : Tick_Time ) is
    begin
        -- move the human player's paddle
        this.keenPlayer.x := this.keenPlayer.x + this.keenPlayer.xv * Float(To_Duration( time.elapsed ));
        if this.keenPlayer.x < this.boundsX1 + this.keenPlayer.w / 2.0 then
            this.keenPlayer.x := this.boundsX1 + this.keenPlayer.w / 2.0;
        elsif this.keenPlayer.x > this.boundsX2 - this.keenPlayer.w / 2.0 then
            this.keenPlayer.x := this.boundsX2 - this.keenPlayer.w / 2.0;
        end if;

        -- move the computer player's paddle
        this.compPlayer.x := this.compPlayer.x + this.compPlayer.xv * Float(To_Duration( time.elapsed ));
        if this.outOfBounds then
            -- the ball is out of bounds, don't chase it
            this.compPlayer.xv := 0.0;
        else
            if this.ball.x >= this.compPlayer.x then
                -- chase to the right
                if this.ball.x - this.ball.w > this.compPlayer.x then
                    -- chase at top speed
                    this.compPlayer.xv := COMP_PADDLE_SPEED;
                else
                    -- try to keep up with the ball
                    this.compPlayer.xv := Float'Min( this.ball.xv, COMP_PADDLE_SPEED );
               end if;
            else
                -- chase to the left
                if this.ball.x + this.ball.w < this.compPlayer.x then
                    -- chase at top speed
                   this.compPlayer.xv := -COMP_PADDLE_SPEED;
                else
                    -- try to keep up with the ball
                    this.compPlayer.xv := Float'Max( this.ball.xv, -COMP_PADDLE_SPEED );
                end if;
            end if;
        end if;

        -- move the ball
        this.ball.y := this.ball.y + this.ball.yv * Float(To_Duration( time.elapsed ));
        this.ball.x := this.ball.x + this.ball.xv * Float(To_Duration( time.elapsed ));

        -- check if the ball bounces off the sides
        if this.ball.x < 0.0 then
            if this.ball.x - this.ball.w / 2.0 < this.boundsX1 then
                Queue_Play_Sound( "paddle_wall" );
                this.ball.xv := -this.ball.xv;
                this.ball.x := this.boundsX1 + this.ball.w / 2.0;
            end if;
        else
            if this.ball.x + this.ball.w / 2.0 > this.boundsX2 then
                Queue_Play_Sound( "paddle_wall" );
                this.ball.xv := -this.ball.xv;
                this.ball.x := this.boundsX2 - this.ball.w / 2.0;
            end if;
        end if;

        -- check of the ball goes out of bounds or hits a paddle
        if not this.outOfBounds then
            if this.ball.yv < 0.0 then
                -- check if the computer player missed it
                if this.ball.y < this.compPlayer.y then
                    Queue_Play_Sound( "paddle_score" );
                    this.keenScore := this.keenScore + 1;
                    this.outOfBounds := True;
                    this.nextBall := time.total + BALL_RESET_DELAY;
                -- check for collision with the computer player
                elsif this.ball.y - this.ball.h / 2.0 < this.compPlayer.y + this.compPlayer.h / 2.0 then        -- vertical
                    if this.ball.x + this.ball.w / 2.0 >= this.compPlayer.x - this.compPlayer.w / 2.0 and then  -- horizontal
                       this.ball.x - this.ball.w / 2.0 <= this.compPlayer.x + this.compPlayer.w / 2.0
                    then
                        -- change direction of the ball
                        Queue_Play_Sound( "paddle_hit2" );
                        this.ball.yv := -this.ball.yv;
                        this.ball.xv := BALL_SPEED * (this.ball.x - this.compPlayer.x) / ((this.ball.w + this.compPlayer.w) / 2.0);
                        this.ball.y := this.compPlayer.y + ((this.compPlayer.h + this.ball.h) / 2.0);
                    end if;
                end if;
            else
                -- check if the human player missed it
                if this.ball.y > this.keenPlayer.y then
                    Queue_Play_Sound( "paddle_miss" );
                    this.cpuScore := this.cpuScore + 1;
                    this.outOfBounds := True;
                    this.nextBall := time.total + BALL_RESET_DELAY;
                -- check for collision with the human player
                elsif this.ball.y + this.ball.h / 2.0 > this.keenPlayer.y - this.keenPlayer.h / 2.0 then        -- vertical
                    if this.ball.x + this.ball.w / 2.0 >= this.keenPlayer.x - this.keenPlayer.w / 2.0 and then  -- horizontal
                       this.ball.x - this.ball.w / 2.0 <= this.keenPlayer.x + this.keenPlayer.w / 2.0
                    then
                        -- change direction of the ball
                        Queue_Play_Sound( "paddle_hit1" );
                        this.ball.yv := -this.ball.yv;
                        this.ball.xv := BALL_SPEED * (this.ball.x - this.keenPlayer.x) / ((this.ball.w + this.keenPlayer.w) / 2.0);
                        this.ball.y := this.keenPlayer.y - ((this.keenPlayer.h + this.ball.h) / 2.0);
                    end if;
                end if;
            end if;
        end if;

        -- reset the ball if it's time
        if this.nextBall <= time.total then
            this.Reset_Ball;
        end if;
    end Tick;

begin

    Register_Style( "PaddleWar",
                    (BACKGROUND_ELEM => To_Unbounded_String( "background" ),
                     TEXT_ELEM       => To_Unbounded_String( "text" ),
                     BALL_ELEM       => To_Unbounded_String( "ball" ),
                     PADDLE_ELEM     => To_Unbounded_String( "paddle" )),
                    (BACKGROUND_ELEM => Area_Element,
                     TEXT_ELEM       => Text_Element,
                     BALL_ELEM       => Icon_Element,
                     PADDLE_ELEM     => Icon_Element),
                    (0 => To_Unbounded_String( "disabled" )) );

end Widgets.Paddle_Wars;
