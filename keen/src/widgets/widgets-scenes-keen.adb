--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;
with Events.Audio;                      use Events.Audio;
with Events.Entities.Camera_Targets;    use Events.Entities.Camera_Targets;
with Events.Game;                       use Events.Game;
with Game_Views;                        use Game_Views;
with Values.Casting;                    use Values.Casting;
with Widgets.Scenes.Cameras;            use Widgets.Scenes.Cameras;

package body Widgets.Scenes.Keen is

    package Connections is new Signals.Connections(Keen_Scene);
    use Connections;

    ----------------------------------------------------------------------------

    procedure On_Game_Paused( this : not null access Keen_Scene'Class );

    procedure On_Hidden( this : not null access Keen_Scene'Class );

    procedure On_Shown( this : not null access Keen_Scene'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Scene( view : not null access Game_Views.Game_View'Class;
                           id   : String := "" ) return A_Scene is
        this : A_Scene := new Keen_Scene;
    begin
        this.Construct( view, id );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Scene;

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this : access Keen_Scene;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String ) is
    begin
        Scene(this.all).Construct( view, id );

        this.view.PausedChanged.Connect( Slot( this, On_Game_Paused'Access ) );
        this.Shown.Connect( Slot( this, On_Shown'Access ) );
        this.Hidden.Connect( Slot( this, On_Hidden'Access ) );

        this.cam.Enable_Camera_Walls( True );
        this.cam.Enable_Loose_Tracking( True );
        this.cam.Set_Centering_Limit( 32.0 );

        this.Listen_For_Event( EVT_FOLLOW_ENTITY );
        this.Listen_For_Event( EVT_VIEW_MESSAGE );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Keen_Scene ) is
    begin
        this.view.PausedChanged.Disconnect( Slot( this, On_Game_Paused'Access ) );
        Scene(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Event( this : access Keen_Scene;
                            evt  : in out A_Event;
                            resp : out Response_Type ) is
        pragma Unreferenced( resp );

        ------------------------------------------------------------------------

        procedure Handle_Follow_Entity( evt : not null A_Follow_Entity_Event ) is
        begin
            if evt.Get_Id /= this.target then
                -- when switching between entities, Set_Target will immediately
                -- center on the new target entity.
                this.Set_Target( evt.Get_Id,
                                 boundsX1 => Float(evt.Get_Bounds_Left),
                                 boundsY1 => Float(evt.Get_Bounds_Top),
                                 boundsX2 => Float(evt.Get_Bounds_Right),
                                 boundsY2 => Float(evt.Get_Bounds_Bottom) );
            else
                -- only the camera bounds changed
                this.cam.Set_Bounds( boundsX1 => Float(evt.Get_Bounds_Left),
                                     boundsY1 => Float(evt.Get_Bounds_Top),
                                     boundsX2 => Float(evt.Get_Bounds_Right),
                                     boundsY2 => Float(evt.Get_Bounds_Bottom) );
            end if;
        end Handle_Follow_Entity;

        ------------------------------------------------------------------------

        procedure Handle_View_Message( evt : not null A_Message_Event ) is
        begin
            if evt.Get_Name = "ScrollScene" then
                this.cam.Scroll_Scene( x => evt.Get_Args.Get_Float( 1 ),
                                       y => evt.Get_Args.Get_Float( 2 ) );
            end if;
        end Handle_View_Message;

        ------------------------------------------------------------------------

    begin
        if this.enable then
            case evt.Get_Event_Id is
                when EVT_FOLLOW_ENTITY => Handle_Follow_Entity( A_Follow_Entity_Event(evt) );
                when EVT_VIEW_MESSAGE  => Handle_View_Message( A_Message_Event(evt) );
                when others            => null;
            end case;
        end if;
        Scene(this.all).Handle_Event( evt, resp );
    end Handle_Event;

    ----------------------------------------------------------------------------

    procedure On_Hidden( this : not null access Keen_Scene'Class ) is
        pragma Unreferenced( this );
    begin
        Queue_Stop_Music;
    end On_Hidden;

    ----------------------------------------------------------------------------

    procedure On_Game_Paused( this : not null access Keen_Scene'Class ) is
    begin
        if this.Get_View.Is_Paused then
            this.Get_View.Detach( this.particles );
        else
            this.Get_View.Attach( this.particles );
        end if;
    end On_Game_Paused;

    ----------------------------------------------------------------------------

    procedure On_Shown( this : not null access Keen_Scene'Class ) is
        music : Value;
    begin
        music := this.Get_World_Property( "music" );
        if Cast_String( music )'Length > 0 then
            Queue_Play_Music( Cast_String( music ) );
        end if;
    end On_Shown;

end Widgets.Scenes.Keen;
