--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings;                       use Ada.Strings;
with Allegro.Displays;                  use Allegro.Displays;
with Assets.Fonts;                      use Assets.Fonts;
--with Debugging;                         use Debugging;
with Drawing;                           use Drawing;
with Drawing.Bitmaps;                   use Drawing.Bitmaps;
with Events.Audio;                      use Events.Audio;
with Events.Game;                       use Events.Game;
with Events.World;                      use Events.World;
with Game_Views;                        use Game_Views;
with Icons;                             use Icons;
with Interfaces;                        use Interfaces;
with Values.Casting;                    use Values.Casting;
with Widget_Styles.Registry;            use Widget_Styles.Registry;

package body Widgets.Progress_Boards is

    package Connections is new Signals.Connections(Progress_Board);
    use Connections;

    LOCATION_TEXT_WIDTH : constant := 150;
    DIGIT_WIDTH         : constant := 8.0;

    ----------------------------------------------------------------------------

    procedure On_Slide_Finished( this : not null access Progress_Board'Class );

    --==========================================================================

    function Create_Progress_Board( view : not null access Game_Views.Game_View'Class ) return A_Progress_Board is
        this : A_Progress_Board := new Progress_Board;
    begin
        this.Construct( view );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Progress_Board;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Progress_Board;
                         view : not null access Game_Views.Game_View'Class ) is
    begin
        Widget(this.all).Construct( view, "", "ProgressBoard" );
        this.sigClosed.Init( this );
        this.sigOpened.Init( this );

        this.Y.Finished.Connect( Slot( this, On_Slide_Finished'Access ) );

        this.Listen_For_Event( EVT_GAME_VAR_CHANGED );
        this.Listen_For_Event( EVT_WORLD_PROPERTY_CHANGED );

        this.Visible.Set( False);
        this.Set_Focusable( False );
    end Construct;

    ----------------------------------------------------------------------------

    function Closed( this : not null access Progress_Board'Class ) return access Signal'Class is (this.sigClosed'Access);

    function Opened( this : not null access Progress_Board'Class ) return access Signal'Class is (this.sigOpened'Access);

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Content( this : access Progress_Board ) is

        ------------------------------------------------------------------------

        -- Draws a number on the board, given the location of the upper left
        -- corner of the most significant digit and the maximum number of digits
        -- to draw. If the number needs more than the maximum number of digits,
        -- the largest number possible will be drawn using the maximum number of
        -- digits. (ex: digits = 2, n = 100, draws "99")
        procedure Draw_Number( number : Natural;
                               x, y   : Float;
                               dgts   : Positive ) is
            number2     : Unsigned_64 := Unsigned_64(number);
            n           : Unsigned_64;
            exp         : Unsigned_64;
            digit       : Natural;
        begin
            if number2 >= 10 ** dgts then
                number2 := 10 ** dgts - 1;
            end if;
            n := number2;

            for i in reverse 0..dgts-1 loop
                exp := 10 ** i;
                if exp <= number2 or else i = 0 then
                    digit := Natural((n - (n mod exp)) / exp);
                    Draw_Bitmap( this.style.Get_Image( DIGIT0_ELEM + digit ).Get_Bitmap,
                                 x + Float((dgts - 1) - i) * DIGIT_WIDTH, y, 0.0,
                                 Lighten( this.style.Get_Tint( DIGIT0_ELEM + digit ),
                                          this.style.Get_Shade( DIGIT0_ELEM + digit ) ) );
                    n := n - Unsigned_64(digit) * exp;
                end if;
            end loop;
        end Draw_Number;

        ------------------------------------------------------------------------

        font     : constant A_Font := this.style.Get_Font( TEXT_ELEM );
        color    : Allegro_Color;
        loc1     : Unbounded_String;
        loc2     : Unbounded_String;
        locSplit : Integer;
        textX    : Float;
        textY    : Float;
    begin
        Al_Hold_Bitmap_Drawing( True );

        Draw_Bitmap( this.style.Get_Image( BACKGROUND_ELEM ).Get_Bitmap,
                     0.0, 0.0, 0.0,
                     Lighten( this.style.Get_Tint( BACKGROUND_ELEM ),
                              this.style.Get_Shade( BACKGROUND_ELEM ) ) );

        -- draw the location text
        locSplit := Length( this.location ) + 1;
        while font.Text_Length( Slice( this.location, 1, locSplit - 1 ) ) > LOCATION_TEXT_WIDTH loop
            locSplit := Index( this.location, " ", locSplit - 1, Backward );
             if locSplit < 1 then
                 locSplit := Length( this.location ) + 1;
                 exit;
             end if;
        end loop;

        color := Lighten( this.style.Get_Color( TEXT_ELEM ), this.style.Get_Shade( TEXT_ELEM ) );
        if locSplit > Length( this.location ) then
            loc1 := this.location;
            textX := 40.0 + 76.0 - Float(font.Text_Length( To_String( loc1 ) )) / 2.0;
            textY := 40.0 + 8.0 - Float(font.Ascent) / 2.0;
            font.Draw_String( To_String( this.location ), textX, textY, color );
        else
            loc1 := Unbounded_Slice( this.location, 1, locSplit - 1);
            loc2 := Unbounded_Slice( this.location, locSplit + 1, Length( this.location ) );

            textX := 40.0 + 76.0 - Float(font.Text_Length( To_String( loc1 ) )) / 2.0;
            textY := 40.0 + 4.0 - Float(font.Ascent) / 2.0;
            font.Draw_String( To_String( loc1 ), textX, textY, color );

            textX := 40.0 + 76.0 - Float(font.Text_Length( To_String( loc2 ) )) / 2.0;
            textY := 48.0 + 4.0 - Float(font.Ascent) / 2.0;
            font.Draw_String( To_String( loc2 ), textX, textY, color );
        end if;

        Draw_Number( this.points, 37.0, 72.0, dgts => 8 );
        Draw_Number( this.nextLife, 133.0, 72.0, dgts => 8 );
        Draw_Number( this.ammo, 173.0, 112.0, dgts => 3 );
        Draw_Number( this.lives, 85.0, 128.0, dgts => 2 );
        Draw_Number( this.drops, 181.0, 128.0, dgts => 2 );

        for i in 0..this.ancients-1 loop
            Draw_Bitmap( this.style.Get_Image( ANCIENT_ELEM ).Get_Bitmap,
                         36.0 + Float(i * 8), 96.0, 0.0,
                         Lighten( this.style.Get_Tint( ANCIENT_ELEM ),
                                  this.style.Get_Shade( ANCIENT_ELEM ) ) );
        end loop;

        if this.redKey then
            Draw_Bitmap( this.style.Get_Image( REDKEY_ELEM ).Get_Bitmap,
                         76.0, 114.0, 0.0,
                         Lighten( this.style.Get_Tint( REDKEY_ELEM ),
                                  this.style.Get_Shade( REDKEY_ELEM ) ) );
        end if;
        if this.blueKey then
            Draw_Bitmap( this.style.Get_Image( BLUEKEY_ELEM ).Get_Bitmap,
                         84.0, 114.0, 0.0,
                         Lighten( this.style.Get_Tint( BLUEKEY_ELEM ),
                                  this.style.Get_Shade( BLUEKEY_ELEM ) ) );
        end if;
        if this.yellowKey then
            Draw_Bitmap( this.style.Get_Image( YELLOWKEY_ELEM ).Get_Bitmap,
                         92.0, 114.0, 0.0,
                         Lighten( this.style.Get_Tint( YELLOWKEY_ELEM ),
                                  this.style.Get_Shade( YELLOWKEY_ELEM ) ) );
        end if;
        if this.greenKey then
            Draw_Bitmap( this.style.Get_Image( GREENKEY_ELEM ).Get_Bitmap,
                         100.0, 114.0, 0.0,
                         Lighten( this.style.Get_Tint( GREENKEY_ELEM ),
                                  this.style.Get_Shade( GREENKEY_ELEM ) ) );
        end if;

        -- draw the wetsuit text
        if this.scuba then
            textX := Float'Floor( 36.0 + 33.0 - Float(font.Text_Length( "Wetsuit" )) / 2.0 );
            textY := Float'Floor( 143.0 + 5.0 - Float(font.Ascent) / 2.0 );
            font.Draw_String( "Wetsuit", textX, textY, color );
        else
            textX := Float'Floor( 36.0 + 33.0 - Float(font.Text_Length( "???" )) / 2.0 );
            textY := Float'Floor( 143.0 + 5.0 - Float(font.Ascent) / 2.0 );
            font.Draw_String( "???", textX, textY, color );
        end if;

        Al_Hold_Bitmap_Drawing( False );
    end Draw_Content;

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Height( this : access Progress_Board ) return Float
    is (Float(this.style.Get_Image( BACKGROUND_ELEM ).Get_Height));

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Width( this : access Progress_Board ) return Float
    is (Float(this.style.Get_Image( BACKGROUND_ELEM ).Get_Width));

    ----------------------------------------------------------------------------

    overriding
    procedure Handle_Event( this : access Progress_Board;
                            evt  : in out A_Event;
                            resp : out Response_Type ) is
        pragma Unreferenced( resp );
    begin
        if evt.Get_Event_Id = EVT_GAME_VAR_CHANGED then
            declare
                var : constant String := A_Game_Var_Changed_Event(evt).Get_Var;
                val : constant Value := A_Game_Var_Changed_Event(evt).Get_Value;
            begin
                if var = "points" then
                    this.points := val.To_Int;
                elsif var = "drops" then
                    this.drops := val.To_Int;
                elsif var = "ammo" then
                    this.ammo := val.To_Int;
                elsif var = "lives" then
                    this.lives := val.To_Int;
                elsif var = "nextLife" then
                    this.nextLife := val.To_Int;
                elsif var = "ancients" then
                    this.ancients := val.To_Int;
                elsif var = "blueGem" then
                    this.blueKey := val.To_Int > 0;
                elsif var = "greenGem" then
                    this.greenKey := val.To_Int > 0;
                elsif var = "redGem" then
                    this.redKey := val.To_Int > 0;
                elsif var = "yellowGem" then
                    this.yellowKey := val.To_Int > 0;
                elsif var = "scuba" then
                    this.scuba := val.To_Int > 0;
                end if;
            end;
        elsif evt.Get_Event_Id = EVT_WORLD_PROPERTY_CHANGED then
            declare
                name : constant String := A_World_Property_Event(evt).Get_Property_Name;
                val  : constant Value := A_World_Property_Event(evt).Get_Value;
            begin
                if name = "title" then
                    this.location := Cast_Unbounded_String( val );
                end if;
            end;
        end if;
    end Handle_Event;

    ----------------------------------------------------------------------------

    function Is_Open( this : not null access Progress_Board'Class ) return Boolean
    is (this.Is_Visible);

    ----------------------------------------------------------------------------

    procedure On_Slide_Finished( this : not null access Progress_Board'Class ) is
    begin
        if this.slideDir < 0 then
            this.Visible.Set( False );
            this.Closed.Emit;
        end if;
        this.slideDir := 0;
    end On_Slide_Finished;

    ----------------------------------------------------------------------------

    procedure Show( this : not null access Progress_Board'Class; enabled : Boolean ) is
    begin
        if enabled then
            -- slide down
            if this.slideDir /= 1 then
                if not this.Is_Visible then
                    this.Y.Set( 0.0, SLIDE_DURATION - this.Y.Remaining, Quad_Out );
                    this.Visible.Set( True );
                    this.Opened.Emit;
                end if;
                this.slideDir := 1;
                Queue_Play_Sound( "menu_open" );
            end if;
        elsif this.Is_Visible then
            -- slide up
            if this.slideDir /= -1 then
                this.slideDir := -1;
                this.Y.Set( -this.Height.Get, SLIDE_DURATION - this.Y.Remaining, Quad_In );
                Queue_Play_Sound( "menu_close" );
            end if;
        end if;
    end Show;

    ----------------------------------------------------------------------------

    procedure Toggle( this : not null access Progress_Board'Class ) is
    begin
        this.Show( not this.Is_Open );
    end Toggle;

begin

    Register_Style( "ProgressBoard",
                    (BACKGROUND_ELEM => To_Unbounded_String( "background" ),
                     DIGIT0_ELEM     => To_Unbounded_String( "digit0" ),
                     DIGIT1_ELEM     => To_Unbounded_String( "digit1" ),
                     DIGIT2_ELEM     => To_Unbounded_String( "digit2" ),
                     DIGIT3_ELEM     => To_Unbounded_String( "digit3" ),
                     DIGIT4_ELEM     => To_Unbounded_String( "digit4" ),
                     DIGIT5_ELEM     => To_Unbounded_String( "digit5" ),
                     DIGIT6_ELEM     => To_Unbounded_String( "digit6" ),
                     DIGIT7_ELEM     => To_Unbounded_String( "digit7" ),
                     DIGIT8_ELEM     => To_Unbounded_String( "digit8" ),
                     DIGIT9_ELEM     => To_Unbounded_String( "digit9" ),
                     ANCIENT_ELEM    => To_Unbounded_String( "ancient" ),
                     REDKEY_ELEM     => To_Unbounded_String( "redKey" ),
                     BLUEKEY_ELEM    => To_Unbounded_String( "blueKey" ),
                     YELLOWKEY_ELEM  => To_Unbounded_String( "yellowKey" ),
                     GREENKEY_ELEM   => To_Unbounded_String( "greenKey" ),
                     TEXT_ELEM       => To_Unbounded_String( "text" )),
                    (BACKGROUND_ELEM => Area_Element,
                     DIGIT0_ELEM     => Icon_Element,
                     DIGIT1_ELEM     => Icon_Element,
                     DIGIT2_ELEM     => Icon_Element,
                     DIGIT3_ELEM     => Icon_Element,
                     DIGIT4_ELEM     => Icon_Element,
                     DIGIT5_ELEM     => Icon_Element,
                     DIGIT6_ELEM     => Icon_Element,
                     DIGIT7_ELEM     => Icon_Element,
                     DIGIT8_ELEM     => Icon_Element,
                     DIGIT9_ELEM     => Icon_Element,
                     ANCIENT_ELEM    => Icon_Element,
                     REDKEY_ELEM     => Icon_Element,
                     BLUEKEY_ELEM    => Icon_Element,
                     YELLOWKEY_ELEM  => Icon_Element,
                     GREENKEY_ELEM   => Icon_Element,
                     TEXT_ELEM       => Text_Element),
                    (0 => To_Unbounded_String( "disabled" )) );

end Widgets.Progress_Boards;
