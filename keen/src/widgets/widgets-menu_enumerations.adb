--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Assets.Fonts;                      use Assets.Fonts;
--with Debugging;                         use Debugging;
with Drawing.Bitmaps;                   use Drawing.Bitmaps;
with Drawing.Primitives;                use Drawing.Primitives;
with Drawing.Tiles;                     use Drawing.Tiles;
with Game_Views;                        use Game_Views;
with Gamepads;                          use Gamepads;
with Keyboard;                          use Keyboard;
with Styles;                            use Styles;
with Widget_Styles.Registry;            use Widget_Styles.Registry;
with Support;                           use Support;
with Values.Strings;                    use Values.Strings;

package body Widgets.Menu_Enumerations is

    package Connections is new Signals.Connections(Menu_Enumeration);
    use Connections;

    package Gamepad_Connections is new Signals.Gamepads.Connections(Menu_Enumeration);
    use Gamepad_Connections;

    package Key_Connections is new Signals.Keys.Connections(Menu_Enumeration);
    use Key_Connections;

    ----------------------------------------------------------------------------

    procedure Ignore_Key( this : not null access Menu_Enumeration'Class ) is null;

    procedure On_Gamepad_Pressed( this : not null access Menu_Enumeration'Class );

    -- Called when the left/right axis of the gamepad changes.
    procedure On_Stick_X( this      : not null access Menu_Enumeration'Class;
                          gamepadId : Integer;
                          position  : Float );

    procedure On_Style_Changed( this : not null access Menu_Enumeration'Class );

    -- Changes the enum selection to the previous choice.
    procedure Select_Prev( this : not null access Menu_Enumeration'Class );

    -- Changes the enum selection to the next choice.
    procedure Select_Next( this : not null access Menu_Enumeration'Class );

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create_Menu_Enum( view : not null access Game_Views.Game_View'Class;
                               id   : String := "" ) return A_Menu_Enumeration is
        this : A_Menu_Enumeration := new Menu_Enumeration;
    begin
        this.Construct( view, id );
        return this;
    exception
        when others =>
            Delete( A_Widget(this) );
            raise;
    end Create_Menu_Enum;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Menu_Enumeration;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String ) is
    begin
        Widget(this.all).Construct( view, id, "MenuEnum" );
        this.sigAccepted.Init( this );

        this.StyleChanged.Connect( Slot( this, On_Style_Changed'Access ) );

        -- change option
        this.GamepadMoved.Connect( Slot( this, On_Stick_X'Access, AXIS_LXN ) );
        this.GamepadMoved.Connect( Slot( this, On_Stick_X'Access, AXIS_LXP ) );
        this.GamepadPressed.Connect( Slot( this, On_Gamepad_Pressed'Access, BUTTON_SOUTH ) );
        this.KeyPressed.Connect( Slot( this, Select_Prev'Access, ALLEGRO_KEY_LEFT, (others=>No) ) );
        this.KeyPressed.Connect( Slot( this, Select_Next'Access, ALLEGRO_KEY_RIGHT, (others=>No) ) );

        -- prevent the default tab-focus-switching behavior
        this.KeyTyped.Connect( Slot( this, Ignore_Key'Access, ALLEGRO_KEY_TAB, (SHIFT=>Either, others=>No) ) );

        this.Set_Icon( this.style.Get_Image( ICON_ELEM ) );
    end Construct;

    ----------------------------------------------------------------------------

    function Accepted( this : not null access Menu_Enumeration'Class ) return access Signal'Class is (this.sigAccepted'Access);

    ----------------------------------------------------------------------------

    procedure Add_Option( this : not null access Menu_Enumeration'Class;
                          text : String ) is
    begin
        this.options.Append( text );
        if this.index = 0 then
            this.Set_Index( 1, notify => False );
        end if;
    end Add_Option;

    ----------------------------------------------------------------------------

    overriding
    procedure Draw_Content( this : access Menu_Enumeration ) is
        state         : constant Widget_State := this.Get_Visual_State;
        text          : constant String := this.Get_Text;
        align         : constant Align_Type := this.style.Get_Align( BACKGROUND_ELEM, state );
        icon          : constant Icon_Type := this.style.Get_Image( ICON_ELEM, state );
        color         : Allegro_Color;
        font          : A_Font;
        iconWidth     : Float := 0.0;
        iconHeight    : Float := 0.0;
        textWidth     : Float := 0.0;
        textHeight    : Float := 0.0;
        contentWidth  : Float := 0.0;
        contentHeight : Float := 0.0;
        contentX      : Float;
        contentY      : Float;
        iconX, iconY  : Float;
        textX, textY  : Float;
    begin
        -- - - - Calculate Positions - - - --

        -- calculate icon size --
        iconWidth := this.style.Get_Width( ICON_ELEM, state );
        if iconWidth = 0.0 then
            iconWidth := Float(icon.Get_Width);
        end if;
        iconWidth := Float'Min( iconWidth, this.viewport.width );
        iconHeight := this.style.Get_Height( ICON_ELEM, state );
        if iconHeight = 0.0 then
            iconHeight := Float(icon.Get_Height);
        end if;
        iconHeight := Float'Min( iconHeight, this.viewport.height );

        -- calculate text size --
        if text'Length > 0 then
            font := this.style.Get_Font( TEXT_ELEM, state );
            textWidth := Float(font.Text_Length( text ));
            textHeight := Float(font.Line_Height);
        end if;

        -- calculate total content size --
        if iconWidth > 0.0 and textWidth > 0.0 then
            contentWidth := iconWidth + this.style.Get_Spacing( BACKGROUND_ELEM, state ) + textWidth;
        elsif textWidth > 0.0 then
            contentWidth := textWidth;
        else
            contentWidth := iconWidth;
        end if;
        contentHeight := Float'Max( iconHeight, textHeight );

        -- calculate alignment --
        Align_Rect( align,
                    this.viewport.width - (this.style.Get_Pad_Left( BACKGROUND_ELEM, state ) + this.style.Get_Pad_Right( BACKGROUND_ELEM, state )),
                    this.viewport.height - (this.style.Get_Pad_Top( BACKGROUND_ELEM, state ) + this.style.Get_Pad_Bottom( BACKGROUND_ELEM, state )),
                    contentWidth, contentHeight,
                    contentX, contentY );
        contentX := this.style.Get_Pad_Left( BACKGROUND_ELEM, state ) + contentX;
        contentY := this.style.Get_Pad_Top( BACKGROUND_ELEM, state ) + contentY;
        iconX := Float'Rounding( contentX + this.style.Get_Offset_X( ICON_ELEM, state ) );
        iconY := Float'Rounding( contentY + Align_Vertical( align, contentHeight, iconHeight ) + this.style.Get_Offset_Y( ICON_ELEM, state ) );
        textX := contentX + (contentWidth - textWidth) + this.style.Get_Offset_X( TEXT_ELEM, state );
        textY := contentY + Align_Vertical( align, contentHeight, textHeight ) + this.style.Get_Offset_Y( TEXT_ELEM, state );

        -- - - - Draw Content - - - --

        -- draw the background --
        color := this.style.Get_Color( BACKGROUND_ELEM, state );
        if color /= Transparent then
            Rectfill( 0.0, 0.0, this.viewport.width, this.viewport.height,
                      Lighten( color, this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );
        else
            Draw_Tile_Filled( this.style.Get_Image( BACKGROUND_ELEM, state ).Get_Tile,
                              0.0, 0.0, 0.0,
                              this.viewport.width, this.viewport.height,
                              Lighten( this.style.Get_Tint( BACKGROUND_ELEM, state ),
                                       this.style.Get_Shade( BACKGROUND_ELEM, state ) ) );
        end if;

        -- draw the content --
        if icon.Get_Bitmap /= null then
            Draw_Bitmap_Stretched( icon.Get_Bitmap,
                                   iconX, iconY, 0.0,
                                   iconWidth, iconHeight,
                                   Fit,
                                   0.0,
                                   Lighten( this.style.Get_Tint( ICON_ELEM, state ),
                                            this.style.Get_Shade( ICON_ELEM, state ) ) );
        end if;
        if textWidth > 0.0 then
            font.Draw_String( text,
                              textX, textY,
                              Lighten( this.style.Get_Color( TEXT_ELEM, state ),
                                       this.style.Get_Shade( TEXT_ELEM, state ) ) );
        end if;
    end Draw_Content;

    ----------------------------------------------------------------------------

    procedure Enable_Wrap( this : not null access Menu_Enumeration'Class; enabled : Boolean ) is
    begin
        this.wrapEnabled := enabled;
    end Enable_Wrap;

    ----------------------------------------------------------------------------

    function Get_Count( this : not null access Menu_Enumeration'Class ) return Natural is (Integer(this.options.Length));

    ----------------------------------------------------------------------------

    function Get_Index( this : not null access Menu_Enumeration'Class ) return Natural is (this.index);

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Height( this : access Menu_Enumeration ) return Float is
        padTop     : constant Float := this.style.Get_Pad_Top( BACKGROUND_ELEM );
        padBottom  : constant Float := this.style.Get_Pad_Bottom( BACKGROUND_ELEM );
        iconHeight : Float := 0.0;
        textHeight : Float := 0.0;
    begin
        iconHeight := (if this.style.Get_Height( ICON_ELEM ) > 0.0
                       then this.style.Get_Height( ICON_ELEM )
                       else Float(this.style.Get_Image( ICON_ELEM ).Get_Height));

        -- Note: Reserve "Line Height" in space, not "Ascent"
        textHeight := Float(this.style.Get_Font( TEXT_ELEM ).Line_Height);

        return padTop + Float'Max( iconHeight, textHeight ) + padBottom;
    end Get_Min_Height;

    ----------------------------------------------------------------------------

    overriding
    function Get_Min_Width( this : access Menu_Enumeration ) return Float is
        padLeft   : constant Float := this.style.Get_Pad_Left( BACKGROUND_ELEM );
        padRight  : constant Float := this.style.Get_Pad_Right( BACKGROUND_ELEM );
        spacing   : constant Float := this.style.Get_Spacing( BACKGROUND_ELEM );
        iconWidth : Float := 0.0;
        textWidth : Float := 0.0;
    begin
        iconWidth := (if this.style.Get_Width( ICON_ELEM ) > 0.0
                      then this.style.Get_Width( ICON_ELEM )
                      else Float(this.style.Get_Image( ICON_ELEM ).Get_Width));

        for i in 1..Integer(this.options.Length) loop
            textWidth := Float'Max( textWidth,
                                    Float(this.style.Get_Font( TEXT_ELEM ).Text_Length( this.options.Element( i ) )) );
        end loop;

        if iconWidth > 0.0 and then textWidth > 0.0 then
            return padLeft + iconWidth + spacing + textWidth + padRight;
        end if;

        -- one of these widths is zero
        return padLeft + Float'Max( iconWidth, textWidth ) + padRight;
    end Get_Min_Width;

    ----------------------------------------------------------------------------

    function Get_Text( this : not null access Menu_Enumeration'Class ) return String
    is (if this.index > 0 and then this.index <= Natural(this.options.Length) then this.options.Element( this.index ) else "");

    ----------------------------------------------------------------------------

    overriding
    function Get_Visual_State( this : access Menu_Enumeration ) return Widget_State is
    (
           (if not this.Is_Enabled then DISABLED_STATE else 0)
        or (if this.Is_Focused     then FOCUS_STATE    else 0)
    );

    ----------------------------------------------------------------------------

    procedure On_Gamepad_Pressed( this : not null access Menu_Enumeration'Class ) is
    begin
        if this.index < Natural(this.options.Length) then
            this.Set_Index( this.index + 1 );
        else
            -- always wrap around for a gamepad button
            this.Set_Index( 1 );
        end if;
    end On_Gamepad_Pressed;

    ----------------------------------------------------------------------------

    procedure On_Stick_X( this      : not null access Menu_Enumeration'Class;
                          gamepadId : Integer;
                          position  : Float ) is
        pragma Unreferenced( gamepadId );
    begin
        if this.stickCentered then
            if position < -AXIS_SELECTED_THRESHOLD then
                this.Select_Prev;
            elsif position > AXIS_SELECTED_THRESHOLD then
                this.Select_Next;
            end if;
        end if;

        if (abs position) > AXIS_SELECTED_THRESHOLD then
            this.stickCentered := False;
        elsif (abs position) < AXIS_CENTERED_THRESHOLD then
            this.stickCentered := True;
        end if;
    end On_Stick_X;

    ----------------------------------------------------------------------------

    procedure On_Style_Changed( this : not null access Menu_Enumeration'Class ) is
    begin
        -- don't allow a style that doesn't define an icon to remove the
        -- existing one. set the icon back again.
        if this.icon /= NO_ICON and then this.style.Get_Image( ICON_ELEM ) = NO_ICON then
            this.Set_Icon( this.icon );                -- keep the same icon
        else
            this.Set_Icon( this.style.Get_Image( ICON_ELEM ) );
        end if;
    end On_Style_Changed;

    ----------------------------------------------------------------------------

    procedure Select_Next( this : not null access Menu_Enumeration'Class ) is
    begin
        if this.index < Natural(this.options.Length) then
            this.Set_Index( this.index + 1 );
        elsif this.wrapEnabled then
            this.Set_Index( 1 );
        end if;
    end Select_Next;

    ----------------------------------------------------------------------------

    procedure Select_Prev( this : not null access Menu_Enumeration'Class ) is
    begin
        if this.index > 1 then
            this.Set_Index( this.index - 1 );
        elsif this.wrapEnabled then
            this.Set_Index( Natural(this.options.Length) );
        end if;
    end Select_Prev;

    ----------------------------------------------------------------------------

    procedure Set_Icon( this : not null access Menu_Enumeration'Class;
                        icon : Icon_Type ) is
        tileId : constant Natural := icon.Get_Tile.Get_Id;
    begin
        this.icon := icon;
        this.style.Set_Property( ICON_ELEM, STYLE_IMAGE, DEFAULT_STATE,
                                 Create( (if tileId > 0
                                          then icon.Get_Library.Get.Get_Name & ":" & Image( tileId )
                                          else "") ) );
        this.Update_Geometry;
    end Set_Icon;

    ----------------------------------------------------------------------------

    procedure Set_Index( this   : not null access Menu_Enumeration'Class;
                         index  : Positive;
                         notify : Boolean := True ) is
    begin
        if index /= this.index and then index <= Natural(this.options.Length) then
            this.index := index;
            if notify then
                this.Accepted.Emit;
            end if;
        end if;
    end Set_Index;

    ----------------------------------------------------------------------------

    overriding
    procedure Tick( this : access Menu_Enumeration; time : Tick_Time ) is
        state : constant Widget_State := this.Get_Visual_State;
    begin
        this.style.Update_Animation( ICON_ELEM, state, time.total );
    end Tick;

begin

    Register_Style( "MenuEnum",
                    (BACKGROUND_ELEM => To_Unbounded_String( "background" ),
                     ICON_ELEM       => To_Unbounded_String( "icon" ),
                     TEXT_ELEM       => To_Unbounded_String( "text" )),
                    (BACKGROUND_ELEM => Area_Element,
                     ICON_ELEM       => Icon_Element,
                     TEXT_ELEM       => Text_Element),
                    (0 => To_Unbounded_String( "disabled" ),
                     1 => To_Unbounded_String( "focus" )) );

end Widgets.Menu_Enumerations;

