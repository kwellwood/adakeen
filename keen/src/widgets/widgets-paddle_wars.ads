--
-- Copyright (c) 2012-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Widgets.Paddle_Wars is

    -- A Paddle War widget implements the pong-like Paddle War game in the Keen
    -- menu. The player plays against a computer opponent. The game logic is
    -- implemented as an Animated widget, using the Tick procedure to update the
    -- game state. There is no maximum score.
    type Paddle_War is new Widget and Animated with private;
    type A_Paddle_War is access all Paddle_War'Class;

    -- Creates a new Paddle War widget with a new game ready to play.
    function Create_Paddle_War( view    : not null access Game_Views.Game_View'Class;
                                id      : String := "" ) return A_Paddle_War;
    pragma Postcondition( Create_Paddle_War'Result /= null );

    -- Manually resets the Paddle War game. This can only be called after the
    -- widget has been parented. The game will also automatically reset when the
    -- widget becomes visible.
    procedure Reset( this : not null access Paddle_War'Class );

private

    BACKGROUND_ELEM : constant := 1;
    TEXT_ELEM       : constant := 2;
    BALL_ELEM       : constant := 3;
    PADDLE_ELEM     : constant := 4;

    DISABLED_STATE : constant Widget_State := 2**0;

    ----------------------------------------------------------------------------

    type Entity_Rec is
        record
            x, y   : Float := 0.0;
            xv, yv : Float := 0.0;
            w, h   : Float := 0.0;
            w2, h2 : Float := 0.0;    -- half the width and height
        end record;

    type Paddle_War is new Widget and Animated with
        record
            boundsX1,
            boundsY1,
            boundsX2,
            boundsY2     : Float := 0.0;

            ball,
            compPlayer,
            keenPlayer   : Entity_Rec;

            cpuScore,
            keenScore    : Natural := 0;

            outOfBounds  : Boolean := False;          -- ball is out of bounds
            nextBall     : Time_Span := Time_Span_Last;  -- time of next ball reset
        end record;

    procedure Construct( this : access Paddle_War;
                         view : not null access Game_Views.Game_View'Class;
                         id   : String );

    procedure Draw_Content( this : access Paddle_War );

    function Get_Min_Height( this : access Paddle_War ) return Float;

    function Get_Min_Width( this : access Paddle_War ) return Float;

    procedure Tick( this : access Paddle_War; time : Tick_Time );

end Widgets.Paddle_Wars;
