--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Applications.Gui.Games.Keen is

    function Create_Keen_App return A_Application;

    APP_NAME    : constant String := "keen";
    APP_COMPANY : constant String := "Kevin Wellwood";

private

    type Keen_Application is new Game_Application with null record;

    procedure On_Initialize( this : access Keen_Application );

end Applications.Gui.Games.Keen;
