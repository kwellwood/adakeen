--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

--with Debugging;                         use Debugging;

package body Sessions.Keen is

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Create return A_Session is
        this : constant A_Session := new Keen_Session;
    begin
        Keen_Session(this.all).Construct;
        return this;
    end Create;

end Sessions.Keen;
