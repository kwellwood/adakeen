--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Text_IO;                       use Ada.Text_IO;
with Allegro;
with Applications;                      use Applications;
with Applications.Gui.Games.Keen;       use Applications.Gui.Games.Keen;
with Debugging;                         use Debugging;
with Debugging.CLI;
with Version;

package body Main is

    procedure Display_Usage is
    begin
        New_Line;
        Put_Line( APP_NAME &
                  " v" & Version.Release &
                  " - " & APP_COMPANY & " " &
                  "Copyright (C) " & Version.Copyright_Year );
        New_Line;
        Put_Line( "Usage:" );
        Put_Line( "  " & APP_NAME & " [switches]" );
        New_Line;
        Put_Line( "Where optional [switches] are:" );
        Put_Line( "  -DV=<verbosity>           (Debugging Verbosity)" );
        Put_Line( "  -DV<channel>=<verbosity>  (Debugging Verbosity per channel)" );
        Put_Line( "    Where:" );
        Put_Line( "      <verbosity> is the verbosity level:" );
        Put_Line( "        e : Errors only (default)" );
        Put_Line( "        w : Warnings and errors only" );
        Put_Line( "        i : All info, warnings and errors" );
        Put_Line( "      <channel> is the name of a specific debugging channel:" );
        Put_Line( "        log" );
        Put_Line( "        audio" );
        Put_Line( "        entity" );
        Put_Line( "        script" );
        Put_Line( "        events" );
        Put_Line( "        game" );
        Put_Line( "        gui" );
        Put_Line( "        init" );
        Put_Line( "        input" );
        Put_Line( "        physics" );
        Put_Line( "        prefs" );
        Put_Line( "        procs" );
        Put_Line( "        res" );
        Put_Line( "        tiles" );
        Put_Line( "        view" );
        Put_Line( "        app" );
        Put_Line( "        world" );
        Put_Line( "        font" );
        Put_Line( "        particles" );
        New_Line;
        Put_Line( "  -DD=<decoration>         (Debugging Decoration)" );
        Put_Line( "    Where:" );
        Put_Line( "      <decoration> is the level of decoration for debug output:" );
        Put_Line( "        0 : No decoration" );
        Put_Line( "        1 : Prefixed with current task" );
        Put_Line( "        2 : Prefixed with calling source unit and current task" );
        New_Line;
        Put_Line( "  -DF=<filename>           (Debugging File)" );
        Put_Line( "    Where:" );
        Put_Line( "      <filename> is the file name or path to write debugging information." );
        Put_Line( "        Debugging output will be appended to the file. If no path is given," );
        Put_Line( "        the file will be written relative to the application's configuration" );
        Put_Line( "        directory." );
        New_Line;
        Put_Line( "  -log                     (Open a logging window)" );
        New_Line;
        Put_Line( "  -level <filename>        (Play a level immediately)" );
        Put_Line( "    Where:" );
        Put_Line( "      <filename> is the file name or path of a .world file to play." );
        New_Line;
    end Display_Usage;

    ----------------------------------------------------------------------------

    procedure Main is
        app  : A_Application;
        stat : Integer := 0;
    begin
        -- Display usage and quit
        for i in 1..Argument_Count loop
            if Argument( i ) = "?" or else
               Argument( i ) = "-?" or else
               Argument( i ) = "/?" or else
               Argument( i ) = "-help" or else
               Argument( i ) = "/help"
            then
                Display_Usage;
                Set_Exit_Status( Exit_Status(0) );
                return;
            end if;
        end loop;

        app := Create_Keen_App;
        stat := app.Run;
        Delete( app );

        Set_Exit_Status( Exit_Status(stat) );
    exception
        when e : others =>
            Dbg( "Exception in Main: ...", D_DEV, Error );
            Dbg( e, D_DEV, Error );
            Set_Exit_Status( Failure );
    end Main;

begin
    Allegro.Set_Ada_Main( Main'Access );
    Debugging.CLI.Parse_Arguments;
end Main;
