--
-- Copyright (c) 2012-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Widgets.Aspect_Layouts;            use Widgets.Aspect_Layouts;
with Widgets.Game_Screens;              use Widgets.Game_Screens;
with Widgets.Screen_Managers;           use Widgets.Screen_Managers;

package Game_Views.Keen is

    type Keen_View is new Game_View with private;
    type A_Keen_View is access all Keen_View'Class;

    function Create return A_Game_View;

    -- Enables/disables the widescreen aspect ratio.
    procedure Enable_Widescreen( this    : not null access Keen_View'Class;
                                 enabled : Boolean );

    -- Loads a saved game from the main menu, aborting a game on success, if one
    -- is currently in progress.
    procedure Load_Saved_Game( this : not null access Keen_View'Class );

    -- Pauses or resumes the game, in the context of a menu or dialog on the
    -- screen. For every pause by a menu, there must be a corresponding resume
    -- as a menu.
    --
    -- Nested menus may all pause the game, so a menu pause count is preserved
    -- and tracked by this procedure. The first time a menu calls
    -- Pause_By_Menu(True), the game play will be paused (if it wasn't already
    -- paused by the player.) When the same menu calls the corresponding
    -- Pause_By_Menu(False), the menu pause count is decremented. When the count
    -- reaches zero, play will resume (again, if it wasn't already paused by the
    -- player.)
    procedure Pause_By_Menu( this    : not null access Keen_View'Class;
                             enabled : Boolean );

    -- Pauses or resumes the game, in the context of the player. If game has
    -- already been paused by a menu (via Pause_By_Menu) then the player will
    -- not be allowed to pause or resume play.
    procedure Pause_By_Player( this    : not null access Keen_View'Class;
                               enabled : Boolean );

    -- Quits the application, exiting to the desktop.
    procedure Quit_App( this : not null access Keen_View'Class );

    -- Saves the current game session to disk from the main menu.
    procedure Save_Game_To_Disk( this : not null access Keen_View'Class );

    -- Starts a new game from the main menu, aborting a game if one is currently
    -- in progress.
    procedure Start_New_Game( this : not null access Keen_View'Class );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    MIN_WINDOW_WIDTH  : constant := 320;
    MIN_WINDOW_HEIGHT : constant := 200;

    WIDE_WINDOW_WIDTH  : constant := 352;
    WIDE_WINDOW_HEIGHT : constant := 198;

    -- Semantic actions taken by the user. These are all possible inputs for the
    -- game, which can be mapped to any set of keys and game controller buttons.
    --
    INPUT_UP       : constant Input_Action :=  0;       -- move/look up
    INPUT_DOWN     : constant Input_Action :=  1;       -- move/look down
    INPUT_LEFT     : constant Input_Action :=  2;       -- move left
    INPUT_RIGHT    : constant Input_Action :=  3;       -- move right
    INPUT_SHOOT    : constant Input_Action :=  4;       -- shoot stunner gun
    INPUT_JUMP     : constant Input_Action :=  5;       -- jump up
    INPUT_POGO     : constant Input_Action :=  6;       -- toggle pogostick
    INPUT_PROGRESS : constant Input_Action :=  7;       -- toggle progress board
    INPUT_PAUSE    : constant Input_Action :=  8;       -- pause/resume play
    INPUT_BACK     : constant Input_Action :=  9;       -- exit back
    INPUT_CONSOLE  : constant Input_Action := 10;       -- open/close console

    PLAYER_INPUTS : constant Input_Actions :=
    (
        INPUT_UP,
        INPUT_DOWN,
        INPUT_LEFT,
        INPUT_RIGHT,
        INPUT_SHOOT,
        INPUT_JUMP,
        INPUT_POGO,
        INPUT_PROGRESS,
        INPUT_PAUSE,
        INPUT_CONSOLE,
        INPUT_BACK
    );

    AXIS_INPUTS : constant Input_Actions :=
    (
        INPUT_UP,
        INPUT_DOWN,
        INPUT_LEFT,
        INPUT_RIGHT
    );

    -- Timing constants shared by multiple screens.
    LOADING_DELAY : constant Time_Span := Seconds( 2 );
    FADE_OUT_TIME : constant Time_Span := Milliseconds( 500 );
    FADE_IN_TIME  : constant Time_Span := Milliseconds( 500 );

private

    type Keen_View is new Game_View with
        record
            -- game play was paused explicitly by the player
            pausedByPlayer : Boolean := False;

            -- a count of the number of times the game as been automatically
            -- paused by the menu system. the menus must unpause the same number
            -- of times before game play may resume. the is a count instead of a
            -- boolean because of nested pause situations (e.g. the status board
            -- is being shown (pausedByMenu=1) and the player exits back to the
            -- menu (pausedByMenu=2).
            pausedByMenu : Integer := 0;

            -- minimum amount of time to display the loading screen. it will be
            -- hidden again when the game thread is finished loading, or the
            -- minimum loading screen delay has expired;  whichever comes last.
            loadingHideDelay : Time_Span := LOADING_DELAY;

            -- used to detect when loading a world from the command line fails
            userMapOk : Boolean := False;

            aspect      : A_Aspect_Layout;
            screenMgr   : A_Screen_Manager;
            scrGameplay : A_Game_Screen;
        end record;

    procedure Delete( this : in out Keen_View );

    -- This is called when the user clicks the X window button on the window.
    procedure On_Close_Window( this : access Keen_View; allowed : in out Boolean );

    procedure On_Message( this : access Keen_View;
                          name : String;
                          args : List_Value'Class );

end Game_Views.Keen;
